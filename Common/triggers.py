from ctypes import c_uint
from enum import Enum

from Common.register import Register


class CardType(Enum):
    HPCC = 0
    DPS = 1
    RTOS = 2
    EVENT = 5
    SCS = 9
    INVALID = 62
    LTC = 63


class TriggerEncoding(Register):
    _fields_ = [('payload', c_uint, 19),
                ('is_software_trigger', c_uint, 1),
                ('dut_domain_id', c_uint, 6),
                ('card_type', c_uint, 6)]


def generate_trigger(card_type, payload, is_software_trigger=False,
                     dut_domain_id=0):
    trigger = TriggerEncoding(card_type=get_card_type_value(card_type),
                              dut_domain_id=dut_domain_id,
                              is_software_trigger=int(is_software_trigger),
                              payload=payload).value
    return trigger


def get_card_type_value(name):
    name = name.lower()
    if 'hpcc' in name:
        return CardType.HPCC.value
    elif 'dps' in name:
        return CardType.DPS.value
    else:
        card_type = {'rtos': CardType.RTOS.value,
                     'event': CardType.EVENT.value,
                     'scs': CardType.SCS.value,
                     'invalid': CardType.INVALID.value,
                     'ltc': CardType.LTC.value}
        return card_type[name]


def is_software_trigger(trigger):
    encoded = TriggerEncoding(value=trigger)
    if encoded.card_type == CardType.RTOS.value:
        return True
    elif encoded.card_type == CardType.HPCC.value or \
            encoded.card_type == CardType.DPS.value:
        return encoded.is_software_trigger
    else:
        return False


class EventTriggerEncoding(Register):
    _fields_ = [('event_type', c_uint, 12),
                ('reserved_0', c_uint, 3),
                ('dps_internal_control', c_uint, 1),
                ('reserved_1', c_uint, 10),
                ('card_type', c_uint, 6)]


class EventType(Enum):
    S2H_STOP = 0
    S2H_START = 1
    S2H_PACKET_COUNTER_RESET = 2
    SCOPESHOT_STOP = 3
    SCOPESHOT_START = 4
    TIME_STAMP_RESET = 5


class SCSTriggerEncoding(Register):
    _fields_ = [('sensor_data', c_uint, 10),
                ('reserved', c_uint, 2),
                ('user_id', c_uint, 10),
                ('pin_id', c_uint, 2),
                ('ac_slice_id', c_uint, 2),
                ('card_type', c_uint, 6)]


class SCSMemoryEncoding(Register):
    _fields_ = [('sensor_data', c_uint, 10),
                ('reserved_0', c_uint, 2),
                ('user_id', c_uint, 10),
                ('pin_id', c_uint, 2),
                ('ac_slice_id', c_uint, 2),
                ('link_num', c_uint, 4),
                ('reserved_1', c_uint, 2)]