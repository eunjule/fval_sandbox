################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: dps.py
#-------------------------------------------------------------------------------
#     Purpose: Models the behavior of a HDDPS
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/01/15
#       Group: HDMT FPGA Validation
################################################################################

from _build.bin import hddpstbc
from Common import configs
from Common import fval
from Common import hilmon

class Hddps(fval.Component):
    def __init__(self, slot, name = 'Hddps'):
        super().__init__(name)
        self.slot = slot
        self.hddpsSim = [
            hddpstbc.MotherboardSimulator(slot, 0, configs.HDDPS_BOARD_SIM_MEM),
            hddpstbc.DaughterboardSimulator(slot, 1, configs.HDDPS_BOARD_SIM_MEM),
        ]
        hilmon.RegisterMonitor('dps', self)

    def ConnectRcModel(self, rcModel):
        self.hddpsSim[0].Connect(rcModel.sim)
        self.hddpsSim[1].Connect(rcModel.sim)

    def hddpsConnect(self, slot, subslot):
        pass

    def hddpsDisconnect(self, slot, subslot):
        pass

    def hddpsBarRead(self, slot, subslot, bar, offset, data):
        if slot == self.slot:
            self.hddpsSim[subslot].BarRead(bar, offset, data)

    def hddpsBarWrite(self, slot, subslot, bar, offset, data):
        if slot == self.slot:
            self.hddpsSim[subslot].BarWrite(bar, offset, data)

    def hddpsDmaRead(self, slot, subslot, address, data):
        if slot == self.slot:
            self.hddpsSim[subslot].DmaRead(address, data)

    def hddpsDmaWrite(self, slot, subslot, address, data):
        if slot == self.slot:
            self.hddpsSim[subslot].DmaWrite(address, data)

