################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: hpcc.py
#-------------------------------------------------------------------------------
#     Purpose: Models the behavior of a HPCC, against input and expect data
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/01/15
#       Group: HDMT FPGA Validation
################################################################################

from _build.bin import hpcctbc
from Hpcc import hpcc_configs as configs
from Common import hilmon
from Common.fval import Component

class Hpcc(Component):
    def __init__(self, slot, name = 'Hpcc'):
        super().__init__(name)
        self.slot = slot
        self.acSim = []
        for slice in range(2):
            sim = hpcctbc.HpccAcSimulator(slot, slice, configs.HPCC_AC_SIM_MEM)
            self.acSim.append(sim)
        hilmon.RegisterMonitor('hpccAc', self)

    def ConnectRcModel(self, rcModel):
        self.acSim[0].Connect(rcModel.sim)
        self.acSim[1].Connect(rcModel.sim)

    def hpccAcConnect(self, slot):
        pass

    def hpccAcDisconnect(self, slot):
        pass

    def hpccAcBarRead(self, slot, fpgaIndex, bar, offset, data):
        if slot == self.slot:
            self.acSim[fpgaIndex].BarRead(bar, offset, data)

    def hpccAcBarWrite(self, slot, fpgaIndex, bar, offset, data):
        if slot == self.slot:
            self.acSim[fpgaIndex].BarWrite(bar, offset, data)

    def hpccAcDmaRead(self, slot, fpgaIndex, address, data):
        if slot == self.slot:
            self.acSim[fpgaIndex].DmaRead(address, data)

    def hpccAcDmaWrite(self, slot, fpgaIndex, address, data):
        if slot == self.slot:
            self.acSim[fpgaIndex].DmaWrite(address, data)

    def hpccDcConnect(self, slot):
        pass

    def hpccDcDisconnect(self, slot):
        pass

    def hpccDcBarRead(self, slot, fpgaIndex, bar, offset):
        pass

    def hpccDcBarWrite(self, slot, fpgaIndex, bar, offset, data):
        pass

    def hpccDcDmaRead(self, slot, fpgaIndex, address, length):
        pass

    def hpccDcDmaWrite(self, slot, fpgaIndex, address, data):
        pass

