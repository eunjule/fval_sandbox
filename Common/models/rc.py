################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: rc.py
#-------------------------------------------------------------------------------
#     Purpose: Models the behavior of RC
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 02/24/16
#       Group: HDMT FPGA Validation
################################################################################

from _build.bin import rctbc
from Common.fval import Component


class Rc(Component):
    def __init__(self, name = 'Rc'):
        super().__init__(name)
        self.sim = rctbc.RcSimulator()

