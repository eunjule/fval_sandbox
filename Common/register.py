################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes
import types

def create_field_dictionary(*args):
    table = {}
    for arg in args:
        for field in arg.fields():
            table[field] = getattr(arg, field)
    return table

def get_field_names(*args):
    names = []
    for arg in args:
        names.extend(arg.fields())
    return names

class Register(ctypes.LittleEndianStructure):
    """Register base class - defines setter/getter for integer value of register"""
    BAR = 0
    ADDR = 0
    BASEMUL = 4
    REGCOUNT = 1

    def __init__(self, value=None, **kwargs):
        super().__init__()
        if value is not None and isinstance(value, int):
            self.value = value
        for field, value in kwargs.items():
            if not hasattr(self, field):
                raise AttributeError(f'{self.__class__.__name__} does not '
                                     f'have attribute {field}')
            setattr(self, field, value)
    
    def Pack(self):
        return self.value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i.to_bytes(4, byteorder='little'), 4)

    @classmethod
    def address(cls, index=0):
        min_index = 0
        max_index = cls.REGCOUNT - 1
        if not min_index <= index <= max_index:
            raise Register.IndexOutOfRangeError(f'Invalid index of {index}. Valid range is {min_index} to {max_index}')
        return cls.ADDR + index * cls.BASEMUL

    @classmethod
    def index(cls, address):
        if cls.BASEMUL == 0:
            raise Register.RegisterError(f'{cls.name()}\'s BASEMUL value '
                                         f'cannot be zero.')
        index = int((address - cls.ADDR) / cls.BASEMUL)
        return index

    class IndexOutOfRangeError(Exception):
        pass

    class RegisterError(Exception):
        pass

    @classmethod
    def fields(cls):
        return [x[0] for x in cls._fields_]

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        try:
            l.append('ADDR=0x{:04X}'.format(self.ADDR))
        except AttributeError:
            pass
        try:
            l.append('REGCOUNT={}'.format(self.REGCOUNT))
        except AttributeError:
            pass
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)

    def name(self):
        return type(self).__name__.upper()
