# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


def get_device_class(instrument_name):
    return _InstrumentClass.get_class(instrument_name)


class _InstrumentClass:
    @classmethod
    def get_class(cls, instrument_name):
        """Use the method with the same name as the instrument"""
        return getattr(cls, instrument_name.lower())()

    @staticmethod
    def hbirctc():
        from Hbirctc.instrument.hbirctc import Hbirctc
        return Hbirctc

    @staticmethod
    def hddps():
        from Common.instruments.dps.hddps import Hddps
        return Hddps

    @staticmethod
    def hddpsmodel():
        from Common.models.hddps import Hddps as HddpsModel
        return HddpsModel

    @staticmethod
    def hvdpscombo():
        from Common.instruments.dps.hvdpsCombo import HvdpsCombo
        return HvdpsCombo

    @staticmethod
    def hpcc():
        from Hpcc.instrument.hpcc import Hpcc
        return Hpcc

    @staticmethod
    def hpccmodel():
        from Common.models.hpcc import Hpcc as HpccModel
        return HpccModel

    @staticmethod
    def tdaubnk():
        from Tdaubnk.instrument.tdaubnk import Tdaubnk
        return Tdaubnk

    @staticmethod
    def rc2():
        from Rc2.instrument.rc2 import Rc2
        return Rc2

    @staticmethod
    def hbicc():
        from Hbicc.instrument.hbicc import Hbicc
        return Hbicc

    @staticmethod
    def patgen():
        from Hbicc.instrument.patgen import PatGen
        return PatGen

    @staticmethod
    def ringmultiplier():
        from Hbicc.instrument.ringmultiplier import RingMultiplier
        return RingMultiplier

    @staticmethod
    def pinmultiplier():
        from Hbicc.instrument.pinmultiplier import PinMultiplier
        return PinMultiplier

    @staticmethod
    def psdb():
        from Hbicc.instrument.psdb import Psdb
        return Psdb

    @staticmethod
    def mainboard():
        from Hbicc.instrument.mainboard import MainBoard
        return MainBoard

    @staticmethod
    def hbidps():
        from Hbidps.instrument.hbidps import Hbidps
        return Hbidps

    @staticmethod
    def backplane():
        from Common.instruments.backplane import Backplane
        return Backplane

    @staticmethod
    def cb2():
        from Common.instruments.cb2 import Cb2
        return Cb2

    @staticmethod
    def hbidtb():
        from Hbidtb.instrument.hbidtb import Hbidtb
        return Hbidtb

    @staticmethod
    def rc3():
        from Rc3.instrument.rc3 import Rc3
        return Rc3
