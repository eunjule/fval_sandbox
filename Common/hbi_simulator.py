# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval
from Common.blt_simulator import BLT
from Common.instruments.s2h_interface import S2hBufferIndex
from Common.mainboard_simulator import MainBoard
from Hbicc.testbench.patgen_simulator import PatGen
from Hbicc.testbench.psdb_simulator import Psdb
from Hbicc.testbench.ring_multiplier_simulator import RingMultiplier
from Hbirctc.testbench.hbirctc_simulator import Rc
from Hbidps.testbench.hbidps_simulator import Hbidps
from ThirdParty.HIL.x64.Release import hil as real_hil


class HbiSimulator(fval.Object):
    def __init__(self, name='HbiSimulator'):
        super().__init__(name)

        self.name = name
        self.rc = Rc(system=self)
        self.patgen = PatGen(system=self)
        self.ring_multiplier = RingMultiplier(system=self)
        self.psdb = [Psdb(slot=x, system=self) for x in range(2)]
        self.ring_multiplier.connect_psdb(self.psdb)
        self.mainboard_device = {0:self.rc, 1:self.patgen, 2:self.ring_multiplier}
        self.mainboard = MainBoard()
        self.dps = [Hbidps(slot) for slot in range(16)]

    def hbiMbBarWrite(self, index, bar, offset, data):
        self.mainboard_device[index].bar_write(bar, offset, data)

    def hbiMbBarRead(self, index, bar, offset):
        return self.mainboard_device[index].bar_read(bar, offset)

    def hbiMbDmaRead(self, fpga, offset, length):
        return self.mainboard_device[fpga].dma_read(offset, length)

    def hbiMbDmaWrite(self, fpga, offset, data):
        return self.mainboard_device[fpga].dma_write(offset, data)

    def hbiMbBltBoardRead(self):
        return self.mainboard.read_blt()

    def hbiPsdbBarWrite(self, slot, index, bar, offset, data):
        self.psdb[slot].pin_multiplier[index].bar_write(bar, offset, data)
    
    def hbiPsdbBarRead(self, slot, index, bar, offset):
        return self.psdb[slot].pin_multiplier[index].bar_read(bar, offset)

    def hbiMbSi5344Read(self, register):
        return self.patgen.si5344_read(register)

    def hbiMbSi5344Write(self, register, data):
        self.patgen.si5344_write(register, data)

    def hbiMbFpgaVersionString(self, fpga):
        device = [self.rc, self.patgen, self.ring_multiplier][fpga]
        return device.fpga_version_string()

    def hbiMbFpgaVersion(self, fpga):
        device = [self.rc, self.patgen, self.ring_multiplier][fpga]
        return device.fpga_version()

    def hbiPsdbFpgaVersionString(self, slot, fpga):
        return self.psdb[slot].pin_multiplier[fpga].fpga_version_string()

    def hbiPsPmbusRead(self, interface_num, cmd, byte_length):
        data = self.rc.power_supplies[interface_num].pmbus_read(cmd)
        return data

    def hbiPsPmbusWrite(self, interface_num, cmd, byte_data):
        self.rc.power_supplies[interface_num].pmbus_write(
            cmd, byte_data)

    def trigger_down(self, data):
        self.patgen.trigger_down(data)
        self.ring_multiplier.trigger(data)

    def hbiDpsBltBoardRead(self, slot):
        return self.dps[slot].read_blt()

    def hbiDpsBarRead(self, slot, bar, offset):
        return self.dps[slot].bar_read(bar, offset)

    def hbiDpsBarWrite(self, slot, bar, offset, data):
        return self.dps[slot].bar_write(bar, offset, data)

    def hbiDpsLtc2975PmbusWrite(self, slot, chip, page, command, data):
        self.dps[slot].ltc2975_write(chip, page, command, data)

    def hbiMbFpgaLoad(self, fpga_index, image):
        if fpga_index == 0:
            self.rc.hbirctc_fpga_load(image)
        else:
            pass

    def hilFailsafeDisable(self, boolean):
        self.rc.is_fail_safe_disable = boolean

    def hbiMbMax10CpldWrite(self, address, data_list):
        for data in data_list:
            self.rc.max10.write_word(address, data)
            address += 1

    def hbiMbMax10CpldRead(self, address, num_words):
        data = []
        for i in range(num_words):
            data.append(self.rc.max10.read_word(address))
            address += 1
        return data

    def hbiMbStreamingBufferEx(self, index):
        buf_name = S2hBufferIndex(index).name.lower()
        return getattr(self.rc, f's2h_{buf_name}').stream_info()

    def hbiPsdbVmonRead(self, slot, channel):
        if channel in [1, 2, 3, 4]:
            dac_channel = channel - 1
            return self.ring_multiplier.ad5684r.voltage(dac_channel)
        else:
            return 30.1

    def hbiPsdbVmonReadAll(self, slot):
        vmon_read_all = [30.1] * 9
        for channel in [1, 2, 3, 4]:
            dac_channel = channel - 1
            vmon_read_all[channel] = \
                self.ring_multiplier.ad5684r.voltage(dac_channel)
        vmon_read_all[8] = \
            self.ring_multiplier.ltm4678.voltage()
        return vmon_read_all

    def rcConnect(self):
        raise RuntimeError('The specified device cannot be found.')

    def rc3Connect(self):
        raise RuntimeError('The specified device cannot be found.')

    def hbiMbConnect(self):
        pass

    def hbiMbDeviceDisable(self, fpga):
        pass

    def hbiMbDeviceEnable(self, fpga):
        pass

    def pciDeviceOpen(self, guid, viddid, slot, additional):
        pass

    def pciDeviceClose(self, handle):
        pass

    def hbiDpsConnect(self, slot):
        pass

    def hbiDpsDisconnect(self, slot):
        pass

    def hbiDpsDeviceDisable(self, slot):
        pass

    def hbiDpsDeviceEnable(self, slot):
        pass

    def hbiDpsFpgaLoad(self, slot, image):
        pass

    def hbiDpsFpgaVersionString(self, slot):
        return 'x.y.z'

    def hbiDpsCpldVersionString(self, slot):
        return 'x.y.z'

    def hbiDpsInit(self, slot):
        pass

    def hbiDpsISourceVsenseMax14662Read(self, slot, chip):
        return 0

    def hbiDpsISourceVsenseMax14662Write(self, slot, chip, data):
        pass

    def hbiDpsVtVmLbMax14662Read(self, slot, chip):
        return 0

    def hbiDpsVtVmLbMax14662Write(self, slot, chip, data):
        pass

    def hbiDpsLcmGangMax14662Write(self, slot, chip, data):
        self.dps[slot].lcm_gang_max14662_write(chip, data)

    def hbiDpsLtc2975PmbusRead(self, slot, chip, page, command, length):
        return self.dps[slot].ltc2975_read(chip, page, command, length)

    def hbiDpsLtm4680PmbusWrite(self, slot, chip, page, command, pCmdData):
        pass

    def hbiDpsLtm4680PmbusRead(self, slot, chip, page, command, length):
        return b'\x00'*length

    def hbiDpsTmonRead(self, slot, channel):
        return self.dps[slot].temperature_monitor_read(channel)

    def hbiDpsVmonRead(self, slot, channel):
        return self.dps[slot].voltage_monitor_read(channel)

    def hbiDpsDmaRead(self, slot, address, length):
        return self.dps[slot].dma_read(address, length)

    def hbiDpsDmaWrite(self, slot, address, data):
        self.dps[slot].dma_write(address, data)

    def hbiDtbTmonRead(self, dtb_index, channel):
        return 30

    def hbiPsdbBltBoardRead(self, slot):
        blt = BLT()
        blt.PartNumberCurrent = 'AAJ97698-301'
        return blt

    def hbiMbHpsDmaWrite(self, offset, data):
        pass

    def hbiMbCpldVersionString(self, index):
        return 'x.y.z'

    def hbiMbCpldVersionString(self, index):
        return 'x.y.z'

    def hbiMbFanRead(self, chip_num):
        return self.rc.fan_rps(chip_num)

    def hbiMbTmonRead(self, channel):
        if channel == 11:
            rc_temperature_register = 0xA04
            return self.hbiMbBarRead(
                index=0, bar=1, offset=rc_temperature_register) / 256
        elif channel == 12:
            patgen_temperature_register = 0x30
            return self.hbiMbBarRead(
                index=1, bar=1, offset=patgen_temperature_register) / 256
        elif channel == 13:
            ring_multiplier_temperature_register = 0x30
            return self.hbiMbBarRead(
                index=2, bar=1, offset=ring_multiplier_temperature_register) \
                   / 256
        else:
            return 30.5

    def hbiPsdbDeviceDisable(self, slot, index):
        pass

    def hbiPsdbDeviceEnable(self, slot, index):
        pass

    def hbiPsdbFpgaLoad(self, slot, index, image):
        pass

    def hbiPsdbCpldVersionString(self, slot, index):
        return 'x.y.z'

    def hbiPsdbTmonRead(self, slot, fpga):
        return 30.1

    def hilToL11(self, data):
        return real_hil.hilToL11(data)

    def hilFromL11(self, data):
        return real_hil.hilFromL11(data)

    def hilToL16(self, data, mode):
        return real_hil.hilToL16(data, mode)

    def hilFromL16(self, data, mode):
        return real_hil.hilFromL16(data, mode)

    def hbiMbAlarmWait(self):
        return self.rc.alarm_wait()

    def hbiMbAlarmWaitCancel(self):
        self.rc.alarm_wait_cancel()

    def hbiDtbPca9505Write(self, dtb_index, chip, reg, data):
        pass

    def hbiDtbPca9505Read(self, dtb_index, chip, reg):
        return 0
