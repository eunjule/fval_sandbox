################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from Common import fval

class MainBoard(fval.Object):
    def __init__(self):
        self.blt = Blt()

    def read_blt(self):
        return self.blt

class Blt():
    def __init__(self):
        self.DeviceName = 'MbDeviceName'
        self.VendorName = 'MbVendorName'
        self.PartNumberAsBuilt = 'AAK15216-403'
        self.PartNumberCurrent = 'AAK15216-403'
        self.SerialNumber = 'MbSerialNumber'
        self.ManufactureDate = 'MbManufactureDate'
