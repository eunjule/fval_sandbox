################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: globals.py
#-------------------------------------------------------------------------------
#     Purpose: global variables for all
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 06/30/15
#       Group: HDMT FPGA Validation
################################################################################

### HPCC AC ###
CHANNEL_PER_FPGA = 56
ALL_PINS = list(range(CHANNEL_PER_FPGA))
EVEN_PINS = list(range(0,CHANNEL_PER_FPGA,2))
ODD_PINS = list(range(1,CHANNEL_PER_FPGA,2))
PICO_SECONDS_PER_SECOND = 1000000000000
MINIMUM_PRECISION = 10
