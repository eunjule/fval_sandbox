# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""
S2H transfers sensor data from its Mailbox (MB) Ram to the SiteC Ram. The HPS
Firmware loads bulk data into the MB which is eventually streamed to the SiteC
using S2H. HPS is a hard processor within the FPGA.

S2H register definitions:
- S2H Packet Byte Count: Size of a single packet
- S2H Valid Packet Index: Index of last packet written
- S2H Packet Count: Number of packets in stream
- S2H Status: Check if stream is enabled
- S2H Control: Reset controller, reset stream, stop and start stream
"""
from time import perf_counter

from .s2h_bulk_data_base import S2hBulkDataBase
from .s2h_system_power_monitoring_base import S2hSystemPowerMonitoringBase
from Common.fval import Object
from Common.instruments.base import Instrument
from Common.instruments.s2h_interface import S2hInterface
from Common.utilities import performance_sleep


class Functional(Object, S2hBulkDataBase, S2hSystemPowerMonitoringBase):
    TEST_ITERATIONS = 10
    MAX_FAIL_COUNT = 3
    PACKET_HEADER_TEST_ITERATIONS = 10

    def __init__(self, stream, rc_test):
        super().__init__()
        self.stream = stream
        self.rc_test = rc_test
        self.rc_test.max_fail_count = Functional.MAX_FAIL_COUNT
        self.buffer_name = stream.buffer_index().name.lower()
        Functional.PACKET_HEADER_TEST_ITERATIONS = \
            self.stream.packet_count() * 2

    def DirectedValidatePacketHeadersTest(self):
        self.set_test_iterations(Functional.PACKET_HEADER_TEST_ITERATIONS)
        failed_entries = []

        with S2hTester(self.stream):
            self.stream.wait_on_valid_valid_packet_index()
            entries = self.get_packet_entries(self.rc_test.test_iterations)

            previous_index = entries[0].get('valid_packet_index')
            previous_entry = entries[0]
            previous_packet_counter = self._packet_counter(previous_entry)

            num_entries = len(entries)
            for iteration in range(1, num_entries):
                current_index = entries[iteration].get('valid_packet_index')
                current_entry = entries[iteration]

                if not self._validate_header(current_entry, previous_index,
                                             previous_packet_counter):
                    previous_entry = entries[iteration - 1]
                    next_entry = entries[iteration + 1]

                    failed_entries.append(previous_entry)
                    failed_entries.append(current_entry)
                    if iteration < (num_entries - 1):
                        failed_entries.append(next_entry)

                    self.rc_test.update_failed_iterations(success=False)

                previous_index = current_index
                previous_packet_counter = self._packet_counter(current_entry)

                if self.rc_test.fail_count >= self.rc_test.max_fail_count:
                    break

        if failed_entries:
            table = Instrument.contruct_text_table(data=failed_entries)
            self.rc_test.Log('error', table)

        self.rc_test.validate_iterations()

    def set_test_iterations(self, num_iterations):
        self.rc_test.fail_count = 0
        self.rc_test.test_iterations = num_iterations

    def _starting_packet_index(self, stream):
        method_name = f'{self.buffer_name}_starting_packet_index'
        return getattr(self, method_name)(stream)

    def _create_table_entry_from_stream(self, iteration, index):
        method_name = f'{self.buffer_name}_create_table_entry_from_stream'
        return getattr(self, method_name)(self.stream, iteration, index)

    def _packet_counter(self, entry):
        return getattr(self, f'{self.buffer_name}_packet_counter')(entry)

    def _validate_header(self, current_entry, previous_index,
                         previous_packet_counter):
        method_name = f'{self.buffer_name}_validate_header'
        return getattr(self, method_name)(
            self.stream, current_entry, previous_index, previous_packet_counter)

    def _validate_header_changes(self, previous_entry, current_entry):
        method_name = f'{self.buffer_name}__validate_header_changes'
        return getattr(self, method_name)(self.stream, previous_entry,
                                          current_entry)

    def DirectedValidatePacketTest(self):
        self.set_test_iterations(self.rc_test.test_iterations)

        with S2hTester(self.stream):
            self.stream.wait_on_valid_valid_packet_index()
            entries = self.get_packet_entries(self.rc_test.test_iterations)

            self.rc_test.fail_count = self.validate_packets(entries)
            self.rc_test.validate_iterations()

    def get_packet_entries(self, num_packets):
        packet_refresh_secs = self.stream.PACKET_REFRESH_MS / 1e3

        start_index = self.stream.valid_packet_index()
        for i in range(num_packets-1):
            start = perf_counter()
            self.advance_valid_index()
            performance_sleep(packet_refresh_secs - (perf_counter() - start))
        self.stream.disable_stream()
        stop_index = self.stream.valid_packet_index()

        if start_index >= stop_index:
            first_half = (self.stream.packet_count() - start_index) + 1
            second_half = stop_index + 1
            num_packets_captured = first_half + second_half
        else:
            num_packets_captured = (stop_index - start_index) + 1

        index = start_index
        entries = []
        for i in range(num_packets_captured-1):
            entries.append(self._create_table_entry_from_stream(0, index))
            index = self.stream.increment_valid_packet_index(index)

        return entries

    def advance_valid_index(self):
        """Do not remove. Needed when running simulation"""
        self.stream.valid_packet_index()

    def validate_packets(self, entries):
        failed_entries = []
        for i in range(len(entries)-1):
            method_name = f'{self.buffer_name}_validate_packets'
            if not getattr(self, method_name)(
                    self.stream, entries[i], entries[i+1]):
                failed_entries.append(entries[i])
                failed_entries.append(entries[i+1])

            if len(failed_entries) >= self.rc_test.max_fail_count:
                break

        if failed_entries:
            table = Instrument.contruct_text_table(data=failed_entries)
            self.rc_test.Log('error', table)

        return len(failed_entries)

    def DirectedEnableStreamTest(self):
        self.set_test_iterations(self.rc_test.test_iterations)

        with S2hTester(self.stream):
            previous_index = self.stream.wait_on_valid_valid_packet_index()
            self.stream.wait_on_packet_refresh()

            for iteration in range(self.rc_test.test_iterations):
                self.stream.wait_on_packet_refresh()
                current_index = self.stream.valid_packet_index()

                if (previous_index == current_index) or \
                        (current_index == S2hInterface.INVALID_PACKET_INDEX):
                    self.rc_test.Log('error', self.error_msg_enable_stream(
                        iteration=iteration,
                        current=current_index,
                        previous=previous_index))
                    self.rc_test.update_failed_iterations(success=False)
                previous_index = current_index

                if self.rc_test.fail_count >= self.rc_test.max_fail_count:
                    break

        self.rc_test.validate_iterations()

    def DirectedDisableStreamTest(self):
        self.set_test_iterations(Functional.TEST_ITERATIONS)

        with S2hTester(self.stream):
            for iteration in range(self.rc_test.test_iterations):
                self.stream.disable_stream()
                current_index = self.stream.valid_packet_index()
                self.stream.wait_on_packet_refresh()
                next_index = self.stream.valid_packet_index()

                if current_index != next_index:
                    self.rc_test.Log('error', self.error_msg_disable_stream(
                        iteration=0,
                        current_index=current_index,
                        next_index=next_index))
                    self.rc_test.update_failed_iterations(success=False)

                self.stream.enable_stream()
                self.stream.wait_on_valid_valid_packet_index()

                if self.rc_test.fail_count >= self.rc_test.max_fail_count:
                    break

        self.rc_test.validate_iterations()

    def DirectedResetStreamTest(self):
        self.set_test_iterations(Functional.TEST_ITERATIONS)

        with S2hTester(self.stream):
            for iteration in range(self.rc_test.test_iterations):
                self.stream.reset_stream()
                for i in range(3):
                    self.stream.wait_on_packet_refresh()
                index = self.stream.valid_packet_index()
                if index != S2hInterface.INVALID_PACKET_INDEX:
                    self.rc_test.Log('error', self.error_msg_reset_stream(
                        iteration=iteration,
                        index=index))
                    self.rc_test.update_failed_iterations(success=False)

                self.stream.enable_stream()
                self.stream.wait_on_valid_valid_packet_index()

                if self.rc_test.fail_count >= self.rc_test.max_fail_count:
                    break

        self.rc_test.validate_iterations()

    def error_msg_enable_stream(self, iteration, previous, current):
        return f'Iteration {iteration}: Valid packet index did not change (' \
               f'previous,  current): {previous}, {current}'
    
    def error_msg_disable_stream(self, iteration, current_index, next_index):
        return f'Iteration {iteration}: Valid packet index register changed ' \
               f'(previous, current): {next_index}, {current_index}'
    
    def error_msg_reset_stream(self, iteration, index):
        return f'Iteration {iteration}: Valid Packet Index invalid ' \
               f'(expected, actual): ' \
               f'{S2hInterface.INVALID_PACKET_INDEX}, {index}'


class S2hTester():
    def __init__(self, stream):
        self.stream = stream

    def __enter__(self):
        self.stream.reset_stream()
        self.stream.enable_stream()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stream.disable_stream()
