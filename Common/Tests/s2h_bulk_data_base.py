# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from Common.instruments.s2h_interface import S2hBDHeaderId

class S2hBulkDataBase():

    @staticmethod
    def bd_create_table_entry_from_stream(stream, iteration, index):
        packet = stream.stream_packet(index)
        signature = packet.packet_signature
        bulk_packet_version = packet.bulk_packet_version
        packet_counter = packet.packet_counter

        entry = {'iteration': iteration,
                 S2hBDHeaderId.packet_signature.name: f'0x{signature:08X}',
                 S2hBDHeaderId.bulk_packet_version.name: bulk_packet_version,
                 'packet_counter': packet_counter,
                 'valid_packet_index': index,
                 'errors': ''}
        return entry

    @staticmethod
    def bd_validate_header(stream, current_entry, previous_index,
                         previous_packet_counter):
        error_fields = []

        if (current_entry.get(S2hBDHeaderId.packet_signature.name) !=
                f'0x{S2hBDHeaderId.packet_signature.value:08X}'):
            error_fields.append(S2hBDHeaderId.packet_signature.name)

        if (current_entry.get(S2hBDHeaderId.bulk_packet_version.name) !=
                S2hBDHeaderId.bulk_packet_version.value):
            error_fields.append(S2hBDHeaderId.bulk_packet_version.name)

        max_counter = (2 ** 32) - 1
        expected_current_packet_counter = \
            (previous_packet_counter + 1) % max_counter
        if (expected_current_packet_counter !=
                current_entry.get('packet_counter')):
            error_fields.append('packet_counter')

        expected_current_index = stream.increment_valid_packet_index(
            previous_index)
        if (expected_current_index != current_entry.get('valid_packet_index')):
            error_fields.append('valid_packet_index')

        current_entry['errors'] = str(error_fields).strip('[]')

        return (len(error_fields) == 0)

    @staticmethod
    def bd_validate_packets(stream, previous_entry, current_entry):
        error_fields = \
            S2hBulkDataBase.bd_validate_header_changes(
                stream, previous_entry, current_entry)
        error_fields.extend(
            S2hBulkDataBase.bd_validate_data(current_entry))

        current_entry['errors'] = str(error_fields).strip('[]')

        return (len(error_fields) == 0)

    @staticmethod
    def bd_validate_header_changes(stream, previous_entry, current_entry):
        error_fields = []

        if not (previous_entry.get(S2hBDHeaderId.packet_signature.name) ==
                current_entry.get(S2hBDHeaderId.packet_signature.name) ==
                f'0x{S2hBDHeaderId.packet_signature.value:08X}'):
            error_fields.append(S2hBDHeaderId.packet_signature.name)

        if not (previous_entry.get(S2hBDHeaderId.bulk_packet_version.name) ==
                current_entry.get(S2hBDHeaderId.bulk_packet_version.name) ==
                S2hBDHeaderId.bulk_packet_version.value):
            error_fields.append(S2hBDHeaderId.bulk_packet_version.name)

        max_counter = (2 ** 32) - 1
        expected_current_packet_counter =\
            (previous_entry.get('packet_counter') + 1) % max_counter
        if (expected_current_packet_counter !=
                current_entry.get('packet_counter')):
            error_fields.append('packet_counter')

        expected_current_index = stream.increment_valid_packet_index(
            previous_entry.get('valid_packet_index'))
        if (expected_current_index != current_entry.get('valid_packet_index')):
            error_fields.append('valid_packet_index')

        return error_fields

    @staticmethod
    def bd_validate_data(current_entry):
        error_fields = []
        return error_fields

    @staticmethod
    def bd_packet_counter(entry):
        return entry.get('packet_counter')

    @staticmethod
    def bd_starting_packet_index(stream):
        return stream.wait_on_valid_valid_packet_index()
