# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from math import isclose

from Common.instruments import s2h_interface


class S2hSystemPowerMonitoringBase():

    @staticmethod
    def spm_create_table_entry_from_stream(stream, iteration, index):
        packet = stream.stream_packet(index)

        entry = {
            'iteration': f'{iteration}',
            'valid_packet_index': index,
            'packet_header_count': packet.packet_header_count,
            'bps_0_present': packet.bps_0_present,
            'bps_1_present': packet.bps_1_present,
            'bps_2_present': packet.bps_2_present,
            'spm_enable': packet.spm_enable,
            'packet_type': packet.packet_type,
            'time_stamp_us': packet.time_stamp_us,
            'pout_bps_0': f'{packet.pout_bps_0:.02f}',
            'pout_bps_1': f'{packet.pout_bps_1:.02f}',
            'pout_bps_2': f'{packet.pout_bps_2:.02f}',
            'pout_sum': f'{packet.pout_sum:.02f}',
            'errors': ''
        }
        return entry

    @staticmethod
    def spm_validate_header(stream, current_entry, previous_index,
                         previous_packet_counter):
        error_fields = []

        max_counter = (2 ** 32) - 1
        expected_current_packet_counter = \
            (previous_packet_counter + 1) % max_counter
        if expected_current_packet_counter != \
                current_entry.get('packet_header_count'):
            error_fields.append('packet_header_count')

        expected_present = _bps_present_from_bar_register()
        if current_entry.get('bps_0_present') != expected_present[0]:
            error_fields.append(f'bps_0_present '
                                f'(expected: {expected_present[0]})')
        if current_entry.get('bps_1_present') != expected_present[1]:
            error_fields.append(f'bps_1_present '
                                f'(expected: {expected_present[0]})')
        if current_entry.get('bps_2_present') != expected_present[2]:
            error_fields.append(f'bps_2_present '
                                f'(expected: {expected_present[0]})')

        expected_enable = _spm_enable_from_bar_register()
        if current_entry.get('spm_enable') != expected_enable:
            error_fields.append(f'spm_enable (expected: {expected_enable}')

        if current_entry.get('packet_type') != S2hSPMHeaderId.packet_type:
            error_fields.append(f'packet_type '
                                f'(expected: {S2hSPMHeaderId.packet_type})')

        expected_index = stream.increment_valid_packet_index(previous_index)
        if expected_index != current_entry.get('valid_packet_index'):
            error_fields.append(f'valid_packet_index '
                                f'(expected: {expected_index}')

        expected_stamp = current_entry.get('time_stamp_us') + 4000
        tolerance = 0.01
        if not isclose(current_entry.get('time_stamp_us'), expected_stamp,
                       rel_tol=tolerance):
            error_fields.append(f'time_stamp_us (expected: {expected_stamp} '
                                f'within {tolerance * 100}%')

        current_entry['errors'] = str(error_fields).strip('[]')

        return (len(error_fields) == 0)

    @staticmethod
    def spm_validate_packets(stream, previous_entry, current_entry):
        error_fields = \
            S2hSystemPowerMonitoringBase.spm_validate_header_changes(
                stream, previous_entry, current_entry)
        error_fields.extend(
            S2hSystemPowerMonitoringBase.spm_validate_data(current_entry))

        current_entry['errors'] = str(error_fields).strip('[]')

        return (len(error_fields) == 0)

    @staticmethod
    def spm_validate_header_changes(stream, previous_entry, current_entry):
        error_fields = []
        actual_index = [previous_entry.get('valid_packet_index'),
                        current_entry.get('valid_packet_index')]

        max_counter = (2 ** 32) - 1
        expected_count = \
            (previous_entry.get('packet_header_count') + 1) % max_counter
        actual_count = current_entry.get('packet_header_count')
        if actual_count != expected_count:
            error_fields.append(f'index[{actual_index[1]}] packet_header_count'
                                f' (expected: {expected_count})')

        expected_bps = _bps_present_from_bar_register()
        num_bps= len(expected_bps)
        actual_bps = [[previous_entry.get(f'bps_{i}_present')
                       for i in range(num_bps)],
                      [current_entry.get(f'bps_{i}_present')
                       for i in range(num_bps)]]
        for i in range(len(actual_bps)):
            if not (actual_bps[i] == expected_bps):
                error_fields.append(
                    f'index[{actual_index[i]}] bps_present '
                    f'(expected: {expected_bps})')

        expected_enable = _spm_enable_from_bar_register()
        actual_enable = [previous_entry.get('spm_enable'),
                         current_entry.get('spm_enable')]
        for i in range(len(actual_enable)):
            if not (actual_enable[i] == expected_enable):
                error_fields.append(f'index[{actual_index[i]}] spm_enable '
                                    f'(expected: {expected_enable})')

        expected_type = s2h_interface.S2hSPMHeaderId.packet_type.value
        actual_type = [previous_entry.get('packet_type'),
                       current_entry.get('packet_type')]
        for i in range(len(actual_type)):
            if not (actual_type[i] == expected_type):
                error_fields.append(f'index[{actual_index[i]}] packet_type '
                                    f'(expected: {expected_type})')

        expected_valid_index = stream.increment_valid_packet_index(
            previous_entry.get('valid_packet_index'))
        actual_valid_index = current_entry.get('valid_packet_index')
        if (expected_valid_index != actual_valid_index):
            error_fields.append(
                f'index[{actual_index[1]}] valid_packet_index '
                f'(expected: {expected_valid_index}')

        actual_diff = current_entry.get('time_stamp_us') - \
                      previous_entry.get('time_stamp_us')
        expected_diff = int(s2h_interface.S2hInterface.PACKET_REFRESH_MS * 1e3)
        tolerance = s2h_interface.S2hInterface.TIME_STAMP_TOLERANCE
        if not isclose(actual_diff, expected_diff, rel_tol=tolerance):
            error_fields.append(
                f'index[{actual_index[0]}:{actual_index[1]}]) '
                f'time_stamp_us diff is {actual_diff} (expected: range '
                f'{expected_diff - (tolerance*100)} to '
                f'{expected_diff + (tolerance*100)})')

        return error_fields

    @staticmethod
    def spm_validate_data(current_entry):
        error_fields = []
        present_choice = _bps_present_from_bar_register()
        spm_enabled = _spm_enable_from_bar_register()
        index = current_entry.get('valid_packet_index')

        for i in range(len(present_choice)):
            if not spm_enabled or not bool(present_choice[i]):
                if current_entry.get(f'pout_bps_{i}') != 'nan':
                    error_fields.append(f'index[{index}]) pout_bps_{i} '
                                        f'(expected: nan)')
            else:
                if current_entry.get(f'pout_bps_{i}') == 'nan':
                    error_fields.append(f'index[{index}]) pout_bps_{i} '
                                        f'(expected: not nan)')

        if not spm_enabled or present_choice == [0, 0, 0]:
            if current_entry.get('pout_sum') != 'nan':
                error_fields.append(f'index[{index}]) pout_sum (expected: nan')
        else:
            expected_sum = float(current_entry.get('pout_bps_0')) \
                if bool(present_choice[0]) else 0
            expected_sum += float(current_entry.get('pout_bps_1')) \
                if bool(present_choice[1]) else 0
            expected_sum += float(current_entry.get('pout_bps_2')) \
                if bool(present_choice[2]) else 0
            if expected_sum != float(current_entry.get('pout_sum')):
                error_fields.append(f'index[{index}]) pout_sum '
                                    f'(expected: {expected_sum})')

        return error_fields

    @staticmethod
    def spm_packet_counter(entry):
        return entry.get('packet_header_count')

    @staticmethod
    def spm_starting_packet_index(stream):
        return 0


from Hbirctc.instrument.spm_interface import SpmInterface
_spm = SpmInterface()

def _bps_present_from_bar_register():
    return _spm.bps_present()

def _spm_enable_from_bar_register():
    return int(_spm.is_spm_enabled())

