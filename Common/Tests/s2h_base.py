################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import math
import time
from decimal import getcontext, Decimal
import unittest
from Common import fval
from Common import configs
no_valid_packet_data = 0xFFFF
S2H_packet_after_reset = 0xFFFF
packet_read_time = 120
min_S2H_packets_in_120_seconds = 30000                   # Packet count cannot be less then 30000 per 120 seconds(250*120).
max_S2H_packets_in_120_seconds = 30100                   # added buffer of 30 packets as software cannot generate exact delay
min_S2H_packets_per_sec = 250.0                   # Packet count cannot be less then 250 per second.
max_S2H_packets_per_sec = 252.0                   # added buffer of 2 packets as software cannot generate exact delay
seconds_to_reach_max_count = 5
seconds_to_reach_max_count_daughter_board = 13
s2h_streaming_rate = 0.004
expected_count_after_rollover = 3350

class S2HBase:

    def IsInTheRange(self, LowRange, HighRange, ReadBackValue):
        StepIncrease = 0.01
        startstep = LowRange
        while startstep < HighRange:
            ConvertToDecimal1 = Decimal(ReadBackValue)
            ReadBackValuePre3 = round(ConvertToDecimal1,2)
            ConvertToDecimal2 = Decimal(startstep)
            startstepPre3 = round(ConvertToDecimal2,2)
            if (ReadBackValuePre3 == startstepPre3):
                return True
            startstep += StepIncrease

    def CheckForCompatableFpga(self, dut):
        if dut.S2HSupport() == True:
            pass
        else:
            reason = 'This FPGA: {} does not support S2H'.format(hex(dut.GetFpgaVersion()))
            self.Log('warning', reason)
            raise unittest.SkipTest(reason)

    def DirectedS2HPacketCountControlByRegisterTest(self):
        for dut in self.env.duts:
            self.CheckForCompatableFpga(dut)
            dut.TurnOFFStreamingWithControlReg()
            dut.ResetStreamingWithControlReg()
            last_packet_idx, max_packets_in_site_controller = dut.S2HRegisterAndBufferSetup()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            dut.TurnONStreamingWithControlReg()
            start_timer = self.now()
            total_packets = 0
            while self.now() - start_timer < packet_read_time:
                self.WaitTime(3)
                total_packets += self.GetCount(last_packet_idx, max_packets_in_site_controller, dut)
            #total_packets += self.GetCount(last_packet_idx, max_packets_in_site_controller, dut)
            end_timer = self.now()
            dut.TurnOFFStreamingWithControlReg()
            if self.IsInTheRange(min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds, total_packets):
                self.Log('debug',
                         'Total packets when S2H control register used for S2H ON-OFF = {} Expected is between {} - {}'.format(total_packets, min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds))
            else:
                self.Log('error',
                         'Total packet count out of range when S2H control register used for S2H ON-OFF.Actual: {} Expected is between {} - {} '.format(total_packets,
                                                                                                                                                        min_S2H_packets_in_120_seconds,
                                                                                                                                                        max_S2H_packets_in_120_seconds))
            self.Log('info', 'Total Time = {} '.format(end_timer - start_timer))
            if self.IsInTheRange(min_S2H_packets_per_sec, max_S2H_packets_per_sec, (total_packets/(end_timer-start_timer))):
                self.Log('info', 'Packets per Second = {} '.format(total_packets / (end_timer - start_timer)))
            else:
                self.Log('error', 'Packets per Second count out of range.Actual:{} Expected is :{}'.format(total_packets / (end_timer - start_timer), min_S2H_packets_per_sec))

    def DirectedS2HPacketCountControlByTriggerTest(self):
        for dut in self.env.duts:
            self.CheckForCompatableFpga(dut)
            dut.reset_aurora_trigger_bus()
            dut.TurnOFFStreamingWithTrigger()
            dut.ResetStreamingWithControlReg()
            last_packet_index, max_packets_in_site_controller = dut.S2HRegisterAndBufferSetup()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            dut.TurnONStreamingWithTrigger()
            start_timer = self.now()
            total_packets_received = 0
            while self.now() - start_timer < packet_read_time:
                self.WaitTime(3)
                total_packets_received += self.GetCount(last_packet_index, max_packets_in_site_controller, dut)
            end_timer = self.now()
            dut.TurnOFFStreamingWithTrigger()
            if self.IsInTheRange(min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds, total_packets_received):
                self.Log('info',
                         'Total packets when Trigger register used for S2H ON-OFF = {} Expected is between {} - {}'.format(
                             total_packets_received, min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds))
            else:
                self.Log('error',
                         'Total packet count out of range when Trigger register used for S2H ON-OFF.Actual: {} Expected is between {} - {} '.format(
                             total_packets_received,
                             min_S2H_packets_in_120_seconds,
                             max_S2H_packets_in_120_seconds))
            self.Log('info', 'Total Time = {} '.format(end_timer - start_timer))
            if self.IsInTheRange(min_S2H_packets_per_sec, max_S2H_packets_per_sec,
                                 (total_packets_received / (end_timer - start_timer))):
                self.Log('info', 'Packets per Second = {} '.format(total_packets_received / (end_timer - start_timer)))
            else:
                self.Log('error', 'Packets per Second count out of range.Actual:{} Expected is :{}'.format(
                    total_packets_received / (end_timer - start_timer), min_S2H_packets_per_sec))

    def DirectedS2HTriggerONControlregOFFTest(self):
        for dut in self.env.duts:
            self.CheckForCompatableFpga(dut)
            dut.reset_aurora_trigger_bus()
            dut.StreamingBufferaddrsize()
            dut.TurnOFFStreamingWithTrigger()
            dut.ResetStreamingWithTrigger()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            dut.TurnONStreamingWithTrigger()
            self.WaitTime(3)
            total_packets_when_on = dut.GetTotalNoofPackets()
            dut.TurnOFFStreamingWithControlReg()
            self.WaitTime(1)
            total_packets_when_off = dut.GetTotalNoofPackets()
            self.Log('info', 'Trigger Turn OFF .Actual {} expected {} '.format(total_packets_when_off, total_packets_when_on))
            if total_packets_when_on != total_packets_when_off:
                self.Log('error', 'Turn OFF not successful.Actual {} expected {} '.format(total_packets_when_off, total_packets_when_on))
            dut.ResetStreamingWithControlReg()
            total_packets_when_off = dut.GetTotalNoofPackets()
            self.Log('info', 'Reset .Actual {} Expected {}'.format(hex(total_packets_when_off), hex(S2H_packet_after_reset)))
            if total_packets_when_off != S2H_packet_after_reset:
                    self.Log('error', 'Reset was not successful.Actual {} Expected {}'.format(hex(total_packets_when_off), hex(S2H_packet_after_reset)))

    def DirectedS2HControlregONTriggerOFFTest(self):
        for dut in self.env.duts:
            self.CheckForCompatableFpga(dut)
            dut.reset_aurora_trigger_bus()
            dut.StreamingBufferaddrsize()
            dut.TurnOFFStreamingWithControlReg()
            dut.ResetStreamingWithControlReg()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            dut.TurnONStreamingWithControlReg()
            self.WaitTime(3)
            total_packets_when_on = dut.GetTotalNoofPackets()
            dut.TurnOFFStreamingWithTrigger()
            self.WaitTime(1)
            total_packets_when_off = dut.GetTotalNoofPackets()
            self.Log('info', 'Control register turn OFF .Actual {} expected {} '.format(total_packets_when_off, total_packets_when_on))
            if total_packets_when_on != total_packets_when_off:
                self.Log('error', 'Turn OFF not successful.Actual {} expected {} '.format(total_packets_when_off, total_packets_when_on))
            dut.ResetStreamingWithControlReg()
            total_packets_when_off = dut.GetTotalNoofPackets()
            self.Log('info', 'Reset .Actual {} Expected {}'.format(hex(total_packets_when_off), hex(S2H_packet_after_reset)))
            if total_packets_when_off != S2H_packet_after_reset:
                    self.Log('error', 'Reset was not successful.Actual {} Expected {}'.format(hex(total_packets_when_off), hex(S2H_packet_after_reset)))

    def DirectedS2HPacketCountIncrementTest(self,no_of_iterations=45):
        for dut in self.env.duts:
            self.CheckForCompatableFpga(dut)
            dut.reset_aurora_trigger_bus()
            dut.StreamingBufferaddrsize()
            packet_count = dut.GetPacketCount()
            self.Log('info','Packet count {} '.format(packet_count))
            reset_interval = math.ceil(packet_count * s2h_streaming_rate)
            dut.TurnOFFStreamingWithControlReg()
            dut.ResetStreamingWithControlReg()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            previous_total_packets_when_OFF = 0
            for seconds_count in range(1,no_of_iterations, 1):
                dut.TurnONStreamingWithControlReg()
                self.WaitTime(1)
                dut.TurnOFFStreamingWithTrigger()
                total_packets_when_OFF = dut.GetTotalNoofPackets()
                self.Log('debug', 'Counter value after {} second: {} '.format(seconds_count, total_packets_when_OFF))
                total_packets_when_OFF = self.ErrorifPacketCounterDidNotReset(dut,seconds_count,reset_interval,total_packets_when_OFF,previous_total_packets_when_OFF)
                previous_total_packets_when_OFF = total_packets_when_OFF

    def DirectedS2HPacketCountResetInPacketHeaderTest(self,no_of_iterations=20):
        for dut in self.env.duts:
            self.CheckForCompatableFpga(dut)
            dut.reset_aurora_trigger_bus()
            addr, size = dut.StreamingBufferaddrsize()
            max_packets_in_site_controller, pkt_byte_count, buffer_size = dut.ReadStreamingDetails(addr)
            dut.TurnOFFStreamingWithControlReg()
            dut.ResetStreamingWithControlReg()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            for repeat_count in range(no_of_iterations):
                dut.TurnONStreamingWithControlReg()
                dut.WaitTime(2)
                dut.TurnOFFStreamingWithControlReg()
                packet_index_before_reset,packet_number_from_packet_header_before_reset=dut.ReadPacketIndexandPacketHeader(buffer_size)
                dut.ResetStreamingWithTrigger()
                dut.TurnONStreamingWithControlReg()
                dut.WaitTime(1)
                dut.TurnOFFStreamingWithControlReg()
                packet_index_after_reset, packet_number_from_packet_header_after_reset=dut.ReadPacketIndexandPacketHeader(buffer_size)
                if packet_index_after_reset <= packet_index_before_reset:
                    self.Log('error', 'Packet index register failed to increment')
                if packet_number_from_packet_header_after_reset > packet_number_from_packet_header_before_reset:
                    self.Log('error','Packet number in packet header failed to reset. Actual: {} Expected less than :{}'.format(
                                 packet_number_from_packet_header_after_reset,
                                 packet_number_from_packet_header_before_reset))
                dut.ResetStreamingWithControlReg()

    def DirectedS2HPacketCountRolloverInPacketHeaderTest(self):
        for dut in self.env.duts :
            self.CheckForCompatableFpga(dut)
            dut.reset_aurora_trigger_bus()
            addr, size = dut.StreamingBufferaddrsize()
            max_packets_in_site_controller, pkt_byte_count, buffer_size = dut.ReadStreamingDetails(addr)
            dut.TurnOFFStreamingWithControlReg()
            dut.ResetStreamingWithControlReg()
            if not dut.IsS2HConfigurationDone():
                self.Log('error', 'S2H Configuration is not done')
            dut.TurnONStreamingWithControlReg()
            self.WaitTime(255)
            dut.TurnOFFStreamingWithControlReg()
            packet_index_before_rollover = dut.GetTotalNoofPackets()
            packet_number_from_packet_header_before_rollover = dut.ReadPacketNumber(packet_index_before_rollover,buffer_size)
            self.Log('info','Packet Count in Packet Header before Rollover: {}.'.format(packet_number_from_packet_header_before_rollover))
            dut.TurnONStreamingWithControlReg()
            self.WaitTime(20)
            dut.TurnOFFStreamingWithControlReg()
            packet_index_after_rollover = dut.GetTotalNoofPackets()
            packet_number_from_packet_header_after_rollover = dut.ReadPacketNumber(packet_index_after_rollover,buffer_size)
            self.Log('info','Packet Count in Packet Header after Rollover: {}. '.format(packet_number_from_packet_header_after_rollover))
            if packet_number_from_packet_header_after_rollover > expected_count_after_rollover:
                self.Log('error', 'Packet Count failed to rollover.Expected close to: {} . Actual {}'.format(expected_count_after_rollover,packet_number_from_packet_header_after_rollover))


    def ErrorifPacketCounterDidNotReset(self,dut,seconds_count,reset_time_interval,total_packets_when_OFF,previous_total_packets_when_OFF):
        if (seconds_count % reset_time_interval) == 0:
            if total_packets_when_OFF > previous_total_packets_when_OFF:
                self.Log('error', 'Packet Counter failed to Reset.Actual:{} Expected close to Zero {}'.format(
                    total_packets_when_OFF, previous_total_packets_when_OFF))
            dut.ResetStreamingWithControlReg()
            previous_total_packets_when_OFF = 0
        else:
            if total_packets_when_OFF < previous_total_packets_when_OFF:
                self.Log('error', 'Packet Counter failed to Increment.Actual:{} Expected more than: {}'.format(
                    total_packets_when_OFF, previous_total_packets_when_OFF))
            previous_total_packets_when_OFF = total_packets_when_OFF
        return previous_total_packets_when_OFF

    def GetCount(self, last_packet_index, max_packets_in_site_controller, instrument):
        if not instrument.IsStreamingOn():
            self.Log('warning', 'Streaming is not ON ')
        packet_index = instrument.GetTotalNoofPackets()
        if packet_index == no_valid_packet_data:
            return 0
        elif packet_index == last_packet_index.value:
            return 0
        elif packet_index > last_packet_index.value:
            number_of_packet = packet_index - last_packet_index.value
            self.Log('debug', 'Number of Packets : {} '.format(number_of_packet))
        else:
            number_of_packet = packet_index + max_packets_in_site_controller - last_packet_index.value
            self.Log('debug','Number of Packets : {} '.format(number_of_packet))
        last_packet_index.value = packet_index
        return number_of_packet

    def WaitTime(self, time_to_wait):
        start_time = self.now()
        while self.now() - start_time < time_to_wait:
            pass
    
    def now(self):
        return time.perf_counter()
        
    

