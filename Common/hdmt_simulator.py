# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval
from Common.blt_simulator import BLT
from Dps.testbench.hddps_simulator import Hddps
from Hpcc.testbench.hpcc_simulator import Hpcc
from Rc3.testbench.rc3_simulator import Rc3


class HdmtSimulator(fval.Object):
    def __init__(self, name='HdmtSimulator'):
        super().__init__()
        self.name = name
        self.rc = Rc3(system=self)
        self.hddps = {}
        self.hpcc = {}
        self._tiu_aux_power_state = 0

    class HdmtSimulatorException(Exception):
        pass

    def add_instruments(self, instrument, slots):
            if instrument == 'hpcc':
                for slot in slots:
                    self.add_hpcc(slot)
            elif instrument == 'hddps':
                for slot in slots:
                    self.add_hddps(slot)

    def add_hddps(self, slot):
        if slot is None:
            raise self.SlotError(f'Requested slot: {slot}')
        else:
            self.hddps[slot] = [Hddps(self.rc, slot, 0), Hddps(self.rc, slot, 1)]

    def add_hpcc(self, slot):
        if slot is None:
            raise self.SlotError(f'Requested slot: {slot}')
        else:
            self.hpcc[slot] = Hpcc(self.rc, slot)

    def rc3BarRead(self, bar, offset):
        return self.rc.bar_read(bar, offset)

    def rc3BarWrite(self, bar, offset, data):
        self.rc.bar_write(bar, offset, data)

    def rc3FpgaVersionString(self):
        return self.rc.fpga_version_string()

    def rc3DmaRead(self, address, length):
        return self.rc.dma_read(address, length)

    def rc3DmaWrite(self, address, data):
        self.rc.dma_write(address, data)

    def rc3CpldVersionString(self):
        return self.rc.cpld_version_string()

    def rc3HpsDmaWrite(self, address, data):
        self.rc.hps_dma_write(address, data)

    def rc3StreamingBufferEx(self, index):
        return self.rc.s2h.stream_info()

    def rc3FpgaLoad(self, image):
        self.rc.rc3_fpga_load(image)

    def psPmbusRead(self, interface_num, cmd, byte_length):
        data = self.rc.power_supplies[interface_num].pmbus_read(cmd)
        return data.to_bytes(1, 'little')

    def rc3Ad5064Write(self, command, address, data):
        self.rc.rc3_ad5064_write(command, address, data)

    def rc3TmonRead(self, channel):
        return self.rc.rc3_tmon_read(channel)

    def rc3VmonRead(self, channel):
        return self.rc.rc3_vmon_read(channel)

    def tiuCalBaseConnect(self):
        pass

    def tiuCalBaseInit(self):
        pass

    def tiuCalBaseGpioWrite(self, gpio, state):
        self.rc.tiu_spare_gpio(gpio, state)

    def dpsFpgaVersion(self, slot, subslot):
        return 0

    def dpsBarRead(self, slot, subslot, bar, offset):
        return self.hddps[slot][subslot].bar_read(bar, offset)

    def dpsBarWrite(self, slot, subslot, bar, offset, data):
        self.hddps[slot][subslot].bar_write(bar, offset, data)

    def hddpsAd5560Write(self, slot, subslot, rail, register_address, data):
        pass

    def hddpsAd5560Read(delf, slot, subslot, rail, register_address):
        return 0

    def hddpsMax6662Write(self, slot, subslot, device, register_addr,
                          encoded_temperature):
        pass

    def dpsDmaWrite(self, slot, subslot, address, data):
        pass

    def hpccAcBarRead(self, slot, fpgaIndex, bar, offset):
        return self.hpcc[slot].ac[fpgaIndex].bar_read(bar, offset)

    def hpccAcBarWrite(self, slot, fpgaIndex, bar, offset, data):
        self.hpcc[slot].ac[fpgaIndex].bar_write(bar, offset, data)

    def hpccAcDmaWrite(self, slot, fpgaIndex, address, data):
        self.hpcc[slot].ac[fpgaIndex].dma_write(address, data)

    def hpccAcDmaRead(self, slot, fpgaIndex, address, length):
        return self.hpcc[slot].ac[fpgaIndex].dma_read(address, length)

    def hpccDcBarRead(self, slot, fpgaIndex, bar, offset):
        return self.hpcc[slot].dc[fpgaIndex].bar_read(bar, offset)

    def hpccDcBarWrite(self, slot, fpgaIndex, bar, offset, data):
        self.hpcc[slot].dc[fpgaIndex].bar_write(bar, offset, data)

    def rc3Lmk04832Read(self, address):
        is_clock_init_offset = 0x148
        clock_init_true = 0x33
        if address == is_clock_init_offset:
            return clock_init_true
        else:
            return 0

    def rc3Lmk04832Write(self, address, data):
        pass

    def rc3BltBoardRead(self):
        return self.rc.BLT()

    def rc3BltInstrumentRead(self):
        return self.rc.BLT()

    def psBltBoardRead(self, slot):
        return PsBlt()

    def hilFailsafeDisable(self, boolean):
        pass

    def rc3FailsafeDisable(self, boolean):
        pass

    def rcConnect(self):
        raise RuntimeError('The specified device cannot be found.')

    def rc3Connect(self):
        pass

    def rc3Init(self):
        pass

    def rcRootI2cInit(self, ms_timeout):
        pass

    def rcRootI2cLock(self, ms_timeout):
        pass

    def rcRootI2cUnlock(self):
        pass

    def rcRootI2cMuxSelect(self, selection):
        pass

    def bpRootI2cSwitchSelect(self, channels):
        pass

    def bpTiuAuxPowerEnable(self, state):
        self._tiu_aux_power_state = int(state)

    def bpTiuAuxPowerState(self):
        return self._tiu_aux_power_state

    def rc3LbConnect(self):
        pass

    def hpccAcDeviceEnable(self, slot, fpga_index):
        pass

    def hpccAcDeviceDisable(self, slot, fpga_index):
        pass

    def hpccAcFpgaLoad(self, slot, fpga_index, image):
        pass

    def hpccAcConnect(self, slot):
        if slot in self.hpcc.keys():
            pass
        else:
            raise RuntimeError('The specified device cannot be found.')

    def hpccDcConnect(self, slot):
        if slot in self.hpcc.keys():
            pass
        else:
            raise RuntimeError('The specified device cannot be found.')

    def hpccAcDisconnect(self, slot):
        pass

    def hpccDcDisconnect(self, slot):
        pass

    def hpccBltInstrumentRead(self, slot):
        blt = BLT()
        blt.PartNumberCurrent = 'hpcc_inst'
        return blt

    def hpccAcBltBoardRead(self, slot):
        blt = BLT()
        blt.PartNumberCurrent = 'AAJ49483-100'
        return blt

    def hpccDcBltBoardRead(self, slot):
        blt = BLT()
        blt.PartNumberCurrent = 'AAJ49481-100'
        return blt

    def hpccDcCpldVersionString(self, slot):
        return 'a.b'

    def hpccDcFpgaVersion(self, slot, slice):
        return 0x00000000

    def hpccDcDeviceEnable(self, slot, slice):
        pass

    def hpccDcDeviceDisable(self, slot, slice):
        pass

    def hpccDcFpgaLoad(self, slot, fpga_index, image):
        pass

    def hpccAcCpldVersionString(self, slot):
        return 'd.e'

    def calHpccConnect(self, slot):
        pass

    def calHpccPca9506Write(self, slot, address, data):
        pass

    def tdbConnect(self, slot):
        raise RuntimeError('The specified device cannot be found.')

    def tdbDisconnect(self, slot):
        pass

    def hddpsConnect(self, slot, subslot):
        if slot not in self.hddps.keys():
            raise RuntimeError('The specified device cannot be found.')

    def dpsDeviceDisable(self, slot, subslot):
        pass

    def dpsDeviceEnable(self, slot, subslot):
        pass

    def dpsFpgaLoad(self, slot, subslot, image):
        pass

    def hddpsDisconnect(self, slot, subslot):
        pass

    def hvdpsConnect(self, slot):
        pass

    def dpsBltInstrumentRead(self, slot):
        return BLT()

    def dpsBltBoardRead(self, slot, subslot):
        return BLT()

    def rc3DeviceDisable(self):
        pass

    def rc3DeviceEnable(self):
        pass

    def cypDeviceOpen(self, vidpid, slot, additional):
        pass

    def cypDeviceClose(self, handle):
        pass

    def cypPortBitWrite(self, device, port, bit, enable):
        pass

    def cypPortBitRead(self, device, port, bit):
        return 0

    class SlotError(Exception):
        pass


class PsBlt():
    DeviceName = 'PsBltSim'
    VendorName = 'DELTA'
    PartNumberAsBuilt = ''
    PartNumberCurrent = ''
    SerialNumber = ''
    ManufactureDate = ''
