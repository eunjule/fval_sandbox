################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


# Refer to the link below for more information about the temperature sensor:
# https://www.intel.com/content/www/us/en/programmable/documentation/eis1412131558004.html#wtw1407486022104
def convert_raw_temp_to_degrees_celcius(register_value):
        return to_signed32(register_value)/256
    
def to_signed32(value):
    if value & 0x80000000:
        return -0x100000000 + value
    else:
        return value

def convert_celsius_to_raw(celsius):
    if celsius > 0:
        return int(celsius * 256)
    else:
        return int(celsius * 256) + 0x100000000

