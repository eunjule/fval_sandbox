# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from ctypes import c_float, c_uint32, LittleEndianStructure
from enum import Enum
from time import perf_counter, sleep

from Common import hilmon as hil
from Common.fval import Object


class S2hBDHeaderId(Enum):
    packet_signature = 0x5A12345A
    packet_footer = 0xA54321A5
    bulk_packet_version = 2


class S2hSPMHeaderId(Enum):
    packet_type = 1


class S2hBufferIndex(Enum):
    BD = 0   # Bulk Data
    SCS = 1  # Serial Capture Stream
    SPM = 2  # System Power Monitoring


class S2hInterface(Object):
    TIMEOUT = 10
    INVALID_PACKET_INDEX = 0xFFFF
    PACKET_REFRESH_MS = 4
    TIME_STAMP_TOLERANCE = 0.01

    def __init__(self, rc, index=S2hBufferIndex.BD):
        super().__init__()
        self.rc = rc
        if type(index) == int:
            index = S2hBufferIndex(index)
        self.index = index
        self._packet_count = None
        self._packet_byte_size = None

    def packet_count(self):
        if self._packet_count is None:
            reg_class = getattr(self.rc.registers,
                                f'S2H_{self.index.name}_PACKET_COUNT')
            self._packet_count = self.rc.read_bar_register(reg_class).count
        return self._packet_count

    def packet_byte_size(self):
        if self._packet_byte_size is None:
            reg_class = getattr(self.rc.registers,
                                f'S2H_{self.index.name}_PACKET_BYTE_COUNT')
            self._packet_byte_size = self.rc.read_bar_register(reg_class).count
        return self._packet_byte_size

    def valid_packet_index(self):
        reg_class = getattr(self.rc.registers,
                            f'S2H_{self.index.name}_VALID_PACKET_INDEX')
        return self.rc.read_bar_register(reg_class).index

    def configuration_done(self):
        reg_class = getattr(self.rc.registers, f'S2H_{self.index.name}_CONFIG')
        return self.rc.read_bar_register(reg_class).done

    def ram_refresh_ms(self):
        return self.packet_count() * S2hInterface.PACKET_REFRESH_MS

    def stream_buffer(self):
        if self.rc.name().lower() == 'hbirctc':
            if self.index == S2hBufferIndex.SCS:
                raise S2hInterface.InvalidBufferIndex(
                    f'{self.index.name} is currently not supported in FVAL')
            buf_addr, buf_size = hil.hbiMbStreamingBufferEx(
                self.index.value)
        else:
            if self.index != S2hBufferIndex.BD:
                raise S2hInterface.InvalidBufferIndex(
                    f'{self.index.name} is currently not supported in FVAL')
            buf_addr, buf_size = hil.rc3StreamingBufferEx(self.index.value)
        packet_class = getattr(S2hInterface,
                               f'{self.index.name}StreamPacket')
        stream_size = packet_class * self.packet_count()
        return stream_size.from_address(buf_addr)

    class InvalidBufferIndex(Exception):
        pass

    def disable_stream(self):
        reg_class = getattr(self.rc.registers,
                            f'S2H_{self.index.name}_CONTROL')
        reg = self.rc.read_bar_register(reg_class)
        reg.streaming_stop = 1
        self.rc.write_bar_register(reg)
        for i in range(S2hInterface.TIMEOUT + 1):
            if self.is_stream_enabled():
                sleep(S2hInterface.TIMEOUT / 1000.0)
            else:
                break
        else:
            raise S2hInterface.StreamError(
                f'Timed out waiting on stream disable.')

    def enable_stream(self):
        reg_class = getattr(self.rc.registers,
                            f'S2H_{self.index.name}_CONTROL')
        reg = self.rc.read_bar_register(reg_class)
        reg.streaming_start = 1
        self.rc.write_bar_register(reg)
        for i in range(S2hInterface.TIMEOUT + 1):
            if not self.is_stream_enabled():
                sleep(S2hInterface.TIMEOUT / 1000.0)
            else:
                break
        else:
            raise S2hInterface.StreamError(
                f'Timed out waiting on stream enable.')

    def is_stream_enabled(self):
        reg_class = getattr(self.rc.registers, f'S2H_{self.index.name}_STATUS')
        return bool(self.rc.read_bar_register(reg_class).streaming_enabled)

    def reset_stream(self):
        reg_class = getattr(self.rc.registers,
                            f'S2H_{self.index.name}_CONTROL')
        reg = self.rc.read_bar_register(reg_class)
        reg.streaming_reset = 1
        reg.streaming_stop = 1
        self.rc.write_bar_register(reg)
        for i in range(S2hInterface.TIMEOUT):
            if not self.rc.read_bar_register(reg_class).streaming_reset:
                break
        else:
            raise S2hInterface.StreamError(f'Timed out resetting S2H stream')
        self.wait_on_matching_valid_packet_index(
            S2hInterface.INVALID_PACKET_INDEX)

    def reset_controller(self):
        reg_class = getattr(self.rc.registers,
                            f'S2H_{self.index.name}_CONTROL')
        reg = self.rc.read_bar_register(reg_class)
        reg.controller_reset = 1
        self.rc.write_bar_register(reg)
        reg.controller_reset = 0
        self.rc.write_bar_register(reg)

    def wait_on_matching_valid_packet_index(self, index):
        max_packet_count = self.packet_count()
        for retry in range(max_packet_count ** 2):
            current_valid_index = self.valid_packet_index()
            if index == current_valid_index:
                return index
        else:
            raise S2hInterface.StreamError(
                f'Unable to get valid index of {index}.')

    def wait_on_valid_valid_packet_index(self):
        max_packet_count = self.packet_count()
        for retry in range(max_packet_count ** 2):
            index = self.valid_packet_index()
            if index != S2hInterface.INVALID_PACKET_INDEX:
                return index
        else:
            raise S2hInterface.StreamError(
                f'Unable to get a valid index.')

    def stream_packet(self, index):
        return self.stream_buffer()[index]

    def increment_valid_packet_index(self, index):
        return (index + 1) % self.packet_count()

    def buffer_index(self):
        return self.index

    def wait_on_packet_refresh(self):
        start = perf_counter()
        while perf_counter() - start < (S2hInterface.PACKET_REFRESH_MS / 1e3):
            pass

    class StreamError(Exception):
        pass

    class BDStreamPacket(LittleEndianStructure):
        # Total of 2048 bytes
        _fields_ = \
            [(S2hBDHeaderId.packet_signature.name, c_uint32),  # 4 Bytes
             (S2hBDHeaderId.bulk_packet_version.name, c_uint32),  # 4 Bytes
             ('packet_counter', c_uint32),  # 4 Bytes
             ('status_data', c_uint32),  # 4 Bytes
             ('reserved', c_uint32 * 508)]  # 2032 Bytes

    class SPMStreamPacket(LittleEndianStructure):
        # Total of 6*4 bytes
        _fields_ =[('packet_header_count', c_uint32, 16),
                   ('bps_0_present', c_uint32, 1),
                   ('bps_1_present', c_uint32, 1),
                   ('bps_2_present', c_uint32, 1),
                   ('spm_enable', c_uint32, 1),
                   ('reserved', c_uint32, 4),
                   ('packet_type', c_uint32, 8),
                   ('time_stamp_us', c_uint32),
                   ('pout_bps_0', c_float),
                   ('pout_bps_1', c_float),
                   ('pout_bps_2', c_float),
                   ('pout_sum', c_float)]
