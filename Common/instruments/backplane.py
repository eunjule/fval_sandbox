# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from time import sleep

from Common import hilmon as hil
from Common.instruments.base import Instrument


class Backplane(Instrument):

    def __init__(self):
        Instrument.__init__(self)

    ###########################################################################################
    # Connection should occur with Backplane
    ###########################################################################################
    def Connect(self):
        self.Log('debug',"Connecting to the backplane...")
        self.PowerDown()
        return

    ###########################################################################################
    # Power Up the Backplane
    ###########################################################################################
    def PowerEnable(self):
        hil.bpTiuAuxPowerEnable(True)

    ###########################################################################################
    # Power Down the Backplane
    ###########################################################################################
    def PowerDown(self):
        hil.bpTiuAuxPowerEnable(False)

    ###########################################################################################
    # Disconnect to the Backplane
    ###########################################################################################
    def Disconnect(self):
        self.PowerDown()

    def PowerState(self):
        return hil.bpTiuAuxPowerState()

    def PowerDisable(self):
        hil.bpTiuAuxPowerEnable(False)

    def PowerCycle(self):
        self.PowerDown()
        sleep(3)
        self.PowerEnable()
