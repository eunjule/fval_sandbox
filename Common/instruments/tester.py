################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: tester.py
# -------------------------------------------------------------------------------
#     Purpose: 
# -------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/20/15
#       Group: HDMT FPGA Validation
################################################################################

import collections
import threading
import time

from Common import configs
from Hpcc import hpcc_configs as hpccConfigs
from Common import fval
from Common.get_device_class import get_device_class
from Common.hbi_simulator import HbiSimulator
from Common.hdmt_simulator import HdmtSimulator
from Common import hilmon as hil
from Hbidtb.instrument.hbidtb import Hbidtb

HDMT_NUM_SLOTS = 12
_tester = None


def get_tester():
    global _tester
    if _tester is None:
        _tester = Tester('Tester')
    return _tester


def get_tester_for_unit_test():
    """ !!! Only for use in unit tests !!! """
    global _tester
    _tester = Tester('Tester')
    return _tester


Device = collections.namedtuple('Device', ['device_name', 'slot', 'subslot'])
Devicename = collections.namedtuple('Devicename', 'device_name')


class Tester(fval.Component):
    _rcInitializeDone = False
    __slots__ = ()

    def __init__(self, name=None):
        super().__init__(name)
        self.cards = {}
        self.hpcc_instruments = {}
        self.hpcc_models = {}
        self.hddps_instruments = {}
        self.hddps_models = {}
        self.tdaubnk_instruments = {}
        self.hvdps_instruments = {}
        self.hbidps_instruments = {}
        self.duts = []
        self.dps = []
        self.rc = None
        self.devices = []  # maps from slot number to list of devices
        self._is_hbi_tester = None
        self._name = name
        self._failed_initialization = []

    def name(self):
        return self._name

    def get_tester_rc(self):
        for device in self.devices:
            if device.name() in ['Rc2', 'Rc3', 'HBIRCTC']:
                return device
        else:
            raise Tester.NoRcException('Rc2, Rc3 or Hbirctc has not been discovered yet. Instruments discovery must precced get_tester_rc')

    def Initialize(self):
        start_time = time.perf_counter()
        self.Log('info', 'Initializing Hardware')

        self.rc = self.get_tester_rc()

        if not self.is_hbi_tester():
            if configs.COMMON_INITIALIZE == True:
                self.CommonInitialize()
            else:
                self.Log('info', 'Skipping RC Initialize')

            self.backplane_calbase_init()

            self.initialize_hpcc()
            self.initialize_dps()
            self.initialize_tdaubank()
        else:
            self.initialize_hbirctc()
            self.initialize_hbicc()
            self.initialize_hbidps()

        time_elapsed = time.perf_counter() - start_time
        self.Log('info', f'Time elapsed initializing tester: {time_elapsed} '
                         f'seconds')

    def initialize_hpcc(self):
        for hpcc in self.get_undertest_instruments('Hpcc'):
            #create and connect hpcc model
            hpccModel = get_device_class('hpccmodel')(hpcc.slot, name = f'HpccModel[{hpcc.slot}]')
            hpccModel.ConnectRcModel(self.rc.rcModel)
            self.hpcc_models[hpcc.slot] = hpccModel

            #intialize hpcc calibration for all instruments
            if hpccConfigs.HPCC_ENV_INIT in ['LOADCAL', 'CAL', 'FULL', 'ZEROCAL']:
                hpcc.InitializeCal()

            #initialize hpcc instruments
            if hpccConfigs.HPCC_ENV_INIT in ['LOADCAL', 'FULL', 'ZEROCAL']:
                hpcc.Initialize()

            #update HPCC capabilities
            for slice in [0, 1]:
                if hpccConfigs.HPCC_SLICE0_ONLY and slice != 0:
                     continue
                if hpccConfigs.HPCC_SLICE1_ONLY and slice != 1:
                     continue
                hpcc.ac[slice]._UpdateFpgaCapabilities()

            #calibrate hpcc devices
            hpcc.calibration()

    def initialize_dps(self):
        for hddps in self.get_undertest_instruments('Hddps'):
            hddpsModel = get_device_class('hddpsmodel')(hddps.slot, name=f'HddpsModel[{hddps.slot}]')
            hddpsModel.ConnectRcModel(self.rc.rcModel)
            hddps.model = hddpsModel
            hddps.Initialize()
            self.hddps_instruments[hddps.slot] = hddps

        for hvdps in self.get_undertest_instruments('HvdpsCombo'):
            hvdps.Initialize()
            self.hvdps_instruments[hvdps.slot] = hvdps

    def initialize_tdaubank(self):
        for tdaubnk in self.get_undertest_instruments('Tdaubnk'):
            self.rc.clear_rc_trigger()
            tdaubnk.Initialize()
            self.tdaubnk_instruments[tdaubnk.slot] = tdaubnk

    def initialize_hbirctc(self):
        """
        Initiailize Hbirctc if its the only dut for regressions, o/w it will
        be initialized under the other hbi init methods
        """
        instruments = self.get_undertest_instruments()
        if len(instruments) == 1 and 'HBIRCTC' in instruments[0].name():
            if configs.COMMON_INITIALIZE:
                self.CommonInitialize()

    def initialize_hbidps(self):
        self.CommonInitialize()

        for hbidps in self.get_undertest_instruments('Hbidps'):
            hbidps.dps_fpga_load()
            hbidps.Initialize()

    def initialize_hbicc(self):
        threads = self.add_hbicc_threads()
        if threads:
            threads += self.add_hbirctc_thread()
            self.Log('info',
                     f'Launching HBIRCTC and all HBICC in different threads...')
            self.start_threads(threads)
            self.join_threads(threads)

        hbicc = self.get_hbicc_undertest('Hbicc')
        if hbicc is not None:
            hbicc.initialize()

    def add_hbirctc_thread(self):
        threads = []
        if not self._rcInitializeDone and configs.COMMON_INITIALIZE:
            hbirctc = self.rc
            threads.append(threading.Thread(target=hbirctc.Initialize))
        return threads

    def add_hbicc_threads(self):
        threads = []
        hbicc = self.get_hbicc_undertest('Hbicc')
        if hbicc:
            devices = hbicc.devices_active()
            for device in devices:
                threads.append(
                    threading.Thread(target=device.initialize))
        return threads

    def join_threads(self, threads):
        for process in threads:
            value = process.join()

    def start_threads(self, threads):
        for process in threads:
            process.start()

    def get_undertest_instruments(self, instrument_name=''):
        instruments = [device for device in self.devices if device.under_test == True and instrument_name.lower() in device.name().lower()]
        return instruments

    def get_instrument_present(self, instrument_name=''):
        instruments = [device for device in self.devices if instrument_name.lower() in device.name().lower()]
        return instruments

    def get_hbicc_undertest(self, instrument_name=''):
        for device in self.devices:
            if device.__class__.__name__.lower() == instrument_name.lower() and device.under_test:
                return device

    def get_undertest_dps_instruments(self):
        undertest_dps_instruments = []
        for device in self.devices:
            if device.name() in configs.DPS_DEVICE_TYPES and device.under_test:
                undertest_dps_instruments.append(device)
        if len(undertest_dps_instruments) == 0:
            self.Log('error', 'No Undertest DPS Devices found on the tester')

        return undertest_dps_instruments

    def get_undertest_hbidps_instruments(self):
        undertest_hbidps_instruments = self.get_undertest_instruments(
            instrument_name='hbidps')
        if len(undertest_hbidps_instruments) == 0:
            self.Log('error', 'No Undertest DPS Devices found on the tester')
        return undertest_hbidps_instruments

    def set_under_test(self, instrument_name, slot):
        if slot is None:
            instruments = self.devices
        else:
            instruments = [x for x in self.devices if x.slot == slot]
        for device in instruments:
            if device.name().lower() == instrument_name.lower():
                device.under_test = True

    def discover_all_instruments(self):
        self.clear_device_list()
        self.rc = self.discover_rc()
        self.get_instruments(self.rc)
        return self.devices

    def get_instruments(self, rc):
        if self.is_hbi_tester():
            hbidtbs = self.discover_hbidtb()
            self.get_hbi_instruments(rc, hbidtbs)
        else:
            self.get_gen2_instruments(rc)

    def clear_device_list(self):
        self.devices = []

    def get_hbi_instruments(self, rc, hbidtbs):
        hil.monitor = HbiSimulator('HBI Reference Model')
        threads = []
        for device in configs.HBI_DEVICE_TYPES:
            if device.lower() in ['hbirctc', 'hbidtb']:
                continue
            else:
                class_name = device
                device_class = get_device_class(class_name)
                if device.lower() == 'hbidps':
                    for slot in range(16):
                        hbidtb = hbidtbs[slot//4]
                        threads.append(threading.Thread(target=self.find_hbidps,args=(device_class, rc, slot, hbidtb)))
                else:
                    threads.append(threading.Thread(target=self.find_non_hbidps, args=(device_class, rc, hbidtbs)))
        self.start_threads(threads)
        self.join_threads(threads)

    def find_hbidps(self, device_class, rc, slot, hbidtb):
        dut_instrument = device_class(slot=slot, rc=rc, hbidtb=hbidtb)
        result = dut_instrument.discover_all_devices()
        if result is not None:
            self.devices.append(result)

    def find_non_hbidps(self, device_class, rc, hbidtbs):
        dut_instrument = device_class(rc=rc, hbidtbs=hbidtbs)
        result = dut_instrument.discover_all_devices()
        if result is not None:
            self.devices.append(result)

    def is_hbi_tester(self):
        if self._is_hbi_tester is None:
            if self.rc2_exists() or self.rc3_exists():
                self._is_hbi_tester = False
            elif self.hbirctc_exists():
                self._is_hbi_tester = True
            else:
                raise Tester.NoRcException(f'No Rc2, Rc3 or Hbirctc found!')
        return self._is_hbi_tester

    class NoRcException(Exception):
        pass

    def discover_hbidtb(self):
        hbidtbs = {slot: None for slot in range(4)}
        for slot in range(4):
            hbidtbs[slot] = Hbidtb(slot).discover()
        return hbidtbs

    def rc2_exists(self):
        try:
            hil.rcConnect()
            exists = True
        except RuntimeError:
            exists = False
        return exists

    def rc3_exists(self):
        try:
            hil.rc3Connect()
            exists = True
        except RuntimeError:
            exists = False
        return exists

    def hbirctc_exists(self):
        try:
            hil.hbiMbConnect()
            exists = True
        except RuntimeError:
            exists = False
        return exists

    def discover_rc(self):
        if self.is_hbi_tester():
            rc = get_device_class('hbirctc')(mainboard=get_device_class('mainboard')()).discover_device()
        else:
            if self.rc2_exists() and self.rc3_exists():
                raise Tester.MultipleRcExistException(f'Invalid RC configuration. Rc2 and Rc3 exist.')
            elif self.rc2_exists():
                rc = get_device_class('rc2')()
            elif self.rc3_exists():
                rc = get_device_class('rc3')()
        rc.under_test = True
        self.devices.append(rc)

        return rc

    class MultipleRcExistException(Exception):
        pass

    def get_gen2_instruments(self, rc):
        hil.monitor = HdmtSimulator('HDMT Reference Model')
        for device in configs.HDMT_DEVICE_TYPES:
            classname = device
            device_class = get_device_class(classname)

            if device.lower() == 'hbicc':
                dut_instrument = device_class(rc=rc)
                result = dut_instrument.discover_all_devices()
                if result is not None:
                    self.devices.append(result)
            elif device.lower() == 'hbirctc':
                try:
                    dut_instrument = device_class(mainboard=get_device_class('mainboard')())
                    result = dut_instrument.discover_device()
                    if result is not None:
                        self.devices.append(result)
                        result.under_test = True
                except RuntimeError:
                    self.Log('warning', 'HBIRCTC not found')
            elif device.lower() == 'hbidps':
                for slot in range(HDMT_NUM_SLOTS):
                    dut_instrument = device_class(slot=slot, rc=rc)
                    result = dut_instrument.discover_all_devices()
                    if result is not None:
                        self.devices.append(result)
            else:
                if device.lower() != 'rc2' and device.lower() != 'rc3':
                    for slot in range(HDMT_NUM_SLOTS):
                        dut_instrument = device_class(slot=slot, name='{}[{}]'.format(device, slot), rc=rc)
                        result = dut_instrument.discover()
                        self.devices.extend(result)

    def CommonInitialize(self):
        if not self._rcInitializeDone:  # Initialize RC once per regression
            # Initialize the RC
            cardList = []
            for slot in self.cards:
                cardList.append(slot)
            rc = self.get_tester_rc()
            rc.cardList = cardList
            rc.Initialize()
            self._rcInitializeDone = True

    def backplane_calbase_init(self):
        bp2 = get_device_class('backplane')()
        bp2.PowerCycle()

        rc = self.get_tester_rc()

        # option to skip cal base init for specialized testing that might now have it connected to the tester.
        if not configs.CALBASE_IGNORE:
            if self.is_tiu_physically_connected(rc):
                self.Log('info', 'TIU detected by RCTC status register')
            else:
                self.Log('error', 'RC Failed to detect TIU')
            cb2 = get_device_class('cb2')()
            cb2.Connect()
            cb2.Initialize()
        else:
            fval.core.Log('info', 'Skipping Cal Base init')

    def is_tiu_physically_connected(self, rc):
        return rc.is_tiu_physically_connected()

    def get_hdmt_instrument(self, slot):
        instruments = None
        if not self.is_hbi_tester():
            for instrument in self.devices:
                if instrument.slot == slot:
                    if instruments is None:
                        instruments = []
                    instruments.append(instrument)
            if not instruments:
                self.Log('warning', f'Unable to find hdmt instruments at '
                                    f'slot {slot}.')
        else:
            self.Log('warning', f'HBI tester does not have hdmt instruments.')
        return instruments
		
