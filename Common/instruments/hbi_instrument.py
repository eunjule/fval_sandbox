# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import csv
import os
import re
import time
import random
import zipfile

from Common import configs
from Common.instruments.base import Instrument
from Hpcc.instrument import vcd

SLICECOUNT = range(5)


class HbiInstrument(Instrument):

    def __init__(self):
        super().__init__()
        self.under_test = False

    def discover(self):
        try:
            self.open_pcie_device()
            self.Log('info', 'Found {}'.format(self.name()))
        except RuntimeError:
            self.Log('warning',
                     '{}: Unable to find FPGA using pciDeviceOpen HIL API. Try enabling it in the Device Manager.'.format(
                         self.name()))
            return None
        return self

    def load_fpga(self):
        if configs.SKIP_INIT:
            self.fpga_load = False
        if self.fpga_load:
            try:
                self.Log('info', f'Loading {self.name()} FPGA  with image ' +
                                 f"'{self.fpga_image_path()}'")
                self.load_fpga_using_hil()
                self.Log('info', f'{self.name()} FPGA load succeeded')
            except RuntimeError as e:
                self.enable_device()
                self.Log('error', f'{self.name()} FPGA load was not ' +
                                  f'successful with image ' +
                                  f"'{self.fpga_image_path()}'. {e}.")
        else:
            self.Log('warning', f'{self.name()} FPGA was not loaded due ' +
                                 'to configs setting')
    
    def warn_user_fab_detection_is_bypassed(self, fab_used):
        if not hasattr(self, '_fab_detection_warning_logged'):
            self.Log('warning', f'Loading {self.name()} Fab {fab_used} ' +
                                 'image based on configs attribute ' + 
                                 'instead of HW detection')
            self._fab_detection_warning_logged = True

    def log_blt(self):
        try:
            self.log_board_blt()
        except RuntimeError:
            self.Log('warning', '{} BLT read was unsuccessful'.format(self.name()))

    def log_cpld(self):
        try:
            cpld_version = self.cpld_version_string()
            self.Log('info', '{}: FPGA CPLD Version {}'.format(self.name(), cpld_version))
        except RuntimeError:
            self.Log('warning', '{}: CPLD read was unsuccessful'.format(self.name()))
            return None

    def log_fpga_version(self):
        try:
            fpga_version = self.fpga_version_string()
            major_minor = self._concatenate_fpga_version_string(fpga_version)
            if HbiInstrument.is_non_zero_or_non_f([major_minor]):
                self.Log('info', f'{self.name()}: FPGA Version {fpga_version}')
            else:
                self.Log('warning',
                         f'{self.name()}: FPGA Version read all 0\' or all f\'s {fpga_version}')
        except RuntimeError:
            self.Log('warning', f'{self.name()} Version read was unsuccessful')
            return None

    def _concatenate_fpga_version_string(self, fpga_version):
        split_version = re.split(r"[^0-9]", fpga_version)
        if int(split_version[0]) == 0xFFFF:
            split_version[0] = 'FFFF'
        else:
            split_version[0] = f'{int(split_version[0]) & 0xFFFF:04X}'
        if int(split_version[1]) == 0xFFFF:
            split_version[1] = 'FFFF'
        else:
            split_version[1] = f'{int(split_version[1]) & 0xFFFF:04X}'
        return split_version[0] + split_version[1]

    def log_build(self):
        try:
            hg_id_lower = self.read_bar_register(self.registers.HG_ID_LOWER).value
            hg_id_upper = self.read_bar_register(self.registers.HG_ID_UPPER).value
            if hg_id_upper >> 31:
                hg_clean = ''
            else:
                hg_clean = '+'
            self.Log('info',
                     f'{self.name()}: FPGA Build 0x{hg_id_upper & 0xFFFF:04X}{hg_id_lower:08X}{hg_clean}')
        except RuntimeError:
            self.Log('warning', '{} Build read was unsuccessful'.format(self.name()))
            return None

    def log_chip_id(self):
        chip_id_upper, chip_id_lower = self.chip_id()
        chip_id = (chip_id_upper << 32) + chip_id_lower
        if chip_id != 0 and chip_id_lower != 0xffffffff and chip_id_upper != 0xffffffff:
            self.Log('info', f'{self.name()} Chip ID : 0x{chip_id:016X}')
        else:
            self.Log('error', f'{self.name()} Chip ID read failed. Received 0x{chip_id:016X} ')

    def initialize_memory(self):
        try:
            self.initialize_memory_block()
            self.Log('info', '{}: Successful memory initialization.'.format(self.name()))
        except RuntimeError:
            self.Log('info', '{}: Memory failed to initialize.'.format(self.name()))
            return None

    def save_data_to_csv(self, fieldnames, data, dst):
        with open(dst, 'w') as out_file:
            writer = csv.DictWriter(out_file, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(data)

    def name(self):
        return self.__class__.__name__.upper()

    def read_scratch_reg(self, index):
        return self.read_bar_register(self.registers.SCRATCH_REG, index=index).value

    def write_scratch_reg(self, index, expected):
        self.write_bar_register(self.registers.SCRATCH_REG(value=expected, index=index))

    def check_total_error_count(self, helper, slice_list=None):
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            error_count_value = self.read_slice_register(helper.rx_error_count_register, slice)
            if error_count_value.value != 0:
                helper.fail_count_per_slice[slice] += 1
        if helper.fail_count_per_slice:
            print('info', '{} ---> {}: {}'.format(helper.tx_device.name(), helper.rx_device.name(),
                                                  helper.fail_count_per_slice))

    def combined_hex_values(self, values_list, value_width=32):
        if value_width % 4 != 0:
            self.Log('error', '{} is not divisible by 4'.format(value_width))
            return 0
        if len(values_list) == 1:
            return values_list[0]

        hex_width = int(value_width / 4)

        combine_value = ''
        for value in values_list:
            if not isinstance(value, int):
                self.Log('error', '{} is not an integer value'.format(value))
                return 0
            try:
                hex_value = str(hex(value))
                strip_0x = hex_value.replace('0x', '')
                fill_value = strip_0x.rjust(hex_width, '0')
                combine_value += fill_value
            except AttributeError:
                self.Log('error', 'Error occur while combining 2 or more hex values in {}'.format(self.name()))

        hex_data_rate = '0x' + combine_value

        return int(hex_data_rate, 16)

    def reconfig_bridge_reg(self, helper):
        column_headers = ['Bridge', 'Write Reg', 'Read Reg', 'Tag Reg', 'Target Reg', 'Target Value', 'Reference Tag',
                          'New Tag', 'ReadBack']
        reference_tag = self.read_bar_register(helper.tag_reg).Tag
        self.write_bar_register(helper.bridge_reg(value=helper.target_reg))

        new_tag = self.has_tag_change(helper.tag_reg, reference_tag)
        if new_tag:
            read_back_value = self.read_bar_register(helper.read_reg).value

            if helper.target_reg_value:
                self.write_bar_register(helper.write_reg(value=helper.target_reg_value))

            body = [{'Bridge': '0x{:x}'.format(helper.bridge_reg.address()),
                     'Write Reg': '0x{:x}'.format(helper.write_reg.address()),
                     'Read Reg': '0x{:x}'.format(helper.read_reg.address()),
                     'Tag Reg': '0x{:x}'.format(helper.tag_reg.address()),
                     'Target Reg': '0x{:x}'.format(helper.target_reg),
                     'Target Value': helper.target_reg_value,
                     'Reference Tag': reference_tag,
                     'New Tag': new_tag,
                     'ReadBack': self.convert_number_to_spaced_bin(read_back_value)}]

            table = self.contruct_text_table(column_headers, body)
            self.Log('info', 'Bridge Register Read and/or Write Operation Failed: {}'.format(table))
            return read_back_value

    def has_tag_change(self, tag_register, reference_tag, retries=0xF00):
        for i in range(retries):
            new_tag = self.read_bar_register(tag_register).Tag
            if reference_tag != new_tag:
                break
        else:
            self.Log('error', 'Reconfigure Address Write/Read Tag check failed')
            return None
        return new_tag

    def convert_number_to_spaced_bin(self, number):
        if isinstance(number, int):
            binary = '{:b}'.format(number)
            padded_bin = binary.rjust(32, '0')
            spaced_bin = " ".join(padded_bin[i:i + 4] for i in range(0, len(padded_bin), 4))
            hex_bin = ' <-> '.join(['{0x{:x}'.format(number), spaced_bin])
        return hex_bin

    def log_snooper(self, is_snooper_active, slice_list, title=''):
        if is_snooper_active:
            table = None
            for psdb, pin0, pin1 in self.hbicc.paths:
                table  = pg.read_packet(slice_list, pg.registers.PACKET_SNAPSHOT, 'packet', f'{pg.name()} - {title}', False)
                # table += rm.read_packet(slice_list, rm.registers.PACKET_SNAPSHOT, 'packet', f'{rm.name()} - {title}', False)
                table += pin0.read_packet(slice_list, pin0.registers.PACKET_SNAPSHOT, 'packet', f'{pin0.name()} - {title}', False)

                table += pg.read_packet(slice_list, pg.registers.PACKET_XOR, 'packet', f'{pg.name()} - {title}', False)
                # table += rm.read_packet(slice_list, rm.registers.PACKET_XOR, 'packet', f'{rm.name()} - {title}', False)
                table += pin0.read_packet(slice_list, pin0.registers.PACKET_XOR, 'packet', f'{pin0.name()} - {title}', False)

                table += pg.read_packet(slice_list, pg.registers.PM_E_PACKETS, 'pm', f'{pg.name()} - {title}', True)
                # table += rm.read_packet(slice_list, rm.registers.PM_E_PACKETS, 'pm', f'{rm.name()} - {title}', True)
                table += pin0.read_packet(slice_list, pin0.registers.PM_E_PACKETS, 'pm', f'{pin0.name()} - {title}', True)

                table += pg.read_packet(slice_list, pg.registers.PM_C_PACKETS, 'pm', f'{pg.name()} - {title}', True)
                # table += rm.read_packet(slice_list, rm.registers.PM_C_PACKETS, 'pm', f'{rm.name()} - {title}', True)
                table += pin0.read_packet(slice_list, pin0.registers.PM_C_PACKETS, 'pm', f'{pin0.name()} - {title}', True)

                table += pg.read_packet(slice_list, pg.registers.PM_E_EOB_PACKETS, 'pm', f'{pg.name()} - {title}', True)
                # table += rm.read_packet(slice_list, rm.registers.PM_E_EOB_PACKETS, 'pm', f'{rm.name()} - {title}', True)
                table += pin0.read_packet(slice_list, pin0.registers.PM_E_EOB_PACKETS, 'pm', f'{pin0.name()} - {title}', True)

                table += pg.read_packet(slice_list, pg.registers.PM_C_EOB_PACKETS, 'pm', f'{pg.name()} - {title}', True)
                # table += rm.read_packet(slice_list, rm.registers.PM_C_EOB_PACKETS, 'pm', f'{rm.name()} - {title}', True)
                table += pin0.read_packet(slice_list, pin0.registers.PM_C_EOB_PACKETS, 'pm', f'{pin0.name()} - {title}', True)

                table += pg.read_packet(slice_list, pg.registers.PM_E_INVALID_PACKETS, 'pm', f'{pg.name()} - {title}', True)
                # table += rm.read_packet(slice_list, rm.registers.PM_E_INVALID_PACKETS, 'pm', f'{rm.name()} - {title}', True)
                table += pin0.read_packet(slice_list, pin0.registers.PM_E_INVALID_PACKETS, 'pm', f'{pin0.name()} - {title}', True)

                table += pg.read_packet(slice_list, pg.registers.PM_C_INVALID_PACKETS, 'pm', f'{pg.name()} - {title}', True)
                # table += rm.read_packet(slice_list, rm.registers.PM_C_INVALID_PACKETS, 'pm', f'{rm.name()} - {title}', True)
                table += pin0.read_packet(slice_list, pin0.registers.PM_C_INVALID_PACKETS, 'pm', f'{pin0.name()} - {title}', True)

                self.Log('debug', f'{title} {table}')

            return table

    def read_packet(self, slice_list, reg, col_name='', title='', dec=False):
        data = []
        headers = ['Slice']
        header_ = []
        for i in range(4):
            col = f'{col_name}_{i}'
            header_.append(col)
        header_.reverse()

        headers += header_

        for slice in slice_list:
            read_back = {'Slice': slice}
            for index in range(4):
                col = f'{col_name}_{index}'
                packet = self.read_slice_register(reg, slice, index=index)
                if dec:
                    read_back.update({col: f'{packet.value}'})
                else:
                    read_back.update({col: f'0x{packet.value:08X}'})
            data.append(read_back)
        table = self.contruct_text_table(headers, data, title_in=f'{packet.name()}')

        self.Log('debug', f'{title} {table}')
        return f'{title} {table}'

    def reset_rm_packet_snooper(self, slice_list):
        for slice in slice_list:
            reset_control_register = self.read_slice_register(self.registers.RESET_CONTROL, slice)
            reset_control_register.Data = 1
            self.write_slice_register(reset_control_register, slice)
            reset_control_register.Data = 0
            self.write_slice_register(reset_control_register, slice)
            self.Log('debug', f'{self.name()} Resetting Slice {slice}')

    def set_user_cycle_to_snoop(self, cycle_no, slice_list):
        self.Log('debug', f'{self.name()} Snooper Cycle Number: {cycle_no}')
        for slice in slice_list:
            reset_control_register = self.read_slice_register(self.registers.SNAPSHOT_CYCLE_NUMBER, slice)
            reset_control_register.Data = cycle_no
            self.write_slice_register(reset_control_register, slice)
            self.write_slice_register(reset_control_register, slice)

    def join_threads(self, threads):
        for process in threads:
            process.join()

    def start_threads(self, threads):
        for process in threads:
            process.start()

 

    def load_and_arm_ilas(self, index, file):
        self.load_ila_names(index, file)
        self.arm_ila(index)

    def load_ila_names(self, index, file):
        signal_names = {}
        with open(file, 'r') as fin:
            reader = csv.reader(fin)
            for row in reader:
                if len(row) == 2:
                    signal_number, signal = row
                    signal_names[int(signal_number)] = signal
        self.ila_names.update({index: signal_names})

    def arm_ila(self, slice, ila_number):
        ila_control = self.read_slice_register(self.registers.ILA_CONTROL, slice)
        ila_control.select = ila_number
        ila_control.reset = ila_control.reset | (1 << ila_number)
        self.write_slice_register(ila_control, slice)
        ila_control.reset = ila_control.reset & ~(1 << ila_number)
        self.write_slice_register(ila_control, slice)

    def fire_single_ila(self, ila_number):
        slice = 1
        data = self.read_slice_register(self.registers.ILA_CONTROL,slice)
        data.bypass_trigger = 1
        data.select = ila_number
        self.write_slice_register(data,slice)

    def unfire_ila(self, ila_number):
        slice = 1
        data = self.read_slice_register(self.registers.ILA_CONTROL,slice)
        data.bypass_trigger = 0
        data.select = ila_number
        self.write_slice_register(data,slice)

    def is_ila_data_present(self, slice, index):
        data = self.read_slice_register(self.registers.ILA_CONTROL, slice)
        data.select = index
        self.write_slice_register(data, slice)
        sample_count = self.read_slice_register(self.registers.ILA_STATUS, slice).capture_count
        if sample_count > 0:
            return True
        return False

    def get_ila_samples(self, slice, ila_number):
        # Select given ILA
        data = self.read_slice_register(self.registers.ILA_CONTROL, slice)
        data.select = ila_number
        self.write_slice_register(data, slice)
        # Get number of samples to read
        sample_count = self.read_slice_register(self.registers.ILA_STATUS, slice).capture_count
        # Read the samples
        samples = []
        for i in range(sample_count):
            data.sample = i
            # Read low word
            data.word_in_sample = 0
            self.write_slice_register(data, slice)
            low = self.read_slice_register(self.registers.ILA_DATA, slice).Data
            # Read high word
            data.word_in_sample = 1
            self.write_slice_register(data, slice)
            high = self.read_slice_register(self.registers.ILA_DATA, slice).Data
            # Combine high and low
            sample = (high << 32) | low
            samples.append(sample)
        return samples

    def get_samples_and_create_vcd(self, slice, index, file):
        creator = vcd.VcdCreator(40, 'ila', 'AC logic analyzer version 0.1')
        for num, signalName in self.ila_names[index].items():
            creator.SetSignalName(num, signalName)
            samples = self.get_ila_samples(slice, index)
        creator.CreateVcd(file, samples, '1ns')

    def read_log_set_of_registers(self, registers, slices=SLICECOUNT, title='', log_type='debug'):
        headers = ['Register']
        data = []
        for register in registers:
            observed = self.read_slice_register(register, slice=0)
            regcount = getattr(observed, "REGCOUNT")
            for index in range(regcount):
                entry = {}
                reg_id = '' if index == 0 else f'_{index}'
                for slice in slices:
                    slice_head = f'Slice {slice}'
                    observed = self.read_slice_register(register, slice=slice, index=index)
                    if 'CNT' in observed.name() or 'COUNT' in observed.name() or 'FAKE_CTV' in observed.name():
                        entry.update({slice_head: f'{observed.value:,}'})
                    else:
                        entry.update({slice_head: f'0x{observed.value:08X}'})
                    if slice_head not in headers:
                        headers.append(slice_head)
                entry.update({'Register': f'{observed.name()}{reg_id} (0x{observed.ADDR + (index*4):X})'})
                data.append(entry)
        table = self.contruct_text_table(headers, data)
        self.Log(log_type, f'{title}{table}')
        return table

    def get_active_pins_from_ctp(self, ctp):
        active = []
        for i in range(35):
            pin = ctp & (1 << i)
            if pin != 0:
                active.append(i)
        return active

    def log_underrun_registers(self):
        for psdb, pin0, pin1 in self.paths:
            pin0.read_under_run_registers()
            pin1.read_under_run_registers()

    def zip_files_in_folder(self, folder, remove=True):
        for root, dirs, files in os.walk(folder):
            for file in files:
                if file.endswith('.zip'):
                    continue
                filename = os.path.join(root, file)
                self.zip_file(filename, remove)

    def zip_file(self, filename, remove=True):
        try:
            if filename.endswith('.txt'):
                zip_filename = filename.replace('.txt', '.zip')
            elif filename.endswith('.csv'):
                zip_filename = filename.replace('.csv', '.zip')
            with zipfile.ZipFile(zip_filename, 'w', compression=zipfile.ZIP_DEFLATED) as myzip:
                myzip.write(filename)
        except:
            pass
        finally:
            if remove:
                os.remove(filename)

    def get_int_with_fix_active_bits(self, bit_length, active_number_bits):
        value = 0
        active_bits_list = random.sample(range(0, bit_length), active_number_bits)
        for i in active_bits_list:
            value |= 1 << i
        return value, active_bits_list

    def get_active_bits_list(self, value, max_range):
        active_bits_list = []
        for i in range(max_range):
            if (value >> i) & 1:
                active_bits_list.append(i)
        return active_bits_list

    def precise_wait(self, duration):
        start = time.perf_counter()
        while (time.perf_counter() - start) < duration:
            pass