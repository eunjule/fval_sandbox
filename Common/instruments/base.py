# INTEL CONFIDENTIAL
#
# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import re

from Common import fval
from Common.register import create_field_dictionary


class Instrument(fval.Component):
    def __init__(self, name = None):
        super().__init__(name)

    def name(self):
        return self.__class__.__name__

    @staticmethod
    def is_non_zero_or_non_f(values):
        all_zeros_count = 0
        all_fs_count = 0
        if isinstance(values, str):
            values = [values]
        for value in values:
            invalid_digits = re.findall('[^0-9A-Fa-f]', value)
            if invalid_digits:
                raise Instrument.InvalidDigitsError(f'Invalid digits found in {value}')

            keep_digits_and_fs = re.sub('[^0-9A-Fa-f]', '', value)
            zeros_removed = re.sub('[0]', '', keep_digits_and_fs)
            fs_removed = re.sub('[Ff]', '', keep_digits_and_fs)

            if not zeros_removed:
                all_zeros_count += 1
            if not fs_removed or value == f'{str(0xffff)}.{str(0xffff)}':
                all_fs_count += 1
        if all_zeros_count or all_fs_count:
            return False
        else:
            return True

    class InvalidDigitsError(Exception):
        pass

    def log_device_message(self, message, log_type):
        board_name = self.name()
        message = '{}: {}'.format(board_name, message)
        self.Log(log_type, message)

    @staticmethod
    def contruct_text_table(column_headers=None, data=None, title_in=''):
        column_headers = Instrument.parse_headers(column_headers, data)
        column_width = Instrument.get_column_width(column_headers, data)
        dots = Instrument.get_dots_width(column_width)
        header_text = Instrument.get_header_text(column_headers, column_width, dots)
        title = Instrument.get_title(title_in, len(dots))
        body = Instrument.get_body_text(column_width, data, column_headers)
        return title + header_text + body

    @staticmethod
    def parse_headers(headers, data):
        if headers is None:
            headers_from_data = []
            for entry in data:
                for header in entry.keys():
                    if header not in headers_from_data:
                        headers_from_data.append(header)
            return headers_from_data
        return headers

    @staticmethod
    def get_title(title_in='', width=0):
        if title_in:
            return f'\n\t{title_in:^{width}}'
        return ''

    @staticmethod
    def get_body_text(column_width, data, headers):
        body = ''
        padding = '\t'
        for col_value in data:
            body_line = ''
            for i in headers:
                value = col_value[i]
                if value is None:
                    value = 'None'
                if isinstance(value, bool):
                    if value is True:
                        value = 'True'
                    if value is False:
                        value = 'False'
                body_line += '{:<{}}'.format(value, column_width[i])
            body_line = padding + body_line + '\n'
            body += body_line
        return body

    @staticmethod
    def get_header_text(column_headers, column_width, dots):
        columns = ''
        for header in column_headers:
            columns += '{:<{}}'.format(header, column_width[header])
        padding = '\n\t'
        header_text = padding + columns + dots
        return header_text

    @staticmethod
    def get_dots_width(column_width):
        width = 0
        for key, value in column_width.items():
            width += value
        dots = '\n\t{}\n'.format('-' * width)
        return dots

    @staticmethod
    def get_column_width(column_headers, data):
        column_width = {}
        data_width = [column_width.update({x: len(str(x)) + 3}) for x in column_headers]
        for x in data:
            for key, value in x.items():
                value_width = len(str(value)) + 3
                if key in column_width.keys():
                    if value_width > column_width[key]:
                        column_width[key] = value_width
        return column_width

    def check_almost_equal(self, observed, expected, delta):
        if abs(observed - expected) > delta:
            self.Log('error', f'CheckAlmostEqual: '
                              f'Observed: {observed} Expected: {expected} Delta: {delta}')
            return False
        return True

    def create_table_from_register(self, reg):
        entry = {'name': f'{self.name()}.{reg.name()}'}
        entry.update(create_field_dictionary(reg))
        return self.contruct_text_table(data=[entry])

    class InstrumentError(Exception):
        pass
