# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object

register_table_template = {'STATUS': None,
                           'CONTROL': None,
                           'TX_FIFO': None,
                           'RX_FIFO': None,
                           'RESET': None}


class I2cTestInterface(Object):
    I2C_STATUS_TIMEOUT = 10000
    FIFO_COUNT_BIT_SIZE = 11

    def __init__(self, instrument, i2c_addr, interface_name, interface_index):
        super().__init__()

        self.instrument = instrument
        self.register_table = None
        self.interface_name = interface_name
        self.index = interface_index
        self.i2c_addr = i2c_addr

    def update_register_table(self, register_table):
        if not isinstance(register_table, dict):
            raise I2cTestInterface.I2cTestInterfaceError(
                f'Invalid register_table parameter')
        if set(register_table.keys()) != set(register_table_template.keys()):
            raise I2cTestInterface.I2cTestInterfaceError(
                f'Invalid keys in register_table parameter '
                f'(Expected, Actual): {set(register_table_template.keys())}, '
                f'{set(register_table.keys())}')
        self.register_table = register_table

    def read_status_register(self):
        return self.instrument.read_bar_register(
            self.register_table['STATUS'], self.index)

    def get_transmit_fifo_count_error(self):
        status = self.read_status_register()
        return status.transmit_fifo_count_error

    def get_address_nak(self):
        return self.read_status_register().address_nak

    def get_transmit_fifo_count(self):
        return self.read_status_register().transmit_fifo_count

    def log_status_register(self):
        reg = self.instrument.read_bar_register(self.register_table['STATUS'],
                                                self.index)
        self.Log('info',
                 f'Status register for interface {self.index}:\n\
                 receive_fifo_count       : {reg.receive_fifo_count}\n\
                 transmit_fifo_count      : {reg.transmit_fifo_count}\n\
                 address_nak              : {reg.address_nak}\n\
                 data_nak                 : {reg.data_nak}\n\
                 transmit_fifo_count_error: {reg.transmit_fifo_count_error}\n\
                 i2c_busy_error           : {reg.i2c_busy_error}\n\
                 i2c_busy                 : {reg.i2c_busy}')

    def get_fifo_counts(self):
        reg = self.read_status_register()
        return reg.transmit_fifo_count, reg.receive_fifo_count

    def reset(self):
        self.instrument.write_bar_register(
            self.register_table['RESET'](reset=1), self.index)
        for timeout in range(I2cTestInterface.I2C_STATUS_TIMEOUT):
            reg = self.instrument.read_bar_register(
                self.register_table['STATUS'], self.index)
            # Full reset if all bits except bit 31 (i2c_busy) are 0
            if not (reg.value & 0x7FFFFFFF):
                break
        else:
            raise I2cTestInterface.I2cTestInterfaceError(
                f'I2C({self.interface_name}, {self.index}): '
                f'Timed out waiting on reset.')
        self.instrument.write_bar_register(
            self.register_table['RESET'](reset=0), self.index)

    def wait_i2c_busy(self):
        for timeout in range(I2cTestInterface.I2C_STATUS_TIMEOUT):
            reg = self.read_status_register()
            if not reg.i2c_busy and not reg.transmit_fifo_count:
                break
        else:
            self.log_status_register()
            raise I2cTestInterface.I2cTestInterfaceError('I2C({self.interface_name}, {self.index}): ' +
                'I2C({self.interface_name}, {self.index}): Timed out waiting '
                'on i2c_busy signal to de-assert.')

    def set_transmit_bit(self, enable_stop_bit=1):
        self.instrument.write_bar_register(self.register_table['CONTROL'](
            transmit=enable_stop_bit), self.index)

    def push_data_fifo(self, data):
        self.instrument.write_bar_register(self.register_table['TX_FIFO'](
            data=data), self.index)

    def pop_data_fifo(self):
        reg = self.instrument.read_bar_register(
            self.register_table['RX_FIFO'], self.index)
        return reg.data

    def write(self, address, data=None, enable_stop_bit=1):
        if isinstance(data, int):
            data = [data]
        self.wait_i2c_busy()

        self.push_data_fifo(self.i2c_addr)
        self.push_data_fifo(address)
        if data:
            for i in data:
                self.push_data_fifo(i)

        self.set_transmit_bit(enable_stop_bit)
        self.wait_i2c_busy()

    def read(self, address, num_bytes=1):
        self.write(address, enable_stop_bit=0)
        self.push_data_fifo(self.i2c_addr | 1)
        self.push_data_fifo(num_bytes)
        self.set_transmit_bit(enable_stop_bit=1)
        self.wait_i2c_busy()
        num_bytes_read = self.read_status_register().receive_fifo_count
        if num_bytes_read != num_bytes:
            raise I2cTestInterface.I2cTestInterfaceError(
                f'Invalid number of bytes read (expected, actual): '
                f'{num_bytes}, {num_bytes_read}')
        data = [self.pop_data_fifo() for i in range(num_bytes_read)]
        if num_bytes_read == 1:
            return data[0]
        else:
            return data

    class I2cTestInterfaceError(Exception):
        pass
