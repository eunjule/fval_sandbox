# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
from random import getrandbits

from Common.fval import Log, Object
from Common.instruments import tester
from Common.instruments.base import Instrument
from Common.triggers import CardType, generate_trigger, EventTriggerEncoding, EventType, SCSTriggerEncoding

AVAIL_TRIGGER_INTERFACES = ['hpcc', 'hddps']
HDMT_NUM_SLOTS = 12


class HdmtTriggerInterface(Object):
    __instance = None
    __interfaces = [None] * (HDMT_NUM_SLOTS + 1)
    __init_done = False

    @staticmethod
    def get_interface(slot):
        if HdmtTriggerInterface.__instance is None:
            HdmtTriggerInterface()

        # rc will be at __interfaces[HDMT_NUM_SLOTS]
        interface_index = HDMT_NUM_SLOTS if slot is None else slot
        if HdmtTriggerInterface.__interfaces[interface_index] is None:
            temp = tester.get_tester().get_hdmt_instrument(slot)
            if temp is not None:
                instrument = temp[0]
                HdmtTriggerInterface.__interfaces[interface_index] = \
                    HdmtTriggerInterface.__instance.create_interface(
                        instrument)

        return HdmtTriggerInterface.__interfaces[interface_index]

    @staticmethod
    def get_interfaces():
        interfaces = {None: HdmtTriggerInterface.get_interface(None)}
        rc = tester.get_tester().rc
        remove_links = []
        for link in rc.aurora_link_slots:
            try:
                interfaces[link] = \
                    HdmtTriggerInterface.get_interface(link)
            except (HdmtTriggerInterface.InterfaceNotFound,
                    Instrument.InstrumentError) as e:
                Log('warning', f'Slot {link}: {e}')
                remove_links.append(link)

        for link in remove_links:
            rc.aurora_link_slots.remove(link)

        if not HdmtTriggerInterface.__init_done:
            Log('info', f'Validating Aurora Status of all instruments')
            validate_aurora_statuses(rc.aurora_link_slots, enable_log=True)
            HdmtTriggerInterface.__init_done = True
        return interfaces

    @staticmethod
    def clear_interfaces():
        HdmtTriggerInterface.__instance = None
        HdmtTriggerInterface.__interfaces = [None] * (HDMT_NUM_SLOTS+1)
        HdmtTriggerInterface.__init_done = False

    def __init__(self):
        if HdmtTriggerInterface.__instance is not None:
            raise HdmtTriggerInterface.InvalidUse(
                f'Try HdmtTriggerInterface.get_interface()')
        else:
            super().__init__()
            HdmtTriggerInterface.__instance = self

            self.create_interface_actions = {
                'rc2': self.create_rc2_interface,
                'rc3': self.create_rc3_interface,
                'hddps': self.create_hddps_interface,
                'hpcc': self.create_hpcc_interface,
                'hvdpscombo': self.create_hvdpscombo_interface,
                'tdaubnk': self.create_tdaubnk_interface,
                'accessorycard': self.create_accessorycard_interface}

    class InvalidUse(Exception):
        pass

    def create_interface(self, instrument):
        name = instrument.name().lower()
        return self.create_interface_actions.get(name)(instrument)

    def create_rc2_interface(self, instrument):
        from Rc2.instrument.rc2_trigger_interface import create_interface
        return create_interface(instrument)

    def create_rc3_interface(self, instrument):
        from Rc3.instrument.rc3_trigger_interface import create_interface
        return create_interface(instrument)

    def create_hddps_interface(self, instrument):
        from Common.instruments.dps.hddps_trigger_interface import \
            create_interface
        return create_interface(instrument)

    def create_hpcc_interface(self, instrument):
        from Hpcc.instrument.hpcc_trigger_interface import create_interface
        return create_interface(instrument)

    def create_hvdpscombo_interface(self, instrument):
        raise HdmtTriggerInterface.InterfaceNotFound('HvdpsCombo trigger '
                                                     'interface not found')

    def create_tdaubnk_interface(self, instrument):
        raise HdmtTriggerInterface.InterfaceNotFound('TdauBnk trigger '
                                                     'interface not found')

    def create_accessorycard_interface(self, instrument):
        raise HdmtTriggerInterface.InterfaceNotFound('AccessoryCard trigger '
                                                     'interface not found')

    class InterfaceNotFound(Exception):
        pass


def generate_random_payload_with_exclude(exclude_num):
    paylod_num_bits = 19
    value = exclude_num
    n = 1
    for i in range(n):
        n += 1
        value = getrandbits(paylod_num_bits)
        if value != exclude_num:
            break
    return value


def generate_scs_trigger(slice, pin, user, data):
    trigger = SCSTriggerEncoding(card_type=CardType.SCS.value,
                                 ac_slice_id=slice,
                                 pin_id=pin,
                                 user_id=user,
                                 sensor_data=data).value
    return trigger


def generate_scs_start_trigger():
    trigger = EventTriggerEncoding(event_type=EventType.SCOPESHOT_START.value,
                                   dps_internal_control=0,
                                   card_type=CardType.EVENT.value).value
    return trigger


def generate_scs_stop_trigger():
    trigger = EventTriggerEncoding(event_type=EventType.SCOPESHOT_STOP.value,
                                   dps_internal_control=0,
                                   card_type=CardType.EVENT.value).value
    return trigger


def validate_aurora_statuses(slots, enable_log=False):
    if type(slots) != list:
        slots = [slots]
    rc = tester.get_tester().rc

    for slot in slots:
        instrument = tester.get_tester().get_hdmt_instrument(slot)[0]
        name = f'{instrument.name()}_{slot}'
        hpcc_success = True
        hddps_success = True
        rc_success = True

        if 'hpcc' in name.lower():
            dc_0 = instrument.dc[0]
            if dc_0.read_trigger_errors() != 0:
                dc_0.log_trigger_errors(log_level='error')
                system_pause_on_aurora_error()
                Log('info',
                    f'{name}: Clearing trigger errors by resetting trigger '
                    f'and aurora domains')
                dc_0.clear_trigger_errors()
                hpcc_success = False
            if enable_log and hpcc_success:
                dc_0.log_trigger_errors(log_level='info')
        elif 'hddps' in name.lower():
            subsslot_0 = instrument.subslots[0]
            if not subsslot_0.AuroraIsReady():
                subsslot_0.log_aurora_status(log_level='error')
                system_pause_on_aurora_error()
                Log('info',
                    f'{name}: Clearing trigger errors by setting (self '
                    f' clearing) Soft and Hard Error bits in STATUS register.')
                subsslot_0.clear_trigger_errors()
                hddps_success = False
            if enable_log and hddps_success:
                subsslot_0.log_aurora_status()
                subsslot_0.log_aurora_error_counts()
        else:
           Log('info', f'{name}: Instrument not currently supported in '
                       f'trigger testing')

        if not rc.aurora_link_is_stable(slot):
            rc.log_aurora_status(slot, log_level='error')
            rc.log_aurora_error_counts(slot)
            system_pause_on_aurora_error()
            Log('info',
                f'{rc.name()}_{slot}: Clearing trigger errors by resetting '
                f'fifos and clearing error counts')
            rc.clear_aurora_error_and_fifos(slot)
            rc_success = False
        if enable_log and rc_success:
            rc.log_aurora_status(slot)
            rc.log_aurora_error_counts(slot)

        if not rc_success or not hpcc_success or not hddps_success:
            system_pause_on_aurora_error()


def system_pause_on_aurora_error():
    if os.getenv('DBG_AURORA_ERROR') is not None:
        os.system('pause')


class AuroraStatusError(Exception):
    pass


def run_up_triggger_mini_test(num_triggers):
    interfaces = HdmtTriggerInterface.get_interfaces()
    rc_interface = interfaces.get(None)
    for slot, interface in interfaces.items():
        if slot is not None:
            validate_aurora_statuses(slot)

            for sub_interface in interface:
                sub_name = sub_interface.name()

                for i in range(num_triggers):
                    trigger = generate_trigger(
                        card_type=CardType.INVALID.name,
                        payload=generate_random_payload_with_exclude(0))

                    sub_interface.send_up_trigger(trigger)

                    actual_trigger = \
                        rc_interface[slot].check_for_up_trigger(trigger)
                    up_trigger_pass = (actual_trigger == trigger)
                    if not up_trigger_pass:
                        Log('error',
                            _error_msg(rc_interface[slot].name(),
                                       sub_name,
                                       trigger,
                                       actual_trigger))
            validate_aurora_statuses(slot)


def _error_msg(receive_name, sender_name, expected, actual):
    return f'{sender_name}->{receive_name} failed to receive trigger ' \
           f'(expected, actual): 0x{expected:08X}, 0x{actual:08X}'


def empty_hddps_trigger_buffers():
    hddpss = tester.get_tester().get_instrument_present('hddps')
    for hddps in hddpss:
        hddps.ensure_initialized()
        for subslot in hddps.subslots:
            subslot.empty_trigger_buffer()
			
