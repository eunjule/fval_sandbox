################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: pca9505.py
#-------------------------------------------------------------------------------
#     Purpose: Class for pca9505, NXP 40-bit I2C-bus I/O port with RESET, OE, and INT
#-------------------------------------------------------------------------------
#  Created by: Danielle Love
#        Date: 04/21/15
#       Group: HDMT FPGA Validation
################################################################################

from Common import hilmon as hil
from Common.instruments.base import Instrument

class Pca9505(Instrument):
    AUTOINCREMENTFLAG = 0x80
    NUMBEROFPCA9505PORTS = 5
    INPUTPORT0 = 0
    INPUTPORT1 = 1
    INPUTPORT2 = 2
    INPUTPORT3 = 3
    INPUTPORT4 = 4
    OUTPUTPORT0 = 8
    OUTPUTPORT1 = 9
    OUTPUTPORT2 = 10
    OUTPUTPORT3 = 11
    OUTPUTPORT4 = 12
    POLARITYINVERSIONPORT0 = 16
    POLARITYINVERSIONPORT1 = 17
    POLARITYINVERSIONPORT2 = 18
    POLARITYINVERSIONPORT3 = 19
    POLARITYINVERSIONPORT4 = 20
    CONFIGURATIONPORT0 = 24
    CONFIGURATIONPORT1 = 25
    CONFIGURATIONPORT2 = 26
    CONFIGURATIONPORT3 = 27
    CONFIGURATIONPORT4 = 28
    
    handle = -1
    address = -1
    
    def __init__(self,handle,address,name = None):
        super(Instrument, self).__init__(name)
        self.handle = handle
        self.address = address

    def WriteData(self,data):
        self.Log("debug","PCA9505 Writing: {}".format(data))
        hil.cypI2cWrite(self.handle,self.address,bytes(data))
        

