################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: dps.py
#-------------------------------------------------------------------------------
#     Purpose: Class for one HDDPS card
#-------------------------------------------------------------------------------
#  Created by: Renuka Agrawal
#        Date: 10/30/15
#       Group: HDMT FPGA Validation
################################################################################

import time


from Common import configs
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps.dps import DpsBase
from Common.instruments.dps import hddps_registers
from Common.instruments.dps import ad5560_registers
from Common.instruments.dps import max6662_registers
from Common.instruments.dps import isl55180_registers
from Common.instruments.hdmt_trigger_interface import validate_aurora_statuses

class Hddps(DpsBase):
    registers = hddps_registers
    ad5560regs = ad5560_registers
    max6662regs = max6662_registers
    isl55180regs = isl55180_registers

    def __init__(self, slot, rc, name = None):
        super().__init__(slot,rc,name)

        mainBoard = HddpsSubslot(self, slot, 0, rc)
        mainBoard.railTypes = ['LC','VLC']
        mainBoard.RAIL_COUNT = {'LC': 10, 'VLC': 16}
        daughterBoard = HddpsSubslot(self, slot, 1, rc)
        daughterBoard.railTypes = ['HC']
        daughterBoard.RAIL_COUNT = {'HC': 10}
        self.subslots.append(mainBoard)
        self.subslots.append(daughterBoard)
        self.model = None
        self._is_initialized = False

        self.instrumentblt = CachingBltInfo(callback_function=self.read_instrument_blt,name= 'HDDPS Instrument slot {}'.format(slot))
        self.main_boardblt = CachingBltInfo(callback_function=self.read_main_board_blt,name='HDDPS Main Board slot {}'.format(slot))
        self.daughter_boardblt = CachingBltInfo(callback_function=self.read_daughter_board_blt,name='HDDPS Daughter Board slot {}'.format(slot))

    def Initialize(self):
        # Load image
        super().Initialize()

        self.Log('info', 'Initializing image')
        self.log_blt_info()
        state = hil.bpTiuAuxPowerState()
        self.Log('info', 'The TIU power was enabled. The power state is {}'.format(state))
        self.Log('info', 'Previous HDDPS MB Image slot {} subslot {}: {}'.format(self.slot, 0, hex(self.subslots[0].GetFpgaVersion())))

        if configs.SKIP_INIT:
            self.Log('info', f'Skipping initialization for {self.name()}')
        else:
            self.Log('info', 'Initializing....')
            if configs.HDDPS_MB_FPGA_LOAD:
                image = configs.HDDPS_MB_FPGA_IMAGE
                name = self.subslots[0].name()
                self.Log('info', f'Flashing HDDPS MB FPGA with image '
                                 f'from \'{image}\'')
                try:
                    self.subslots[0].FpgaLoad(configs.HDDPS_MB_FPGA_IMAGE)
                    self.Log('info', f'{name}: FPGA load succeeded')
                except RuntimeError as e:
                    raise Hddps.InstrumentError(
                        f'{name}: Failed to load FPGA. {e}')
            else:
                self.Log('warning', f'{self.subslots[0].name()} FPGA was not loaded due to configs setting')

            self.Log('info', 'Current HDDPS MB Image slot {} subslot {}: {}'.format(self.slot, 0, hex(self.subslots[0].GetFpgaVersion())))

            self.subslots[0].ResetDdr()


        self.train_aurora_link()
        status = self.subslots[0].AuroraIsReady()
        if status:
            self.Log('info','AURORA bus is ready for use')
        else:
            raise Hddps.AuroraBusException('AURORA bus is NOT ready for use')

        if not configs.SKIP_INIT:
            for subslot in [0]:
                if self.subslots[subslot].GetFpgaVersion() in configs.HDDPS_LEGACY_TC_LOOPING_FPGAS:
                    self.subslots[subslot].LegacyTcLooping = True
                    self.registers.GLOBAL_ALARMS = self.registers.GLOBAL_ALARMS_LEGACY_TC_LOOPING
                    self.registers.non_critical_global_alarms = self.registers.non_critical_global_alarms_legacy_tc_looping
                    self.registers.critical_global_alarms = self.registers.critical_global_alarms_legacy_tc_looping

                if self.subslots[subslot].GetFpgaVersion() in configs.HDDPS_NON_VM_MODE_FPGAS:
                    self.subslots[subslot].VmMode = True

            self.subslots[0].InitializeAd5560Rails()
            self.subslots[0].InitializeIsl55180Rails()
            self.subslots[0].ResetThermalAlarmLimits()
            self.subslots[0].clearHclcSampleAlarmRegister()
            self.subslots[0].ClearVlcSampleAlarms()
            self.subslots[0].CheckAlarmRegisters()
            self.subslots[0].ClearTriggerQueue()
            self.subslots[0].EnableAlarms()
            self.subslots[0].SetRailsToSafeState()
            self.subslots[0].UnGangAllRails()
            self.subslots[0].DisableAllUhcs()

        self.Log('info', 'Previous HDDPS DB Image slot {} subslot {}: {}'.format(self.slot, 1, hex(self.subslots[1].GetFpgaVersion())))

        if not configs.SKIP_INIT:
            if configs.HDDPS_DB_FPGA_LOAD:
                self.Log('info', 'Flashing HDDPS DB FPGA with image from \'{}\''.format(configs.HDDPS_DB_FPGA_IMAGE))
                self.subslots[1].FpgaLoad(configs.HDDPS_DB_FPGA_IMAGE)
            else:
                self.Log('warning', f'{self.subslots[1].name()} FPGA was not loaded due to configs setting')

            self.Log('info', 'Current HDDPS DB Image slot {} subslot {}: {}'.format(self.slot, 1, hex(self.subslots[1].GetFpgaVersion())))

            for subslot in [1]:
                if self.subslots[subslot].GetFpgaVersion() in configs.HDDPS_LEGACY_TC_LOOPING_FPGAS:
                    self.subslots[subslot].LegacyTcLooping = True
                    self.registers.GLOBAL_ALARMS = self.registers.GLOBAL_ALARMS_LEGACY_TC_LOOPING
                    self.registers.non_critical_global_alarms = self.registers.non_critical_global_alarms_legacy_tc_looping
                    self.registers.critical_global_alarms = self.registers.critical_global_alarms_legacy_tc_looping

                if self.subslots[subslot].GetFpgaVersion() in configs.HDDPS_NON_VM_MODE_FPGAS:
                    self.subslots[subslot].VmMode = True

            self.subslots[1].ResetDdr()

            self.subslots[1].InitializeAd5560Rails()
            self.subslots[1].ClearHclcRailAlarms()
            self.subslots[1].InitializeIsl55180Rails()
            self.subslots[1].ResetThermalAlarmLimits()
            self.subslots[1].clearHclcSampleAlarmRegister()
            self.subslots[1].ClearVlcSampleAlarms()
            self.subslots[1].ClearTriggerQueue()
            self.subslots[1].SetRailsToSafeState()
            self.subslots[1].UnGangAllRails()              #Scaled up to 12 rails
            self.subslots[1].DisableAllUhcs()
            self.subslots[1].EnableAlarms()
            self.subslots[1].CheckAlarmRegisters()

        self.validate_aurora_bus()
        self._is_initialized = True

    class AuroraBusException(Exception):
        pass

    def validate_aurora_bus(self):
        validate_aurora_statuses(self.slot)

    def train_aurora_link(self):
        if not self.using_rc2():
            num_retries = 20
            name = f'{self.name()}_{self.slot}'

            self.Log('info', f'{name}: Attempting to train Aurora link...')
            for retry in range(num_retries):
                self.subslots[0].reset_aurora()

                self.rc.reset_aurora_bus(self.slot)
                self.rc.clear_aurora_error_counts(self.slot)

                rc_aurora_status = self.rc.aurora_link_is_stable(self.slot)
                dps_aurora_status = self.subslots[0].AuroraIsReady()
                if rc_aurora_status and dps_aurora_status:
                    self.Log('info',
                             f'{name}: Aurora link trained in {retry} tries')
                    break
            else:
                raise Hddps.InstrumentError(f'{name}: Unable to train aurora '
                                            f'link')

    def using_rc2(self):
        return self.rc.name().upper() == 'RC2'

    def ensure_initialized(self):
        if not self._is_initialized:
            self.Initialize()

    def read_blt_roms(self):
        self.hddps_ss0_blt = hil.hddpsBltBoardRead(self.slot,0)
        self.hddps_ss1_blt = hil.hddpsBltBoardRead(self.slot,1)



    def discover(self):
        ss0Found = False
        ss1Found = False
        slot = self.slot


        try:
            subslot = 0  # motherboard (low current)
            hil.hddpsConnect(slot, subslot)
            hil.hddpsDisconnect(slot, subslot)
            ss0Found = True
        except RuntimeError:
            pass
        try:
            subslot = 1  # 1 = daughterboard (high current)
            hil.hddpsConnect(slot, subslot)
            hil.hddpsDisconnect(slot, subslot)
            ss1Found = True
        except RuntimeError:
            pass



        if ss0Found == ss1Found == True:
            return [self]
        else:
            return []


    def read_instrument_blt(self):
        return hil.dpsBltInstrumentRead (self.slot)

    def read_main_board_blt(self):
        return hil.dpsBltBoardRead(self.slot,0)

    def read_daughter_board_blt(self):
        return hil.dpsBltBoardRead(self.slot,1)


    def log_blt_info(self):
        self.instrumentblt.write_to_log()
        self.main_boardblt.write_to_log()
        self.daughter_boardblt.write_to_log()
