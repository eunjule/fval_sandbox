################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes
from ctypes import c_uint

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

import Common.register as register

module_dict = globals()

def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvdpsRegister):
                if attribute is not HvdpsRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvdpsRegister):
                if attribute is not HvdpsRegister:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvdpsStruct):
                if attribute is not HvdpsStruct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvdpsStruct):
                if attribute is not HvdpsStruct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class HvdpsRegister(register.Register):
    BASEMUL = 4
    BAR = 1



class HvdpsStruct(register.Register):
    ADDR = None


    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class DUMMY_STRUCT(HvdpsStruct):
    _fields_ = [('Data', ctypes.c_uint, 32)]


class BAR1_SCRATCH(HvdpsRegister):
    BAR = 1
    ADDR = 0x0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_VERSION(HvdpsRegister):
    BAR = 1
    ADDR = 0x4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RESETS(HvdpsRegister):
    BAR = 1
    ADDR = 0x8
    _fields_ = [('Unused', c_uint, 1),
                ('AuroraReset', c_uint, 1),
                ('DDRControllerReset', c_uint, 1),
                ('BlockAuroraReceive', c_uint, 1),
                ('Data', c_uint, 28)]


class STATUS(HvdpsRegister):
    BAR = 1
    ADDR = 0xc
    _fields_ = [('AuroraPLLLock', c_uint, 1),
                ('AuroraLaneUp', c_uint, 1),
                ('AuroraSoftError', c_uint, 1),
                ('AuroraHardError', c_uint, 1),
                ('AuroraChannelUp', c_uint, 1),
                ('DDRControllerPLLLock', c_uint, 1),
                ('DDRCalibrationDone', c_uint, 1),
                ('Data', c_uint, 25), ]


class SLOT_ID(HvdpsRegister):
    BAR = 1
    ADDR = 0x10
    _fields_ = [('Data', ctypes.c_uint, 32)]


class EXECUTE_TRIGGER(HvdpsRegister):
    BAR = 1
    ADDR = 0x14
    _fields_ = [('ByteAddress', c_uint, 31),
                ('Execute', c_uint, 1)]


class EXECUTE_INTERRUPT(HvdpsRegister):
    BAR = 1
    ADDR = 0x18
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SET_SAFE_STATE(HvdpsRegister):
    BAR = 1
    ADDR = 0x1c
    _fields_ = [('RailSafeBit', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 31)]

class TRIG_RX_TEST_FIFO_DATA_COUNT(HvdpsRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Data', ctypes.c_uint, 32)]

class MEM_WR_DATA_TAG(HvdpsRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MEM_RD_DATA_TAG(HvdpsRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MEM_ADDRESS_TAG(HvdpsRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRIGGERUPTEST(HvdpsRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRIGGERDNTEST(HvdpsRegister):
    BAR = 1
    ADDR = 0x54
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SAMPLE_COLLECTOR_INDEX(HvdpsRegister):
    BAR = 1
    ADDR = 0x68
    _fields_ = [('SelectedIndex', c_uint, 5),
                ('Reserved', c_uint, 11),
                ('SelectedGroup', c_uint, 3),
                ('Reserved', c_uint, 13)]

class SAMPLE_COLLECTOR_COUNT(HvdpsRegister):
    BAR = 1
    ADDR = 0x6c
    _fields_ = [('SampleCount', c_uint, 9),
                ('Data', c_uint, 23)]

class SAMPLE_COLLECTOR_DATA(HvdpsRegister):
    BAR = 1
    ADDR = 0x70
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AURORA_ERROR_COUNTS(HvdpsRegister):
    BAR = 1
    ADDR = 0x74
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_DOMAIN_ID(HvdpsRegister):
    BAR= 1
    ADDR = 0x80
    REGCOUNT = 8
    _fields_ = [('DutDomainId', c_uint, 6),
                ('DutValidBit', c_uint, 1),
                ('Data', c_uint, 25)]

class HCLC_UHC_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xa0
    REGCOUNT = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]



class VLC_UHC0_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xc0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC1_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xc4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC2_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xc8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC3_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xcc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC4_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xd0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC5_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xd4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC6_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xd8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC7_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xdc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_UHC_RAIL_CONFIG(HvdpsRegister):
    BAR = 1
    ADDR = 0xe0
    REGCOUNT = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class RAIL_MASTER_MASK(HvdpsRegister):
    BAR = 1
    ADDR = 0x100
    _fields_ = [('HcRailIndicator', c_uint, 8),
                ('Data', c_uint, 24)]


class REMOTE_SLAVES_MASK(HvdpsRegister):
    BAR = 1
    ADDR = 0x104
    _fields_ = [('SlaveSlotIndicator', c_uint, 8),
                ('Data', c_uint, 24)]


class REMOTE_SLAVES_BOARD_MASK(HvdpsRegister):
    BAR = 1
    ADDR = 0x108
    _fields_ = [('SlavesOnRemoteBoard', c_uint, 12),
                ('Data', c_uint, 20)]


class HC_I_GANG_REG(HvdpsRegister):
    BAR = 1
    ADDR = 0x10c
    _fields_ = [('RailBit', c_uint, 8),
                ('Unused', c_uint, 2),
                ('CrsBrdGangEn', c_uint, 1),
                ('Reserved', c_uint,21)]

class RAIL_GANG_MASTER(HvdpsRegister):
    BAR= 1
    ADDR = 0x110
    _fields_ = [('RailBit', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)]

class HC_IRANGE_24A_19A(HvdpsRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_SLAVE_POWER_STATE(HvdpsRegister):
    BAR = 1
    ADDR = 0x14c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_CACHE_UPDATE_DISABLE(HvdpsRegister):
    BAR = 1
    ADDR = 0x150
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_RAILS_LOOPING_COMPLETE(HvdpsRegister):
    BAR = 1
    ADDR = 0x160
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_RAILS_LOOPING_COMPLETE(HvdpsRegister):
    BAR = 1
    ADDR = 0x164
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_RAIL_MODE_VAL(HvdpsRegister):
    BAR = 1
    ADDR = 0x168
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_RAIL_MODE_VAL(HvdpsRegister):
    BAR = 1
    ADDR = 0x16c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HC_EN_PWM_R(HvdpsRegister):
    BAR = 1
    ADDR = 0x170
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5560_V_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x174
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5560_CL_EN_STATUS(HvdpsRegister):
    BAR = 1
    ADDR = 0x178
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DPS_rail_v(HvdpsRegister):
    BAR = 1
    BASEMUL = 8
    ADDR = 0x180
    REGCOUNT = 8
    _fields_ = [('SoftSpanCode', ctypes.c_uint,3),
                ('ChannelId', ctypes.c_uint, 3),
                ('VoltageValue', ctypes.c_uint, 18),
                ('Data', ctypes.c_uint, 8)]


class DPS_rail_i(HvdpsRegister):
    BAR = 1
    BASEMUL = 8
    ADDR = 0x184
    REGCOUNT = 8
    _fields_ = [('SoftSpanCode', ctypes.c_uint,3),
                ('ChannelId', ctypes.c_uint, 3),
                ('CurrentValue', ctypes.c_uint, 18),
                ('Data', ctypes.c_uint, 8)]


class VLC_rail00_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x200
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail00_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x204
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail01_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x208
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail01_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x20c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail02_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x210
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail02_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x214
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail03_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x218
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail03_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x21c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail04_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x220
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail04_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x224
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail05_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x228
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail05_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x22c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail06_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x230
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail06_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x234
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail07_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x238
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail07_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x23c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail08_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x240
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail08_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x244
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail09_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x248
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_rail09_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x24c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail10_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x250
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail10_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x254
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail11_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x258
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail11_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x25c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail12_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x260
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail12_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x264
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail13_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x268
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail13_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x26c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail14_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x270
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail14_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x274
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail15_v(HvdpsRegister):
    BAR = 1
    ADDR = 0x278
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_rail15_i(HvdpsRegister):
    BAR = 1
    ADDR = 0x27c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_00_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x280
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_01_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x284
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_02_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x288
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_03_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x28c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_04_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x290
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_05_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x294
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_06_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x298
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_07_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x29c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_08_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2a0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RAIL_09_V_ADC_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2a4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class ADC_I_CAL_ADDR(HvdpsRegister):
    BAR = 1
    ADDR = 0x2b0
    _fields_ = [('RamAddress', ctypes.c_uint, 8),
                ('RamSelect', ctypes.c_uint, 24)]

class ADC_I_CAL_DATA(HvdpsRegister):
    BAR = 1
    ADDR = 0x2b4
    _fields_ = [('Offset', ctypes.c_uint, 16),
                ('Gain', ctypes.c_uint, 16)]

class VLC_00_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2c0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_01_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2c4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_02_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2c8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_03_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2cc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_04_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2d0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_05_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2d4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_06_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2d8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_07_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2dc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_08_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2e0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_09_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2e4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_10_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2e8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_11_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2ec
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_12_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2f0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_13_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2f4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_14_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2f8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_15_ADC_V_GAIN_OFFSET(HvdpsRegister):
    BAR = 1
    ADDR = 0x2fc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ENABLE_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x600
    _fields_ = [('AlarmHandler', c_uint, 1),
                ('Data', c_uint, 31)]



class GLOBAL_ALARMS_LEGACY_TC_LOOPING(HvdpsRegister):
    BAR = 1
    ADDR = 0x604
    _fields_ = [('TqNotifyAlarmDut0', c_uint, 1),
                ('TqNotifyAlarmDut1', c_uint, 1),
                ('TqNotifyAlarmDut2', c_uint, 1),
                ('TqNotifyAlarmDut3', c_uint, 1),
                ('TqNotifyAlarmDut4', c_uint, 1),
                ('TqNotifyAlarmDut5', c_uint, 1),
                ('TqNotifyAlarmDut6', c_uint, 1),
                ('TqNotifyAlarmDut7', c_uint, 1),
                ('HclcRailAlarms', c_uint, 1),
                ('HclcCFoldAlarms', c_uint, 1),
                ('HclcSampleAlarms', c_uint, 1),
                ('UnassignedAlarmBit11', c_uint, 1),
                ('UnassignedAlarmBit12', c_uint, 1),
                ('UnassignedAlarmBit13', c_uint, 1),
                ('BrdTempAlarm', c_uint, 1),
                ('AdTempAlarm', c_uint, 1),
                ('UnassignedAlarmBit16', c_uint, 1),
                ('UnassignedAlarmBit17', c_uint, 1),
                ('TrigExecAlarm', c_uint, 1),
                ('Max6627VicorTempAlarm', c_uint,1),
                ('HclcTurboSampleAlarm', c_uint, 1),
                ('DutSampleAlarm', c_uint, 1),
                ('DutTurboSampleAlarm', c_uint, 1),
                ('DutRailAlarms', c_uint, 1),
                ('OvAlarms', c_uint, 1),
                ('DutOvUvCompAlarms', c_uint, 1),
                ('UnassignedAlarmBit26', c_uint, 1),
                ('UnassignedAlarmBit27', c_uint, 1),
                ('UnassignedAlarmBit28', c_uint, 1),
                ('UnassignedAlarmBit29', c_uint, 1),
                ('UnassignedAlarmBit30', c_uint, 1),
                ('UnassignedAlarmBit31', c_uint, 1)
                ]
non_critical_global_alarms_legacy_tc_looping = ['TqNotifyLCHC','TqNotifyVlc','TqNotifyAlarmDut0','TqNotifyAlarmDut1','TqNotifyAlarmDut2','TqNotifyAlarmDut3','TqNotifyAlarmDut4','TqNotifyAlarmDut5','TqNotifyAlarmDut6','TqNotifyAlarmDut7']
critical_global_alarms_legacy_tc_looping = [x for x in register.get_field_names(GLOBAL_ALARMS_LEGACY_TC_LOOPING) if x not in non_critical_global_alarms_legacy_tc_looping]

class GLOBAL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x604
    _fields_ = [('TqNotifyLVM', c_uint, 1),
                ('TqNotifyHV', c_uint, 1),
                ('UnassignedAlarmBit2To7', c_uint, 6),
                ('HclcRailAlarms', c_uint, 1),
                ('HclcCFoldAlarms', c_uint, 1),
                ('HclcSampleAlarms', c_uint, 1),
                ('UnassignedAlarmBit11', c_uint, 1),
                ('UnassignedAlarmBit12', c_uint, 1),
                ('UnassignedAlarmBit13', c_uint, 1),
                ('BrdTempAlarm', c_uint, 1),
                ('AdTempAlarm', c_uint, 1),
                ('UnassignedAlarmBit16', c_uint, 1),
                ('UnassignedAlarmBit17', c_uint, 1),
                ('TrigExecAlarm', c_uint, 1),
                ('Max6627VicorTempAlarm', c_uint,1),
                ('HclcTurboSampleAlarm', c_uint, 1),
                ('DutSampleAlarm', c_uint, 1),
                ('DutTurboSampleAlarm', c_uint, 1),
                ('DutRailAlarms', c_uint, 1),
                ('OvAlarms', c_uint, 1),
                ('TriggerLossAlarm',c_uint,1),
                ('UnassignedAlarmBit26', c_uint, 1),
                ('UnassignedAlarmBit27', c_uint, 1),
                ('UnassignedAlarmBit28', c_uint, 1),
                ('UnassignedAlarmBit29', c_uint, 1),
                ('UnassignedAlarmBit30', c_uint, 1),
                ('UnassignedAlarmBit31', c_uint, 1)
                ]
non_critical_global_alarms = ['TqNotifyHV','TqNotifyLVM','TqNotifyAlarmDut0','TqNotifyAlarmDut1','TqNotifyAlarmDut2','TqNotifyAlarmDut3','TqNotifyAlarmDut4','TqNotifyAlarmDut5','TqNotifyAlarmDut6','TqNotifyAlarmDut7']
critical_global_alarms = [x for x in register.get_field_names(GLOBAL_ALARMS) if x not in non_critical_global_alarms]

class HCLC_SAMPLE_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x608
    _fields_ = [('HclcDut0SampleStart', c_uint, 1),
                ('HclcDut0OutOfBounds', c_uint, 1),
                ('HclcDut1SampleStart', c_uint, 1),
                ('HclcDut1OutOfBounds', c_uint, 1),
                ('HclcDut2SampleStart', c_uint, 1),
                ('HclcDut2OutOfBounds', c_uint, 1),
                ('HclcDut3SampleStart', c_uint, 1),
                ('HclcDut3OutOfBounds', c_uint, 1),
                ('HclcDut4SampleStart', c_uint, 1),
                ('HclcDut4OutOfBounds', c_uint, 1),
                ('HclcDut5SampleStart', c_uint, 1),
                ('HclcDut5OutOfBounds', c_uint, 1),
                ('HclcDut6SampleStart', c_uint, 1),
                ('HclcDut6OutOfBounds', c_uint, 1),
                ('HclcDut7SampleStart', c_uint, 1),
                ('HclcDut7OutOfBounds', c_uint, 1),
                ('HclcSamplesLost', c_uint, 1),
                ('Reserved', c_uint, 15)]


class VLC_SAMPLE_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x60c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRIG_EXEC_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x614
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_RAILS_FOLDED(HvdpsRegister):
    BAR = 1
    ADDR = 0x618
    _fields_ = [('DutRailFolded', ctypes.c_uint, 16),
                ('DutRailBusy', ctypes.c_uint, 16)]



class HCLC_RAILS_FOLDED(HvdpsRegister):
    BAR = 1
    ADDR = 0x624
    _fields_ = [('HCLCRail', ctypes.c_uint, 16),
                ('RailBusy', ctypes.c_uint, 16)]


class VLC_RAILS_FOLDED(HvdpsRegister):
    BAR = 1
    ADDR = 0x628
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_CL_MASK(HvdpsRegister):
    BAR = 1
    ADDR = 0x62c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_OSC_DETECT(HvdpsRegister):
    BAR = 1
    ADDR = 0x630
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_RAILS_BUSY_VALID(HvdpsRegister):
    BAR = 1
    ADDR = 0x634
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_RAILS_BUSY_VALID(HvdpsRegister):
    BAR = 1
    ADDR = 0x638
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_RAILS_BUSY_VALID(HvdpsRegister):
    BAR = 1
    ADDR = 0x63c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_CONTROL(HvdpsRegister):
    BAR = 1
    ADDR = 0x640
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_STOP_TRIGGER(HvdpsRegister):
    BAR = 1
    ADDR = 0x644
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_TRIGGER_FIFO_TIMESTAMP(HvdpsRegister):
    BAR = 1
    ADDR = 0x648
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_TRIGGER_FIFO_PAYLOAD(HvdpsRegister):
    BAR = 1
    ADDR = 0x64c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_HCLC_END_TIMESTAMP(HvdpsRegister):
    BAR = 1
    ADDR = 0x650
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_VLC_END_TIMESTAMP(HvdpsRegister):
    BAR = 1
    ADDR = 0x654
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_HCLC_END_SAMPLE_ADDRESS(HvdpsRegister):
    BAR = 1
    ADDR = 0x658
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SSHOT_VLC_END_SAMPLE_ADDRESS(HvdpsRegister):
    BAR = 1
    ADDR = 0x65c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5560_TURBO_SAMPLE_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x660
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_SAMPLE_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x664
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x668
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x680
    _fields_ = [('Hclc0Alarms', c_uint, 1),
                ('Hclc1Alarms', c_uint, 1),
                ('Hclc2Alarms', c_uint, 1),
                ('Hclc3Alarms', c_uint, 1),
                ('Hclc4Alarms', c_uint, 1),
                ('Hclc5Alarms', c_uint, 1),
                ('Hclc6Alarms', c_uint, 1),
                ('Hclc7Alarms', c_uint, 1),
                ('Data', c_uint, 24)]



class HCLC_PER_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x684
    REGCOUNT = 10
    _fields_= [('ClampAlarm', c_uint, 1),
             ('KelvinAlarm', c_uint, 1),
             ('ComparatorLowAlarm', c_uint, 1),
             ('ComparatorHighAlarm', c_uint, 1),
             ('VSlewRampAlarm', c_uint, 1),
             ('Reserved0', c_uint, 1),
             ('HcOscAlarm', c_uint, 1),
             ('TcLoopAlarm', c_uint, 1),
             ('AD5560CorruptionAlarm', c_uint, 1),
             ('Reserved1', c_uint, 1),
             ('MAX6627ThermalADCLowAlarm', c_uint, 1),
             ('MAX6627ThermalADCHighAlarm', c_uint, 1),
             ('MAX6627BJTLowAlarm', c_uint, 1),
             ('MAX6627BJTHighAlarm', c_uint, 1),
             ('RailBusyAlarm', c_uint, 1),
             ('Reserved2', c_uint, 1),
             ('AD5560UvAlarm', c_uint, 1),
             ('AD5560OvAlarm', c_uint, 1),
             ('Data', c_uint, 14)]



class VLC_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6b4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_00_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6b8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_01_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6bc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_02_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6c0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_03_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6c4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_04_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6c8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_05_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6cc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_06_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6d0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_07_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6d4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_08_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6d8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_09_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6dc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_10_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6e0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_11_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6e4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_12_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6e8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_13_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6ec
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_14_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6f0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_15_RAIL_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x6f4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_CONTROLLED_FOLD_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x700
    _fields_ = [('Rail0Alarm', c_uint, 1),
                ('Rail1Alarm', c_uint, 1),
                ('Rail2Alarm', c_uint, 1),
                ('Rail3Alarm', c_uint, 1),
                ('Rail4Alarm', c_uint, 1),
                ('Rail5Alarm', c_uint, 1),
                ('Rail6Alarm', c_uint, 1),
                ('Rail7Alarm', c_uint, 1),
                ('Reserved', c_uint, 24)]


class HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x704
    REGCOUNT = 12
    _fields_ = [('AssertTestData', c_uint, 8),
                ('AssertTestCmd', c_uint, 8),
                ('CFoldActualData', c_uint, 8),
                ('CFoldReasonCode', c_uint, 8)]


class VLC_CONTROLLED_FOLD_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x734
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_00_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x738
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_01_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x73c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_02_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x740
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_03_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x744
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_04_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x748
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_05_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x74c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_06_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x750
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_07_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x754
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_08_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x758
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_09_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x75c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_10_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x760
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_11_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x764
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_12_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x768
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_13_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x76c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_14_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x770
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_15_CONTROLLED_FOLD_DETAILS(HvdpsRegister):
    BAR = 1
    ADDR = 0x774
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_00_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x780
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_01_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x784
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_02_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x788
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_03_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x78c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_04_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x790
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_05_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x794
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_06_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x798
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_07_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x79c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_08_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x7a0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_09_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x7a4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_10_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x7a8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_11_CLAMP_ALARM_CURRENT(HvdpsRegister):
    BAR = 1
    ADDR = 0x7ac
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_00_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7c0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_01_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7c4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_02_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7c8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_03_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7cc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_04_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7d0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_05_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7d4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_06_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7d8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_07_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7dc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_08_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7e0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_09_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7e4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_10_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7e8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_11_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7ec
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_12_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7f0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_13_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7f4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_14_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7f8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_15_LAST_CURRENT_RANGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x7fc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class P2P_CTRL(HvdpsRegister):
    BAR = 1
    ADDR = 0x800
    _fields_ = [('Data', ctypes.c_uint, 32)]


class S2HControlReg(HvdpsRegister):
    ADDR = 0x0804
    _fields_ = [('S2HStreamingStart', c_uint, 1),
                ('S2HStreamingStop', c_uint, 1),
                ('S2HStreamingReset', c_uint, 1),
                ('Reserved', c_uint, 28),
                ('S2HStreamingStatus', c_uint, 1),]


class P2P_START_ADDR_HI(HvdpsRegister):
    BAR = 1
    ADDR = 0x808
    _fields_ = [('Data', ctypes.c_uint, 32)]


class P2P_START_ADDR_LO(HvdpsRegister):
    BAR = 1
    ADDR = 0x80c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class S2HAddressHighReg(HvdpsRegister):
    ADDR = 0x0810
    _fields_ = [('S2HAddressHigh', c_uint, 32)]

class S2HAddressLowReg(HvdpsRegister):
    ADDR = 0x0814
    _fields_ = [('S2HAddressLow',c_uint, 32)]

class S2HValidPacketIndexReg(HvdpsRegister):
    ADDR = 0x0818
    _fields_ = [('S2HValidPacketIndex', c_uint, 16),
                ('Reserved', c_uint, 16)]

class S2HPacketByteCountReg(HvdpsRegister):
    ADDR = 0x081C
    _fields_ = [('S2HPacketByteCount', c_uint, 16),
                ('Reserved', c_uint, 16)]

class S2HPacketCountReg(HvdpsRegister):
    ADDR = 0x0820
    _fields_ = [('S2HPacketCount', c_uint, 16),
                ('Reserved',c_uint, 16)]

class S2HConfigurationReg(HvdpsRegister):
    ADDR = 0x0824
    _fields_ = [('S2HConfigurationDone', c_uint, 1),
                ('Reserved',c_uint, 31)]

class ECA_CRITICAL_COUNT(HvdpsRegister):
    BAR = 1
    ADDR = 0x828
    _fields_ = [('Data', ctypes.c_uint, 32)]

class ECA_LIMT_HIGH(HvdpsRegister):
    BAR = 1
    ADDR = 0x82c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class ECA_LIMIT_LOW(HvdpsRegister):
    BAR = 1
    ADDR = 0x830
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HvUhcSampleHeaderRegionBase(HvdpsRegister):
    BAR = 1
    ADDR = 0x880
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('HeaderRegionBase', c_uint, 32)]


class HvUhcSampleHeaderRegionSize(HvdpsRegister):
    BAR = 1
    ADDR = 0x884
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('HeaderRegionSize', c_uint, 32)]


class HvUhcSampleRegionBase(HvdpsRegister):
    BAR = 1
    ADDR = 0x888
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('SampleRegionBase', c_uint, 32)]


class HvUhcSampleRegionSize(HvdpsRegister):
    BAR = 1
    ADDR = 0x88C
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('SampleRegionSize', c_uint, 32)]


class VLC_UHC0_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x900
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC0_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x904
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC0_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x908
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC0_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x90c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC1_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x910
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC1_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x914
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC1_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x918
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC1_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x91c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC2_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x920
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC2_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x924
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC2_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x928
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC2_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x92c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC3_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x930
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC3_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x934
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC3_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x938
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC3_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x93c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC4_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x940
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC4_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x944
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC4_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x948
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC4_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x94c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC5_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x950
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC5_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x954
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC5_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x958
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC5_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x95c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC6_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x960
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC6_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x964
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC6_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x968
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC6_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x96c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC7_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x970
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC7_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x974
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC7_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x978
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_UHC7_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x97c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MAX6627_VICOR_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x980
    _fields_ = [('Vicor0LowTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor1LowTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor2LowTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor3LowTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor0HighTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor1HighTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor2HighTemperatureAlarm', ctypes.c_uint, 1),
                ('Vicor3HighTemperatureAlarm', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 24)]


class MAX6627_THERMAL_ADC_UCL(HvdpsRegister):
    BAR = 1
    ADDR = 0x984
    _fields_ = [('ThermalUpperLimit', ctypes.c_uint, 13),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_BJT_SENSOR_UCL(HvdpsRegister):
    BAR = 1
    ADDR = 0x988
    _fields_ = [('BJTUpperLimit', ctypes.c_uint, 13),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_VICOR_UCL(HvdpsRegister):
    BAR = 1
    ADDR = 0x98c
    _fields_ = [('VicorUpperLimit', ctypes.c_uint, 13),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_THERMAL_ADC_LCL(HvdpsRegister):
    BAR = 1
    ADDR = 0x990
    _fields_ = [('ThermalLowerLimit', ctypes.c_uint, 13),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_BJT_SENSOR_LCL(HvdpsRegister):
    BAR = 1
    ADDR = 0x994
    _fields_ = [('BJTLowerLimit', ctypes.c_uint, 13),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_VICOR_LCL(HvdpsRegister):
    BAR = 1
    ADDR = 0x998
    _fields_ = [('VicorLowerLimit', ctypes.c_uint, 13),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_TEMPERATURE(HvdpsRegister):
    BAR = 1
    ADDR = 0x99c
    THERMALADC_DEVICE_ID0 = 0x0
    THERMALADC_DEVICE_ID1 = 0x1
    THERMALADC_DEVICE_ID2 = 0x2
    THERMALADC_DEVICE_ID3 = 0x3
    THERMALADC_DEVICE_ID4 = 0x4
    THERMALADC_DEVICE_ID5 = 0x5
    THERMALADC_DEVICE_ID6 = 0x6
    THERMALADC_DEVICE_ID7 = 0x7
    BJTSENSOR_DEVICE_ID0 = 0x8
    BJTSENSOR_DEVICE_ID1 = 0x9
    BJTSENSOR_DEVICE_ID2 = 0xA
    BJTSENSOR_DEVICE_ID3 = 0xB
    BJTSENSOR_DEVICE_ID4 = 0xC
    BJTSENSOR_DEVICE_ID5 = 0xD
    BJTSENSOR_DEVICE_ID6 = 0xE
    BJTSENSOR_DEVICE_ID7 = 0xF
    VICOR_DEVICE_ID0 = 0x10
    VICOR_DEVICE_ID1 = 0x11
    VICOR_DEVICE_ID2 = 0x12
    VICOR_DEVICE_ID3 = 0x13

    _fields_ = [('Temperature',ctypes.c_uint, 13),
                ('Unused1',ctypes.c_uint,1),
                ('Unused2', ctypes.c_uint, 1),
                ('Unused3', ctypes.c_uint, 1),
                ('MAX6627DeviceSelectField',ctypes.c_uint,5),
                ('Unused4', ctypes.c_uint,1),
                ('Unused5', ctypes.c_uint, 1),
                ('Unused6', ctypes.c_uint, 1),
                ('Unused7', ctypes.c_uint, 1),
                ('Unused8', ctypes.c_uint, 1),
                ('Unused9', ctypes.c_uint, 1),
                ('Unused10', ctypes.c_uint, 1),
                ('Unused11', ctypes.c_uint, 1),
                ('Unused12', ctypes.c_uint, 1),
                ('Unused13', ctypes.c_uint, 1),
                ('Unused14', ctypes.c_uint, 1)
                ]


class LTC2450_ADC_VOLTAGE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9a0
    _fields_ = [('AdcVoltage', ctypes.c_uint, 16),
                ('Reserved',ctypes.c_uint, 15),
                ('AdcPollingEnable',ctypes.c_uint, 1)]


class HVDPS_GPIO(HvdpsRegister):
    BAR = 1
    ADDR = 0x9a4
    _fields_ = [('Data', ctypes.c_uint, 32)]



class OV_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0x9b0
    _fields_ = [('OvHclcRail0_1', ctypes.c_uint, 1),
                ('OvHclcRail2_3', ctypes.c_uint, 1),
                ('OvHclcRail4_5', ctypes.c_uint, 1),
                ('OvHclcRail6_7', ctypes.c_uint, 1),
                ('OvDutRail0_1', ctypes.c_uint, 1),
                ('OvDutRail2_3', ctypes.c_uint, 1),
                ('OvDutRail4_5', ctypes.c_uint, 1),
                ('OvDutRail6_7', ctypes.c_uint, 1),
                ('OvDutRail8_9', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 23)]



class AD5560_TURBO_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9c0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9c4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9c8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9cc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_HEADER_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9d0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_HEADER_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9d4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_REGION_BASE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9d8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_REGION_SIZE(HvdpsRegister):
    BAR = 1
    ADDR = 0x9dc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HV_V_ADC_SOFTSPAN_CODE(HvdpsRegister):
    BAR = 1
    ADDR = 0xa00
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HV_I_ADC_SOFTSPAN_CODE(HvdpsRegister):
    BAR = 1
    ADDR = 0xa04
    _fields_ = [('Data', ctypes.c_uint, 32)]


class LV_V_ADC_SOFTSPAN_CODE(HvdpsRegister):
    BAR = 1
    ADDR = 0xa08
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_00_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa30
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_01_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa34
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_02_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa38
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_03_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa3c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_04_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa40
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_05_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa44
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_06_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa48
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_07_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa4c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_08_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa50
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_09_VOLTAGE_LIMIT_SS3(HvdpsRegister):
    BAR = 1
    ADDR = 0xa54
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_VOLTAGE_LIMIT_SS7(HvdpsRegister):
    BAR = 1
    BASEMUL = 4
    ADDR = 0xa58
    REGCOUNT = 10
    _fields_ = [('OverVoltageLimit', ctypes.c_uint, 16),
                ('UnderVoltageLimit', ctypes.c_uint, 16)]


class DUT_rail_v(HvdpsRegister):
    BAR = 1
    BASEMUL = 4
    ADDR = 0xa80
    REGCOUNT = 10
    _fields_ = [('SoftSpanCode', ctypes.c_uint,3),
                ('ChannelId', ctypes.c_uint, 3),
                ('VoltageValue', ctypes.c_uint, 18),
                ('Data', ctypes.c_uint, 8)]

class DUT_turbo_v(HvdpsRegister):
    BAR = 1
    ADDR = 0xaa8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DPS_turbo_v(HvdpsRegister):
    BAR = 1
    ADDR = 0xaac
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DPS_turbo_i(HvdpsRegister):
    BAR = 1
    ADDR = 0xab0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class LTM_MV_rail_v(HvdpsRegister):
    BAR = 1
    ADDR = 0xac0
    REGCOUNT = 8
    _fields_ = [('SoftSpanCode', ctypes.c_uint,3),
                ('Channel Id', ctypes.c_uint,3),
                ('Data', ctypes.c_uint, 26)]


class DutUhcSampleHeaderRegionBase(HvdpsRegister):
    BAR = 1
    ADDR = 0xb00
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('HeaderRegionBase', c_uint, 32)]


class DutUhcSampleHeaderRegionSize(HvdpsRegister):
    BAR = 1
    ADDR = 0xb04
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('HeaderRegionSize', c_uint, 32)]


class DutUhcSampleRegionBase(HvdpsRegister):
    BAR = 1
    ADDR = 0xb08
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('SampleRegionBase', c_uint, 32)]


class DutUhcSampleRegionSize(HvdpsRegister):
    BAR = 1
    ADDR = 0xb0c
    BASEMUL = 16
    REGCOUNT = 8
    _fields_ = [('SampleRegionSize', c_uint, 32)]


class DUT_ALARMS(HvdpsRegister):
    BAR = 1
    ADDR = 0xb80
    _fields_ = [('DutRail00Alarm', c_uint, 1),
                ('DutRail01Alarm', c_uint, 1),
                ('DutRail02Alarm', c_uint, 1),
                ('DutRail03Alarm', c_uint, 1),
                ('DutRail04Alarm', c_uint, 1),
                ('DutRail05Alarm', c_uint, 1),
                ('DutRail06Alarm', c_uint, 1),
                ('DutRail07Alarm', c_uint, 1),
                ('DutRail08Alarm', c_uint, 1),
                ('DutRail09Alarm', c_uint, 1),
                ('Reserved', c_uint, 22)]


class DUT_RAIL_ALARM(HvdpsRegister):
    BAR = 1
    ADDR = 0xb84
    REGCOUNT=10
    _fields_ = [('UserUvAlarm', ctypes.c_uint, 1),
                ('UserOvAlarm', ctypes.c_uint, 1),
                ('DutRailBusyAlarm', ctypes.c_uint, 1),
                ('HwUvAlarm', ctypes.c_uint, 1),
                ('HwOvAlarm', ctypes.c_uint, 1),
                ('Reserved', c_uint, 27)]



class ADC_ENABLE(HvdpsRegister):
    BAR = 1
    ADDR = 0xbac
    _fields_ = [('Data', ctypes.c_uint, 32)]

class MAX6627_ALARM_FILTER_COUNT(HvdpsRegister):
    BAR = 1
    ADDR = 0xbb4
    _fields_ = [('FilterCount', c_uint, 32)]

class OV_UV_ALARM_FILTER_COUNT(HvdpsRegister):
    BAR = 1
    ADDR = 0xbb8
    _fields_ = [('FilterCount', c_uint, 32)]

class HV_SWITCH_EN_STATUS(HvdpsRegister):
    BAR = 1
    ADDR = 0xbbc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class BAR2_SCRATCH(HvdpsRegister):
    BAR = 2
    ADDR = 0x0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_SAMPLE_ENGINE_RESET(HvdpsRegister):
    BAR = 2
    ADDR = 0x10
    _fields_ = [('Dut0Reset', c_uint, 1),
                ('Dut1Reset', c_uint, 1),
                ('Dut2Reset', c_uint, 1),
                ('Dut3Reset', c_uint, 1),
                ('Dut4Reset', c_uint, 1),
                ('Dut5Reset', c_uint, 1),
                ('Dut6Reset', c_uint, 1),
                ('Dut7Reset', c_uint, 1),
                ('Data', c_uint, 24)]


class HCLC_SAMPLING_ACTIVE(HvdpsRegister):
    BAR = 2
    ADDR = 0x14
    _fields_ = [('ActiveDut', c_uint, 8),
                ('Data', c_uint, 24)]


class AD5560_TURBO_SAMPLE_ENGINE_RESET(HvdpsRegister):
    BAR = 2
    ADDR = 0x18
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLING_ACTIVE(HvdpsRegister):
    BAR = 2
    ADDR = 0x1c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_SAMPLE_ENGINE_RESET(HvdpsRegister):
    BAR = 2
    ADDR = 0x20
    _fields_ = [('ResetDut', c_uint, 8),
                ('Data', c_uint, 24)]


class VLC_SAMPLING_ACTIVE(HvdpsRegister):
    BAR = 2
    ADDR = 0x24
    _fields_ = [('ActiveDut', c_uint, 8),
                ('Data', c_uint, 24)]

class DUT_SAMPLE_ENGINE_RESET(HvdpsRegister):
    BAR = 2
    ADDR = 0x28
    _fields_ = [('Dut0Reset', c_uint, 1),
                ('Dut1Reset', c_uint, 1),
                ('Dut2Reset', c_uint, 1),
                ('Dut3Reset', c_uint, 1),
                ('Dut4Reset', c_uint, 1),
                ('Dut5Reset', c_uint, 1),
                ('Dut6Reset', c_uint, 1),
                ('Dut7Reset', c_uint, 1),
                ('Data', c_uint, 24)]

class DUT_SAMPLING_ACTIVE(HvdpsRegister):
    BAR = 2
    ADDR = 0x2c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_ENGINE_RESET(HvdpsRegister):
    BAR = 2
    ADDR = 0x30
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLING_ACTIVE(HvdpsRegister):
    BAR = 2
    ADDR = 0x34
    _fields_ = [('Data', ctypes.c_uint, 32)]

class CAPABILITIES(HvdpsRegister):
    BAR = 2
    ADDR = 0x7c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_0_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x80
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_0_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x84
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_0_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x88
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_0_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x8c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_0_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x90
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_0_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x94
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_0_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x98
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_0_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x9c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_1_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0xa0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_1_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0xa4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_1_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0xa8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_1_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0xac
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_1_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0xb0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_1_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0xb4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_1_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0xb8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_1_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0xbc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_2_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0xc0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_2_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0xc4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_2_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0xc8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_2_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0xcc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_2_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0xd0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_2_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0xd4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_2_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0xd8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_2_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0xdc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_3_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0xe0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_3_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0xe4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_3_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0xe8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_3_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0xec
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_3_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0xf0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_3_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0xf4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_3_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0xf8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_3_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0xfc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_4_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x100
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_4_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x104
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_4_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x108
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_4_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x10c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_4_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x110
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_4_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x114
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_4_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x118
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_4_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x11c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_5_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x120
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_5_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x124
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_5_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x128
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_5_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x12c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_5_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x130
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_5_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x134
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_5_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x138
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_5_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x13c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_6_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x140
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_6_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x144
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_6_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x148
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_6_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x14c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_6_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x150
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_6_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x154
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_6_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x158
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_6_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x15c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_7_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x160
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_7_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x164
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_7_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x168
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_7_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x16c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_7_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x170
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_7_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x174
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HCLC_7_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x178
    _fields_ = [('Data', ctypes.c_uint, 32)]

class HCLC_7_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x17c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_0_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x180
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_0_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x184
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_0_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x188
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_0_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x18c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_0_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x190
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_0_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x194
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_0_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x198
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_0_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x19c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_1_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x1a0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_1_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x1a4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_1_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x1a8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_1_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x1ac
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_1_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x1b0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_1_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x1b4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_1_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x1b8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_1_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x1bc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_2_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x1c0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_2_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x1c4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_2_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x1c8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_2_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x1cc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_2_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x1d0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_2_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x1d4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_2_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x1d8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_2_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x1dc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_3_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x1e0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_3_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x1e4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_3_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x1e8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_3_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x1ec
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_3_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x1f0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_3_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x1f4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_3_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x1f8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_3_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x1fc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_4_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x200
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_4_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x204
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_4_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x208
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_4_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x20c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_4_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x210
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_4_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x214
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_4_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x218
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_4_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x21c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_5_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x220
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_5_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x224
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_5_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x228
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_5_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x22c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_5_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x230
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_5_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x234
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_5_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x238
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_5_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x23c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_6_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x240
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_6_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x244
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_6_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x248
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_6_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x24c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_6_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x250
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_6_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x254
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_6_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x258
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_6_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x25c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_7_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x260
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_7_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x264
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_7_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x268
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_7_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x26c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_7_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x270
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_7_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x274
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VLC_7_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x278
    _fields_ = [('Data', ctypes.c_uint, 32)]

class VLC_7_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x27c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_0_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x280
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_0_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x284
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_0_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x288
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_0_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x28c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_0_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x290
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_0_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x294
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_0_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x298
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_0_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x29c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_1_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x2a0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_1_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x2a4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_1_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x2a8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_1_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x2ac
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_1_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x2b0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_1_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x2b4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_1_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x2b8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_1_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x2bc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_2_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x2c0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_2_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x2c4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_2_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x2c8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_2_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x2cc
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_2_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x2d0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_2_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x2d4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_2_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x2d8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_2_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x2dc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_3_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x2e0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_3_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x2e4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_3_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x2e8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_3_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x2ec
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_3_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x2f0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_3_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x2f4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_3_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x2f8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_3_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x2fc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_4_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x300
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_4_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x304
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_4_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x308
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_4_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x30c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_4_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x310
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_4_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x314
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_4_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x318
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_4_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x31c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_5_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x320
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_5_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x324
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_5_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x328
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_5_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x32c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_5_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x330
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_5_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x334
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_5_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x338
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_5_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x33c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_6_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x340
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_6_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x344
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_6_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x348
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_6_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x34c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_6_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x350
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_6_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x354
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_6_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x358
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_6_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x35c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_7_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x360
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_7_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x364
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_7_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x368
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_7_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x36c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_7_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x370
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_7_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x374
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_7_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x378
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_7_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x37c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5560_TURBO_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x380
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x384
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x388
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x38c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x390
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x394
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD5560_TURBO_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x398
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5560_TURBO_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x39c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_TURBO_SAMPLE_COUNT(HvdpsRegister):
    BAR = 2
    ADDR = 0x3a0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_RATE(HvdpsRegister):
    BAR = 2
    ADDR = 0x3a4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_METADATA_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x3a8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_METADATA_LO(HvdpsRegister):
    BAR = 2
    ADDR = 0x3ac
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_RAIL_SELECT(HvdpsRegister):
    BAR = 2
    ADDR = 0x3b0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_DELAY(HvdpsRegister):
    BAR = 2
    ADDR = 0x3b4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_TURBO_SAMPLE_START(HvdpsRegister):
    BAR = 2
    ADDR = 0x3b8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class DUT_TURBO_SAMPLE_SSHOT_COUNT_HI(HvdpsRegister):
    BAR = 2
    ADDR = 0x3bc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class Bar2RailCommand(HvdpsStruct):
    _fields_ = [('RailNumberOrResourceId', c_uint, 8),
                ('RailCommand', c_uint, 7)]


class SampleHeaderStruct(HvdpsStruct):
    _fields_ = [('SamplePointer', c_uint, 32),
                ('SampleRate', c_uint, 16),
                ('SampleCount', c_uint, 16),
                ('RailVISelect', c_uint, 32),
                ('MetaLo', c_uint, 16),
                ('MetaHi', c_uint, 16)]


class LTC2630CommandCodeStruct(HvdpsStruct):
    _fields_ = [('Data', c_uint, 10),
                ('C0', c_uint, 1),
                ('C1', c_uint, 1),
                ('C2', c_uint, 1),
                ('C3', c_uint, 1)]