################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from ctypes import c_uint

import Common.register as register


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, max6662Register):
                if attribute is not max6662Register:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, max6662Register):
                if attribute is not max6662Register:
                    # if '__' not in attribute.__name__:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, max6662Struct):
                if attribute is not max6662Struct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, max6662Struct):
                if attribute is not max6662Struct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class max6662Register(register.Register):
    BAR = 1

    def __init__(self, uint=0, length=16):
        super().__init__(value=uint)


class max6662Struct(register.Register):
    ADDR = None

    def __init__(self, uint=0, length=16):
        super().__init__(value=uint)


module_dict = globals()


class WRITECONFIGURATION(max6662Register):
    ADDR = 0x83
    _fields_ = [('Data', c_uint, 16)]


class WRITETHYST(max6662Register):
    ADDR = 0x85
    _fields_ = [('Data', c_uint, 16)]


class WRITETMAX(max6662Register):
    ADDR = 0x87
    _fields_ = [('Data', c_uint, 16)]


class WRITETLOW(max6662Register):
    ADDR = 0x89
    _fields_ = [('Data', c_uint, 16)]


class WRITETHIGH(max6662Register):
    ADDR = 0x8B
    _fields_ = [('Data', c_uint, 16)]


class READTEMPERATURE(max6662Register):
    ADDR = 0xC1
    _fields_ = [('Data', c_uint, 16)]


class READCONFIGURATION(max6662Register):
    ADDR = 0xC3
    _fields_ = [('Data', c_uint, 16)]


class READTHYST(max6662Register):
    ADDR = 0xC5
    _fields_ = [('Data', c_uint, 16)]


class READTMAX(max6662Register):
    ADDR = 0xC7
    _fields_ = [('Data', c_uint, 16)]


class READTLOW(max6662Register):
    ADDR = 0xC9
    _fields_ = [('Data', c_uint, 16)]


class READTHIGH(max6662Register):
    ADDR = 0xCB
    _fields_ = [('Data', c_uint, 16)]


class Max6662Command(max6662Struct):
    _fields_ = [('ReadWrite', c_uint, 1),
                ('DeviceCode', c_uint, 7),
                ('RegisterAddress', c_uint, 8), ]
