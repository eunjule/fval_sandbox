################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: hddpsSubslot.py
# -------------------------------------------------------------------------------
#     Purpose: Class for one HDDPS card
# -------------------------------------------------------------------------------
#  Created by: Renuka Agrawal
#        Date: 10/30/15
#       Group: HDMT FPGA Validation
################################################################################


import io
import operator
import re
import struct
import time

from _build.bin import hddpstbc
from numbers import Number

from Common import hilmon as hil
from Common.instruments.dps import symbols
from Common.instruments.dps.dpsSubslot import DpsSubslot
from Common.instruments.dps.hddps_cal_board import hddps_cal_board
from Common.instruments.dps import ad5560_registers
from Common import configs
from ThirdParty.SASS.suppress_sensor import suppress_sensor

DPS_NUM_AD5560_RAILS = 10
DPS_NUM_ISL55180_RAILS = 16
FPGA_BAR2_DEVICE_ID = 0x000
AD5560_OFFSET_DAC_CODE = 0x58C0
AD550_V_REF = 2.5
AD5560_DAC_SCALE = 0x10000
AD5560_V_CALC_SCALER = 5.125
AD5560_MI_AMP_GAIN = 10.0


class HddpsSubslot(DpsSubslot):
    TOTAL_MEMORY_SIZE = 512 * 1024 * 1024
    hddps_mb_dac_mapping = {0: (0, 2), 1: (0, 3), 2: (1, 2), 3: (1, 3), 4: (2, 1), 5: (2, 0), 6: (1, 0), 7: (1, 1),
                            8: (0, 0), 9: (0, 1)}
    hddps_db_dac_mapping = {0: (0, 0), 1: (0, 1), 2: (0, 2), 3: (0, 3), 4: (2, 0), 5: (1, 3), 6: (1, 2),
                            7: (2, 3), 8: (2, 2), 9: (2, 1)}

    def __init__(self, hddps, slot, subslot, rc):
        super().__init__(slot, subslot)

        self.registers = hddps.registers
        self.ad5560regs = hddps.ad5560regs
        self.max6662regs = hddps.max6662regs
        self.isl55180regs = hddps.isl55180regs
        self.HCLC_RAIL_BASE = 16
        self.hddps = hddps
        self.rc = rc
        self.numberofrails = 10
        self.numberofvlcrails = 16
        self.TOTAL_MEMORY_SIZE = 512 * 1024 * 1024
        self.AD5560_MI_AMP_GAIN = 10.0

    def name(self):
        return f'{self.__class__.__name__}_{self.slot}_{self.subslot}'

    def S2HSupport(self):
        if self.GetFpgaVersion() in configs.HDDPS_NON_S2H_FPGAS:
            return False
        else:
            return True

    def FpgaLoad(self, image):
        result = False
        with suppress_sensor(f'FVAL HDDPS{self.slot}', f'HDMT_HDDPS:{self.slot}'):
            with HddpsSubslot.FpgaPcieDriverDisable(self.slot, self.subslot):
                result = hil.dpsFpgaLoad(self.slot, self.subslot, image)
            time.sleep(1)
        return result

    class FpgaPcieDriverDisable():
        def __init__(self, slot, subslot):
            self.slot = slot
            self.subslot = subslot

        def __enter__(self):
            hil.dpsDeviceDisable(self.slot, self.subslot)
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            hil.dpsDeviceEnable(self.slot, self.subslot)

    def BarRead(self, bar, offset, dev_id=FPGA_BAR2_DEVICE_ID):
        return self.DpsBarRead(bar,offset,dev_id)

    def BarWrite(self, bar, offset, data, dev_id=FPGA_BAR2_DEVICE_ID):
        return self.DpsBarWrite(bar, offset, data, dev_id)

    def Read(self, registerName):
        register = getattr(self.registers, registerName)
        return register(value=self.BarRead(register.BAR, register.ADDR))

    def ReadObject(self, registerName):
        register = getattr(self.registers, registerName)
        return register(self.BarRead(register.BAR, register.ADDR))

    def Write(self, registerName, data):
        register = getattr(self.registers, registerName)
        if isinstance(data, self.registers.HddpsRegister):
            return self.BarWrite(register.BAR, register.ADDR, data.Pack())
        else:
            return self.BarWrite(register.BAR, register.ADDR, data)

    def WriteAd5560Register(self, register_address, data, rail):
        hil.hddpsAd5560Write(self.slot,self.subslot,rail, register_address, data)

    def ReadAd5560Register(self, register_address, rail):
        data = hil.hddpsAd5560Read(self.slot,self.subslot,rail, register_address)
        return data

    def TurnONStreamingWithTrigger(self):
        s2h_start_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_START)
        self.hddps.rc.send_trigger(s2h_start_trigger_value)

    def TurnOFFStreamingWithTrigger(self):
        s2h_stop_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_STOP)
        self.hddps.rc.send_trigger(s2h_stop_trigger_value)

    def ResetStreamingWithTrigger(self):
        s2h_reset_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_RESET)
        self.hddps.rc.send_trigger(s2h_reset_trigger_value)

    # For the HDDPS daughter board we only need to check DDR3 status.
    # We do NOT need to check the aurora status since the aurora bus is on the HDDPS mother board
    # and routed to the HDDPS daughter board through SPI
    def AuroraIsReady(self):
        for i in range(10):
            result = True
            status = self.ReadObject('STATUS')
            if (
                                        status.AuroraPLLLock != 0 or status.AuroraChannelUp != 0 or status.AuroraLaneUp != 0 or status.AuroraHardError == 1 or status.AuroraSoftError == 1):
                self.Log('info', 'AURORA bus is NOT ready for use...')
                result = False
        return result

    def SetMasterRail(self, HwRail):
        rail_master_mask = self.Read('RailMasterMask')
        rail = self.HwRailtoPerDeviceRail(HwRail)
        data = (1 << rail) | rail_master_mask.Pack()
        rail_master_mask.HcRailIndicator = data
        self.Write('RailMasterMask', rail_master_mask)
        self.Log('debug', 'The register RailMasterMask is set to 0x{:x}'.format(rail_master_mask.Pack()))

    def SetSlaveRail(self, HwRail):
        rail = self.HwRailtoPerDeviceRail(HwRail)
        rail_master_mask = self.Read('RailMasterMask')
        data = (0 << rail) | rail_master_mask.Pack()
        rail_master_mask.HcRailIndicator = data
        self.Write('RailMasterMask', rail_master_mask)
        self.Log('debug', 'The register RailMasterMask is set to 0x{:x}'.format(rail_master_mask.Pack()))

    def SetMasterSlaveRails(self, masterSlaveRailData):
        rail_master_mask = self.Read('RailMasterMask')
        data = masterSlaveRailData
        rail_master_mask.HcRailIndicator = data
        self.Write('RailMasterMask', rail_master_mask)
        self.Log('info', 'The register RailMasterMask is set to 0x{:x}'.format(rail_master_mask.Pack()))

    def UnGangAllRails(self):
        self.Log('debug', 'Unganging all the rails')
        self.WriteRegister(self.registers.RailMasterMask(value=0x3FF))
        self.WriteRegister(self.registers.SlaveBoardMask(value=0x000))
        self.WriteRegister(self.registers.RemoteMasterMask(value=0x000))
        self.WriteRegister(self.registers.HcIGang(value=0x7FF))
        default_inv_slave_cnt_value = 0x8000
        railcnt = 10
        for rail in range(railcnt):
            self.WriteRegister(self.registers.RAIL_INV_SLAVE_CNT(value=default_inv_slave_cnt_value), index=rail)

    def InitializeIsl55180Rails(self):
        if self.subslot == 1:
            return
        self.Log('debug', 'Initializing the ISL55180 Rails')

        # Rail = 0-7 will activate actions for ISL555180 Device 0
        # Rail = 8-15 will activate actions for ISL555180 Device 1
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'CPU_RESET_WO', 1)
        self.Log('debug', 'The CPU_RESET_WO is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'CPU_RESET_WO')))
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'CPU_RESET_WO', 1)
        self.Log('debug', 'The CPU_RESET_WO is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'CPU_RESET_WO')))

        # Set and enable voltage monitor
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'MEAS_CONT_GANG', 0x350)
        self.Log('debug', 'The MEAS_CONT_GANG is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'MEAS_CONT_GANG')))
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'MEAS_CONT_GANG', 0x350)
        self.Log('debug', 'The MEAS_CONT_GANG is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'MEAS_CONT_GANG')))

        # set enable current monitor
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'MEAS_IMON_CENT_SEL', 0x50)
        self.Log('debug', 'The MEAS_IMON_CENT_SEL is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'MEAS_IMON_CENT_SEL')))
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'MEAS_IMON_CENT_SEL', 0x50)
        self.Log('debug', 'The MEAS_IMON_CENT_SEL is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'MEAS_IMON_CENT_SEL')))

        for rail in range(DPS_NUM_ISL55180_RAILS):
            deviceid = symbols.ISL55180.ISL55180_0 + rail * 2
            deviceid_reg = symbols.ISL55180.ISL55180_0_REG + rail * 2
            # TODO The following writes to ForceA_Offset/Gain and I_CLAMP_H/L_Offset/Gain are applying default cal values
            # for offset and gain. Once cal is in place, we should probably be loading the actual cal values
            # for offset and gain here

            self.WriteIsl55180Register(deviceid, 'FORCEA_LEVEL', 0x3FFF)
            self.Log('debug',
                     'The FORCEA_LEVEL is set to 0x{:x}'.format(self.ReadIsl55180Register(deviceid, 'FORCEA_LEVEL')))
            self.WriteIsl55180Register(deviceid, 'FORCEB_LEVEL', 0x0000)
            self.Log('debug',
                     'The FORCEB_LEVEL is set to 0x{:x}'.format(self.ReadIsl55180Register(deviceid, 'FORCEB_LEVEL')))
            self.WriteIsl55180Register(deviceid, 'I_CLAMP_H_LEVEL', 0xA1D9)
            self.Log('debug', 'The I_CLAMP_H_LEVEL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid, 'I_CLAMP_H_LEVEL')))
            self.WriteIsl55180Register(deviceid, 'I_CLAMP_L_LEVEL', 0x6A00)
            self.Log('debug', 'The I_CLAMP_L_LEVEL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid, 'I_CLAMP_L_LEVEL')))
            self.WriteIsl55180Register(deviceid, 'FORCEA_OFFSET', 0x7FFF)
            self.Log('debug',
                     'The FORCEA_OFFSET is set to 0x{:x}'.format(self.ReadIsl55180Register(deviceid, 'FORCEA_OFFSET')))
            self.WriteIsl55180Register(deviceid, 'FORCEB_OFFSET', 0x0000)
            self.Log('debug',
                     'The FORCEB_OFFSET is set to 0x{:x}'.format(self.ReadIsl55180Register(deviceid, 'FORCEB_OFFSET')))

            self.WriteIsl55180Register(deviceid, 'FORCEA_GAIN', 0x7FFF)
            self.Log('debug',
                     'The FORCEA_GAIN is set to 0x{:x}'.format(self.ReadIsl55180Register(deviceid, 'FORCEA_GAIN')))
            self.WriteIsl55180Register(deviceid, 'FORCEB_GAIN', 0x0000)
            self.Log('debug',
                     'The FORCEB_GAIN is set to 0x{:x}'.format(self.ReadIsl55180Register(deviceid, 'FORCEB_GAIN')))

            self.WriteIsl55180Register(deviceid, 'I_CLAMP_H_OFFSET', 0x7FFF)
            self.Log('debug', 'The I_CLAMP_H_OFFSET is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid, 'I_CLAMP_H_OFFSET')))

            self.WriteIsl55180Register(deviceid, 'I_CLAMP_L_OFFSET', 0x7FFF)
            self.Log('debug', 'The I_CLAMP_L_OFFSET is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid, 'I_CLAMP_L_OFFSET')))

            self.WriteIsl55180Register(deviceid, 'I_CLAMP_H_GAIN', 0x7FFF)
            self.Log('debug', 'The I_CLAMP_L_GAIN is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid, 'I_CLAMP_H_GAIN')))

            self.WriteIsl55180Register(deviceid, 'I_CLAMP_L_GAIN', 0x7FFF)
            self.Log('debug', 'The I_CLAMP_L_GAIN is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid, 'I_CLAMP_L_GAIN')))

            self.WriteIsl55180Register(deviceid_reg, 'DPS_CONT_MISC', 0x8028)
            self.Log('debug', 'The DPS_CONT_MISC is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'DPS_CONT_MISC')))
            self.WriteIsl55180Register(deviceid_reg, 'DPS_CONT_MISC', 0x8028)
            self.Log('debug', 'The DPS_CONT_MISC is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'DPS_CONT_MISC')))

            # Configures rail to max IRange (256mA), selects chip ground and MV mode
            self.WriteIsl55180Register(deviceid_reg, 'MEAS_UNIT_SRC_SEL', 0x1826)
            self.Log('debug', 'The MEAS_UNIT_SRC_SEL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'MEAS_UNIT_SRC_SEL')))
            self.WriteIsl55180Register(deviceid_reg, 'MEAS_UNIT_SRC_SEL', 0x1826)
            self.Log('debug', 'The MEAS_UNIT_SRC_SEL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'MEAS_UNIT_SRC_SEL')))

            # 8V Range, FV mode, enable tight loop and remote sense
            self.WriteIsl55180Register(deviceid_reg, 'SOURCE_FB_SEL', 0x0542)
            self.Log('debug', 'The SOURCE_FB_SEL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'SOURCE_FB_SEL')))
            self.WriteIsl55180Register(deviceid_reg, 'SOURCE_FB_SEL', 0x0542)
            self.Log('debug', 'The SOURCE_FB_SEL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'SOURCE_FB_SEL')))

            # Set up the channel level alarm control register
            # Disables kelvin and clamp alarms, selects user programmable clamp control

            # Will initialize register to have kelvin and clamp alarms disabled and selects user programmable clamp control

            self.WriteIsl55180Register(deviceid_reg, 'CLAMP_ALARM_CONT', 0xA2A)
            self.Log('debug', 'The CLAMP_ALARM_CONT is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'CLAMP_ALARM_CONT')))
            self.WriteIsl55180Register(deviceid_reg, 'CLAMP_ALARM_CONT', 0x7000)
            self.Log('debug', 'The CLAMP_ALARM_CONT is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'CLAMP_ALARM_CONT')))

            # SET KELVIN THRESHOLD TO 525MV
            self.WriteIsl55180Register(deviceid_reg, 'DIAG_AND_CAL', 0x9000)
            self.Log('debug', 'The DIAG_AND_CAL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'DIAG_AND_CAL')))
            self.WriteIsl55180Register(deviceid_reg, 'DIAG_AND_CAL', 0x9000)
            self.Log('debug', 'The DIAG_AND_CAL is set to 0x{:x}'.format(
                self.ReadIsl55180Register(deviceid_reg, 'DIAG_AND_CAL')))


        # Selects no compensation cap, high current mode and disables the rail


        # Enable chip level alarms for the intersils. Alarm_Control register is global to all rails on a single intersil part
        # so only need to write the value to one rail on each intersil.
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'ALARM_CONTROL', 0x40)
        self.Log('debug', 'The ALARM_CONTROL is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_0_REG, 'ALARM_CONTROL')))
        self.WriteIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'ALARM_CONTROL', 0x40)
        self.Log('debug', 'The ALARM_CONTROL is set to 0x{:x}'.format(
            self.ReadIsl55180Register(symbols.ISL55180.ISL55180_8_REG, 'ALARM_CONTROL')))

    def ResetThermalAlarmLimits(self):
        self.Log('debug', 'Reseting the thermal alarm limits')
        ucl = 64  # Don't let system get hotter than this.  Wide limit (mirrors TIER2 condition in HAL)
        lcl = 10  # Don't let system get cooler than this.  Wide limit (mirrors TIER2 condition in HAL)
        hyst = 5  # Hysteresis value same as HAL init

        # Convert temperature limit into register equivalent value
        writetmax = self.max6662regs.WRITETMAX.ADDR
        self.WriteMax6662Register(0, writetmax, ucl)
        self.WriteMax6662Register(1, writetmax, ucl)
        writethigh = self.max6662regs.WRITETHIGH.ADDR
        self.WriteMax6662Register(0, writethigh, ucl)
        self.WriteMax6662Register(1, writethigh, ucl)
        writetlow = self.max6662regs.WRITETLOW.ADDR
        self.WriteMax6662Register(0, writetlow, lcl)
        self.WriteMax6662Register(1, writetlow, lcl)
        writethyst = self.max6662regs.WRITETHYST.ADDR
        self.WriteMax6662Register(0, writethyst, hyst)
        self.WriteMax6662Register(1, writethyst, hyst)

    def CheckAlarms(self):
        result = dict()
        result["GLOBAL_ALARMS"] = self.ReadRegister(self.registers.GLOBAL_ALARMS).value
        result["HclcSampleAlarms"] = self.ReadRegister(self.registers.HclcSampleAlarms).value
        result["VlcSampleAlarms"] = self.ReadRegister(self.registers.VlcSampleAlarms).value
        result["HclcCfoldAlarms"] = self.ReadRegister(self.registers.HclcCfoldAlarms).value
        result["VlcCfoldAlarms"] = self.ReadRegister(self.registers.VlcCfoldAlarms).value

        dataVlcCfoldData = self.RetrieveMultipleAlarms('Vlc', 'CfoldData', 16)  # 16 refers to # of Vlc Cfold Alarms
        result["VlcCfoldData"] = dataVlcCfoldData
        dataHclcCfoldData = self.RetrieveMultipleAlarms('Hclc', 'CfoldData', 10)  # 10 refers to # of Hclc Cfold Alarms
        result["HclcCfoldData"] = dataHclcCfoldData

        result["HCLC_ALARMS"] = self.ReadRegister(self.registers.HCLC_ALARMS).value
        result["VlcRailAlarms"] = self.ReadRegister(self.registers.VlcRailAlarms).value

        dataVlcRailAlarmsData = self.RetrieveMultipleAlarms('Vlc', 'RailAlarms',
                                                            16)  # 16 refers to # of Vlc Cfold Alarms
        result["VlcRailAlarmsData"] = dataVlcRailAlarmsData

        dataHclcRailAlarmsData = self.RetrieveMultipleAlarms('Hclc', 'Alarms',
                                                             10)  # 10 refers to # of Hclc Cfold Alarms
        result["HclcRailAlarmsData"] = dataHclcRailAlarmsData

        return result

    def RetrieveMultipleAlarms(self, alarmName0, alarmName1, alarmNumber):
        multipleAlarms = list()
        for i in range(alarmNumber):
            alarmName = getattr(self.registers,
                                '{name0}{num:02d}{name1}'.format(name0=alarmName0, num=i, name1=alarmName1))
            multipleAlarms.append(self.ReadRegister(alarmName).value)
        return multipleAlarms

    def ReturnPatternIterator(self, masterSlaveConfig):
        # pattern that matches with 0s starting with 1
        pattern = re.compile('1[0]+')
        # Express config in 10 bits binary form
        binaryString = '{0:010b}'.format(masterSlaveConfig)[::-1]  # Reverse the binary string
        return pattern.finditer(binaryString)

    def SetInvSlaveCnt(self, masterSlaveConfig):
        # The list of Inv Slave Cnt
        # slaveCntList = ['Rail00InvSlaveCnt', 'Rail01InvSlaveCnt', 'Rail02InvSlaveCnt', 'Rail03InvSlaveCnt',
        #                 'Rail04InvSlaveCnt', 'Rail05InvSlaveCnt', 'Rail06InvSlaveCnt', 'Rail07InvSlaveCnt',
        #                 'Rail08InvSlaveCnt', 'Rail09InvSlaveCnt']
        iterator = self.ReturnPatternIterator(masterSlaveConfig)
        # Setting the 0x8000 for all rails as a default value




        for railNum in range(10):
            register = self.registers.RAIL_INV_SLAVE_CNT(value=0x8000)
            self.WriteRegister(register, index=railNum)
            # self.Write(slaveCntList[railNum], 0x8000)

        # Setting slave and master that has slaves to the slave Cnt
        for match in iterator:
            span = match.span()
            slaveCntDivider = abs(operator.sub(*span))
            slaveCnt = int(0x8000 / slaveCntDivider)
            for railNum in range(*span):
                register = self.registers.RAIL_INV_SLAVE_CNT(value=slaveCnt)
                self.WriteRegister(register, index=railNum)

                # self.Write(slaveCntList[railNum], slaveCnt)

    def ConfigureAD5560GangingRegister(self, masterSlaveConfig):
        Ad5560SlaveGangMaskBits = 0xF9FF
        Ad5560MasterConfigBits = 0x0200
        Ad5560EnableClampsBit = 0x10
        Ad5560DisableClampsMask = 0xFFE0
        Ad5560MaxSlewRateMask = 0x8FFF
        Ad5560SlaveConfigMask = 0x0600
        Ad5560NoAlarmsMask = 0x1540

        iterator = self.ReturnPatternIterator(masterSlaveConfig)
        DPS_REGISTER1 = self.ad5560regs.DPS_REGISTER1.ADDR
        DPS_REGISTER2 = self.ad5560regs.DPS_REGISTER2.ADDR
        ALARM_CLEAR = self.ad5560regs.ALARM_CLEAR.ADDR
        for match in iterator:
            span = match.span()
            # print('span is {}'.format( span ) )
            # Setting Master/slave on DPS_REGISTER
            for idx, railNum in enumerate(range(*span)):
                oldDpsRegister1 = self.ReadAd5560Register(DPS_REGISTER1, railNum)
                oldDpsRegister2 = self.ReadAd5560Register(DPS_REGISTER2, railNum)
                # Master configguration
                if idx == 0:
                    # Enable clamps on master
                    self.WriteAd5560Register(DPS_REGISTER1, oldDpsRegister1 | Ad5560EnableClampsBit, railNum)
                    # Master Out=MI for slaves in FI mode according to the data sheet.
                    self.WriteAd5560Register(DPS_REGISTER2,
                                             (oldDpsRegister2 & Ad5560SlaveGangMaskBits) | Ad5560MasterConfigBits,
                                             railNum)
                    # self.WriteAd5560Register(DPS_REGISTER2, 0x8280, railNum)
                    # self.Log('info', 'DPS Register 1 for Rail{} is {} '.format( railNum, str(self.ReadAd5560Register(DPS_REGISTER1, railNum) ) ) )
                    # self.Log('info', 'DPS Register 2 for Rail{} is {} '.format( railNum, str(self.ReadAd5560Register(DPS_REGISTER2, railNum) ) ) )
                    self.Log('info', 'DPS Register 1 for Master Rail{} is {:x} '.format(railNum,
                                                                                        self.ReadAd5560Register(
                                                                                            DPS_REGISTER1, railNum)))
                    self.Log('info', 'DPS Register 2 for Master Rail{} is {:x} '.format(railNum,
                                                                                        self.ReadAd5560Register(
                                                                                            DPS_REGISTER2, railNum)))
                # Slave configuration
                else:
                    # Disable clamps on master
                    self.WriteAd5560Register(DPS_REGISTER1, oldDpsRegister1 & Ad5560DisableClampsMask, railNum)
                    # Set slave FI mode and max slew rate:
                    self.WriteAd5560Register(DPS_REGISTER2,
                                             (oldDpsRegister2 | Ad5560SlaveConfigMask) & Ad5560MaxSlewRateMask, railNum)
                    # Disable kelvin alarms
                    self.WriteAd5560Register(ALARM_CLEAR, Ad5560NoAlarmsMask, railNum)
                    # self.Log('info', 'DPS Register 1 for Rail{} is {} '.format( railNum, str(self.ReadAd5560Register(DPS_REGISTER1, railNum) ) ) )
                    # self.Log('info', 'DPS Register 2 for Rail{} is {} '.format( railNum, str(self.ReadAd5560Register(DPS_REGISTER2, railNum) ) ) )
                    self.Log('info', 'DPS Register 1 for Slave Rail{} is {:x} '.format(railNum, self.ReadAd5560Register(
                        DPS_REGISTER1, railNum)))
                    self.Log('info', 'DPS Register 2 for Slave Rail{} is {:x} '.format(railNum, self.ReadAd5560Register(
                        DPS_REGISTER2, railNum)))
                    self.Log('info', 'ALARM_CLEAR for Rail{} is {} '.format(railNum, str(
                        self.ReadAd5560Register(ALARM_CLEAR, railNum))))

    def WriteMax6662Register(self, device, register_addr, temperature):
        encoded_temperature = (temperature & 0x1FF) << 7
        hil.hddpsMax6662Write(self.slot, self.subslot, device, register_addr, encoded_temperature)

    def WriteIsl55180Register(self, deviceid, registerName, data):
        #########################################################################################
        # [17] : 1 = W, 0 = R.  This must agree with PCIe read or write.
        # [16:10] : device id   ([14] selects which ISL55180 of the two, [10] maps directly to ISL55180 "R" bit.)
        #                         device id is one of ISL55180_0, ISL55180_0_REG, ISL55180_1, ISL55180_1_REG, etc.
        # [9] : maps directly to ISL55180 "C" bit
        # [8:2] : register offset
        ##########################################################################################
        register = getattr(self.isl55180regs, registerName)
        registeraddr = register.ADDR
        self.Log('debug', 'The deviceid 0x{:x} RegisterAddress 0x{:x}'.format(deviceid, registeraddr))
        offset = self.isl55180regs.ISL55180Command(uint=0, length=32)
        offset.Bar2Base = 0x0000
        offset.ReadWrite = 1
        offset.DeviceId = deviceid
        offset.RegisterAddr = registeraddr
        offset = offset.Pack()
        self.Log('debug', 'The offset address for ISL55180 register is 0x{:x}'.format(offset))
        self.BarWrite(2, offset * 4, data)

    def ReadIsl55180Register(self, deviceid, registerName):
        #########################################################################################
        # [17] : 1 = W, 0 = R.  This must agree with PCIe read or write.
        # [16:10] : device id   ([14] selects which ISL55180 of the two, [10] maps directly to ISL55180 "R" bit.)
        #                         device id is one of ISL55180_0, ISL55180_0_REG, ISL55180_1, ISL55180_1_REG, etc.
        # [9] : maps directly to ISL55180 "C" bit
        # [8:2] : register offset
        ##########################################################################################
        register = getattr(self.isl55180regs, registerName)
        registeraddr = register.ADDR
        self.Log('debug', 'The deviceid 0x{:x} RegisterAddress 0x{:x}'.format(deviceid, registeraddr))
        offset = self.isl55180regs.ISL55180Command(uint=0, length=32)
        offset.Bar2Base = 0x0000
        offset.ReadWrite = 0
        offset.DeviceId = deviceid
        offset.RegisterAddr = registeraddr
        offset = offset.Pack()
        self.Log('debug', 'The offset address for ISL55180 register is 0x{:x}'.format(offset))
        data = self.BarRead(2, offset * 4)
        return data

    def ConfigureAndStartSamplingEngine(self, uhc, delay, count, rate, rail_type, rails = 0x3ff):
        if rail_type ==  'HC' or rail_type == 'LC':
            result = ''
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleDelay, data=0x{:x}\n'.format(uhc, delay)
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleCount, data=0x{:x}\n'.format(uhc, count)
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleRate, data=0x{:x}\n'.format(uhc, rate)
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleMetadataHi, data=0x7654\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleMetadataLo, data=0x3210\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleRailSelect, data=0x{:x}\n'.format(uhc, rails)
            result = result + 'Q cmd=0x0, arg=Hclc{}SampleStart, data=0x0001'.format(uhc)
        else:
            result = ''
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleDelay, data=0x{:x}\n'.format(uhc, delay)
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleCount, data=0x{:x}\n'.format(uhc, count)
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleRate, data=0x{:x}\n'.format(uhc, rate)
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleMetadataHi, data=0x7654\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleMetadataLo, data=0x3210\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleRailSelect, data=0x{:x}\n'.format(uhc, rails)
            result = result + 'Q cmd=0x0, arg=Vlc{}SampleStart, data=0x0001'.format(uhc)
        return result

    def ConfigureHclcSamplingEngineforScopeShotMode(self, deviceid, dutid, samplesize, count, rate, metahi,
                                                    metalo, rail):
        engineresetvalue = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]
        SampleCountReg = getattr(self.registers,'Hclc' + str(dutid) + 'SampleCount')
        SampleRateReg = getattr(self.registers,'Hclc' + str(dutid) + 'SampleRate')
        SampleMetaHiReg = getattr(self.registers,'Hclc' + str(dutid) + 'SampleMetadataHi')
        SampleMetaLoReg = getattr(self.registers,'Hclc' + str(dutid) + 'SampleMetadataLo')
        SampleRailSelReg = getattr(self.registers,'Hclc' + str(dutid) + 'SampleRailSelect')
        SampleEngineResetReg = getattr(self.registers,'HclcSampleEngineReset')

        self.WriteRegister(self.registers.HclcUhcSampleRegionSize(value=samplesize), index=dutid)
        self.WriteRegister(SampleEngineResetReg(value=engineresetvalue[dutid]))
        self.WriteRegister(SampleCountReg(value=count))
        self.WriteRegister(SampleRateReg(value=rate))
        self.WriteRegister(SampleMetaHiReg(value=metahi))
        self.WriteRegister(SampleMetaLoReg(value=metalo))
        self.WriteRegister(SampleRailSelReg(value=rail))

    def ConfigureVlcSamplingEngineforScopeShotMode(self, deviceid, dutid, samplesize, count, rate, metahi,
                                                   metalo, rail):
        engineresetvalue = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]

        SampleSizeReg = getattr(self.registers,'VlcDut' + str(dutid) + 'SampleRegionSize')
        SampleCountReg = getattr(self.registers,'Vlc' + str(dutid) + 'SampleCount')
        SampleRateReg = getattr(self.registers,'Vlc' + str(dutid) + 'SampleRate')
        SampleMetaHiReg = getattr(self.registers,'Vlc' + str(dutid) + 'SampleMetadataHi')
        SampleMetaLoReg = getattr(self.registers,'Vlc' + str(dutid) + 'SampleMetadataLo')
        SampleRailSelReg = getattr(self.registers,'Vlc' + str(dutid) + 'SampleRailSelect')
        SampleEngineResetReg = getattr(self.registers,'VlcSampleEngineReset')

        self.WriteRegister(self.registers.HclcUhcSampleRegionSize(value=samplesize), index=dutid)
        self.WriteRegister(SampleSizeReg(value=samplesize))
        self.WriteRegister(SampleEngineResetReg(value=engineresetvalue[dutid]))
        self.WriteRegister(SampleCountReg(value=count))
        self.WriteRegister(SampleRateReg(value=rate))
        self.WriteRegister(SampleMetaHiReg(value=metahi))
        self.WriteRegister(SampleMetaLoReg(value=metalo))
        self.WriteRegister(SampleRailSelReg(value=rail))

    # GLOBAL HclcRailAlarms  HCLC_ALARM
    # 0                       0           Pass, return false (which means register doesnt need to be cleared)
    # 0                       1           Pass, return false (which means register doesnt need to be cleared)
    # 1                       0           Error
    # 1                       1           Pass, return true (which means we need to clear register)
    def CheckHclcRailAlarm(self):
        globalalarm = self.ReadRegister(self.registers.GLOBAL_ALARMS)
        if globalalarm.HclcRailAlarms == 1:
            hclcrailalarm = self.ReadRegister(self.registers.HCLC_ALARMS)
            if hclcrailalarm.value != 0x0:
                self.Log('warning', 'The hclcrailalarm should not be set, need to clear -> 0x{:x}'.format(
                    hclcrailalarm.value))
                return True
            else:
                self.Log('error', 'The hclcrailalarm should be set if hclcglobal is set Actual -> 0x{:x}'.format(
                    hclcrailalarm.value))
        else:
            return False

    def CheckVoltageForceCalRegister(self):
        defaultCalRegValue = 0x80008000
        for rail in range(10):
            voltageForceCalRegName = 'Rail0' + str(rail) + 'VOutGainOffset'
            voltageForceCalReg = self.Read(voltageForceCalRegName).Pack()
            if voltageForceCalReg != defaultCalRegValue:
                self.Log('error',
                         'Voltage force calibration register {} is not set to the {}'.format(voltageForceCalRegName,
                                                                                             defaultCalRegValue))
                self.Log('error',
                         'Voltage force calibration register {} is set to the {}'.format(voltageForceCalRegName,
                                                                                         voltageForceCalReg))

                # Set the AD5560 HCLC force voltage calibration register for specified rail

    def SetVoltageForceCalRegister(self, rail, gain, offset):
        calRegValue = (gain << 16) + offset
        voltageForceCalRegName = 'Rail0' + str(rail) + 'VOutGainOffset'
        self.Write(voltageForceCalRegName, calRegValue)
        voltageForceCalReg = self.Read(voltageForceCalRegName).Pack()
        if voltageForceCalReg != calRegValue:
            self.Log('error',
                     'Voltage force calibration register {} is not set to the {}'.format(voltageForceCalRegName,
                                                                                         calRegValue))

            # Check  AD7609 ADC voltage calibration registers for each rail to insure default value is programmed 8/8/16

    def CheckVoltageAdcCalRegister(self):
        defaultCalRegValue = 0x80008000
        for rail in range(10):
            voltageAdcCalRegName = 'Rail0' + str(rail) + 'VAdcGainOffset'
            voltageAdcCalReg = self.Read(voltageAdcCalRegName).Pack()
            if voltageAdcCalReg != defaultCalRegValue:
                self.Log('error',
                         'Voltage Adc calibration register {} is not set to the {}'.format(voltageAdcCalRegName,
                                                                                           defaultCalRegValue))
                self.Log('error', 'Voltage Adc calibration register {} is set to the {}'.format(voltageAdcCalRegName,
                                                                                                voltageAdcCalReg))

                # Set the AD7609 ADC voltage calibration register for specified gain and offset  8/8/16

    def SetVoltageAdcCalRegister(self, rail, gain, offset):
        calRegValue = (gain << 16) + offset
        voltageAdcCalRegName = 'Rail0' + str(rail) + 'VAdcGainOffset'
        self.Write(voltageAdcCalRegName, calRegValue)
        voltageAdcCalReg = self.Read(voltageAdcCalRegName).Pack()
        if voltageAdcCalReg != calRegValue:
            self.Log('error', 'Voltage Adc calibration register {} is not set to the {}'.format(voltageAdcCalRegName,
                                                                                                calRegValue))




            # Check  AD7609 ADC current calibration registers for each rail and specified i range to insure default value is programmed 8/8/16

    def CheckCurrentAdcCalRegister(self, iRange):
        defaultCalRegValue = 0x80008000
        for rail in range(10):
            if iRange == '25A':
                currentAdcCalRegName = 'Rail0' + str(rail) + 'IAdc' + '1p2A' + 'GainOffset'
            else:
                currentAdcCalRegName = 'Rail0' + str(rail) + 'IAdc' + iRange + 'GainOffset'
            currentAdcCalReg = self.ReadRegister(getattr(self.registers, currentAdcCalRegName)).value
            if currentAdcCalReg != defaultCalRegValue:
                self.Log('error', 'Current Adc calibration register {} is set to the {:x}, expecting {}'.format(
                    currentAdcCalRegName,
                    currentAdcCalReg, hex(defaultCalRegValue)))
            else:
                self.Log('debug', 'Current Adc Calibration register {} is set to {:x}'.format(currentAdcCalRegName,
                                                                                              currentAdcCalReg))


                # Set the AD7609 ADC current calibration register for specified gain and offset  8/8/16

    def SetCurrentAdcCalRegister(self, rail, gain, offset, iRange):
        calRegValue = (gain << 16) + offset
        if iRange == '25A':
            currentAdcCalRegName = 'Rail0' + str(rail) + 'IAdc' + '1p2A' + 'GainOffset'
        else:
            currentAdcCalRegName = 'Rail0' + str(rail) + 'IAdc' + iRange + 'GainOffset'
        self.Write(currentAdcCalRegName, calRegValue)
        currentAdcCalReg = self.Read(currentAdcCalRegName).Pack()
        if currentAdcCalReg != calRegValue:
            self.Log('error', 'Voltage Adc calibration register {} is not set to the {:x}'.format(currentAdcCalRegName,
                                                                                                  calRegValue))
        else:
            self.Log('info', 'Current Adc Calibration register {} is set to {:x}'.format(currentAdcCalRegName,
                                                                                         currentAdcCalReg))



            # Check  that Overvoltage calibration register is set to the default value for each rail 8/24/16

    def CheckOverVoltageCalRegister(self):
        defaultCalRegValue = 0x80008000
        for rail in range(10):
            overVoltageCalRegName = 'Rail0' + str(rail) + 'OvCompGainOffset'
            overVoltageCalReg = self.Read(overVoltageCalRegName).Pack()
            if overVoltageCalReg != defaultCalRegValue:
                self.Log('error',
                         'Overvoltage calibration register {} is not set to the {:x}'.format(overVoltageCalRegName,
                                                                                             defaultCalRegValue))
                self.Log('error', 'Overvoltage calibration register {} is set to the {:x}'.format(overVoltageCalRegName,
                                                                                                  overVoltageCalReg))
            else:
                self.Log('info', 'Overvoltage Calibration register {} is set to {:x}'.format(overVoltageCalRegName,
                                                                                             overVoltageCalReg))


                # Set the overvoltage calibration register for specified rail, gain and offset

    def SetOverVoltageCalRegister(self, rail, gain, offset):
        calRegValue = (gain << 16) + offset
        overVoltageCalRegName = 'Rail0' + str(rail) + 'OvCompGainOffset'
        self.Write(overVoltageCalRegName, calRegValue)
        overVoltageCalReg = self.Read(overVoltageCalRegName).Pack()
        if overVoltageCalReg != calRegValue:
            self.Log('error', 'Overvoltage calibration register {} is not set to the {:x}'.format(overVoltageCalRegName,
                                                                                                  calRegValue))
            self.Log('error', 'Overvoltage calibration register {} is set to the {:x}'.format(overVoltageCalRegName,
                                                                                              overVoltageCalReg))
        else:
            self.Log('info', 'Overvoltage calibration register {} is set to {:x}'.format(overVoltageCalRegName,
                                                                                         overVoltageCalReg))



            # Check  that Undervoltage calibration register is set to the default value for each rail 8/24/16

    def CheckUnderVoltageCalRegister(self):
        defaultCalRegValue = 0x80008000
        for rail in range(10):
            UnderVoltageCalRegName = 'Rail0' + str(rail) + 'UvCompGainOffset'
            UnderVoltageCalReg = self.Read(UnderVoltageCalRegName).Pack()
            if UnderVoltageCalReg != defaultCalRegValue:
                self.Log('error',
                         'Undervoltage calibration register {} is not set to the {:x}'.format(UnderVoltageCalRegName,
                                                                                              defaultCalRegValue))
                self.Log('error',
                         'Undervoltage calibration register {} is set to the {:x}'.format(UnderVoltageCalRegName,
                                                                                          UnderVoltageCalReg))
            else:
                self.Log('info', 'Undervoltage Calibration register {} is set to {:x}'.format(UnderVoltageCalRegName,
                                                                                              UnderVoltageCalReg))


                # Set the Undervoltage calibration register for specified rail, gain and offset

    def SetUnderVoltageCalRegister(self, rail, gain, offset):
        calRegValue = (gain << 16) + offset
        UnderVoltageCalRegName = 'Rail0' + str(rail) + 'UvCompGainOffset'
        self.Write(UnderVoltageCalRegName, calRegValue)
        UnderVoltageCalReg = self.Read(UnderVoltageCalRegName).Pack()
        if UnderVoltageCalReg != calRegValue:
            self.Log('error',
                     'Undervoltage calibration register {} is not set to the {:x}'.format(UnderVoltageCalRegName,
                                                                                          calRegValue))
            self.Log('error', 'Undervoltage calibration register {} is set to the {:x}'.format(UnderVoltageCalRegName,
                                                                                               UnderVoltageCalReg))
        else:
            self.Log('info', 'Undervoltage calibration register {} is set to {:x}'.format(UnderVoltageCalRegName,
                                                                                          UnderVoltageCalReg))

    def SetSampleCollectorIndexRegister(self, rail, railType, vOrI):
        # Configuring Sample Collector Index Register
        # Setting sample collecotr index register also starts collecting sample data
        # Writing sample collector index register should be close to reading out data from sample collector
        index = None
        if railType == "HCLC" and vOrI == "V":
            # HCLC Rail 0 Voltage starts with 0x20
            # Multiplication 2 is coming from data from each rail in pair of voltage and current
            index = rail * 2 + 0x20
        elif railType == "HCLC" and vOrI == "I":
            # HCLC Rail 0 Current starts with 0x21
            index = rail * 2 + 0x21
        elif railType == "VLC" and vOrI == "V":
            # VLC Rail 0 voltage starts with 0x0
            index = rail * 2 + 0x00
        elif railType == "VLC" and vOrI == "I":
            # VLC Rail 0 Current starts with 0x1
            index = rail * 2 + 0x01
        sampleCollectorIndex = 'SampleCollectorIndex'
        self.Write(sampleCollectorIndex, index)
        sampleCollectorIndexAfterWrite = self.Read(sampleCollectorIndex).Pack()
        if sampleCollectorIndexAfterWrite != index:
            self.Log('error', 'Sample Collecotr Index Register  is not set to the {} but {}'.format(index,
                                                                                                    sampleCollectorIndexAfterWrite))

    def SingleSampleCollectorData(self):
        sampleCollectorCount = 'SampleCollectorCount'
        sampleCollectorData = 'SampleCollectorData'
        collectorCount = self.Read(sampleCollectorCount).Pack()
        collectorData = self.Read(sampleCollectorData).Pack()
        collectorDataAfterDecode = hddpstbc.DecodeFloatingPoint32(collectorData)

        self.Log('debug', 'Sample Collecotr Count Register  is  set to the {}'.format(collectorCount))
        self.Log('debug', 'Sample Collecotr Data  is  set to the {}'.format(collectorDataAfterDecode))
        return collectorCount, collectorDataAfterDecode

    def ReadSampleCollectorData(self, rail, railType, vOrI, delay, driveValue, driveType, load):
        expectedDrive = None
        loadValue = load
        if load is "NONE":
            loadValue = 10 ^ 6  # Setting 1 MOhm to model the infinite resistance from "NONE"
        self.SetSampleCollectorIndexRegister(rail, railType, vOrI)
        # delay is set to give enough time to sample collecotr to fill the data queue
        # without delay, sample collector count will be random, but still valid
        time.sleep(delay)
        if driveType == "VFORCE" and vOrI == "V":
            expectedDrive = driveValue
        elif driveType == "VFORCE" and vOrI == "I":
            expectedDrive = driveValue / loadValue
        elif driveType == "IFORCE" and vOrI == "I":
            expectedDrive = driveValue
        elif driveType == "IFORCE" and vOrI == "V":
            expectedDrive = driveValue * loadValue
        # HLD spec the 258 is the maximum capacity of sample collection
        for i in range(0, 259):
            # Actual count from FPGA will start from 258 and going down.
            # So creating 258-i
            indexAfterCorrection = 258 - i
            collectorCount, collectDataAfterDecode = self.SingleSampleCollectorData()
            # delta between FPGA value and expected value.
            error = abs(collectDataAfterDecode - expectedDrive)
            if collectorCount != indexAfterCorrection:
                self.Log('error', 'Sample delay {}s doesnt seem to be set properly'.format(delay))
                self.Log('error',
                         'Sample Collecotr Count Register  is  set to the {} while expected count is {}.'.format(
                             collectorCount, indexAfterCorrection))
            if error >= 0.2:
                self.Log('error', 'Sample Collecotr data value is set to the {} while expected data is {}.'.format(
                    collectDataAfterDecode, expectedDrive))


    def CheckHclcSamplingActive(self, dutid):
        active = self.ReadRegister(self.registers.HclcSamplingActive).value
        self.Log('info', 'Hclc Sampling Active reg is set to 0x{:x}'.format(active))
        expected = 0x1 << dutid
        if active != expected:
            self.Log('warning',
                     'The sampling active is not set for expected dutid {}. Expected -> 0x{:x} Actual -> 0x{:X}'.format(
                         dutid, expected, active))
        while active != 0:
            active = self.ReadRegister(self.registers.HclcSamplingActive).value

        self.Log('info', 'Hclc Sampling Active reg is set to 0x{:x}'.format(active))

    def CheckHclcMultipleSamplingActive(self, dutid):
        active = self.ReadRegister(self.registers.HclcSamplingActive).value
        self.Log('info', 'Hclc Sampling Active reg is set to 0x{:x}'.format(active))
        expected = dutid
        if active != expected:
            self.Log('warning',
                     'The sampling active is not set for expected dutid {}. Expected -> 0x{:x} Actual -> 0x{:X}'.format(
                         dutid, expected, active))
        while active != 0:
            active = self.ReadRegister(self.registers.HclcSamplingActive).value

        self.Log('info', 'Hclc Sampling Active reg is set to 0x{:x}'.format(active))

    def CheckVlcSamplingActive(self, dutid):
        active = self.ReadRegister(self.registers.VlcSamplingActive).value
        self.Log('info', 'Vlc Sampling Active reg is set to 0x{:x}'.format(active))
        expected = 0x1 << dutid
        if active != expected:
            self.Log('error',
                     'The sampling active is not set for expected dutid {}. Expected -> 0x{:x} Actual -> 0x{:X}'.format(
                         dutid, expected, active))
        while active != 0:
            active = self.ReadRegister(self.registers.VlcSamplingActive).value

        self.Log('info', 'Vlc Sampling Active reg is set to 0x{:x}'.format(active))

    def CheckHclcSampleAlarms(self):
        globalalarm = self.ReadRegister(self.registers.GLOBAL_ALARMS)
        if globalalarm.HclcSampleAlarms == 1:
            self.Log('error', 'Hclc Sample Alarms is set . Value ->0x{:x}'.format(globalalarm.value))
            hclcsamplealarm = self.ReadRegister(self.registers.HclcSampleAlarms)
            self.Log('info', 'HclcSampleAlarms Register is set to 0x{:x}'.format(hclcsamplealarm.value))

    def CheckVlcSampleAlarms(self):
        globalalarm = self.Read('GLOBAL_ALARMS')
        if globalalarm.VlcSampleAlarms == 1:
            self.Log('error',
                     'Vlc Sample Alarms is set in GLOBAL_ALARMS Register. Actual ->0x{:x}'.format(globalalarm.Pack()))
            vlcsamplealarm = self.Read('VlcSampleAlarms')
            self.Log('info', 'VlcSampleAlarms Register is set to 0x{:x}'.format(vlcsamplealarm.Pack()))

    def ExecuteTriggerQueueWithExpectedAlarm(self, offset, dutid, expected_alarm):
        trigger = self.ReadRegister(self.registers.ExecuteTrigger)
        trigger.Execute = 1
        trigger.ByteAddress = offset
        self.Write('ExecuteTrigger', trigger.value)
        self.Log('info', 'The ExecuteTrigger register is set to 0x{:x}'.format(trigger.value))
        if self.LegacyTcLooping:
            tqnotifyalarm = 'TqNotifyAlarmDut' + str(dutid)
        else:
            tqnotifyalarm = expected_alarm
        self.WaitForGlobalAlarmBit(tqnotifyalarm)

    def ExecuteTriggerQueueviaRc(self, offset, dutid, triggerValue, rail_type):
        self.Log('debug', 'The RESETS register is set to \n{}'.format(self.Read('RESETS')))

        expected = triggerValue
        self.rc.send_trigger(triggerValue)
        time.sleep(0.010)
        self.Log('debug', 'RC sent trigger 0x{:x}'.format(triggerValue))

        triggerValue = self.Read('TRIGGERDOWNTEST')
        if triggerValue.Pack() != expected:
            self.Log('error', 'HDDPS received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected,
                                                                                                        triggerValue.Pack()))
        else:
            self.Log('debug',
                     'HDDPS({}, {}) received trigger 0x{:x}'.format(self.slot, self.subslot, triggerValue.Pack()))
        self.CheckRailFold()
        self.CheckTqNotifyAlarm(dutid, rail_type)

    def CheckTqNotifyAlarm(self, dutid, rail_type):
        if self.LegacyTcLooping:
            tqnotifyalarm = 'TqNotifyAlarmDut' + str(dutid)
        else:
            if 'HC' == rail_type or 'LC' == rail_type:
                tqnotifyalarm = 'TqNotifyLCHC'
            elif 'VLC' == rail_type:
                tqnotifyalarm = 'TqNotifyVlc'
            else:
                self.Log('error', 'Rail Type not supported: {}'.format(self.railTypes))
        self.WaitForGlobalAlarmBit(tqnotifyalarm)

    def CheckRailFold(self):
        hclctqbusy = self.Read('HclcRailsFolded')
        self.Log('info', 'HclcRail Fold Register is set to 0x{:x}'.format(hclctqbusy.Pack()))
        vlctqbusy = self.Read('VlcRailsFolded')
        self.Log('info', 'VlcRail Fold Register is set to 0x{:x}'.format(vlctqbusy.Pack()))

    def ConnectCalBoard(self, channel, load):
        cb = hddps_cal_board(self.slot)
        cb.init()
        # cb.dumpRawConfig()
        cb.selectChannel(channel)
        cb.selectLoad(load)
        cb.apply()
        # cb.dumpRawConfig()

    def RaiseHclcSampleAlarms(self):
        globalalarm = self.Read('GLOBAL_ALARMS')
        if globalalarm.HclcSampleAlarms == 1:
            self.Log('error',
                     'Hclc Sample Alarms is set in GLOBAL_ALARMS Register. Actual ->0x{:x}'.format(globalalarm.Pack()))
            hclcsamplealarm = self.Read('HclcSampleAlarms')
            self.Log('info', 'HclcSampleAlarms Register is set to 0x{:x}'.format(hclcsamplealarm.Pack()))
            raise Exception('Due to Sample Alarms, sample header reading aborted')

    def RaiseVlcSampleAlarms(self):
        globalalarm = self.Read('GLOBAL_ALARMS')
        if globalalarm.VlcSampleAlarms == 1:
            self.Log('error',
                     'Vlc Sample Alarms is set in GLOBAL_ALARMS Register. Actual ->0x{:x}'.format(globalalarm.Pack()))
            vlcsamplealarm = self.Read('VlcSampleAlarms')
            self.Log('info', 'VlcSampleAlarms Register is set to 0x{:x}'.format(vlcsamplealarm.Pack()))
            raise Exception('Due to Sample Alarms, sample header reading aborted')


    def WriteSamplesToCSV(self,filename,headers):
        sample_block = 0
        time_string = time.strftime("%Y%m%d%H%M%S")
        filename = filename + '_' + time_string + '.csv'

        with open(filename, 'w') as f:
            # write header to file
            temp_list = []
            temp_list.append('BLOCK')
            temp_list.append('SAMPLE')
            temp_list.append('RAIL')
            temp_list.append('VOLTAGE')
            temp_list.append('CURRENT')
            print(','.join(temp_list), file=f)

            for header in headers:
                rails = []
                #need to check for rail count of rail type on hardware vlc, vs, hc.lc
                rail_count = 10
                rails_selected = header.RailSelect
                sample_start_offset = header.SamplePointer
                sample_count = header.SampleCount

                for i in range(rail_count):
                    rail_active = (rails_selected >> i) & 0x0001
                    if rail_active == 1:
                        rails.append(i)

                sample_memory_usage = len(rails) * (4 + 4) * sample_count
                sample_data = self.DmaRead(sample_start_offset, sample_memory_usage)
                sample_data_stream = io.BytesIO(sample_data)
                sample_data_stream.seek(0)
                for sample in range(sample_count):
                    for rail_index in range(len(rails)):
                        voltage = struct.unpack('<f', sample_data_stream.read(4))[0]
                        current = struct.unpack('<f', sample_data_stream.read(4))[0]
                        print('{},{},{},{},{}'.format(sample_block,sample,rails[rail_index],voltage,current), file = f)

                sample_block = sample_block + 1


    def ClearDpsAlarms(self):
        self.ClearVlcRailAlarm()
        self.ClearVlcSampleAlarms()
        self.ClearHclcRailAlarms()
        self.ClearHclcSampleAlarms()
        self.ClearTrigExecAlarms()
        self.ClearHclcCfoldAlarms()
        self.ClearVlcCfoldAlarms()
        self.ClearGlobalAlarms()

    def FindSampleHeaderSize(self, uhc, fpgaBoard):
        loopCond = True
        headerSize = 0
        headerRegionBase = None
        headerRegionSize = None
        if fpgaBoard == 'HCLC':
            headerRegionBase = self.ReadRegister(self.registers.HclcUhcSampleHeaderRegionBase, index=uhc).value
            headerRegionSize = self.ReadRegister(self.registers.HclcUhcSampleHeaderRegionSize, index=uhc).value
            self.RaiseHclcSampleAlarms()
        elif fpgaBoard == 'VLC':
            headerRegionBase = self.ReadRegister(self.registers.VlcUhcSampleHeaderRegionBase, index=uhc).value
            headerRegionSize = self.ReadRegister(self.registers.VlcUhcSampleHeaderRegionSize, index=uhc).value
            self.RaiseVlcSampleAlarms()
        while loopCond:
            data = self.DmaRead(headerRegionBase + headerSize,
                                4)  # Read in 4 bytes of data, which is 1 DWord sample header data
            convertedData = self.ReadMemoryIn4Bytes(data)
            self.Log('debug', "headerRegion data is  {}".format(int(convertedData, 16)))
            if int(convertedData, 16) == 0xFFFFFFFF:  # Sample Header of 4 DWord has moving header end 0xFFFFFFFF.
                loopCond = False
                return headerSize
            # Header is increased by 4 Bytes.
            headerSize += 4
            # Check whether header Size in loop is beyond Header Region size from register
            if headerSize > headerRegionSize:
                globalalarms = self.Read('GLOBAL_ALARMS')
                if globalalarms.HclcRailAlarms == 1:
                    loopCond = False
                    return headerSize
                else:
                    raise Exception('Sample Header End cannot be found. Looping is beyond Header Region Size')

    def FindSampleRegionSize(self, dutid, fpgaBoard):
        # Header Format
        # 0x00 - sample pointer
        # 0x04 - sample count, sample rate ( 16 bit each)
        # 0x08 - rail config, rail select  (16 bit each)
        # 0x0c - metadata
        sampleHeader = self.ReadSampleHeader(dutid, fpgaBoard)
        sampleSizeList = []  # sample Size List in Bytes for multiple Runs on corresponding sampling engine
        LOWER16BITMASK = 0x0000FFFF
        UPPER16BITMASK = 0xFFFF0000
        tempDwordList = []  # Temporary Dword List for each Dword
        sampleCount = 0
        SampleRate = 0
        railConfig = 0
        railSelect = 0
        for idx, sampleHeaderByte in enumerate(sampleHeader):  # sampleHeader read by BYTE
            sampleHeaderDword = []  # sample header DWord that needs to be processed
            dWordIdx = idx % 4  # D word Boundary
            sampleHeaderIdx = idx % 16  # Find Sample Count and Sample Rate Dword Index end
            wordRailIdx = idx % 16  # Find Rail Count and Rail Rate Dword Index end
            tempDwordList.append('{:02x}'.format(sampleHeaderByte))
            if sampleHeaderIdx == 7:  # 7 is the byte address for end of 0x04 in header, which is sample count, sample rate
                sampleHeaderDword = int(''.join(reversed(tempDwordList)),
                                        16)  # Reversing the Dword and interprete it in hex number
                sampleCount = (
                                  sampleHeaderDword & UPPER16BITMASK) >> 16  # Take upper 16 bit and rightshift out 16 bit to take only upper 16 bits.
                sampleRate = sampleHeaderDword & LOWER16BITMASK
            elif sampleHeaderIdx == 11:  # 11 is the byte address for end of 0x08 in header, which is rail config, rail select
                sampleHeaderDword = int(''.join(reversed(tempDwordList)), 16)
                railConfig = (sampleHeaderDword & UPPER16BITMASK) >> 16
                railSelect = sampleHeaderDword & LOWER16BITMASK
                railsFromRailConfigAndRailSelect = railConfig & railSelect  # selected Rails are common of railConfig and railSelect
                railsInBinaryNumbers = '{0:016b}'.format(
                    railsFromRailConfigAndRailSelect)  # express it in binary string
                actualNumberOfRails = self.SumBinaryString(
                    railsInBinaryNumbers)  # sum of all binary bits to count number of rails.
                totalSampleNumbers = sampleCount * actualNumberOfRails * 2  # 2 is coming from voltage and current for each rail as a pair
                totalSampleSizeInByte = totalSampleNumbers * 4  # 4 is one sample data size in byte, which is 32 bit.
                sampleSizeList.append(
                    totalSampleSizeInByte)  # add in list for multiple runs in the corresponding sample engine.
                sampleCount = 0  # Reinit it to 0
                sampleRate = 0  # Reinit it to 0
            if dWordIdx == 3:  # reinit tempDwordList on every Dword.
                tempDwordList = []
        result = sum(sampleSizeList)  # sum all the runs
        return result

    def SumBinaryString(self, value):
        sum = 0
        for i in value:
            sum = sum + int(i, 2)
        return sum

    def ReadMemoryIn4Bytes(self, data):
        s = []
        for byte in data:
            s.append('{:02x}'.format(byte))
            if len(s) == 4:
                # if ''.join(s) == '00000000':
                #    s = []
                #    break
                byteData = '0x' + ''.join(reversed(s))
                self.Log('debug', byteData)
                return byteData
                # s = []
        if len(s) != 0:
            raise Exception('Data length is expected to be a multiple of 4 (i.e. dword aligned)')

    def ReadSampleHeader(self, uhc, fpgaBoard):
        headerRegionBase = None
        if fpgaBoard == 'HCLC':
            headerRegionBase = self.ReadRegister(self.registers.HclcUhcSampleHeaderRegionBase, index=uhc).value
        elif fpgaBoard == 'VLC':
            headerRegionBase = self.ReadRegister(self.registers.VlcUhcSampleHeaderRegionBase, index=uhc).value
        # Finding Ending Tag for Multiple Sample Runs in Sample Engine corresponding to UHC.
        headerRegionSize = self.FindSampleHeaderSize(uhc,
                                                     fpgaBoard)  # board.Read('HclcDut{}HeaderRegionSize'.format(dutid))
        self.Log('debug',
                 'Reading sample header for ({}, {}): base=0x{:x}, size=0x{:x}'.format(self.slot, self.subslot,
                                                                                       headerRegionBase,
                                                                                       headerRegionSize))
        return self.DmaRead(headerRegionBase, headerRegionSize)  # Shall be the exact valid block, nothing nothing less

    def ReadSampleData(self, uhc, fpgaBoard):
        if fpgaBoard == 'HCLC':
            sampleRegionBase = self.ReadRegister(self.registers.HclcUhcSampleRegionBase, index=uhc).value
        elif fpgaBoard == 'VLC':
            sampleRegionBase = self.ReadRegister(self.registers.VlcUhcSampleRegionBase, index=uhc).value
        sampleRegionSize = self.FindSampleRegionSize(uhc,
                                                     fpgaBoard)  # board.Read('HclcDut2SampleRegionSize'.format(dutid)).Pack()  # FIXME
        self.Log('info',
                 'Reading sample data for ({}, {}): base=0x{:x}, size=0x{:x}'.format(self.slot, self.subslot,
                                                                                     sampleRegionBase,
                                                                                     sampleRegionSize))
        return self.DmaRead(sampleRegionBase, sampleRegionSize)  # Shall be the exact valid block, nothing nothing less

    def ReadHeaderSampleRegion(self, HeaderBase, hsize, SampleBase, ssize):
        self.Log('info', 'Reading header')
        data = self.DmaRead(HeaderBase, hsize)
        self.ReadMemory(data)

        self.Log('info', 'Reading sample engine')
        data = self.DmaRead(SampleBase, ssize)
        self.ReadSamples(data)

    def ReturnSampleCurrentData(self, SampleBase):
        # The goal of this code is to return a stable voltage point
        self.Log('info', 'Reading sample engine')
        # Sample base offset to sample a stable voltage
        # 4*256 is hardcoded 256 data points
        # 128 pair of voltage and current
        # 1.28mS data point
        offset = SampleBase + 4 * 256
        # Reading only one voltage point
        data = self.DmaRead(offset + 4, 4)  # 4 is hardcoded Dword for one data point
        # print(binascii.hexlify(data))
        currentData = self.ReturnSamples(data)
        return currentData

    def ReturnSampleData(self, SampleBase):
        # The goal of this code is to return a stable voltage point
        self.Log('info', 'Reading sample engine')
        # Sample base offset to sample a stable voltage
        # 4*256 is hardcoded 256 data points
        # 128 pair of voltage and current
        # 1.28mS data point
        offset = SampleBase + 4 * 256
        # Reading only one voltage point
        data = self.DmaRead(offset, 4)  # 4 is hardcoded Dword for one data point
        voltageData = self.ReturnSamples(data)
        return voltageData

    def ReturnSamples(self, data):
        dword = 0
        sampleData = None
        # print(enumerate(data))
        for i, byte in enumerate(data):
            # print('0x{:x}'.format(byte))
            j = i % 4
            # print(j)
            dword = dword | (byte << (8 * j))
            if j == 3:
                # got a full dword
                # print(dword)
                sampleData = '{}'.format(hddpstbc.DecodeFloatingPoint32(dword))
                # print(sampleData)
                dword = 0
                return float(sampleData)



    def ReadSamples(self, data):
        dword = 0
        for i, byte in enumerate(data):
            # print('0x{:x}'.format(byte))
            j = i % 4
            dword = dword | (byte << (8 * j))
            if j == 3:
                # got a full dword
                self.Log('info', '0x{:x} {}'.format(dword, hddpstbc.DecodeFloatingPoint32(dword)))
                dword = 0

    def ClearTrigExecAlarms(self):
        self.Write('TrigExecAlarm', 0xffffffff)

    def ClearTriggerDebugFifo(self):
        while (self.Read('TRIGGERDOWNTEST').Pack() != 0x0):
            pass

    def CheckVlcRailAlarm(self):
        globalalarm = self.Read('GLOBAL_ALARMS')
        if globalalarm.VlcRailAlarms == 1:
            vlcrailalarm = self.Read('VlcRailAlarms')
            if vlcrailalarm.Pack() != 0x0:
                self.Log('error',
                         'The vlcrailalarm should not be set for any rail Actual -> 0x{:x}'.format(vlcrailalarm.Pack()))
                return True
            else:
                return False

    def CheckRailAssertTest(self, rail, assert_test, mode):
        self.Log('info', 'Testing rail -> {} on slot: {} subslot: {}'.format(rail, self.slot, self.subslot))
        self.SetRailsToSafeState()
        railno = self.HwRailtoPerDeviceRail(rail)
        if rail < 16:
            self.ClearVlcCfoldAlarms()
            if rail < 10:
                self.ConnectCalBoard('VLC0{}'.format(rail), 'NONE')
            else:
                self.ConnectCalBoard('VLC{}'.format(rail), 'NONE')
            number_of_rails = 16
            resource_target = 'VLC'
            self.bring_rails_out_of_safe_state(resource_target)
        else:
            if self.subslot == 0:
                self.ConnectCalBoard('LC{}'.format(railno), 'NONE')
            else:
                self.ConnectCalBoard('HC{}'.format(railno), 'NONE')
            self.ClearHclcCfoldAlarms()
            number_of_rails = 10
            resource_target = 'HC'
            self.bring_rails_out_of_safe_state(resource_target)
        self.ClearGlobalAlarms()
        assert_test_data = 99
        if assert_test == 'POWER':
            self.Log('debug', 'Testing POWER STATE mode: {}'.format(mode))
            self.clearHclcRailsFoldedRegister()
            HclcRailsFolded = self.Read('HclcRailsFolded').Pack()
            self.Log('debug', 'Rail {} Hclc Rails Folded register status before in first if {}'.format(rail, hex(HclcRailsFolded)))
            # Setting VForce mode
            command = symbols.RAILCOMMANDS.SET_MODE
            data = symbols.SETMODECMD.VFORCE
            self.WriteBar2RailCommand(command, data, rail)
            self.Log('debug', 'RailCommand SET_MODE is set to VFORCE {}'.format(data))
            # Setting Current Range
            command = symbols.RAILCOMMANDS.SET_CURRENT_RANGE
            if rail < 16:
                # For VLC rail the current range is being set to 256 MA and i.e. represented by 5.
                data = symbols.VLCIRANGE.I_256_MA
                self.Log('debug', 'Setting the IRANGE to 256MA for VLC rails')
                self.WriteBar2RailCommand(command, data, rail)
            else:
                # (FIXME) Set current clamp hi and clamp lo for the rail and set the current range
                self.SetAD5560CurrentClamp([], rail, symbols.HCLCIRANGE.I_500_MA, -0.4)
                self.SetAD5560CurrentClamp([], rail, symbols.HCLCIRANGE.I_500_MA, 0.45)
            if mode == 'ON':
                self.Log('debug', 'The rail should be enabled in ON mode but is turned OFF to simulate cfold alarm.')
                command = symbols.RAILCOMMANDS.ENABLE_DISABLE_RAIL
                self.WriteBar2RailCommand(command, 0, rail)
                assert_test_data = 1
            else:
                self.Log('debug', 'The rail should be disabled in OFF mode but is turned ON to simulate cfold alarm.')
                command = symbols.RAILCOMMANDS.ENABLE_DISABLE_RAIL
                self.WriteBar2RailCommand(command, 1, rail)
                assert_test_data = 0

        elif assert_test == 'OPMODE':
            command = symbols.RAILCOMMANDS.SET_MODE
            if mode == 'VFORCE':
                self.Log('debug', 'Testing opmode VFORCE but setting mode to IFORCE to simulate cfold alarm')
                data = symbols.SETMODECMD.IFORCE
                assert_test_data = 0
            elif mode == 'IFORCE':
                self.Log('debug', 'Testing opmode IFORCE but setting mode to ISINK to simulate cfold alarm')
                data = symbols.SETMODECMD.ISINK
                assert_test_data = 1
            elif mode == 'ISINK':
                self.Log('debug', 'Testing opmode ISINK but setting mode to VFORCE to simulate cfold alarm')
                data = symbols.SETMODECMD.VFORCE
                assert_test_data = 2
            self.WriteBar2RailCommand(command, data, rail)
        elif assert_test == 'IRANGE':
            self.Log('debug', 'Testing rail {} in IRANGE mode for range->{}'.format(rail, mode))
            command = symbols.RAILCOMMANDS.SET_CURRENT_RANGE
            if rail < 16:
                data = getattr(symbols.VLCIRANGE, mode)
                assert_test_data = 7
            else:
                data = getattr(symbols.HCLCIRANGE, mode)
                assert_test_data = 7
            self.WriteBar2RailCommand(command, data, rail)
        else:
            self.Log('error', 'The assert_test provided is not a valid option')

        # Do assertion

        assert_command = getattr(symbols.ASSERTTEST, assert_test)
        data = (assert_command << 8) | assert_test_data
        self.Log('debug', 'Setting the Assert Test Rail command to value 0x{:x}'.format(data))
        command = symbols.RAILCOMMANDS.ASSERT_TEST_RAIL
        self.WriteBar2RailCommand(command, data, rail)
        # Delay needed
        time.sleep(0.001)
        if rail < 16:
            self.CheckVlcCfoldAlarm(rail)
        else:
            self.ClearHclcRailAlarms()
            self.CheckForHclcCfoldAlarm(rail)

    def CheckVlcCfoldAlarm(self, rail):
        globalalarm = self.Read('GLOBAL_ALARMS').Pack()
        if globalalarm != 0x1000:
            self.Log('error',
                     'Only the VlcCfoldAlarms should get set in the global alarm register Actual -> 0x{:x} Expected -> 0x1000'.format(
                         globalalarm))
        else:
            self.Log('info', 'GLOBAL_ALARMS Register for rail {} is set to 0x{:x}'.format(rail, globalalarm))

        railalarm = 'Rail' + str(rail) + 'Alarm'
        vlccfoldalarm = self.Read('VlcCfoldAlarms')
        if getattr(vlccfoldalarm, railalarm) != 1:
            self.Log('error', 'The CFOLD alarm bit for rail->{} should get set. Actual -> 0x{:x}'.format(rail,
                                                                                                         vlccfoldalarm.Pack()))
        else:
            self.Log('info',
                     'The CFOLD alarm for rail->{} is set to Actual -> 0x{:x}'.format(rail, vlccfoldalarm.Pack()))

        # regName = 'Vlc' + str(rail) + 'CfoldData'
        regName = 'Vlc{:02d}CfoldData'.format(rail)
        vlccfolddata = self.Read(regName)
        actual = getattr(vlccfolddata, 'CFoldActualData')
        expected = getattr(vlccfolddata, 'AssertTestData')
        if actual == expected:
            self.Log('error',
                     'VLC CFoldActualData should be same as AssertTestData. Actual -> {} Expected -> {}'.format(actual,
                                                                                                                expected))
        else:
            self.Log('info', 'VlcCfoldData register is set to value -> 0x{:x}'.format(vlccfolddata.Pack()))

    def CheckForHclcGlobalAlarm(self, alarmType):
        globalalarm = self.ReadRegister(self.registers.GLOBAL_ALARMS)
        alarmSetting = getattr(globalalarm, alarmType)
        if alarmSetting != 1:
            self.Log('error',
                     'The HclcCfoldAlarms was expected, we shouldsee it in the global alarm register. Read value -> 0x{:x}'.format(
                         globalalarm.value))
        else:
            self.Log('info', 'GLOBAL_ALARMS Register is set to 0x{:x}'.format(globalalarm.value))

    def CheckForHclcRailAlarm(self, rail):
        rail = self.HwRailtoPerDeviceRail(rail)
        railalarm = 'Rail' + str(rail) + 'Alarm'
        hclccfoldalarm = self.ReadRegister(self.registers.HclcCfoldAlarms)
        if getattr(hclccfoldalarm, railalarm) != 1:
            self.Log('error', 'The CFOLD alarm bit for rail->{} should be set. Actual -> 0x{:x}'.format(rail,
                                                                                                        hclccfoldalarm.value))
        else:
            self.Log('info', 'The CFOLD alarm for rail->{} is set to expected value -> 0x{:x}'.format(rail,
                                                                                                      hclccfoldalarm.value))

    def CheckValidHclcFoldData(self, rail):
        rail = self.HwRailtoPerDeviceRail(rail)
        hclccfolddata = self.getHcLcPerRailControlledFoldDetailsRegister()
        hclccfolddata = self.ReadRegister(hclccfolddata, index=rail)
        actual = hclccfolddata.CFoldActualData
        expected = hclccfolddata.AssertTestData
        if actual == expected:
            self.Log('error',
                     'HCLC CFoldActualData shouldn\'t be same as AssertTestData. Actual -> {} Expected -> {}'.format(
                         actual, expected))
        else:
            self.Log('info', 'HclcCfoldData register is set to value -> 0x{:x}'.format(hclccfolddata.value))

    def CheckForHclcCfoldAlarm(self, rail):
        self.CheckForHclcGlobalAlarm('HclcCFoldAlarms')
        self.CheckForHclcRailAlarm(rail)
        self.CheckValidHclcFoldData(rail)

    def ad5560DpsReg1Initialize(self, registerType):
        externalRange1 = 0x6
        externalRange2 = 0x5
        compareDutVoltage = 0x3
        measureOutISense = 0x1

        register = registerType()
        register.ForceAmpEnable = 0x0
        register.CurrentRange = externalRange1
        register.ComparatorFunction = compareDutVoltage
        register.MeasureOutEnable = 0x1
        register.MeasureOut = measureOutISense
        register.ClampEnable = 0x1
        return register

    def ad5560DpsReg2Initialize(self, registerType):
        masterOut = 0x1
        internalSenseResistor = 0x1
        externalSense = 0x0
        slewRate_0p35_V_per_uS = 0x6
        slewRate_1V_per_uS = 0x0

        register = registerType()
        register.SystemForceSense = 0x1
        register.SlewRateControl = slewRate_1V_per_uS
        register.GPO = 0x0
        register.SlaveGangMode = masterOut
        register.INT10K = externalSense
        register.HighZGuard = 0x1
        return register

    def ad5560SysControlInitInitialize(self, registerType):
        tempLimit100C = 0x3
        register = registerType()
        register.TemperatureLimit = tempLimit100C
        register.Gain = 0x1  # Gain=1 / Measout Gain = 1 / MI Gain = 10
        register.FinToGround = 0x0  # Switch Force Amp to Output DAC
        register.ComparatorOutput = 0x0  # Disable simple window compare
        register.ForceAmpPowerdown = 0x1  # Power down force amplifier block
        register.ActiveLoad = 0x0  # Normal operation of clamp enable and hw inhibit
        return register

    def ad5560Compensation1Initialize(self, registerType):
        register = registerType()
        register.CompensationResistanceDut = 0x0  # 0nF to 50nF
        register.EquivalentSeriesResistance = 0x0  # 0mOhm to 1 mOhm
        register.SafeMode = 0x0  # override values in Comp Register 1 to make force amp stable
        return register

    def ad5560Compensation2Initialize(self, registerType):
        register = registerType()
        register.ManualCompensation = 0x1  # manual mode enabled
        register.ResistanceZero = 0x2  # Rz -> 5kohm and Fz = 320 KHz
        register.ResistancePole = 0x0  # Rp -> 200ohm and Fp = 100 Mhz
        register.TransconductanceForceAmp = 0x2  # 300 uA/V and 490 kHz
        register.FeedForwardCapacitorSelect = 0x5  # CF4 switched on
        register.CapThreeTo100K = 0x0  # Disconnected
        register.CapTwoTo25K = 0x0  # Disconnected
        register.CapOneTo6K = 0x0  # Disconnected
        return register

    def ad5560AlarmSetupInitialize(self, registerType):
        register = registerType()
        register.LatchTemperatureAlarm = 0
        register.DisableTemperatureAlarm = 0
        register.LatchOscillationAlarm = 0
        register.DisableOscillationAlarm = 1
        register.LatchDutAlarm = 0
        register.DisableDutAlarm = 1
        register.LatchClampAlarm = 0
        register.DisableClampAlarm = 1
        register.LatchGroundAlarm = 0
        register.DisableGroundAlarm = 1
        return register

    def InitializeAd5560Rails(self):
        self.Log('debug', 'Initializing the Ad5560 registers')
        ad5560DpsReg1Init = self.ad5560DpsReg1Initialize(ad5560_registers.DPS_REGISTER1)
        ad5560DpsReg2Init = self.ad5560DpsReg2Initialize(ad5560_registers.DPS_REGISTER2)
        ad5560SysControlInit = self.ad5560SysControlInitInitialize(ad5560_registers.SYSTEM_CONTROL)
        ad5560Compensation1 = self.ad5560Compensation1Initialize(ad5560_registers.COMPENSATION1)
        ad5560Compensation2 = self.ad5560Compensation2Initialize(ad5560_registers.COMPENSATION2)
        ad5560AlarmSetupInit = self.ad5560AlarmSetupInitialize(ad5560_registers.ALARM_SETUP)
        Ad5560VoltageRangeConstOffset = 0x58C0

        for rail in range(DPS_NUM_AD5560_RAILS):
            self.WriteAd5560Register(ad5560_registers.OFFSET_DACX.ADDR, Ad5560VoltageRangeConstOffset, rail)
            self.WriteAd5560Register(ad5560SysControlInit.ADDR, ad5560SysControlInit.value, rail)
            self.WriteAd5560Register(ad5560DpsReg1Init.ADDR, ad5560DpsReg1Init.value, rail)
            self.WriteAd5560Register(ad5560DpsReg2Init.ADDR, ad5560DpsReg2Init.value, rail)
            self.WriteAd5560Register(ad5560AlarmSetupInit.ADDR, ad5560AlarmSetupInit.value, rail)
            self.WriteAd5560Register(ad5560Compensation1.ADDR, ad5560Compensation1.value, rail)
            self.WriteAd5560Register(ad5560_registers.RAMP_STEP_SIZE.ADDR, 20, rail)
            self.WriteAd5560Register(ad5560_registers.FIN_DAC_X1.ADDR, Ad5560VoltageRangeConstOffset, rail)
            if self.subslot == 1:
                self.WriteAd5560Register(ad5560Compensation2.ADDR, ad5560Compensation2.value, rail)

    def SetAD5560CurrentClamp(self, isLow, rail, iRange, current_value):
        # Set Rail to Mask
        self.Log('debug', 'Setting Current clamp for rail->{}'.format(rail))

        command = symbols.RAILCOMMANDS.SET_CURRENT_RANGE
        self.WriteBar2RailCommand(command, iRange, rail)

        command = symbols.RAILCOMMANDS.SET_CURRENT_CLAMP_HI
        if (isLow):
            command = symbols.RAILCOMMANDS.SET_CURRENT_CLAMP_LOW
        current = hddpstbc.EncodeSFP(current_value)
        self.WriteBar2RailCommand(command, current, rail)

    def SetBfmConfig(self, config, loadValue):
        # Intial Draft for HDDPS SetConfig
        defaultResistance = 10
        validConfigs = ['ResistiveLoopback']
        if config not in validConfigs:
            raise Exception('Invalid envinronment config \'{}\'. Expecting a name in {}'.format(config, validConfigs))
        self.config = config
        self.Log('debug', 'Using {} environment configuration'.format(self.config))
        bfm = None
        if self.config == 'ResistiveLoopback':
            if isinstance(loadValue, Number):
                bfm = hddpstbc.ResistiveLoadBfm(self.slot, self.subslot, loadValue)
            elif isinstance(loadValue, dict):
                bfm = hddpstbc.ResistiveLoadBfm(self.slot, self.subslot, defaultResistance)
                for rail, resistance in loadValue.items():
                    bfm.SetResistance(self.slot, self.subslot, rail, resistance)
        self.bfm = bfm
        self.hddps.model.hddpsSim[0].SetBfm(self.bfm)
        self.hddps.model.hddpsSim[1].SetBfm(self.bfm)

    def AlarmCheckers(self, railNum, fpgaBoard):
        sim = self.hddps.model.hddpsSim[self.subslot]
        alarmList = None
        if fpgaBoard == "HCLC":
            alarmList = ['GLOBAL_ALARMS', 'HclcSampleAlarms', 'HCLC_ALARMS', 'HclcRailAlarmsData']
        elif fpgaBoard == "VLC":
            alarmList = ['GLOBAL_ALARMS', 'VlcSampleAlarms', 'VlcRailAlarms', 'VlcRailAlarmsData']
        fpgaAlarms = self.CheckAlarms()
        for alarm in alarmList:
            if type(fpgaAlarms[
                        alarm]) is not list:  # checking fpga alarm data has list. List means that it's array structure of alarms like 'CfoldData' or 'RailAlarmsData'
                if (fpgaAlarms[alarm] == sim.GetHddpsAlarms(alarm, railNum)):  # railNum is dummy variable here
                    self.Log('debug', "Sim: HDDPS alarm {0} are matching with simulated {0} alarm".format(alarm))
                else:
                    self.Log('error', "Sim: HDDPS alarm {0} show mismatch with simulated {0} alarm".format(alarm))
                    self.AlarmBitComparison(fpgaAlarms[alarm],
                                            sim.GetHddpsAlarms(alarm, railNum))  # railNum is a dummy variable
            elif type(fpgaAlarms[alarm]) is list:
                self.ArrayAlarmBitComparison(fpgaAlarms[alarm], sim, alarm)

    def ArrayAlarmBitComparison(self, fpgaAlarmList, simulatedInstance, alarmName):
        sim = simulatedInstance
        for railNum, fpgaAlarm in enumerate(fpgaAlarmList):
            self.Log('debug', "HDDPS sim alarm {0} for rail {1} is {2}".format(alarmName, railNum,
                                                                               sim.GetHddpsAlarms(alarmName, railNum)))
            if (fpgaAlarm != sim.GetHddpsAlarms(alarmName, railNum)):
                self.Log('error',
                         "Sim: HDDPS alarm {0} rail {1} are mismatching with simulated {0} rail {1} alarm".format(
                             alarmName, railNum))
                self.AlarmBitComparison(fpgaAlarm, sim.GetHddpsAlarms(alarmName, railNum))
            else:
                self.Log('debug',
                         "HDDPS alarm {0} {1} are matching with simulated {0} {1} alarm".format(alarmName, railNum))

    def AlarmBitComparison(self, fpgaAlarmValue, simAlarmValue):  # Finding the mismatching alarm bits.
        fpgaBinaryAlarmValue = reversed(
            '{0:032b}'.format(fpgaAlarmValue))  # reversed value to start comparison from digit 0.
        simBinaryAlarmValue = reversed('{0:032b}'.format(simAlarmValue))
        for idx, (fpgaBit, simBit) in enumerate(
                zip(fpgaBinaryAlarmValue, simBinaryAlarmValue)):  # 32 are from total 32 bits
            if (fpgaBit != simBit):
                self.Log('error',
                         'Sim: Simulated alarm bit is {} while Fpga alarm bit {} for Bit {}'.format(simBit, fpgaBit,
                                                                                                    idx))

    def RunCheckers(self, dutid, bfmConfig, bfmLoad, fpgaBoard="HCLC", maxCaptureErrors=20):

        # Inserting time delay to avoid missing header end
        # It looks like python executes faster than generating header end.
        time.sleep(2)
        # Setting up Simulator BFM, might be need to spin off as seperate command later
        self.SetBfmConfig(bfmConfig, bfmLoad)
        # Read sample header and data
        header = self.ReadSampleHeader(dutid, fpgaBoard)
        sampleRegionSize = self.FindSampleRegionSize(dutid, fpgaBoard)
        if (sampleRegionSize == 0):
            self.Log('info', 'Sample Region Size is set to 0, so simulator is not running')
            return
        data = self.ReadSampleData(self.slot, self.subslot, dutid, fpgaBoard)
        # Create sample region structure for checker
        region = hddpstbc.SampleEngineRegion()
        region.SetHeader(header)
        region.SetData(data)
        region.SetSize(sampleRegionSize)
        # Create checker context
        context = hddpstbc.SampleCheckerContext()
        context.AddRegion(dutid, region)
        checker = hddpstbc.SampleChecker(maxCaptureErrors)
        self.Log('info', "Checker is running for Uhc or Dut Id {}".format(dutid))
        # checker.Run(context, hddpsSims[slot].hddpsSim[subslot])
        sim = self.hddps.model.hddpsSim[self.subslot]
        checker.Run(context, sim)
        # Checking Alarm and needs to be placed at the end of runchecker. Simulated alarms generated after going through TQ.
        self.AlarmCheckers(dutid,
                           fpgaBoard)  # Here dutid is a dummy variable placement for C++ function GetHddpsAlarms.
        passed = checker.totalErrorsFound == 0
        del checker
        # del hddpsSims
        return passed

    def MultiSampleEngineRunCheckers(self, dutIdList, bfmConfig, bfmLoad, fpgaBoard="HCLC",
                                     maxCaptureErrors=20):
        for dutId in dutIdList:
            self.RunCheckers(dutId, bfmConfig, bfmLoad, fpgaBoard, maxCaptureErrors)

    def getResetsRegister(self):
        return self.registers.RESETS

    def getStatusRegister(self):
        return self.registers.STATUS

    def getTriggerUpTestRegister(self):
        return self.registers.TRIGGERUPTEST

    def getTriggerDownTestRegister(self):
        return self.registers.TRIGGERDOWNTEST

    def getHcLcPerRailAlarmRegister(self):
        return self.registers.HCLC_PER_RAIL_ALARMS

    def getHcLcPerRailControlledFoldDetailsRegister(self):
        return self.registers.HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS

    def getHcLcControlledFoldAlarmsRegister(self):
        return self.registers.HclcCfoldAlarms

    def getVlcPerRailControlledFoldDetailsRegister(self):
        return self.registers.VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS

    def clearPerRailHclcAlarmsRegister(self):
        hclc_per_rail_register = self.getHcLcPerRailAlarmRegister()
        for rail in range(self.numberofrails):
            self.WriteRegister(hclc_per_rail_register(value=0xffffffff), index=rail)

    def clearHclcRailsFoldedRegister(self):
        hclc_rails_folded = self.registers.HclcRailsFolded
        self.WriteRegister(hclc_rails_folded(value=0x3ff))

    def clearGlobalAlarmRegister(self):
        global_alarm_register = self.registers.GLOBAL_ALARMS
        self.WriteRegister(global_alarm_register(value=0xffffffff))

    def clearHclcCfoldAlarmsRegister(self):
        HclcCfoldAlarmsRegister = self.registers.HclcCfoldAlarms
        self.WriteRegister(HclcCfoldAlarmsRegister(value=0x3ff))

    def clearHclcSampleAlarmRegister(self):
        hclc_sample_alarm_register = self.registers.HclcSampleAlarms
        self.WriteRegister(hclc_sample_alarm_register(value=0xffffffff))

    def ClearVlcRailAlarm(self):
        for rail in range(self.numberofvlcrails):
            vlcrail_register_name = 'Vlc{:02d}RailAlarms'.format(rail)
            vlcrailregister = getattr(self.registers, vlcrail_register_name)
            self.WriteRegister(vlcrailregister(value=0xFFFFFFFF))

    def ClearVlcSampleAlarms(self):
        vlc_sample_alarm_register = self.registers.VlcSampleAlarms
        self.WriteRegister(vlc_sample_alarm_register(value=0xFFFFFFFF))

    def ClearVlcCfoldAlarms(self):
        vlcCfoldAlarmsRegister = self.registers.VlcCfoldAlarms
        self.WriteRegister(vlcCfoldAlarmsRegister(value=0xffff))

    def EnableAlarms(self):
        enable_alarm_register = self.registers.ENABLE_ALARMS()
        enable_alarm_register.AlarmHandler = 1
        self.WriteRegister(enable_alarm_register)

    def getDutDomainIdRegisterType(self):
        return self.registers.DUT_DOMAIN_ID

    def getSafeStateRegister(self):
        return self.registers.SET_SAFE_STATE

    def CheckAlarmRegisters(self):
        global_alarm = self.ReadObject('GLOBAL_ALARMS').value
        self.Log('info', 'GlobalAlarms register value {}'.format(global_alarm))
        if global_alarm != 0x0:
            self.Log('error', 'Global Alarms Register is not cleared, value is set to 0x{:x}'.format(global_alarm))
        else:
            self.Log('debug', 'GlobalAlarms register is cleared')

    def getGlobalAlarmRegisterType(self):
        return self.registers.GLOBAL_ALARMS

    def initialize_hddps_rail_for_test(self, dutid, rail, resource_id, dutdomainId = 15, calBoardLoad ='NONE'):
        self.SetRailsToSafeState()
        self.ConnectCalBoard('{}{}'.format(resource_id,rail), calBoardLoad)
        self.ClearDpsAlarms()
        self.EnableAlarms()
        self.EnableOnlyOneUhc(dutid,dutdomainId)
        self.ConfigureUhcRail(dutid, 0x1 << rail,resource_id)
        self.UnGangAllRails()

    def getUhcRailConfigRegister(self,rail_type):
        if rail_type == 'HC' or rail_type =='LC':
            return self.registers.HclcDutIdRailConfig
        else:
            return self.registers.VlcUhcRailConfig

    def getHcLcDutRailConfigRegsiter(self):
        return self.registers.HclcDutIdRailConfig

    def getHclcRailModeValRegisterType(self):
        return self.registers.HclcRailModeVal

    def getVlcRailModeValRegisterType(self):
        return self.registers.VlcRailModeVal

    def getSampleHeaderRegionBaseRegister(self, rail_type):
        if rail_type == 'VLC':
            return self.registers.VlcUhcSampleHeaderRegionBase
        else:
            return self.registers.HclcUhcSampleHeaderRegionBase

    def getSampleHeaderRegionSizeRegister(self, rail_type):
        if rail_type == 'VLC':
            return self.registers.VlcUhcSampleHeaderRegionSize
        else:
            return self.registers.HclcUhcSampleHeaderRegionSize

    def getSampleRegionBaseRegister(self, rail_type):
        if rail_type == 'VLC':
            return self.registers.VlcUhcSampleRegionBase
        else:
            return self.registers.HclcUhcSampleRegionBase

    def getSampleRegionSizeRegister(self, rail_type):
        if rail_type == 'VLC':
            return self.registers.VlcUhcSampleRegionSize
        else:
            return self.registers.HclcUhcSampleRegionSize

    def getSampleEngineResetRegister(self, rail_type):
        if rail_type == 'VLC':
            return self.registers.VlcSampleEngineReset
        else:
            return self.registers.HclcSampleEngineReset

    def ReadAd5764Register(self, DAC_INDEX, REGISTER_VALUE, DAC_CHANNEL):
        return hil.hddpsAd5764Read(self.slot,self.subslot,DAC_INDEX,REGISTER_VALUE,DAC_CHANNEL)

    def WritetoAd5764(self,DAC_INDEX,REGISTER_OFFSET,DAC_CHANNEL,data):
        return hil.hddpsAd5764Write(self.slot,self.subslot,DAC_INDEX,REGISTER_OFFSET,DAC_CHANNEL,data)

    def get_dac_mapping(self):
        if self.subslot == 0:
            return self.hddps_mb_dac_mapping
        else:
            return self.hddps_db_dac_mapping

    def getExecuteTriggerRegister(self):
        return self.registers.ExecuteTrigger

    def get_rail_voltage(self,rail,rail_type):
        if rail_type == 'VLC':
            rail_voltage_regsiter = 'VlcRail' + str(rail) + 'V'
        else:
            rail_voltage_regsiter = 'DpsRail' + str(rail) + 'V'
        rail_voltage = self.ReadRegister(getattr(self.registers, rail_voltage_regsiter)).value
        rail_voltage = struct.unpack('f', struct.pack('I', rail_voltage))[0]
        return rail_voltage

    def get_rail_current(self,rail,rail_type):
        if rail_type == 'VLC':
            rail_current_regsiter_type = 'VlcRail' + str(rail) + 'I'
        else:
            rail_current_regsiter_type = 'DpsRail' + str(rail) + 'I'
        rail_current_from_bar1 = self.ReadRegister((getattr(self.registers, rail_current_regsiter_type))).value
        converted_current = struct.unpack('f', struct.pack('I', rail_current_from_bar1))[0]
        return converted_current

    def getBar2RailCommandRegister(self):
        return self.registers.Bar2RailCommand

    def cal_load_connect(self,load,calboard):
        calboard.selectLoad(load)
        calboard.apply()

    def cal_connect_force_sense_lines(self,rail,rail_type,calboard):
        if rail_type == 'VLC':
            if rail< 10:
                channel = '{}{}{}'.format(rail_type,0,rail)
            else:
                channel = '{}{}'.format(rail_type, rail)
        else:
            channel = '{}{}'.format(rail_type,rail)
        calboard.selectChannel(channel)
        calboard.apply()

    def cal_disconnect_force_sense_lines(self,rail,rail_type,calboard):
        calboard.selectChannel('NONE')
        calboard.apply()

    def create_cal_board_instance_and_initialize(self):
        calboard = hddps_cal_board(self.slot)
        calboard.init()
        return calboard

    def voltage_to_ad5560_raw_dac_code(self, voltage):
        offset_dacx_voltage = AD5560_V_CALC_SCALER * AD550_V_REF * AD5560_OFFSET_DAC_CODE / AD5560_DAC_SCALE
        raw_dac_code = int(AD5560_DAC_SCALE * (voltage + offset_dacx_voltage) / (AD5560_V_CALC_SCALER * AD550_V_REF))
        if raw_dac_code < 0:
            raw_dac_code = 0
        elif raw_dac_code > 0xFFFF:
            raw_dac_code = 0xFFFF
        return raw_dac_code

    def i_clamp_current_to_ad5560_raw_dac_code(self, current, irange):
        associated_rsense = 'R_SENSE_{}'.format(irange)
        r_sense = getattr(symbols.AD5560RSENSE, associated_rsense)
        r_sense_gain = r_sense * AD5560_MI_AMP_GAIN
        scaled_vref = AD5560_V_CALC_SCALER * AD550_V_REF
        r_sense_gain_scaled_vref = scaled_vref / r_sense_gain
        iclamp_plus_vref = current + (r_sense_gain_scaled_vref * .5)
        clamp_dac = int((iclamp_plus_vref * AD5560_DAC_SCALE) / r_sense_gain_scaled_vref)
        self.is_raw_conversion_in_expected_range(clamp_dac, current)
        return clamp_dac

    def is_raw_conversion_in_expected_range(self, clamp_dac, current):
        if (clamp_dac > 0xFFFF):
            self.Log('error', 'IClamp to DAC conversion out of range')
        if current < 0 and clamp_dac > 0x7FFF:
            self.Log('error', 'Invalid negative IClamp to DAC conversion')
        if current > 0 and clamp_dac < 0x8000:
            self.Log('error', 'Invalid positive IClamp to DAC conversion')

    def conversion_to_ad5764daccode(self,current,irange,rail_type):
        if rail_type == 'LC':
            AD5560_V_REF= 5.0
        else:
            AD5560_V_REF = 2.5
        associated_rsense = 'R_SENSE_{}'.format(irange)
        r_sense = getattr(symbols.AD5560RSENSE, associated_rsense)
        vrsense= current *r_sense
        vdac = (vrsense*AD5560_MI_AMP_GAIN)+2
        dacCode = round((vdac *32768)/(2*AD5560_V_REF))
        return dacCode

    def empty_trigger_buffer_and_ddr(self):
        initialize_to_zero = 0

        self.empty_trigger_buffer()
        self.empty_ddr_for_uhc_zero()
        self.DisableAllUhcs()
        self.WriteRegister(self.getResetsRegister()(value=initialize_to_zero))

    def reset_aurora(self):
        self.WriteRegister(self.getResetsRegister()(AuroraReset=1))

    def log_aurora_status(self, log_level='info'):
        reg = self.ReadObject('STATUS')
        self.Log(log_level, f'{self.name()} (aurora_status): 0x{reg.Pack():08X}')

    def log_aurora_error_counts(self, log_level='info'):
        reg = self.ReadObject('AURORA_ERROR_COUNTS')
        self.Log(log_level, f'{self.name()} (error_counts): 0x{reg.Pack():08X}')

    def reset_trigger_errors(self):
        reg = self.ReadObject('STATUS')
        reg.AuroraHardError = 1
        reg.AuroraSoftError = 1
        self.Write('STATUS', reg)

    def clear_trigger_errors(self, num_retries=50, force=False):
        if not self.AuroraIsReady() or force:
            for retry in range(num_retries):
                self.reset_trigger_errors()
                time.sleep(0.1)
                if self.AuroraIsReady():
                    self.Log('info', f'{self.name()}: Cleared trigger errors '
                                     f'in {retry} out of {num_retries} tries')
                    break
            else:
                self.Log('warning',
                         f'{self.name()}: Unable to clear trigger errors')
        self.log_aurora_status(log_level='info')
