################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
from Common import fval
from Common import hilmon as hil

class hddps_cal_board_v04:

    version = 0.4

    def __init__(self,slot):
        self.h = 0
        self.h = hil.cypDeviceOpen(hil.CYP_VIDPID_HDDPS_CAL, slot, None)
        
        
    def __del__(self):
        if (self.h):
            hil.cypDeviceClose(self.h)
        
     
    #------------------------------------------------------------------------------------------------------------------
    def setChannelDefaults(self):

        # array index        0     1     2     3     4     5
        # pca9505 port       8     9     a     b     c
        self.cached_0x40 = [0x88, 0xff, 0xff, 0xff, 0xff, 0xff]
        self.cached_0x48 = [0x88, 0xff, 0xff, 0xff, 0xff, 0xff]
   
     
    #------------------------------------------------------------------------------------------------------------------
    def setLoadDefaults(self): 
     
        # array index        0     1     2     3     4     5
        # pca9505 port       8     9     a     b     c
        self.cached_0x44 = [0x88, 0xff, 0xff, 0xff, 0xff, 0xff]
          
     
    #------------------------------------------------------------------------------------------------------------------
    def getVersion(self):
        return self.version


    #------------------------------------------------------------------------------------------------------------------
    def init(self):

        hil.cypI2cWrite(self.h, 0x40, b'\x90\x00\x00\x00\x00\x00')      # no polarity inversion
        hil.cypI2cWrite(self.h, 0x40, b'\x98\x00\x00\x00\x00\x00')      # ios are outputs
        hil.cypI2cWrite(self.h, 0x44, b'\x90\x00\x00\x00\x00\x00')      # no polarity inversion
        hil.cypI2cWrite(self.h, 0x44, b'\x98\x00\x00\x00\x00\x00')      # ios are outputs
        hil.cypI2cWrite(self.h, 0x48, b'\x90\x00\x00\x00\x00\x00')      # no polarity inversion
        hil.cypI2cWrite(self.h, 0x48, b'\x98\x00\x00\x00\x00\x00')      # ios are outputs
    
        self.setLoadDefaults();
        self.setChannelDefaults();
    
        self.apply()

    #------------------------------------------------------------------------------------------------------------------
    def apply(self):

        self.enableDMMSense()
    
        hil.cypI2cWrite(self.h, 0x40, bytes(self.cached_0x40))                      # output values
        hil.cypI2cWrite(self.h, 0x44, bytes(self.cached_0x44))                      # output values
        hil.cypI2cWrite(self.h, 0x48, bytes(self.cached_0x48))                      # output values
        time.sleep(0.1)


    #------------------------------------------------------------------------------------------------------------------
    def enableDMMSense(self):
        
        self.cached_0x44[5] &= 0xdf

        
    #------------------------------------------------------------------------------------------------------------------
    def dumpRawConfig(self):
        for dev in range(3):
            devWrAddr = 0x40 + dev * 4
            devRdAddr = 0x41 + dev * 4
            print("0x{:x}  ".format(devWrAddr), end = "")
            hil.cypI2cWrite(self.h, devWrAddr, b'\x88')
            print("  outputs:  ", end = "")
            for i in hil.cypI2cRead(self.h, devRdAddr, 5):
                print("{:02x} ".format(i), end = "")
            print()

            
    #------------------------------------------------------------------------------------------------------------------
    def selectChannel(self, ch):

        ch = ch.lower()

        # devices 0x40 (sense) and 0x48 (force), bank 0
        
        if (ch == 'vlc00'):
            self.cached_0x40[1] &= 0xfe            
            self.cached_0x48[1] &= 0xfe            
        elif (ch == 'vlc01'):
            self.cached_0x40[1] &= 0xfd            
            self.cached_0x48[1] &= 0xfd            
        elif (ch == 'vlc02'):
            self.cached_0x40[1] &= 0xfb            
            self.cached_0x48[1] &= 0xfb            
        elif (ch == 'vlc03'):
            self.cached_0x40[1] &= 0xf7            
            self.cached_0x48[1] &= 0xf7            
        elif (ch == 'vlc04'):
            self.cached_0x40[1] &= 0xef            
            self.cached_0x48[1] &= 0xef            
        elif (ch == 'vlc05'):
            self.cached_0x40[1] &= 0xdf            
            self.cached_0x48[1] &= 0xdf            
        elif (ch == 'vlc06'):
            self.cached_0x40[1] &= 0xbf            
            self.cached_0x48[1] &= 0xbf            
        elif (ch == 'vlc07'):
            self.cached_0x40[1] &= 0x7f            
            self.cached_0x48[1] &= 0x7f            

        # devices 0x40 (sense) and 0x48 (force), bank 1
        elif (ch == 'vlc08'):
            self.cached_0x40[2] &= 0xfe            
            self.cached_0x48[2] &= 0xfe            
        elif (ch == 'vlc09'):
            self.cached_0x40[2] &= 0xfd            
            self.cached_0x48[2] &= 0xfd            
        elif (ch == 'vlc10'):
            self.cached_0x40[2] &= 0xfb            
            self.cached_0x48[2] &= 0xfb            
        elif (ch == 'vlc11'):
            self.cached_0x40[2] &= 0xf7            
            self.cached_0x48[2] &= 0xf7            
        elif (ch == 'vlc12'):
            self.cached_0x40[2] &= 0xef            
            self.cached_0x48[2] &= 0xef            
        elif (ch == 'vlc13'):
            self.cached_0x40[2] &= 0xdf            
            self.cached_0x48[2] &= 0xdf            
        elif (ch == 'vlc14'):
            self.cached_0x40[2] &= 0xbf            
            self.cached_0x48[2] &= 0xbf            
        elif (ch == 'vlc15'):
            self.cached_0x40[2] &= 0x7f            
            self.cached_0x48[2] &= 0x7f            

        # devices 0x40 (sense) and 0x48 (force), bank 2
        elif (ch == 'lc0'):
            self.cached_0x40[3] &= 0xfe            
            self.cached_0x48[3] &= 0xfe            
        elif (ch == 'lc1'):
            self.cached_0x40[3] &= 0xfd            
            self.cached_0x48[3] &= 0xfd            
        elif (ch == 'lc2'):
            self.cached_0x40[3] &= 0xfb            
            self.cached_0x48[3] &= 0xfb            
        elif (ch == 'lc3'):
            self.cached_0x40[3] &= 0xf7            
            self.cached_0x48[3] &= 0xf7            
        elif (ch == 'lc4'):
            self.cached_0x40[3] &= 0xef            
            self.cached_0x48[3] &= 0xef            
        elif (ch == 'lc5'):
            self.cached_0x40[3] &= 0xdf            
            self.cached_0x48[3] &= 0xdf            
        elif (ch == 'lc6'):
            self.cached_0x40[3] &= 0xbf            
            self.cached_0x48[3] &= 0xbf            
        elif (ch == 'lc7'):
            self.cached_0x40[3] &= 0x7f            
            self.cached_0x48[3] &= 0x7f            

        # devices 0x40 (sense) and 0x48 (force), bank 3
        elif (ch == 'lc8'):
            self.cached_0x40[4] &= 0xfe            
            self.cached_0x48[4] &= 0xfe            
        elif (ch == 'lc9'):
            self.cached_0x40[4] &= 0xfd            
            self.cached_0x48[4] &= 0xfd            
        elif (ch == 'hc0'):
            self.cached_0x40[4] &= 0xfb            
            self.cached_0x48[4] &= 0xfb            
        elif (ch == 'hc1'):
            self.cached_0x40[4] &= 0xf7            
            self.cached_0x48[4] &= 0xf7            
        elif (ch == 'hc2'):
            self.cached_0x40[4] &= 0xef            
            self.cached_0x48[4] &= 0xef            
        elif (ch == 'hc3'):
            self.cached_0x40[4] &= 0xdf            
            self.cached_0x48[4] &= 0xdf            
        elif (ch == 'hc4'):
            self.cached_0x40[4] &= 0xbf            
            self.cached_0x48[4] &= 0xbf            
        elif (ch == 'hc5'):
            self.cached_0x40[4] &= 0x7f            
            self.cached_0x48[4] &= 0x7f            

        # devices 0x40 (sense) and 048 (force), bank 4
        elif (ch == 'hc6'):
            self.cached_0x40[5] &= 0xfe            
            self.cached_0x48[5] &= 0xfe            
        elif (ch == 'hc7'):
            self.cached_0x40[5] &= 0xfd            
            self.cached_0x48[5] &= 0xfd            
        elif (ch == 'hc8'):
            self.cached_0x40[5] &= 0xfb            
            self.cached_0x48[5] &= 0xfb            
        elif (ch == 'hc9'):
            self.cached_0x40[5] &= 0xf7            
            self.cached_0x48[5] &= 0xf7            

        elif (ch == 'none'):
            self.setChannelDefaults()
                        
        else:
            assert (0), "unknown channel"


    #------------------------------------------------------------------------------------------------------------------
    def selectLoad(self, load):

        load = load.lower()

        if (load == 'ohm_0_025'):
            self.cached_0x44[2] &= 0xef
            self.cached_0x44[4] &= 0xfc
        elif (load == 'ohm_0_05'):
            self.cached_0x44[2] &= 0xdf
            self.cached_0x44[4] &= 0xfb
        elif (load == 'ohm_0_25'):
            self.cached_0x44[2] &= 0xbf
            self.cached_0x44[4] &= 0xf7
        elif (load == 'ohm_0_8'):
            self.cached_0x44[2] &= 0x7f
            self.cached_0x44[4] &= 0xef
        elif (load == 'ohm_4'):
            self.cached_0x44[3] &= 0xfe
            self.cached_0x44[4] &= 0xdf
        elif (load == 'ohm_10'):
            self.cached_0x44[3] &= 0xfd
            self.cached_0x44[4] &= 0xbf
        elif (load == 'ohm_100'):
            self.cached_0x44[3] &= 0xfb
            self.cached_0x44[4] &= 0x7f
        elif (load == 'ohm_1k'):
            self.cached_0x44[3] &= 0xf7
            self.cached_0x44[5] &= 0xfe
        elif (load == 'ohm_10k'):
            self.cached_0x44[3] &= 0xef
            self.cached_0x44[5] &= 0xfd
        elif (load == 'ohm_100k'):
            self.cached_0x44[3] &= 0xdf
            self.cached_0x44[5] &= 0xfb
        elif (load == 'ohm_500k'):
            self.cached_0x44[3] &= 0xbf
            self.cached_0x44[5] &= 0xf7
        elif (load == 'ohm_1m'):
            self.cached_0x44[3] &= 0x7f
            self.cached_0x44[5] &= 0xef
            
        elif (load == 'none'):
            self.setLoadDefaults()

        else:
            assert (0), "unknown load"

    