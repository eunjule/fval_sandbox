################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time

from Common import hilmon as hil


class hddps_cal_board:

    version = 0.3


    def __init__(self,slot):
        self.h = 0
        self.h = hil.cypDeviceOpen(hil.CYP_VIDPID_HDDPS_CAL, slot, None)
        self.slot = slot
        # hil.calHddpsConnect(self.slot)
        # array index                       0   1   2   3   4   5
        # pca9505 port                          8   9   a   b   c
        self.cached_0x40_default = bytearray(b"\x88\xff\xff\xff\xff\xff")
        self.cached_0x44_default = bytearray(b"\x88\xff\xff\xff\xff\x5f")
        self.cached_0x48_default = bytearray(b"\x88\xff\xff\xff\xff\xff")

        self.cached_0x40 = self.cached_0x40_default
        self.cached_0x44 = self.cached_0x44_default
        self.cached_0x48 = self.cached_0x48_default

    def __del__(self):
        # hil.calHddpsDisconnect(self.slot)
        if (self.h):
            hil.cypDeviceClose(self.h)
        
     
    #------------------------------------------------------------------------------------------------------------------
    def getVersion(self):
        return version


    #------------------------------------------------------------------------------------------------------------------
    def init(self):

        hil.calHddpsInit(self.slot)



    #------------------------------------------------------------------------------------------------------------------
    def apply(self):

        hil.cypI2cWrite(self.h, 0x40, bytes(self.cached_0x40))                      # output values
        hil.cypI2cWrite(self.h, 0x44, bytes(self.cached_0x44))                      # output values
        hil.cypI2cWrite(self.h, 0x48, bytes(self.cached_0x48))                      # output values
        time.sleep(0.1)

    #------------------------------------------------------------------------------------------------------------------
    def dumpRawConfig(self):
        for dev in range(3):
            devWrAddr = 0x40 + dev * 4
            devRdAddr = 0x41 + dev * 4
            print("0x{:x}".format(devWrAddr))
            #hil.cypI2cWrite(self.h, devWrAddr, b'\x80')
            #print("  inputs {}".format(hil.cypI2cRead(self.h, devRdAddr, 5)))
            hil.cypI2cWrite(self.h, devWrAddr, b'\x88')
            print("  outputs {}".format(hil.cypI2cRead(self.h, devRdAddr, 5)))
            #hil.cypI2cWrite(self.h, devWrAddr, b'\x90')
            #print("  invert {}".format(hil.cypI2cRead(self.h, devRdAddr, 5)))
            #hil.cypI2cWrite(self.h, devWrAddr, b'\x98')
            #print("  I/O {}".format(hil.cypI2cRead(self.h, devRdAddr, 5)))

    
    
    #------------------------------------------------------------------------------------------------------------------
    def selectChannel(self, ch):

        # devices 0x40 (sense) and 0x48 (force), bank 0
        
        if (ch == 'VLC00'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xfe            
            self.cached_0x48[1] = 0xfe            
        elif (ch == 'VLC01'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xfd            
            self.cached_0x48[1] = 0xfd            
        elif (ch == 'VLC02'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xfb            
            self.cached_0x48[1] = 0xfb            
        elif (ch == 'VLC03'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xf7
            self.cached_0x48[1] = 0xf7
        elif (ch == 'VLC04'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xef            
            self.cached_0x48[1] = 0xef
        elif (ch == 'VLC05'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xdf            
            self.cached_0x48[1] = 0xdf
        elif (ch == 'VLC06'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0xbf            
            self.cached_0x48[1] = 0xbf            
        elif (ch == 'VLC07'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[1] = 0x7f            
            self.cached_0x48[1] = 0x7f

        # devices 0x40 (sense) and 0x48 (force), bank 1
        elif (ch == 'VLC08'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xfe
            self.cached_0x48[2] = 0xfe            
        elif (ch == 'VLC09'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xfd            
            self.cached_0x48[2] = 0xfd            
        elif (ch == 'VLC10'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xfb            
            self.cached_0x48[2] = 0xfb            
        elif (ch == 'VLC11'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xf7
            self.cached_0x48[2] = 0xf7            
        elif (ch == 'VLC12'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xef            
            self.cached_0x48[2] = 0xef
        elif (ch == 'VLC13'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xdf
            self.cached_0x48[2] = 0xdf
        elif (ch == 'VLC14'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0xbf
            self.cached_0x48[2] = 0xbf
        elif (ch == 'VLC15'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[2] = 0x7f            
            self.cached_0x48[2] = 0x7f

        # devices 0x40 (sense) and 0x48 (force), bank 2
        elif (ch == 'LC0'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xfe            
            self.cached_0x48[3] = 0xfe            
        elif (ch == 'LC1'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xfd            
            self.cached_0x48[3] = 0xfd            
        elif (ch == 'LC2'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xfb            
            self.cached_0x48[3] = 0xfb            
        elif (ch == 'LC3'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xf7            
            self.cached_0x48[3] = 0xf7            
        elif (ch == 'LC4'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xef            
            self.cached_0x48[3] = 0xef            
        elif (ch == 'LC5'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xdf            
            self.cached_0x48[3] = 0xdf            
        elif (ch == 'LC6'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0xbf            
            self.cached_0x48[3] = 0xbf            
        elif (ch == 'LC7'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[3] = 0x7f            
            self.cached_0x48[3] = 0x7f            

        # devices 0x40 (sense) and 0x48 (force), bank 3
        elif (ch == 'LC8'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xfe            
            self.cached_0x48[4] = 0xfe            
        elif (ch == 'LC9'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xfd            
            self.cached_0x48[4] = 0xfd            
        elif (ch == 'HC0'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xfb            
            self.cached_0x48[4] = 0xfb            
        elif (ch == 'HC1'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xf7            
            self.cached_0x48[4] = 0xf7            
        elif (ch == 'HC2'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xef            
            self.cached_0x48[4] = 0xef            
        elif (ch == 'HC3'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xdf            
            self.cached_0x48[4] = 0xdf            
        elif (ch == 'HC4'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0xbf            
            self.cached_0x48[4] = 0xbf            
        elif (ch == 'HC5'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[4] = 0x7f            
            self.cached_0x48[4] = 0x7f            

        # devices 0x40 (sense) and 048 (force), bank 4
        elif (ch == 'HC6'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[5] = 0xfe            
            self.cached_0x48[5] = 0xfe            
        elif (ch == 'HC7'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[5] = 0xfd            
            self.cached_0x48[5] = 0xfd            
        elif (ch == 'HC8'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[5] = 0xfb            
            self.cached_0x48[5] = 0xfb            
        elif (ch == 'HC9'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
            self.cached_0x40[5] = 0xf7            
            self.cached_0x48[5] = 0xf7            

        elif (ch == 'none' or ch == 'NONE'):
            self.cached_0x40 = self.cached_0x40_default
            self.cached_0x48 = self.cached_0x48_default
                        
        else:
            assert (0), "unknown channel"


    #------------------------------------------------------------------------------------------------------------------
    def selectLoad(self, load):

        if (load == 'OHM_0_025'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[2] = 0xef
            self.cached_0x44[4] = 0xfd
        elif (load == 'OHM_0_05'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[2] = 0xdf
            self.cached_0x44[4] = 0xfb
        elif (load == 'OHM_0_25'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[2] = 0xbf
            self.cached_0x44[4] = 0xf7
        elif (load == 'OHM_0_8'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[2] = 0x7f
            self.cached_0x44[4] = 0xef
        elif (load == 'OHM_4'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xfe
            self.cached_0x44[4] = 0xdf
        elif (load == 'OHM_10'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xfd
            self.cached_0x44[4] = 0xbf
        elif (load == 'OHM_100'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xfb
            self.cached_0x44[4] = 0x7f
        elif (load == 'OHM_1K'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xf7
            self.cached_0x44[5] = 0x5e
        elif (load == 'OHM_10K'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xef
            self.cached_0x44[5] = 0x5d
        elif (load == 'OHM_100K'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xdf
            self.cached_0x44[5] = 0x5b
        elif (load == 'OHM_500K'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0xbf
            self.cached_0x44[5] = 0x57
        elif (load == 'OHM_1M'):
            self.cached_0x44 = self.cached_0x44_default
            self.cached_0x44[3] = 0x7f
            self.cached_0x44[5] = 0x4f

        elif (load == 'none' or load == 'NONE'):
            self.cached_0x44 = self.cached_0x44_default

        else:
            assert (0), "unknown load"

    
