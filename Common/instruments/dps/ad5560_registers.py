################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from ctypes import c_uint

import Common.register as register


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, AD5560Register):
                if attribute is not AD5560Register:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, AD5560Register):
                if attribute is not AD5560Register:
                    # if '__' not in attribute.__name__:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, AD5560Struct):
                if attribute is not AD5560Struct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, AD5560Struct):
                if attribute is not AD5560Struct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class AD5560Register(register.Register):
    BAR = 1

    def __init__(self, uint=0, length=16):
        super().__init__(value=uint)


class AD5560Struct(register.Register):
    ADDR = None

    def __init__(self, uint=0, length=16):
        super().__init__(value=uint)


module_dict = globals()


class Nop(AD5560Register):
    ADDR = 0
    _fields_ = [('Data', c_uint, 16)]


class SYSTEM_CONTROL(AD5560Register):
    ADDR = 1
    _fields_ = [('Reserved', c_uint, 7),
                ('ActiveLoad', c_uint, 2),
                ('ForceAmpPowerdown', c_uint, 1),
                ('ComparatorOutput', c_uint, 1),
                ('FinToGround', c_uint, 1),
                ('Gain', c_uint, 2),
                ('TemperatureLimit', c_uint, 2)]


class DPS_REGISTER1(AD5560Register):
    ADDR = 2
    _fields_ = [('ReservedBlock2', c_uint, 4),
                ('ClampEnable', c_uint, 1),
                ('MeasureOut', c_uint, 3),
                ('MeasureOutEnable', c_uint, 1),
                ('ComparatorFunction', c_uint, 2),
                ('CurrentRange', c_uint, 3),
                ('ReservedBlock1', c_uint, 1),
                ('ForceAmpEnable', c_uint, 1)
                 ]


class DPS_REGISTER2(AD5560Register):
    ADDR = 3
    _fields_ = [('Reserved', c_uint, 7),
                ('HighZGuard', c_uint, 1),
                ('INT10K', c_uint, 1),
                ('SlaveGangMode', c_uint, 2),
                ('GPO', c_uint, 1),
                ('SlewRateControl', c_uint, 3),
                ('SystemForceSense', c_uint, 1)]


class COMPENSATION1(AD5560Register):
    ADDR = 4
    _fields_ = [('Reserved', c_uint, 7),
                ('SafeMode', c_uint, 1),
                ('EquivalentSeriesResistance', c_uint, 4),
                ('CompensationResistanceDut', c_uint, 4)]



class COMPENSATION2(AD5560Register):
    ADDR = 5
    _fields_ = [('Reserved', c_uint, 1),
                ('CapOneTo6K', c_uint, 1),
                ('CapTwoTo25K', c_uint, 1),
                ('CapThreeTo100K', c_uint, 1),
                ('FeedForwardCapacitorSelect', c_uint, 3),
                ('TransconductanceForceAmp', c_uint, 2),
                ('ResistancePole', c_uint, 3),
                ('ResistanceZero', c_uint, 3),
                ('ManualCompensation', c_uint, 1)]


class ALARM_SETUP(AD5560Register):
    ADDR = 6
    _fields_ = [('Reserved', c_uint, 6),
                ('DisableGroundAlarm', c_uint, 1),
                ('LatchGroundAlarm', c_uint, 1),
                ('DisableClampAlarm', c_uint, 1),
                ('LatchClampAlarm', c_uint, 1),
                ('DisableDutAlarm', c_uint, 1),
                ('LatchDutAlarm', c_uint, 1),
                ('DisableOscillationAlarm', c_uint, 1),
                ('LatchOscillationAlarm', c_uint, 1),
                ('DisableTemperatureAlarm', c_uint, 1),
                ('LatchTemperatureAlarm', c_uint, 1)]


class DIAGNOSTIC(AD5560Register):
    ADDR = 7
    _fields_ = [('Data', c_uint, 16)]


class FIN_DAC_X1(AD5560Register):
    ADDR = 8
    _fields_ = [('Data', c_uint, 16)]


class FIN_DAC_M(AD5560Register):
    ADDR = 9
    _fields_ = [('Data', c_uint, 16)]


class FIN_DAC_C(AD5560Register):
    ADDR = 10
    _fields_ = [('Data', c_uint, 16)]


class OFFSET_DACX(AD5560Register):
    ADDR = 11
    _fields_ = [('Data', c_uint, 16)]


class OSD_DACX(AD5560Register):
    ADDR = 12
    _fields_ = [('Data', c_uint, 16)]


class CLL_DAC_X1(AD5560Register):
    ADDR = 13
    _fields_ = [('Data', c_uint, 16)]


class CLL_DAC_M(AD5560Register):
    ADDR = 14
    _fields_ = [('Data', c_uint, 16)]


class CLL_DAC_C(AD5560Register):
    ADDR = 15
    _fields_ = [('Data', c_uint, 16)]


class CLH_DAC_X1(AD5560Register):
    ADDR = 16
    _fields_ = [('Data', c_uint, 16)]


class CLH_DAC_M(AD5560Register):
    ADDR = 17
    _fields_ = [('Data', c_uint, 16)]


class CLH_DAC_C(AD5560Register):
    ADDR = 18
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_5uA(AD5560Register):
    ADDR = 19
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_5uA(AD5560Register):
    ADDR = 20
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_5uA(AD5560Register):
    ADDR = 21
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_25uA(AD5560Register):
    ADDR = 22
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_25uA(AD5560Register):
    ADDR = 23
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_25uA(AD5560Register):
    ADDR = 24
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_250uA(AD5560Register):
    ADDR = 25
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_250uA(AD5560Register):
    ADDR = 26
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_250uA(AD5560Register):
    ADDR = 27
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_2_5mA(AD5560Register):
    ADDR = 28
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_2_5mA(AD5560Register):
    ADDR = 29
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_2_5mA(AD5560Register):
    ADDR = 30
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_25mA(AD5560Register):
    ADDR = 31
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_25mA(AD5560Register):
    ADDR = 32
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_25mA(AD5560Register):
    ADDR = 33
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_EXT2(AD5560Register):
    ADDR = 34
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_EXT2(AD5560Register):
    ADDR = 35
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_EXT2(AD5560Register):
    ADDR = 36
    _fields_ = [('Data', c_uint, 16)]


class CPL_DACX1_EXT1(AD5560Register):
    ADDR = 37
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M_EXT1(AD5560Register):
    ADDR = 38
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C_EXT1(AD5560Register):
    ADDR = 39
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_5uA(AD5560Register):
    ADDR = 40
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_5uA(AD5560Register):
    ADDR = 41
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_5uA(AD5560Register):
    ADDR = 42
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_25uA(AD5560Register):
    ADDR = 43
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_25uA(AD5560Register):
    ADDR = 44
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_25uA(AD5560Register):
    ADDR = 45
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_250uA(AD5560Register):
    ADDR = 46
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_250uA(AD5560Register):
    ADDR = 47
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_250uA(AD5560Register):
    ADDR = 48
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_2_5mA(AD5560Register):
    ADDR = 49
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_2_5mA(AD5560Register):
    ADDR = 50
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_2_5mA(AD5560Register):
    ADDR = 51
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_25mA(AD5560Register):
    ADDR = 52
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_25mA(AD5560Register):
    ADDR = 53
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_25mA(AD5560Register):
    ADDR = 54
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_EXT2(AD5560Register):
    ADDR = 55
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_EXT2(AD5560Register):
    ADDR = 56
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_EXT2(AD5560Register):
    ADDR = 57
    _fields_ = [('Data', c_uint, 16)]


class CPH_DACX1_EXT1(AD5560Register):
    ADDR = 58
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M_EXT1(AD5560Register):
    ADDR = 59
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C_EXT1(AD5560Register):
    ADDR = 60
    _fields_ = [('Data', c_uint, 16)]


class DGS_DAC(AD5560Register):
    ADDR = 61
    _fields_ = [('Data', c_uint, 16)]


class RAMP_END_CODE(AD5560Register):
    ADDR = 62
    _fields_ = [('Data', c_uint, 16)]


class RAMP_STEP_SIZE(AD5560Register):
    ADDR = 63
    _fields_ = [('Data', c_uint, 16)]


class RCLK_DIVIDER(AD5560Register):
    ADDR = 64
    _fields_ = [('Data', c_uint, 16)]


class ENABLE_RAMP(AD5560Register):
    ADDR = 65
    _fields_ = [('Data', c_uint, 16)]


class INTERRUPT_RAMP(AD5560Register):
    ADDR = 66
    _fields_ = [('Data', c_uint, 16)]


class ALARM_STATUS(AD5560Register):
    ADDR = 67
    _fields_ = [('Data', c_uint, 16)]


class ALARM_CLEAR(AD5560Register):
    ADDR = 68
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_X1(AD5560Register):
    ADDR = 69
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_M(AD5560Register):
    ADDR = 70
    _fields_ = [('Data', c_uint, 16)]


class CPL_DAC_C(AD5560Register):
    ADDR = 71
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_X1(AD5560Register):
    ADDR = 72
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_M(AD5560Register):
    ADDR = 73
    _fields_ = [('Data', c_uint, 16)]


class CPH_DAC_C(AD5560Register):
    ADDR = 74
    _fields_ = [('Data', c_uint, 16)]


class END(AD5560Register):
    ADDR = 75
    _fields_ = [('Data', c_uint, 16)]


class Ad5560Command(AD5560Struct):
    BAR = 2
    _fields_ = [('RegisterAddress', c_uint, 8),
                ('DeviceCode', c_uint, 7),
                ('ReadWrite', c_uint, 1), ]
