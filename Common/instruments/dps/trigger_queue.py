################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


from Common import configs
from Common.instruments.dps import symbols
from _build.bin import hddpstbc

class TriggerQueueString():

    def __init__(self,board,uhc,dutid):

        self.board = board
        self.uhc = uhc
        self.dutid = dutid

        self.free_drive = False
        self.open_socket = True
        self.sample_force = False
        self.turbo_sample_force = False
        self.sample_sense = False
        self.turbo_sample_sense = False

        self._force_rail = None
        self._force_voltage = None
        self._tracking_voltage = None
        self._force_rail_type = None
        self._force_rail_irange = None
        self._force_clamp_low = None
        self._force_clamp_high = None
        self._force_low_voltage_limit = None
        self._force_high_voltage_limit = None

        if self.board.LegacyTcLooping:
            self._legacy_tc_looping = True
        else:
            self._legacy_tc_looping = False

        self._sense_rail_type = None
        self._sense_rail = None
        self._sense_low_voltage_limit = None
        self._sense_high_voltage_limit = None

        self._sample_delay = None
        self._sample_count = None
        self._sample_rate = None

        self._force_free_drive_high = None
        self._force_free_drive_low = None
        self._force_free_drive_delay = None
   
    @property
    def force_free_drive_delay(self):
        if self._force_free_drive_delay == None:
            raise Exception('sample rate not set')
        return self._force_free_drive_delay

    @force_free_drive_delay.setter
    def force_free_drive_delay(self,value):
        self._force_free_drive_delay = value

    @property
    def force_free_drive_low(self):
        if self._force_free_drive_low == None:
            raise Exception('sample rate not set')
        return self._force_free_drive_low

    @force_free_drive_low.setter
    def force_free_drive_low(self,value):
        self._force_free_drive_low = value

    @property
    def force_free_drive_high(self):
        if self._force_free_drive_high == None:
            raise Exception('sample rate not set')
        return self._force_free_drive_high

    @force_free_drive_high.setter
    def force_free_drive_high(self,value):
        self._force_free_drive_high = value

    @property
    def sample_rate(self):
        if self._sample_rate == None:
            raise Exception('sample rate not set')
        return self._sample_rate

    @sample_rate.setter
    def sample_rate(self,value):
        self._sample_rate = value

    @property
    def sample_count(self):
        if self._sample_count == None:
            raise Exception('sample count not set')
        return self._sample_count

    @sample_count.setter
    def sample_count(self,value):
        self._sample_count = value

    @property
    def sample_delay(self):
        if self._sample_delay == None:
            raise Exception('sample delay not set')
        return self._sample_delay

    @sample_delay.setter
    def sample_delay(self,value):
        self._sample_delay = value
        
    @property
    def sense_rail_type(self):
        if self._sense_rail_type == None:
            raise Exception('Sense rail type not set')
        return self._sense_rail_type

    @sense_rail_type.setter
    def sense_rail_type(self,value):
        self._sense_rail_type = value

    @property
    def sense_rail(self):
        return self._sense_rail

    @sense_rail.setter
    def sense_rail(self,value):
        self._sense_rail = value

    @property
    def sense_low_voltage_limit(self):
        if self._sense_low_voltage_limit == None:
            raise Exception('sense voltage low limit not set')
        return self._sense_low_voltage_limit

    @sense_low_voltage_limit.setter
    def sense_low_voltage_limit(self,value):
        self._sense_low_voltage_limit = value

    @property
    def sense_high_voltage_limit(self):
        if self._sense_high_voltage_limit == None:
            raise Exception('sense voltage high limit not set')
        return self._sense_high_voltage_limit

    @sense_high_voltage_limit.setter
    def sense_high_voltage_limit(self,value):
        self._sense_high_voltage_limit = value

    @property
    def force_low_voltage_limit(self):
        if self._force_low_voltage_limit == None:
            raise Exception('Force voltage low limit not set')
        return self._force_low_voltage_limit

    @force_low_voltage_limit.setter
    def force_low_voltage_limit(self,value):
        self._force_low_voltage_limit = value

    @property
    def force_high_voltage_limit(self):
        if self._force_high_voltage_limit == None:
            raise Exception('Force voltage high limit not set')
        return self._force_high_voltage_limit

    @force_high_voltage_limit.setter
    def force_high_voltage_limit(self,value):
        self._force_high_voltage_limit = value

    @property
    def force_clamp_low(self):
        if self._force_clamp_low == None:
            raise Exception('Force rail clamp low not set')
        return self._force_clamp_low

    @force_clamp_low.setter
    def force_clamp_low(self,value):
        self._force_clamp_low = value

    @property
    def force_clamp_high(self):
        if self._force_clamp_high == None:
            raise Exception('Force rail clamp high not set')
        return self._force_clamp_high

    @force_clamp_high.setter
    def force_clamp_high(self,value):
        self._force_clamp_high = value

    @property
    def force_rail(self):
        if self._force_rail == None:
            raise Exception('Force rail not set')
        return self._force_rail

    @force_rail.setter
    def force_rail(self,value):
        self._force_rail = value

    @property
    def force_rail_irange(self):
        if self._force_rail_irange == None:
            raise Exception('Force rail irange not set')
        return self._force_rail_irange

    @force_rail_irange.setter
    def force_rail_irange(self,value):
        self._force_rail_irange = value

    @property
    def force_rail_type(self):
        if self._force_rail_type == None:
            raise Exception('Force rail type not set')
        return self._force_rail_type

    @force_rail_type.setter
    def force_rail_type(self,value):
        self._force_rail_type = value

    @property
    def force_voltage(self):
        if self._force_voltage == None:
            raise Exception('Force voltage not set')
        return self._force_voltage

    @force_voltage.setter
    def force_voltage(self,value):
        self._force_voltage = value

    @property
    def tracking_voltage(self):
        if self._tracking_voltage == None:
            raise Exception('Tracking voltage not set')
        return self._tracking_voltage

    @tracking_voltage.setter
    def tracking_voltage(self, value):
        self._tracking_voltage = value


    def generateForceVoltageTriggerQueue(self):
        enable_local_sense = 1
        trigger_queue = ''

        force_trigger_queue_header = self.get_trigger_queue_header(self.force_rail,self.force_rail_type)
        trigger_queue = trigger_queue + force_trigger_queue_header

        if self.sense_rail != None:
            force_trigger_queue_header_lvm = self.get_trigger_queue_header(self.sense_rail, self.sense_rail_type)
            trigger_queue = trigger_queue + '\n' + force_trigger_queue_header_lvm


        if self.open_socket:
            enable_sense = self.get_trigger_queue_enable_disable_sense(self.force_rail, self.force_rail_type,enable_local_sense)
            trigger_queue = trigger_queue + '\n' + enable_sense

        hw_switch_string = self.get_hw_switch_enable(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + hw_switch_string

        if self.force_rail_type == 'HC' or self.force_rail_type == 'LC' or self.force_rail_type == 'HV':
            set_thermal_limit = 'Q cmd = SET_THERMAL_I_H_CLAMP, arg = {}, data = 0x{:x}'.format(self.force_rail, 0xFFFF)
            trigger_queue = trigger_queue + '\n' + set_thermal_limit

        flush_clamps = self.get_flush_clamps(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + flush_clamps

        current_range = self.get_current_range(self.force_rail, self.force_rail_type, self.force_rail_irange)
        trigger_queue = trigger_queue + '\n' + current_range

        auto_compensation = self.get_set_auto_compensation(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + auto_compensation

        if self.force_rail_type == 'HV':
            tracking_voltage = self.get_tracking_voltage_string(0, self.force_rail,
                                                                self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + tracking_voltage

        if self.sense_rail != None:
            disable_rail = self.get_disable_rail(self.sense_rail, self.sense_rail_type)
            trigger_queue = trigger_queue + '\n' + disable_rail

        disable_rail = self.get_disable_rail(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + disable_rail

        if self.free_drive == True:
            free_drive_current = self.get_free_drive_current_string(self.force_rail_irange,self.force_rail, self.force_rail_type, self.force_free_drive_low, self.force_free_drive_high)
            trigger_queue = trigger_queue + '\n' + free_drive_current

        current_clamps = self.get_current_clamp_limits_string(self.force_rail,self.force_rail_type, self.force_rail_irange,
                                                              self.force_clamp_low,self.force_clamp_high)
        trigger_queue = trigger_queue + '\n' + current_clamps

        test_mode = self.get_test_mode(self.force_rail, self.force_rail_type, 'VFORCE')
        trigger_queue = trigger_queue + '\n' + test_mode

        auto_compensation = self.get_set_auto_compensation(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + auto_compensation

        set_v_range = self.get_set_v_range(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + set_v_range

        auto_compensation = self.get_set_auto_compensation(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + auto_compensation

        soft_span_code = self.get_soft_span_code(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + soft_span_code

        if self.force_rail_type=='HV' or self.force_rail_type=='HC' or self.force_rail_type=='LC':
            voltage_limits = self.get_force_voltage_limits_string(self.force_rail, self.force_rail_type,
                                                                  self.force_low_voltage_limit, self.force_high_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        time_delay_1 = self.get_time_delay(self.force_rail, self.force_rail_type,1)
        trigger_queue = trigger_queue + '\n' + time_delay_1

        if self.force_rail_type == 'HV':
            tracking_voltage = self.get_tracking_voltage_string(0, self.force_rail,
                                                                self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + tracking_voltage

        force_voltage = self.get_force_voltage_string(0, self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + force_voltage

        supress_alarm = self.get_supress_alaram(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + supress_alarm

        if self.sense_rail != None:

            voltage_limits = self.get_sense_voltage_limits_string(self.sense_rail, self.sense_rail_type, self.sense_low_voltage_limit, self.sense_high_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        if self.sample_force:
            sample_engine_string = self.get_sample_engine(self.force_rail,self.force_rail_type, self.sample_delay, self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.sample_sense:
            sample_engine_string = self.get_sample_engine(self.sense_rail,self.sense_rail_type, self.sample_delay, self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.turbo_sample_force:
            sample_engine_string = self.get_turbo_sample_engine(self.force_rail,self.force_rail_type, self.sample_delay, self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.turbo_sample_sense:
            sample_engine_string = self.get_turbo_sample_engine(self.sense_rail,self.sense_rail_type, self.sample_delay, self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.sense_rail != None:
            enable_rail = self.get_enable_rail(self.sense_rail,self.sense_rail_type)
            trigger_queue = trigger_queue + '\n' + enable_rail

        if self.force_rail_type == 'HV':
            enable_rail = self.get_enable_rail(self.force_rail, self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + enable_rail

        assert_test_rail = self.get_assert_test_rail(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + assert_test_rail

        if self.force_rail_type == 'HV':
            tracking_voltage = self.get_tracking_voltage_string(self.tracking_voltage, self.force_rail,
                                                                self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + tracking_voltage

        if self.free_drive == True:
            free_drive_voltage = self.get_free_drive_string(self.force_voltage, self.force_rail, self.force_rail_type, self.force_free_drive_delay)
            trigger_queue = trigger_queue + '\n' + free_drive_voltage
        else:
            force_voltage = self.get_force_voltage_string(self.force_voltage, self.force_rail, self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + force_voltage
        if self.force_rail_type != 'HV':
            enable_rail = self.get_enable_rail(self.force_rail, self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + enable_rail

        time_delay = self.get_time_delay(self.force_rail,self.force_rail_type, 700)
        trigger_queue = trigger_queue + '\n' + time_delay

        force_trigger_queue_footer =  self.get_trigger_queue_footer(self.force_rail,self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + force_trigger_queue_footer

        if self.sense_rail != None:
            force_trigger_queue_footer_lvm = self.get_trigger_queue_footer(self.sense_rail, self.sense_rail_type)
            trigger_queue = trigger_queue + '\n' + force_trigger_queue_footer_lvm

        tq_complete = self.get_tq_complete()
        trigger_queue = trigger_queue + '\n' + tq_complete
        return trigger_queue

    def generateDutFoldPolicyTriggerQueue(self,fault_alarm= False):
        enable_local_sense = 1
        trigger_queue = ''

        force_trigger_queue_header = self.get_trigger_queue_header(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + force_trigger_queue_header

        force_trigger_queue_header_lvm = self.get_trigger_queue_header(self.sense_rail, self.sense_rail_type)
        trigger_queue = trigger_queue + '\n'+ force_trigger_queue_header_lvm

        hw_switch_string = self.get_hw_switch_enable(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + hw_switch_string

        if self.force_rail_type == 'HC' or self.force_rail_type == 'LC' or self.force_rail_type == 'HV':
            set_thermal_limit = 'Q cmd = SET_THERMAL_I_H_CLAMP, arg = {}, data = 0x{:x}'.format(self.force_rail, 0xFFFF)
            trigger_queue = trigger_queue + '\n' + set_thermal_limit

        flush_clamps = self.get_flush_clamps(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + flush_clamps

        current_range = self.get_current_range(self.force_rail, self.force_rail_type, self.force_rail_irange)
        trigger_queue = trigger_queue + '\n' + current_range

        auto_compensation = self.get_set_auto_compensation(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + auto_compensation

        if self.force_rail_type == 'HV':
            tracking_voltage = self.get_tracking_voltage_string(0, self.force_rail,
                                                                self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + tracking_voltage

        if self.open_socket:
            enable_sense = self.get_trigger_queue_enable_disable_sense(self.force_rail, self.force_rail_type,
                                                                       enable_local_sense)
            trigger_queue = trigger_queue + '\n' + enable_sense

        if self.sense_rail != None:
            disable_rail = self.get_disable_rail(self.sense_rail, self.sense_rail_type)
            trigger_queue = trigger_queue + '\n' + disable_rail

        disable_rail = self.get_disable_rail(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + disable_rail

        if self.free_drive == True:
            free_drive_current = self.get_free_drive_current_string(self.force_rail_irange,self.force_rail, self.force_rail_type,
                                                                    self.force_free_drive_low,
                                                                    self.force_free_drive_high)
            trigger_queue = trigger_queue + '\n' + free_drive_current

        current_clamps = self.get_current_clamp_limits_string(self.force_rail, self.force_rail_type,
                                                              self.force_rail_irange,
                                                              self.force_clamp_low, self.force_clamp_high)
        trigger_queue = trigger_queue + '\n' + current_clamps

        test_mode = self.get_test_mode(self.force_rail, self.force_rail_type, 'VFORCE')
        trigger_queue = trigger_queue + '\n' + test_mode

        auto_compensation = self.get_set_auto_compensation(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + auto_compensation

        set_v_range = self.get_set_v_range(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + set_v_range

        auto_compensation = self.get_set_auto_compensation(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + auto_compensation

        soft_span_code = self.get_soft_span_code(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + soft_span_code

        if self.force_rail_type == 'HV' or self.force_rail_type == 'HC' or self.force_rail_type == 'LC':
            voltage_limits = self.get_force_voltage_limits_string(self.force_rail, self.force_rail_type,
                                                                  self.force_low_voltage_limit,
                                                                  self.force_high_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        time_delay_1 = self.get_time_delay(self.force_rail, self.force_rail_type,1)
        trigger_queue = trigger_queue + '\n' + time_delay_1

        if self.force_rail_type == 'HV':
            tracking_voltage = self.get_tracking_voltage_string(0, self.force_rail,
                                                                self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + tracking_voltage

        force_voltage = self.get_force_voltage_string(0, self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + force_voltage

        supress_alarm = self.get_supress_alaram(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + supress_alarm

        if self.sense_rail != None:
            voltage_limits = self.get_sense_voltage_limits_string(self.sense_rail, self.sense_rail_type,
                                                                  self.sense_low_voltage_limit,
                                                                  self.sense_high_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        if fault_alarm == True:
            user_enable_disable = 'Q cmd = SET_USER_OV_UV_ALARM_ENABLE_DISABLE, arg = {}, data = {}'.format(self.sense_rail,0)
            trigger_queue = trigger_queue + '\n' + user_enable_disable
        else:
            user_enable_disable = 'Q cmd = SET_USER_OV_UV_ALARM_ENABLE_DISABLE, arg = {}, data = {}'.format(self.sense_rail, 1)
            trigger_queue = trigger_queue + '\n' + user_enable_disable

        if self.sample_force:
            sample_engine_string = self.get_sample_engine(self.force_rail, self.force_rail_type, self.sample_delay,
                                                          self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.sample_sense:
            sample_engine_string = self.get_sample_engine(self.sense_rail, self.sense_rail_type, self.sample_delay,
                                                          self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.turbo_sample_force:
            sample_engine_string = self.get_turbo_sample_engine(self.force_rail, self.force_rail_type,
                                                                self.sample_delay, self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.turbo_sample_sense:
            sample_engine_string = self.get_turbo_sample_engine(self.sense_rail, self.sense_rail_type,
                                                                self.sample_delay, self.sample_count, self.sample_rate)
            trigger_queue = trigger_queue + '\n' + sample_engine_string

        if self.sense_rail != None:
            enable_rail = self.get_enable_rail(self.sense_rail, self.sense_rail_type)
            trigger_queue = trigger_queue + '\n' + enable_rail

        enable_rail = self.get_enable_rail(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + enable_rail

        assert_test_rail = self.get_assert_test_rail(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + assert_test_rail

        if self.force_rail_type == 'HV' and self.force_rail_irange == 'I_7500_MA':
            tracking_voltage = self.get_tracking_voltage_string(self.tracking_voltage, self.force_rail,
                                                                self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + tracking_voltage


        if self.free_drive == True:
            free_drive_voltage = self.get_free_drive_string(self.force_voltage, self.force_rail, self.force_rail_type,
                                                            self.force_free_drive_delay)
            trigger_queue = trigger_queue + '\n' + free_drive_voltage
        else:
            force_voltage = self.get_force_voltage_string(self.force_voltage, self.force_rail, self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + force_voltage

        if self.force_rail_type == 'HC' or self.force_rail_type == 'LC':
            set_vcomp_alarms = self.get_set_vcomp_alarms(self.force_rail, self.force_rail_type)
            trigger_queue = trigger_queue + '\n' + set_vcomp_alarms

        time_delay = self.get_time_delay(self.force_rail, self.force_rail_type, 700)
        trigger_queue = trigger_queue + '\n' + time_delay

        force_trigger_queue_footer = self.get_trigger_queue_footer(self.force_rail, self.force_rail_type)
        trigger_queue = trigger_queue + '\n' + force_trigger_queue_footer

        force_trigger_queue_footer_lvm = self.get_trigger_queue_footer(self.sense_rail, self.sense_rail_type)
        trigger_queue = trigger_queue + '\n' + force_trigger_queue_footer_lvm

        tq_complete = self.get_tq_complete()
        trigger_queue = trigger_queue + '\n' + tq_complete
        return trigger_queue

    def add_header_footer_to_trigger_queue(self, trigger_queue):

        force_trigger_queue_header = self.get_trigger_queue_header(self.force_rail, self.force_rail_type)
        trigger_queue_with_header_footer = force_trigger_queue_header + trigger_queue

        time_delay = self.get_time_delay(self.force_rail, self.force_rail_type, 700)
        trigger_queue_with_header_footer = trigger_queue_with_header_footer + '\n' + time_delay

        force_trigger_queue_footer = self.get_trigger_queue_footer(self.force_rail, self.force_rail_type)
        trigger_queue_with_header_footer = trigger_queue_with_header_footer + '\n' + force_trigger_queue_footer

        tq_complete = self.get_tq_complete()
        trigger_queue_with_header_footer = trigger_queue_with_header_footer + '\n' + tq_complete
        return trigger_queue_with_header_footer

    def add_header_footer_to_dut_trigger_queue(self,trigger_queue):

        force_trigger_queue_header_lv = self.get_trigger_queue_header(self.sense_rail,self.sense_rail_type)
        trigger_queue_with_header_footer_hv_lv = force_trigger_queue_header_lv + '\n'+ trigger_queue

        time_delay = self.get_time_delay(self.sense_rail,self.sense_rail_type, 700)
        trigger_queue_with_header_footer = trigger_queue_with_header_footer_hv_lv + '\n' + time_delay

        force_trigger_queue_footer =  self.get_trigger_queue_footer(self.sense_rail,self.sense_rail_type)
        trigger_queue_with_header_footer = trigger_queue_with_header_footer + '\n' + force_trigger_queue_footer

        tq_complete = self.get_tq_complete()
        trigger_queue_with_header_footer = trigger_queue_with_header_footer + '\n' + tq_complete
        return trigger_queue_with_header_footer

    def get_sample_engine(self,rail,rail_type,delay,count,rate):
        sample_engine_string = self.board.ConfigureAndStartSamplingEngine(uhc=self.uhc,
                                                                          delay=delay,
                                                                          count=count,
                                                                          rate=rate,
                                                                          rail_type=rail_type,
                                                                          rails = 0x1 << rail)

        return sample_engine_string

    def get_turbo_sample_engine(self,rail,rail_type,delay,count,rate):
        sample_engine_string = self.board.ConfigureAndStartTurboSamplingEngine(delay=delay,
                                                                          count=count,
                                                                          rate=rate,
                                                                          rail_type=rail_type,
                                                                          rails = 0x1 << rail)

        return sample_engine_string

    def get_tq_complete(self):
        return 'TqComplete rail = 0, value = 0'

    def get_time_delay(self,rail,rail_type,delay):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        return 'TimeDelay rail={}, value={}'.format(rail,delay)

    def get_set_vcomp_alarms(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        return 'SetVCompAlarm rail = {}, value = 0'.format(rail)

    def get_flush_clamps(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        return 'Q cmd = FORCE_CLAMP_FLUSH, arg = {}, data = 1'.format(rail)

    def get_hw_switch_enable(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        hv_switch_enable = 'Q cmd = HW_SWITCH_ENABLE, arg = {}, data = 1'.format(rail)
        time_delay = self.get_time_delay(self.force_rail, self.force_rail_type, 11000)
        irange_string = hv_switch_enable + '\n' + time_delay
        return irange_string

    def get_current_range(self,rail,rail_type,irange):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        irange_string = 'SetCurrentRange rail = {}, value = {}'.format(rail,irange)
        # if rail_type == 'HV' and irange == 'I_7500_MA':
        #     hv_switch_enable = 'Q cmd = HW_SWITCH_ENABLE, arg = {}, data = 1'.format(rail)
        #     time_delay = self.get_time_delay(self.force_rail, self.force_rail_type, 11000)
        #     irange_string = irange_string +'\n' +hv_switch_enable+'\n'+ time_delay
        return irange_string

    def get_test_mode(self,rail,rail_type, mode):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        return 'SetMode rail={}, value={}'.format(rail,mode)

    def get_trigger_queue_enable_disable_sense(self, rail, rail_type, value):
        if rail_type == 'LVM':
            rail = rail
        else:
            rail = rail+16
        return 'EnableDisableLocalSense rail={}, value={}'.format(rail, value)

    def get_set_auto_compensation(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type == 'VLC':
            rail = rail
        else:
            rail = rail + 16
        return 'Q cmd = SET_AUTO_COMPENSATION, arg = {}, data = 1'.format(rail)

    def get_soft_span_code(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type == 'VLC':
            rail = rail
        else:
            rail = rail + 16
        return 'Q cmd = SET_SOFTSPAN_CODE, arg = {}, data = 7'.format(rail)

    def get_supress_alaram(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type == 'VLC':
            rail = rail
        else:
            rail = rail + 16
        return 'Q cmd=SUPPRESS_ALARMS, arg={}, data=80\n'.format(rail)

    def get_assert_test_rail(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type == 'VLC':
            rail = rail
        else:
            rail = rail + 16
        return 'Q cmd=ASSERT_TEST_RAIL, arg={}, data=0\n'.format(rail)

    def get_set_v_range(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type == 'VLC':
            rail = rail
        else:
            rail = rail + 16
        return 'Q cmd = SET_V_RANGE, arg = {}, data = 1'.format(rail)

    def get_disable_rail(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        if self.open_socket == False and rail_type == 'HV':
            return 'EnableDisableRail rail={}, value=0'.format(rail)
        else:
            return 'EnableDisableRail rail={}, value=0'.format(rail)

    def get_enable_rail(self,rail,rail_type):
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        if self.open_socket == False and rail_type == 'HV':
            return 'EnableDisableRail rail={}, value=0x1'.format(rail)
        else:
            return 'EnableDisableRail rail={}, value=1'.format(rail)

    def get_trigger_queue_header(self,rail,rail_type):
        begin = (self.dutid << 1) | 1
        rail_mask = 0x1 << rail
        resource_id = getattr(symbols.RESOURCEIDS, rail_type)
        if self._legacy_tc_looping:
            header_line1 = 'LegacyTcLoopingTqNotify rail=0, value={}'.format(begin)
            header_line2 = 'LegacyTcLoopingTqNotify rail=16, value={}'.format(begin)
            trigger_queue_header_string = header_line1 + '\n' + header_line2
        else:
            header_line1 = 'WakeRail resourceid = {}, railmask = {}'.format(resource_id, rail_mask)
            header_line2 = 'SetRailBusy resourceid = {}, railmask = {}'.format(resource_id, rail_mask)
            trigger_queue_header_string = header_line1 + '\n' + header_line2
        return trigger_queue_header_string

    def get_trigger_queue_footer(self,rail,rail_type):
        end = (self.dutid << 1)
        rail_mask = 0x1 << rail
        resource_id = getattr(symbols.RESOURCEIDS, rail_type)

        if self._legacy_tc_looping:
            header_line1 = 'LegacyTcLoopingTqNotify rail=0, value={}'.format(end)
            header_line2 = 'LegacyTcLoopingTqNotify rail=16, value={}'.format(end)
            trigger_queue_footer_string = header_line1 + '\n' + header_line2
        else:
            trigger_queue_footer_string = 'TqNotify railmask = {}, resourceid = {}'.format(rail_mask,resource_id)
        return trigger_queue_footer_string

    def EncodeFixedPoint3_16(self, f):
        return hddpstbc.EncodeFixedPoint16(12, f) & 0xFFFF

    def get_clamp_limit_values(self,rail_type,clamp_low,clamp_high,irange):
        if rail_type == 'HV' or rail_type == 'LVM':
            converted_clamp_low = self.board.i_clamp_current_to_ad5560_raw_dac_code(clamp_low,irange)
            converted_clamp_high = self.board.i_clamp_current_to_ad5560_raw_dac_code(clamp_high,irange)
        else:
            converted_clamp_low = self.board.i_clamp_current_to_ad5560_raw_dac_code(clamp_low,irange)
            converted_clamp_high = self.board.i_clamp_current_to_ad5560_raw_dac_code(clamp_high,irange)
        return converted_clamp_low,converted_clamp_high

    def get_current_clamp_limits_string(self,rail,rail_type,irange, low_clamp, high_clamp):
        string_line1 = self.get_low_current_clamp_limits_string(rail, rail_type, low_clamp,irange)
        string_line2 = self.get_high_current_clamp_limits_string(rail, rail_type, high_clamp,irange)
        clamp_limit_string = string_line1 + '\n' + string_line2
        return clamp_limit_string

    def get_low_current_clamp_limits_string(self,rail, rail_type, clamp_low,irange):
        clamp_low, clamp_high = self.get_clamp_limit_values(rail_type=rail_type, clamp_low=clamp_low, clamp_high=0,irange= irange)
        if rail_type == 'LVM' or rail_type =='VLC':
            rail = rail
        else:
            rail = rail+16
        string_line1 = 'Q cmd = SET_CURRENT_CLAMP_LOW, arg = {}, data = 0x{:x}'.format(rail, clamp_low)
        return string_line1

    def get_high_current_clamp_limits_string(self,rail, rail_type, clamp_high,irange):
        clamp_low, clamp_high = self.get_clamp_limit_values(rail_type=rail_type, clamp_low=0, clamp_high=clamp_high,irange= irange)
        if rail_type == 'LVM' or rail_type =='VLC':
            rail = rail
        else:
            rail = rail+16
        string_line2 = 'Q cmd = SET_CURRENT_CLAMP_HI, arg = {}, data = 0x{:x}'.format(rail, clamp_high)
        return string_line2

    def get_voltage_limit_values(self,limit_low,limit_high, rail_type):
        if rail_type == 'LVM':
            converted_volt_low_limit = self.board.VoltageToSS7Ad5764Dac(limit_low)
            converted_volt_high_limit = self.board.VoltageToSS7Ad5764Dac(limit_high)
        else:
            converted_volt_low_limit = self.board.voltage_to_ad5560_raw_dac_code(limit_low)

            # converted_volt_high_limit = 0xe0de
            converted_volt_high_limit = self.board.voltage_to_ad5560_raw_dac_code(limit_high)
        return converted_volt_low_limit,converted_volt_high_limit

    def get_force_voltage_limits_string(self, rail, rail_type, low_limit, high_limit):
        volt_low_limit, volt_high_limit = self.get_voltage_limit_values(low_limit, high_limit, rail_type)

        if rail_type == 'LVM':
            string_line1 = 'Q cmd = SET_DUT_UV_LIMIT_SS7, arg = {}, data = {}'.format(rail, volt_low_limit)
            string_line2 = 'Q cmd = SET_DUT_OV_LIMIT_SS7, arg = {}, data = {}'.format(rail, volt_high_limit)
        else:
            string_line1 = 'Q cmd = SET_UNDER_VOLTAGE, arg = {}, data = 0x{:x}'.format(rail+16,volt_low_limit)
            string_line2 = 'Q cmd = SET_OVER_VOLTAGE, arg = {}, data = 0x{:x}'.format(rail+16,volt_high_limit)
        volt_limit_string = string_line1 + '\n' + string_line2
        return volt_limit_string

    def get_sense_voltage_limits_string(self, rail, rail_type, low_limit, high_limit):
        if rail_type == 'LVM':
            string_line1 = self.get_sense_uv_voltage_limits_string(rail, rail_type, low_limit)
            string_line2 = self.get_sense_ov_voltage_limits_string(rail, rail_type, high_limit)
        else:
            string_line1 = self.get_sense_uv_voltage_limits_string(rail, rail_type, low_limit)
            string_line2 = self.get_sense_ov_voltage_limits_string(rail, rail_type, high_limit)
        volt_limit_string = string_line1 + '\n' + string_line2

        return volt_limit_string

    def get_sense_ov_voltage_limits_string(self, rail, rail_type, high_limit):
        volt_low_limit, volt_high_limit = self.get_voltage_limit_values(limit_low=0, limit_high=high_limit,
                                                                        rail_type=rail_type)

        if rail_type == 'LVM':
            string_line2 = 'Q cmd = SET_DUT_OV_LIMIT_SS7, arg = {}, data = 0x{:x}'.format(rail, volt_high_limit)
        else:
            string_line2 = 'Q cmd = SET_OVER_VOLTAGE, arg = {}, data = 0x{:x}'.format(rail+16,volt_high_limit)
        return string_line2

    def get_sense_uv_voltage_limits_string(self, rail, rail_type, low_limit):
        volt_low_limit, volt_high_limit = self.get_voltage_limit_values(limit_low=low_limit, limit_high=0,
                                                                        rail_type=rail_type)

        if rail_type == 'LVM':
            string_line1 = 'Q cmd = SET_DUT_UV_LIMIT_SS7, arg = {}, data = 0x{:x}'.format(rail, volt_low_limit)
        else:
            string_line1 = 'Q cmd = SET_UNDER_VOLTAGE, arg = {}, data = 0x{:x}'.format(rail + 16, volt_low_limit)
        return string_line1

    def get_force_voltage_value(self,force_voltage, rail_type):
        converted_force_voltage = self.board.voltage_to_ad5560_raw_dac_code(force_voltage)
        return converted_force_voltage

    def get_force_voltage_string(self,force_voltage,rail,rail_type):
        force_voltage_value = self.get_force_voltage_value(force_voltage, rail_type)
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        force_volt_string = 'Q cmd = SET_VOLTAGE, arg = {}, data = {}'.format(rail,force_voltage_value)
        return force_volt_string

    def get_free_drive_string(self, force_voltage, rail,rail_type,delay):
        force_voltage_value = self.get_force_voltage_value(force_voltage, rail_type)
        string_line0 = 'Q cmd=AD5560_{}_ADDR, arg=ALARM_SETUP, data=0x100\n'.format(rail)
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        string_line1 = 'Q cmd = START_FREE_DRIVE, arg = {}, data = {}'.format(rail,force_voltage_value)
        string_line2 = 'Q cmd = TIME_DELAY, arg = {}, data = {}'.format(rail,delay)
        string_line3 = 'Q cmd = END_FREE_DRIVE, arg = {}, data = 0'.format(rail)
        string_line4 = 'Q cmd=AD5560_{}_ADDR, arg=ALARM_SETUP, data=0x00\n'.format(rail-16)
        return  string_line0 + '\n'+string_line1 + '\n' + string_line2 + '\n' + string_line3 + '\n' + string_line4

    def get_free_drive_current_string(self,irange, rail, rail_type, low_current, high_current):
        free_drive_low, free_drive_high = self.get_free_drive_current_values(irange,low_current,high_current)
        if rail_type == 'LVM' or rail_type=='VLC':
            rail = rail
        else:
            rail = rail+16
        string_line1 = 'Q cmd = SET_FREE_DRIVE_LOW_CURRENT, arg = {}, data = 0x{:x}'.format(rail,free_drive_low)
        string_line2 = 'Q cmd=  SET_FREE_DRIVE_HIGH_CURRENT, arg={}, data=0x{:X}'.format(rail,free_drive_high)
        return string_line1 + '\n' + string_line2

    def get_free_drive_current_values(self,irange, low_current, high_current):
        if irange == 'I_25_MA':
            irange = 'I_500_MA'
        force_rail_irange = irange
        converted_low_current = self.board.i_clamp_current_to_ad5560_raw_dac_code(low_current, force_rail_irange)
        converted_high_current = self.board.i_clamp_current_to_ad5560_raw_dac_code(high_current, force_rail_irange)
        return converted_low_current,converted_high_current

    def get_tracking_voltage_string(self,tracking_voltage,rail,rail_type):
        if rail_type == 'LVM':
            rail = rail
        else:
            rail = rail+16
        tracking_voltage = self.board.GetCmdDataForSetTrackingVoltage((tracking_voltage))
        tracking_voltage_string = 'Q cmd = SET_TRACKING_VOLTAGE, arg = {}, data = 0x{:x}'.format(rail,tracking_voltage)
        return tracking_voltage_string