# INTEL CONFIDENTIAL
#
# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Log, LoggedError, Object
from Common.instruments.dps.hddps import Hddps
from Common.instruments.dps.hddps_registers import TRIGGERDOWNTEST, \
    TRIGGERUPTEST


def create_interface(instrument):
    attempt_initialization(instrument)
    return [TriggerInterface(i) for i in instrument.subslots]

def attempt_initialization(instrument):
    num_retries = 3
    name = f'{instrument.name()}_{instrument.slot}'
    Log('info', f'{name}: Initializing trigger interface')

    for retry in range(num_retries):
        try:
            instrument.ensure_initialized()
            break
        except (Hddps.AuroraBusException, Hddps.InstrumentError, LoggedError,
                RuntimeError) as e:
            Log('warning', f'{name}: {e}. Re-initializing ({retry+1} of '
                           f'{num_retries})')
    else:
        raise Hddps.InstrumentError(f'{name}: Unable to initialize FPGA')


class TriggerInterface(Object):
    def __init__(self, subslot):
        super().__init__()
        self._subslot = subslot

    def setup_interface(self):
        self._subslot.empty_trigger_buffer()

    def send_up_trigger(self, trigger):
        self._subslot.BarWrite(TRIGGERUPTEST.BAR,
                               TRIGGERUPTEST.ADDR,
                               trigger)

    def check_for_down_trigger(self, expected_trigger, num_retries=50):
        actual_trigger = 0
        for retry in range(num_retries):
            actual_trigger = self.read_down_trigger()
            if actual_trigger == expected_trigger:
                break

        return actual_trigger

    def log_status(self):
        pass

    def read_down_trigger(self):
        return self._subslot.BarRead(TRIGGERDOWNTEST.BAR, TRIGGERDOWNTEST.ADDR)

    def verify_aurora_link_is_stable(self):
        if not self.aurora_link_is_stable():
            self.Log(
                'warning',
                self.create_table_aurora_link_status())
            """ TODO: There's a mismatch in detrmining if link is stable when
             comparing between FVAL and HLD
            status bit     : (FVAL, HLD)
            AuroraPLLLock  : (0, 1)
            AuroraChannelUp: (0, 1) 
            AuroraLaneUp   : (0, 1)
            AuroraHardError: (1, 1) 
            AuroraSoftError: (1, 1)
            """
        else:
            self.Log('info', f'{self.name()}: Aurora link is stable')

    def aurora_link_is_stable(self):
        status_reg = self._subslot.Read('STATUS')

        pll_lock_status = (status_reg.AuroraPLLLock and
                           status_reg.DDRControllerPLLLock)
        error_status = status_reg.AuroraSoftError or status_reg.AuroraHardError
        up_status = status_reg.AuroraLaneUp and status_reg.AuroraChannelUp

        if not pll_lock_status or error_status or not up_status:
            return False
        else:
            return True

    def reset_trigger_interface(self):
        resets_reg = self._subslot.Read('RESETS')
        resets_reg.AuroraReset = 1
        self._subslot.Write('RESETS', resets_reg.value)
        resets_reg.AuroraReset = 0
        self._subslot.Write('RESETS', resets_reg.value)

    def create_table_aurora_link_status(self):
        status_reg = self._subslot.Read('STATUS')
        return self._subslot.create_table_from_register(status_reg)

    def slot(self):
        return self._subslot.slot

    def subslot_num(self):
        return self._subslot.subslot

    def name(self):
        return self._subslot.name()
