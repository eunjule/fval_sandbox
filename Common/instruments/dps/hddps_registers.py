################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from ctypes import c_uint

import Common.register as register
from operator import itemgetter


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HddpsRegister):
                if attribute is not HddpsRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HddpsRegister):
                if attribute is not HddpsRegister:
                    # if '__' not in attribute.__name__:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HddpsStruct):
                if attribute is not HddpsStruct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HddpsStruct):
                if attribute is not HddpsStruct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class HddpsRegister(register.Register):
    BAR = 1


class HddpsStruct(register.Register):
    ADDR = None


module_dict = globals()

class BAR1_SCRATCH(HddpsRegister):
    BAR = 1
    ADDR = 0x0
    _fields_ = [('Data', c_uint, 32)]


class FpgaVersion(HddpsRegister):
    BAR = 1
    ADDR = 0x4
    _fields_ = [('Data', c_uint, 32)]


class RESETS(HddpsRegister):
    BAR = 1
    ADDR = 0x8
    _fields_ = [('Unused', c_uint, 1),
                ('AuroraReset', c_uint, 1),
                ('DDRControllerReset', c_uint, 1),
                ('BlockAuroraReceive', c_uint, 1),
                ('Data', c_uint, 28)]


class STATUS(HddpsRegister):
    BAR = 1
    ADDR = 0xc
    _fields_ = [('AuroraPLLLock', c_uint, 1),
                ('AuroraLaneUp', c_uint, 1),
                ('AuroraSoftError', c_uint, 1),
                ('AuroraHardError', c_uint, 1),
                ('AuroraChannelUp', c_uint, 1),
                ('DDRControllerPLLLock', c_uint, 1),
                ('DDRCalibrationDone', c_uint, 1),
                ('Data', c_uint, 25)]


class PcieSlotId(HddpsRegister):
    BAR = 1
    ADDR = 0x10
    _fields_ = [('Data', c_uint, 32)]


class ExecuteTrigger(HddpsRegister):
    BAR = 1
    ADDR = 0x14
    _fields_ = [('ByteAddress', c_uint, 31),
                ('Execute', c_uint, 1)]


class ExecuteInterrupt(HddpsRegister):
    BAR = 1
    ADDR = 0x18
    _fields_ = [('Data', c_uint, 32)]


class SET_SAFE_STATE(HddpsRegister):
    BAR = 1
    ADDR = 0x1c
    _fields_ = [('RailSafeBit', c_uint, 1),
                ('Data', c_uint, 31)]

class TRIG_RX_TEST_FIFO_DATA_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Data', c_uint, 32)]

class MemWrDataTag(HddpsRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('Data', c_uint, 32)]


class MemRdDataTag(HddpsRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('Data', c_uint, 32)]


class MemAddressTag(HddpsRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('Data', c_uint, 32)]


class TRIGGERUPTEST(HddpsRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('Data', c_uint, 32)]


class TRIGGERDOWNTEST(HddpsRegister):
    BAR = 1
    ADDR = 0x54
    _fields_ = [('Data', c_uint, 32)]


class SampleCollectorIndex(HddpsRegister):
    BAR = 1
    ADDR = 0x68
    _fields_ = [('Index', c_uint, 6),
                ('Data', c_uint, 26)]


class SampleCollectorCount(HddpsRegister):
    BAR = 1
    ADDR = 0x6c
    _fields_ = [('Count', c_uint, 9),
                ('Data', c_uint, 23)]


class SampleCollectorData(HddpsRegister):
    BAR = 1
    ADDR = 0x70
    _fields_ = [('Data', c_uint, 32)]


class AURORA_ERROR_COUNTS(HddpsRegister):
    BAR = 1
    ADDR = 0x74
    _fields_ = [('AuroraSoftErrorCount', c_uint, 16),
                ('AuroraHardErrorCount', c_uint, 16)]


class DUT_DOMAIN_ID(HddpsRegister):
    BAR = 1
    ADDR = 0x80
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('DutDomainId', c_uint, 6),
                ('DutValidBit', c_uint, 1),
                ('Data', c_uint, 25)]


class HclcDutIdRailConfig(HddpsRegister):
    BAR = 1
    ADDR = 0xa0
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('HclcDutRailConfig', c_uint, 12),
                ('Data', c_uint, 20)]


class VlcUhcRailConfig(HddpsRegister):
    BAR = 1
    ADDR = 0xc0
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('Data', c_uint, 32)]


class RailMasterMask(HddpsRegister):
    BAR = 1
    ADDR = 0x100
    _fields_ = [('HcRailIndicator', c_uint, 12),
                ('Data', c_uint, 20)]


class SlaveBoardMask(HddpsRegister):
    BAR = 1
    ADDR = 0x104
    _fields_ = [('SlaveSlotIndicator', c_uint, 12),
                ('Data', c_uint, 20)]


class RemoteMasterMask(HddpsRegister):
    BAR = 1
    ADDR = 0x108
    _fields_ = [('SlavesOnRemoteBoard', c_uint, 12),
                ('Data', c_uint, 20)]


class HcIGang(HddpsRegister):
    BAR = 1
    ADDR = 0x10c
    _fields_ = [('PwmConfigMaster', c_uint, 12),
                ('Data', c_uint, 19),
                ('PwmCrsBrdGangEn', c_uint, 1)]


class RAIL_INV_SLAVE_CNT(HddpsRegister):
    BAR = 1
    ADDR = 0x110
    REGCOUNT = 10
    BASEMUL = 4
    _fields_ = [('SlaveCntValue', c_uint, 16),
                ('Reserved', c_uint, 16)]


class HcIRange24A19A(HddpsRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('HcIRangeValue', c_uint, 12),
                ('Data', c_uint, 20)]


class HclcSlavePowerState(HddpsRegister):
    BAR = 1
    ADDR = 0x14c
    _fields_ = [('HclcSlavePowerStateValue', c_uint, 12),
                ('Data', c_uint, 20)]


class HclcCacheUpdateDisable(HddpsRegister):
    BAR = 1
    ADDR = 0x150
    _fields_ = [('HclcCacheUpdateDisableValue', c_uint, 12),
                ('Data', c_uint, 20)]


class HclcRailsLooping(HddpsRegister):
    BAR = 1
    ADDR = 0x160
    _fields_ = [('HclcLoopCompleteBits', c_uint, 12),
                ('Unused1', c_uint, 4),
                ('HclcLoopingBits', c_uint, 12),
                ('Unused2', c_uint, 4)]


class VlcRailsLooping(HddpsRegister):
    BAR = 1
    ADDR = 0x164
    _fields_ = [('VlcLoopCompleteBits', c_uint, 16),
                ('VlcLoopingBits', c_uint, 16)]


class HclcRailModeVal(HddpsRegister):
    BAR = 1
    ADDR = 0x168
    _fields_ = [('HclcRailModeValue', c_uint, 24),
                ('Data', c_uint, 8)]


class VlcRailModeVal(HddpsRegister):
    BAR = 1
    ADDR = 0x16c
    _fields_ = [('VlcRailModeValue', c_uint, 32)]


class HcEnPwmR(HddpsRegister):
    BAR = 1
    ADDR = 0x170
    _fields_ = [('PwmREn', c_uint, 12),
                ('Data', c_uint, 19),
                ('PwmEnSelect', c_uint, 1)]

class AD5560_V_RANGES(HddpsRegister):
    BAR = 1
    ADDR = 0x174
    _fields_ = [('Data', c_uint, 32)]

class DpsRail0V(HddpsRegister):
    BAR = 1
    ADDR = 0x180
    _fields_ = [('Data', c_uint, 32)]


class DpsRail0I(HddpsRegister):
    BAR = 1
    ADDR = 0x184
    _fields_ = [('Data', c_uint, 32)]


class DpsRail1V(HddpsRegister):
    BAR = 1
    ADDR = 0x188
    _fields_ = [('Data', c_uint, 32)]


class DpsRail1I(HddpsRegister):
    BAR = 1
    ADDR = 0x18c
    _fields_ = [('Data', c_uint, 32)]


class DpsRail2V(HddpsRegister):
    BAR = 1
    ADDR = 0x190
    _fields_ = [('Data', c_uint, 32)]


class DpsRail2I(HddpsRegister):
    BAR = 1
    ADDR = 0x194
    _fields_ = [('Data', c_uint, 32)]


class DpsRail3V(HddpsRegister):
    BAR = 1
    ADDR = 0x198
    _fields_ = [('Data', c_uint, 32)]


class DpsRail3I(HddpsRegister):
    BAR = 1
    ADDR = 0x19c
    _fields_ = [('Data', c_uint, 32)]


class DpsRail4V(HddpsRegister):
    BAR = 1
    ADDR = 0x1a0
    _fields_ = [('Data', c_uint, 32)]


class DpsRail4I(HddpsRegister):
    BAR = 1
    ADDR = 0x1a4
    _fields_ = [('Data', c_uint, 32)]


class DpsRail5V(HddpsRegister):
    BAR = 1
    ADDR = 0x1a8
    _fields_ = [('Data', c_uint, 32)]


class DpsRail5I(HddpsRegister):
    BAR = 1
    ADDR = 0x1ac
    _fields_ = [('Data', c_uint, 32)]


class DpsRail6V(HddpsRegister):
    BAR = 1
    ADDR = 0x1b0
    _fields_ = [('Data', c_uint, 32)]


class DpsRail6I(HddpsRegister):
    BAR = 1
    ADDR = 0x1b4
    _fields_ = [('Data', c_uint, 32)]


class DpsRail7V(HddpsRegister):
    BAR = 1
    ADDR = 0x1b8
    _fields_ = [('Data', c_uint, 32)]


class DpsRail7I(HddpsRegister):
    BAR = 1
    ADDR = 0x1bc
    _fields_ = [('Data', c_uint, 32)]


class DpsRail8V(HddpsRegister):
    BAR = 1
    ADDR = 0x1c0
    _fields_ = [('Data', c_uint, 32)]


class DpsRail8I(HddpsRegister):
    BAR = 1
    ADDR = 0x1c4
    _fields_ = [('Data', c_uint, 32)]


class DpsRail9V(HddpsRegister):
    BAR = 1
    ADDR = 0x1c8
    _fields_ = [('Data', c_uint, 32)]


class DpsRail9I(HddpsRegister):
    BAR = 1
    ADDR = 0x1cc
    _fields_ = [('Data', c_uint, 32)]





class VlcRail0V(HddpsRegister):
    BAR = 1
    ADDR = 0x200
    _fields_ = [('Data', c_uint, 32)]


class VlcRail0I(HddpsRegister):
    BAR = 1
    ADDR = 0x204
    _fields_ = [('Data', c_uint, 32)]


class VlcRail1V(HddpsRegister):
    BAR = 1
    ADDR = 0x208
    _fields_ = [('Data', c_uint, 32)]


class VlcRail1I(HddpsRegister):
    BAR = 1
    ADDR = 0x20c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail2V(HddpsRegister):
    BAR = 1
    ADDR = 0x210
    _fields_ = [('Data', c_uint, 32)]


class VlcRail2I(HddpsRegister):
    BAR = 1
    ADDR = 0x214
    _fields_ = [('Data', c_uint, 32)]


class VlcRail3V(HddpsRegister):
    BAR = 1
    ADDR = 0x218
    _fields_ = [('Data', c_uint, 32)]


class VlcRail3I(HddpsRegister):
    BAR = 1
    ADDR = 0x21c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail4V(HddpsRegister):
    BAR = 1
    ADDR = 0x220
    _fields_ = [('Data', c_uint, 32)]


class VlcRail4I(HddpsRegister):
    BAR = 1
    ADDR = 0x224
    _fields_ = [('Data', c_uint, 32)]


class VlcRail5V(HddpsRegister):
    BAR = 1
    ADDR = 0x228
    _fields_ = [('Data', c_uint, 32)]


class VlcRail5I(HddpsRegister):
    BAR = 1
    ADDR = 0x22c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail6V(HddpsRegister):
    BAR = 1
    ADDR = 0x230
    _fields_ = [('Data', c_uint, 32)]


class VlcRail6I(HddpsRegister):
    BAR = 1
    ADDR = 0x234
    _fields_ = [('Data', c_uint, 32)]


class VlcRail7V(HddpsRegister):
    BAR = 1
    ADDR = 0x238
    _fields_ = [('Data', c_uint, 32)]


class VlcRail7I(HddpsRegister):
    BAR = 1
    ADDR = 0x23c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail8V(HddpsRegister):
    BAR = 1
    ADDR = 0x240
    _fields_ = [('Data', c_uint, 32)]


class VlcRail8I(HddpsRegister):
    BAR = 1
    ADDR = 0x244
    _fields_ = [('Data', c_uint, 32)]


class VlcRail9V(HddpsRegister):
    BAR = 1
    ADDR = 0x248
    _fields_ = [('Data', c_uint, 32)]


class VlcRail9I(HddpsRegister):
    BAR = 1
    ADDR = 0x24c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail10V(HddpsRegister):
    BAR = 1
    ADDR = 0x250
    _fields_ = [('Data', c_uint, 32)]


class VlcRail10I(HddpsRegister):
    BAR = 1
    ADDR = 0x254
    _fields_ = [('Data', c_uint, 32)]


class VlcRail11V(HddpsRegister):
    BAR = 1
    ADDR = 0x258
    _fields_ = [('Data', c_uint, 32)]


class VlcRail11I(HddpsRegister):
    BAR = 1
    ADDR = 0x25c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail12V(HddpsRegister):
    BAR = 1
    ADDR = 0x260
    _fields_ = [('Data', c_uint, 32)]


class VlcRail12I(HddpsRegister):
    BAR = 1
    ADDR = 0x264
    _fields_ = [('Data', c_uint, 32)]


class VlcRail13V(HddpsRegister):
    BAR = 1
    ADDR = 0x268
    _fields_ = [('Data', c_uint, 32)]


class VlcRail13I(HddpsRegister):
    BAR = 1
    ADDR = 0x26c
    _fields_ = [('Data', c_uint, 32)]


class VlcRail14V(HddpsRegister):
    BAR = 1
    ADDR = 0x270
    _fields_ = [('Data', c_uint, 32)]


class VlcRail14I(HddpsRegister):
    BAR = 1
    ADDR = 0x274
    _fields_ = [('Data', c_uint, 32)]


class VlcRail15V(HddpsRegister):
    BAR = 1
    ADDR = 0x278
    _fields_ = [('Data', c_uint, 32)]


class VlcRail15I(HddpsRegister):
    BAR = 1
    ADDR = 0x27c
    _fields_ = [('Data', c_uint, 32)]


class Rail00VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x280
    _fields_ = [('Data', c_uint, 32)]


class Rail01VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x284
    _fields_ = [('Data', c_uint, 32)]


class Rail02VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x288
    _fields_ = [('Data', c_uint, 32)]


class Rail03VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x28c
    _fields_ = [('Data', c_uint, 32)]


class Rail04VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x290
    _fields_ = [('Data', c_uint, 32)]


class Rail05VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x294
    _fields_ = [('Data', c_uint, 32)]


class Rail06VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x298
    _fields_ = [('Data', c_uint, 32)]


class Rail07VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x29c
    _fields_ = [('Data', c_uint, 32)]


class Rail08VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x2a0
    _fields_ = [('Data', c_uint, 32)]


class Rail09VAdcGainOffset(HddpsRegister):
    BAR = 1
    ADDR = 0x2a4
    _fields_ = [('Data', c_uint, 32)]


class ADC_I_CAL_ADDR(HddpsRegister):
    BAR = 1
    ADDR = 0x2b0
    _fields_ = [('Data', c_uint, 32)]


class ADC_I_CAL_DATA(HddpsRegister):
    BAR = 1
    ADDR = 0x2b4
    _fields_ = [('Data', c_uint, 32)]

class VLC_00_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2c0
    _fields_ = [('Data', c_uint, 32)]

class VLC_01_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2c4
    _fields_ = [('Data', c_uint, 32)]

class VLC_02_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2c8
    _fields_ = [('Data', c_uint, 32)]

class VLC_03_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2cc
    _fields_ = [('Data', c_uint, 32)]

class VLC_04_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2d0
    _fields_ = [('Data', c_uint, 32)]

class VLC_05_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2d4
    _fields_ = [('Data', c_uint, 32)]

class VLC_06_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2d8
    _fields_ = [('Data', c_uint, 32)]

class VLC_07_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2dc
    _fields_ = [('Data', c_uint, 32)]

class VLC_08_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2e0
    _fields_ = [('Data', c_uint, 32)]

class VLC_09_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2e4
    _fields_ = [('Data', c_uint, 32)]

class VLC_10_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2e8
    _fields_ = [('Data', c_uint, 32)]

class VLC_11_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2ec
    _fields_ = [('Data', c_uint, 32)]

class VLC_12_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2f0
    _fields_ = [('Data', c_uint, 32)]

class VLC_13_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2f4
    _fields_ = [('Data', c_uint, 32)]

class VLC_14_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2f8
    _fields_ = [('Data', c_uint, 32)]

class VLC_15_ADC_V_GAIN_OFFSET(HddpsRegister):
    BAR = 1
    ADDR = 0x2fc
    _fields_ = [('Data', c_uint, 32)]

class AD5560_00_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x580
    _fields_ = [('Data', c_uint, 32)]

class AD5560_01_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x584
    _fields_ = [('Data', c_uint, 32)]

class AD5560_02_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x588
    _fields_ = [('Data', c_uint, 32)]

class AD5560_03_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x58c
    _fields_ = [('Data', c_uint, 32)]

class AD5560_04_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x590
    _fields_ = [('Data', c_uint, 32)]

class AD5560_05_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x594
    _fields_ = [('Data', c_uint, 32)]

class AD5560_06_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x598
    _fields_ = [('Data', c_uint, 32)]

class AD5560_07_IFORCE_ADC_OOB_VALUE(HddpsRegister):
    BAR = 1
    ADDR = 0x59c
    _fields_ = [('Data', c_uint, 32)]


class ENABLE_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x600
    _fields_ = [('AlarmHandler', c_uint, 1),
                ('Data', c_uint, 31)]


class GLOBAL_ALARMS_LEGACY_TC_LOOPING(HddpsRegister):
    BAR = 1
    ADDR = 0x604
    _fields_ = [('TqNotifyAlarmDut0', c_uint, 1),
                ('TqNotifyAlarmDut1', c_uint, 1),
                ('TqNotifyAlarmDut2', c_uint, 1),
                ('TqNotifyAlarmDut3', c_uint, 1),
                ('TqNotifyAlarmDut4', c_uint, 1),
                ('TqNotifyAlarmDut5', c_uint, 1),
                ('TqNotifyAlarmDut6', c_uint, 1),
                ('TqNotifyAlarmDut7', c_uint, 1),
                ('HclcRailAlarms', c_uint, 1),
                ('HclcCFoldAlarms', c_uint, 1),
                ('HclcSampleAlarms', c_uint, 1),
                ('VlcRailAlarms', c_uint, 1),
                ('VlcCFoldAlarms', c_uint, 1),
                ('VlcSampleAlarms', c_uint, 1),
                ('BrdTempAlarm', c_uint, 1),
                ('AdTempAlarm', c_uint, 1),
                ('VlcAOverTempAlarm', c_uint, 1),
                ('VlcBOvertempAlarm', c_uint, 1),
                ('TrigExecAlarm', c_uint, 1),
                ('UnassignedAlarmBit19', c_uint, 1),
                ('UnassignedAlarmBit20', c_uint, 1),
                ('UnassignedAlarmBit21', c_uint, 1),
                ('UnassignedAlarmBit22', c_uint, 1),
                ('UnassignedAlarmBit23', c_uint, 1),
                ('UnassignedAlarmBit24', c_uint, 1),
                ('UnassignedAlarmBit25', c_uint, 1),
                ('UnassignedAlarmBit26', c_uint, 1),
                ('UnassignedAlarmBit27', c_uint, 1),
                ('UnassignedAlarmBit28', c_uint, 1),
                ('UnassignedAlarmBit29', c_uint, 1),
                ('UnassignedAlarmBit30', c_uint, 1),
                ('UnassignedAlarmBit31', c_uint, 1)]

non_critical_global_alarms_legacy_tc_looping = ['TqNotifyLCHC','TqNotifyVlc','TqNotifyAlarmDut0','TqNotifyAlarmDut1','TqNotifyAlarmDut2','TqNotifyAlarmDut3','TqNotifyAlarmDut4','TqNotifyAlarmDut5','TqNotifyAlarmDut6','TqNotifyAlarmDut7']
critical_global_alarms_legacy_tc_looping = [x for x in register.get_field_names(GLOBAL_ALARMS_LEGACY_TC_LOOPING) if x not in non_critical_global_alarms_legacy_tc_looping]

class GLOBAL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x604
    _fields_ = [('TqNotifyVlc', c_uint, 1),
                ('TqNotifyLCHC', c_uint, 1),
                ('Reserved', c_uint, 6),
                ('HclcRailAlarms', c_uint, 1),
                ('HclcCFoldAlarms', c_uint, 1),
                ('HclcSampleAlarms', c_uint, 1),
                ('VlcRailAlarms', c_uint, 1),
                ('VlcCFoldAlarms', c_uint, 1),
                ('VlcSampleAlarms', c_uint, 1),
                ('BrdTempAlarm', c_uint, 1),
                ('AdTempAlarm', c_uint, 1),
                ('VlcAOverTempAlarm', c_uint, 1),
                ('VlcBOvertempAlarm', c_uint, 1),
                ('TrigExecAlarm', c_uint, 1),
                ('EcaLo', c_uint, 1),
                ('EcaHi', c_uint, 1),
                ('UnassignedAlarmBit21', c_uint, 1),
                ('UnassignedAlarmBit22', c_uint, 1),
                ('UnassignedAlarmBit23', c_uint, 1),
                ('UnassignedAlarmBit24', c_uint, 1),
                ('UnassignedAlarmBit25', c_uint, 1),
                ('UnassignedAlarmBit26', c_uint, 1),
                ('UnassignedAlarmBit27', c_uint, 1),
                ('UnassignedAlarmBit28', c_uint, 1),
                ('UnassignedAlarmBit29', c_uint, 1),
                ('UnassignedAlarmBit30', c_uint, 1),
                ('UnassignedAlarmBit31', c_uint, 1)]

non_critical_global_alarms = ['TqNotifyLCHC','TqNotifyVlc','Reserved']
critical_global_alarms = [x for x in register.get_field_names(GLOBAL_ALARMS) if x not in non_critical_global_alarms]

class HclcSampleAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x608
    _fields_ = [('HclcDut0SampleStart', c_uint, 1),
                ('HclcDut0OutOfBounds', c_uint, 1),
                ('HclcDut1SampleStart', c_uint, 1),
                ('HclcDut1OutOfBounds', c_uint, 1),
                ('HclcDut2SampleStart', c_uint, 1),
                ('HclcDut2OutOfBounds', c_uint, 1),
                ('HclcDut3SampleStart', c_uint, 1),
                ('HclcDut3OutOfBounds', c_uint, 1),
                ('HclcDut4SampleStart', c_uint, 1),
                ('HclcDut4OutOfBounds', c_uint, 1),
                ('HclcDut5SampleStart', c_uint, 1),
                ('HclcDut5OutOfBounds', c_uint, 1),
                ('HclcDut6SampleStart', c_uint, 1),
                ('HclcDut6OutOfBounds', c_uint, 1),
                ('HclcDut7SampleStart', c_uint, 1),
                ('HclcDut7OutOfBounds', c_uint, 1),
                ('HclcSamplesLost', c_uint, 1),
                ('Data', c_uint, 15)]


class VlcSampleAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x60c
    _fields_ = [('VlcDut0SampleStart', c_uint, 1),
                ('VlcDut0OutOfBounds', c_uint, 1),
                ('VlcDut1SampleStart', c_uint, 1),
                ('VlcDut1OutOfBounds', c_uint, 1),
                ('VlcDut2SampleStart', c_uint, 1),
                ('VlcDut2OutOfBounds', c_uint, 1),
                ('VlcDut3SampleStart', c_uint, 1),
                ('VlcDut3OutOfBounds', c_uint, 1),
                ('VlcDut4SampleStart', c_uint, 1),
                ('VlcDut4OutOfBounds', c_uint, 1),
                ('VlcDut5SampleStart', c_uint, 1),
                ('VlcDut5OutOfBounds', c_uint, 1),
                ('VlcDut6SampleStart', c_uint, 1),
                ('VlcDut6OutOfBounds', c_uint, 1),
                ('VlcDut7SampleStart', c_uint, 1),
                ('VlcDut7OutOfBounds', c_uint, 1),
                ('VlcSamplesLost', c_uint, 1),
                ('Data', c_uint, 15)]


class TrigExecAlarm(HddpsRegister):
    BAR = 1
    ADDR = 0x614
    _fields_ = [('InvalidTQContent0Alarm', c_uint, 1),
                ('InvalidTQContent1Alarm', c_uint, 1),
                ('InvalidTQContent2Alarm', c_uint, 1),
                ('InvalidTQContent3Alarm', c_uint, 1),
                ('InvalidTQContent4Alarm', c_uint, 1),
                ('InvalidTQContent5Alarm', c_uint, 1),
                ('InvalidTQContent6Alarm', c_uint, 1),
                ('InvalidTQContent7Alarm', c_uint, 1),
                ('Reserved', c_uint, 24)]


class DUT_RAILS_BUSY_FOLDED(HddpsRegister):
    BAR = 1
    ADDR = 0x618
    _fields_ = [('Data', c_uint, 32)]


class HclcRailsFolded(HddpsRegister):
    BAR = 1
    ADDR = 0x624
    _fields_ = [('HCLCRail0Folded', c_uint, 1),
                ('HCLCRail1Folded', c_uint, 1),
                ('HCLCRail2Folded', c_uint, 1),
                ('HCLCRail3Folded', c_uint, 1),
                ('HCLCRail4Folded', c_uint, 1),
                ('HCLCRail5Folded', c_uint, 1),
                ('HCLCRail6Folded', c_uint, 1),
                ('HCLCRail7Folded', c_uint, 1),
                ('HCLCRail8Folded', c_uint, 1),
                ('HCLCRail9Folded', c_uint, 1),
                ('Data', c_uint, 22)]


class VlcRailsFolded(HddpsRegister):
    BAR = 1
    ADDR = 0x628
    _fields_ = [('VLCRail0Folded', c_uint, 1),
                ('VLCRail1Folded', c_uint, 1),
                ('VLCRail2Folded', c_uint, 1),
                ('VLCRail3Folded', c_uint, 1),
                ('VLCRail4Folded', c_uint, 1),
                ('VLCRail5Folded', c_uint, 1),
                ('VLCRail6Folded', c_uint, 1),
                ('VLCRail7Folded', c_uint, 1),
                ('VLCRail8Folded', c_uint, 1),
                ('VLCRail9Folded', c_uint, 1),
                ('VLCRail10Folded', c_uint, 1),
                ('VLCRail11Folded', c_uint, 1),
                ('VLCRail12Folded', c_uint, 1),
                ('VLCRail13Folded', c_uint, 1),
                ('VLCRail14Folded', c_uint, 1),
                ('VLCRail15Folded', c_uint, 1),
                ('Data', c_uint, 16)]


class FpgaClMask(HddpsRegister):
    BAR = 1
    ADDR = 0x62c
    _fields_ = [('Data', c_uint, 20),
                ('FpgaClMaskValue', c_uint, 12)]


class HclcOscDetect(HddpsRegister):
    BAR = 1
    ADDR = 0x630
    _fields_ = [('OvTestMode', c_uint, 1),
                ('OvHysteresisCount', c_uint, 6),
                ('OvCpohFilterCount', c_uint, 9),
                ('HclcOscDetWindow', c_uint, 8),
                ('HclcOscDetThreshold', c_uint, 8)]
class HCLC_RAILS_BUSY_VALID(HddpsRegister):
    BAR = 1
    ADDR = 0x634
    _fields_ = [('Data', c_uint, 32)]

class VLC_RAILS_BUSY_VALID(HddpsRegister):
    BAR = 1
    ADDR = 0x638
    _fields_ = [('Data', c_uint, 32)]

class DUT_RAILS_BUSY_VALID(HddpsRegister):
    BAR = 1
    ADDR = 0x63c
    _fields_ = [('Data', c_uint, 32)]

class SShotControl(HddpsRegister):
    BAR = 1
    ADDR = 0x640
    _fields_ = [('SShotEventMaskTrigger', c_uint, 1),
                ('SShotEventMaskAlarm', c_uint, 1),
                ('Unused1', c_uint, 6),
                ('SShotEventSignatureTrigger', c_uint, 1),
                ('SShotEventSignatureAlarm', c_uint, 1),
                ('Unused2', c_uint, 14),
                ('SShotEnable', c_uint, 1),
                ('Unused3', c_uint, 3),
                ('SShotUhcSelect', c_uint, 3),
                ('Unused4', c_uint, 1)]


class SShotStopTrigger(HddpsRegister):
    BAR = 1
    ADDR = 0x644
    _fields_ = [('Data', c_uint, 32)]


class SShotTriggerFifoTimeStamp(HddpsRegister):
    BAR = 1
    ADDR = 0x648
    _fields_ = [('Data', c_uint, 32)]


class SShotTriggerFifoPayload(HddpsRegister):
    BAR = 1
    ADDR = 0x64c
    _fields_ = [('Data', c_uint, 32)]


class SShotHcLcEndTimestamp(HddpsRegister):
    BAR = 1
    ADDR = 0x650
    _fields_ = [('Data', c_uint, 32)]


class SShotVlcEndTimestamp(HddpsRegister):
    BAR = 1
    ADDR = 0x654
    _fields_ = [('Data', c_uint, 32)]


class SShotHclcEndSampleAddress(HddpsRegister):
    BAR = 1
    ADDR = 0x658
    _fields_ = [('SShotHclcSampleAddr', c_uint, 29),
                ('Unused1', c_uint, 2),
                ('SShotHclcSampleAddrRollover', c_uint, 1)]


class SShotVlcEndSampleAddress(HddpsRegister):
    BAR = 1
    ADDR = 0x65c
    _fields_ = [('SShotVlcSampleAddr', c_uint, 29),
                ('Unused1', c_uint, 2),
                ('SShotVlcSampleAddrRollover', c_uint, 1)]

class AD5560_TURBO_SAMPLE_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x660
    _fields_ = [('Data', c_uint, 32)]

class DUT_SAMPLE_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x664
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x668
    _fields_ = [('Data', c_uint, 32)]

class IFORCE_MODE_VOLTAGE_SHIFT(HddpsRegister):
    BAR = 1
    ADDR = 0x66c
    _fields_ = [('Data', c_uint, 32)]

class RCLK_PULSE_PERIOD(HddpsRegister):
    BAR = 1
    ADDR = 0x670
    _fields_ = [('Data', c_uint, 32)]

class HCLC_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x680
    _fields_ = [('Hclc0Alarms', c_uint, 1),
                ('Hclc1Alarms', c_uint, 1),
                ('Hclc2Alarms', c_uint, 1),
                ('Hclc3Alarms', c_uint, 1),
                ('Hclc4Alarms', c_uint, 1),
                ('Hclc5Alarms', c_uint, 1),
                ('Hclc6Alarms', c_uint, 1),
                ('Hclc7Alarms', c_uint, 1),
                ('Hclc8Alarms', c_uint, 1),
                ('Hclc9Alarms', c_uint, 1),
                ('Hclc10Alarms', c_uint, 1),
                ('Hclc11Alarms', c_uint, 1),
                ('Data', c_uint, 20)]


class HCLC_PER_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x684
    REGCOUNT = 10
    BASEMUL = 4
    _fields_ = [('ClampAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('ComparatorLowAlarm', c_uint, 1),
                ('ComparatorHighAlarm', c_uint, 1),
                ('VSlewRampAlarm', c_uint, 1),
                ('LtmTempAlarm', c_uint, 1),
                ('HcOscAlarm', c_uint, 1),
                ('LoopinginProgressAlarm', c_uint, 1),
                ('CorruptionAlarm', c_uint, 1),
                ('Reserved', c_uint, 4),
                ('RailBusyAlarm', c_uint, 1),
                ('ThermalClampAlarm', c_uint, 1),
                ('Data', c_uint, 17)]


class VlcRailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6b4
    _fields_ = [('Vlc00RailAlarms', c_uint, 1),
                ('Vlc01RailAlarms', c_uint, 1),
                ('Vlc02RailAlarms', c_uint, 1),
                ('Vlc03RailAlarms', c_uint, 1),
                ('Vlc04RailAlarms', c_uint, 1),
                ('Vlc05RailAlarms', c_uint, 1),
                ('Vlc06RailAlarms', c_uint, 1),
                ('Vlc07RailAlarms', c_uint, 1),
                ('Vlc08RailAlarms', c_uint, 1),
                ('Vlc09RailAlarms', c_uint, 1),
                ('Vlc10RailAlarms', c_uint, 1),
                ('Vlc11RailAlarms', c_uint, 1),
                ('Vlc12RailAlarms', c_uint, 1),
                ('Vlc13RailAlarms', c_uint, 1),
                ('Vlc14RailAlarms', c_uint, 1),
                ('Vlc15RailAlarms', c_uint, 1),
                ('Data', c_uint, 16)]


class Vlc00RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6b8
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc01RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6bc
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc02RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6c0
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc03RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6c4
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc04RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6c8
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc05RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6cc
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc06RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6d0
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc07RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6d4
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc08RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6d8
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc09RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6dc
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc10RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6e0
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc11RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6e4
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc12RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6e8
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc13RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6ec
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc14RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6f0
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class Vlc15RailAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x6f4
    _fields_ = [('ClampLoAlarm', c_uint, 1),
                ('ClampHiAlarm', c_uint, 1),
                ('KelvinAlarm', c_uint, 1),
                ('VlcLoopinginProgressAlarm', c_uint, 1),
                ('RailBusyAlarm', c_uint, 1),
                ('Data', c_uint, 27)]


class HclcCfoldAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x700
    _fields_ = [('Rail0Alarm', c_uint, 1),
                ('Rail1Alarm', c_uint, 1),
                ('Rail2Alarm', c_uint, 1),
                ('Rail3Alarm', c_uint, 1),
                ('Rail4Alarm', c_uint, 1),
                ('Rail5Alarm', c_uint, 1),
                ('Rail6Alarm', c_uint, 1),
                ('Rail7Alarm', c_uint, 1),
                ('Rail8Alarm', c_uint, 1),
                ('Rail9Alarm', c_uint, 1),
                ('Rail10Alarm', c_uint, 1),
                ('Rail11Alarm', c_uint, 1),
                ('Data', c_uint, 20)]


class HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(HddpsRegister):
    BAR = 1
    ADDR = 0x704
    REGCOUNT = 10
    BASEMUL = 4
    _fields_ = [('AssertTestData', c_uint, 8),
                ('AssertTestCmd', c_uint, 8),
                ('CFoldActualData', c_uint, 8),
                ('CFoldReasonCode', c_uint, 8)]


class VlcCfoldAlarms(HddpsRegister):
    BAR = 1
    ADDR = 0x734
    _fields_ = [('Rail0Alarm', c_uint, 1),
                ('Rail1Alarm', c_uint, 1),
                ('Rail2Alarm', c_uint, 1),
                ('Rail3Alarm', c_uint, 1),
                ('Rail4Alarm', c_uint, 1),
                ('Rail5Alarm', c_uint, 1),
                ('Rail6Alarm', c_uint, 1),
                ('Rail7Alarm', c_uint, 1),
                ('Rail8Alarm', c_uint, 1),
                ('Rail9Alarm', c_uint, 1),
                ('Rail10Alarm', c_uint, 1),
                ('Rail11Alarm', c_uint, 1),
                ('Rail12Alarm', c_uint, 1),
                ('Rail13Alarm', c_uint, 1),
                ('Rail14Alarm', c_uint, 1),
                ('Rail15Alarm', c_uint, 1),
                ('Data', c_uint, 16)]


class VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(HddpsRegister):
    BAR = 1
    ADDR = 0x738
    REGCOUNT = 16
    BASEMUL = 4
    _fields_ = [('AssertTestData', c_uint, 8),
                ('AssertTestCmd', c_uint, 8),
                ('CFoldActualData', c_uint, 8),
                ('CFoldReasonCode', c_uint, 8)]


class Hclc0ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x780
    _fields_ = [('Data', c_uint, 32)]


class Hclc1ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x784
    _fields_ = [('Data', c_uint, 32)]


class Hclc2ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x788
    _fields_ = [('Data', c_uint, 32)]


class Hclc3ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x78c
    _fields_ = [('Data', c_uint, 32)]


class Hclc4ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x790
    _fields_ = [('Data', c_uint, 32)]


class Hclc5ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x794
    _fields_ = [('Data', c_uint, 32)]


class Hclc6ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x798
    _fields_ = [('Data', c_uint, 32)]


class Hclc7ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x79c
    _fields_ = [('Data', c_uint, 32)]


class Hclc8ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x7a0
    _fields_ = [('Data', c_uint, 32)]


class Hclc9ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x7a4
    _fields_ = [('Data', c_uint, 32)]


class Hclc10ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x7a8
    _fields_ = [('Data', c_uint, 32)]


class Hclc11ClampAlarmCurrent(HddpsRegister):
    BAR = 1
    ADDR = 0x7ac
    _fields_ = [('Data', c_uint, 32)]


class Vlc0LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7c0
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc1LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7c4
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc2LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7c8
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc3LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7cc
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc4LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7d0
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc5LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7d4
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc6LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7d8
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc7LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7dc
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc8LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7e0
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc9LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7e4
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc10LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7e8
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc11LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7ec
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc12LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7f0
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc13LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7f4
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc14LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7f8
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class Vlc15LastCurrentRange(HddpsRegister):
    BAR = 1
    ADDR = 0x7fc
    _fields_ = [('VlcIRangeValue', c_uint, 4),
                ('Data', c_uint, 28)]


class P2PCtrl(HddpsRegister):
    BAR = 1
    ADDR = 0x800
    _fields_ = [('Data', c_uint, 30),
                ('P2PAddrType', c_uint, 1),
                ('P2PEn', c_uint, 1)]


class S2HControlReg(HddpsRegister):
    BAR = 1
    ADDR = 0x804
    _fields_ = [('S2HStreamingStart', c_uint, 1),
                ('S2HStreamingStop', c_uint, 1),
                ('S2HStreamingReset', c_uint, 1),
                ('Reserved', c_uint, 28),
                ('S2HStreamingStatus', c_uint, 1)]


class P2PStartAddrHi(HddpsRegister):
    BAR = 1
    ADDR = 0x808
    _fields_ = [('Data', c_uint, 32)]


class P2PStartAddrLo(HddpsRegister):
    BAR = 1
    ADDR = 0x80c
    _fields_ = [('Data', c_uint, 32)]


class S2HAddressHighReg(HddpsRegister):
    BAR = 1
    ADDR = 0x810
    _fields_ = [('S2HAddressHigh', c_uint, 32)]


class S2HAddressLowReg(HddpsRegister):
    BAR = 1
    ADDR = 0x814
    _fields_ = [('S2HAddressLow', c_uint, 32)]


class S2HValidPacketIndexReg(HddpsRegister):
    BAR = 1
    ADDR = 0x818
    _fields_ = [('S2HValidPacketIndex', c_uint, 16),
                ('Reserved', c_uint, 16)]


class S2HPacketByteCountReg(HddpsRegister):
    BAR = 1
    ADDR = 0x81c
    _fields_ = [('S2HPacketByteCount', c_uint, 16),
                ('Reserved', c_uint, 16)]


class S2HPacketCountReg(HddpsRegister):
    BAR = 1
    ADDR = 0x820
    _fields_ = [('S2HPacketCount', c_uint, 16),
                ('Reserved', c_uint, 16)]


class S2HConfigurationReg(HddpsRegister):
    BAR = 1
    ADDR = 0x824
    _fields_ = [('S2HConfigurationDone', c_uint, 1),
                ('Reserved', c_uint, 31)]

class ECA_CRITICAL_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0x828
    _fields_ = [('Data', c_uint, 32)]

class ECA_LIMIT_HI_FLOAT(HddpsRegister):
    BAR = 1
    ADDR = 0x82c
    _fields_ = [('Data', c_uint, 32)]

class ECA_LIMIT_LO_FLOAT(HddpsRegister):
    BAR = 1
    ADDR = 0x830
    _fields_ = [('Data', c_uint, 32)]


class HclcUhcSampleHeaderRegionBase(HddpsRegister):
    BAR = 1
    ADDR = 0x880
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('HeaderRegionBase', c_uint, 32)]


class HclcUhcSampleHeaderRegionSize(HddpsRegister):
    BAR = 1
    ADDR = 0x884
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('HeaderRegionSize', c_uint, 32)]


class HclcUhcSampleRegionBase(HddpsRegister):
    BAR = 1
    ADDR = 0x888
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('SampleRegionBase', c_uint, 32)]


class HclcUhcSampleRegionSize(HddpsRegister):
    BAR = 1
    ADDR = 0x88c
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('SampleRegionSize', c_uint, 32)]


class VlcUhcSampleHeaderRegionBase(HddpsRegister):
    BAR = 1
    ADDR = 0x900
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('HeaderRegionBase', c_uint, 32)]


class VlcUhcSampleHeaderRegionSize(HddpsRegister):
    BAR = 1
    ADDR = 0x904
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('HeaderRegionSize', c_uint, 32)]


class VlcUhcSampleRegionBase(HddpsRegister):
    BAR = 1
    ADDR = 0x908
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('SampleRegionBase', c_uint, 32)]


class VlcUhcSampleRegionSize(HddpsRegister):
    BAR = 1
    ADDR = 0x90c
    REGCOUNT = 8
    BASEMUL = 16
    _fields_ = [('SampleRegionSize', c_uint, 32)]



class MAX6627_VICOR_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x980
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_THERMAL_ADC_UCL(HddpsRegister):
    BAR = 1
    ADDR = 0x984
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_BJT_SENSOR_UCL(HddpsRegister):
    BAR = 1
    ADDR = 0x988
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_VICOR_UCL(HddpsRegister):
    BAR = 1
    ADDR = 0x98c
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_THERMAL_ADC_LCL(HddpsRegister):
    BAR = 1
    ADDR = 0x990
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_BJT_SENSOR_LCL(HddpsRegister):
    BAR = 1
    ADDR = 0x994
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_VICOR_LCL(HddpsRegister):
    BAR = 1
    ADDR = 0x998
    _fields_ = [('Data', c_uint, 32)]

class MAX6627_TEMPERATURE(HddpsRegister):
    BAR = 1
    ADDR = 0x99c
    _fields_ = [('Data', c_uint, 32)]

class LTC2450_ADC_VOLTAGE(HddpsRegister):
    BAR = 1
    ADDR = 0x9a0
    _fields_ = [('Data', c_uint, 32)]

class HVDPS_GPIO(HddpsRegister):
    BAR = 1
    ADDR = 0x9a4
    _fields_ = [('Data', c_uint, 32)]

class OV_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0x9b0
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0x9c0
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0x9c4
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0x9c8
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0x9cc
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0x9d0
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0x9d4
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0x9d8
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0x9dc
    _fields_ = [('Data', c_uint, 32)]

class HV_V_ADC_SOFTSPAN_CODE(HddpsRegister):
    BAR = 1
    ADDR = 0xa00
    _fields_ = [('Data', c_uint, 32)]

class HV_I_ADC_SOFTSPAN_CODE(HddpsRegister):
    BAR = 1
    ADDR = 0xa04
    _fields_ = [('Data', c_uint, 32)]

class LV_V_ADC_SOFTSPAN_CODE(HddpsRegister):
    BAR = 1
    ADDR = 0xa08
    _fields_ = [('Data', c_uint, 32)]

class DUT_00_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa30
    _fields_ = [('Data', c_uint, 32)]

class DUT_01_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa34
    _fields_ = [('Data', c_uint, 32)]

class DUT_02_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa38
    _fields_ = [('Data', c_uint, 32)]

class DUT_03_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa3c
    _fields_ = [('Data', c_uint, 32)]

class DUT_04_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa40
    _fields_ = [('Data', c_uint, 32)]

class DUT_05_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa44
    _fields_ = [('Data', c_uint, 32)]

class DUT_06_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa48
    _fields_ = [('Data', c_uint, 32)]

class DUT_07_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa4c
    _fields_ = [('Data', c_uint, 32)]

class DUT_08_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa50
    _fields_ = [('Data', c_uint, 32)]

class DUT_09_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xa54
    _fields_ = [('Data', c_uint, 32)]

class DUT_00_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa58
    _fields_ = [('Data', c_uint, 32)]

class DUT_01_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa5c
    _fields_ = [('Data', c_uint, 32)]

class DUT_02_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa60
    _fields_ = [('Data', c_uint, 32)]

class DUT_03_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa64
    _fields_ = [('Data', c_uint, 32)]

class DUT_04_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa68
    _fields_ = [('Data', c_uint, 32)]

class DUT_05_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa6c
    _fields_ = [('Data', c_uint, 32)]

class DUT_06_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa70
    _fields_ = [('Data', c_uint, 32)]

class DUT_07_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa74
    _fields_ = [('Data', c_uint, 32)]

class DUT_08_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa78
    _fields_ = [('Data', c_uint, 32)]

class DUT_09_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xa7c
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail00_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa80
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail01_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa84
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail02_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa88
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail03_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa8c
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail04_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa90
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail05_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa94
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail06_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa98
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail07_v(HddpsRegister):
    BAR = 1
    ADDR = 0xa9c
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail08_v(HddpsRegister):
    BAR = 1
    ADDR = 0xaa0
    _fields_ = [('Data', c_uint, 32)]

class DUT_rail09_v(HddpsRegister):
    BAR = 1
    ADDR = 0xaa4
    _fields_ = [('Data', c_uint, 32)]

class DUT_turbo_v(HddpsRegister):
    BAR = 1
    ADDR = 0xaa8
    _fields_ = [('Data', c_uint, 32)]

class DPS_turbo_v(HddpsRegister):
    BAR = 1
    ADDR = 0xaac
    _fields_ = [('Data', c_uint, 32)]

class DPS_turbo_i(HddpsRegister):
    BAR = 1
    ADDR = 0xab0
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail00_v(HddpsRegister):
    BAR = 1
    ADDR = 0xac0
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail01_v(HddpsRegister):
    BAR = 1
    ADDR = 0xac4
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail02_v(HddpsRegister):
    BAR = 1
    ADDR = 0xac8
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail03_v(HddpsRegister):
    BAR = 1
    ADDR = 0xacc
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail04_v(HddpsRegister):
    BAR = 1
    ADDR = 0xad0
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail05_v(HddpsRegister):
    BAR = 1
    ADDR = 0xad4
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail06_v(HddpsRegister):
    BAR = 1
    ADDR = 0xad8
    _fields_ = [('Data', c_uint, 32)]

class LTM_MV_rail07_v(HddpsRegister):
    BAR = 1
    ADDR = 0xadc
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC0_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb00
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC0_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb04
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC0_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb08
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC0_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb0c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC1_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb10
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC1_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb14
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC1_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb18
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC1_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb1c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC2_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb20
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC2_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb24
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC2_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb28
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC2_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb2c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC3_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb30
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC3_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb34
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC3_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb38
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC3_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb3c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC4_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb40
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC4_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb44
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC4_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb48
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC4_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb4c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC5_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb50
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC5_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb54
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC5_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb58
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC5_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb5c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC6_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb60
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC6_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb64
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC6_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb68
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC6_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb6c
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC7_HEADER_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb70
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC7_HEADER_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb74
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC7_SAMPLE_REGION_BASE(HddpsRegister):
    BAR = 1
    ADDR = 0xb78
    _fields_ = [('Data', c_uint, 32)]

class DUT_UHC7_SAMPLE_REGION_SIZE(HddpsRegister):
    BAR = 1
    ADDR = 0xb7c
    _fields_ = [('Data', c_uint, 32)]

class DUT_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb80
    _fields_ = [('Data', c_uint, 32)]

class DUT_00_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb84
    _fields_ = [('Data', c_uint, 32)]

class DUT_01_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb88
    _fields_ = [('Data', c_uint, 32)]

class DUT_02_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb8c
    _fields_ = [('Data', c_uint, 32)]

class DUT_03_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb90
    _fields_ = [('Data', c_uint, 32)]

class DUT_04_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb94
    _fields_ = [('Data', c_uint, 32)]

class DUT_05_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb98
    _fields_ = [('Data', c_uint, 32)]

class DUT_06_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xb9c
    _fields_ = [('Data', c_uint, 32)]

class DUT_07_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xba0
    _fields_ = [('Data', c_uint, 32)]

class DUT_08_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xba4
    _fields_ = [('Data', c_uint, 32)]

class DUT_09_RAIL_ALARMS(HddpsRegister):
    BAR = 1
    ADDR = 0xba8
    _fields_ = [('Data', c_uint, 32)]

class ADC_ENABLE(HddpsRegister):
    BAR = 1
    ADDR = 0xbac
    _fields_ = [('Data', c_uint, 32)]

class HV_SWITCH_EN_STATUS(HddpsRegister):
    BAR = 1
    ADDR = 0xbbc
    _fields_ = [('Data', c_uint, 32)]

class AD5560_00_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbc0
    _fields_ = [('Data', c_uint, 32)]

class AD5560_01_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbc4
    _fields_ = [('Data', c_uint, 32)]

class AD5560_02_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbc8
    _fields_ = [('Data', c_uint, 32)]

class AD5560_03_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbcc
    _fields_ = [('Data', c_uint, 32)]

class AD5560_04_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbd0
    _fields_ = [('Data', c_uint, 32)]

class AD5560_05_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbd4
    _fields_ = [('Data', c_uint, 32)]

class AD5560_06_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbd8
    _fields_ = [('Data', c_uint, 32)]

class AD5560_07_VOLTAGE_LIMIT_SS3(HddpsRegister):
    BAR = 1
    ADDR = 0xbdc
    _fields_ = [('Data', c_uint, 32)]

class AD5560_00_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbe0
    _fields_ = [('Data', c_uint, 32)]

class AD5560_01_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbe4
    _fields_ = [('Data', c_uint, 32)]

class AD5560_02_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbe8
    _fields_ = [('Data', c_uint, 32)]

class AD5560_03_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbec
    _fields_ = [('Data', c_uint, 32)]

class AD5560_04_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbf0
    _fields_ = [('Data', c_uint, 32)]

class AD5560_05_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbf4
    _fields_ = [('Data', c_uint, 32)]

class AD5560_06_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbf8
    _fields_ = [('Data', c_uint, 32)]

class AD5560_07_VOLTAGE_LIMIT_SS7(HddpsRegister):
    BAR = 1
    ADDR = 0xbfc
    _fields_ = [('Data', c_uint, 32)]

class HCLC_00_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc00
    _fields_ = [('Data', c_uint, 32)]

class HCLC_01_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc04
    _fields_ = [('Data', c_uint, 32)]

class HCLC_02_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc08
    _fields_ = [('Data', c_uint, 32)]

class HCLC_03_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc0c
    _fields_ = [('Data', c_uint, 32)]

class HCLC_04_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc10
    _fields_ = [('Data', c_uint, 32)]

class HCLC_05_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc14
    _fields_ = [('Data', c_uint, 32)]

class HCLC_06_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc18
    _fields_ = [('Data', c_uint, 32)]

class HCLC_07_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc1c
    _fields_ = [('Data', c_uint, 32)]

class HCLC_08_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc20
    _fields_ = [('Data', c_uint, 32)]

class HCLC_09_USER_I_H_CLAMP(HddpsRegister):
    BAR = 1
    ADDR = 0xc24
    _fields_ = [('Data', c_uint, 32)]

class LTM_00_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc40
    _fields_ = [('Data', c_uint, 32)]

class LTM_01_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc44
    _fields_ = [('Data', c_uint, 32)]

class LTM_02_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc48
    _fields_ = [('Data', c_uint, 32)]

class LTM_03_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc4c
    _fields_ = [('Data', c_uint, 32)]

class LTM_04_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc50
    _fields_ = [('Data', c_uint, 32)]

class LTM_05_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc54
    _fields_ = [('Data', c_uint, 32)]

class LTM_06_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc58
    _fields_ = [('Data', c_uint, 32)]

class LTM_07_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc5c
    _fields_ = [('Data', c_uint, 32)]

class LTM_08_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc60
    _fields_ = [('Data', c_uint, 32)]

class LTM_09_TEMP_ALARM_EVENT_COUNT(HddpsRegister):
    BAR = 1
    ADDR = 0xc64
    _fields_ = [('Data', c_uint, 32)]

class LTM_TEMP_ALARM_THRESHOLD(HddpsRegister):
    BAR = 1
    ADDR = 0xc68
    _fields_ = [('Data', c_uint, 32)]






class Scratch(HddpsRegister):
    BAR = 2
    ADDR = 0x0
    _fields_ = [('Data', c_uint, 32)]


class HclcSampleEngineReset(HddpsRegister):
    BAR = 2
    ADDR = 0x10
    _fields_ = [('Uhc0Reset', c_uint, 1),
                ('Uhc1Reset', c_uint, 1),
                ('Uhc2Reset', c_uint, 1),
                ('Uhc3Reset', c_uint, 1),
                ('Uhc4Reset', c_uint, 1),
                ('Uhc5Reset', c_uint, 1),
                ('Uhc6Reset', c_uint, 1),
                ('Uhc7Reset', c_uint, 1),
                ('Reserved', c_uint, 24)]


class HclcSamplingActive(HddpsRegister):
    BAR = 2
    ADDR = 0x14
    _fields_ = [('ActiveDut', c_uint, 8),
                ('Data', c_uint, 24)]

class AD5560_TURBO_SAMPLE_ENGINE_RESET(HddpsRegister):
    BAR = 2
    ADDR = 0x18
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLING_ACTIVE(HddpsRegister):
    BAR = 2
    ADDR = 0x1c
    _fields_ = [('Data', c_uint, 32)]

class VlcSampleEngineReset(HddpsRegister):
    BAR = 2
    ADDR = 0x20
    _fields_ = [('ResetDut', c_uint, 8),
                ('Data', c_uint, 24)]


class VlcSamplingActive(HddpsRegister):
    BAR = 2
    ADDR = 0x24
    _fields_ = [('ActiveDut', c_uint, 8),
                ('Data', c_uint, 24)]

class DUT_SAMPLE_ENGINE_RESET(HddpsRegister):
    BAR = 2
    ADDR = 0x28
    _fields_ = [('Data', c_uint, 32)]

class DUT_SAMPLING_ACTIVE(HddpsRegister):
    BAR = 2
    ADDR = 0x2c
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_ENGINE_RESET(HddpsRegister):
    BAR = 2
    ADDR = 0x30
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLING_ACTIVE(HddpsRegister):
    BAR = 2
    ADDR = 0x34
    _fields_ = [('Data', c_uint, 32)]

class CAPABILITIES(HddpsRegister):
    BAR = 2
    ADDR = 0x7c
    _fields_ = [('Data', c_uint, 32)]

class Hclc0SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x80
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc0SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x84
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc0SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x88
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc0SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x8c
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc0SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x90
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc0SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x94
    _fields_ = [('Data', c_uint, 32)]


class Hclc0SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x98
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_0_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x9c
    _fields_ = [('Data', c_uint, 32)]

class Hclc1SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0xa0
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc1SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0xa4
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc1SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0xa8
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc1SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0xac
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc1SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0xb0
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc1SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0xb4
    _fields_ = [('Data', c_uint, 32)]


class Hclc1SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0xb8
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_1_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0xbc
    _fields_ = [('Data', c_uint, 32)]

class Hclc2SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0xc0
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc2SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0xc4
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc2SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0xc8
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc2SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0xcc
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc2SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0xd0
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc2SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0xd4
    _fields_ = [('Data', c_uint, 32)]


class Hclc2SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0xd8
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_2_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0xdc
    _fields_ = [('Data', c_uint, 32)]

class Hclc3SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0xe0
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc3SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0xe4
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc3SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0xe8
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc3SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0xec
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc3SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0xf0
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc3SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0xf4
    _fields_ = [('Data', c_uint, 32)]


class Hclc3SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0xf8
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_3_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0xfc
    _fields_ = [('Data', c_uint, 32)]

class Hclc4SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x100
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc4SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x104
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc4SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x108
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc4SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x10c
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc4SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x110
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc4SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x114
    _fields_ = [('Data', c_uint, 32)]


class Hclc4SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x118
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_4_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x11c
    _fields_ = [('Data', c_uint, 32)]

class Hclc5SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x120
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc5SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x124
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc5SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x128
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc5SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x12c
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc5SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x130
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc5SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x134
    _fields_ = [('Data', c_uint, 32)]


class Hclc5SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x138
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_5_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x13c
    _fields_ = [('Data', c_uint, 32)]

class Hclc6SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x140
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc6SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x144
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc6SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x148
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc6SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x14c
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc6SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x150
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc6SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x154
    _fields_ = [('Data', c_uint, 32)]


class Hclc6SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x158
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_6_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x15c
    _fields_ = [('Data', c_uint, 32)]

class Hclc7SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x160
    _fields_ = [('SampleCount', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc7SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x164
    _fields_ = [('DiscardSamples', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc7SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x168
    _fields_ = [('MetaHiData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc7SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x16c
    _fields_ = [('MetaLoData', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc7SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x170
    _fields_ = [('MaskRails', c_uint, 16),
                ('Data', c_uint, 16)]


class Hclc7SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x174
    _fields_ = [('Data', c_uint, 32)]


class Hclc7SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x178
    _fields_ = [('StartSamplingEngine', c_uint, 16),
                ('Data', c_uint, 16)]

class HCLC_7_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x17c
    _fields_ = [('Data', c_uint, 32)]

class Vlc0SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x180
    _fields_ = [('Data', c_uint, 32)]


class Vlc0SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x184
    _fields_ = [('Data', c_uint, 32)]


class Vlc0SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x188
    _fields_ = [('Data', c_uint, 32)]


class Vlc0SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x18c
    _fields_ = [('Data', c_uint, 32)]


class Vlc0SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x190
    _fields_ = [('Data', c_uint, 32)]


class Vlc0SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x194
    _fields_ = [('Data', c_uint, 32)]


class Vlc0SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x198
    _fields_ = [('Data', c_uint, 32)]

class VLC_0_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x19c
    _fields_ = [('Data', c_uint, 32)]

class Vlc1SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x1a0
    _fields_ = [('Data', c_uint, 32)]


class Vlc1SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x1a4
    _fields_ = [('Data', c_uint, 32)]


class Vlc1SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x1a8
    _fields_ = [('Data', c_uint, 32)]


class Vlc1SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x1ac
    _fields_ = [('Data', c_uint, 32)]


class Vlc1SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x1b0
    _fields_ = [('Data', c_uint, 32)]


class Vlc1SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x1b4
    _fields_ = [('Data', c_uint, 32)]


class Vlc1SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x1b8
    _fields_ = [('Data', c_uint, 32)]

class VLC_1_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x1bc
    _fields_ = [('Data', c_uint, 32)]

class Vlc2SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x1c0
    _fields_ = [('Data', c_uint, 32)]


class Vlc2SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x1c4
    _fields_ = [('Data', c_uint, 32)]


class Vlc2SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x1c8
    _fields_ = [('Data', c_uint, 32)]


class Vlc2SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x1cc
    _fields_ = [('Data', c_uint, 32)]


class Vlc2SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x1d0
    _fields_ = [('Data', c_uint, 32)]


class Vlc2SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x1d4
    _fields_ = [('Data', c_uint, 32)]


class Vlc2SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x1d8
    _fields_ = [('Data', c_uint, 32)]

class VLC_2_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x1dc
    _fields_ = [('Data', c_uint, 32)]

class Vlc3SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x1e0
    _fields_ = [('Data', c_uint, 32)]


class Vlc3SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x1e4
    _fields_ = [('Data', c_uint, 32)]


class Vlc3SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x1e8
    _fields_ = [('Data', c_uint, 32)]


class Vlc3SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x1ec
    _fields_ = [('Data', c_uint, 32)]


class Vlc3SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x1f0
    _fields_ = [('Data', c_uint, 32)]


class Vlc3SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x1f4
    _fields_ = [('Data', c_uint, 32)]


class Vlc3SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x1f8
    _fields_ = [('Data', c_uint, 32)]

class VLC_3_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x1fc
    _fields_ = [('Data', c_uint, 32)]

class Vlc4SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x200
    _fields_ = [('Data', c_uint, 32)]


class Vlc4SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x204
    _fields_ = [('Data', c_uint, 32)]


class Vlc4SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x208
    _fields_ = [('Data', c_uint, 32)]


class Vlc4SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x20c
    _fields_ = [('Data', c_uint, 32)]


class Vlc4SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x210
    _fields_ = [('Data', c_uint, 32)]


class Vlc4SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x214
    _fields_ = [('Data', c_uint, 32)]


class Vlc4SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x218
    _fields_ = [('Data', c_uint, 32)]

class VLC_4_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x21c
    _fields_ = [('Data', c_uint, 32)]

class Vlc5SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x220
    _fields_ = [('Data', c_uint, 32)]


class Vlc5SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x224
    _fields_ = [('Data', c_uint, 32)]


class Vlc5SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x228
    _fields_ = [('Data', c_uint, 32)]


class Vlc5SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x22c
    _fields_ = [('Data', c_uint, 32)]


class Vlc5SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x230
    _fields_ = [('Data', c_uint, 32)]


class Vlc5SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x234
    _fields_ = [('Data', c_uint, 32)]


class Vlc5SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x238
    _fields_ = [('Data', c_uint, 32)]

class VLC_5_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x23c
    _fields_ = [('Data', c_uint, 32)]

class Vlc6SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x240
    _fields_ = [('Data', c_uint, 32)]


class Vlc6SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x244
    _fields_ = [('Data', c_uint, 32)]


class Vlc6SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x248
    _fields_ = [('Data', c_uint, 32)]


class Vlc6SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x24c
    _fields_ = [('Data', c_uint, 32)]


class Vlc6SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x250
    _fields_ = [('Data', c_uint, 32)]


class Vlc6SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x254
    _fields_ = [('Data', c_uint, 32)]


class Vlc6SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x258
    _fields_ = [('Data', c_uint, 32)]

class VLC_6_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x25c
    _fields_ = [('Data', c_uint, 32)]

class Vlc7SampleCount(HddpsRegister):
    BAR = 2
    ADDR = 0x260
    _fields_ = [('Data', c_uint, 32)]


class Vlc7SampleRate(HddpsRegister):
    BAR = 2
    ADDR = 0x264
    _fields_ = [('Data', c_uint, 32)]


class Vlc7SampleMetadataHi(HddpsRegister):
    BAR = 2
    ADDR = 0x268
    _fields_ = [('Data', c_uint, 32)]


class Vlc7SampleMetadataLo(HddpsRegister):
    BAR = 2
    ADDR = 0x26c
    _fields_ = [('Data', c_uint, 32)]


class Vlc7SampleRailSelect(HddpsRegister):
    BAR = 2
    ADDR = 0x270
    _fields_ = [('Data', c_uint, 32)]


class Vlc7SampleDelay(HddpsRegister):
    BAR = 2
    ADDR = 0x274
    _fields_ = [('Data', c_uint, 32)]


class Vlc7SampleStart(HddpsRegister):
    BAR = 2
    ADDR = 0x278
    _fields_ = [('Data', c_uint, 32)]

class VLC_7_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x27c
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x280
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x284
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x288
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x28c
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x290
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x294
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x298
    _fields_ = [('Data', c_uint, 32)]

class DUT_0_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x29c
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x2a0
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x2a4
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x2a8
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x2ac
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x2b0
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x2b4
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x2b8
    _fields_ = [('Data', c_uint, 32)]

class DUT_1_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x2bc
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x2c0
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x2c4
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x2c8
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x2cc
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x2d0
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x2d4
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x2d8
    _fields_ = [('Data', c_uint, 32)]

class DUT_2_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x2dc
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x2e0
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x2e4
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x2e8
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x2ec
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x2f0
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x2f4
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x2f8
    _fields_ = [('Data', c_uint, 32)]

class DUT_3_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x2fc
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x300
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x304
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x308
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x30c
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x310
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x314
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x318
    _fields_ = [('Data', c_uint, 32)]

class DUT_4_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x31c
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x320
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x324
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x328
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x32c
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x330
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x334
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x338
    _fields_ = [('Data', c_uint, 32)]

class DUT_5_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x33c
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x340
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x344
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x348
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x34c
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x350
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x354
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x358
    _fields_ = [('Data', c_uint, 32)]

class DUT_6_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x35c
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x360
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x364
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x368
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x36c
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x370
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x374
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x378
    _fields_ = [('Data', c_uint, 32)]

class DUT_7_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x37c
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x380
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x384
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x388
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x38c
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x390
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x394
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x398
    _fields_ = [('Data', c_uint, 32)]

class AD5560_TURBO_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x39c
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_COUNT(HddpsRegister):
    BAR = 2
    ADDR = 0x3a0
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_RATE(HddpsRegister):
    BAR = 2
    ADDR = 0x3a4
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_METADATA_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x3a8
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_METADATA_LO(HddpsRegister):
    BAR = 2
    ADDR = 0x3ac
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_RAIL_SELECT(HddpsRegister):
    BAR = 2
    ADDR = 0x3b0
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_DELAY(HddpsRegister):
    BAR = 2
    ADDR = 0x3b4
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_START(HddpsRegister):
    BAR = 2
    ADDR = 0x3b8
    _fields_ = [('Data', c_uint, 32)]

class DUT_TURBO_SAMPLE_SSHOT_COUNT_HI(HddpsRegister):
    BAR = 2
    ADDR = 0x3bc
    _fields_ = [('Data', c_uint, 32)]
	
# structures
class AssertCommand(HddpsStruct):
    _fields_ = [('TestData', c_uint, 8),
                ('AssertTest', c_uint, 8),
                ('RailNumber', c_uint, 8),
                ('RailCommand', c_uint, 7),
                ('ReadWrite', c_uint, 1)]


class Bar2RailCommand(HddpsStruct):
    _fields_ = [('RailNumberOrResourceId', c_uint, 8),
                ('RailCommand', c_uint, 7)]


class SampleHeaderStruct(HddpsStruct):
    _fields_ = [('SamplePointer', c_uint, 32),
                ('SampleRate', c_uint, 16),
                ('SampleCount', c_uint, 16),
                ('RailSelect', c_uint, 32),
                ('MetaLo', c_uint, 16),
                ('MetaHi', c_uint, 16)]