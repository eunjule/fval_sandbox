################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
from Common import configs
from Common.instruments.dps.dps import DpsBase
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo
from Common.instruments.dps import hvdps_registers
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps.hvilSubslot import HvilSubslot



class HvdpsCombo(DpsBase):
    # CALIBRATION CONSTANTS
    # will need to verify this with HIL
    U3 = 0  # Force and Sense Select
    U11 = 1  # Sense and Load Resistor Select
    U1 = 2  # External Connections and Fan/Temp Fail

    IN_PORT_OFFSET = 0x00
    OUT_PORT_OFFSET = 0x08
    PI_PORT_OFFSET = 0x10
    CFG_PORT_OFFSET = 0x18
    MASK_PORT_OFFSET = 0x20
    # ------------------------------
    # Force_Sel = [Chip,Bank,I/O]

    ForceList = []
    ForceList.append([U3, 0, 5])  # HV0
    ForceList.append([U3, 0, 4])
    ForceList.append([U3, 0, 3])
    ForceList.append([U3, 0, 2])
    ForceList.append([U3, 0, 1])
    ForceList.append([U3, 0, 0])
    ForceList.append([U3, 0, 6])
    ForceList.append([U3, 0, 7])  # HV7

    # Sense_Sel = [Chip,Bank,I/O]
    SenseList = []
    SenseList.append([U3, 3, 0])  # HV0
    SenseList.append([U3, 3, 5])
    SenseList.append([U3, 3, 6])
    SenseList.append([U3, 3, 3])
    SenseList.append([U3, 3, 2])
    SenseList.append([U3, 3, 7])
    SenseList.append([U3, 3, 1])
    SenseList.append([U3, 3, 4])  # HV7

    # Low Voltage Sense Select
    LvSenseList = []
    LvSenseList.append([U3, 2, 0])  # LV CH0
    LvSenseList.append([U3, 2, 1])
    LvSenseList.append([U3, 2, 2])
    LvSenseList.append([U3, 2, 3])
    LvSenseList.append([U3, 2, 4])
    LvSenseList.append([U3, 2, 5])
    LvSenseList.append([U3, 2, 6])
    LvSenseList.append([U3, 2, 7])
    LvSenseList.append([U3, 4, 1])
    LvSenseList.append([U3, 4, 2])  # LV CH9

    # LoopBack Channel Select
    # These were adjusted due to the design issue that swizzled the channels
    LbChannelList = []
    LbChannelList.append([U3, 1, 0])  # LB CH0
    LbChannelList.append([U3, 1, 7])
    LbChannelList.append([U3, 1, 6])
    LbChannelList.append([U3, 1, 5])
    LbChannelList.append([U3, 1, 4])
    LbChannelList.append([U3, 1, 3])
    LbChannelList.append([U3, 1, 2])
    LbChannelList.append([U3, 1, 1])  # LB CH7

    # Res_Sel: 'Bus':[Chip,Bank,I/O]
    res_dict = {}
    res_dict['0.4Ohm'] = {'exc': [U11, 0, 2], 'force': [U11, 3, 0], 'sense': [U11, 0, 0]}
    res_dict['0.8Ohm'] = {'exc': [U11, 0, 3], 'force': [U11, 3, 4], 'sense': [U11, 0, 1]}
    res_dict['4.7Ohm'] = {'exc': [U11, 0, 4], 'force': [U11, 3, 6], 'sense': [U11, 0, 5]}
    res_dict['10Ohm'] = {'exc': [U11, 0, 7], 'force': [U11, 4, 0], 'sense': [U11, 0, 6]}
    res_dict['15Ohm'] = {'exc': [U11, 1, 0], 'force': [U11, 4, 1], 'sense': [U11, 4, 2]}
    res_dict['33Ohm'] = {'exc': [U11, 1, 1], 'force': [U11, 1, 3], 'sense': [U11, 4, 3]}
    res_dict['100Ohm'] = {'exc': [U11, 1, 2], 'force': [U11, 1, 4], 'sense': [U11, 1, 5]}
    res_dict['1000Ohm'] = {'exc': [U11, 1, 7], 'force': [U11, 1, 6], 'sense': [U11, 2, 0]}
    res_dict['10000Ohm'] = {'exc': [U11, 2, 1], 'force': [U11, 4, 7], 'sense': [U11, 4, 5]}
    res_dict['100000Ohm'] = {'exc': [U11, 2, 5], 'force': [U11, 4, 4], 'sense': [U11, 2, 2]}
    res_dict['1000000Ohm'] = {'exc': [U11, 2, 6], 'force': [U11, 4, 6], 'sense': [U11, 2, 3]}

    # Ext_Sel: 'Connection':[Chip,Bank,I/O]
    ext_dict = {}
    ext_dict['Exc_DMM_Null'] = [U1, 0,
                                0]  # Connects common sense bus to the DMM exc bus to allow for high voltage measurement
    ext_dict['DMM_Sense'] = [U1, 0, 1]
    ext_dict['DMM_Exc'] = [U1, 0, 2]
    ext_dict['Ext_Load'] = [U1, 0, 3]  # Sense connection for ext load
    ext_dict['DMM_Short'] = [U1, 0, 4]  # short to ground
    ext_dict['No_Load_Force'] = [U1, 0, 5]
    ext_dict['Cross_Brd_Gang'] = [U1, 0, 6]
    ext_dict['DB_Cal'] = [U3, 4, 0]

    registers = hvdps_registers

    def __init__(self, slot, rc, name=None):
        super().__init__(slot, rc, name)
        self.slot = slot
        mainBoard = HvdpsSubslot(self, self.slot, 0)
        self.subslots.append(mainBoard)
        self.instrumentblt = CachingBltInfo(callback_function=self.ReadInstrumentBlt,
                                            name='HVDPS Instrument slot {}'.format(slot))
        self.hvdpsboardblt = CachingBltInfo(callback_function=self.ReadHvdpsBoardBlt,
                                            name='HVDPS Board slot {}'.format(slot))
        self.hvilboardblt = CachingBltInfo(callback_function=self.ReadHvilBoardBlt,
                                            name='HVIL Board slot {}'.format(slot))
        self.calboardblt = CachingBltInfo(callback_function=self.ReadCalCardBoardBlt,
                                       name='HVDPS Cal Board slot {}'.format(slot))

    def discover(self):
        ss0Found = False

        slot = self.slot
        daughterBoard = HvilSubslot(self, self.slot, 1)

        try:
            hil.hvdpsConnect(slot)
            hil.hvdpsDisconnect(slot)
            ss0Found = True
            try:
                hil.hvilConnect(slot)
                hil.hvilDisconnect(slot)

                self.subslots.append(daughterBoard)
            except RuntimeError:
                pass
        except RuntimeError:
             pass

        if ss0Found == True:
            return [self]

        else:
            return []

    def ReadInstrumentBlt(self):
        return hil.dpsBltInstrumentRead(self.slot)

    def ReadHvdpsBoardBlt(self):
        return hil.dpsBltBoardRead(self.slot,0)

    def ReadHvilBoardBlt(self):
        return hil.dpsBltBoardRead(self.slot, 1)

    def CalCardConnect(self):
        hil.calHvdpsConnect(self.slot)

    def CalCardInitialize(self):
        slot = self.slot
        # will need to verify this with HIL
        U3 = 0  # Force and Sense Select
        U11 = 1  # Sense and Load Resistor Select
        U1 = 2  # External Connections and Fan/Temp Fail

        OUT_PORT_OFFSET = 0x08
        CFG_PORT_OFFSET = 0x18
        for chip in [U3, U11]:
            for bank in range(0, 5):
                # Set Up all I/Os as Outputs on U3 and U11 and set them to 0
                hil.calHvdpsPca9505Write(slot, chip, CFG_PORT_OFFSET + bank, 0x00)
                hil.calHvdpsPca9505Write(slot, chip, OUT_PORT_OFFSET + bank, 0xFF)

        # Setup Bank 0 of U1 as Output and set them to 0
        bank = 0
        hil.calHvdpsPca9505Write(slot, U1, CFG_PORT_OFFSET + bank, 0x00)
        hil.calHvdpsPca9505Write(slot, U1, OUT_PORT_OFFSET + bank, 0xFF)

        # Setup Bank 3,4 of U1 as Input
        for bank in range(3, 5):
            hil.calHvdpsPca9505Write(slot, U1, CFG_PORT_OFFSET + bank, 0xFF)

    def ReadCalCardBoardBlt(self):
        return hil.calHvdpsBltBoardRead(self.slot)

    def Initialize(self):
        hvdpsSubslot = self.subslots[0]

        self.LogBltInfo()
        self.Log('info', 'Current HVDPS MB Image slot {} subslot {}: 0x{}'.format(self.slot, 0,hex(hvdpsSubslot.GetFpgaVersion())))
        if configs.HVDPS_FPGA_LOAD:
            hvdpsSubslot.LoadFpgaAndGetVersion()

        #delay to let alarm registers brodcast to all levels before initialization
        post_load_delay = 0.1
        time.sleep(post_load_delay)

        self.cal_card_initialize()

        if configs.HVDPS_FPGA_INITIALIZE:
            if hvdpsSubslot.GetFpgaVersion() in configs.HVDPS_LEGACY_TC_LOOPING_FPGAS:
                hvdpsSubslot.LegacyTcLooping = True
                self.registers.GLOBAL_ALARMS = self.registers.GLOBAL_ALARMS_LEGACY_TC_LOOPING
                self.registers.non_critical_global_alarms = self.registers.non_critical_global_alarms_legacy_tc_looping
                self.registers.critical_global_alarms = self.registers.critical_global_alarms_legacy_tc_looping
            if hvdpsSubslot.GetFpgaVersion() in configs.HVDPS_NON_VM_MODE_FPGAS:
                hvdpsSubslot.VmMode = True

            if hvdpsSubslot.CheckForAlarms():
                self.Log('error','Alarms detected post FPGA load')

            hvdpsSubslot.ResetDdr()
            hvdpsSubslot.ResetThermalAlarmLimitForMAX6627()

            hvdpsSubslot.SetRailsToSafeState()
            hvdpsSubslot.WriteRegister(self.registers.RESETS(BlockAuroraReceive=1))
            hvdpsSubslot.UnGangAllHvdpsRails()

            hvdpsSubslot.WriteRegister(self.registers.HCLC_UHC_RAIL_CONFIG(value = 0xFF),index=7)
            hvdpsSubslot.WriteRegister(self.registers.DUT_UHC_RAIL_CONFIG(value = 0x3FF),index=7)
            hvdpsSubslot.WriteRegister(self.registers.DUT_DOMAIN_ID(value = 0x7F),index=7)

            hvdpsSubslot.initialize_lvm_rails()
            hvdpsSubslot.ClearDpsAlarms()
            hvdpsSubslot.InitializeAd5560Rails()
            dutId = 63
            data = 2 * dutId + 1
            rail = 0

            for rail in range(8):
                hvdpsSubslot.WriteTQHeaderViaBar2(data,rail,'HV')
                hvdpsSubslot.WriteTQHeaderViaBar2(data,rail,'LVM')

            time.sleep(1)
            hvdpsSubslot.get_folded_state()

            hvdpsSubslot.setup_adcs()
            hvdpsSubslot.ClearDpsAlarms()
            hvdpsSubslot.SetRailsToSafeState()
            hvdpsSubslot.WriteRegister(self.registers.RESETS(BlockAuroraReceive=0))
            hvdpsSubslot.EnableAlarms()

            hvdpsSubslot.ClearDpsAlarms()

            if hvdpsSubslot.CheckForAlarms():
                self.Log('error','Alarms detected post initialization')

        if len(self.subslots) == 2:
            wait_time_for_max6627 = 0.2
            hvilSubslot = self.subslots[1]
            self.Log('info', 'Current HvilSubslot Image slot {} subslot {}: 0x{}'.format(self.slot, 1,hex(hvilSubslot.GetFpgaVersion())))
            if configs.HVIL_FPGA_LOAD:
                hvilSubslot.LoadFpgaAndGetVersion()
                time.sleep(wait_time_for_max6627)
            if configs.HVIL_FGPA_INITIALIZE:
                hvilSubslot.SetRailsToSafeState()
                hvilSubslot.ResetDefaultTemperatureLimitForMAX6627()
                hvilSubslot.SetDefaultCurrentClampLimit()
                hvilSubslot.SetDefaultThermalCurrentClampLimit()
                hvilSubslot.SetDefaultVoltageComparatorLimits()
            if hvilSubslot.GetFpgaVersion() in configs.HVIL_LEGACY_TC_LOOPING_FPGAS:
                hvilSubslot.LegacyTcLooping = True

    def cal_card_initialize(self):
        if configs.HVDPS_CAL_CARD_INITIALIZE:
            if not configs.CALBASE_IGNORE:
                self.Log('warning',
                         'CalBase not initialized, HVDPS cal card cant be initialized.Update config to initialize Cal Base')
            else:
                self.CalCardConnect()
                self.CalCardInitialize()
                self.calboardblt.write_to_log()
        else:
            self.Log('warning', 'HVDPS Cal Card not initialized.Update config to initialize Cal Card')

    def LogBltInfo(self):
        self.instrumentblt.write_to_log()
        self.hvdpsboardblt.write_to_log()
        self.hvilboardblt.write_to_log()


    # *************************************************************************************
    # pca9505SetBit(slot,chip,bank,io,bitVal)
    #
    #
    #       slot - HDMT slot
    #       chip - PCA9505 id [0-2]
    #       bank - IO bank [0-4], relative address to the offset.
    #       io   - specific io position in the bank [0-7]
    #       bitVal - new bit value
    #
    # *************************************************************************************

    def Pca9505SetBit(self, chip, bank, io, bitVal):

        bankVal = hil.calHvdpsPca9505Read(self.slot, chip, self.IN_PORT_OFFSET + bank)
        # print("BankVal = {}".format(bankVal))
        newBankVal = self.SetBit(bankVal, io, bitVal)
        # print("NewBankVal = {}".format(newBankVal))

        # write the new register value if it is different from the original one
        if newBankVal != bankVal:
            hil.calHvdpsPca9505Write(self.slot, chip, self.OUT_PORT_OFFSET + bank, newBankVal)

            # verify that new value took place
            rbackVal = hil.calHvdpsPca9505Read(self.slot, chip, self.IN_PORT_OFFSET + bank)
            # print("ReadBackVal = {}".format(rbackVal))
            if rbackVal != newBankVal:
                return ('ERROR: Failed to set the bit')

    def GetBitMask(self, size, bit, val):

        mask = 0
        if val == 0:
            for ind in range(0, size):
                mask = mask << 1
                if bit != 7 - ind:
                    mask = mask + 1
        if val == 1:
            for ind in range(0, size):
                mask = mask << 1
                if bit == 7 - ind:
                    mask = mask + 1
        return (mask)

    def SetBit(self, word, bit, val):
        mask = self.GetBitMask(8, bit, val)
        if val == 0:
            newword = word & mask
        else:
            newword = word | mask
        return (newword)

    def GetBit(self, size, word, bit):
        mask = self.GetBitMask(size, bit, 1)
        val = word & mask
        if val > 0:
            return (1)
        else:
            return (0)

                





