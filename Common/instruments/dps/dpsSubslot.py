################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import re
import operator
import struct
import time
import math
import random
from decimal import getcontext, Decimal
import ctypes
import io

from Common import fval
from Common import configs
from Common import hilmon as hil
from Common.instruments.base import Instrument
from Common.instruments.dps import symbols


from Common.instruments.dps import ad5560_registers
from Common.register import create_field_dictionary

DPS_NUM_AD5560_RAILS = 10
DPS_NUM_ISL55180_RAILS = 16
v_ref_term = 5.125 * 2.5
ad5560_sense_resistors = [0.08, 100e3, 20e3, 2e3, 200, 20, 1, 0.4, 0.02, 0.02, 0.02, 0.02, 0.02]
mi_amp_gain = 10


class DpsSubslot(Instrument):
    NoValidPacketData = 0xFFFF

    def __init__(self,slot,subslot):
        self.slot = slot
        self.subslot = subslot
        self.railTypes = []
        self.offset = 0
        self.AD5560_OFFSET_DAC_CODE = 0x2100
        self.AD550_V_REF = 5.0
        self.AD5560_DAC_SCALE = 0x10000
        self.AD5560_V_CALC_SCALER = 5.125
        self.UHC_COUNT = 8
        self.symbols = symbols
        self.LegacyTcLooping = False
        self.VmMode = False

    def S2HSupport(self):
        return True

    def ClearHclcRailAlarms(self):
        self.clearPerRailHclcAlarmsRegister()

    def ClearGlobalAlarms(self):
        self.clearGlobalAlarmRegister()

    def ClearHclcSampleAlarms(self):
        self.clearHclcSampleAlarmRegister()


    def ClearHclcCfoldAlarms(self):
        self.clearHclcCfoldAlarmsRegister()

    def ResetDdr(self, numberOfTries = 20):
        resetsRegisterType = self.getResetsRegister()
        statusRegisterType = self.getStatusRegister()
        resetsRegister = resetsRegisterType()
        #reseting DDR and Aurora
        resetsRegister.AuroraReset = 1
        resetsRegister.DDRControllerReset = 1
        for attempt in range(numberOfTries):
            self.WriteRegister(resetsRegister)
            time.sleep(0.01)
            status = self.ReadRegister(statusRegisterType)
            if status.DDRControllerPLLLock == 0 and status.DDRCalibrationDone == 0:
                break
            if status.DDRControllerPLLLock != 0:
                self.Log('warning', 'DDR Controller PLL not set in attempt {}'.format(attempt))
            if status.DDRCalibrationDone != 0:
                self.Log('warning', 'DDR Calibration not done in attempt {}'.format(attempt))
        else:
            self.Log('error', 'Reset Timeout Error on slot {}'.format(self.slot))


    def CheckGlobalAlarms(self):
        global_alarm_register_type = self.getGlobalAlarmRegisterType()
        global_alarm_register = self.ReadRegister(global_alarm_register_type)
        if global_alarm_register.value != 0x0:
            self.GetMoreAlarmDetails(global_alarm_register)
            return True
        else:
            return False

    def GetMoreAlarmDetails(self,global_alarm_register):
        global_alarms_dict = create_field_dictionary(global_alarm_register)
        active_alarms = [name for name, status in global_alarms_dict.items() if status != 0]
        if active_alarms != []:
            self.Log('warning', '  \n'.join(['Following Alarms received:'] + sorted(active_alarms)))


    def SetRailsToSafeState(self):
        rail_state_register_type = self.getSafeStateRegister()
        rail_state = rail_state_register_type()
        rail_state.RailSafeBit = 1
        self.WriteRegister(rail_state)
        time.sleep(0.1)

    def EnableOnlyOneUhc(self, uhc, dutdomainId = 15):
        for i in range(8):
            dut_domain_register = self.ReadRegister(self.getDutDomainIdRegisterType(),index=i)
            if i== uhc:
                dut_domain_register.DutValidBit = 1
                dut_domain_register.DutDomainId = dutdomainId
            else:
                dut_domain_register.DutValidBit = 0
                dut_domain_register.DutDomainId = 0
            self.WriteRegister(dut_domain_register,index=i)

    def DisableAllUhcs(self):
        uhc = 8
        for dut_id in range(uhc):
            dut_domain_id_register_type = self.getDutDomainIdRegisterType()
            dut_domain_id_register = dut_domain_id_register_type()
            dut_domain_id_register.DutDomainId = 0
            dut_domain_id_register.DutValidBit = 0
            self.WriteRegister(dut_domain_id_register, index=dut_id)

    def empty_trigger_buffer(self):
        loopCount = 10000
        for iteration in range(loopCount):
            trigger_buffer_content = self.ReadRegister(self.getTriggerDownTestRegister())
            self.Log('debug', 'trigger_buffer_content 0x{:x}'.format(trigger_buffer_content.value))
            if trigger_buffer_content.value == 0:
                break
        else:
            self.Log('error', 'Trigger buffer never cleared after trying {} read iterations'.format(loopCount))

    def empty_ddr_for_uhc_zero(self):
        lut_dword_address = 0x10000000
        dut_id = 0
        uhc_specific_triggers_lut_address = lut_dword_address + (dut_id * 0x10000)
        self.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
        time.sleep(0.000001)

    def ClearTriggerQueue(self):
        buffer_length = 4 * 1024 * 1024  # 4MB
        memoryBuffer = bytes(buffer_length)

        while (self.offset < self.TOTAL_MEMORY_SIZE):
            self.DmaWrite(self.offset, memoryBuffer)
            self.offset = self.offset + buffer_length

        if self.offset == self.TOTAL_MEMORY_SIZE:
            self.Log('debug', 'Cleared the trigger queue, value of offset {}'.format(self.offset))
        else:
            self.Log('error', 'Not able to clear Trigger queue, value of offset:{}'.format(self.offset))

    def MAX6627TemperatureDecimalToCelcius(self, temperature_in_decimal):
        temperature_in_celcius = temperature_in_decimal * 0.0625
        return temperature_in_celcius

    def ReadRegister(self, RegisterType, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        regvalue = self.BarRead(RegisterType.BAR, RegisterType.address(index))
        return RegisterType(value=regvalue)

        
    def WriteRegister(self, register, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        self.BarWrite(register.BAR, register.address(index), register.value)

    def decode_sfp_to_current_in_amps(self, current_in_sfp):
        sign = (current_in_sfp >> 15) & 0x0001
        precision = (current_in_sfp >> 5) & 0x03ff
        exponent = current_in_sfp & 0x0001f
        current_in_amps = precision / pow(2, exponent)
        if sign:
            current_in_amps = - current_in_amps
        return current_in_amps

    def encode_current_in_amps_to_sfp(self, current_in_amps):
        if int(current_in_amps) not in range(-1024,1024):
            self.Log('error', 'Current out of range. Expected value is between -1024 to 1024')
        sign = 0
        if current_in_amps < 0.0:
            sign = 1
            current_in_amps = - current_in_amps
        fraction = math.frexp(current_in_amps)
        current_in_sfp = 0
        current_in_sfp |= (10 - fraction[1]) & 0x0001f
        current_in_sfp |= (int(fraction[0] * 0x0400) & 0x03ff) << 5
        current_in_sfp |= sign << 15
        return current_in_sfp

    def decode_x1_to_current_in_amps(self, current_in_x1, current_range):
        current_in_amps = (v_ref_term * (current_in_x1 - 32768)/ pow(2, 16)) / (ad5560_sense_resistors[current_range] * mi_amp_gain)
        return current_in_amps

    def encode_current_in_amps_to_x1(self, current_in_amps, current_range):
        current_in_x1 = int((current_in_amps * ad5560_sense_resistors[current_range] * mi_amp_gain * pow(2, 16) / v_ref_term) + 32768)
        return current_in_x1

    def is_close(self, expected, value, fudge_factor_percentage,zero_buffer = 0):
        if expected == 0:
            maximum_value = expected + zero_buffer
            mininum_value = expected - zero_buffer
        else:
            maximum_value = expected + (abs(expected)*(fudge_factor_percentage/100))
            mininum_value = expected - (abs(expected)*(fudge_factor_percentage/100))
        if value >= mininum_value and value <= maximum_value:
            return True
        else:
            return False

    def is_in_maximum_allowed_deviation(self, expected_value, actual_value, expected_deviation):
        deviation =abs((abs(expected_value)-abs(actual_value)))
        if deviation >=expected_deviation:
            return False
        else:
            return True

    def actual_tracking_voltage_is_close_to_volt_variation(self, expected_voltage, actual_voltage, voltage_variation):
        maximum_value = expected_voltage + voltage_variation
        minimum_value = expected_voltage - voltage_variation
        if actual_voltage >= minimum_value and actual_voltage <= maximum_value:
            return True
        else:
            return False


    def WriteTriggerQueue(self, offset, data):
        self.DmaWrite(offset, data)

    def DmaWrite(self, address, data):
        return hil.dpsDmaWrite(self.slot, self.subslot, address, data)

    def ExecuteTriggerQueue(self, offset, dutid, rail_type):
        trigger_register_type =  self.getExecuteTriggerRegister()
        trigger_register = trigger_register_type()
        trigger_register.Execute = 1
        trigger_register.ByteAddress = offset
        self.WriteRegister(trigger_register)
        self.CheckTqNotifyAlarm(dutid, rail_type)

    def WaitForGlobalAlarmBit(self, global_alarm_bit, waitLoop=20000):
        for count in range(waitLoop):
            globalalarm = self.ReadRegister(self.registers.GLOBAL_ALARMS)
            if globalalarm.value != 0:
                if getattr(globalalarm, global_alarm_bit) != 0:
                    break
        else:
            self.Log('error', 'Global Alarm Bit {} failed to set'.format(global_alarm_bit))

    def getTriggerQueueHeaderString(self, rail, resourceid_string):
        resourceid = getattr(symbols.RESOURCEIDS,resourceid_string)
        if self.LegacyTcLooping:
            trigger_queue_header = '''LegacyTcLoopingTqNotify rail=0, value=BEGIN 
                                      LegacyTcLoopingTqNotify rail=16, value=BEGIN'''
        else:
            rail_mask = 0x1 << rail
            header_line1 = 'WakeRail resourceid = {}, railmask = {}'.format(resourceid,rail_mask)
            header_line2 = 'SetRailBusy resourceid = {}, railmask = {}'.format(resourceid,rail_mask)
            trigger_queue_header = header_line1 + '\n' + header_line2

        return trigger_queue_header

    def getTriggerQueueFooterString(self, rail, resourceid_string):
        resourceid = getattr(symbols.RESOURCEIDS,resourceid_string)

        if self.LegacyTcLooping:
            trigger_queue_footer = '''LegacyTcLoopingTqNotify rail=0, value=END 
                                      LegacyTcLoopingTqNotify rail=16, value=END'''
        else:
            rail_mask = 0x1 << rail
            resource_target = resourceid
            trigger_queue_footer = 'TqNotify railmask = {}, resourceid = {}'.format(rail_mask,resource_target)

        return trigger_queue_footer

    def getSetOverUnderVoltString(self, rail_type, set_hi_volt, set_low_volt):
        if rail_type == 'HV':
            set_volt_hi_low = '''RawSetOV rail=RAIL, value={}
                                 RawSetUV rail=RAIL, value={}'''.format(set_hi_volt, set_low_volt)
        else:
            set_volt_hi_low = '''SetOV rail=RAIL, value={}  
                                 SetUV rail=RAIL, value={}'''.format(set_hi_volt, set_low_volt)
        return set_volt_hi_low

    def getSetIClampHiLowString(self, rail_type, set_hi_clamp, set_low_clamp):
        if rail_type == 'HV':
            set_i_clamp_hi_low = '''RawSetIClampHi rail=RAIL, value={}
                                    RawSetIClampLo rail=RAIL, value={}'''.format(set_hi_clamp,set_low_clamp)
        else:
            set_i_clamp_hi_low = '''SetIClampHi rail=RAIL, value={}
                                    SetIClampLo rail=RAIL, value={}'''.format(set_hi_clamp,set_low_clamp)
        return set_i_clamp_hi_low

    def getSetVoltageString(self,rail_type,voltage_to_set):
        if rail_type == 'HV':
            set_voltage_string = 'RawSetVoltage rail=RAIL, value={}'.format(voltage_to_set)
        else:
            set_voltage_string = 'SetVoltage rail=RAIL, value={}'.format(voltage_to_set)
        return set_voltage_string

    def get_global_alarms(self):
        alarms = []
        global_alarm = self.ReadRegister(self.getGlobalAlarmRegisterType())
        for field in global_alarm.fields():
            if getattr(global_alarm,field) == 1:
                if field in self.registers.critical_global_alarms:
                    alarms.append(field)
        return alarms


    def GetFpgaVersion(self):
        return hil.dpsFpgaVersion(self.slot, self.subslot)
        
        
    def ReadMemory(self, data):
        s = []
        for byte in data:
            s.append('{:02x}'.format(byte))
            if len(s) == 4:
                if ''.join(s) == '00000000':
                    s = []
                    break
                self.Log('info', '0x' + ''.join(reversed(s)))
                s = []
        if len(s) != 0:
            raise Exception('Data length is expected to be a multiple of 4 (i.e. dword aligned)')
            

    def DmaRead(self, address, length):
        return hil.dpsDmaRead(self.slot, self.subslot, address, length)

    def DpsBarRead(self, bar, offset, device_id):
        if bar == 2:
            if device_id > 127:
                raise Exception('Device ID too large')
            write_bit = 0
            offset = (write_bit << 17) | (device_id << 10) | offset
        return hil.dpsBarRead(self.slot, self.subslot, bar, offset)

    def DpsBarWrite(self, bar, offset, data, device_id):
        if bar == 2:
            if device_id > 127:
                raise Exception('Device ID too large')
            write_bit = 1
            offset = (write_bit << 17) | (device_id << 10) | offset
        return hil.dpsBarWrite(self.slot, self.subslot, bar, offset, data)


    def WriteBar2RailCommand(self, command, data, rail_or_resourceid):
        offset_type = self.getBar2RailCommandRegister()
        offset = offset_type()
        offset.RailCommand = command
        offset.RailNumberOrResourceId = rail_or_resourceid
        offset_value = offset.value << 2
        offset = offset_value
        self.BarWrite(2, offset, data)


    def WriteTQHeaderViaBar2(self, dutid, rail, resourceid_string):
        if self.LegacyTcLooping:
            data = 2 * dutid + 1
            if resourceid_string == 'RLOAD':
                self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, rail +0)
            else:
                self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, rail + 0)
                self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, rail + 16)
        else:
            resourceid = getattr(symbols.RESOURCEIDS, resourceid_string)
            data = 0x1 << rail
            self.WriteBar2RailCommand(symbols.RAILCOMMANDS.WAKE_RAIL, data, resourceid)
            self.WriteBar2RailCommand(symbols.RAILCOMMANDS.SET_RAIL_BUSY, data, resourceid)

    def WriteTQFooterViaBar2(self, dutid, rail, resourceid_string):
        if self.LegacyTcLooping:
            data = 2 * dutid
            self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, rail + 0)
            self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, rail + 16)
        else:
            resourceid = getattr(symbols.RESOURCEIDS, resourceid_string)
            data = 0x1 << rail
            self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, resourceid)
            # self.WriteBar2RailCommand(symbols.RAILCOMMANDS.TQ_NOTIFY, data, resourceid)


    def ConfigureUhcRail(self, uhc, rail_mask,rail_type):
        uhc_rail_config_register = self.getUhcRailConfigRegister(rail_type)
        for i in range(8):
            if i == uhc:
                self.WriteRegister(uhc_rail_config_register(value=rail_mask), index=i)
            else:
                self.WriteRegister(uhc_rail_config_register(value=0x0000), index=i)
        unused_uhc = list(range(8))
        unused_uhc.remove(uhc)
        spare_uhc = random.choice(unused_uhc)
        inverted_rails = (~rail_mask & 0xFFFF)
        self.WriteRegister(uhc_rail_config_register(value=inverted_rails), index=spare_uhc)

    def i_clamp_current_to_ad5560_raw_dac_code(self, current, irange):
        associated_rsense = 'R_SENSE_{}'.format(irange)
        r_sense = getattr(symbols.AD5560RSENSE, associated_rsense)
        r_sense_gain = r_sense * self.AD5560_MI_AMP_GAIN
        scaled_vref = self.AD5560_V_CALC_SCALER * self.AD550_V_REF
        r_sense_gain_scaled_vref = scaled_vref / r_sense_gain
        iclamp_plus_vref = current + (r_sense_gain_scaled_vref * .5)
        clamp_dac =  int((iclamp_plus_vref * self.AD5560_DAC_SCALE) / r_sense_gain_scaled_vref)
        self.is_raw_conversion_in_expected_range(clamp_dac, current)
        return clamp_dac

    def is_raw_conversion_in_expected_range(self, clamp_dac, current):
        if (clamp_dac > 0xFFFF):
            self.Log('error', 'IClamp to DAC conversion out of range')
        if current < 0 and clamp_dac > 0x7FFF:
            self.Log('error', 'Invalid negative IClamp to DAC conversion')
        if current > 0 and clamp_dac < 0x8000:
            self.Log('error', 'Invalid positive IClamp to DAC conversion')

    def get_positive_random_voltage(self, low_voltage, high_voltage, step_size):
        return self.get_random_voltage(low_voltage, high_voltage, step_size)

    def get_negative_random_voltage(self, low_voltage, high_voltage, step_size):
        return self.get_random_voltage(low_voltage, high_voltage,step_size)

    def get_random_voltage(self,low_voltage, high_voltage,step_size):
        number_of_steps = (high_voltage - low_voltage) / step_size
        force_voltage = (random.randint(0, number_of_steps) * step_size) + low_voltage
        return force_voltage

    def WriteToSampleCollectorIndexRegister(self,selected_index,group):
        sample_collector_index_register = self.getSampleCollectorIndexRegister()
        sample_collector_index_obj =sample_collector_index_register()
        sample_collector_index_obj.SelectedIndex = selected_index
        sample_collector_index_obj.SelectedGroup = group
        self.WriteRegister(sample_collector_index_obj)

    def WriteToSampleCollectorIndexRegister_Rload(self, selected_index):
        sample_collector_index_register = self.getSampleCollectorIndexRegister()
        RawOrFloatingPointValue = 1<< 31
        SelectedIndex = selected_index
        self.WriteRegister(sample_collector_index_register(value = (RawOrFloatingPointValue|SelectedIndex)))

    def ReadSampleCollectorCount(self):
        sample_count_register = self.getSampleCollectorCountRegister()
        sample_count_value = self.ReadRegister(sample_count_register).value
        return sample_count_value

    def ReadSampleCollectorData(self):
        sample_collector_data_register = self.getSampleCollectorDataRegister()
        sample_collector_data = self.ReadRegister(sample_collector_data_register).value
        return  sample_collector_data

    def WriteToTurboSampleCollectorIndexRegister(self,selected_index):
        sample_collector_turbo_index_register = self.getTurboSampleCollectorIndexRegister()
        RawOrFloatingPointValue = 1 << 31
        SelectedIndex = selected_index
        self.WriteRegister(sample_collector_turbo_index_register(value=(RawOrFloatingPointValue | SelectedIndex)))


    def ReadTurboSampleCollectorCount(self):
        sample_count_register = self.getTurboSampleCollectorCountRegister()
        sample_count_value = self.ReadRegister(sample_count_register).value
        return sample_count_value

    def ReadTurboSampleCollectorData(self):
        sample_collector_data_register = self.getTurboSampleCollectorDataRegister()
        sample_collector_data = self.ReadRegister(sample_collector_data_register).value
        return  sample_collector_data

    def read_raw_spancode_and_channel_id_from_sample_collector(self,selected_index,group):
        self.WriteToSampleCollectorIndexRegister(selected_index,group)
        for number_of_attempts in range(20):
            if self.ReadSampleCollectorCount() > 0:
                break
        else:
            raise DpsSubslot.SampleCollectorError('No samples found after {} attempts.'.format(number_of_attempts))

        raw_adc_value = self.ReadSampleCollectorData()
        softspan_code = raw_adc_value & 0x7
        channel_id = (raw_adc_value >>3) & 0x7
        return softspan_code,channel_id

    def WriteToSampleCollectorIndexRegisterRload(self, selected_index):
        self.WriteToSampleCollectorIndexRegister_Rload(selected_index)
        for number_of_attempts in range(20):
            if self.ReadSampleCollectorCount() > 0:
                break
        else:
            raise DpsSubslot.SampleCollectorError('No samples found after {} attempts.'.format(number_of_attempts))

    def read_raw_spancode_and_channel_id_from_sample_collector_rload(self):
        raw_adc_value = self.ReadSampleCollectorData()
        softspan_code = raw_adc_value & 0x7
        channel_id = (raw_adc_value >>3) & 0x7
        return softspan_code,channel_id

    def write_to_turbo_sample_collector_index_register(self,selected_index):
        self.WriteToTurboSampleCollectorIndexRegister(selected_index)
        for number_of_attempts in range(20):
            if self.ReadTurboSampleCollectorCount() > 0:
                break
        else:
            raise DpsSubslot.SampleCollectorError('No samples found after {} attempts.'.format(number_of_attempts))

    def read_raw_spancode_and_channel_id_from_turbo_sample_collector(self):
        raw_adc_value = self.ReadTurboSampleCollectorData()
        softspan_code = raw_adc_value & 0x7
        channel_id = (raw_adc_value >> 3) & 0x7
        return softspan_code, channel_id

    class SampleCollectorError(Exception):
        pass

    def InitializeSampleEngine(self, HeaderBase, HeaderSize, SampleBase, SampleSize, uhc, rail_type):
        self.WriteRegister(self.getSampleHeaderRegionBaseRegister(rail_type)(value=HeaderBase), index=uhc)
        self.WriteRegister(self.getSampleHeaderRegionSizeRegister(rail_type)(value=HeaderSize), index=uhc)
        self.WriteRegister(self.getSampleRegionBaseRegister(rail_type)(value=SampleBase), index=uhc)
        self.WriteRegister(self.getSampleRegionSizeRegister(rail_type)(value=SampleSize), index=uhc)
        self.WriteRegister(self.getSampleEngineResetRegister(rail_type)(value=1 << uhc))
        if rail_type == 'LVM':
            self.DmaWrite(HeaderBase | 0x20000000, struct.pack('<L', 0x00) * HeaderSize)
            self.DmaWrite(HeaderBase | 0x20000000, struct.pack('<L', 0xFFFFFFFF))
        else:
            self.DmaWrite(HeaderBase, struct.pack('<L', 0x00) * HeaderSize)
            self.DmaWrite(HeaderBase, struct.pack('<L', 0xFFFFFFFF))

    def InitializeTurboSampleEngine(self, HeaderBase, HeaderSize, SampleBase, SampleSize, uhc, rail_type):
        self.WriteRegister(self.getTurboSampleHeaderRegionBaseRegister(rail_type)(value=HeaderBase))
        self.WriteRegister(self.getTurboSampleHeaderRegionSizeRegister(rail_type)(value=HeaderSize))
        self.WriteRegister(self.getTurboSampleRegionBaseRegister(rail_type)(value=SampleBase))
        self.WriteRegister(self.getTurboSampleRegionSizeRegister(rail_type)(value=SampleSize))
        self.WriteRegister(self.getTurboSampleEngineResetRegister(rail_type)(value=1))

        self.DmaWrite(HeaderBase | 0x20000000, struct.pack('<L', 0x00) * HeaderSize)
        self.DmaWrite(HeaderBase | 0x20000000, struct.pack('<L', 0xFFFFFFFF))

    def GetSampleHeaders(self, uhc, expected_sample_count, rail_type, wait_loops = 10000, turbo = False):
        header_byte_size = 16
        header_terminator_byte_size = 4
        sample_headers = []

        if rail_type == 'LVM' or turbo == True:
            memory_offset = 0x20000000
        else:
            memory_offset = 0x0

        if turbo == True:
            header_base_offset = self.ReadRegister(self.getTurboSampleHeaderRegionBaseRegister(rail_type)).value | memory_offset
            header_size_offset = self.ReadRegister(self.getTurboSampleHeaderRegionSizeRegister(rail_type)).value
        else:
            header_base_offset = self.ReadRegister(self.getSampleHeaderRegionBaseRegister(rail_type),index = uhc).value | memory_offset
            header_size_offset = self.ReadRegister(self.getSampleHeaderRegionSizeRegister(rail_type), index=uhc).value

        #check that user isnt requesting more headers then possible
        max_header_count = int((header_size_offset - header_terminator_byte_size)/header_byte_size)
        if expected_sample_count > max_header_count:
            raise Exception('Requested more sample headers: {}, then allocated memory: {}, allows for.'.format(expected_sample_count,header_size_offset))

        header_terminator_location = header_base_offset + (expected_sample_count*header_byte_size)

        for i in range(wait_loops):
            terminator = self.DmaRead(header_terminator_location,header_terminator_byte_size)
            if '0xffffffff' == hex(int.from_bytes(terminator,'little')):
                break
        else:
            self.Log('error', 'Sample engine terminator never raised')
            return None

        memory_top = self.DmaRead(header_base_offset,header_terminator_byte_size)
        if '0xffffffff' == hex(int.from_bytes(memory_top,'little')):
            self.Log('error', 'Sample engine never run')
            return None

        header_data = self.DmaRead(header_base_offset, header_size_offset)
        sample_header_data = io.BytesIO(header_data)
        sample_header_data.seek(0)
        for i in range(expected_sample_count):
            sample_header_struct = self.registers.SampleHeaderStruct()
            header = sample_header_data.read(16)
            ctypes.memmove(ctypes.addressof(sample_header_struct), header, 16)
            sample_headers.append(sample_header_struct)

        return sample_headers


    def randomize_rail_uhc(self,rail_type):
        rail_uhc_tuple_list = []
        for rail in range(self.RAIL_COUNT[rail_type]):
            for uhc in range(self.UHC_COUNT):
                rail_uhc_tuple_list.append((rail, uhc))
        random.shuffle(rail_uhc_tuple_list)
        return rail_uhc_tuple_list

    def check_for_single_hclc_rail_alarm(self,rail,alarm):
        global_alarms = self.get_global_alarms()
        if global_alarms == ['HclcRailAlarms']:
            hclc_alarms = self.ReadRegister(self.registers.HCLC_ALARMS)
            hclc_per_rail_alarm = self.getHcLcPerRailAlarmRegister()
            rail_mask = 1 << rail
            if rail_mask == hclc_alarms.value:
                hclc_per_rail_alarm = self.ReadRegister(hclc_per_rail_alarm, index=rail)
                rail_alarms = []
                for field in hclc_per_rail_alarm.fields():
                    if getattr(hclc_per_rail_alarm, field) == 1:
                        rail_alarms.append(field)
                if rail_alarms == [alarm]:
                    self.Log('debug','Expected {} Alarm received on rail {}'.format(alarm, rail))
                    return True
                else:
                    self.Log('error','Expected only {} to be set in Hclc rail {} specific alarm register, actual alarms set were {}'.format(
                                 alarm, rail, rail_alarms))
            else:
                hclc_alarms_set = []
                for field in hclc_alarms.fields():
                    if getattr(hclc_alarms, field) == 1:
                        hclc_alarms_set.append(field)
                self.Log('error','Expected only Rail {} bit to be set in HCLC_alarms register, but got the following: {}'.format(rail,hclc_alarms_set))
        else:
            self.Log('error','Expected only \'HclcRailAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(global_alarms))
        return False

    def check_for_hclc_cfold_actual_and_assert_data(self, rail):
        rail = self.HwRailtoPerDeviceRail(rail)
        global_alarms = self.get_global_alarms()
        if global_alarms == ['HclcCFoldAlarms']:
            hclc_cfold_alarms = self.ReadRegister(self.getHcLcControlledFoldAlarmsRegister())
            hclc_per_rail_controlled_fold_alarm = self.getHcLcPerRailControlledFoldDetailsRegister()
            rail_mask = 1 << rail
            if rail_mask == hclc_cfold_alarms.value:
                hclc_per_rail_controlled_fold_alarm = self.ReadRegister(hclc_per_rail_controlled_fold_alarm, index=rail)
                actual = hclc_per_rail_controlled_fold_alarm.CFoldActualData
                expected = hclc_per_rail_controlled_fold_alarm.AssertTestData
                if actual == expected:
                    self.Log('error',
                             'HCLC CFoldActualData shouldn\'t be same as AssertTestData. Actual -> {} Expected -> {}'.format(
                                 actual, expected))
                else:
                    self.Log('debug', 'HclcCfoldData register is set to value -> 0x{:x}'.format(hclc_per_rail_controlled_fold_alarm.value))
                    return True
            else:
                hclc_control_fold_alarm_set = []
                for field in hclc_cfold_alarms.fields():
                    if getattr(hclc_cfold_alarms, field) == 1:
                        hclc_control_fold_alarm_set.append(field)
                self.Log('error','Expected only Rail {} bit to be set in HCLC_control_fold_alarms register, but got the following: {}'.format(rail,hclc_control_fold_alarm_set))
        else:
            self.Log('error','Expected only \'HclcCFoldAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(global_alarms))
        return False

    def check_for_single_vlc_rail_alarm(self,rail,alarm):
        global_alarms = self.get_global_alarms()
        if global_alarms == ['VlcRailAlarms']:
            vlc_alarms = self.ReadRegister(self.registers.VlcRailAlarms)
            rail_mask = 1 << rail
            if rail_mask == vlc_alarms.value:
                vlc_rail_alarms_register = 'Vlc{:02d}RailAlarms'.format(rail)
                hclc_rail_alarm = self.ReadRegister(getattr(self.registers, vlc_rail_alarms_register))
                rail_alarms = []
                for field in hclc_rail_alarm.fields():
                    if getattr(hclc_rail_alarm, field) == 1:
                        rail_alarms.append(field)
                if rail_alarms == [alarm]:
                    self.Log('debug','Expected {} Alarm recived on rail {}'.format(alarm, rail))
                    return True
                else:
                    self.Log('error',
                             'Expected only {} to be set in Vlc rail {} specific alarm register, actual alarms set were {}'.format(
                                 alarm, rail, rail_alarms))
            else:
                hclc_alarms_set = []
                for field in vlc_alarms.fields():
                    if getattr(vlc_alarms, field) == 1:
                        hclc_alarms_set.append(field)
                self.Log('error','Expected only Rail {} bit to be set in VLC_alarms register, but got the following: {}'.format(rail,hclc_alarms_set))
        else:
            self.Log('error','Expected only \'VlcRailAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(global_alarms))
        return False

    def check_for_vlc_cfold_actual_and_assert_data(self, rail):
        global_alarms = self.get_global_alarms()
        if global_alarms == ['VlcCFoldAlarms']:
            vlc_cfold_alarms = self.ReadRegister(self.registers.VlcCfoldAlarms)
            vlc_per_rail_controlled_fold_alarm = self.getVlcPerRailControlledFoldDetailsRegister()
            rail_mask = 1 << rail
            if rail_mask == vlc_cfold_alarms.value:
                vlc_per_rail_controlled_fold_alarm = self.ReadRegister(vlc_per_rail_controlled_fold_alarm, index=rail)
                actual = vlc_per_rail_controlled_fold_alarm.CFoldActualData
                expected = vlc_per_rail_controlled_fold_alarm.AssertTestData
                if actual == expected:
                    self.Log('error',
                             'VLC CFoldActualData shouldn\'t be same as AssertTestData. Actual -> {} Expected -> {}'.format(
                                 actual, expected))
                else:
                    self.Log('debug', 'VlcCfoldData register is set to value -> 0x{:x}'.format(
                        vlc_per_rail_controlled_fold_alarm.value))
                    return True
            else:
                vlc_control_fold_alarm_set = []
                for field in vlc_cfold_alarms.fields():
                    if getattr(vlc_cfold_alarms, field) == 1:
                        vlc_control_fold_alarm_set.append(field)
                self.Log('error',
                         'Expected only Rail {} bit to be set in HCLC_control_fold_alarms register, but got the following: {}'.format(
                             rail, vlc_control_fold_alarm_set))
        else:
            self.Log('error',
                     'Expected only \'VlcCFoldAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(
                         global_alarms))
        return False

    def get_non_zero_fields(self, register):
            non_zero_fields = []
            for field in register.fields():
                if getattr(register, field) == 1:
                    non_zero_fields.append(field)
            return non_zero_fields

    def get_shuffled_device_list(self, number_of_devices):
        list_of_devices = list(range(number_of_devices))
        random.shuffle(list_of_devices)
        return list_of_devices

    def verify_if_fpga_supports_vmeasure(self):
        if self.VmMode:
            reason = 'This FPGA: {} does not support V Measure Mode'.format(hex(self.GetFpgaVersion()))
            self.Log('warning', reason)
            return False
        else:
            return True

    def bring_rails_out_of_safe_state(self,dutid,rail, rail_type):
        hclchv_rail_base = 16
        if rail_type != 'VLC':
            rail = rail- hclchv_rail_base
        self.WriteTQHeaderViaBar2(dutid, rail, rail_type)

    def send_tq_footer_and_check_tq_notify_alarm(self,dutid,rail, rail_type):
        hclchv_rail_base = 16
        if rail_type != 'VLC':
            rail = rail - hclchv_rail_base
        self.WriteTQFooterViaBar2(dutid, rail, rail_type)
        self.CheckTqNotifyAlarm(dutid, rail_type)

    ############################################################################################################
    # hwRailtoPerDeviceRail(self, rail):
    #   Takes a HW Rail, and translates it to a per-device rail. (See header at top of this file)
    ############################################################################################################
    def HwRailtoPerDeviceRail(self, rail):
        if (self.subslot == 0):
            # If this condition met, we are ISL55180 Device 0
            if (rail >= 0 and rail <= 7):
                return (rail)
            # If this condition met, we are ISL55180 Device 1
            elif (rail >= 8 and rail <= 15):
                return (rail - 8)
            # If this condition met, we are MB AD5560 Device 0
            elif (rail >= 16 and rail <= 25):
                return (rail - 16)
        else:
            # If this condition met, we are DC AD5560 Device 1
            if (rail >= 16 and rail <= 25):
                return (rail - 16)

        # User supplied incorrect rail value
        self.Log("critical", "This hardware rail {} is not recognized for subslot {}".format(rail, self.subslot))
        return (-1)

    def check_for_single_dut_rail_alarm(self,rail,alarm):
        global_alarms = self.get_global_alarms()
        if global_alarms == ['DutRailAlarms']:
            dut_alarms = self.ReadRegister(self.registers.DUT_ALARMS)
            dut_per_rail_alarm = self.getDutPerRailAlarmRegister()
            rail_mask = 1 << rail
            if rail_mask == dut_alarms.value:
                hclc_per_rail_alarm = self.ReadRegister(dut_per_rail_alarm, index=rail)
                rail_alarms = []
                for field in hclc_per_rail_alarm.fields():
                    if getattr(hclc_per_rail_alarm, field) == 1:
                        rail_alarms.append(field)
                if rail_alarms == [alarm]:
                    self.Log('debug','Expected {} Alarm received on rail {}'.format(alarm, rail))
                    return True
                else:
                    self.Log('error','Expected only {} to be set in Dut rail {} specific alarm register, actual alarms set were {}'.format(
                                 alarm, rail, rail_alarms))
            else:
                hclc_alarms_set = []
                for field in dut_alarms.fields():
                    if getattr(dut_alarms, field) == 1:
                        hclc_alarms_set.append(field)
                self.Log('error','Expected only Rail {} bit to be set in Dut_alarms register, but got the following: {}'.format(rail,hclc_alarms_set))
        else:
            self.Log('error','Expected only \'DutRailAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(global_alarms))
        return False

    def S2HRead(self, RegisterType, index=0):
        '''Reads a register of the given type, the index is for register arrays'''
        regValue = self.BarRead(RegisterType.BAR, RegisterType.address(index))
        return RegisterType(value=regValue)

    def S2HWrite(self, register, index=0):
        '''Writes a register of the given type, the index is for register arrays'''
        self.BarWrite(register.BAR, register.address(index), register.value)

    def RCPeerToPeerMemoryBase(self):
        return hil.rcPeerToPeerMemoryBase()

    def StreamingBufferaddrsize(self):
        self.Log('info', 'Slot {} Subslot {}'.format(self.slot, self.subslot))
        return hil.dpsStreamingBuffer(self.slot, self.subslot)

    def GetTotalNoofPackets(self):
        no_of_packets = self.S2HRead(self.registers.S2HValidPacketIndexReg)
        return no_of_packets.value

    def GetPacketCount(self):
        packet_count = self.S2HRead(self.registers.S2HPacketCountReg)
        return packet_count.value

    def IsStreamingOn(self):
        S2HControlRegister = self.S2HRead(self.registers.S2HControlReg)
        return S2HControlRegister.S2HStreamingStatus

    def TurnONStreamingWithControlReg(self):
        S2HControlRegister = self.registers.S2HControlReg()
        streaming_on_status_value = 1
        S2HControlRegister.S2HStreamingStart = streaming_on_status_value
        self.S2HWrite(S2HControlRegister)

    def TurnOFFStreamingWithControlReg(self):
        S2HControlRegister = self.registers.S2HControlReg()
        streaming_off_status_value = 1
        S2HControlRegister.S2HStreamingStop = streaming_off_status_value
        self.S2HWrite(S2HControlRegister)
        streaming_off_status_value = self.S2HRead(self.registers.S2HControlReg).S2HStreamingStop
        for turnoff_attempt in range(100):
            if streaming_off_status_value != 0:
                streaming_off_status_value = self.S2HRead(self.registers.S2HControlReg).S2HStreamingStop
                time.sleep(.02)
        if streaming_off_status_value == 0:
            self.Log('debug',
                     'S2H Off Successful.S2HControlRegister Status {} '.format(hex(streaming_off_status_value)))
        else:
            self.Log('error', 'S2H Off was Unsuccessful in 100 attempts')

    def ResetStreamingWithControlReg(self):
        if self.IsStreamingOn():
            self.Log('error', 'Reset unsuccessful as Streaming is ON ')
        S2HControlRegister = self.registers.S2HControlReg()
        streaming_reset_status_value = 1
        S2HControlRegister.S2HStreamingReset = streaming_reset_status_value
        self.S2HWrite(S2HControlRegister)
        for reset_attempt in range(100):
            if streaming_reset_status_value != 0:
                streaming_reset_status_value = self.S2HRead(self.registers.S2HControlReg).S2HStreamingReset
                time.sleep(.02)
        if streaming_reset_status_value == 0:
            self.Log('debug',
                     'S2H Reset Successful . S2HControlRegister Status {} '.format(hex(streaming_reset_status_value)))
        else:
            self.Log('error', 'S2H reset was Unsuccessful in 100 attempts')

    def IsS2HConfigurationDone(self):
        S2HConfigurationRegister = self.S2HRead(self.registers.S2HConfigurationReg)
        if S2HConfigurationRegister.S2HConfigurationDone == 1:
            return True
        else:
            return False

    def ReadPacketNumber(self, idx, buffersize):
        return struct.unpack('<H', bytes(buffersize[idx][:2]))[0]

    def S2HRegisterAndBufferSetup(self):
        addr, size = self.StreamingBufferaddrsize()
        max_packets_in_site_controller, pkt_byte_count, buffersize = self.ReadStreamingDetails(addr)
        last_packet_index = self.S2HRead(self.registers.S2HValidPacketIndexReg)
        self.Log('debug', 'max packets before loss = {} '.format(max_packets_in_site_controller))
        self.Log('debug', 'last packet index inside setup {}'.format(last_packet_index.value))
        if last_packet_index.value == self.NoValidPacketData:
            last_packet_index.value = max_packets_in_site_controller - 1
        return last_packet_index, max_packets_in_site_controller

    def ReadStreamingDetails(self, addr):
        pkt_byte_count = self.S2HRead(self.registers.S2HPacketByteCountReg)
        max_packets_in_site_controller = self.S2HRead(self.registers.S2HPacketCountReg)
        buffer_size = (ctypes.c_ubyte * pkt_byte_count.value * max_packets_in_site_controller.value).from_address(addr)
        self.Log('debug', 'pkt_byte_count: {} NumberOfPackets: {} buffersize: {}'.format(pkt_byte_count.value,
                                                                                         max_packets_in_site_controller.value,
                                                                                         buffer_size))
        return max_packets_in_site_controller.value, pkt_byte_count, buffer_size

    def ReadPacketIndexandPacketHeader(self, buffer_size):
        packet_index = self.S2HRead(self.registers.S2HValidPacketIndexReg).value
        packet_number_from_packet_header = self.ReadPacketNumber(packet_index, buffer_size)
        self.Log('info',
                 'Packet Index Register: {} Packet Count: {}. '.format(packet_index, packet_number_from_packet_header))
        return packet_index, packet_number_from_packet_header

    def WaitTime(self, time_to_wait):
        start_time = time.perf_counter()
        while time.perf_counter() - start_time < time_to_wait:
            pass

    def reset_aurora_trigger_bus(self):
        resets_register = self.getResetsRegister()
        self.WriteRegister(resets_register(value=0))
