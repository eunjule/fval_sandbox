################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import ctypes
from ctypes import c_uint

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

import Common.register as register


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvilRegister):
                if attribute is not HvilRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvilRegister):
                if attribute is not HvilRegister:
                    # if '__' not in attribute.__name__:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvilStruct):
                if attribute is not HvilStruct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HvilStruct):
                if attribute is not HvilStruct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class HvilRegister(register.Register):
    BASEMUL = 4

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        try:
            l.append('ADDR=0x{:04X}'.format(self.ADDR))
        except AttributeError:
            pass
        try:
            l.append('REGCOUNT={}'.format(self.REGCOUNT))
        except AttributeError:
            pass
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class HvilStruct(register.Register):
    ADDR = None

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class DUMMY_STRUCT(HvilStruct):
    _fields_ = [('Data', c_uint, 32)]


class BAR1_SCRATCH(HvilRegister):
    BAR = 1
    ADDR = 0x0
    _fields_ = [('Data', c_uint, 32)]


class FPGA_VERSION(HvilRegister):
    BAR = 1
    ADDR = 0x4
    _fields_ = [('Data', c_uint, 32)]


class RESETS(HvilRegister):
    BAR = 1
    ADDR = 0x8
    _fields_ = [('Unused', c_uint, 2),
                ('DDRControllerReset', c_uint, 1),
                ('BlockAuroraReceive', c_uint, 1),
                ('Data', c_uint, 28)]


class STATUS(HvilRegister):
    BAR = 1
    ADDR = 0xc
    _fields_ = [('Unused', c_uint, 5),
                ('DDRControllerPLLLock', c_uint, 1),
                ('DDRCalibrationDone', c_uint, 1),
                ('Data', c_uint, 25), ]


class SLOT_ID(HvilRegister):
    BAR = 1
    ADDR = 0x10
    _fields_ = [('Data', c_uint, 32)]


class EXECUTE_TRIGGER(HvilRegister):
    BAR = 1
    ADDR = 0x14
    _fields_ = [('ByteAddress', c_uint, 29),
                ('TriggerPending',c_uint, 1),
                ('Reserved',c_uint, 1),
                ('Execute', c_uint, 1)]


class EXECUTE_INTERRUPT(HvilRegister):
    BAR = 1
    ADDR = 0x18
    _fields_ = [('Data', c_uint, 32)]


class SET_SAFE_STATE(HvilRegister):
    BAR = 1
    ADDR = 0x1c
    _fields_ = [('RailSafeBit', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 31)]

class TRIG_RX_TEST_FIFO_DATA_COUNT(HvilRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Data', c_uint, 32)]

class MEM_WR_DATA_TAG(HvilRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('Data', c_uint, 32)]


class MEM_RD_DATA_TAG(HvilRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('Data',c_uint, 32)]


class MEM_ADDRESS_TAG(HvilRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('Data', c_uint, 32)]


class TRIGGER_UP_TEST(HvilRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('Data',c_uint, 32)]


class TRIGGER_DN_TEST(HvilRegister):
    BAR = 1
    ADDR = 0x54
    _fields_ = [('Data', c_uint, 32)]


class SAMPLE_COLLECTOR_INDEX(HvilRegister):
    BAR = 1
    ADDR = 0x0068
    _fields_ = [('SelectedIndex', c_uint, 5),
                ('Data', c_uint, 26),
                ('RawOrFloatingPointValue', c_uint, 1)]


class SAMPLE_COLLECTOR_COUNT(HvilRegister):
    BAR = 1
    ADDR = 0x006c
    _fields_ = [('SampleCount', c_uint, 9),
                ('Data', c_uint, 23)]


class SAMPLE_COLLECTOR_DATA(HvilRegister):
    BAR = 1
    ADDR = 0x0070
    _fields_ = [('Data', c_uint, 32)]


class TURBO_SAMPLE_COLLECTOR_INDEX(HvilRegister):
    BAR = 1
    ADDR = 0x0058
    _fields_ = [('SelectedIndex', c_uint, 5),
                ('Data', c_uint, 26),
                ('RawOrFloatingPointValue', c_uint, 1)]

class TURBO_SAMPLE_COLLECTOR_COUNT(HvilRegister):
    BAR = 1
    ADDR = 0x005c
    _fields_ = [('SampleCount', c_uint, 9),
                ('Data', c_uint, 23)]


class TURBO_SAMPLE_COLLECTOR_DATA(HvilRegister):
    BAR = 1
    ADDR = 0x0060
    _fields_ = [('Data', c_uint, 32)]

module_dict = globals()


class DUT_DOMAIN_ID(HvilRegister):
    BAR= 1
    ADDR = 0x80
    REGCOUNT = 8
    _fields_ = [('DutDomainId', c_uint, 6),
                ('DutValidBit', c_uint, 1),
                ('Data', c_uint, 25)]


class UHC_CHANNEL_CONFIG(HvilRegister):
    BAR= 1
    ADDR = 0xa0
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]



class TURBO_MODE_CURRENT_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0xc0
    _fields_ = [('Data', c_uint, 32)]


class TURBO_MODE_CURRENT_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0xc4
    _fields_ = [('Data', c_uint, 32)]


class TURBO_MODE_CURRENT_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0xc8
    _fields_ = [('Data', c_uint, 32)]


class TURBO_MODE_CURRENT_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0xcc
    _fields_ = [('Data', c_uint, 32)]


class CHANNELS_LOOPING(HvilRegister):
    BAR = 1
    ADDR = 0xe0
    _fields_ = [('Data', c_uint, 32)]



class SSHOT_CONTROL(HvilRegister):
    BAR = 1
    ADDR = 0xe4
    _fields_ = [('Data', c_uint, 32)]

class HVIL_GPIO(HvilRegister):
    BAR = 1
    ADDR = 0x074
    _fields_ = [ ('RRPollingEnable', ctypes.c_uint, 1),
                ('TurboPollingEnable', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 30)]



class SSHOT_STOP_TRIGGER(HvilRegister):
    BAR = 1
    ADDR = 0xe8
    _fields_ = [('Data', c_uint, 32)]


class SSHOT_TRIGGER_FIFO_TIMESTAMP(HvilRegister):
    BAR = 1
    ADDR = 0xec
    _fields_ = [('Data', c_uint, 32)]


class SSHOT_TRIGGER_FIFO_PAYLOAD(HvilRegister):
    BAR = 1
    ADDR = 0xf0
    _fields_ = [('Data', c_uint, 32)]


class SSHOT_END_TIMESTAMP(HvilRegister):
    BAR = 1
    ADDR = 0xf4
    _fields_ = [('Data', c_uint, 32)]


class SSHOT_CHANNEL_END_SAMPLE_ADDRESS(HvilRegister):
    BAR = 1
    ADDR = 0xf8
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_0_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x100
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_0_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x104
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_0_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x108
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_0_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x10c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_1_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x110
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_1_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x114
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_1_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x118
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_1_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x11c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_2_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x120
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_2_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x124
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_2_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x128
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_2_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x12c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_3_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x130
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_3_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x134
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_3_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x138
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_3_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x13c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_4_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x140
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_4_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x144
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_4_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_4_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x14c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_5_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x150
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_5_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x154
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_5_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x158
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_5_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x15c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_6_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x160
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_6_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x164
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_6_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x168
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_6_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x16c
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_7_HEADER_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x170
    _fields_ = [('Data',c_uint, 32)]


class CHANNEL_UHC_7_HEADER_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x174
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_UHC_7_SAMPLE_REGION_BASE(HvilRegister):
    BAR = 1
    ADDR = 0x178
    _fields_ = [('Data',c_uint, 32)]


class CHANNEL_UHC_7_SAMPLE_REGION_SIZE(HvilRegister):
    BAR = 1
    ADDR = 0x17c
    _fields_ = [('Data', c_uint, 32)]

class CHANNEL_OVERCURRENT_ALARM_FAILING_CURRENT_VALUE(HvilRegister):
    BAR = 1
    REGCOUNT = 8
    ADDR = 0x180
    _fields_ = [('Data', c_uint, 32)]

class CHANNEL_REGULAR_CURRENT_ADC_GAIN_OFFSET(HvilRegister):
    BAR = 1
    REGCOUNT = 8
    ADDR = 0x1ac
    _fields_ = [('Offset', ctypes.c_uint, 16),
                ('Gain', ctypes.c_uint, 16)]

class CAL_CHANNEL_0_MEASURE_MUX(HvilRegister):
    BAR = 1
    ADDR = 0x1d8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VCOMP_SAFETY_LIMIT_HIGH(HvilRegister):
    BAR = 1
    ADDR = 0x1dc
    _fields_ = [('VoltageValue', ctypes.c_uint, 18),
                ('Data',ctypes.c_uint, 14) ]


class VCOMP_SAFETY_LIMIT_LOW(HvilRegister):
    BAR = 1
    ADDR = 0x1e0
    _fields_ = [('VoltageValue', ctypes.c_uint, 18),
                ('Data',ctypes.c_uint, 14) ]

class OVERCURRENT_FILTER_COUNT(HvilRegister):
    BAR = 1
    ADDR = 0x1e4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class OVERVOLTAGE_FILTER_COUNT(HvilRegister):
    BAR = 1
    ADDR = 0x1e8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHANNEL_V(HvilRegister):
    BAR = 1
    ADDR = 0x200
    REGCOUNT = 8
    BASEMUL = 8
    _fields_ = [('SoftSpanCode', ctypes.c_uint, 3),
                ('ChannelId', ctypes.c_uint, 3),
                ('VoltageValue', ctypes.c_uint, 18),
                ('Data', ctypes.c_uint, 8)]


class CHANNEL_I(HvilRegister):
    BAR = 1
    ADDR = 0x204
    REGCOUNT = 8
    BASEMUL = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class CHANNEL_TURBO_I(HvilRegister):
    BAR = 1
    ADDR = 0x240
    _fields_ = [('Data', ctypes.c_uint, 32)]

class ADT7411_I2C_STATUS_REG(HvilRegister):
    BAR = 1
    ADDR = 0x24c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ADT7411_I2C_CONTROL_REG(HvilRegister):
    BAR = 1
    ADDR = 0x250
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ADT7411_I2C_TX_FIFO_DATA_REG(HvilRegister):
    BAR = 1
    ADDR = 0x254
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ADT7411_I2C_RX_FIFO_DATA_REG(HvilRegister):
    BAR = 1
    ADDR = 0x258
    _fields_ = [('Data', ctypes.c_uint, 32)]

class ADC_SOFTSPAN_CODE_VALUE_REGISTER(HvilRegister):
    BAR = 1
    ADDR = 0x01cc
    _fields_ = [('SoftSpanCode', c_uint, 3),
                ('Data', c_uint, 29)]

class CAPABILITY_REGISTER(HvilRegister):
    BAR = 1
    ADDR = 0x078
    _fields_ = [('CapabilityBit', c_uint, 1),
                ('Data', c_uint, 31)]


class GLOBAL_ALARMS(HvilRegister):
    BAR = 1
    ADDR = 0x280
    _fields_ = [('TqNotifyAlarmDut0', c_uint, 1),
                ('TqNotifyAlarmDut1', c_uint, 1),
                ('TqNotifyAlarmDut2', c_uint, 1),
                ('TqNotifyAlarmDut3', c_uint, 1),
                ('TqNotifyAlarmDut4', c_uint, 1),
                ('TqNotifyAlarmDut5', c_uint, 1),
                ('TqNotifyAlarmDut6', c_uint, 1),
                ('TqNotifyAlarmDut7', c_uint, 1),
                ('ChannelGlobalAlarm', c_uint, 1),
                ('ChannelGlobalSampleAlarm', c_uint, 1),
                ('ChannelGlobalControlledFoldAlarm', c_uint, 1),
                ('BrdTempAlarm', c_uint, 1),
                ('TrigExecAlarm', c_uint, 1),
                ('Data', c_uint, 19)]
non_critical_global_alarms = ['TqNotifyLCHC','TqNotifyVlc','TqNotifyAlarmDut0','TqNotifyAlarmDut1','TqNotifyAlarmDut2','TqNotifyAlarmDut3','TqNotifyAlarmDut4','TqNotifyAlarmDut5','TqNotifyAlarmDut6','TqNotifyAlarmDut7']
critical_global_alarms = [x for x in register.get_field_names(GLOBAL_ALARMS) if x not in non_critical_global_alarms]


class CHANNEL_SAMPLE_ALARMS(HvilRegister):
    BAR = 1
    ADDR = 0x284
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_ALARMS(HvilRegister):
    BAR = 1
    ADDR = 0x288
    _fields_ = [('Data', c_uint, 32)]



class CHANNEL_ALARMS_DETAILS(HvilRegister):
    BAR = 1
    ADDR = 0x28c
    REGCOUNT = 8
    _fields_ = [('ChannelOverVoltage', c_uint, 1),
             ('ChannelOverCurrent',c_uint, 1),
             ('ChannelThermalOverCurrent', c_uint, 1),
             ('ChannelLooping', c_uint, 1),
             ('RailBusy', c_uint, 1),
             ('Data', c_uint, 27)]


class CHANNEL_CONTROLLED_FOLD_ALARMS(HvilRegister):
    BAR = 1
    ADDR = 0x2bc
    _fields_ = [('Data', c_uint, 32)]


class CHANNEL_CONTROLLED_FOLD_ALARM_DETAILS(HvilRegister):
    BAR = 1
    ADDR = 0x2c0
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]


class TRIG_EXEC_ALARMS(HvilRegister):
    BAR = 1
    ADDR = 0x2e0
    _fields_ = [('Data', c_uint, 32)]


class MAX6627_TEMP_ALARMS_DETAILS(HvilRegister):
    BAR = 1
    ADDR = 0x2e4
    _fields_ = [('MAX6627Device0HighAlarm', c_uint, 1),
                ('MAX6627Device1HighAlarm', c_uint, 1),
                ('MAX6627Device2HighAlarm', c_uint, 1),
                ('MAX6627Device3HighAlarm', c_uint, 1),
                ('MAX6627Device4HighAlarm', c_uint, 1),
                ('MAX6627Device5HighAlarm', c_uint, 1),
                ('MAX6627Device0LowAlarm', c_uint, 1),
                ('MAX6627Device1LowAlarm', c_uint, 1),
                ('MAX6627Device2LowAlarm', c_uint, 1),
                ('MAX6627Device3LowAlarm', c_uint, 1),
                ('MAX6627Device4LowAlarm', c_uint, 1),
                ('MAX6627Device5LowAlarm', c_uint, 1),
                ('UnassignedBit13', c_uint, 1),
                ('UnassignedBit14', c_uint, 1),
                ('UnassignedBit15', c_uint, 1),
                ('UnassignedBit16', c_uint, 1),
                ('UnassignedBit17', c_uint, 1),
                ('UnassignedBit18', c_uint, 1),
                ('UnassignedBit19', c_uint, 1),
                ('UnassignedBit20', c_uint, 1),
                ('UnassignedBit21', c_uint, 1),
                ('UnassignedBit22', c_uint, 1),
                ('UnassignedBit23', c_uint, 1),
                ('UnassignedBit24', c_uint, 1),
                ('UnassignedBit25', c_uint, 1),
                ('UnassignedBit26', c_uint, 1),
                ('UnassignedBit27', c_uint, 1),
                ('UnassignedBit28', c_uint, 1),
                ('UnassignedBit29', c_uint, 1),
                ('UnassignedBit30', c_uint, 1),
                ('UnassignedBit31', c_uint, 1),
                ('UnassignedBit32', c_uint, 1),]


class CHANNELS_FOLDED(HvilRegister):
    BAR = 1
    ADDR = 0x2e8
    _fields_ = [('Data',c_uint, 32)]


class P2P_CTRL(HvilRegister):
    BAR = 1
    ADDR = 0x300
    _fields_ = [('Data', c_uint, 32)]


class P2P_START_ADDR_HI(HvilRegister):
    BAR = 1
    ADDR = 0x304
    _fields_ = [('Data', c_uint, 32)]


class P2P_START_ADDR_LO(HvilRegister):
    BAR = 1
    ADDR = 0x308
    _fields_ = [('Data', c_uint, 32)]

class CHANNEL_LOAD_STATE(HvilRegister):
    BAR = 1
    ADDR = 0x380
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]

class CHANNEL_TURBO_CURRENT_ADC_GAIN_OFFSET(HvilRegister):
    BAR = 1
    REGCOUNT = 8
    ADDR = 0x400
    _fields_ = [('Offset', ctypes.c_uint, 16),
                ('Gain', ctypes.c_uint, 16)]

class S2HControlReg(HvilRegister):
    BAR = 1
    ADDR = 0x0804
    _fields_ = [('S2HStreamingStart', c_uint, 1),
                ('S2HStreamingStop', c_uint, 1),
                ('S2HStreamingReset', c_uint, 1),
                ('Reserved', c_uint, 28),
                ('S2HStreamingStatus', c_uint, 1),]


class S2HAddressHighReg(HvilRegister):
    BAR = 1
    ADDR = 0x0810
    _fields_ = [('S2HAddressHigh', c_uint, 32)]


class S2HAddressLowReg(HvilRegister):
    BAR = 1
    ADDR = 0x0814
    _fields_ = [('S2HAddressLow',c_uint, 32)]


class S2HValidPacketIndexReg(HvilRegister):
    BAR = 1
    ADDR = 0x0818
    _fields_ = [('S2HValidPacketIndex', c_uint, 16),
                ('Reserved', c_uint, 16)]


class S2HPacketByteCountReg(HvilRegister):
    BAR = 1
    ADDR = 0x081C
    _fields_ = [('S2HPacketByteCount', c_uint, 16),
                ('Reserved', c_uint, 16)]


class S2HPacketCountReg(HvilRegister):
    BAR = 1
    ADDR = 0x0820
    _fields_ = [('S2HPacketCount', c_uint, 16),
                ('Reserved',c_uint, 16)]


class S2HConfigurationReg(HvilRegister):
    BAR = 1
    ADDR = 0x824
    _fields_ = [('S2HConfigurationDone', c_uint, 1),
                ('Reserved',c_uint, 31)]

class MAX6627_LOWER_TEMP_LIMIT(HvilRegister):
    BAR = 1
    ADDR = 0x340
    REGCOUNT = 6
    _fields_ = [('LowerTemperatureValue', ctypes.c_uint, 12),
                ('SignBit', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 19)]


class MAX6627_UPPER_TEMP_LIMIT(HvilRegister):
    BAR= 1
    ADDR = 0x328
    REGCOUNT = 6
    _fields_ = [('UpperTemperatureValue', ctypes.c_uint, 12),
                ('SignBit', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 19)]

class MAX6627_TEMP_VALUE(HvilRegister):
    BAR = 1
    ADDR = 0x358
    REGCOUNT = 6
    _fields_ = [('TemperatureValue', ctypes.c_uint, 12),
                ('SignBit', ctypes.c_uint, 1),
                ('Data', ctypes.c_uint, 19)]


class BAR2_SCRATCH(HvilRegister):
    BAR = 2
    ADDR = 0x0
    _fields_ = [('Data', c_uint, 16)]


class UHC_SAMPLE_ENGINE_RESET(HvilRegister):
    BAR = 2
    ADDR = 0x4
    _fields_ = [('Data', c_uint, 16)]


class UHC_SAMPLING_ACTIVE(HvilRegister):
    BAR = 2
    ADDR = 0x8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0x80
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0x84
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0x88
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0x8c
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0x90
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0x94
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_0_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0x98
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0xa0
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0xa4
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0xa8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0xac
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0xb0
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0xb4
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_1_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0xb8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0xc0
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0xc4
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0xc8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0xcc
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0xd0
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0xd4
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_2_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0xd8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0xe0
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0xe4
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0xe8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0xec
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0xf0
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0xf4
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_3_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0xf8
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0x100
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0x104
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0x108
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0x10c
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0x110
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0x114
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_4_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0x118
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0x120
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0x124
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0x128
    _fields_ = [('Data',c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0x12c
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0x130
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0x134
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_5_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0x138
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0x140
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0x144
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0x148
    _fields_ = [('Data',c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0x14c
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0x150
    _fields_ = [('Data',c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0x154
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_6_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0x158
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0x160
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0x164
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0x168
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0x16c
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0x170
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0x174
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_UHC_7_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0x178
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_COUNT(HvilRegister):
    BAR = 2
    ADDR = 0x180
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_RATE(HvilRegister):
    BAR = 2
    ADDR = 0x184
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_METADATA_HI(HvilRegister):
    BAR = 2
    ADDR = 0x188
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_METADATA_LO(HvilRegister):
    BAR = 2
    ADDR = 0x18c
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_CHANNEL_SELECT(HvilRegister):
    BAR = 2
    ADDR = 0x190
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_DELAY(HvilRegister):
    BAR = 2
    ADDR = 0x194
    _fields_ = [('Data', c_uint, 16)]


class TURBO_MODE_CURRENT_SAMPLE_START(HvilRegister):
    BAR = 2
    ADDR = 0x198
    _fields_ = [('Data', c_uint, 16)]


class CHANNEL_LOAD_CONTROL(HvilRegister):
    BAR = 2
    ADDR = 0x1a0
    REGCOUNT = 8
    _fields_ = [('ChannelLoadResistor', ctypes.c_uint, 8),
                ('Data', ctypes.c_uint, 8)]


class CHANNEL_LOAD_ACTIVATE(HvilRegister):
    BAR = 2
    ADDR = 0x1c8
    _fields_ = [('Data', c_uint, 16)]


class SET_CLEAR_LOAD_CHANNEL_ENABLE(HvilRegister):
    BAR = 2
    ADDR = 0x1cc
    _fields_ = [('Data', c_uint, 16)]



class CHANNEL_USER_CURRENT_CLAMP_LIMIT(HvilRegister):
    ADDR = 0x1d0
    REGCOUNT = 8
    BAR = 2
    _fields_ = [('CurrentClampLimit', c_uint, 16)]

class CHANNEL_THERMAL_CURRENT_CLAMP_LIMIT(HvilRegister):
    ADDR = 0x200
    REGCOUNT = 8
    BAR = 2
    _fields_ = [('ThermalCurrentClampLimit', c_uint, 16)]


class Bar2RailCommand(HvilStruct):
    _fields_ = [('RailNumberOrResourceId', c_uint, 8),
                ('RailCommand', c_uint, 7)]

class NonBroadcastBar2RailCommand(HvilStruct):
    _fields_ = [('RailProcessor', c_uint, 3),
                ('Reserved', c_uint, 1),
                ('ResourceType', c_uint, 1),
                ('ChannelID', c_uint, 3),
                ('RailCommand', c_uint, 7),
                ('Reserved', c_uint, 1)]
