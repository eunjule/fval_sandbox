################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename= symbols.py
#-------------------------------------------------------------------------------
#     Purpose= HDDPS commands
#-------------------------------------------------------------------------------
#  Created by= Renuka Agrawal
#        Date= 11/17/15
#       Group= HDMT FPGA Validation
################################################################################

import sys

class RESOURCEIDS:
    LC = 0x10
    HC = 0x10
    HV = 0x10
    VLC = 0x00
    LVM = 0x00
    RLOAD = 0x00

class RAILCOMMANDS:
    SET_VOLTAGE                   = 0x17
    SET_CURRENT                   = 0x18
    SET_CURRENT_RANGE             = 0x19
    ENABLE_DISABLE_RAIL           = 0x1A
    SET_OVER_VOLTAGE              = 0x1B
    SET_UNDER_VOLTAGE             = 0x1C
    SET_CURRENT_CLAMP_HI          = 0x1D
    SET_CURRENT_CLAMP_LOW         = 0x1E
    GENERAL_COMMUNICATION         = 0x20
    SET_SAFE_STATE                = 0x21
    TIME_DELAY                    = 0x22
    SET_MODE                      = 0x23
    SET_RAIL_SLEW_RATE            = 0x24
    ENABLE_VOLTAGE_COMPARE_ALARM  = 0x25
    START_FREE_DRIVE              = 0x26 # HC and LC
    END_FREE_DRIVE                = 0x27 # HC and LC
    FORCE_CLAMP_FLUSH             = 0x28 #LC
    ENABLE_DISABLE_TRIGGER_SLAVES = 0x2A
    SET_FREE_DRIVE_HIGH_CURRENT   = 0x2B # HC and LC
    SET_FREE_DRIVE_LOW_CURRENT    = 0x2C # HC and LC
    END_FREE_DRIVE_ENABLE         = 0x2F # Not to be used in tq, Used internally
    TRIGGER_Q_COMPLETE            = 0x31
    TQ_NOTIFY                     = 0x32
    SET_HC_ENABLE_DISABLE         = 0x37
    SET_I_CLAMP_DISABLE           = 0x38
    ASSERT_TEST_RAIL              = 0x39
    SET_SLAVE_RAIL_ENABLE_DISABLE = 0x3A
    SUPPRESS_ALARMS               = 0x3B
    ENABLE_DISABLE_LOCAL_SENSE    = 0x3C
    LOOP_CONTROL                  = 0x3D
    LOOP_TRIGGER                  = 0x3E
    SOFTWARE_TRIGGER              = 0x3F
    # ASSERT_LOOP_COMPLETE          = 0x40
    HARDWARE_TRIGGER              = 0x41
    WAKE_RAIL                     = 0x43
    SET_RAIL_BUSY                 = 0x44
    SET_TRACKING_VOLTAGE          = 0x45
    SET_THERMAL_I_H_CLAMP         = 0x46
    SET_DUT_OV_LIMIT_SS3          = 0x48
    SET_DUT_UV_LIMIT_SS3          = 0x49
    SET_DUT_OV_LIMIT_SS7          = 0x4a
    SET_DUT_UV_LIMIT_SS7          = 0x4b
    RESET_OV_LIMIT                = 0x4c
    SET_SOFTSPAN_CODE             = 0x4d
    TRIGGER_SLAVES_TRACKING_VOLTAGE = 0x4e
    SET_V_RANGE                   = 0x4f
    SET_USER_OV_UV_ALARM_ENABLE_DISABLE = 0x50
    HW_SWITCH_ENABLE              = 0x51
    TRIGGER_SLAVES_HW_SWITCH_ENABLE = 0x52
    SET_AUTO_COMPENSATION =         0x53

    SET_CHANNEL_LOAD_SELECT =        0x50
    SET_CHANNEL_LOAD_ACTIVATE =     0x51
    CHANNEL_CONNECT=                0x52
    CHANNEL_DISCONNECT=             0x53
    SET_USER_CURRENT_CLAMP =        0x54
    SET_THERMAL_CURRENT_CLAMP =     0x55

class SETMODECMD:
    VFORCE                        = 0
    IFORCE                        = 1
    ISINK                         = 2
    VMEASURE                      = 3

class VRANGE:
    V_6_VOLT                  = 0
    V_22_VOLT                  = 1
    V_25_VOLT                  = 2


class HCLCIRANGE:
    I_5_UA                  = 0                
    I_25_UA                 = 1
    I_250_UA                = 2    
    I_2_5_MA                = 3    
    I_25_MA                 = 4
    I_500_MA                = 5    
    I_1200_MA               = 6
    I_7500_MA               = 7
    I_10000_MA              = 7
    I_25000_MA              = 11
    I_3000_MA               = 12

class AD5560RSENSE:
    R_SENSE_I_5_UA = 100000.0
    R_SENSE_I_25_UA = 20000.0
    R_SENSE_I_250_UA = 2000.0
    R_SENSE_I_2_5_MA = 200.0
    R_SENSE_I_25_MA = 20.0
    R_SENSE_I_500_MA = 1.0
    R_SENSE_I_1200_MA = 0.4
    R_SENSE_I_7500_MA = 0.05
    R_SENSE_I_10000_MA = 0.05
    R_SENSE_I_25000_MA = 0.02
    R_SENSE_I_3000_MA = 0.4

class VLCIRANGE:
    I_2_56_UA               = 0
    I_25_6_UA               = 1
    I_256_UA                = 2 
    I_2_56_MA               = 3
    I_25_6_MA               = 4
    I_256_MA                = 5

class ASSERTTEST:
    OPMODE                        = 0
    POWER                         = 1
    HCPOWER                       = 2
    IRANGE                        = 3
    SLAVE_POWER_STATE             = 4
    LOOP_COMPLETE                 = 5 

class DPSTRIGGERS:
    DPSTRIGGERTYPE                = 0x1
    LTCTRIGGERTYPE                = 0x3F
    EVENTTRIGGERTYPE              = 0x5

class EVENTTRIGGER:
    START                       = 0x4
    STOP                        = 0x3
    S2H_START                   = 0x1
    S2H_STOP                    = 0x0
    S2H_RESET                   = 0x2
    
class CROSSBOARDTRIGGER:
    DPSCTRLTYPE                 = 0x1
    CBGENABLE                   = 0x0
    CBGDISABLE                  = 0x1
    CBFOLD                      = 0x2
    
class ISL55180:
    ISL55180_0                    = 0x60
    ISL55180_0_REG                = 0x61
    ISL55180_1                    = 0x62
    ISL55180_1_REG                = 0x63
    ISL55180_2                    = 0x64
    ISL55180_2_REG                = 0x65
    ISL55180_3                    = 0x66
    ISL55180_3_REG                = 0x67
    ISL55180_4                    = 0x68
    ISL55180_4_REG                = 0x69
    ISL55180_5                    = 0x6A
    ISL55180_5_REG                = 0x6B
    ISL55180_6                    = 0x6C
    ISL55180_6_REG                = 0x6D
    ISL55180_7                    = 0x6E
    ISL55180_7_REG                = 0x6F
 
    ISL55180_8                    = 0x70
    ISL55180_8_REG                = 0x71
    ISL55180_9                    = 0x72
    ISL55180_9_REG                = 0x73
    ISL55180_10                   = 0x74
    ISL55180_10_REG               = 0x75
    ISL55180_11                   = 0x76
    ISL55180_11_REG               = 0x77
    ISL55180_12                   = 0x78
    ISL55180_12_REG               = 0x79
    ISL55180_13                   = 0x7A
    ISL55180_13_REG               = 0x7B
    ISL55180_14                   = 0x7C
    ISL55180_14_REG               = 0x7D
    ISL55180_15                   = 0x7E
    ISL55180_15_REG               = 0x7F

class ISL55180REGS:
    FORCEA_LEVEL                  = 0x00
    FORCEB_LEVEL                  = 0x01
    I_CLAMP_H_LEVEL               = 0x02
    I_CLAMP_L_LEVEL               = 0x03
    FORCEA_OFFSET                 = 0x10
    FORCEB_OFFSET                 = 0x11
    I_CLAMP_H_OFFSET              = 0x12
    I_CLAMP_L_OFFSET              = 0x13
    FORCEA_GAIN                   = 0x20
    FORCEB_GAIN                   = 0x21
    I_CLAMP_H_GAIN                = 0x22
    I_CLAMP_L_GAIN                = 0x23
    SOURCE_FB_SEL                 = 0
    DPS_CONT_MISC                 = 1
    CLAMP_ALARM_CONT              = 2
    STATUS_READ_BACK              = 3
    DIAG_AND_CAL                  = 4
    MEAS_UNIT_SRC_SEL             = 5
    OFFSET_ADJ                    = 6
    GAIN_ADJ                      = 7
    CME_ADJ_IR0                   = 8
    CME_ADJ_IR1                   = 9
    CME_ADJ_IR2                   = 10
    CME_ADJ_IR3_LV                = 11
    CME_ADJ_IR3_HV                = 12
    CME_ADJ_IR4_LV                = 13
    CME_ADJ_IR4_HV                = 14
    CME_ADJ_IR5                   = 15
    ESP_RES_COMP_VAL              = 16
    CPU_RESET_WO                  = 128
    MEAS_CONT_GANG                = 129
    MEAS_IMON_CENT_SEL            = 130
    ALARM_CONTROL                 = 131
    UPPER_DAC_BIT_CAL             = 132
    MID_DAC_BIT_CAL               = 133
    DACN_DACP                     = 134
    DIE_ID_RO                     = 255

class AD5560:

    AD5560_0_ADDR = 0x05
    AD5560_1_ADDR = 0x06
    AD5560_2_ADDR = 0x07
    AD5560_3_ADDR = 0x08
    AD5560_4_ADDR = 0x09
    AD5560_5_ADDR = 0x0A
    AD5560_6_ADDR = 0x0B
    AD5560_7_ADDR = 0x0C
    AD5560_8_ADDR = 0x0D
    AD5560_9_ADDR = 0x0E

class AD5560REGS:
    NOP                       = 0
    SYSTEM_CONTROL            = 1
    DPS_REGISTER1             = 2
    DPS_REGISTER2             = 3
    COMPENSATION1             = 4
    COMPENSATION2             = 5 
    ALARM_SETUP               = 6
    DIAGNOSTIC                = 7
    FIN_DAC_X1                = 8 
    FIN_DAC_M                 = 9 
    FIN_DAC_C                 = 10
    OFFSET_DACX               = 11
    OSD_DACX                  = 12
    CLL_DAC_X1                = 13
    CLL_DAC_M                 = 14
    CLL_DAC_C                 = 15 
    CLH_DAC_X1                = 16
    CLH_DAC_M                 = 17
    CLH_DAC_C                 = 18
    CPL_DACX1_5uA             = 19
    CPL_DAC_M_5uA             = 20
    CPL_DAC_C_5uA             = 21
    CPL_DACX1_25uA            = 22
    CPL_DAC_M_25uA            = 23
    CPL_DAC_C_25uA            = 24
    CPL_DACX1_250uA           = 25
    CPL_DAC_M_250uA           = 26
    CPL_DAC_C_250uA           = 27
    CPL_DACX1_2_5mA           = 28
    CPL_DAC_M_2_5mA           = 29
    CPL_DAC_C_2_5mA           = 30
    CPL_DACX1_25mA            = 31
    CPL_DAC_M_25mA            = 32
    CPL_DAC_C_25mA            = 33
    CPL_DACX1_EXT2            = 34
    CPL_DAC_M_EXT2            = 35
    CPL_DAC_C_EXT2            = 36
    CPL_DACX1_EXT1            = 37
    CPL_DAC_M_EXT1            = 38
    CPL_DAC_C_EXT1            = 39
    CPH_DACX1_5uA             = 40
    CPH_DAC_M_5uA             = 41
    CPH_DAC_C_5uA             = 42
    CPH_DACX1_25uA            = 43 
    CPH_DAC_M_25uA            = 44
    CPH_DAC_C_25uA            = 45
    CPH_DACX1_250uA           = 46
    CPH_DAC_M_250uA           = 47
    CPH_DAC_C_250uA           = 48
    CPH_DACX1_2_5mA           = 49
    CPH_DAC_M_2_5mA           = 50
    CPH_DAC_C_2_5mA           = 51
    CPH_DACX1_25mA            = 52
    CPH_DAC_M_25mA            = 53
    CPH_DAC_C_25mA            = 54
    CPH_DACX1_EXT2            = 55
    CPH_DAC_M_EXT2            = 56
    CPH_DAC_C_EXT2            = 57
    CPH_DACX1_EXT1            = 58
    CPH_DAC_M_EXT1            = 59
    CPH_DAC_C_EXT1            = 60
    DGS_DAC                   = 61
    RAMP_END_CODE             = 62
    RAMP_STEP_SIZE            = 63
    RCLK_DIVIDER              = 64
    ENABLE_RAMP               = 65
    INTERRUPT_RAMP            = 66
    ALARM_STATUS              = 67
    ALARM_CLEAR               = 68
    CPL_DAC_X1                = 69
    CPL_DAC_M                 = 70
    CPL_DAC_C                 = 71
    CPH_DAC_X1                = 72
    CPH_DAC_M                 = 73
    CPH_DAC_C                 = 74
    END                       = 75

class CONSTANTS:
    DisableClampAndKelvinAlarmMask = 0x200
    ClampAlarmContSmallComp        = 0x002F
    SourceFBSelEnableRemoteSense   = 0x0050
    DisableTightLoop               = 0x3
    EnableCompensationCap3         = 0x8200
    ClampRangeMaskNoAlarm          = 0x2B
    ClearClampAndKelvinAlarmMask   = 0x7000
    EnableClampAndKelvinAlarmMask  = 0x380

class BAR2REGS:
    Scratch                      = 0x00
    
    HclcSampleEngineReset        = 0x04
    HclcSamplingActive           = 0x05
    
    VlcSampleEngineReset         = 0x08
    VlcSamplingActive            = 0x09
    
    Hclc0SampleCount             = 0x20
    Hclc0SampleRate              = 0x21
    Hclc0SampleMetadataHi        = 0x22
    Hclc0SampleMetadataLo        = 0x23
    Hclc0SampleRailSelect        = 0x24
    Hclc0SampleDelay             = 0x25
    Hclc0SampleStart             = 0x26

    Hclc1SampleCount             = 0x28
    Hclc1SampleRate              = 0x29
    Hclc1SampleMetadataHi        = 0x2a
    Hclc1SampleMetadataLo        = 0x2b
    Hclc1SampleRailSelect        = 0x2c
    Hclc1SampleDelay             = 0x2d
    Hclc1SampleStart             = 0x2e

    Hclc2SampleCount             = 0x30
    Hclc2SampleRate              = 0x31
    Hclc2SampleMetadataHi        = 0x32
    Hclc2SampleMetadataLo        = 0x33
    Hclc2SampleRailSelect        = 0x34
    Hclc2SampleDelay             = 0x35
    Hclc2SampleStart             = 0x36

    Hclc3SampleCount             = 0x38
    Hclc3SampleRate              = 0x39
    Hclc3SampleMetadataHi        = 0x3a
    Hclc3SampleMetadataLo        = 0x3b
    Hclc3SampleRailSelect        = 0x3c
    Hclc3SampleDelay             = 0x3d
    Hclc3SampleStart             = 0x3e

    Hclc4SampleCount             = 0x40
    Hclc4SampleRate              = 0x41
    Hclc4SampleMetadataHi        = 0x42
    Hclc4SampleMetadataLo        = 0x43
    Hclc4SampleRailSelect        = 0x44
    Hclc4SampleDelay             = 0x45
    Hclc4SampleStart             = 0x46

    Hclc5SampleCount             = 0x48
    Hclc5SampleRate              = 0x49
    Hclc5SampleMetadataHi        = 0x4a
    Hclc5SampleMetadataLo        = 0x4b
    Hclc5SampleRailSelect        = 0x4c
    Hclc5SampleDelay             = 0x4d
    Hclc5SampleStart             = 0x4e

    Hclc6SampleCount             = 0x50
    Hclc6SampleRate              = 0x51
    Hclc6SampleMetadataHi        = 0x52
    Hclc6SampleMetadataLo        = 0x53
    Hclc6SampleRailSelect        = 0x54
    Hclc6SampleDelay             = 0x55
    Hclc6SampleStart             = 0x56

    Hclc7SampleCount             = 0x58
    Hclc7SampleRate              = 0x59
    Hclc7SampleMetadataHi        = 0x5a
    Hclc7SampleMetadataLo        = 0x5b
    Hclc7SampleRailSelect        = 0x5c
    Hclc7SampleDelay             = 0x5d
    Hclc7SampleStart             = 0x5e

    Vlc0SampleCount              = 0x60
    Vlc0SampleRate               = 0x61
    Vlc0SampleMetadataHi         = 0x62
    Vlc0SampleMetadataLo         = 0x63
    Vlc0SampleRailSelect         = 0x64
    Vlc0SampleDelay              = 0x65
    Vlc0SampleStart              = 0x66

    Vlc1SampleCount              = 0x68
    Vlc1SampleRate               = 0x69
    Vlc1SampleMetadataHi         = 0x6a
    Vlc1SampleMetadataLo         = 0x6b
    Vlc1SampleRailSelect         = 0x6c
    Vlc1SampleDelay              = 0x6d
    Vlc1SampleStart              = 0x6e

    Vlc2SampleCount              = 0x70
    Vlc2SampleRate               = 0x71
    Vlc2SampleMetadataHi         = 0x72
    Vlc2SampleMetadataLo         = 0x73
    Vlc2SampleRailSelect         = 0x74
    Vlc2SampleDelay              = 0x75
    Vlc2SampleStart              = 0x76

    Vlc3SampleCount              = 0x78
    Vlc3SampleRate               = 0x79
    Vlc3SampleMetadataHi         = 0x7a
    Vlc3SampleMetadataLo         = 0x7b
    Vlc3SampleRailSelect         = 0x7c
    Vlc3SampleDelay              = 0x7d
    Vlc3SampleStart              = 0x7e

    Vlc4SampleCount              = 0x80
    Vlc4SampleRate               = 0x81
    Vlc4SampleMetadataHi         = 0x82
    Vlc4SampleMetadataLo         = 0x83
    Vlc4SampleRailSelect         = 0x84
    Vlc4SampleDelay              = 0x85
    Vlc4SampleStart              = 0x86

    Vlc5SampleCount              = 0x88
    Vlc5SampleRate               = 0x89
    Vlc5SampleMetadataHi         = 0x8a
    Vlc5SampleMetadataLo         = 0x8b
    Vlc5SampleRailSelect         = 0x8c
    Vlc5SampleDelay              = 0x8d
    Vlc5SampleStart              = 0x8e

    Vlc6SampleCount              = 0x90
    Vlc6SampleRate               = 0x91
    Vlc6SampleMetadataHi         = 0x92
    Vlc6SampleMetadataLo         = 0x93
    Vlc6SampleRailSelect         = 0x94
    Vlc6SampleDelay              = 0x95
    Vlc6SampleStart              = 0x96

    Vlc7SampleCount              = 0x98
    Vlc7SampleRate               = 0x99
    Vlc7SampleMetadataHi         = 0x9a
    Vlc7SampleMetadataLo         = 0x9b
    Vlc7SampleRailSelect         = 0x9c
    Vlc7SampleDelay              = 0x9d
    Vlc7SampleStart              = 0x9e

class HVDPSBAR2REGS:
    HCLC_0_SAMPLE_COUNT = 0x20
    HCLC_0_SAMPLE_RATE = 0x21
    HCLC_0_SAMPLE_METADATA_HI = 0x22
    HCLC_0_SAMPLE_METADATA_LO = 0x23
    HCLC_0_SAMPLE_RAIL_SELECT = 0x24
    HCLC_0_SAMPLE_DELAY = 0x25
    HCLC_0_SAMPLE_START = 0x26
    HCLC_1_SAMPLE_COUNT = 0x28
    HCLC_1_SAMPLE_RATE = 0x29
    HCLC_1_SAMPLE_METADATA_HI = 0x2a
    HCLC_1_SAMPLE_METADATA_LO = 0x2b
    HCLC_1_SAMPLE_RAIL_SELECT = 0x2c
    HCLC_1_SAMPLE_DELAY = 0x2d
    HCLC_1_SAMPLE_START = 0x2e
    HCLC_2_SAMPLE_COUNT = 0x30
    HCLC_2_SAMPLE_RATE = 0x31
    HCLC_2_SAMPLE_METADATA_HI = 0x32
    HCLC_2_SAMPLE_METADATA_LO = 0x33
    HCLC_2_SAMPLE_RAIL_SELECT = 0x34
    HCLC_2_SAMPLE_DELAY = 0x35
    HCLC_2_SAMPLE_START = 0x36
    HCLC_3_SAMPLE_COUNT = 0x38
    HCLC_3_SAMPLE_RATE = 0x39
    HCLC_3_SAMPLE_METADATA_HI = 0x3a
    HCLC_3_SAMPLE_METADATA_LO = 0x3b
    HCLC_3_SAMPLE_RAIL_SELECT = 0x3c
    HCLC_3_SAMPLE_DELAY = 0x3d
    HCLC_3_SAMPLE_START = 0x3e
    HCLC_4_SAMPLE_COUNT = 0x40
    HCLC_4_SAMPLE_RATE = 0x41
    HCLC_4_SAMPLE_METADATA_HI = 0x42
    HCLC_4_SAMPLE_METADATA_LO = 0x43
    HCLC_4_SAMPLE_RAIL_SELECT = 0x44
    HCLC_4_SAMPLE_DELAY = 0x45
    HCLC_4_SAMPLE_START = 0x46
    HCLC_5_SAMPLE_COUNT = 0x48
    HCLC_5_SAMPLE_RATE = 0x49
    HCLC_5_SAMPLE_METADATA_HI = 0x4a
    HCLC_5_SAMPLE_METADATA_LO = 0x4b
    HCLC_5_SAMPLE_RAIL_SELECT = 0x4c
    HCLC_5_SAMPLE_DELAY = 0x4d
    HCLC_5_SAMPLE_START = 0x4e
    HCLC_6_SAMPLE_COUNT = 0x50
    HCLC_6_SAMPLE_RATE = 0x51
    HCLC_6_SAMPLE_METADATA_HI = 0x52
    HCLC_6_SAMPLE_METADATA_LO = 0x53
    HCLC_6_SAMPLE_RAIL_SELECT = 0x54
    HCLC_6_SAMPLE_DELAY = 0x55
    HCLC_6_SAMPLE_START = 0x56
    HCLC_7_SAMPLE_COUNT = 0x58
    HCLC_7_SAMPLE_RATE = 0x59
    HCLC_7_SAMPLE_METADATA_HI = 0x5a
    HCLC_7_SAMPLE_METADATA_LO = 0x5b
    HCLC_7_SAMPLE_RAIL_SELECT = 0x5c
    HCLC_7_SAMPLE_DELAY = 0x5d
    HCLC_7_SAMPLE_START = 0x5e

    DUT_0_SAMPLE_COUNT = 0x280>>2
    DUT_0_SAMPLE_RATE = 0x284>>2
    DUT_0_SAMPLE_METADATA_HI = 0x288>>2
    DUT_0_SAMPLE_METADATA_LO = 0x28C>>2
    DUT_0_SAMPLE_RAIL_SELECT = 0x290>>2
    DUT_0_SAMPLE_DELAY = 0x294>>2
    DUT_0_SAMPLE_START = 0x298>>2
    DUT_1_SAMPLE_COUNT = 0x2a0>>2
    DUT_1_SAMPLE_RATE = 0x2a4>>2
    DUT_1_SAMPLE_METADATA_HI = 0x2a8>>2
    DUT_1_SAMPLE_METADATA_LO = 0x2ac>>2
    DUT_1_SAMPLE_RAIL_SELECT = 0x2b0>>2
    DUT_1_SAMPLE_DELAY = 0x2b4>>2
    DUT_1_SAMPLE_START = 0x2b8>>2
    DUT_2_SAMPLE_COUNT = 0x2c0>>2
    DUT_2_SAMPLE_RATE = 0x2c4>>2
    DUT_2_SAMPLE_METADATA_HI = 0x2c8>>2
    DUT_2_SAMPLE_METADATA_LO = 0x2cc>>2
    DUT_2_SAMPLE_RAIL_SELECT = 0x2d0>>2
    DUT_2_SAMPLE_DELAY = 0x2d4>>2
    DUT_2_SAMPLE_START = 0x2d8>>2
    DUT_3_SAMPLE_COUNT = 0x2e0>>2
    DUT_3_SAMPLE_RATE = 0x2e4>>2
    DUT_3_SAMPLE_METADATA_HI = 0x2e8>>2
    DUT_3_SAMPLE_METADATA_LO = 0x2ec>>2
    DUT_3_SAMPLE_RAIL_SELECT = 0x2f0>>2
    DUT_3_SAMPLE_DELAY = 0x2f4>>2
    DUT_3_SAMPLE_START = 0x2f8>>2
    DUT_4_SAMPLE_COUNT = 0x300>>2
    DUT_4_SAMPLE_RATE = 0x304>>2
    DUT_4_SAMPLE_METADATA_HI = 0x308>>2
    DUT_4_SAMPLE_METADATA_LO = 0x30c>>2
    DUT_4_SAMPLE_RAIL_SELECT = 0x310>>2
    DUT_4_SAMPLE_DELAY = 0x314>>2
    DUT_4_SAMPLE_START = 0x318>>2
    DUT_5_SAMPLE_COUNT = 0x320>>2
    DUT_5_SAMPLE_RATE = 0x324>>2
    DUT_5_SAMPLE_METADATA_HI = 0x328>>2
    DUT_5_SAMPLE_METADATA_LO = 0x32c>>2
    DUT_5_SAMPLE_RAIL_SELECT = 0x330>>2
    DUT_5_SAMPLE_DELAY = 0x334>>2
    DUT_5_SAMPLE_START = 0x338>>2
    DUT_6_SAMPLE_COUNT = 0x340>>2
    DUT_6_SAMPLE_RATE = 0x344>>2
    DUT_6_SAMPLE_METADATA_HI = 0x348>>2
    DUT_6_SAMPLE_METADATA_LO = 0x34c>>2
    DUT_6_SAMPLE_RAIL_SELECT = 0x350>>2
    DUT_6_SAMPLE_DELAY = 0x354>>2
    DUT_6_SAMPLE_START = 0x358>>2
    DUT_7_SAMPLE_COUNT = 0x360>>2
    DUT_7_SAMPLE_RATE = 0x364>>2
    DUT_7_SAMPLE_METADATA_HI = 0x368>>2
    DUT_7_SAMPLE_METADATA_LO = 0x36c>>2
    DUT_7_SAMPLE_RAIL_SELECT = 0x370>>2
    DUT_7_SAMPLE_DELAY = 0x374>>2
    DUT_7_SAMPLE_START = 0x378>>2

    AD5560_TURBO_SAMPLE_COUNT = 0x380>>2
    AD5560_TURBO_SAMPLE_RATE = 0x384>>2
    AD5560_TURBO_SAMPLE_METADATA_HI = 0x388>>2
    AD5560_TURBO_SAMPLE_METADATA_LO = 0x38c>>2
    AD5560_TURBO_SAMPLE_RAIL_SELECT = 0x390>>2
    AD5560_TURBO_SAMPLE_DELAY = 0x394>>2
    AD5560_TURBO_SAMPLE_START = 0x398>>2

    DUT_TURBO_SAMPLE_COUNT = 0x3a0>>2
    DUT_TURBO_SAMPLE_RATE = 0x3a4>>2
    DUT_TURBO_SAMPLE_METADATA_HI = 0x3a8>>2
    DUT_TURBO_SAMPLE_METADATA_LO = 0x3ac>>2
    DUT_TURBO_SAMPLE_RAIL_SELECT = 0x3b0>>2
    DUT_TURBO_SAMPLE_DELAY = 0x3b4>>2
    DUT_TURBO_SAMPLE_START = 0x3b8>>2



def Load():
    allConstants = {} # dict of all constants
    names = set()   # set of all constant names
    conflicts = set()
    for className, cls in vars(sys.modules[__name__]).items():
        if not className.startswith('_') and className.upper() == className:
            for name, value in _GetClassConstants(cls).items():
                if name in names:
                    conflicts.add(name)
                else:
                    names.add(name)
                allConstants[(className, name)] = value
    constants = {}   # list of output constants
    for key, value in allConstants.items():
        className, name = key
        # Always add "full form" of the constant
        constants['{}.{}'.format(className, name)] = value
        if name not in conflicts:
            # Convenient "short form" (not supported for conficts)
            constants[name] = value
    return constants, {}

def _GetClassConstants(cls):
    s = {}
    for var, value in vars(cls).items():
        if not var.startswith('_'):
            s[var] = value
    return s

if __name__ == "__main__":
    # Parse command line options
    import os
    import argparse
    import datetime
    import getpass
    from string import Template
    parser = argparse.ArgumentParser(description = 'C++ symbols header file generator')
    parser.add_argument('outfile', help = 'Output filename', type = str)
    args = parser.parse_args()

    filename = os.path.basename(args.outfile)
    split = os.path.splitext(filename)

    template = {
        'USER'     : getpass.getuser(),
        'DATE'     : datetime.datetime.now().strftime("%m/%d/%Y"),
        'FILENAME' : filename,
        'BASENAME' : split[0].upper(),
        'FILEEXT'  : split[1].upper().replace('.', ''),
    }

    fout = open(args.outfile, 'w')
    # Print header
    fout.write(Template("""\
////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: ${FILENAME} (auto-generated file, see symbols.py)
//------------------------------------------------------------------------------
//    Purpose: HDDPS testbench symbols
//------------------------------------------------------------------------------
// Created by: ${USER}
//       Date: ${DATE}
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////
#ifndef __${BASENAME}_${FILEEXT}__
#define __${BASENAME}_${FILEEXT}__

#include <cstdint>

namespace hddpstbc {

""").substitute(template))

    constants, functions = Load()
    for name, value in constants.items():
        if isinstance(value,float):
            fout.write('const float {} = {}f;\n'.format(name.replace('.', '_'), value))
        else:
            fout.write('const uint64_t {} = 0x{:x};\n'.format(name.replace('.', '_'), value))

    # Print footer
    fout.write(Template("""\

}  // namespace hddpstbc

#endif  // __${BASENAME}_H__
""").substitute(template))

    fout.close()
