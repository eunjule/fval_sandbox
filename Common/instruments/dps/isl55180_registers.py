################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from ctypes import c_uint

import Common.register as register


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, isl55180Register):
                if attribute is not isl55180Register:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, isl55180Register):
                if attribute is not isl55180Register:
                    # if '__' not in attribute.__name__:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, isl55180Struct):
                if attribute is not isl55180Struct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, isl55180Struct):
                if attribute is not isl55180Struct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class isl55180Register(register.Register):
    BAR = 1

    def __init__(self, uint=0, length=16):
        super().__init__(value=uint)


class isl55180Struct(register.Register):
    ADDR = None

    def __init__(self, uint=0, length=32):
        super().__init__(value=uint)


module_dict = globals()


class FORCEA_LEVEL(isl55180Register):
    ADDR = 0x00
    _fields_ = [('Data', c_uint, 16)]

class FORCEB_LEVEL(isl55180Register):
    ADDR = 0x01
    _fields_ = [('Data', c_uint, 16)]

class I_CLAMP_H_LEVEL(isl55180Register):
    ADDR = 0x02
    _fields_ = [('Data', c_uint, 16)]

class I_CLAMP_L_LEVEL(isl55180Register):
    ADDR = 0x03
    _fields_ = [('Data', c_uint, 16)]

class FORCEA_OFFSET(isl55180Register):
    ADDR = 0x10
    _fields_ = [('Data', c_uint, 16)]

class FORCEB_OFFSET(isl55180Register):
    ADDR = 0x11
    _fields_ = [('Data', c_uint, 16)]

class I_CLAMP_H_OFFSET(isl55180Register):
    ADDR = 0x12
    _fields_ = [('Data', c_uint, 16)]


class I_CLAMP_L_OFFSET(isl55180Register):
    ADDR = 0x13
    _fields_ = [('Data', c_uint, 16)]

class FORCEA_GAIN(isl55180Register):
    ADDR = 0x20
    _fields_ = [('Data', c_uint, 16)]

class FORCEB_GAIN(isl55180Register):
    ADDR = 0x21
    _fields_ = [('Data', c_uint, 16)]

class I_CLAMP_H_GAIN(isl55180Register):
    ADDR = 0x22
    _fields_ = [('Data', c_uint, 16)]

class I_CLAMP_L_GAIN(isl55180Register):
    ADDR = 0x23
    _fields_ = [('Data', c_uint, 16)]

class SOURCE_FB_SEL(isl55180Register):
    ADDR = 0
    _fields_ = [('Data', c_uint, 16)]

class DPS_CONT_MISC(isl55180Register):
    ADDR = 1
    _fields_ = [('Data', c_uint, 16)]

class CLAMP_ALARM_CONT(isl55180Register):
    ADDR = 2
    _fields_ = [('Data', c_uint, 16)]

class STATUS_READ_BACK(isl55180Register):
    ADDR = 3
    _fields_ = [('Data', c_uint, 16)]

class DIAG_AND_CAL(isl55180Register):
    ADDR = 4
    _fields_ = [('Data', c_uint, 16)]

class MEAS_UNIT_SRC_SEL(isl55180Register):
    ADDR = 5
    _fields_ = [('Data', c_uint, 16)]

class OFFSET_ADJ(isl55180Register):
    ADDR = 6
    _fields_ = [('Data', c_uint, 16)]

class GAIN_ADJ(isl55180Register):
    ADDR = 7
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR0(isl55180Register):
    ADDR = 8
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR1(isl55180Register):
    ADDR = 9
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR2(isl55180Register):
    ADDR = 10
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR3_LV(isl55180Register):
    ADDR = 11
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR3_HV(isl55180Register):
    ADDR = 12
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR4_LV(isl55180Register):
    ADDR = 13
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR4_HV(isl55180Register):
    ADDR = 14
    _fields_ = [('Data', c_uint, 16)]

class CME_ADJ_IR5(isl55180Register):
    ADDR = 15
    _fields_ = [('Data', c_uint, 16)]

class ESP_RES_COMP_VAL(isl55180Register):
    ADDR = 16
    _fields_ = [('Data', c_uint, 16)]

class CPU_RESET_WO(isl55180Register):
    ADDR = 128
    _fields_ = [('Data', c_uint, 16)]

class MEAS_CONT_GANG(isl55180Register):
    ADDR = 129
    _fields_ = [('Data', c_uint, 16)]

class MEAS_IMON_CENT_SEL(isl55180Register):
    ADDR = 130
    _fields_ = [('Data', c_uint, 16)]

class ALARM_CONTROL(isl55180Register):
    ADDR = 131
    _fields_ = [('Data', c_uint, 16)]

class UPPER_DAC_BIT_CAL(isl55180Register):
    ADDR = 132
    _fields_ = [('Data', c_uint, 16)]

class MID_DAC_BIT_CAL(isl55180Register):
    ADDR = 133
    _fields_ = [('Data', c_uint, 16)]

class DACN_DACP(isl55180Register):
    ADDR = 134
    _fields_ = [('Data', c_uint, 16)]

class DIE_ID_RO(isl55180Register):
    ADDR = 255
    _fields_ = [('Data', c_uint, 16)]

class ISL55180Command(isl55180Struct):
    _fields_ = [('RegisterAddr', c_uint, 8),
                ('DeviceId', c_uint, 7),
                ('ReadWrite', c_uint, 1),
                ('Bar2Base', c_uint, 16)]



