################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import time
import struct
from Common import configs
from Common import hilmon as hil
from Common.instruments.dps.dpsSubslot import DpsSubslot
from Common.instruments.dps import hvil_registers as hvil_regs
from Common.instruments.dps import symbols
from ThirdParty.SASS.suppress_sensor import suppress_sensor

FPGA_BAR2_DEVICE_ID = 0x000
OLD_FAB_REV = ['100','101']
NEW_FAB_REV = ['201']


def encode_clamp_current(amps):
    max_current = 29.88
    max_positive_value = 2**15 -1
    max_negative_value = 2**16 -1
    if amps > max_current :
        raise ClampValueError('Requested current value is greater than allowed value')
    if amps < -max_current:
        raise ClampValueError('Requested current value is lesser than allowed value')

    signed_integer = int((max_positive_value / max_current) * amps)
    if amps < 0 :
        return max_negative_value + signed_integer
    else :
        return signed_integer
        
class ClampValueError(Exception):
    pass

class HvilSubslot(DpsSubslot):

    def __init__(self,hvdps,slot, subslot):
        super().__init__(slot, subslot)
        self.rail_count = 8
        self.hvdps = hvdps
        self.railTypes = ['RLOAD']
        self.registers = hvil_regs
        self.RAIL_COUNT = {'RLOAD': 8}

    def S2HSupport(self):
        if self.GetFpgaVersion() in configs.HVIL_NON_S2H_FPGAS:
            return False
        else:
            return True

    def BarRead(self, bar, offset, dev_id=FPGA_BAR2_DEVICE_ID):
        return self.DpsBarRead(bar,offset,dev_id)

    def BarWrite(self, bar, offset, data, dev_id=FPGA_BAR2_DEVICE_ID):
        return self.DpsBarWrite(bar, offset, data, dev_id)

    def getBar2RailCommandRegister(self):
        return self.registers.Bar2RailCommand

    def LoadFpgaAndGetVersion(self):
        self.FpgaImageAttributeExistsInConfigs()
        self.Log('info', 'Flashing HVIL FPGA with image from \'{}\''.format(configs.HVIL_FPGA_IMAGE))
        try:
            self.FpgaLoad(configs.HVIL_FPGA_IMAGE)
        except RuntimeError as e:
            self.Log('error', 'HIL raised a RuntimeError with message "{}"'.format(e))
        self.Log('info', 'New HVIL Image slot {} subslot {}: 0x{}'.format(self.slot, 1,
                                                                                  hex(self.GetFpgaVersion())))

    def FpgaLoad(self, image):
        if self.FpgaImageIsValid(image):
            with suppress_sensor(f'FVAL HVIL {self.slot}', f'HDMT_HDDPS:{self.slot}'):
                hil.dpsDeviceDisable(self.slot,1)
                result = hil.dpsFpgaLoad(self.slot,1, image)
                time.sleep(1)
                hil.dpsDeviceEnable(self.slot,1)
        else:
            self.Log('critical', 'Invalid HVIL Image on slot {}'.format(self.slot))
        return result

    def FpgaImageIsValid(self, image):
        if 'hvil' in image.lower():
            return True
        else:
            return False

    def FpgaImageAttributeExistsInConfigs(self):
        fpga_configs = ['HVIL_FPGA_IMAGE']
        for config in fpga_configs:
            try:
                getattr(configs, config)

            except AttributeError:
                self.Log('critical', '\'{}\' is not defined in configs.py'.format(config))

    def SetDefaultVoltageComparatorLimits(self):
        self.SetVoltageComparatorLimits(voltage_low_limit = -0.5,voltage_high_limit = 5)

    def SetVoltageComparatorLimits(self,voltage_low_limit,voltage_high_limit):
        hil.hvilVoltageComparatorLimitsSet(self.slot, voltage_low_limit, voltage_high_limit)

    def ResetDefaultTemperatureLimitForMAX6627(self):
        self.SetTemperatureLimitForAllMAX6627(low_temperature=1, high_temperature= 40)

    def SetTemperatureLimitForAllMAX6627(self, low_temperature, high_temperature):
        for MAX6627_index in range(6):
            hil.hvilMax6627LimitsSet(self.slot, MAX6627_index, low_temperature, high_temperature)

    def SetTemperatureLimitForSingleMAX6627(self, MAX6627_index, low_temperature, high_temperature):
        hil.hvilMax6627LimitsSet(self.slot, MAX6627_index, low_temperature, high_temperature)

    def SetDefaultCurrentClampLimit(self):
        self.SetCurrentClampLimit(1.0)

    def SetDefaultThermalCurrentClampLimit(self):
        self.SetThermalCurrentClampLimit(1.0)

    def SetCurrentClampLimit(self,amps):
        for rail in range(self.rail_count):
            clamp_value_16_bit = encode_clamp_current(amps)
            self.WriteRegister(hvil_regs.CHANNEL_USER_CURRENT_CLAMP_LIMIT(value=clamp_value_16_bit), index=rail)

    def SetThermalCurrentClampLimit(self,amps):
        for rail in range(self.rail_count):
            clamp_value_16_bit = encode_clamp_current(amps)
            self.WriteRegister(hvil_regs.CHANNEL_THERMAL_CURRENT_CLAMP_LIMIT(value=clamp_value_16_bit), index=rail)

    def MAX6627TemperatureRead(self,device_id):
        return hil.hvilTmonRead (self.slot,device_id)

    def getSafeStateRegister(self):
        return self.registers.SET_SAFE_STATE

    def getDutDomainIdRegisterType(self):
        return self.registers.DUT_DOMAIN_ID

    def getUhcRailConfigRegister(self,rail_type):
        if rail_type == 'RLOAD':
            return self.registers.UHC_CHANNEL_CONFIG

    def ClearRailSafeState(self, dutdomainId):
        rail_type = 'RLOAD'
        rail = 0
        self.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
        self.WriteRegister(self.registers.CHANNELS_FOLDED(value=0xffffffff))
        time.sleep(1)
        hclcfold = self.ReadRegister(self.registers.CHANNELS_FOLDED).value
        self.Log('debug','in clearRailsafe state on folded as expected.  Fold register value: 0x{:x}'.format(hclcfold))

    # def ClearMAX6627TemperatureAlarmLevel2(self):
    #     hil.dpsBarWrite(self.slot, 1, 1, self.registers.MAX6627_TEMP_ALARMS_DETAILS.ADDR, 0xffffffff)


    def ClearMAX6627TemperatureAlarmDetails(self):
        MAX6627TemperatureAlarmsRegister = hvil_regs.MAX6627_TEMP_ALARMS_DETAILS
        self.WriteRegister(MAX6627TemperatureAlarmsRegister(value=0xffffffff))



    def ClearDpsAlarms(self):
        self.ClearMAX6627TemperatureAlarmDetails()
        self.ClearChannelAlarms()
        self.ClearChannelSampleAlarm()
        self.ClearCfoldAlarms()
        self.ClearTrigExecAlarms()
        self.ClearGlobalAlarms()


    def ClearChannelAlarms(self):
        self.clearPerChannelAlarmsRegister()

    def clearPerChannelAlarmsRegister(self):
        for channel in range(self.rail_count):
            self.WriteRegister(self.registers.CHANNEL_ALARMS_DETAILS(value=0xFFFFFFFF), index = channel)

    def ClearCfoldAlarms(self):
        self.WriteRegister(self.registers.CHANNEL_CONTROLLED_FOLD_ALARMS(value=0xffffffff))

    def ClearChannelSampleAlarm(self):
        self.WriteRegister(self.registers.CHANNEL_SAMPLE_ALARMS(value=0xffffffff))

    def ClearTrigExecAlarms(self):
        self.WriteRegister(self.registers.TRIG_EXEC_ALARMS(value=0xffffffff))

    def clearGlobalAlarmRegister(self):
        self.WriteRegister(self.registers.GLOBAL_ALARMS(value=0xffffffff))

    def getSampleCollectorIndexRegister(self):
        return self.registers.SAMPLE_COLLECTOR_INDEX

    def getSampleCollectorCountRegister(self):
        return self.registers.SAMPLE_COLLECTOR_COUNT

    def getSampleCollectorDataRegister(self):
        return self.registers.SAMPLE_COLLECTOR_DATA


    def getTurboSampleCollectorIndexRegister(self):
        return self.registers.TURBO_SAMPLE_COLLECTOR_INDEX

    def getTurboSampleCollectorCountRegister(self):
        return self.registers.TURBO_SAMPLE_COLLECTOR_COUNT

    def getTurboSampleCollectorDataRegister(self):
        return self.registers.TURBO_SAMPLE_COLLECTOR_DATA

    def getGlobalAlarmRegisterType(self):
        return self.registers.GLOBAL_ALARMS

    def getExecuteTriggerRegister(self):
        return self.registers.EXECUTE_TRIGGER

    def create_cal_board_instance_and_initialize(self):
        self.hvdps.CalCardInitialize()
        return None

    def cal_load_connect(self,load,calboard=None):
        self.hvdps.subslots[0].cal_load_toggle(res=load, dir=False)

    def ConnectRailForce(self, rail, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True =
        self.hvdps.Pca9505SetBit(self.hvdps.LbChannelList[rail][0], self.hvdps.LbChannelList[rail][1], self.hvdps.LbChannelList[rail][2], dir)

    def ConnectExternalLoad(self, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True = On
        # self.Pca9505SetBit(ext_dict['DB_Cal'][0], ext_dict['DB_Cal'][1], ext_dict['DB_Cal'][2], dir)
        self.hvdps.Pca9505SetBit(self.hvdps.ext_dict['Ext_Load'][0], self.hvdps.ext_dict['Ext_Load'][1], self.hvdps.ext_dict['Ext_Load'][2], dir)

    def get_rail_voltage(self,rail,rail_type):
        voltage = hil.hvilRailVoltageRead(self.slot,rail)
        return voltage

    def get_rail_current(self,rail,turbo= False):
        if turbo == True:
            rail_current_regsiter_type = self.registers.CHANNEL_TURBO_I
            rail_current_from_bar1 = self.ReadRegister(rail_current_regsiter_type).value
        else:
            rail_current_regsiter_type = self.registers.CHANNEL_I
            rail_current_from_bar1 = self.ReadRegister(rail_current_regsiter_type, index=rail).value
        converted_current = struct.unpack('f', struct.pack('I', rail_current_from_bar1))[0]
        return converted_current

    def getResetsRegister(self):
        return self.registers.RESETS

    def CheckTqNotifyAlarm(self, dutid, rail_type):
        if self.LegacyTcLooping:
            tqnotifyalarm = 'TqNotifyAlarmDut' + str(dutid)
            self.WaitForGlobalAlarmBit(tqnotifyalarm)

    def get_folded_state(self):
        self.WriteRegister(self.registers.CHANNELS_FOLDED(value=0xffffffff))
        time.sleep(1)
        hclcfold = self.ReadRegister(self.registers.CHANNELS_FOLDED).value
        self.Log('info', 'Channel {} fold status 0x{:x}'.format(hclcfold))

    def getPerChannelAlarmRegister(self):
        return self.registers.CHANNEL_ALARMS_DETAILS

    def check_per_channel_alarm_details(self,rail,alarm):

        global_alarms = self.get_global_alarms()
        if global_alarms == ['ChannelGlobalAlarm']:
            channel_alarm = self.ReadRegister(self.registers.CHANNEL_ALARMS)
            per_channel_alarms_register = self.getPerChannelAlarmRegister()
            rail_mask = 1 << rail
            if rail_mask == channel_alarm.value:
                per_channel_alarms = self.ReadRegister(per_channel_alarms_register, index=rail)
                rail_alarms = []
                for field in per_channel_alarms.fields():
                    if getattr(per_channel_alarms, field) == 1:
                        rail_alarms.append(field)
                if rail_alarms == [alarm]:
                    self.Log('debug', 'Expected {} Alarm received on rail {}'.format(alarm, rail))
                    return True
                else:
                    self.Log('error',
                             'Expected only {} to be set in Rload channel {} specific alarm register, actual alarms set were {}'.format(
                                 alarm, rail, rail_alarms))
            else:
                channel_alarms_set = []
                for field in channel_alarm.fields():
                    if getattr(channel_alarm, field) == 1:
                        channel_alarms_set.append(field)
                self.Log('error',
                         'Expected only Rail {} bit to be set in Channel_Alarms register, but got the following: {}'.format(
                             rail, channel_alarms_set))
        else:
            self.Log('error',
                     'Expected only \'ChannelAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(
                         global_alarms))
        return False

    def TurnONStreamingWithTrigger(self):
        s2h_start_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_START)
        self.hvdps.rc.send_trigger(s2h_start_trigger_value)

    def TurnOFFStreamingWithTrigger(self):
        s2h_stop_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_STOP)
        self.hvdps.rc.send_trigger(s2h_stop_trigger_value)

    def ResetStreamingWithTrigger(self):
        s2h_reset_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_RESET)
        self.hvdps.rc.send_trigger(s2h_reset_trigger_value)

    def getResetsRegister(self):
        return self.registers.RESETS

    def setup_load_control(self, channel):
        hvil_board_blt= self.hvdps.hvilboardblt.PartNumberCurrent
        current_hvil_fab_version = re.match('.*?([0-9]+)$', str(hvil_board_blt)).group(1)
        if current_hvil_fab_version in OLD_FAB_REV:
            eight_ohm_load_select = 0x100
            self.WriteRegister(self.registers.CHANNEL_LOAD_CONTROL(value=eight_ohm_load_select), index=channel)
        elif current_hvil_fab_version in NEW_FAB_REV:
            if channel == 0 or channel == 2 or channel == 5 or channel == 7:
                eight_ohm_load_select = 0x100
                self.WriteRegister(self.registers.CHANNEL_LOAD_CONTROL(value = eight_ohm_load_select),index=channel)
            elif channel == 1 or channel==4 or channel == 6:
                eight_ohm_load_select = 0x10
                self.WriteRegister(self.registers.CHANNEL_LOAD_CONTROL(value=eight_ohm_load_select), index=channel)
            elif channel ==3:
                eight_ohm_load_select = 0x2
                self.WriteRegister(self.registers.CHANNEL_LOAD_CONTROL(value=eight_ohm_load_select), index=channel)
        self.WriteRegister(self.registers.CHANNEL_LOAD_ACTIVATE(value=0x1<<channel))
        self.WriteRegister(self.registers.SET_CLEAR_LOAD_CHANNEL_ENABLE(value=0x1<<channel))


    def load_disconnect(self,channel):
        shift_mask = channel +8
        self.WriteRegister(self.registers.CHANNEL_LOAD_CONTROL(value=0x0), index=channel)
        self.WriteRegister(self.registers.CHANNEL_LOAD_ACTIVATE(value=0x1<<channel))
        self.WriteRegister(self.registers.SET_CLEAR_LOAD_CHANNEL_ENABLE(value=0x1<<shift_mask))


    def get_16bit_encoded_current_value(self, amps):
        clamp_value_16_bit = encode_clamp_current(amps)
        return clamp_value_16_bit

    def WriteBar2NonBroadcastRailCommand(self, command, channel, data, rail_or_resourceid):
        offset = self.registers.NonBroadcastBar2RailCommand()
        offset.RailCommand = command
        offset.ChannelID = channel
        offset.RailProcessor = channel
        offset.ResourceType = rail_or_resourceid
        offset_value = offset.value <<2
        offset = offset_value
        self.BarWrite(2, offset, data)
