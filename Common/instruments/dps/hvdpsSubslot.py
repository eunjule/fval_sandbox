################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import struct
import time

from Common.instruments.dps.dpsSubslot import DpsSubslot
from Common.instruments.dps.symbols import RAILCOMMANDS
from Common import hilmon as hil
from Common import configs
from Common.instruments.dps import hvdps_registers as hvdps_regs
from Common.instruments.dps import ad5560_registers
from Common.instruments.dps import symbols
from ThirdParty.SASS.suppress_sensor import suppress_sensor

DPS_NUM_AD5560_RAILS = 8
FPGA_BAR2_DEVICE_ID = 0x000
AD5560_OFFSET_DAC_CODE = 0x2100
AD550_V_REF = 5.0
AD5560_DAC_SCALE = 0x10000
AD5560_V_CALC_SCALER = 5.125
AD5560_MI_AMP_GAIN = 20.0

class HvdpsSubslot(DpsSubslot):
    hvdps_hv_dac_mapping = {0: (0, 0), 1: (0, 1), 2: (0, 2), 3: (1, 0), 4: (1, 1), 5: (1, 2), 6: (2, 0), 7: (2, 1)}

    def __init__(self, hvdps, slot, subslot):
        super().__init__(slot, subslot)
        self.hvdps = hvdps
        self.hvdps_registers = hvdps_regs
        self.ad5560_registers = ad5560_registers
        self.registers = hvdps_regs
        self.numberofrails = 8
        self.TOTAL_MEMORY_SIZE = 2 * 512 * 1024 * 1024  # 1gb
        self.railTypes = ['HV', 'LVM']
        self.RAIL_COUNT = {'HV': 8, 'LVM': 10}
        self.AD5560_MI_AMP_GAIN = 20.0

    def S2HSupport(self):
        if self.GetFpgaVersion() in configs.HVDPS_NON_S2H_FPGAS:
            return False
        else:
            return True

    def BarRead(self, bar, offset, dev_id=FPGA_BAR2_DEVICE_ID):
        return self.DpsBarRead(bar, offset, dev_id)

    def BarWrite(self, bar, offset, data, dev_id=FPGA_BAR2_DEVICE_ID):
        return self.DpsBarWrite(bar, offset, data, dev_id)

    def ResetThermalAlarmLimitForMAX6627(self):
        self.MAX6627AdcLimitsSet(-256, 161.0)
        self.Max6627BjtSensorLimitsSet(10, 80.0)
        self.Max6627VicorLimitsSet(10, 80.0)

    def UnGangAllHvdpsRails(self):
        self.WriteRegister(hvdps_regs.RAIL_MASTER_MASK(value=0xFF))
        self.WriteRegister(hvdps_regs.REMOTE_SLAVES_MASK(value=0x0))
        self.WriteRegister(hvdps_regs.REMOTE_SLAVES_BOARD_MASK(value=0x0))
        self.WriteRegister(hvdps_regs.HC_I_GANG_REG(value=0xFF))

    def setup_adcs(self, vss=7, iss=1):
        for i in range(8):
            self.WriteBar2RailCommand(self.symbols.RAILCOMMANDS.SET_SOFTSPAN_CODE, vss, 16 + i)
            self.WriteBar2RailCommand(self.symbols.RAILCOMMANDS.SET_SOFTSPAN_CODE, iss | 0x8, 16 + i)
        for i in range(10):
            self.WriteBar2RailCommand(self.symbols.RAILCOMMANDS.SET_SOFTSPAN_CODE, vss, i)

    def get_folded_state(self):
        self.WriteRegister(self.registers.HCLC_RAILS_FOLDED(value=0xFF))
        self.WriteRegister(self.registers.DUT_RAILS_FOLDED(value=0x3FF))
        hv_rails_folded = self.ReadRegister(self.registers.HCLC_RAILS_FOLDED).value
        lv_rails_folded = self.ReadRegister(self.registers.DUT_RAILS_FOLDED).value
        self.Log('info', 'HV RAILS FOLD STATUS: 0x{:x}'.format(hv_rails_folded))
        self.Log('info', 'LV RAILS FOLD STATUS: 0x{:x}'.format(lv_rails_folded))
        return hv_rails_folded, lv_rails_folded

    def CheckForAlarms(self):
        return self.CheckGlobalAlarms()

    def getResetsRegister(self):
        return hvdps_regs.RESETS

    def getStatusRegister(self):
        return hvdps_regs.STATUS

    def getTriggerUpTestRegister(self):
        return hvdps_regs.TRIGGERUPTEST

    def getTriggerDownTestRegister(self):
        return hvdps_regs.TRIGGERDNTEST

    def getHcLcControlledFoldAlarmsRegister(self):
        return hvdps_regs.HCLC_CONTROLLED_FOLD_ALARMS

    def getHcLcPerRailControlledFoldDetailsRegister(self):
        return hvdps_regs.HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS

    def ClearDpsAlarms(self):
        self.ClearHclcRailAlarms()
        self.clearHclcCfoldAlarmsRegister()
        self.clearHclcSampleAlarmRegister()
        self.ClearTrigExecAlarms()
        vicor_alarm = self.registers.MAX6627_VICOR_ALARMS(value=0xffffffff)
        self.WriteRegister(vicor_alarm)
        turbo_sample_alarm = self.registers.AD5560_TURBO_SAMPLE_ALARMS(value=0xffffffff)
        self.WriteRegister(turbo_sample_alarm)
        self.clearPerRailDutAlarmsRegister()
        self.initialize_lvm_rails()
        self.WriteRegister(self.registers.DUT_SAMPLE_ALARMS(value = 0xffffffff))
        self.WriteRegister(self.registers.DUT_TURBO_SAMPLE_ALARMS(value=0xffffffff))
        self.WriteRegister(self.registers.OV_ALARMS(value=0xffffffff))
        self.ClearGlobalAlarms()


    def clearPerRailHclcAlarmsRegister(self):
        for rail in range(self.numberofrails):
            hclcrailsregister = self.getHcLcPerRailAlarmRegister()
            self.WriteRegister(hclcrailsregister(value=0xffffffff), index=rail)

    def clearPerRailDutAlarmsRegister(self):
        for rail in range(self.RAIL_COUNT['LVM']):
            self.WriteRegister(self.registers.DUT_RAIL_ALARM(value=0xffffffff), index=rail)

    def clearGlobalAlarmRegister(self):
        global_alarm_register = hvdps_regs.GLOBAL_ALARMS
        self.WriteRegister(global_alarm_register(value=0xffffffff))

    def clearMAX6627VicorAlarmRegister(self):
        global_alarm_register = hvdps_regs.MAX6627_VICOR_ALARMS
        self.WriteRegister(global_alarm_register(value=0xffffffff))

    def clearHclcCfoldAlarmsRegister(self):
        HclcCfoldAlarmsRegister = hvdps_regs.HCLC_CONTROLLED_FOLD_ALARMS
        self.WriteRegister(HclcCfoldAlarmsRegister(value=0xffffffff))

    def clearHclcSampleAlarmRegister(self):
        hclc_sample_alarm_register = hvdps_regs.HCLC_SAMPLE_ALARMS
        self.WriteRegister(hclc_sample_alarm_register(value=0xffffffff))

    def EnableAlarms(self):
        enable_alarm_register = hvdps_regs.ENABLE_ALARMS()
        enable_alarm_register.AlarmHandler = 1
        self.WriteRegister(enable_alarm_register)

    def getDutDomainIdRegisterType(self):
        return hvdps_regs.DUT_DOMAIN_ID

    def getSafeStateRegister(self):
        return hvdps_regs.SET_SAFE_STATE

    def ad5560DpsReg1Initialize(self, registerType):
        externalRange2 = 0x5
        compareDutVoltage = 0x3
        measureOutISense = 0x1

        register = registerType()
        register.ForceAmpEnable = 0x0
        register.CurrentRange = externalRange2
        register.ComparatorFunction = compareDutVoltage
        register.MeasureOutEnable = 0x1
        register.MeasureOut = measureOutISense
        register.ClampEnable = 0x0
        return register

    def ad5560DpsReg2Initialize(self, registerType):
        masterOut = 0x1
        internalSenseResistor = 0x1
        slewRate_0p35_V_per_uS = 0x6
        register = registerType()
        register.SystemForceSense = 0x1
        register.SlewRateControl = slewRate_0p35_V_per_uS
        register.GPO = 0x0
        register.SlaveGangMode = masterOut
        register.INT10K = internalSenseResistor
        register.HighZGuard = 0x1
        return register

    def ad5560SysControlInitInitialize(self, registerType):
        tempLimit100C = 0x3
        register = registerType()
        register.TemperatureLimit = tempLimit100C
        register.Gain = 0x2  # Gain=0 / Measout Gain = 1 / MI Gain = 20
        register.FinToGround = 0x0  # Switch Force Amp to Output DAC
        register.ComparatorOutput = 0x0  # Disable simple window compare
        register.ForceAmpPowerdown = 0x1  # Power down force amplifier block
        register.ActiveLoad = 0x0  # Normal operation of clamp enable and hw inhibit
        return register

    def ad5560Compensation1Initialize(self, registerType):
        register = registerType()
        register.CompensationResistanceDut = 0x0  # 0nF to 50nF
        register.EquivalentSeriesResistance = 0x0  # 0mOhm to 1 mOhm
        register.SafeMode = 0x0  # override values in Comp Register 1 to make force amp stable
        return register

    def ad5560Compensation2Initialize(self, registerType):
        register = registerType()
        register.ManualCompensation = 0x0  # manual mode disabled
        register.ResistanceZero = 0x0  # Rz -> 500ohm and Fz = 3.2 MHz
        register.ResistancePole = 0x0  # Rp -> 200ohm and Fp = 100 Mhz
        register.TransconductanceForceAmp = 0x2  # 300 uA/V and 490 kHz
        register.FeedForwardCapacitorSelect = 0x1  # CF0 switched on
        register.CapThreeTo100K = 0x0  # Disconnected
        register.CapTwoTo25K = 0x0  # Disconnected
        register.CapOneTo6K = 0x0  # Disconnected
        return register

    def ad5560AlarmSetupInitialize(self, registerType):
        register = registerType()
        register.LatchTemperatureAlarm = 0
        register.DisableTemperatureAlarm = 0
        register.LatchOscillationAlarm = 0
        register.DisableOscillationAlarm = 1
        register.LatchDutAlarm = 0
        register.DisableDutAlarm = 1
        register.LatchClampAlarm = 0
        register.DisableClampAlarm = 1
        register.LatchGroundAlarm = 0
        register.DisableGroundAlarm = 1
        return register

    def InitializeAd5560Rails(self):

        ad5560DpsReg1Init = self.ad5560DpsReg1Initialize(ad5560_registers.DPS_REGISTER1)
        ad5560DpsReg2Init = self.ad5560DpsReg2Initialize(ad5560_registers.DPS_REGISTER2)
        ad5560SysControlInit = self.ad5560SysControlInitInitialize(ad5560_registers.SYSTEM_CONTROL)
        ad5560Compensation1 = self.ad5560Compensation1Initialize(ad5560_registers.COMPENSATION1)
        ad5560Compensation2 = self.ad5560Compensation2Initialize(ad5560_registers.COMPENSATION2)
        ad5560AlarmSetupInit = self.ad5560AlarmSetupInitialize(ad5560_registers.ALARM_SETUP)

        Ad5560VoltageRangeConstOffset = 0x2100  # 0 = -0.125V and 0xFFFF = 25.499V

        for rail in range(DPS_NUM_AD5560_RAILS):
            self.WriteAd5560Register(ad5560_registers.FIN_DAC_X1.ADDR, Ad5560VoltageRangeConstOffset, rail)
            self.WriteAd5560Register(ad5560_registers.OFFSET_DACX.ADDR, Ad5560VoltageRangeConstOffset, rail)
            self.WriteAd5560Register(ad5560DpsReg1Init.ADDR, ad5560DpsReg1Init.value, rail)
            self.WriteAd5560Register(ad5560DpsReg2Init.ADDR, ad5560DpsReg2Init.value, rail)
            self.WriteAd5560Register(ad5560AlarmSetupInit.ADDR, ad5560AlarmSetupInit.value, rail)
            self.WriteAd5560Register(ad5560_registers.OSD_DACX.ADDR, 0xFFFF, rail)
            self.WriteAd5560Register(ad5560_registers.DGS_DAC.ADDR, 0xFFFF, rail)
            self.WriteAd5560Register(ad5560Compensation1.ADDR, ad5560Compensation1.value, rail)
            self.WriteAd5560Register(ad5560Compensation2.ADDR, ad5560Compensation2.value, rail)
            self.WriteAd5560Register(ad5560_registers.RAMP_STEP_SIZE.ADDR, 65, rail)
            self.WriteAd5560Register(ad5560SysControlInit.ADDR, ad5560SysControlInit.value, rail)

    def WriteAd5560Register(self, register_address, data, rail):
        hil.hvdpsAd5560Write(self.slot, rail, register_address, data)

    def ReadAd5560Register(self, register_address, rail):
        data = hil.hvdpsAd5560Read(self.slot, rail, register_address)
        return data

    def getGlobalAlarmRegisterType(self):
        return hvdps_regs.GLOBAL_ALARMS

    def getHclcAlarmRegisterType(self):
        return hvdps_regs.HCLC_ALARMS

    def ReadAd5764Register(self, DAC_INDEX, REGISTER_VALUE, DAC_CHANNEL):
        return hil.hvdpsAd5764Read(self.slot, DAC_INDEX, REGISTER_VALUE, DAC_CHANNEL)

    def WritetoAd5764(self, DAC_INDEX, REGISTER_OFFSET, DAC_CHANNEL, data):
        return hil.hvdpsAd5764Write(self.slot, DAC_INDEX, REGISTER_OFFSET, DAC_CHANNEL, data)

    def MAX6627ThermalAdcTemperatureRead(self, device_id):
        return hil.hvdpsMax6627ThermalAdcTmonRead(self.slot, device_id)

    def MAX6627BJTTemperatureRead(self, device_id):
        return hil.hvdpsMax6627BjtSensorTmonRead(self.slot, device_id)

    def MAX6627VicorTemperatureRead(self, device_id):
        return hil.hvdpsMax6627VicorTmonRead(self.slot, device_id)

    def MAX6627AdcLimitsSet(self, low_temperature, high_temperature):
        return hil.hvdpsMax6627AdcLimitsSet(self.slot, low_temperature, high_temperature)

    def Max6627BjtSensorLimitsSet(self, low_temperature, high_temperature):
        return hil.hvdpsMax6627BjtSensorLimitsSet(self.slot, low_temperature, high_temperature)

    def Max6627VicorLimitsSet(self, low_temperature, high_temperature):
        return hil.hvdpsMax6627VicorLimitsSet(self.slot, low_temperature, high_temperature)

    def getHcLcDutRailConfigRegsiter(self):
        return hvdps_regs.HCLC_UHC_RAIL_CONFIG

    def getHcLcPerRailAlarmRegister(self):
        return self.registers.HCLC_PER_RAIL_ALARMS

    def getDutPerRailAlarmRegister(self):
        return self.registers.DUT_RAIL_ALARM

    def clear_dut_rail_alarm(self):
        rail_count = 10
        write_one_to_uv_ov_bit = 0x03
        dut_rail_alarm_register = hvdps_regs.DUT_RAIL_ALARM
        for i in range(rail_count):
            self.WriteRegister(dut_rail_alarm_register(value=write_one_to_uv_ov_bit), index=i)

    def clear_all_alarms_before_test(self):
        self.SetRailsToSafeState()
        self.ClearHclcRailAlarms()
        self.ClearHclcSampleAlarms()
        self.clear_dut_rail_alarm()
        self.ClearGlobalAlarms()

    def getExecuteTriggerRegister(self):
        return self.registers.EXECUTE_TRIGGER

    def ClearTrigExecAlarms(self):
        self.WriteRegister(self.registers.TRIG_EXEC_ALARMS(value=0xffffffff))

    def CheckTqNotifyAlarm(self, dutid, rail_type):
        if self.LegacyTcLooping:
            tqnotifyalarm = 'TqNotifyAlarmDut' + str(dutid)
        else:
            if 'HV' == rail_type:
                tqnotifyalarm = 'TqNotifyHV'
            elif 'LVM' == rail_type :
                tqnotifyalarm = 'TqNotifyLVM'
            # elif 'RLOAD' == rail_type:
            #     tqnotifyalarm = 'TqNotifyLVM'
            else:
                self.Log('error', 'Rail Type not supported: {}'.format(self.railTypes))
        self.WaitForGlobalAlarmBit(tqnotifyalarm)

    def UnGangAllRails(self):
        self.Log('debug', 'Unganging all the rails')
        self.WriteRegister(self.registers.RAIL_MASTER_MASK(value=0x3FF))
        self.WriteRegister(self.registers.REMOTE_SLAVES_MASK(value=0x000))
        self.WriteRegister(self.registers.REMOTE_SLAVES_BOARD_MASK(value=0x000))
        self.WriteRegister(self.registers.HC_I_GANG_REG(value=0x7FF))
        self.WriteRegister(self.registers.RAIL_GANG_MASTER(value=0))

    def get_rail_voltage(self, rail, rail_type, turbo=False):
        if rail_type == 'HV' and turbo == False:
            rail_voltage_regsiter_type = self.registers.DPS_rail_v
            rail_voltage_from_bar1 = self.ReadRegister(rail_voltage_regsiter_type, index=rail).value
            converted_voltage = struct.unpack('f', struct.pack('I', rail_voltage_from_bar1))[0]
        elif rail_type == 'HV' and  turbo == True:
            rail_voltage_regsiter_type = self.registers.DPS_turbo_v
            rail_voltage_from_bar1 = self.ReadRegister(rail_voltage_regsiter_type).value
            converted_voltage = struct.unpack('f', struct.pack('I', rail_voltage_from_bar1))[0]
        elif rail_type == 'LVM' and turbo == False:
            rail_voltage_regsiter_type = self.registers.DUT_rail_v
            rail_voltage_from_bar1 = self.ReadRegister(rail_voltage_regsiter_type, index=rail).value
            converted_voltage = struct.unpack('f', struct.pack('I', rail_voltage_from_bar1))[0]
        elif rail_type == 'LVM' and turbo == True:
            rail_voltage_regsiter_type = self.registers.DUT_turbo_v
            rail_voltage_from_bar1 = self.ReadRegister(rail_voltage_regsiter_type).value
            converted_voltage = struct.unpack('f', struct.pack('I', rail_voltage_from_bar1))[0]
        return converted_voltage

    def get_rail_current(self,rail,rail_type,turbo = False):
        if turbo == True:
            rail_current_regsiter_type = self.registers.DPS_turbo_i
            rail_current_from_bar1 = self.ReadRegister(rail_current_regsiter_type).value
            converted_current = struct.unpack('f', struct.pack('I', rail_current_from_bar1))[0]
        else:
            rail_current_regsiter_type = self.registers.DPS_rail_i
            rail_current_from_bar1 = self.ReadRegister(rail_current_regsiter_type, index=rail).value
            converted_current = struct.unpack('f', struct.pack('I', rail_current_from_bar1))[0]
        return converted_current


    def getBar2RailCommandRegister(self):
        return self.registers.Bar2RailCommand

    def initialize_lvm_rails(self):
        for rail in range(10):
            self.init_lvm_rail(rail)

    def init_lvm_rail(self, chNum):
        reset_ov_uv_limit_command = 0x4c
        self.WriteBar2RailCommand(reset_ov_uv_limit_command, 0, chNum)
        # clear_rail_safestate(slot)

    def get_dac_mapping(self):
        return self.hvdps_hv_dac_mapping

    def getUhcRailConfigRegister(self, rail_type):
        if rail_type == 'HV':
            return self.registers.HCLC_UHC_RAIL_CONFIG
        else:
            return self.registers.DUT_UHC_RAIL_CONFIG

    def voltage_to_ad5560_raw_dac_code(self, voltage):
        offset_dacx_voltage = AD5560_V_CALC_SCALER * AD550_V_REF * AD5560_OFFSET_DAC_CODE / AD5560_DAC_SCALE
        raw_dac_code = int(AD5560_DAC_SCALE * (voltage + offset_dacx_voltage) / (AD5560_V_CALC_SCALER * AD550_V_REF))
        if raw_dac_code < 0:
            raw_dac_code = 0
        elif raw_dac_code > 0xFFFF:
            raw_dac_code = 0xFFFF
        return raw_dac_code

    def ad5560_raw_dac_code_to_voltge(self, raw_dac_code):
        offset_dacx_voltage = AD5560_V_CALC_SCALER * AD550_V_REF * AD5560_OFFSET_DAC_CODE / AD5560_DAC_SCALE
        dac_voltage = AD5560_V_CALC_SCALER * AD550_V_REF * raw_dac_code / AD5560_DAC_SCALE
        v_out = dac_voltage - offset_dacx_voltage
        if v_out < -5 or v_out > 30:
            self.Log('info', 'Invalid DAC to voltage conversion')
        return v_out

    def Ltc2450AdcVoltageRead(self):
        return hil.hvdpsLtc2450AdcVoltageRead(self.slot)

    def getLTC2450AdcVoltageRegister(self):
        return self.registers.LTC2450_ADC_VOLTAGE

    def enableLtc2450AdcPolling(self):
        self.WriteRegister(self.registers.LTC2450_ADC_VOLTAGE(AdcPollingEnable=0x1))

    def disableLtc2450AdcPolling(self):
        self.WriteRegister(self.registers.LTC2450_ADC_VOLTAGE(AdcPollingEnable=0x0))

    def ConnectExternalLoad(self, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True = On
        self.hvdps.Pca9505SetBit(self.hvdps.ext_dict['Ext_Load'][0], self.hvdps.ext_dict['Ext_Load'][1], self.hvdps.ext_dict['Ext_Load'][2], dir)

    def cal_load_toggle(self, res, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True = On

        self.hvdps.Pca9505SetBit(self.hvdps.res_dict[res]['force'][0], self.hvdps.res_dict[res]['force'][1],
                                 self.hvdps.res_dict[res]['force'][2], dir)

        self.hvdps.Pca9505SetBit(self.hvdps.res_dict[res]['sense'][0], self.hvdps.res_dict[res]['sense'][1],
                            self.hvdps.res_dict[res]['sense'][2], dir)

    def cal_connect_force_sense_lines(self,rail,rail_type,calboard_instance=None):
        self.ConnectRailForce(rail)
        self.ConnectRailSense(rail)

    def cal_disconnect_force_sense_lines(self,rail,rail_type,calboard_instance =None):
        self.ConnectRailForce(rail,dir=False)
        self.ConnectRailSense(rail,dir=False)

    def cal_load_connect(self,load,calboard=None):
        self.cal_load_toggle(res=load, dir=True)

    def cal_load_disconnect(self,load,calboard=None):
        self.cal_load_toggle(res=load, dir=False)

    # # -------------------------------------------------
    # # ConnectLvRailSense(slot)
    # #
    # # Connects Sense Relays for a specific rail low voltage
    # # -------------------------------------------------
    def ConnectLvRailSense(self, rail, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True = On
        self.hvdps.Pca9505SetBit(self.hvdps.LvSenseList[rail][0], self.hvdps.LvSenseList[rail][1], self.hvdps.LvSenseList[rail][2], dir)
    # -------------------------------------------------
    # ConnectRailForce(slot)
    #
    # Connects Force Relays for a specific rail
    # -------------------------------------------------
    def ConnectRailForce(self, rail, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True = On
        self.hvdps.Pca9505SetBit(self.hvdps.ForceList[rail][0], self.hvdps.ForceList[rail][1], self.hvdps.ForceList[rail][2], dir)

    def ConnectRailSense(self,rail, dir=True):
        dir = not dir  # relays are reverse logic, but it's easier to remember that True = On
        self.hvdps.Pca9505SetBit(self.hvdps.SenseList[rail][0], self.hvdps.SenseList[rail][1], self.hvdps.SenseList[rail][2], dir)

    def VoltageToSS7Ad5764Dac(self, voltage):
        # >>> hex(((((int(((6.25/6.25)*2**17))) ^ 0x3FFFF) +1) | 0x20000)>>2)
        dac_offset = int(abs((voltage / 6.25) * 2 ** 17))
        if voltage == 0:
            dac_code = dac_offset
        if voltage > 0:
            dac_code = (dac_offset - 1) >> 2
        if voltage < 0:
            twos_complement_dac_offset = (dac_offset ^ 0x3FFFF) + 1
            dac_code = (twos_complement_dac_offset | 0x20000) >> 2
        return dac_code

    def getSampleCollectorIndexRegister(self):
        return self.registers.SAMPLE_COLLECTOR_INDEX

    def getSampleCollectorCountRegister(self):
        return self.registers.SAMPLE_COLLECTOR_COUNT

    def getSampleCollectorDataRegister(self):
        return self.registers.SAMPLE_COLLECTOR_DATA

    def getTurboSampleCollectorIndexRegister(self):
        return self.registers.TURBO_SAMPLE_COLLECTOR_INDEX

    def getTurboSampleCollectorCountRegister(self):
        return self.registers.TURBO_SAMPLE_COLLECTOR_COUNT

    def getTurboSampleCollectorDataRegister(self):
        return self.registers.TURBO_SAMPLE_COLLECTOR_DATA

    def getSampleHeaderRegionBaseRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DutUhcSampleHeaderRegionBase
        else:
            return self.registers.HvUhcSampleHeaderRegionBase

    def getSampleHeaderRegionSizeRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DutUhcSampleHeaderRegionSize
        else:
            return self.registers.HvUhcSampleHeaderRegionSize

    def getSampleRegionBaseRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DutUhcSampleRegionBase
        else:
            return self.registers.HvUhcSampleRegionBase

    def getSampleRegionSizeRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DutUhcSampleRegionSize
        else:
            return self.registers.HvUhcSampleRegionSize

    def getSampleEngineResetRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DUT_SAMPLE_ENGINE_RESET
        else:
            return self.registers.HCLC_SAMPLE_ENGINE_RESET

    def getTurboSampleHeaderRegionBaseRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DUT_TURBO_HEADER_REGION_BASE
        else:
            return self.registers.AD5560_TURBO_HEADER_REGION_BASE

    def getTurboSampleHeaderRegionSizeRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DUT_TURBO_HEADER_REGION_SIZE
        else:
            return self.registers.AD5560_TURBO_HEADER_REGION_SIZE

    def getTurboSampleRegionBaseRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DUT_TURBO_SAMPLE_REGION_BASE
        else:
            return self.registers.AD5560_TURBO_SAMPLE_REGION_BASE

    def getTurboSampleRegionSizeRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DUT_TURBO_SAMPLE_REGION_SIZE
        else:
            return self.registers.AD5560_TURBO_SAMPLE_REGION_SIZE

    def getTurboSampleEngineResetRegister(self, rail_type):
        if rail_type == 'LVM':
            return self.registers.DUT_TURBO_SAMPLE_ENGINE_RESET
        else:
            return self.registers.AD5560_TURBO_SAMPLE_ENGINE_RESET


    def ConfigureAndStartSamplingEngine(self, uhc, delay, count, rate, rail_type, rails=0x3ff):
        if rail_type == 'LVM':
            result = ''
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_COUNT, data=0x{:x}\n'.format(uhc, count)
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_RATE, data=0x{:x}\n'.format(uhc, rate)
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_METADATA_HI, data=0x7654\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_METADATA_LO, data=0x3210\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_RAIL_SELECT, data=0x{:x}\n'.format(uhc, rails & 0x3ff)
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_DELAY, data=0x{:x}\n'.format(uhc, delay)
            result = result + 'Q cmd=0x0, arg=DUT_{}_SAMPLE_START, data=0x0001'.format(uhc)
        else:
            result = ''
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_COUNT, data=0x{:x}\n'.format(uhc, count)
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_RATE, data=0x{:x}\n'.format(uhc, rate)
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_METADATA_HI, data=0x7654\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_METADATA_LO, data=0x3210\n'.format(uhc)
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_RAIL_SELECT, data=0x{:x}\n'.format(uhc, rails & 0xff)
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_DELAY, data=0x{:x}\n'.format(uhc, delay)
            result = result + 'Q cmd=0x0, arg=HCLC_{}_SAMPLE_START, data=0x0001'.format(uhc)
        return result

    def ConfigureAndStartTurboSamplingEngine(self, delay, count, rate, rail_type, rails=0x3ff):
        if rail_type == 'LVM':
            result = ''
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_COUNT, data=0x{:x}\n'.format(count)
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_RATE, data=0x{:x}\n'.format(rate)
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_METADATA_HI, data=0x7654\n'
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_METADATA_LO, data=0x3210\n'
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_RAIL_SELECT, data=0x{:x}\n'.format(rails & 0x3ff)
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_DELAY, data=0x{:x}\n'.format(delay)
            result = result + 'Q cmd=0x0, arg=DUT_TURBO_SAMPLE_START, data=0x0001'
        else:
            result = ''
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_COUNT, data=0x{:x}\n'.format(count)
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_RATE, data=0x{:x}\n'.format(rate)
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_METADATA_HI, data=0x7654\n'
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_METADATA_LO, data=0x3210\n'
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_RAIL_SELECT, data=0x{:x}\n'.format(rails & 0xff)
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_DELAY, data=0x{:x}\n'.format(delay)
            result = result + 'Q cmd=0x0, arg=AD5560_TURBO_SAMPLE_START, data=0x0001'
        return result


    def GetCmdDataForSetTrackingVoltage(self, voltage_set):
        CommandCodeObject = self.registers.LTC2630CommandCodeStruct()
        CommandCodeObject.C0 = 1
        CommandCodeObject.C1 = 1
        CommandCodeObject.C2 = 0
        CommandCodeObject.C3 = 0
        CommandCodeObject.Data = round(self.Ltc2630TrackingDacVoltageConversion(voltage_set))
        cmdData = CommandCodeObject.value
        return cmdData

    def add_tracking_regulator_offset(self,voltage_set):
        HvAd5560TrackingRegulatorOffset = 4.6
        return voltage_set + HvAd5560TrackingRegulatorOffset

    def Ltc2630TrackingDacVoltageConversion(self, voltage_set):
        HvAd5560MinTrackingOutput = 5.0
        HvAd5560MaxTrackingOutput = 28.0
        TrackingRegulatorVoltageScaleFromDacOutput = 9.0
        HvLtc2630VRef = 4.096
        HvLtc2630DacMaxValue = 0x30A  # 28V
        HvLtc2630DacScaleMax = 2 ** 10
        voltage_set = self.add_tracking_regulator_offset(voltage_set)
        if voltage_set < HvAd5560MinTrackingOutput:
            self.Log('debug',
                     'Minimum Voltage required is {}, voltage cannot be set to {}'.format(HvAd5560MinTrackingOutput,
                                                                                          voltage_set))
            voltage_set = HvAd5560MinTrackingOutput
        elif voltage_set > HvAd5560MaxTrackingOutput:
            self.Log('debug', 'Maximum Voltage can be {},Voltage cannot be set to'.format(HvAd5560MaxTrackingOutput,
                                                                                            voltage_set))
            voltage_set = HvAd5560MaxTrackingOutput
        ltc_vout = voltage_set / TrackingRegulatorVoltageScaleFromDacOutput
        ltc_dac_tracking_voltage = (ltc_vout / HvLtc2630VRef) * HvLtc2630DacScaleMax
        if ltc_dac_tracking_voltage >= HvLtc2630DacMaxValue:
            return HvLtc2630DacMaxValue
        else:
            return ltc_dac_tracking_voltage

    def HvTrackingVoltageAdcValueConversion(self, voltage):
        HvRailVoltageHwScalar = 2.402
        Dps25VAdcMax_FullRange = 12.5
        tracking_voltage_from_18bit_dac = voltage / 2 ** 17
        tracking_voltage_adc_value = tracking_voltage_from_18bit_dac * HvRailVoltageHwScalar * Dps25VAdcMax_FullRange
        return tracking_voltage_adc_value

    def initialize_ad5560_hv_force_rail_to_load_or_sense(self, hvRail):
        ad5560_dps_register_two = self.ad5560_registers.DPS_REGISTER2()
        ad5560_dps_register_two.HighZGuard = 1
        ad5560_dps_register_two.INT10K = 1
        ad5560_dps_register_two.SlaveGangMode = 1
        ad5560_dps_register_two.SlewRateControl = 6
        ad5560_dps_register_two.SystemForceSense = 1
        self.WriteAd5560Register(ad5560_dps_register_two.ADDR, ad5560_dps_register_two.value, hvRail)

    def FpgaLoad(self, image):
        if self.FpgaImageIsValid(image):
            with suppress_sensor(f'FVAL HVDPS{self.slot}', f'HDMT_HDDPS:{self.slot}'):
                hil.dpsDeviceDisable(self.slot,0)
                result = hil.dpsFpgaLoad(self.slot,0, image)
                time.sleep(1)
                hil.dpsDeviceEnable(self.slot,0)
        else:
            self.Log('critical', 'Invalid HVDPS Image on slot {}'.format(self.slot))
        return result

    def FpgaImageIsValid(self, image):
        if 'hvdps' in image.lower():
            return True
        else:
            return False

    def FpgaImageAttributeExistsInConfigs(self):
        fpga_configs = ['HVDPS_FPGA_IMAGE']
        for config in fpga_configs:
            try:
                getattr(configs,config)

            except AttributeError:
                self.Log('critical','\'{}\' is not defined in configs.py'.format(config))

    def FpgaImageLoadAttributeExistsInConfigs(self):
        fpga_configs = ['HVDPS_FPGA_LOAD']
        for config in fpga_configs:
            try:
                getattr(configs, config)

            except AttributeError:
                self.Log('critical','\'{}\' is not defined in configs.py'.format(config))

    def LoadFpgaAndGetVersion(self):

        self.FpgaImageAttributeExistsInConfigs()

        self.Log('info', 'Flashing HVDPS MB FPGA with image from \'{}\''.format(configs.HVDPS_FPGA_IMAGE))
        try:
            self.FpgaLoad(configs.HVDPS_FPGA_IMAGE)
        except RuntimeError as e:
            self.Log('error', 'HIL raised a RuntimeError with message "{}"'.format(e))
        self.Log('info', 'New HVDPS MB Image slot {} subslot {}: 0x{}'.format(self.slot, 0,
                                                                              hex(self.GetFpgaVersion())))

    def create_cal_board_instance_and_initialize(self):
        self.hvdps.CalCardInitialize()
        return None

    def ResetVoltageSoftSpanCode(self,rail_type,dutdomainId):
        default_softspancode = 7
        HV_RAIL_START_INDEX = 16
        for rail in range(self.RAIL_COUNT[rail_type]):
            self.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
            if rail_type == 'HV':
                self.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, default_softspancode, rail + HV_RAIL_START_INDEX)
            else :
                self.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, default_softspancode, rail)
        self.SetRailsToSafeState()


    def ResetCurrentSoftSpanCode(self,rail_type,dutdomainId):
        default_softspancode = 1
        HV_RAIL_START_INDEX = 16
        if rail_type == 'HV':
            for rail in range(self.RAIL_COUNT[rail_type]):
                self.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
                self.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, default_softspancode | 0x8, rail + HV_RAIL_START_INDEX)
            self.SetRailsToSafeState()

    def check_for_single_lvm_rail_comparator_alarm(self, expected_alarm_field):
        global_alarms = self.get_global_alarms()
        if global_alarms == ['DutOvUvCompAlarms']:
            dut_comparison_alarms = self.ReadRegister(self.registers.DUT_COMPARISON_ALARMS)
            dut_comparison_alarms_list = []
            for field in dut_comparison_alarms.fields():
                if getattr(dut_comparison_alarms, field) == 1:
                    dut_comparison_alarms_list.append(field)
            if dut_comparison_alarms_list == [expected_alarm_field]:
                self.Log('debug',
                         'Expected alarm : {} Received alarm: {}'.format(expected_alarm_field, dut_comparison_alarms_list))
                return True
            else:
                self.Log('error',
                         'Expected only {} to be set in dut comparison alarm register. Actual alarms set were {}'.format(
                             expected_alarm_field, dut_comparison_alarms_list))
                return False
        else:
            self.Log('error',
                     'Expected only \'DutOvUvCompAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(
                         global_alarms))
        return False

    def TurnONStreamingWithTrigger(self):
        s2h_start_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_START)
        self.hvdps.rc.send_trigger(s2h_start_trigger_value)

    def TurnOFFStreamingWithTrigger(self):
        s2h_stop_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_STOP)
        self.hvdps.rc.send_trigger(s2h_stop_trigger_value)

    def ResetStreamingWithTrigger(self):
        s2h_reset_trigger_value = (symbols.DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | (symbols.EVENTTRIGGER.S2H_RESET)
        self.hvdps.rc.send_trigger(s2h_reset_trigger_value)
