################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os

from Common import fval
from Common.instruments.base import Instrument
from Common import hilmon as hil
from Common.instruments.dps.dpsSubslot import DpsSubslot



class DpsBase(Instrument):

    def __init__(self, slot, rc=None, name=None):
        super().__init__(name)
        self.slot = slot
        self.rc = rc
        self.subslots = []
        self.under_test = False


    def Discover(self,slot):
        pass



    def Initialize(self):
        pass




