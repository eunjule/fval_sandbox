# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import hilmon as hil
from Common.fval import Log
from Common.instruments.base import Instrument

class Cb2(Instrument):
    def __init__(self, name = None):
        super(Instrument, self).__init__(name)

    def Connect(self):
        try:
            hil.tiuCalBaseConnect()
            return True
        except RuntimeError:
            Log('warning', f'{self.name()}: Unable to Connect')
            return False

    def Initialize(self):
        hil.tiuCalBaseInit()

    def Disconnect(self):
        hil.tiuCalBaseDisconnect()

    def toggle_negedge_tiu_spare_gpio_bit(self, bit):
        hil.tiuCalBaseGpioWrite(bit, 1)
        hil.tiuCalBaseGpioWrite(bit, 0)
        hil.tiuCalBaseGpioWrite(bit, 1)

    class ConnectError(Exception):
        pass
