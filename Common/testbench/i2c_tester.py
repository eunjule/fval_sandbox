# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import random
from random import getrandbits


class I2cTester():
    def __init__(self, i2c_interface):
        self.i2c_interface = i2c_interface

    def __enter__(self):
        self.reset()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.reset()

    def reset(self):
        self.i2c_interface.reset()

    def send_msg(self, msg):
        for data in msg:
            self.push_data(data)
        self.transmit()
        if len(msg) >= 2:
            self.wait_i2c_busy()

    def push_data(self, data):
        self.i2c_interface.push_data_fifo(data)

    def transmit(self, enable_stop_bit=1):
        self.i2c_interface.set_transmit_bit(enable_stop_bit)

    def get_transmit_fifo_count(self):
        return self.i2c_interface.get_transmit_fifo_count()

    def wait_i2c_busy(self):
        self.i2c_interface.wait_i2c_busy()

    def get_address_nak(self):
        return self.i2c_interface.get_address_nak()

    def get_transmit_fifo_count_error(self):
        return self.i2c_interface.get_transmit_fifo_count_error()

    def log_status_register(self):
        self.i2c_interface.log_status_register()


def tx_fifo_count_error_bit_test(i2c_interface):
    msg_list = [
        [],  # Empty message
        [i2c_interface.i2c_addr],  # One byte message
        [i2c_interface.i2c_addr, 0],  # Two byte message
        [i2c_interface.i2c_addr, 0, 1, 2]  # Greater than two byte message
    ]
    error_msgs = []
    for msg in msg_list:
        with I2cTester(i2c_interface) as dut:
            dut.send_msg(msg)
            observed = dut.get_transmit_fifo_count_error()
        if not observed and (len(msg) < 2):
            error_msgs.append(expected_vs_observed_msg(name=i2c_interface.interface_name,
                                                       expected=1,
                                                       observed=observed))
        elif observed and (len(msg) >= 2):
            error_msgs.append(expected_vs_observed_msg(name=i2c_interface.interface_name,
                                                       expected=0,
                                                       observed=observed))
    return error_msgs


def address_nak_test(i2c_interface, unused_i2c_addr=0x20):
    msg_list = [[i2c_interface.i2c_addr, 0x00], [unused_i2c_addr, 0x00]]
    error_msgs = []

    with I2cTester(i2c_interface) as dut:
        dut.send_msg(msg_list[0])
        observed = dut.get_address_nak()
    if observed:
        error_msgs.append(expected_vs_observed_msg(name=i2c_interface.interface_name,
                                                   expected=0,
                                                   observed=observed))

    with I2cTester(i2c_interface) as dut:
        dut.send_msg(msg_list[1])
        observed = dut.get_address_nak()
    if not observed:
        error_msgs.append(expected_vs_observed_msg(name=i2c_interface.interface_name,
                                                   expected=1,
                                                   observed=observed))

    return error_msgs


def tx_fifo_count_test(i2c_interface):
    push_count_list = generate_push_count_list(10)
    error_msgs = []

    for expected_count in push_count_list:
        with I2cTester(i2c_interface) as dut:
            for i in range(expected_count):
                dut.push_data(data=0)
            observed_count = dut.get_transmit_fifo_count()

        if observed_count != expected_count:
            error_msgs.append(expected_vs_observed_msg(name=i2c_interface.interface_name,
                                                       expected=expected_count,
                                                       observed=observed_count))

    return error_msgs


def validate_i2c_address_test(i2c_interface):
    error_msgs = []
    available_addresses = get_valid_i2c_addresses(i2c_interface)
    if i2c_interface.i2c_addr not in available_addresses:
        observed = [f'0x{i:02X}' for i in available_addresses]
        error_msgs.append(expected_vs_observed_msg(
            name=i2c_interface.interface_name,
            expected=f'0x{i2c_interface.i2c_addr:02X}',
            observed=observed))

    return error_msgs

def generate_push_count_list(list_size):
    fifo_count_bit_length = 11
    fifo_count_max_value = 2 ** fifo_count_bit_length - 1
    corner_case_list = [0, fifo_count_max_value]
    list_size -= len(corner_case_list)
    assert(list_size > 0)
    random_list = [getrandbits(fifo_count_bit_length) for i in range(list_size)]
    return  corner_case_list + random_list


def expected_vs_observed_msg(name, expected, observed):
    return f'{name}, Expected: {expected}, Observed: {observed}'

def get_valid_i2c_addresses(i2c_interface):
    valid_addresses = []
    for addr in range(0, 0x100, 2):
        with I2cTester(i2c_interface) as dut:
            dut.send_msg([addr, 0])
            if not dut.get_address_nak():
                valid_addresses.append(addr)
    return valid_addresses


def tx_fifo_reset_test(test, i2c_interface):
    error_msgs = []
    expected = tx_fifo_data_list(test)

    for fifo_entry_count in expected:
        fill_up_generic_i2c_tx_fifo(i2c_interface, fifo_entry_count)
        error_msgs += check_tx_fifo_count(i2c_interface,
                                          test.iteration, fifo_entry_count)

        i2c_interface.instrument.reset_generic_i2c(i2c_interface.interface_index)

        error_msgs += check_tx_fifo_count(i2c_interface,
                            test.iteration, expected=test.emtpy_tx_fifo_depth)
    return error_msgs

def tx_fifo_data_list(test):
    return [0, random.randint(0, test.tx_fifo_depth), test.tx_fifo_depth]

def fill_up_generic_i2c_tx_fifo(i2c_interface, fifo_entry_count):
    i2c_interface.instrument.fill_up_generic_i2c_tx_fifo(
        fifo_entry_count, i2c_interface.interface_index)


def check_tx_fifo_count(i2c_interface, iteration, expected):
    observed = read_tx_fifo_count(i2c_interface)
    if observed != expected:
        return [tx_fifo_error_msg(iteration, i2c_interface.interface_index, expected, observed)]
    return []


def read_tx_fifo_count(i2c_interface):
    return i2c_interface.instrument.read_generic_i2c_tx_fifo_count(i2c_interface.interface_index)


def tx_fifo_error_msg(iteration, group, expected, observed):
    return f'Iteration: {iteration} Generic {group} I2C Tx FIFO Count Failure. ' \
           f'Expected {expected} Observed {observed}.'
