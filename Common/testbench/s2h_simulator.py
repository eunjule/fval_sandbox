# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import ctypes
from math import isnan, nan

from Common.fval import Object
from Common.instruments.s2h_interface import S2hBufferIndex, S2hBDHeaderId,\
    S2hInterface, S2hSPMHeaderId
from Hbirctc.instrument import spm_interface


class S2h(Object):
    BD_MAX_PACKET_BYTE_SIZE = 2048
    BD_MAX_NUM_PACKETS = 1280
    SPM_MAX_PACKET_BYTE_SIZE = 24
    SPM_MAX_NUM_PACKETS = 1365

    def __init__(self, rc_register, index=S2hBufferIndex.BD):
        super().__init__()
        if isinstance(index, int):
            index = S2hBufferIndex(index)
        self.index = index
        self.max_num_packets = getattr(S2h,
                                       f'{self.index.name}_MAX_NUM_PACKETS')
        self.buffer_address = None

        self.init_registers(rc_register)
        self.init_stream()
        self.init_actions()

    def init_registers(self, rc_register):
        buffer_name = self.index.name
        self.control_reg = getattr(rc_register, f'S2H_{buffer_name}_CONTROL')()
        self.status_reg = getattr(rc_register, f'S2H_{buffer_name}_STATUS')()
        self.packet_count_reg = getattr(
            rc_register, f'S2H_{buffer_name}_PACKET_COUNT')(
            count=self.max_num_packets)
        self.address_upper_reg = getattr(
            rc_register, f'S2H_{buffer_name}_ADDRESS_UPPER')()
        self.address_lower_reg = getattr(
            rc_register, f'S2H_{buffer_name}_ADDRESS_LOWER')()
        self.valid_packet_index_reg = getattr(
            rc_register, f'S2H_{buffer_name}_VALID_PACKET_INDEX')(
            index=S2hInterface.INVALID_PACKET_INDEX)
        self.packet_byte_count_reg = getattr(
            rc_register, f'S2H_{buffer_name}_PACKET_BYTE_COUNT')(
            count=getattr(S2h, f'{buffer_name}_MAX_PACKET_BYTE_SIZE'))

    def init_actions(self):
        self.write_actions = {
            self.control_reg.ADDR: self.control_write}

        self.read_actions = {
            self.packet_count_reg.ADDR: self.read_packet_count,
            self.packet_byte_count_reg.ADDR: self.read_packet_byte_count,
            self.status_reg.ADDR: self.read_status,
            self.control_reg.ADDR: self.read_control,
            self.valid_packet_index_reg.ADDR: self.update_valid_index}

    def init_stream(self):
        self.status_reg.streaming_enabled = 1
        self.valid_packet_index_reg.index = self.max_num_packets - 1
        self.prev_valid_index = self.max_num_packets - 1
        self._spm_time_stamp_counter = 0
        self._packet_count_addend = 0

    def refresh_bd_stream(self):
        stream = self.stream()
        i = self.valid_packet_index_reg.index
        stream[i].packet_signature = S2hBDHeaderId.packet_signature.value
        stream[i].bulk_packet_version = \
            S2hBDHeaderId.bulk_packet_version.value
        stream[i].packet_counter = i + self._packet_count_addend
        self.update_addends()

    def refresh_spm_stream(self):
        stream = self.stream()
        bps_present = self.bps_present_for_packet()
        i = self.valid_packet_index_reg.index

        stream[i].packet_header_count = self.counter_for_packet(i)
        stream[i].bps_0_present = bps_present[0]
        stream[i].bps_1_present = bps_present[1]
        stream[i].bps_2_present = bps_present[2]
        stream[i].spm_enable = self.spm_enable_for_packet()
        stream[i].packet_type = self.packet_type_for_packet()
        stream[i].time_stamp_us = self.time_stamp_for_packet()
        stream[i].pout_bps_0 = self.pout_data_for_packet(0)
        stream[i].pout_bps_1 = self.pout_data_for_packet(1)
        stream[i].pout_bps_2 = self.pout_data_for_packet(2)
        stream[i].pout_sum = self.pout_sum_data_for_packet(stream[i])

        self.update_addends()

    def bps_present_for_packet(self):
        return spm_interface.SpmInterface().bps_present()

    def counter_for_packet(self, valid_packet_index):
        return valid_packet_index + self._packet_count_addend

    def spm_enable_for_packet(self):
        return int(spm_interface.SpmInterface().is_spm_enabled())

    def packet_type_for_packet(self):
        return S2hSPMHeaderId.packet_type.value

    def time_stamp_for_packet(self):
        return int(self._spm_time_stamp_counter)

    def pout_data_for_packet(self, bps_num):
        return spm_interface.SpmInterface().bps_pout_data(bps_num)

    def pout_sum_data_for_packet(self, packet):
        if [packet.bps_0_present, packet.bps_1_present, packet.bps_2_present] \
            == [0, 0, 0] or not bool(packet.spm_enable):
            return nan
        else:
            pout_sum = \
                (0 if isnan(packet.pout_bps_0) else packet.pout_bps_0) + \
                (0 if isnan(packet.pout_bps_1) else packet.pout_bps_1) + \
                (0 if isnan(packet.pout_bps_2) else packet.pout_bps_2)
        return pout_sum

    def update_addends(self):
        if self.valid_packet_index_reg.index == self.max_num_packets - 1:
            self._packet_count_addend += self.max_num_packets
        self._spm_time_stamp_counter += (S2hInterface.PACKET_REFRESH_MS * 1e3)

    def control_write(self, offset, data):
        self.control_reg.value = data

        if self.control_reg.streaming_start:
            self.status_reg.streaming_enabled = int(True)
            self.control_reg.streaming_start = 0  # Cleared upon read
        if self.control_reg.streaming_stop:
            self.status_reg.streaming_enabled = int(False)
            self.control_reg.streaming_stop = 0  # Cleared upon read
        if self.control_reg.streaming_reset:
            self.valid_packet_index_reg.index = \
                S2hInterface.INVALID_PACKET_INDEX
            self.control_reg.streaming_reset = 0  # Cleared upon read
            self._packet_count_addend = 0

    def read_status(self, offset):
        return self.status_reg.value

    def read_control(self, offset):
        return self.control_reg.value

    def stream_info(self):
        buffer_size = self.packet_count_reg.count * \
                      self.packet_byte_count_reg.count
        if not hasattr(self,'_stream_buf'):
            self._stream_buf = ctypes.create_string_buffer(buffer_size)
        self.buffer_address = ctypes.addressof(self._stream_buf)

        return self.buffer_address, buffer_size

    def stream(self):
        packet_count = self.packet_count_reg.count
        stream_class = getattr(S2hInterface, f'{self.index.name}StreamPacket')
        if self.buffer_address is None:
            self.stream_info()
        stream = (stream_class * packet_count).from_address(self.buffer_address)
        return stream

    def get_circular_valid_index(self, index):
        return index % self.max_num_packets

    def update_valid_index(self, offset):
        if self.status_reg.streaming_enabled:
            if self.valid_packet_index_reg.index != \
                    S2hInterface.INVALID_PACKET_INDEX:
                self.prev_valid_index = self.valid_packet_index_reg.index
                increment_index = self.valid_packet_index_reg.index + 1
                self.valid_packet_index_reg.index = \
                    self.get_circular_valid_index(increment_index)
            else:
                increment_index = self.prev_valid_index + 1
                self.valid_packet_index_reg.index = \
                    self.get_circular_valid_index(increment_index)
            getattr(self, f'refresh_{self.index.name.lower()}_stream')()
        return self.valid_packet_index_reg.index

    def read_packet_count(self, offset):
        return self.packet_count_reg.count

    def read_packet_byte_count(self, offset):
        return self.packet_byte_count_reg.count
