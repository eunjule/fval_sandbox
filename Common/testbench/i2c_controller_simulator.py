################################################################################
# INTEL CONFIDENTIAL - Copyright 2020. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from ctypes import c_uint

from Common import fval
from Common.register import Register


class I2CInterfaceSimulator(fval.Object):
    def __init__(self, i2c_addr, base_offset, write_cmd, read_cmd):
        super().__init__()

        if isinstance(i2c_addr, int):
            i2c_addr = [i2c_addr]
        self.i2c_addr = i2c_addr
        self.status_offset = base_offset
        self.transmit_offset = base_offset + 0x04
        self.tx_fifo_offset = base_offset + 0x08
        self.rx_fifo_offset = base_offset + 0x0C
        self.reset_offset = base_offset + 0x10
        self.write_cmd = write_cmd
        self.read_cmd = read_cmd

        self.write_actions = {self.transmit_offset: self.transmit,
                              self.tx_fifo_offset: self.push_data,
                              self.reset_offset: self.reset}
        self.read_actions = {self.status_offset: self.update_status_reg,
                             self.rx_fifo_offset: self.pop_data}

        self.status_register = STATUS()
        self.tx_fifo = []
        self.rx_fifo = []
        self.current_cmd = None

    def push_data(self, offset, data):
        self.tx_fifo.append(data & 0xFF)

    def pop_data(self, offset):
        data = 0
        if len(self.rx_fifo):
            data = self.rx_fifo.pop(0)
        return data

    def reset(self, offset, enable):
        if enable:
            self.tx_fifo.clear()
            self.rx_fifo.clear()
            self.status_register.value = 0

    def update_status_reg(self, offset):
        self.status_register.transmit_fifo_count = len(self.tx_fifo)
        self.status_register.receive_fifo_count = len(self.rx_fifo)
        return self.status_register.value

    def transmit(self, offset, data):
        # Check for invalid tx_fifo size
        if len(self.tx_fifo) < 2:
            self.status_register.transmit_fifo_count_error = 1
            return

        i2c_addr = self.tx_fifo.pop(0)
        # Check for valid i2c_addr
        if (i2c_addr & 0xFE) not in self.i2c_addr:
            self.reset(offset=0, enable=1)
            self.status_register.address_nak = 1
            return

        if i2c_addr & 0x01:
            self._read_from_device()
        else:
            self._write_to_device()

    def _read_from_device(self):
        cmd = self.current_cmd
        num_bytes_to_read = self.tx_fifo.pop(0)
        for i in range(num_bytes_to_read):
            data = self.read_cmd(cmd)
            if type(data) == bytes:
                data = int.from_bytes(data, 'little')
            self.rx_fifo.append(data)
            cmd += 1

    def _write_to_device(self):
        cmd = self.tx_fifo.pop(0)
        self.current_cmd = cmd
        num_bytes_to_write = len(self.tx_fifo)
        for i in range(num_bytes_to_write):
            self.write_cmd(cmd, self.tx_fifo.pop(0))
            cmd += 1


class STATUS(Register):
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]
