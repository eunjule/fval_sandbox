# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object


def create_pmbus_devices(num_devices):
    return [PMBus() for i in range(num_devices)]


# HBI MbBoard Delta Power Supply
# Model Number: DTQ3000BW12M32
# Part Number: ECD16020101
class PMBus(Object):

    # See "http://www.pmbus.org/Assets/PDFS/
    # Public/PMBus_Specification_Part_II_Rev_1-2_20100906.pdf"
    # for command codes.
    KNOWN_VALUE_REGISTERS = {0x19: b'\xB0',
                             0x98: b'\x22'}

    def __init__(self):
        super().__init__()

        self.registers = self.KNOWN_VALUE_REGISTERS

    def pmbus_write(self, command, data):
        command &= 0xFF
        if command in self.registers.keys():
            self.registers[command] = data

    def pmbus_read(self, command):
        return self.registers[command & 0xFF]
