# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.



class DmaSimulator():
    DWORD_BYTE_SIZE = 4

    def __init__(self):
        self.memory_table = {}

    def dma_read(self, offset, length):
        is_dword_multiple = (length % DmaSimulator.DWORD_BYTE_SIZE) == 0
        assert is_dword_multiple

        block_offset, block_data = self.get_memory_block(offset)
        block_size = len(block_data)
        if offset == block_offset and length == block_size:
            return bytes(block_data)
        else:
            data = bytes(block_data)
            for i in range(block_size, length, block_size):
                offset += block_size
                block_offset, block_data = self.get_memory_block(offset)
                data += bytes(block_data)
            return data

    def dma_write(self, offset, data):
        num_bytes = len(data)
        is_dword_multiple = (num_bytes % DmaSimulator.DWORD_BYTE_SIZE) == 0
        assert is_dword_multiple

        if offset in self.memory_table:
            del self.memory_table[offset]
        self.memory_table[offset] = data
        self._remove_stale_ram_data()

    def get_memory_block(self, offset):
        for block_offset in reversed(list(self.memory_table.keys())):
            block_data = self.memory_table[block_offset]
            if block_offset <= offset < (block_offset + len(block_data)):
                self._mark_memory_block_least_recently_used(block_offset)
                return block_offset, block_data
        else:
            self.memory_table[offset] = bytearray(1024)
            self._remove_stale_ram_data()
            return offset, self.memory_table[offset]

    def _mark_memory_block_least_recently_used(self, block_offset):
        '''Use insertion order for LRU algorithm'''
        block_data = self.memory_table[block_offset]
        del self.memory_table[block_offset]
        self.memory_table[block_offset] = block_data

    def _remove_stale_ram_data(self):
        '''Avoid using lots of RAM for simulation, becomes slow otherwise'''
        if len(self.memory_table.keys()) > 10000:
            oldest_key = list(self.memory_table.keys())[0]
            del self.memory_table[oldest_key]

    def reset_ddr(self):
        self.memory_table = {}
