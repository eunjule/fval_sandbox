################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re

from Common import fval


class CachingBltInfo:

    def __init__(self,callback_function,name=None):
        self._blt = None
        self.name = name
        self.Log = fval.Log
        self.callback_function = callback_function

    @property
    def VendorName(self):
        if not self._blt:
            self._blt = self.callback_function()
        return self._blt.VendorName

    @property
    def DeviceName(self):
        if not self._blt:
            self._blt = self.callback_function()
        return self._blt.DeviceName

    @property
    def PartNumberAsBuilt(self):
        if not self._blt:
            self._blt = self.callback_function()
        return self._blt.PartNumberAsBuilt

    @property
    def PartNumberCurrent(self):
        # print(self.callback_function)
        if not self._blt:
            self._blt = self.callback_function()
        return self._blt.PartNumberCurrent

    @property
    def SerialNumber(self):
        if not self._blt:
            self._blt = self.callback_function()
        return self._blt.SerialNumber

    @property
    def ManufactureDate(self):
        if not self._blt:
            self._blt = self.callback_function()
        return self._blt.ManufactureDate
    
    def validate_blt_part_number(self):
        # Example BLT PartNumberCurrent: AAK15216-301
        if not re.match('^[A-Z]{3}[0-9]{5}-[1-9]{1}[0-9]{2}$', self.PartNumberCurrent):
            raise BltError(f'Invalid BLT PartNumberCurrent: {self.PartNumberCurrent}. Format error.')
    
    def fab_letter(self):
        self.validate_blt_part_number()
        return 'ABCDEFGHI'[int(self.PartNumberCurrent[9])-1]
    
    def eco_number(self):
        self.validate_blt_part_number()
        return int(self.PartNumberCurrent[10:12])

    def write_to_log(self):
        self.Log('info','{} BLT:'.format(self.name))
        # self.Log('debug', '  VendorName = {}'.format(self.VendorName))
        # self.Log('debug', '  DeviceName = {}'.format(self.DeviceName))
        # self.Log('debug', '  PartNumberAsBuilt = {}'.format(self.PartNumberAsBuilt))
        self.Log('info', '  PartNumberCurrent = {}'.format(self.PartNumberCurrent))
        self.Log('info', '  SerialNumber = {}'.format(self.SerialNumber))
        # self.Log('debug', '  ManufactureDate = {}'.format(self.ManufactureDate))


class BltError(Exception):
    pass

