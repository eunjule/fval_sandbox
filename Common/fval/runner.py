# Copyright 2019-2020 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import random
import time
import sys
import traceback

from .core import Log, RepeatColor, ResetColor, set_current_test
from .seed import TestSeed
from .testresult import TestResult
from .testsuite import TestSuite


class TestRunner():
    def __init__(self, seed_generator, ordered_seed_list=None, failfast=False):
        self._seed_generator = seed_generator
        self._ordered_seed_list = ordered_seed_list
        self._failfast = failfast
        self._test_seed = self._seed_generator.GetTestSeed()

    def _run_suite(self, suite, result, seed, repeat_number):
        if self._failfast and not result.was_successful():

            return
        suite_list = []
        for suite_item in suite:
            suite_list.append(suite_item)
        test_index = 0
        index = 0
        while (len(suite_list) > 0):
            if self._ordered_seed_list == None:
                test_index = self._test_seed.seed % len(suite_list)

            suite_list[test_index].repeat_number = repeat_number
            # if _is_suite(suite_list[test_index]):
            #     self._run_suite(suite_list[test_index], result, seed, repeat_number)
            # else:
                # NOTE: Pass the current test instance to fval so that we can connect
                # logging errors to test failures
            set_current_test(suite_list[test_index])

                # set up test seed and seed random number generator
            if seed is None:
                if self._ordered_seed_list != None:
                    ts = TestSeed(self._ordered_seed_list[index])
                    random.seed(ts.seed)  # set random number generator for the next test
                    suite_list[test_index].seed = ts.seed  # pass seed number to the test
                else:
                    suite_list[test_index].seed = self._test_seed.seed  # pass seed number to the test
            else:  # supercede test seed if provided
                ts = TestSeed(seed)
                random.seed(ts.seed)  # set random number generator for the next test
                suite_list[test_index].seed = ts.seed  # pass seed number to the test

            try:
                suite_list[test_index](result)  # run test
            except KeyboardInterrupt:
                Log('error',f'Keyboard Interrupt from user')
                Log('error',f'{traceback.format_exc()} ')
                break
            if self._failfast and not result.was_successful():
                break

            self._test_seed = self._seed_generator.GetTestSeed()
            suite_list.pop(test_index)
            index += 1

    def run(self, test, seed, repeat_count):
        "Run the given test case or test suite."
        result = TestResult()
        start_time = time.time()
        set_current_test(None)

        for repeat_number in range(1, repeat_count + 1):
            Log('info', f'{RepeatColor()}>>>>> REPEAT ITERATION '\
                             f'{repeat_number} / {repeat_count} <<<<<{ResetColor()}')
            self._run_suite(test, result, seed, repeat_number)

        testsRun =  len(result.skipped) \
                                          + len(result.expected_failures) \
                                          + len(result.failures)\
                                          + len(result.unexpected_successes)\
                                          + len(result.success)

        time_taken = float(time.time() - start_time)

        message = f'Total tests {testsRun}. ('\
                  f'Passes = {len(result.success)}, '\
                  f'Unexpected Passes = {len(result.unexpected_successes)} '\
                  f'Fails = {len(result.failures)}, '\
                  f'Expected Fails = {len(result.expected_failures)}, '\
                  f'Skipped = {len(result.skipped)}), '\
                  f'Time Taken: {time_taken})'

        Log('info', '{0}'.format(message))
        return result


def _is_suite(suite):
    try:
        return issubclass(suite, TestSuite)
    except TypeError:
        return False