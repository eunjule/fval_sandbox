################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: __init__.py
#-------------------------------------------------------------------------------
#     Purpose: fval module entry. This file just imports other files.
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/01/15
#       Group: HDMT FPGA Validation
################################################################################

import importlib.machinery
import os

this_directory = os.path.dirname(os.path.realpath(__file__))
projectpaths_file = os.path.join(this_directory, '..', '..', 'Tools', 'projectpaths.py')
importlib.machinery.SourceFileLoader('projectpaths', projectpaths_file).load_module()
    
from Common.fval.core import LevelToColor, ResetColor, RepeatColor, colorize
from Common.fval.core import Timestamp, TimeElapsedLogger
from Common.fval.core import ConfigLogger, Log, SetLoggerLevel, level_is_logged, LoggedError
from Common.fval.core import Component, Object, Env

from Common.fval.bitstruct import Bits, BitStruct, BitStructGroup, LoadStructs, LoadStructsString
from Common.fval.seed import MasterSeed, TestSeed
from Common.fval.tlist import TestList, ResultList

from Common.fval import assembler as asm

from .testcase import TestCase, SkipTest
from .testcasedecorators import expected_failure, skip, process_snapshot, skip_snapshot, deactivate_simulator
from .testsuite import TestSuite

