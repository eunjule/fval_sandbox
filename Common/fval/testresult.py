# Copyright 2020 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

from functools import wraps
import traceback

from .core import LoggedError


def failfast(method):
    @wraps(method)
    def inner(self, *args, **kw):
        if getattr(self, 'failfast', False):
            self.stop()
        return method(self, *args, **kw)
    return inner


class TestResult():
    def __init__(self):
        self.failfast = False
        self.failures = []
        self.success=[]
        self.skipped = []
        self.expected_failures = []
        self.unexpected_successes = []
        self.ordered_tests = []

    @failfast
    def addFailure(self, test, err):
        """err' is a tuple of values as returned by sys.exc_info()."""
        self.failures.append((test, self._exc_info_to_string(err)))

    def addSuccess(self, test):
        if test._softErrors:
            self.addFailure(test, [LoggedError, '', FakeTraceBack()])
        else:
            self.success.append(test)

    def addSkip(self, test, reason):
        self.skipped.append((test, reason))

    def addExpectedFailure(self, test, err):
        self.expected_failures.append(
            (test, self._exc_info_to_string(err)))

    def addUnexpectedSuccess(self, test):
        if test._softErrors:
            self.addExpectedFailure(test, [LoggedError, '', FakeTraceBack()])
        else:
            self.reallyAddUnexpectedSuccess(test)

    def addToOrderedList(self, test, seed):
        self.ordered_tests.append({'TestName': test, 'Seed': seed})

    @failfast
    def reallyAddUnexpectedSuccess(self, test):
        self.unexpected_successes.append(test)

    def addSubTest(self, test, subtest, err):
        if err is not None:
            if getattr(self, 'failfast', False):
                self.stop()
            self.failures.append((subtest, self._exc_info_to_string(err)))

    def was_successful(self):
        return not self.failures

    def _exc_info_to_string(self, err):
        tb_e = traceback.TracebackException(*err)
        return ''.join(list(tb_e.format()))


class FakeTraceBackCode():
    co_filename = None
    co_name = None


class FakeTraceBackFrame():
    f_globals = []
    f_code = FakeTraceBackCode()


class FakeTraceBack():
    tb_frame = FakeTraceBackFrame()
    tb_lineno = -1
    tb_next = None
