################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: bitstruct.py
#-------------------------------------------------------------------------------
#     Purpose: This module contains classes to pack/unpack bit data structures
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/05/15
#       Group: HDMT FPGA Validation
################################################################################

import sys
import os
import copy

from ThirdParty import bitstring
from ThirdParty import pyparsing

class Bits:
    def __init__(self, auto = None, length = None, offset = None, **kwargs):
        self._array = bitstring.BitArray(auto, length, offset, **kwargs)
    # Convert weirdly flipped bitstring slices to "normal" SystemVerilog-gy slices (which align with the HLD register defs)
    def __getitem__(self, key):
        reverse = False
        msb = key.start  # Most significant bit
        lsb = key.stop   # Least significant bit
        if msb < lsb:
            # bitstring only allows msb >= lsb
            reverse = True
            msb, lsb = lsb, msb  # swap(msb, lsb)
        start = len(self._array) - msb - 1
        stop  = len(self._array) - lsb
        value = self._array[start:stop]
        if reverse:
            value.reverse()
        return value.uint
    def __setitem__(self, key, value):
        reverse = False
        msb = key.start  # Most significant bit
        lsb = key.stop   # Least significant bit
        if msb < lsb:
            # bitstring only allows msb >= lsb
            reverse = True
            msb, lsb = lsb, msb  # swap(msb, lsb)
        start = len(self._array) - msb - 1
        stop  = len(self._array) - lsb
        self._array[start:stop] = value
        if reverse:
            self._array[start:stop].reverse()
    @property
    def uint(self):
        return self._array.uint
    @property
    def bytes(self):
        return self._array.bytes

class BitStructGroup(object):
    def __init__(self):
        pass

# Remember: int(x, 0) means figure out the base from the '0b' / '0x' prefix

class BitStruct(object):
    _tree = None
    _decorator = None
    _packing = None
    @classmethod
    def _ClassInit(cls):
        # Performance optimization only available for packing high
        if cls._packing == 'low':
            # We can't use bitstring.Bits for packing low because of the reverse() calls
            cls._bitArray = bitstring.BitArray
        else:
            cls._bitArray = bitstring.Bits
    def __init__(self, auto = None, length = None, offset = None, **kwargs):
        self.Clear()
        if (len(kwargs) > 0) or (auto is not None):
            self.Unpack(auto, length, offset, **kwargs)
    def Clear(self):
        self._fields = []
        self._lengths = {}
        self._constants = copy.deepcopy(self._classConstants)
    def Unpack(self, auto = None, length = None, offset = None, **kwargs):
        # Clear fields before unpacking
        self.Clear()
        if (len(kwargs) > 0) or (auto is not None):
            stream = bitstring.BitStream(auto, length, offset, **kwargs)
            if self._packing == 'low':
                stream.reverse()
            self._Unpack(self._tree, stream)
    def Pack(self, packFormat = 'uint'):
        stream = bitstring.BitStream()
        self._Pack(self._tree, stream)
        if self._packing == 'low':
            stream.reverse()
        if packFormat == 'uint':
            return stream.uint
        elif packFormat == 'bytes':
            return stream.bytes
        else:
            raise Exception('Unknown pack format \'{}\''.format(packFormat))
    def _Unpack(self, tree, stream):
        for statement in tree:
            # statement := ['bit', [msb], fieldName, [arraySize], defaultValues]
            # statement := ['bit', [msb, lsb], fieldName, [arraySize], defaultValues]
            # statement := ['when', fieldName, whenValue, whenTree]
            if statement[0] == 'bit':
                bitDef = statement[1]
                msb = None  # Most significant bit
                lsb = None  # Least significant bit
                if len(bitDef) == 1:
                    msb = int(bitDef[0])
                    lsb = 0
                elif len(bitDef) == 2:
                    msb = int(bitDef[0])
                    lsb = int(bitDef[1])
                length = abs(msb - lsb) + 1
                fieldName = statement[2]
                arraySize = int(statement[3][0])
                if arraySize == 1:
                    data = stream.read(length)
                    if self._packing == 'low':
                        data.reverse()
                    setattr(self, fieldName, data.uint)
                else:
                    array = []
                    for i in range(arraySize):
                        data = stream.read(length)
                        if self._packing == 'low':
                            data.reverse()
                        array.append(data.uint)
                    setattr(self, fieldName, array)
                self._fields.append(fieldName)
                self._lengths[fieldName] = length
            elif statement[0] == 'when':
                fieldName = statement[1]
                whenValue = None
                try:
                    whenValue = int(statement[2], 0)
                except ValueError:
                    whenValue = getattr(self, statement[2])
                whenTree  = statement[3]
                if getattr(self, fieldName) == whenValue:
                    self._Unpack(whenTree, stream)
    def _Pack(self, tree, stream):
        for statement in tree:
            # statement := ['bit', [msb], fieldName, [arraySize], defaultValues]
            # statement := ['bit', [msb, lsb], fieldName, [arraySize], defaultValues]
            # statement := ['when', fieldName, whenValue, whenTree]
            if statement[0] == 'bit':
                bitDef = statement[1]
                msb = None  # most significant bit
                lsb = None  # least significant bit
                if len(bitDef) == 1:
                    msb = int(bitDef[0])
                    lsb = 0
                elif len(bitDef) == 2:
                    msb = int(bitDef[0])
                    lsb = int(bitDef[1])
                length = abs(msb - lsb) + 1
                fieldName = statement[2]
                arraySize = int(statement[3][0])
                if arraySize == 1:
                    defaultValue = int(statement[4][0], 0)
                    value = None
                    if hasattr(self, fieldName):
                        value = getattr(self, fieldName)
                    else:
                        value = defaultValue
                    data = self._bitArray(uint = value, length = length)
                    if self._packing == 'low':
                        data.reverse()
                    stream.append(data)
                else:  # arraySize > 1
                    defaultValues = [int(x, 0) for x in statement[4]]
                    array = None
                    if hasattr(self, fieldName):
                        array = getattr(self, fieldName)
                    else:
                        array = defaultValues
                    for element in array:
                        data = self._bitArray(uint = element, length = length)
                        if self._packing == 'low':
                            data.reverse()
                        stream.append(data)
            elif statement[0] == 'when':
                fieldName = statement[1]
                whenValue = None
                try:
                    whenValue = int(statement[2], 0)
                except ValueError:
                    whenValue = getattr(self, statement[2])
                whenTree  = statement[3]
                if getattr(self, fieldName) == whenValue:
                    self._Pack(whenTree, stream)
    def __str__(self):
        FORMAT_NAME_LEN = 29
        FORMAT_SIZE_LEN = 7
        FORMAT_VALUE_LEN = 30
        FORMAT = '{:<%0d} | {:>%0d} | {:<%0d}\n' % (FORMAT_NAME_LEN, FORMAT_SIZE_LEN, FORMAT_VALUE_LEN)
        result = FORMAT.format('Name', 'Size', 'Value')
        result = result + '{}-|-{}-|-{}\n'.format('-' * FORMAT_NAME_LEN, '-' * FORMAT_SIZE_LEN, '-' * FORMAT_VALUE_LEN)
        for name in self._fields:
            value = getattr(self, name)
            if isinstance(value, list):
                result = result + FORMAT.format(name, len(value), '-')
                groupSize = 5
                groupNames = []
                groupValues = []
                for i, item in enumerate(value):
                    subName = '[{}]'.format(i)
                    subValue = '0x{:x}'.format(item)
                    if groupSize == 1:
                        result = result + FORMAT.format(subName, self._lengths[name], subValue)
                    else:
                        groupNames.append(subName)
                        groupValues.append(subValue)
                        if (i % groupSize) == groupSize - 1:
                            result = result + FORMAT.format(' '.join(groupNames), self._lengths[name], ' '.join(groupValues))
                            groupNames = []
                            groupValues = []

                if groupSize > 1:
                    if len(groupNames) > 0:
                        result = result + FORMAT.format(' '.join(groupNames), self._lengths[name], ' '.join(groupValues))

            else:
                result = result + FORMAT.format(name, self._lengths[name], '0x{:x}'.format(value))
        for name in self._constants:
            value = getattr(self, name)
            result = result + FORMAT.format(name, '-', '0x{:x}'.format(value))
        return result


def _LoadStructs(parseTree, constants = {}):
    def __init__(self, auto = None, length = None, offset = None, **kwargs):
        BitStruct.__init__(self, auto, length, offset, **kwargs)

    result = BitStructGroup()

    for statement in parseTree:
        if statement[1] == 'struct':
            structDecorator = statement[0][0]
            name = statement[2]
            tree = statement[3]
            # Assign class-level constants
            classDict = {'__init__': __init__}
            classDict['_classConstants'] = []
            for subStatement in tree:
                if subStatement[0][0] == 'assign':
                    constantName = subStatement[0][1]
                    value = int(subStatement[0][2], 0)
                    classDict[constantName] = value
                    classDict['_classConstants'].append(constantName)
            for const, value in constants.items():
                classDict[const] = value
                classDict['_classConstants'].append(const)
            s = type(name, (BitStruct,), classDict)
            setattr(result, name, s)
            s._tree = statement[3]
            s._decorator = structDecorator
            if 'packing.high' in s._decorator:
                s._packing = 'high'
            else:
                s._packing = 'low'
            s._ClassInit()
        elif statement[0] == 'assign':
            name = statement[1]
            value = int(statement[2], 0)
            setattr(result, name, value)
    return result

def _CheckSliceInfo(s, loc, toks):
    if len(toks[0]) > 1:
        msb = int(toks[0][0])
        lsb = int(toks[0][1])
        if lsb != 0:
            raise NotImplementedError('Least significant bit != 0 is not currently supported')
        if lsb > msb:
            raise NotImplementedError('Least significant bit > most significant bit is not currently supported')

def _CheckStructDecorator(s, loc, toks):
    decorator = toks[0][0]
    if decorator not in ['', 'packing.low', 'packing.high']:
        raise Exception('Unknown decorator \'{}\'' .format(decorator))

def _ReorderConstantAssignment(s, loc, toks):
    toks[0] = ['assign', toks[0][0], toks[0][2]]

def LoadStructsString(s, constants = {}):
    # References: http://stackoverflow.com/questions/15418379/simple-demonstration-of-using-pyparsings-indentedblock-recursively
    #             http://stackoverflow.com/questions/1547944/how-do-i-parse-indents-and-dedents-with-pyparsing
    #             http://pyparsing.wikispaces.com/file/view/indentedGrammarExample.py

    # A common indent stack for all indented blocks is important for the parser to work properly
    indentStack = [1]

    EOL                = pyparsing.Suppress(pyparsing.LineEnd())
    SOL                = pyparsing.Suppress(pyparsing.LineStart())
    SEMICOLON          = pyparsing.Suppress(pyparsing.Literal(':'))
    COMMA              = pyparsing.Suppress(pyparsing.Literal(','))
    LBRACKET           = pyparsing.Suppress(pyparsing.Literal('['))
    RBRACKET           = pyparsing.Suppress(pyparsing.Literal(']'))
    EQUALS             = pyparsing.Suppress(pyparsing.Literal('=='))
    ATMARK             = pyparsing.Suppress(pyparsing.Literal('@'))
    ASSIGNMENT         = pyparsing.Literal('=')

    BIT                = pyparsing.Keyword('bit')
    PASS               = pyparsing.Keyword('pass')
    WHEN               = pyparsing.Keyword('when')
    STRUCT             = pyparsing.Keyword('struct')

    comment            = pyparsing.Suppress(pyparsing.pythonStyleComment)
    blankline          = SOL + EOL
    identifier         = pyparsing.Word(pyparsing.alphas, pyparsing.alphanums + '_')
    decoratorName      = pyparsing.Word(pyparsing.alphas, pyparsing.alphanums + '_.')
    integer            = pyparsing.Word(pyparsing.nums)
    numberConstant     = pyparsing.Combine('0b' + pyparsing.Word('01')) | pyparsing.Combine('0x' + pyparsing.Word('0123456789ABCDEFabcdef')) | pyparsing.Word(pyparsing.nums)

    constantAssignment = pyparsing.Group(identifier + ASSIGNMENT + numberConstant).setParseAction(_ReorderConstantAssignment)

    sliceInfo          = pyparsing.Group(LBRACKET + integer + pyparsing.Optional(SEMICOLON + integer) + RBRACKET).setParseAction(_CheckSliceInfo)
    arrayInfo          = pyparsing.Group(LBRACKET + integer + RBRACKET)
    
    # NOTE(rodny): Default values are only used for packing
    defaultValues      = pyparsing.Group(pyparsing.Suppress(ASSIGNMENT) + numberConstant + pyparsing.Optional(COMMA + numberConstant) + pyparsing.Optional(COMMA))

    fieldStatement     = BIT + pyparsing.Optional(sliceInfo, default = ['0']) + identifier + pyparsing.Optional(arrayInfo, default = ['1']) + pyparsing.Optional(defaultValues, default = ['0x0'])

    innerStatement     = fieldStatement | constantAssignment | PASS

    whenStatement      = WHEN + identifier + EQUALS + (numberConstant | identifier) + SEMICOLON
    whenBlock          = pyparsing.Forward()
    whenBlockContent   = innerStatement | whenBlock
    whenBlock          << whenStatement + pyparsing.indentedBlock(whenBlockContent, indentStack)

    structDecorator    = pyparsing.Group(ATMARK + decoratorName).setParseAction(_CheckStructDecorator)

    structStatement    = pyparsing.Optional(structDecorator, default = ['']) + STRUCT + identifier + SEMICOLON
    structBlock        = pyparsing.Forward()
    structBlockContent = innerStatement | whenBlock
    structBlock        << structStatement + pyparsing.indentedBlock(structBlockContent, indentStack)

    statement          = pyparsing.Group(structBlock) | constantAssignment

    parser             = pyparsing.ZeroOrMore(statement) + pyparsing.StringEnd()

    # Ignore comments and blank lines
    parser.ignore(comment)
    parser.ignore(blankline)

    parseTree = parser.parseString(s).asList()
    #import json
    #print(json.dumps(parseTree, indent=4, separators=(',', ': ')))
    return _LoadStructs(parseTree, constants)

def LoadStructs(filename, constants = {}):
    # Search in sys.path  for the given filename
    for path in sys.path:
        fn = os.path.join(path, filename)
        if os.path.isfile(fn):  # If fn exists
            filename = fn  # If found, update the path
            break
    # Load the structures in the file
    with open(filename, 'r') as fin:
        return LoadStructsString(fin.read(), constants)

