////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: logging.h
//------------------------------------------------------------------------------
//    Purpose: Common logging classes and methods
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/08/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __LOGGING_H__
#define __LOGGING_H__

#include <string>
#include <sstream>
#include <cstdint>
#include <algorithm>

#define LOG(verbosity) logging::LogStream(__FILE__, __LINE__, __FUNCTION__, verbosity)

inline std::string LPad(std::string x, size_t length)
{
    size_t s = x.size();
    if (s < length) {
        for (size_t i = 0; i < length - s; i++) {
            x = "0" + x;
        }
    }
    return x;
}

inline std::string Hex(uint64_t x)
{
    std::stringstream ss;
    ss << "0x" << std::hex << x;
    return ss.str();
}

inline std::string Bin(uint64_t x)
{
    std::string result;
    do {
        result.push_back('0' + (x & 1));
    } while (x >>= 1);
    std::reverse(result.begin(), result.end());
    return result;
}

namespace logging
{

class Logger
{
public:
    Logger() {};
    virtual ~Logger() {};
    virtual void Log(const std::string& file, size_t line, const std::string& function, const std::string& verbosity, const std::string& message) = 0;
};

void SetCommonLogger(Logger* logger);
void _Log(const std::string& file, size_t line, const std::string& function, const std::string& verbosity, const std::string& message);

class LogStream
{
public:
    explicit LogStream(const std::string& file, size_t line, const std::string& function, const std::string& verbosity)
    {
        this->file      = file;
        this->line      = line;
        this->function  = function;
        this->verbosity = verbosity;
    }
    virtual ~LogStream()
    {
        _Log(file, line, function, verbosity, message);
    }
    LogStream& operator<<(std::string s)
    {
        message.append(s);
        return *this;
    }
    LogStream& operator<<(const char* c)
    {
        std::stringstream ss(c);
        return *this << ss.str();
    }
    LogStream& operator<<(int i)
    {
        std::stringstream ss;
        ss << i;
        return *this << ss.str();
    }
    LogStream& operator<<(uint64_t i)
    {
        std::stringstream ss;
        ss << i;
        return *this << ss.str();
    }
    LogStream& operator<<(uint32_t i)
    {
        std::stringstream ss;
        ss << i;
        return *this << ss.str();
    }
    LogStream& operator<<(uint8_t i)
    {
        std::stringstream ss;
        ss << i;
        return *this << ss.str();
    }
    LogStream& operator<<(float f)
    {
        std::stringstream ss;
        ss << f;
        return *this << ss.str();
    }
    std::string file;
    size_t line;
    std::string function;
    std::string verbosity;
    std::string message;
};

}  // namespace logging

#endif  // __LOGGING_H__
