////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: sequel.cc
//------------------------------------------------------------------------------
//    Purpose:
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/18/14
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

template<typename CustomReader>
bool parse_string(const std::string& data, CustomReader* reader)
{
	bool success = false;
	// set up the lexical analyzer (i.e. the scanner) and the parser
	scanner::yyscan_t scanner;
	scanner::yylex_init(&scanner);
	scanner::YY_BUFFER_STATE buffer_state = scanner::yy_scan_string(data.c_str(), scanner);
	void* parser = parser::ParseAlloc(malloc);
	// clear the scanner location
	my_line_number = 0;
	column_number = 0;
	last_token = "";
	scanner::yyset_lineno(1, scanner);
	// list that stores all the values that will be sent to the parser
	std::vector<std::string*> values_to_parser;
	// clear the parser state (error and complete flags)
	reader->internal_error = false;
	reader->parser_failed = false;
	reader->parser_complete = false;
	do {
		reader->scanner_token = scanner::yylex(scanner);
		std::string* value = nullptr;
		if (parser_value != "") {
			value = new std::string();  // allocate memory for this new parser value
			*value = parser_value;
			values_to_parser.push_back(value);
		}
		parser::Parse(parser, reader->scanner_token, value, static_cast<CustomReader*>(reader));
		// (reader->scanner_token <= 0) --> scanner is done! (error only if scanner_token < 0)
		// (reader->internal_error) --> internal error detected!
		// (reader->parser_failed) --> parser error detected!
		// (reader->parser_complete) --> parser is done!
	} while (reader->scanner_token > 0 && !reader->internal_error && !reader->parser_failed && !reader->parser_complete);

	// release all the allocated memory for the parser values
	for (auto it = values_to_parser.begin(); it != values_to_parser.end(); it++) {
		delete *it;
	}

	if (reader->scanner_token < 0) {  // check for scanner errors
		reader->last_error = "Syntax error at line=" + str(my_line_number) + ": unexpected \"" + last_token + "\"";
	} else if (reader->internal_error) {
		if (reader->parser_failed) {  // check for parser errors
			std::string expected = "";
			auto end = reader->expected_tokens.end();
			for (auto it = reader->expected_tokens.begin(); it != end; it++) {
				expected += *it + " ";
			}
			reader->last_error = "Parse error at line=" + str(my_line_number) + ": unexpected \"" + last_token + "\", expecting: " + expected;
		} else {
			// other internal errors
		}
	} else if (!reader->parser_complete) {  // check if the parser did not complete
		reader->last_error = "Unexpected end of the file found at line=" + str(my_line_number);
	} else {
		success = true;  // everything looks good
	}
	// cleanup the scanner and parser
	scanner::yy_delete_buffer(buffer_state, scanner);
	scanner::yylex_destroy(scanner);
	parser::ParseFree(parser, free);
	return success;
}

template<typename CustomReader>
bool parse(const std::string& filename, CustomReader* reader)
{
	std::string contents;
	std::ifstream fin;
	// enable fstream exceptions
	fin.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try {
		// open the file
		fin.open(filename, std::ifstream::in);
		if (!fin.is_open()) {
			reader->last_error = "Error opening \"" + filename + "\"";
			return false;
		}
		// go to the end of the file
		fin.seekg(0, std::ios::end);
		// grab the file size
		size_t file_size = fin.tellg();
		// reserve memory for the entire file
		contents.reserve(file_size);
		// go to the beginning of the file
		fin.seekg(0, std::ios::beg);
		// read the entire file into memory
		contents.assign((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
		// explicitly close the file
		fin.close();
		return parse_string<CustomReader>(contents, reader);
	} catch (std::ifstream::failure e) {
		reader->last_error = "Error reading \"" + filename + "\"";
		return false;
	}
}

