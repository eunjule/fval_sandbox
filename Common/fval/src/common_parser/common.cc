////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: common.cc
//------------------------------------------------------------------------------
//    Purpose:
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/21/14
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

#include "common.h"

#include <string>
#include <sstream>

std::string str(char* s)
{
	std::stringstream ss(s);
	return ss.str();
}

std::string str(int i)
{
	std::stringstream ss;
	ss << i;
	return ss.str();
}

std::string str(size_t i)
{
	std::stringstream ss;
	ss << i;
	return ss.str();
}

std::string str(float f)
{
	std::stringstream ss;
	ss << f;
	return ss.str();
}

std::string str(double i)
{
	std::stringstream ss;
	ss << i;
	return ss.str();
}

uint64_t uint64(std::string s)
{
	std::stringstream ss(s);
    uint64_t i;
    ss >> i;
    return i;
}

std::string trim_quotes(const std::string& s)
{
	if (s.length() == 0) {
		return "";
	} else if (s.length() == 1) {
		if (s[0] == '"') {
			return "";
		} else {
			return s;
		}
	} else {  // s.length() >= 2
		size_t start = 0;
		size_t length = s.length();
		if (s[0] == '"') {
			start = 1;
			length--;
		}
		if (s[s.length() - 1] == '"') {
			length--;
		}
		return s.substr(start, length);
	}
}

