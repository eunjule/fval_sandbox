////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: base.h
//------------------------------------------------------------------------------
//    Purpose:
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/18/14
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

class Reader
{
public:
	bool internal_error;
	// Don't mess with these fields (read access is fine)!
	bool parser_failed;
	bool parser_complete;
	std::string last_error;
	std::vector<std::string> expected_tokens;
	int scanner_token;
};

template<typename CustomReader>
bool parse_string(const std::string& filedata, CustomReader* reader);

template<typename CustomReader>
bool parse(const std::string& filename, CustomReader* reader);
