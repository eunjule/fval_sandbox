////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: prequel.cc
//------------------------------------------------------------------------------
//    Purpose:
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/18/14
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

#define UPDATE_LOCATION() \
my_line_number   = yylineno; \
column_number = yycolumn + 1; \
last_token    = std::string(yytext)

#define UPDATE_VALUE() \
parser_value  = std::string(yytext)

//
// Scanner/Parser Location
//
/// Token updated by the lexer and read by the parser
std::string last_token;
/// Line number of the current token
int my_line_number;
/// Column number of the current token
int column_number;
/// Value updated by the lexer and read by the parser
std::string parser_value;

#define PARSER_SYNTAX_ERROR() \
	reader->internal_error = true; \
	reader->parser_failed = true; \
	int n = sizeof(yyTokenName) / sizeof(yyTokenName[0]); \
	for (int i = 0; i < n; ++i) { \
		int a = yy_find_shift_action(yypParser, (YYCODETYPE)i); \
		if (a < YYNSTATE + YYNRULE) { \
			reader->expected_tokens.push_back(std::string(yyTokenName[i])); \
		} \
	}

