////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: common.h
//------------------------------------------------------------------------------
//    Purpose:
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/21/14
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

#ifndef __OLA_PARSERS_GENERIC_COMMON_H__
#define __OLA_PARSERS_GENERIC_COMMON_H__

#include <string>
#include <cstdint>

std::string trim_quotes(const std::string& s);

std::string str(char* s);
std::string str(size_t i);
std::string str(int i);
std::string str(float f);
std::string str(double i);

uint64_t uint64(std::string s);

#endif  // __OLA_PARSERS_GENERIC_COMMON_H__

