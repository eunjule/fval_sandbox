////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: fvalc.h
//------------------------------------------------------------------------------
//    Purpose: FVAL C/C++ Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/05/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __FVALC_H__
#define __FVALC_H__

#include "defines.h"
#include <chrono>

namespace fvalc {

class HighResTimer
{
public:
    HighResTimer();
    float Timestamp();
protected:
    bool firstCall;
    std::chrono::time_point<std::chrono::high_resolution_clock> startTime;
};

FVALC_API void SetSeed(uint64_t seed);
FVALC_API uint64_t Rand64();
FVALC_API float RandFloat();
FVALC_API bool RandBool(float probTrue = 0.5);

}  // namespace fvalc

#endif  // __FVALC_H__

