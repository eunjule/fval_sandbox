////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: waveform.h
//------------------------------------------------------------------------------
//    Purpose: Magical waveform template class
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 06/18/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __WAVEFORM_H__
#define __WAVEFORM_H__

namespace fvalc {

template <typename T>
struct Event
{
    float time;
    T value;
};

template <typename T>
struct DefaultUnknownValue {};

template <>
struct DefaultUnknownValue<char>
{
    char operator()() const
    {
        return 'x';
    }
};

template <>
struct DefaultUnknownValue<float>
{
    float operator()() const
    {
        return 999.0f;
    }
};

template <typename T, size_t EVENT_QUEUE_SIZE>
class Waveform
{
public:
    Waveform()
    {
        UNKNOWN_VALUE = defaultUnknownValue();
        Clear();
    }
    virtual ~Waveform() {};
#ifndef SWIG
    virtual const Event<T>& First() const
    {
        return events[readIndex];
    }
    virtual const Event<T>& Last() const
    {
        size_t lastIndex = (readIndex + size - 1) % EVENT_QUEUE_SIZE;
        return events[lastIndex];
    }
#endif
    virtual bool DriveAt(float time, T value)
    {
        if (size < EVENT_QUEUE_SIZE) {
            // Simple "Insert sort"-like algorithm to add the given event to the right spot in the sorted event queue
            for (int i = static_cast<int>(size) - 1; i >= 0; i--) {
                size_t index = (readIndex + i) % EVENT_QUEUE_SIZE;
                if (time >= events[index].time) {
                    Event<T>& event = events[(index + 1) % EVENT_QUEUE_SIZE];
                    event.time = time;
                    event.value = value;
                    size++;
                    return true;
                }
                // Move one event to the right
                events[(index + 1) % EVENT_QUEUE_SIZE] = events[index];
            }
            // We could not find a spot in the event queue to insert the current time,
            // which means that the given time happened earlier than everything else in the queue
            Event<T>& event = events[readIndex];
            event.time = time;
            event.value = value;
            size++;
            return true;
        } else {
            // Event queue is full
            return false;
        }
    }
    virtual T SampleAt(float time)
    {
        if (size <= 0) {
            // No events to read
            return UNKNOWN_VALUE;
        } else if (time < First().time) {
            // Event in the past
            return UNKNOWN_VALUE;
        } else if (time > Last().time) {
            // Event after the last one
            return Last().value;
        } else {
            // Event somewhere in the middle
            T value = First().value;
            for (size_t i = 1; i < size; i++) {
                size_t index = (readIndex + i) % EVENT_QUEUE_SIZE;
                Event<T>& event = events[index];
                if (time < event.time) {
                    break;
                }
                value = event.value;
            }
            return value;
        }
    }
    /// Number of events in this wire
    virtual size_t Size()
    {
        return size;
    }
    virtual void AdvanceTo(float time)
    {
        if (size <= 0) {
            // No events to drop
            return;
        } else if (time < First().time) {
            // We can't advance to the past, nothing dropped
            return;
        } else if (time > Last().time) {
            // Drop everything and advance to the future
            T value = Last().value;
            readIndex = 0;
            Event<T>& event = events[readIndex];
            event.time = time;
            event.value = value;
            size = 1;
            return;
        } else {
            size_t prevIndex = readIndex;
            T prevValue = First().value;
            for (size_t i = 0; i < (size - 1); i++) {
                size_t index = (readIndex + i + 1) % EVENT_QUEUE_SIZE;
                Event<T>& event = events[index];
                if (time < event.time) {
                    Event<T>& prevEvent = events[prevIndex];
                    prevEvent.time = time;
                    prevEvent.value = prevValue;
                    readIndex = prevIndex;
                    size -= i;
                    break;
                }
                prevValue = event.value;
                prevIndex = index;
            }
        }
    }
    void Clear()
    {
        readIndex = 0;
        size = 0;
    }
    /// Event circular buffer. See https://en.wikipedia.org/wiki/Circular_buffer
    Event<T> events[EVENT_QUEUE_SIZE];
    size_t readIndex;
    size_t size;
    T UNKNOWN_VALUE;
    DefaultUnknownValue<T> defaultUnknownValue;
};

}  // namespace fvalc

#endif  // __WAVEFORM_H__

