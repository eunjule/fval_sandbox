////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: fvalc.cc
//------------------------------------------------------------------------------
//    Purpose: FVAL C/C++ Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/05/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <random>

#include "fvalc.h"

namespace fvalc {

HighResTimer::HighResTimer()
{
    firstCall = true;
}

float HighResTimer::Timestamp()
{
    if (firstCall) {
        startTime = std::chrono::high_resolution_clock::now();
        firstCall = false;
        return 0.0;
    } else {
        std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
        return (std::chrono::duration_cast<std::chrono::duration<float>>(now - startTime).count()) * 1000.0f;  // In milliseconds
    }
}

std::linear_congruential_engine<uint64_t, 48271, 0, 0xFFFFFFFFFFFFFFFF> randomEngine;

void SetSeed(uint64_t seed)
{
    randomEngine.seed(seed);
}

uint64_t Rand64()
{
    return randomEngine();
}

// Random float in [0, 1)
float RandFloat()
{
    return static_cast<float>(Rand64()) / static_cast<float>(0xFFFFFFFFFFFFFFFF);
}

bool RandBool(float probTrue)
{
    return RandFloat() < probTrue;
}

}  // namespace fvalc
