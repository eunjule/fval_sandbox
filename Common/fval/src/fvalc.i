////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: fvalc.i
//------------------------------------------------------------------------------
//    Purpose: FVAL C/C++ Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/05/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

%module(directors="1") fvalc

%include <stdint.i>
%include <exception.i>
%include <std_string.i>
%include <std_vector.i>

%feature("director");

#if SWIG_VERSION < 0x020008
#error Requires SWIG 2.0.8 or later.
#endif

%begin %{
#ifdef _MSC_VER
    #include <codeanalysis\warnings.h>
    #pragma warning (disable:ALL_CODE_ANALYSIS_WARNINGS)
    #pragma warning(disable:4100) // unreferenced formal parameter
    #pragma warning(disable:4127) // conditional expression is constant
    #pragma warning(disable:4211) // nonstandard extension used : redefined extern to static
    #pragma warning(disable:4706) // assignment within conditional expression
    #pragma warning(disable:4996) // This function or variable may be unsafe
    #pragma warning(disable:4701) // Potentially uninitialized local variable used
    #pragma warning(disable:4101) // 'swig_obj': unreferenced local variable
    #pragma warning(disable:4244) // warning C4244: 'argument': conversion from 'Py_ssize_t' to 'int', possible loss
    #pragma warning(disable:4459) // warning C4459: declaration of 'swig_this' hides global declaration
    
#else
    #pragma GCC diagnostic ignored "-Wunused-variable"
    #pragma GCC diagnostic ignored "-Wunused-value"
#endif
%}

%{
// Includes these headers in the wrapper code
#include "defines.h"
#include "fvalc.h"
#include "randomvar.h"
%}

namespace fvalc {



}  // namespace fvalc

// Parse the header files to generate wrappers
%include "defines.h"
%include "fvalc.h"
%include "randomvar.h"

