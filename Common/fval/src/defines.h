////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: defines.h
//------------------------------------------------------------------------------
//    Purpose: FVAL C/C++ Defines
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 10/15/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __DEFINES_H__
#define __DEFINES_H__

#include <cstdint>

#define FVALC_EXPORT     __declspec(dllexport)
#define FVALC_IMPORT     __declspec(dllimport)
#define FVALC_BREAKPOINT __debugbreak()

// The preprocessor block below is the magic that allows the
// FVALC API to be imported from the external projects but
// exported from the this project. The build script takes care
// of defining FVALC_API_EXPORT when the project is being
// compiled.
#ifdef SWIG
    #define FVALC_API
#elif _MSC_VER
    #ifdef FVALC_API_EXPORT
        #define FVALC_API FVALC_EXPORT  // Export the API
    #else
        #define FVALC_API FVALC_IMPORT  // Import the API
    #endif
#else
    #define FVALC_API
#endif

#define UNMASK_0(x) (static_cast<uint64_t>(x) & 0x0000000000000000)
#define UNMASK_1(x) (static_cast<uint64_t>(x) & 0x0000000000000001)
#define UNMASK_2(x) (static_cast<uint64_t>(x) & 0x0000000000000003)
#define UNMASK_3(x) (static_cast<uint64_t>(x) & 0x0000000000000007)
#define UNMASK_4(x) (static_cast<uint64_t>(x) & 0x000000000000000F)
#define UNMASK_5(x) (static_cast<uint64_t>(x) & 0x000000000000001F)
#define UNMASK_6(x) (static_cast<uint64_t>(x) & 0x000000000000003F)
#define UNMASK_7(x) (static_cast<uint64_t>(x) & 0x000000000000007F)
#define UNMASK_8(x) (static_cast<uint64_t>(x) & 0x00000000000000FF)
#define UNMASK_9(x) (static_cast<uint64_t>(x) & 0x00000000000001FF)
#define UNMASK_10(x) (static_cast<uint64_t>(x) & 0x00000000000003FF)
#define UNMASK_11(x) (static_cast<uint64_t>(x) & 0x00000000000007FF)
#define UNMASK_12(x) (static_cast<uint64_t>(x) & 0x0000000000000FFF)
#define UNMASK_13(x) (static_cast<uint64_t>(x) & 0x0000000000001FFF)
#define UNMASK_14(x) (static_cast<uint64_t>(x) & 0x0000000000003FFF)
#define UNMASK_15(x) (static_cast<uint64_t>(x) & 0x0000000000007FFF)
#define UNMASK_16(x) (static_cast<uint64_t>(x) & 0x000000000000FFFF)
#define UNMASK_17(x) (static_cast<uint64_t>(x) & 0x000000000001FFFF)
#define UNMASK_18(x) (static_cast<uint64_t>(x) & 0x000000000003FFFF)
#define UNMASK_19(x) (static_cast<uint64_t>(x) & 0x000000000007FFFF)
#define UNMASK_20(x) (static_cast<uint64_t>(x) & 0x00000000000FFFFF)
#define UNMASK_21(x) (static_cast<uint64_t>(x) & 0x00000000001FFFFF)
#define UNMASK_22(x) (static_cast<uint64_t>(x) & 0x00000000003FFFFF)
#define UNMASK_23(x) (static_cast<uint64_t>(x) & 0x00000000007FFFFF)
#define UNMASK_24(x) (static_cast<uint64_t>(x) & 0x0000000000FFFFFF)
#define UNMASK_25(x) (static_cast<uint64_t>(x) & 0x0000000001FFFFFF)
#define UNMASK_26(x) (static_cast<uint64_t>(x) & 0x0000000003FFFFFF)
#define UNMASK_27(x) (static_cast<uint64_t>(x) & 0x0000000007FFFFFF)
#define UNMASK_28(x) (static_cast<uint64_t>(x) & 0x000000000FFFFFFF)
#define UNMASK_29(x) (static_cast<uint64_t>(x) & 0x000000001FFFFFFF)
#define UNMASK_30(x) (static_cast<uint64_t>(x) & 0x000000003FFFFFFF)
#define UNMASK_31(x) (static_cast<uint64_t>(x) & 0x000000007FFFFFFF)
#define UNMASK_32(x) (static_cast<uint64_t>(x) & 0x00000000FFFFFFFF)
#define UNMASK_33(x) (static_cast<uint64_t>(x) & 0x00000001FFFFFFFF)
#define UNMASK_34(x) (static_cast<uint64_t>(x) & 0x00000003FFFFFFFF)
#define UNMASK_35(x) (static_cast<uint64_t>(x) & 0x00000007FFFFFFFF)
#define UNMASK_36(x) (static_cast<uint64_t>(x) & 0x0000000FFFFFFFFF)
#define UNMASK_37(x) (static_cast<uint64_t>(x) & 0x0000001FFFFFFFFF)
#define UNMASK_38(x) (static_cast<uint64_t>(x) & 0x0000003FFFFFFFFF)
#define UNMASK_39(x) (static_cast<uint64_t>(x) & 0x0000007FFFFFFFFF)
#define UNMASK_40(x) (static_cast<uint64_t>(x) & 0x000000FFFFFFFFFF)
#define UNMASK_41(x) (static_cast<uint64_t>(x) & 0x000001FFFFFFFFFF)
#define UNMASK_42(x) (static_cast<uint64_t>(x) & 0x000003FFFFFFFFFF)
#define UNMASK_43(x) (static_cast<uint64_t>(x) & 0x000007FFFFFFFFFF)
#define UNMASK_44(x) (static_cast<uint64_t>(x) & 0x00000FFFFFFFFFFF)
#define UNMASK_45(x) (static_cast<uint64_t>(x) & 0x00001FFFFFFFFFFF)
#define UNMASK_46(x) (static_cast<uint64_t>(x) & 0x00003FFFFFFFFFFF)
#define UNMASK_47(x) (static_cast<uint64_t>(x) & 0x00007FFFFFFFFFFF)
#define UNMASK_48(x) (static_cast<uint64_t>(x) & 0x0000FFFFFFFFFFFF)
#define UNMASK_49(x) (static_cast<uint64_t>(x) & 0x0001FFFFFFFFFFFF)
#define UNMASK_50(x) (static_cast<uint64_t>(x) & 0x0003FFFFFFFFFFFF)
#define UNMASK_51(x) (static_cast<uint64_t>(x) & 0x0007FFFFFFFFFFFF)
#define UNMASK_52(x) (static_cast<uint64_t>(x) & 0x000FFFFFFFFFFFFF)
#define UNMASK_53(x) (static_cast<uint64_t>(x) & 0x001FFFFFFFFFFFFF)
#define UNMASK_54(x) (static_cast<uint64_t>(x) & 0x003FFFFFFFFFFFFF)
#define UNMASK_55(x) (static_cast<uint64_t>(x) & 0x007FFFFFFFFFFFFF)
#define UNMASK_56(x) (static_cast<uint64_t>(x) & 0x00FFFFFFFFFFFFFF)
#define UNMASK_57(x) (static_cast<uint64_t>(x) & 0x01FFFFFFFFFFFFFF)
#define UNMASK_58(x) (static_cast<uint64_t>(x) & 0x03FFFFFFFFFFFFFF)
#define UNMASK_59(x) (static_cast<uint64_t>(x) & 0x07FFFFFFFFFFFFFF)
#define UNMASK_60(x) (static_cast<uint64_t>(x) & 0x0FFFFFFFFFFFFFFF)
#define UNMASK_61(x) (static_cast<uint64_t>(x) & 0x1FFFFFFFFFFFFFFF)
#define UNMASK_62(x) (static_cast<uint64_t>(x) & 0x3FFFFFFFFFFFFFFF)
#define UNMASK_63(x) (static_cast<uint64_t>(x) & 0x7FFFFFFFFFFFFFFF)
#define UNMASK_64(x) (static_cast<uint64_t>(x) & 0xFFFFFFFFFFFFFFFF)

template <const unsigned int bits>
const uint64_t Mask()
{
    return ((1ull << (bits)) - 1);
}

#define SLICE(x, msb, lsb) (((x) >> (lsb)) & Mask<(msb) - (lsb) + 1>())
#define SLICEM(x, msb, lsb, mask) (((x) >> (lsb)) & (mask))
#define BIT(x, bit) (((x) >> (bit)) & 0x1)
#define SETBIT(x, bit) (x |= 1ULL << (bit))     // 1i64 = 64-bit "1"
#define CLRBIT(x, bit) (x &= ~(1ULL << (bit)))  // 1i64 = 64-bit "1"

#define Lambda(return_type, function_arguments, function_body) \
struct { return_type operator () function_arguments function_body }

#endif  // __DEFINES_H__

