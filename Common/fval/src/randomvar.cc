////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: randomvar.cc
//------------------------------------------------------------------------------
//    Purpose: Random variable parser
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 07/06/15
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <streambuf>

#ifdef _MSC_VER
    #pragma warning(disable: 4127)  // conditional expression is constant
    #pragma warning(disable: 4244)  // conversion from 'const fvalc::scanner::flex_int32_t' to 'fvalc::scanner::YY_CHAR', possible loss of data
    #pragma warning(disable: 4100)  // 'yyscanner' : unreferenced formal parameter
    #pragma warning(disable: 4065)  // switch statement contains 'default' but no 'case' labels
    #pragma warning(disable: 4189)  // 'reader' : local variable is initialized but not referenced
    #pragma warning(disable: 4505)  // 'fvalc::scanner::yyunput' : unreferenced local function has been removed
#else
    #pragma GCC diagnostic ignored "-Wunused-variable"
    #pragma GCC diagnostic ignored "-Wunused-function"
    #pragma GCC diagnostic ignored "-Wsign-compare"
#endif

#include "common_parser/base.h"
#include "common_parser/common.h"

using std::string;

#include "fvalc.h"
#include "randomvar.h"

namespace fvalc {

#include "common_parser/prequel.cc"

namespace scanner {
#include "randomvar_flex.c"
}  // namespace scanner

namespace parser {
#include "randomvar_lemon.h"
// FIXME(rodny): Ugly hack to workaround auto-generate code issues with NDEBUG define
#ifdef NDEBUG
#define __NDEBUG_DEFINED__
#undef NDEBUG
#endif
#include "randomvar_lemon.c"
#ifdef __NDEBUG_DEFINED__
#define NDEBUG
#endif
}  // namespace parser

#include "common_parser/sequel.cc"

RandomVar::RandomVar()
{
    type = INVALID;
    weightedValues = nullptr;
}

RandomVar::RandomVar(uint64_t value)
{
    type = SINGLE_VALUE;
    this->value = value;
    weightedValues = nullptr;
}

RandomVar::RandomVar(uint64_t minValue, uint64_t maxValue)
{
    type = MIN_MAX_RANGE;
    range.minValue = minValue;
    range.maxValue = maxValue;
    weightedValues = nullptr;
}

RandomVar::RandomVar(const RandomVar& other)
{
    _DeepCopy(other);
}

RandomVar& RandomVar::operator=(const RandomVar& other)
{
    if (this != &other) {
        _DeepCopy(other);
    }
    return *this;
}

void RandomVar::_DeepCopy(const RandomVar& other)
{
    this->type = other.type;
    this->weightedValues = nullptr;
    switch (this->type) {
    case INVALID:
        break;
    case SINGLE_VALUE:
        this->value = other.value;
        break;
    case MIN_MAX_RANGE:
        this->range = other.range;
        break;
    case WEIGHTED_VALUES:
        this->weightedValues = new std::vector<WeightValue>();
        std::vector<WeightValue>& thisVector = *this->weightedValues;
        std::vector<WeightValue>& otherVector = *other.weightedValues;
        for (size_t i = 0; i < otherVector.size(); i++) {
            WeightValue wv;
            wv.weight = otherVector[i].weight;
            wv.value = new RandomVar(*otherVector[i].value);
            thisVector.push_back(wv);
        }
    }
}

RandomVar::~RandomVar()
{
    if ((type == WEIGHTED_VALUES) && (weightedValues != nullptr)) {
        std::vector<WeightValue>& vector = *weightedValues;
        for (size_t i = 0; i < vector.size(); i++) {
            RandomVar* var = vector[i].value;
            if (var != nullptr) {
                delete var;
            }
        }
        delete weightedValues;
        weightedValues = nullptr;
    }
}

RandomVar RandomVar::Compile(const std::string& expr)
{
    RandomVar result;
    RandomVarReader reader(&result);
    bool success = parse_string<RandomVarReader>(expr, &reader);
    if (reader.internal_error && reader.parser_failed) {
        for (size_t i = 0; i < reader.expected_tokens.size(); i++) {
            std::cout << "Expected tokens: " << reader.expected_tokens[i] << std::endl;
        }
    }
    return result;
}

bool RandomVar::Valid() const
{
    return type != INVALID;
}

uint64_t RandomVar::operator()() const
{
    switch (type) {
    case INVALID:
        // ERROR
        return 0;
    case SINGLE_VALUE:
        return value;
    case MIN_MAX_RANGE:
        return range.minValue + Rand64() % (range.maxValue - range.minValue + 1);
    case WEIGHTED_VALUES:
        std::vector<WeightValue>& vector = *weightedValues;
        uint64_t weightSum = 0;
        for (size_t i = 0; i < vector.size(); i++) {
            weightSum += vector[i].weight;
        }
        uint64_t rnd = Rand64() % weightSum;
        uint64_t sum = 0;
        for (size_t i = 0; i < vector.size(); i++) {
            if (sum <= rnd && rnd < sum + vector[i].weight) {
                return (*vector[i].value)();
            }
            sum += vector[i].weight;
        }
    }
    return 0;
}

RandomVarReader::RandomVarReader(RandomVar* result)
{
    this->result = result;
    this->result->type = WEIGHTED_VALUES;
    this->result->weightedValues = new std::vector<WeightValue>();
}

void RandomVarReader::PushValue(std::string value)
{
    RandomVar* var = new RandomVar(uint64(value));
    varStack.push(var);
}

void RandomVarReader::PushRange(std::string minValue, std::string maxValue)
{
    RandomVar* var = new RandomVar(uint64(minValue), uint64(maxValue));
    varStack.push(var);
}

void RandomVarReader::PopAndCombineWithWeight(std::string weight)
{
    WeightValue wv;
    wv.weight = uint64(weight);
    if (varStack.size() > 0) {
        wv.value = varStack.top(); varStack.pop();
        result->weightedValues->push_back(wv);
    } else {
        internal_error = true;
    }
}

}  // namespace fvalc
