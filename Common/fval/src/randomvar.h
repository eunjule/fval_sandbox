////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: randomvar.h
//------------------------------------------------------------------------------
//    Purpose: Random variable parser
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 07/06/15
//      Group: STTD Sort PDE
////////////////////////////////////////////////////////////////////////////////

#ifndef __RANDOMVAR_H__
#define __RANDOMVAR_H__

#include <string>
#include <stack>
#include <cstdint>

#include "defines.h"

namespace fvalc {

enum RandomVarType
{
    INVALID,
    SINGLE_VALUE,
    MIN_MAX_RANGE,
    WEIGHTED_VALUES
};

// Forward declaration
class RandomVar;

struct FVALC_API MinMaxRange
{
    uint64_t minValue;
    uint64_t maxValue;
};

struct FVALC_API WeightValue
{
    uint64_t weight;
    RandomVar* value;
};

class FVALC_API RandomVar
{
public:
    RandomVar();
    RandomVar(uint64_t value);
    RandomVar(uint64_t minValue, uint64_t maxValue);
    RandomVar(const RandomVar& other);
#ifndef SWIG
    RandomVar& operator=(const RandomVar& other);
#endif
    void _DeepCopy(const RandomVar& other);
    ~RandomVar();
    
    bool Valid() const;
    static RandomVar Compile(const std::string& expr);


    uint64_t operator()() const;

    RandomVarType type;
    uint64_t value;
    MinMaxRange range;
    std::vector<WeightValue>* weightedValues;
};

#include "common_parser/base.h"

#ifndef SWIG

class RandomVarReader : public Reader
{
public:
    RandomVarReader(RandomVar* result);
    void PushValue(std::string value);
    void PushRange(std::string minValue, std::string maxValue);
    void PopAndCombineWithWeight(std::string weight);
    std::stack<RandomVar*> varStack;
    RandomVar* result;
};

#endif

}  // namespace fvalc

#endif  // __RANDOMVAR_H__
