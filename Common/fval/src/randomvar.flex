/*//////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2014. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: randomvar.flex
//------------------------------------------------------------------------------
//    Purpose: Random variable flex file
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 07/06/15
//      Group: STTD Sort PDE
//////////////////////////////////////////////////////////////////////////////*/

%{
// This lexer assumes that the parent file has defined UPDATE_LOCATION and
// UPDATE_VALUE macros.
#include "randomvar_lemon.h"
#include <string>
%}

%option reentrant
%option noyywrap
%option yylineno
%option nounistd
%option never-interactive

integer [0-9]+

%%

"#"[^\n]*                    {/* consume comment */}
"{"                          {UPDATE_LOCATION(); return OPEN_CURLY;}
"}"                          {UPDATE_LOCATION(); return CLOSE_CURLY;}
"["                          {UPDATE_LOCATION(); return OPEN_SQUARE;}
"]"                          {UPDATE_LOCATION(); return CLOSE_SQUARE;}
":"                          {UPDATE_LOCATION(); return COLON;}
":="                         {UPDATE_LOCATION(); return WEIGHTS;}
","                          {UPDATE_LOCATION(); return COMMA;}
{integer}                    {UPDATE_LOCATION(); UPDATE_VALUE(); return INTEGER;}
[ \t\r\n]                    {/* consume whitespace */}

.                            {UPDATE_LOCATION(); return -1;/* error case */}

%%
