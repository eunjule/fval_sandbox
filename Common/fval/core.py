################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: core.py
#-------------------------------------------------------------------------------
#     Purpose: Core fval (FPGA Validation) module
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/01/15
#       Group: HDMT FPGA Validation
################################################################################

import atexit
from datetime import datetime
import inspect
import logging
import logging.handlers
import multiprocessing
import re
import sys
import time

from ThirdParty import colorama
try:
    from _build.bin import fvalc
except ImportError:
    python_version = f'{sys.version_info.major}.{sys.version_info.minor}'
    raise ImportError(f'Loading fvalc failed.\n'
                      f'Possible reasons:\n'
                      f'1) Is the Visual Studio 2019 Redistributable'
                      f' installed?\n'
                      f'2) You are using Python {python_version}, should you'
                      f' be?')

colorama.init()

_NOCOLOR_FORMAT = '%(TIMESTAMP)s|%(LEVEL)s| %(message)s'  # Timestamp worst case = 7*24*60*60*1000.0
_COLOR_FORMAT = '%(TIMESTAMP)s|%(COLOR)s%(LEVEL)s%(COLORRESET)s| %(COLOR)s%(message)s%(COLORRESET)s'  # Timestamp worst case = 7*24*60*60*1000.0
_LOGGER_CALLBACK_FORMAT = '{TIMESTAMP}|{COLOR}{LEVEL}{COLORRESET}| {COLOR}{message}{COLORRESET}'

_LOG_LEVEL = 15
_LOG_NAME = None

timer = fvalc.HighResTimer()


START_TIME = None
def Timestamp():
    global  START_TIME
    if START_TIME is None:
        START_TIME = time.perf_counter()

    total = time.perf_counter() - START_TIME
    minutes = int(total/60)
    today = datetime.now().strftime("%y-%b-%d %H:%M:%S")
    elapsed = f'{minutes:03d}'
    return f'{today} {elapsed}'


def SetLoggerLevel(level):
    global _LOG_LEVEL
    _LOG_LEVEL = level


def ConfigLogger(filename=None):
    if filename is None:
        logging.basicConfig(format=_COLOR_FORMAT, level=logging.DEBUG)
    else:
        configure_multiprocess_logger(filename)


def configure_multiprocess_logger(filename):
    """Create process for each log destination, and redirect stdout/stderr

    Sending stdout/stderr to the log prevents interrupted strings on
    the console, the only process updating the console is the logger process.
    """
    sh = create_log_handler_process(console_log_handler)
    fh = create_log_handler_process(file_log_handler, filename)
    logging.basicConfig(handlers=[sh, fh])
    sys.stdout = StdHandler('STDOUT', 'info')
    sys.stderr = StdHandler('STDERR', 'error')


def create_log_handler_process(target, *args):
    """Launch a separate process to handle logging

    Messages are created in the main process and sent to the created
    process via a queue.
    The method that will cause the log process to exit is registered
    with atexit before the process is started to prevent hangs.
    """
    queue = multiprocessing.Queue()
    atexit.register(close_multiprocess_log, queue)
    args_tuple = (queue,) + args
    process = multiprocessing.Process(target=target, args=args_tuple)
    process.start()
    return logging.handlers.QueueHandler(queue)


def console_log_handler(queue):
    """Create log handler for console and log to it"""
    root = logging.getLogger()
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter(_COLOR_FORMAT))
    root.addHandler(handler)
    log_from_queue(queue)


def file_log_handler(queue, filename):
    """Create log handler for transcript file and log to it"""
    root = logging.getLogger()
    handler = FileHandler(filename)
    handler.setFormatter(logging.Formatter(_NOCOLOR_FORMAT))
    root.addHandler(handler)
    log_from_queue(queue)


def log_from_queue(queue):
    """Read messages from the queue and send to logger"""
    logger = logging.getLogger()
    while True:
        try:
            record = queue.get()
            if record is None:  # We send this as a sentinel to tell the listener to quit.
                break
            logger.handle(record)
        except KeyboardInterrupt:
            continue


def close_multiprocess_log(queue):
    """Send message to logger process to finish"""
    queue.put(None)


class FileHandler(logging.FileHandler):
    def emit(self, record):
        record.msg = remove_ansi_graphics_mode_escape_sequences(record.msg)
        super().emit(record)


class StdHandler():
    """Redirect std stream writes to logs"""
    def __init__(self, name, level):
        self.name = name
        self.level = level
        self.drop_next_newline = False

    def write(self, message):
        """Convert stdout and stderr style stream writes to logged messages

        The std streams explicitly write newlines as separate writes.
        Logging also appends a newline after each logged message.
        To avoid double newlines, keep track and drop the extras, while
        preserving cases where the user used a print to get a blank line.
        """
        m = message.strip()
        if m or not self.drop_next_newline:
            self.drop_next_newline = True
        elif self.drop_next_newline:
            self.drop_next_newline = False
            return
        Log(self.level, f'{self.name}: {m}')


def remove_ansi_graphics_mode_escape_sequences(s):
    return re.sub('\x1b.*?m', '', str(s))
    
def SetLoggerCallback(callback):
    _loggerCallback = callback

_currentTest = None
_currentTestWarningLogged = False

_loggerCallback = None

def set_current_test(test):  # Called by regress.py
    global _currentTest
    _currentTest = test

log_level_colors = {'info'    :                         colorama.Fore.WHITE  + colorama.Back.BLACK,
                    'debug'   : colorama.Style.BRIGHT + colorama.Fore.CYAN   + colorama.Back.BLACK,
                    'warning' : colorama.Style.BRIGHT + colorama.Fore.YELLOW + colorama.Back.BLACK,
                    'error'   : colorama.Style.BRIGHT + colorama.Fore.RED    + colorama.Back.BLACK,
                    'critical': colorama.Style.BRIGHT + colorama.Fore.WHITE  + colorama.Back.RED}

def colorize(msg, color):
    return f'{color}{msg}{ResetColor()}'

def LevelToColor(level):
    return log_level_colors.get(level, colorama.Style.RESET_ALL)

def PassColor():
    return colorama.Style.BRIGHT + colorama.Fore.GREEN

def FailColor():
    return colorama.Style.BRIGHT + colorama.Fore.RED

def HighlightColor():
    return colorama.Style.BRIGHT + colorama.Fore.CYAN
    
def RepeatColor():
    return colorama.Style.BRIGHT + colorama.Fore.MAGENTA
    
def BlueColor():
    return colorama.Style.BRIGHT + colorama.Fore.BLUE

def BrightYellow():
    return colorama.Style.BRIGHT + colorama.Fore.YELLOW
    
def ResetColor():
    return colorama.Style.RESET_ALL

def Log(level, *args, **kwargs):
    global _currentTest, _currentTestWarningLogged, _LOG_LEVEL, _loggerCallback, _LOGGER_CALLBACK_FORMAT

    logger = logging.getLogger(__name__)
    logger.setLevel(_LOG_LEVEL)
    kwargs['extra'] = {}
    kwargs['extra']['LEVEL'] = level.upper()[0]
    kwargs['extra']['COLOR'] = LevelToColor(level)
    kwargs['extra']['COLORRESET'] = ResetColor()
    kwargs['extra']['TIMESTAMP'] = Timestamp()
    getattr(logger, level)(*args, **kwargs)

    if not(_loggerCallback is None):
        values = kwargs['extra']
        values['message'] = args[0]
        _loggerCallback(_LOGGER_CALLBACK_FORMAT.format(**values))

    if level == 'critical':
        raise Exception(str(*args))
    elif level == 'error':
        if _currentTest is None:
            if not _currentTestWarningLogged:
                Log('warning', 'Current test is not set. This is expected in Initialize() and Finalize().')
            _currentTestWarningLogged = True
        else:
            _currentTest._softErrors.append(str(*args))
            
def level_is_logged(level):
    return _LOG_LEVEL <= level


class Object:
    def __init__(self, name = ''):
        pass
        #self.name = name
    
    @classmethod
    def Log(obj, level, *args, **kwargs):
        global _currentTest, _currentTestWarningLogged, _LOG_LEVEL, _loggerCallback, _LOGGER_CALLBACK_FORMAT
        
        logger = None
        if inspect.isclass(obj):
            logger = logging.getLogger(obj.__name__)
        else:
            logger = logging.getLogger(type(obj).__name__)            
        
        logger.setLevel(_LOG_LEVEL)
        kwargs['extra'] = {}
        kwargs['extra']['LEVEL'] = level.upper()[0]
        kwargs['extra']['COLOR'] = LevelToColor(level)
        kwargs['extra']['COLORRESET'] = ResetColor()
        kwargs['extra']['TIMESTAMP'] = Timestamp()
        getattr(logger, level)(*args, **kwargs)

        if not(_loggerCallback is None):
            values = kwargs['extra']
            values['message'] = args[0]
            _loggerCallback(_LOGGER_CALLBACK_FORMAT.format(**values))

        if level == 'critical':
            raise LoggedError(str(*args))
        elif level == 'error':
            if _currentTest is None:
                if not _currentTestWarningLogged:
                    Log('warning', 'Current test is not set. This is expected in Initialize() and Finalize().')
                raise LoggedError(str(*args))
                _currentTestWarningLogged = True
            else:
                _currentTest._softErrors.append(str(*args))

                
class LoggedError(Exception):
    pass
                
                
# Object capable of being randomized
class Randomizable(Object):
    def __init__(self, name = ''):
        super().__init__(name)
    def PreRandomize(self):
        pass
    def Randomize(self):
        pass
    def PostRandomize(self):
        pass

# Generic component (non-randomizable)
class Component(Object):
    def __init__(self, name = ''):
        super().__init__(name)

class Factory(Component):
    def __init__(self, name = ''):
        super().__init__(name)
        self.typeSupersede = {}
        self.nameSupersede = {}
    def Create(self, type, name):
        cls = type
        if type in self.typeSupersede:
            cls = self.typeSupersede[type]
        if name in self.nameSupersede:
            cls = self.nameSupersede[name]
        return cls(name)
    def PrintSupersedes(self):
        self.Log('info', str(self.typeSupersede))
        self.Log('info', str(self.nameSupersede))
    def SupersedeByType(self, originalType, newType):
        self.typeSupersede[originalType] = newType        
    def SupersedeByName(self, name, newType):
        self.nameSupersede[name] = newType


# Object environment for all validation environments
class Env(Component):
    def __init__(self, name = ''):
        super().__init__(name)
    def __init__(self, test):
        self.test = test
        self.factory = Factory()
    @classmethod
    def Initialize(cls):
        pass
    @classmethod
    def Finalize(cls):
        pass
    def PreRun(self):
        pass
    def Run(self):
        pass
    def PostRun(self):
        pass
    def Start(self):
        self.PreRun()
        self.Run()
        self.PostRun()


class TimeElapsedLogger(Object):
    def __init__(self, message):
        super().__init__()
        self.message = message

    def __enter__(self):
        self.start_time = time.perf_counter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        total_seconds = time.perf_counter() - self.start_time
        minutes = int(total_seconds / 60)
        seconds = int(total_seconds)
        milliseconds = int(total_seconds * 1e3)
        microseconds = int(total_seconds * 1e6)
        nanoseconds = int(total_seconds * 1e9)
        self.Log('info', f'{self.message}: Time elapsed in mm, ss, ms, us, ns = '
                         f'{minutes:02}, {seconds % 60:02}, '
                         f'{milliseconds}, {microseconds}, {nanoseconds}')
