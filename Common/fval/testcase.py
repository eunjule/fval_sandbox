# Copyright 2020 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import contextlib
import datetime
import re
import sys
import traceback

import collections

from .core import Object, BrightYellow, HighlightColor, PassColor, \
                  FailColor, ResetColor, colorize


_subtest_msg_sentinel = object()


# Validation base test case class (all tests shall extend from this class)
# unittest failure v.s. unittest error v.s. self._softErrors
# unittest failure := exceptions raised by self.assert*()
# unittest error   := any other exception
# self._softErrors := "soft" errors that don't stop the test (e.g. Log('error'), self.Expect*(), etc)
class TestCase(Object):
    def __init__(self, methodName = 'runTest'):
        super().__init__(methodName)
        self._testMethodName = methodName
        self._cleanups = []
        self._subtest = None
        self.seed = None
        self.repeat_number = 1
        self._softErrors = []
        self.start_time = datetime.datetime.now()
        self._outcome = None
        self.is_snaphot_taken = False
        self.subtest_iteration_count=0
        self.subtest_file_name=''
        self.snapshot_active = False
        self._is_process_snapshot = False
        self._is_snapshot_active = True
        self.env = None

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def id(self):
        return "%s.%s" % (strclass(self.__class__), self._testMethodName)

    def __call__(self, *args, **kwds):
        return self.run(*args, **kwds)

    def countTestCases(self):
        return 1

    def LogStart(self):
        self.Log('info', '{}Running {}.{}.{} TestSeed: {}{}'.format(HighlightColor(), self.__module__, self.__class__.__name__, self.test_name(), self.seed, ResetColor()))
        self.start_time = datetime.datetime.now()
        self._softErrors = []

    def LogEnd(self):
        self.Log('info', 'RERUN: -tn {} -ts {}\''.format(self.Name(), self.seed))
        self.Log('info', self._result_text())

    def _result_text(self):
        message = self._pass_fail_text()
        message += f' {self.Name()}'
        message += self._expected_fail_text()
        message += self._error_count_text()
        message += self._time_taken_text()
        return message

    def _time_taken_text(self):
        self.endTime = datetime.datetime.now()
        timeTaken = self.endTime - self.start_time
        time_taken_text = ' [%.3fms]' % (1000.0 * timeTaken.total_seconds())
        return time_taken_text

    def _expected_fail_text(self):
        if self.is_expected_to_fail():
            reason = self.expecting_failure_reason()
            expected_fail_text = colorize(f' Expected to Fail: {reason}', BrightYellow())
        else:
            expected_fail_text = ''
        return expected_fail_text

    def _pass_fail_text(self):
        if self._test_failed():
            message = colorize('FAILED', FailColor())
        else:
            message = colorize('PASSED', PassColor())
        return message

    def _error_count_text(self):
        num_soft_errors = len(self._softErrors)
        if num_soft_errors > 0:
            if num_soft_errors > 1:
                plural = 's'
            else:
                plural = ''
            error_string = f' ({num_soft_errors} error{plural})'
        else:
            error_string = ''
        return error_string

    def _test_failed(self):
        failed = False
        if len(self._softErrors) > 0:
            failed = True
        if len(self._outcome.errors) > 0:
            for test, error in self._outcome.errors:
                if error != None:
                    failed = True
        # Check for soft errors
        if self._softErrors:
            failed = True
        return failed

    def Name(self):
        return '{}.{}.{}'.format(self.__module__, self.__class__.__name__, self.test_name())

    def test_name(self):
        return self.id().split('.')[-1]

    def Module(self):
        return f'{self.__module__}'

    def run(self, result=None):
        orig_result = result
        if result is None:
            result = self.defaultTestResult()
            startTestRun = getattr(result, 'startTestRun', None)
            if startTestRun is not None:
                startTestRun()

        if self.is_skipped():
            skip_why = self.skip_reason()
            result.addSkip(self, skip_why)
            return result

        expecting_failure = self.is_expected_to_fail()
        outcome = _Outcome(result)
        self._outcome = outcome
        result.addToOrderedList(self.Name(), self.seed)
        outcome.expecting_failure = expecting_failure
        testMethod = getattr(self, self._testMethodName)
        with outcome.testPartExecutor(self, isTest=True):
            self.setUp()
        if outcome.success:
            with outcome.testPartExecutor(self, isTest=True):
                testMethod()
            outcome.expecting_failure = False
            with outcome.testPartExecutor(self):
                if not self.is_snaphot_taken:
                    self.take_post_test_snapshot()
                self.tearDown()

        for test, reason in outcome.skipped:
            result.addSkip(test, reason)

        self._feedErrorsToResult(result, outcome.errors)
        if outcome.success:
            if expecting_failure:
                if outcome.expected_failure:
                    result.addExpectedFailure(self, outcome.expected_failure)
                else:
                    result.addUnexpectedSuccess(self)
            else:
                result.addSuccess(self)
        return result

        # explicitly break reference cycles:
        # outcome.errors -> frame -> outcome -> outcome.errors
        # outcome.expectedFailure -> frame -> outcome -> outcome.expectedFailure
        outcome.errors.clear()
        outcome.expected_failure = None

        # clear the outcome, no more needed
        self._outcome = None

    def is_skipped(self):
        testcase_skip = getattr(self.__class__, "__unittest_skip__", False)
        testmethod = getattr(self, self._testMethodName)
        testmethod_skip = getattr(testmethod, "__unittest_skip__", False)
        return testcase_skip or testmethod_skip

    def skip_reason(self):
        testMethod = getattr(self, self._testMethodName)
        skip_why = (getattr(self.__class__, '__unittest_skip_why__', '')
                    or getattr(testMethod, '__unittest_skip_why__', ''))
        return skip_why

    def is_expected_to_fail(self):
        method = getattr(self, self._testMethodName)
        expecting_failure_method = getattr(method, "__unittest_expecting_failure__", False)
        expecting_failure_class = getattr(self, "__unittest_expecting_failure__", False)
        return expecting_failure_class or expecting_failure_method

    def expecting_failure_reason(self):
        testMethod = getattr(self, self._testMethodName)
        return (getattr(self.__class__, '__expecting_failure_reason__', '')
                or getattr(testMethod, '__expecting_failure_reason__', ''))

    def _feedErrorsToResult(self, result, errors):
        for test, exc_info in errors:
            if isinstance(test, _SubTest):
                result.addSubTest(test.test_case, test, exc_info)
            elif exc_info is not None:
                result.addFailure(test, exc_info)

    @contextlib.contextmanager
    def subTest(self, msg=_subtest_msg_sentinel, **params):
        """Return a context manager that will return the enclosed block
        of code in a subtest identified by the optional message and
        keyword parameters.  A failure in the subtest marks the test
        case as failed but resumes execution at the end of the enclosed
        block, allowing further test code to be executed.
        """

        subtest_param= next(iter(params.items()))
        self.subtest_iteration_count = self.subtest_iteration_count+1
        self.subtest_file_name = f'{subtest_param[0]}'+'_'+f'{subtest_param[1]}'
        if re.search(r'[\\/:"*?<>|]+',self.subtest_file_name):
            self.Log('warning','Subtest params not suitable for snapshot filename.Using default convention.')
            self.subtest_file_name= f'iteration_{self.subtest_iteration_count }'
        params_text = ', '.join([f'{key}={value}' for key, value in params.items()])
        log_message = f'Starting sub-test with parameters: {params_text}'
        self.Log('info', colorize(log_message, HighlightColor()))
        if self._outcome is None: # or not self._outcome.result_supports_subtests:
            yield
            return
        parent = self._subtest
        if parent is None:
            params_map = collections.ChainMap(params)
        else:
            params_map = parent.params.new_child(params)
        self._subtest = _SubTest(self, msg, params_map)
        try:
            with self._outcome.testPartExecutor(self._subtest, isTest=True):
                yield
            self.take_post_test_snapshot()
            self.is_snaphot_taken = True
            if not self._outcome.success:
                result = self._outcome.result
                if result is not None and result.failfast:
                    raise _ShouldStop
            elif self._outcome.expected_failure:
                # If the test is expecting a failure, we really want to
                # stop now and register the expected failure.
                raise _ShouldStop
        finally:
            self._subtest = parent

    def shortDescription(self):
        doc = getattr(self, self._testMethodName).__doc__
        return doc and doc.split("\n")[0].strip() or None

    def check_almost_equal(self, observed, expected, delta, log_type='error'):
        if abs(observed - expected) > delta:
            self.Log(log_type, f'CheckAlmostEqual: '
                              f'Observed: {observed} Expected: {expected} Delta: {delta}')
            return False
        return True

    def take_post_test_snapshot(self):
        if self._is_snapshot_active:
            self.take_snapshot_if_enabled()
            if self._is_process_snapshot:
                self.process_snapshots()

    @contextlib.contextmanager
    def processSnapshot(self):
        '''Context Manager for Processing Snapshot

       Deactive simulator by setting a flag in the simulator
       model before test execution and reactive at the end. When the
       simulator is deactivated, test does not have a simulated
       result to check against, including end_status.

        Example:
            def test():
                with processSnapshot():
                    execute_test()
        '''
        try:
            self._is_process_snapshot = True
            yield
        finally:
            pass

    @contextlib.contextmanager
    def skipSnapshot(self):
        '''Context manager for Skipping Snapshot

        During TearDown, the snapshot_active flag will be check and
        skip when set to True. If it's not skipped, then the global
        flag HBICC_SNAP_SHOT in configs.py will evaluated.

        Example:
            def test():
                with skipSnapshot():
                    execute_test()
        '''
        try:
            self._is_snapshot_active = False
            yield
        finally:
            pass

    @contextlib.contextmanager
    def deactivateSimulator(self):
        '''Context manager for Deactivating Simulator

        Deactive simulator by setting a flag in the simulator model
        before test execution and reactive at the end. When the simulator
        is deactivated, test does not have a simulated result to check against, including
        end_status

        Example:
            def test():
                with deactivateSimulator():
                    execute_test()
        '''
        try:
            self.disable_simulator()
            yield
        finally:
            self.enable_simulator()

    def disable_simulator(self):
        pass

    def enable_simulator(self):
        pass

    def process_snapshots(self):
        pass

    def take_snapshot_if_enabled(self):
        pass


class _SubTest(TestCase):

    def __init__(self, test_case, message, params):
        super().__init__()
        self._message = message
        self.test_case = test_case
        self.params = params

    def runTest(self):
        raise NotImplementedError("subtests cannot be run directly")

    def _subDescription(self):
        parts = []
        if self._message is not _subtest_msg_sentinel:
            parts.append("[{}]".format(self._message))
        if self.params:
            params_desc = ', '.join(
                "{}={!r}".format(k, v)
                for (k, v) in sorted(self.params.items()))
            parts.append("({})".format(params_desc))
        return " ".join(parts) or '(<subtest>)'

    def id(self):
        return "{} {}".format(self.test_case.id(), self._subDescription())

    def shortDescription(self):
        return self.test_case.shortDescription()

    def __str__(self):
        return "{} {}".format(self.test_case, self._subDescription())


def strclass(cls):
    return "%s.%s" % (cls.__module__, cls.__qualname__)


class _Outcome(Object):
    def __init__(self, result=None):
        self.expecting_failure = False
        self.result = result
        self.result_supports_subtests = hasattr(result, "addSubTest")
        self.success = True
        self.skipped = []
        self.expected_failure = None
        self.errors = []

    @contextlib.contextmanager
    def testPartExecutor(self, test_case, isTest=False):
        old_success = self.success
        self.success = True
        try:
            yield
        except KeyboardInterrupt:
            raise
        except SkipTest as e:
            self.success = False
            self.skipped.append((test_case, str(e)))
            self.Log('warning', f'test skipped due to SkipTest exception: {e}')
        except _ShouldStop:
            pass
        except Exception as e:
            exc_info = sys.exc_info()
            if self.expecting_failure:
                self.expected_failure = exc_info
            else:
                self.success = False
                self.errors.append((test_case, exc_info))
            # explicitly break a reference cycle:
            # exc_info -> frame -> exc_info
            # traceback.print_exception(*exc_info)
            self.Log('error', f'{type(e)}: {e}')
            for line in traceback.format_exception(*exc_info):
                for part in line.split('\n'):
                    if part:
                        self.Log('warning', part)
        else:
            if self.result_supports_subtests and self.success:
                self.errors.append((test_case, None))
        finally:
            self.success = self.success and old_success


class SkipTest(Exception):
    """
    Raise this exception in a test to skip it.

    Usually you can use TestCase.skipTest() or one of the skipping decorators
    instead of raising this directly.
    """


class _ShouldStop(Exception):
    """
    The test should stop.
    """
