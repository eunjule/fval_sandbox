################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: assembler.py
#-------------------------------------------------------------------------------
#     Purpose: Generic two-pass assembler:
#                * First pass expands (flattens out) macro calls and captures labels.
#                  (Note that generators are not expanded).
#                     - Executed in Load* methods
#                * Second pass expands generators, resolves values (including labels),
#                  and creates encoded words.
#                     - Executed when the assembler object is generated
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/18/15
#       Group: HDMT FPGA Validation
################################################################################


import sys
import os
import uuid
import re
import copy
import random
import gzip

from ThirdParty import pyparsing

from _build.bin import fvalc

class Assembler:
    _ENCODED_WORD_SIZE       = None
    _BUILTIN_DIRECTIVES      = []
    _BUILTIN_GENERATORS      = []
    _GENERATOR_NAMESPACE     = None
    _GENERATOR_CONTEXT_CLASS = None
    _CUSTOM_LITERALS         = None
    _EXTENDED_EVAL_SYMBOLS   = False

    @staticmethod
    def _ObjectSize(assemblerObject):
        if isinstance(assemblerObject, tuple):
            return 1
        elif hasattr(assemblerObject, 'Size') and hasattr(assemblerObject, 'Generate'):
            return assemblerObject.Size()
        else:
            raise Exception('Invalid assembler object {}'.format(assemblerObject))

    # Repeat generator
    class Repeat:
        def __init__(self, assembler, count, objects):
            self.count = count
            self.assembler = assembler
            self.objects = objects
        def Size(self):
            size = 0
            for assemblerObject in self.objects:
                size = size + self.assembler._ObjectSize(assemblerObject)
            return size * self.count
        def Generate(self, array, offset):
            words = 0
            for i in range(self.count):
                for assemblerObject in self.objects:
                    count = self.assembler._Assemble(array, offset + words * self.assembler._ENCODED_WORD_SIZE, assemblerObject)
                    words = words + count
            return words

    def __init__(self, startAddress = 0):
        self.startaddr = startAddress
        self.objects = []
        self.sizeStack = [0]
        self.labels = {}
        self.evalSymbols = {}
        self.evalSymbols['int32'] = self._int32
        self.evalSymbols['int64'] = self._int64
        self.evalSymbols['rand'] = self._rand
        self.macros = copy.deepcopy(self.classMacros)
        self.symbols = copy.deepcopy(self.classSymbols)
        if self._EXTENDED_EVAL_SYMBOLS:
            for key, value in self.symbols.items():
                self.evalSymbols[key] = value
        self.contexts = []
        self.cachedObj = None
        self.vars = {}

    def __del__(self):
        if self.cachedObj is not None:
            # Cached blob could be pretty big. Explicit del = don't want to take chances here
            del self.cachedObj

    def AddLabel(self, name):
        if name in self.vars:
            raise Exception('Variable exists already with that name \'{}\''.format(name))
        if name in self.symbols:
            raise Exception('Symbol exists already with that name \'{}\''.format(name))
        if name in self.labels:
            raise Exception('Duplicate label name \'{}\''.format(name))
        nextAddress = self.sizeStack[-1]
        self.labels[name] = self.startaddr + nextAddress
        self.evalSymbols[name] = self.labels[name]

    def SetVar(self, name, value):
        if name in self.symbols:
            raise Exception('Symbol exists already with that name \'{}\''.format(name))
        if name in self.labels:
            raise Exception('Label exists already with that name \'{}\''.format(name))
        self.vars[name] = value
        if self._EXTENDED_EVAL_SYMBOLS:
            self.evalSymbols[name] = self.labels[name]

    @staticmethod
    def _ArgListToArgsKwargs(argList):
        # Build list of args and kwargs
        args   = []
        kwargs = {}
        allargs = []
        lastKwarg = None
        for i, arg in enumerate(argList):
            if isinstance(arg, list):
                kwargs[arg[0]] = arg[1]
                lastKwarg = arg[0]
                allargs.append(arg[0])
            else:
                if lastKwarg is not None:
                    raise Exception('Non-keyword arg \'{}\' after keyword arg \'{}\''.format(arg, lastKwarg))
                args.append(arg)
                allargs.append(arg)
        return (args, kwargs, allargs)

    @staticmethod
    def _CreateMacro(name, argList, statements):
        # NOTE(rodny): This function takes advantage of closures
        #              See this: https://www.google.com/webhp?&ie=UTF-8#q=closures+in+python

        positionalArgs, defaultArgs, allArgs = Assembler._ArgListToArgsKwargs(argList)
        macroStatements = statements
        
        # Perform substitutions at the word boundary
        def _SubWords(values, s):
            result = s
            for search, replace in values.items():
                result = re.sub(r'\b{}\b'.format(search), replace, result)
            return result

        # Every time a new macro is "called" / created a new instance of this function will be returned
        def Macro(*args, **kwargs):
            argumentValues = {}
            # Initialize argumentValues based on positionalArgs and defaultArgs
            for i, arg in enumerate(positionalArgs):
                argumentValues[arg] = None
            # Set default argumentValues from defaultArgs
            for arg, value in defaultArgs.items():
                argumentValues[arg] = value
            # Check for invalid arguments
            for arg in kwargs:
                if arg not in allArgs:
                    raise Exception('Invalid argument \'{}\''.format(arg))
            # Now let's see what we actually got for this instance
            for i, arg in enumerate(allArgs):
                argSet = False
                if arg in kwargs:
                    argumentValues[arg] = kwargs[arg]  # Valid key/word argument
                    argSet = True
                if i < len(args):
                    if argSet:
                        raise Exception('Value already set for argument \'{}\''.format(arg))
                    argumentValues[arg] = args[i]  # Valid positional argument
                    argSet = True
                if arg not in argumentValues:
                    raise Exception('Value not set for argument \'{}\''.format(arg))
            
            def ResolveArguments(statements):
                # Flatten out the macro and resolve arguments (perform argument value translation)
                newStatements = []
                labels = {}
                for statement in statements:
                    if len(statement) == 1:
                        # Label only
                        label = statement[0]
                        labels[label] = '{}_{}_{}'.format(name, uuid.uuid4(), label)
                        label = labels[label]
                        newStatements.append([label])
                    else:
                        if statement[0] == '%macro':
                            raise Exception('Nested macros are not allowed!')
                        elif statement[0] == '%var':
                            var_name = statement[1]
                            value = self.Resolve(statement[2], False)
                            self.SerVar(var_name, value)
                        elif statement[0] == '%repeat':
                            repeatCount = statement[1]
                            repeatStatements = statement[2:]
                            newRepeatCount = _SubWords(argumentValues, repeatCount)
                            newRepeatStatements = ResolveArguments(repeatStatements)
                            newStatement = ['%repeat', newRepeatCount]
                            newStatement.extend(newRepeatStatements)
                            newStatements.append(newStatement)
                        else:
                            # Macro or Directive (at this level, they are treated the same)
                            label     = statement[0]
                            if label != '':
                                labels[label] = '{}_{}_{}'.format(name, uuid.uuid4(), label)
                                label = labels[label]
                            directive = statement[1]
                            argList   = statement[2:]
                            args, kwargs, allargs = Assembler._ArgListToArgsKwargs(argList)
                            newStatement = [label, directive]
                            for value in args:
                                newStatement.append(_SubWords(argumentValues, value))
                            for key, value in kwargs.items():
                                newStatement.append([key, _SubWords(argumentValues, value)])
                            newStatements.append(newStatement)

                return newStatements

            return ResolveArguments(macroStatements)
        return Macro

    def CreateMacro(self, name, argList, statements):
        if name in self.macros:
            raise Exception('Macro \'{}\' is already defined!'.format(name))
        return self._CreateMacro(name, argList, statements)

    @staticmethod
    def _int32(x):
        return x & 0xFFFFFFFF

    @staticmethod
    def _int64(x):
        return x & 0xFFFFFFFFFFFFFFFF

    @staticmethod
    def _rand(bits):
        return random.randint(0, (1 << bits) - 1)

    @staticmethod
    def _num(s):
        try:
            return int(s, 0)
        except ValueError:
            return float(s)

    def _Assemble(self, array, offset, assemblerObject):
        if isinstance(assemblerObject, tuple):
            methodName, args, kwargs = assemblerObject
            # Return a packed word
            getattr(self, methodName)(array, offset, *args, **kwargs)
            return 1
        elif hasattr(assemblerObject, 'Size') and hasattr(assemblerObject, 'Generate'):
            return assemblerObject.Generate(array, offset)
        else:
            raise Exception('Invalid assembler object {}'.format(assemblerObject))

    def Generate(self):
        wordCount = self.sizeStack[-1]
        totalSize = wordCount * self._ENCODED_WORD_SIZE
        data = bytearray(totalSize)
        self.curaddr = self.startaddr
        words = 0
        for assemblerObject in self.objects:
            count = self._Assemble(data, self._ENCODED_WORD_SIZE * words, assemblerObject)
            self.curaddr = self.curaddr + count
            words = words + count
        del self.curaddr
        self.cachedObj = bytes(data)
        return self.cachedObj

    def CachedObj(self, forceGen = False):
        if forceGen or (self.cachedObj is None):  # Only re-compile the blob if we have not compiled it before or if forceGen is True
            self.Generate()
        return self.cachedObj

    def SaveObj(self, filename, forceGen = False):
        if filename.lower().endswith('.gz'):
            fout = gzip.open(filename, 'wb')
            fout.write(self.CachedObj(forceGen))
            fout.close()
        else:
            fout = open(filename, 'wb')
            fout.write(self.CachedObj(forceGen))
            fout.close()

    def SaveSymbols(self, filename):
        fout = open(filename, 'w')
        for key, value in self.symbols.items():
            fout.write('{} = {}\n'.format(key, value))
        for key, value in self.labels.items():
            fout.write('{} = {}\n'.format(key, value))
        for key, value in self.vars.items():
            fout.write('{} = {}\n'.format(key, value))
        fout.close()

    @staticmethod
    def _CreateKwarg(s, loc, toks):
        toks[0] = [toks[0][0], toks[0][2]]

    @staticmethod
    def _CreateEvalExpr(s, loc, toks):
        toks[0] = '[' + toks[0][0] + ']'

    @staticmethod
    def _CreateQuotedEvalExpr(s, loc, toks):
        toks[0] = '`' + toks[0] + '`'

    @staticmethod
    def _CreateRandomVarExpr(s, loc, toks):
        toks[0] = '{' + toks[0][0] + '}'

    COMMENT_REGEX = re.compile('#.*')

    @staticmethod
    def _ParseString(cls, s):
        # Exclude newlines from the default whitespace characters.
        # We need to deal with them manually.
        prevws = copy.deepcopy(pyparsing.ParserElement.DEFAULT_WHITE_CHARS)
        ws = ' \t'
        pyparsing.ParserElement.setDefaultWhitespaceChars(ws)

        EOL                = pyparsing.Suppress(pyparsing.LineEnd())
        SOL                = pyparsing.Suppress(pyparsing.LineStart())
        COLON              = pyparsing.Suppress(pyparsing.Literal(':'))
        SEMICOLON          = pyparsing.Suppress(pyparsing.Literal(';'))
        COMMA              = pyparsing.Suppress(pyparsing.Literal(','))
        LPAREN             = pyparsing.Suppress(pyparsing.Literal('('))
        RPAREN             = pyparsing.Suppress(pyparsing.Literal(')'))
        ASSIGNMENT         = pyparsing.Literal('=')
        HASHMARK           = pyparsing.Literal('#')
    
        MACRO              = pyparsing.Keyword('%macro')
        VAR                = pyparsing.Keyword('%var')
        REPEAT             = pyparsing.Keyword('%repeat')
        END                = pyparsing.Suppress(pyparsing.Keyword('%end'))

        identifier         = pyparsing.Word(pyparsing.alphas, pyparsing.alphanums + '_.')
        integer            = pyparsing.Word(pyparsing.nums)
        negativeInteger    = pyparsing.Combine(pyparsing.Optional('-') + pyparsing.Word(pyparsing.nums))
        label              = identifier + COLON
        directive          = identifier
        numericLiterals    = pyparsing.Combine('0b' + pyparsing.Word('01')) | \
                             pyparsing.Combine('0o' + pyparsing.Word('01234567')) | \
                             pyparsing.Combine('0x' + pyparsing.Word('0123456789ABCDEFabcdef')) | \
                             integer | negativeInteger
        floatLiteral       = pyparsing.Combine(pyparsing.Word('0123456789') + '.' + pyparsing.Word('0123456789'))
        negativeFloatLiteral = pyparsing.Combine(pyparsing.Optional('-') + pyparsing.Word('0123456789') + '.' + pyparsing.Word('0123456789'))

        protoFunctionExpr  = \
            pyparsing.Combine('eval' + pyparsing.nestedExpr('[', ']').setParseAction(Assembler._CreateEvalExpr)) | \
            pyparsing.Combine(pyparsing.QuotedString('`').setParseAction(Assembler._CreateQuotedEvalExpr))

        randomVarExpr      = pyparsing.Combine(pyparsing.nestedExpr('{', '}').setParseAction(Assembler._CreateRandomVarExpr))

        value = None
        if cls._CUSTOM_LITERALS is None:
            value          = negativeFloatLiteral | floatLiteral | numericLiterals | protoFunctionExpr | randomVarExpr | identifier
        else:
            value          = cls._CUSTOM_LITERALS | negativeFloatLiteral | floatLiteral | numericLiterals | protoFunctionExpr | randomVarExpr | identifier

        # Positional arguments
        arg                = value
        # KeyWord ARGumentS
        kwarg              = pyparsing.Group(identifier + ASSIGNMENT + value).setParseAction(Assembler._CreateKwarg)
        # Generic argument = arg | kwarg
        # We should support:
        #   - a
        #   - a,
        #   - a, b, c
        #   - a, b, c,
        argument           = kwarg | arg
        argumentList       = pyparsing.Optional(argument + pyparsing.ZeroOrMore(COMMA + argument) + pyparsing.Optional(COMMA))
        statement          = pyparsing.Group(label + EOL) | \
                pyparsing.Group(pyparsing.Optional(label, default = '') + directive + pyparsing.Optional(argumentList) + EOL) | \
                pyparsing.Group(VAR + identifier + pyparsing.Suppress(ASSIGNMENT) + value + EOL)

        # Forward declaration is used to support repeat blocks inside macros
        macroStatements    = pyparsing.Forward()

        # Forward declaration is used to support nested repeat blocks
        repeatBlock        = pyparsing.Forward()
        repeatCount        = identifier | integer
        repeatHeader       = REPEAT + repeatCount + EOL
        repeatBody         = pyparsing.ZeroOrMore(statement | repeatBlock)
        repeatFooter       = END + EOL
        repeatBlock        << pyparsing.Group(repeatHeader + repeatBody + repeatFooter)

        macroStatements    << pyparsing.ZeroOrMore(statement | repeatBlock)

        macroHeader        = MACRO + identifier + pyparsing.Group(pyparsing.Optional(LPAREN + argumentList + RPAREN)) + EOL
        macroBody          = macroStatements
        macroFooter        = END + EOL
        macroBlock         = pyparsing.Group(macroHeader + macroBody + macroFooter)

        parser             = pyparsing.ZeroOrMore(statement | macroBlock | repeatBlock) + pyparsing.StringEnd()
    
        ### FIXME: This is a workaround for pyparsing not wanting to ignore comments
        stripped = ''
        for line in s.split('\n'):
            line = line.strip(' \t\n')
            newline = Assembler.COMMENT_REGEX.sub('', line)
            if len(newline) > 0:
                stripped = stripped + newline + '\n'
        ###
        tree = parser.parseString(stripped).asList()

        # Restore white space chars or BitStruct will get really unhappy and messed up
        pyparsing.ParserElement.setDefaultWhitespaceChars(prevws)

        #import json
        #print(json.dumps(tree, indent = 4, separators = (',', ': ')))
        return tree

    def _PushFrame(self):
        self.sizeStack.append(self.sizeStack[-1])

    def _PopFrame(self):
        return self.sizeStack.pop()

    def _UpdateSize(self, assemblerObject):
        self.sizeStack[-1] = self.sizeStack[-1] + self._ObjectSize(assemblerObject)

    def CreateGenerator(self, namespace, directive, args, kwargs, builtin = False):
        if hasattr(namespace, directive):
            self._PushFrame()
            # Resolve generator arguments
            resolvedArgs = [self.Resolve(x, False) for x in args]
            resolvedKwargs = {}
            for key, value in kwargs.items():
                resolvedKwargs[key] = self.Resolve(value, False)
            # Create assembler context
            context = self._GENERATOR_CONTEXT_MAKER()
            for name, value in self.vars.items():
                context.SetVar(name, value)
            # Keep a reference to the context so that it does not get garbage-collected
            self.contexts.append(context)
            # Create a generator instance
            if not builtin:
                return (self._PopFrame(), getattr(namespace, directive)(context, *resolvedArgs, **resolvedKwargs))
            else:
                return (self._PopFrame(), getattr(namespace, directive)(self, context, *resolvedArgs, **resolvedKwargs))
        else:
            raise Exception('Invalid generator \'{}\''.format(directive))

    def CreateRepeat(self, count, statements):
        self._PushFrame()
        repeatObjects = []
        for statement in statements:
            if len(statement) == 1:
                # Label only
                label = statement[0]
                raise Exception('No labels are allowed inside repeat blocks but label \'{}\' found'.format(label))
            elif statement[0] == '%var':
                name = statement[1]
                value = self.Resolve(statement[2], False)
                self.SetVar(name, value)
            elif statement[0] == '%repeat':  # Nested repeat
                repeatCount      = self.Resolve(statement[1], False)
                repeatStatements = statement[2:]
                size, repeat = self.CreateRepeat(repeatCount, repeatStatements)
                repeatObjects.append(repeat)
            else:
                label     = statement[0]
                directive = statement[1]
                argList   = statement[2:]
                if label != '':
                    raise Exception('No labels are allowed inside repeat blocks but label \'{}\' found'.format(label))
                args, kwargs, allargs = Assembler._ArgListToArgsKwargs(argList)
                objects = self._DirectiveToObjects(directive, args, kwargs)
                repeatObjects.extend(objects)
        return (self._PopFrame(), Assembler.Repeat(self, count, repeatObjects))

    def _DirectiveToObjects(self, directive, args, kwargs):
        objects = []
        if directive in self._BUILTIN_DIRECTIVES:
            statement = (directive, args, kwargs)
            objects.append(statement)
            self._UpdateSize(statement)
        elif directive in self.macros:
            # To flatten out macro, call the macro function (which returns simple statements)
            statements = self.macros[directive](*args, **kwargs)
            # Load the statements returned by the macro
            size, macroObjects = self._ExecuteTree(statements)
            for assemblerObject in macroObjects:
                objects.append(assemblerObject)
                self._UpdateSize(assemblerObject)
        elif directive in self._BUILTIN_GENERATORS:
            size, generator = self.CreateGenerator(self, directive, args, kwargs, True)
            # Add the generator instance to the intermediate objects list
            objects.append(generator)
            self._UpdateSize(generator)
        elif hasattr(self._GENERATOR_NAMESPACE, directive):
            size, generator = self.CreateGenerator(self._GENERATOR_NAMESPACE, directive, args, kwargs)
            # Add the generator instance to the intermediate objects list
            objects.append(generator)
            self._UpdateSize(generator)
        else:
            raise Exception('Unknown directive \'{}\''.format(directive))
        return objects

    def _ExecuteTree(self, tree):
        self._PushFrame()
        objects = []
        for statement in tree:
            if len(statement) == 1:
                # Label only
                label = statement[0]
                self.AddLabel(label)
            elif statement[0] == '%macro':
                name            = statement[1]
                argList         = statement[2]
                macroStatements = statement[3:]
                self.macros[name] = self.CreateMacro(name, argList, macroStatements)
            elif statement[0] == '%var':
                name = statement[1]
                value = self.Resolve(statement[2], False)
                self.SetVar(name, value)
            elif statement[0] == '%repeat':
                repeatCount      = self.Resolve(statement[1], False)
                repeatStatements = statement[2:]
                size, repeat = self.CreateRepeat(repeatCount, repeatStatements)
                objects.append(repeat)
                self._UpdateSize(repeat)
            else:
                # Macro or Directive (at this level, they are treated the same)
                label     = statement[0]
                directive = statement[1]
                argList   = statement[2:]
                if label != '':
                    self.AddLabel(label)
                args, kwargs, allargs = self._ArgListToArgsKwargs(argList)
                subObjects = self._DirectiveToObjects(directive, args, kwargs)
                objects.extend(subObjects)
        return (self._PopFrame(), objects)

    def LoadString(self, s):
        tree = self._ParseString(self.__class__, s)
        size, objects = self._ExecuteTree(tree)
        for assemblerObject in objects:
            self.objects.append(assemblerObject)
        self.sizeStack[-1] = size
        # Loading new source code clears the cache
        self.cachedObj = None

    def Load(self, filename):
        self.LoadString(open(filename, 'r').read())


def LoadMacros(filename, assemblerClass):
    macros = {}
    # Search in sys.path  for the given filename
    for path in sys.path:
        fn = os.path.join(path, filename)
        if os.path.isfile(fn):  # If fn exists
            filename = fn  # If found, update the path
            break
    with open(filename, 'r') as f:
        s = f.read()
    tree = assemblerClass._ParseString(assemblerClass, s)
    for statement in tree:
        if len(statement) == 1:
            # Label only
            label = statement[0]
            raise Exception('Labels are not supported in LoadMacros() but label \'{}\' found'.format(label))
        elif statement[0] == '%macro':
            name            = statement[1]
            argList         = statement[2]
            macroStatements = statement[3:]
            macros[name] = assemblerClass._CreateMacro(name, argList, macroStatements)
        elif statement[0] == '%var':
            raise Exception('Variables are not supported in LoadMacros() but \'{}\' found'.format(statement))
        elif statement[0] == '%repeat':
            raise Exception('Repeat blocks are not supported in LoadMacros() but \'{}\' found'.format(statement))
        else:
            raise Exception('Directives are not supported in LoadMacros() but \'{}\' found'.format(statement))
    return macros

