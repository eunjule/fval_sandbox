# Copyright 2019 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import importlib
import fnmatch
import glob
import os
import sys

from .testcase import TestCase
from .testsuite import TestSuite


default_test_prefixes = ('test', 'Mini', 'Directed', 'Random', 'Thr', 'Stress')
default_test_postfix = 'Test'

def discover_tests(directory, test_name_pattern):
    add_directory_to_path(directory)
    suiteList = []
    for test_name_pattern in test_name_pattern:
        suiteList.extend(get_tests_from_directory(directory, test_name_pattern))
    return TestSuite(suite_to_test_list(suiteList))

def suite_to_test_list(suite):
    test_list = []
    for item in suite:
        if type(item) is TestSuite:
            test_list.extend(suite_to_test_list(item))
        else:
            test_list.append(item)
    return test_list

def add_directory_to_path(directory):
    if directory not in sys.path:
         sys.path = [directory] + sys.path

def get_tests_from_directory(directory, test_name_pattern):
    test_files = get_test_files(directory, test_name_pattern)
    return [get_tests_from_module(x, test_name_pattern) for x in test_files]

def get_test_files(directory, test_name_pattern):
    file_pattern = get_filename_pattern(test_name_pattern)
    long_file_names = glob.glob(os.path.join(directory, file_pattern))
    short_file_names = [os.path.basename(x).split('.')[0] for x in long_file_names]
    return short_file_names

def get_tests_from_module(module_name, test_name_pattern):
    tests = []
    for obj in get_objects_matching_class_name(module_name, test_name_pattern):
        tests.extend(get_tests_from_object(obj, get_method_name_pattern(test_name_pattern)))
    return TestSuite(tests)

def get_objects_matching_class_name(module_name, test_name_pattern):
    module = importlib.import_module(module_name)
    return [getattr(module, x) for x in dir(module) if class_name_matches(x, test_name_pattern)]

def class_name_matches(name, test_name_pattern):
    return fnmatch.fnmatch(name, get_class_name_pattern(test_name_pattern))

def get_filename_pattern(pattern):
    return pattern.split('.')[0] + '.py'

def get_class_name_pattern(pattern):
    return pattern.split('.')[1]

def get_method_name_pattern(pattern):
    return pattern.split('.')[2]

def get_tests_from_object(obj, prefix):
    if is_test_case(obj):
        return get_tests_from_testcase(obj, prefix)
    else:
        return []

def is_test_case(obj):
    return isinstance(obj, type) and issubclass(obj, TestCase)
    
def get_tests_from_testcase(testCaseClass, prefix):
    testCaseNames = getTestCaseNames(testCaseClass, prefix)
    if not testCaseNames and hasattr(testCaseClass, 'runTest'):
        testCaseNames = ['runTest']
    tests = [testCaseClass(x) for x in testCaseNames]
    return TestSuite(tests)

def getTestCaseNames(testCaseClass, prefix):
    return sorted([x for x in dir(testCaseClass) if isTestMethod(x, testCaseClass, prefix)])
    
def isTestMethod(attrname, testCaseClass, prefix):
    if callable(getattr(testCaseClass, attrname)):
        return method_name_matches(attrname, prefix)
    else:
        return False

def method_name_matches(attrname, prefix):
    if prefix == '*':
        return attrname.startswith(default_test_prefixes) and attrname.endswith(default_test_postfix)
    else:
        return fnmatch.fnmatch(attrname, prefix)
    
    

