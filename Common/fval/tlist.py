################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: list.py
# -------------------------------------------------------------------------------
#     Purpose: Test Class for test list
# -------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/18/15
#       Group: HDMT FPGA Validation
################################################################################

import os
import unittest

class TestList:
    def __init__(self, filename):
        self.file = open(filename)

    def GetTestList(self):
        testList = []
        for line in self.file:
            testName = line.rstrip()
            if len(testName) > 0:
                testList.append(testName)
        return testList

    def GetOrderedTestList(self):
        testList = []
        seedList = []

        for line in self.file:
            testline = line.rstrip()
            try:
                testName = testline.split(',')[0]
                testSeed = testline.split(',')[1]
            except IndexError:
                raise Exception(
                    'Error Parsing Ordered Test List File: Format'\
                    ' of ordered test list file must be: testname,seed')

            if len(testName) > 0:
                testList.append(testName)
                seedList.append(int(testSeed))
        print('seedlist: ', seedList)
        return testList, seedList


class ResultList:
    def __init__(self, outputPath):
        self.outputPath = outputPath

    def GetAllRunTests(self, suite):
        allRunTests = []
        for test in suite:
            if unittest.suite._isnotsuite(test):
                allRunTests.append(test)
            else:
                allRunTests.extend(self.GetAllRunTests(test))
        return allRunTests

    def GenerateResultList(self, result, suite):
        all_run_tests = self.GetAllRunTests(suite)
        
        with open(self.file_path('allTestList.txt'), 'w') as f:
            for test in all_run_tests:
                print(test.Name(), file=f)

        with open(self.file_path('passList.txt'), 'w') as f:
            for test in result.success:
                print(test.Name(), file=f)

        with open(self.file_path('failList.txt'), 'w') as f:
            for test, traceback in result.failures:
                print(test.Name(), file=f)

        with open(self.file_path('failWithSeedsList.txt'), 'w') as f:
            for test, traceback in result.failures:
                print(f'{test.Name()}, {test.seed}', file=f)

        with open(self.file_path('skipList.txt'), 'w') as f:
            for test, reason in result.skipped:
                print(f'{test.Name()}, Reason: {reason}\n', file=f)

        with open(self.file_path('expectedFailList.txt'), 'w') as f:
            for test, traceback in result.expected_failures:
                print(test.Name(), file=f)
        
        with open(self.file_path('unexpectedPassList.txt'), 'w') as f:
            for test in result.unexpected_successes:
                print(test.Name(), file=f)

        with open(self.file_path('orderedTestListWithSeed.txt'), 'w') as f:
            for test_seed in result.ordered_tests:
                print(f'{test_seed["TestName"]}, {test_seed["Seed"]}', file=f)
    
    def file_path(self, filename):
        return os.path.join(self.outputPath, filename)

