def process_snapshot(decorated):
    '''Deactivate Simulator Decorator

    Deactive simulator by setting a flag in the simulator model before test execution and reactive at the end.
    When the simulator is deactivated, test does not have a simulated result to check against, including
    end_status.'''

    def wrapper(test, *args, **kwargs):
        with test.processSnapshot():
            decorated(test, *args, **kwargs)

    return wrapper


def skip_snapshot(decorated):
    '''Skips Snapshot Decorator

    During TearDown, the snapshot_active flag will be check and skip when set to True.
    If it's not skipped, then the global flag HBICC_SNAP_SHOT in configs.py will evaluated.
    '''

    def wrapper(test, *args, **kwargs):
        with test.skipSnapshot():
            decorated(test, *args, **kwargs)

    return wrapper


def deactivate_simulator(decorated):
    '''Deactivate Simulator Decorator

    Deactive simulator by setting a flag in the simulator model before test execution and reactive at the end.
    When the simulator is deactivated, test does not have a simulated result to check against, including
    end_status.'''

    def wrapper(test, *args, **kwargs):
        with test.deactivateSimulator():
            decorated(test, *args, **kwargs)

    return wrapper


def skip(reason):
    def decorator(test_item):
        test_item.__unittest_skip__ = True
        test_item.__unittest_skip_why__ = reason
        return test_item

    return decorator


def expected_failure(test_item_or_reason):
    def decorator(test_item):
        test_item.__unittest_expecting_failure__ = True
        test_item.__expecting_failure_reason__ = test_item_or_reason
        return test_item

    if type(test_item_or_reason) is str:
        return decorator
    else:
        return decorator(test_item_or_reason)
