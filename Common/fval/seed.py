################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: seed.py
#-------------------------------------------------------------------------------
#     Purpose: Test Class for MasterSeed and TestSeed
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/06/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import sys
import time

class Seed:
    def __init__(self, seedNum = -1):
        if seedNum >= 0:
            self.seed = seedNum
        else:
            self.seed = int(time.time() * 1000)
        random.seed(self.seed)
        
    def GetNextInt(self, min, max):
        return random.randint(min,max)

class TestSeed(Seed):
    # seedNum for TestSeed cannot be random
    def __init__(self, seedNum):
        super().__init__(seedNum)
      
class MasterSeed(Seed):
    def __init__(self, seedLen, seedNum = -1):
        super().__init__(seedNum)
        self.seedLen = seedLen
        self.testSeeds = []
        self.testIndex = 0
        for i in range(self.seedLen):
            self.testSeeds.append(self.GetNextInt(0,sys.maxsize))
    
    def GetTestSeed(self):
        if self.testIndex < len(self.testSeeds):
            nextTestSeed = TestSeed(self.testSeeds[self.testIndex])
            self.testIndex += 1
            return nextTestSeed
        else:
            return -1
