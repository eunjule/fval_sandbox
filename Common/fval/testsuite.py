# Copyright 2020 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.


class TestSuite():
    def __init__(self, tests=()):
        self._tests = []
        self._removed_tests = 0
        self.addTests(tests)

    def __iter__(self):
        return iter(self._tests)

    def countTestCases(self):
        cases = self._removed_tests
        for test in self:
            if test:
                cases += test.countTestCases()
        return cases

    def addTest(self, test):
        self._tests.append(test)

    def addTests(self, tests):
        for test in tests:
            self.addTest(test)
