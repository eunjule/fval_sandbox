################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: config.py
#-------------------------------------------------------------------------------
#     Purpose: Configuration file with *all* FPGA image paths and flags
#              to control image load/reflash operation.
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/21/15
#       Group: HDMT FPGA Validation
################################################################################

HBICC_IN_HDMT = False
HBICC_IN_HDMT_FAB = 'B'
HBICC_SNAP_SHOT = False
HBICC_DUMP_ILA = False
HBICC_DUMP_REFERENCE_ILA = False
PSDB_0 = True
PSDB_1 = True
DPS_DEVICE_TYPES = ['Hddps', 'HvdpsCombo']
HBI_DEVICE_TYPES = ['Hbicc', 'Hbirctc', 'Hbidps', 'Hbidtb']
HDMT_DEVICE_TYPES = ['Hpcc', 'Rc2', 'Rc3', 'Tdaubnk'] + DPS_DEVICE_TYPES
if HBICC_IN_HDMT:
    HDMT_DEVICE_TYPES.extend(HBI_DEVICE_TYPES)

RESULTS_DIR = r'results'


SKIP_NO_CAL_CARD_SLOTS = False

COMMON_INITIALIZE = True
CONFIGURABLE_AURORA_CLOCK_COMPENSATION = False

RC2_FPGA_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\RCTC2\MDUT\rctc2_rc_fpga_rev6_5_0.bin'
RC2_FPGA_LOAD = True

RC3_FPGA_IMAGE_FAB_A = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\RCTC3\Release\Fab_A\Dynamic_Load_Image\9.0.0\rctc3_top_v9.0.0.rbf'
RC3_FPGA_IMAGE_FAB_B = RC3_FPGA_IMAGE_FAB_A
RC3_FPGA_LOAD = True

# dont check that the cal base board is present
CALBASE_IGNORE = False


###################################################################################################
# TEST_DB_IMAGE = "$image_path" <= This points to the FPGA image to test for the HDDPS Daughter Card
# TEST_MB_IMAGE = "$image_path" <= This points to the FPGA image to test for the HDDPS Main Card
# Reminder: Main Card houses VLC and LC rails.  Daughter card houses HC rails.
###################################################################################################
HDDPS_MB_FPGA_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HDDPS\6.x\6.b.1\hddps_mb_fpga_rev06_0b_0001.bin'
HDDPS_MB_FPGA_LOAD = True

HDDPS_DB_FPGA_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HDDPS\6.x\6.b.0\hddps_db_fpga_rev06_0b_0000.bin'
HDDPS_DB_FPGA_LOAD = True

HDDPS_BOARD_SIM_MEM = 512 * 1024  # Default simulator memory size = 512 entries per board

HDDPS_LEGACY_TC_LOOPING_FPGAS = [0x4010001,0x4010002,0x5000000,0x5000001,0x5010000,0x5010001]
HVDPS_LEGACY_TC_LOOPING_FPGAS = [0x5000001,0xa5000001,0x5010000]
HVIL_LEGACY_TC_LOOPING_FPGAS = [0x5010000]

HDDPS_NON_DYNAMIC_POWER_LIMIT_FPGAS = []
HDDPS_NON_VM_MODE_FPGAS = [0x4010001,0x4010002,0x5000000,0x5000001,0x5010000,0x5010001]

HDDPS_NON_S2H_FPGAS = [0x4010001,0x4010002,0x4020000,0x4020001,0x4020002,0x5000000,0x5000001]
HVDPS_NON_S2H_FPGAS =  [0x5000001,0xa5000001,0x5010000]
HVDPS_NON_VM_MODE_FPGAS = [0x5000001,0xa5000001,0x5010000]
HVIL_NON_S2H_FPGAS = [0x5010000]
TDAUBNK_NON_S2H_FPGAS = []

# TDAU BANK ENV VARS ####
TDAUBNK_FPGA_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\TDAU_Bank\tdau_bank_fpga_rev5_3.bin'
TDAUBNK_FPGA_LOAD = True

TDAUBNK_ENV_INIT = 'FULL'  # Valid values: NONE, FULL

HVDPS_FPGA_LOAD = True
HVDPS_FPGA_INITIALIZE = True
HVDPS_CAL_CARD_INITIALIZE = True

HVIL_FPGA_LOAD = True
HVIL_FGPA_INITIALIZE = True


HVIL_FPGA_IMAGE = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HVIL\6.x\6.2.0\hvil_fpga_rev06_02_0000.bin"

HVDPS_FPGA_IMAGE = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HVDPS\6.x\6.12.0\hvdps_fpga_rev06_0c_0000.bin"

HBIDPS_FPGA_IMAGE_LOAD = True

HBIDPS_FPGA_IMAGE = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_dps\Release\rev_3_24_0\hbi_dps_v3.24.0.rbf"

HBIPATGEN_FPGA_IMAGE_LOAD = True
HBIPATGEN_FPGA_IMAGE_FAB_B = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_pattern_generator\Release\FAB B\patgen_top_2019-10-20_21.06.07_v5.4.1\patgen_top_v5.4.1.rbf"
HBIPATGEN_FPGA_IMAGE_FAB_C = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_pattern_generator\Release\FAB C\patgen_2021-02-25_00.23.03_v8.35.50\patgen_top_v8.35.50.rbf"
HBIPATGEN_FPGA_IMAGE_FAB_D = HBIPATGEN_FPGA_IMAGE_FAB_C
SPLITTER_FEATURE = True

HBIRINGMULTIPLIER_FPGA_IMAGE_LOAD = True
HBIRINGMULTIPLIER_FPGA_IMAGE_FAB_B = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_ring_multiplier\Release\FAB B\ring_multiplier_top_2019-10-16_15.14.57_v5.4.1\ring_multiplier_top_v5.4.1.rbf"
HBIRINGMULTIPLIER_FPGA_IMAGE_FAB_C = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_ring_multiplier\Release\FAB C\ring_multiplier_2020-10-15_07.42.46_v9.6.114\ring_multiplier_top_v9.6.114.rbf"
HBIRINGMULTIPLIER_FPGA_IMAGE_FAB_D = HBIRINGMULTIPLIER_FPGA_IMAGE_FAB_C

HBIPINMULTIPLIER_FPGA_IMAGE_LOAD = True
HBIPINMULTIPLIER_FPGA_IMAGE_0_FAB_B = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_pin_multiplier\Release\FAB B\pin_multiplier0_top_2019-09-24_14.36.36_v4.5.0\pin_multiplier0_top_v4.5.0.rbf'
HBIPINMULTIPLIER_FPGA_IMAGE_1_FAB_B = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_pin_multiplier\Release\FAB B\pin_multiplier1_top_2019-09-24_14.50.21_v4.5.0\pin_multiplier1_top_v4.5.0.rbf'

HBIPINMULTIPLIER_FPGA_IMAGE_0_FAB_C = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_pin_multiplier\Release\FAB C\pin_multiplier0_2021-03-02_16.20.59_v7.25.33\pin_multiplier0_top_v7.25.33.rbf"
HBIPINMULTIPLIER_FPGA_IMAGE_1_FAB_C = r"\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_pin_multiplier\Release\FAB C\pin_multiplier1_2021-03-02_14.39.14_v7.25.33\pin_multiplier1_top_v7.25.33.rbf"

HBIRCTC_FPGA_IMAGE_LOAD = True
HBIRCTC_FPGA_IMAGE_FAB_B = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_rc_tc\Release\Fab_B\1.3.1\hbi_rc_tc_top_v1.3.1.rbf'
HBIRCTC_FPGA_IMAGE_FAB_C = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_rc_tc\Release\Fab_C\7.0.2\hbi_rc_tc_top_v7.0.2.rbf'
HBIRCTC_FPGA_IMAGE_FAB_D = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HBI_FPGAs\hbi_rc_tc\Release\Fab_D\8.1.3\hbi_rc_tc_top_v8.1.3.rbf'

SKIP_INIT = False
SKIP_HDMT_INIT = False
SKIP_MEMORY_INIT = False
