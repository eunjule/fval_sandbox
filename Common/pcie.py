################################################################################
#  INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: pcie.py
#-------------------------------------------------------------------------------
#     Purpose: House all PCIe GUID, VIDDID and Additional parameters as outlined
#              HIL chm.
#-------------------------------------------------------------------------------
#  Created by: Edgar Rosales
#        Date: 10/10/2018
#       Group: HDMT FPGA Validation
################################################################################

ring_multiplier_guid = '4CADCCDB-DEAB-4FEB-9C18-F61C72C2A78A'
ring_multiplier_viddid = 0x8086B102

pin_multiplier_guid = '11DDEA72-7F3C-4AD2-9700-751BFFF094CD'
pin_multiplier_viddid = 0x8086B103
pin_multiplier_additional = ['#PCI(0000)#PCI(0100)#PCI(0000)', '#PCI(0000)#PCI(1200)#PCI(0000)']

pat_gen_guid = '2BD22ED2-AAF4-405E-A404-0505111EB1C4'
pat_gen_viddid = 0x8086B101

hbirctc_guid = 'A76F9F67-6552-415A-B572-7D7E923B480D'
hbirctc_viddid = 0x8086B100
