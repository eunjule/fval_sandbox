# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import importlib.machinery
import os

this_directory = os.path.dirname(os.path.realpath(__file__))
projectpaths_file = os.path.join(this_directory, '..', '..', 'Tools',
                                 'projectpaths.py')
importlib.machinery.SourceFileLoader('projectpaths',
                                     projectpaths_file).load_module()

from .bit_manipulator import compliment_bits, compliment_bytes,\
    negative_one, negative_one_bits
from .conversions import dword_to_float, float_to_dword
from .format_docstring import format_docstring
from .performance_sleep import performance_sleep
from .relative_tolerance import relative_tolerance
