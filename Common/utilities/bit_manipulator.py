# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


def compliment_bytes(num_to_convert, num_bytes):
    mask = negative_one(num_bytes)
    compliment = num_to_convert ^ mask
    return compliment


def negative_one(num_bytes):
    num_bits = num_bytes * 8
    return negative_one_bits(num_bits)


def compliment_bits(num_to_convert, num_bits):
    mask = negative_one_bits(num_bits)
    compliment = num_to_convert ^ mask
    return compliment


def negative_one_bits(num_bits):
    return int(f'1' * num_bits, 2)
