# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import platform

if platform.system() == 'Windows':
    from ThirdParty.HIL.x64.Release.hil import *
    import ThirdParty.HIL.x64.Release.hil as hil
else:
    class DummyHil:
        def hilVersionString(self):
            return 'Dummy HIL'

        class BLT:
            DeviceName = ''
            ManufactureDate = ''
            PartNumberAsBuilt = ''
            PartNumberCurrent = ''
            VendorName = ''
    hil = DummyHil()

from Common import fval

fval.Log('info', f'Using HIL version {hil.hilVersionString()}')

_monitors = {
    'dps': [],
    'hpccAc': [],
    'hpccDc': [],
    'rc': []
}

monitor = None


def attach_model(model):
    global monitor
    monitor = model


def RegisterMonitor(instrument, monitor):
    global _monitors
    if instrument not in _monitors:
        raise ValueError(f'Invalid instrument \'{instrument}\'. '
                         f'Expecting {str(_monitors.keys())}')
    if monitor not in _monitors[instrument]:
        _monitors[instrument].append(monitor)


BLT = hil.BLT


def hilVersionString():
    return hil.hilVersionString()


def pciDeviceOpen(guid, viddid, slot, additional):
    return hil.pciDeviceOpen(guid, viddid, slot, additional)


def pciDeviceClose(handle):
    hil.pciDeviceClose(handle)


def hddpsConnect(slot, subslot):
    for monitor in _monitors['dps']:
        monitor.hddpsConnect(slot, subslot)
    return hil.hddpsConnect(slot, subslot)


def hddpsDisconnect(slot, subslot):
    for monitor in _monitors['dps']:
        monitor.hddpsDisconnect(slot, subslot)
    return hil.hddpsDisconnect(slot, subslot)


def dpsBarRead(slot, subslot, bar, offset):
    data = hil.dpsBarRead(slot, subslot, bar, offset)
    for monitor in _monitors['dps']:
        monitor.hddpsBarRead(slot, subslot, bar, offset, data)
    return data


def dpsBarWrite(slot, subslot, bar, offset, data):
    for monitor in _monitors['dps']:
        monitor.hddpsBarWrite(slot, subslot, bar, offset, data)
    return hil.dpsBarWrite(slot, subslot, bar, offset, data)


def dpsDmaRead(slot, subslot, address, length):
    data = hil.dpsDmaRead(slot, subslot, address, length)
    for monitor in _monitors['dps']:
        monitor.hddpsDmaRead(slot, subslot, address, data)
    return data


def dpsDmaWrite(slot, subslot, address, data):
    for monitor in _monitors['dps']:
        monitor.hddpsDmaWrite(slot, subslot, address, data)
    return hil.dpsDmaWrite(slot, subslot, address, data)


def hpccAcConnect(slot):
    for monitor in _monitors['hpccAc']:
        monitor.hpccAcConnect(slot)
    return hil.hpccAcConnect(slot)


def hpccAcDeviceEnable(slot, fpga_index):
    hil.hpccAcDeviceEnable(slot, fpga_index)


def hpccAcDisconnect(slot):
    for monitor in _monitors['hpccAc']:
        monitor.hpccAcDisconnect(slot)
    return hil.hpccAcDisconnect(slot)


def hpccAcBarRead(slot, fpgaIndex, bar, offset):
    data = hil.hpccAcBarRead(slot, fpgaIndex, bar, offset)
    for monitor in _monitors['hpccAc']:
        monitor.hpccAcBarRead(slot, fpgaIndex, bar, offset, data)
    return data


def hpccAcBarWrite(slot, fpgaIndex, bar, offset, data):
    for monitor in _monitors['hpccAc']:
        monitor.hpccAcBarWrite(slot, fpgaIndex, bar, offset, data)
    return hil.hpccAcBarWrite(slot, fpgaIndex, bar, offset, data)


def hpccAcDmaRead(slot, fpgaIndex, address, length):
    data = hil.hpccAcDmaRead(slot, fpgaIndex, address, length)
    for monitor in _monitors['hpccAc']:
        monitor.hpccAcDmaRead(slot, fpgaIndex, address, data)
    return data


def hpccAcDmaWrite(slot, fpgaIndex, address, data):
    for monitor in _monitors['hpccAc']:
        monitor.hpccAcDmaWrite(slot, fpgaIndex, address, data)
    return hil.hpccAcDmaWrite(slot, fpgaIndex, address, data)


def hpccAcCpldVersionString(slot):
    return hil.hpccAcCpldVersionString(slot)


def hpccAcBltBoardRead(slot):
    return hil.hpccAcBltBoardRead(slot)


def hpccAcFpgaLoad(slot, fpga_index, image):
    hil.hpccAcFpgaLoad(slot, fpga_index, image)


def hpccAcDeviceDisable(slot, slice):
    hil.hpccAcDeviceDisable(slot, slice)


def calHpccConnect(slot):
    hil.calHpccConnect(slot)


def calHpccPca9506Write(slot, address, data):
    hil.calHpccPca9506Write(slot, address, data)


def hpccDcDeviceEnable(slot, fpga_index):
    hil.hpccDcDeviceEnable(slot, fpga_index)


def hpccDcConnect(slot):
    for monitor in _monitors['hpccDc']:
        monitor.hpccDcConnect(slot)
    return hil.hpccDcConnect(slot)


def hpccDcDisconnect(slot):
    for monitor in _monitors['hpccDc']:
        monitor.hpccDcDisconnect(slot)
    return hil.hpccDcDisconnect(slot)


def hpccDcBarRead(slot, fpgaIndex, bar, offset):
    data = hil.hpccDcBarRead(slot, fpgaIndex, bar, offset)
    for monitor in _monitors['hpccDc']:
        monitor.hpccDcBarRead(slot, fpgaIndex, bar, offset, data)
    return data


def hpccDcBarWrite(slot, fpgaIndex, bar, offset, data):
    for monitor in _monitors['hpccDc']:
        monitor.hpccDcBarWrite(slot, fpgaIndex, bar, offset, data)
    return hil.hpccDcBarWrite(slot, fpgaIndex, bar, offset, data)


def hpccDcDmaRead(slot, fpgaIndex, address, length):
    data = hil.hpccDcDmaRead(slot, fpgaIndex, address, length)
    for monitor in _monitors['hpccDc']:
        monitor.hpccDcDmaRead(slot, fpgaIndex, address, length, data)
    return data


def hpccDcDmaWrite(slot, fpgaIndex, address, data):
    for monitor in _monitors['hpccDc']:
        monitor.hpccDcDmaWrite(slot, fpgaIndex, address, length, data)
    return hil.hpccDcDmaWrite(slot, fpgaIndex, address, data)


def hpccDcCpldVersionString(slot):
    return hil.hpccDcCpldVersionString(slot)


def hpccDcBltBoardRead(slot):
    return hil.hpccDcBltBoardRead(slot)


def hpccDcFpgaLoad(slot, fpga_index, image):
    hil.hpccDcFpgaLoad(slot, fpga_index, image)


def hpccDcFpgaVersion(slot, slice):
    return hil.hpccDcFpgaVersion(slot, slice)


def hpccDcDeviceDisable(slot, slice):
    hil.hpccDcDeviceDisable(slot, slice)


def hpccBltInstrumentRead(slot):
    return hil.hpccBltInstrumentRead(slot)


def rcConnect():
    for monitor in _monitors['rc']:
        monitor.rcConnect()
    return hil.rcConnect()


def rcDisconnect():
    for monitor in _monitors['rc']:
        monitor.rcDisconnect()
    return hil.rcDisconnect()


def rcBarRead(bar, offset):
    data = hil.rcBarRead(bar, offset)
    for monitor in _monitors['rc']:
        monitor.rcBarRead(bar, offset, data)
    return data


def rcBarWrite(bar, offset, data):
    for monitor in _monitors['rc']:
        monitor.rcBarWrite(bar, offset, data)
    return hil.rcBarWrite(bar, offset, data)


def rcDmaRead(address, length):
    data = hil.rcDmaRead(address, length)
    for monitor in _monitors['rc']:
        monitor.rcDmaRead(address, data)
    return data


def rcDmaWrite(address, data):
    for monitor in _monitors['rc']:
        monitor.rcDmaRead(address, data)
    return hil.rcDmaWrite(address, data)


def rc3FpgaVersionString():
    return hil.rc3FpgaVersionString()


def rc3FailsafeDisable(disable):
    hil.rc3FailsafeDisable(disable)


def rc3DeviceDisable():
    hil.rc3DeviceDisable()


def rc3DeviceEnable():
    hil.rc3DeviceEnable()


def rc3FpgaLoad(image):
    hil.rc3FpgaLoad(image)


def rc3Connect():
    hil.rc3Connect()


def rc3Disconnect():
    hil.rc3Disconnect()


def rc3BarRead(bar, offset):
    data = hil.rc3BarRead(bar, offset)
    return data


def rc3BarWrite(bar, offset, data):
    return hil.rc3BarWrite(bar, offset, data)


def rc3CpldVersionString():
    return hil.rc3CpldVersionString()


def rc3BltInstrumentRead():
    return hil.rc3BltInstrumentRead()


def rc3BltBoardRead():
    return hil.rc3BltBoardRead()


def rc3LbConnect():
    hil.rc3LbConnect()


def tdbConnect(slot):
    hil.tdbConnect(slot)


def tdbDisconnect(slot):
    hil.tdbDisconnect(slot)


def hvdpsConnect(slot):
    hil.hvdpsConnect(slot)


def hbiDpsConnect(slot):
    hil.hbiDpsConnect(slot)


def hbiDpsDisconnect(slot):
    hil.hbiDpsDisconnect(slot)


def hbiDpsDeviceDisable(slot):
    hil.hbiDpsDeviceDisable(slot)


def hbiDpsFpgaLoad(slot, image):
    hil.hbiDpsFpgaLoad(slot, image)


def hbiDpsDeviceEnable(slot):
    hil.hbiDpsDeviceEnable(slot)


def hbiDpsFpgaVersion(slot):
    hil.hbiDpsFpgaVersion(slot)


def hbiDpsFpgaVersionString(slot):
    return hil.hbiDpsFpgaVersionString(slot)


def hbiDpsInit(slot):
    hil.hbiDpsInit(slot)


def hbiDpsBarRead(slot, bar, offset):
    return hil.hbiDpsBarRead(slot, bar, offset)


def hbiDpsBarWrite(slot, bar, offset, data):
    hil.hbiDpsBarWrite(slot, bar, offset, data)


def hbiDpsDmaRead(slot, address, data):
    return hil.hbiDpsDmaRead(slot, address, data)


def hbiDpsDmaWrite(slot, address, length):
    hil.hbiDpsDmaWrite(slot, address, length)


def hbiDpsCpldVersionString(slot):
    return hil.hbiDpsCpldVersionString(slot)


def hbiDpsLtm4680PmbusWrite(slot, chip, page, command, pCmdData):
    return hil.hbiDpsLtm4680PmbusWrite(slot, chip, page, command, pCmdData)


def hbiDpsLtm4680PmbusRead(slot, chip, page, command, readLength):
    return hil.hbiDpsLtm4680PmbusRead(slot, chip, page, command, readLength)


def hbiDpsLtc2975PmbusWrite(slot, chip, page, command, data):
    hil.hbiDpsLtc2975PmbusWrite(slot, chip, page, command, data)


def hbiDpsLtc2975PmbusRead(slot, chip, page, command, length):
    return hil.hbiDpsLtc2975PmbusRead(slot, chip, page, command, length)


def hbiDpsBltBoardRead(slot):
    return hil.hbiDpsBltBoardRead(slot)


def hbiDpsLcRailVoltageRead(slot, rail):
    return hil.hbiDpsLcRailVoltageRead(slot, rail)


def hbiDpsLcRailCurrentRead(slot, rail):
    return hil.hbiDpsLcRailCurrentRead(slot, rail)


def hbiDpsHcRailVoltageRead(slot, channel):
    return hil.hbiDpsHcRailVoltageRead(slot, channel)


def hbiDpsHcRailCurrentRead(slot, channel):
    return hil.hbiDpsHcRailCurrentRead(slot, channel)


def hbiDpsAd5676Write(slot, chip_index, command, address, data):
    hil.hbiDpsAd5676Write(slot, chip_index, command, address, data)


def hbiDpsAd5676Read(slot, chip_index, address):
    return hil.hbiDpsAd5676Read(slot, chip_index, address)


def hbiDpsVMeasureVoltageRead(slot, channel):
    return hil.hbiDpsVMeasureVoltageRead(slot, channel)


def hbiDpsLcmGangMax14662Write(slot, chip, data):
    hil.hbiDpsLcmGangMax14662Write(slot, chip, data)


def hbiDpsLcmGangMax14662Read(slot, chip):
    return hil.hbiDpsLcmGangMax14662Read(slot, chip)


def hbiDpsISourceVsenseMax14662Write(slot, chip, data):
    hil.hbiDpsISourceVsenseMax14662Write(slot, chip, data)


def hbiDpsISourceVsenseMax14662Read(slot, chip):
    return  hil.hbiDpsISourceVsenseMax14662Read(slot, chip)


def hbiDpsVtVmLbMax14662Write(slot, chip, data):
    hil.hbiDpsVtVmLbMax14662Write(slot, chip, data)


def hbiDpsVtVmLbMax14662Read(slot, chip):
    return hil.hbiDpsVtVmLbMax14662Read(slot, chip)


def hbiDpsVmonRead(slot, channel):
    return hil.hbiDpsVmonRead(slot, channel)


def hbiDpsTmonRead(slot, channel):
    return hil.hbiDpsTmonRead(slot, channel)


def hilToL11(data):
    return hil.hilToL11(data)


def hilToL16(data, mode):
    return hil.hilToL16(data, mode)


def hilFromL11(data):
    return hil.hilFromL11(data)


def hilFromL16(data, mode):
    return hil.hilFromL16(data, mode)


def hbiDpsDmaWrite(slot, address, data):
    return hil.hbiDpsDmaWrite(slot, address, data)


def hbiDpsDmaRead(slot, address, length):
    return hil.hbiDpsDmaRead(slot, address, length)


def hbiMbConnect():
    hil.hbiMbConnect()


def hbiMbDisconnect():
    hil.hbiMbDisconnect()


def hbiMbBarWrite(index, bar, offset, data):
    if monitor:
        monitor.hbiMbBarWrite(index, bar, offset, data)
    return hil.hbiMbBarWrite(index, bar, offset, data)


def hbiMbBarRead(index, bar, offset):
    if monitor:
        monitor.hbiMbBarRead(index, bar, offset)
    data = hil.hbiMbBarRead(index, bar, offset)
    return data


def hbiMbBltBoardRead():
    return hil.hbiMbBltBoardRead()


def hbiMbCpldVersionString(index):
    return hil.hbiMbCpldVersionString(index)


def hbiMbDeviceDisable(fpga):
    return hil.hbiMbDeviceDisable(fpga)


def hbiMbDeviceEnable(fpga):
    return hil.hbiMbDeviceEnable(fpga)


def hbiMbDmaRead(fpga, offset, length):
    if monitor:
        monitor.hbiMbDmaRead(fpga, offset, length)
    return hil.hbiMbDmaRead(fpga, offset, length)


def hbiMbDmaWrite(fpga, offset, data):
    if monitor:
        monitor.hbiMbDmaWrite(fpga, offset, data)
    return hil.hbiMbDmaWrite(fpga, offset, data)


def hilFailsafeDisable(boolean):
    return hil.hilFailsafeDisable (boolean)


def hbiMbFpgaLoad(fpga, image):
    return hil.hbiMbFpgaLoad(fpga, image)


def hbiMbFpgaVersionString(fpga):
    return hil.hbiMbFpgaVersionString(fpga)


def hbiMbFpgaVersion(fpga):
    return hil.hbiMbFpgaVersion(fpga)


def hbiMbTmonRead(fpga):
    return hil.hbiMbTmonRead(fpga)


def hbiMbStreamingBufferEx(index):
    return hil.hbiMbStreamingBufferEx(index)


def hbiPsdbBarWrite(slot, index, bar, offset, data):
    if monitor:
        monitor.hbiPsdbBarWrite(slot, index, bar, offset, data)
    return hil.hbiPsdbBarWrite(slot, index, bar, offset, data)


def hbiPsdbBarRead(slot, index, bar, offset):
    if monitor:
        monitor.hbiPsdbBarRead(slot, index, bar, offset)
    data = hil.hbiPsdbBarRead(slot, index, bar, offset)
    return data


def hbiPsdbBltBoardRead(slot):
    return hil.hbiPsdbBltBoardRead(slot)


def hbiPsdbCpldVersionString(slot, index):
    return hil.hbiPsdbCpldVersionString(slot, index)


def hbiPsdbDeviceDisable(slot, index):
    hil.hbiPsdbDeviceDisable(slot, index)


def hbiPsdbDeviceEnable(slot, index):
    hil.hbiPsdbDeviceEnable(slot, index)


def hbiPsdbFpgaLoad(slot, index, image):
    hil.hbiPsdbFpgaLoad(slot, index, image)


def hbiPsdbFpgaVersionString(slot, index):
    return hil.hbiPsdbFpgaVersionString(slot, index)


def hbiPsdbTmonRead(slot, fpga):
    return hil.hbiPsdbTmonRead(slot, fpga)


def hbiPsdbVmonRead(slot, fpga):
    return hil.hbiPsdbVmonRead(slot, fpga)


def hbiPsdbVmonReadAll(slot):
    return hil.hbiPsdbVmonReadAll(slot)


def hbiPsPmbusRead(interface_num, cmd, byte_length):
    return hil.hbiPsPmbusRead(interface_num, cmd, byte_length)


def hbiPsPmbusWrite(interface_num, cmd, byte_data):
    hil.hbiPsPmbusWrite(interface_num, cmd, byte_data)


def hbiMbFanRead(chip_num):
    return hil.hbiMbFanRead(chip_num)


def hbiMbMax665xRead(chip_num, reg):
    return hil.hbiMbMax665xRead(chip_num, reg)


def hbiMbMax665xWrite(chip_num, reg, data):
    hil.hbiMbMax665xWrite(chip_num, reg, data)


def hbiMbMax10CpldWrite(address, data_list):
    hil.hbiMbMax10CpldWrite(address, data_list)


def hbiMbMax10CpldRead(address, num_words):
    return hil.hbiMbMax10CpldRead(address, num_words)


def hbiDtbFanRead(dtb_index):
    return hil.hbiDtbFanRead(dtb_index)


def hbiMbSi5344Write(register, data):
    hil.hbiMbSi5344Write(register, data)


def hbiMbSi5344Read(register):
    return hil.hbiMbSi5344Read(register)


def bpTiuAuxPowerEnable(enable):
    hil.bpTiuAuxPowerEnable(enable)


def rc3Lmk04832Read(address):
    return hil.rc3Lmk04832Read(address)


def rc3Lmk04832Write(address, data):
    hil.rc3Lmk04832Write(address, data)


def rc3Init():
    hil.rc3Init()


def rc3PsPmbusRead(interface_num, cmd, byte_length):
    return hil.psPmbusRead(interface_num, cmd, byte_length)


def rc3PsPmbusWrite(interface_num, cmd, byte_data):
    return hil.psPmbusWrite(interface_num, cmd, byte_data)


def rc3DmaRead(address, length):
    return hil.rc3DmaRead(address, length)


def rc3DmaWrite(address, data):
    hil.rc3DmaWrite(address, data)


def rc3HpsDmaWrite(address, data):
    hil.rc3HpsDmaWrite(address, data)


def rc3TmonRead(channel):
    return hil.rc3TmonRead(channel)


def rc3Ad5064Write(command, address, data):
    hil.rc3Ad5064Write(command, address, data)


def rcRootI2cLock(ms_timeout):
    return hil.rcRootI2cLock(ms_timeout)


def rcRootI2cMuxSelect(selection):
    return hil.rcRootI2cMuxSelect(selection)


def rc3VmonRead(channel):
    return hil.rc3VmonRead(channel)


def rc3StreamingBufferEx(index):
    try:
        return hil.rc3StreamingBufferEx(index)
    except RuntimeError as e:
        if str(e) == 'A parameter passed to a call is invalid.':
            fval.Log('error', f'HDMT RCTC3 Driver Upgrade Required.')
        raise


def rcRootI2cUnlock():
    return hil.rcRootI2cUnlock()


def rcRootI2cInit(ms_timeout):
    return hil.rcRootI2cInit(ms_timeout)


def bpRootI2cSwitchSelect(channels):
    return hil.bpRootI2cSwitchSelect(channels)


def dpsFpgaVersion(slot, subslot):
    return hil.dpsFpgaVersion(slot, subslot)


def hddpsAd5560Write(slot, subslot, rail, register_address, data):
    hil.hddpsAd5560Write(slot, subslot, rail, register_address, data)


def hddpsAd5560Read(slot, subslot, rail, register_address):
    return hil.hddpsAd5560Read(slot, subslot, rail, register_address)


def hddpsMax6662Write(slot, subslot, device, register_addr,
                      encoded_temperature):
    hil.hddpsMax6662Write(slot, subslot, device, register_addr,
                          encoded_temperature)


def dpsBltInstrumentRead(slot):
    return hil.dpsBltInstrumentRead(slot)


def dpsBltBoardRead(slot, subslot):
    return hil.dpsBltBoardRead(slot, subslot)


def bpTiuAuxPowerState():
    return hil.bpTiuAuxPowerState()


def dpsDeviceDisable(slot, subslot):
    hil.dpsDeviceDisable(slot, subslot)


def dpsDeviceEnable(slot, subslot):
    hil.dpsDeviceEnable(slot, subslot)


def dpsFpgaLoad(slot, subslot, image):
    hil.dpsFpgaLoad(slot, subslot, image)


def cypDeviceOpen(vidpid, slot, additional):
    return hil.cypDeviceOpen(vidpid, slot, additional)


def cypPortBitWrite(device, port, bit, enable):
    hil.cypPortBitWrite(device, port, bit, enable)


def cypPortBitRead(device, port, bit):
    return hil.cypPortBitRead(device, port, bit)


def cypDeviceClose(handle):
    hil.cypDeviceClose(handle)


def tiuCalBaseGpioWrite(gpio, state):
    hil.tiuCalBaseGpioWrite(gpio, state)


def tiuCalBaseConnect():
    hil.tiuCalBaseConnect()


def tiuCalBaseInit():
    hil.tiuCalBaseInit()


def rc3TiuAlarmEnable(enable):
    hil.rc3TiuAlarmEnable(enable)


def tiuCalBaseDisconnect():
    hil.tiuCalBaseDisconnect()


def rc3TiuAlarmEnable(enable):
    hil.rc3TiuAlarmEnable(enable)


def hbiMbHpsDmaWrite(offset, data):
    hil.hbiMbHpsDmaWrite(offset, data)


def psBltBoardRead(slot):
    return hil.psBltBoardRead(slot)


def hbiDtbTmonRead(dtb_index, channel):
    return hil.hbiDtbTmonRead(dtb_index, channel)


def hbiMbAlarmWait():
    return hil.hbiMbAlarmWait()


def hbiMbAlarmWaitCancel():
    hil.hbiMbAlarmWaitCancel()


def hbiDtbPca9505Write(dtb_index, chip, reg, data):
    hil.hbiDtbPca9505Write(dtb_index, chip, reg, data)


def hbiDtbPca9505Read(dtb_index, chip, reg):
    return hil.hbiDtbPca9505Read(dtb_index, chip, reg)
