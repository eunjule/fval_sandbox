:: INTEL CONFIDENTIAL

:: Copyright 2017-2019 Intel Corporation.

:: This software and the related documents are Intel copyrighted materials,
:: and your use of them is governed by the express license under which they
:: were provided to you ("License"). Unless the License provides otherwise,
:: you may not use, modify, copy, publish, distribute, disclose or transmit
:: this software or the related documents without Intel's prior written
:: permission.

:: This software and the related documents are provided as is, with no express
:: or implied warranties, other than those that are expressly stated in the
:: License.

@echo off
setlocal enabledelayedexpansion

call:GETTIME
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc
py ./tools/regress.py hbicc



EXIT /B %ERRORLEVEL%

:GETTIME
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:, " %%a in ("%TIME%") do (set mytime=%%a%%b)
::echo %mydate%_%mytime%