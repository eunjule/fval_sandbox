////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: logging.cc
//------------------------------------------------------------------------------
//    Purpose: Common logging classes and methods
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/08/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "logging.h"
#include <iostream>

namespace logging
{

static Logger* commonLogger = nullptr;

void SetCommonLogger(Logger* logger)
{
    commonLogger = logger;
}

void _Log(const std::string& file, size_t line, const std::string& function, const std::string& verbosity, const std::string& message)
{
    if (commonLogger != nullptr) {
        commonLogger->Log(file, line, function, verbosity, message);
    } else {
        std::cout << file << " " << line << " " << function << " " << verbosity << " " << message << std::endl;
    }
}

}  // namespace logging
