////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: rcsim.h
//------------------------------------------------------------------------------
//    Purpose: RC Simulator
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 2/16/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __RCSIM_H__
#define __RCSIM_H__

#include <cstdint>
#include <string>
#include <vector>

namespace rctbc {

class RcSimulator;

enum CardType
{
    HPCC_AC_CARD = 0x0,
    DPS_CARD     = 0x1,
    SOFTWARE     = 0x2,
    PXIE_CARD    = 0x3,
    HPCC_DC_CARD = 0x4
};

class InstrumentSimulator
{
public:
    virtual ~InstrumentSimulator() {};
    /// Name of this instrument
    virtual std::string Name() const = 0;
    /// Called by env
    virtual void Connect(RcSimulator* rc);
    /// Called by RC simulator
    virtual void ProcessTrigger(uint32_t trigger) = 0;
    int cardType;
    int slot;
    int m_dutId;
    RcSimulator* m_rc;
};

class RcSimulator
{
public:
    friend class InstrumentSimulator;
    RcSimulator() {};
    void SendTrigger(uint32_t trigger);
    void SendTriggerHpccAc(uint8_t cardType, uint8_t dutId, uint8_t domain, uint8_t count, uint8_t triggerType);
    void SendTriggerHddps(uint8_t cardType, uint8_t dutId, uint16_t lutIndex);
    std::vector<InstrumentSimulator*> instruments;
private:
    void Connect(InstrumentSimulator* instrument);
};

}  // namespace rctbc

#endif  // __RCSIM_H__

