////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: rcsim.cc
//------------------------------------------------------------------------------
//    Purpose: RC Simulator
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 2/16/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "rcsim.h"
#include "defines.h"
#include "logging.h"

#include <cstddef>

namespace rctbc {

void InstrumentSimulator::Connect(RcSimulator* rc)
{
    this->m_rc = rc;
    this->m_rc->Connect(this);
}

void RcSimulator::Connect(InstrumentSimulator* instrument)
{
    instruments.push_back(instrument);
}

void RcSimulator::SendTrigger(uint32_t trigger)
{
    LOG("debug") << "RC Simulator received trigger " << Hex(trigger);
    uint8_t cardType = static_cast<uint8_t>(SLICE(trigger, 31, 26));
    for (size_t i = 0; i < instruments.size(); i++) {
        InstrumentSimulator& instrument = *instruments[i];
        bool match = cardType == instrument.cardType;
        // Broadcast the trigger to all the matching instruments
        if (match) {
            LOG("debug") << "RC Simulator broadcasted trigger " << Hex(trigger) << " to " << instruments[i]->Name();
            instrument.ProcessTrigger(trigger);
        }
    }
}

void RcSimulator::SendTriggerHpccAc(uint8_t cardType, uint8_t dutId, uint8_t domain, uint8_t count, uint8_t triggerType)
{
    uint32_t trigger = (cardType << 26) | (dutId << 20) | (domain << 8) | (count << 4) | triggerType;
    SendTrigger(trigger);
}

void RcSimulator::SendTriggerHddps(uint8_t cardType, uint8_t dutId, uint16_t lutIndex)
{
    uint32_t trigger = (cardType << 26) | (dutId << 20) | lutIndex;
    SendTrigger(trigger);
}

}  // namespace rctbc

