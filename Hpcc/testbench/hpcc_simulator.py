# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from time import clock, perf_counter

from Common.fval import Object
from Hpcc.instrument.hpcc import dc_registers
from Hpcc.instrument.hpccAcRegs import PatternControl, ResetsAndStatus,\
    SyncStartEdgeResult
from Hpcc.testbench.ad9914_simulator import Ad9914Simulator


class Hpcc(Object):
    def __init__(self, rc, slot):
        super().__init__()
        self.rc = rc
        self.slot = slot
        self.dc = [HpccDc(rc, slot, slice=0), HpccDc(rc, slot, slice=1)]
        self.ac = [HpccAc(rc, slot, 0, self.dc[0]), HpccAc(rc, slot, 1, self.dc[0])]

    def receive_sync_pulse(self):
        for ac in self.ac:
            ac.start_pattern()

class HpccDc(Object):
    def __init__(self, rc, slot, slice):
        super().__init__()
        self.rc = rc
        self.slot = slot
        self.slice = slice
        self.registers = {}
        self.read_actions = {0x230: self.down_trigger}
        self.write_actions = {0x8010: self.resets_write,
                              0x8050: self.gpio_write}

    def bar_read(self, bar, offset):
        return self.read_actions.get(offset, self.default_read_action)(offset)

    def bar_write(self, bar, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def default_read_action(self, offset):
        data = self.registers.get(offset, 0)
        return data

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def down_trigger(self, offset):
        trigger = self.rc.read_trigger_down(self.slot)
        return trigger

    def up_trigger(self, trigger):
        self.registers[0x240] = trigger

    def resets_write(self, offset, data):
        reg = dc_registers.Resets(uint=data, length=32)
        reg.InstructionBlockDomainReset = 0
        reg.DataCaptureReset = 0
        reg.AdAte320DomainReset = 0
        reg.Ad7609DomainReset = 0
        self.registers[offset] = reg.Pack()

    def gpio_write(self, offset, data):
        reg = dc_registers.Gpio(uint=data, length=32)
        reg.PowerSwitchStatus = reg.PowerSwitchControl
        self.registers[offset] = reg.Pack()


class HpccAc(Object):
    def __init__(self, rc, slot, slice, dc0):
        super().__init__()
        self.rc = rc
        self.slot = slot
        self.slice = slice
        self.dc0 = dc0
        self.ad9914 = [Ad9914Simulator(), Ad9914Simulator()]
        self.memory_table = {}

        self.registers = {0x00C: 0x00,
                          0x8240: 0x08000000,
                          0x85F0: 0xacdc7d4b}

        self.write_actions = {0x050: self.send_up_trigger,
                              0x8190: self.send_up_trigger,
                              0x8230: self.write_pattern_control,
                              0x8240: self.ad9914_control_write}
        self.read_actions = {0x054: self.read_trigger_down,
                             0x230: self.read_trigger_down,
                             0x8050: self.gpio_power_switch_status,
                             0x8110: self.resets_and_status,
                             0x8190: self.read_trigger_up,
                             0x8220: self.read_total_fail_count,
                             0x8230: self.read_pattern_control,
                             0x84C0: self.read_pattern_end_status,
                             0x8530: self.read_sync_start_edge_result}

        self.sync_start_edge_count = 0

        self.init_reset_and_status_reg()
        self.init_pattern_control_reg()

    def name(self):
        return f'{self.__class__.__name__}_{self.slot}_{self.slice}'

    def bar_write(self, bar, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def bar_read(self, bar, offset):
        # When reading down triggers, HPCC uses AC FPGA's Trigger register
        # instead of DC FPGAs down trigger. Redirect to compensate
        if self.slice == 1 and offset == 0x8190:
            offset = 0x230

        data = self.read_actions.get(offset, self.default_read_action)(offset)
        return data

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def default_read_action(self, offset):
        if offset not in self.registers:
            self.registers[offset] = 0x0

        return self.registers[offset]

    def read_trigger_down(self, offset):
        trigger = self.rc.read_trigger_down(self.slot)
        return trigger

    def read_trigger_up(self, offset):
        trigger = self.rc.read_trigger_up(self.slot)
        return trigger

    def send_up_trigger(self, offset, data):
        self.rc.write_trigger_up(self.slot, data)
        if self.slice == 0:
            self.registers[0x240] = data
        self.dc0.up_trigger(data)

    def dma_write(self, offset, data):
        offset &= 0xFFFFFFFFF
        if offset in self.memory_table:
            del self.memory_table[offset]
        self.memory_table[offset] = data
        self._remove_stale_ram_data()

    def _remove_stale_ram_data(self):
        '''Avoid using lots of RAM for simulation, becomes slow otherwise'''
        if len(self.memory_table.keys()) > 10:
            oldest_key = list(self.memory_table.keys())[0]
            del self.memory_table[oldest_key]

    def dma_read(self, offset, length):
        offset &= 0xFFFFFFFFF
        block_offset, block_data = self.get_memory_block(offset)
        if offset == block_offset and length == len(block_data):
            return bytes(block_data)
        else:
            start = offset - block_offset
            end = start + length
            return bytes(block_data[start:end])

    def get_memory_block(self, offset):
        for block_offset in reversed(list(self.memory_table.keys())):
            block_data = self.memory_table[block_offset]
            if block_offset <= offset < (block_offset + len(block_data)):
                self._mark_memory_block_least_recently_used(block_offset)
                return block_offset, block_data
        else:
            self.memory_table[offset] = bytearray(1024)
            self._remove_stale_ram_data()
            return offset, self.memory_table[offset]

    def _mark_memory_block_least_recently_used(self, block_offset):
        '''Use insertion order for LRU algorithm'''
        block_data = self.memory_table[block_offset]
        del self.memory_table[block_offset]
        self.memory_table[block_offset] = block_data

    def resets_and_status(self, offset):
        return self.reset_and_status_reg.value

    def ad9914_control_write(self, offset, data):
        self.registers[offset] = data
        ad9914_select = (data >> 10) & 1
        ad9914 = self.ad9914[ad9914_select]
        if 0x100 & data:
            address = data & 0x7F
            if 0x80 & data:
                self.registers[0x8260] = ad9914.read(address)
            else:
                ad9914.write(address, self.registers[0x8250])
            self.registers[offset] |= 1 << 27

    def gpio_power_switch_status(self, offset):
        return 1 << 29

    def read_total_fail_count(self, offset):
        return 1

    def read_pattern_end_status(self, offset):
        return 0xdeadbeef

    def read_sync_start_edge_result(self, offset):
        self.sync_start_edge_count += 1
        reg = SyncStartEdgeResult()
        reg.Count = 50  # self.sync_start_edge_count
        return reg.value

    def start_pattern(self):
        if self.pattern_control_reg.StartOnNextRCSyncPulse:
            self.reset_and_status_reg.PatternRunning = 0

    def init_reset_and_status_reg(self):
        self.reset_and_status_reg = ResetsAndStatus(SlotNumber=self.slot)
        self.reset_and_status_reg.PLLLocks = 0b1111
        self.reset_and_status_reg.DDR3ControllerInitialized = 0b1111
        self.reset_and_status_reg.Lmk04808StatusLd = 1
        self.reset_and_status_reg.Lmk04808StatusClkIn0 = 1
        self.reset_and_status_reg.Lmk04808StatusClkIn1 = 1

    def init_pattern_control_reg(self):
        self.pattern_control_reg = PatternControl()

    def read_pattern_control(self, offset):
        return self.pattern_control_reg.Pack()

    def write_pattern_control(self, offset, data):
        self.pattern_control_reg.value = data
        self.reset_and_status_reg.PatternRunning = \
            self.pattern_control_reg.StartOnNextRCSyncPulse