# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from ctypes import c_uint, LittleEndianStructure


class Ad9914Simulator():
    def __init__(self):
        self.registers = {}

    def write(self, command, data):
        self.registers[command] = data

    def read(self, command):
        return self.registers[command]


class CFR1(LittleEndianStructure):
    _fields = [('lsb_first_mode', c_uint, 1),
               ('sdio_input_only', c_uint, 1),
               ('open_bit2', c_uint, 1),
               ('external_power_down_control', c_uint, 1),
               ('open_bit4', c_uint, 1),
               ('ref_clk_input_power_down', c_uint, 1),
               ('dac_power_down', c_uint, 1),
               ('digital_power_down', c_uint, 1),
               ('osk_enable', c_uint, 1),
               ('external_osk_enable', c_uint, 1),
               ('open_bit10', c_uint, 1),
               ('clear_phase_accumulator', c_uint, 1),
               ('clear_digital_ramp_accumulator', c_uint, 1),
               ('autoclear_phase_accumulator', c_uint, 1),
               ('autoclear_digital_ramp_accumulator', c_uint, 1),
               ('load_lrr_at_io_update', c_uint, 1),
               ('eable_sine_output', c_uint, 1),
               ('parallel_port_streaming_enable', c_uint, 1),
               ('open_bits23_18', c_uint, 6),
               ('vco_cal_enable', c_uint, 1),
               ('open_bits31:25', c_uint, 7)]

#
# class CFR2(LittleEndianStructure):
#     _fields_ = [('')]