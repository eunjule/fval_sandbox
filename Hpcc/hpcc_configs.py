# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os

DBG_SKIP_FPGA_LOAD = os.getenv('DBG_SKIP_HPCC_FPGA_LOAD') == '1'

CALIBRATION_DIR = r'results\_calibration'
HPCC_SLICE0_ONLY = False  # AC only
HPCC_SLICE1_ONLY = False  # Only run slice 1 tests (slice 0 is still needed for full init)

HPCC_ALARM_DEBUG = False
HPCC_AC_FPGA_LOAD = True if not DBG_SKIP_FPGA_LOAD else False
HPCC_DCL_FPGA_LOAD = True if not DBG_SKIP_FPGA_LOAD else False
HPCC_DCU_FPGA_LOAD = True if not DBG_SKIP_FPGA_LOAD else False

HPCC1_AC_FPGA_FABD = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_AC\Release\Fab D\hpcc_ac_variant0_2017-09-26_14.00.14__v.30.2.2'
# HPCC1_AC_FPGA_FABD = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_AC\Release\Fab D\hpcc_ac_variant0_2020-10-09_00.05.48__v.33.1.2'
HPCC1_AC_FPGA_FABE = HPCC1_AC_FPGA_FABD
HPCC1_AC_FPGA_FABG = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_AC\Release\HPPC2_FabA\hpcc_ac_variant0_2018-11-09_13.54.47__v.31.5.6'
HPCC1_AC_FPGA_FABH = HPCC1_AC_FPGA_FABD

HPCC1_DCL_FPGA_MDUT_FabC_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\MDUT_FabE\6.2\hpcc_dcl_fpga_rev6_2(sib0_0).bin'
HPCC1_DCL_FPGA_MDUT_FabD_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\MDUT_FabG\10.5\hpcc_dcl_fpga_rev10_5(sib0_0).bin'
HPCC1_DCL_FPGA_MDUT_FabF_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\MDUT_FabF\8.2\hpcc_dcl_fpga_rev8_2(sib0_0).bin'
HPCC1_DCL_FPGA_MDUT_FabG_IMAGE = HPCC1_DCL_FPGA_MDUT_FabD_IMAGE

HPCC1_DCU_FPGA_MDUT_FabC_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\MDUT_FabE\6.2\hpcc_dcu_fpga_rev6_2(sib0_0).bin'
HPCC1_DCU_FPGA_MDUT_FabD_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\MDUT_FabG\10.5\hpcc_dcu_fpga_rev10_5(sib0_0).bin'
HPCC1_DCU_FPGA_MDUT_FabF_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\MDUT_FabF\8.2\hpcc_dcu_fpga_rev8_2(sib0_0).bin'
HPCC1_DCU_FPGA_MDUT_FabG_IMAGE = HPCC1_DCU_FPGA_MDUT_FabD_IMAGE


HPCC2_AC_FPGA_FABA = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_AC\Release\HPPC2_FabA\hpcc_ac_variant0_2018-11-09_13.54.47__v.31.5.6'
HPCC2_DCL_FPGA_MDUT_FabA_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\HPCC2_FabA\8.6\hpcc_dcl_fpga_rev8_6(sib0_0).bin'
HPCC2_DCU_FPGA_MDUT_FabA_IMAGE = r'\\datagrovehf.hf.intel.com\STTD\TSTD\FPGAs\HPCC_DC\HPCC2_FabA\8.6\hpcc_dcu_fpga_rev8_6(sib0_0).bin'


# FULL invokes:
#   tester.PrHpccCalInit() (CalKit / TIU)
#   hpcc.InitializeCal() (CalKit / TIU)
#   hpcc.Initialize()
#   dcCal.DcCalibration()
#   loopbackCal.LoopbackCal()
# CAL invokes:
#   tester.PrHpccCalInit() (CalKit / TIU)
#   hpcc.InitializeCal() (CalKit / TIU)
#   dcCal.DcCalibration()
#   loopbackCal.LoopbackCal()
# LOADCAL invokes:
#   tester.PrHpccCalInit() (CalKit / TIU)
#   hpcc.InitializeCal() (CalKit / TIU)
#   hpcc.Initialize()
# ZEROCAL invokes: but will not load ac cal data 
#   tester.PrHpccCalInit() (CalKit / TIU)
#   hpcc.InitializeCal() (CalKit / TIU)
#   hpcc.Initialize()
# NONE does not invoke any of the methods above
HPCC_ENV_INIT = 'FULL'  # Valid values: NONE, LOADCAL, CAL, FULL, ZEROCAL

HPCC_TAKE_SNAPSHOT = False
HPCC_NO_PAT_SNAPSHOT = False
HPCC_TAKE_DCCAL_SNAPSHOT = False

HPCC_AC_SIM_MEM = 100 * 1024 * 1024  # Default simulator memory size = 100M vectors per slice
INTERNAL_LOOPBACK_PERIOD = 1.2e-9

# skip DDR memory init
HPCC_SC_SKIP_MEM_INIT = False



