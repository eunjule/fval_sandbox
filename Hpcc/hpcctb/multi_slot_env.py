# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from Hpcc.hpcctb.env import Env


class MultiSlotEnv(Env):
    def ExecuteMultiSlicePatternViaSyncPulse(self, slots, start):
        max_retries=500
        for slot in slots:
            hpcc = self.instruments[slot]

            for slice in range(2):
                hpcc.ac[slice].PrestagePattern(start, max_retries)

        self.rc.send_sync_pulse()

        completed = []
        for slot in slots:
            hpcc = self.instruments[slot]
            for ac in hpcc.ac:
                ac_completed = hpcc.ac[0].WaitForCompletePattern(max_retries)
                if not ac_completed:
                    self.Log('error', 'Pattern execution did not complete')
                    ac.AbortPattern(waitForComplete = True)
                    abort_completed = ac.IsPatternComplete()
                    self.Log('warning', f'abort pattern, pattern complete = {abort_completed}')
            completed.append(ac_completed)
        return all(completed)

    def WriteExecuteMultiSlicePatternAndSetupCapture(self, slots, pattern = None, offset = 0x0, start = 0x0,
                                                     captureAddress = None, captureType = 1, captureFails = False,
                                                     captureCTVs = False, captureAll = True, stopOnMaxFail = False,
                                                     dramOverflowWrap = False,
                                                     captureLength = 0xFFFFFFFF):
        self._setup_pattern_and_capture(captureAddress, captureAll, captureCTVs, captureFails, captureLength,
                                        captureType, dramOverflowWrap, offset, pattern, slots, stopOnMaxFail)
        return self.ExecuteMultiSlicePatternViaSyncPulse(slots, start)

    def _setup_pattern_and_capture(self, captureAddress, captureAll, captureCTVs, captureFails, captureLength,
                                   captureType, dramOverflowWrap, offset, pattern, slots, stopOnMaxFail):
        for slot in slots:
            hpcc = self.instruments[slot]

            for slice in range(2):
                if not (pattern is None):  # Write pattern to memory
                    self.WritePattern(slot, slice, offset, pattern)

                if captureAddress is None:
                    if pattern is None:
                        raise Exception('If no pattern is set, captureAddress must be set!')
                    else:
                        captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern))

                hpcc.ac[slice].SetCaptureBaseAddress(captureAddress)
                hpcc.ac[slice].SetCaptureCounts(captureLength=captureLength)
                hpcc.ac[slice].SetCaptureControl(captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail,
                                                 dramOverflowWrap)
