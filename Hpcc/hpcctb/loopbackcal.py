################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: loopbackcal.py
# -------------------------------------------------------------------------------
#     Purpose: ac timing calibration, using loopback cal card using eye based methodology
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 03/29/17
#       Group: HDMT FPGA Validation
################################################################################

import gzip
import os
import time

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')

from Common import fval
from Hpcc.hpcctb.assembler import PatternAssembler
import Hpcc.instrument.hpccAcRegs as ac_registers

PICO_SECONDS_PER_SECOND = 1000000000000

class Calibration(fval.Object):
    def __init__(self, hpcc, rc, maskPin=[-1]):
        self.slot = hpcc.slot
        self.maskPin = maskPin
        self.rc = rc
        self.calType = ''
        self.hpcc = hpcc
        self.CALIBRATION_PATH = os.path.join(repo_root_path, self.hpcc.hpccConfigs.CALIBRATION_DIR)
        self.Log('info', 'Calibration path = {}'.format(self.CALIBRATION_PATH))

        self.CALIBRATION_PERIOD = self.hpcc.ac[0].minQdrMode / PICO_SECONDS_PER_SECOND

        if self.hpcc.hpccConfigs.HPCC_SLICE1_ONLY:
            self.CALIBRATION_PERIOD = self.hpcc.ac[1].minQdrMode / PICO_SECONDS_PER_SECOND

    def LoopbackCal(self):
        self.evenToOddEyeCal()
        if not (self.hpcc.hpccConfigs.HPCC_SLICE0_ONLY or self.hpcc.hpccConfigs.HPCC_SLICE1_ONLY):
            self.lowToHighEyeCal()
        if (self.hpcc.hpccConfigs.HPCC_SLICE0_ONLY or self.hpcc.hpccConfigs.HPCC_SLICE1_ONLY):
            self.Log('info', 'Skipping Low to High Cal Because only one slice is activated')

################Putting ETO Eye Based Calibration Here
    def _clearcaloffset(self,hpcc):
        for slice in [0, 1]:
            for ch in range(56):
                hpcc.ac[slice].caldata.channelData[ch].inputCal = 0


    def _createetocoarsepat(self,patdata):
        pattern = PatternAssembler()
        pattern.LoadString("""\
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                    BlockFails 1
                    Drive0CompareLVectors length=1024
                    BlockFails 0
                    %repeat 256
                    V link=0, ctv=0, mtv=1, lrpt=0, data=0vH0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    %end
                    Drive1CompareHVectors length=1024
                    PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                """)
        patdata = pattern.Generate()
        return patdata


    def _createotecoarsepat(self,patdata):
        pattern = PatternAssembler()
        pattern.LoadString("""\
                    S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                    BlockFails 1
                    Drive0CompareLVectors length=1024
                    BlockFails 0
                    %repeat 256
                    V link=0, ctv=0, mtv=1, lrpt=0, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H
                    %end
                    Drive1CompareHVectors length=1024
                    PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                """)
        patdata = pattern.Generate()
        return patdata

    def _executecoarsepat(self,hpcc, slice, pdata, period):
        mtv = 1
        mask = 0
        CALIBRATION_PERIOD = period
        hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
        hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
        hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
        hpcc.ac[slice].captureAll = False
        hpcc.ac[slice].SetPeriod(CALIBRATION_PERIOD)
        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': 0.0}, True, channels='ALL_PINS')
        hpcc.ac[slice].WritePattern(0, pdata)
        hpcc.ac[slice].ExecutePattern()



    def _executeLTHcoarsepat(self, hpcc, pdata, period):
        mtv = 1
        mask = 0
        CALIBRATION_PERIOD = period
        for slice in [0,1]:
            hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
            hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
            hpcc.ac[slice].captureAll = False
            hpcc.ac[slice].SetPeriod(CALIBRATION_PERIOD)
            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': 0.0}, True, channels='ALL_PINS')
            hpcc.ac[slice].WritePattern(0, pdata)
            self.hpcc.ac[slice].PrestagePattern()

        # Send sync pulse
        self.rc.send_sync_pulse()

        # Wait for all slices to complete
        for slice in [0, 1]:
            self.hpcc.ac[slice].WaitForCompletePattern()




    def evenToOddEyeCal(self):
        hpcc = self.hpcc
        calibrationPeriod = (self.CALIBRATION_PERIOD * PICO_SECONDS_PER_SECOND * 2) - hpcc.ac[0].nominalFineDelayValue
        pdata = {}
        coarseetopatdata = self._createetocoarsepat(pdata)
        coarseotepatdata = self._createotecoarsepat(pdata)

        patStr = r'Hpcc\Patterns\Calibration_even_to_odd_odd_pins_1490673486.724198.obj.gz'
        pattern = gzip.open(os.path.join(repo_root_path, patStr), 'rb').read()
        fineetopatdata = pattern

        patStr = r'Hpcc\Patterns\Calibration_even_to_odd_even_pins_1490673486.724198.obj.gz'
        pattern = gzip.open(os.path.join(repo_root_path, patStr), 'rb').read()
        fineotepatdata = pattern

        self.Log('info', 'Running Even To Odd Calibration with a period of: {}'.format(calibrationPeriod))

        self.findETOCal(calibrationPeriod, coarseetopatdata, coarseotepatdata, fineetopatdata, fineotepatdata)



    def lowToHighEyeCal(self):
        hpcc = self.hpcc
        calibrationPeriod = (self.CALIBRATION_PERIOD * PICO_SECONDS_PER_SECOND * 2) - hpcc.ac[0].nominalFineDelayValue
        pdata = {}
        coarseetopatdata = self._createetocoarsepat(pdata)
        coarseotepatdata = self._createotecoarsepat(pdata)

        patStr = r'Hpcc\Patterns\Calibration_even_to_odd_odd_pins_1490673486.724198.obj.gz'
        pattern = gzip.open(os.path.join(repo_root_path, patStr), 'rb').read()
        fineetopatdata = pattern

        patStr = r'Hpcc\Patterns\Calibration_even_to_odd_even_pins_1490673486.724198.obj.gz'
        pattern = gzip.open(os.path.join(repo_root_path, patStr), 'rb').read()
        fineotepatdata = pattern

        self.Log('info', 'Running Low To High Calibration with a period of: {}'.format(calibrationPeriod))

        self.findLTHCal(calibrationPeriod, coarseetopatdata, coarseotepatdata, fineetopatdata, fineotepatdata)





    def findETOCal(self, period, coarseetopatdata, coarseotepatdata, fineetopatdata, fineotepatdata):

        self.calType = 'EvenToOddLoopback'
        hpcc = self.hpcc

        # set up data structure to hold shmoo results
        caldat = {}
        slices = [0, 1]
        oddOnly = False
        for slice in slices:
            for ch in range(56): caldat[(slice, ch)] = {'ETOCALVALUE': -999, 'LTHCALVALUE': -999, 'COARSE': -999, 'COARSE_FAILS': -999, 'LEFTEDGE': -999, 'RIGHTEDGE': -999, 'LEFTEDGEINDEX': -999,
                                                        'RIGHTEDGEINDEX': -999, 'EYECENTER': -999, 'EYEWIDTH': -999, 'SHMOO': [], 'EYESTEPS': -999,
                                                        'LEFTEDGE2': -999, 'RIGHTEDGE2': -999, 'LEFTEDGEINDEX2': -999, 'RIGHTEDGEINDEX2': -999, 'EYECENTER2': -999, 'EYEWIDTH2': -999, 'SHMOO2': [],
                                                        'EYESTEPS2': -999}

        calibrationPeriod = period

        # # Set even to odd loopback
        hpcc.cal.SetEvenToOddChannelLoopback()
        for ac in hpcc.ac:
            ac.internalLoopback = False

        self._clearcaloffset(hpcc)
        self.Log('info', 'test slot {}'.format(hpcc.slot))

        for slice in slices:
            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0}, True, channels='ALL_PINS')


        for slice in slices:
            # execute odd pattern
            self._executecoarsepat(hpcc, slice, coarseetopatdata, calibrationPeriod / PICO_SECONDS_PER_SECOND)
            startChannel = 1
            # get number of fails for each odd pin, offset by the expected count and write to results structure
            for ch in range(56):
                if ch % 2 == startChannel:
                    captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                    caldat[(slice, ch)]['COARSE_FAILS'] = captureCount.ChannelFailCaptureCount - 256

        if oddOnly is not True:
            for slice in slices:
                # execute odd pattern
                self._executecoarsepat(hpcc, slice, coarseotepatdata, calibrationPeriod / PICO_SECONDS_PER_SECOND)
                startChannel = 0
                # get number of fails for each even pin, offset by the expected count and write to results structure
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['COARSE_FAILS'] = captureCount.ChannelFailCaptureCount - 256

        # Set coarse Values
        for slice in slices:
            for ch in range(56):
                if ch % 2 == 0 and oddOnly is True:
                    continue
                else:
                    caldat[(slice, ch)]['COARSE'] = (caldat[(slice, ch)]['COARSE_FAILS'] * calibrationPeriod) - (1.5 * calibrationPeriod)
                    self.Log('info', 'Slice: {}, CH: {}, Starting point from COARSE data: {}'.format(slice, ch, caldat[(slice, ch)]['COARSE']))

        steps = 16
        stepSize = calibrationPeriod / 4

        # slice0/1 odd
        for slice in slices:
            currentOffset = 0
            hpcc.ac[slice].WritePattern(0, fineetopatdata)
            for step in range(steps):
                startChannel = 1
                for ch in range(56):
                    if ch % 2 == startChannel:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['COARSE'] + currentOffset) / PICO_SECONDS_PER_SECOND}, True, channels=[ch])
                # execute odd pattern
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].ExecutePattern()
                # get number of fails for each odd pin, offset by the expected count and write to results structure
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['SHMOO'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['COARSE'] + currentOffset)])
                currentOffset = currentOffset + stepSize
        if oddOnly is not True:
            # slice0/1 even
            for slice in slices:
                currentOffset = 0
                hpcc.ac[slice].WritePattern(0, fineotepatdata)
                for step in range(steps):
                    startChannel = 0
                    for ch in range(56):
                        if ch % 2 == startChannel:
                            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['COARSE'] + currentOffset) / PICO_SECONDS_PER_SECOND}, True,
                                                           channels=[ch])
                    # execute odd pattern
                    hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                    hpcc.ac[slice].ExecutePattern()
                    # get number of fails for each odd pin, offset by the expected count and write to results structure
                    for ch in range(56):
                        if ch % 2 == startChannel:
                            captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                            caldat[(slice, ch)]['SHMOO'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['COARSE'] + currentOffset)])
                    currentOffset = currentOffset + stepSize

        # for slice in slices:
        #     for ch in range(56):
        #         self.Log('info', 'CH {}, SLICE {}'.format(ch, slice))
        #         for list in caldat[(slice, ch)]['SHMOO']:
        #             self.Log('info', 'FAILS: {} OFFSET: {}'.format(list[0], list[1]))

        for slice in slices:
            for ch in range(56):
                if ch % 2 == 0 and oddOnly is True:
                    continue
                else:
                    # reset count used for initial loop
                    count = 0
                    # reset low and high eye values
                    lowval = 0
                    highval = 0
                    index = 0
                    for list in caldat[(slice, ch)]['SHMOO']:
                        # if there were no fails on the current step
                        if list[0] <= 40000:
                            # for the first passing step set the low and high to the offset value
                            if count == 0:
                                lowval = list[1]
                                lowvalindex = index
                                highval = list[1]
                                highvalindex = index
                            # find the high value for the eye
                            if list[1] >= highval:
                                highval = list[1]
                                highvalindex = index

                            count += 1
                        index = index + 1
                    if count == 0:
                        lowval = -999
                        lowvalindex = -999
                        highval = -999
                        highvalindex = -999
                    else:
                        lowval = caldat[(slice, ch)]['SHMOO'][lowvalindex - 1][1]
                        highval = caldat[(slice, ch)]['SHMOO'][highvalindex + 1][1]
                        count = count + 1

                    self.Log('info', 'CH {}, SLICE {}, Low Value: {}, High Value: {}, Width: {}, Width Steps: {}'.format(ch, slice, lowval, highval, highval - lowval, count))
                    caldat[(slice, ch)]['LEFTEDGE'] = lowval
                    caldat[(slice, ch)]['LEFTEDGEINDEX'] = lowvalindex
                    caldat[(slice, ch)]['RIGHTEDGE'] = highval
                    caldat[(slice, ch)]['RIGHTEDGEINDEX'] = highvalindex
                    caldat[(slice, ch)]['EYEWIDTH'] = highval - lowval
                    caldat[(slice, ch)]['EYECENTER'] = lowval + ((highval - lowval) / 2)
                    caldat[(slice, ch)]['EYESTEPS'] = count


                    # now sweep at finer resoliton

        stepSize = calibrationPeriod / 16

        # ODD PINS
        startChannel = 1
        for slice in slices:
            # find max width for ODD PINS
            hpcc.ac[slice].WritePattern(0, fineetopatdata)
            currentOffset = 0
            maxWidth = 0
            for ch in range(56):
                if ch % 2 == startChannel:
                    if caldat[(slice, ch)]['EYESTEPS'] > maxWidth:
                        maxWidth = caldat[(slice, ch)]['EYESTEPS']
            # set stepcount
            numberSteps = maxWidth * 4
            for step in range(numberSteps):
                for ch in range(56):
                    if ch % 2 == startChannel:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset) / PICO_SECONDS_PER_SECOND}, True, channels=[ch])
                hpcc.ac[slice].ExecutePattern()
                # get number of fails for each odd pin, offset by the expected count and write to results structure
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['SHMOO2'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset)])
                currentOffset = currentOffset + stepSize

        if oddOnly is not True:
            # EVEN PINS
            startChannel = 0
            for slice in slices:
                # find max width for ODD PINS
                hpcc.ac[slice].WritePattern(0, fineotepatdata)
                currentOffset = 0
                maxWidth = 0
                for ch in range(56):
                    if ch % 2 == startChannel:
                        if caldat[(slice, ch)]['EYESTEPS'] > maxWidth:
                            maxWidth = caldat[(slice, ch)]['EYESTEPS']
                # set stepcount
                numberSteps = maxWidth * 4
                for step in range(numberSteps):
                    for ch in range(56):
                        if ch % 2 == startChannel:
                            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset) / PICO_SECONDS_PER_SECOND}, True,
                                                           channels=[ch])
                    hpcc.ac[slice].ExecutePattern()
                    # get number of fails for each odd pin, offset by the expected count and write to results structure
                    for ch in range(56):
                        if ch % 2 == startChannel:
                            captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                            caldat[(slice, ch)]['SHMOO2'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset)])
                    currentOffset = currentOffset + stepSize

        for slice in slices:
            for ch in range(56):
                if ch % 2 == 0 and oddOnly is True:
                    continue
                else:
                    # reset count used for initial loop
                    count = 0
                    # reset low and high eye values
                    lowval = 0
                    highval = 0
                    index = 0
                    for list in caldat[(slice, ch)]['SHMOO2']:
                        # if there were no fails on the current step
                        if list[0] <= 100:
                            # for the first passing step set the low and high to the offset value
                            if count == 0:
                                lowval = list[1]
                                lowvalindex = index
                                highval = list[1]
                                highvalindex = index
                            # find the high value for the eye
                            if list[1] >= highval:
                                highval = list[1]
                                highvalindex = index

                            count += 1
                        index = index + 1
                    if count == 0:
                        lowval = -999
                        lowvalindex = -999
                        highval = -999
                        highvalindex = -999
                    else:
                        lowval = caldat[(slice, ch)]['SHMOO2'][lowvalindex - 1][1]
                        highval = caldat[(slice, ch)]['SHMOO2'][highvalindex + 1][1]
                        count = count + 1
                    self.Log('info', 'CH {}, SLICE {}, Low Value: {}, High Value: {}, Width: {}, Width Steps: {}'.format(ch, slice, lowval, highval, highval - lowval, count))
                    caldat[(slice, ch)]['LEFTEDGE2'] = lowval
                    caldat[(slice, ch)]['LEFTEDGEINDEX2'] = lowvalindex
                    caldat[(slice, ch)]['RIGHTEDGE2'] = highval
                    caldat[(slice, ch)]['RIGHTEDGEINDEX2'] = highvalindex
                    caldat[(slice, ch)]['EYEWIDTH2'] = highval - lowval
                    caldat[(slice, ch)]['EYECENTER2'] = lowval + ((highval - lowval) / 2)
                    caldat[(slice, ch)]['EYESTEPS2'] = count

                    # find eye center
                    centerRange = caldat[(slice, ch)]['LEFTEDGE2'] + ((caldat[(slice, ch)]['RIGHTEDGE2'] - caldat[(slice, ch)]['LEFTEDGE2']) / 2)

                    caldat[(slice, ch)]['ETOCALVALUE'] = int(round(centerRange - (calibrationPeriod / 2)))

        CALIBRATION_PATH = os.path.join(repo_root_path, self.hpcc.hpccConfigs.CALIBRATION_DIR)
        self.Log('info', 'Calibration path = {}'.format(CALIBRATION_PATH))
        time_string = time.strftime("%Y%m%d%H%M%S")
        self.Log('info', 'ETO Calibration Values')
        for slice in slices:
            fout = open(os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_ac_cal_eo.txt".format(self.hpcc.instrumentblt.SerialNumber, self.slot, slice)), 'w')
            #               fout = open(os.path.join(CALIBRATION_PATH, r"{}_{}_{}_ac_cal_eo_{}.txt".format(hpcc.instrumentblt.SerialNumber, slot, slice, time_string)), 'w')
            for ch in range(56):
                fout.write("{},{}\n".format(ch, caldat[(slice, ch)]['ETOCALVALUE']))
                self.Log('info', 'SLICE: {}, CH: {}, CAL: {}'.format(slice, ch, caldat[(slice, ch)]['ETOCALVALUE']))

            fout.close()

    def findLTHCal(self, period, coarseetopatdata, coarseotepatdata, fineetopatdata, fineotepatdata):

        self.calType = 'LowToHighLoopback'
        hpcc = self.hpcc

        # set up data structure to hold shmoo results
        caldat = {}
        slices = [0, 1]
        oddOnly = False
        for slice in slices:
            for ch in range(56): caldat[(slice, ch)] = {'ETOCALVALUE': -999, 'LTHCALVALUE': -999, 'COARSE': -999, 'COARSE_FAILS': -999, 'LEFTEDGE': -999, 'RIGHTEDGE': -999, 'LEFTEDGEINDEX': -999,
                                                        'RIGHTEDGEINDEX': -999, 'EYECENTER': -999, 'EYEWIDTH': -999, 'SHMOO': [], 'EYESTEPS': -999,
                                                        'LEFTEDGE2': -999, 'RIGHTEDGE2': -999, 'LEFTEDGEINDEX2': -999, 'RIGHTEDGEINDEX2': -999, 'EYECENTER2': -999, 'EYEWIDTH2': -999, 'SHMOO2': [],
                                                        'EYESTEPS2': -999}

        calibrationPeriod = period

        # # Set even to odd loopback
        hpcc.cal.SetLowToHighChannelLoopback()
        for ac in hpcc.ac:
            ac.internalLoopback = False

        self._clearcaloffset(hpcc)
        self.Log('info', 'test slot {}'.format(hpcc.slot))

        for slice in slices:
            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0}, True, channels='ALL_PINS')


        # execute odd pattern
        self._executeLTHcoarsepat(hpcc, coarseetopatdata, calibrationPeriod / PICO_SECONDS_PER_SECOND)
        startChannel = 1
        # get number of fails for each odd pin, offset by the expected count and write to results structure
        for slice in slices:
            for ch in range(56):
                if ch % 2 == startChannel:
                    captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                    caldat[(slice, ch)]['COARSE_FAILS'] = captureCount.ChannelFailCaptureCount - 256

        # execute odd pattern
        self._executeLTHcoarsepat(hpcc, coarseotepatdata, calibrationPeriod / PICO_SECONDS_PER_SECOND)
        startChannel = 0
        # get number of fails for each even pin, offset by the expected count and write to results structure
        for slice in slices:
            for ch in range(56):
                if ch % 2 == startChannel:
                    captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                    caldat[(slice, ch)]['COARSE_FAILS'] = captureCount.ChannelFailCaptureCount - 256


        # Set coarse Values
        for slice in slices:
            for ch in range(56):
                caldat[(slice, ch)]['COARSE'] = (caldat[(slice, ch)]['COARSE_FAILS'] * calibrationPeriod) - (1.5 * calibrationPeriod)
                self.Log('info', 'Slice: {}, CH: {}, Starting point from COARSE data: {}'.format(slice, ch, caldat[(slice, ch)]['COARSE']))

        steps = 16
        stepSize = calibrationPeriod / 4
        mtv = 1
        mask = 0

        # slice0/1 odd
        currentOffset = 0
        hpcc.ac[0].WritePattern(0, fineetopatdata)
        hpcc.ac[1].WritePattern(0, fineetopatdata)

        for step in range(steps):
            startChannel = 1
            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['COARSE'] + currentOffset) / PICO_SECONDS_PER_SECOND}, True,
                                                       channels=[ch])
                        hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)

            for slice in slices:
                hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
                hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].captureAll = False
                hpcc.ac[slice].SetPeriod(calibrationPeriod / PICO_SECONDS_PER_SECOND)
                self.hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
            self.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in slices:
                self.hpcc.ac[slice].WaitForCompletePattern()

            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['SHMOO'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['COARSE'] + currentOffset)])

            currentOffset = currentOffset + stepSize

        # slice0/1 even
        currentOffset = 0
        hpcc.ac[0].WritePattern(0, fineotepatdata)
        hpcc.ac[1].WritePattern(0, fineotepatdata)

        for step in range(steps):
            startChannel = 0
            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['COARSE'] + currentOffset) / PICO_SECONDS_PER_SECOND}, True,
                                                       channels=[ch])
                        hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)

            for slice in slices:
                hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
                hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].captureAll = False
                hpcc.ac[slice].SetPeriod(calibrationPeriod / PICO_SECONDS_PER_SECOND)
                self.hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
            self.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in slices:
                self.hpcc.ac[slice].WaitForCompletePattern()

            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['SHMOO'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['COARSE'] + currentOffset)])

            currentOffset = currentOffset + stepSize

        # for slice in slices:
        #     for ch in range(56):
        #         self.Log('info', 'CH {}, SLICE {}'.format(ch, slice))
        #         for list in caldat[(slice, ch)]['SHMOO']:
        #             self.Log('info', 'FAILS: {} OFFSET: {}'.format(list[0], list[1]))

        for slice in slices:
            for ch in range(56):
                if ch % 2 == 0 and oddOnly is True:
                    continue
                else:
                    # reset count used for initial loop
                    count = 0
                    # reset low and high eye values
                    lowval = 0
                    highval = 0
                    index = 0
                    for list in caldat[(slice, ch)]['SHMOO']:
                        # if there were no fails on the current step
                        if list[0] <= 40000:
                            # for the first passing step set the low and high to the offset value
                            if count == 0:
                                lowval = list[1]
                                lowvalindex = index
                                highval = list[1]
                                highvalindex = index
                            # find the high value for the eye
                            if list[1] >= highval:
                                highval = list[1]
                                highvalindex = index

                            count += 1
                        index = index + 1
                    if count == 0:
                        lowval = -999
                        lowvalindex = -999
                        highval = -999
                        highvalindex = -999
                    else:
                        lowval = caldat[(slice, ch)]['SHMOO'][lowvalindex - 1][1]
                        highval = caldat[(slice, ch)]['SHMOO'][highvalindex + 1][1]
                        count = count + 1

                    self.Log('info', 'CH {}, SLICE {}, Low Value: {}, High Value: {}, Width: {}, Width Steps: {}'.format(ch, slice, lowval, highval, highval - lowval, count))
                    caldat[(slice, ch)]['LEFTEDGE'] = lowval
                    caldat[(slice, ch)]['LEFTEDGEINDEX'] = lowvalindex
                    caldat[(slice, ch)]['RIGHTEDGE'] = highval
                    caldat[(slice, ch)]['RIGHTEDGEINDEX'] = highvalindex
                    caldat[(slice, ch)]['EYEWIDTH'] = highval - lowval
                    caldat[(slice, ch)]['EYECENTER'] = lowval + ((highval - lowval) / 2)
                    caldat[(slice, ch)]['EYESTEPS'] = count



###############
                                            #
        stepSize = calibrationPeriod / 16


        #calculate max steps
        maxWidth = 0
        for slice in slices:
            for ch in range(56):
                if caldat[(slice, ch)]['EYESTEPS'] > maxWidth:
                    maxWidth = caldat[(slice, ch)]['EYESTEPS']
        steps = maxWidth * 4


        # slice0/1 odd
        currentOffset = 0
        hpcc.ac[0].WritePattern(0, fineetopatdata)
        hpcc.ac[1].WritePattern(0, fineetopatdata)


        for step in range(steps):
            startChannel = 1
            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset)/PICO_SECONDS_PER_SECOND}, True,channels=[ch])
                        hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)

            for slice in slices:
                hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
                hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].captureAll = False
                hpcc.ac[slice].SetPeriod(calibrationPeriod / PICO_SECONDS_PER_SECOND)
                self.hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
            self.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in slices:
                self.hpcc.ac[slice].WaitForCompletePattern()

            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['SHMOO2'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset)])

            currentOffset = currentOffset + stepSize

        #even pins
        currentOffset = 0
        hpcc.ac[0].WritePattern(0, fineotepatdata)
        hpcc.ac[1].WritePattern(0, fineotepatdata)


        for step in range(steps):
            startChannel = 0
            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset)/PICO_SECONDS_PER_SECOND}, True,channels=[ch])
                        hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)

            for slice in slices:
                hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
                hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].captureAll = False
                hpcc.ac[slice].SetPeriod(calibrationPeriod / PICO_SECONDS_PER_SECOND)
                self.hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
            self.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in slices:
                self.hpcc.ac[slice].WaitForCompletePattern()

            for slice in slices:
                for ch in range(56):
                    if ch % 2 == startChannel:
                        captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                        caldat[(slice, ch)]['SHMOO2'].append([captureCount.ChannelFailCaptureCount, (caldat[(slice, ch)]['LEFTEDGE'] + currentOffset)])

            currentOffset = currentOffset + stepSize


        # for slice in slices:
        #     for ch in range(56):
        #         self.Log('info', 'CH {}, SLICE {}'.format(ch, slice))
        #         for list in caldat[(slice, ch)]['SHMOO2']:
        #             self.Log('info', 'FAILS: {} OFFSET: {}'.format(list[0], list[1]))




        for slice in slices:
            for ch in range(56):
                if ch % 2 == 0 and oddOnly is True:
                    continue
                else:
                    # reset count used for initial loop
                    count = 0
                    # reset low and high eye values
                    lowval = 0
                    highval = 0
                    index = 0
                    for list in caldat[(slice, ch)]['SHMOO2']:
                        # if there were no fails on the current step
                        if list[0] <= 100:
                            # for the first passing step set the low and high to the offset value
                            if count == 0:
                                lowval = list[1]
                                lowvalindex = index
                                highval = list[1]
                                highvalindex = index
                            # find the high value for the eye
                            if list[1] >= highval:
                                highval = list[1]
                                highvalindex = index

                            count += 1
                        index = index + 1
                    if count == 0:
                        lowval = -999
                        lowvalindex = -999
                        highval = -999
                        highvalindex = -999
                    else:
                        lowval = caldat[(slice, ch)]['SHMOO2'][lowvalindex - 1][1]
                        highval = caldat[(slice, ch)]['SHMOO2'][highvalindex + 1][1]
                        count = count + 1
                    self.Log('info', 'CH {}, SLICE {}, Low Value: {}, High Value: {}, Width: {}, Width Steps: {}'.format(ch, slice, lowval, highval, highval - lowval, count))
                    caldat[(slice, ch)]['LEFTEDGE2'] = lowval
                    caldat[(slice, ch)]['LEFTEDGEINDEX2'] = lowvalindex
                    caldat[(slice, ch)]['RIGHTEDGE2'] = highval
                    caldat[(slice, ch)]['RIGHTEDGEINDEX2'] = highvalindex
                    caldat[(slice, ch)]['EYEWIDTH2'] = highval - lowval
                    caldat[(slice, ch)]['EYECENTER2'] = lowval + ((highval - lowval) / 2)
                    caldat[(slice, ch)]['EYESTEPS2'] = count

                    # find eye center
                    centerRange = caldat[(slice, ch)]['LEFTEDGE2'] + ((caldat[(slice, ch)]['RIGHTEDGE2'] - caldat[(slice, ch)]['LEFTEDGE2']) / 2)

                    caldat[(slice, ch)]['LTHCALVALUE'] = int(round(centerRange - (calibrationPeriod / 2)))

        CALIBRATION_PATH = os.path.join(repo_root_path, self.hpcc.hpccConfigs.CALIBRATION_DIR)
        self.Log('info', 'Calibration path = {}'.format(CALIBRATION_PATH))
        time_string = time.strftime("%Y%m%d%H%M%S")
        self.Log('info', 'LTH Calibration Values')
        for slice in slices:
            fout = open(os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_ac_cal_lh.txt".format(self.hpcc.instrumentblt.SerialNumber, self.slot, slice)), 'w')
            #               fout = open(os.path.join(CALIBRATION_PATH, r"{}_{}_{}_ac_cal_lh_{}.txt".format(hpcc.instrumentblt.SerialNumber, slot, slice, time_string)), 'w')
            for ch in range(56):
                fout.write("{},{}\n".format(ch, caldat[(slice, ch)]['LTHCALVALUE']))
                self.Log('info', 'SLICE: {}, CH: {}, CAL: {}'.format(slice, ch, caldat[(slice, ch)]['LTHCALVALUE']))
            fout.close()

