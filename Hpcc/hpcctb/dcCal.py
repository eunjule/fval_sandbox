################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: dcCal.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC DC Calibration
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 01/27/16
#       Group: HDMT FPGA Validation
################################################################################

import os
import tarfile
import tempfile

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')

from Common import fval
from Hpcc import hpcc_configs as hpccConfigs
from Hpcc.hpcctb.assembler import PatternAssembler
import Hpcc.instrument.hpccAcRegs as ac_registers

PICO_SECONDS_PER_SECOND = 1000000000000
VOX_BASE = 1.5
STABLE_ITER = 5

class DcCalibration(fval.Object):
    def __init__(self, hpcc, rc, maskPin = [-1]):
        #self.env = Env(self)
        self.slot = hpcc.slot
        self.rc = rc
        self.maskPin = maskPin
        self.CALIBRATION_PATH = os.path.join(repo_root_path, hpccConfigs.CALIBRATION_DIR)
        self.Log('info', 'DC Calibration path = {}'.format(self.CALIBRATION_PATH))
        self.hpcc = hpcc
        self.CALIBRATION_PERIOD = hpcc.ac[0].minQdrMode / PICO_SECONDS_PER_SECOND
        if hpccConfigs.HPCC_SLICE1_ONLY:
            self.CALIBRATION_PERIOD = hpcc.ac[1].minQdrMode / PICO_SECONDS_PER_SECOND
        self._snapshotOffsets = {}
        self._snapshotLengths = {}

        self.enabledIlas = []
        self.enabledSimTraces = []
        self.enabledCalDumps = []

        if hpccConfigs.HPCC_TAKE_SNAPSHOT:
            if hpccConfigs.HPCC_SLICE1_ONLY:
                slices = [1]
            else:
                slices = [0,1]
            for slice in slices:
                self.enabledIlas.append((self.slot, slice))
                self.enabledSimTraces.append((self.slot, slice))
                self.enabledCalDumps.append((self.slot, slice))
    
        
    def DcCalibration(self):
        cal = self.DcCalEvenToOdd()
        if not (hpccConfigs.HPCC_SLICE0_ONLY or hpccConfigs.HPCC_SLICE1_ONLY):
            cal = self.DCCalLowToHigh(cal)
        if (hpccConfigs.HPCC_SLICE0_ONLY or hpccConfigs.HPCC_SLICE1_ONLY):
            self.Log('info', 'Skipping DC Low to High Cal Because only one slice is activated')
            
        # write to file
        for slice in [0, 1]:
            if hpccConfigs.HPCC_SLICE0_ONLY and slice != 0:
                continue
            if hpccConfigs.HPCC_SLICE1_ONLY and slice != 1:
                continue
                
            fout = open(os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_dc_cal.txt".format(self.hpcc.instrumentblt.SerialNumber, self.slot, slice)), 'w')
            for ch in range(56):
                fout.write("{},{}\n".format(ch, cal[(slice, ch)]))
            fout.close()
    
    def DcCalEvenToOdd(self):
        # # Set even to odd loopback
        self.hpcc.cal.SetEvenToOddChannelLoopback()
        for ac in self.hpcc.ac:
            ac.internalLoopback = False        
        slot = self.slot
        cal = {}
        
        for slice in [0, 1]:
            if hpccConfigs.HPCC_SLICE0_ONLY and slice != 0:
                continue
            if hpccConfigs.HPCC_SLICE1_ONLY and slice != 1:
                continue
            self.Log('info', 'Calibrating HPCC DC VOX and TermVRef slot {} slice {} Even to Odd Loopback, VCL = 0V, VCH = 3V'.format(slot, slice))
            
            for ch in range(56):
                cal[(slice, ch)] = VOX_BASE
            # odd channel
            pattern = PatternAssembler()
            pattern.LoadString("""\
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
                BlockFails 1
                Drive0CompareLVectors length=1024
                BlockFails 0
                %repeat 256
                V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
                %end
                V link=0, ctv=1, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1  
                %repeat 256                                                                               
                V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            """)        
            pdata = pattern.Generate() 
            self.RunDcCalEvenToOdd(pdata, slot, slice, cal, 1)
            # even channel
            pattern = PatternAssembler()
            pattern.LoadString("""\
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
                BlockFails 1
                Drive0CompareLVectors length=1024
                BlockFails 0
                %repeat 256
                V link=0, ctv=0, mtv=1, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L  
                %end
                V link=0, ctv=1, mtv=1, lrpt=0, data=0v1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L  
                %repeat 256                                                                               
                V link=0, ctv=0, mtv=1, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L  
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
            """)        
            pdata = pattern.Generate() 
            self.RunDcCalEvenToOdd(pdata, slot, slice, cal, 0)
            
        return cal
            
    # start with cal for even to odd
    def DCCalLowToHigh(self, cal):
        # # Set even to odd loopback
        self.hpcc.cal.SetLowToHighChannelLoopback()
        for ac in self.hpcc.ac:
            ac.internalLoopback = False
        slot = self.slot        
        
        # odd channel
        self.Log('info', 'Calibrating HPCC DC slot {} Odd Channels Low to High Loopback'.format(slot))
        pattern = PatternAssembler()
        pattern.LoadString("""\
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            BlockFails 1
            Drive0CompareLVectors length=1024
            BlockFails 0
            %repeat 256
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
            %end
            V link=0, ctv=1, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1  
            %repeat 256                                                                               
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)        
        pdata = pattern.Generate() 
        self.RunDcCalLowToHigh(pdata, slot, cal, 1)
        # even channel
        self.Log('info', 'Calibrating HPCC slot {} Even Channels Low to High Loopback'.format(slot))
        pattern = PatternAssembler()
        pattern.LoadString("""\
            S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
            BlockFails 1
            Drive0CompareLVectors length=1024
            BlockFails 0
            %repeat 256
            V link=0, ctv=0, mtv=1, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L  
            %end
            V link=0, ctv=1, mtv=1, lrpt=0, data=0v1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L  
            %repeat 256                                                                               
            V link=0, ctv=0, mtv=1, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L  
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
        """)        
        pdata = pattern.Generate() 
        self.RunDcCalLowToHigh(pdata, slot, cal, 0)

        return cal

    def RunDcCalEvenToOdd(self, pattern, slot, slice, cal, startChannel):
        hpcc = self.hpcc
        mtv = 1
        mask = 0
        hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
        hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
        hpcc.ac[slice].WritePattern(0, pattern)
        hpcc.ac[slice].captureAll = False        
        hpcc.ac[slice].SetupCapture(0x20000, ac_registers.CaptureControl.PIN_STATE)
        period = self.CALIBRATION_PERIOD
        hpcc.ac[slice].SetPeriod(period)
        
        passed = False
        stableCount = 0
        while (not passed) or stableCount < STABLE_ITER:
            #print(cal)
            for ch in range(56):
                hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0, 'VOX': cal[(slice, ch)], 'termVRef': cal[(slice, ch)]}, True, channels = [ch])
            if hpccConfigs.HPCC_TAKE_DCCAL_SNAPSHOT:
                self.WriteExecutePatternAndSetupCapture(slot, slice, pattern)
            else:
                hpcc.ac[slice].ExecutePattern()             
            passed = self.GetErrorFromCapture(hpcc, slice, startChannel, cal, slot)
            if passed:
                stableCount += 1
                self.Log('info', 'iteration has been stable for {} loops'.format(stableCount))
                if hpccConfigs.HPCC_TAKE_DCCAL_SNAPSHOT:
                    if startChannel == 1:
                        filename = 'OddChannel_snapshot_slice{}_iteration_{}'.format(slice,stableCount)
                    else:
                        filename = 'EvenChannel_snapshot_slice{}_iteration_{}'.format(slice, stableCount)
                    self.TakeDCCalSnapshot(filename,slice,slot)
            else:
                stableCount = 0
        
    def RunDcCalLowToHigh(self, pattern, slot, cal, startChannel):
        hpcc = self.hpcc
        for slice in [0,1]:
            mtv = 1
            mask = 0
            hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
            hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
            hpcc.ac[slice].WritePattern(0, pattern)
            hpcc.ac[slice].captureAll = False        
            hpcc.ac[slice].SetupCapture(0x20000, ac_registers.CaptureControl.PIN_STATE)
            period = self.CALIBRATION_PERIOD
            hpcc.ac[slice].SetPeriod(period)
        
        passed = False
        stableCount = 0
        while (not passed) or stableCount < STABLE_ITER:
            for slice in [0,1]:
                #print(cal)
                for ch in range(56):
                    hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0, 'VOX': cal[(slice, ch)], 'termVRef': cal[(slice, ch)]}, True, channels = [ch])
                hpcc.ac[slice].PrestagePattern()
            
            self.rc.send_sync_pulse()
            completed = hpcc.ac[slice].WaitForCompletePattern()              
            
            bothPass = True
            for slice in [0,1]:
                bothPass = bothPass and self.GetErrorFromCapture(hpcc, slice, startChannel, cal, slot)
            passed = bothPass
            if passed:
                stableCount += 1
                self.Log('info', 'iteration has been stable for {} loops'.format(stableCount))
            else:
                stableCount = 0
        
        
    def GetErrorFromCapture(self, hpcc, slice, startChannel, cal, slot):
        passed = True
        hpcc.DumpCapture(slice)
        for ch in range(startChannel, 56, 2):
            channelFailCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount').Pack()
            #print(channelFailCount)
            if channelFailCount > 2:
                if hpccConfigs.HPCC_TAKE_DCCAL_SNAPSHOT:
                    self.Log('info','Calibration failed for slice {} channel {} . Creating Snapshot '.format(slice, ch))
                    if startChannel == 1:
                        filename = 'OddChannel_snapshot_slice{}'.format(slice)
                    else:
                        filename = 'EvenChannel_snapshot_slice{}'.format(slice)
                    self.TakeDCCalSnapshot(filename,slice,slot)
                self.Log('error', 'slice {} channel {} channelFailCount = {}, stuck at 1??'.format(slice, ch, channelFailCount))
            elif channelFailCount > 1:
                #cal[(slice, ch)] += 0.1
                #passed = False
                #self.Log('info','adjusting channel {} to {}'.format(ch, cal[(slice, ch)]))
                self.Log('info','double 1 on slice {} channel {}, vox {}'.format(slice, ch, cal[(slice, ch)]))
            elif channelFailCount == 0:
                cal[(slice, ch)] -= 0.1
                passed = False
                self.Log('info','adjusting slice {} channel {} to {}'.format(slice, ch, cal[(slice, ch)]))
                if cal[(slice, ch)] < 0.5:
                    if hpccConfigs.HPCC_TAKE_DCCAL_SNAPSHOT:
                        self.Log('info','Calibration failed for slice {} channel {}. Creating Snapshot '.format(slice, ch))
                        if startChannel == 1:
                            filename = 'OddChannel_snapshot_slice{}'.format(slice)
                        else:
                            filename = 'EvenChannel_snapshot_slice{}'.format(slice)
                        self.TakeDCCalSnapshot(filename,slice,slot)
                    self.Log('error', 'Adjusting VOX and termVRef to {} V, too low, stuck at 0??'.format(cal[(slice, ch)]))
        return passed

        
    def WriteExecutePatternAndSetupCapture(self, slot, slice, pattern = None, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, CaptureCTVAfterMaxFail = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500):
        hpcc = self.hpcc

        if not(pattern is None):  # Write pattern to memory
            hpcc.ac[slice].WritePattern(offset, pattern)
            self._snapshotOffsets[(slot, slice)] = offset
            self._snapshotLengths[(slot, slice)] = len(pattern)

        if captureAddress is None:
            if pattern is None:
                raise Exception('If no pattern is set, captureAddress must be set!')
            else:
                captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern))
        # Setup capture
        hpcc.ac[slice].SetCaptureBaseAddress(captureAddress)
        hpcc.ac[slice].SetCaptureCounts(maxFailCountPerPatern = maxFailCountPerPatern, maxFailCaptureCount = maxFailCaptureCount, maxCaptureCount = maxCaptureCount, captureLength = captureLength)
        hpcc.ac[slice].SetCaptureControl(captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap, CaptureCTVAfterMaxFail)
        
        
        if (slot, slice) in self.enabledIlas:
            # Arm ILAs just before pattern execution
            hpcc.ac[slice].LoadAndArmIlas(slot, slice)
            
        hpcc.ac[slice].PrestagePattern(start, numberOfTries)
        # Send sync pulse    
        self.rc.send_sync_pulse()
        # Wait for complete
        completed = False
        if waitForComplete:
            completed = hpcc.ac[slice].WaitForCompletePattern(numberOfTries)
            
        if waitForComplete and not maskFailureToComplete:
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
        
        if (slot, slice) in self.enabledIlas:
            # Dump ILAs
            hpcc.ac[slice].DumpIlas(slot, slice)    
        
        
        # Return pattern status
        return completed, hpcc.ac[slice].Read('PatternEndStatus').Pack()        
        
        
    def TakeDCCalSnapshot(self, filename, slice, slot):
        
        offsets = self._snapshotOffsets
        lengths = self._snapshotLengths
        def MakeTar(filename, sourceDir):
            if not(filename.endswith('.tar.gz')):
                filename = '{}.tar.gz'.format(filename)
            self.Log('info', 'Creating snapshot {}'.format(filename))
            with tarfile.open(filename, "w:gz") as tar:
                tar.add(sourceDir, arcname = os.path.basename(sourceDir))
        snapshotDir = tempfile.mkdtemp()
        self.Log('debug', 'Temporary snapshot directory {}'.format(snapshotDir))
        path = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}'.format(slot, slice))
        os.makedirs(path)
        self.hpcc.ac[slice].TakeRegistersSnapshot(os.path.join(path, 'registers.csv'))
        self.hpcc.ac[slice].TakeTimingSnapshot(slot, slice, os.path.join(path, 'timingInfo.txt'))
        if (slot, slice) in self.enabledCalDumps:
            self.hpcc.ac[slice].TakeCalSnapshot(slot, slice, os.path.join(path, 'AcCalibration.csv'))
        if not hpccConfigs.HPCC_NO_PAT_SNAPSHOT:
            patternMemoryPath = os.path.join(path, 'PatternMemory')
            os.makedirs(patternMemoryPath)
            offset = None
            if type(offsets) == dict:
                offset = offsets[(slot, slice)]
            else:
                offset = offsets
            length = None
            if type(lengths) == dict:
                length = lengths[(slot, slice)]
            else:
                length = lengths
            self.hpcc.ac[slice].TakePatternMemorySnapshot(slot, slice, offset, length, patternMemoryPath)
            self.hpcc.ac[slice].TakeCaptureMemorySnapshot(slot, slice, patternMemoryPath)
            if (slot, slice) in self.enabledSimTraces:
                simPath = os.path.join(path, 'Sim')
                os.makedirs(simPath)
                self.hpcc.ac[slice].TransferSimTraces(slot, slice, simPath)
        if (slot, slice) in self.enabledIlas:
            ilaPath = os.path.join(path, 'ILAs')
            os.makedirs(ilaPath)
            self.hpcc.ac[slice].TransferIlas(slot, slice, ilaPath)
        MakeTar(filename, snapshotDir)