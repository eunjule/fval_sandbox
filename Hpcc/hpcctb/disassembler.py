################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: disassembler.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC AC single slice pattern (.obj) disassembler
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/24/15
#       Group: HDMT FPGA Validation
################################################################################

import os

if __name__ == '__main__':
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
from _build.bin import hpcctbc
from Hpcc.hpcctb import symbols
from Hpcc import hpcctb


def disassemble(array, offset = 0):
    type = hpcctbc.ReadPatternWordType(array, offset)
    result = ''
    if type == hpcctb.symbols.PATTERN.VECTOR_WORD:
        link, ctv, mtv, lrpt, data, pvcrsvd = hpcctbc.ReadVectorWord(array, offset)
        result = 'V link={}, ctv={}, mtv={}, lrpt={}, data=0x{:x}'.format(link, ctv, mtv, lrpt, data)
    elif type == hpcctb.symbols.PATTERN.METADATA_WORD:
        data = hpcctbc.ReadMetadataWord(array, offset)
        result = 'M data=0x{:x}'.format(data)
    elif type == hpcctb.symbols.PATTERN.PINSTATE_WORD:
        stype, data, undefined, unused = hpcctbc.ReadPinStateWord(array, offset)
        result = 'S stype={}, data=0x{:x}'.format(stype, data)
    elif type == hpcctb.symbols.PATTERN.INSTRUCTION_WORD:
        optype, extop, action, opdest, opsrc, aluop, vptype, vpop, invcond, cond, base, br, dest, regB, regA, imm, swrsvd, rsvd = hpcctbc.ReadInstructionWord(array, offset)
        result = 'I optype={}, extop={}, action={}, opdest={}, opsrc={}, aluop={}, vptype={}, vpop={}, invcond={}, cond={}, base={}, br={}, dest={}, regB={}, regA={}, imm={}, swrsvd={}'.format(optype, extop, action, opdest, opsrc, aluop, vptype, vpop, invcond, cond, base, br, dest, regB, regA, imm, swrsvd)
    return result

if __name__ == "__main__":
    # Parse command line options
    import argparse
    parser = argparse.ArgumentParser(description = 'HPCC AC single slice pattern (.obj) disassembler')
    parser.add_argument('objfile', help = 'Pattern filename', type = str)
    args = parser.parse_args()
    
    data = open(args.objfile, 'rb').read()
    vectorCount = len(data) // 16

    for i in range(vectorCount):
        d = disassemble(data, i * 16)
        print(d)


