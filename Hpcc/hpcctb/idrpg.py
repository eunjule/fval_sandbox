################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: idrpg.py
#-------------------------------------------------------------------------------
#     Purpose: Instruction Density Random Pattern Generator
#-------------------------------------------------------------------------------
#  Created by: Mark E. Tolonen
#        Date: 02/16/16
#       Group: HDMT FPGA Validation
################################################################################

import random
import collections
from pprint import pprint
from Hpcc.hpcctb.assembler import PatternAssembler

def rindex(iterable,value):
    for i,v in enumerate(reversed(iterable)):
        if v == value:
            return -i-1
    raise ValueError('{} is not in list',value)

class RegisterUnderrun(RuntimeError): pass
class RegisterOverrun(RuntimeError): pass

class State:

    def __init__(self,parameters):
        self._parameters = parameters
        self._patterns = collections.OrderedDict()
        self._subroutines = collections.OrderedDict()
        self._max_registers = 32
        self._register = 0
        self._depth = 0
        self._label = 0
        self._pattern = 0
        self._subroutine = 0
        self._costs = 0
        self._patternid = 0
        self._loop_stack = []

    def alloc_register(self):
        r = self._register
        self._register += 1
        if self._register > self._max_registers:
            raise RegisterOverrun
        return r

    def free_register(self):
        self._register -= 1
        if self._register < 0:
            raise RegisterUnderrun

    def label(self):
        self._label += 1
        return 'LABEL{}'.format(self._label)

    def pattern_label(self):
        self._pattern += 1
        return 'PATTERN{}'.format(self._pattern)

    def subroutine_label(self):
        self._subroutine += 1
        return 'SUB{}'.format(self._subroutine)

    def pattern_id(self):
        self._patternid += 1
        return self._patternid

    def start_loop(self):
        self._loop_stack.append(len(self._costs))
        self._costs.append(0) # zero is special and maintains its value

    def end_loop(self,count):
        loop_start = self._loop_stack.pop()
        # zero is eliminated and portion after zero is a tuple of (count,costs)
        self._costs,loop = self._costs[:loop_start],self._costs[loop_start + 1:]
        if count > 1:
            self._costs.append((count-1,loop))
        elif count == 1:
            self._costs.extend(loop)
        loop = loop[:] # shallow copy loop for editing final jump.

        # This should be always true for a loop because it ends in a DecRegister and Goto.
        assert loop[-1] < self._parameters['jump_cost']

        loop[-1] -= self._parameters['jump_cost'] + 1 # final loop doesn't take jump. Add back jump cost, but -1 for instruction.
        self._costs.extend(loop)

    def start_cost_profile(self):
        self._costs = []

    def get_cost_profile(self):
        return self._costs

    def cost(self,*prices):
        for price in prices:
            assert price != 0 # Zero is special and used to mark loops.
            if self._costs:
                last = self._costs[-1]
                if type(last) == int and type(price) == int and (last < 0 and price < 0 or last > 0 and price > 0):
                    self._costs[-1] += price
                else:
                    self._costs.append(price)
            else:
                self._costs.append(price)

    def compute_cost(self):
        def apply_costs(current_cost,min_cost,costs):
            for item in costs:
                if type(item) == int:
                    current_cost = min(current_cost + item, self._parameters['prestage_max'])
                    min_cost = min(min_cost,current_cost)
                    #print(item,current_cost,min_cost)
                else:
                    count,more_costs = item
                    for i in range(count):
                        current_cost,min_cost = apply_costs(current_cost,min_cost,more_costs)
            return current_cost,min_cost
        return apply_costs(0,self._parameters['prestage_max'],self._costs)

class Block:

    def __init__(self,state):
        self.state = state
        pass

class PCall(Block):

    def __init__(self,state):
        super().__init__(state)

    def code(self):
        self.state.cost(-1,self.state._parameters['jump_cost']) # PatternId amd PCall
        pattern = random.choice(list(self.state._patterns))
        costs = self.state._patterns[pattern]
        self.state.cost(*costs)       # Pattern costs
        return '''\
        PatternId {}
        PCall `{}`
        '''.format(self.state.pattern_id(),pattern)

class Call(Block):

    def __init__(self,state):
        super().__init__(state)

    def code(self):
        self.state.cost(self.state._parameters['jump_cost']) # Call
        sub = random.choice(list(self.state._subroutines))
        costs = self.state._subroutines[sub]
        self.state.cost(*costs)    # Subroutine costs
        return '''\
        Call `{}`
        '''.format(sub)

class Subroutine(Block):

    def __init__(self,state):
        super().__init__(state)
        self.blocks = [Loop,FlatVector,Call] # Call must be last
        self.deep_blocks = [FlatVector]

    def code(self):
        return (self.prefix() +
                self.content() +
                self.postfix())

    def prefix(self):
        self._label = self.state.subroutine_label()
        return '{}:\n'.format(self._label)

    def content(self):
        s = []
        self.state.start_cost_profile()
        for i in range(random.randint(*self.state._parameters['sub_items'])):
            if self.state._depth < self.state._parameters['depth_max']:
                if self.state._subroutines:
                    block = random.choice(self.blocks)
                else:
                    block = random.choice(self.blocks[:-1]) # exclude call if no subs available yet.
            else:
                block = random.choice(self.deep_blocks)
            s.append(block(self.state).code())
        return ''.join(s)

    def postfix(self):
        self.state.cost(self.state._parameters['jump_cost']) # Return
        self.state._subroutines[self._label] = self.state.get_cost_profile()
        return 'Return\n'

class Pattern(Block):

    def __init__(self,state):
        super().__init__(state)
        self.blocks = [Loop,FlatVector,Call]

    def code(self):
        return (self.prefix() +
                self.content() +
                self.postfix())

    def prefix(self):
        self._label = self.state.pattern_label()
        return '{}:\n'.format(self._label)

    def content(self):
        s = []
        self.state.start_cost_profile()
        for i in range(random.randint(*self.state._parameters['pat_items'])):
            block = random.choice(self.blocks)
            s.append(block(self.state).code())
        return ''.join(s)

    def postfix(self):
        self.state.cost(self.state._parameters['jump_cost']) # Return
        self.state._patterns[self._label] = self.state.get_cost_profile()
        return 'Return\n'

class Code(Block):

    def __init__(self,parameters):
        super().__init__(State(parameters))
        self.blocks = [PCall]

    def code(self):
        return (self.patterns_and_subs() +
                self.prefix() +
                self.plist() +
                self.postfix())

    def patterns_and_subs(self):
        s = []
        for i in range(random.randint(*self.state._parameters['subroutines'])):
            self.state._depth = 0 # Each subroutine starts at depth 0
            s.append(Subroutine(self.state).code())
        for i in range(random.randint(*self.state._parameters['patterns'])):
            self.state._depth = 0 # Each pattern starts at depth 0
            s.append(Pattern(self.state).code())
        random.shuffle(s)
        return ''.join(s)

    def prefix(self):
        self.state.start_cost_profile()
        self.state.cost(1,self.state._parameters['prestage_max']) # State and prestage vectors.
        return '''\
        START:
        S stype=IOSTATEJAM, data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
        RandomPassingEvenToOddNoSwitchVectors length={pre}
        '''.format(pre=self.state._parameters['prestage_max'])

    def plist(self):
        s = []
        for i in range(random.randint(*self.state._parameters['plists'])):
            block = random.choice(self.blocks)
            s.append(block(self.state).code())
        return ''.join(s)

    def postfix(self):
        self.state.cost(-1,1) # Instruction and State.
        return '''\
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        S stype=IOSTATEJAM, data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
        '''

class FlatVector(Block):

    def __init__(self,state):
        super().__init__(state)

    def code(self):
        length = random.randint(*self.state._parameters['flat_vectors'])
        self.state.cost(length)
        return '''\
        RandomPassingEvenToOddNoSwitchVectors length={len}
        '''.format(len=length)

class Loop(Block):

    def __init__(self,state):
        super().__init__(state)
        self.blocks = [Loop,FlatVector,Call] # Call must be last
        self.deep_blocks = [FlatVector]

    def code(self):
        return (self.prefix() +
                self.content() +
                self.postfix())

    def prefix(self):
        self._reg = self.state.alloc_register()
        self._label = self.state.label()
        self.state._depth += 1
        self.state.cost(-1) # Instruction
        self.state.start_loop()
        self._count = random.randint(*self.state._parameters['loops'])
        return '''\
        PushRegister {reg}
        SetRegister {reg},{count}
        {label}:
        '''.format(reg=self._reg,count=self._count,label=self._label)

    def content(self):
        s = []
        for i in range(random.randint(*self.state._parameters['loop_items'])):
            if self.state._depth < self.state._parameters['depth_max']:
                if self.state._subroutines:
                    block = random.choice(self.blocks)
                else:
                    block = random.choice(self.blocks[:-1]) # exclude call if no subs available yet.
            else:
                block = random.choice(self.deep_blocks)
            s.append(block(self.state).code())
        return ''.join(s)

    def postfix(self):
        self.state.cost(-1,self.state._parameters['jump_cost']) # Instruction and Goto
        self.state.free_register()
        self.state.end_loop(self._count)
        return '''\
            DecRegister {reg}
        GotoIfNonZero `{label}`
        PopRegister {reg}
        '''.format(label=self._label,reg=self._reg)

def code_generator(parameters):
    model = Code(parameters)
    code = {}
#    print('Generating code')
    code['listing'] = model.code()
#    print('Computing cost')
    code['cost'],code['min_cost'] = model.state.compute_cost()
#    print('DONE')
    code['costs'] = model.state.get_cost_profile()
    code['prediction'] = 'FAIL' if code['min_cost'] < 0 else 'PASS'
    #print(model.state._subroutines)
    #print(model.state._patterns)
    return code

if __name__ == '__main__':
    import time
    
    import __init__
    import hpcctb

    def assemble(source):
        # Run real pattern
        asm = PatternAssembler()
        asm.LoadString(source)
        return asm.Generate()

    random.seed(int(time.time()))

    parameters = {}
    parameters['prestage_max'] = 16384
    parameters['jump_cost']    = -90
    parameters['depth_max']    = 5
    parameters['sub_items']    = 2,5    # Min/max entries wrapped by a subroutine.
    parameters['pat_items']    = 2,5    # Min/max entries wrapped by a pattern.
    parameters['subroutines']  = 2,5    # Min/max number of subroutines to generate.
    parameters['patterns']     = 2,5    # Min/max number of patterns to generate.
    parameters['plists']       = 2,5    # Min/max number of P-List entries to generate.
    parameters['flat_vectors'] = 20,100 # Min/max length to generate for a RandomPassingEvenToOddNoSwitchVectors macro.
    parameters['loops']        = 2,5    # Min/max loop count for a loop.
    parameters['loop_items']   = 1,5    # Min/max entries wrapped by a loop.

    code = code_generator(parameters)

    print('''\
Seed         = {}
Cost         = {}
Minimum Cost = {}
Prediction   : {}\n'''.format(parameters['seed'],code['cost'],code['min_cost'],code['prediction']))

    # Not required, but formatted for nicer display.
    L = '\n'.join(x.strip() if x.rstrip().endswith(':') else '  '+x.strip() for x in code['listing'].splitlines())
    #print(L)
    #pprint(code['costs'])

    # Validate source
    pattern = assemble(code['listing'])
