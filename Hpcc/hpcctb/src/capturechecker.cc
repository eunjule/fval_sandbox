////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: capturechecker.cc
//------------------------------------------------------------------------------
//    Purpose: HPCC AC Capture Data Checker
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/03/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include <vector>     // std::vector

#include "capturechecker.h"
#include "defines.h"

static_assert(HPCC_CHANNELS >= 33, "Expecting at least 33 channels (or the checker code won't work)");
static_assert(HPCC_CHANNELS <= 64, "Expecting 64 channels of less (or the checker code won't work)");

namespace hpcctbc {

CaptureCheckerContext::CaptureCheckerContext(const uint8_t* array, size_t length, size_t blockCount, size_t captureCount, HpccAcSimulator* sim, size_t ratio)
{
    this->array = const_cast<uint8_t*>(array);
    this->length = length;
    this->blockCount = blockCount;
    this->captureCount = captureCount;
    this->sim = sim;
    this->ratio = ratio;
    this->firstFailOffset = UINT32_MAX;
}

CaptureCheckerContext::~CaptureCheckerContext()
{

}

CaptureChecker::CaptureChecker(size_t maxErrors, uint64_t channelsToMask)
{
    this->channelsToMask = channelsToMask;
    this->maxErrors = maxErrors;
    groupPinLaneData = 0;  // 0 = don't group
    Clear();
}

CaptureChecker::~CaptureChecker()
{
    if (disabledRules.size() > 0) {
        std::stringstream ss;
        ss << "CaptureChecker disabled rules: ";
        for (size_t i = 0; i < disabledRules.size(); i++) {
            ss << disabledRules[i] << " ";
        }
        LOG("debug") << ss.str();
    }
}

void CaptureChecker::Clear()
{
    enabledErrorsFound = 0;
    disabledErrorsFound = 0;
    totalErrorsFound = 0;
    totalMissingCapture = 0;
}

bool CaptureChecker::DisableRule(const std::string& id)
{
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter == disabledRules.end()) {
        disabledRules.push_back(id);
        return true;
    } else {
        return false;
    }
}

bool CaptureChecker::EnableRule(const std::string& id)
{
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter != disabledRules.end()) {
        disabledRules.erase(iter);
        return true;
    } else {
        return false;
    }
}

bool CaptureChecker::LogError(const std::string& id, const std::string& s)
{
    totalErrorsFound++;
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter != disabledRules.end()) {
        // The rule is disabled
        disabledErrorsFound++;
        return false;
    } else {
        // The rule is enabled
        enabledErrorsFound++;
        bool logError = true;
        if (maxErrors > 0) {  // 0 means log all errors
            if (enabledErrorsFound > maxErrors) {
                logError = false;
            }
        }
        if (logError) {
            // LOG("error") << "[" << id << "] " << s;
            LOG("error") << s;
        }
        if (enabledErrorsFound == maxErrors) {
            LOG("error") << "Logged max capture data errors (maxErrors=" << maxErrors << "). Additional errors will not be logged.";
        }
        return logError;
    }
}

bool CaptureChecker::StepSimulator(HpccAcSimulator* sim, ExpectedCaptureState* expected, const std::set<uint32_t>& breakpoints, long numberOfSteps)
{
    long i = 0;
    while (i != numberOfSteps)
    {
        auto it = breakpoints.find(sim->vecaddr);
        if (it != breakpoints.end()) {
            return true;
        }
        //
        // Step each simulator one tester cycle and execute the drive channels
        //
        if (sim->isStopped) {  // Don't run tester cycle is slice is done
            return false;
        }
        // Execute drive logic for the slice
        bool success = sim->StepTesterCycle(&expected->testerCycleContext);
        if (success) {
            sim->ExecuteTesterCycleDrivePhase(&expected->testerCycleContext);
        }
        // Check if all the simulators stopped, this is our global stop condition
        if (sim->isStopped) {
            return false;
        }
        //
        // Execute compare channels and check captures
        //
        // Execute compare logic for the slice
        sim->ExecuteTesterCycleComparePhase(&expected->testerCycleContext);
        bool captureValid = sim->ExecuteTesterCycleCapturePhase(&expected->testerCycleContext, &expected->capture);
        if (captureValid) {
            // self.captures.append('addr={}, data=0b{:056b}, fail={}, patid=0x{:x}'.format(self.expected.capture.vectorAddress, self.expected.capture.pinLaneData, '1' if self.expected.capture.failEvent else '0', self.expected.capture.patternInstanceId))
        }
        // Advance simulation time
        float time = sim->tcc * sim->m_period;
        sim->m_bfm->AdvanceTo(time);

        i++;
    }
    return true;
}

void CaptureChecker::SimulateAndCheckCapture(const std::vector<CaptureCheckerContext*>& contexts, Bfm* bfm, float period, size_t ratio)
{
    // Clear the error counts, etc...
    Clear();

    LOG("debug") << "Initializing capture checker state list";

    // Initialize capture states
    std::vector<ActualCaptureState>   actualStates;
    std::vector<ExpectedCaptureState> expectedStates;
    std::stringstream ss;
    for (size_t i = 0; i < contexts.size(); i++) {
        const CaptureCheckerContext& context = *contexts[i];
        actualStates.push_back(ActualCaptureState());
        expectedStates.push_back(ExpectedCaptureState());
        // Unpack capture data into blocks
        LOG("debug") << "Unpacking capture data into blocks for (" << context.sim->slot << ", " << context.sim->slice << "): length = " << context.length << ", blockCount = " << context.blockCount;
        UnpackCapture(context.array, context.length, context.blockCount, &actualStates[i].blocks);
        ss << "(" << context.sim->slot << "," << context.sim->slice << ")";
        if (i != contexts.size() - 1) {
            ss << " ";
        }
    }
    LOG("debug") << "Running capture checker for: [" << ss.str() << "], period=" << (period * 1E9f) << "ns, ratio=" << ratio;

    uint64_t cycle = 0;

    bool done = false;
    while (!done) {
        // Step each simulator one tester cycle and execute the drive channels
        for (size_t i = 0; i < contexts.size(); i++) {
            const CaptureCheckerContext& context = *contexts[i];
            if (cycle % context.ratio == 0) {
                ExpectedCaptureState& expected = expectedStates[i];
                HpccAcSimulator* sim = context.sim;
                // Don't run tester cycle is slice is done
                if (sim->isStopped) continue;
                // Execute drive logic for the slice
                bool success = sim->StepTesterCycle(&expected.testerCycleContext);
                if (success) {
                    sim->ExecuteTesterCycleDrivePhase(&expected.testerCycleContext);
                }
            }
        }
        // Check if all the simulators stopped, this is our global stop condition
        done = true;
        for (size_t i = 0; i < contexts.size(); i++) {
            CaptureCheckerContext& context = *contexts[i];
            HpccAcSimulator* sim = context.sim;
            if (!sim->isStopped) {
                done = false;
            }
        }
        // Execute compare channels and check captures
        for (size_t i = 0; i < contexts.size(); i++) {
            CaptureCheckerContext& context = *contexts[i];
            if ((cycle + 1) % context.ratio == 0) { // compare at the last cycle for slow slice when all the driving are done in fast slice
                ExpectedCaptureState& expected = expectedStates[i];
                ActualCaptureState& actual = actualStates[i];
                HpccAcSimulator* sim = context.sim;
                // Don't run tester cycle is slice is done
                if (sim->isStopped) continue;
                // Execute compare logic for the slice
                sim->ExecuteTesterCycleComparePhase(&expected.testerCycleContext);
                bool captureValid = sim->ExecuteTesterCycleCapturePhase(&expected.testerCycleContext, &expected.capture);
                if (captureValid) {
                    _CheckCapture(&context, &actual, &expected);
                }
            }
        }
        // Advance simulation time
        cycle++;
        float time = cycle * period;
        if (cycle % ratio == 0) {
            bfm->AdvanceTo(time);
        }
    }

    for (size_t i = 0; i < contexts.size(); i++) {
        CaptureCheckerContext& context = *contexts[i];
        ExpectedCaptureState& expected = expectedStates[i];
        ActualCaptureState& actual = actualStates[i];
        if (context.sim->exactCaptureCounts && !expected.abortDetected) {
            if (!((!context.sim->exactFailCounts) && BIT(context.sim->captureControl, 11))) { // don't check if CTV after max fail, real abort during sim momentum
                _CheckCounts(context.sim->slot, context.sim->slice, context.captureCount, expected.totalCaptureCount);
            }
        }
        LOG("debug") << "Compared " << actual.totalCaptureCount << " actual captured vectors against " << expected.totalCaptureCount << " simulated captured vectors for HpccAc(" << context.sim->slot << ", " << context.sim->slice << ")";      
        // pass the first fail ddr3 offset to context so that env can read it
        context.firstFailOffset = actual.firstFailOffset;
        context.firstFailOffsetFound = actual.firstFailOffsetFound;
    }
    
    if (totalMissingCapture != 0) {
        LOG("warning") << "Total missing capture data after stop on max fail = " << totalMissingCapture;
    }
}

void CaptureChecker::_CheckCapture(CaptureCheckerContext* context, ActualCaptureState* actual, ExpectedCaptureState* expected)
{
    // Increase the number of expected vectors captured
    expected->totalCaptureCount++;
    // Grab a valid capture data item from the "queue"
    CaptureHeader* actualCaptureHeaderPtr;
    CaptureData* actualCapturePtr;
    bool success = actual->GetValidCaptureData(&actualCaptureHeaderPtr, &actualCapturePtr, 1 << (SLICE(context->sim->patternControl, 27, 26) + 1));

    if (!success) {
        std::stringstream ss;
        ss << "Missing capture data for vector address 0x" << std::hex << expected->capture.vectorAddress << ", ";
        ss << "simulated=" << "0x" << std::hex << expected->capture.pinLaneData;
        bool logWarning  = false;
        if (expected->capture.afterAbort) {
            // sim cannot accurately predict FPGA momentum, 2k is likely to be the upper limit
            totalMissingCapture++;
            if (totalMissingCapture <= maxErrors) {
                LOG("warning") << ss.str() << " (after stop on Max fail)";
            }
        } else {
            logWarning = LogError("missing-capture", ss.str());
        }
        if (logWarning) {
            LOG("warning") << "expected=" << SimulatedCapture::Group(expected->capture.PinLaneDataAsStr(), groupPinLaneData);
        }
    } else {
        // Increase the number of actual vectors captured
        actual->totalCaptureCount++;

        const CaptureHeader& actualCaptureHeader = *actualCaptureHeaderPtr;
        const CaptureData& actualCapture = *actualCapturePtr;
        
        // Check total cycle count
        if (actualCaptureHeader.totalCycleCount != expected->capture.totalCycleCount) {
            std::stringstream ss;
            ss << "Total cycle count mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.totalCycleCount << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.totalCycleCount;
            LogError("tcc-mismatch", ss.str());
        }
        // Check vector address
        if (actualCapture.vectorAddress != expected->capture.vectorAddress) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << ", vector address mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.vectorAddress << ", ";
            ss << "actual=0x" << std::hex << actualCapture.vectorAddress;
            LogError("vecaddr-mismatch", ss.str());
        }
        // Check pattern cycle count
        if (actualCaptureHeader.patternCycleCount != expected->capture.patternCycleCount) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "Pattern cycle count mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.patternCycleCount << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.patternCycleCount;
            LogError("pcc-mismatch", ss.str());
        }
        // Check cycle count within repeat
#ifndef LOCAL_REPEAT_THEN_CHANNEL_LINKING        
        if (actual->firstValidCapture) { 
            // NOTE: We only check CCWR for the first valid vector in capture (i.e. we only check the CCWR field in the capture header)
            // FIXME: Remove expected->capture.validCycleCountWithinRepeat filter
            if (actualCaptureHeader.cycleCountWithinRepeat != expected->capture.cycleCountWithinRepeat && expected->capture.validCycleCountWithinRepeat) {
                std::stringstream ss;
                ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
                ss << "Cycle count within repeat mismatch: ";
                ss << "expected=0x" << std::hex << expected->capture.cycleCountWithinRepeat << ", ";
                ss << "actual=0x" << std::hex << actualCaptureHeader.cycleCountWithinRepeat;
                LogError("ccwr-mismatch", ss.str());
            }
        }
#else        
        if (actualCaptureHeader.cycleCountWithinRepeat != expected->capture.cycleCountWithinRepeat) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "Cycle count within repeat mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.cycleCountWithinRepeat << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.cycleCountWithinRepeat;
            LogError("ccwr-mismatch", ss.str());
        }
#endif
        // Check user cycle count
        if (actualCaptureHeader.userCycleCount != expected->capture.userCycleCount) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "User cycle count mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.userCycleCount << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.userCycleCount;
            LogError("ucc-mismatch", ss.str());
        }
        // Check pattern id
        if (actualCaptureHeader.patternInstanceId != expected->capture.patternInstanceId) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "Pattern Id mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.patternInstanceId << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.patternInstanceId;
            LogError("patid-mismatch", ss.str());
        }
        // Check user log 1
        if (actualCaptureHeader.userLogRegister1 != expected->capture.userLogRegister1) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "User log 1 mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.userLogRegister1 << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.userLogRegister1;
            LogError("userlog1-mismatch", ss.str());
        }
        // Check user log 2
        if (actualCaptureHeader.userLogRegister2 != expected->capture.userLogRegister2) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "User log 2 mismatch: ";
            ss << "expected=0x" << std::hex << expected->capture.userLogRegister2 << ", ";
            ss << "actual=0x" << std::hex << actualCaptureHeader.userLogRegister2;
            LogError("userlog2-mismatch", ss.str());
        }

        // Fail event
        if (!BIT(context->sim->captureControl, 9)) {  // NOTE: We ignore "Fail Event" in internal loopback mode because it does not work!
            if (actualCapture.failEvent != expected->capture.failEvent) {
                std::stringstream ss;
                ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
                ss << "Fail event mismatch: ";
                ss << "expected=" << expected->capture.failEvent << ", ";
                ss << "actual=" << actualCapture.failEvent;
                LogError("fail-mismatch", ss.str());
            }
        }
        // CTV event
        if (actualCapture.ctvEvent != expected->capture.ctvEvent) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "CTV event mismatch: ";
            ss << "expected=" << expected->capture.ctvEvent << ", ";
            ss << "actual=" << actualCapture.ctvEvent;
            LogError("ctv-mismatch", ss.str());
        }
        // Link mode
        if (actualCapture.linkMode != expected->capture.linkMode) {
            std::stringstream ss;
            ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
            ss << "Link mode mismatch: ";
            ss << "expected=" << int(expected->capture.linkMode) << ", ";
            ss << "actual=" << int(actualCapture.linkMode);
            LogError("link-mismatch", ss.str());
        }
        if (context->sim->stop) {
            if (!expected->abortDetected && context->sim->m_abort) {
                // Check capture counts here (v.s. at the end)
                LOG("debug") << "Capture checker detected abort for HpccAc(" << context->sim->slot << ", " << context->sim->slice << ")";
                _CheckCounts(context->sim->slot, context->sim->slice, actual->totalCaptureCount, expected->totalCaptureCount);
                expected->abortDetected = true;
            }
        } else {
            // NOTE: We only check capture pin lane data before the end instruction
            uint64_t pinLaneCheckMask    = expected->capture.pinLaneCheckMask | channelsToMask;
            uint64_t actualCheckedData   = actualCapture.pinLaneData & ~pinLaneCheckMask;
            uint64_t expectedCheckedData = expected->capture.pinLaneData & ~pinLaneCheckMask;
            if (actualCheckedData != expectedCheckedData) {
                std::stringstream ss;
                ss << "At cycle 0x" << std::hex << expected->capture.totalCycleCount << " (vecaddr=0x" << std::hex << expected->capture.vectorAddress << "), ";
                ss << "Capture data mismatch: simulated=" << "0x" << std::hex << expected->capture.pinLaneData << ", ";
                ss << "actual=" << "0x" << std::hex << actualCapture.pinLaneData;
                bool logWarning = LogError("data-mismatch", ss.str());
                if (logWarning) {
                    LOG("warning") << " expected=" << SimulatedCapture::Group(expected->capture.PinLaneDataAsStr(), groupPinLaneData) << " (" << expected->capture.TypeAsStr() << ")";
                    LOG("warning") << "   actual=" << SimulatedCapture::Group(SimulatedCapture::PinLaneDataToStr(actualCapture.pinLaneData, pinLaneCheckMask), groupPinLaneData) << " (" << expected->capture.TypeAsStr() << ")";
                    LOG("warning") << "simvector=" << SimulatedCapture::Group(expected->capture.VectorAsStr(), groupPinLaneData);
                    LOG("warning") << " simstate=" << SimulatedCapture::Group(expected->capture.PinStateAsStr(), groupPinLaneData);
                    LOG("warning") << "Found " << PinMismatches(expected->capture.pinLaneData, actualCapture.pinLaneData, pinLaneCheckMask) << " bit(s) mismatching";
                }
            }
        }
        // Advance actual capture data pointer 
        actual->Next();
    }
}

void CaptureChecker::_CheckCounts(int slot, int slice, size_t actualCount, size_t expectedCount)
{
    LOG("debug") << "Performing capture count checks for HpccAc(" << slot << ", " << slice << ")";
    if (actualCount < expectedCount) {
        size_t delta = expectedCount - actualCount;
        std::stringstream ss;
        ss << "HpccAc(" << slot << "," << slice << ") dropped " << delta << " vectors in capture data.";
        LogError("dropped-vectors", ss.str());
    } else if (actualCount > expectedCount) {
        size_t delta = actualCount - expectedCount;
        std::stringstream ss;
        ss << "HpccAc(" << slot << "," << slice << ") generated " << delta << " extra vectors in capture data.";
        LogError("extra-vectors", ss.str());
    }
}

uint64_t CaptureChecker::PinMismatches(uint64_t expected, uint64_t actual, uint64_t checkMask)
{
    uint64_t actualChecked   = actual & ~checkMask;
    uint64_t expectedChecked = expected & ~checkMask;
    if (actualChecked == expectedChecked) {
        return 0;  // No mismatches
    } else {
        uint64_t mismatches = 0;
        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
            if (BIT(checkMask, ch) == 0x0) { // Check this bit
                if (BIT(expected, ch) != BIT(actual, ch)) {
                    mismatches++;
                }
            }
        }
        return mismatches;
    }
}

std::string CaptureChecker::CaptureEquality(std::vector<FlatCapture>& expectedCapture, std::vector<FlatCapture>& actualCapture, bool checkCaptureCount, bool checkFailEvent, uint64_t pinLaneDataMask)
{
    if (checkCaptureCount) {
        if (actualCapture.size() < expectedCapture.size()) {
            std::stringstream ss;
            ss << "Capture has less vectors than expected, actual=" << actualCapture.size() << " < expected=" << expectedCapture.size();
            return ss.str();
        } else if (actualCapture.size() > expectedCapture.size()) {
            std::stringstream ss;
            ss << "Capture has more vectors than expected, actual=" << actualCapture.size() << " > expected=" << expectedCapture.size();
            return ss.str();
        }
    }

    for (size_t i = 0; i < std::min(expectedCapture.size(), actualCapture.size()); i++) {
        FlatCapture& captureA = expectedCapture[i];
        FlatCapture& captureB = actualCapture[i];

        // Both captures must have the same header data
        if (captureA.patternInstanceId != captureB.patternInstanceId) {
            std::stringstream ss;
            ss << "Pattern Id mismatch, expected=0x" << std::hex << captureA.patternInstanceId << " != actual=0x" << std::hex << captureB.patternInstanceId;
            return ss.str();
        }
        if (captureA.userLogRegister1 != captureB.userLogRegister1) {
            std::stringstream ss;
            ss << "User Log 1 Id mismatch, expected=0x" << std::hex << captureA.userLogRegister1 << " != actual=0x" << std::hex << captureB.userLogRegister1;
            return ss.str();
        }
        if (captureA.userLogRegister2 != captureB.userLogRegister2) {
            std::stringstream ss;
            ss << "User Log 2 Id mismatch, expected=0x" << std::hex << captureA.userLogRegister2 << " != actual=0x" << std::hex << captureB.userLogRegister2;
            return ss.str();
        }
        if (captureA.patternCycleCount != captureB.patternCycleCount) {
            std::stringstream ss;
            ss << "Pattern cycle count Id mismatch, expected=0x" << std::hex << captureA.patternCycleCount << " != actual=0x" << std::hex << captureB.patternCycleCount;
            return ss.str();
        }
/*
        if (headerA->cycleCountWithinRepeat != headerB->cycleCountWithinRepeat) {
            std::stringstream ss;
            ss << "Cycle count within repeat mismatch";
            return ss.str();
        }
*/
        if (captureA.userCycleCount != captureB.userCycleCount) {
            std::stringstream ss;
            ss << "User cycle count Id mismatch, expected=0x" << std::hex << captureA.userCycleCount << " != actual=0x" << std::hex << captureB.userCycleCount;
            return ss.str();
        }
        if (captureA.totalCycleCount != captureB.totalCycleCount) {
            std::stringstream ss;
            ss << "Total cycle count Id mismatch, expected=0x" << std::hex << captureA.totalCycleCount << " != actual=0x" << std::hex << captureB.totalCycleCount;
            return ss.str();
        }

        if (captureA.vectorAddress != captureB.vectorAddress) {
            std::stringstream ss;
            ss << "Vector address mismatch, expected=0x" << std::hex << captureA.vectorAddress << " != actual=0x" << std::hex << captureB.vectorAddress;
            return ss.str();
        }
        if (checkFailEvent && (captureA.failEvent != captureB.failEvent)) {
            std::stringstream ss;
            ss << "Fail event mismatch, expected=" << std::hex << captureA.failEvent << " != actual=" << std::hex << captureB.failEvent;
            return ss.str();
        }
        if (captureA.ctvEvent != captureB.ctvEvent) {
            std::stringstream ss;
            ss << "CTV event mismatch, expected=" << std::hex << captureA.ctvEvent << " != actual=" << std::hex << captureB.ctvEvent;
            return ss.str();
        }
        if (captureA.linkMode != captureB.linkMode) {
            std::stringstream ss;
            ss << "Link mode mismatch, expected=" << std::hex << captureA.linkMode << " != actual=" << std::hex << captureB.linkMode;
            return ss.str();
        }
        if ((captureA.pinLaneData & ~pinLaneDataMask) != (captureB.pinLaneData & ~pinLaneDataMask)) {
            std::stringstream ss;
            ss << "Pin lane data mismatch, expected=0x" << std::hex << captureA.pinLaneData << " != actual=0x" << std::hex << captureB.pinLaneData;
            return ss.str();
        }
    }
    return "";
}

}  // namespace hpcctbc

