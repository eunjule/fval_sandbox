// INTEL CONFIDENTIAL

// Copyright 2016-2018 Intel Corporation.

// This software and the related documents are Intel copyrighted materials,
// and your use of them is governed by the express license under which they
// were provided to you ("License"). Unless the License provides otherwise,
// you may not use, modify, copy, publish, distribute, disclose or transmit
// this software or the related documents without Intel's prior written
// permission.

// This software and the related documents are provided as is, with no express
// or implied warranties, other than those that are expressly stated in the
// License.

%module(directors="1") creditcounter

%feature(director);

#if SWIG_VERSION < 0x020008
#error Requires SWIG 2.0.8 or later.
#endif

%include <stdint.i>
%include <exception.i>

%begin %{
#ifdef _MSC_VER
    #include <codeanalysis\warnings.h>
    #pragma warning (disable:ALL_CODE_ANALYSIS_WARNINGS)
    #pragma warning(disable:4100) // unreferenced formal parameter
    #pragma warning(disable:4127) // conditional expression is constant
    #pragma warning(disable:4211) // nonstandard extension used : redefined extern to static
    #pragma warning(disable:4706) // assignment within conditional expression
    #pragma warning(disable:4996) // This function or variable may be unsafe
    #pragma warning(disable:4701) // Potentially uninitialized local variable used
    #pragma warning(disable:4101) // 'swig_obj': unreferenced local variable
	#pragma warning(disable:4459) // warning C4459: declaration of 'swig_this' hides global declaration
	#pragma warning(disable:4244) // warning C4244: 'argument': conversion from 'Py_ssize_t' to 'int', possible loss of data

#else
    #pragma GCC diagnostic ignored "-Wunused-variable"
    #pragma GCC diagnostic ignored "-Wunused-value"
#endif
%}

%{
#include "simplecreditcounter.h"
%}

%exception {
    try {
        $action
    } catch (const std::exception& e) {
        SWIG_exception(SWIG_RuntimeError, e.what());
    } catch (...) {
        SWIG_exception(SWIG_UnknownError, "C++ anonymous exception");
    }
}

namespace hpcctbc {

%catches(std::exception) SimpleCreditCounter;

}

%rename(CreditCounter) hpcctbc::SimpleCreditCounter;
%include "simplecreditcounter.h"