////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: creditcountermodel.cc
//------------------------------------------------------------------------------
//    Purpose: Credit counter model
//------------------------------------------------------------------------------
// Created by: Dean Glazeski
//       Date: 02/25/2016
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "creditcountermodel.h"

namespace hpcctbc
{
    const float CreditCounterModel::MinQDR = 1.18e-9f;
    const float CreditCounterModel::MaxQDR = MinQDR * 256;
    const int64_t CreditCounterModel::MaxBalance = 16 * 1024;
    const int64_t CreditCounterModel::PreStageAmount = 1 * 1024;
    const uint64_t CreditCounterModel::MaxSubCycle = 7U;
    const int64_t CreditCounterModel::InstructionCost = -1;
    const int64_t CreditCounterModel::MetadataCost = -1;
    const int64_t CreditCounterModel::RepeatCost[] = { -300, -70, 17, 30, 30, 30, 30, 30 }; // last 4 buckets are duplicated, not from data collection
    const int64_t CreditCounterModel::BranchCost[] = { -900, -200, -50, -28, -28, -28, -28, -28 }; // last 4 buckets are duplicated, not from data collection
}