////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: generators.cc
//------------------------------------------------------------------------------
//    Purpose: HPCC Pattern Assembler Generators
//------------------------------------------------------------------------------
// Created by: Yuan Feng / Rodny Rodriguez
//       Date: 05/28/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "generators.h"
#include "hpcctbc.h"
#include "fvalc.h"
#include <iostream>

using namespace fvalc;

namespace hpcctbc {

static_assert(HPCC_CHANNELS >= 33, "Expecting at least 33 channels (or the code won't work)");
static_assert(HPCC_CHANNELS <= 64, "Expecting 64 channels of less (or the code won't work)");

void PatternAssemblerContext::SetVar(std::string name, uint64_t value)
{
    vars[name] = value;
}

Generator::Generator(PatternAssemblerContext* context)
{
    this->context = context;
}

//////////////////////// Data Editors /////////////////////////////////

uint64_t PassingEditor(uint64_t data)
{
    uint64_t result = 0;
    for (size_t i = 0; i < 16; i++) {
        uint64_t nibble = data & 0xf;
        switch(nibble) {
        case 0x2: //0b0010:
        case 0xE: //0b1110:
            result = result | 0xA;
            break;
        case 0x3: //0b0011:
        case 0xB: //0b1011:
            result = result | 0xF; //0b1010;
            break;
        case 0x8: //0b1000:
        case 0xC: //0x1100:
            result = result | 0x0; //0b1111;
            break;
        default:
            result = result | nibble;
        }
        if (i < 15) {
            data = data >> 4;
            result = result << 4;
        }
    }
    return result;
}

// SWITCH_COMB = {'0100': '0000', '0110': '1010', '0111': '1111', '0001': '0000', '1001': '1010', '1101': '1111'}
uint64_t SwitchEditor(uint64_t data)
{
    uint64_t result = 0;
    for (size_t i = 0; i < 16; i++) {
        uint64_t nibble = data & 0xf;
        switch(nibble) {
        case 0x4: //0b0100:
        case 0x1: //0b0001:
            result = result | 0x0;
            break;
        case 0x6: //0b0110:
        case 0x9: //0b1001:
            result = result | 0xA; //0b1010;
            break;
        case 0x7: //0b0111:
        case 0xD: //0x1101:
            result = result | 0xF; //0b1111;
            break;
        default:
            result = result | nibble;
        }
        if (i < 15) {
            data = data >> 4;
            result = result << 4;
        }
    }
    return result;
}

// Z: 00 -> 10 ZERO; X: 00 -> 10 Low
// S: 01 -> 11 High or 1, since S can cause ZL/ZH in channel link
uint64_t ZXEditor(uint64_t data)
{
    uint64_t result = 0;
    for (size_t i = 0; i < 32; i++) {
        uint64_t twoBits = data & 0x3;
        switch(twoBits) {
        case 0x0: // Z or X
            result = result | 0x2; //0b10
            break;
        case 0x1: // S
            result = result | 0x3; //0b11
            break;
        default:
            result = result | twoBits;
        }
        if (i < 31) {
            data = data >> 2;
            result = result << 2;
        }
    }
    return result;
}

// S: 01 -> 10 Low or Zero
uint64_t SEditor(uint64_t data)
{
    uint64_t result = 0;
    for (size_t i = 0; i < 32; i++) {
        uint64_t twoBits = data & 0x3;
        switch(twoBits) {
        case 0x1: // S
            result = result | 0x2; //0b10
            break;
        default:
            result = result | twoBits;
        }
        if (i < 31) {
            data = data >> 2;
            result = result << 2;
        }
    }
    return result;
}

////////////////////////////////////////////////////////////////////////////////

RandomVectors::RandomVectors(PatternAssemblerContext* context, size_t length, float ctvRate, float mtvRate, float linkRate, float lrptRate) : Generator(context)
{
    this->m_length = length;
    this->ctvRate = ctvRate;
    this->mtvRate = mtvRate;
    this->linkRate = linkRate;
    this->lrptRate = lrptRate;
}

size_t RandomVectors::Size() const
{
    return m_length;
}

size_t RandomVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    for (size_t k = 0; k < this->m_length; k++) {
        uint64_t link = 0;
        uint64_t ctv = 0;
        uint64_t mtv = 0;
        uint64_t lrpt = 0;
        if (RandBool(linkRate)) {
            link = 1;
        }
        if (RandBool(ctvRate)) {
            ctv = 1;
        }
        if (RandBool(mtvRate)) {
            mtv = Rand64() & Mask<3>();
        }
        if (RandBool(lrptRate)) {
            lrpt = Rand64() & Mask<5>();
        }
        uint64_t datah = Rand64() & Mask<48>();  // Clear the upper 16 bits
        uint64_t datal = Rand64();
        uint64_t pvcrsvd = 0;
        WriteVectorWord(array, length, offset, link, ctv, mtv, lrpt, datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

////////////////////////////////////////////////////////////////////////////////

AddressVectors::AddressVectors(PatternAssemblerContext* context, size_t length) : Generator(context)
{
    this->start = context->vecaddr;
    this->m_length = length;
}

size_t AddressVectors::Size() const
{
    return m_length;
}

size_t AddressVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    Lambda(void, (uint64_t& address, uint64_t& data, size_t shift), {
        uint64_t bit = address & 0x1;
        if (bit == 0x0) {
            data = data | (0x2ULL << shift);  // 0
        } else {
            data = data | (0x3ULL << shift);  // 1
        }
        address = address >> 1;
    }) _EncodeAddress;

    for (size_t k = 0; k < this->m_length; k++) {
        uint64_t address = start + k;
        uint64_t link = 0;
        uint64_t ctv = 0;
        uint64_t mtv = 0;
        uint64_t lrpt = 0;
        uint64_t datal = 0;
        for (size_t i = 0; i < 32; i++) {
            _EncodeAddress(address, datal, 2 * i);
        }
        uint64_t datah = 0;
        for (size_t i = 32; i < HPCC_CHANNELS; i++) {
            _EncodeAddress(address, datah, 2 * i);
        }
        uint64_t pvcrsvd = 0;
        WriteVectorWord(array, length, offset, link, ctv, mtv, lrpt, datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

////////////////////////////////////////////////////////////////////////////////

RandomEvenToOddVectors::RandomEvenToOddVectors(PatternAssemblerContext* context, size_t length, float passingRate, size_t simple, size_t noZX, float ctvRate, float mtvRate, float linkRate, float lrptRate) : Generator(context)
{
    this->m_length = length;
    this->passingRate = passingRate;
    this->simple = simple;
    this->noZX = noZX;
    this->ctvRate = ctvRate;
    this->mtvRate = mtvRate;
    this->linkRate = linkRate;
    this->lrptRate = lrptRate;
}

size_t RandomEvenToOddVectors::Size() const
{
    return m_length;
}

size_t RandomEvenToOddVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    std::vector<int> ctvList(this->m_length, 0); // init to 0
    std::vector<int> mtvList(this->m_length, 0); // init to 0
    std::vector<int> linkList(this->m_length, 0); // init to 0
    std::vector<int> lrptList(this->m_length, 0); // init to 0
    if (this->ctvRate > 0) {
        for (size_t i = 0; i < this->m_length; i++) {
            ctvList[i] = RandBool(this->ctvRate);
        }
    }
    if (this->mtvRate > 0) {
        for (size_t i = 0; i < this->m_length; i++) {
            if (RandBool(this->mtvRate)) {
                mtvList[i] = (Rand64() % 7) + 1; // even distribution between 1-7
            }
        }
    }
    if (this->linkRate > 0) {
        for (size_t i = 0; i < this->m_length; i++) {
            linkList[i] = RandBool(this->linkRate);
        }
        linkList[0] = 0; // first vector after IO state jam cannot have channel link
    }
    if (this->lrptRate > 0) {
        for (size_t i = 0; i < this->m_length; i++) {
            if (RandBool(this->lrptRate)) {
                lrptList[i] = (Rand64() % 31) + 1; // even distribution between 1-31
            }
        }
    }
    for (size_t i = 0; i < this->m_length; i++) {
        uint64_t link = linkList[i];
        uint64_t ctv = ctvList[i];
        uint64_t mtv = mtvList[i];
        uint64_t lrpt = lrptList[i];
        uint64_t datah = SwitchEditor(Rand64());
        uint64_t datal = SwitchEditor(Rand64());
        if (this->passingRate > 0) {
            if (RandBool(this->passingRate)) {
                datah = PassingEditor(datah);
                datal = PassingEditor(datal);
            }
        }
        if ((this->simple > 0) || (link == 1) || (lrpt > 0)) { // no switch in channel linking / lrpt vectors
            datah = SEditor(datah);
            datal = SEditor(datal);
        }
        if (this->noZX > 0){ // no ZX vectors
            datah = ZXEditor(datah);
            datal = ZXEditor(datal);
        }
        datah = datah & Mask<48>();  // Clear the upper 16 bits

        uint64_t pvcrsvd = 0;
        WriteVectorWord(array, length, offset, link, ctv, mtv, lrpt, datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

////////////////////////////////////////////////////////////////////////////////

RandomPassingEvenToOddVectors::RandomPassingEvenToOddVectors(PatternAssemblerContext* context, size_t length, size_t simple, size_t noZX) : Generator(context)
{
    this->m_length = length;
    this->simple = simple;
    this->noZX = noZX;
}

size_t RandomPassingEvenToOddVectors::Size() const
{
    return m_length;
}

size_t RandomPassingEvenToOddVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    for (size_t i = 0; i < this->m_length; i++) {
        uint64_t link = 0;
        uint64_t ctv = 0;
        uint64_t mtv = 0;
        uint64_t lrpt = 0;
        uint64_t datah = PassingEditor(SwitchEditor(Rand64()));
        uint64_t datal = PassingEditor(SwitchEditor(Rand64()));
        if (this->simple > 0) {
            datah = SEditor(datah); 
            datal = SEditor(datal);
        }
        if (this->noZX > 0){ // no ZX vectors
            datah = ZXEditor(datah);
            datal = ZXEditor(datal);
        }
        datah = datah & Mask<48>();  // Clear the upper 16 bits
        uint64_t pvcrsvd = 0;
        WriteVectorWord(array, length, offset, link, ctv, mtv, lrpt, datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

////////////////////////////////////////////////////////////////////////////////

RandomPassingEvenToOddNoSwitchVectors::RandomPassingEvenToOddNoSwitchVectors(PatternAssemblerContext* context, size_t length, RandomVar ctv, RandomVar mtv, RandomVar link, RandomVar lrpt) : Generator(context)
{
    this->m_length = length;
    this->ctv = ctv;
    this->mtv = mtv;
    this->link = link;
    this->lrpt = lrpt;
}

size_t RandomPassingEvenToOddNoSwitchVectors::Size() const
{
    return m_length;
}

size_t RandomPassingEvenToOddNoSwitchVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    for (size_t i = 0; i < this->m_length; i++) {
        uint64_t datah = 0;
        uint64_t datal = 0;
        uint64_t pvcrsvd = 0;
        uint64_t dir = Rand64();
        for (size_t j = 0; j < HPCC_CHANNELS / 2; j++) {
            size_t pair;
            if (BIT(dir, j) == 0x1) {
                pair = 0xF;  // Drive 1, Compare 1
            } else {
                pair = 0xA;  // Drive 0, Compare 0
            }
            if (j < 16) {
                datal = (datal << 4) | pair;
            } else {
                datah = (datah << 4) | pair;
            }
        }
        datah = datah & Mask<48>();  // Clear the upper 16 bits
        WriteVectorWord(array, length, offset, link(), ctv(), mtv(), lrpt(), datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

////////////////////////////////////////////////////////////////////////////////

Drive0CompareLVectors::Drive0CompareLVectors(PatternAssemblerContext* context, size_t length) : Generator(context)
{
    this->m_length = length;
}

size_t Drive0CompareLVectors::Size() const
{
    return m_length;
}

size_t Drive0CompareLVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    for (size_t k = 0; k < this->m_length; k++) {
        uint64_t link = 0;
        uint64_t ctv = 0;
        uint64_t mtv = 0;
        uint64_t lrpt = 0;
        uint64_t datah = 0xAAAAAAAAAAAAAAAA;  // 1010 Drive 0 compare L
        uint64_t datal = 0xAAAAAAAAAAAAAAAA;
        uint64_t pvcrsvd = 0;
        WriteVectorWord(array, length, offset, link, ctv, mtv, lrpt, datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

////////////////////////////////////////////////////////////////////////////////

Drive1CompareHVectors::Drive1CompareHVectors(PatternAssemblerContext* context, size_t length) : Generator(context)
{
    this->m_length = length;
}

size_t Drive1CompareHVectors::Size() const
{
    return m_length;
}

size_t Drive1CompareHVectors::Generate(uint8_t* array, size_t length, size_t offset) const
{
    for (size_t k = 0; k < this->m_length; k++) {
        uint64_t link = 0;
        uint64_t ctv = 0;
        uint64_t mtv = 0;
        uint64_t lrpt = 0;
        uint64_t datah = 0xFFFFFFFFFFFFFFFF;  // 1111 Drive 1 compare H
        uint64_t datal = 0xFFFFFFFFFFFFFFFF;
        uint64_t pvcrsvd = 0;
        WriteVectorWord(array, length, offset, link, ctv, mtv, lrpt, datah, datal, pvcrsvd);
        offset += PATTERN_WORD_SIZE;
    }
    return this->m_length;
}

}  // namespace hpcctbc

