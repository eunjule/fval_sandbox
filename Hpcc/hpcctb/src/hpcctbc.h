////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpcctbc.h
//------------------------------------------------------------------------------
//    Purpose: HPCC High-Performance Testbench Methods and Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/30/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __HPCCTBC_H__
#define __HPCCTBC_H__

#ifdef _MSC_VER
    #pragma warning(disable:4100) // unreferenced formal parameter
#endif

#include <cstdint>
#include <string>
#include <vector>

#define PATTERN_WORD_SIZE 16  // 1 vector = 16 bytes
#define HPCC_CHANNELS     56

#define LOCAL_REPEAT_THEN_CHANNEL_LINKING

namespace hpcctbc {

uint8_t EncodeVectorChar(char c, bool enclk, bool keepmode);
void EncodeVectorLiteral(const std::string& literal, uint64_t enclk, uint64_t keepmode, uint64_t* datah, uint64_t* datal);

uint8_t EncodeIOStateJamChar(char c, bool enclk);
void EncodeIOStateJamLiteral(const std::string& literal, uint64_t enclk, uint64_t* datah, uint64_t* datal);

void WriteVectorWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t link,
    uint64_t ctv,
    uint64_t mtv,
    uint64_t lrpt,
    uint64_t datah,
    uint64_t datal,
    uint64_t pvcrsvd
);

void WriteMetadataWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t datah,
    uint64_t datal
);

void WritePinStateWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t stype,
    uint64_t datah,
    uint64_t datal,
    uint64_t undefined,
    uint64_t unused
);

void WriteInstructionWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t optype,
    uint64_t extop,
    uint64_t action,
    uint64_t opdest,
    uint64_t opsrc,
    uint64_t aluop,
    uint64_t vptype,
    uint64_t vpop,
    uint64_t invcond,
    uint64_t cond,
    uint64_t base,
    uint64_t br,
    uint64_t dest,
    uint64_t regB,
    uint64_t regA,
    uint64_t imm,
    uint64_t swrsvd,
    uint64_t rsvd
);

void WriteRawPatternWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t datah,
    uint64_t datal
);

uint8_t ReadPatternWordType
(
    const uint8_t* array,
    size_t length,
    size_t offset
);

void ReadVectorWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* link,
    uint64_t* ctv,
    uint64_t* mtv,
    uint64_t* lrpt,
    uint64_t* datah,
    uint64_t* datal,
    uint64_t* pvcrsvd
);

void ReadMetadataWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* datah,
    uint64_t* datal
);

void ReadPinStateWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* stype,
    uint64_t* datah,
    uint64_t* datal,
    uint64_t* undefined,
    uint64_t* unused
);

void ReadInstructionWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* optype,
    uint64_t* extop,
    uint64_t* action,
    uint64_t* opdest,
    uint64_t* opsrc,
    uint64_t* aluop,
    uint64_t* vptype,
    uint64_t* vpop,
    uint64_t* invcond,
    uint64_t* cond,
    uint64_t* base,
    uint64_t* br,
    uint64_t* dest,
    uint64_t* regB,
    uint64_t* regA,
    uint64_t* imm,
    uint64_t* swrsvd,
    uint64_t* rsvd
);

struct CaptureHeader
{
    bool     validFlags[8];
    uint32_t patternInstanceId;
    uint32_t userLogRegister1;
    uint32_t userLogRegister2;
    uint32_t patternCycleCount;
    uint32_t cycleCountWithinRepeat;
    uint32_t userCycleCount;
    uint64_t totalCycleCount;
    bool     repeatReset[8];
};

struct CaptureData
{
    uint32_t vectorAddress;
    bool     failEvent;
    bool     ctvEvent;
    uint8_t  reserved;
    bool     ccwrReset;
    uint8_t  linkMode;
    uint64_t pinLaneData;
};

struct CaptureBlock
{
    CaptureHeader header;
    CaptureData data[8];
};

void ReadCaptureHeader(const uint8_t* array, size_t length, size_t offset, CaptureHeader* header);
void ReadCaptureData(const uint8_t* array, size_t length, size_t offset, CaptureData* data);
void UnpackCapture(const uint8_t* array, size_t length, size_t blockCount, std::vector<CaptureBlock>* blocks);

}  // namespace hpcctbc

#endif  // __HPCCTBC_H__

