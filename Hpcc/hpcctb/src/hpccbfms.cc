////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpccbfms.cc
//------------------------------------------------------------------------------
//    Purpose: HPCC AC Bus Functional Models
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/12/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "hpccbfms.h"
#include "logging.h"

static_assert(HPCC_CHANNELS >= 33, "Expecting at least 33 channels (or the code won't work)");
static_assert(HPCC_CHANNELS <= 64, "Expecting 64 channels of less (or the code won't work)");

namespace hpcctbc {

void LoopbackBfm::SetUp(int slot, int slice)
{
}

void LoopbackBfm::Start(int slot, int slice)
{
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        size_t vch = MapChannel(slot, slice, ch);
        loopback[vch].Clear();
        overrides[vch].startTime = -1.0;
        overrides[vch].stopTime = -1.0;
    }
}

void LoopbackBfm::AdvanceTo(float time)
{
    for (auto it = loopback.begin(); it != loopback.end(); it++) {
        it->second.AdvanceTo(time);
    }
    for (auto it = overrides.begin(); it != overrides.end();) {
        if (time > it->second.stopTime) {
            it = overrides.erase(it);
        } else {
            it++;
        }
    }
}

void LoopbackBfm::Drive(int slot, int slice, size_t ch, float time, char logic)
{
    size_t vch = MapChannel(slot, slice, ch);
    bool success = loopback[vch].DriveAt(time, logic);
    if (!success) {
        LOG("error") << "Failed to drive logic value of '" << logic << "' for channel " << ch << " of (" << slot << "," << slice << ")";
    }
}

void LoopbackBfm::OverrideDrive(int slot, int slice, size_t ch, float startTime, float stopTime, char logic)
{
    size_t vch = MapChannel(slot, slice, ch);
    OverrideWaveform wave;
    wave.startTime = startTime;
    wave.stopTime = stopTime;
    wave.logic = logic;
    overrides[vch] = wave;
}

char LoopbackBfm::Sample(int slot, int slice, size_t ch, float time)
{
    size_t vch = MapChannel(slot, slice, ch);
    auto it = overrides.find(vch);
    if (it != overrides.end()) {
        if (it->second.startTime >= 0.0 && it->second.startTime <= time && time < it->second.stopTime) {
            return it->second.logic;
        }
    }
    return loopback[vch].SampleAt(time);
}

char LoopbackBfm::SampleEndState(int slot, int slice, size_t ch, float delta)
{
    size_t vch = MapChannel(slot, slice, ch);
    float time = loopback[vch].Last().time;
    auto it = overrides.find(vch);
    if (it != overrides.end()) {
        time = std::max(time, it->second.startTime);
    }
    return Sample(slot, slice, ch, time + delta);
}

}  // namespace hpcctbc

