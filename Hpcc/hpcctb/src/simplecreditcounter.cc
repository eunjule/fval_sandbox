////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: simplecreditcounter.cc
//------------------------------------------------------------------------------
//    Purpose: Credit counter model
//------------------------------------------------------------------------------
// Created by: Dean Glazeski
//       Date: 02/24/2016
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "simplecreditcounter.h"

#include <creditcountermodel.h>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <stdexcept>

namespace hpcctbc
{
    SimpleCreditCounter::SimpleCreditCounter(float period, int64_t threshold, int64_t initialBalance)
        :threshold(threshold),
         balance(initialBalance),
         underrunAmount(0),
         prestage(false)
    {
        if (period < CreditCounterModel::MinQDR || period > CreditCounterModel::MaxQDR)
        {
            std::stringstream ss;
            ss << "Invalid period=" << period << ", expecting period to be in the range [" << CreditCounterModel::MinQDR
                << ", " << CreditCounterModel::MaxQDR << "] range";
            throw std::runtime_error(ss.str());
        }

        m_subcycle = static_cast<int64_t>(std::log(period / CreditCounterModel::MinQDR) / std::log(2));
    }

    SimpleCreditCounter::SimpleCreditCounter()
        :threshold(0),
         balance(0),
         underrunAmount(0),
         m_subcycle(0),
         prestage(true)
    {
    }

    void SimpleCreditCounter::SetSubcycle(uint64_t subcycle)
    {
        if(subcycle > CreditCounterModel::MaxSubCycle)
        {
            std::stringstream ss;
            ss << "Invalid subcycle=" << subcycle << ", expected subcycle to be in the range [0, "
                << CreditCounterModel::MaxSubCycle << "] range";
            throw std::runtime_error(ss.str());
        }

        this->m_subcycle = subcycle;
    }

    SimpleCreditCounter::~SimpleCreditCounter()
    {
    }

    int64_t SimpleCreditCounter::Update(int64_t delta)
    {
        if(prestage)
        {
            if(delta > 0)
            {
                balance = std::min(balance + delta, CreditCounterModel::MaxBalance);
                if(balance >= CreditCounterModel::PreStageAmount)
                {
                    prestage = false;
                }
            }
        }
        else
        {
            auto newBalance = std::min(balance + delta, CreditCounterModel::MaxBalance);
            if(newBalance < threshold)
            {
                std::stringstream ss;
                ss << "Pattern will under run.  Balance of [" << newBalance << "] is below threshold of " << threshold;
                underrunAmount = threshold - newBalance;
                throw std::runtime_error(ss.str());
            }

            balance = newBalance;
            underrunAmount = 0;
        }

        return balance;
    }

    int64_t SimpleCreditCounter::GetUnderrunAmount() const
    {
        return underrunAmount;
    }

    int64_t SimpleCreditCounter::Vector(int localRepeat, int linkWidth)
    {
        return Update((localRepeat + 1) * linkWidth);
    }

    int64_t SimpleCreditCounter::Metadata()
    {
        return Update(CreditCounterModel::MetadataCost);
    }

    int64_t SimpleCreditCounter::Instruction()
    {
        return Update(CreditCounterModel::InstructionCost);
    }

    int64_t SimpleCreditCounter::InstructionRepeat(int64_t /*repeatCount*/)
    {
        return Update(CreditCounterModel::RepeatCost[m_subcycle]);
    }

    int64_t SimpleCreditCounter::Branch()
    {
        return Update(CreditCounterModel::BranchCost[m_subcycle]);
    }
}
