////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: creditcountermodel.h
//------------------------------------------------------------------------------
//    Purpose: Credit counter model
//------------------------------------------------------------------------------
// Created by: Dean Glazeski
//       Date: 02/25/2016
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <cstdint>

namespace hpcctbc
{
    class CreditCounterModel
    {
    public:
        static const float MinQDR;
        static const float MaxQDR;
        static const int64_t MaxBalance;
        static const int64_t PreStageAmount;

        static const uint64_t MaxSubCycle;
        static const int64_t InstructionCost;
        static const int64_t MetadataCost;
        static const int64_t RepeatCost[];
        static const int64_t BranchCost[];
    };
}
