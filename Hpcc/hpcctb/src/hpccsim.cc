////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpccsim.cc
//------------------------------------------------------------------------------
//    Purpose: HPCC AC Fast Pattern Simulator
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/01/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <cstring>
#include <sstream>
#include <stdexcept>
#include <fstream>

#include "hpccsim.h"
#include "defines.h"
#include "logging.h"

#undef OVERFLOW  // math.h defines OVERFLOW, shame on them
#include "symbols.h"

#define TRIGGER_CONTROL              0x00008180
#define CAPTURE_CONTROL              0x000081A0
#define PATTERN_CONTROL              0x00008230
#define PATTERN_START_ADDRESS        0x000082A0
#define MAX_FAIL_CAPTURE_COUNT       0x00008430
#define MAX_CAPTURE_COUNT            0x000081C0
#define MAX_FAIL_COUNT_PER_PATTERN   0x000081D0
#define TEST_CLASS_FAIL_MASK_LOWER   0x000081E0
#define TEST_CLASS_FAIL_MASK_UPPER   0x000081F0
#define SELECTABLE_FAIL_MASK_0_LOWER 0x00008300
#define SELECTABLE_FAIL_MASK_0_UPPER 0x00008310
#define SELECTABLE_FAIL_MASK_1_LOWER 0x00008320
#define SELECTABLE_FAIL_MASK_1_UPPER 0x00008330
#define SELECTABLE_FAIL_MASK_2_LOWER 0x00008340
#define SELECTABLE_FAIL_MASK_2_UPPER 0x00008350
#define SELECTABLE_FAIL_MASK_3_LOWER 0x00008360
#define SELECTABLE_FAIL_MASK_3_UPPER 0x00008370
#define SELECTABLE_FAIL_MASK_4_LOWER 0x00008380
#define SELECTABLE_FAIL_MASK_4_UPPER 0x00008390
#define SELECTABLE_FAIL_MASK_5_LOWER 0x000083A0
#define SELECTABLE_FAIL_MASK_5_UPPER 0x000083B0
#define SELECTABLE_FAIL_MASK_6_LOWER 0x000083C0
#define SELECTABLE_FAIL_MASK_6_UPPER 0x000083D0
#define SELECTABLE_FAIL_MASK_7_LOWER 0x000083E0
#define SELECTABLE_FAIL_MASK_7_UPPER 0x000083F0

#define PER_CHANNEL_CONTROL_CONFIG1  0x00009000
#define PER_CHANNEL_CONTROL_CONFIG2  0x0000A000
#define PER_CHANNEL_CONTROL_CONFIG3  0x0000B000
#define PER_CHANNEL_CONTROL_CONFIG4  0x0000E000

#define ALUSIGN(result) (((result) & 0x80000000) != 0)
#define ALUZERO(result) ((result) == 0x0)
// The rules for turning on the carry flag in binary/integer math are two:
// 1. The carry flag is set if the addition of two numbers causes a carry
//    out of the most significant (leftmost) bits added.
// 2. The carry (borrow) flag is also set if the subtraction of two numbers
//    requires a borrow into the most significant (leftmost) bits subtracted.
// Otherwise, the carry flag is turned off (zero).
// Reference: http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
#define ALUCARRY(result) (((result) > 0xFFFFFFFF) || ((result) < 0x0))

#define LSHLCARRY(result) (((result) & 0x100000000) != 0)
// The rules for turning on the overflow flag in binary/integer math are two:
// 1. If the sum of two numbers with the sign bits off yields a result number
//    with the sign bit on, the "overflow" flag is turned on.
// 2. If the sum of two numbers with the sign bits on yields a result number
//    with the sign bit off, the "overflow" flag is turned on.
// Otherwise, the overflow flag is turned off.
// Reference: http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
#define ALUOVERFLOW(src1, src2, result) ((!ALUSIGN(src1) && !ALUSIGN(src2) && ALUSIGN(result)) || (ALUSIGN(src1) && ALUSIGN(src2) && !ALUSIGN(result)))

#define ABORT_END_STATUS 0x1800dead

#define STOP_ON_FAIL_MOMENTUM (2 * 1024)
#define FASTEST_PERIOD_TO_CHECK_DRIVE_CHANNELS (30E-9)

static_assert(HPCC_CHANNELS >= 33, "Expecting at least 33 channels (or the simulator code won't work)");
static_assert(HPCC_CHANNELS <= 64, "Expecting 64 channels of less (or the simulator code won't work)");

namespace hpcctbc {

std::string SimulatedCapture::Group(std::string data, size_t groupSize, std::string separator)
{
    std::string result;
    size_t L = data.length();
    for (size_t i = 0; i < L; i++) {
        if (i > 0 && groupSize > 1) {
            if (i % groupSize == 0) {
                result = separator + result;
            }
        }
        result = data[L - 1 - i] + result;
    }
    return result;
}

std::string SimulatedCapture::PinLaneDataToStr(uint64_t data, uint64_t checkMask)
{
    std::string result = std::string(HPCC_CHANNELS, '?');
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        char c;
        if (BIT(checkMask, ch) == 0x0) { // Check this bit
            if (BIT(data, ch) == 0x0) {
                c = '0';
            } else {
                c = '1';
            }
        } else {
            c = '_';
        }
        result[HPCC_CHANNELS - 1 - ch] = c;
    }
    return result;
}

std::string SimulatedCapture::TypeAsStr() const
{
    if (type == PIN_STATE) {
        return "PIN_STATE";
    } else if (type == FAIL_STATUS) {
        return "FAIL_STATUS";
    } else if (type == PIN_DIRECTION) {
        return "PIN_DIRECTION";
    } else if (type == MASKED_FAIL) {
        return "MASKED_FAIL";
    } else {
        std::stringstream ss;
        ss << "Unknown capture type " << type;
        return ss.str();
    }
}

std::string SimulatedCapture::PinLaneDataAsStr() const
{
    return SimulatedCapture::PinLaneDataToStr(pinLaneData, pinLaneCheckMask);
}

std::string SimulatedCapture::PinStateAsStr() const
{
    std::string result = std::string(HPCC_CHANNELS, '?');
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        char c;
        if (BIT(pinState, ch) == 0x0) {
            c = '0';
        } else {
            c = '1';
        }
        result[HPCC_CHANNELS - 1 - ch] = c;
    }
    return result;
}

std::string SimulatedCapture::VectorAsStr() const
{
    std::string result = std::string(HPCC_CHANNELS, '?');
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        char c;
        if (BIT(pinEnabledClock, ch) == 0x0) {  // Normal vector (i.e. non-enabled clock)
            if (BIT(pinVectorDirection, ch) == 0x1) {  // 1 = Drive
                if (BIT(pinVectorEnable, ch) == 0x1) {
                    if (BIT(pinVectorValue, ch) == 0x0) {
                        c = '0';
                    } else {
                        c = '1';
                    }
                } else {
                    c = 'Z';
                }
            } else {  // 0 = Compare
                if (BIT(pinVectorEnable, ch) == 0x1) {
                    if (BIT(pinVectorValue, ch) == 0x0) {
                        c = 'L';
                    } else {
                        c = 'H';
                    }
                } else {
                    c = 'X';
                }
            }
        } else {  // Enabled clock
            c = pinEnabledClockChar[ch];
        }
        result[HPCC_CHANNELS - 1 - ch] = c;
    }
    return result;
}

HpccAcSimulator::HpccAcSimulator(int slot, int slice, size_t memorySize)
{
    m_bfm = nullptr;

    cardType = rctbc::HPCC_AC_CARD;
    m_domain = 0;
    m_dutId = 0;
    domainMaster = false;
    this->slot = slot;
    this->slice = slice;
    this->memorySize = memorySize;
    memory = new PatternWord[this->memorySize];  // allocate vector memory

    // Reset
    ResetChannels();
    ResetCountersAndMiscFlags();

    // Clear Pcie registers
    patternControl = 0x0;
    captureControl = 0x0;
    triggerControl = 0x0;
    patternStartAddress = 0x0;
    maxFailCaptureCount = 0x0;
    maxFailCountPerPattern = 0x0;
    testClassFailMask = 0x0;
    for (size_t i = 0; i < 8; i++) {
        selectableFailMask[i] = 0x0;
    }
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        perChannelControlConfig1[ch] = 0x0;
        perChannelControlConfig2[ch] = 0x0;
        perChannelControlConfig3[ch] = 0x0;
        perChannelControlConfig4[ch] = 0x0;
    }

    reg.clear();
    for (size_t i = 0; i < 32; i++) {
        reg.push_back(0);
    }
    ResetFlags();
    ResetAlarms();

    cfb = 0;
    pfb = 0;

    endstatus = 0x0;

    log1 = 0;
    log2 = 0;
    patid = 0;

    stagedPatmask = 0x0;
    patmask = 0x0;
    vecmask = 0x0;
    mask = 0x0;

    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        Channel& channel = channels[ch];
        channel.zislogic1 = true;
    }

    externalAbort = false;
    externalAbortAddress = 0x0;

    traceStartCycle = 0x0;
    traceStopCycle = 0xFFFFFFFFFFFFFFFF;
    ioStateJamTrace = false;
    instructionTrace = false;
    captureTrace = false;

    endState = "";
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        endState = endState + std::string("0");
    }
    endStateLogic = endState;

    checkz = false;
}

HpccAcSimulator::~HpccAcSimulator()
{
    if (memory != nullptr) {
        delete[] memory;
    }
}

std::string HpccAcSimulator::Name() const
{
    std::stringstream ss;
    ss << "HPCC AC Simulator (" << slot << ", " << slice << ")";
    return ss.str();
}

void HpccAcSimulator::ResetChannels()
{
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        Channel& channel = channels[ch];
        channel.mode = DRIVE_MODE;
        channel.drvEnable = false;
        channel.drv = '0';
        channel.cmpEnable = false;
        channel.cmp = 'L';
        // Start the enabled clocks in the reset state with a ratio count of 0
        channel.clk = 'R';
        channel.clkRatioCount = channel.clkRatio - 1;
        channel.failcnt = 0;
        channel.freqCounter = 0;
        channel.prevPinState = 0;
    }
}

void HpccAcSimulator::ResetCountersAndMiscFlags()
{
    // Clear cycle count registers
    tcc = 0;
    pcc = 0;
    ucc = 0;
    // Clear misc flags
    stop = false;
    m_abort = false;
    jumpFlag = false;
    repeatFlag = false; repeat = 0;
    failbrFlag = false; failbr = 0;
    failcnt = 0;
    patfcnt = 0;
    captureCount = 0;
}

void HpccAcSimulator::BarRead(uint32_t bar, uint32_t offset, uint32_t data)
{
    // Nothing to do here
}

void HpccAcSimulator::BarWrite(uint32_t bar, uint32_t offset, uint32_t data)
{
    if (bar == 0) {
        if (offset == TRIGGER_CONTROL) {
            // Set the card type the AC FPGA should respond to
            cardType = static_cast<int>(SLICE(data, 3, 0));
            // Define what time domain an AC FPGA belongs to
            m_domain = static_cast<int>(SLICE(data, 7, 4));
            // If asserted, AC FPGA is the master for their time domain
            domainMaster = BIT(data, 8) == 0x1;
            // Set the MDUT group to belong to
            m_dutId = static_cast<int>(SLICE(data, 20, 15));
        } else if (offset == CAPTURE_CONTROL) {        // Capture Control
            captureControl = data;
        } else if (offset == PATTERN_CONTROL) {        // Pattern Control
            patternControl = data;
        } else if (offset == PATTERN_START_ADDRESS) {  // Pattern Start Address
            patternStartAddress = data;
        } else if (offset == MAX_FAIL_CAPTURE_COUNT) {
            maxFailCaptureCount = data;
        } else if (offset == MAX_CAPTURE_COUNT) {
            maxCaptureCount = data;
        } else if (offset == MAX_FAIL_COUNT_PER_PATTERN) {
            maxFailCountPerPattern = data;
        } else if (offset == TEST_CLASS_FAIL_MASK_LOWER) {
            // Note: class level mask feature has been ZBBed in FPGA, should not use
            // Update test class-level mask
            testClassFailMask = (testClassFailMask & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == TEST_CLASS_FAIL_MASK_UPPER) {  
            // Update test class-level mask
            testClassFailMask = (testClassFailMask & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_0_LOWER) {  
            selectableFailMask[0] = (selectableFailMask[0] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_0_UPPER) {  
            selectableFailMask[0] = (selectableFailMask[0] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_1_LOWER) {  
            selectableFailMask[1] = (selectableFailMask[1] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_1_UPPER) {  
            selectableFailMask[1] = (selectableFailMask[1] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_2_LOWER) {  
            selectableFailMask[2] = (selectableFailMask[2] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_2_UPPER) {  
            selectableFailMask[2] = (selectableFailMask[2] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_3_LOWER) {  
            selectableFailMask[3] = (selectableFailMask[3] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_3_UPPER) {  
            selectableFailMask[3] = (selectableFailMask[3] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_4_LOWER) {  
            selectableFailMask[4] = (selectableFailMask[4] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_4_UPPER) {  
            selectableFailMask[4] = (selectableFailMask[4] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_5_LOWER) {  
            selectableFailMask[5] = (selectableFailMask[5] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_5_UPPER) {  
            selectableFailMask[5] = (selectableFailMask[5] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_6_LOWER) {  
            selectableFailMask[6] = (selectableFailMask[6] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_6_UPPER) {  
            selectableFailMask[6] = (selectableFailMask[6] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else if (offset == SELECTABLE_FAIL_MASK_7_LOWER) {  
            selectableFailMask[7] = (selectableFailMask[7] & 0xFFFFFFFF00000000) | static_cast<uint64_t>(data);
        } else if (offset == SELECTABLE_FAIL_MASK_7_UPPER) {  
            selectableFailMask[7] = (selectableFailMask[7] & 0xFFFFFFFF) | (static_cast<uint64_t>(data) << 32);
        } else {
            for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
                if (offset == PER_CHANNEL_CONTROL_CONFIG1 + 16 * ch) {
                    perChannelControlConfig1[ch] = data;
                    break;
                } else if (offset == PER_CHANNEL_CONTROL_CONFIG2 + 16 * ch) {
                    perChannelControlConfig2[ch] = data;
                    break;
                } else if (offset == PER_CHANNEL_CONTROL_CONFIG3 + 16 * ch) {
                    perChannelControlConfig3[ch] = data;
                    break;
                } else if (offset == PER_CHANNEL_CONTROL_CONFIG4 + 16 * ch) {
                    perChannelControlConfig4[ch] = data;
                    break;
                }
            }
        }
    }
}

void HpccAcSimulator::DmaRead(uint64_t address, const uint8_t* data, uint64_t length)
{
    // Nothing to do here
}

void HpccAcSimulator::DmaWrite(uint64_t address, const uint8_t* data, uint64_t length)
{
    if (address % PATTERN_WORD_SIZE != 0) {
        std::stringstream ss;
        ss << "DmaWrite address " << address << " is not a multiple of " << PATTERN_WORD_SIZE;
        throw std::runtime_error(ss.str());
    }
    if (length % PATTERN_WORD_SIZE != 0) {
        std::stringstream ss;
        ss << "DmaWrite length " << address << " is not a multiple of " << PATTERN_WORD_SIZE;
        throw std::runtime_error(ss.str());
    }

    size_t vectorAddress = address / PATTERN_WORD_SIZE;  // Convert to vector address
    size_t vectorLength  = length / PATTERN_WORD_SIZE;  // Convert to vector length

    if (vectorAddress >= memorySize) {
        // Drop the write, target address is outside our reach
        return;
    }
    // At this point we know that address < memorySize
    if (vectorAddress + vectorLength > memorySize) {
        // Truncate it, we can't write the entire thing but can write some of it
        vectorLength = memorySize - vectorAddress;
    }
    // Copy the data
    memcpy(&memory[vectorAddress], data, vectorLength * PATTERN_WORD_SIZE);
}

void HpccAcSimulator::DumpMemory(std::string filename, uint64_t vectorAddress, uint64_t vectorLength) const
{
    std::ofstream fout;
    fout.open(filename.c_str(), std::ios::out | std::ios::binary);
    fout.write(reinterpret_cast<char*>(memory + vectorAddress), vectorLength * sizeof(PatternWord));
    fout.close();
}


void HpccAcSimulator::SetupTrace(uint64_t startCycle, uint64_t stopCycle)
{
    traceStartCycle = startCycle;
    traceStopCycle = stopCycle;
}

void HpccAcSimulator::EnableIOStateJamTrace(std::string filename)
{
    ioStateJamTrace = true;
    ioStateJamTraceFout.open(filename.c_str());
    ioStateJamTraceFout << "VectorAddress,";
    ioStateJamTraceFout << "\n";
}

void HpccAcSimulator::EnableInstructionTrace(std::string filename)
{
    instructionTrace = true;
    instructionTraceFout.open(filename.c_str());
    instructionTraceFout << "VectorAddress,";
    instructionTraceFout << "\n";
}

void HpccAcSimulator::EnableCaptureTrace(std::string filename)
{
    captureTrace = true;
    captureTraceFout.open(filename.c_str());
    // Write header
    captureTraceFout << "TotalCycleCount,";
    captureTraceFout << "UserCycleCount,";
    captureTraceFout << "CycleCountWithinRepeat,";
    captureTraceFout << "PatternCycleCount,";
    captureTraceFout << "UserLogRegister2,";
    captureTraceFout << "UserLogRegister1";
    captureTraceFout << "PatternInstanceId,";
    captureTraceFout << "VectorAddress,";
    captureTraceFout << "FailEvent,";
    captureTraceFout << "CtvEvent,";
    captureTraceFout << "LinkMode,";
    captureTraceFout << "PinLaneDataHigh,";
    captureTraceFout << "PinLaneDataLow,";
    captureTraceFout << "PinLaneData,";
    captureTraceFout << "\n";
}

void HpccAcSimulator::SetBfm(Bfm* bfm)
{
    this->m_bfm = bfm;
}

void HpccAcSimulator::SetPeriod(float period)
{
    this->m_period = period;
    creditCounter.SetPeriod(period);
}

void HpccAcSimulator::SetDriveDelay(const std::vector<size_t>& channelList, float delay)
{
    for (auto iter = channelList.begin(); iter != channelList.end(); iter++) {
        Channel& channel = channels[*iter];
        channel.tdrv = delay;
    }
}

void HpccAcSimulator::SetCompareDelay(const std::vector<size_t>& channelList, float delay)
{
    for (auto iter = channelList.begin(); iter != channelList.end(); iter++) {
        Channel& channel = channels[*iter];
        channel.tcmp = delay;
    }
}

void HpccAcSimulator::SetEnClkRatio(const std::vector<size_t>& channelList, uint8_t ratio)
{
    for (auto iter = channelList.begin(); iter != channelList.end(); iter++) {
        Channel& channel = channels[*iter];
        channel.clkRatio = ratio;
    }
}

void HpccAcSimulator::SetEnClkTrise(const std::vector<size_t>& channelList, float trise)
{
    for (auto iter = channelList.begin(); iter != channelList.end(); iter++) {
        Channel& channel = channels[*iter];
        channel.trise = trise;
    }
}

void HpccAcSimulator::SetEnClkTfall(const std::vector<size_t>& channelList, float tfall)
{
    for (auto iter = channelList.begin(); iter != channelList.end(); iter++) {
        Channel& channel = channels[*iter];
        channel.tfall = tfall;
    }
}

void HpccAcSimulator::Start()
{
    ResetFlags();
    ResetAlarms();
    ResetChannels();
    ResetCountersAndMiscFlags();

    vecaddr = patternStartAddress;

    endstatus = 0x0;

    log1 = 0;
    log2 = 0;
    patid = 0;

    edgeCounter = false;
    blockFails = false;

    stagedPatmask = 0x0;
    patmask = 0x0;
    vecmask = 0x0;
    mask = 0x0;

    isStopped = false;
    exactFailCounts = true;
    exactCaptureCounts = true;

    // Reset the BFM on every new pattern execution
    m_bfm->Start(slot, slice);

    // Drive initial state on all channels
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        m_bfm->Drive(slot, slice, ch, 0.0, endState[HPCC_CHANNELS - 1 - ch]);
    }

    // Clear internal vectors
    while (vectors.size() > 0) {
        vectors.pop();
    }

	// Clear stack
    while (stack.size() > 0) {
        stack.pop();
    }
	
	// Clear mask stack
    while (maskStack.size() > 0) {
        maskStack.pop();
    }

	
	// Reset credit counter
    creditCounter.Reset();

    LOG("debug") << "Started " << Name() << " at address=" << Hex(vecaddr);
    startTime = std::chrono::high_resolution_clock::now();
}

void HpccAcSimulator::Stop(uint32_t status, size_t vectorCount, bool abort)
{
    // Simple heuristic to figure out if the given stop event overrides any existing one
    bool assertStop = false;
    if (!stop) {
        // No previous stop event, this is the first one
        assertStop = true;
    } else {
        // Assert stop if the given stop address happens sooner than the current one
        if ((vecaddr + vectorCount) < stopAddressEstimate) {  // First stop wins
            assertStop = true;
        }
    }
    if (assertStop) {
        stop = true;
        this->m_abort = abort;
        endstatus = status;
        stopCount = vectorCount + 1;  // Simulator architecture takes one less vector to stop, adding one to compensate
        stopAddressEstimate = vecaddr + vectorCount;
        // Capture pattern end time and pattern end state
        endPinDirection = 0x0;
        endPinEnable = 0x0;
        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
            Channel& channel = channels[ch];
            char logic = m_bfm->SampleEndState(slot, slice, ch, 1e-15f);
            endState[HPCC_CHANNELS - 1 - ch] = logic;
            if (logic == 'z') {
                if (channel.zislogic1) {
                    endStateLogic[HPCC_CHANNELS - 1 - ch] = '1';
                } else {
                    endStateLogic[HPCC_CHANNELS - 1 - ch] = '0';
                }
            } else {
                endStateLogic[HPCC_CHANNELS - 1 - ch] = logic;
            }
            if (channel.mode == DRIVE_MODE || channel.mode == ENABLED_CLOCK_MODE) {
                SETBIT(endPinDirection, ch);
            }
            if (channel.drvEnable || channel.cmpEnable) {
                SETBIT(endPinEnable, ch);
            }
        }
    }
    if (ioStateJamTrace) {
        ioStateJamTrace = false;
        ioStateJamTraceFout.close();
    }
    if (instructionTrace) {
        instructionTrace = false;
        instructionTraceFout.close();
    }
    if (captureTrace) {
        captureTrace = false;
        captureTraceFout.close();
    }
}

bool HpccAcSimulator::StepVectorProcessor()
{
    if (vectors.size() > 0) {
        std::stringstream ss;
        ss << "Can't run the vector processor if there are intermediate vectors pending to be processed";
        throw std::runtime_error(ss.str());
    }

    // Check address bounds
    if (vecaddr >= memorySize) {
        std::stringstream ss;
        ss << "Invalid vecaddr=" << vecaddr << ", memorySize=" << memorySize;
        throw std::runtime_error(ss.str());
    }

    if (externalAbort) {
        if (vecaddr == externalAbortAddress) {
            Stop(ABORT_END_STATUS, 0, true);  // Set the abort bit!
            externalAbort = false;
            externalAbortAddress = 0x0;
        }
    }

    // Update pattern word pointer
    uint8_t* pw = (uint8_t*) &memory[vecaddr];

    // Snoop pattern word type and use it to control switch statement
    uint8_t type = ReadPatternWordType(pw, memorySize, 0);
    switch (type) {
        case VECTOR_WORD: {
            uint64_t link;
            uint64_t ctv;
            uint64_t mtv;
            uint64_t lrpt;
            uint64_t datah;
            uint64_t datal;
            uint64_t pvcrsvd;
            ReadVectorWord(pw, memorySize, 0, &link, &ctv, &mtv, &lrpt, &datah, &datal, &pvcrsvd);
            _PushVector(link, ctv, mtv, lrpt, datah, datal);
            break;
        }
        case METADATA_WORD: {
            uint64_t datah;
            uint64_t datal;
            ReadMetadataWord(pw, memorySize, 0, &datah, &datal);
            // Update credit counter
            creditCounter.Metadata(vecaddr);
            break;
        }
        case PINSTATE_WORD: {
            uint64_t stype;
            uint64_t datah;
            uint64_t datal;
            uint64_t undefined;
            uint64_t unused;
            ReadPinStateWord(pw, memorySize, 0, &stype, &datah, &datal, &undefined, &unused);
            if (stype == IOSTATEJAM) {
                if (ioStateJamTrace && (traceStartCycle <= tcc) && (tcc <= traceStopCycle)) {
                    ioStateJamTraceFout << "0x" << std::hex << vecaddr << ",";
                    ioStateJamTraceFout << "\n";
                }
                _PushIOStateJam(datah, datal);
            } else if (stype == MASK) {
                // OR data with current pattern-level mask
                stagedPatmask |= datal;
            } else if (stype == MASK_APPLY) {
                // OR data with current pattern-level mask
                patmask |= datal;
            } else if (stype == UNMASK) {
                // AND data with current pattern-level mask
                stagedPatmask &= datal;
            } else {
                std::stringstream ss;
                ss << "Invalid stype=" << stype;
                throw std::runtime_error(ss.str());
            }
            break;
        }
        case INSTRUCTION_WORD: {
            uint64_t optype;
            uint64_t extop;
            uint64_t action;
            uint64_t opdest;
            uint64_t opsrc;
            uint64_t aluop;
            uint64_t vptype;
            uint64_t vpop;
            uint64_t invcond;
            uint64_t cond;
            uint64_t base;
            uint64_t br;
            uint64_t dest;
            uint64_t regB;
            uint64_t regA;
            uint64_t imm;
            uint64_t swrsvd;
            uint64_t rsvd;
            ReadInstructionWord(pw, memorySize, 0, &optype, &extop, &action, &opdest, &opsrc,
                    &aluop, &vptype, &vpop, &invcond, &cond, &base, &br, &dest, &regB, &regA, &imm, &swrsvd, &rsvd);

            // Update credit counter
            creditCounter.Instruction(vecaddr);
            if (instructionTrace && (traceStartCycle <= tcc) && (tcc <= traceStopCycle)) {
                instructionTraceFout << "0x" << std::hex << vecaddr << ",";
                instructionTraceFout << "\n";
            }

            if (optype == EXT) {
                if (extop == TRIGGER) {
                    if (action == TRGEXT_I) {
                        uint32_t trigger = static_cast<uint32_t>(imm);
                        m_rc->SendTrigger(trigger);
                    } else if (action == TRGDOM_I) {
                        uint32_t trigger = static_cast<uint32_t>(imm);
                        uint8_t count       = static_cast<uint8_t>(SLICE(trigger, 7, 4));
                        uint8_t triggerType = static_cast<uint8_t>(SLICE(trigger, 3, 0));
                        uint8_t dutId       = static_cast<uint8_t>(SLICE(trigger, 25, 20));
                        // Overwrite cardType and domain fields
                        m_rc->SendTriggerHpccAc(
                                static_cast<uint8_t>(this->cardType),
                                dutId,
                                static_cast<uint8_t>(this->m_domain),
                                count,
                                triggerType
                        );
                    } else if (action == TRGEXT_RA) {
                        uint32_t trigger = static_cast<uint32_t>(reg[regA]);
                        m_rc->SendTrigger(trigger);
                    } else if (action == TRGDOM_RA) {
                        uint32_t trigger = static_cast<uint32_t>(reg[regA]);
                        uint8_t count       = static_cast<uint8_t>(SLICE(trigger, 7, 4));
                        uint8_t triggerType = static_cast<uint8_t>(SLICE(trigger, 3, 0));
                        uint8_t dutId       = static_cast<uint8_t>(SLICE(trigger, 25, 20));
                        // Overwrite cardType and domain fields
                        m_rc->SendTriggerHpccAc(
                                static_cast<uint8_t>(this->cardType),
                                dutId,
                                static_cast<uint8_t>(this->m_domain),
                                count,
                                triggerType
                        );
                    } else {
                        std::stringstream ss;
                        ss << "Invalid action=" << action;
                        throw std::runtime_error(ss.str());
                    }
                } else if (extop == OTHER) {
                    if (action == CD_LOAD) {
                        // NOTE: Deprecated instruction!
                        std::stringstream ss;
                        ss << "CD_LOAD is a deprecated instruction";
                        throw std::runtime_error(ss.str());
                    } else if (action == CD_STORE) {
                        // NOTE: Deprecated instruction!
                        std::stringstream ss;
                        ss << "CD_STORE is a deprecated instruction";
                        throw std::runtime_error(ss.str());
                    } else {
                        std::stringstream ss;
                        ss << "Invalid action=" << action;
                        throw std::runtime_error(ss.str());
                    }
                } else if (extop == TRIGGERNOW) {
                    if (action == TRGEXT_I) {
                        uint32_t trigger = static_cast<uint32_t>(imm);
                        m_rc->SendTrigger(trigger);
                    } else if (action == TRGDOM_I) {
                        uint32_t trigger = static_cast<uint32_t>(imm);
                        uint8_t count       = static_cast<uint8_t>(SLICE(trigger, 7, 4));
                        uint8_t triggerType = static_cast<uint8_t>(SLICE(trigger, 3, 0));
                        uint8_t dutId       = static_cast<uint8_t>(SLICE(trigger, 25, 20));
                        // Overwrite cardType and domain fields
                        m_rc->SendTriggerHpccAc(
                                static_cast<uint8_t>(this->cardType),
                                dutId,
                                static_cast<uint8_t>(this->m_domain),
                                count,
                                triggerType
                        );
                    } else if (action == TRGEXT_RA) {
                        uint32_t trigger = static_cast<uint32_t>(reg[regA]);
                        m_rc->SendTrigger(trigger);
                    } else if (action == TRGDOM_RA) {
                        uint32_t trigger = static_cast<uint32_t>(reg[regA]);
                        uint8_t count       = static_cast<uint8_t>(SLICE(trigger, 7, 4));
                        uint8_t triggerType = static_cast<uint8_t>(SLICE(trigger, 3, 0));
                        uint8_t dutId       = static_cast<uint8_t>(SLICE(trigger, 25, 20));
                        // Overwrite cardType and domain fields
                        m_rc->SendTriggerHpccAc(
                                static_cast<uint8_t>(this->cardType),
                                dutId,
                                static_cast<uint8_t>(this->m_domain),
                                count,
                                triggerType
                        );
                    } else {
                        std::stringstream ss;
                        ss << "Invalid action=" << action;
                        throw std::runtime_error(ss.str());
                    }
                } else {
                    std::stringstream ss;
                    ss << "Invalid extop=" << extop;
                    throw std::runtime_error(ss.str());
                }
            } else if (optype == ALU) {
                // Get data from sources
                uint32_t src1;
                uint32_t src2;
                if (opsrc == ALUSRC_RA_I) {
                    src1 = reg[regA];
                    src2 = (uint32_t) imm;
                } else if (opsrc == ALUSRC_RA_RB) {
                    src1 = reg[regA];
                    src2 = reg[regB];
                } else if (opsrc == ALUSRC_FLAGS_I) {
                    src1 = flags;
                    src2 = (uint32_t) imm;
                } else if (opsrc == ALUSRC_FLAGS_RB) {
                    src1 = flags;
                    src2 = reg[regB];
                } else {
                    std::stringstream ss;
                    ss << "Invalid opsrc=" << opsrc;
                    throw std::runtime_error(ss.str());
                }
                // Execute ALU operation
                uint32_t result;
                if (aluop == XOR) {
                    result = src1 ^ src2;
                    SetAluFlags(ALUZERO(result), false, false, ALUSIGN(result));
                } else if (aluop == AND) {
                    result = src1 & src2;
                    SetAluFlags(ALUZERO(result), false, false, ALUSIGN(result));
                } else if (aluop == OR) {
                    result = src1 | src2;
                    SetAluFlags(ALUZERO(result), false, false, ALUSIGN(result));
                } else if (aluop == LSHL) {
                    // NOTE: Using any src2 value above 63 is invalid/undefined.
                    // The current FPGA behavior is to use the lower 6 bits
                    // and that is what we are doing here. This may change in the future.
                    uint64_t intermediate;
                    if ((src2 & 0x3F) < 32) {
                        result = src1 << (src2 & 0x3F);
                        intermediate = static_cast<uint64_t>(src1) << (static_cast<uint64_t>(src2) & 0x3F);
                    } else {
                        result = 0;
                        intermediate = 0;
                    }
                    SetAluFlags(ALUZERO(result), LSHLCARRY(intermediate), false, ALUSIGN(result));
                } else if (aluop == LSHR) {
                    // NOTE: Using any src2 value above 63 is invalid/undefined.
                    // The current FPGA behavior is to use the lower 6 bits
                    // and that is what we are doing here. This may change in the future.
                    uint64_t intermediate;
                    if ((src2 & 0x3F) < 32) {
                        result = src1 >> (src2 & 0x3F);
                        intermediate = static_cast<uint64_t>(src1) >> (static_cast<uint64_t>(src2) & 0x3F);
                    } else {
                        result = 0;
                        intermediate = 0;
                    }
                    SetAluFlags(ALUZERO(result), ALUCARRY(intermediate), false, ALUSIGN(result));
                } else if (aluop == ADD) {
                    // Perform addition in signed 64-bit space
                    int64_t intermediate = static_cast<int32_t>(src1);
                    intermediate += static_cast<int32_t>(src2);
                    uint64_t uintermediate = src1;
                    uintermediate += src2;
                    // Truncate the result back to 32-bits
                    result = static_cast<uint32_t>(intermediate);
                    // ALUCARRY: unsigned addition
                    SetAluFlags(ALUZERO(result), ALUCARRY(uintermediate), ALUOVERFLOW(src1, src2, intermediate), ALUSIGN(result));  
                } else if (aluop == SUB) {
                    // Perform subtraction in signed 64-bit space
                    int64_t intermediate = static_cast<int32_t>(src1);
                    intermediate -= static_cast<int32_t>(src2);
                    // Truncate the result back to 32-bits
                    result = static_cast<uint32_t>(intermediate);
                    // ALUOVERFLOW: change src2 to -src2 in 2's complement since ALUOVERFLOW algorithm is based on addition 
                    // ALUCARRY: in subtraction, carry flag is on when src1 < src2. ALUCARRY definition is not correct when src1 is a negative int32
                    if (src1 >= src2) {
                        SetAluFlags(ALUZERO(result), false, ALUOVERFLOW(src1, (~src2)+1, intermediate), ALUSIGN(result));
                    } else {
                        SetAluFlags(ALUZERO(result), true, ALUOVERFLOW(src1, (~src2)+1, intermediate), ALUSIGN(result));
                    }                    
                } else if (aluop == MUL) {
                    // Perform multiplication in unsigned 32-bit space but truncate the sources to 16 bits (see HPCC AC HLD)
                    uint32_t intermediate = UNMASK_16(src1);
                    intermediate *= UNMASK_16(src2);
                    SetAluFlags(ALUZERO(intermediate), false, false, ALUSIGN(intermediate));
                    result = intermediate;
                } else {
                    std::stringstream ss;
                    ss << "Invalid aluop=" << aluop;
                    throw std::runtime_error(ss.str());
                }
                // Store operation result in destination
                if (opdest == ALUDEST_NONE) {
                    // Nothing needs to be done here
                } else if (opdest == ALUDEST_REG) {
                    reg[dest] = result;
                } else if (opdest == ALUDEST_FLAGS) {
                    AssignValueToFlags(result);
                } else {
                    std::stringstream ss;
                    ss << "Invalid opdest=" << opdest;
                    throw std::runtime_error(ss.str());
                }
            } else if (optype == REGISTER) {
                uint32_t src = 0;
                if (opsrc == IMM) {
                    src = (uint32_t) imm;
                } else if (opsrc == REGSRC_REG) {
                    src = reg[regA];
                } else if (opsrc == TCC) {
                    src = (uint32_t) tcc;
                } else if (opsrc == VECADDR) {
                    src = vecaddr;
                } else if (opsrc == REGSRC_CFB) {
                    src = cfb;
                } else if (opsrc == REGSRC_FLAGS) {
                    src = flags;
                } else if (opsrc == DIRTYMASK) {
                    // NOTE: Deprecated instruction!
                    std::stringstream ss;
                    ss << "DIRTYMASK is a deprecated instruction";
                    throw std::runtime_error(ss.str());
                } else if (opsrc == REGSRC_STACK) {
                    if (stack.size() > 0) {
                        src = stack.top();
                        stack.pop();
                    } else {
                        stackUnderflowAlarm = true;
                    }
                } else {
                    std::stringstream ss;
                    ss << "Invalid opsrc=" << opsrc;
                    throw std::runtime_error(ss.str());
                }
                if (opdest == REGDEST_REG) {
                    reg[dest] = src;
                } else if (opdest == REGDEST_CFB) {
                    cfb = src;
                } else if (opdest == REGDEST_FLAGS) {
                    AssignValueToFlags(src);
                } else if (opdest == REGDEST_STACK) {
                    stack.push(src);
                } else {
                    std::stringstream ss;
                    ss << "Invalid opdest=" << opdest;
                    throw std::runtime_error(ss.str());
                }
            } else if (optype == VECTOR) {
                if (vptype == VPLOCAL) {
                    if (vpop == REPEAT_I) {
                        // Repeat next vector the amount of times specified by the Immediate field.
                        // If the value is all 1's loop forever.
                        repeatFlag = true;
                        repeat = (uint32_t) imm;
                    } else if (vpop == END_I) {
                        // End pattern with status code from immediate
                        Stop((uint32_t) imm, 1);
                    } else if (vpop == FAILBR_I) {
                        // Set the address to branch to on next fail from immediate field
                        failbrFlag = true;
                        failbr = (uint32_t) imm;
                    } else if (vpop == REPEAT_RA) {
                        // Repeat next vector the amount of times specified by the value in Register A.
                        // If the value is all 1's loop forever.
                        repeatFlag = true;
                        repeat = reg[regA];
                    } else if (vpop == END_RA) {
                        // End pattern with status code from Register A
                        Stop(reg[regA], 1);
                    } else if (vpop == FAILBR_RA) {
                        // Set the address to branch to on next fail from Register A
                        failbrFlag = true;
                        failbr = reg[regA];
                    } else {
                        std::stringstream ss;
                        ss << "Invalid vpop=" << vpop;
                        throw std::runtime_error(ss.str());
                    }
                } else if (vptype == VPLOG) {
                    if (vpop == LOG1_I) {
                        log1 = (uint32_t) imm;
                    } else if (vpop == LOG2_I) {
                        log2 = (uint32_t) imm;
                    } else if (vpop == PATID_I) {
                        patid = (uint32_t) imm;
                        // Clear per-pattern fail count. TODO: Update HLD that says it is cleared on a pcall
                        patfcnt = 0;
                        // Clear pattern cycle count. TODO: Update HLD that says it is cleared on a pcall
                        pcc = 0;
                    } else if (vpop == LOG1_RA) {
                        log1 = reg[regA];
                    } else if (vpop == LOG2_RA) {
                        log2 = reg[regA];
                    } else if (vpop == PATID_RA) {
                        patid = reg[regA];
                        // Clear per-pattern fail count. TODO: Update HLD that says it is cleared on a pcall
                        patfcnt = 0;
                        // Clear pattern cycle count. TODO: Update HLD that says it is cleared on a pcall
                        pcc = 0;
                    } else {
                        std::stringstream ss;
                        ss << "Invalid vpop=" << vpop;
                        throw std::runtime_error(ss.str());
                    }
                } else if (vptype == VPOTHER) {
                    if (vpop == TRIGGERSIB) {
                        // TODO
                    } else if (vpop == BLKFAIL_I) {
                        blockFails = (imm == 0x1);
                    } else if (vpop == DRAINFIFO_I) {
                        // Drain Pin FIFO
                        // Purposely create a near underrun condition to minimize latency
                        // between instructions operations and final output to the DUT.
                        // Internal. No need to implement it in this model.
                    } else if (vpop == EDGECNTR_I) {
                        edgeCounter = (imm == 0x1);
                    } else if (vpop == CLRSTICKY) {
                        // Clear sticky error flags
                        CLRBIT(flags, FLAGS_LSE_BIT);
                        CLRBIT(flags, FLAGS_GSE_BIT);
                        CLRBIT(flags, FLAGS_DSE_BIT);
                    } else if (vpop == CAPTURE_I) {
                        // TODO
                    } else if (vpop == RSTCYCLCNT) {
                        // Reset the user cycle counter special register
                        ucc = 0;
                    } else if (vpop == RSTCAPMEM) {
                        // TODO
                    } else if (vpop == RSTFAILCNT) {
                        // Resets total fail count and pin fail counters via internal PVC  
                        failcnt = 0;
                        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
                            Channel& channel = channels[ch];
                            channel.failcnt = 0;
                        }                        
                    } else {
                        std::stringstream ss;
                        ss << "Invalid vpop=" << vpop;
                        throw std::runtime_error(ss.str());
                    }
                }
            } else if (optype == BRANCH) {
                // Decide whether or not we have to branch
                bool branch = false;
                if (cond == COND_NONE) {
                    branch = true;
                } else if (cond == LSE) {
                    branch = BIT(flags, 17) == 0x1;
                } else if (cond == DSE) {
                    branch = BIT(flags, 18) == 0x1;
                } else if (cond == GSE) {
                    branch = BIT(flags, 19) == 0x1;
                } else if (cond == WIREDOR) {
                    branch = BIT(flags, 20) == 0x1;
                } else if (cond == LOADACTIVE) {
                    branch = BIT(flags, 21) == 0x1;
                } else if (cond == SWTRIGRCVD) {
                    branch = BIT(flags, 22) == 0x1;
                } else if (cond == DMTRIGRCVD) {
                    branch = BIT(flags, 23) == 0x1;
                } else if (cond == RCTRIGRCVD) {
                    branch = BIT(flags, 24) == 0x1;
                } else if (cond == ZERO) {
                    branch = BIT(flags, 25) == 0x1;
                } else if (cond == CARRY) {
                    branch = BIT(flags, 26) == 0x1;
                } else if (cond == OVERFLOW) {
                    branch = BIT(flags, 27) == 0x1;
                } else if (cond == SIGN) {
                    branch = BIT(flags, 28) == 0x1;
                } else if (cond == BE) {
                    // The branch is executed if the carry flag or the zero flag are set.
                    branch = (BIT(flags, 26) == 0x1) || (BIT(flags, 25) == 0x1);
                } else if (cond == LTE) {
                    // The branch is executed if the zero flag is set or if the sign flag is not equal to the over flow flag.
                    branch = (BIT(flags, 25) == 0x1) || (BIT(flags, 28) != BIT(flags, 27));
                } else if (cond == GTE) {
                    // The branch is executed if the zero flag is set or the sign flag is equal to the over flow flag.
                    branch = (BIT(flags, 25) == 0x1) || (BIT(flags, 28) == BIT(flags, 27));
                } else {
                    std::stringstream ss;
                    ss << "Invalid cond=" << cond;
                    throw std::runtime_error(ss.str());
                }
                
                if (invcond) {
                    branch = !branch;
                }

                if (branch) {  // If we have to branch
                    // Find out branch base offset
                    uint32_t offset;
                    if (base == BRBASE_GLOBAL) {
                        offset = 0;
                    } else if (base == BRBASE_CFB) {
                        offset = cfb;
                    } else if (base == BRBASE_RELATIVE) {
                        offset = vecaddr;
                    } else if (base == BRBASE_PFB) {
                        offset = pfb;
                    } else {
                        std::stringstream ss;
                        ss << "Invalid base=" << base;
                        throw std::runtime_error(ss.str());
                    }
                    // Then perform the actual jump
                    if (br == BRANCH_NONE) {
                        // No branch, do nothing
                    } else if (br == GOTO_I) {
                        StageJump(static_cast<uint32_t>(imm + offset));
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == CALL_I) {
                        // Push return vector address to the stack
                        stack.push(vecaddr + 1);
                        // Push pattern-level mask
                        maskStack.push(patmask);
                        // Apply staged pattern-level mask
                        patmask = stagedPatmask;
                        // Stage branch operation
                        StageJump(static_cast<uint32_t>(imm + offset));
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == PCALL_I) {
                        // Push return vector address to the stack
                        stack.push(vecaddr + 1);
                        // Push pattern-level mask
                        maskStack.push(patmask);
                        // Apply staged pattern-level mask
                        patmask = stagedPatmask;
                        // Update pfb
                        uint32_t address = static_cast<uint32_t>(imm + offset);
                        pfb = address;
                        // Stage branch operation
                        StageJump(address);
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == RET) {
                        // Pop return address from the stack and stage branch operation
                        if (stack.size() > 0) {
                            StageJump(stack.top());
                            stack.pop();
                        } else {
                            stackUnderflowAlarm = true;
                        }
                        // Pop pattern-level mask from the mask stack
                        if (maskStack.size() > 0) {
                            patmask = maskStack.top();
                            stagedPatmask = patmask;
                            maskStack.pop();
                        } else {
                            stackUnderflowAlarm = true;
                        }
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == GOTO_R) {
                        StageJump(reg[dest] + offset);
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == CALL_R) {
                        // Push return vector address to the stack
                        stack.push(vecaddr + 1);
                        // Push pattern-level mask
                        maskStack.push(patmask);
                        // Stage branch operation
                        StageJump(reg[dest] + offset);
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == PCALL_R) {
                        // Push return vector address to the stack
                        stack.push(vecaddr + 1);
                        // Push pattern-level mask
                        maskStack.push(patmask);
                        // Update pfb
                        uint32_t address = reg[dest] + offset;
                        pfb = address;
                        // Stage branch operation
                        StageJump(address);
                        // Update credit counter
                        creditCounter.Branch(vecaddr);
                    } else if (br == CLRSTACK) {
                        // Clear stack
                        while (stack.size() > 0) {
                            stack.pop();
                        }
                        // Clear mask stack
                        while (maskStack.size() > 0) {
                            maskStack.pop();
                        }
                    } else {
                        std::stringstream ss;
                        ss << "Invalid br=" << br;
                        throw std::runtime_error(ss.str());
                    }
                }
            }
            break;
        }
    }

    if (stop) {
        if (stopCount == 0) {
            std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
            LOG("debug") << "Stopped " << Name() << " at address=" << Hex(vecaddr) << " with endstatus=" << Hex(endstatus) << ", abort=" << m_abort;
            float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(now - startTime).count();
            LOG("debug") << "Simulator took " << elapsed << " wall clock seconds to simulate " << Time() << " seconds (" << tcc << " tester cycles)";
            // LOG("debug") << "Simulator speed = " << ((tcc / elapsed) / 1000.0f) << "kcycles/second";
            isStopped = true;
            return false;
        }
        stopCount--;
    }

    if (jumpFlag) {
        //std::cout << "Jumped from 0x" << std::hex << vecaddr << " to 0x" << std::hex << jumpTargetAddress << std::endl;
        vecaddr = jumpTargetAddress;
        // Clear staged jump
        jumpFlag = false;
    } else {
        vecaddr++;
    }

    return true;
}

void HpccAcSimulator::_PushVector(uint64_t link, uint64_t ctv, uint64_t mtv, uint64_t lrpt, uint64_t datah, uint64_t datal)
{
    if (repeatFlag && repeat == 0) {
        // Don't push the vector
        return;
    }
    InternalVector vector;
    vector.state = InternalVector::EXECUTE_VECTOR;
    vector.address                = vecaddr;
    vector.instructionRepeat      = repeatFlag;
    vector.instructionRepeatCount = 0;
    vector.instructionRepeatTotal = repeat;
    vector.channelLinking         = (link != 0);
    vector.channelLinkingCount    = 0;
    vector.channelLinkingTotal    = 1 << (SLICE(patternControl, 27, 26) + 1);
    vector.localRepeat            = lrpt > 0;
    vector.localRepeatCount       = 0;
    vector.localRepeatTotal       = static_cast<uint32_t>(lrpt + 1);
    vector.ctv                    = (ctv == 0x1);
    vector.mtv                    = static_cast<int>(mtv);
    vector.datah                  = datah;
    vector.datal                  = datal;
    vectors.push(vector);
    if (repeatFlag) {
        repeatFlag = false;
    }
    // Update credit counter
    creditCounter.Vector(vecaddr, static_cast<long>(lrpt), static_cast<long>(vector.channelLinkingTotal));
}

void HpccAcSimulator::_PushIOStateJam(uint64_t datah, uint64_t datal)
{
    if (repeatFlag && repeat == 0) {
        // Don't push the vector
        return;
    }
    InternalVector vector;
    vector.state                  = InternalVector::EXECUTE_IO_STATE_JAM;
    vector.address                = vecaddr;
    vector.instructionRepeat      = repeatFlag;
    vector.instructionRepeatCount = 0;
    vector.instructionRepeatTotal = repeat;
    vector.datah                  = datah;
    vector.datal                  = datal;
    vectors.push(vector);
    if (repeatFlag) {
        repeatFlag = false;
    }
    // Update credit counter
    creditCounter.Vector(vecaddr, 0, 1);
}

bool HpccAcSimulator::StepInternalVectorProcessor(TesterCycleContext* context)
{
    if (vectors.size() == 0) {
        return false;
    }

    InternalVector& vector = vectors.front();
    bool vectorDone = true;

    switch (vector.state) {
        case InternalVector::EXECUTE_IO_STATE_JAM: {
            _ExecuteIOStateJam(vector.datah, vector.datal);
            context->address = vector.address;
            context->ctv = false;
            context->linkCount = 0;
            context->ccwr = vector.instructionRepeatCount;
            context->validccwr = vector.instructionRepeat;
            if (vector.instructionRepeat) {
                vector.instructionRepeatCount++;
                if (vector.instructionRepeatCount < vector.instructionRepeatTotal) {
                    vectorDone = false;
                }
            }
            break;
        }
        case InternalVector::EXECUTE_VECTOR: {
            _ExecuteVector(vector.mtv, vector.channelLinking, vector.channelLinkingCount, vector.channelLinkingTotal, vector.datah, vector.datal);
            context->address = vector.address;
            context->ctv = vector.ctv;
            context->linkCount = vector.channelLinkingCount;
            context->ccwr = vector.instructionRepeatCount * vector.localRepeatTotal + vector.localRepeatCount;
            context->validccwr = vector.instructionRepeat || vector.localRepeat;
            bool done = true;
#ifndef LOCAL_REPEAT_THEN_CHANNEL_LINKING
            if (vector.localRepeat) {
                vector.localRepeatCount++;
                if (vector.localRepeatCount < vector.localRepeatTotal) {
                    done = false;
                }
            }
            if (done && vector.channelLinking) {
                vector.localRepeatCount = 0;
                vector.channelLinkingCount++;
                if (vector.channelLinkingCount < vector.channelLinkingTotal) {
                    done = false;
                }
            }
#else
            if (vector.channelLinking) {
                vector.channelLinkingCount++;
                if (vector.channelLinkingCount < vector.channelLinkingTotal) {
                    done = false;
                }
            }
            if (done && vector.localRepeat) {
                vector.channelLinkingCount = 0;
                vector.localRepeatCount++;
                if (vector.localRepeatCount < vector.localRepeatTotal) {
                    done = false;
                }
            }
#endif
            if (done && vector.instructionRepeat) {
                vector.localRepeatCount = 0;
                vector.channelLinkingCount = 0;
                vector.instructionRepeatCount++;
                if (vector.instructionRepeatCount < vector.instructionRepeatTotal) {
                    done = false;
                }
            }
            vectorDone = done;
            break;
        }
    }

    if (vectorDone) {
        vectors.pop();
    }
    return true;
}

bool HpccAcSimulator::StepTesterCycle(TesterCycleContext* context)
{
    while (!isStopped) {
        if (vectors.size() == 0) {
            // Keep executing pattern words / instructions until one or more output vectors are generated
            while (vectors.size() == 0) {
                bool success = StepVectorProcessor();
                if (!success) {
                    break;
                }
            }
        } else {
            // Execute first internal vector
            return StepInternalVectorProcessor(context);
        }
    }
    return false;
}

void HpccAcSimulator::_UpdateChannelEnabledClock(size_t ch, uint8_t c)
{
    Channel& channel = channels[ch];
    channel.mode = ENABLED_CLOCK_MODE;
    if (c == 0x0) {  // 0b00: 0
        channel.clk = '0';
    } else if (c == 0x1) {  // 0b01: 1
        channel.clk = '1';
    } else if (c == 0x2) {  // 0b10: R
        channel.clk = 'R';
        channel.clkRatioCount = channel.clkRatio - 1;
    } else if (c == 0x3) {  // 0b11: E
        channel.clk = 'E';
    }
    if (channel.clkRatio > 1) {
        channel.clkRatioCount = (channel.clkRatioCount + 1) % channel.clkRatio;
    }
}

void HpccAcSimulator::_ExecuteVector(int mtv, bool link, int linkCount, int numberOfLinks, uint64_t datah, uint64_t datal)
{
    // See Table: IO Drive/Compare mode 2-bit vector encodings
    Lambda(void, (HpccAcSimulator* sim, size_t ch, uint8_t c), {
        Channel& channel = sim->channels[ch];
        channel.source = VECTOR_SOURCE;
        channel.swtch = NO_SWITCH;
        if (BIT(sim->perChannelControlConfig1[ch], 29) == 0x0) {  // Non Enabled Clock Channel
            if (c == 0x0) {  // 0b00
                if (channel.mode == DRIVE_MODE) {
                    // Drive High-Z
                    channel.drvEnable = false;
                } else if (channel.mode == COMPARE_MODE) {
                    // Don't compare
                    channel.cmpEnable = false;
                }
            } else if (c == 0x1) {  // 0b01
                if (BIT(sim->perChannelControlConfig1[ch], 28) == 0x0) {  // Drive/Compare mode
                    // Switch character
                    if (channel.mode == DRIVE_MODE) {
                        channel.swtch = DRIVE_TO_COMPARE_SWITCH;
                        channel.mode = COMPARE_MODE;
                        // Source HPCC AC HLD: 102. Table: Per Channel Control Config3 Register Bits 
                        // Note: For Drive to Compare switches the pin simply becomes Z
                        // TODO: HLD should say 'X' instead of 'Z'
                        channel.cmpEnable = false;
                    } else if (channel.mode == COMPARE_MODE) {
                        channel.swtch = COMPARE_TO_DRIVE_SWITCH;
                        channel.mode = DRIVE_MODE;
                        channel.drvEnable = false;
                    }
                } else {  // Keep mode
                    // Do nothing
                }
            } else if (c == 0x2) {  // 0b10
                if (channel.mode == DRIVE_MODE) {
                    // Drive 0
                    channel.drv = '0';
                    channel.drvEnable = true;
                } else if (channel.mode == COMPARE_MODE) {
                    // Compare L
                    channel.cmp = 'L';
                    channel.cmpEnable = true;
                }
            } else if (c == 0x3) {  // 0b11
                if (channel.mode == DRIVE_MODE) {
                    // Drive 1
                    channel.drv = '1';
                    channel.drvEnable = true;
                } else if (channel.mode == COMPARE_MODE) {
                    // Compare H
                    channel.cmp = 'H';
                    channel.cmpEnable = true;
                }
            }
        } else {  // Enabled Clock
            sim->_UpdateChannelEnabledClock(ch, c);
        }
    }) _UpdateChannel;

    // Update vector-level mask
    vecmask = selectableFailMask[mtv];

    if (!link) {
        // Non-linked vector
        for (size_t ch = 0; ch < 32; ch++) {
            uint8_t c = SLICEM(datal, 2 * ch + 1, 2 * ch, 0x3);
            _UpdateChannel(this, ch, c);
        }
        for (size_t ch = 32; ch < HPCC_CHANNELS; ch++) {
            uint8_t c = SLICEM(datah, 2 * ch + 1, 2 * ch, 0x3);
            _UpdateChannel(this, ch, c);
        }
    } else {
        // Channel linked vector
        for (size_t ch = 0; ch < 32; ch++) {
            size_t vch = ch + linkCount;  // Virtual channel index
            if (numberOfLinks == 1 || ch % numberOfLinks == 0) {
                // "Linked" channel
                uint8_t c;
                c = SLICEM(datal, 2 * vch + 1, 2 * vch, 0x3);
                _UpdateChannel(this, ch, c);
            } else {
                // "Non-linked" channel: Keep this channel at the current state.
            }
        }
        for (size_t ch = 32; ch < HPCC_CHANNELS; ch++) {
            size_t vch = ch + linkCount;  // Virtual channel index
            if (numberOfLinks == 1 || ch % numberOfLinks == 0) {
                // "Linked" channel
                uint8_t c;
                c = SLICEM(datah, 2 * vch + 1, 2 * vch, 0x3);
                _UpdateChannel(this, ch, c);
            } else {
                // "Non-linked" channel: Keep this channel at the current state.
            }
        }
    }
}

void HpccAcSimulator::_ExecuteIOStateJam(uint64_t datah, uint64_t datal)
{
    Lambda(void, (HpccAcSimulator* sim, size_t ch, uint8_t c), {
        Channel& channel = sim->channels[ch];
        channel.source = IOSTATEJAM_SOURCE;
        if (BIT(sim->perChannelControlConfig1[ch], 29) == 0x0) {  // Non Enabled Clock Channel
            if (c == 0x0) {  // 0b00
                // Drive 0
                channel.mode = DRIVE_MODE;
                channel.drv = '0';
                channel.drvEnable = true;
            } else if (c == 0x1) {  // 0b01
                // Drive 1
                channel.mode = DRIVE_MODE;
                channel.drv = '1';
                channel.drvEnable = true;
            } else if (c == 0x2) {  // 0b10
                // Drive High-Z
                channel.mode = DRIVE_MODE;
                channel.drvEnable = false;
            } else if (c == 0x3) {  // 0b11
                // Don't compare
                channel.mode = COMPARE_MODE;
                channel.cmpEnable = false;
            }
        } else {  // Enabled Clock
            sim->_UpdateChannelEnabledClock(ch, c);
        }
    }) _UpdateChannel;

    for (size_t ch = 0; ch < 32; ch++) {
        uint8_t c = SLICEM(datal, 2 * ch + 1, 2 * ch, 0x3);
        _UpdateChannel(this, ch, c);
    }
    for (size_t ch = 32; ch < HPCC_CHANNELS; ch++) {
        uint8_t c = SLICEM(datah, 2 * ch + 1, 2 * ch, 0x3);
        _UpdateChannel(this, ch, c);
    }
}

void HpccAcSimulator::ExecuteTesterCycleDrivePhase(TesterCycleContext* context)
{
    context->pinEnabledClock = 0x0;     // Per-pin enabled clock state

    context->pinVectorDirection = 0x0;  // Per-pin vector direction: 1 = Drive, 0 = Compare
    context->pinVectorDirectionInCapture = 0x0;
    context->pinVectorEnable = 0x0;     // Per-pin vector enabled? (i.e. enabled = 0, 1, L, H; disabled = Z, X)
    context->pinVectorValue = 0x0;      // Per-pin vector value: For drive: {0, 1}, For compare: {L, H}

    context->pinState = 0x0;            // Per-pin logic state

    context->time = Time();

    if (m_bfm != nullptr) {
        //
        // Logic to support generic devices/bfms connected to the simulator
        //
        // Encode the current state and logic value of the channels
        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
            const Channel& channel = channels[ch];
            if (channel.mode == DRIVE_MODE) {
                SETBIT(context->pinVectorDirection, ch);
                if (channel.drvEnable) {
                    SETBIT(context->pinVectorDirectionInCapture, ch);
                    SETBIT(context->pinVectorEnable, ch);
                    if (channel.drv == '1') {
                        SETBIT(context->pinVectorValue, ch);
                    }
                    // Drive drv character ('0' or '1') at (time + tdrv)
                    m_bfm->Drive(slot, slice, ch, context->time + channel.tdrv, channel.drv);
                } else {
                    // Drive 'Z' character logic at (time + tdrv)
                    m_bfm->Drive(slot, slice, ch, context->time + channel.tdrv, 'z');
                }
            } else if (channel.mode == COMPARE_MODE) {
                // Note that we don't need to drive the logic for compares (except for internal loopback)
                if (BIT(captureControl, 9)) {  // Internal Loopback
                    // Set compare logic in internal loopback mode because this is how the design is coded
                    if (channel.cmpEnable) {
                        if (channel.cmp == 'L') {
                            m_bfm->Drive(slot, slice, ch, context->time + channel.tdrv, '0');
                        } else if (channel.cmp == 'H') {
                            m_bfm->Drive(slot, slice, ch, context->time + channel.tdrv, '1');
                        } else {
                            LOG("error") << "Invalid compare character '" << std::string(1, channel.cmp) << "' for channel " << ch << " of (" << slot << "," << slice << ")";
                        }
                    } else {
                        // Put a dummy (but valid) value of '0'
                        // or the "Invalid logic value from BFM for channel" check (below) will fail
                        m_bfm->Drive(slot, slice, ch, context->time + channel.tdrv, '0');
                    }
                }
                // End workaround
            } else if (channel.mode == ENABLED_CLOCK_MODE) {
                SETBIT(context->pinEnabledClock, ch);
                uint8_t riseRatioCount = static_cast<uint8_t>(channel.trise / m_period);
                float relTrise = channel.trise - riseRatioCount * m_period;
                uint8_t fallRatioCount = static_cast<uint8_t>(channel.tfall / m_period);
                float relTfall = channel.tfall - fallRatioCount * m_period;

                // TODO: Only first edge is implemented
                float firstEdge = std::min(relTrise, relTfall);
                float drive0Time = context->time + firstEdge;
                float drive1Time = context->time + firstEdge;
                
                //
                // NOTE: The Enabled Clock "waveform" is always running in the background
                //

                // TODO: 17-16 EC Direct Drive Event Select: 
                // Set 01 for direct edges to transition on first edge timing, set 00 for second edge timing
                // Set 10 to observe native Trise timing for rising edges and Tfall timing for falling edges 
                if (channel.clkRatio == 1) {
                    // Drive two edges
                    m_bfm->Drive(slot, slice, ch, context->time + channel.trise, '1');
                    m_bfm->Drive(slot, slice, ch, context->time + channel.tfall, '0');
                } else {
                    char logic = 'x';
                    float delta = 0.0;
                    if (channel.clkRatioCount == riseRatioCount) {
                        logic = '1';
                        delta = relTrise;
                        m_bfm->Drive(slot, slice, ch, context->time + delta, logic);
                    }
                    if (channel.clkRatioCount == fallRatioCount) {
                        logic = '0';
                        delta = relTfall;
                        m_bfm->Drive(slot, slice, ch, context->time + delta, logic);
                    }
                }

                //
                // NOTE: The 01R characters override the default Enabled Clock "waveform"
                //

                if (channel.clk == '0') {
                    m_bfm->OverrideDrive(slot, slice, ch, drive0Time, drive0Time + m_period, '0');
                } else if (channel.clk == '1') {
                    m_bfm->OverrideDrive(slot, slice, ch, drive1Time, drive1Time + m_period, '1');
                } else if (channel.clk == 'R' && channel.clkRatio > 1) {
                    // EC R Event Select
                    if (SLICE(perChannelControlConfig2[ch], 19, 18) == 0x0 || SLICE(perChannelControlConfig2[ch], 19, 18) == 0x1) {
                        // Transition: Just let the enabled clock waveform run
                    } else if (SLICE(perChannelControlConfig2[ch], 19, 18) == 0x2 || SLICE(perChannelControlConfig2[ch], 19, 18) == 0x3) {
                        // TODO: Implement "no transition"
                    }
                }
            } else {
                LOG("error") << "Invalid mode=" << channel.mode << " value for channel " << ch << " of (" << slot << "," << slice << ")";
            }
        }
        // FIXME: Warning! Nasty workaround! Warning!
        if (BIT(captureControl, 9)) {  // Internal Loopback
            for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
                const Channel& channel = channels[ch];
                if (channel.source == IOSTATEJAM_SOURCE && channel.mode == COMPARE_MODE && !channel.cmpEnable) {
                    m_bfm->Drive(slot, slice, ch, context->time + channel.tdrv, '1');
                }
            }
        }
        // End Nasty workaround
    }
}

void HpccAcSimulator::ExecuteTesterCycleComparePhase(TesterCycleContext* context)
{
    context->pinLaneData = 0x0;         // Expected capture data value
    context->pinLaneCheckMask = 0x0;    // Expected capture data check mask (0 = check = report error if mismatches; 1 = don't check = don't care)
    context->type = PIN_STATE;          // Capture type as specified in captureControl register
    context->pinFailures = 0x0;         // Per-pin failures
    context->pinMaskedFailures = 0x0;         // Per-pin failures, including masked failures
    context->pinZ = 0x0;

    // Compute active mask
    mask = testClassFailMask | patmask | vecmask;

    if (m_bfm != nullptr) {
        //
        // Logic to support generic devices/bfms connected to the simulator
        //

        // See what the bfm wrote to our compare channels
        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
            bool channelFailEvent = false;
            Channel& channel = channels[ch];

            // Update the pin state for this channel
            char logic = m_bfm->Sample(slot, slice, ch, context->time + channel.tcmp);
            char compareLogic = logic;
            if (logic == '0') {
                // Pin lane data value is 0, no update needed
            } else if (logic == '1') {
                // Update pin state value to a 1
                SETBIT(context->pinState, ch);
            } else if (logic == 'z') {
                // Mark this channel as a 'Z'
                SETBIT(context->pinZ, ch);
                if (channel.zislogic1) {
                    compareLogic = '1';
                    // Update pin state value to a 1
                    SETBIT(context->pinState, ch);
                } else {
                    compareLogic = '0';
                }
            } else {
                LOG("error") << "At tcc=" << tcc << ", invalid logic value of '" << std::string(1, logic) << "' from BFM for channel " << ch << " of " << Name();
            }

            if (channel.mode == DRIVE_MODE) {
                // Don't care about our drive channels (already taken care of above)
            } else if (channel.mode == COMPARE_MODE) {
                // Update fail event for this channel
                if (channel.cmpEnable) {
                    SETBIT(context->pinVectorEnable, ch);
                    if (channel.cmp == 'L') {
                        // Fail if we did not get a low value
                        channelFailEvent = compareLogic != '0';
                    } else if (channel.cmp == 'H') {
                        SETBIT(context->pinVectorValue, ch);
                        // Fail if we did not get a high value
                        channelFailEvent = compareLogic != '1';
                    } else {
                        LOG("error") << "Invalid compare character '" << std::string(1, channel.cmp) << "' for channel " << ch << " of " << Name();
                    }
                } else {
                    // If the pin is X (i.e. don't care), channelFailEvent does not need to be updated
                }
                if (channelFailEvent && BIT(mask, ch) == 0x0) {
                    SETBIT(context->pinFailures, ch);
                }
                if (channelFailEvent) {
                    SETBIT(context->pinMaskedFailures, ch);
                }
            } else if (channel.mode == ENABLED_CLOCK_MODE) {
                // Don't care about the enabled clock channels (already taken care of above)
            }
        }

        // Compute Pin Lane Data by looking at both the drive and compare side
        uint64_t captureType = SLICE(captureControl, 1, 0);
        if (captureType == 0x0) {  // 0b00 = Fail Status
            context->type = FAIL_STATUS;
            context->pinLaneData = context->pinFailures;
            context->pinLaneCheckMask = 0x0;  // 56-bit of zeros, i.e. check every channel
        } else if (captureType == 0x1) {  // 0b01 = Pin State
            context->type = PIN_STATE;
            context->pinLaneData = context->pinState;
            context->pinLaneCheckMask = 0x0;  // 56-bit of zeros, i.e. check every channel
            // Figure out which pins we will not check
            for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
                const Channel& channel = channels[ch];
                //
                // ATTENTION PLEASE: Here are the things that we don't check for!
                //
                bool maskCheck = false;
                if (!checkz) {
                    if (BIT(context->pinZ, ch)) {
                        // Don't check the pin if it is in the High-Z state
                        maskCheck = true;
                    }
                }
                if (channel.mode == COMPARE_MODE && channel.source == VECTOR_SOURCE && channel.swtch == DRIVE_TO_COMPARE_SWITCH) {  // Don't check drive-to-compare switch pins
                    maskCheck = true;
                }
                if (channel.mode == ENABLED_CLOCK_MODE && channel.clkRatio == 1 && (channel.clk == 'E' || channel.clk == 'R')) {
                    // FIXME: We can't correctly predict the state of E and R channels in pinLaneData for Ratio 1
                    maskCheck = true;
                }
                if (m_period < FASTEST_PERIOD_TO_CHECK_DRIVE_CHANNELS) {
                    // This is due a design limitation
                    if (channel.mode == DRIVE_MODE || channel.mode == ENABLED_CLOCK_MODE) {
                        maskCheck = true;
                    }
                }
                if (maskCheck) {
                    SETBIT(context->pinLaneCheckMask, ch);  
                }
            }
        } else if (captureType == 0x2) {  // 0b10 = Pin Direction
            context->type = PIN_DIRECTION;
            context->pinLaneData = context->pinVectorDirectionInCapture;
            context->pinLaneCheckMask = 0x0;  // 56-bit of zeros, i.e. check every channel
        } else if (captureType == 0x3) {  // 0b11 = Masked Fail 
            context->type = MASKED_FAIL;
            context->pinLaneData = context->pinMaskedFailures;
            context->pinLaneCheckMask = 0x0;  // 56-bit of zeros, i.e. check every channel
        } else {
            LOG("error") << "Invalid captureType=" << captureType << " for " << Name();
        }

    }
}

bool HpccAcSimulator::ExecuteTesterCycleCapturePhase(TesterCycleContext* context, SimulatedCapture* capture)
{
    bool failEvent = (context->pinFailures & ~mask) != 0;

    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        Channel& channel = channels[ch];
        if (BIT(context->pinFailures, ch) == 0x1) {
            if (exactFailCounts && !blockFails) {
                channel.failcnt++;
            }
        }
    }

    if (blockFails) {
        failEvent = false;
    }

    if (edgeCounter) {
        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
            Channel& channel = channels[ch];
            bool risingEdge  = BIT(context->pinLaneData, ch) == 0x1 && channel.prevPinState == 0x0;
            bool fallingEdge = BIT(context->pinLaneData, ch) == 0x0 && channel.prevPinState == 0x1;
            if (SLICE(perChannelControlConfig3[ch], 17, 16) == 0x1) {  // Rise
                if (risingEdge) {
                    channel.freqCounter++;
                }
            } else if (SLICE(perChannelControlConfig3[ch], 17, 16) == 0x2) {  // Fall
                if (fallingEdge) {
                    channel.freqCounter++;
                }
            } else if (SLICE(perChannelControlConfig3[ch], 17, 16) == 0x3) {  // Both
                if (risingEdge || fallingEdge) {
                    channel.freqCounter++;
                }
            }
        }
    }
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        Channel& channel = channels[ch];
        channel.prevPinState = BIT(context->pinLaneData, ch);
    }

    if (failEvent) {
        if (!BIT(flags, FLAGS_LSE_BIT)) {
            // Send a sticky error trigger on first local fail
            m_rc->SendTriggerHpccAc(
                static_cast<uint8_t>(this->cardType),
                static_cast<uint8_t>(this->m_dutId),
                static_cast<uint8_t>(this->m_domain),
                0,
                STICKY_ERROR
            );
            // Set the local sticky error flag
            SETBIT(flags, FLAGS_LSE_BIT);
        }
    }

    bool captureCountsWithinFailLimits = (failcnt < maxFailCaptureCount) && (patfcnt < maxFailCountPerPattern);
    bool captureCountsWithinCaptureLimits = (captureCount < maxCaptureCount);
    bool captureVector = false;

    if (BIT(captureControl, 2)) {  // Capture Fails
        captureVector = captureVector || (failEvent && captureCountsWithinFailLimits && captureCountsWithinCaptureLimits);
    }
    if (BIT(captureControl, 3)) {  // Capture CTVs
        captureVector = captureVector || (context->ctv && captureCountsWithinFailLimits && captureCountsWithinCaptureLimits);
    }
    if (BIT(captureControl, 4)) {  // Capture All
        captureVector = captureCountsWithinFailLimits && captureCountsWithinCaptureLimits;
    }    
    if (BIT(captureControl, 11)) {  // capture CTV after max fail
        captureVector = captureVector || (context->ctv && captureCountsWithinCaptureLimits);
    }    

    if (captureVector) {
        // Generate output capture
        for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
            const Channel& channel = channels[ch];
            if (channel.mode == ENABLED_CLOCK_MODE) {
                capture->pinEnabledClockChar[ch] = channel.clk;
            }
        }
        capture->patternInstanceId = patid;
        capture->userLogRegister1 = log1;
        capture->userLogRegister2 = log2;
        capture->totalCycleCount = tcc;
        capture->patternCycleCount = pcc;
        capture->cycleCountWithinRepeat = context->ccwr;
        capture->validCycleCountWithinRepeat = context->validccwr;
        capture->userCycleCount = ucc;
        capture->vectorAddress = context->address;
        capture->failEvent = failEvent;
        capture->ctvEvent = context->ctv;
        capture->linkMode = static_cast<uint8_t>(context->linkCount);
        capture->pinLaneData = context->pinLaneData;
        // Simulator/Checker fields
        capture->type = context->type;
        capture->pinLaneCheckMask = context->pinLaneCheckMask;
        capture->pinEnabledClock = context->pinEnabledClock;
        capture->pinVectorDirection = context->pinVectorDirection;
        capture->pinVectorEnable = context->pinVectorEnable;
        capture->pinVectorValue = context->pinVectorValue;
        capture->pinState = context->pinState;
        capture->pinFailures = context->pinFailures;
        capture->pinMaskedFailures = context->pinMaskedFailures;
        capture->afterAbort = false;

        if (captureTrace && (traceStartCycle <= tcc) && (tcc <= traceStopCycle)) {
            uint32_t pinLaneDataLow = 0xFFFFFFFF & capture->pinLaneData;
            uint32_t pinLaneDataHigh = 0xFFFFFFFF & (capture->pinLaneData >> 32);
            captureTraceFout << "0x" << std::hex << capture->totalCycleCount << ",";
            captureTraceFout << "0x" << std::hex << capture->userCycleCount << ",";
            captureTraceFout << "0x" << std::hex << capture->cycleCountWithinRepeat << ",";
            captureTraceFout << "0x" << std::hex << capture->patternCycleCount << ",";
            captureTraceFout << "0x" << std::hex << capture->userLogRegister2 << ",";
            captureTraceFout << "0x" << std::hex << capture->userLogRegister1 << ",";
            captureTraceFout << "0x" << std::hex << capture->patternInstanceId << ",";
            captureTraceFout << "0x" << std::hex << capture->vectorAddress << ",";
            captureTraceFout << static_cast<int>(capture->failEvent) << ",";
            captureTraceFout << static_cast<int>(capture->ctvEvent) << ",";
            captureTraceFout << static_cast<int>(capture->linkMode) << ",";
            captureTraceFout << "0x" << std::hex << pinLaneDataHigh << ",";
            captureTraceFout << "0x" << std::hex << pinLaneDataLow << ",";
            captureTraceFout << "0b" << LPad(Bin(capture->pinLaneData), HPCC_CHANNELS) << ",";
            captureTraceFout << "\n";
        }

        captureCount++;
    }

    if (failEvent) {
        if (exactFailCounts) {
            failcnt++;
            patfcnt++;
        }
    }

    if (failEvent) {
        if (failbrFlag) {
            StageJump(static_cast<uint32_t>(failbr));
        }
        failbrFlag = false;
    }

    if (failcnt >= maxFailCaptureCount) {
        stopOnFailAlarm = true;
        if (BIT(captureControl, 8)) {  // Stop on Max Fail Capture Count Hit
            exactFailCounts = false;  // Stop increasing fail counters for this pattern burst
            Stop(ABORT_END_STATUS, STOP_ON_FAIL_MOMENTUM, true);  // Set the abort bit!
            capture->afterAbort = true; // this vector is captured after abort is issued due to fpga momentum
        }
    }

    if (captureCount >= maxCaptureCount) {
        exactCaptureCounts = false;
    }

    // Increase cycle count registers
    tcc++;
    pcc++;
    ucc++;

    return captureVector;
}

void HpccAcSimulator::StageJump(uint32_t address)
{
    jumpFlag = true;
    jumpTargetAddress = address;
}

void HpccAcSimulator::SetAluFlags(bool zero, bool carry, bool overflow, bool sign)
{
    // LOG("info") << "zero " << zero << " carry " << carry << " overflow " << overflow << " sign " << sign ;
    if (zero) {
        SETBIT(flags, FLAGS_ALUZERO_BIT);
    } else {
        CLRBIT(flags, FLAGS_ALUZERO_BIT);
    }
    if (carry) {
        SETBIT(flags, FLAGS_ALUCARRY_BIT);
    } else {
        CLRBIT(flags, FLAGS_ALUCARRY_BIT);
    }
    if (overflow) {
        SETBIT(flags, FLAGS_ALUOVERFLOW_BIT);
    } else {
        CLRBIT(flags, FLAGS_ALUOVERFLOW_BIT);
    }
    if (sign) {
        SETBIT(flags, FLAGS_ALUSIGN_BIT);
    } else {
        CLRBIT(flags, FLAGS_ALUSIGN_BIT);
    }
}

void HpccAcSimulator::ResetFlags()
{
    flags = 0x0;
    // Always set bit 16
    SETBIT(flags, FLAGS_ALWAYSSET_BIT);
}

void HpccAcSimulator::AssignValueToFlags(uint32_t value)
{
    // Clear the bits that we can assign to
    flags &= ~0x01F0FFFF;
    // Assign the value to those bits
    flags |= value & 0x01F0FFFF;
    // Always set bit 16
    SETBIT(flags, FLAGS_ALWAYSSET_BIT);
}

void HpccAcSimulator::ResetAlarms()
{
    stopOnFailAlarm = false;
    stackOverflowAlarm = false;
    stackUnderflowAlarm = false;
}

std::string HpccAcSimulator::PinState() const
{
    std::string result = std::string(HPCC_CHANNELS, '?');
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        const Channel& channel = channels[ch];
        char c;
        if (channel.mode == DRIVE_MODE) {
            if (channel.drvEnable) {
                c = channel.drv;
            } else {
                c = 'Z';
            }
        } else if (channel.mode == COMPARE_MODE) {
            if (channel.cmpEnable) {
                c = channel.cmp;
            } else {
                c = 'X';
            }
        } else {
            c = 'E';
        }
        result[HPCC_CHANNELS - 1 - ch] = c;
    }
    return result;
}

std::string HpccAcSimulator::PinDirection() const
{
    std::string result = std::string(HPCC_CHANNELS, '?');
    for (size_t ch = 0; ch < HPCC_CHANNELS; ch++) {
        const Channel& channel = channels[ch];
        char c = '?';
        if (channel.mode == DRIVE_MODE || channel.mode == ENABLED_CLOCK_MODE) {
            c = 'D';
        } else if (channel.mode == COMPARE_MODE) {
            c = 'C';
        }
        result[HPCC_CHANNELS - 1 - ch] = c;
    }
    return result;
}

void HpccAcSimulator::ReadPattern(uint64_t address, uint8_t* patternWord) const
{
    char* data = reinterpret_cast<char*>(&memory[address]);
    memcpy(patternWord, data, PATTERN_WORD_SIZE);
}

void HpccAcSimulator::ProcessTrigger(uint32_t trigger)
{
    //uint8_t cardType         = static_cast<uint8_t>(SLICE(trigger, 15, 12));
    uint8_t domain           = static_cast<uint8_t>(SLICE(trigger, 11, 8));
    //uint8_t count            = static_cast<uint8_t>(SLICE(trigger, 7, 4));
    uint8_t triggerType      = static_cast<uint8_t>(SLICE(trigger, 3, 0));
    uint8_t dutId            = static_cast<uint8_t>(SLICE(trigger, 25, 20));
    uint64_t stickyErrorMask = SLICE(patternControl, 23, 9); 
    switch (triggerType) {
        case PATTERN_RESUME: {
            // Domain master(s) targeted will rebroadcast to their domain
            if (dutId == this->m_dutId && domain == this->m_domain && domainMaster) {
                m_rc->SendTriggerHpccAc(
                    static_cast<uint8_t>(this->cardType),
                    static_cast<uint8_t>(this->m_dutId),
                    static_cast<uint8_t>(this->m_domain),
                    0,
                    PATTERN_RESUME_PROPAGATE
                );
            }
            break;
        }
        case PATTERN_RESUME_PROPAGATE: {
            // Sets a testable flag (TOS Trigger Flag)
            if (dutId == this->m_dutId && domain == this->m_domain) {
                SETBIT(flags, FLAGS_SWTRIG_BIT);
            }
            break;
        }
        case DOMAIN_: {
            // Domain master(s) targeted will rebroadcast to their domain and update central domain
            if (dutId == this->m_dutId && domain == this->m_domain && domainMaster) {
                m_rc->SendTriggerHpccAc(
                    static_cast<uint8_t>(this->cardType),
                    static_cast<uint8_t>(this->m_dutId),
                    static_cast<uint8_t>(this->m_domain),
                    0,
                    DOMAIN_PROPAGATE
                );
            }
            break;
        }
        case DOMAIN_PROPAGATE: {
            // Domain members targeted set a testable flag (Domain Trigger Flag)
            if (dutId == this->m_dutId && domain == this->m_domain) {
                SETBIT(flags, DMTRIG_BIT);
            }
            break;
        }
        case STICKY_ERROR: {
            if (dutId == this->m_dutId && domain == this->m_domain) {
                SETBIT(flags, FLAGS_DSE_BIT);
            }
            if (dutId == this->m_dutId && this->domainMaster && BIT(stickyErrorMask, domain) == 0x1) {
                m_rc->SendTriggerHpccAc(
                    static_cast<uint8_t>(this->cardType),
                    static_cast<uint8_t>(this->m_dutId),
                    static_cast<uint8_t>(this->m_domain),
                    0,
                    GLOBAL_STICKY_ERROR_PROPAGATE
                );
            }
            break;
        }
        case GLOBAL_STICKY_ERROR_PROPAGATE: {
            // All AC FPGAs targeted set a testable flag (Global Sticky Error Flag)
            if (dutId == this->m_dutId && domain == this->m_domain) {
                SETBIT(flags, FLAGS_GSE_BIT);
            }
            break;
        }
        case CLEAR_GLOBAL_STICKY_ERROR: {
            // Domain master(s) targeted will rebroadcast to their domain. Master updates DSE and GSE flags in the central domain. 
            if (dutId == this->m_dutId && domain == this->m_domain) {
                CLRBIT(flags, FLAGS_GSE_BIT);
                CLRBIT(flags, FLAGS_DSE_BIT);
            }
            if (dutId == this->m_dutId && this->domainMaster && BIT(stickyErrorMask, domain) == 0x1) {
                m_rc->SendTriggerHpccAc(
                    static_cast<uint8_t>(this->cardType),
                    static_cast<uint8_t>(this->m_dutId),
                    static_cast<uint8_t>(this->m_domain),
                    0,
                    CLEAR_GLOBAL_STICKY_ERROR_PROPAGATE
                );
            }
            break;
        }
        case CLEAR_GLOBAL_STICKY_ERROR_PROPAGATE: {
            // Domain members targeted clear testable flags (Global Sticky Error and Domain Sticky Error Flag) 
            if (dutId == this->m_dutId && domain == this->m_domain) {
                CLRBIT(flags, FLAGS_GSE_BIT);
                CLRBIT(flags, FLAGS_DSE_BIT);
            }
            break;
        }
        case RC_: {
            // Domain master(s) targeted will rebroadcast to their domain
            if (dutId == this->m_dutId && domain == this->m_domain && domainMaster) {
                m_rc->SendTriggerHpccAc(
                    static_cast<uint8_t>(this->cardType),
                    static_cast<uint8_t>(this->m_dutId),
                    static_cast<uint8_t>(this->m_domain),
                    0,
                    RC_PROPAGATE
                );
            }
            break;
        }
        case RC_PROPAGATE: {
            // Domain members targeted set a testable flag (RC Trigger Flag)
            if (dutId == this->m_dutId && domain == this->m_domain) {
                SETBIT(flags, RCTRIG_BIT);
            }
            break;
        }
    }
}

}  // namespace hpcctbc

