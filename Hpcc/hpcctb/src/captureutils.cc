////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: captureutils.cc
//------------------------------------------------------------------------------
//    Purpose: 
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 11/23/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "captureutils.h"

namespace hpcctbc {

void FlattenCapture(const uint8_t* array, size_t length, size_t blockCount, std::vector<FlatCapture>* flatCapture)
{
    flatCapture->clear();
    ActualCaptureState actual;

    // Unpack capture data into blocks
    UnpackCapture(array, length, blockCount, &actual.blocks);

    bool success = true;
    while (success) {
        CaptureHeader* header;
        CaptureData* data;
        success = actual.GetValidCaptureData(&header, &data);
        if (success) {
            FlatCapture capture;
            capture.patternInstanceId      = header->patternInstanceId;
            capture.userLogRegister1       = header->userLogRegister1;
            capture.userLogRegister2       = header->userLogRegister2;
            capture.patternCycleCount      = header->patternCycleCount;
            capture.cycleCountWithinRepeat = header->cycleCountWithinRepeat;
            capture.userCycleCount         = header->userCycleCount;
            capture.totalCycleCount        = header->totalCycleCount;
            capture.vectorAddress = data->vectorAddress;
            capture.failEvent     = data->failEvent;
            capture.ctvEvent      = data->ctvEvent;
            capture.linkMode      = data->linkMode;
            capture.pinLaneData   = data->pinLaneData;
            flatCapture->push_back(capture);
            // Increase the number of actual vectors captured
            actual.totalCaptureCount++;
            // Advance actual capture data pointers
            actual.Next();
        } else {
            break;
        }
    }
}

void CaptureOffsets(const uint8_t* array, size_t length, size_t blockCount, std::vector<CaptureOffsetEntry>* offsets)
{
    offsets->clear();
    ActualCaptureState actual;

    // Unpack capture data into blocks
    UnpackCapture(array, length, blockCount, &actual.blocks);

    bool success = true;
    while (success) {
        CaptureHeader* header;
        CaptureData* data;
        success = actual.GetValidCaptureData(&header, &data);
        if (success) {
            uint64_t pinLaneData = data->pinLaneData;
            for (int ch = 0; ch < HPCC_CHANNELS; ch++) {
                if ((pinLaneData & 0x1) == 0x1) {
                    CaptureOffsetEntry offset;
                    offset.channel = ch;
                    offset.offset = data->vectorAddress;
                    offsets->push_back(offset);
                }
                pinLaneData = pinLaneData >> 1;
            }
            // Increase the number of actual vectors captured
            actual.totalCaptureCount++;
            // Advance actual capture data pointers
            actual.Next();
        } else {
            break;
        }
    }
}

}  // namespace hpcctbc

