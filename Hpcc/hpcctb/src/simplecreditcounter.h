////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: simplecreditcounter.h
//------------------------------------------------------------------------------
//    Purpose: Credit counter model
//------------------------------------------------------------------------------
// Created by: Dean Glazeski
//       Date: 02/24/2016
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <cstdint>

namespace hpcctbc {

class SimpleCreditCounter
{
public:
    /**
     * Create a simple credit counter that throws when the balance gets too low.
     *
     * @param period The period the FPGA is expected to run at.
     * @param threshold Marks the low-level watermark for the credit counter.
     * @param initial_balance The initial amount of balance available before pattern execution.  Defaults to 0.
     */
    explicit SimpleCreditCounter(float period, int64_t threshold = 0, int64_t initialBalance = 0);

#ifndef SWIG
    /**
     * Advanced usage constructor that enables prestage simulation and assumes a subcycle of zero.  It is expected that
     * the user will follow this up with a call to SetSubcycle() to update to the proper subcycle for simulation.
     */
    SimpleCreditCounter();

    /**
     * Sets the desired subcycle for simulation.
     *
     * @param subcycle The subcycle for simulation.
     */
    void SetSubcycle(uint64_t subcycle);
#endif

    /**
     * Simple destructor with very little to do.
     */
    ~SimpleCreditCounter();

    /**
     * Simulates the effect of a vector on the credit balance.
     *
     * @param local_repeat The amount of local repeat assigned to this vector.  Defaults to 0.
     * @param link_width The link mode of this vector.  Defaults to 1.
     *
     * @return The available balance after vector execution.
     */
    int64_t Vector(int localRepeat = 0, int linkWidth = 1);

    /**
     * Simulates the effect of a metadata vector on the credit balance.
     *
     * @return The available balance after vector execution.
     */
    int64_t Metadata();

    /**
     * Simulates the effect of doing a simple ALU type instruction on the credit balance.
     *
     * @return The available balance after instruction execution.
     */
    int64_t Instruction();

    /**
     * Simulates executing a repeat instruction with a given repeat count.
     *
     * @param repeat_count The number of cycles the repeat is active for.
     *
     * @return The available balance after instruction execution.
     */
    int64_t InstructionRepeat(int64_t repeatCount);

    /**
     * Simulates executing a jump, call, or return instruction.
     *
     * @return The available balance after the branch instruction.
     */
    int64_t Branch();

    /**
     * Provides information on how much the balance was overdrawn.
     *
     * @return The amount of overrun encountered.
     */
    int64_t GetUnderrunAmount() const;

private:
    int64_t threshold; ///< Threshold for triggering an underrun.
    int64_t balance; ///< Running balance available.
    int64_t underrunAmount; ///< Amount we overran our buffer.
    uint64_t m_subcycle; ///< Subcycle the FPGA is running at.  This depends on period.
    bool prestage; ///< Flag to control prestage.  If true, we model FPGA prestage.

    int64_t Update(int64_t delta);
};

}
