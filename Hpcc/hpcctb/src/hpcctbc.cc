////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpcctbc.cc
//------------------------------------------------------------------------------
//    Purpose: HPCC High-Performance Testbench Methods and Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/30/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include <stdexcept>

#include "hpcctbc.h"
#include "defines.h"
#include "logging.h"

#undef OVERFLOW  // math.h defines OVERFLOW, shame on them
#include "symbols.h"

namespace hpcctbc {

static_assert(HPCC_CHANNELS >= 33, "Expecting at least 33 channels (or the code won't work)");
static_assert(HPCC_CHANNELS <= 64, "Expecting 64 channels of less (or the code won't work)");

uint8_t EncodeVectorChar(char c, bool enclk, bool keepmode)
{
    if (!enclk) {
        // 2-bit Pin Data Encoding - Drive/Compare Mode
        // Bit encoding     When pin is in Drive mode          When pin is in Compare mode 
        // 00               Z - High-Z                         X - No Compare 
        // 01               S - Switch to Compare mode         S - Switch to Drive mode 
        // 10               0 - Drive 0                        L - Compare 0 
        // 11               1 - Drive 1                        H - Compare 1 
        // Alternate "Keep" mode 2-bit Pin Data Encoding 
        // Bit encoding     For pin in Drive mode              For pin in Compare mode 
        // 00               Z - High-Z                         X - No Compare 
        // 01               K - Repeat last cycle drive state  K - Repeat last cycle compare state 
        // 10               0 - Drive 0                        L - Compare 0 
        // 11               1 - Drive 1                        H - Compare 1
        if (c == 'Z' || c == 'X') {
            return 0x0;  // 0b00
        } else if (c == 'S' && !keepmode) {
            return 0x1;  // 0b01
        } else if (c == 'K' && keepmode) {
            return 0x1;  // 0b01
        } else if (c == '0' || c == 'L') {
            return 0x2;  // 0b10
        } else if (c == '1' || c == 'H') {
            return 0x3;  // 0b11
        } else {
            std::stringstream ss;
            ss << "Invalid Vector data character '" << c << "'";
            throw std::runtime_error(ss.str());
        }
    } else {
        // Enabled Clock mode 2-bit Pin Data Encoding
        // Bit encoding     For pin in Enabled Clock Mode 
        // 00               0 - Drive 0
        // 01               1 - Drive 1
        // 10               R - Reset
        // 11               E - Enable
        if (c == '0') {
            return 0x0;  // 0b00
        } else if (c == '1') {
            return 0x1;  // 0b01
        } else if (c == 'R') {
            return 0x2;  // 0b10
        } else if (c == 'E') {
            return 0x3;  // 0b11
        } else {
            std::stringstream ss;
            ss << "Invalid Vector data character '" << c << "' for enabled clock channel";
            throw std::runtime_error(ss.str());
        }
    }
}

void EncodeVectorLiteral(const std::string& literal, uint64_t enclk, uint64_t keepmode, uint64_t* datah, uint64_t* datal)
{
    if (literal.size() != HPCC_CHANNELS) {
        std::stringstream ss;
        ss << "Expecting exactly " << HPCC_CHANNELS << " characters (Vector data) but " << literal.size() << " found";
        throw std::runtime_error(ss.str());
    }
    uint64_t& low = *datal;
    low = 0;
    for (int ch = 32 - 1; ch >= 0; ch--) {
        size_t j = (HPCC_CHANNELS - 1) - ch;
        low = low << 2;
        low = low | EncodeVectorChar(literal[j], BIT(enclk, ch), BIT(keepmode, ch));
    }
    uint64_t& high = *datah;
    high = 0;
    for (int ch = HPCC_CHANNELS - 1; ch >= 32; ch--) {
        size_t j = (HPCC_CHANNELS - 1) - ch;
        high = high << 2;
        high = high | EncodeVectorChar(literal[j], BIT(enclk, ch), BIT(keepmode, ch));
    }
}

uint8_t EncodeIOStateJamChar(char c, bool enclk)
{
    if (!enclk) {
        // Pin State Word Encoding 
        // Bit encoding     Description
        // 00               0 - Drive a low state on the channel 
        // 01               1 - Drive a high state on the channel
        // 10               Z - Set a drive direction, but tri-state the channel 
        // 11               X - Set a compare direction, but do not do a compare event 
        if (c == '0') {
            return 0x0;  // 0b00
        } else if (c == '1') {
            return 0x1;  // 0b01
        } else if (c == 'Z') {
            return 0x2;  // 0b10
        } else if (c == 'X') {
            return 0x3;  // 0b11
        } else {
            std::stringstream ss;
            ss << "Invalid IO State Jam data character '" << c << "'";
            throw std::runtime_error(ss.str());
        }
    } else {
        // Enabled Clock mode 2-bit Pin Data Encoding
        // Bit encoding     For pin in Enabled Clock Mode 
        // 00               0 - Drive 0
        // 01               1 - Drive 1
        // 10               R - Reset
        // 11               E - Enable
        if (c == '0') {
            return 0x0;  // 0b00
        } else if (c == '1') {
            return 0x1;  // 0b01
        } else if (c == 'R') {
            return 0x2;  // 0b10
        } else if (c == 'E') {
            return 0x3;  // 0b11
        } else {
            std::stringstream ss;
            ss << "Invalid IO State data character '" << c << "' for enabled clock channel";
            throw std::runtime_error(ss.str());
        }
    }
}

void EncodeIOStateJamLiteral(const std::string& literal, uint64_t enclk, uint64_t* datah, uint64_t* datal)
{
    if (literal.size() != HPCC_CHANNELS) {
        std::stringstream ss;
        ss << "Expecting exactly " << HPCC_CHANNELS << " characters (IO State Jam data) but " << literal.size() << " found";
        throw std::runtime_error(ss.str());
    }
    uint64_t& low = *datal;
    low = 0;
    for (int ch = 32 - 1; ch >= 0; ch--) {
        size_t j = (HPCC_CHANNELS - 1) - ch;
        low = low << 2;
        low = low | EncodeIOStateJamChar(literal[j], BIT(enclk, ch));
    }
    uint64_t& high = *datah;
    high = 0;
    for (int ch = HPCC_CHANNELS - 1; ch >= 32; ch--) {
        size_t j = (HPCC_CHANNELS - 1) - ch;
        high = high << 2;
        high = high | EncodeIOStateJamChar(literal[j], BIT(enclk, ch));
    }
}

/*

@packing.high
struct PatternWord:
    # Word types
    VECTOR_WORD      = 0b00
    METADATA_WORD    = 0b01
    PINSTATE_WORD    = 0b10
    INSTRUCTION_WORD = 0b11
    bit[1:0] Type
    when Type == VECTOR_WORD:
        # Per Vector Control (PVC) bits
        bit[3:0]   PVCReserved              # pvcrsvd
        bit        LinkEnable               # link
        bit        CTV                      # ctv
        bit[2:0]   MTV                      # mtv
        bit[4:0]   LocalRepeat              # lrpt
        # Vector data
        bit[111:0] Data                     # data
    when Type == METADATA_WORD:
        bit[125:0] Data                     # data
    when Type == PINSTATE_WORD:
        bit[11:0] Undefined                 # undefined
        bit[1:0] PinStateWordType           # stype
        when PinStateWordType == IOSTATEJAM:
            bit[111:0] Data                 # data
        when PinStateWordType == PLISTMASK:
            bit[55:0] Data                  # data
            bit[55:0] Unused                # unused
        when PinStateWordType == PLISTUNMASK:
            bit[55:0] Data                  # data
            bit[55:0] Unused                # unused
        # Undefined pinstate type
        when PinStateWordType == 0b11:
            bit[111:0] Invalid
    when Type == INSTRUCTION_WORD:
        bit[15:0] SoftwareReserved          # swrsvd
        bit[39:0] Reserved                  # rsvd
        bit[3:0]  OpTypeSelect              # optype
        when OpTypeSelect == EXT:
            bit[2:0] ActionType             # extop
            when ActionType == TRIGGER:
                bit[3:0] Action             # action
            when ActionType == OTHER:
                bit[3:0] Action             # action
            when ActionType == TRIGGERNOW:
                bit[3:0] Action             # action
            # Invalid action types. TODO: Add more
            when ActionType == 0b000:
                bit[3:0] Invalid
        when OpTypeSelect == ALU:
            bit[1:0] OpDestination          # opdest
            bit[1:0] OpSource               # opsrc
            bit[2:0] Operation              # aluop
        when OpTypeSelect == REGISTER:
            bit[3:0] OpDestination          # opdest
            bit[2:0] OpSource               # opsrc
        when OpTypeSelect == VECTOR:
            bit[2:0] VPType                 # vptype
            when VPType == VPLOG:
                bit[3:0] VPOp               # vpop
            when VPType == VPLOCAL:
                bit[3:0] VPOp               # vpop
            when VPType == VPOTHER:
                bit[3:0] VPOp               # vpop
            # Invalid VP types. TODO: Add more
            when VPType == 0b000:
                bit[3:0] Invalid
        when OpTypeSelect == BRANCH:
            bit[6:0] OperationCoding          
        bit       InvCondition              # invcond
        bit[3:0]  Condition                 # cond
        bit[1:0]  BranchBase                # base
        bit[4:0]  BranchType                # br
        bit[4:0]  Destination               # dest
        bit[4:0]  RegisterSelectB           # regB
        bit[4:0]  RegisterSelectA           # regA
        bit[31:0] ImmediateValue            # imm

struct CaptureHeader:
    bit       CapturedVectorsValidFlags[8]
    bit[31:0] PatternInstanceId
    bit[31:0] UserLogRegister1
    bit[31:0] UserLogRegister2
    bit[31:0] PatternCycleCount
    bit[31:0] CycleCountWithinRepeat
    bit[31:0] UserCycleCount
    bit[31:0] TotalCycleCount

@packing.high
struct CaptureData:
    bit[31:0] VectorAddress
    bit       FailEvent
    bit       CtvEvent
    bit[2:0]  Reserved
    bit[2:0]  LinkMode
    bit[55:0] PinLaneData

*/

void WriteVectorWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t link,
    uint64_t ctv,
    uint64_t mtv,
    uint64_t lrpt,
    uint64_t datah,
    uint64_t datal,
    uint64_t pvcrsvd
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In WriteVectorWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    array[0xF] = (UNMASK_2(VECTOR_WORD) << 6) | (UNMASK_4(pvcrsvd) << 2) | (UNMASK_1(link) << 1) | (UNMASK_1(ctv));
    array[0xE] = (UNMASK_3(mtv) << 5) | (UNMASK_5(lrpt));
    array[0xD] = UNMASK_8(datah >> 40);
    array[0xC] = UNMASK_8(datah >> 32);
    array[0xB] = UNMASK_8(datah >> 24);
    array[0xA] = UNMASK_8(datah >> 16);
    array[0x9] = UNMASK_8(datah >> 8);
    array[0x8] = UNMASK_8(datah);
    array[0x7] = UNMASK_8(datal >> 56);
    array[0x6] = UNMASK_8(datal >> 48);
    array[0x5] = UNMASK_8(datal >> 40);
    array[0x4] = UNMASK_8(datal >> 32);
    array[0x3] = UNMASK_8(datal >> 24);
    array[0x2] = UNMASK_8(datal >> 16);
    array[0x1] = UNMASK_8(datal >> 8);
    array[0x0] = UNMASK_8(datal);
}

void WriteMetadataWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t datah,
    uint64_t datal
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In WriteMetadataWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    array[0xF] = (UNMASK_2(METADATA_WORD) << 6) | (UNMASK_6(datah >> 56));
    array[0xE] = UNMASK_8(datah >> 48);
    array[0xD] = UNMASK_8(datah >> 40);
    array[0xC] = UNMASK_8(datah >> 32);
    array[0xB] = UNMASK_8(datah >> 24);
    array[0xA] = UNMASK_8(datah >> 16);
    array[0x9] = UNMASK_8(datah >> 8);
    array[0x8] = UNMASK_8(datah);
    array[0x7] = UNMASK_8(datal >> 56);
    array[0x6] = UNMASK_8(datal >> 48);
    array[0x5] = UNMASK_8(datal >> 40);
    array[0x4] = UNMASK_8(datal >> 32);
    array[0x3] = UNMASK_8(datal >> 24);
    array[0x2] = UNMASK_8(datal >> 16);
    array[0x1] = UNMASK_8(datal >> 8);
    array[0x0] = UNMASK_8(datal);
}

void WritePinStateWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t stype,
    uint64_t datah,
    uint64_t datal,
    uint64_t undefined,
    uint64_t unused
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In WritePinStateWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    array[0xF] = (UNMASK_2(PINSTATE_WORD) << 6) | (UNMASK_6(undefined >> 6));
    array[0xE] = (UNMASK_6(undefined) << 2) | (UNMASK_2(stype));
    if (stype == IOSTATEJAM) {
        array[0xD] = UNMASK_8(datah >> 40);
        array[0xC] = UNMASK_8(datah >> 32);
        array[0xB] = UNMASK_8(datah >> 24);
        array[0xA] = UNMASK_8(datah >> 16);
        array[0x9] = UNMASK_8(datah >> 8);
        array[0x8] = UNMASK_8(datah);
        array[0x7] = UNMASK_8(datal >> 56);
        array[0x6] = UNMASK_8(datal >> 48);
        array[0x5] = UNMASK_8(datal >> 40);
        array[0x4] = UNMASK_8(datal >> 32);
        array[0x3] = UNMASK_8(datal >> 24);
        array[0x2] = UNMASK_8(datal >> 16);
        array[0x1] = UNMASK_8(datal >> 8);
        array[0x0] = UNMASK_8(datal);
    } else if (stype == MASK || stype == UNMASK || stype == MASK_APPLY) {
        array[0xD] = UNMASK_8(unused >> 40);
        array[0xC] = UNMASK_8(unused >> 32);
        array[0xB] = UNMASK_8(unused >> 24);
        array[0xA] = UNMASK_8(unused >> 16);
        array[0x9] = UNMASK_8(unused >> 8);
        array[0x8] = UNMASK_8(unused);
        array[0x7] = UNMASK_8(datal >> 56);
        array[0x6] = UNMASK_8(datal >> 48);
        array[0x5] = UNMASK_8(datal >> 40);
        array[0x4] = UNMASK_8(datal >> 32);
        array[0x3] = UNMASK_8(datal >> 24);
        array[0x2] = UNMASK_8(datal >> 16);
        array[0x1] = UNMASK_8(datal >> 8);
        array[0x0] = UNMASK_8(datal);
    }
}

void WriteInstructionWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t optype,
    uint64_t extop,
    uint64_t action,
    uint64_t opdest,
    uint64_t opsrc,
    uint64_t aluop,
    uint64_t vptype,
    uint64_t vpop,
    uint64_t invcond,
    uint64_t cond,
    uint64_t base,
    uint64_t br,
    uint64_t dest,
    uint64_t regB,
    uint64_t regA,
    uint64_t imm,
    uint64_t swrsvd,
    uint64_t rsvd
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In WriteInstructionWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    array[0xF] = (UNMASK_2(INSTRUCTION_WORD) << 6) | UNMASK_6(swrsvd >> 10);
    array[0xE] = UNMASK_8(swrsvd >> 2);
    array[0xD] = UNMASK_2(swrsvd) | UNMASK_6(rsvd >> 34);
    array[0xC] = UNMASK_8(rsvd >> 26);
    array[0xB] = UNMASK_8(rsvd >> 18);
    array[0xA] = UNMASK_8(rsvd >> 10);
    array[0x9] = UNMASK_8(rsvd >> 2);
    array[0x8] = (UNMASK_2(rsvd) << 6) | (UNMASK_4(optype) << 2);
    if (optype == EXT) {
        array[0x8] |= UNMASK_2(extop >> 1);
        array[0x7] = (UNMASK_1(extop) << 7) | (UNMASK_4(action) << 3);
    } else if (optype == ALU) {
        array[0x8] |= UNMASK_2(opdest);
        array[0x7] = (UNMASK_2(opsrc) << 6) | (UNMASK_3(aluop) << 3);
    } else if (optype == REGISTER) {
        array[0x8] |= UNMASK_2(opdest >> 2);
        array[0x7] = (UNMASK_2(opdest) << 6) | (UNMASK_3(opsrc) << 3);
    } else if (optype == VECTOR) {
        array[0x8] |= UNMASK_2(vptype >> 1);
        array[0x7] = (UNMASK_1(vptype) << 7) | (UNMASK_4(vpop) << 3);
    } else if (optype == BRANCH) {
        // TODO
    }
    array[0x7] |= (UNMASK_1(invcond) << 2) | (UNMASK_2(cond >> 2));
    array[0x6] = (UNMASK_2(cond) << 6) | (UNMASK_2(base) << 4) | (UNMASK_4(br >> 1));
    array[0x5] = (UNMASK_1(br) << 7) | (UNMASK_5(dest) << 2) | (UNMASK_2(regB >> 3));
    array[0x4] = (UNMASK_3(regB) << 5) | (UNMASK_5(regA));
    array[0x3] = UNMASK_8(imm >> 24);
    array[0x2] = UNMASK_8(imm >> 16);
    array[0x1] = UNMASK_8(imm >> 8);
    array[0x0] = UNMASK_8(imm);
}

void WriteRawPatternWord
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t datah,
    uint64_t datal

) {
    if (offset + 16 > length) {
        throw std::out_of_range("In WriteRawPatternWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    array[0xF] = UNMASK_8(datah >> 56);
    array[0xE] = UNMASK_8(datah >> 48);
    array[0xD] = UNMASK_8(datah >> 40);
    array[0xC] = UNMASK_8(datah >> 32);
    array[0xB] = UNMASK_8(datah >> 24);
    array[0xA] = UNMASK_8(datah >> 16);
    array[0x9] = UNMASK_8(datah >> 8);
    array[0x8] = UNMASK_8(datah);
    array[0x7] = UNMASK_8(datal >> 56);
    array[0x6] = UNMASK_8(datal >> 48);
    array[0x5] = UNMASK_8(datal >> 40);
    array[0x4] = UNMASK_8(datal >> 32);
    array[0x3] = UNMASK_8(datal >> 24);
    array[0x2] = UNMASK_8(datal >> 16);
    array[0x1] = UNMASK_8(datal >> 8);
    array[0x0] = UNMASK_8(datal);
}

uint8_t ReadPatternWordType
(
    const uint8_t* array,
    size_t length,
    size_t offset
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In ReadVectorWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    return SLICE(array[0xF], 7, 6);
}

void ReadVectorWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* link,
    uint64_t* ctv,
    uint64_t* mtv,
    uint64_t* lrpt,
    uint64_t* datah,
    uint64_t* datal,
    uint64_t* pvcrsvd
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In ReadVectorWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    if (SLICE(array[0xF], 7, 6) != VECTOR_WORD) {
        throw std::runtime_error("In ReadVectorWord(), pattern word is not a vector word");
    }
    *pvcrsvd = SLICE(array[0xF], 5, 2);
    *link    = BIT(array[0xF], 1);
    *ctv     = BIT(array[0xF], 0);
    *mtv     = SLICE(array[0xE], 7, 5);
    *lrpt    = SLICE(array[0xE], 4, 0);
    *datah   = (UNMASK_8(array[0xD]) << 40) | (UNMASK_8(array[0xC]) << 32) | (UNMASK_8(array[0xB]) << 24) | (UNMASK_8(array[0xA]) << 16) | (UNMASK_8(array[0x9]) << 8) | (UNMASK_8(array[0x8]));
    *datal   = (UNMASK_8(array[0x7]) << 56) | (UNMASK_8(array[0x6]) << 48) | (UNMASK_8(array[0x5]) << 40) | (UNMASK_8(array[0x4]) << 32) | (UNMASK_8(array[0x3]) << 24) | (UNMASK_8(array[0x2]) << 16) | (UNMASK_8(array[0x1]) << 8) | (UNMASK_8(array[0x0]));
}

void ReadMetadataWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* datah,
    uint64_t* datal
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In ReadMetadataWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    if (SLICE(array[0xF], 7, 6) != METADATA_WORD) {
        throw std::runtime_error("In ReadMetadataWord(), pattern word is not a metadata word");
    }
    *datah   = (UNMASK_6(array[0xF]) << 56) | (UNMASK_8(array[0xE]) << 48) | (UNMASK_8(array[0xD]) << 40) | (UNMASK_8(array[0xC]) << 32) | (UNMASK_8(array[0xB]) << 24) | (UNMASK_8(array[0xA]) << 16) | (UNMASK_8(array[0x9]) << 8) | (UNMASK_8(array[0x8]));
    *datal   = (UNMASK_8(array[0x7]) << 56) | (UNMASK_8(array[0x6]) << 48) | (UNMASK_8(array[0x5]) << 40) | (UNMASK_8(array[0x4]) << 32) | (UNMASK_8(array[0x3]) << 24) | (UNMASK_8(array[0x2]) << 16) | (UNMASK_8(array[0x1]) << 8) | (UNMASK_8(array[0x0]));
}

void ReadPinStateWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* stype,
    uint64_t* datah,
    uint64_t* datal,
    uint64_t* undefined,
    uint64_t* unused
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In ReadPinStateWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    if (SLICE(array[0xF], 7, 6) != PINSTATE_WORD) {
        throw std::runtime_error("In ReadPinStateWord(), pattern word is not a pin state word");
    }
    *undefined  = (SLICE(array[0xF], 5, 0) << 6) | SLICE(array[0xE], 7, 2);
    *stype      = SLICE(array[0xE], 1, 0);
    if (*stype == IOSTATEJAM) {
        *datah  = (UNMASK_8(array[0xD]) << 40) | (UNMASK_8(array[0xC]) << 32) | (UNMASK_8(array[0xB]) << 24) | (UNMASK_8(array[0xA]) << 16) | (UNMASK_8(array[0x9]) << 8) | (UNMASK_8(array[0x8]));
        *datal  = (UNMASK_8(array[0x7]) << 56) | (UNMASK_8(array[0x6]) << 48) | (UNMASK_8(array[0x5]) << 40) | (UNMASK_8(array[0x4]) << 32) | (UNMASK_8(array[0x3]) << 24) | (UNMASK_8(array[0x2]) << 16) | (UNMASK_8(array[0x1]) << 8) | (UNMASK_8(array[0x0]));
    } else if (*stype == MASK || *stype == UNMASK || *stype == MASK_APPLY) {
        *unused = (UNMASK_8(array[0xD]) << 40) | (UNMASK_8(array[0xC]) << 32) | (UNMASK_8(array[0xB]) << 24) | (UNMASK_8(array[0xA]) << 16) | (UNMASK_8(array[0x9]) << 8) | (UNMASK_8(array[0x8]));
        *datal  = (UNMASK_8(array[0x7]) << 56) | (UNMASK_8(array[0x6]) << 48) | (UNMASK_8(array[0x5]) << 40) | (UNMASK_8(array[0x4]) << 32) | (UNMASK_8(array[0x3]) << 24) | (UNMASK_8(array[0x2]) << 16) | (UNMASK_8(array[0x1]) << 8) | (UNMASK_8(array[0x0]));
    }
}

void ReadInstructionWord
(
    const uint8_t* array,
    size_t length,
    size_t offset,
    uint64_t* optype,
    uint64_t* extop,
    uint64_t* action,
    uint64_t* opdest,
    uint64_t* opsrc,
    uint64_t* aluop,
    uint64_t* vptype,
    uint64_t* vpop,
    uint64_t* invcond,
    uint64_t* cond,
    uint64_t* base,
    uint64_t* br,
    uint64_t* dest,
    uint64_t* regB,
    uint64_t* regA,
    uint64_t* imm,
    uint64_t* swrsvd,
    uint64_t* rsvd
) {
    if (offset + 16 > length) {
        throw std::out_of_range("In ReadInstructionWord(), offset is out of range");
    }
    // Advance the pointer to the intended pattern word
    array += offset;

    if (SLICE(array[0xF], 7, 6) != INSTRUCTION_WORD) {
        throw std::runtime_error("In ReadInstructionWord(), pattern word is not an instruction word");
    }
    *swrsvd    = (SLICE(array[0xF], 5, 0) << 10) | (UNMASK_8(array[0xE]) << 2) | (SLICE(array[0xD], 7, 6));
    *rsvd      = (SLICE(array[0xD], 5, 0) << 34) | (UNMASK_8(array[0xC]) << 26) | (UNMASK_8(array[0xB]) << 18) | (UNMASK_8(array[0xA]) << 10) | (UNMASK_8(array[0x9]) << 2) | SLICE(array[0x8], 7, 6);
    *optype    = SLICE(array[0x8], 5, 2);
    if (*optype == EXT) {
        *extop  = (SLICE(array[0x8], 1, 0) << 1) | BIT(array[0x7], 7);
        *action = SLICE(array[0x7], 6, 3);
    } else if (*optype == ALU) {
        *opdest = SLICE(array[0x8], 1, 0);
        *opsrc  = SLICE(array[0x7], 7, 6);
        *aluop  = SLICE(array[0x7], 5, 3);
    } else if (*optype == REGISTER) {
        *opdest = (SLICE(array[0x8], 1, 0) << 2) | SLICE(array[0x7], 7, 6);
        *opsrc  = SLICE(array[0x7], 5, 3);
    } else if (*optype == VECTOR) {
        *vptype = (SLICE(array[0x8], 1, 0) << 1) | BIT(array[0x7], 7);
        *vpop   = SLICE(array[0x7], 6, 3);
    } else if (*optype == BRANCH) {
        // TODO
    }
    *invcond = BIT(array[0x7], 2);
    *cond    = (SLICE(array[0x7], 1, 0) << 2) | (SLICE(array[0x6], 7, 6));
    *base    = SLICE(array[0x6], 5, 4);
    *br      = (SLICE(array[0x6], 3, 0) << 1) | BIT(array[0x5], 7);
    *dest    = SLICE(array[0x5], 6, 2);
    *regB    = (SLICE(array[0x5], 1, 0) << 3) | (SLICE(array[0x4], 7, 5));
    *regA    = SLICE(array[0x4], 4, 0);
    *imm     = (UNMASK_8(array[0x3]) << 24) | (UNMASK_8(array[0x2]) << 16) | (UNMASK_8(array[0x1]) << 8) | (UNMASK_8(array[0x0]));
}

void ReadCaptureHeader(const uint8_t* array, size_t length, size_t offset, CaptureHeader* header)
{
    if (offset + 32 > length) {
        LOG("error") << "In ReadCaptureHeader(), offset is out of range, offset = " << offset << ", length = " << length;
        return;
    }
    // Advance the pointer to the intended location
    array += offset;

    // Captured Vectors Valid Flags  8    1 bit set for each valid vector contained in the capture. 
    // Pattern Instance ID           32   Pattern Instance number 
    // User Log Register 1           32   User assignable register.
    // User Log Register 2           32   User assignable register. 
    // Pattern Cycle Count           32   Counter that is reset by a PCALL instruction 
    // Cycle Count within Repeat     32   Auto increments for instruction or local repeats.  Reset by first repeat.
    // User Cycle Count              32   Increments once every tester cycle. Reset by dedicated instruction. 
    // Total Cycle Count             48   Increments once every tester cycle. 
    // Repeat Reset                  8    
    for (size_t i = 0; i < 8; i++) {
        header->validFlags[i] = BIT(array[0], i) == 0x1;
    }
    header->patternInstanceId      = (UNMASK_8(array[4]) << 24)  | (UNMASK_8(array[3]) << 16)  | (UNMASK_8(array[2]) << 8)   | UNMASK_8(array[1]);
    header->userLogRegister1       = (UNMASK_8(array[8]) << 24)  | (UNMASK_8(array[7]) << 16)  | (UNMASK_8(array[6]) << 8)   | UNMASK_8(array[5]);
    header->userLogRegister2       = (UNMASK_8(array[12]) << 24) | (UNMASK_8(array[11]) << 16) | (UNMASK_8(array[10]) << 8)  | UNMASK_8(array[9]);
    header->patternCycleCount      = (UNMASK_8(array[16]) << 24) | (UNMASK_8(array[15]) << 16) | (UNMASK_8(array[14]) << 8)  | UNMASK_8(array[13]);
    header->cycleCountWithinRepeat = (UNMASK_8(array[20]) << 24) | (UNMASK_8(array[19]) << 16) | (UNMASK_8(array[18]) << 8)  | UNMASK_8(array[17]);
    header->userCycleCount         = (UNMASK_8(array[24]) << 24) | (UNMASK_8(array[23]) << 16) | (UNMASK_8(array[22]) << 8)  | UNMASK_8(array[21]);
    header->totalCycleCount        = (UNMASK_8(array[30]) << 40) | (UNMASK_8(array[29]) << 32) | (UNMASK_8(array[28]) << 24) | (UNMASK_8(array[27]) << 16) | (UNMASK_8(array[26]) << 8) | UNMASK_8(array[25]);
    for (size_t i = 0; i < 8; i++) {
        header->repeatReset[i] = BIT(array[31], i) == 0x1;
    }
}

void ReadCaptureData(const uint8_t* array, size_t length, size_t offset, CaptureData* data)
{
    if (offset + 12 > length) {
        LOG("error") << "In ReadCaptureData(), offset is out of range, offset = " << offset << ", length = " << length;
        return;
    }
    // Advance the pointer to the intended location
    array += offset;

    data->vectorAddress = (UNMASK_8(array[0xB]) << 24)  | (UNMASK_8(array[0xA]) << 16)  | (UNMASK_8(array[0x9]) << 8)   | UNMASK_8(array[0x8]);
    data->failEvent     = BIT(array[0x7], 7) == 0x1;
    data->ctvEvent      = BIT(array[0x7], 6) == 0x1;
    data->reserved      = SLICE(array[0x7], 5, 4);
    data->ccwrReset     = BIT(array[0x7], 3) == 0x1;
    data->linkMode      = SLICE(array[0x7], 2, 0);
    data->pinLaneData   = (UNMASK_8(array[0x6]) << 48) | (UNMASK_8(array[0x5]) << 40) | (UNMASK_8(array[0x4]) << 32) | (UNMASK_8(array[0x3]) << 24) | (UNMASK_8(array[0x2]) << 16) | (UNMASK_8(array[0x1]) << 8) | UNMASK_8(array[0x0]);
}

void UnpackCapture(const uint8_t* array, size_t length, size_t blockCount, std::vector<CaptureBlock>* blocks)
{
    blocks->clear();
    size_t offset = 0;
    for (size_t i = 0; i < blockCount; i++) {
        CaptureBlock block;
        // Capture block = (256 bits Capture Header) + (96 bits Capture Data) * 8
        //               = (32 bytes Capture Header) + (12 bytes Capture Data) * 8
        //               = 128 bytes

        // NOTE: The FPGA puts the data before the header :|
        for (size_t j = 0; j < 8; j++) {
            ReadCaptureData(array, length, offset, &block.data[j]);
            offset += 12;  // 12 bytes of data
        }
        ReadCaptureHeader(array, length, offset, &block.header);
        offset += 32;  // 32 bytes of header
        blocks->push_back(block);
    }
}

}  // namespace hpcctbc

