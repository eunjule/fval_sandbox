////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: captureutils.h
//------------------------------------------------------------------------------
//    Purpose: 
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 11/23/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __CAPTUREUTILS_H__
#define __CAPTUREUTILS_H__

#include "hpcctbc.h"
#include "logging.h"

#include <cstdint>
#include <string>
#include <sstream>
#include <vector>

namespace hpcctbc {

class ActualCaptureState
{
public:
    std::vector<CaptureBlock> blocks;
    size_t blockIndex;
    int dataIndex;
    bool firstValidCapture;
    int firstValidDataIndex;
    size_t totalCaptureCount;
    CaptureHeader adjustedHeader;
    uint32_t currentCcwr;
    uint8_t previousValidLinkMode;
    int validEntryDistance;
    int ccwrResetDistance;
    uint32_t firstFailOffset; //by vectors, include valid and invalid ones
    bool firstFailOffsetFound;

    ActualCaptureState()
    {
        adjustedHeader = CaptureHeader();
        blockIndex = 0;
        dataIndex = 0;
        firstValidCapture = false;
        firstValidDataIndex = -1;
        totalCaptureCount = 0;
        currentCcwr = 0;
        previousValidLinkMode = 0; 
        validEntryDistance = 0;
        ccwrResetDistance = 99; // default to larger than 7. If not reset, use validEntryDistance
        firstFailOffset = UINT32_MAX;
        firstFailOffsetFound = false;
    };

    void Next()
    {
        dataIndex++;
        if (dataIndex >= 8) {
            blockIndex++;
            dataIndex = 0;
            firstValidCapture = false;
            firstValidDataIndex = -1;
        }
    }

    // if pass the correct linkMode, will use the new method, which only look at the previous invalid linkMode
    // if left linkMode = -1 as default, will use the old method, where every linkMode entry (valid or invalid) are checked
    bool GetValidCaptureData(CaptureHeader** header, CaptureData** data, int linkMode = -1, bool rawHeader = false)
    {
        while (blockIndex < blocks.size()) {
            // calculate ccwr using invalid vectors, doesn't depend on patternControl.linkMode
            if (linkMode == -1) {
                if (blocks[blockIndex].header.repeatReset[dataIndex]) {
                    currentCcwr = 0;
                } else {
                    if (blocks[blockIndex].data[dataIndex].linkMode == 0) {
                        currentCcwr++;
                    }    
                }
            }
                        
            validEntryDistance++;
            if (!firstFailOffsetFound) {
                if (firstFailOffset == UINT32_MAX) {
                    firstFailOffset = 0; // in case the first capture is fail
                } else {
                    firstFailOffset++; // count both valid and invalid vectors, capturing offset for DDR address
                }
            }
            if (blocks[blockIndex].header.repeatReset[dataIndex]) {
                ccwrResetDistance = 0;
            } else {
                ccwrResetDistance++;
            }
            if (blocks[blockIndex].header.validFlags[dataIndex]) {
                // We found a valid captured vector                
                if (firstValidDataIndex == -1) {
                    firstValidCapture = true;
                    firstValidDataIndex = dataIndex;
                } else {
                    firstValidCapture = false;
                }
                // found a fail vector
                if (blocks[blockIndex].data[dataIndex].failEvent) {
                    firstFailOffsetFound = true;
                }
                
                if (rawHeader) {
                    *header = &blocks[blockIndex].header;
                } else {
                    // Update the cycle counts in the header
                    adjustedHeader = blocks[blockIndex].header;
                    adjustedHeader.totalCycleCount += dataIndex;
                    adjustedHeader.patternCycleCount += dataIndex - firstValidDataIndex;
                    adjustedHeader.userCycleCount += dataIndex - firstValidDataIndex;
                    if (firstValidCapture) {
                        // no adjustment needed
                        currentCcwr = adjustedHeader.cycleCountWithinRepeat;
                    } else {
                        // calculate ccwr using invalid vectors, doesn't depend on patternControl.linkMode
                        if (linkMode == -1) {
                            adjustedHeader.cycleCountWithinRepeat = currentCcwr;
                        }
                        else { // calculate ccwr using linkMode calculation
                            if (blocks[blockIndex].header.repeatReset[dataIndex]) {
                                currentCcwr = 0;
                                ccwrResetDistance = 0;
                            } else {
                                if ((blocks[blockIndex].data[dataIndex].linkMode == 0) && (blocks[blockIndex].data[dataIndex-1].linkMode == 0)) {
                                    if (ccwrResetDistance <= validEntryDistance) {
                                        currentCcwr = ccwrResetDistance;
                                    } else {
                                        currentCcwr += validEntryDistance;
                                    }
                                } 
                                else { // must be a repeated linked vector
                                    if (ccwrResetDistance <= validEntryDistance) {
                                        currentCcwr = ccwrResetDistance / linkMode; // int/int, floor
                                    } else {
                                        if (previousValidLinkMode > blocks[blockIndex].data[dataIndex].linkMode) {
                                            currentCcwr++;
                                        }
                                        currentCcwr += validEntryDistance / linkMode; // int/int, floor
                                    }
                                }
                            }
                            adjustedHeader.cycleCountWithinRepeat = currentCcwr;
                        }
                    }
                    *header = &adjustedHeader;
                }
                previousValidLinkMode = blocks[blockIndex].data[dataIndex].linkMode;
                *data = &blocks[blockIndex].data[dataIndex];     
                validEntryDistance = 0;
                return true;
            } else {
                Next();
            }
        }
        return false;
    }
};

class FlatCapture
{
public:
    // Fields in capture header
    uint32_t patternInstanceId;
    uint32_t userLogRegister1;
    uint32_t userLogRegister2;
    uint32_t patternCycleCount;
    uint32_t cycleCountWithinRepeat;
    uint32_t userCycleCount;
    uint64_t totalCycleCount;
    // Fields in capture data
    uint32_t vectorAddress;
    bool     failEvent;
    bool     ctvEvent;
    uint8_t  linkMode;
    uint64_t pinLaneData;
    std::string str() const
    {
        std::stringstream ss;
        ss << "0x" << std::hex << patternInstanceId << ",";
        ss << "0x" << std::hex << userLogRegister1 << ",";
        ss << "0x" << std::hex << userLogRegister2 << ",";
        ss << "0x" << std::hex << patternCycleCount << ",";
        ss << "0x" << std::hex << cycleCountWithinRepeat << ",";
        ss << "0x" << std::hex << userCycleCount << ",";
        ss << "0x" << std::hex << totalCycleCount << ",";
        ss << "0x" << std::hex << vectorAddress << ",";
        ss << static_cast<int>(failEvent) << ",";
        ss << static_cast<int>(ctvEvent) << ",";
        ss << static_cast<int>(linkMode) << ",";
        ss << "0b" << Bin(pinLaneData) << ",";
        return ss.str();
    }
};

void FlattenCapture(const uint8_t* array, size_t length, size_t blockCount, std::vector<FlatCapture>* flatCapture);

class CaptureOffsetEntry
{
public:
    int channel;
    int offset;
    std::string str() const
    {
        std::stringstream ss;
        ss << channel << ",";
        ss << offset << ",";
        return ss.str();
    }
};

void CaptureOffsets(const uint8_t* array, size_t length, size_t blockCount, std::vector<CaptureOffsetEntry>* offsets);

}  // namespace hpcctbc

#endif  // __CAPTUREUTILS_H__

