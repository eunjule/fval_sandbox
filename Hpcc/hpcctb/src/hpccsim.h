////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpccsim.h
//------------------------------------------------------------------------------
//    Purpose: HPCC AC Fast Pattern Simulator
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/01/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __HPCCSIM_H__
#define __HPCCSIM_H__

#include <cstdint>
#include <vector>
#include <stack>
#include <queue>
#include <fstream>
#include <chrono>

#include "hpcctbc.h"
#include "captureutils.h"
#include "creditcounter.h"
#include "rcsim.h"

namespace hpcctbc {

struct PatternWord
{
    uint8_t b[PATTERN_WORD_SIZE];
};

enum ChannelMode
{
    DRIVE_MODE,
    COMPARE_MODE,
    ENABLED_CLOCK_MODE
};

enum SourceType
{
    VECTOR_SOURCE,
    IOSTATEJAM_SOURCE
};

enum SwitchType
{
    NO_SWITCH,
    DRIVE_TO_COMPARE_SWITCH,
    COMPARE_TO_DRIVE_SWITCH
};

enum CaptureType
{
    PIN_STATE,
    FAIL_STATUS,
    PIN_DIRECTION,
    MASKED_FAIL
};

class Channel
{
public:
    friend class HpccAcSimulator;  // HpccAcSimulator can access our private members
    ChannelMode mode;
    /// Is the driver on?
    bool drvEnable;
    /// Drive character in ['0', '1']
    char drv;
    /// Drive delay
    float tdrv;
    /// If checking, is 'Z' a logic '1'?
    bool zislogic1;
    /// Is the comparator on?
    bool cmpEnable;
    /// Compare character in ['0', '1']
    char cmp;
    /// Compare delay
    float tcmp;
    /// Current enabled clock character in ['0', '1', 'E', 'R']
    char clk;
    /// Enabled clock ratio
    uint8_t clkRatio;
    /// Enabled clock ratio counter in [0, clkRatio-1]
    uint8_t clkRatioCount;
    /// Enabled clock rise time
    float trise;
    /// Enabled clock fall time
    float tfall;
    SourceType source;
    SwitchType swtch;
    // Per channel fail count
    size_t failcnt;
    // Frequency counter value
    size_t freqCounter;
    int prevPinState;
};

/// FPGA Output/Pin Electronics/TIU/DUT BFM = Bus Functional Model (See http://en.wikipedia.org/wiki/Bus_Functional_Model)
class Bfm
{
public:
    /// Called by the test environment for every (slot, slice) in the system
    virtual void SetUp(int slot, int slice) = 0;
    /// Called by each simulator before executing a pattern
    virtual void Start(int slot, int slice) = 0;
    /// Called by each simulator group after executing a tester cycle
    virtual void AdvanceTo(float time) = 0;
    /// Called by the simulator to write logic values for BFM's input channels
    virtual void Drive(int slot, int slice, size_t ch, float time, char logic) = 0;
    /// Called by the simulator to override BFM's logic values in a window of time (used for enabled clock)
    virtual void OverrideDrive(int slot, int slice, size_t ch, float startTime, float stopTime, char logic) = 0;
    /// Called by the simulator to read logic values for BFM's output channels
    virtual char Sample(int slot, int slice, size_t ch, float time) = 0;
    /// Called by the test environment to read the end state of a pattern
    virtual char SampleEndState(int slot, int slice, size_t ch, float delta) = 0;
    virtual ~Bfm() { };
};

class InternalVector
{
public:
    enum State {
        EXECUTE_VECTOR,
        EXECUTE_IO_STATE_JAM
    };
    State state;
    uint32_t address;
    bool instructionRepeat;
    uint32_t instructionRepeatCount;
    uint32_t instructionRepeatTotal;
    bool channelLinking;
    uint32_t channelLinkingCount;
    uint32_t channelLinkingTotal;
    bool localRepeat;
    uint32_t localRepeatCount;
    uint32_t localRepeatTotal;
    bool ctv;
    int mtv;
    uint64_t datah;
    uint64_t datal;
};

class SimulatedCapture : public FlatCapture
{
public:
    // Simulator/checker fields and methods
    bool     afterAbort; // due to momentum
    bool     validCycleCountWithinRepeat;
    CaptureType type;
    uint64_t pinLaneCheckMask;

    uint64_t pinEnabledClock;
    char     pinEnabledClockChar[HPCC_CHANNELS];
    uint64_t pinVectorDirection;
    uint64_t pinVectorDirectionInCapture;
    uint64_t pinVectorEnable;
    uint64_t pinVectorValue;
    uint64_t pinState;
    uint64_t pinFailures;
    uint64_t pinMaskedFailures;
    static std::string Group(std::string data, size_t groupSize = 0, std::string separator = " ");
    static std::string PinLaneDataToStr(uint64_t data, uint64_t checkMask = 0x0);
    std::string TypeAsStr() const;
    std::string PinLaneDataAsStr() const;
    std::string PinStateAsStr() const;
    std::string VectorAsStr() const;
};

class TesterCycleContext
{
public:
    uint32_t address;
    bool ctv;
    int linkCount;
    uint32_t ccwr;
    bool     validccwr;
    uint64_t pinLaneData;
    uint64_t pinLaneCheckMask;
    CaptureType type;
    uint64_t pinEnabledClock;
    uint64_t pinVectorDirection;
    uint64_t pinVectorDirectionInCapture;
    uint64_t pinVectorEnable;
    uint64_t pinVectorValue;
    uint64_t pinState;
    uint64_t pinFailures;
    uint64_t pinMaskedFailures;
    uint64_t pinZ;
    float time;
};


class HpccAcSimulator : public rctbc::InstrumentSimulator
{
public:
    HpccAcSimulator(int slot, int slice, size_t memorySize);
    virtual ~HpccAcSimulator();

    virtual std::string Name() const;

    void ResetChannels();
    void ResetCountersAndMiscFlags();

    virtual void BarRead(uint32_t bar, uint32_t offset, uint32_t data);
    virtual void BarWrite(uint32_t bar, uint32_t offset, uint32_t data);
    // address and length are in bytes
    virtual void DmaRead(uint64_t address, const uint8_t* data, uint64_t length);
    // address and length are in bytes
    virtual void DmaWrite(uint64_t address, const uint8_t* data, uint64_t length);

    virtual void DumpMemory(std::string filename, uint64_t vectorAddress, uint64_t vectorLength) const;

    virtual void SetupTrace(uint64_t startCycle = 0x0, uint64_t stopCycle = 0xFFFFFFFFFFFFFFFF);
    virtual void EnableIOStateJamTrace(std::string filename);
    virtual void EnableInstructionTrace(std::string filename);
    virtual void EnableCaptureTrace(std::string filename);

    virtual void SetBfm(Bfm* bfm);

    virtual void SetPeriod(float period);
    virtual void SetDriveDelay(const std::vector<size_t>& channelList, float delay);
    virtual void SetCompareDelay(const std::vector<size_t>& channelList, float delay);
    virtual void SetEnClkRatio(const std::vector<size_t>& channelList, uint8_t ratio);
    virtual void SetEnClkTrise(const std::vector<size_t>& channelList, float trise);
    virtual void SetEnClkTfall(const std::vector<size_t>& channelList, float tfall);

    /// Start the simulation
    virtual void Start();
    /// Stop the simulation after the given number of vectors
    virtual void Stop(uint32_t status, size_t vectorCount = 0, bool abort = false);
    /// Execute one pattern word (vector or instruction). This function returns false after the pattern ends (i.e. stops or aborts).
    /// To prevent data loss, the caller must step through the tester cycles before calling StepVectorProcessor() again.
    virtual bool StepVectorProcessor();
    /// Execute one internal vector (= one tester cycle). This function returns false after all the internal vectors have been processed.
    /// To create more internal vectors, call StepVectorProcessor().
    virtual bool StepInternalVectorProcessor(TesterCycleContext* context);
    /// Execute one tester cycle. This function returns false after the pattern ends.
    /// This function calls StepVectorProcessor() and StepInternalVectorProcessor() when required.
    virtual bool StepTesterCycle(TesterCycleContext* context);

    /// Called by StepVectorProcessor()
    virtual void _PushVector(uint64_t link, uint64_t ctv, uint64_t mtv, uint64_t lrpt, uint64_t datah, uint64_t datal);
    /// Called by StepVectorProcessor()
    virtual void _PushIOStateJam(uint64_t datah, uint64_t datal);
    /// Called by _ExecuteVector() and _ExecuteIOStateJam()
    virtual void _UpdateChannelEnabledClock(size_t ch, uint8_t c);
    /// Called by StepInternalVectorProcessor()
    virtual void _ExecuteVector(int mtv, bool link, int linkCount, int numberOfLinks, uint64_t datah, uint64_t datal);
    /// Called by StepInternalVectorProcessor()
    virtual void _ExecuteIOStateJam(uint64_t datah, uint64_t datal);

    virtual void ExecuteTesterCycleDrivePhase(TesterCycleContext* context);
    virtual void ExecuteTesterCycleComparePhase(TesterCycleContext* context);
    virtual bool ExecuteTesterCycleCapturePhase(TesterCycleContext* context, SimulatedCapture* capture);

    virtual float Time() const
    {
        return tcc * m_period;
    }

    virtual void StageJump(uint32_t address);

    virtual void SetAluFlags(bool zero, bool carry, bool overflow, bool sign);
    void ResetFlags();
    /// Only sets the bits that we can assign to
    virtual void AssignValueToFlags(uint32_t value);

    void ResetAlarms();

    virtual Channel& GetChannel(size_t ch) { return channels[ch]; }

    /// Debug functions
    virtual std::string PinState() const;
    virtual std::string PinDirection() const;
    virtual void ReadPattern(uint64_t address, uint8_t* patternWord) const;

    virtual void ProcessTrigger(uint32_t trigger);

    /// Slice of this simulator (slot is already part of base class)
    int slice;
    /// Main menory size
    size_t memorySize;
    /// Main memory
    PatternWord* memory;

    /// Tester cycle period in seconds
    float m_period;

    /// (Pin Electronics, TIU, DUT) BFM that this simulator is connected to
    Bfm* m_bfm;

    int m_domain;
    bool domainMaster;

    // Pcie Registers
    uint32_t patternControl;
    uint32_t captureControl;
    uint32_t triggerControl;
    uint32_t patternStartAddress;
    uint32_t maxFailCaptureCount;
    uint32_t maxCaptureCount;
    uint32_t maxFailCountPerPattern;
    uint64_t testClassFailMask;
    uint64_t selectableFailMask[8];
    uint32_t perChannelControlConfig1[HPCC_CHANNELS];
    uint32_t perChannelControlConfig2[HPCC_CHANNELS];
    uint32_t perChannelControlConfig3[HPCC_CHANNELS];
    uint32_t perChannelControlConfig4[HPCC_CHANNELS];
    // Alarms
    bool stopOnFailAlarm;
    bool stackOverflowAlarm;  // FIXME: Currently the stack of this simulator in infinite
    bool stackUnderflowAlarm;

    // Internal registers
    uint32_t vecaddr;  // Instruction pointer
    std::vector<uint32_t> reg;
    uint32_t cfb;
    uint32_t pfb;
    uint32_t flags;
    std::stack<uint32_t> stack;
    std::stack<uint64_t> maskStack;
    // Log registers
    uint32_t log1;
    uint32_t log2;
    uint32_t patid;
    // Cycle count registers
    uint64_t tcc;
    uint32_t pcc;

    uint32_t ucc;
    // Repeat registers
    bool repeatFlag;
    uint32_t repeat;
    // Fail branch registers
    bool failbrFlag;
    uint32_t failbr;
    // Fail counters
    uint32_t failcnt;
    uint32_t patfcnt;
    uint32_t captureCount;
    bool exactFailCounts;
    bool exactCaptureCounts;
    // End status code
    uint32_t endstatus;

    // Internal per vector control
    Channel channels[HPCC_CHANNELS];
    uint64_t stagedPatmask;  // Staged Pattern-level mask
    uint64_t patmask;        // Active Pattern-level mask
    uint64_t vecmask;        // Active Vector-level mask
    uint64_t mask;           // Active pattern mask

    bool jumpFlag;
    uint32_t jumpTargetAddress;
    
    bool stop;
    bool m_abort;  // is this stop an abort?
    size_t stopCount;
    size_t stopAddressEstimate;
    bool isStopped;

    bool externalAbort;
    uint32_t externalAbortAddress;

    std::string endState;
    std::string endStateLogic;
    uint64_t endPinDirection;
    uint64_t endPinEnable;

    std::chrono::time_point<std::chrono::high_resolution_clock> startTime;

    std::queue<InternalVector> vectors;

    /// Do we want to check z characters? (default = false)
    bool checkz;

    bool edgeCounter;
    bool blockFails;

    // Common trace fields
    uint64_t traceStartCycle;
    uint64_t traceStopCycle;

    // IO State Jam trace fields
    bool ioStateJamTrace;
#ifndef SWIG
    std::ofstream ioStateJamTraceFout;
#endif

    // Instruction trace fields
    bool instructionTrace;
#ifndef SWIG
    std::ofstream instructionTraceFout;
#endif
    // Capture trace fields
    bool captureTrace;
#ifndef SWIG
    std::ofstream captureTraceFout;
#endif

	CreditCounter creditCounter;
};

}  // namespace hpcctbc

#endif  // __HPCCSIM_H__

