////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: creditcounter.h
//------------------------------------------------------------------------------
//    Purpose: Credit counter model
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 01/30/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __CREDITCOUNTER_H__
#define __CREDITCOUNTER_H__

#include <cmath>

namespace hpcctbc {

class CreditCounter
{
public:
    CreditCounter()
    {
        Reset();
        prestageSize = 1 * 1024;
        highClamp = 16 * 1024;
        threshold = 0;
        minqdr = 1.18e-9f;
        subcyle = 0;
    }

    inline void SetPeriod(float period)
    {
        subcyle = static_cast<long>(std::log(period / minqdr) / std::log(2));
    }

    inline void Reset()
    {
        prestageDone = false;
        balance = 0;
        willFail = false;
    }

    inline void Vector(uint32_t vecaddr, long lrpt, long channelLinkingWidth)
    {
        _Update(vecaddr, (lrpt + 1) * channelLinkingWidth);
    }

    inline void Metadata(uint32_t vecaddr)
    {
        _Update(vecaddr, -1);
    }

    inline void Instruction(uint32_t vecaddr)
    {
        _Update(vecaddr, -1);
    }

    inline void Branch(uint32_t vecaddr)
    {
        long cost[8] = {-900, -200, -50, -28, -28, -28, -28, -28}; // last 4 buckets are duplicated, not from data collection
        _Update(vecaddr, cost[subcyle]);
    }

    inline void _Update(uint32_t vecaddr, long delta)
    {
        if (!prestageDone) {
            // During prestage, we only add up positive deltas
            if (delta > 0) {
                balance = std::min(balance + delta, highClamp);
                if (balance >= prestageSize) {
                    prestageDone = true;
                    prestageDoneAddr = vecaddr;
                }
            }
        } else {
            // After prestage, all the standard rules apply
            balance = std::min(balance + delta, highClamp);
            // Capture underrun vector address the first time it happens
            if ((balance < threshold) && !willFail) {
                willFail = true;
                failVecAddr = vecaddr;
            }
        }
    }

    long prestageSize;
    bool prestageDone;
    long prestageDoneAddr;
    long highClamp;
    long threshold;

    float minqdr;
    long subcyle;
    long balance;
    bool willFail;
    uint32_t failVecAddr;
};

}  // namespace hpcctbc

#endif  // __CREDITCOUNTER_H__

