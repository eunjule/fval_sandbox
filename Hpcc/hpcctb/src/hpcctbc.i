////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpcctbc.i
//------------------------------------------------------------------------------
//    Purpose: HPCC High-Performance Testbench Methods and Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/30/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

%module(directors="1") hpcctbc

%include <stdint.i>
%include <exception.i>
%include <std_string.i>
%include <std_vector.i>
%include <std_set.i>

%feature("director");
%feature("nodirector") hpcctbc::HpccAcSimulator;

#if SWIG_VERSION < 0x020008
#error Requires SWIG 2.0.8 or later.
#endif

%begin %{
#ifdef _MSC_VER
    #include <codeanalysis\warnings.h>
    #pragma warning (disable:ALL_CODE_ANALYSIS_WARNINGS)
    #pragma warning(disable:4100) // unreferenced formal parameter
    #pragma warning(disable:4127) // conditional expression is constant
    #pragma warning(disable:4211) // nonstandard extension used : redefined extern to static
    #pragma warning(disable:4706) // assignment within conditional expression
    #pragma warning(disable:4996) // This function or variable may be unsafe
    #pragma warning(disable:4701) // Potentially uninitialized local variable used
    #pragma warning(disable:4101) // 'swig_obj': unreferenced local variable
    #pragma warning(disable:4244) //  warning C4244: 'argument': conversion from 'Py_ssize_t' to 'int',
    #pragma warning(disable:4459) //warning C4459: declaration of 'swig_this' hides global declaration
    #pragma warning(disable:4703) //warning C4703: potentially uninitialized local pointer variable 'p' used

#else
    #pragma GCC diagnostic ignored "-Wunused-variable"
    #pragma GCC diagnostic ignored "-Wunused-value"
#endif
%}

%{
// Includes these headers in the wrapper code
#include "defines.h"
#include "randomvar.h"
#include "logging.h"

#include "hpcctbc.h"
#include "captureutils.h"
#include "creditcounter.h"
#include "generators.h"
#include "../../../Rc/rctb/src/rcsim.h"
#include "hpccsim.h"
#include "waveform.h"
#include "hpccbfms.h"
#include "capturechecker.h"
%}

namespace hpcctbc {

%catches(std::exception) HpccAcSimulator;
%catches(std::exception) CaptureChecker;

%exception {
    try {
        $action
    } catch (const std::exception& e) {
        SWIG_exception(SWIG_RuntimeError, e.what());
    } catch (const std::string& e) {
        SWIG_exception(SWIG_RuntimeError, e.c_str());
    } catch (...) {
        SWIG_exception(SWIG_UnknownError, "C++ anonymous exception");
    }
}

%typemap(in) (const uint8_t* array, size_t length) {
    if (PyByteArray_Check($input)) {
        $1 = (uint8_t*) PyByteArray_AsString($input);
        $2 = (uint64_t) PyByteArray_Size($input);
    } else if (PyBytes_Check($input)) {
        $1 = (uint8_t*) PyBytes_AsString($input);
        $2 = (uint64_t) PyBytes_Size($input);
    } else {
        SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
}

%typemap(in) (const uint8_t* data, uint64_t length) {
    if (PyByteArray_Check($input)) {
        $1 = (uint8_t*) PyByteArray_AsString($input);
        $2 = (uint64_t) PyByteArray_Size($input);
    } else if (PyBytes_Check($input)) {
        $1 = (uint8_t*) PyBytes_AsString($input);
        $2 = (uint64_t) PyBytes_Size($input);
    } else {
        SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
}

%typemap(in) (uint8_t* array, size_t length) {
    if (PyByteArray_Check($input)) {
        $1 = (uint8_t*) PyByteArray_AsString($input);
        $2 = (uint64_t) PyByteArray_Size($input);
    } else if (PyBytes_Check($input)) {
        $1 = (uint8_t*) PyBytes_AsString($input);
        $2 = (uint64_t) PyBytes_Size($input);
    } else {
        SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
}

%typemap(in) (uint8_t* data, uint64_t length) {
    if (PyByteArray_Check($input)) {
        $1 = (uint8_t*) PyByteArray_AsString($input);
        $2 = (uint64_t) PyByteArray_Size($input);
    } else if (PyBytes_Check($input)) {
        $1 = (uint8_t*) PyBytes_AsString($input);
        $2 = (uint64_t) PyBytes_Size($input);
    } else {
        SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
}

%typemap(in) (uint64_t datah, uint64_t datal) {
    if (!PyLong_Check($input)) {
        SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
    // Right-shift input 64 times to get the high bits
    PyObject* shift = PyInt_FromLong(64);
    PyObject* high  = PyNumber_Rshift($input, shift);
    $1 = (uint64_t) PyLong_AsUnsignedLongLong(high);
    // And $input with 64-bit mask
    PyObject* mask  = PyLong_FromUnsignedLongLong(0xFFFFFFFFFFFFFFFF);
    PyObject* low   = PyNumber_And($input, mask);
    $2 = (uint64_t) PyLong_AsUnsignedLongLong(low);
    // Cleanup
    Py_DECREF(mask);
    Py_DECREF(low);
    Py_DECREF(shift);
    Py_DECREF(high);
}

%typemap(in, numinputs = 0) (uint64_t* link, uint64_t* ctv, uint64_t* mtv, uint64_t* lrpt, uint64_t* datah, uint64_t* datal, uint64_t* pvcrsvd) (uint64_t temp1, uint64_t temp2, uint64_t temp3, uint64_t temp4, uint64_t temp5, uint64_t temp6, uint64_t temp7) {
   $1 = &temp1;
   $2 = &temp2;
   $3 = &temp3;
   $4 = &temp4;
   $5 = &temp5;
   $6 = &temp6;
   $7 = &temp7;
}

%typemap(argout) (uint64_t* link, uint64_t* ctv, uint64_t* mtv, uint64_t* lrpt, uint64_t* datah, uint64_t* datal, uint64_t* pvcrsvd) {
    PyObject* _link        = PyLong_FromUnsignedLongLong(*$1);
    PyObject* _ctv         = PyLong_FromUnsignedLongLong(*$2);
    PyObject* _mtv         = PyLong_FromUnsignedLongLong(*$3);
    PyObject* _lrpt        = PyLong_FromUnsignedLongLong(*$4);
    PyObject* _datah       = PyLong_FromUnsignedLongLong(*$5);
    PyObject* _datal       = PyLong_FromUnsignedLongLong(*$6);
    PyObject* _pvcrsvd     = PyLong_FromUnsignedLongLong(*$7);
    PyObject* shift        = PyInt_FromLong(64);
    PyObject* intermediate = PyNumber_Lshift(_datah, shift);
    // data = (_datah << 64) + _datal
    PyObject* data         = PyNumber_Add(_datal, intermediate);
    PyObject* result       = PyTuple_New(6);
    PyTuple_SetItem(result, 0, _link);
    PyTuple_SetItem(result, 1, _ctv);
    PyTuple_SetItem(result, 2, _mtv);
    PyTuple_SetItem(result, 3, _lrpt);
    PyTuple_SetItem(result, 4, data);
    PyTuple_SetItem(result, 5, _pvcrsvd);
    // Cleanup
    Py_DECREF(_datah);
    Py_DECREF(_datal);
    Py_DECREF(shift);
    Py_DECREF(intermediate);
    $result = result;
}

%typemap(in, numinputs = 0) (uint64_t* datah, uint64_t* datal) (uint64_t temp1, uint64_t temp2) {
   $1 = &temp1;
   $2 = &temp2;
}

%typemap(argout) (uint64_t* datah, uint64_t* datal) {
    PyObject* _datah       = PyLong_FromUnsignedLongLong(*$1);
    PyObject* _datal       = PyLong_FromUnsignedLongLong(*$2);
    PyObject* shift        = PyInt_FromLong(64);
    PyObject* intermediate = PyNumber_Lshift(_datah, shift);
    // data = (_datah << 64) + _datal
    PyObject* data         = PyNumber_Add(_datal, intermediate);
    // Cleanup
    Py_DECREF(_datah);
    Py_DECREF(_datal);
    Py_DECREF(shift);
    Py_DECREF(intermediate);
    $result = data;
}


%typemap(in, numinputs = 0) (uint64_t* stype, uint64_t* datah, uint64_t* datal, uint64_t* undefined, uint64_t* unused) (uint64_t temp1, uint64_t temp2, uint64_t temp3, uint64_t temp4, uint64_t temp5) {
   $1 = &temp1;
   $2 = &temp2;
   $3 = &temp3;
   $4 = &temp4;
   $5 = &temp5;
}

%typemap(argout) (uint64_t* stype, uint64_t* datah, uint64_t* datal, uint64_t* undefined, uint64_t* unused) {
    PyObject* _stype       = PyLong_FromUnsignedLongLong(*$1);
    PyObject* _datah       = PyLong_FromUnsignedLongLong(*$2);
    PyObject* _datal       = PyLong_FromUnsignedLongLong(*$3);
    PyObject* _undefined   = PyLong_FromUnsignedLongLong(*$4);
    PyObject* _unused      = PyLong_FromUnsignedLongLong(*$5);
    PyObject* shift        = PyInt_FromLong(64);
    PyObject* intermediate = PyNumber_Lshift(_datah, shift);
    // data = (_datah << 64) + _datal
    PyObject* data         = PyNumber_Add(_datal, intermediate);
    PyObject* result       = PyTuple_New(4);
    PyTuple_SetItem(result, 0, _stype);
    PyTuple_SetItem(result, 1, data);
    PyTuple_SetItem(result, 2, _undefined);
    PyTuple_SetItem(result, 3, _unused);
    // Cleanup
    Py_DECREF(_datah);
    Py_DECREF(_datal);
    Py_DECREF(shift);
    Py_DECREF(intermediate);
    $result = result;
}

%typemap(in, numinputs = 0) (uint64_t* optype, uint64_t* extop, uint64_t* action, uint64_t* opdest, uint64_t* opsrc, uint64_t* aluop, uint64_t* vptype, uint64_t* vpop, uint64_t* invcond, uint64_t* cond, uint64_t* base, uint64_t* br, uint64_t* dest, uint64_t* regB, uint64_t* regA, uint64_t* imm, uint64_t* swrsvd, uint64_t* rsvd) (uint64_t temp1, uint64_t temp2, uint64_t temp3, uint64_t temp4, uint64_t temp5, uint64_t temp6, uint64_t temp7, uint64_t temp8, uint64_t temp9, uint64_t temp10, uint64_t temp11, uint64_t temp12, uint64_t temp13, uint64_t temp14, uint64_t temp15, uint64_t temp16, uint64_t temp17, uint64_t temp18) {
   $1 = &temp1;
   $2 = &temp2;
   $3 = &temp3;
   $4 = &temp4;
   $5 = &temp5;
   $6 = &temp6;
   $7 = &temp7;
   $8 = &temp8;
   $9 = &temp9;
   $10 = &temp10;
   $11 = &temp11;
   $12 = &temp12;
   $13 = &temp13;
   $14 = &temp14;
   $15 = &temp15;
   $16 = &temp16;
   $17 = &temp17;
   $18 = &temp18;
}

%typemap(argout) (uint64_t* optype, uint64_t* extop, uint64_t* action, uint64_t* opdest, uint64_t* opsrc, uint64_t* aluop, uint64_t* vptype, uint64_t* vpop, uint64_t* invcond, uint64_t* cond, uint64_t* base, uint64_t* br, uint64_t* dest, uint64_t* regB, uint64_t* regA, uint64_t* imm, uint64_t* swrsvd, uint64_t* rsvd) {
    PyObject* _optype  = PyLong_FromUnsignedLongLong(*$1);
    PyObject* _extop   = PyLong_FromUnsignedLongLong(*$2);
    PyObject* _action  = PyLong_FromUnsignedLongLong(*$3);
    PyObject* _opdest  = PyLong_FromUnsignedLongLong(*$4);
    PyObject* _opsrc   = PyLong_FromUnsignedLongLong(*$5);
    PyObject* _aluop   = PyLong_FromUnsignedLongLong(*$6);
    PyObject* _vptype  = PyLong_FromUnsignedLongLong(*$7);
    PyObject* _vpop    = PyLong_FromUnsignedLongLong(*$8);
    PyObject* _invcond = PyLong_FromUnsignedLongLong(*$9);
    PyObject* _cond    = PyLong_FromUnsignedLongLong(*$10);
    PyObject* _base    = PyLong_FromUnsignedLongLong(*$11);
    PyObject* _br      = PyLong_FromUnsignedLongLong(*$12);
    PyObject* _dest    = PyLong_FromUnsignedLongLong(*$13);
    PyObject* _regB    = PyLong_FromUnsignedLongLong(*$14);
    PyObject* _regA    = PyLong_FromUnsignedLongLong(*$15);
    PyObject* _imm     = PyLong_FromUnsignedLongLong(*$16);
    PyObject* _swrsvd  = PyLong_FromUnsignedLongLong(*$17);
    PyObject* _rsvd    = PyLong_FromUnsignedLongLong(*$18);
    PyObject* result   = PyTuple_New(18);
    PyTuple_SetItem(result, 0, _optype);
    PyTuple_SetItem(result, 1, _extop);
    PyTuple_SetItem(result, 2, _action);
    PyTuple_SetItem(result, 3, _opdest);
    PyTuple_SetItem(result, 4, _opsrc);
    PyTuple_SetItem(result, 5, _aluop);
    PyTuple_SetItem(result, 6, _vptype);
    PyTuple_SetItem(result, 7, _vpop);
    PyTuple_SetItem(result, 8, _invcond);
    PyTuple_SetItem(result, 9, _cond);
    PyTuple_SetItem(result, 10, _base);
    PyTuple_SetItem(result, 11, _br);
    PyTuple_SetItem(result, 12, _dest);
    PyTuple_SetItem(result, 13, _regB);
    PyTuple_SetItem(result, 14, _regA);
    PyTuple_SetItem(result, 15, _imm);
    PyTuple_SetItem(result, 16, _swrsvd);
    PyTuple_SetItem(result, 17, _rsvd);
    // Cleanup
    // Nothing needs to be done
    $result = result;
}

class PatternSimulator;

// FIXME: Workaround for swig being naughty
%typemap(in) (const std::vector<size_t>& channelList) (std::vector<size_t> temp1) {
    $1 = &temp1;
    if (PyList_Check($input)) {
        for (size_t i = 0; i < static_cast<size_t>(PyList_Size($input)); i++) {
            PyObject* item = PyList_GetItem($input, i);
            if (PyInt_Check(item)) {
                ($1)->push_back(PyInt_AsLong(item));
            } else {
                SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
            }
        }
    } else {
        SWIG_exception_fail(SWIG_TypeError, "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
}

%typemap(in, numinputs = 0) (uint8_t* patternWord) (uint8_t temp1[PATTERN_WORD_SIZE]) {
   $1 = temp1;
}

%typemap(argout) (uint8_t* patternWord) {
    $result = PyByteArray_FromStringAndSize(reinterpret_cast<const char*>($1), PATTERN_WORD_SIZE);
}

}  // namespace hpcctbc

// Parse the header files to generate wrappers
%include "defines.h"
%include "randomvar.h"
%include "logging.h"

%include "hpcctbc.h"
%include "captureutils.h"
%include "creditcounter.h"
%include "generators.h"
%include "../../../Rc/rctb/src/rcsim.h"
%include "hpccsim.h"
%include "waveform.h"
%include "hpccbfms.h"
%include "capturechecker.h"

// Instantiate templates used by hpcctbc
namespace std {
%template(string_vector) std::vector<std::string>;
%template(uint32_t_vector) std::vector<uint32_t>;
%template(size_t_vector) std::vector<size_t>;
%template(CaptureCheckerContext_ptr_vector) std::vector<hpcctbc::CaptureCheckerContext*>;
%template(FlatCapture_vector) std::vector<hpcctbc::FlatCapture>;
%template(CaptureOffsetEntry_vector) std::vector<hpcctbc::CaptureOffsetEntry>;
%template(uint32_t_set) std::set<uint32_t>;
}

namespace fvalc {
%template(HpccWaveform) Waveform<char, 2048>;
}

