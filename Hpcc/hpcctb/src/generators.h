////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: generators.h
//------------------------------------------------------------------------------
//    Purpose: HPCC Pattern Assembler Generators
//------------------------------------------------------------------------------
// Created by: Yuan Feng / Rodny Rodriguez
//       Date: 05/28/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __GENERATORS_H__
#define __GENERATORS_H__

#include <string>
#include <unordered_map>

#include "hpcctbc.h"
#include "randomvar.h"

using fvalc::RandomVar;

namespace hpcctbc {

class PatternAssemblerContext
{
public:
    virtual ~PatternAssemblerContext() {};
    size_t startaddr;
    size_t vecaddr;
    virtual void SetVar(std::string name, uint64_t value);
    std::unordered_map<std::string, uint64_t> vars;
};

class Generator
{
public:
    Generator(PatternAssemblerContext* context);
    virtual ~Generator() {};
    virtual size_t Size() const = 0;  // In vectors
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const = 0;  // Inputs are in bytes, result is in vectors
    PatternAssemblerContext* context;
};

class RandomVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") RandomVectors;
#endif
public:
    RandomVectors(PatternAssemblerContext* context, size_t length = 256, float ctvRate = 0, float mtvRate = 0, float linkRate = 0, float lrptRate = 0);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t m_length;
    float ctvRate;
    float mtvRate;
    float linkRate;
    float lrptRate;
};

class AddressVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") AddressVectors;
#endif
public:
    AddressVectors(PatternAssemblerContext* context, size_t length = 256);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t start;
    size_t m_length;
};

class RandomEvenToOddVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") RandomEvenToOddVectors;
#endif
public:
    RandomEvenToOddVectors(PatternAssemblerContext* context, size_t length = 256, float passingRate = 0, size_t simple = 0, size_t noZX = 1, float ctvRate = 0, float mtvRate = 0, float linkRate = 0, float lrptRate = 0);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t m_length;
    size_t simple;
	size_t noZX; // since Z ~ VOX by default, we cannot predict its value, should be disabled when Z <> VOX  
    float ctvRate; // [0,1]
    float mtvRate; // [0,1]
    float linkRate; // [0,1]
    float lrptRate; // [0,1]
    float passingRate; // [0,1]
};

class RandomPassingEvenToOddVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") RandomPassingEvenToOddVectors;
#endif
public:
    RandomPassingEvenToOddVectors(PatternAssemblerContext* context, size_t length = 256, size_t simple = 0, size_t noZX = 1);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t m_length;
    size_t simple;
	size_t noZX; // since Z ~ VOX by default, we cannot predict its value, should be disabled when Z <> VOX
};

class RandomPassingEvenToOddNoSwitchVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") RandomPassingEvenToOddNoSwitchVectors;
#endif
public:
    RandomPassingEvenToOddNoSwitchVectors(PatternAssemblerContext* context, size_t length = 256, RandomVar ctv = 0, RandomVar mtv = 0, RandomVar link = 0, RandomVar lrpt = 0);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t m_length;
    RandomVar ctv;
    RandomVar mtv;
    RandomVar link;
    RandomVar lrpt;
};

class Drive0CompareLVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") Drive0CompareLVectors;
#endif
public:
    Drive0CompareLVectors(PatternAssemblerContext* context, size_t length = 256);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t m_length;
};

class Drive1CompareHVectors : public Generator
{
#ifdef SWIG
    %feature("kwargs") Drive1CompareHVectors;
#endif
public:
    Drive1CompareHVectors(PatternAssemblerContext* context, size_t length = 256);
    virtual size_t Size() const;
    virtual size_t Generate(uint8_t* array, size_t length, size_t offset) const;
    size_t m_length;
};

}  // namespace hpcctbc

#endif  // __GENERATORS_H__

