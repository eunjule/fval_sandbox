////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: capturechecker.h
//------------------------------------------------------------------------------
//    Purpose: HPCC AC Capture Data Checker
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/01/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __CAPTURE_CHECKER_H__
#define __CAPTURE_CHECKER_H__

#include "hpccsim.h"

#include <cstdint>
#include <string>
#include <vector>
#include <set>

namespace hpcctbc {

class CaptureCheckerContext
{
public:
    CaptureCheckerContext(const uint8_t* array, size_t length, size_t blockCount, size_t captureCount, HpccAcSimulator* sim = nullptr, size_t ratio = 0);
    virtual ~CaptureCheckerContext();
    uint8_t* array;
    size_t length;
    size_t blockCount;
    size_t captureCount;
    HpccAcSimulator* sim;
    size_t ratio;
    uint32_t firstFailOffset; //by vectors, include valid and invalid ones
    bool firstFailOffsetFound;
};

class ExpectedCaptureState
{
public:
    size_t totalCaptureCount;
    TesterCycleContext testerCycleContext;
    SimulatedCapture capture;
    bool abortDetected;

    ExpectedCaptureState()
    {
        totalCaptureCount = 0;
        abortDetected = false;
    }
};

class CaptureChecker
{
public:
    CaptureChecker(size_t maxErrors, uint64_t channelsToMask = 0x0);  // maxErrors == 0 means log all errors
    virtual ~CaptureChecker();

    void Clear();

    virtual bool DisableRule(const std::string& id);
    virtual bool EnableRule(const std::string& id);

    virtual bool LogError(const std::string& id, const std::string& s);

    static bool StepSimulator(HpccAcSimulator* sim, ExpectedCaptureState* expected, const std::set<uint32_t>& breakpoints, long numberOfSteps = 1);

    virtual void SimulateAndCheckCapture(const std::vector<CaptureCheckerContext*>& contexts, Bfm* bfm, float period, size_t ratio);
    virtual void _CheckCapture(CaptureCheckerContext* context, ActualCaptureState* actual, ExpectedCaptureState* expected);
    virtual void _CheckCounts(int slot, int slice, size_t actualCount, size_t expectedCount);

    virtual uint64_t PinMismatches(uint64_t expected, uint64_t actual, uint64_t checkMask = 0x0);

    static std::string CaptureEquality(std::vector<FlatCapture>& expectedCapture, std::vector<FlatCapture>& actualCapture, bool checkCaptureCount = true, bool checkFailEvent = true, uint64_t pinLaneDataMask = 0x0);

    std::vector<std::string> disabledRules;

    uint64_t channelsToMask;

    size_t maxErrors;  // 0 means log all errors
    size_t enabledErrorsFound;
    size_t disabledErrorsFound;
    size_t totalErrorsFound;
    size_t totalMissingCapture;

    size_t groupPinLaneData;
};

}  // namespace hpcctbc

#endif  // __CAPTURE_CHECKER_H__

