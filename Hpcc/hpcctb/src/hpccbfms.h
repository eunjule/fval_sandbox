////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hpccbfms.h
//------------------------------------------------------------------------------
//    Purpose: HPCC AC Bus Functional Models
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 05/12/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __BFMS_H__
#define __BFMS_H__

#include <unordered_map>
#include <vector>

#include "hpccsim.h"
#include "waveform.h"

namespace hpcctbc {

struct OverrideWaveform
{
public:
    float startTime;
    float stopTime;
    char logic;
};

class LoopbackBfm : public Bfm
{
public:
    virtual size_t MapChannel(int slot, int slice, size_t ch) const = 0;
    // virtual void SetCompareDelays(const std::unordered_map<size_t, size_t>& delays);
    virtual void SetUp(int slot, int slice);
    virtual void Start(int slot, int slice);
    virtual void AdvanceTo(float time);
    virtual void Drive(int slot, int slice, size_t ch, float time, char logic);
    virtual void OverrideDrive(int slot, int slice, size_t ch, float startTime, float stopTime, char logic);
    virtual char Sample(int slot, int slice, size_t ch, float time);
    virtual char SampleEndState(int slot, int slice, size_t ch, float delta = 0.0);
protected:
    std::unordered_map<size_t, OverrideWaveform> overrides;
    std::unordered_map<size_t, fvalc::Waveform<char, 2048> > loopback;
};

class InternalLoopbackBfm : public LoopbackBfm
{
public:
    inline virtual size_t MapChannel(int slot, int slice, size_t ch) const
    {
        // Every channel must map to a unique value
        return (slot * 2 * HPCC_CHANNELS) + (slice * HPCC_CHANNELS) + ch;
    }
};


class EvenToOddLoopbackBfm : public LoopbackBfm
{
public:
    inline virtual size_t MapChannel(int slot, int slice, size_t ch) const
    {
        // Even and odd channels must map to the same value
        return (slot * HPCC_CHANNELS) + (slice * HPCC_CHANNELS / 2) + (ch / 2);
    }
};

class LowToHighLoopbackBfm : public LoopbackBfm
{
public:
    inline virtual size_t MapChannel(int slot, int slice, size_t ch) const
    {
        // Low and high channels must map to the same value
        if (slice == 0) {
            return (slot * 2 * HPCC_CHANNELS) + (slice * HPCC_CHANNELS) + ch;
        } else {
            if (ch % 2 == 0) {
                return (slot * 2 * HPCC_CHANNELS) + ((slice - 1) * HPCC_CHANNELS) + (ch + 1);
            } else {
                return (slot * 2 * HPCC_CHANNELS) + ((slice - 1) * HPCC_CHANNELS) + (ch - 1);
            }
        }
    }
};

}  // namespace hpcctbc

#endif  // __BFMS_H__

