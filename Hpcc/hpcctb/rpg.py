################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: rpg.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC AC random pattern generator
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/29/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import sys

from Hpcc.hpcctb.assembler import PatternAssembler
from Common import fval

BRANCH_TYPE = ['CALL_I', 'CALL_R', 'PCALL_I', 'PCALL_R'] 
BRANCH_BASE = ['GLOBAL', 'RELATIVE', 'BRBASE.CFB', 'PFB']

PATTERN_TYPE = {'PATTERN': 0, 'SUBROUTINE': 1}

class RPG:
    def __init__(self, constraints = 0, noZX = 1):
        self.constraints = constraints
        self.assembler = PatternAssembler()
        self.patternStr = []
        self._cfb = -1
        self._pfb = -1
        self._numPat = -1 
        self.noZX = noZX
    
    def GenerateFlatPattern(self, mode):
        if (not 'LEN_PAT' in self.constraints):
            raise Exception('LEN_PAT is not defined, cannot create plist')
        length = random.randint(self.constraints['LEN_PAT'][0], self.constraints['LEN_PAT'][1])
        if mode == 'ADDRESS':
            self.GenerateAddressPattern("PATTERN_START", length)
        elif mode == 'ZERO':
            self.GenerateFlatZeroPattern("PATTERN_START", length)
        elif mode == 'RANDOM_EVEN_TO_ODD':
            self.GenerateEvenToOddRandomPattern("PATTERN_START", length)
        else:
            raise Exception('mode {} for GenerateFlatPattern is not supported'.format(mode))
        self.patternStr.append("""\
PATTERN_END:
    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0                              
""")
    
    # TODO: need to factor out Return
    def GeneratePlist(self, mode, returnFlag = False):
        if (not 'NUM_PAT' in self.constraints) or (not 'LEN_PAT' in self.constraints):
            raise Exception('NUM_PAT or LEN_PAT is not defined, cannot create plist')
            
        self._numPat = random.randint(self.constraints['NUM_PAT'][0], self.constraints['NUM_PAT'][1])
        fval.Log('debug', "generating {} patterns of length {} to {}".format(self._numPat, self.constraints['LEN_PAT'][0], self.constraints['LEN_PAT'][1]))
        plist = PList(self.constraints)
        self._cfb = plist._cfb
        
        if 'CALL' in self.constraints:
            numSub = random.randint(self.constraints['NUM_PAT'][0], self.constraints['NUM_PAT'][1]) # not used
            for i in range(self._numPat):
                tag = "SUBROUTINE" + str(i)
                self._GeneratePattern(tag, mode, PATTERN_TYPE['SUBROUTINE'])
        
        for i in range(self._numPat):
            tag = "PATTERN" + str(i)
            self._GeneratePattern(tag, mode, PATTERN_TYPE['PATTERN'])
            if 'PMASK' in self.constraints:
                if self.constraints['PMASK']:
                    plist.MaskUnmask()
            patternID = random.randint(0, self._numPat-1)
            callTag = "PATTERN" + str(patternID)
            plist.CallPattern(callTag)
        
        if returnFlag:
            plist.EndPatternWithFlag()
        else:
            plist.EndPattern()
        
        self.patternStr.extend(plist.plistStr)
        fval.Log('debug', "generate patterns done")
            
    def _GeneratePattern(self, tag, mode, type):
        length = random.randint(self.constraints['LEN_PAT'][0], self.constraints['LEN_PAT'][1])
        if mode == 'ZERO':
            self.GenerateFlatZeroPattern(tag, length)
        elif mode == 'RANDOM_EVEN_TO_ODD':
            self.GenerateEvenToOddRandomPattern(tag, length, type)
        elif mode == 'ADDRESS':
            self.GenerateAddressPattern(tag, length)
        else:
            raise Exception('mode {} for GeneratePlist is not supported'.format(mode))
        self.Return()
        
    def GenerateFlatZeroPattern(self, tag, length):
        self.patternStr.append("""\
{}:
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0 
    Drive0CompareLVectors {}
""".format(tag, length))
        
    def GenerateAddressPattern(self, tag, length):
        self.patternStr.append("""\
{}:
    S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 
    AddressVectors {}
""".format(tag, length))
    
    def GenerateEvenToOddRandomPattern(self, tag, length, type = PATTERN_TYPE['PATTERN']): 
        ctvRate = 0
        if "CTV" in self.constraints:
            ctvRate = self.constraints['CTV']
        mtvRate = 0
        if "MTV" in self.constraints:
            mtvRate = self.constraints['MTV']
        linkRate = 0
        if "CLINK" in self.constraints:
            linkRate = self.constraints['CLINK']
        lrptRate = 0
        if "LRPT" in self.constraints:
            lrptRate = self.constraints['LRPT']
            
        # randomize instructions
        #idlogRate = 0
        #if "PATIDURSLOG" in self.constraints:
        #    idlogRate = self.constraints['PATIDURSLOG']
        callRate = 0
        callNum = 0
        callIndex = []
        rptRate = 0
        rptNum = 0
        rptIndex = []
        aluRate = 0
        aluNum = 0
        aluIndex = []
        metaRate = 0
        metaNum = 0
        metaIndex = []
        vpopRate = 0
        vpopNum = 0
        vpopIndex = []
        if 'CALL' in self.constraints and type == PATTERN_TYPE['PATTERN']: # Todo: allow nested subroutine
            callRate = self.constraints['CALL']
            callNum = round(callRate * length)
            callIndex = random.sample(range(1, length), callNum)
        if 'RPT' in self.constraints: 
            rptRate = self.constraints['RPT']
            rptNum = round(rptRate * length)
            rptIndex = random.sample(range(1, length), rptNum)
        if 'ALU' in self.constraints: 
            aluRate = self.constraints['ALU']
            aluNum = round(aluRate * length)
            aluIndex = random.sample(range(1, length), aluNum)
        if 'META' in self.constraints: 
            metaRate = self.constraints['META']
            metaNum = round(metaRate * length)
            metaIndex = random.sample(range(1, length), metaNum)
        if 'VPOP' in self.constraints: # supports resetCycleCount, resetCaptureCount
            vpopRate = self.constraints['VPOP']
            vpopNum = round(vpopRate * length)
            vpopIndex = random.sample(range(1, length), vpopNum)    
        
        totalNum = callNum + rptNum + aluNum + metaNum + vpopNum
        totalIndex = sorted(list(set(callIndex + rptIndex + aluIndex + metaIndex + vpopIndex))) + [length] # sorted
        
        self.patternStr.append("""\
{}:
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0 
""".format(tag))
        if 'EDGE_COUNTER' in self.constraints:
            if self.constraints['EDGE_COUNTER']:
                self.EnabledEdgeCounter(True)
        
        if totalNum == 0:
            self.patternStr.append("""\
    RandomEvenToOddVectors {}, ctvRate=`{}`, mtvRate=`{}`, linkRate=`{}`, lrptRate=`{}`, noZX ={}
""".format(length, ctvRate, mtvRate, linkRate, lrptRate, self.noZX))
        else:
            # starting part
            self.patternStr.append("""\
    RandomEvenToOddVectors {}, ctvRate=`{}`, mtvRate=`{}`, linkRate=`{}`, lrptRate=`{}`, noZX ={}
""".format(totalIndex[0], ctvRate, mtvRate, linkRate, lrptRate, self.noZX))
            
            # instruction + block of vectors
            # TODO: randomize the order of these instructions
            for i in range(0, len(totalIndex)-1):
                '''
                instructions = [0,1,2]
                random.shuffle(instructions)
                for instr in range(3):
                    if bool(random.getrandbits(1)):
                        if instructions[instr] == 0: # patter ID
                            self.SetPatternId(random.randint(0, 65535))
                        elif instructions[instr] == 1: # usrlog 1
                            self.SetUsrLog1(random.randint(0, 65535))
                        elif instructions[instr] == 2: # usrlog 2
                            self.SetUsrLog2(random.randint(0, 65535))
                        else:
                            raise Exception('RPG error: unexpected instruction')
                '''
                if totalIndex[i] in rptIndex: 
                    # TODO: update the upper limit? just to be sure that pattern won't exceed capture memory limit
                    numRpt = random.randint(64, 5000)
                    if bool(random.getrandbits(1)):
                        self.Repeat('REPEAT_I', numRpt)
                    else:
                        self.Repeat('REPEAT_RA', numRpt)
                    # instruction repeat has to be followed by a vector
                    self.patternStr.append("""\
    RandomEvenToOddVectors 1, ctvRate=`{}`, mtvRate=`{}`, linkRate=`{}`, lrptRate=`{}`, noZX ={}
""".format(ctvRate, mtvRate, linkRate, lrptRate, self.noZX))

                if totalIndex[i] in callIndex:
                    if self._numPat <= 0:
                        raise Exception('numPat = {}, should not happen'.format(self._numPat))
                    subID = random.randint(0, self._numPat-1)
                    subtag = "SUBROUTINE" + str(subID)
                    branchType = BRANCH_TYPE[random.randint(0, len(BRANCH_TYPE) - 3)] # CALL only for subroutine, no PCALL
                    branchBase = BRANCH_BASE[random.randint(0, len(BRANCH_BASE) - 1)] 
                    self.CallSubr(branchBase, branchType, subtag, pfb = tag)
                    
                if totalIndex[i] in aluIndex:
                    self.Alu()
                    self.patternStr.append("""\
    RandomEvenToOddVectors 1, ctvRate=`{}`, mtvRate=`{}`, linkRate=`{}`, lrptRate=`{}`, noZX ={}
""".format(ctvRate, mtvRate, linkRate, lrptRate, self.noZX))

                if totalIndex[i] in metaIndex:
                    self.MetaData()
                    
                if totalIndex[i] in vpopIndex:
                    self.VpOp()  
                    self.patternStr.append("""\
    RandomEvenToOddVectors 1, ctvRate=`{}`, mtvRate=`{}`, linkRate=`{}`, lrptRate=`{}`, noZX ={}
""".format(ctvRate, mtvRate, linkRate, lrptRate, self.noZX))
                    
                self.patternStr.append("""\
    RandomEvenToOddVectors {}, ctvRate=`{}`, mtvRate=`{}`, linkRate=`{}`, lrptRate=`{}`, noZX ={}
""".format(totalIndex[i+1] - totalIndex[i], ctvRate, mtvRate, linkRate, lrptRate, self.noZX))
                
        if 'EDGE_COUNTER' in self.constraints:
            if self.constraints['EDGE_COUNTER']:
                self.EnabledEdgeCounter(False)
        
    def GetPattern(self):
        return "".join(self.patternStr)  
    
    def EndPattern(self):
        self.patternStr.append("""\
PATTERN_END:
    StopPattern 0xdeadbeef                                
""")
        
    def StartPattern(self):
        self.patternStr.append("""\
    S stype=IOSTATEJAM,                 data=0j00000000000000000000000000000000000000000000000000000000 
""")
    
    def MetaData(self):
        self.patternStr.append("""\
    M data=`rand(126)` 
""")

    #  supports resetCycleCount, resetCaptureCount
    def VpOp(self):
        op = random.choice([0b1100, 0b1001])
        if op == 0b1100: # resetCycleCount
            self.patternStr.append("""\
    I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
""")
        elif op == 0b1001: # resetCaptureCount, resets total fail count and channel fail count
            self.patternStr.append("""\
    I optype=VECTOR, vptype=VPOTHER, vpop=RSTFAILCNT
""")
    
    def Alu(self):
        # save reg 0 for nested subr and goto
        regA = random.getrandbits(5)
        if regA == 0:
            regA = 1
        regB = random.getrandbits(5)
        if regB == 0:
            regB = 1
        regDest = random.getrandbits(5)
        if regDest == 0:
            regDest = 1
        aluop = random.getrandbits(3)
        #opdest = random.randint(0, 2) # no flag
        opdest = random.randint(0, 1)
        opsrc = random.getrandbits(1)
        #opsrc = random.getrandbits(2) # no flag
        if aluop == 7:
            self.patternStr.append("""\
        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={0}, imm=`rand(16)`
        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={1}, imm=`rand(16)`
        #I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm=`rand(32)`
        I optype=ALU, aluop={2}, opsrc={3}, opdest={4}, regA={0}, regB={1}, imm=`rand(16)`, dest={5}
    """.format(regA, regB, aluop, opsrc, opdest, regDest))
        elif aluop in [3,4]:
            self.patternStr.append('I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={}, imm=`rand(32)`\n'.format(regA))
            self.patternStr.append('I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={}, imm={}\n'.format(regB, random.randint(0, 32)))
            self.patternStr.append('I optype=ALU, aluop={}, opsrc={}, opdest={}, regA={}, regB={}, imm={}, dest={}\n'.format(aluop, opsrc, opdest, regA, regB, random.randint(0, 32), regDest))
        else:
            self.patternStr.append("""\
        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={0}, imm=`rand(32)`
        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={1}, imm=`rand(32)`
        #I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm=`rand(32)`
        I optype=ALU, aluop={2}, opsrc={3}, opdest={4}, regA={0}, regB={1}, imm=`rand(32)`, dest={5}
    """.format(regA, regB, aluop, opsrc, opdest, regDest))
    
    def Repeat(self, vpop, numRepeat):
        if vpop == 'REPEAT_I':
            self.patternStr.append("""\
    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={}
""".format(numRepeat))
        elif vpop == 'REPEAT_RA':
            regNum = random.randint(0, 31)
            self.SetRegFromImm(regNum, numRepeat)
            self.patternStr.append("""\
    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA={}
""".format(regNum))
    
    def SetCFB(self, CFBAddr):
        self._cfb = CFBAddr
        self.patternStr.append("""\
    I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={}
""".format(CFBAddr))
        
    def ClearStack(self):
        self.patternStr.append("""\
    I optype=BRANCH, br=CLRSTACK
""")
    
    def AddSubroutine(self, label, repeat, data):
        subr = Subroutine(label)
        self.patternStr.append(subr.GetPattern(repeat, data))
        
    def AddBlock(self, label, repeat, data):
        self.patternStr.append("""\
{}:
    %repeat {}
    V link=0, ctv=0, mtv=0, lrpt=0, data=0v{:056b}
    %end
""".format(label, repeat, data))
        
    def CallSubr(self, base, branchType, imm, cond="COND.NONE", inv="0", pfb = -1):
        self._pfb = pfb
        if "_I" in branchType:
            self.CallSubr_I(base, branchType, imm, cond, inv)
        elif "_R" in branchType:
            self.CallSubr_R(base, branchType, imm, cond, inv)
        else:
            raise RuntimeError("incorrect branchType: " + branchType)
            
    def SetPatternId(self, imm):
        self.patternStr.append("""\
    I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm={}
""".format(imm))

    def SetUsrLog1(self, imm):
        self.patternStr.append("""\
        I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm={}
""".format(imm))        

    def SetUsrLog2(self, imm):
        self.patternStr.append("""\
        I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm={}
""".format(imm))          
        
    def SetRegFromImm(self, reg, imm):
        self.patternStr.append("""\
    I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest={}, imm={}
""".format(reg, imm))
        
    # reg ?= imm
    def AluRegImm(self, oper, reg, imm):
        self.patternStr.append("""\
    I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop={}, dest={}, regA={}, imm={}
""".format(oper, reg, reg, imm))
    
    def CallSubr_I(self, base, branchType, imm, cond, inv):
        if isinstance(imm, int):
            immValue = imm
        else:
            if base == 'GLOBAL':
                immValue = 'eval[{}]'.format(imm)
            elif base == 'RELATIVE':
                immValue = 'eval[int32({}-vecaddr)]'.format(imm)
            elif base == 'BRBASE.CFB':
                immValue = 'eval[int32({}-{})]'.format(imm, self._cfb)
            elif base == 'PFB':
                if self._pfb == -1:
                    raise RuntimeError("PFB = {} is not provided, incorrect usage".format(self._pfb))
                immValue = 'eval[int32({}-{})]'.format(imm, self._pfb)
            else:
                raise RuntimeError("incorrect branchBase: " + base)
        self.patternStr.append("""\
    I optype=BRANCH, base={}, br={}, imm={}, cond={}, invcond={}
""".format(base, branchType, immValue, cond, inv))
        

    def CallSubr_R(self, base, branchType, imm, cond, inv):
        if isinstance(imm, int):
            immValue = imm
        else:
            if base == 'GLOBAL':
                immValue = 'eval[{}]'.format(imm)
            elif base == 'RELATIVE':
                immValue = "eval[int32({}-vecaddr-1)]".format(imm)
            elif base == 'BRBASE.CFB':
                immValue = "eval[int32({}-{})]".format(imm, self._cfb)
            elif base == 'PFB':
                if self._pfb == -1:
                    raise RuntimeError("PFB = {} is not provided, incorrect usage".format(self._pfb))
                immValue = 'eval[int32({}-{})]'.format(imm, self._pfb)
            else:
                raise RuntimeError("incorrect branchBase: " + base)
        # hard code use register 0
        self.SetRegFromImm(0, immValue)
        self.patternStr.append("""\
    I optype=BRANCH, base={}, br={}, dest=0, cond={}, invcond={}
""".format(base, branchType, cond, inv))
    
    def Jump(self, branchType, imm):
        self.patternStr.append("""\
    I optype=BRANCH, br={}, imm={}
""".format(branchType, imm))
        
    def Return(self):
        self.patternStr.append("""\
    I optype=BRANCH, br=RET                                  
""")

    def EnabledEdgeCounter(self, enable):
        self.patternStr.append("""\
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
	I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm={}
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
        """.format(int(enable)))
        

class Subroutine(RPG):
    def __init__(self, label, constraints = 0):
        self.constraints = constraints
        self.label = label
        self.patternStr = []

    def GetPattern(self, repeat, data):
        self.patternStr.append("""\
{}:
    %repeat {}
    V link=0, ctv=0, mtv=0, lrpt=0, data=0v{:056b}   
    %end
    I optype=BRANCH, br=RET                                  
""".format(self.label, repeat, data))
        return "".join(self.patternStr)  

class PList():
    def __init__(self, constraints = 0):
        self._cfb = -1 
        self.constraints = constraints
        self.plistStr = ["PATTERN_START:\n"]
        self.plistStr.append("""\
    ClearRegisters
    ClearFlags
""")
        self._SetCFB(random.randint(0, 0xffffffff))
        self._patternId = 0
        
    def MaskUnmask(self):
        self.plistStr.append("""\
    S stype=MASK,       data=`rand(56)`
    S stype=UNMASK,     data=`rand(56)`
""")
        
    def CallPattern(self, patternTag):
        self.plistStr.append("""\
    I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm={}
    I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`{}`
""".format(str(self._patternId), patternTag))
        self._patternId += 1
        
    def EndPattern(self):
        self.plistStr.append("""\
PATTERN_END:
    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0                              
""")

    def EndPatternWithFlag(self):
        self.plistStr.append("""\
PATTERN_END:
    StopPatternReturnFlags
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
""")

    def _SetCFB(self, CFBAddr):
        self._cfb = CFBAddr
        self.plistStr.append("""\
    I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={}
""".format(CFBAddr))



