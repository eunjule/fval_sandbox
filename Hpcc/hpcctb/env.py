################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: env.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC Testbench Environment
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 04/01/15
#       Group: HDMT FPGA Validation
################################################################################

from collections import namedtuple
import csv
from itertools import combinations, chain
import operator
import os
import random
import re
import tarfile
import tempfile
import time

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')

from Common import fval
from Common.instruments.tester import get_tester
from Hpcc import hpcc_configs as hpccConfigs
from Common import globals
from fractions import Fraction
from Hpcc.hpcctb.assembler import PatternAssembler
import Hpcc.instrument.hpccAcRegs as ac_registers
from _build.bin import hpcctbc
from _build.bin import rctbc
from _build.bin import fvalc

PICO_SECONDS_PER_SECOND = 1000000000000
VECTOR_SIZE = 16
MINIMUM_PRECISION = 10


class LogProxy(hpcctbc.Logger):
    def __init__(self):
        super().__init__()
    def Log(self, file, line, function, verbosity, message):
        fval.Log(verbosity, message)

logProxy = LogProxy()
hpcctbc.SetCommonLogger(logProxy)
rctbc.SetCommonLogger(logProxy)

halTester = None
calTiu = None

# Create pattern that clears all the registers
asm = PatternAssembler()
asm.LoadString("""
    StartPattern
    RandomVectors length=1024
    ClearRegisters
    RandomVectors length=1024
    StopPattern 0x12345678
""")
_clearRegistersPattern = asm.Generate()


class Env(fval.Env):
    @staticmethod
    def gcd(a, b):
        """Return greatest common divisor using Euclid's Algorithm."""
        while b:      
            a, b = b, a % b
        return a
    @staticmethod
    def lcm(a, b):
        """Return lowest common multiple."""
        return a * b // Env.gcd(a, b)

    # This method is called once per test fixture
    def __init__(self, test):
        super().__init__(test)
        self.Log('info', 'Creating new Hpcc test environment instance')
        self.Log('info', '--- INITIALIZE PHASE ---')

        self.calMode = "None"
        self.CALIBRATION_PATH = os.path.join(repo_root_path, hpccConfigs.CALIBRATION_DIR)

        # FIXME: Workaround
        fvalc.SetSeed(random.randint(0, 1 << 64 - 1))

        self._snapshotOffsets = {}
        self._snapshotLengths = {}
    
    def tester(self):
        return get_tester()
    
    @property
    def rc(self):
        return self.tester().get_tester_rc()
    
    @property
    def models(self):
        return self.tester().hpcc_models
        
    @property
    def enabledIlas(self):
        if hpccConfigs.HPCC_TAKE_SNAPSHOT:
            return self.get_hpcc_ac_fpgas()
        else:
            return []
    
    @property
    def enabledSimTraces(self):
        if hpccConfigs.HPCC_TAKE_SNAPSHOT:
            return self.get_hpcc_ac_fpgas()
        else:
            return []
    
    @property
    def enabledCalDumps(self):
        if hpccConfigs.HPCC_TAKE_SNAPSHOT:
            return self.get_hpcc_ac_fpgas()
        else:
            return []
    
    @property
    def instruments(self):
        hpcc_list =  self.tester().get_undertest_instruments('Hpcc')
        if not any(hpcc.name() == 'Hpcc' for hpcc in hpcc_list):
            self.Log('error', 'No Hpcc cards on tester')
        return {hpcc.slot:hpcc for hpcc in hpcc_list}
    
    @property
    def fpgas(self):
        return self.get_hpcc_ac_fpgas()

    def get_hpcc_ac_fpgas(self):
        fpgas = []
        for slot, hpcc in self.instruments.items():
            for ac in hpcc.ac:
                if hpccConfigs.HPCC_SLICE1_ONLY and ac.slice != 1:
                    continue
                key = (hpcc.slot, ac.slice)
                fpgas.append(key)
        return fpgas

    def take_snapshot_if_enabled(self):
        if hpccConfigs.HPCC_TAKE_SNAPSHOT:
            filename = '{}.tar.gz'.format(self.test.Name().replace('.', '_'))
            self.TakeSnapshot(filename)

    def SetConfig(self, config, caloffset = 0, loadCal = True):
        if (hpccConfigs.HPCC_ENV_INIT in ['NONE', 'ZEROCAL']):
            loadCal = False
        validConfigs = ['InternalLoopback', 'EvenToOddLoopback', 'LowToHighLoopback', 'AllPinDrive']
        if config not in validConfigs:
            raise Exception('Invalid envinronment config \'{}\'. Expecting a name in {}'.format(config, validConfigs))
        self.config = config
        self.Log('debug', 'Using {} environment configuration'.format(self.config))
        bfm = None
        if self.config == 'InternalLoopback':
            bfm = hpcctbc.InternalLoopbackBfm()
            for slot, hpcc in self.instruments.items():
                for ac in hpcc.ac:
                    bfm.SetUp(ac.slot, ac.slice);
                    ac.internalLoopback = True
        elif self.config == 'AllPinDrive':
            bfm = hpcctbc.InternalLoopbackBfm()
            for slot, hpcc in self.instruments.items():
                hpcc.cal.ClearAll()
                for ac in hpcc.ac:
                    bfm.SetUp(ac.slot, ac.slice);
                    ac.internalLoopback = False
        elif self.config == 'EvenToOddLoopback':
            bfm = hpcctbc.EvenToOddLoopbackBfm()
            for slot, hpcc in self.instruments.items():
                if loadCal and self.calMode != 'EvenToOddLoopback':
                    if not hpccConfigs.HPCC_SLICE1_ONLY:
                        self.Log('debug', 'Loading even to odd cal data for HPCC slot {} slice {} from {}'.format(slot, 0, hpcc.calFile.EvenToOddSlice0))
                        hpcc.ac[0].caldata.LoadInputCal(hpcc.calFile.EvenToOddSlice0,caloffset)
                    if not hpccConfigs.HPCC_SLICE0_ONLY:
                        self.Log('debug', 'Loading even to odd cal data for HPCC slot {} slice {} from {}'.format(slot, 1, hpcc.calFile.EvenToOddSlice1))
                        hpcc.ac[1].caldata.LoadInputCal(hpcc.calFile.EvenToOddSlice1,caloffset)
                
                hpcc.cal.SetEvenToOddChannelLoopback()
                for ac in hpcc.ac:
                    bfm.SetUp(ac.slot, ac.slice);
                    ac.internalLoopback = False
            if loadCal:
                self.calMode = 'EvenToOddLoopback'
        elif self.config == 'LowToHighLoopback':
            bfm = hpcctbc.LowToHighLoopbackBfm()
            for slot, hpcc in self.instruments.items():
                if loadCal and self.calMode != 'LowToHighLoopback':
                    if not hpccConfigs.HPCC_SLICE1_ONLY:
                        self.Log('debug', 'Loading low to high cal data for HPCC slot {} slice {} from {}'.format(slot, 0, hpcc.calFile.LowToHighSlice0))
                        hpcc.ac[0].caldata.LoadInputCal(hpcc.calFile.LowToHighSlice0)
                    if not hpccConfigs.HPCC_SLICE0_ONLY:
                        self.Log('debug', 'Loading low to high cal data for HPCC slot {} slice {} from {}'.format(slot, 1, hpcc.calFile.LowToHighSlice1))
                        hpcc.ac[1].caldata.LoadInputCal(hpcc.calFile.LowToHighSlice1)
                
                hpcc.cal.SetLowToHighChannelLoopback()
                for ac in hpcc.ac:
                    bfm.SetUp(ac.slot, ac.slice);
                    ac.internalLoopback = False
            if loadCal:
                self.calMode = 'LowToHighLoopback'
        # Save a bfm reference in the environment or future bfm access from the simulator will crash
        self.bfm = bfm
        for slot, hpccModel in self.models.items():
            hpccModel.acSim[0].SetBfm(self.bfm)
            hpccModel.acSim[1].SetBfm(self.bfm)

    def SetDomains(self, domains):
        for (key, value) in domains.items():
            slot, slice = key
            master, domainId = value
            hpcc = self.instruments[slot]
            data = hpcc.ac[slice].read_register(ac_registers.TriggerControl)
            data.CardType = 0x0
            data.DomainID = domainId
            data.DomainMaster = 0b1 if master else 0b0
            hpcc.ac[slice].write_register(data)

    def RandomSEMaskGenerator(self, domainIdList):
        SEMask =''
        swappedDomainIdList = [ 15 - i for i in domainIdList ]
        for i in range(16):
            if i in swappedDomainIdList:
                SEMask += str( random.randint(0, 1) )
            else:
                SEMask += '0'
        return int(SEMask, 2)

    def SetDomainsDutID32BitWithNamedTuple(self, domains):
        for domain in domains:
            slot, slice = domain.slotslice
            master, domainId, dutID = domain.master, domain.domainId, domain.dutID
            hpcc = self.instruments[slot]
            data = hpcc.ac[slice].read_register(ac_registers.TriggerControl)
            data.CardType = 0x0
            data.DomainID = domainId
            data.DomainMaster = 0b1 if master else 0b0
            data.DutID = dutID
            hpcc.ac[slice].write_register(data)

        
    def MTDdomainDutIDGroupGenerator(self, alwaysMaster=1, dutIDRange=[63, 64]):
        dutIDRangeStart = dutIDRange[0]
        dutIDRangeStop = dutIDRange[1]
        alwaysMasterExist = alwaysMaster
        domainID = list( range(15) )
        dutID = list( range(dutIDRangeStart, dutIDRangeStop) )
        fpgas =  self.fpgas
        fpgasLength = len( fpgas )
        
        random.shuffle(fpgas) # randomly select one of permuted tuple of FPGA tuples
        selectedPermutedFpgasList = [ list(i) for i in  fpgas ] # Change tuple into List for further processing
        selectedPermutedFpgasList1 = [ list(i) for i in  fpgas ] # Change tuple into List for further processing
         
        mtdDomainGroup = namedtuple('mtdDomainGroup', 'slotslice master domainId synch dutID')
        mtdDomainGroupList = []
        assignedDomainIDList = []
        assignedDutIDList = []
        synch_modifier = 5
        master = 1
        member = 0
        for domain in  random.choice( self.SumToN( fpgasLength) ) :
            isMasterExist = random.randint(alwaysMasterExist, 1)
            masterID = random.randint( 1, domain )
            assignedDomainID = random.choice( domainID) #domainID.pop( random.randint( 0, len(domainID) - 1 ) )
            assignedDutID = random.choice(dutID) # Forcing same DUT ID for Same domain ID 
            for idx, grp in enumerate( list( self.PopRange(selectedPermutedFpgasList, 0, domain-1) ), 1 ):
                assignedDutIDList.append(assignedDutID)
                if idx == masterID and isMasterExist and not assignedDomainID in assignedDomainIDList:
                    mtdDomainGroupList.append(mtdDomainGroup( tuple(grp), master, assignedDomainID, synch_modifier, assignedDutID ))
                else:
                    mtdDomainGroupList.append(mtdDomainGroup( tuple(grp), member, assignedDomainID, synch_modifier, assignedDutID ))

            assignedDomainIDList.append(assignedDomainID) # to avoid multiple domain master for the same randomly assigned domainID.
        return mtdDomainGroupList, assignedDomainIDList, assignedDutIDList
    
    def MTDdomainGroupGenerator(self):
        domainID = list( range(15) )
        fpgas =  self.fpgas
        fpgasLength = len( fpgas )
        
        random.shuffle(fpgas) # randomly select one of permuted tuple of FPGA tuples
        selectedPermutedFpgasList = [ list(i) for i in  fpgas ] # Change tuple into List for further processing

        mtdDomainGroup = namedtuple('mtdDomainGroup', 'slotslice master domainId synch')
        mtdDomainGroupList = []
        assignedDomainIDList = []
        for domain in random.choice( self.SumToN( fpgasLength) ):
            isMasterExist = random.randint(0, 1)
            masterID = random.randint( 1, domain )
            assignedDomainID = random.choice( domainID) #domainID.pop( random.randint( 0, len(domainID) - 1 ) )
            assignedDomainIDList.append(assignedDomainID)
            for idx, grp in enumerate( self.PopRange(selectedPermutedFpgasList, 0, domain-1), 1 ):
                if idx == masterID and isMasterExist:
                    mtdDomainGroupList.append(mtdDomainGroup( tuple(grp), 1, assignedDomainID, 0 ))
                else:
                    mtdDomainGroupList.append(mtdDomainGroup( tuple(grp), 0, assignedDomainID, 0 ))
        return mtdDomainGroupList, assignedDomainIDList
    
    # Generate the list of numbers to be summed up to n. This list is used for generating number of domain members given number of slices.
    def SumToN(self, n):
        b, mid, e = [0], list(range(1,n) ), [n]
        splits = (d for i in range(n) for d in combinations(mid, i) )
        return list(list( map(operator.sub, chain(s,e), chain(b,s) )) for s in splits)
    
    # Generate the random iterable to group fpga devices. 
    def RandomRange(self, start, stop):
        x = stop
        while x >= start:
            y = random.randint( start, x )
            yield y
            x -=  y

    def PopRange(self, lst,start, stop):
        x = start
        while x <= stop:
            yield lst.pop(0)
            x += 1


    def UnzipSnapshot(self, snapshot):
        snapshotDir = tempfile.mkdtemp()
        self.Log('debug', 'Extracting snapshot to {}'.format(snapshotDir))
        tar = tarfile.open(snapshot, "r:gz")
        tar.extractall(snapshotDir)
        tar.close()
        
        subdirs = next(os.walk(snapshotDir))[1]
        if len(subdirs) == 1:
            snapshotDir = os.path.join(snapshotDir, subdirs[0])
            self.Log('debug', 'Updated snapshotDir to {}'.format(snapshotDir))
            
        return snapshotDir
        
    def LoadPatternMemoryFromSnapshot(self, slot, slice, path):
        self.Log('info', 'Loading pattern memory from \'{}\''.format(path))
        PATTERN_FILE_REGEX = re.compile('Pattern_Type(0x.*)_Id(0x.*)_Offset(0x.*?).bin')
        if not os.path.isdir(path):
            raise Exception('\'{}\' is not a valid directory'.format(path))
        hpcc = self.instruments[slot]
        count = 0
        subfiles = next(os.walk(path))[2]  # Immediate child files
        for subfile in subfiles:
            filename = os.path.join(path, subfile)
            if os.path.isfile(filename):
                m = PATTERN_FILE_REGEX.match(subfile)
                if m:
                    groups = m.groups()
                    patternType = int(groups[0], 0)
                    patternId = int(groups[1], 0)
                    patternOffset = int(groups[2], 0) * VECTOR_SIZE
                    if patternType in [0x0, 0x1]:  # 0x0 = Pattern, 0x1 = Plist, 0x2 = Capture
                        self.Log('debug', 'Loading {} to 0x{:x}'.format(subfile, patternOffset))
                        fin = open(filename, 'rb')
                        hpcc.ac[slice].DmaWrite(patternOffset, fin.read())
                        fin.close()
                        count = count + 1
        self.Log('info', 'Loaded {} binary objects'.format(count))

    def LoadRegistersFromSnapshot(self, slot, slice, filename):
        self.Log('info', 'Loading registers from \'{}\''.format(filename))
        hpcc = self.instruments[slot]
        fin = open(filename, 'r')
        firstLine = True
        for row in csv.reader(fin):
            if firstLine:
                firstLine = False
                continue
            offset = int(str(row[0]), 0)
            data = int(str(row[1]), 0)
            hpcc.ac[slice].BarWrite(0, offset, data)
        fin.close()


        
    @staticmethod
    def _ConvertToFraction(decimal):
        fract = Fraction(int(decimal * MINIMUM_PRECISION), MINIMUM_PRECISION)
        return fract
        


    def convert(self,types, values):
        return [t(v) for t, v in zip(types, values)]

    def WriteHeaderCsv(self,header, filename):
        headers = header
        filename = filename
        tmp = open( filename + '.tmp', 'w')
        orig = open(filename, 'r')
        tmp.write(','.join(headers) + '\n')
        for line in orig.readlines():
            tmp.write(line)
        orig.close()
        tmp.close()
        os.remove(filename)
        os.rename( filename+ '.tmp', filename)

    def TakeSnapshot(self, filename, fpgas = None, offsets = None, lengths = None):
        if fpgas is None:
            fpgas = self.fpgas
        if offsets is None:
            offsets = self._snapshotOffsets
        if lengths is None:
            lengths = self._snapshotLengths
        def MakeTar(filename, sourceDir):
            if not(filename.endswith('.tar.gz')):
                filename = '{}.tar.gz'.format(filename)
            self.Log('info', 'Creating snapshot {}'.format(filename))
            with tarfile.open(filename, "w:gz") as tar:
                tar.add(sourceDir, arcname = os.path.basename(sourceDir))
        
        try:
            snapshotDir = tempfile.mkdtemp(prefix = self.test.Name().replace('.', '_') + '_')
        except AttributeError:
            snapshotDir = tempfile.mkdtemp(prefix = filename.replace('.', '_') + '_')
        
        
        self.Log('debug', 'Temporary snapshot directory {}'.format(snapshotDir))
        for slot, slice in fpgas:
            hpcc = self.instruments[slot]
            path = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}'.format(slot, slice))
            os.makedirs(path)
            hpcc.ac[slice].TakeRegistersSnapshot(os.path.join(path, 'registers.csv'))
            hpcc.ac[slice].TakeTimingSnapshot(slot, slice, os.path.join(path, 'timingInfo.txt'))
            if (slot, slice) in self.enabledCalDumps:
                hpcc.ac[slice].TakeCalSnapshot(slot, slice, os.path.join(path, 'AcCalibration.csv') )
            if not hpccConfigs.HPCC_NO_PAT_SNAPSHOT:
                patternMemoryPath = os.path.join(path, 'PatternMemory')
                os.makedirs(patternMemoryPath)
                
                if offsets == {}:
                    offset = None
                elif type(offsets) == dict:
                    offset = offsets[(slot, slice)]
                else:
                    offset = offsets
                length = None
                
                if lengths == {}:
                    length = None
                elif type(lengths) == dict:
                    length = lengths[(slot, slice)]
                else:
                    length = lengths
                if offset is None:
                    self.Log('warning', 'No pattern offset recorded for snapshot')
                elif length is None:
                    self.Log('warning', 'No pattern offset recorded for snapshot')
                else:
                    hpcc.ac[slice].TakePatternMemorySnapshot(slot, slice, offset, length, patternMemoryPath)
                
                hpcc.ac[slice].TakeCaptureMemorySnapshot(slot, slice, patternMemoryPath)
                if (slot, slice) in self.enabledSimTraces:
                    simPath = os.path.join(path, 'Sim')
                    os.makedirs(simPath)
                    hpcc.ac[slice].TransferSimTraces(slot, slice, simPath)
            if (slot, slice) in self.enabledIlas:
                ilaPath = os.path.join(path, 'ILAs')
                os.makedirs(ilaPath)
                hpcc.ac[slice].TransferIlas(slot, slice, ilaPath)
        MakeTar(filename, snapshotDir)

    def SetSelectableFailMask(self, slot, slice, mtv, mask):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
        hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)

    def ClearRegisters(self, slot, slice):
        # Run the clear pattern at a slow speed
        self.SetConfig('InternalLoopback')
        self.RunPattern(slot, slice, _clearRegistersPattern, period = 100e-9)
        self.RunCheckers(slot, slice, checkRegisters = True)

    def WritePattern(self, slot, slice, offset, pattern, checkSimMemory=True):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].WritePattern(offset, pattern, checkSimMemory)
        self._snapshotOffsets[(slot, slice)] = offset
        self._snapshotLengths[(slot, slice)] = len(pattern)

    def ReadPattern(self, slot, slice, offset, length):
        hpcc = self.instruments[slot]
        return hpcc.ac[slice].ReadPattern(offset, length)

    def ExecutePatternViaSyncPulse(self, slot, slice, start, numberOfTries = 500, waitForComplete = True):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].PrestagePattern(start, numberOfTries)
        # Send sync pulse    
        self.rc.send_sync_pulse()
        # Wait for complete
        completed = False
        if waitForComplete:
            completed = hpcc.ac[slice].WaitForCompletePattern(numberOfTries)
        return completed
    
    def WriteExecutePatternAndSetupCapture(self, slot, slice, pattern = None, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, CaptureCTVAfterMaxFail = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500):
        hpcc = self.instruments[slot]

        if not(pattern is None):  # Write pattern to memory
            self.WritePattern(slot, slice, offset, pattern)

        if captureAddress is None:
            if pattern is None:
                raise Exception('If no pattern is set, captureAddress must be set!')
            else:
                captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern))
        # Setup capture
        hpcc.ac[slice].SetCaptureBaseAddress(captureAddress)
        hpcc.ac[slice].SetCaptureCounts(maxFailCountPerPatern = maxFailCountPerPatern, maxFailCaptureCount = maxFailCaptureCount, maxCaptureCount = maxCaptureCount, captureLength = captureLength)
        hpcc.ac[slice].SetCaptureControl(captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap, CaptureCTVAfterMaxFail)
        # Clear fail counts
        hpcc.ac[slice].Write('TotalFailCount', 0)
        for ch in range(56):
            hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)

        if (slot, slice) in self.enabledIlas:
            # Arm ILAs just before pattern execution
            hpcc.ac[slice].LoadAndArmIlas(slot, slice)

        completed = self.ExecutePatternViaSyncPulse(slot, slice, start, numberOfTries, waitForComplete)                    
        #completed = hpcc.ac[slice].ExecutePattern(start, waitForComplete, numberOfTries)
        
        if waitForComplete and not maskFailureToComplete:
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))

        if (slot, slice) in self.enabledIlas:
            # Dump ILAs
            hpcc.ac[slice].DumpIlas(slot, slice)
        
        # Return pattern status
        return completed, hpcc.ac[slice].Read('PatternEndStatus').Pack()

    #the method setups Capture Counts, Capture Control and Pattern Control.   
    #only used by the test case FailCapture.DirectedClearFailCountFromPrevPatternTest	
    def PatternSetupCapture(self, slot, slice, pattern = None, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, CaptureCTVAfterMaxFail = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500):
        hpcc = self.instruments[slot]

        if not(pattern is None):  # Write pattern to memory
            self.WritePattern(slot, slice, offset, pattern)

        if captureAddress is None:
            if pattern is None:
                raise Exception('If no pattern is set, captureAddress must be set!')
            else:
                captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern))
        
        hpcc.ac[slice].SetCaptureBaseAddress(captureAddress)
        self.Log('debug', 'AlarmControl before setCaptureCount register= {:x}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
        hpcc.ac[slice].SetCaptureCounts(maxFailCountPerPatern = maxFailCountPerPatern, maxFailCaptureCount = maxFailCaptureCount, maxCaptureCount = maxCaptureCount, captureLength = captureLength)
        self.Log('debug', 'AlarmControl after setup Capture Count register= {:x}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
        hpcc.ac[slice].SetCaptureControl(captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap, CaptureCTVAfterMaxFail)
        self.Log('info', 'AlarmControl after setup Capture Control register = {:x}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
        

 
        
        # Return pattern status
        return hpcc.ac[slice].Read('PatternEndStatus').Pack()
        
    def ExecutePatternViaSyncPulseWithoutWaitForComplete(self, slot, slice, start, numberOfTries ):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].PrestagePattern(start, numberOfTries)
        # Send sync pulse    
    
    def WriteExecutePatternAndSetupCapturePerSlice(self, slot, slice, pattern, offset , start , captureAddress , captureType , captureFails , captureCTVs , captureAll , stopOnMaxFail , dramOverflowWrap , maxFailCountPerPatern , maxFailCaptureCount , maxCaptureCount , captureLength , waitForComplete , maskFailureToComplete, numberOfTries ):
        hpcc = self.instruments[slot]

        if not(pattern is None):  # Write pattern to memory
            self.WritePattern(slot, slice, offset, pattern)

        if captureAddress is None:
            if pattern is None:
                raise Exception('If no pattern is set, captureAddress must be set!')
            else:
                captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern))
        # Setup capture
        hpcc.ac[slice].SetCaptureBaseAddress(captureAddress)
        hpcc.ac[slice].SetCaptureCounts(maxFailCountPerPatern = maxFailCountPerPatern, maxFailCaptureCount = maxFailCaptureCount, maxCaptureCount = maxCaptureCount, captureLength = captureLength)
        hpcc.ac[slice].SetCaptureControl(captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap)
        # Clear fail counts
        hpcc.ac[slice].Write('TotalFailCount', 0)

        if (slot, slice) in self.enabledIlas:
            # Arm ILAs just before pattern execution
            hpcc.ac[slice].LoadAndArmIlas(slot, slice)

        self.ExecutePatternViaSyncPulseWithoutWaitForComplete(slot, slice, start, numberOfTries)                    
        #completed = hpcc.ac[slice].ExecutePattern(start, waitForComplete, numberOfTries)
        
    def SetPeriodACandSim(self, slot, slice, period):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].SetPeriod(period)
        hpccModel = self.models[slot]
        hpccModel.acSim[slice].SetPeriod(period)
        
    def SetPeriodSubcycleAndInternalACandSimWithZeroDelay(self, slot, slice, subCycleCount, internalPeriod):
        hpcc = self.instruments[slot]
        #hpcc.ac[slice].SetPeriodSubcycleAndInternal(subCycleCount, internalPeriod)
        hpcc.ac[slice].SetPeriodSubcycleZeroAndInternal(subCycleCount, internalPeriod)
        hpccModel = self.models[slot]
        hpccModel.acSim[slice].SetPeriod(hpcc.ac[slice].period)
    
    def SetPeriodSubcycleAndInternalACandSim(self, slot, slice, subCycleCount, internalPeriod):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].SetPeriodSubcycleAndInternal(subCycleCount, internalPeriod)
        #hpcc.ac[slice].SetPeriodSubcycleZeroAndInternal(subCycleCount, internalPeriod)
        hpccModel = self.models[slot]
        hpccModel.acSim[slice].SetPeriod(hpcc.ac[slice].period)
    
    def SetDelayStackZero(self, slot, slice, channels = []):
        hpcc =self.instruments[slot]
        hpcc.dc[slice].SetZeroDelay(channels)


    def SetPEAttributesDCandSimWithZeroDelay(self, slot, slice, PEAttributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peList = globals.ALL_PINS
        elif channels == 'EVEN_PINS':
            peList = globals.EVEN_PINS
        elif channels == 'ODD_PINS':
            peList = globals.ODD_PINS
        elif isinstance(channels, list):
            peList = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        hpcc = self.instruments[slot]
        hpcc.dc[slice].SetPEAttributesWithZeroDelay(PEAttributes, apply, peList)
        hpccModel = self.models[slot]
        for attribute in PEAttributes:
            value = PEAttributes[attribute]
            if attribute.upper() == "DRIVE":
                hpccModel.acSim[slice].SetDriveDelay(peList, value)
            elif attribute.upper() == "COMPARE":
                hpccModel.acSim[slice].SetCompareDelay(peList, value)
            elif attribute.upper() == "DRIVE_TYPE":
                continue
                # if value == 'FIRST':
                    # self._driveType = 1
                # elif value == 'SECOND':
                    # self._driveType = 0
                # elif value == 'NATIVE':
                    # self._driveType = 2
                # else:
                     # raise Exception('attribute DRIVE_TYPE {} is not supported'.format(value))
            elif attribute.upper() == "RESET":
                continue
                # if value == 'NO_TRANSITION':
                    # self._resetState = 1
                # elif value == 'TRANSITION':
                    # self._resetState = 0
                # else:
                     # raise Exception('attribute RESET {} is not supported'.format(value))
            elif attribute.upper() == "RATIO":
                hpccModel.acSim[slice].SetEnClkRatio(peList, value)
            elif attribute.upper() == "TRISE":
                hpccModel.acSim[slice].SetEnClkTrise(peList, value)
            elif attribute.upper() == "TFALL":
                hpccModel.acSim[slice].SetEnClkTfall(peList, value)
            elif attribute.upper() == "FIXED_DRIVE_STATE":
                pass
                #hpccModel.acSim[slice].fixedDriveState = value
            elif attribute.upper() == "IS_ENABLED_CLOCK": #nothing need to be done on the sim side
                continue
            else:
                raise Exception('set attribute {} is not supported in Sim'.format(attribute))
        
    def SetKeepModeAttributes(self, slot, slice, PEAttributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peList = globals.ALL_PINS
        elif channels == 'EVEN_PINS':
            peList = globals.EVEN_PINS
        elif channels == 'ODD_PINS':
            peList = globals.ODD_PINS
        elif isinstance(channels, list):
            peList = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        hpcc = self.instruments[slot]
        hpcc.dc[slice].SetPEKeepMode(PEAttributes, apply, peList)

    def SetFreqCounterModeAttributes(self, slot, slice, PEAttributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peList = globals.ALL_PINS
        elif channels == 'EVEN_PINS':
            peList = globals.EVEN_PINS
        elif channels == 'ODD_PINS':
            peList = globals.ODD_PINS
        elif isinstance(channels, list):
            peList = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        hpcc = self.instruments[slot]
        hpcc.dc[slice].SetFreqCounterMode(PEAttributes, apply, peList)


    def SetPEAttributesDCandSimTimingZeroDelay(self, slot, slice, PEAttributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peList = globals.ALL_PINS
        elif channels == 'EVEN_PINS':
            peList = globals.EVEN_PINS
        elif channels == 'ODD_PINS':
            peList = globals.ODD_PINS
        elif isinstance(channels, list):
            peList = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        hpcc = self.instruments[slot]
        hpcc.dc[slice].SetPEAttributesWithZeroDelay(PEAttributes, apply, peList)
        hpccModel = self.models[slot]
        for attribute in PEAttributes:
            value = PEAttributes[attribute]
            if attribute.upper() == "DRIVE":
                hpccModel.acSim[slice].SetDriveDelay(peList, value)
            elif attribute.upper() == "COMPARE":
                hpccModel.acSim[slice].SetCompareDelay(peList, value)
            elif attribute.upper() == "DRIVE_TYPE":
                continue
                # if value == 'FIRST':
                    # self._driveType = 1
                # elif value == 'SECOND':
                    # self._driveType = 0
                # elif value == 'NATIVE':
                    # self._driveType = 2
                # else:
                     # raise Exception('attribute DRIVE_TYPE {} is not supported'.format(value))
            elif attribute.upper() == "RESET":
                continue
                # if value == 'NO_TRANSITION':
                    # self._resetState = 1
                # elif value == 'TRANSITION':
                    # self._resetState = 0
                # else:
                     # raise Exception('attribute RESET {} is not supported'.format(value))
            elif attribute.upper() == "RATIO":
                hpccModel.acSim[slice].SetEnClkRatio(peList, value)
            elif attribute.upper() == "TRISE":
                hpccModel.acSim[slice].SetEnClkTrise(peList, value)
            elif attribute.upper() == "TFALL":
                hpccModel.acSim[slice].SetEnClkTfall(peList, value)
            elif attribute.upper() == "FIXED_DRIVE_STATE":
                pass
                #hpccModel.acSim[slice].fixedDriveState = value
            elif attribute.upper() == "IS_ENABLED_CLOCK": #nothing need to be done on the sim side
                continue
            else:
                raise Exception('set attribute {} is not supported in Sim'.format(attribute))
        
    def SetPEAttributesDCandSim(self, slot, slice, PEAttributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peList = globals.ALL_PINS
        elif channels == 'EVEN_PINS':
            peList = globals.EVEN_PINS
        elif channels == 'ODD_PINS':
            peList = globals.ODD_PINS
        elif isinstance(channels, list):
            peList = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        hpcc = self.instruments[slot]
        hpcc.dc[slice].SetPEAttributes(PEAttributes, apply, peList)
        hpccModel = self.models[slot]
        for attribute in PEAttributes:
            value = PEAttributes[attribute]
            if attribute.upper() == "DRIVE":
                hpccModel.acSim[slice].SetDriveDelay(peList, value)
            elif attribute.upper() == "COMPARE":
                hpccModel.acSim[slice].SetCompareDelay(peList, value)
            elif attribute.upper() == "DRIVE_TYPE":
                continue
                # if value == 'FIRST':
                    # self._driveType = 1
                # elif value == 'SECOND':
                    # self._driveType = 0
                # elif value == 'NATIVE':
                    # self._driveType = 2
                # else:
                     # raise Exception('attribute DRIVE_TYPE {} is not supported'.format(value))
            elif attribute.upper() == "RESET":
                continue
                # if value == 'NO_TRANSITION':
                    # self._resetState = 1
                # elif value == 'TRANSITION':
                    # self._resetState = 0
                # else:
                     # raise Exception('attribute RESET {} is not supported'.format(value))
            elif attribute.upper() == "RATIO":
                hpccModel.acSim[slice].SetEnClkRatio(peList, value)
            elif attribute.upper() == "TRISE":
                hpccModel.acSim[slice].SetEnClkTrise(peList, value)
            elif attribute.upper() == "TFALL":
                hpccModel.acSim[slice].SetEnClkTfall(peList, value)
            elif attribute.upper() == "FIXED_DRIVE_STATE":
                pass
                #hpccModel.acSim[slice].fixedDriveState = value
            elif attribute.upper() == "IS_ENABLED_CLOCK": #nothing need to be done on the sim side
                continue
            elif attribute.upper() == "VOX": #nothing need to be done on the sim side
                continue
            elif attribute.upper() == "TERMVREF": #nothing need to be done on the sim side
                continue
            else:
                raise Exception('set attribute {} is not supported in Sim'.format(attribute))
        
    def SetPeriodAndPEAttributesWithZeroDelay(self, slot, slice, period, attributes = None, subcycleCount = None, internalPeriod = None, zeroDelay=None):
        hpcc = self.instruments[slot]
        if (subcycleCount is not None) and (internalPeriod is not None) and (zeroDelay is not None):
            self.SetPeriodSubcycleAndInternalACandSimWithZeroDelay(slot, slice, subcycleCount, internalPeriod)
            # There is no need to set PE attributes in internal loopback mode
            if self.config not in ['InternalLoopback']:
                if attributes is None:
                    attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': hpcc.ac[slice].period / 2}
                self.SetPEAttributesDCandSimWithZeroDelay(slot, slice, attributes, True)
            else: # for internal loopback
                hpccModel = self.models[slot]
                hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
                hpccModel.acSim[slice].SetCompareDelay(list(range(56)), hpcc.ac[slice].period / 2)
        else:
            if period is not None:
                self.SetPeriodACandSim(slot, slice, period)
                # There is no need to set PE attributes in internal loopback mode
                if self.config not in ['InternalLoopback']:
                    if attributes is None:
                        attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}
                    self.SetPEAttributesDCandSim(slot, slice, attributes, True)
                else: # for internal loopback
                    hpccModel = self.models[slot]
                    hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
                    hpccModel.acSim[slice].SetCompareDelay(list(range(56)), period / 2)
    
    def SetPeriodAndPEAttributes(self, slot, slice, period, attributes = None, subcycleCount = None, internalPeriod = None):
        hpcc = self.instruments[slot]
        if (subcycleCount is not None) and (internalPeriod is not None):
            self.SetPeriodSubcycleAndInternalACandSim(slot, slice, subcycleCount, internalPeriod)
            # There is no need to set PE attributes in internal loopback mode
            if self.config not in ['InternalLoopback']:
                if attributes is None:
                    attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': hpcc.ac[slice].period / 2}
                self.SetPEAttributesDCandSim(slot, slice, attributes, True)
            else: # for internal loopback
                hpccModel = self.models[slot]
                hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
                hpccModel.acSim[slice].SetCompareDelay(list(range(56)), hpcc.ac[slice].period / 2)
        else:
            if period is not None:
                self.SetPeriodACandSim(slot, slice, period)
                # There is no need to set PE attributes in internal loopback mode
                if self.config not in ['InternalLoopback']:
                    if attributes is None:
                        attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': (period / 2  )}
                    self.SetPEAttributesDCandSim(slot, slice, attributes, True)
                else: # for internal loopback
                    hpccModel = self.models[slot]
                    hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
                    hpccModel.acSim[slice].SetCompareDelay(list(range(56)), period / 2)
    
    def SetPeriodAndPEAttributesTimingZeroDelay(self, slot, slice, period, attributes = None, subcycleCount = None, internalPeriod = None):
        hpcc = self.instruments[slot]
        if (subcycleCount is not None) and (internalPeriod is not None):
            self.SetPeriodSubcycleAndInternalACandSim(slot, slice, subcycleCount, internalPeriod)
            # There is no need to set PE attributes in internal loopback mode
            if self.config not in ['InternalLoopback']:
                if attributes is None:
                    attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': hpcc.ac[slice].period / 2}
                self.SetPEAttributesDCandSim(slot, slice, attributes, True)
            else: # for internal loopback
                hpccModel = self.models[slot]
                hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
                hpccModel.acSim[slice].SetCompareDelay(list(range(56)), hpcc.ac[slice].period / 2)
        else:
            if period is not None:
                self.SetPeriodACandSim(slot, slice, period)
                # There is no need to set PE attributes in internal loopback mode
                if self.config not in ['InternalLoopback']:
                    print('Non Internal Loopback Path')
                    if attributes is None:
                        attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}
                    self.SetPEAttributesDCandSimTimingZeroDelay(slot, slice, attributes, True)
                else: # for internal loopback
                    print('Internal Loopback Right PATH')
                    if attributes is None:
                        attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}
                    self.SetPEAttributesDCandSimTimingZeroDelay(slot, slice, attributes, True)
                    #hpccModel = self.models[slot]
                    #hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
                    #hpccModel.acSim[slice].SetCompareDelay(list(range(56)), period / 2)
    
    # default capture type = 1 = pin_state
    # always drive at 0, strobe in the middle, i.e. half of the period
    def RunPattern(self, slot, slice, pattern = None, period = 10e-9, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, CaptureCTVAfterMaxFail = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500, subcycleCount = None, internalPeriod = None):
        #self.Log('info', 'Period is {}'.format(period) )
        self.SetPeriodAndPEAttributes(slot, slice, period, subcycleCount = subcycleCount, internalPeriod = internalPeriod)
        return self.WriteExecutePatternAndSetupCapture(slot, slice, pattern, offset, start, captureAddress, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap, CaptureCTVAfterMaxFail, maxFailCountPerPatern, maxFailCaptureCount, maxCaptureCount, captureLength, waitForComplete, maskFailureToComplete, numberOfTries)
    
    #copy from the method RunPattern above but only for capture control and pattern control setup 
	#move execute pattern into test case itself
	#Only used in the test cases FailCapture.DirectedClearFailCountFromPrevPatternTest
    def RunPatternSetUp(self, slot, slice, pattern = None, period = 10e-9, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, CaptureCTVAfterMaxFail = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500, subcycleCount = None, internalPeriod = None):
        #self.Log('info', 'Period is {}'.format(period) )
        self.SetPeriodAndPEAttributes(slot, slice, period, subcycleCount = subcycleCount, internalPeriod = internalPeriod)
        return self.PatternSetupCapture(slot, slice, pattern, offset, start, captureAddress, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap, CaptureCTVAfterMaxFail, maxFailCountPerPatern, maxFailCaptureCount, maxCaptureCount, captureLength, waitForComplete, maskFailureToComplete, numberOfTries)
    
    def RunPatternTimingZeroDelay(self, slot, slice, pattern = None, period = 10e-9, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, CaptureCTVAfterMaxFail = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500, subcycleCount = None, internalPeriod = None):
        self.SetPeriodAndPEAttributesTimingZeroDelay(slot, slice, period, subcycleCount = subcycleCount, internalPeriod = internalPeriod)
        return self.WriteExecutePatternAndSetupCapture(slot, slice, pattern, offset, start, captureAddress, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap, CaptureCTVAfterMaxFail, maxFailCountPerPatern, maxFailCaptureCount, maxCaptureCount, captureLength, waitForComplete, maskFailureToComplete, numberOfTries)

    def AbortPattern(self, slot, slice):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].AbortPattern()

    def SetSimAbort(self, slot, slice, address):
        hpccModel = self.models[slot]
        hpccModel.acSim[slice].externalAbort = True
        hpccModel.acSim[slice].externalAbortAddress = address

    def CaptureCount(self, slot, slice):
        hpcc = self.instruments[slot]
        return hpcc.ac[slice].Read('TotalCaptureCount').Pack()



    def ReadAndFlattenCapture(self, slot, slice):
        hpcc = self.instruments[slot]
        captureData, blockCount, captureCount = hpcc.ReadCapture(slice)
        flatCapture = hpcctbc.FlatCapture_vector()
        hpcctbc.FlattenCapture(captureData, blockCount, flatCapture)
        return flatCapture

    def CaptureEquality(self, expectedCapture, actualCapture, checkCaptureCount = True, checkFailEvent = True, pinLaneDataMask = 0x0):
        return hpcctbc.CaptureChecker.CaptureEquality(expectedCapture, actualCapture, checkCaptureCount, checkFailEvent, pinLaneDataMask)

    def RunMultiSliceCheckers(self, multiSlices, captureDisableRules = [], maxCaptureErrors = 5, channelsToMask = 0):
        contexts = []
        alarmList = []
        periodsAndRatios = []
        simRatio = 1
        captureDataList = []  # This is critical to ensure that Python's Garbage Collector does not get rid of captureData variable
        for kargs in multiSlices:
            slot = kargs['slot']
            slice = kargs['slice']
            alarmsToMask = [] if 'alarmsToMask' not in kargs else kargs['alarmsToMask']
            checkRegisters = False if 'checkRegisters' not in kargs else kargs['checkRegisters']
            ratio = 1 if 'ratio' not in kargs else kargs['ratio']
            hpcc = self.instruments[slot]
            hpccModel = self.models[slot]
            # Check alarms
            alarms = self.CheckAlarms(slot, slice, alarmsToMask)
            alarmList.append(alarms)
            # Read capture data
            captureData, blockCount, captureCount = hpcc.ReadCapture(slice)
            # Update simulator parameters
            sim = hpccModel.acSim[slice]
            period = hpcc.ac[slice].period
            if (slot, slice) in self.enabledSimTraces:
                sim.SetupTrace()
                sim.EnableIOStateJamTrace('io_state_jam_trace_{}_{}.txt'.format(slot, slice))
                sim.EnableInstructionTrace('instruction_trace_{}_{}.txt'.format(slot, slice))
                sim.EnableCaptureTrace('capture_trace_{}_{}.txt'.format(slot, slice))
            # Start pattern simulator (which will simulate the pattern vector by vector and send capture data to the capture checker)
            sim.Start()
            # Create capture context and add to list
            context = hpcctbc.CaptureCheckerContext(captureData, blockCount, captureCount, sim, ratio)
            captureDataList.append(captureData)
            contexts.append(context)
            # Figure out simulator ratio, which is the LCM of all the ratios
            simRatio = Env.lcm(simRatio, ratio)
            periodsAndRatios.append((period, ratio))
        
        # Figure out simulator period
        simPeriod = 0
        for period, ratio in periodsAndRatios:
            # Add normalized period
            simPeriod = simPeriod + (period / ratio)
        simPeriod = simPeriod / len(periodsAndRatios)
        # Validate simulator period
        for period, ratio in periodsAndRatios:
            error = abs((period / ratio) - simPeriod)
            if error > 1e-15:
                raise Exception('Invalid (period, ratio) combination: ({}, {})'.format(period, ratio))

        # Check capture data for all slices
        captureChecker = hpcctbc.CaptureChecker(maxCaptureErrors, channelsToMask)
        # Disable rules
        for rule in captureDisableRules:
            captureChecker.DisableRule(rule)
        # Run the checker for all slices
        captureChecker.SimulateAndCheckCapture(hpcctbc.CaptureCheckerContext_ptr_vector(contexts), self.bfm, simPeriod, simRatio)
        passed = captureChecker.totalErrorsFound == 0
        del captureChecker

        for kargs in multiSlices:
            slot = kargs['slot']
            slice = kargs['slice']
            endStatusMask = 0x0 if 'endStatusMask' not in kargs else kargs['endStatusMask']
            checkRegisters = False if 'checkRegisters' not in kargs else kargs['checkRegisters']
            hpcc = self.instruments[slot]
            hpccModel = self.models[slot]
            sim = hpccModel.acSim[slice]
            if self.config not in ['InternalLoopback']:
                context = contexts.pop(0)
                if context.firstFailOffsetFound:
                    acutalFirstFailAddress = hpcc.ac[slice].Read('FirstFailAddress').Pack()
                    expectedFirstFailAddress = ((context.firstFailOffset // 8) * 8) + hpcc.ac[slice].Read('CaptureBaseAddress').Pack() # block address, not vector address
                    if expectedFirstFailAddress != acutalFirstFailAddress:
                        self.Log('error', 'First Fail Address mismatch, expected=0x{:x}, actual=0x{:x}'.format(expectedFirstFailAddress, acutalFirstFailAddress))
                    else:
                        self.Log('debug', 'First Fail Address match, expected=0x{:x}, actual=0x{:x}'.format(expectedFirstFailAddress, acutalFirstFailAddress))
                else:
                    self.Log('debug', 'No fail captured, skip first fail address check')
                
                if sim.exactFailCounts:
                    # Check Total Fail Count
                    actualTotalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
                    if actualTotalFailCount != sim.failcnt:
                        self.Log('error', 'Total Fail Count mismatch, expected=0x{:x}, actual=0x{:x}'.format(sim.failcnt, actualTotalFailCount))
                        passed = False                    
                    for ch in range(56):
                        # skip checking fail counts for sim masked channel
                        if channelsToMask >> ch & 1:
                            continue
                        
                        # Check Per-Channel Fail Counts
                        actualChannelFailCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount').Pack()
                        channel = sim.GetChannel(ch)
                        if actualChannelFailCount != channel.failcnt:
                            self.Log('error', 'Channel Fail Count mismatch for channel {}, expected=0x{:x}, actual=0x{:x}'.format(ch, channel.failcnt, actualChannelFailCount))
                            passed = False
                        
                        # check frequency counter
                        # channel.mode: 0: drive, 1: compare, 2: enabled clock
                        # only check compare pins for fast period tests since drive side is not calibrated
                        if hpcc.ac[slice].Read('Version').MajorVersion >= 24:
                            if channel.mode == 1 or hpcc.ac[slice].period > 30e-9:
                                actualFreqCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFrequencyCounts').Pack()
                                if actualFreqCount != channel.freqCounter:
                                    self.Log('error', 'Channel Freqency Count mismatch for channel {}, expected=0x{:x}, actual=0x{:x}'.format(ch, channel.freqCounter, actualFreqCount))
                                    passed = False
                                #else:
                                #    self.Log('info', 'Channel Freqency Count mismatch for channel {}, expected=0x{:x}, actual=0x{:x}'.format(ch, channel.freqCounter, actualFreqCount))
                else: # max fail count reached, stopOnMaxFail = True
                    # Check Total Fail Count
                    actualTotalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
                    if actualTotalFailCount < sim.failcnt:
                        self.Log('error', 'Total Fail Count mismatch, expected>=0x{:x}, actual=0x{:x}'.format(sim.failcnt, actualTotalFailCount))
                        passed = False
                    if actualTotalFailCount > (sim.failcnt + 2000): # worst case, pattern should be stopped by FPGA within 2k vectors (stop on max fail)
                        self.Log('error', 'Total Fail Count mismatch, expected<=0x{:x}, actual=0x{:x}'.format(sim.failcnt + 2000, actualTotalFailCount))
                        passed = False
                    # Check Per-Channel Fail Counts
                    for ch in range(56):
                        # skip checking fail counts for sim masked channel
                        if channelsToMask >> ch & 1:
                            continue
                        actualChannelFailCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount').Pack()
                        channel = sim.GetChannel(ch)
                        if actualChannelFailCount < channel.failcnt:
                            self.Log('error', 'Channel Fail Count mismatch for channel {}, expected>=0x{:x}, actual=0x{:x}'.format(ch, channel.failcnt, actualChannelFailCount))
                            passed = False
            # Check pattern end status register
            # only when not aborted, if abort due to reaching max fail count, FPGA keeps going for <16k vectors, may or may not reach END 
            if sim.exactFailCounts and ('PatternEndStatusCheck' not in alarmsToMask):
                actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
                if (actualEndStatus & ~endStatusMask) != (sim.endstatus & ~endStatusMask):
                    self.Log('error', 'Pattern end status mismatch for ({},{}), expected=0x{:x}, actual=0x{:x}'.format(slot, slice, sim.endstatus & ~endStatusMask, actualEndStatus & ~endStatusMask))
                    passed = False
                # checking pin state after pattern execution ends
                if self.config == 'EvenToOddLoopback' or self.config == 'LowToHighLoopback':
                    result = self.CheckEndState(slot, slice)
                    if not result:
                        passed = False
            if checkRegisters:
                if sim.exactFailCounts:
                    for i in range(32):
                        actual = hpcc.ac[slice].Read('CentralDomainRegister{}'.format(i)).Pack()
                        expected = sim.reg[i]
                        if actual != expected:
                            self.Log('error', 'Register r{} value mismatch for ({},{}), expected=0x{:x}, actual=0x{:x}'.format(i, slot, slice, expected, actual))
                            passed = False
                else:
                    self.Log('warning', 'skip register check since pattern was aborted.')
        for captureData in captureDataList:
            del captureData

        return passed, alarmList

    def RunCheckers(self, slot, slice, endStatusMask = 0x0, alarmsToMask = [], checkRegisters = False, captureDisableRules = [], maxCaptureErrors = 5, channelsToMask = 0):
        passed, alarmList = self.RunMultiSliceCheckers([{
            'slot': slot,
            'slice': slice,
            'endStatusMask': endStatusMask,
            'alarmsToMask': alarmsToMask,
            'checkRegisters': checkRegisters
        }], captureDisableRules, maxCaptureErrors, channelsToMask)
        return passed, alarmList[0]

    def ClearAlarms(self, slot, slice):
        hpcc = self.instruments[slot]
        hpcc.ac[slice].Write('AlarmControl', 0x0)

    def CheckAlarms(self, slot, slice, alarmsToMask = []):
        hpcc = self.instruments[slot]
        return hpcc.ac[slice].CheckAlarms(alarmsToMask)

    def LogPerSlice(self, slot, slice):
        self.Log('info', 'Testing HPCC Slot {} Slice {}'.format(slot, slice))

    def LogPerSlot(self, slot):
        self.Log('info', 'Testing HPCC Slot {}'.format(slot))

    # should be call the first thing in a test case
    @staticmethod
    def OverrideTestSeed(seed):
        random.seed(seed)
        fvalc.SetSeed(random.randint(0, 1 << 64 - 1))
        
    def RandomizeParameters(self, slot, slice, pattern, avePerPatternLen):
        EDGE_COUNTER_PARAM = {0: 'OFF', 1: 'RISE', 2: 'FALL', 3: 'BOTH'}
                
        hpcc = self.instruments[slot]
        if hpcc.ac[slice].minQdrMode == -1:
            self.Log('warning', 'It looks like the FPGA capabilities were not loaded (was Initialize() skipped?). Loading them now...')
            hpcc.ac[slice]._UpdateFpgaCapabilities()
        
        #offset = 0x0, # hard to randomize because of sim
        #captureLength = 0xFFFFFFFF
        
        patternVectorLen = len(pattern) // VECTOR_SIZE
        #print(patternVectorLen)
        
        result = {}
        result['captureAll'] = bool(random.getrandbits(1))
        result['captureFails'] = bool(random.getrandbits(1))
        result['captureCTVs'] = bool(random.getrandbits(1))
        result['CaptureCTVAfterMaxFail'] = bool(random.getrandbits(1))
        #result['captureType'] = random.randint(0,3)
        result['captureType'] = random.choice([0,1,3])        
        result['maxFailCaptureCount'] = random.randint(1, patternVectorLen * 10)
        result['maxFailCountPerPatern'] = random.randint(1, avePerPatternLen)
        result['maxCaptureCount'] = random.randint(1, patternVectorLen)
        result['stopOnMaxFail'] = bool(random.getrandbits(1))
        result['edgeCounter'] = EDGE_COUNTER_PARAM[random.randint(0,3)]
        #result['captureLength'] = 64
        
        result['linkMode'] = random.randint(0,2)
        
        #result['period'] = random.uniform(hpcc.ac[slice].minQdrMode, hpcc.ac[slice].maxPeriod) / PICO_SECONDS_PER_SECOND
        result['period'] = random.uniform(30000, hpcc.ac[slice].maxPeriod) / PICO_SECONDS_PER_SECOND #random.uniform(2500, 3000) / PICO_SECONDS_PER_SECOND 
        # randomize capture address
        # Note: maxCaptureByteAddress may not be enough if there are too much compression/loops in the pattern
        maxCaptureByteAddress = 32 * 1024 * 1024 * 1024 - hpccConfigs.HPCC_AC_SIM_MEM * VECTOR_SIZE
        randomCaptureAddress = random.randint(len(pattern), maxCaptureByteAddress)
        result['captureAddress'] = hpcc.ac[slice].SinglePatternCaptureBaseAddress(randomCaptureAddress)
        
        self.Log('debug', "Random Parameters: " + str(result))
        return result
        
    def RandomizeMTVFailMask(self, slot, slice):
        self.SetSelectableFailMask(slot, slice, 0, 0b00000000000000000000000000000000000000000000000000000000) # default
        self.SetSelectableFailMask(slot, slice, 1, 0b11111111111111111111111111111111111111111111111111111111)
        self.SetSelectableFailMask(slot, slice, 2, random.getrandbits(56))
        self.SetSelectableFailMask(slot, slice, 3, random.getrandbits(56))
        self.SetSelectableFailMask(slot, slice, 4, random.getrandbits(56))
        self.SetSelectableFailMask(slot, slice, 5, random.getrandbits(56))
        self.SetSelectableFailMask(slot, slice, 6, random.getrandbits(56))
        self.SetSelectableFailMask(slot, slice, 7, random.getrandbits(56))
        
    def ClearMTVFailMask(self, slot, slice):
        for mtv in range(8):
            self.SetSelectableFailMask(slot, slice, mtv, 0b00000000000000000000000000000000000000000000000000000000)
        
    def CheckEndState(self, slot, slice):
        hpcc = self.instruments[slot]
        sim = self.models[slot].acSim[slice]
        endState = sim.endState
        #print(endState)
        
        time.sleep(0.05) # sleep 0.05 sec, make sure end state holds        
        result = True
        for channel in range(56):    
            VOX = hpcc.dc[slice].pe[channel].attribute.vox
            voltage = hpcc.dc[slice].GetPmuMeasurements(channel, 5)
            # do not check Z by default, since Z ~ VOX by default
            if endState[55 - channel] != 'z':
                expectHigh = int(endState[55 - channel])
                #print("channel {} voltage {}".format(channel, voltage))
                if expectHigh and voltage < VOX:
                    self.Log('error', 'HPCC {} SLICE {} Channel {}: expect voltage > {}, actual voltage {}'.format(slot, slice, channel, VOX, voltage))
                    result = False
                elif (not expectHigh) and voltage > VOX:
                    self.Log('error', 'HPCC {} SLICE {} Channel {}: expect voltage < {}, actual voltage {}'.format(slot, slice, channel, VOX, voltage))
                    result = False
        return result
        
    def LogCentralDomainRegisters(self, slot, slice):
        hpcc = self.instruments[slot]
        for i in range(32):
            self.Log('info', 'r{} = {}'.format(i, hpcc.ac[slice].Read('CentralDomainRegister{}'.format(i)).Pack()))

