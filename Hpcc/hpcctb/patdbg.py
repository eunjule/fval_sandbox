################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: patdbg.py
#-------------------------------------------------------------------------------
#     Purpose: Pattern debugger
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/14/15
#       Group: HDMT FPGA Validation
################################################################################

import cmd
import csv
import gzip

from Hpcc.hpcctb import disassembler
from _build.bin import hpcctbc

HPCC_AC_SIM_MEM = 100 * 1024 * 1024


class PatternDebugger(cmd.Cmd):
    intro = 'Welcome to the HPCC Pattern Debugger. Type help or ? to list commands.\n'
    prompt = '(patdb) '
    def Init(self):
        self.sim = hpcctbc.HpccAcSimulator(8, 0, HPCC_AC_SIM_MEM)
        self.expected = hpcctbc.ExpectedCaptureState()
        #self.bfm = hpcctbc.InternalLoopbackBfm()
        self.bfm = hpcctbc.EvenToOddLoopbackBfm()
        self.sim.SetBfm(self.bfm)
        self.breakpoints = set()
    def do_echo(self, args):
        self.Echo(*parse(args))
    def do_load(self, args):
        self.Load(*parse(args))
    def do_loadregs(self, args):
        self.LoadRegs(*parse(args))
    def do_start(self, args):
        self.Start(*parse(args))
    def do_step(self, args):
        self.Step(*parse(args))
    def do_break(self, args):
        self.Breakpoint(*parse(args))
    def do_continue(self, args):
        self.Continue(*parse(args))
    def do_regs(self, args):
        def BoolToChar(b):
            return '<' if b else ' '
        numberOfRows = 16
        # Add oddball registers
        l = [
            ('vecaddr', '0x{:08x}'.format(self.sim.vecaddr)),
            ('tcc', '0x{:08x}'.format(self.sim.tcc)),
            ('pcc', '0x{:08x}'.format(self.sim.pcc)),
            ('ucc', '0x{:08x}'.format(self.sim.ucc)),
            ('patid', '0x{:08x}'.format(self.sim.patid)),
            ('log1', '0x{:08x}'.format(self.sim.log1)),
            ('log2', '0x{:08x}'.format(self.sim.log2)),
            ('flags', '0x{:08x}'.format(self.sim.flags)),
            ('repeat', '0x{:08x} {}'.format(self.sim.repeat, BoolToChar(self.sim.repeatFlag))),
            ('failbr', '0x{:08x} {}'.format(self.sim.failbr, BoolToChar(self.sim.failbrFlag))),
            ('failcnt', '0x{:08x}'.format(self.sim.failcnt)),
            ('patfcnt', '0x{:08x}'.format(self.sim.patfcnt)),
            ('cfb', '0x{:08x}'.format(self.sim.cfb)),
            ('pfb', '0x{:08x}'.format(self.sim.pfb)),
            ('endstatus', '0x{:08x}'.format(self.sim.endstatus)),
        ]
        # Add padding
        padding = numberOfRows - len(l)
        for i in range(padding):
            l.append(None)
        # Add general purpose registers
        for i, value in enumerate(list(self.sim.reg)):
            l.append(('reg[{}]'.format(i), '0x{:08x}'.format(value)))
        # Print registers
        rows = ['' for i in range(numberOfRows)]  # Create empty rows
        for i, item in enumerate(l):
            cell = ''
            if item is not None:
                name, value = item
                cell = '{:>9} = {}'.format(name, value)
            rows[i % numberOfRows] = rows[i % numberOfRows] + '{:<24} '.format(cell)
        print('\n'.join(rows))
        # Print pin state
        if hasattr(self.sim, 'PinLogic'):
            print('    logic = {}'.format(self.sim.PinLogic()))
        print('     pins = {}'.format(self.sim.PinState()))
        print('      dir = {}'.format(self.sim.PinDirection()))
        print('     mask = {:056b}'.format(self.sim.mask & 0b11111111111111111111111111111111111111111111111111111111))
    def do_setupcapture(self, args):
        self.SetupCapture(*parse(args))
    def do_captures(self, args):
        print("\n".join(self.captures))
    def do_masks(self, args):
        print('  clsmask = {:056b}'.format(self.sim.TestClassFailMask & 0b11111111111111111111111111111111111111111111111111111111))
        print('  patmask = {:056b}'.format(self.sim.patmask & 0b11111111111111111111111111111111111111111111111111111111))
        print('  vecmask = {:056b}'.format(self.sim.vecmask & 0b11111111111111111111111111111111111111111111111111111111))
        print('     mask = {:056b}'.format(self.sim.vecmask & 0b11111111111111111111111111111111111111111111111111111111))
    def do_disassemble(self, args):
        self.Disassemble(*parse(args))
    def do_enclk(self, args):
        self.Enclk(*parse(args))
    def do_credits(self, args):
        self.Credits(*parse(args))
    def do_quit(self, args):
        """
        Exit from patdb.
        """
        return True
    def do_EOF(self, args):
        return True
    def Echo(self, message):
        print(message)
    def Load(self, filename, address = '0'):  # address is in vectors
        if filename.endswith('.gz'):
            self.sim.DmaWrite(16 * int(address, 0), gzip.open(filename, 'rb').read())
        else:
            self.sim.DmaWrite(16 * int(address, 0), open(filename, 'rb').read())
    def LoadRegs(self, filename):
        fin = open(filename, 'r')
        firstLine = True
        for row in csv.reader(fin):
            if firstLine:
                firstLine = False
                continue
            offset = int(str(row[0]), 0)
            data = int(str(row[1]), 0)
            self.sim.BarWrite(0, offset, data)
        fin.close()
    def Start(self, address = '0'):  # address is in vectors
        self.sim.patternStartAddress = int(address, 0)
        self.sim.Start()
    def SetupCapture(self, captureType = 'PIN_STATE', captureAll = '0', captureFails = '0', captureCTVs = '0'):
        data = 0
        if captureType == 'FAIL_STATUS':
            data = data | 0
        elif captureType == 'PIN_STATE':
            data = data | 1
        elif captureType == 'PIN_DIR':
            data = data | 2
        else:
            print('Invalid capture type = {}'.format(captureType))
        data = data | (int(captureFails, 0) & 0b1) << 2
        data = data | (int(captureCTVs, 0) & 0b1) << 3
        data = data | (int(captureAll, 0) & 0b1) << 4
        self.sim.BarWrite(0, 0x000081A0, data)

    def Breakpoint(self, address):
        self.breakpoints.add(int(address, 0))
        print('Breakpoint {} at {}'.format(len(self.breakpoints), address))
    def Step(self, numberOfSteps = '1'):
        """
        Execute a tester cycle and then return to the command line interpreter.
        """
        breakpoints = hpcctbc.uint32_t_set()
        for b in self.breakpoints:
            breakpoints.insert(b)
        return hpcctbc.CaptureChecker.StepSimulator(self.sim, self.expected, breakpoints, int(numberOfSteps, 0))
    def Continue(self):
        breakpoints = hpcctbc.uint32_t_set()
        for b in self.breakpoints:
            breakpoints.insert(b)
        return hpcctbc.CaptureChecker.StepSimulator(self.sim, self.expected, breakpoints, -1)
    def Disassemble(self, numberOfVectors = 1):
        for i in range(int(numberOfVectors, 0)):
            print(disassembler.disassemble(self.sim.ReadPattern(self.sim.vecaddr + i)))
    def Enclk(self, data):
        data = int(data, 0)
        for i in range(56):
            b = data & 0b1
            data = data >> 1
            self.sim.BarWrite(0, 0x00009000 + 16 * i, b << 29)
    def Credits(self):
        print('Balance = {}, willFail = {}, failVecAddr = {}'.format(self.sim.creditCounter.balance, self.sim.creditCounter.willFail, self.sim.creditCounter.failVecAddr))
    # Shortcuts
    def do_q(self, args):
        return self.do_quit(args)
    def do_s(self, args):
        return self.do_step(args)
    def do_b(self, args):
        return self.do_break(args)
    def do_c(self, args):
        return self.do_continue(args)

def parse(args):
    return tuple(args.split())


if __name__ == '__main__':
    patdb = PatternDebugger()
    patdb.Init()
    patdb.cmdloop()

