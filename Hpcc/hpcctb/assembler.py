################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: assembler.py
# -------------------------------------------------------------------------------
#     Purpose: HPCC AC Pattern Assembler. Key Features:
#                1. *All* possible pattern words are supported (every pattern word field can be set).
#                2. User can compile pattern "assembly code" (v.s. calling Python functions).
#                3. Supports macros in the pattern "assembly code" and in Python.
#                4. Supports generators to create vector content on the fly (for memory efficiency and performance).
#                   Generators are only supported in Python/C++.
#                5. Supports "literals" (e.g. 0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0).
#                6. Implements all the low-level primitives in C++ (for performance).
#                7. Can be called from the command-line to compile pattern source code.
#              Architected as a two-pass assembler:
#                * First pass expands (flattens out) macro calls and captures labels.
#                  (Note that generators are not expanded).
#                     - Executed in Load* methods
#                * Second pass expands generators, resolves values (including labels),
#                  and creates vectors (i.e. pattern words).
#                     - Executed when the pattern is compiled
# -------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/24/15
#       Group: HDMT FPGA Validation
################################################################################

import os

from ThirdParty import pyparsing

from _build.bin import fvalc
from _build.bin import hpcctbc
from Common import fval
from Hpcc.hpcctb import structs, symbols


def CreatePatternAssemblerContext(assembler):
    context = hpcctbc.PatternAssemblerContext()
    context.startaddr = assembler.Resolve('startaddr', False)
    context.vecaddr = assembler.Resolve('vecaddr', False)
    return context


class PatternAssembler(fval.asm.Assembler):
    VECTOR_WIDTH = 128  # bits
    VECTOR_SIZE = VECTOR_WIDTH // 8  # bytes
    _ENCODED_WORD_SIZE = VECTOR_SIZE
    _BUILTIN_DIRECTIVES = ['V', 'M', 'S', 'I', 'R']
    _GENERATOR_NAMESPACE = hpcctbc
    _GENERATOR_CONTEXT_MAKER = CreatePatternAssemblerContext
    LITERAL_CHARACTERS = pyparsing.Word('01XZLHSKRE')
    _CUSTOM_LITERALS = pyparsing.Combine('0j' + LITERAL_CHARACTERS) | pyparsing.Combine('0v' + LITERAL_CHARACTERS)
    vector_literal_encoding = {'Z': 0, 'X': 0, 'K': 1, '0': 2, 'L': 2, '1': 3, 'H': 3}  # Vector symbols
    vector_alternate_encoding = {'0': 0, '1': 1, 'R': 2, 'E': 3, 'Z': 2, 'X': 3}  # enabled clock and io jam symbols

    def V(self, array, offset, bcast=0, sblink=0, nprpt=0, sce=0, link=0, ctv=0, mtv=0, lrpt=0, data=0):
        type = 0
        data = self.Resolve(data)
        array[offset:offset + 14] = data.to_bytes(length=14, byteorder='little')
        array[offset + 15] = ((type << 6) | (self.Resolve(bcast) << 5) | (self.Resolve(sblink) << 4) | self.Resolve(
            nprpt) << 3) | (self.Resolve(sce) << 2) | (self.Resolve(link) << 1) | (self.Resolve(ctv));
        array[offset + 14] = (self.Resolve(mtv) << 5) | (self.Resolve(lrpt));

    def M(self, array, offset, data=0):
        data = symbols.PATTERN.METADATA_WORD << 126 | self.Resolve(data)
        array[offset:offset + 16] = data.to_bytes(length=self.VECTOR_SIZE, byteorder='little')

    def S(self, array, offset, stype=0, lsb_shift=None, virtual_pin_id=None, record_length=None, serial_channel=None, data=0, undefined=0,
          unused=0):
        if stype == 'SCC':
            self.serial_capture_control_word(array, offset, stype, serial_channel, record_length, virtual_pin_id,
                                             lsb_shift)
        else:
            hpcctbc.WritePinStateWord(
                array,
                offset,
                self.Resolve(stype),
                self.Resolve(data),
                self.Resolve(undefined),
                self.Resolve(unused)
            )

    def serial_capture_control_word(self, array, offset, stype, serial_channel, record_length, virtual_pin_id,
                                    lsb_shift):
        psw = structs.SCCPinStateWord()
        psw.type = 2
        psw.pin_state_type = self.Resolve(stype)
        psw.reserved = 0
        psw.undefined = 0
        if lsb_shift is not None:
            psw.lsb_shift = self.Resolve(lsb_shift)
        if virtual_pin_id is not None:
            psw.virtual_pin_id = self.Resolve(virtual_pin_id)
        if record_length is not None:
            psw.record_length = self.Resolve(record_length)
        if serial_channel is not None:
            psw.serial_channel = self.Resolve(serial_channel)
        array[offset:offset + 16] = psw.value

    def I(self, array, offset, optype, extop=None, action=None, opdest=None, opsrc=None, aluop=None, vptype=None,
          vpop=None, invcond=None, cond=None, base=None, br=None, dest=None, regB=None, regA=None, imm=None, swrsvd=0,
          rsvd=0):
        i = structs.Instruction()
        i.type = symbols.PATTERN.INSTRUCTION_WORD
        i.op_type = getattr(symbols.OPTYPE, optype)
        if i.op_type == symbols.OPTYPE.EXT:
            op_code = (self.Resolve(extop) << 4) | self.Resolve(action)
            i.op_code_4_0 = op_code
            i.op_code_6_5 = op_code >> 5
        elif i.op_type == symbols.OPTYPE.ALU:
            i.op_code_4_0 = (self.Resolve(opsrc) << 3) | self.Resolve(aluop)
            i.op_code_6_5 = self.Resolve(opdest)
        elif i.op_type == symbols.OPTYPE.REGISTER:
            opdest = self.symbols[opdest]
            opsrc = self.symbols[opsrc]
            i.op_code_4_0 = (opdest << 3) | opsrc
            i.op_code_6_5 = opdest >> 2
        elif i.op_type == symbols.OPTYPE.VECTOR:
            vptype = getattr(symbols.VPTYPE, vptype)
            if vptype == symbols.VPTYPE.VPLOG:
                vpop = getattr(symbols.VPLOG, vpop)
            elif vptype == symbols.VPTYPE.VPLOCAL:
                vpop = getattr(symbols.VPLOCAL, vpop)
            else:
                vpop = getattr(symbols.VPOTHER, vpop)
            vp = (vptype << 4) | vpop
            i.op_code_4_0 = vp
            i.op_code_6_5 = vp >> 5
        if invcond is not None:
            i.invert_condition = int(invcond, 0)
        if cond is not None:
            i.condition_type = self.Resolve(cond)
        if base is not None:
            i.branch_base = self.Resolve(base)
        if br is not None:
            i.branch_type = getattr(symbols.BRANCH, br)
        if dest is not None:
            i.destination = self.Resolve(dest)
        if regB is not None:
            i.b = int(regB, 0)
        if regA is not None:
            i.a = int(regA, 0)
        if imm is not None:
            i.immediate = self.Resolve(imm)

        i.reserved = swrsvd
        i.reserved2 = rsvd
        array[offset:offset + 16] = i.value

    def R(self, array, offset, data=0):
        array[offset:offset + self.VECTOR_SIZE] = int(data, base=0).to_bytes(length=self.VECTOR_SIZE,
                                                                             byteorder='little')

    def __init__(self, startAddress=0):
        super(PatternAssembler, self).__init__(startAddress)
        # Default variables
        self.vars = {
            'enclk': 0,
            'keepmode': 0,
        }

    # Supported literals:
    #   - Vector data (e.g. 0vLHHHHLLLSXXXZKZ)
    #   - IO State Jam data (e.g. 0j01010111XZ)
    def ResolveLiteral(self, expr):
        value = self.ResolvePreviouslySeenLiteral(expr)
        if value is None:
            value = self.ResolveLiteralFromString(expr)
            self.RecordLiteral(expr, value)
        return value

    def ResolvePreviouslySeenLiteral(self, expr):
        try:
            return self.literal_seen[expr]
        except AttributeError:
            self.literal_seen = {}
        except KeyError:
            pass

    def RecordLiteral(self, expr, value):
        self.literal_seen[expr] = value

    def ResolveLiteralFromString(self, expr):
        self.CheckLiteral(expr)
        value = 0
        enabled_clock = self.vars['enclk']
        bit_mask = 1 << 55
        for c in expr[2:]:
            if expr[:2] == '0j' or enabled_clock & bit_mask:
                value = (value << 2) | self.EncodeEnabledClock(c)
            else:
                value = (value << 2) | self.EncodeData(c)
            bit_mask = bit_mask >> 1
        return value

    def EncodeData(self, char):
        try:
            return self.vector_literal_encoding[char]
        except KeyError:
            raise ValueError(f'Invalid Vector data character "{char}"')

    def EncodeEnabledClock(self, char):
        try:
            return self.vector_alternate_encoding[char]
        except KeyError:
            raise ValueError(f'Invalid Vector data character "{char}" for enabled clock channel')

    def CheckLiteral(self, expr):
        if expr[:2] not in ['0v', '0j']:
            raise TypeError(f'Invalid vector type {expr[:2]}')
        if len(expr[2:]) != 56:
            raise ValueError(
                f'Expecting exactly 56 characters after \'{expr[:2]}\' but {len(expr) - 2} characters found')

    # This method resolves simple expressions, symbols, and literals
    # Supported proto-functions / expressions:
    #   - eval[evalExpr] or `evalExpr`
    #   - LABEL
    #   - startaddr
    #   - vecaddr
    def Resolve(self, expr, secondPass=True):
        if isinstance(expr, int) or isinstance(expr, float):
            return expr
        elif isinstance(expr, str):
            try:
                return self.ResolveLiteral(expr)
            except TypeError:
                pass
            if expr.startswith('{') and expr.endswith('}'):
                return fvalc.RandomVar.Compile(expr)
            elif (expr.startswith('eval[') and expr.endswith(']')) or (expr.startswith('`') and expr.endswith('`')):
                evalExpr = None
                if expr.startswith('eval['):
                    evalExpr = expr[5:-1]
                else:
                    evalExpr = expr[1:-1]
                # Build dictionary of symbols that can be used in eval
                self.evalSymbols['startaddr'] = self.Resolve('startaddr')
                if secondPass:
                    if hasattr(self, 'curaddr'):
                        self.evalSymbols['vecaddr'] = self.curaddr
                else:
                    self.evalSymbols['vecaddr'] = self.sizeStack[-1]
                result = eval(evalExpr, self.evalSymbols)
                return result
            elif expr == 'startaddr':
                return self.startaddr
            elif expr == 'vecaddr':
                if secondPass:
                    return self.curaddr
                else:
                    return self.sizeStack[-1]
            elif expr in self.vars:
                return self.Resolve(self.vars[expr], secondPass)
            elif expr in self.symbols:
                return self.Resolve(self.symbols[expr], secondPass)
            elif expr in self.labels:
                return self.Resolve(self.labels[expr], secondPass)
            else:
                # Let the _num() function take care of it. Supported formats:
                #   - Decimal (e.g. 1, 234)
                #   - Binary (e.g. 0b001, 0b000101010100111100)
                #   - Hexadecimal (e.g. 0xAB6969)
                #   - Float numbers
                return PatternAssembler._num(expr)
        elif hasattr(expr, '__call__'):
            return self.Resolve(expr(), secondPass)
        else:
            raise Exception('Invalid expression \'{}\''.format(expr))

    def Generate(self):
        ### FIXME: This is a workaround to make the DmaWrite API work properly!
        ALIGNMENT = 16
        vectorCount = self.sizeStack[-1]
        remainder = vectorCount % ALIGNMENT
        padding = None
        if remainder == 0:
            padding = 0
        else:
            padding = ALIGNMENT - remainder
        ###
        patternSize = (vectorCount + padding) * self._ENCODED_WORD_SIZE
        data = bytearray(patternSize)
        self.curaddr = self.startaddr
        vectors = 0
        for assemblerObject in self.objects:
            count = self._Assemble(data, self._ENCODED_WORD_SIZE * vectors, assemblerObject)
            self.curaddr = self.curaddr + count
            vectors = vectors + count
        del self.curaddr
        self.cachedObj = bytes(data)
        return self.cachedObj


# Load standard symbols
constants, functions = symbols.Load()
standardSymbols = {}
standardSymbols.update(constants)
standardSymbols.update(functions)
PatternAssembler.classSymbols = standardSymbols

# Load standard macros
macro_file = os.path.join(os.path.dirname(__file__), 'macros.rpat')
PatternAssembler.classMacros = fval.asm.LoadMacros(macro_file, PatternAssembler)

if __name__ == "__main__":
    # Parse command line options
    import argparse

    parser = argparse.ArgumentParser(description='HPCC AC single slice pattern (.obj) assembler')
    parser.add_argument('infile', help='Pattern source code input filename', type=str)
    parser.add_argument('outfile', help='Pattern .obj output filename', type=str)
    parser.add_argument('-s', '--symbols', help='Create pattern symbols', action="store_true")
    args = parser.parse_args()

    pattern = PatternAssembler()
    pattern.Load(args.infile)
    pattern.SaveObj(args.outfile)
    if args.symbols:
        pattern.SaveSymbols(args.outfile + '.symbols')

