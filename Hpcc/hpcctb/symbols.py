################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: symbols.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC AC constants and pattern assembler symbols
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/29/15
#       Group: HDMT FPGA Validation
################################################################################

import sys

# Enums!

# Convention: len(class.name) <= 8 and len(field.name) <= 10

class PATTERN:
    VECTOR_WORD      = 0b00
    METADATA_WORD    = 0b01
    PINSTATE_WORD    = 0b10
    INSTRUCTION_WORD = 0b11

class STYPE:
    IOSTATEJAM      = 0b000
    MASK            = 0b001
    UNMASK          = 0b010
    MASK_APPLY      = 0b011
    SCC             = 0b100
    FIXWORD_Repeat  = 0b101


class OPTYPE:
    EXT        = 0b0001
    ALU        = 0b0010
    REGISTER   = 0b0100
    VECTOR     = 0b1000
    BRANCH     = 0b0000

class EXT:
    TRIGGER    = 0b001
    OTHER      = 0b010
    TRIGGERNOW = 0b100

class EXTOTHER:
    CD_LOAD    = 0b0010
    CD_STORE   = 0b0011

class EXTTRIG:
    TRGEXT_I   = 0b0001
    TRGDOM_I   = 0b0100
    TRGEXT_RA  = 0b1001
    TRGDOM_RA  = 0b1100

class ALUDEST:
    NONE       = 0b00
    REG        = 0b01
    FLAGS      = 0b10

class ALUSRC:
    RA_I       = 0b00
    RA_RB      = 0b01
    FLAGS_I    = 0b10
    FLAGS_RB   = 0b11

class ALUOP:
    XOR        = 0b000
    AND        = 0b001
    OR         = 0b010
    LSHL       = 0b011
    LSHR       = 0b100
    ADD        = 0b101
    SUB        = 0b110
    MUL        = 0b111

class REGDEST:
    REG        = 0b0001
    CFB        = 0b0010
    FLAGS      = 0b0100
    STACK      = 0b1000

class REGSRC:
    IMM        = 0b000
    REG        = 0b001
    TCC        = 0b010
    VECADDR    = 0b011
    CFB        = 0b100
    FLAGS      = 0b101
    DIRTYMASK  = 0b110
    STACK      = 0b111

class VPTYPE:
    VPLOG      = 0b001
    VPLOCAL    = 0b010
    VPOTHER    = 0b100

class VPLOG:
    LOG1_I     = 0b0001
    LOG2_I     = 0b0010
    PATID_I    = 0b0100
    LOG1_RA    = 0b1001
    LOG2_RA    = 0b1010
    PATID_RA   = 0b1100

class VPLOCAL:
    REPEAT_I   = 0b0001
    END_I      = 0b0010
    FAILBR_I   = 0b0100
    REPEAT_RA  = 0b1001
    END_RA     = 0b1010
    FAILBR_RA  = 0b1100

class VPOTHER:
    TRIGGERSIB  = 0b0001
    BLKFAIL_I   = 0b0010
    DRAINFIFO_I = 0b0011
    EDGECNTR_I  = 0b0100
    CLRSTICKY   = 0b0101
    CAPTURE_I   = 0b1000
    RSTCYCLCNT  = 0b1100
    RSTCAPMEM   = 0b1010
    RSTFAILCNT  = 0b1001
    DRAINFIFO_R = 0b1011

class BRANCH:
    NONE       = 0b00000
    GOTO_I     = 0b00001
    CALL_I     = 0b00010
    PCALL_I    = 0b00100
    RET        = 0b01000
    GOTO_R     = 0b10001
    CALL_R     = 0b10010
    PCALL_R    = 0b10100
    CLRSTACK   = 0b11000

class BRBASE:
    GLOBAL     = 0b00
    CFB        = 0b01
    RELATIVE   = 0b10
    PFB        = 0b11

class COND:
    NONE       = 0b0000
    LSE        = 0b0001
    DSE        = 0b0010
    GSE        = 0b0011
    WIREDOR    = 0b0100
    LOADACTIVE = 0b0101
    SWTRIGRCVD = 0b0110
    DMTRIGRCVD = 0b0111
    RCTRIGRCVD = 0b1000
    ZERO       = 0b1001
    CARRY      = 0b1010
    OVERFLOW   = 0b1011
    SIGN       = 0b1100
    BE         = 0b1101
    LTE        = 0b1110
    GTE        = 0b1111

class FLAGS:
    ALWAYSSET_BIT   = 16
    LSE_BIT         = 17
    DSE_BIT         = 18
    GSE_BIT         = 19
    WIREDOR_BIT     = 20
    LOADACTIVE_BIT  = 21
    SWTRIG_BIT      = 22
    DMTRIG_BIT      = 23
    RCTRIG_BIT      = 24
    ALUZERO_BIT     = 25
    ALUCARRY_BIT    = 26
    ALUOVERFLOW_BIT = 27
    ALUSIGN_BIT     = 28

class TRIGGER:
    PATTERN_RESUME                      = 0
    PATTERN_RESUME_PROPAGATE            = 1
    DOMAIN_                             = 2
    DOMAIN_PROPAGATE                    = 3
    STICKY_ERROR                        = 4
    GLOBAL_STICKY_ERROR_PROPAGATE       = 5
    CLEAR_DOMAIN_STICKY_ERROR_PROPAGATE = 6
    CLEAR_GLOBAL_STICKY_ERROR           = 7
    CLEAR_GLOBAL_STICKY_ERROR_PROPAGATE = 8
    CENTRAL_REGISTER_LOAD_ACTIVE        = 9
    CLEAR_CENTRAL_REGISTER_LOAD_ACTIVE  = 10
    RC_                                 = 11
    RC_PROPAGATE                        = 12
    PATTERN_STOP                        = 13

def Load():
    allConstants = {} # dict of all constants
    names = set()   # set of all constant names
    conflicts = set()
    for className, cls in vars(sys.modules[__name__]).items():
        if not className.startswith('_') and className.upper() == className:
            for name, value in _GetClassConstants(cls).items():
                if name in names:
                    conflicts.add(name)
                else:
                    names.add(name)
                allConstants[(className, name)] = value
    constants = {}   # list of output constants
    for key, value in allConstants.items():
        className, name = key
        # Always add "full form" of the constant
        constants['{}.{}'.format(className, name)] = value
        if name not in conflicts:
            # Convenient "short form" (not supported for conficts)
            constants[name] = value
    return constants, {}

def _GetClassConstants(cls):
    s = {}
    for var, value in vars(cls).items():
        if not var.startswith('_'):
            s[var] = value
    return s

if __name__ == "__main__":
    # Parse command line options
    import os
    import argparse
    import datetime
    import getpass
    from string import Template
    parser = argparse.ArgumentParser(description = 'C++ symbols header file generator')
    parser.add_argument('outfile', help = 'Output filename', type = str)
    args = parser.parse_args()

    filename = os.path.basename(args.outfile)
    split = os.path.splitext(filename)

    template = {
        'USER'     : getpass.getuser(),
        'DATE'     : datetime.datetime.now().strftime("%m/%d/%Y"),
        'FILENAME' : filename,
        'BASENAME' : split[0].upper(),
        'FILEEXT'  : split[1].upper().replace('.', ''),
    }

    fout = open(args.outfile, 'w')
    # Print header
    fout.write(Template("""\
////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: ${FILENAME} (auto-generated file, see symbols.py)
//------------------------------------------------------------------------------
//    Purpose: HPCC testbench symbols
//------------------------------------------------------------------------------
// Created by: ${USER}
//       Date: ${DATE}
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////
#ifndef __${BASENAME}_${FILEEXT}__
#define __${BASENAME}_${FILEEXT}__

#include <cstdint>

namespace hpcctbc {

""").substitute(template))

    constants, functions = Load()
    for name, value in constants.items():
        fout.write('const uint64_t {} = 0x{:x};\n'.format(name.replace('.', '_'), value))

    # Print footer
    fout.write(Template("""\

}  // namespace hpcctbc

#endif  // __${BASENAME}_H__
""").substitute(template))

    fout.close()

