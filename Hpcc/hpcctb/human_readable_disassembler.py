################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: disassembler.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC AC single slice pattern (.obj) disassembler
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/24/15
#       Group: HDMT FPGA Validation
################################################################################

import argparse
import os

if __name__ == '__main__':
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
from Hpcc.hpcctb import structs as pstructs


def pattern_file_reader(filename):
    '''generator for reading vectors from a file'''
    with open(filename, 'rb') as f:
        while True:
            raw_vector = f.read(16)
            if not raw_vector:
                break
            
            vector = pstructs.PatternWord(raw_vector)
            if vector.type == pstructs.PIN_DATA_WORD:
                yield pstructs.PinDataWord.from_buffer(vector)
            elif vector.type == pstructs.PIN_STATE_WORD:
                yield pstructs.PinStateWord.from_buffer(vector)
            elif vector.type == pstructs.INSTRUCTION_WORD:
                yield pstructs.Instruction.from_buffer(vector)
            else:
                yield pstructs.MetadataWord.from_buffer(vector)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('objfile', help='Pattern filename', type=str)
    args = parser.parse_args()

    for i, vector in enumerate(pattern_file_reader(args.objfile)):
        print('{:08X} {}'.format(i, vector))


