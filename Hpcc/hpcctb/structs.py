################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes
import types


PIN_DATA_WORD = 0
METADATA_WORD = 1
PIN_STATE_WORD = 2
INSTRUCTION_WORD = 3


class HpccStruct(ctypes.LittleEndianStructure):
    """Structure base class - defines setter/getter for bytes value of register"""
    def __init__(self, value=None):
        super().__init__()
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return bytes(self)

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i, len(i))

    @classmethod
    def from_bytes(cls, data):
        return cls(value=int.from_bytes(data, byteorder="little"))


class PatternWord(HpccStruct):
    _fields_ = [('bits31_0', ctypes.c_uint, 32),
                ('bits63_32', ctypes.c_uint, 32),
                ('bits95_64', ctypes.c_uint, 32),
                ('bits125_96', ctypes.c_uint, 30),
                ('type', ctypes.c_uint, 2)]
    
    def __str__(self):
        return 'type={} {:08X}{:08X}{:08X}{:08X}'.format(self.type,
                                                         self.bits125_96,
                                                         self.bits95_64,
                                                         self.bits63_32,
                                                         self.bits31_0)


class MetadataWord(HpccStruct):
    _fields_ = [('bits31_0', ctypes.c_uint, 32),
                ('bits63_32', ctypes.c_uint, 32),
                ('bits95_64', ctypes.c_uint, 32),
                ('bits125_96', ctypes.c_uint, 30),
                ('type', ctypes.c_uint, 2)]
    
    def __str__(self):
        return 'Metadata: {:08X}{:08X}{:08X}{:08X}'.format(self.type,
                                                         self.bits125_96,
                                                         self.bits95_64,
                                                         self.bits63_32,
                                                         self.bits31_0)


class PinDataWord(HpccStruct):
    _fields_ = [('data', ctypes.c_uint64, 64),
                ('data2', ctypes.c_uint64, 48),
                ('repeat', ctypes.c_uint64, 5),
                ('mtv', ctypes.c_uint64, 3),
                ('ctv', ctypes.c_uint64, 1),
                ('link', ctypes.c_uint64, 1),
                ('sce', ctypes.c_uint64, 1),  # Serial Capture Enable
                ('nprpt', ctypes.c_uint64, 1),
                ('sblink', ctypes.c_uint64, 1),   # Symbol Link Enable
                ('bcast', ctypes.c_uint64, 1),    # Broadcast Enable
                ('type', ctypes.c_uint64, 2)]

    def __str__(self):
        l = []
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)

class PinStateWord(HpccStruct):
    _fields_ = [('pin_data', ctypes.c_uint8 * 14),
                ('pin_state_type', ctypes.c_uint16, 3),
                ('reserved', ctypes.c_uint16, 11),
                ('type', ctypes.c_uint16, 2)]

    def __str__(self):
        if self.pin_state_type == 0:
            return 'IO Jam:   ' + ''.join(['{:02X}'.format(x) for x in reversed(self.pin_data[:])])
        elif self.pin_state_type == 1:
            return 'Mask:     ' + ''.join(['{:02X}'.format(x) for x in reversed(self.pin_data[:7])])
        elif self.pin_state_type == 2:
            return 'Unmask:   ' + ''.join(['{:02X}'.format(x) for x in reversed(self.pin_data[:7])])
        elif self.pin_state_type == 3:
            return 'Apply:   ' + ''.join(
                ['{:02X}'.format(x) for x in reversed(self.pin_data[:7])])  # Need be checked

class SCCPinStateWord(HpccStruct):
    _fields_ = [('serial_channel', ctypes.c_uint64, 3),
                ('record_length', ctypes.c_uint64, 4),
                ('virtual_pin_id', ctypes.c_uint64, 10),
                ('reserved', ctypes.c_uint64, 47),
                ('reserved2', ctypes.c_uint64, 47),
                ('lsb_shift', ctypes.c_uint64, 1),
                ('pin_state_type', ctypes.c_uint64, 3),
                ('undefined', ctypes.c_uint64, 11),
                ('type', ctypes.c_uint64, 2)]

    def __str__(self):
        l = []
        if self.lsb_shift == 1:
            l.append('Serial Capture Control: lsb_shift = LSb')
        else:
            l.append('Serial Capture Control: lsb_shift = MSb')
        l.append('virtual_pin_id = {:04X}'.format(self.virtual_pin_id))
        l.append('record_length = {:02X}'.format(self.record_length))
        l.append('serial_channel = {:01X}'.format(self.serial_channel))
        return ' '.join(l)

BRANCH_TYPE_STRING_LOOKUP = {
    0x01: 'JUMP',
    0x02: 'CALL',
    0x04: 'PCALL',
    0x08: 'RETURN',
    0x11: 'JUMP',
    0x12: 'CALL',
    0x14: 'PCALL',
    0x18: 'CLEAR_STACK'}

BRANCH_BASE_STRING_LOOKUP = ['GLOBAL', 'CFB', 'RELATIVE', 'PFB']
CONDITIONAL_STRING_LOOKUP = ['NONE', 'LSE', 'DSE', 'GSE',
                             'WIRED_OR', 'LOAD_ACTIVE', 'SW_TRIGGER', 'DOMAIN_TRIGGER',
                             'RC_TRIGGER', 'ZERO', 'CARRY', 'OVERFLOW',
                             'SIGN', 'BELOW_OR_EQUAL', 'LESS_THAN_OR_EQUAL', 'GREATER_THAN_OR_EQUAL']
MOVE_SOURCE_STRING_LOOKUP = ['IMMEDIATE', 'A', 'TCC', 'VECTOR_ADDRESS', 'CFB', 'FLAGS', 'DIRTY_MASK', 'STACK']
MOVE_DESTINATION_STRING_LOOKUP = {
    0x1: 'REGISTER',
    0x2: 'CFB',
    0x4: 'FLAGS',
    0x8: 'STACK'}

ALU_OPERATION_STRING_LOOKUP = ['XOR', 'AND', 'OR', 'LSHL', 'LSHR', 'ADD', 'SUBTRACT', 'MULTIPLY']

EXTERNAL_OPERATION_STRING_LOOKUP = {
    0x11: 'EXTERNAL TRIGGER',
    0x14: 'DOMAIN TRIGGER',
    0x19: 'EXTERNAL TRIGGER',
    0x1C: 'DOMAIN TRIGGER',
    0x22: 'LOAD FROM CENTRAL DOMAIN',
    0x23: 'STORE TO CENTRAL DOMAIN',
    0x41: 'EXTERNAL TRIGGER NOW',
    0x44: 'DOMAIN TRIGGER NOW',
    0x49: 'EXTERNAL TRIGGER NOW',
    0x4C: 'DOMAIN TRIGGER NOW'}
    
VECTOR_OPERATION_STRING_LOOKUP = {
    0x11: 'LOG1',
    0x12: 'LOG2',
    0x14: 'PATTERN_ID',
    0x19: 'LOG1',
    0x1A: 'LOG2',
    0x1C: 'PATTERN_ID',
    0x21: 'REPEAT',
    0x22: 'END',
    0x24: 'FAIL_BRANCH',
    0x29: 'REPEAT',
    0x2A: 'END',
    0x2C: 'FAIL_BRANCH',
    0x41: 'TRIGGER_SNAP_IN',
    0x42: 'BLOCK_FAILS',
    0x43: 'DRAIN_PIN_FIFO',
    0x44: 'EDGE_COUNTER',
    0x45: 'CLEAR_STICKY_ERRORS',
    0x48: 'CAPTURE',
    0x49: 'RESET_FAIL_COUNTS',
    0x4A: 'RESET_CAPTURE_MEMORY',
    0x4C: 'RESET_CYCLE_COUNT'}


class Instruction(HpccStruct):
    _pack_ = 1
    _fields_ = [('immediate', ctypes.c_uint64, 32),
                ('a', ctypes.c_uint64, 5),
                ('b', ctypes.c_uint64, 5),
                ('destination', ctypes.c_uint64, 5),
                ('branch_type', ctypes.c_uint64, 5),
                ('branch_base', ctypes.c_uint64, 2),
                ('condition_type', ctypes.c_uint64, 4),
                ('invert_condition', ctypes.c_uint64, 1),
                # op code has to be split because it crosses a 64-bit boundary
                ('op_code_4_0', ctypes.c_uint64, 5),
                ('op_code_6_5', ctypes.c_uint64, 2),
                ('op_type', ctypes.c_uint64, 4),
                ('reserved', ctypes.c_uint64, 40),
                ('reserved2', ctypes.c_uint64, 16),
                ('type', ctypes.c_uint64, 2)]
                
    def __str__(self):
        f = []
        if self.op_type:
            op_code = self.op_code_4_0 + (self.op_code_6_5 << 5)
            if self.op_type == 0x01:
                f.append(EXTERNAL_OPERATION_STRING_LOOKUP.get(self.op_type, 'INVALID EXTERNAL OPERATION'))
                if self.op_type in [0x11, 0x14, 0x41, 0x44]:
                    f.append('0x{:08X}'.format(self.immediate))
                else:
                    f.append('0x{:08X}'.format(self.a))
            elif self.op_type == 0x02:
                f.append(ALU_OPERATION_STRING_LOOKUP[op_code & 0x7])
                
                source = (op_code >> 3) & 0x3
                if source == 0:
                    f.append('REGISTER_{:02X}, 0x{:08X}'.format(self.a, self.immediate))
                elif source == 1:
                    f.append('REGISTER_{:02X}, REGISTER_{:02X}'.format(self.a, self.b))
                elif source == 2:
                    f.append('FLAGS, 0x{:08X}'.format(self.immediate))
                elif source == 3:
                    f.append('FLAGS, REGISTER_{:02X}'.format(self.b))
                
                destination = op_code >> 5
                f.append('STORE IN')
                if destination == 0:
                    f.append('NONE')
                elif destination == 1:
                    f.append('REGISTER_{:02X}'.format(self.destination))
                elif destination == 2:
                    f.append('FLAGS')
            elif self.op_type == 0x4:
                f.append('MOVE')
                if op_code & 0x7 == 0b000:
                    f.append('0x{:08X}'.format(self.immediate))
                elif op_code & 0x7 == 0b001:
                    f.append('REGISTER_{:02X}'.format(self.a))
                else:
                    f.append('{}'.format(MOVE_SOURCE_STRING_LOOKUP[op_code & 0x7]))
                f.append('TO')
                
                destination = op_code >> 3
                if destination == 1:
                    f.append('REGISTER_{:02X}'.format(self.destination))
                else:
                    f.append(MOVE_DESTINATION_STRING_LOOKUP.get(op_code >> 3, 'INVALID_DESTINATION'))
            elif self.op_type == 0x8:
                f.append(VECTOR_OPERATION_STRING_LOOKUP.get(op_code, 'INVALID_VECTOR_OPERATION'))
                if op_code in [0x11, 0x12, 0x14, 0x21, 0x22, 0x24, 0x42, 0x44, 0x48]:
                    f.append('0x{:08X}'.format(self.immediate))
                elif op_code in [0x19, 0x1A, 0x1C, 0x29, 0x2A, 0x2C]:
                    f.append('REGISTER_{:02X}'.format(self.a))
            else:
                f.append('INVALID_OPERATION_TYPE')
            # f.append('b=0x{:02X}'.format(self.b))
                
        if self.branch_type:
            f.append(BRANCH_TYPE_STRING_LOOKUP.get(self.branch_type, 'INVALID_BRANCH_TYPE'))
            if self.invert_condition:
                f.append('INVERT')
            if self.condition_type:
                f.append('IF')
                f.append(CONDITIONAL_STRING_LOOKUP[self.condition_type])
            if self.branch_type in [0x11, 0x12, 0x14]:
                f.append('TO ADDRESS IN')
                f.append(BRANCH_BASE_STRING_LOOKUP[self.branch_base])
                f.append('REGISTER_{:02X}'.format(self.destination))
            elif self.branch_type in [0x01, 0x02, 0x04]:
                f.append('TO')
                f.append(BRANCH_BASE_STRING_LOOKUP[self.branch_base])
                f.append('0x{:08X}'.format(self.immediate))
        return ' '.join(f)


if __name__ == '__main__':
    psw = PinStateWord(b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F')
    print(psw)

    
    
    
    
    
    