# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

'''HPCC Credit Counter Factory

Maintains Python 2 and Python 3 compatibility.
'''


def new_credit_counter(tos_revision, period, threshold=0, initial_balance=0):
    '''Factory method to get a CreditCounter instance'''
    return _REVISION_TABLE[tos_revision](period, threshold, initial_balance)


def _new_legacy_credit_counter(period, threshold, initial_balance):
    from .creditcounter_legacy import CreditCounter
    return CreditCounter(period, threshold, initial_balance)

def _new_tos_3_4_credit_counter(period, threshold, initial_balance):
    from .creditcounter_34 import CreditCounter
    return CreditCounter(period, threshold, initial_balance)

def _new_tos_3_6_credit_counter(period, threshold, initial_balance):
    from .creditcounter_36 import CreditCounter
    return CreditCounter(period, threshold, initial_balance)


_REVISION_TABLE = {
    'Legacy': _new_legacy_credit_counter,
    'TOS_3.4': _new_tos_3_4_credit_counter,
    'TOS_3.6': _new_tos_3_6_credit_counter}
    
if __name__ == '__main__':
    help(__name__)
    print('Valid tos_revision values are:\n    {}'.format('\n    '.join(_REVISION_TABLE.keys())))
    