################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: simgen.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC AC sim test generator
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 08/31/15
#       Group: HDMT FPGA Validation
###############################################################################

import csv
import datetime
import getpass
from string import Template

SIM_TEMPLATE = """\
////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: ${TEST_NAME}.sv (auto-generated)
//------------------------------------------------------------------------------
// Created by: ${USER}
//       Date: ${DATE}
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

`START_OF_TEST(${TEST_NAME}_test)
    logic [127:0] pattern[];
    int fin;
    
    // Create FPGA interface
    fpga_package::Fpga fpga = new(tb.fpga_interface, tb.axi_driver);
    fpga.wait_for_initialization();

    // Write registers
${REGISTER_BLOCK}
    // Set period
    tb.AD9914.set_period(${PERIOD});
    #100ns;
    // tb.AD9914.set_period(625.7ps);
    // #100ns;

    // Read pattern
    fin = $$fopen("${OBJ_FILENAME}", "rb");
    $$fread(pattern, fin);
    // Load pattern
    tb.dram.ddr_controller.load_vectors(${OBJ_ADDRESS}, pattern);
    // Execute pattern
    tb.start_pattern(${START_ADDRESS});
    // Wait for pattern to complete
    tb.wait_for_capture_complete();
`END_OF_TEST
"""

def CreateTest(testName, csvFilename, period, objFilename, objAddress = 0, startAddress = 0):

    registerBlock = ''

    fin = open(csvFilename, 'r')
    for row in csv.reader(fin):
        if len(row) == 3:
            offset = row[0]
            value = row[1]
            details = row[2]
            registerBlock = registerBlock + '    tb.write_register({}, {})  // {} \n'.format(offset, value, details)
        else:
            raise Exception('Invalid number of columns. Expecting 3 columns: Offset,Value,Details')

    template = {
        'TEST_NAME'      : testName,
        'USER'           : getpass.getuser(),
        'DATE'           : datetime.datetime.now().strftime("%m/%d/%Y"),
        'REGISTER_BLOCK' : registerBlock,
        'PERIOD'         : period,
        'OBJ_FILENAME'   : objFilename,
        'OBJ_ADDRESS'    : objAddress,
        'START_ADDRESS'  : startAddress,
    }

    fout = open('{}.sv'.format(testName), 'w')
    fout.write(Template(SIM_TEMPLATE).substitute(template))
    fout.close()

if __name__ == "__main__":
    # Parse command line options
    import argparse
    parser = argparse.ArgumentParser(description = 'HPCC AC sim test generator')
    parser.add_argument('regs', help = 'Register snapshot filename (in csv format)', type = str)
    parser.add_argument('period', help = 'Test period', type = str)
    parser.add_argument('outfile', help = 'Output test filename', type = str)
    args = parser.parse_args()

    CreateTest(args.outfile, args.regs, args.period, '{}.obj'.format(args.outfile))

