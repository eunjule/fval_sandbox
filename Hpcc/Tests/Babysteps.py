################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Babysteps.py
#-------------------------------------------------------------------------------
#     Purpose: 
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 11/25/15
#       Group: HDMT FPGA Validation
################################################################################

import random

from _build.bin import hpcctbc
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest

PICO_SECONDS_PER_SECOND = 1000000000000

# Create the patterns
asm = PatternAssembler()
asm.LoadString("""\
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
    BlockFails 1
    Drive0CompareLVectors length=1024
    BlockFails 0
    %repeat 256
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
    %end
    V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
    %repeat 256
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
    %end
    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
""")
EVEN_PATTERN = asm.Generate()

asm = PatternAssembler()
asm.LoadString("""\
    S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
    BlockFails 1
    Drive0CompareLVectors length=1024
    BlockFails 0
    %repeat 256
    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
    %end
    V link=0, ctv=1, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
    %repeat 256
    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
    %end
    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
    S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
""")
ODD_PATTERN = asm.Generate()


class NoCalibration(HpccTest):

    def MiniSimplePatternAtMinQdrTest(self):
        self.RunScenario(0.00)

    def MiniSimplePatternAt05PercentMinMaxQdrTest(self):
        self.RunScenario(0.05)

    def MiniSimplePatternAt10PercentMinMaxQdrTest(self):
        self.RunScenario(0.10)

    def MiniSimplePatternAt25PercentMinMaxQdrTest(self):
        self.RunScenario(0.25)

    def MiniSimplePatternAt50PercentMinMaxQdrTest(self):
        self.RunScenario(0.50)

    def MiniSimplePatternAt75PercentMinMaxQdrTest(self):
        self.RunScenario(0.75)

    def MiniSimplePatternAt90PercentMinMaxQdrTest(self):
        self.RunScenario(0.90)

    def MiniSimplePatternAt95PercentMinMaxQdrTest(self):
        self.RunScenario(0.95)

    def MiniSimplePatternAtMaxQdrTest(self):
        self.RunScenario(1.00)

    def RunPattern(self, slot, slice, pattern, alpha):
        scoreboard = {}
        hpcc = self.env.instruments[slot]
        if hpcc.ac[slice].minQdrMode < 0:
            hpcc.ac[slice]._UpdateFpgaCapabilities()
        period = ((1.0 - alpha) * hpcc.ac[slice].minQdrMode + alpha * hpcc.ac[slice].maxQdrMode) / PICO_SECONDS_PER_SECOND
        self.env.RunPattern(slot, slice, pattern, period = period, captureFails = True, captureCTVs = True, captureAll = False)

        captureData, blockCount, captureCount = hpcc.ReadCapture(slice)
        entries = hpcctbc.CaptureOffsetEntry_vector()
        hpcctbc.CaptureOffsets(captureData, blockCount, entries)
        for entry in entries:
            if entry.channel in scoreboard:
                scoreboard[entry.channel].append(entry.offset)
            else:
                scoreboard[entry.channel] = [entry.offset]
        return scoreboard

    def RunScenario(self, alpha):
        self.env.SetConfig('EvenToOddLoopback', loadCal = False)
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            patternstart = hpcc.ac[slice].Read('PatternControl').VectorProcessorStarted
            dumpCapture = False
            scoreboard = self.RunPattern(slot, slice, EVEN_PATTERN, alpha)
            if patternstart == 0:
                self.Log('error', 'pattern not executed')
            for ch, captures in scoreboard.items():
                if not(self._IsConsecutive(captures)):
                    self.Log('error', 'Non-consecutive captures for channel {}'.format(ch))
                    dumpCapture = True
            if dumpCapture:
                hpcc.DumpCapture(slice)
            dumpCapture = False
            scoreboard = self.RunPattern(slot, slice, ODD_PATTERN, alpha)
            if patternstart == 0:
                self.Log('error', 'pattern not executed')
            for ch, captures in scoreboard.items():
                if not(self._IsConsecutive(captures)):
                    self.Log('error', 'Non-consecutive captures for channel {}'.format(ch))
                    dumpCapture = True
            if dumpCapture:
                hpcc.DumpCapture(slice)

    @staticmethod
    def _IsConsecutive(L):
        if len(L) <= 1:
            return True
        else:
            last = L[0]
            for i, item in enumerate(L):
                if i > 1:
                    if last + 1 != item:
                        return False
                last = item
            return True
