################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Babysteps.py
#-------------------------------------------------------------------------------
#     Purpose: 
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 11/25/15
#       Group: HDMT FPGA Validation
################################################################################

import os

from Common.fval import skip
from Hpcc.Tests.hpcctest import HpccTest
from Hpcc.hpcctb.assembler import PatternAssembler
import Hpcc.instrument.hpccAcRegs as ac_registers


class Ddr(HpccTest):

    def RandomDdrDimmFailChannelDetectTest(self):
        for ac in [self.env.instruments[slot].ac[slice] for slot, slice in self.env.fpgas]:
            self.env.LogPerSlice(ac.slot, ac.slice)
            chunk_size = 128
            self.Log('info', 'chunk_size {} '.format(chunk_size))
            for address in range(0, 2048 * 2, chunk_size):
                write_buffer = os.urandom(chunk_size)
                self.Log('info', 'Writing {} bytes to offset {}'.format(chunk_size, address))
                ac.DmaWrite(address, write_buffer)
                for same_address_ten_times in range(10):
                    read_buffer = ac.DmaRead(address, len(write_buffer))
                    self.Log('info', 'Reading {} bytes to offset {} for times :{} '.format(chunk_size, address,
                                                                                           same_address_ten_times))
                    if write_buffer != read_buffer:
                        int_write_buffer = int.from_bytes(write_buffer, byteorder='big')
                        int_read_buffer = int.from_bytes(read_buffer, byteorder='big')
                        self.Log('error', 'DMA block compare failed')
                        self.Log('error', 'Data written     {:x}'.format(int_write_buffer))
                        self.Log('error', 'Data readback    {:x}'.format(int_read_buffer))

    def RandomAcDdrTest(self):
        for ac in [self.env.instruments[slot].ac[slice] for slot, slice in self.env.fpgas]:
            self.env.LogPerSlice(ac.slot, ac.slice)
            chunk_size = ac.ddr_memory_size // 128
            for address in range(0, ac.ddr_memory_size, chunk_size):
                write_buffer = os.urandom(chunk_size)
                self.Log('debug', 'Writing {} bytes to offset {:X}'.format(chunk_size, address))
                ac.DmaWrite(address, write_buffer)
                self.Log('debug', 'Reading {} bytes to offset {:X}'.format(chunk_size, address))
                read_buffer = ac.DmaRead(address, chunk_size)
                if write_buffer != read_buffer:
                    ac.log_ecc_errors()
                    self.Log('error', 'DMA block compare failed')
            if ac.get_alarms()['ECCError']:
                ac.log_ecc_errors()
                for i in range(ac_registers.CorrectableECCCount.REGCOUNT):
                    count = ac.read_register(ac_registers.CorrectableECCCount, index=i).Count
                    if count > 1:
                        log_level = 'error'
                    else:
                        log_level = 'info'
                    self.Log(log_level, 'Correctable ECC Error detected. Count {}   = {:13d}'.format(i, count))
                for i in range(ac_registers.UncorrectableECCCount.REGCOUNT):
                    count = ac.read_register(ac_registers.UncorrectableECCCount, index=i).Count
                    if count > 0:
                        log_level = 'error'
                    else:
                        log_level = 'info'
                    self.Log(log_level, 'Uncorrectable ECC Error Detected. Count {} = {:13d}'.format(i, count))

    def DirectedAcDdrTest(self):
        pattern = self._create_infinite_pattern()
        chunk_size = 64 * 1024
        write_buffer = os.urandom(chunk_size)
        self.env.SetConfig('InternalLoopback')
        for ac in [self.env.instruments[slot].ac[slice] for slot, slice in self.env.fpgas]:
            blocklocation = ac.ddr_memory_size - (chunk_size * 2)
            self.env.LogPerSlice(ac.slot, ac.slice)
            self.Log('debug', 'Writing {} bytes to offset {:X}'.format(chunk_size, blocklocation))
            ac.DmaWrite(blocklocation, write_buffer)
            self.env.RunPattern(ac.slot, ac.slice, pattern, period=5e-09, captureAll=False, captureFails=False,
                                captureCTVs=False, waitForComplete=False)
            for attemptloop in range(0,1000):
                if ac.IsPatternComplete():
                    break
                self.Log('info', 'Reading {} bytes from offset {:X}'.format(chunk_size, blocklocation))
                read_buffer = ac.DmaRead(blocklocation, chunk_size)
                if write_buffer != read_buffer:
                    self.Log('error', 'DMA block compare failed.')
                    break
                else:
                    self.Log('info','DMA read successful for count: {}'.format(attemptloop))
            ac.AbortPattern()

    def _create_infinite_pattern(self):
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

            %repeat 10000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            Goto PATTERN_START
        """)
        return pattern.Generate()

    @skip('Not implemented')
    def DirectedDcDdrTest(self):
        """Write/Read DC FPGA DDR Memory"""
