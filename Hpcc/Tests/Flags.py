################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Flags.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for the flags register
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 04/01/15
#       Group: HDMT FPGA Validation
################################################################################

from collections import namedtuple
import copy
import random
import sys
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers

class Alu(HpccTest):
    def DirectedSetCarryTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the carry flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 1:
                self.Log('error', 'ALU carry bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedClearCarryTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the carry flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 0:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedSetOverflowTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the overflow flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the overflow flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUOverflow != 1:
                self.Log('error', 'ALU overflow bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedClearOverflowTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the overflow flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the overflow flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUOverflow != 0:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedSetZeroTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x00000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x00000000
            # r1 = r0 + 0x00000000 (this should set the zero flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the zero flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUZero != 1:
                self.Log('error', 'ALU zero bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags

    def DirectedClearZeroTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x00000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x00000000
            # r1 = r0 + 0x00000000 (this should set the zero flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the zero flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUZero != 0:
                self.Log('error', 'ALU zero bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedSetSignTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000000
            # r1 = r0 + 0x00000000 (this should set the sign flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the sign flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUSign != 1:
                self.Log('error', 'ALU sign bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedClearSignTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000000
            # r1 = r0 + 0x00000000 (this should set the sign flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the sign flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUSign != 0:
                self.Log('error', 'ALU sign bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
            
    ############################################################
    ############### Carry vs Overflow Test #####################
    ############################################################
    def DirectedAddCarryOffOverflowOffTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("ADD", 0x2FFFFFFF, 0x2FFFFFFF) # = 0x4FFFFFFF
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 0:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 0:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
            
    def DirectedAddCarryOnOverflowOffTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("ADD", 0xFFFFFFFF, 0xFFFFFFFF) # = 0xFFFFFFFE
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 1:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 0:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
            
    def DirectedAddCarryOffOverflowOnTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("ADD", 0x4FFFFFFF, 0x4FFFFFFF) # = 0x8FFFFFFF
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 0:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 1:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags

    def DirectedAddCarryOnOverflowOnTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("ADD", 0x80000000, 0x80000000) # = 0x0
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 1:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 1:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
            
    def DirectedSubCarryOffOverflowOffTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("SUB", 0xFFFFFFFF, 0x1) # = 0xFFFFFFFE
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 0:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 0:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
            
    def DirectedSubCarryOnOverflowOffTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("SUB", 0xAFFFFFFF, 0xFFFFFFFF) # = 0xB0000000
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 1:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 0:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
            
    def DirectedSubCarryOffOverflowOnTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("SUB", 0x80000001, 0x2) # = 0x7FFFFFFF
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 0:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 1:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags

    def DirectedSubCarryOnOverflowOnTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._GetALUPattern("SUB", 0x4FFFFFFE, 0xB0000000) # = 0x9FFFFFFE
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 1:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            if flags.ALUOverflow != 1:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags        
            
    def _GetALUPattern(self, alu, src1, src2):
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags            
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={} 
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=1, imm={} 
            I optype=ALU, aluop={}, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=2 
            StopPatternReturnFlags
        """.format(src1, src2, alu))
        pattern = asm.Generate()
        return pattern
            
    ############################################################
    
    def RandomALUCheckFlagTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [1, 1], 'LEN_PAT': [1000, 2000], 'ALU': 0.01}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD', returnFlag = True) 
        source = p.GetPattern()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('alu.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            result = self.env.RunCheckers(slot, slice, endStatusMask = 0xE00E0000) # mask GSE, DSE, LSE, since test within pre-stage
            

class StickyErrors(HpccTest):
    def DirectedSetLocalStickyErrorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            ClearFlags
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            RandomPassingEvenToOddVectors length=16384
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            # Check the LSE flag
            flags = ac_registers.Flags(endStatus)
            if flags.LocalStickyError != 1:
                self.Log('error', 'Local Sticky Error bit is not set in flags=0x{:x}'.format(endStatus))
            # Don't check the 3 most significant bits of the flags
            # Don't check GSE, since domain master is not set
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  
            
    def DirectedClearStickyErrorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            ClearFlags
            RandomPassingEvenToOddVectors length=20000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            RandomPassingEvenToOddVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            RandomPassingEvenToOddVectors length=2000
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            # Check the LSE flag
            flags = ac_registers.Flags(endStatus)
            if flags.LocalStickyError != 0:
                self.Log('error', 'Local Sticky Error bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0040000)  # Don't check the 3 most significant bits of the flags, and DSE
            
    # padding between CLRSTICKY and the next fail
    # 26.0.2: @1.25ns, 20 doesn't work, 30 works
    #         @2.5ns,  20 doesn't work, 30 works
    #         @5ns,    20 works
    def DirectedFailAfterClearStickyErrorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            ClearFlags
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            RandomPassingEvenToOddVectors length=2000
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            RandomPassingEvenToOddVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY    
            RandomPassingEvenToOddVectors length=30            
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            RandomPassingEvenToOddVectors length=2000
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            RandomPassingEvenToOddVectors length=16384
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            # Check the LSE flag
            flags = ac_registers.Flags(endStatus)
            if flags.LocalStickyError != 1:
                self.Log('error', 'Local Sticky Error bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0040000)  # Don't check the 3 most significant bits of the flags, and DSE

    # HDMT Ticket 16317        
    def DirectedPaddingAfterClearStickyErrorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            ClearFlags
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            RandomPassingEvenToOddVectors length=2000
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            RandomPassingEvenToOddVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY    
            RandomPassingEvenToOddVectors length=1            
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            RandomPassingEvenToOddVectors length=2000
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            RandomPassingEvenToOddVectors length=16384
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for period in [1.25e-9, 100e-9]:
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                # Run the pattern
                completed, endStatus = self.env.RunPattern(slot, slice, pattern, period, captureAll=False,
                                                           captureFails=False, captureCTVs=False)
                # Check the LSE flag
                flags = ac_registers.Flags(endStatus)
                if flags.LocalStickyError != 1:
                    self.Log('error', f'Local Sticky Error bit is not set in flags=0x{endStatus:x}')
                self.env.RunCheckers(slot, slice, endStatusMask=0xE0040000)
                # Don't check the 3 most significant bits of the flags, and DSE

    # why simulator read weird numbers
    def DirectedGSETest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9

        asm = [None, None]
        pattern = [None, None]

        asm[0] = PatternAssembler()
        asm[0].LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            ClearFlags
            %repeat 256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            Repeat 60000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            StopPatternReturnFlags
        """)
        pattern[0] = asm[0].Generate()

        asm[1] = PatternAssembler()
        asm[1].LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            ClearFlags
            %repeat 256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            Repeat 60000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            StopPatternReturnFlags
        """)
        pattern[1] = asm[1].Generate()

        # Get set of slots
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributes(slot, 0, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            self.env.SetPeriodAndPEAttributes(slot, 1, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            
            for slice in [0, 1]:
                data1 = hpcc.ac[slice].Read('TriggerControl')
                data1.DomainMaster = 1
                data1.DomainID = slice +1
                hpcc.ac[slice].Write('TriggerControl', data1)
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, True, False, False, False) 
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                #patternControl = hpcc.ac[slice].Read('PatternControl')
                #patternControl.StickyErrorDomainMask = 0xfff
                #hpcc.ac[1].Write('PatternControl', patternControl)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1]:
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                #self.env.DumpCapture(slot, slice)
                
               
            patternEndStatus = ac_registers.Flags(hpcc.ac[0].Read('PatternEndStatus').Pack())
            if patternEndStatus.LocalStickyError != 1: 
                self.Log('error', 'Expected slice 0 LocalStickyError to be set, but it is not, actual pattern end status 0x{:X}'.format(patternEndStatus.Pack()))
            if patternEndStatus.DomainStickyError != 1: 
                self.Log('error', 'Expected slice 0 DomainStickyError to be set, but it is not, actual pattern end status 0x{:X}'.format(patternEndStatus.Pack()))
            if patternEndStatus.GlobalStickyError != 1: 
                self.Log('error', 'Expected slice 0 GlobalStickyError to be set, but it is not, actual pattern end status 0x{:X}'.format(patternEndStatus.Pack()))
            patternEndStatus = ac_registers.Flags(hpcc.ac[1].Read('PatternEndStatus').Pack())
            if patternEndStatus.LocalStickyError != 0: 
                self.Log('error', 'Expected slice 1 LocalStickyError to be 0, but it is not, actual pattern end status 0x{:X}'.format(patternEndStatus.Pack()))
            if patternEndStatus.DomainStickyError != 0: 
                self.Log('error', 'Expected slice 1 DomainStickyError to be 0, but it is not, actual pattern end status 0x{:X}'.format(patternEndStatus.Pack()))
            if patternEndStatus.GlobalStickyError != 1: 
                self.Log('error', 'Expected slice 1 GlobalStickyError to be set, but it is not, actual pattern end status 0x{:X}'.format(patternEndStatus.Pack()))
               

            # Run checkers
            self.env.RunMultiSliceCheckers([
                {'slot': slot, 'slice': 0, 'endStatusMask': 0xE0000000},
                {'slot': slot, 'slice': 1, 'endStatusMask': 0xE0000000},
            ])
            

# FIXME: These tests are sooo dumb
class Misc(HpccTest):
    def SetScenario(self, flagName, flagBit):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['VALUE'] = 0b1 << flagBit
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            I optype=ALU, aluop=OR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=VALUE
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the flag
            flags = ac_registers.Flags(endStatus)
            if getattr(flags, flagName) != 1:
                self.Log('error', '{} bit is not set in flags=0x{:x}'.format(flagName, endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xF0000000)  # Don't check the 4 most significant bits of the flags
    def DirectedSetWiredOrTest(self):
        self.SetScenario('WireOr', 20)
    def DirectedSetLoadActiveTest(self):
        self.SetScenario('LoadActive', 21)
    def DirectedSetSoftwareTriggerTest(self):
        self.SetScenario('SoftwareTrigger', 22)
    def DirectedSetDomainTriggerTest(self):
        self.SetScenario('DomainTrigger', 23)
    def DirectedSetRCTriggerTest(self):
        self.SetScenario('RCTrigger', 24)
    def ClearScenario(self, flagName, flagBit):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['VALUE'] = 0b1 << flagBit
        asm.LoadString("""\
            StartPattern
            ClearFlags
            RandomVectors length=128
            I optype=ALU, aluop=OR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=VALUE
            RandomVectors length=128
            I optype=ALU, aluop=XOR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=VALUE
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern)
            # Check the flag
            flags = ac_registers.Flags(endStatus)
            if getattr(flags, flagName) != 0:
                self.Log('error', '{} bit is set in flags=0x{:x}'.format(flagName, endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xF0000000)  # Don't check the 4 most significant bits of the flags
    def DirectedClearWiredOrTest(self):
        self.ClearScenario('WireOr', 20)
    def DirectedClearLoadActiveTest(self):
        self.ClearScenario('LoadActive', 21)
    def DirectedClearSoftwareTriggerTest(self):
        self.ClearScenario('SoftwareTrigger', 22)
    def DirectedClearDomainTriggerTest(self):
        self.ClearScenario('DomainTrigger', 23)
    def DirectedClearRCTriggerTest(self):
        self.ClearScenario('RCTrigger', 24)

class Triggers(HpccTest):
    def Scenario(self, trigger, condition):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['CONDITION'] = condition
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            LOOP:
                RandomVectors length=256
            GotoIfNon `LOOP`, CONDITION
            StopPattern `rand(32)`
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetDomains({
                (slot, slice): (True, slot),
            })
            hpcc = self.env.instruments[slot]            
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, captureAll=False, captureFails=True, captureCTVs=True, waitForComplete=False)
            self.env.rc.send_trigger(trigger, slot)
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
                self.env.AbortPattern(slot, slice) 
                
    def DirectedDomainTriggerTest(self):
        self.Scenario(0x0ff2, 'COND.DMTRIGRCVD')

    def DirectedRCTriggerTest(self):
        self.Scenario(0x0ffb, 'COND.RCTRIGRCVD')

    def RandomDSE32BitClearGlobalTriggerFromACToACTest(self):
        # AC to AC 32 bit CLEAR GLOBAL trigger from pattern
        # randomize SEMask, DutId, Domain Id
        # Trigger is sent to the same Dut ID as the slice sending trigger
        # CLEAR trigger sent to the same domain/dut id that the original trigger was sent to.
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup( dutIdRange=[62, 64] ) 
        SEMask = 'Random' #self.env.RandomSEMaskGenerator(domainIDList)
        acDSETrigger = '0x0' + format(sourceDutID, 'x') + '11' + format(targetTriggerDomain, 'x') + 'f' + '4' #  Sending Sticky Error Trigger
        acDSEClearTrigger = '0x0' + format(sourceDutID, 'x') + '11' + format(targetTriggerDomain, 'x') + 'f' + '7' #Clear Global Trigger Hardcoded
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask domainIDList')
        triggerPattern = Pattern('_BasicClearACtoRCPatternSlice0Internal', sourceSlice,[ int(acDSETrigger, 16),int(acDSEClearTrigger, 16) ], SEMask, domainIDList)
        nonTriggerPattern = Pattern('_BasicClearRCtoACPatternInternal', None, 'COND.DSE', SEMask, domainIDList)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), int(acDSEClearTrigger, 16), 'DomainStickyError', clearTriggerOption=False, RcToAcTrigger=False)

    def RandomDSE32BitTriggerAcToAcWithZeroSEMaskTest(self):
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup() 
        SEMask = 0x0
        acDSETrigger = '0x0' + '3F' + '11' + format(targetTriggerDomain, 'x') + 'a' + '4' # DUT ID is 0 for now.
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask')
        triggerPattern = Pattern('_BasicACtoRCPatternSlice0Internal', sourceSlice, int(acDSETrigger, 16), SEMask)
        nonTriggerPattern = Pattern('_BasicRCtoACPatternInternal', None, 'COND.DSE', SEMask)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), 0x0,'DomainStickyError', RcToAcTrigger=False)

    def RandomDSE32BitTriggerAcToAcWithRandomSEMaskTest(self):
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup() 
        SEMask1 = self.env.RandomSEMaskGenerator(domainIDList)
        SEMask2 = self.env.RandomSEMaskGenerator(domainIDList)
        acDSETrigger = '0x0' + '3F' + '11' + format(targetTriggerDomain, 'x') + 'a' + '4' # DUT ID is 0 for now.
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask')
        triggerPattern = Pattern('_BasicACtoRCPatternSlice0Internal', sourceSlice, int(acDSETrigger, 16), SEMask1)
        nonTriggerPattern = Pattern('_BasicRCtoACPatternInternal', None, 'COND.DSE', SEMask2)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), 0x0,'DomainStickyError', RcToAcTrigger=False)

    def RandomDSE32BitTriggerAcToAcWithRandomDutIdTest(self):
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup( dutIdRange=[62, 64] ) 
        SEMask = 0x7fff #'Random' #0x7fff
        acDSETrigger = '0x0' + format(sourceDutID, 'x') + '11' + format(targetTriggerDomain, 'x') + 'a' + '4' # DUT ID is 0 for now.
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask domainIDList')
        triggerPattern = Pattern('_BasicACtoRCPatternSlice0Internal', sourceSlice, int(acDSETrigger, 16), SEMask, domainIDList)
        nonTriggerPattern = Pattern('_BasicRCtoACPatternInternal', None, 'COND.DSE', SEMask, domainIDList)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), 0x0,'DomainStickyError', RcToAcTrigger=False)

    def RandomDSE32BitTriggerAcToAcWithRandomSEMaskDutIdTest(self):
        # AC to AC 32 bit trigger from pattern
        # randomize SEMask, DutId, Domain Id
        # Trigger is sent to the same Dut ID as the slice sending trigger
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup( dutIdRange=[62, 64] ) 
        SEMask = 'Random' #self.env.RandomSEMaskGenerator(domainIDList)
        acDSETrigger = '0x0' + format(sourceDutID, 'x') + '11' + format(targetTriggerDomain, 'x') + 'a' + '4' # DUT ID is 0 for now.
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask domainIDList')
        triggerPattern = Pattern('_BasicACtoRCPatternSlice0Internal', sourceSlice, int(acDSETrigger, 16), SEMask, domainIDList)
        nonTriggerPattern = Pattern('_BasicRCtoACPatternInternal', None, 'COND.DSE', SEMask, domainIDList)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), 0x0,'DomainStickyError', RcToAcTrigger=False)

    def RandomDSE32BitTriggerAcToAcToExistingDomainTest(self):
        # AC to AC 32bit trigger from pattern sent to the existing domain
        # Same Dut ID for all slices, SEMask set to all slices, randomized domain and domain
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup( ) 
        SEMask = 0x7fff # Hard coded to ever domain
        acDSETrigger = '0x0' + '3F' + '00' + format(targetTriggerDomain, 'x') + 'a' + '4' # DUT ID is 0 for now.
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask')
        triggerPattern = Pattern('_BasicACtoRCPatternSlice0Internal', sourceSlice, int(acDSETrigger, 16), SEMask)
        nonTriggerPattern = Pattern('_BasicRCtoACPatternInternal', None, 'COND.DSE', SEMask)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), 0x0,'DomainStickyError', RcToAcTrigger=False)

    def RandomDSE32BitTriggerAcToAcTestToNonExistingDomainTest(self):
        # AC to AC 32bit trigger from pattern sent to the nonexisting domain
        # Same Dut ID for all slices, SEMask set to all slices, randomized domain and domain
        mtdDomainGroup, domainIDList, sourceSlice, sourceDutID, targetTriggerDomain = self.TriggerSetup( ) 
        SEMask = 0x7fff #self.env.RandomSEMaskGenerator(domainIDList)
        allDomainIDs = range(0, 15)
        nonExistingDomain = list( set(allDomainIDs).difference( domainIDList) )
        acDSETriggerDomain = random.choice(nonExistingDomain)
        acDSETrigger = '0x0' + '3F' + '00' + format(acDSETriggerDomain, 'x') + 'a' + '4' # DUT ID is 0 for now.
        Pattern = namedtuple('Pattern', 'name slotslice params SEMask')
        triggerPattern = Pattern('_BasicACtoRCPatternSlice0Internal', sourceSlice, int(acDSETrigger, 16), SEMask)
        nonTriggerPattern = Pattern('_BasicRCtoACPatternInternal', None, 'COND.DSE', SEMask)
        self.DSEGSE32BitScenario([triggerPattern, nonTriggerPattern], mtdDomainGroup, int(acDSETrigger, 16), 0x0,'DomainStickyError', RcToAcTrigger=False)


    def _BasicRCtoACPatternNoFail(self, condition):
        self.env.SetConfig('EvenToOddLoopback')
        #self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['CONDITION'] = condition
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            #ClearFlags
            %repeat 20480 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %repeat 102400 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            %end
            #LOOP:
            #    RandomPassingEvenToOddNoSwitchVectors length=`1024`
            #GotoIfNon `LOOP`, CONDITION
            StopPatternReturnFlags
        """)
        return asm.Generate()

    def _BasicClearACtoRCPatternSlice0Internal(self, triggerValues):
        #self.env.SetConfig('EvenToOddLoopback')
        # Create the pattern
        asm = PatternAssembler()
        #asm.symbols['CONDITION'] = condition
        #print(triggerValues)
        triggerValue, clearTriggerValue = triggerValues

        asm.symbols['TRIGGER'] = triggerValue
        asm.symbols['TRIGGERCLEAR'] = clearTriggerValue
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            ClearFlags
            %repeat 20480 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            I optype=EXT, extop=TRIGGER, action=TRGDOM_I, imm=TRIGGER
            %repeat 102400
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            I optype=EXT, extop=TRIGGER, action=TRGDOM_I, imm=TRIGGERCLEAR
            %repeat 102400
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            StopPatternReturnFlags
        """)
        return asm.Generate()

    def _BasicClearRCtoACPatternInternal(self, condition):
        #self.env.SetConfig('EvenToOddLoopback')
        #self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['CONDITION'] = condition
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            ClearFlags
            %repeat 20480 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            %repeat 102400 #40960    # 20480 #5120 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            %repeat 102400 #40960    # 20480 #5120 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            #LOOP:
            #    RandomVectors length=256
            #GotoIfNon `LOOP`, CONDITION
            StopPatternReturnFlags
        """)
        return asm.Generate()

    def _BasicACtoRCPatternSlice0Internal(self, triggerValue):
        #self.env.SetConfig('EvenToOddLoopback')
        # Create the pattern
        asm = PatternAssembler()
        #asm.symbols['CONDITION'] = condition
        asm.symbols['TRIGGER'] = triggerValue
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            ClearFlags
            %repeat 20480 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            I optype=EXT, extop=TRIGGER, action=TRGDOM_I, imm=TRIGGER
            %repeat 102400  #5120 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            StopPatternReturnFlags
        """)
        return asm.Generate()

    def _BasicRCtoACPatternInternal(self, condition):
        #self.env.SetConfig('EvenToOddLoopback')
        #self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['CONDITION'] = condition
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            ClearFlags
            %repeat 20480 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            %repeat 102400 #40960    # 20480 #5120 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            StopPatternReturnFlags
        """)
        return asm.Generate()

    def Trigger32BitCheckOnRC(self, AcTrigger):
        ## Checking whether RC received correct trigger from AC or other source.
        targetRsc = 2 # RC
        AcTriggerValue = 0
        if AcTrigger:  
            AcTriggerValue = AcTrigger
        if ( (AcTriggerValue & 0b11111100000000000000000000000000 ) >> 26 ) == targetRsc:
            expected = AcTrigger
            triggerOnRc = self.env.rc.next_trigger()
            if not(triggerOnRc is None):
                triggerId, triggerSource, triggerValue = triggerOnRc
                if triggerValue != expected:
                    self.Log('error', 'RC received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected, triggerValue))
                else:
                    self.Log('debug', 'RC received trigger 0x{:x}'.format(triggerValue))
            else:
                self.Log('error', 'RC did not received trigger: expected=0x{:x}'.format(expected))
        return AcTriggerValue


    def TotalFailCount(self, slot, slice, placement):
        
        ## Total Fail Count
        hpcc = self.env.instruments[slot]
        totalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
        if placement=='before':
            self.Log('debug', 'Before Pattern Running, Total Fail COunt is {} for slot {} slice {}'.format(totalFailCount, slot, slice) )
        elif placement=='after':
            self.Log('debug', 'After Pattern Running, Total Fail COunt is {} for slot {} slice {}'.format(totalFailCount, slot, slice) )
        # Channel Fail Count instead of total fail count.
        #for ch in range(56):
        #    ChannelFailCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount').Pack()
        #    self.Log('info', 'Channel Fail COunt for Channel {} is {} for slot {} slice {}'.format(ch, ChannelFailCount, slot, slice) )

    def PatternCompleteInTriggerTest(self, slot, slice):
        ## Check Whether Pattern is Complete 
        hpcc = self.env.instruments[slot]
        completed = hpcc.ac[slice].WaitForCompletePattern()
        if not completed:
            self.Log('error', 'slot {} slice {} pattern execution did not complete'.format(slot, slice) )
        else:
            self.Log('debug', 'slot {} slice {} Pattern execution completed'.format(slot, slice) )

    def Trigger32BitCheckOnAC(self,slot, slice, RcTrigger, AcTriggerValue): 
        hpcc = self.env.instruments[slot]
        data = hpcc.ac[slice].Read('TriggerEvent')
        triggerValue = data.Pack()
        if not AcTriggerValue:
            self.Log('debug', 'slot {} slice {}  trigger Value received on HPCCAC is {} while {} is sent from RC'.format(slot, slice, hex(triggerValue), hex(RcTrigger) ) )
        else:
            self.Log('debug', 'slot {} slice {}  trigger Value received on HPCCAC is {} while {} is sent from RC'.format(slot, slice, hex(triggerValue), hex(AcTriggerValue) ) )

    def CheckPatternEndStatus(self, slot, slice, flagName):
        hpcc = self.env.instruments[slot]
        endStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
        # Check the flag
        flags = ac_registers.Flags(endStatus)
        if getattr(flags, flagName) != 1:
            self.Log('debug', '{} bit in slot {} slice {} is not set in flags=0x{:x}'.format( flagName,slot, slice, endStatus))
        if getattr(flags, flagName) == 1:
            self.Log('debug', '{} bit in slot {} slice {} is set in flags=0x{:x}'.format( flagName,slot, slice, endStatus))
        # CD flag Read
        CDFlags = hpcc.ac[slice].Read('CentralDomainRegisterFlags').Pack()
        self.Log('debug', 'slot {} slice {} CD Flag is {}'.format(slot, slice, hex(CDFlags)))
        # SE Mask flag Read
        SEMask = hpcc.ac[slice].Read('PatternControl').StickyErrorDomainMask
        self.Log('debug', 'slot {} slice {} Sticky Error Mask is {}'.format(slot, slice, bin(SEMask)))

    def HpccAC32BitCardInfo(self, slot, slice):
        hpcc = self.env.instruments[slot]
        dataTriggerControl = hpcc.ac[slice].Read('TriggerControl')
        triggerCard = dataTriggerControl.CardType
        triggerID = dataTriggerControl.DomainID
        triggerMaster = dataTriggerControl.DomainMaster
        triggerCardTypeExtended = dataTriggerControl.CardTypeExtended
        triggerDutID = dataTriggerControl.DutID
        self.Log('debug', 'slot {} slice {}  trigger card type is {}. Trigger Card Extended is {}. DomainID is {}. Domain Master is {}. Dut ID is {}.'.format(slot, slice, triggerCard, triggerCardTypeExtended , triggerID, bool(triggerMaster), triggerDutID ) )
    
    def WritePatternOnEachSlice(self, patternList, periods ):
        fpgas = list( self.env.fpgas )
        for pattern in patternList:
            #print(pattern)
            if pattern.slotslice == 'ALL' :
                for slot, slice in fpgas:
                    self.env.Log('debug', 'Loading Pattern on slot {}, slice{}'.format(slot, slice) )
                    self.WritePatternForSlice(slot, slice, pattern, periods)
            elif type(pattern.slotslice) is list or type(pattern.slotslice) is tuple:
                for slot, slice in pattern.slotslice:
                    self.WritePatternForSlice(slot, slice, pattern, periods)
                    fpgas.pop( fpgas.index( (slot,slice) ) )
            elif pattern.slotslice is None:
                for slot, slice in fpgas:
                    self.WritePatternForSlice(slot, slice, pattern, periods)

    def WriteSetupForSlice1(self, slot, slice, pattern = None, patternNamedTuple = None, offset = 0x0, start = 0x0, captureAddress = None, captureType = 1, captureFails = False, captureCTVs = False, captureAll = True, stopOnMaxFail = False, dramOverflowWrap = False, maxFailCountPerPatern = 0xFFFFFFFF, maxFailCaptureCount = 0xFFFFFFFF, maxCaptureCount = 0xFFFFFFFF, captureLength = 0xFFFFFFFF, waitForComplete = True, maskFailureToComplete = False, numberOfTries = 500):
        self.WriteStickyErrorMask(slot, slice, patternNamedTuple)
        self.env.WriteExecutePatternAndSetupCapturePerSlice(slot, slice, pattern , offset, start , captureAddress , captureType , captureFails , captureCTVs , captureAll , stopOnMaxFail , dramOverflowWrap , maxFailCountPerPatern , maxFailCaptureCount , maxCaptureCount , captureLength , waitForComplete , maskFailureToComplete, numberOfTries )
        
    def WriteStickyErrorMask(self, slot, slice, pattern):
        hpcc = self.env.instruments[slot]
        StickyErrorMask = hpcc.ac[slice].Read('PatternControl')
        #if pattern.SEMask == 'Random':
        if pattern.SEMask == 'Random':
            StickyErrorMask.StickyErrorDomainMask = self.env.RandomSEMaskGenerator(pattern.domainIDList)
            hpcc.ac[slice].Write('PatternControl', StickyErrorMask)
            #print(StickyErrorMask.StickyErrorDomainMask) 
        else:
            StickyErrorMask.StickyErrorDomainMask = pattern.SEMask
            hpcc.ac[slice].Write('PatternControl', StickyErrorMask)

    def WritePatternForSlice(self, slot, slice, pattern, periods):
        hpcc = self.env.instruments[slot]
        classObject = getattr(sys.modules[__name__], type(self).__name__ )
        patternKey = getattr( classObject, pattern.name)
        self.env.SetPeriodAndPEAttributes(slot, slice, periods[slice], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': periods[slice] / 2})
        self.WriteSetupForSlice1( slot, slice, patternKey(self, pattern.params), pattern  )

    def TriggerSetup(self, dutIdRange=[63, 64]):
        cond = True
        differentDutIds = False
        onlyOneSlot = False
        onlyOneSlice = False
        if len( self.env.fpgas ) == 2 : onlyOneSlot = True
        if len( self.env.fpgas ) == 1 : onlyOneSlice = True 
        if ( dutIdRange[-1] - dutIdRange[0] ) >= 2 : differentDutIds = True 
        
        if onlyOneSlice: 
            cond=False
            self.Log('error', 'At least two slices required to run trigger test')

        while cond:
            mtdDomainGroup, domainIDList, DutIDList  = self.env.MTDdomainDutIDGroupGenerator(dutIDRange = dutIdRange)
            sourceDomainGroup = random.choice(mtdDomainGroup)
            targetDomainGroup = random.choice(mtdDomainGroup)
            
            if differentDutIds:
                if (sourceDomainGroup.domainId != targetDomainGroup.domainId ) and (sourceDomainGroup.dutID == targetDomainGroup.dutID) and (sourceDomainGroup.master == 1) and (len(set(DutIDList) ) >= 2 ) and not onlyOneSlot:
                    self.Log('debug', 'source slot, slice is {}'.format(sourceDomainGroup.slotslice )  )
                    return mtdDomainGroup, domainIDList, [sourceDomainGroup.slotslice], sourceDomainGroup.dutID, targetDomainGroup.domainId
                elif (sourceDomainGroup.domainId != targetDomainGroup.domainId ) and (sourceDomainGroup.dutID == targetDomainGroup.dutID) and (sourceDomainGroup.master == 1) and onlyOneSlot:
                    return mtdDomainGroup, domainIDList, [sourceDomainGroup.slotslice], sourceDomainGroup.dutID, targetDomainGroup.domainId
            else:
                if (sourceDomainGroup.domainId != targetDomainGroup.domainId ) and (sourceDomainGroup.dutID == targetDomainGroup.dutID) and (sourceDomainGroup.master == 1): 
                    self.Log('debug', 'source slot, slice is {}'.format(sourceDomainGroup.slotslice )  )
                    return mtdDomainGroup, domainIDList, [sourceDomainGroup.slotslice], sourceDomainGroup.dutID, targetDomainGroup.domainId

        
    def DSEGSE32BitScenario(self, triggerPattern, mtdDomain, RcTrigger,  RcClearTrigger, flagName, AcTrigger=None, clearTriggerOption=False, RcToAcTrigger=True):
        #self.env.SetConfig('EvenToOddLoopback')
        self.env.SetConfig('InternalLoopback')
        periods = [ 50e-9, 50e-9]

        self.env.SetDomainsDutID32BitWithNamedTuple( mtdDomain )

        # Writing Pattern on each slice using dictionary 
        self.WritePatternOnEachSlice(triggerPattern, periods)
        
        # Priningint out Total fail count on each slice before running pattern
        for slot, slice in sorted(self.env.fpgas):
            self.HpccAC32BitCardInfo(slot, slice)
            self.TotalFailCount(slot, slice, 'before')

        # Send sync pulse to start the pattern
        self.env.rc.send_sync_pulse()

        if RcToAcTrigger:
            self.env.rc.send_trigger(RcTrigger)

        ## Another Trigger send from RC module to AC module to check clearing sticky error bit
        if clearTriggerOption:
            self.env.rc.send_trigger(RcClearTrigger)
        
        for slot, slice in sorted(self.env.fpgas):
            #Checking whether Pattern completes in each test 
            self.PatternCompleteInTriggerTest(slot, slice)

            ## Total Fail Count after running pattern
            self.TotalFailCount(slot, slice, 'after')
            AcTriggerValue  =  self.Trigger32BitCheckOnRC(AcTrigger)
            self.Trigger32BitCheckOnAC(slot, slice, RcTrigger, AcTriggerValue )
            self.CheckPatternEndStatus(slot, slice, flagName)

        # MultiSlice Simulation
        endstatusMask = 0xF0000000
        multiSliceCheckerList = []
        multiSliceCheckerDict = {}
        for slot, slice in sorted(self.env.fpgas):
            multiSliceCheckerDict['slot'] = slot
            multiSliceCheckerDict['slice'] = slice
            multiSliceCheckerDict['endStatusMask'] = endstatusMask
            multiSliceCheckerList.append( copy.copy(multiSliceCheckerDict ) )
        
        self.env.RunMultiSliceCheckers(multiSliceCheckerList,) 
