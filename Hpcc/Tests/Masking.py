################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Masking.py
#-------------------------------------------------------------------------------
#     Purpose: Pattern masking tests
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 04/29/15
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class VectorLevel(HpccTest):
    def DirectedSelectableFailMaskTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # This vector should FAIL and NOT get masked!
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L1  # This vector should FAIL and NOT get masked!
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1  # This vector should FAIL and NOT get masked!
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=2, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=3, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=4, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=5, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=6, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=7, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=256
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Set the masks
            self.env.SetSelectableFailMask(slot, slice, 0, 0b00000000000000000000000000000000000000000000000000000000)
            self.env.SetSelectableFailMask(slot, slice, 1, 0b00000000000000000000000000000000000000000000000000000011)
            self.env.SetSelectableFailMask(slot, slice, 2, 0b00000000000000000000000000000000000000000000000000001100)
            self.env.SetSelectableFailMask(slot, slice, 3, 0b00000000000000000000000000000000000000000000000000110000)
            self.env.SetSelectableFailMask(slot, slice, 4, 0b00000000000000000000000000000000000000000000000011000000)
            self.env.SetSelectableFailMask(slot, slice, 5, 0b00000000000000000000000000000000000000000000001100000000)
            self.env.SetSelectableFailMask(slot, slice, 6, 0b00000000000000000000000000000000000000000000110000000000)
            self.env.SetSelectableFailMask(slot, slice, 7, 0b00000000000000000000000000000000000000000011000000000000)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, captureFails = True, captureCTVs = False, captureAll = False)
            captureCount = self.env.CaptureCount(slot, slice)
            if captureCount != 3:
                self.Log('error', 'Only 3 capture fails expected, but got {}'.format(captureCount))
            self.env.RunCheckers(slot, slice)

    def DirectedMTVFailStatusTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # This vector should FAIL and NOT get masked!
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L1  # This vector should FAIL and NOT get masked!
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1  # This vector should FAIL and NOT get masked!
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=2, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=3, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=4, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=5, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=6, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=7, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # This vector should fail but get masked!
            %repeat 200
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            %end
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Set the masks
            self.env.SetSelectableFailMask(slot, slice, 0, 0b00000000000000000000000000000000000000000000000000000000)
            self.env.SetSelectableFailMask(slot, slice, 1, 0b00000000000000000000000000000000000000000000000000000011)
            self.env.SetSelectableFailMask(slot, slice, 2, 0b00000000000000000000000000000000000000000000000000001100)
            self.env.SetSelectableFailMask(slot, slice, 3, 0b00000000000000000000000000000000000000000000000000110000)
            self.env.SetSelectableFailMask(slot, slice, 4, 0b00000000000000000000000000000000000000000000000011000000)
            self.env.SetSelectableFailMask(slot, slice, 5, 0b00000000000000000000000000000000000000000000001100000000)
            self.env.SetSelectableFailMask(slot, slice, 6, 0b00000000000000000000000000000000000000000000110000000000)
            self.env.SetSelectableFailMask(slot, slice, 7, 0b00000000000000000000000000000000000000000011000000000000)
            # Run the pattern
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, period, captureType = ac_registers.CaptureControl.FAIL_STATUS)
            self.env.RunCheckers(slot, slice)
    
    def RandomMTVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'MTV': 0.1}
        p = RPG(constraints=constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        pdata = pattern.Generate()
        #pattern.SaveObj('mtv.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, pdata, 2000)            
            # Set the masks
            self.env.SetSelectableFailMask(slot, slice, 0, 0b00000000000000000000000000000000000000000000000000000000) # default
            self.env.SetSelectableFailMask(slot, slice, 1, 0b11111111111111111111111111111111111111111111111111111111)
            self.env.SetSelectableFailMask(slot, slice, 2, random.getrandbits(56))
            self.env.SetSelectableFailMask(slot, slice, 3, random.getrandbits(56))
            self.env.SetSelectableFailMask(slot, slice, 4, random.getrandbits(56))
            self.env.SetSelectableFailMask(slot, slice, 5, random.getrandbits(56))
            self.env.SetSelectableFailMask(slot, slice, 6, random.getrandbits(56))
            self.env.SetSelectableFailMask(slot, slice, 7, random.getrandbits(56))
            
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, param['period'], start=pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
             

class PatternLevel(HpccTest):
    def DirectedPlistMaskTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
        PATTERN1:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=256
            Return
        PATTERN2:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=256
            Return
        PATTERN_START:
            S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000001100
            PatternId 0
            PCall `PATTERN1`
            S stype=MASK,                   data=0b00110000000000000000000000000000000000000000000000000000
            PatternId 1
            PCall `PATTERN2`
        PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, start = asm.Resolve('PATTERN_START'), captureFails = True, captureCTVs = False, captureAll = False)
            self.env.RunCheckers(slot, slice)  

    def DirectedPlistMaskCheckingStackTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
        PATTERN1:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256, simple=1                                               #remove switch to ensure the next one is L1 instead of 0H
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=256, simple=1
            Return
        PATTERN2:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256, simple=1
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # This vector should fail because only one pin is masked
            RandomPassingEvenToOddVectors length=256, simple=1
            Return
        PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000001100
            PatternId 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0
            RandomPassingEvenToOddVectors length=256, simple=1
            PCall `PATTERN1`
            S stype=MASK,                   data=0b00110000000000000000000000000000000000000000000000000000
            PatternId 1
            PCall `PATTERN2`
        PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, start = asm.Resolve('PATTERN_START'), captureFails = True, captureCTVs = False, captureAll = False)
            captureCount = self.env.CaptureCount(slot, slice)
            if captureCount != 2:
                self.Log('error', 'Two capture fails expected, but got {}'.format(captureCount))
            self.env.RunCheckers(slot, slice)
    
    def DirectedPlistMaskNestedTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
        PATTERN1:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            RandomPassingEvenToOddVectors length=200, simple=1                                               # 1 - 200, remove switch to ensure the next one is L1 instead of 0H
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 201, This vector should fail but get masked!
            S stype=MASK,                   data=0b00110000000000000000000000000000000000000000000000000000  # 202
            PCall `PATTERN2`
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 204, should fail
            Return
        PATTERN2:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 206
            RandomPassingEvenToOddVectors length=200, simple=1                                               # 207 - 406
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 407, This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=200, simple=1                                               # 408 - 607
            Return
        PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 609
            S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000001100  # 610          
            PatternId 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 612, should fail
            PCall `PATTERN1`
        PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, start = asm.Resolve('PATTERN_START'), captureType = ac_registers.CaptureControl.FAIL_STATUS, captureFails = True, captureCTVs = False, captureAll = False)
            captureCount = self.env.CaptureCount(slot, slice)
            if captureCount != 2:
                self.Log('error', 'Two capture fails expected, but got {}'.format(captureCount))
            self.env.RunCheckers(slot, slice)

    # make sure mask is initialized correctly
    def DirectedMaskAbortTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN1:                                                                                   
                S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
                LOOP:  
                    Drive0CompareLVectors 1000   
                Goto `LOOP`
                Return
            PATTERN_START:
                S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
                S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000000000            
                PatternId 0
                PCall `PATTERN1`
            PATTERN_END:            
                StopPattern 0xdeadbeef
        """)
        
        pdata = pattern.Generate()
        maxFailCaptureCount = 1

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureType = ac_registers.CaptureControl.FAIL_STATUS, stopOnMaxFail = True, maxFailCaptureCount = maxFailCaptureCount, start = pattern.Resolve('PATTERN_START'), captureFails = True, captureCTVs = False, captureAll = False)
                      
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus == 0xdeadbeef:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead, fail to stop on max fail'.format(actualEndStatus))
            else:
                self.Log('debug', 'Pattern End Status = 0x{:x}'.format(actualEndStatus))
            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'StopOnFirstFail' not in alarms:
                self.Log('error', 'StopOnFirstFail is not set in AlarmControl')
            else:
                self.Log('warning', 'Caught StopOnFirstFail alarm, expected')

        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN1:                                                                                   
                S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
                RandomPassingEvenToOddVectors length=256, simple=1 
                Return
            PATTERN_START:
                S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
                PatternId 0
                PCall `PATTERN1`
            PATTERN_END:            
                StopPattern 0xdeadbeef
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, start = pattern.Resolve('PATTERN_START'), captureType = ac_registers.CaptureControl.FAIL_STATUS, captureFails = True, captureCTVs = False, captureAll = False)
            self.env.RunCheckers(slot, slice)

        
    def DirectedPlistUnmaskTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
        PATTERN1:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=256
            Return
        PATTERN2:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # This vector should FAIL and NOT get masked!
            RandomPassingEvenToOddVectors length=256
            Return
        PATTERN_START:
            S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000001100
            PatternId 0
            PCall `PATTERN1`
            S stype=MASK,                   data=0b00110000000000000000000000000000000000000000000000000000
            S stype=UNMASK,                 data=0b11001111111111111111111111111111111111111111111111111111
            PatternId 1
            PCall `PATTERN2`
        PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, start = asm.Resolve('PATTERN_START'), captureFails = True, captureCTVs = False, captureAll = False)
            captureCount = self.env.CaptureCount(slot, slice)
            if captureCount != 1:
                self.Log('error', 'One capture fail expected, but got {}'.format(captureCount))
            self.env.RunCheckers(slot, slice)

    def RandomPerPatternMaskTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()     
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'PMASK': True}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        pdata = pattern.Generate()
        #pattern.SaveObj('pmask.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, pdata, 2000)                 
            self.env.RunPattern(slot, slice, pdata, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            
    def DirectedCQ14982RegOpMaskTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            ClearRegisters 

            PushRegister 0  
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x1
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_RB, opdest=ALUDEST.NONE, regA=0, regB=0
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            PopRegister 0  # Restore r0

            S stype=MASK,       data=`rand(56)`
            S stype=UNMASK,     data=`rand(56)`
            
            RandomPassingEvenToOddVectors length=2000
            
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, captureFails = True, captureCTVs = False, captureAll = False)
            self.env.RunCheckers(slot, slice)
            
            
class Mix(HpccTest):
    def DirectedMaskUnmaskMTVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
        PATTERN1:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # This vector should fail but get masked!
            RandomPassingEvenToOddVectors length=256
            Return
        PATTERN_START:
            S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000000000
            S stype=UNMASK,                 data=0b00111111111111111111111111111111111111111111111111111111
            PatternId 0
            PCall `PATTERN1`
        PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.SetSelectableFailMask(slot, slice, 0, 0b00000000000000000000000000000000000000000000000000000000)
            self.env.SetSelectableFailMask(slot, slice, 1, 0b11000000000000000000000000000000000000000000000000000000)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, start = asm.Resolve('PATTERN_START'), captureType = ac_registers.CaptureControl.FAIL_STATUS)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            
    def RandomVectorAndPatternMaskTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'MTV': 0.1, 'PMASK': True}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        pdata = pattern.Generate()
        #pattern.SaveObj('mtv.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, pdata, 2000)            
            self.env.RandomizeMTVFailMask(slot, slice) 
            
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            