################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: ACTiming.py
#-------------------------------------------------------------------------------
#     Purpose: ac timing / edge placement test, don't expect to run regularly
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/28/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import re

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers

PICO_SECONDS_PER_SECOND = 1000000000000


class DelayStack(HpccTest):
    def _GenerateCompareDelayStackShmooSlice1Pattern(self, ratio, compare):
    
        pattern = """\
            %repeat RATIO
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            %end
            %repeat RATIO
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            %repeat LENGTH
            """
            
        for vector in range(ratio):
            if vector == compare:
                pattern = pattern + """V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
                """
            else:
                pattern = pattern + """V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                """
                
        pattern = pattern + """%end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x89ABCDEF
        """
        
        return pattern

    def _CompareDelayShmooTest(self, period, ratio, driveSubCycleCount, driveStepSize, compareSubCycleCount, compareStepSize, dynamicDelay = False):
        qdrCount = 4
        maxDynamicPhaseDelay = 13
        self.env.SetConfig('LowToHighLoopback')

        periods = [None, None]
        periods[0] = period * ratio
        periods[1] = period
        
        asm = [None, None]
        pattern = [None, None]

        repeats = 1024

        asm[0] = PatternAssembler()
        asm[0].symbols['REPEATS'] = repeats
        asm[0].LoadString("""\
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        """)

        pattern[0] = asm[0].Generate()

        compareVector = (ratio // 2)
        asm[1] = PatternAssembler()
        asm[1].symbols['LENGTH'] = repeats
        asm[1].symbols['RATIO'] = ratio
        asm[1].LoadString(self._GenerateCompareDelayStackShmooSlice1Pattern(ratio, compareVector))
        pattern[1] = asm[1].Generate()

        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        goldenCapture = None
        count = 50 #update delay stack status to user every 50 steps for ScQd test
        top = False

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            #set compare time to start of compare vector time + .5TPER to strobe at 50% of cycle
            self.env.SetPeriodAndPEAttributes(slot, 0, periods[0], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': (periods[1] * (compareVector+.55))})
            self.env.SetPeriodAndPEAttributes(slot, 1, periods[1], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': periods[1]/2})

            if(dynamicDelay):
                for channel in range(56):
                    slice0regs = hpcc.ac[0].ReadChannel(channel, 'PerChannelControlConfig3')
                    slice1regs = {'PerChannelControlConfig1': hpcc.ac[1].ReadChannel(channel, 'PerChannelControlConfig1'),'PerChannelControlConfig4': hpcc.ac[1].ReadChannel(channel, 'PerChannelControlConfig4')}
                    #dynamic phase delay values above 13 produce unreliable results based on last clock state
                    #input dynamic phase delay is inverted so set to maximum, adjust output delay stack to match
                    compareBias = maxDynamicPhaseDelay - slice0regs.InputDynamicPhaseDelay
                    slice0regs.InputDynamicPhaseDelay += compareBias
                    driveBias = compareBias * driveStepSize
                    #convert input delay stack to QDR count and subtract off the phase delay bias
                    driveTotalQdr = (slice1regs['PerChannelControlConfig4'].OutputTesterCycleDelay * driveSubCycleCount + slice1regs['PerChannelControlConfig1'].OutputSubCycleDelay) * qdrCount + slice1regs['PerChannelControlConfig1'].OutputQdrPhaseSelect
                    driveTotalQdr -= driveBias
                    #convert back to TC-SC-QDR convention
                    driveTcState = driveTotalQdr // (qdrCount * driveSubCycleCount)
                    driveScState = (driveTotalQdr // qdrCount) % driveSubCycleCount
                    driveQdState =  driveTotalQdr % qdrCount
                    slice1regs['PerChannelControlConfig1'].OutputQdrPhaseSelect = driveQdState
                    slice1regs['PerChannelControlConfig1'].EnableQdrPhaseSelect = driveQdState
                    slice1regs['PerChannelControlConfig1'].OutputSubCycleDelay = driveScState
                    slice1regs['PerChannelControlConfig1'].EnableSubCycleDelay = driveScState
                    slice1regs['PerChannelControlConfig4'].OutputTesterCycleDelay -= compareBias# driveTcState
                    slice1regs['PerChannelControlConfig4'].EnableTesterCycleDelay -= compareBias #driveTcState
                    #write register changes
                    hpcc.ac[0].WriteChannel(channel, 'PerChannelControlConfig3', slice0regs)
                    for key in slice1regs.keys():
                        hpcc.ac[1].WriteChannel(channel, key, slice1regs[key])

            #initialize step sizes to 0 for preshmoo compare capture
            currentDriveStepSize = 0
            currentCompareStepSize = 0
            while not top:
                for slice in [0, 1]:
                    self.env.WritePattern(slot, slice, 0, pattern[slice])
                    # Setup capture
                    captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                    hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                    hpcc.ac[slice].SetCaptureCounts()
                    hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True, False) #capture all - most vectors should be fails for this test!
                    # Clear fail counts
                    hpcc.ac[slice].Write('TotalFailCount', 0)

                    for channel in range(56):
                        hpcc.ac[slice].WriteChannel(channel, 'ChannelFailCount', 0)

                        confDrive = {'PerChannelControlConfig1': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig1'),'PerChannelControlConfig2': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig2'),'PerChannelControlConfig3': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig3'),'PerChannelControlConfig4': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig4')}
                        if slice == 1:
                            driveQdrOffset = confDrive['PerChannelControlConfig1'].OutputQdrPhaseSelect + currentDriveStepSize

                            driveTcStep = confDrive['PerChannelControlConfig4'].OutputTesterCycleDelay + ((confDrive['PerChannelControlConfig1'].OutputSubCycleDelay + (driveQdrOffset // qdrCount)) // driveSubCycleCount)
                            driveScStep = (confDrive['PerChannelControlConfig1'].OutputSubCycleDelay + (driveQdrOffset // qdrCount)) % driveSubCycleCount
                            driveQdStep =  driveQdrOffset % qdrCount

                            confDrive['PerChannelControlConfig1'].OutputQdrPhaseSelect = driveQdStep
                            confDrive['PerChannelControlConfig1'].EnableQdrPhaseSelect = driveQdStep
                            confDrive['PerChannelControlConfig1'].OutputSubCycleDelay = driveScStep 
                            confDrive['PerChannelControlConfig1'].EnableSubCycleDelay = driveScStep
                            confDrive['PerChannelControlConfig4'].OutputTesterCycleDelay = driveTcStep
                            confDrive['PerChannelControlConfig4'].EnableTesterCycleDelay = driveTcStep

                            if(driveTcStep + driveStepSize > 1022):
                                top = True
                        #if dynamic delay is enabled, only step here, else step delay stack
                        elif(dynamicDelay):
                            #inverse, so subtract step size
                            compareDpStep = confDrive['PerChannelControlConfig3'].InputDynamicPhaseDelay - currentCompareStepSize
                            confDrive['PerChannelControlConfig3'].InputDynamicPhaseDelay = compareDpStep
                            #stop at delay = 0
                            if(compareDpStep - currentCompareStepSize < 0):
                                top = True
                        else:
                            #determine how many QDRs we have to shift
                            qdrOffset = confDrive['PerChannelControlConfig3'].InputSubCycleDelay + currentCompareStepSize

                            #modify delay stack by QDR offset
                            tcStep = confDrive['PerChannelControlConfig4'].InputTesterCycleDelay + (qdrOffset // (qdrCount * compareSubCycleCount))
                            scStep = qdrOffset % (qdrCount * compareSubCycleCount)

                            confDrive['PerChannelControlConfig3'].InputSubCycleDelay = scStep
                            confDrive['PerChannelControlConfig4'].InputTesterCycleDelay = tcStep
                        #write register changes
                        for key in confDrive.keys():
                            hpcc.ac[slice].WriteChannel(channel, key, confDrive[key])

                    hpcc.ac[slice].PrestagePattern()
                if(dynamicDelay):
                    print("Shmoo Compare Delay Stack - Dynamic Delay (inverse) = " + str(compareDpStep))
                    print("Shmoo Drive Delay Stack - TC = " + str(driveTcStep) + " SC = " + str(driveScStep) + " QDR = " + str(driveQdStep))
                elif(count == 50 or top):
                    print("Shmoo Compare Delay Stack - TC = " + str(tcStep) + " SC = " + str(scStep>>2) + " QDR = " + str(scStep&3))
                    print("Shmoo Drive Delay Stack - TC = " + str(driveTcStep) + " SC = " + str(driveScStep) + " QDR = " + str(driveQdStep))
                    count = 0
                else:
                    count += 1

                # Send sync pulse
                self.env.rc.send_sync_pulse()

                # Wait for all slices to complete
                for slice in [0, 1]:
                    completed = hpcc.ac[slice].WaitForCompletePattern()
                    if not completed:
                        self.Log('error', 'Pattern execution did not complete')
                #use checkers to validate first capture
                if goldenCapture is None:
                    self.env.RunMultiSliceCheckers([
                        {'slot': slot, 'slice': 0, 'ratio': ratio},
                        {'slot': slot, 'slice': 1, 'ratio': 1},
                    ])
                    goldenCapture = self.env.ReadAndFlattenCapture(slot, 0)
                    #self.env.DumpCapture(slot, 0)
                #compare remaining captures against first, should all match.
                else:
                    self.env.CheckAlarms(slot, 0)
                    capture = self.env.ReadAndFlattenCapture(slot, 0)
                    failSignature = self.env.CaptureEquality(goldenCapture, capture, checkCaptureCount = False)
                    if failSignature != '':
                        self.Log('error', failSignature)
                #self.env.DumpCapture(slot, 0)
                
                #set to configured step size.  can be changed later for dynamic step sizes
                currentCompareStepSize = compareStepSize
                currentDriveStepSize = driveStepSize

    def _DriveDelayShmooTest(self, period, ratio, compareSubCycleCount, compareStepSize, driveSubCycleCount, driveStepSize, dynamicDelay = False):
        qdrCount = 4
        self.env.SetConfig('LowToHighLoopback')

        periods = [None, None]
        periods[0] = period * ratio
        periods[1] = period
        
        asm = [None, None]
        pattern = [None, None]

        repeats = 150

        asm[0] = PatternAssembler()
        asm[0].symbols['REPEATS'] = repeats
        asm[0].LoadString("""\
            S stype=IOSTATEJAM,             data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        """)

        pattern[0] = asm[0].Generate()

        asm[1] = PatternAssembler()
        asm[1].symbols['LENGTH'] = 10 * ratio * repeats
        asm[1].symbols['RATIO'] = ratio
        asm[1].LoadString("""\
            %repeat RATIO
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %end
            %repeat RATIO
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %end
            %repeat LENGTH
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x89ABCDEF
        """)
        pattern[1] = asm[1].Generate()

        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        goldenCapture = None
        count = 50 #update delay stack status to user every 50 steps
        top = False

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributes(slot, 0, periods[0], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': periods[0] / 3})
            self.env.SetPeriodAndPEAttributes(slot, 1, periods[1], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': periods[1] / 3})

            if(dynamicDelay):
                for channel in range(56):
                    slice0regs = hpcc.ac[0].ReadChannel(channel, 'PerChannelControlConfig1')
                    slice1regs = {'PerChannelControlConfig3': hpcc.ac[1].ReadChannel(channel, 'PerChannelControlConfig3'),'PerChannelControlConfig4': hpcc.ac[1].ReadChannel(channel, 'PerChannelControlConfig4')}

                    driveBias = slice0regs.OutputDynamicPhaseDelay
                    slice0regs.OutputDynamicPhaseDelay -= driveBias
                    slice0regs.EnableDynamicPhaseDelay -= driveBias

                    compareBias = driveBias * compareStepSize

                    #convert input delay stack to QDR count and subtract off the phase delay bias
                    compareQdrState = slice1regs['PerChannelControlConfig4'].InputTesterCycleDelay * compareSubCycleCount * qdrCount + slice1regs['PerChannelControlConfig3'].InputSubCycleDelay
                    compareQdrState -= compareBias
                    #convert back to TC-SC-QDR convention
                    slice1regs['PerChannelControlConfig4'].InputTesterCycleDelay = compareQdrState // (qdrCount * compareSubCycleCount)
                    slice1regs['PerChannelControlConfig3'].InputSubCycleDelay = compareQdrState % (qdrCount * compareSubCycleCount)

                    hpcc.ac[0].WriteChannel(channel, 'PerChannelControlConfig1', slice0regs)
                    for key in slice1regs.keys():
                        hpcc.ac[1].WriteChannel(channel, key, slice1regs[key])

            #initialize step sizes to 0 for preshmoo compare capture
            currentDriveStepSize = 0
            currentCompareStepSize = 0
            while not top:
                for slice in [0, 1]:
                    self.env.WritePattern(slot, slice, 0, pattern[slice])
                    # Setup capture
                    captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                    hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                    hpcc.ac[slice].SetCaptureCounts()
                    hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, True, False, False, False) #capture fails
                    # Clear fail counts
                    hpcc.ac[slice].Write('TotalFailCount', 0)

                    for channel in range(56):
                        hpcc.ac[slice].WriteChannel(channel, 'ChannelFailCount', 0)

                        confDrive = {'PerChannelControlConfig1': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig1'),'PerChannelControlConfig2': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig2'),'PerChannelControlConfig3': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig3'),'PerChannelControlConfig4': hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig4')}
                        if slice == 1:
                            compareQdrOffset = confDrive['PerChannelControlConfig3'].InputSubCycleDelay + currentCompareStepSize
                            #modify delay stack by QDR offset
                            compareTcStep = confDrive['PerChannelControlConfig4'].InputTesterCycleDelay + (compareQdrOffset // (qdrCount * compareSubCycleCount))
                            compareScStep = compareQdrOffset % (qdrCount * compareSubCycleCount)

                            confDrive['PerChannelControlConfig3'].InputSubCycleDelay = compareScStep
                            confDrive['PerChannelControlConfig4'].InputTesterCycleDelay = compareTcStep
                            if(compareTcStep + compareStepSize > 1021):
                                top = True

                        elif(dynamicDelay):
                            driveDpStep = confDrive['PerChannelControlConfig1'].OutputDynamicPhaseDelay + currentDriveStepSize
                            
                            confDrive['PerChannelControlConfig1'].OutputDynamicPhaseDelay = driveDpStep
                            confDrive['PerChannelControlConfig1'].EnableDynamicPhaseDelay = driveDpStep
                            #dynamic phase delay values above 13 produce unreliable results based on last clock state
                            if(driveDpStep + driveStepSize > 13):
                                top = True

                        else:
                            #determine how many QDRs we have to shift
                            qdrOffset = confDrive['PerChannelControlConfig1'].OutputQdrPhaseSelect + currentDriveStepSize

                            #modify delay stack by QDR offset
                            tcStep = confDrive['PerChannelControlConfig4'].OutputTesterCycleDelay + ((confDrive['PerChannelControlConfig1'].OutputSubCycleDelay + (qdrOffset // qdrCount)) // driveSubCycleCount)
                            scStep = (confDrive['PerChannelControlConfig1'].OutputSubCycleDelay + (qdrOffset // qdrCount)) % driveSubCycleCount
                            qdrStep = qdrOffset % qdrCount

                            confDrive['PerChannelControlConfig1'].OutputQdrPhaseSelect = qdrStep
                            confDrive['PerChannelControlConfig1'].EnableQdrPhaseSelect = qdrStep
                            confDrive['PerChannelControlConfig1'].OutputSubCycleDelay = scStep
                            confDrive['PerChannelControlConfig1'].EnableSubCycleDelay = scStep
                            confDrive['PerChannelControlConfig4'].OutputTesterCycleDelay = tcStep
                            confDrive['PerChannelControlConfig4'].EnableTesterCycleDelay = tcStep

                        for key in confDrive.keys():
                            hpcc.ac[slice].WriteChannel(channel, key, confDrive[key])

                    hpcc.ac[slice].PrestagePattern()
                if(dynamicDelay):
                    print("Shmoo Drive Delay Stack - Dynamic Delay = " + str(driveDpStep))
                    print("Shmoo Compare Delay Stack: TC = " + str(compareTcStep) + " SC = " + str(compareScStep>>2) + " QDR = " + str(compareScStep&3))
                elif(count == 50 or top):
                    print("Shmoo Drive Delay Stack: TC = " + str(tcStep) + " SC = " + str(scStep) + " QDR = " + str(qdrStep))
                    print("Shmoo Compare Delay Stack: TC = " + str(compareTcStep) + " SC = " + str(compareScStep>>2) + " QDR = " + str(compareScStep&3))
                    count = 0
                else:
                    count += 1
                # Send sync pulse
                self.env.rc.send_sync_pulse()

                # Wait for all slices to complete
                for slice in [0, 1]:
                    completed = hpcc.ac[slice].WaitForCompletePattern()
                    if not completed:
                        self.Log('error', 'Pattern execution did not complete')
                #use checkers to verify first capture
                if goldenCapture is None:
                    self.env.RunMultiSliceCheckers([
                        {'slot': slot, 'slice': 0, 'ratio': ratio},
                        {'slot': slot, 'slice': 1, 'ratio': 1},
                    ])
                    goldenCapture = self.env.ReadAndFlattenCapture(slot, 1)
                    #self.env.DumpCapture(slot, 1)
                #compare remainder of captures vs first
                else:
                    self.env.CheckAlarms(slot, 1)
                    capture = self.env.ReadAndFlattenCapture(slot, 1)
                    failSignature = self.env.CaptureEquality(goldenCapture, capture, checkCaptureCount = False)
                    if failSignature != '':
                        self.Log('error', failSignature)
                #self.env.DumpCapture(slot, 1)
                
                #set to configured step size, can be used for dynamic step size
                currentDriveStepSize = driveStepSize
                currentCompareStepSize = compareStepSize

    def DirectedCompareTCDelayShmooTest(self):
        period = 2.5e-9

        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 10

        pattern.LoadString("""\
            %var                           enclk=0b00000000000000000001000000010000000100000000000000010000
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0XEX0X0X0XEX0X0X0XEX0X0X0X0X0X0X0XEX0X0  # 0
            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 2, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 3, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 4, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 5, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 6, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 7, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 8, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 9, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 10,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 11,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 12,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 13,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1LEL1L1L1LEL1L1L1LEL1L1L1L1L1L1L1LEL1L1  # 14,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1LEL1L1L1LEL1L1L1LEL1L1L1L1L1L1L1LEL1L1  # 15,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1LEL1L1L1LEL1L1L1LEL1L1L1L1L1L1L1LEL1L1  # 16,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 17,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 18,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 19,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 20,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 21,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 22,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 23,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 24,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 25,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 26,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 27,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 28,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 29,
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        patternSize = len(pdata)
        disabledChannels = [0, 16, 24, 32, 48]
        shmooChannels = [2, 26, 34]
        enabledChannels = [4, 20, 28, 36]
        channelMask = 0
        for channel in disabledChannels:
            channelMask = channelMask | (1 << (channel+1)) # mask the compare channel
        #print(bin(channelMask))
        checkMask = 0
        for channel in shmooChannels:
            checkMask |= 1 << (channel+1)
        
        for (slot, slice) in self.env.fpgas:
            expectedFailAddress = []
            for i in range(pattern.symbols['REPEATS']):
                expectedFailAddress.append(13 + 28*i)
                expectedFailAddress.append(14 + 28*i)
                expectedFailAddress.append(15 + 28*i)
            
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            self.env.SetSelectableFailMask(slot, slice, 1, channelMask)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0, 'compare': period / 3}, True)
            # enabled channels
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': period / 2}, True, enabledChannels)
            # disabled channels
            for channel in disabledChannels:
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig1', 0) # drive channels
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig2', 0)
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig3', 0)
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig4', 0)
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig1', 0) # compare channels
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig2', 0)
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig3', 0)
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig4', 0)
            # shmoo channels      
            compareDelay = period / 3

            for channel in shmooChannels:
                conf4Drive = hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig4')
                conf4Compare = hpcc.ac[slice].ReadChannel(channel+1, 'PerChannelControlConfig4')
                #print(conf4Drive)
                #print(conf4Compare)
                if conf4Drive.OutputTesterCycleDelay  < (conf4Compare.InputTesterCycleDelay * 2):
                    bias = conf4Drive.OutputTesterCycleDelay 
                    conf4Drive.OutputTesterCycleDelay -= bias 
                    conf4Drive.EnableTesterCycleDelay -= bias 
                    conf4Drive.InputTesterCycleDelay -= (bias // 2) 
                    conf4Compare.InputTesterCycleDelay -= (bias // 2) + 1 # subtract extra 1 since loop add 1 to start with
                    # Need to update drive values as well since channel may be in drive mode in the previous pattern
                    # If drive delay > compare delay, pin will be driving the previous pin state value before change to Z for compare
                    conf4Compare.OutputTesterCycleDelay -= bias 
                    conf4Compare.EnableTesterCycleDelay -= bias 
                    
                    # subtract 0.5 TC less, therefore, compared 0.5 TC later
                    # doesn't matter, since this is not an EC channel
                    #if bias % 2 != 0:
                    #    hpccModel.acSim[slice].SetCompareDelay([channel+1], period * 2 / 3)
                    
                    hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig4', conf4Drive)
                    hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig4', conf4Compare)


                else:
                    self.Log('error', 'To be implemented - drive delay {} >= 2 * compare delay {}, double due to ratio 1 EC'.format(conf4Drive.OutputTesterCycleDelay, conf4Compare.InputTesterCycleDelay))
            
            self.Log('info', 'shmoo starting InputTesterCycleDelay = {}'.format(conf4Compare.InputTesterCycleDelay + 1))  
            top = False
            goldenCapture = None
            while not top:
                for channel in shmooChannels:
                    conf4Compare = hpcc.ac[slice].ReadChannel(channel+1, 'PerChannelControlConfig4')
                    conf4Compare.InputTesterCycleDelay += 1
                    hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig4', conf4Compare)
                    # if one channel reaches the top, stop looping
                    if conf4Compare.InputTesterCycleDelay == 1021:
                        top = True
                    
                #self.Log('info', expectedFailAddress)
                self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)  
                
                # use simulator to check all other channels except for the ones we are shmooing
                if goldenCapture is None:
                    self.env.RunCheckers(slot, slice, channelsToMask = checkMask)
                    goldenCapture = self.env.ReadAndFlattenCapture(slot, slice)
                else:
                    self.env.CheckAlarms(slot, slice)
                    capture = self.env.ReadAndFlattenCapture(slot, slice)
                    failSignature = self.env.CaptureEquality(goldenCapture, capture, pinLaneDataMask = checkMask)
                    if failSignature != '':
                        self.Log('error', failSignature)
                
                # check channels under shmoo
                captureData = hpcc.ac[slice].ReadCapture()
                #self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            actual = data[i].PinLaneData
                            vaddr = data[i].VectorAddress
                            for channel in shmooChannels:
                                # checking channel +1 = 1
                                if vaddr in expectedFailAddress:
                                    if not ((actual >> channel+1) & 1):                                        
                                        actualString = '{:056b}'.format(actual)
                                        vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                                        self.Log('error', 'InputTesterCycleDelay = {}, expect channel {} to fail at vector address {}, but did not fail'.format(conf4Compare.InputTesterCycleDelay, channel+1, vaddr)) 
                                        self.Log('error', str(vaddr) + "\t" + vectorString) 
                                else: # checking channel +1 = 0
                                    if ((actual >> channel+1) & 1):                                        
                                        actualString = '{:056b}'.format(actual)
                                        vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                                        self.Log('error', 'InputTesterCycleDelay = {}, expect channel {} to pass at vector address {}, but did not pass'.format(conf4Compare.InputTesterCycleDelay, channel+1, vaddr)) 
                                        self.Log('error', str(vaddr) + "\t" + vectorString) 
                
                # update expected fail address
                newFailAddress = []
                for addr in expectedFailAddress:
                    if addr > 1: # since addr 0 is IO Jam XZ
                        newFailAddress.append(addr - 1)
                expectedFailAddress = newFailAddress
                
            self.Log('info', 'shmoo ending InputTesterCycleDelay = {}'.format(conf4Compare.InputTesterCycleDelay))  
            # restore mask
            self.env.SetSelectableFailMask(slot, slice, 1, 0)
            
    # assume Compare TC delay is good, move it with Drive TC delay
    def DirectedDriveTCDelayShmooTest(self):
        period = 2.5e-9
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 10

        pattern.LoadString("""\
            %var                           enclk=0b00000000000000000001000000010000000100000000000000010000
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0XEX0X0X0XEX0X0X0XEX0X0X0X0X0X0X0XEX0X0  # 0
            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 2, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 3, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 4, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 5, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 6, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 7, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 8, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 9, 
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 10,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 11,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 12,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 13,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1LEL1L1L1LEL1L1L1LEL1L1L1L1L1L1L1LEL1L1  # 14,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1LEL1L1L1LEL1L1L1LEL1L1L1L1L1L1L1LEL1L1  # 15,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1LEL1L1L1LEL1L1L1LEL1L1L1L1L1L1L1LEL1L1  # 16,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 17,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 18,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 19,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 20,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 21,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 22,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 23,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 24,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 25,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 26,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 27,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 28,
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0LEL0L0L0LEL0L0L0LEL0L0L0L0L0L0L0LEL0L0  # 29,
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        patternSize = len(pdata)
        disabledChannels = [0, 16, 24, 32, 48]
        shmooChannels = [2, 26, 34]
        enabledChannels = [4, 20, 28, 36]
        channelMask = 0
        for channel in disabledChannels:
            channelMask = channelMask | (1 << (channel+1)) # mask the compare channel
        #print(bin(channelMask))
        
        for (slot, slice) in self.env.fpgas:            
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.SetSelectableFailMask(slot, slice, 1, channelMask)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0, 'compare': period / 3}, True)
            # enabled channels
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': period / 2}, True, enabledChannels)
            # disabled channels
            for channel in disabledChannels:
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig1', 0) # drive channels
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig2', 0)
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig3', 0)
                hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig4', 0)
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig1', 0) # compare channels
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig2', 0)
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig3', 0)
                hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig4', 0)
            # shmoo channels    
            moveInput = {}

            for channel in shmooChannels:
                conf4Drive = hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig4')
                conf4Compare = hpcc.ac[slice].ReadChannel(channel+1, 'PerChannelControlConfig4')   
                #print(conf4Drive)
                #print(conf4Compare)
                if conf4Drive.OutputTesterCycleDelay  < (conf4Compare.InputTesterCycleDelay * 2):
                    bias = conf4Drive.OutputTesterCycleDelay 
                    conf4Drive.OutputTesterCycleDelay -= bias # start at 0
                    conf4Drive.EnableTesterCycleDelay -= bias # start at 0
                    conf4Drive.InputTesterCycleDelay -= (bias // 2) # start at 0
                    conf4Compare.InputTesterCycleDelay -= (bias // 2) 
                    conf4Compare.OutputTesterCycleDelay -= bias
                    conf4Compare.EnableTesterCycleDelay -= bias
                    hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig4', conf4Drive)
                    hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig4', conf4Compare)

                else:
                    self.Log('error', 'To be implemented - drive delay {} >= 2 * compare delay {}, double due to ratio 1 EC'.format(conf4Drive.OutputTesterCycleDelay, conf4Compare.InputTesterCycleDelay))
                if bias % 2 != 0:
                    moveInput[channel] = False
                else:
                    moveInput[channel] = True
            
            self.Log('info', 'shmoo starting OutputTesterCycleDelay = {}'.format(conf4Drive.OutputTesterCycleDelay))  
            top = False
            goldenCapture = None
            while not top:
                for channel in shmooChannels:
                    conf4Drive = hpcc.ac[slice].ReadChannel(channel, 'PerChannelControlConfig4')
                    conf4Compare = hpcc.ac[slice].ReadChannel(channel+1, 'PerChannelControlConfig4')
                    conf4Drive.OutputTesterCycleDelay += 1
                    conf4Drive.EnableTesterCycleDelay += 1
                    if moveInput[channel]:
                        # input TC delay is tested in DirectedCompareTCDelayShmooTest, okay to not go to top here
                        conf4Compare.InputTesterCycleDelay += 1
                        moveInput[channel] = False
                    else:
                        moveInput[channel] = True
                    hpcc.ac[slice].WriteChannel(channel, 'PerChannelControlConfig4', conf4Drive)
                    hpcc.ac[slice].WriteChannel(channel+1, 'PerChannelControlConfig4', conf4Compare)
                    #print(conf4Drive)
                    #print(conf4Compare)

                    # if one channel reaches the top, stop looping
                    # max valid = 1021, increase to 1021 after change to by 1 step
                    if conf4Drive.OutputTesterCycleDelay >= 1021:
                        top = True
                    
                #self.Log('info', expectedFailAddress)
                self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)  
                
                # use simulator to check all other channels except for the ones we are shmooing
                if goldenCapture is None:
                    self.env.RunCheckers(slot, slice)
                    goldenCapture = self.env.ReadAndFlattenCapture(slot, slice)
                else:
                    self.env.CheckAlarms(slot, slice)
                    capture = self.env.ReadAndFlattenCapture(slot, slice)
                    failSignature = self.env.CaptureEquality(goldenCapture, capture)
                    if failSignature != '':
                        self.Log('error', failSignature)
                #self.env.DumpCapture(slot, slice)
                
            self.Log('info', 'shmoo ending OutputTesterCycleDelay = {}'.format(conf4Drive.OutputTesterCycleDelay))  
            # restore mask
            self.env.SetSelectableFailMask(slot, slice, 1, 0)

    def DirectedDriveScQdDelayShmooTest(self):
        #hardcode to run at specific parameters
        #TODO: update to run dynamically based on MaxPeriod and quarterDomainCycle
        #slice1 period, ratio, compare (slice1) sub cycle count, compare (slice1) step size in QD, drive (slice0) sub cycle count, drive (slice0) step size 
        self._DriveDelayShmooTest(1.25e-9, 128, 1, 4, 32, 1)

    def DirectedDriveDpDelayShmooTest(self):
        #hardcode to run at specific parameters
        #TODO: update to run dynamically based on MaxPeriod and quarterDomainCycle
        #slice1 period, ratio, compare (slice1) sub cycle count, compare (slice1) step size in QD, drive (slice0) sub cycle count, drive (slice0) step size, enable dynamic delay test 
        self._DriveDelayShmooTest(1.25e-9, 128, 1, 4, 32, 1, True)

    def DirectedCompareScQdDelayShmooTest(self):
        #hardcode to run at specific parameters
        #TODO: update to run dynamically based on MaxPeriod and quarterDomainCycle
        self._CompareDelayShmooTest(1.25e-9, 128, 1, 4, 32, 1)

    def DirectedCompareDpDelayShmooTest(self):
        #hardcode to run at specific parameters
        #TODO: update to run dynamically based on MaxPeriod and quarterDomainCycle
        #slice1 period, ratio, drive (slice1) sub cycle count, drive (slice1) step size in QD, compare (slice0) sub cycle count, compare (slice0) step size, enable dynamic delay test 
        self._CompareDelayShmooTest(1.25e-9, 128, 1, 4, 32, 1, True)

class ACTiming(HpccTest):
    def Sync9914(self):
        with open('sync.csv', 'w') as output:
            for i in range(20):
                for slot in self.env.instruments:
                    slice = 0
                    hpcc = self.env.instruments[slot]
                    
                    
                    #hpcc.Initialize()
                    hpcc.ac[slice].FpgaLoad(hpcc.ac0Image[0])
                    hpcc.ac[slice]._UpdateFpgaCapabilities()
                    hpcc._PerformClockSync()                   
                    #hpcc.ac[slice]._InitACFPGA()                
                    # check reset and status reg after init
                    statusReg = hpcc.ac[slice].Read('ResetsAndStatus')
                    if (statusReg.PLLLocks != 0b1111):
                        self.Log('error', "Incorrect PLLLocks bits after initialize hpcc {} slice {} (actual = 0x{:X}, expected = 0x1111).".format(self.slot, slice, statusReg.PLLLocks))
                    if (statusReg.DDR3ControllerInitialized != 0b1111):
                        self.Log('error', "Incorrect DDR3ControllerInitialized bits after initialize hpcc {} slice {} (actual = 0x{:X}, expected = 0x1111).".format(self.slot, slice, statusReg.DDR3ControllerInitialized))
                    if (slice == 0) and ((statusReg.Lmk04808StatusLd != 1) or (statusReg.Lmk04808StatusClkIn0 != 1) or (statusReg.Lmk04808StatusClkIn1 != 1)):
                        self.Log('error', "Incorrect Lmk04808Status bits after initialize hpcc {} slice {} (actual = 0x{:X}).".format(self.slot, slice, statusReg.Pack()))
                            
                    
                    hpcc.ac[slice].ad9914.SetPeriod(8e-9)
                    hpcc.ac[slice].ad9914.EnableOutput(False)
                    hpcc.ac[slice].ad9914.EnableOutput(True)
                    states = []            
                    for j in range(100):
                        state = hpcc.ac[slice].ad9914.RunAndGetDistribution(j+1, hpcc.ac[slice].lmk01000)
                        states.append(str(state))
                        # self.env.rc.SendSyncPulse() # for scope
                    print(states)
                    output.write('{}_{}_run{},{}\n'.format(slot, slice,i,','.join(states)))
                    
                    statusReg = hpcc.ac[slice].Read('ResetsAndStatus')
                    for k in range(20):
                        if statusReg.Lmk04808StatusLd != 1 or statusReg.Lmk04808StatusClkIn0 != 1 or statusReg.Lmk04808StatusClkIn1 != 1:
                            self.Log('info', 'Initialized HPCC AC0 LMK04808 on slot {} AC FPGA 0. Lmk04808StatusLd: 0x{:X}, Lmk04808StatusClkIn0: 0x{:X}, Lmk04808StatusClkIn1: 0x{:X}. Total times to try {}. Tried {} times.'.format(self.slot, statusReg.Lmk04808StatusLd, statusReg.Lmk04808StatusClkIn0, statusReg.Lmk04808StatusClkIn1, self.NUMBER_TRIES, self.NUMBER_TRIES - tries))
                            self.Log('error', 'Program Lmk04808 Failed')
                
    
    def _getSlewRateMask(self, startChannel):
        mask = 0b11111111111111111111111111111111111111111111111111111100
        for i in range(2, startChannel, 2):
            mask = ((mask << 2) | 0b11) & 0xffffffffffffff
        return mask
        
    def _geSlewRatePattern(self, oddCompare):
        pattern = PatternAssembler()
        if oddCompare:
            pattern.LoadString("""\
                PATTERN_START:                                                                                   
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
                %end
                V link=0, ctv=0, mtv=1, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1  
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            """)   
        else:
            pattern.LoadString("""\
                PATTERN_START:                                                                                   
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
                %repeat 100                                                                                    
                V link=0, ctv=0, mtv=1, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L  
                %end                                                                                          
                V link=0, ctv=0, mtv=1, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H  
                %repeat 100                                                                                    
                V link=0, ctv=0, mtv=1, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L  
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
            """)   
        pdata = pattern.Generate() 
        return pdata
    
    def SlewRate(self):
        self.env.SetConfig('EvenToOddLoopback')
        ### configure  ###
        slot = 4
        slice = 0
        oddCompare = True
        startChannel = 1
        endChannel = 55
        loop = 10
        ##################
        if oddCompare:
            if not startChannel % 2 == 1:
                self.Log('error', 'startChannel = {}, expect an odd channel'.format(startChannel))
            if not endChannel % 2 == 1:
                self.Log('error', 'endChannel = {}, expect an odd channel'.format(endChannel))
        else:
            if not startChannel % 2 == 0:
                self.Log('error', 'startChannel = {}, expect a even channel'.format(startChannel))
            if not endChannel % 2 == 0:
                self.Log('error', 'endChannel = {}, expect a even channel'.format(endChannel))
                
                
        pdata = self._geSlewRatePattern(oddCompare)        
        hpcc = self.env.instruments[slot]
        self.env.WritePattern(slot, slice, 0, pdata)
        hpcc.ac[slice].captureAll = False        
        hpcc.ac[slice].SetupCapture(0x20000, ac_registers.CaptureControl.FAIL_STATUS)
        
        hpcc.ac[slice]._UpdateFpgaCapabilities()
        period = hpcc.ac[slice].minQdrMode / PICO_SECONDS_PER_SECOND   
        hpcc.ac[slice].SetPeriod(period)   
        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': period / 2, 'compare': 0.0}, True)
        
        resultDict = dict()
        
        for repeat in range(loop):
            mask = self._getSlewRateMask(startChannel)            
            for pin in range(startChannel,endChannel+1,2):
                print('{:056b}'.format(mask))
                if not pin in resultDict:
                    resultDict[pin] = dict()
                self.env.SetSelectableFailMask(slot, slice, 1, mask)
                stepSize = hpcc.ac[slice].nominalFineDelayValue * 1e-12
                offset = period / 4
                compare = offset
                compareList = []
                while (compare <= (period * 1.75)) :           
                    if oddCompare:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': period / 2, 'compare': compare}, True, channels = [pin, pin-1])
                    else:
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': period / 2, 'compare': compare}, True, channels = [pin, pin+1])
                    hpcc.ac[slice].ExecutePattern()
                    captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                    '''
                    captureData = hpcc.ac[slice].ReadCapture()
                    for block in hpcc.ac[slice].UnpackCapture(captureData):
                        header, data = block
                        for i in range(8):
                            if header.CapturedVectorsValidFlags[i] == 1:
                                actual = data[i].PinLaneData
                                vaddr = data[i].VectorAddress
                                actualString = '{:056b}'.format(actual)
                                vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                                #if vaddr < 105 and vaddr > 95:
                                print(str(vaddr) + "    " + vectorString) 
                    '''
                    if captureCount == 0:
                        result = True
                        if round(compare*1e12) in resultDict[pin]:
                            resultDict[pin][round(compare*1e12)] += 1
                        else:
                            resultDict[pin][round(compare*1e12)] = 1
                    else:
                        result = False
                        if round(compare*1e12) not in resultDict[pin]:
                            resultDict[pin][round(compare*1e12)] = 0
                    compareList.append(round(compare*1e12))
                    print("{},{},{},{}".format(repeat, pin, compare*1e9, result))
                    compare += stepSize
                    
                mask = ((mask << 2) | 0b11) & 0xffffffffffffff
            
        self.env.SetSelectableFailMask(slot, slice, 1, 0)
        hpcc.ac[slice].captureAll = True
        
        # output result
        resultCSV = open('slewRate.csv', 'w')
        resultCSV.write(',{}\n'.format(','.join(str(i) for i in compareList)))
        for pin in resultDict:
            resultCSV.write('{},{}\n'.format(pin, ','.join(str(resultDict[pin][compare]) for compare in compareList)))
        resultCSV.close()

    
    def MiniTimingWalkingOneCKShmooTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pdata = self._WalkingOnePatternShmoo()

        periodBase = 10e-9        
        CompareBase = 1e-9
        periodRange = 10
        compareRange = 20
        driveDelay = 5.0e-9
        #schmooResult ={}
        shmooPassFail = {}
        for (slot, slice) in self.env.fpgas:
            schmooResult = {}
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]

            self.env.WritePattern(slot, slice, 0, pdata)
            hpcc.ac[slice].SetupCapture(0x20000, ac_registers.CaptureControl.PIN_STATE) #FAIL_STATUS)
            hpcc.ac[slice].captureAll=False
            passFailResult = ''
            for i in range(periodRange):
                period = periodBase + i * 1e-9
                periodKey = int( period*1e9 )

                self.env.SetPeriodACandSim(slot, slice, period)
                #print(int(period * 1e9))
                compareResult = []
                for j in range(compareRange):
                    compare = CompareBase * j
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': driveDelay, 'compare': compare}, True)
                    #try:
                    
                    completed = hpcc.ac[slice].ExecutePattern()

                    hpcc.ac[slice].Write('TotalFailCount', 0)
                    for ch in range(56):
                        hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)

                    captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
                    if captureCount == 0:
                       result = 'True'
                    else:
                       result = 'False'
                    #print('{},{}'.format(compare, result))
                    #self.env.DumpCapture(slot, slice, limit = 50)
                    
                    #simResult = self.env.RunCheckers(slot, slice)
                    #result = str( simResult[0] )
                    #alarms = simResult[1]
                    compareResult.append( self._PassFailConversion(result) )
                    if completed == False:
                    #except Exception as e:
                        #print(e)
                        result = 'exception' 
                        compareResult.append( self._PassFailConversion(result) )
                compareResultString = ''.join(compareResult)
                self.Log('info','{}, {}'.format(periodKey, compareResultString))
                schmooResult.setdefault( ( periodKey), compareResultString )
            self.Log('info', ' slot{} slice{}: {} '.format(slot, slice, schmooResult) )
            processShmoo = self._ProcessShmoo(schmooResult)
            #print(processShmoo)
            shmooPassFail.setdefault( (slot, slice), processShmoo)
        #print(shmooPassFail)
        if all( list( shmooPassFail.values() ) ): 
            self.Log('info', 'Shmoo Wall test Passing!')
        else:
            self.Log('error', 'Something Wrong with Shmoo Wall and needs more investigation')
                    

    def _ProcessShmoo(self, shmooResult, pattern=r'^000000111'):
        # Logic to  process the Shmoo result only for one slice
        expectedPattern = re.compile(pattern)
        containerForPassFail = []
        for i in shmooResult.values():
            match = expectedPattern.search(i)
            if match is not None: containerForPassFail.append(True)
            else: containerForPassFail.append(False)
            #put here to add logic to process regular expression based on the expected result
            # add the processed pass(True)/fail(False) in the containerForPassFail
        return all( containerForPassFail ) # Pass/Fail for one slice

        # Procee the whole slot/slice combination
        #for key, value in d.items():
            # print out the result for Slot/Slice using the function above.



    def _PassFailConversion(self, result):
        if result == 'True':
            return '1'
        elif result == 'False':
            return '0'
        else:
            return 'X'

    #def _SchmooWallCheck(self, period, compare, result):

    def _WalkingOnePatternShmoo(self):
        pattern = PatternAssembler()
        pattern.symbols['DUMMY'] = 20  # 1021
        pattern.symbols['REPEATS'] = 20  # 1021
        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 0

            %repeat DUMMY
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            %end

            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0L0  # 17,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0L0  # 18,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0L0  # 19,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0L0  # 20,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0L0  # 21,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0L0  # 22,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0L0  # 23,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0L0  # 24,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0L0  # 25,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0L0  # 26,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0L0  # 27,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1L0  # 28,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1  # 29,
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        
        return pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 

class SELatency(HpccTest):
    # get the cycle count for the first vector
    def _GetCaptureCycle(self, slot, slice):
        hpcc = self.env.instruments[slot]
        captureData = hpcc.ac[slice].ReadCapture()
        self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
        for block in hpcc.ac[slice].UnpackCapture(captureData):
            header, data = block
            for i in range(8):
                if header.CapturedVectorsValidFlags[i] == 1:
                    totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                    indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                    return indCycleCount   

    def _RunSTDStickyErrorLatencyPattern(self, slot, slice, period, se, latencyPadding):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.symbols['PADDING_BEFORE'] = random.randint(0,20000)
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            Drive0CompareLVectors length=20000
            Drive0CompareLVectors length=PADDING_BEFORE  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            Drive0CompareLVectors length={}
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm=3000
            
            Drive0CompareLVectors length=1 
            I optype=BRANCH, br=GOTO_I, cond=COND.{}, imm=`PATTERN_END`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdead     
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0            
            PATTERN_END:  
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xaaaa                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """.format(latencyPadding, se))
        pdata = pattern.Generate()   
        
        hpcc = self.env.instruments[slot]   
        triggerControlReg = hpcc.ac[slice].Read('TriggerControl')
        triggerControlReg.DomainMaster = 1
        triggerControlReg.DomainID = slot
        hpcc.ac[slice].Write('TriggerControl', triggerControlReg)
        
        completed, endStatus = self.env.RunPattern(slot, slice, pdata, period, captureAll = False, captureFails = True)
        self.env.RunCheckers(slot, slice, endStatusMask = 0xFFFF)
        return (endStatus == 0xaaaa)      

    def _RunMTDStickyErrorLatencyPattern(self, slot, period, se, latencyPadding):
        self.env.SetConfig('EvenToOddLoopback')
        paddingBefore = random.randint(0,20000)
        patternFail = PatternAssembler()
        patternFail.symbols['PADDING_BEFORE'] = paddingBefore
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            Drive0CompareLVectors length=16000                                              
            Drive0CompareLVectors length=PADDING_BEFORE                                             
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            Drive0CompareLVectors length={}
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm=3000
            
            Drive0CompareLVectors length=1 
            I optype=BRANCH, br=GOTO_I, cond=COND.{}, imm=`PATTERN_END`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdead     
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0            
            PATTERN_END:  
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xaaaa                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """.format(latencyPadding, se))
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.symbols['PADDING_BEFORE'] = paddingBefore
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            Drive0CompareLVectors length=16000                                              
            Drive0CompareLVectors length=PADDING_BEFORE                                             
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            Drive0CompareLVectors length={}
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm=3000
            
            Drive0CompareLVectors length=1 
            I optype=BRANCH, br=GOTO_I, cond=COND.{}, imm=`PATTERN_END`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdead     
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0            
            PATTERN_END:  
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xaaaa                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """.format(latencyPadding, se))
        pPass = patternPass.Generate() 
        
        hpcc = self.env.instruments[slot]
        passed = True
        for slice in [0,1]:
            triggerControlReg = hpcc.ac[slice].Read('TriggerControl')
            # multi-slice for DSE, MTD for GSE
            if slice == 0 and se != 'GSE':
                triggerControlReg.DomainMaster = 0
            else:
                triggerControlReg.DomainMaster = 1
            if se != 'GSE':
                triggerControlReg.DomainID = slot
            else:
                triggerControlReg.DomainID = slice + 1
            hpcc.ac[slice].Write('TriggerControl', triggerControlReg)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            if slice == 0:
                self.env.WritePattern(slot, slice, 0, pFail)
            else:
                self.env.WritePattern(slot, slice, 0, pPass)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # capture fail
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern()
  
        self.env.rc.send_sync_pulse()
                
        # Wait for all slices to complete
        for slice in [0, 1]:
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                passed = False
                self.Log('error', 'slot {} slice {} Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
            
            if hpcc.ac[slice].Read('PatternEndStatus').Pack() != 0xaaaa:
                self.Log('warning', 'slot {} slice {} pattern end status mismatch, actual = 0x{:X}, expected 0xAAAA'.format(slot, slice, hpcc.ac[slice].Read('PatternEndStatus').Pack()))
                passed = False                

        # Run checkers
        self.env.RunMultiSliceCheckers([
            {'slot': slot, 'slice': 0, 'endStatusMask': 0xFFFF},
            {'slot': slot, 'slice': 1, 'endStatusMask': 0xFFFF},
        ])
        
        return passed    
        
    def _RunStickyErrorLatencyShmoo(self, slot, slice, se, STD):
        hpcc = self.env.instruments[slot]   
        periodList = []
        result = dict()
        period = 1.25e-9
        while period < (hpcc.ac[slice].maxPeriod / PICO_SECONDS_PER_SECOND):
            periodList.append(period)
            period *= 2   
        
        for period in periodList:
            paddingStart = 2000
            paddiingStep = 200
            
            passed = False
            padding = paddingStart
            stableCount = 0
            for loop in range(500):
                if stableCount == 5:
                    break
                if loop == 499:
                    self.Log('error', 'Timed out')
                if STD:
                    passed = self._RunSTDStickyErrorLatencyPattern(slot, slice, period, se, padding)
                else:
                    passed = self._RunMTDStickyErrorLatencyPattern(slot, period, se, padding)
                if passed:
                    self.Log('info', '{} period {} latency {} cycles passed'.format(se, period, padding))
                    stableCount += 1
                else:
                    self.Log('warning', '{} period {} latency {} cycles failed'.format(se, period, padding))
                    padding += paddiingStep
                    stableCount = 0
            result[period] = padding
            
            #print(hpcc.ac[slice].ReadChannel(0,'PerChannelControlConfig4'))
            #print(hpcc.ac[slice].ReadChannel(1,'PerChannelControlConfig4'))
        
        return periodList, result
    
    # MTD no applicable
    def DirectedSTDLSELatencyTest(self):   
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            periodList, result = self._RunStickyErrorLatencyShmoo(slot, slice, 'LSE', STD = True)
            
            for period in periodList:
                self.Log('info', 'period = {}, latency = {} cycles'.format(period, result[period]))
                
            
    def DirectedSTDDSELatencyTest(self):
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            periodList, result = self._RunStickyErrorLatencyShmoo(slot, slice, 'DSE', STD = True)
            
            for period in periodList:
                self.Log('info', 'period = {}, latency = {} cycles'.format(period, result[period]))
    
    def DirectedSTDGSELatencyTest(self):
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            periodList, result = self._RunStickyErrorLatencyShmoo(slot, slice, 'GSE', STD = True)
            
            for period in periodList:
                self.Log('info', 'period = {}, latency = {} cycles'.format(period, result[period]))
    
    def DirectedMultiSliceDSELatencyTest(self):
        for (slot, slice) in self.env.fpgas:
            if slice == 0: # test per slot
                self.env.LogPerSlot(slot)
                periodList, result = self._RunStickyErrorLatencyShmoo(slot, slice, 'DSE', STD = False)
                
                for period in periodList:
                    self.Log('info', 'period = {}, latency = {} cycles'.format(period, result[period]))
                    
    def DirectedMTDGSELatencyTest(self):
        for (slot, slice) in self.env.fpgas:
            if slice == 0: # test per slot
                self.env.LogPerSlot(slot)
                periodList, result = self._RunStickyErrorLatencyShmoo(slot, slice, 'GSE', STD = False)
                
                for period in periodList:
                    self.Log('info', 'period = {}, latency = {} cycles'.format(period, result[period]))                
                        