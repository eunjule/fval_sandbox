################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: AluOps.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for the ALU instructions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Sungjin Ho
#        Date: 08/15/15
#       group: hdmt fpga validation
################################################################################

from itertools import product
import random
import time

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest

class AluOps(HpccTest):

    # Test Case Start
    def MiniXORRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniXORRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniXORRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniXORRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniXORRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniXORRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniXORFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniXORFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniXORFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniXORFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniXORFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniXORFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'XOR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniANDRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniANDRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniANDRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniANDRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniANDRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniANDRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniANDFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniANDFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniANDFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniANDFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniANDFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniANDFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'AND', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniORRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniORRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniORRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniORRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniORRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniORRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniORFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniORFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniORFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniORFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniORFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniORFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'OR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHLRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHLRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHLRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHLRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHLRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHLRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHLFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHLFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHLFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHLFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHLFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHLFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHL', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHRRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHRRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHRRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHRRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHRRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHRRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHRFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHRFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHRFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniLSHRFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniLSHRFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniLSHRFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'LSHR', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniADDRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniADDRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniADDRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniADDRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniADDRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniADDRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniADDFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniADDFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniADDFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniADDFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniADDFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniADDFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'ADD', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniSUBRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniSUBRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniSUBRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniSUBRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniSUBRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniSUBRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniSUBFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniSUBFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniSUBFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniSUBFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniSUBFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniSUBFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'SUB', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniMULRARBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'RA_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniMULRARBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'RA_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniMULRARBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'RA_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniMULRAIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'RA_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniMULRAIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'RA_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniMULRAIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'RA_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniMULFLAGSIToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniMULFLAGSIToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniMULFLAGSIToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'FLAGS_I', destOp = 'NONE')
        self.RunAluTest(asmSource)

    def MiniMULFLAGSRBToREGDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'REG')
        self.RunAluTest(asmSource)

    def MiniMULFLAGSRBToFLAGSDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'FLAGS')
        self.RunAluTest(asmSource)

    def MiniMULFLAGSRBToNONEDestTest(self):
        asmSource = self.AluPatGen(aluOp = 'MUL', regRange = 32, sourceOp = 'FLAGS_RB', destOp = 'NONE')
        self.RunAluTest(asmSource)


    # Test Case End
    def DebugAluOps(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        source = """\
            StartPattern
            RandomVectors length=256
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm=0
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=`rand(32)`
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=1, imm=`rand(32)`
            I optype=ALU, aluop=MUL, opsrc=ALUSRC.RA_RB, opdest=ALUDEST.REG, regA=0, regB=1, imm=`rand(32)`, dest=2
            StopPattern 0xdeadbeef
            """
        asm.LoadString(source)
        pattern = asm.Generate()
        asm.SaveObj('Mul_Src_RARB_Des_REG.obj')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern)
            hpcc = self.env.instruments[slot]
            print("regF = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegisterFlags').Pack()) )
            print("reg1 = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegister1').Pack()) )
            print("reg0 = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegister0').Pack()) )
            print("reg2 = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegister2').Pack()) )
            self.env.RunCheckers(slot, slice)
    
    def AluTestCaseGen(self):
        """Dummy function to Print out ALU Test Cases
        Maybe need to consider exec() for dynamic scriptting
        """
        aluTest = [ "'XOR'", "'AND'", "'OR'", "'LSHL'", "'LSHR'", "'ADD'", "'SUB'", "'MUL'" ]
        aluSource = [ "'RA_RB'"  , "'RA_I'", "'FLAGS_I'", "'FLAGS_RB'"  ]
        aluDest = [ "'REG'", "'FLAGS'", "'NONE'" ]
        regRange = 32 
        aluComb = product(aluTest, aluSource, aluDest)
        patGenSource = "        asmSource = self.AluPatGen(aluOp = {0}, regRange = {1}, sourceOp = {2}, destOp = {3})\n"
        runPatSource = "        self.RunAluTest(asmSource)\n"
        funcNameSource = "    def Mini{0}{1}To{2}DestTest(self):\n"
        for testcase in aluComb:
            totalSource = funcNameSource.format(testcase[0].replace("'",''), testcase[1].replace("_",'').replace("'",''), testcase[2].replace("'",'') ) +\
            patGenSource.format( testcase[0], regRange , testcase[1], testcase[2] ) +\
            runPatSource
            print(totalSource)

    def AluPatGen(self, aluOp='XOR', regRange=5, sourceOp='RA_RB', destOp='REG'):
        source = """\
StartPattern
RandomVectors length=256
"""
        regAddrRange = range(0, regRange)
        regComb = product( regAddrRange, repeat=2)
        for i, j in regComb:
            source += """\
I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={0}, imm=`rand(32)`
I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={1}, imm=`rand(32)`
I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm=`rand(32)`
I optype=ALU, aluop={2}, opsrc=ALUSRC.{3}, opdest=ALUDEST.{4}, regA={0}, regB={1}, imm=`rand(32)`, dest=`rand(5)`
RandomVectors length=1
""".format(i, j, aluOp, sourceOp, destOp)
        source += 'StopPattern 0xdeadbeef'
        return source

    def RunAluTest(self, source):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString(source)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            #try:
            self.env.RunPattern(slot, slice, pattern)
                #hpcc = self.env.instruments[slot]
                #print("reg0 = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegisterFlags').Pack()) )
                #print("reg1 = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegister1').Pack()) )
                #print("reg2 = {0:#034b}".format(hpcc.ac[slice].Read('CentralDomainRegister2').Pack()) )
                #self.env.RunCheckers(slot, slice, checkRegisters=True)
            self.env.RunCheckers(slot, slice, checkRegisters=False)

            #except Exception as e:
            #    self.Log('error', str(e)) #print(e)
            #    asm.SaveObj('ALU_Exception_{}.obj'.format(time.time()) )
                
    def RandomALUTest(self):
        for (slot, slice) in self.env.fpgas:
            self.env.ClearRegisters(slot, slice)
            
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [1, 1], 'LEN_PAT': [1000, 2000], 'ALU': 0.01}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        source = p.GetPattern()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('alu.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            result = self.env.RunCheckers(slot, slice, checkRegisters=True)  
            if result[0]==False and slice==0:
                fout = open('AluRandom_{}.pat'.format(time.time()), 'w')
                fout.write(source)
                fout.close()
                pattern.SaveObj('AluRandom_{}.obj'.format(time.time()) )


class ReportedDefects(HpccTest):
    def DirectedIntermittentStackErrorTest(self):
        '''Detects issue identified in TFS Ticket 92865'''
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString('''\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            %repeat 30000
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, imm=1
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                V data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, imm=1
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        ''')
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            capture_address = self.env.instruments[slot].ac[slice].SinglePatternCaptureBaseAddress(len(pattern))
            self.env.LogPerSlice(slot, slice)
            self.env.WritePattern(slot, slice, offset=0, pattern=pattern)
            for i in range(1, 11):
                self.Log('info', 'Iteration {}'.format(i))
                self.env.RunPattern(slot, slice, pattern=None, captureAddress=capture_address)
                self.env.RunCheckers(slot, slice, checkRegisters=False)
                
    

