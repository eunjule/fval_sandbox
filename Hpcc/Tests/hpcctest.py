# Copyright 2017 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

from Common.fval import TestCase
import Hpcc.instrument.hpccAcRegs as ac_registers
from Hpcc.hpcctb.env import Env
import traceback

class HpccTest(TestCase):
    dutName = 'hpcc'
    
    def setUp(self):
        try:
            self.LogStart()
            self.env = Env(self)
            for (slot, slice) in self.env.fpgas:
                self.env.ClearMTVFailMask(slot, slice)
                hpcc = self.env.instruments[slot]
                hpcc.dc[slice].SetPEAttributesWithZeroDelay({'IS_ENABLED_CLOCK': False, 'FIXED_DRIVE_STATE': 'Off'}, apply=True)
                trigger_control = ac_registers.TriggerControl()
                trigger_control.SynchModifier = 5
                hpcc.ac[slice].write_register(trigger_control)
        except Exception as e:
            exceptionMsg = traceback.format_exc().split('\n')
            for line in exceptionMsg:
                self.Log('error', line)
            self.LogEnd()
        
    def tearDown(self):
        self.env.take_snapshot_if_enabled()
        self.LogEnd()
