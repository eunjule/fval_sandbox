################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: MultiTimeDomain.py
#-------------------------------------------------------------------------------
#     Purpose: Multi-time domain tests
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 09/07/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import time

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers

PICO_SECONDS_PER_SECOND = 1000000000000


class IntegerRatios(HpccTest):
    def DirectedRatio1Test(self):
        self.Scenario(50e-9, 1)

    def DirectedRatio2Test(self):
        self.Scenario(50e-9, 2)

    def DirectedRatio3Test(self):
        self.Scenario(50e-9, 3)

    def DirectedRatio4Test(self):
        self.Scenario(10e-9, 4)

    def DirectedRatio5Test(self):
        self.Scenario(10e-9, 5)

    def DirectedRatio10Test(self):
        self.Scenario(10e-9, 10)

    def Scenario(self, period, ratio):
        # period = period of the fastest slice (i.e. slice 1)
        self.env.SetConfig('LowToHighLoopback')

        periods = [None, None]
        periods[0] = period * ratio
        periods[1] = period
        
        asm = [None, None]
        pattern = [None, None]

        asm[0] = PatternAssembler()
        asm[0].LoadString("""\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            %repeat 200
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
        """)
        pattern[0] = asm[0].Generate()

        asm[1] = PatternAssembler()
        asm[1].symbols['LENGTH'] = 256 * ratio
        asm[1].symbols['RATIO'] = ratio
        asm[1].LoadString("""\
            %repeat RATIO
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %end
            %repeat LENGTH
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            PATTERN_END:
                StopPattern 0x89ABCDEF
        """)
        pattern[1] = asm[1].Generate()

        # Get set of slots
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)
            
        
        for slot in slots:
            slice0 = 0
            slice1 = 1
            master = 1
            domain_a = 7
            domain_b = 8
            self.env.SetDomains({(slot, slice0): (master, domain_a),
                                 (slot, slice1): (master, domain_b)})
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributes(slot, 0, periods[0], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': periods[0] / 2})
            self.env.SetPeriodAndPEAttributes(slot, 1, periods[1], {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': periods[1] / 2})
            for slice in [0, 1]:
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True, False) # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1]:
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')

            # Run checkers
            self.env.RunMultiSliceCheckers([
                {'slot': slot, 'slice': 0, 'ratio': ratio},
                {'slot': slot, 'slice': 1, 'ratio': 1},
            ])

    def _GetWalkingOnePattern(self, ratio=1):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 1*ratio
        #asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 17,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 18,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0  # 19,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0  # 20,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0  # 21,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # 22,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # 23,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # 24,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # 25,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # 26,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 27,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 28,
            %end
            ClearFlags
            LOOP:
                RandomVectors length=256
                #V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 27,
            GotoIfNon `LOOP`, COND.RCTRIGRCVD  ## Error Generated??
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 17,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 18,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0  # 19,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0  # 20,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0  # 21,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # 22,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # 23,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # 24,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # 25,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # 26,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 27,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 28,
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        return pattern.Generate()

    def _DriveWalkingOneAllPattern(self):
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 1*ratio
        #asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            ClearFlags
            LOOP:
                RandomVectors length=256
                #V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 27,
            GotoIfNon `LOOP`, COND.RCTRIGRCVD  ## Error Generated??
            #S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        return pattern.Generate()
    
    def _DriveWalkingOneAllPatternCTV(self):
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 1*ratio
        #asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            #SendTrigger 0x2AAA
            I optype=EXT, extop=TRIGGERNOW, action=TRGEXT_I, imm=0x2aaa
            #10101010101010 
            #0x2AAA
            ClearFlags
            LOOP:
                RandomVectors length= 512
                #V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            GotoIfNon `LOOP`, COND.RCTRIGRCVD  ## Error Generated??

            #%repeat 28
            #V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            #V link=0, ctv=1, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            #%end
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            #S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        #pattern.SaveObj('Slice0_Drive.obj')
        return pattern.Generate()

    def _CaptWalkingOneAllPattern(self):

        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 1*ratio
        pattern.symbols['LENGTH'] = 56 #*ratio
        pattern.symbols['LENGTH1'] = 56 #*ratio
        pattern.symbols['LOOPLENGTH'] = 512

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %repeat LENGTH
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            ClearFlags
            LOOP:
                #RandomVectors length=512
                %repeat LOOPLENGTH
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                %end
            GotoIfNon `LOOP`, COND.RCTRIGRCVD  ## Error Generated??

            %repeat LENGTH1
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            #S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        #pattern.SaveObj('Slice1_Capture.obj')
        return pattern.Generate()

    def _DriveTriggerPattern(self):
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            %repeat 512
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            #RandomVectors length=512
            I optype=EXT, extop=TRIGGERNOW, action=TRGEXT_I, imm=0x2aaa
            %repeat 512
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            V link=0, ctv=1, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            #RandomVectors length=512
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            """)
        pattern.SaveObj('Slice0_Trigger.obj')
        return pattern.Generate()

    def _CaptureTriggerPattern(self):
        pattern = PatternAssembler()
        pattern.symbols['LENGTH'] = 1500 #*ratio

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %repeat LENGTH
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            """)
        pattern.SaveObj('Slice1_Capture.obj')
        return pattern.Generate()


    def _DrivePulseOneAllPattern(self):
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 1*ratio
        #asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
                PATTERN_START:                                                                                   
                S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                %end
                V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            """)        
        return pattern.Generate()
    
    def _CaptPulseOneAllPattern(self):

        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 1*ratio
        pattern.symbols['LENGTH'] = 201*20 #*ratio

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %repeat LENGTH
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            StopPattern 0x89ABCDEF
        """)
        return pattern.Generate()

            
class Synchronization(HpccTest):
    def DirectedMultiSlotSWTriggerSyncTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 16000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            LOOP:
                %repeat 10                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 51-60, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1                      # 61
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            %repeat 50                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678                                     # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        period = 100e-9

        domainMasterSet = False
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            data1 = hpcc.ac[slice].Read('TriggerControl')
            if not domainMasterSet:
                data1.DomainMaster = 1
                domainMasterSet = True
            data1.DomainID = 7
            hpcc.ac[slice].Write('TriggerControl', data1)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            self.env.WritePattern(slot, slice, 0, pdata)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pdata))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, True, False, False, False) # capture all
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern()

        # Send sync pulse    
        self.env.rc.send_sync_pulse()
        time.sleep(0.1)
        self.env.rc.send_trigger(0x0FF0)

        # Wait for all slices to complete
        failPinLaneData = "10 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"
        failCycle = -1
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            completed = hpcc.ac[slice].WaitForCompletePattern(numberOfTries = 5000)
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
            self.env.CheckAlarms(slot, slice)
            #self.env.DumpCapture(slot, slice)

            pat_reg = hpcc.ac[slice].Read('PatternEndStatus').value

            captureData = hpcc.ac[slice].ReadCapture()
            self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
            for block in hpcc.ac[slice].UnpackCapture(captureData):
                header, data = block
                for i in range(8):
                    if header.CapturedVectorsValidFlags[i] == 1:
                        totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                        indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                        actual = data[i].PinLaneData
                        vaddr = data[i].VectorAddress
                        actualString = '{:056b}'.format(actual)
                        vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                        self.Log('info', str(indCycleCount) + "\t" + vectorString)  #str(totalCycleCount)str(vaddr)
                        #self.Log('info', str(vaddr) + "\t" + vectorString)  
                        if failCycle == -1:
                            failCycle = indCycleCount
                        else:
                            if failCycle != indCycleCount:
                                self.Log('error', 'other slice failCycle {}, slot {} slice {} failCycle {}, un-sync'.format(failCycle, slot, slice, indCycleCount))
                        if failPinLaneData != vectorString:
                            self.Log('error', 'expected failPinLaneData {}, slot {} slice {} failPinLaneData {}, mismatch'.format(failPinLaneData, slot, slice, vectorString))
            if pat_reg != 305419896:
                self.Log('error', 'pattern not executed')
            #print(hpcc.ac[slice].Read('CentralDomainRegisterFlags'))
            #print(hpcc.ac[slice].Read('PatternEndStatus'))
            
    def DirectedMTDTriggerCollisionTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        patternFail = PatternAssembler()
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pPass = patternPass.Generate()
        period = 5e-9 

        count = 0
        self.env.rc.clear_rc_trigger()
        while count < 10:
            self.Log('info', '~~~ loop {} ~~~'.format(count))
            count += 1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                data1 = hpcc.ac[slice].Read('TriggerControl')
                data1.DomainMaster = 1
                data1.DomainID = slice + 1
                hpcc.ac[slice].Write('TriggerControl', data1)
                
                self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
                if slice == 0:
                    self.env.WritePattern(slot, slice, 0, pFail)
                else:
                    self.env.WritePattern(slot, slice, 0, pPass)
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # capture fail
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].LoadAndArmIlas(slot, slice) # arm ILA
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            #time.sleep(0.1)
            #self.env.rc.SendHILTrigger(0x0FF0)
            
            # Wait for all slices to complete
            failCycle = -1
            failed = False
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                    hpcc.ac[slice].AbortPattern(waitForComplete = True)
                    completed = hpcc.ac[slice].IsPatternComplete()
                    self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                    failed = True
                    
                hpcc.DumpCapture(slice)
                
                # check capture
                captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                if captureCount != 1:
                    self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))     
                    failed = True
                
                captureData = hpcc.ac[slice].ReadCapture()
                self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                            indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                            actual = data[i].PinLaneData
                            vaddr = data[i].VectorAddress
                            actualString = '{:056b}'.format(actual)
                            vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                            self.Log('info', str(indCycleCount) + "\t" + vectorString)  #str(totalCycleCount)str(vaddr)
                            #self.Log('info', str(vaddr) + "\t" + vectorString)  
                            if failCycle == -1:
                                failCycle = indCycleCount
                            else:
                                if failCycle != indCycleCount:
                                    self.Log('warning', 'other slice failCycle {}, slot {} slice {} failCycle {}, un-sync by {} vectors'.format(failCycle, slot, slice, indCycleCount, abs(indCycleCount-failCycle)))
                            #if failPinLaneData != vectorString:
                            #    self.Log('error', 'expected failPinLaneData {}, slot {} slice {} failPinLaneData {}, mismatch'.format(failPinLaneData, slot, slice, vectorString))
                
                # check trigger and flag
                triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
                self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
                #print(hpcc.ac[slice].Read('CentralDomainRegisterFlags'))
                flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
                if slice == 0:
                    if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                        failed = True
                else:
                    if flags.LocalStickyError != 0 or flags.DomainStickyError != 0 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                        failed = True
                        
                #check RC trigger
                self.env.rc.clear_rc_trigger()
                        
            '''
            if failed:
                for (slot, slice) in self.env.fpgas:
                    hpcc.ac[slice].DumpIlas(slot, slice)
                return
            '''            
                

    def DirectedSTDTriggerCollisionTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        patternFail = PatternAssembler()
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pPass = patternPass.Generate()
        period = 5e-9 

        count = 0
        self.env.rc.clear_rc_trigger()
        while count < 10:
            self.Log('info', '~~~ loop {} ~~~'.format(count))
            count += 1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                data1 = hpcc.ac[slice].Read('TriggerControl')
                if slice == 0:
                    data1.DomainMaster = 1
                else:
                    data1.DomainMaster = 0
                data1.DomainID = slot
                hpcc.ac[slice].Write('TriggerControl', data1)
                #print(data1)
                
                self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
                if slice == 0:
                    self.env.WritePattern(slot, slice, 0, pFail)
                else:
                    self.env.WritePattern(slot, slice, 0, pPass)
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # capture fail
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            #time.sleep(0.1)
            #self.env.rc.SendHILTrigger(0x0FF0)
            
            # Wait for all slices to complete
            failCycle = -1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                    hpcc.ac[slice].AbortPattern(waitForComplete = True)
                    completed = hpcc.ac[slice].IsPatternComplete()
                    self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                    
                hpcc.DumpCapture(slice)
                
                # check capture
                captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                if captureCount != 1:
                    self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))                
                
                captureData = hpcc.ac[slice].ReadCapture()
                self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                            indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                            actual = data[i].PinLaneData
                            vaddr = data[i].VectorAddress
                            actualString = '{:056b}'.format(actual)
                            vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                            self.Log('info', str(indCycleCount) + "\t" + vectorString)  #str(totalCycleCount)str(vaddr)
                            #self.Log('info', str(vaddr) + "\t" + vectorString)  
                            if failCycle == -1:
                                failCycle = indCycleCount
                            else:
                                if failCycle != indCycleCount:
                                    # change to error after CQ14180 is fix
                                    self.Log('warning', 'other slice failCycle {}, slot {} slice {} failCycle {}, un-sync by {} vectors'.format(failCycle, slot, slice, indCycleCount, abs(indCycleCount-failCycle)))
                            #if failPinLaneData != vectorString:
                            #    self.Log('error', 'expected failPinLaneData {}, slot {} slice {} failPinLaneData {}, mismatch'.format(failPinLaneData, slot, slice, vectorString))
                
                # check trigger and flag
                triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
                self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
                #print(hpcc.ac[slice].Read('CentralDomainRegisterFlags'))
                flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
                if slice == 0:
                    if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                else:
                    if flags.LocalStickyError != 0 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                        
                #check RC trigger
                self.env.rc.clear_rc_trigger()

    # CQ 14180
    def DirectedSTDSWTriggerSyncTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        patternFail = PatternAssembler()
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 40000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 40000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pPass = patternPass.Generate()
        period = 5e-9 

        count = 0
        self.env.rc.clear_rc_trigger()
        while count < 10:
            self.Log('info', '~~~ loop {} ~~~'.format(count))
            count += 1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                data1 = hpcc.ac[slice].Read('TriggerControl')
                if slice == 0:
                    data1.DomainMaster = 1
                else:
                    data1.DomainMaster = 0
                data1.DomainID = slot
                hpcc.ac[slice].Write('TriggerControl', data1)
                #print(data1)
                
                self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
                if slice == 0:
                    self.env.WritePattern(slot, slice, 0, pFail)
                else:
                    self.env.WritePattern(slot, slice, 0, pPass)
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # capture fail
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            time.sleep(0.1)
            self.env.rc.send_trigger(0x0FF0)
            
            # Wait for all slices to complete
            failCycle = -1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                    hpcc.ac[slice].AbortPattern(waitForComplete = True)
                    completed = hpcc.ac[slice].IsPatternComplete()
                    self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                    
                hpcc.DumpCapture(slice)
                
                # check capture
                captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                if captureCount != 1:
                    self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))                
                
                captureData = hpcc.ac[slice].ReadCapture()
                self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                            indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                            actual = data[i].PinLaneData
                            vaddr = data[i].VectorAddress
                            actualString = '{:056b}'.format(actual)
                            vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                            self.Log('info', str(indCycleCount) + "\t" + vectorString)  #str(totalCycleCount)str(vaddr)
                            #self.Log('info', str(vaddr) + "\t" + vectorString)  
                            if failCycle == -1:
                                failCycle = indCycleCount
                            else:
                                if failCycle != indCycleCount:
                                    self.Log('error', 'other slice failCycle {}, slot {} slice {} failCycle {}, un-sync by {} vectors'.format(failCycle, slot, slice, indCycleCount, abs(indCycleCount-failCycle)))
                            #if failPinLaneData != vectorString:
                            #    self.Log('error', 'expected failPinLaneData {}, slot {} slice {} failPinLaneData {}, mismatch'.format(failPinLaneData, slot, slice, vectorString))
                
                # check trigger and flag
                triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
                self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
                #print(hpcc.ac[slice].Read('CentralDomainRegisterFlags'))
                flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
                if slice == 0:
                    if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                else:
                    if flags.LocalStickyError != 0 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                        
                #check RC trigger
                self.env.rc.clear_rc_trigger()
                
    '''
    KBL match loop usage
    MatchLoop: # Label
        CLEARDOMAINSTICKYERROR # Instruction to clear domain sticky errors
        #100 cycles padding needed 
        #Strobe Vector
        #14k cycles padding needed 
        DRAINPINFIFO # Instruction needed by the pattern generator
        JDSE MatchLoop # Jump domain sticky error instruction

    '''
    def DirectedSTDMatchLoopSyncTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        PRESTAGE_PADDING = random.randint(18000,50000)
        patternFail = PatternAssembler()
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat {}                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            MATCHLOOP:
                I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
                %repeat 14000                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                I optype=BRANCH, br=GOTO_I, cond=COND.DSE, imm=`MATCHLOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """.format(PRESTAGE_PADDING))
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat {}                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            MATCHLOOP:
                I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
                %repeat 14000                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                I optype=BRANCH, br=GOTO_I, cond=COND.DSE, imm=`MATCHLOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """.format(PRESTAGE_PADDING))
        pPass = patternPass.Generate()
        period = 5e-9 

        count = 0
        self.env.rc.clear_rc_trigger()
        while count < 10:
            self.Log('info', '~~~ loop {} ~~~'.format(count))
            count += 1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                data1 = hpcc.ac[slice].Read('TriggerControl')
                if slice == 0:
                    data1.DomainMaster = 1
                else:
                    data1.DomainMaster = 0
                data1.DomainID = slot
                hpcc.ac[slice].Write('TriggerControl', data1)
                #print(data1)
                
                self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
                if slice == 0:
                    self.env.WritePattern(slot, slice, 0, pFail)
                else:
                    self.env.WritePattern(slot, slice, 0, pPass)
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # capture fail
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            
            # Wait for all slices to complete
            failCycle = -1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                    hpcc.ac[slice].AbortPattern(waitForComplete = True)
                    completed = hpcc.ac[slice].IsPatternComplete()
                    self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                    
                hpcc.DumpCapture(slice)
                
                # check capture
                captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                if captureCount != 1:
                    self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))                
                
                captureData = hpcc.ac[slice].ReadCapture()
                self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                            indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                            actual = data[i].PinLaneData
                            vaddr = data[i].VectorAddress
                            actualString = '{:056b}'.format(actual)
                            vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                            self.Log('info', str(indCycleCount) + "\t" + vectorString)  #str(totalCycleCount)str(vaddr)
                            #self.Log('info', str(vaddr) + "\t" + vectorString)  
                            if failCycle == -1:
                                failCycle = indCycleCount
                            else:
                                if failCycle != indCycleCount:
                                    self.Log('error', 'other slice failCycle {}, slot {} slice {} failCycle {}, un-sync by {} vectors'.format(failCycle, slot, slice, indCycleCount, abs(indCycleCount-failCycle)))
                            #if failPinLaneData != vectorString:
                            #    self.Log('error', 'expected failPinLaneData {}, slot {} slice {} failPinLaneData {}, mismatch'.format(failPinLaneData, slot, slice, vectorString))
                
                # check trigger and flag
                triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
                self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
                #print(hpcc.ac[slice].Read('CentralDomainRegisterFlags'))
                flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
                if slice == 0:
                    if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                else:
                    if flags.LocalStickyError != 0 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                        
                #check RC trigger
                self.env.rc.clear_rc_trigger()

    # clear sticky error instruction only affects the current slot/slice
    # it doesn't send any triggers to communicate with its own domain members or other domains
    def DirectedMTDClearStickyErrorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        patternFail = PatternAssembler()
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            LOOP2:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP2`, invcond=0 
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            LOOP2:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP2`, invcond=0 
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pPass = patternPass.Generate()
        period = 5e-9 

        count = 0
        self.env.rc.clear_rc_trigger()
        while count < 1:
            self.Log('info', '~~~ loop {} ~~~'.format(count))
            count += 1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                data1 = hpcc.ac[slice].Read('TriggerControl')
                data1.DomainMaster = 1
                data1.DomainID = slice + 4
                hpcc.ac[slice].Write('TriggerControl', data1)
                
                self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
                if slice == 0:
                    self.env.WritePattern(slot, slice, 0, pFail)
                else:
                    self.env.WritePattern(slot, slice, 0, pPass)
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # CTV
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].LoadAndArmIlas(slot, slice) # arm ILA
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            #time.sleep(0.1)
            #self.env.rc.SendHILTrigger(0x0FF0)
            
            # Wait for all slices to complete
            failCycle = -1
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    if slice == 0:
                        self.Log('error', 'Pattern execution did not complete')
                    hpcc.ac[slice].AbortPattern(waitForComplete = True)
                    completed = hpcc.ac[slice].IsPatternComplete()
                    self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                    
                hpcc.DumpCapture(slice)
                
                # check capture
                captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                if captureCount != 1:
                    self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))   
                
                captureData = hpcc.ac[slice].ReadCapture()
                self.Log('info', "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            totalCycleCount = header.TotalCycleCount # Adding Total Cycle Count per Capture Block, if not captured, TotalCycleCount doesn't increase.
                            indCycleCount = totalCycleCount + i # Incrementing each cycle based on Total Cycle Count.
                            actual = data[i].PinLaneData
                            vaddr = data[i].VectorAddress
                            actualString = '{:056b}'.format(actual)
                            vectorString = ' '.join(actualString[i:i+2] for i in range(0,len(actualString),2))
                            self.Log('info', str(indCycleCount) + "\t" + vectorString)  #str(totalCycleCount)str(vaddr)
                            #self.Log('info', str(vaddr) + "\t" + vectorString)  
                            if failCycle == -1:
                                failCycle = indCycleCount
                            else:
                                if failCycle != indCycleCount:
                                    self.Log('warning', 'other slice failCycle {}, slot {} slice {} failCycle {}, un-sync by {} vectors'.format(failCycle, slot, slice, indCycleCount, abs(indCycleCount-failCycle)))
                            #if failPinLaneData != vectorString:
                            #    self.Log('error', 'expected failPinLaneData {}, slot {} slice {} failPinLaneData {}, mismatch'.format(failPinLaneData, slot, slice, vectorString))
                
                # check trigger and flag
                triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
                self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
                #print(hpcc.ac[slice].Read('CentralDomainRegisterFlags'))
                flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
                if slice == 0:
                    if flags.LocalStickyError != 0 or flags.DomainStickyError != 0 or flags.GlobalStickyError != 0:
                        self.Log('error', flags)
                else:
                    if flags.LocalStickyError != 0 or flags.DomainStickyError != 0 or flags.GlobalStickyError != 1:
                        self.Log('error', flags)
                        
                #check RC trigger
                self.env.rc.clear_rc_trigger()
