################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Random.py
#-------------------------------------------------------------------------------
#     Purpose: Super RPG tests
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 09/14/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import time

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest

class Random(HpccTest):
    # TODO: add user log, pattern id, mask, subroutine after the current one is stable
    def RandomSuperRPGTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 300], 'LEN_PAT': [1000, 2000], 'CLINK': 0.01, 'LRPT': 0.01, 'CTV': 0.1, 'RPT': 0.002, 'MTV': 0.01, 'PMASK': True, 'Meta': 0.01, 'ALU': 0.01}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('random{}.obj'.format(time.time()))
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice) 
            
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000) 
            self.env.RandomizeMTVFailMask(slot, slice) 
            
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode']
            hpcc.ac[slice].Write('PatternControl', data)
                        
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], CaptureCTVAfterMaxFail = param['CaptureCTVAfterMaxFail'], stopOnMaxFail = param['stopOnMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], maxCaptureCount = param['maxCaptureCount'], captureAddress = param['captureAddress'])
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'], checkRegisters=True)
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
                
    # VOX < termVRef, Z = High
    def RandomSuperRPGZHighTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CLINK': 0.1, 'LRPT': 0.1, 'CTV': 0.1}
        p = RPG(constraints = constraints, noZX = 0)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('random{}.obj'.format(time.time()))
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice) 
            
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)            
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode']
            hpcc.ac[slice].Write('PatternControl', data)
                        
            for ch in range(56):
                hpcc.dc[slice].SetPEAttributes({'VOX': 1.5, 'termVRef': 2.0}, False, [ch])
            hpccModel.acSim[slice].checkz = True
            attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': param['period'] / 2}
            self.env.SetPeriodAndPEAttributes(slot, slice, param['period'], attributes = attributes)
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, patternData, start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], CaptureCTVAfterMaxFail = param['CaptureCTVAfterMaxFail'], stopOnMaxFail = param['stopOnMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], maxCaptureCount = param['maxCaptureCount'], captureAddress = param['captureAddress'])
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
                
            # restore cal value
            hpcc.dc[0].LoadDcCal(hpcc.calFile.DcSlice0)
            hpcc.dc[1].LoadDcCal(hpcc.calFile.DcSlice1)
            hpccModel.acSim[slice].checkz = False
                
    # VOX > termVRef, Z = Low    
    def RandomSuperRPGZLowTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CLINK': 0.1, 'LRPT': 0.1, 'CTV': 0.1}
        p = RPG(constraints = constraints, noZX = 0)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('random{}.obj'.format(time.time()))
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice) 
            
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)            
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode']
            hpcc.ac[slice].Write('PatternControl', data)
                        
            for ch in range(56):
                hpcc.dc[slice].SetPEAttributes({'VOX': 2.0, 'termVRef': 1.5}, False, [ch])
                hpccModel.acSim[slice].GetChannel(ch).zislogic1 = False
            hpccModel.acSim[slice].checkz = True
            attributes = {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': param['period'] / 2}
            self.env.SetPeriodAndPEAttributes(slot, slice, param['period'], attributes = attributes)
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, patternData, start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], CaptureCTVAfterMaxFail = param['CaptureCTVAfterMaxFail'], stopOnMaxFail = param['stopOnMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], maxCaptureCount = param['maxCaptureCount'], captureAddress = param['captureAddress'])
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
                
            # restore cal value            
            hpcc.dc[0].LoadDcCal(hpcc.calFile.DcSlice0)
            hpcc.dc[1].LoadDcCal(hpcc.calFile.DcSlice1)
            for ch in range(56):
                hpccModel.acSim[slice].GetChannel(ch).zislogic1 = True
            hpccModel.acSim[slice].checkz = False
                
