# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
"""Validate DTS(Digital Temperature Sensor) or Serial Capture Stream feature for Gen2E """
import collections
from ctypes import c_uint
import random
from Common.triggers import SCSTriggerEncoding, SCSMemoryEncoding
from Common.fval import SkipTest
from Common.register import Register
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest

SCOPESHOT_ADDRESS_LIMIT_DEFAULT = int(1.5 * (1 << 30)) - 1
NUM_TRIGGERS = 6000
TEST_ITERATIONS = 3000
MAX_FAIL_COUNT = 10

ChannelDataCombinations = collections.namedtuple('ChannelDataCombinations', ['scs_channel', 'lsb_shift'])
LeftRightShiftData = collections.namedtuple('LeftRightShiftDataCombination', ['expected_data'])

VARIABLE_SHIFT_COMBINATIONS = {ChannelDataCombinations(scs_channel=0, lsb_shift=0): LeftRightShiftData(expected_data=0x3ED),
                               ChannelDataCombinations(scs_channel=0, lsb_shift=1): LeftRightShiftData(expected_data=0x2DF),
                               ChannelDataCombinations(scs_channel=1, lsb_shift=0): LeftRightShiftData(expected_data=0x9E),
                               ChannelDataCombinations(scs_channel=1, lsb_shift=1): LeftRightShiftData(expected_data=0x1E4),
                               ChannelDataCombinations(scs_channel=2, lsb_shift=0): LeftRightShiftData(expected_data=0xEB),
                               ChannelDataCombinations(scs_channel=2, lsb_shift=1): LeftRightShiftData(expected_data=0x35c),
                               ChannelDataCombinations(scs_channel=3, lsb_shift=0): LeftRightShiftData(expected_data=0x267),
                               ChannelDataCombinations(scs_channel=3, lsb_shift=1): LeftRightShiftData(expected_data=0x399)}

ChannelLengthCombinations = collections.namedtuple('ChannelDataCombinations', ['scs_channel', 'length'])
VariableLengthDataCombination = collections.namedtuple('VariableLengthDataCombination', ['expected_data'])

VARIABLE_LENGTH_COMBINATIONS = {ChannelLengthCombinations(scs_channel=0, length=10): VariableLengthDataCombination(expected_data=0x3ED),
                                ChannelLengthCombinations(scs_channel=0, length=9):  VariableLengthDataCombination(expected_data=0x1ED),
                                ChannelLengthCombinations(scs_channel=0, length=8):  VariableLengthDataCombination(expected_data=0xED),
                                ChannelLengthCombinations(scs_channel=1, length=10): VariableLengthDataCombination(expected_data=0x39E),
                                ChannelLengthCombinations(scs_channel=1, length=9):  VariableLengthDataCombination(expected_data=0x19E),
                                ChannelLengthCombinations(scs_channel=1, length=8):  VariableLengthDataCombination(expected_data=0x9E),
                                ChannelLengthCombinations(scs_channel=2, length=10): VariableLengthDataCombination(expected_data=0x3EB),
                                ChannelLengthCombinations(scs_channel=2, length=9):  VariableLengthDataCombination(expected_data=0x1EB),
                                ChannelLengthCombinations(scs_channel=2, length=8):  VariableLengthDataCombination(expected_data=0xEB),
                                ChannelLengthCombinations(scs_channel=3, length=10): VariableLengthDataCombination(expected_data=0x367),
                                ChannelLengthCombinations(scs_channel=3, length=9):  VariableLengthDataCombination(expected_data=0x167),
                                ChannelLengthCombinations(scs_channel=3, length=8):  VariableLengthDataCombination(expected_data=0x67)}

ChannelLinkLocalRepeatCombinations = collections.namedtuple('ChannelDataCombinations', ['scs_channel','lsb_shift'])
VariableDataCombination = collections.namedtuple('VariableDataCombination', ['expected_data'])

VARIABLE_COMPRESSION_COMBINATIONS = {
                                ChannelLinkLocalRepeatCombinations(scs_channel=0, lsb_shift=1): VariableDataCombination(expected_data=0x3BA),
                                ChannelLinkLocalRepeatCombinations(scs_channel=0, lsb_shift=0): VariableDataCombination(expected_data=0x177),
                                ChannelLinkLocalRepeatCombinations(scs_channel=1, lsb_shift=1): VariableDataCombination(expected_data=0x05A),
                                ChannelLinkLocalRepeatCombinations(scs_channel=1, lsb_shift=0): VariableDataCombination(expected_data=0x168),
                                ChannelLinkLocalRepeatCombinations(scs_channel=2, lsb_shift=1): VariableDataCombination(expected_data=0x3DA),
                                ChannelLinkLocalRepeatCombinations(scs_channel=2, lsb_shift=0): VariableDataCombination(expected_data=0x16F),
                                ChannelLinkLocalRepeatCombinations(scs_channel=3, lsb_shift=1): VariableDataCombination(expected_data=0x3B8),
                                ChannelLinkLocalRepeatCombinations(scs_channel=3, lsb_shift=0): VariableDataCombination(expected_data=0x77)}


class BaseTest(HpccTest):

    def log_compare_results(self, expected_trigger, trigger_at_hpcc_dc, trigger_at_rctc, fail_count):
        msg = f'Expected trigger 0x{expected_trigger:08X} ' \
              f'AC to DC trigger up 0x{trigger_at_hpcc_dc:08X} ' \
              f'DC to RC trigger up 0x{trigger_at_rctc:08X}'
        if expected_trigger != trigger_at_hpcc_dc:
            self.Log('error', msg)
            fail_count = fail_count + 1
        else:
            self.Log('debug', msg)
        return fail_count

    def check_pattern_end_status(self, hpcc, expected_pattern_end_status, slice):
        actual_pattern_end_status = hpcc.ac[slice].Read('PatternEndStatus').Pack()
        validate_pattern_end_status(actual_pattern_end_status, expected_pattern_end_status, self.Log)

    def create_expected_variable_shift_scs_trigger(self, scs_channel, scs_pin_id, lsb_shift, slice):
        scs_data = self.get_variable_shift_scs_data(lsb_shift, scs_channel)
        expected_trigger = self.get_scs_trigger(scs_pin_id, slice, scs_data, scs_channel)
        return expected_trigger

    def create_expected_variable_length_scs_trigger(self, scs_channel, scs_pin_id, slice, length):
        scs_data = self.get_variable_length_scs_data(scs_channel, length)
        expected_trigger = self.get_scs_trigger(scs_pin_id, slice, scs_data, scs_channel)
        return expected_trigger

    def get_variable_shift_scs_data(self, lsb_shift, scs_channel):
        LeftRightShiftDataCombination = VARIABLE_SHIFT_COMBINATIONS[(scs_channel, lsb_shift)]
        return LeftRightShiftDataCombination.expected_data

    def get_variable_length_scs_data(self, scs_channel, length):
        VariableLengthDataCombination = VARIABLE_LENGTH_COMBINATIONS[(scs_channel, length)]
        return VariableLengthDataCombination.expected_data

    def create_expected_compression_scs_trigger(self, scs_channel, scs_pin_id, lsb_shift, slice):
        scs_data = self.get_compression_scs_data(lsb_shift, scs_channel)
        expected_trigger = self.get_scs_trigger(scs_pin_id, slice, scs_data, scs_channel)
        return expected_trigger

    def get_compression_scs_data(self, lsb_shift, scs_channel):
        CompressionDataCombination = VARIABLE_COMPRESSION_COMBINATIONS[(scs_channel, lsb_shift)]
        return CompressionDataCombination.expected_data

    def get_scs_trigger(self, scs_pin_id, slice, scs_data, scs_channel):
        scs_trigger_structure = SCSTriggerStructure()
        scs_trigger_structure.sensor_data = scs_data
        scs_trigger_structure.reserved = 0
        scs_trigger_structure.user_id_lower = scs_pin_id
        scs_trigger_structure.virtual_pin_id = scs_channel
        scs_trigger_structure.ac_slice_id = slice
        scs_trigger_structure.trigger_type = 0x9
        return scs_trigger_structure.value

    def start_scopeshot(self):
        self.env.rc.start_scopeshot_event()

    def stop_scopeshot(self):
        self.env.rc.stop_scopeshot_event()

    def reset_scopeshot(self):
        self.env.rc.set_scopsehot_address_limit_default()
        starting_address = self.env.rc.scopeshot_address()
        if starting_address != 0:
            self.Log('error', f'Scopeshot address did not reset to zero. '
                              f'0x{starting_address:08X}')
        limit_address = self.env.rc.scopeshot_address_limit()
        if limit_address != SCOPESHOT_ADDRESS_LIMIT_DEFAULT:
            self.Log('error', f'Scopeshot address limit invalid. '
                              f'0x{limit_address:08X}')

    def reset_memory(self, num_triggers):
        starting_address = self.env.rc.scopeshot_address()
        expected_end_address = starting_address + (8 * num_triggers)
        addresses = [i for i in range(starting_address,
                                      expected_end_address, 8)]
        for address in addresses:
            dword = bytes(4)
            self.env.rc.dma_write(address, dword)

    def rc2_present(self):
        return self.env.tester().get_instrument_present('rc2')


def validate_time_stamp(start_pattern, finish_pattern, time_checker, log, fail_count=0):
    for idx in range(TEST_ITERATIONS):
        if (time_checker[idx] > start_pattern) and \
                (time_checker[idx] < finish_pattern):
            pass
        else:
            fail_count += 1
            log('error', f'Test_Iteration: {idx}, '
                         f'Expected timestamp: {time_checker[idx]}, '
                         f'between Start_Time: {start_pattern} and '
                         f'Finish_Time: {finish_pattern}')
        if fail_count > 3:
            break

    return fail_count


def validate_memory_data(expected_data, actual_data, log, fail_count=0):
    if len(expected_data) != len(actual_data):
        log('error', f'Expected Data size {len(expected_data)} '
                     f'does not match Actual data {len(actual_data)} ')
        return False
    for idx in range(len(expected_data)):
        if expected_data[idx].value != actual_data[idx].value:
            log('debug', f'trigger count:  {idx}, '
                         f'expected scs memory pin_id: {expected_data[idx].pin_id}, '
                         f'actual pin_id: {actual_data[idx].pin_id},')
            log('debug', f'trigger count:  {idx}, '
                         f'expected scs memory data: {expected_data[idx].sensor_data}, '
                         f'actual data: {actual_data[idx].sensor_data},')
            log('error', f'Failed on iteration: {idx}, '
                         f'expected/actual value: 0x{expected_data[idx].value:08X}, '
                         f'0x{actual_data[idx].value:08X}')
            fail_count += 1
            if fail_count > 3:
                return False
    return True


def log_results(exp_memory, act_memory, log, cycles=20):
    for idx in range(0, len(exp_memory), cycles):
        log('debug', f'trigger count:  {idx}, expected scs memory value: 0x{exp_memory[idx].value:08X}, '
                     f'actual value: 0x{act_memory[idx].value:08X},')
        log('debug', f'trigger count:  {idx}, expected scs memory data: {exp_memory[idx].sensor_data}, '
                     f'actual data: {act_memory[idx].sensor_data},')


def adjust_sequence(exp_memory, act_memory, log, step=4):
    for idx in range(0, len(exp_memory), step):
        if (exp_memory[idx].value != act_memory[idx].value) and \
                (exp_memory[idx].pin_id != act_memory[idx].pin_id):
            if exp_memory[idx-1].value == act_memory[idx].value:
                value1 = act_memory[idx].value
                value2 = act_memory[idx-1].value
                act_memory[idx].value = value2
                act_memory[idx-1].value = value1
            log('debug', f'adjusted value:  {idx}, expected/actual scs memory value: '
                         f' 0x{exp_memory[idx].value:08X}, '
                         f' 0x{act_memory[idx].value:08X},')


def validate_pattern_end_status(actual_status, expected_status, log):
    if actual_status != expected_status:
        log('error', f'Actual Pattern End Status = 0x{actual_status:x}  '
                     f'Expected Pattern end Status = 0x{expected_status:x}')
        return False
    else:
        log('debug', f'Actual Pattern End Status = 0x{actual_status:x}  '
                     f'Expected Pattern end Status = 0x{expected_status:x}')
        return True


def generate_expected_scs_memory(scs_trigger, link_num):
    trigger = SCSTriggerEncoding(value=scs_trigger)
    scs_memory = SCSMemoryEncoding(
        sensor_data=trigger.sensor_data,
        user_id=trigger.user_id,
        pin_id=trigger.pin_id,
        ac_slice_id=trigger.ac_slice_id,
        link_num=link_num)
    return scs_memory


class SCSTriggerStructure(Register):
    _fields_ = [('sensor_data', c_uint, 10),
                ('reserved', c_uint, 2),
                ('user_id_lower', c_uint, 10),
                ('virtual_pin_id', c_uint, 2),
                ('ac_slice_id', c_uint, 2),
                ('trigger_type', c_uint, 6)]


class FunctionalityTests(BaseTest):
    """Test complete flow of data from HPCC instrument to RCTC."""

    num_triggers = NUM_TRIGGERS
    test_iterations = TEST_ITERATIONS
    max_fail_count = MAX_FAIL_COUNT
    patternStr = []

    def SCSMultiTriggerfromSinglePatternTest(self):
        """Use SCS pin state word and vector with SCE=1

        Generate serial capture trigger by randomizing channel,pin id and left or right shift. Keeping data length
        constant to 10. Verify if correct trigger is generated at RCTC.
        """
        if self.rc2_present():
            raise SkipTest('RC2 Not Supported')
        trig_count = 0
        starting_address = 0
        time_checker = []
        exp_memory = []
        act_memory = []
        DataCombination = [1005, 735, 158, 484, 235, 860, 615, 921]
        self.env.SetConfig('EvenToOddLoopback')
        lsb_bit = [random.randint(0, 1) for i in range(750)]
        user_id = [random.randint(1, 2047) for i in range(750)]
        pin_id = [random.randint(0, 3) for i in range(750)]
        pattern_end_status = random.randint(0, 0xFFFFFFFF)
        pattern_0 = self._GenPattern(lsb_bit, user_id, pin_id, pattern_end_status)
        self.reset_scopeshot()
        self.start_scopeshot()
        self.reset_memory(self.num_triggers)
        for slot, slice in self.env.fpgas:
            for i in range(750):
                scs_data = self.get_variable_shift_scs_data(lsb_bit[i], pin_id[i])
                exp_trigger = self.get_scs_trigger(user_id[i], slice, scs_data, pin_id[i])
                for _ in range(4):
                    exp_memory.append(generate_expected_scs_memory(exp_trigger, slot))

        for slot, slice in self.env.fpgas:
            start_pattern = self.env.rc.live_time_stamp()
            completed, actualEndStatus = self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
            status_ok = validate_pattern_end_status(actualEndStatus, pattern_end_status, self.Log)
            if not status_ok:
                break
            finish_pattern = self.env.rc.live_time_stamp()
            for iteration in range(self.test_iterations):
                exp_address = (starting_address + (8 * trig_count))
                scs_memory, memory_time = self.env.rc.read_sensor_data_from_memory(exp_address)
                time_checker.append(memory_time)
                act_memory.append(scs_memory)
                trig_count += 1

            fail_count = validate_time_stamp(start_pattern, finish_pattern, time_checker, self.Log)
            time_checker.clear()
            if fail_count > 3:
                break

        self.stop_scopeshot()
        adjust_sequence(exp_memory, act_memory, self.Log)
        validate_memory_data(exp_memory, act_memory, self.Log)
        log_results(exp_memory, act_memory, self.Log)

    def _GenPattern(self, lsb_bit, user_id, pin_id, pattern_end_status, lvalue=4, repeat=750):
        self.patternStr.append(f'''\
            PATTERN_START:
            S stype=IOSTATEJAM,                 data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
            Drive0CompareLVectors length=1024\n''')
        for i in range(repeat):
            self.patternStr.append(f'''\
            S stype = SCC, lsb_shift = {lsb_bit[i]}, virtual_pin_id = {user_id[i]},record_length = 10, serial_channel = {pin_id[i]}
            Call `SUBR`\n''')
        self.patternStr.append(f'''\
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={pattern_end_status}
            SUBR:
            %repeat {lvalue}
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101011Z1100110Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z            
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
            %end
            Return\n''')

        pattern = PatternAssembler()
        pattern.LoadString("".join(self.patternStr))
        return pattern.Generate()

    def RandomChannelShiftPinIdDummyVectorsTest(self):
        """Send pattern from HPCC and receive data on single serial capture channel at a time.

        Generate serial capture trigger , by randomizing channel,pin id and left or right shift.Keeping data length
        constant to 10. Add dummy vectors and non SCE vectors in between and make sure correct trigger is generated and
        passed to HPCC DC and at RCTC.
        """

        fail_count = 0
        for test_execution in range(100):
            self.env.SetConfig('EvenToOddLoopback')
            scs_pin_id = random.randint(0, 2047)
            channel_under_test = [0, 1, 2, 3]
            scs_channel = random.choice(channel_under_test)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            lsb_shift = random.randint(0, 1)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                S stype=IOSTATEJAM,                         data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                Drive0CompareLVectors length=1024
                S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel={}  
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
                Drive0CompareLVectors length=10
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101011Z1100110Z
                Drive0CompareLVectors length=90
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                Drive0CompareLVectors length=50
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                PATTERN_END:
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
            """.format(lsb_shift, scs_pin_id, scs_channel, pattern_end_status))
            pattern_0 = pattern.Generate()

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
                expected_trigger = self.create_expected_variable_shift_scs_trigger(scs_channel, scs_pin_id, lsb_shift, slice)
                trigger_at_hpcc_dc = hpcc.dc[0].Read('UpTrigger').Pack()
                trigger_at_rctc = self.env.rc.read_trigger_up(slot)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                fail_count = self.log_compare_results(expected_trigger, trigger_at_hpcc_dc, trigger_at_rctc, fail_count)
            if fail_count > 10:
                break

    def DirectedSerialCaptureControlStateWordUpdateTest(self):
        """Select a channel and send multiple vectors making sure that less than 10 vectors contain valid DTS data.

           Change the channel, and send 10 vectors, add random dummy vectors as well as non SCE vectors.Make
           sure total 10 vectors are valid before changing channel. Confirm previous channel data is ignored and only
           new channel data is captured and sent to RCTC.
        """
        fail_count = 0
        for test_execution in range(100):
            self.env.SetConfig('EvenToOddLoopback')
            scs_pin_id = random.randint(0, 2047)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            lsb_shift = random.randint(0, 1)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                S stype=IOSTATEJAM,            				data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                Drive0CompareLVectors length=1024
                S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel=0  
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel=1
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
                Drive0CompareLVectors length=10
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101011Z1100110Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                V sce=0, link=0, ctv=0, mtv=0, lrpt=20,     data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                V sce=0, link=0, ctv=1, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel=0  
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                PATTERN_END:
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
            """.format(lsb_shift, scs_pin_id, lsb_shift, scs_pin_id, lsb_shift, scs_pin_id, pattern_end_status))
            pattern_0 = pattern.Generate()
            scs_channel = 1

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                expected_trigger = self.create_expected_variable_shift_scs_trigger(scs_channel, scs_pin_id, lsb_shift, slice)
                trigger_at_hpcc_dc = hpcc.dc[0].Read('UpTrigger').Pack()
                trigger_at_rctc = self.env.rc.read_trigger_up(slot)
                fail_count = self.log_compare_results(expected_trigger, trigger_at_hpcc_dc, trigger_at_rctc, fail_count)
            if fail_count > 10:
                break

    def DirectedRandomVectorLengthTest(self):
        """Select a channel randomize length in record length parameter. Expected lengths are 8,9,10.

           Make sure correct length of data gets selected in trigger register.
        """
        fail_count = 0
        for test_execution in range(100):
            self.env.SetConfig('EvenToOddLoopback')
            scs_pin_id = random.randint(0, 2047)
            channel_under_test = [0, 1, 2, 3]
            scs_channel = random.choice(channel_under_test)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            supported_record_lengths = [8, 9, 10]
            record_length = random.choice(supported_record_lengths)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                S stype=IOSTATEJAM,            				data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                Drive0CompareLVectors length=1024
                S stype=SCC, lsb_shift=0, virtual_pin_id={}, record_length={}, serial_channel={}  
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
                Drive1CompareHVectors length=1024
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101011Z1100110Z
                Drive1CompareHVectors length=1024
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100011Z
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100011Z
                PATTERN_END:
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
            """.format(scs_pin_id, record_length, scs_channel, pattern_end_status))
            pattern_0 = pattern.Generate()

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                expected_trigger = self.create_expected_variable_length_scs_trigger(scs_channel, scs_pin_id, slice, record_length)
                trigger_at_hpcc_dc = hpcc.dc[0].Read('UpTrigger').Pack()
                trigger_at_rctc = self.env.rc.read_trigger_up(slot)
                fail_count = self.log_compare_results(expected_trigger, trigger_at_hpcc_dc, trigger_at_rctc, fail_count)
            if fail_count > 10:
                break

    def DirectedSerialCaptureWithLocalRepeatTest(self):
        """By using local repeat , repeat one vector. Add randomization related to channel , pin id, shift.

           Make sure we are getting expected trigger data back.
        """
        fail_count = 0
        for test_execution in range(100):
            self.env.SetConfig('EvenToOddLoopback')
            scs_pin_id = random.randint(0, 2047)
            channel_under_test = [0, 1, 2, 3]
            scs_channel = random.choice(channel_under_test)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            lsb_shift = random.randint(0, 1)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                      S stype=IOSTATEJAM,            			  data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                      Drive0CompareLVectors length=1024
                      S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel={}  
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101011Z1100110Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=1,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                      PATTERN_END:
                      I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
                  """.format(lsb_shift, scs_pin_id, scs_channel, pattern_end_status))
            pattern_0 = pattern.Generate()

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                expected_trigger = self.create_expected_variable_shift_scs_trigger(scs_channel, scs_pin_id, lsb_shift,
                                                                                   slice)
                trigger_at_hpcc_dc = hpcc.dc[0].Read('UpTrigger').Pack()
                trigger_at_rctc = self.env.rc.read_trigger_up(slot)
                fail_count = self.log_compare_results(expected_trigger, trigger_at_hpcc_dc, trigger_at_rctc, fail_count)
            if fail_count > 10:
                break

    def DirectedSerialCaptureWithChannelLinkLocalRepeatTest(self):
        """By using local repeat and channel link , repeat vector. Add randomization related to channel , pin id, shift.

           Make sure we are getting expected trigger data back.
        """
        fail_count = 0
        for test_execution in range(100):
            self.env.SetConfig('EvenToOddLoopback')
            scs_pin_id = random.randint(0, 2047)
            channel_under_test = [0, 1, 2, 3]
            scs_channel = random.choice(channel_under_test)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            lsb_shift = random.randint(0, 1)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                      S stype=IOSTATEJAM,            			  data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                      Drive0CompareLVectors length=1024
                      S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel={}  
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                      V sce=1, link=1, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101001Z1100010Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100110Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=1,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100010Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100010Z
                      PATTERN_END:
                      I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
                  """.format(lsb_shift, scs_pin_id, scs_channel, pattern_end_status))
            pattern_0 = pattern.Generate()

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                data = hpcc.ac[slice].Read('PatternControl')
                data.LinkMode = 0
                hpcc.ac[slice].Write('PatternControl', data)
                self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                expected_trigger = self.create_expected_compression_scs_trigger(scs_channel, scs_pin_id, lsb_shift,
                                                                                   slice)
                trigger_at_hpcc_dc = hpcc.dc[0].Read('UpTrigger').Pack()
                trigger_at_rctc = self.env.rc.read_trigger_up(slot)
                fail_count = self.log_compare_results(expected_trigger, trigger_at_hpcc_dc, trigger_at_rctc, fail_count)
            if fail_count > 5:
                break


class SCSAlarmTests(BaseTest):
    """Generate alarm conditions and confirm alarm gets generated in HPCC"""

    def DirectedOverFlowAlarmTest(self):
        """Send multiple vectors back to back and see if OverFlow alarm can be generated. Need to confirm if this is

        possible, as only one HPCC will be under test."""
        expected_alarm = 'SCEOverFlowAlarm'
        fail_count = 0
        self.env.SetConfig('EvenToOddLoopback')
        for test_execution in range(100):
            scs_pin_id = random.randint(0, 2047)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            channel_under_test = [0, 1, 2, 3]
            scs_channel = random.choice(channel_under_test)
            lsb_shift = random.randint(0, 1)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                          S stype=IOSTATEJAM,            			  data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                          Drive0CompareLVectors length=1024
                          S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=8, serial_channel={} 
                          %repeat 10
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010010Z0010100Z0101011Z1100110Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                          V sce=1, link=0, ctv=0, mtv=0, lrpt=80,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                          %end
                          PATTERN_END:
                          I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
                      """.format(lsb_shift, scs_pin_id, scs_channel, pattern_end_status))
            pattern_0 = pattern.Generate()

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                self.env.RunPattern(slot, slice, pattern_0, period=1e-9)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                alarms = self.env.CheckAlarms(slot, slice, alarmsToMask=expected_alarm)
                if alarms == []:
                    self.Log('error', f' Alarm not generated. Expected alarm {expected_alarm} ')
                else:
                    if alarms[0] != expected_alarm:
                        self.Log('error', f' Expected alarm {expected_alarm} not generated. Actual alarms {alarms}')
                        fail_count = fail_count + 1
            if fail_count > 10:
                break

    def DirectedIncompleteSCEeventBeforeSCSEventAlarmTest(self):
        """Send incomplete length of vectors based on configuration and make sure Incomplete SCE event before SCS event

        Alarm got generated """
        expected_alarm = 'IncompleteSCEeventBeforeSCSEvent'
        fail_count = 0
        for test_execution in range(10):
            self.env.SetConfig('EvenToOddLoopback')
            scs_pin_id = random.randint(0, 2047)
            channel_under_test = [0, 1, 2, 3]
            scs_channel = random.choice(channel_under_test)
            pattern_end_status = random.randint(0, 0xFFFFFFFF)
            lsb_shift = random.randint(0, 1)
            pattern = PatternAssembler()
            pattern.LoadString("""\
                      S stype=IOSTATEJAM,            			  data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                      Drive0CompareLVectors length=1024
                      S stype=SCC, lsb_shift={}, virtual_pin_id={}, record_length=10, serial_channel={}  
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101011Z1100010Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101011Z1100111Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010101Z0101010Z1100111Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010101Z0101011Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010010Z0010100Z0101010Z1100011Z
                      V sce=1, link=0, ctv=0, mtv=0, lrpt=0,      data=0v0101010010101011100011110010011Z0010100Z0101010Z1100011Z
                      PATTERN_END:
                      I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={}
                  """.format(lsb_shift, scs_pin_id, scs_channel, pattern_end_status))
            pattern_0 = pattern.Generate()

            for slot, slice in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                self.env.RunPattern(slot, slice, pattern_0, period=10e-9)
                self.check_pattern_end_status(hpcc, pattern_end_status, slice)
                alarms = self.env.CheckAlarms(slot, slice, alarmsToMask=expected_alarm)
                if alarms == []:
                    self.Log('error', f' Alarm not generated. Expected alarm {expected_alarm} ')
                else:
                    if alarms[0] != expected_alarm:
                        self.Log('error', f' Expected alarm {alarms[0]} not generated. Actual alarms {alarms}')
                        fail_count = fail_count + 1
            if fail_count > 5:
                break
