################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: InstructionDensity.py
#-------------------------------------------------------------------------------
#     Purpose: Tests that measure and *check* the instruction
#              density limits of the design
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/22/15
#       Group: HDMT FPGA Validation
################################################################################

import random

import Hpcc.hpcctb.idrpg as idrpg
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest


class CaptureAll(HpccTest):
    EPSILON = 1E-11

    def SearchPeriod(self, slot, slice, pattern, start, end, resolution, disabledRules):
        end, start = sorted(list([start, end]))
        period = None
        value = start
        while value - end > self.EPSILON:
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period = value, maskFailureToComplete = True)
            passed, alarms = self.env.RunCheckers(slot, slice, endStatusMask = 0xffffffff, alarmsToMask = ['PVCCacheUnderRun'], captureDisableRules = disabledRules)
            passed = passed and completed
            if not(passed):
                break
            period = value
            value = value - resolution
        return period
    def DirectedMinPeriodTest(self):
        # Disabled checker rules that are expected to fail at really fast speeds
        disabledRules = [
            'missing-capture',
            'vecaddr-mismatch',
            'data-mismatch',
            'ucc-mismatch',
            'pcc-mismatch',
            'ctv-mismatch',
            'userlog1-mismatch',
            'userlog2-mismatch',
            'extra-vectors',
            'dropped-vectors',
            'tcc-mismatch',
            'patid-mismatch',
        ]
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=102400
            StopPattern 0x0
        """)
        pattern = asm.Generate()
        start = 3E-9
        end = 1.20E-9
        resolution = 0.10E-9
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            period = self.SearchPeriod(slot, slice, pattern, start, end, resolution, disabledRules)
            self.Log('info', 'For ({},{}), fastest passing period = {}'.format(slot, slice, period))
            if period > 2.5e-9:
                self.Log('error', '({},{}) failed "min period" test, expected<2.5e-9, actual={}'.format(slot, slice, period))

class Random(HpccTest):

    def DirectedTest(self):
        self.env.SetConfig('InternalLoopback')

        parameters = {}
        parameters['prestage_max'] = 16384
        parameters['jump_cost']    = -90
        parameters['depth_max']    = 2
        parameters['sub_items']    = 2,3    # Min/max entries wrapped by a subroutine.
        parameters['pat_items']    = 2,3    # Min/max entries wrapped by a pattern.
        parameters['subroutines']  = 2,3    # Min/max number of subroutines to generate.
        parameters['patterns']     = 2,3    # Min/max number of patterns to generate.
        parameters['plists']       = 2,3    # Min/max number of P-List entries to generate.
        parameters['flat_vectors'] = 20,100 # Min/max length to generate for a RandomPassingEvenToOddNoSwitchVectors macro.
        parameters['loops']        = 2,3    # Min/max loop count for a loop.
        parameters['loop_items']   = 1,3    # Min/max entries wrapped by a loop.

        code = idrpg.code_generator(parameters)

        self.Log('info', 'cost = {}'.format(code['cost']))
        self.Log('info', 'minimum cost = {}'.format(code['min_cost']))
        self.Log('info', 'prediction = {}'.format(code['prediction']))

        src = code['listing']

        fout = open('irpg.rpat', 'w')
        fout.write(src)
        fout.close()

        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString(src)
        pattern = asm.Generate()

        fout = open('irpg.obj', 'wb')
        fout.write(pattern)
        fout.close()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            pat_reg = hpcc.ac[slice].Read('PatternEndStatus').value
            #for period in [2.5e-9, 4e-9, 8e-9, 16e-9, 32e-9, 64e-9]:
            period = 2.5e-9

            self.env.RunPattern(slot, slice, pattern, start = asm.Resolve('START'), period = period, captureFails = False, captureCTVs = False, captureAll = False, numberOfTries = 2000)
            #self.env.RunCheckers(slot, slice, alarmsToMask = ['PVCCacheUnderRun'])
            alarms = self.env.CheckAlarms(slot, slice, alarmsToMask = ['PVCCacheUnderRun','DeepCaptureStartFail'])

            if code['prediction'] == 'FAIL':
                self.Log('error', 'Model prediction = {}'.format(code['prediction']))
            pat_reg = hpcc.ac[slice].Read('PatternEndStatus').value
            if pat_reg != 305419896:
                self.Log('error', 'pattern not executed')
            #filename = '{}_{}_{}.tar.gz'.format(self.env.test.Name().replace('.', '_'), slot, slice)
            #self.env.TakeSnapshot(filename, self.env.fpgas)

