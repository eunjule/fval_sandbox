################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: FailCapture.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for max fail, stop on fail .etc
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/18/15
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class MaxFail(HpccTest):
    def DirectedPerPatternMaxFailTest(self):
        self.PerPatternMaxFail(237)
        
    # should show stop on first fail alarm
    def DirectedPerPatternMaxFail_StopOnFirstFail_Test(self):
        self.PerPatternMaxFail(236) 
        
    def PerPatternMaxFail(self, maxFailCaptureCount):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        pattern = PatternAssembler()
        pattern.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 1
            PATTERN1:  # 6 fails                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0H0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0   
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0   
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0   
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0   
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0            
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0    
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9,
            %repeat 50 # 0 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10-59
            %end
            I optype=BRANCH, br=RET                                                                          # 60
            PATTERN2: 
            %repeat 10 # 10 fails, exact
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1  # 61-70
            %end
            %repeat 49 # 0 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 71-120
            %end
            I optype=BRANCH, br=RET 
            PATTERN3:  # 11 fails   
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 122, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 132,
            %repeat 48 # 0 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 133-180
            %end
            I optype=BRANCH, br=RET 
            PATTERN4:  # 9 fails                
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 182, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 190,
            %repeat 50 # 0 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 191-240
            %end
            I optype=BRANCH, br=RET 
            PATTERN_START:                                                                                   # 
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 242
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 243
            %repeat 100 # 0 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 244 - 343
            %end
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=0                                               # 344
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN1]                                   # 345
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1                                               # 346
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN2]                                   # 347
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2                                               # 348
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN3]                                   # 349
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=3                                               # 350
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN4]                                   # 351
            %repeat 200 # 200 fails
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 352 - 551
            %end
            PATTERN_END:                                                                                     # 
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 552
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 553
        """)
        
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 


        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, start = pattern.Resolve('eval[PATTERN_START]'), captureType = ac_registers.CaptureControl.FAIL_STATUS, captureAll = False, captureFails = True, maxFailCountPerPatern = 10, maxFailCaptureCount = maxFailCaptureCount)
            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
            
    # 9.0.2 fix: stop capture all after reaching max fail, no need for an extra fail
    def DirectedAbortStopOnFirstFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200000                                                                      # 20K > 16K
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        maxFailCaptureCount = 1
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, stopOnMaxFail = True, maxFailCaptureCount = maxFailCaptureCount)
                      
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if captureCount != 2:
                self.Log('error', 'capture count = {}, expected = 2, fail to stop on max fail'.format(captureCount))
                
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus == 0xdeadbeef:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead, fail to stop on max fail'.format(actualEndStatus))
            else:
                self.Log('debug', 'Pattern End Status = 0x{:x}'.format(actualEndStatus))
            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
                
                
    def DirectedStopOnFirstFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200000                                                                      # 20K > 16K
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        maxFailCaptureCount = 1
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed, endstate = self.env.RunPattern(slot, slice, pdata, period, stopOnMaxFail = False, maxFailCaptureCount = maxFailCaptureCount, maxFailCountPerPatern = 0)
            #print(hex(endstate))
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            #self.env.DumpCapture(slot, slice)
    
    def DirectedPerPatternMaxFailDoesNotAbortTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 20000                                                                      # 20K > 16K
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, stopOnMaxFail = True, captureAll = False, captureFails = True, captureCTVs = True, maxFailCountPerPatern = 1, maxFailCaptureCount = 2)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
    
    # After maxFailCaptureCount or maxFailCountPerPatern is reached, no more CTV will be captured overall or per pattern, respectively.
    # 1. passing CTV is not counted towards fail count
    # 2. Fixed in 9.0.2: need an extra fail to stop adding CTV to captures
    def DirectedPerPatternMaxFailStopCaptureCTVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 20000                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, stopOnMaxFail = True, captureAll = False, captureFails = True, captureCTVs = True, maxFailCountPerPatern = 1)
            self.env.RunCheckers(slot, slice)
            
    def DirectedMaxFailStopCaptureCTVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        prePadding = random.randint(0,20000)
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            Drive0CompareLVectors {}
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 2000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """.format(prePadding))
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, stopOnMaxFail = True, captureAll = False, captureFails = True, captureCTVs = True, maxFailCaptureCount = 1)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if captureCount != 1:
                self.Log('error', 'TotalCaptureCount = {}, expect 1'.format(captureCount))
                
            totalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if totalFailCount != 1:
                self.Log('error', 'TotalFailCount = {}, expect 1'.format(totalFailCount))
                
    def DirectedMaxCaptureCountTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  #
            %repeat 5
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            %end
            %repeat 10
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            %end
            Drive0CompareLVectors 20000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = False, captureFails = True, captureCTVs = True, maxCaptureCount = 10)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)


	# Test case verifies the fail count from previous pattern had been cleared
    # and the fail count from next pattern is captured correctly and next pattern is aborted if the max fail count hit
    # Set up:  
    #   Even to Odd loopback
    #   Period set as 50 ns
    #   Pattern A with 256 vectors with 19 fails
    #   Set StopOnMaxFail mode in Control Register
    #   Set the MaxFailCount to 20
    #   Execute the Pattern A
    #   Verify: StopAtFirstFail in Alarm Control bit not set and
    #   Verify: Correct capture FailCount from Pattern B
    #   Pattern B with 256 vectors with 1 fail 
    #   Continue StopOnMaxFail mode
    #   Update MaxFailCount to 1
	#   Verify: AlarmControl - StopAtFirstFail not set Max
    #   Execute the Pattern B
    #   Verify: AlarmControl - StopAtFirstFail set after executing pattern B
    #   Verify: Correct capture FailCount from Pattern B. 
    #   Note: because the pattern B is small, the FPGA can not aborted the pattern in time in this test case.
    def DirectedClearFailCountFromPrevPatternTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        numberOfTries  = 500
        waitForComplete = True
        
        failCountsPatA     = 19
        maxFailCountsPatA  = 20
        failCountsPatB     = 1
        maxFailCountsPatB  = 1
        
        pattern = PatternAssembler()
        #first pattern 256 vector - 19 fails - max fail count to 20
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 100
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            %end
			V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 135
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            #Verify the correct capture Fail Count from Pattern A   
            failCounts = hpcc.ac[slice].Read('TotalFailCount').Pack()
            self.Log('info', 'Total Fail Counts captured before Pattern A {} execute'.format(failCounts))
            
            #set the StopAtMaxFail mode and MaxFailCount
            self.env.RunPattern(slot, slice, pdata,  period, captureAll = False, captureFails = True, captureCTVs = True, stopOnMaxFail = True, maxFailCaptureCount = maxFailCountsPatA)
            
            # Verify the Alarm Control - StopOnFirtFail not set and correct capture Fail from pattern A    
            alarmCtrlReg= hpcc.ac[slice].Read('AlarmControl')
            if alarmCtrlReg.StopOnFirstFail == 0:
                self.Log('info', 'Alarm Control - bit Stop At First Fail not set')
            else:    
                self.Log('error', 'Alarm Control - bit Stop At First Fail set incorrectly')
            
            #Verify the correct capture Fail Count from Pattern A   
            failCounts = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if failCounts == failCountsPatA:
                self.Log('info', 'Total Fail Counts captured from Pattern A {}'.format(failCounts))
            else:
                self.Log('info', 'Total Fail Counts captured from Pattern A {} while expected {}'.format(failCounts, failCountsPatA))
                
        # set Pattern B with 256 vectors and 2 fails
        pattern2 = PatternAssembler()
        pattern2.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            %repeat 100
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            %end
			#V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 154
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata2 = pattern2.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.Log('info','Starting 2nd pattern')
            #RunPatternSetUp is only tester periods and capture control registers
            self.env.RunPatternSetUp(slot, slice, pdata2,  period, captureAll = False, captureFails = True, captureCTVs = True, stopOnMaxFail = True,  maxFailCaptureCount = maxFailCountsPatB)    
            
			#Verify the Alarm Control - StopAtFirtFail not set while set up capture control for next pattern
            alarmCtrlReg= hpcc.ac[slice].Read('AlarmControl')
            if alarmCtrlReg.StopOnFirstFail == 0:
                self.Log('info', 'Slice {} - Alarm Control - bit StopAtFirstFail not set during setup capture next pattern'.format(slice))
            else:    
                self.Log('error', 'Slice {} - Alarm Control - bit StopAtFirstFail set incorrectly during setup capture the next pattern'.format(slice))
			 
            #prestage the pattern B
            hpcc.ac[slice].PrestagePattern(0x0, numberOfTries)
			
            # Verify the Alarm Control - StopAtFirtFail not set while prestage the next pattern
            alarmCtrlReg= hpcc.ac[slice].Read('AlarmControl')
            if alarmCtrlReg.StopOnFirstFail == 0:
                self.Log('info', 'Slice {} - Alarm Control - bit StopAtFirstFail not set during prestage for pattern'.format(slice))
            else:    
                self.Log('error', 'Slice {} - Alarm Control - bit StopAtFirstFail set incorrectly during prestage next pattern'.format(slice))
            
            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            # Wait for complete
            completed = False
            if waitForComplete:
                completed = hpcc.ac[slice].WaitForCompletePattern(numberOfTries)
            
			# The pattern execute supposes to not not complete but the pattern so small, it will complete before abort command
            # is execute
            if not completed:
                self.Log('info', 'Pattern execution did not complete')
            else:
                self.Log('info', 'completed {}'.format(completed))
			
            
            # Verify the Alarm Control - StopAtFirtFail set when failCount hits maxFailCounts
            alarmCtrlReg= hpcc.ac[slice].Read('AlarmControl')
            if alarmCtrlReg.StopOnFirstFail == 1:
                self.Log('info', 'Slice {} - Alarm Control - bit StopAt FirstFail set when MaxFailCount Hits'.format(slice))
            else:    
                self.Log('error', 'Slice {} - Alarm Control - bit StopAtFirstFail not set when MaxFailCount Hits'.format(slice))
            
            #Verify the correct capture FailCounts from Pattern B
            failCounts = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if failCounts == failCountsPatB:
                self.Log('info', 'Total Fail Counts captured from Pattern B {}'.format(failCounts))
            else:
                self.Log('error', 'Total Fail Counts captured from Pattern B {} while expected {}'.format(failCounts, failCountsPatB))
            
            
    def RandomMaxCaptureCountTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()           
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CTV': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, pdata, 2000)
            
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], maxCaptureCount = param['maxCaptureCount'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if captureCount > (param['maxCaptureCount'] + 20):
                self.Log('error', 'total capture count = {}, expected capture count <= {}'.format(captureCount, param['maxCaptureCount'] + 20))
            else:
                self.Log('info', 'total capture count = {}, expected capture count <= {}'.format(captureCount, param['maxCaptureCount'] + 20))
                        
    def RandomScoreboardTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()  
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000]}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        pdata = pattern.Generate()
        #pattern.SaveObj('scoreboard1.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, pdata, 2000)
            
            self.env.RunPattern(slot, slice, pdata, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], stopOnMaxFail = param['stopOnMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], captureAddress = param['captureAddress'])            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
                
class NoCapture(HpccTest):
    def DirectedNoCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200000                                                                      # 20K > 16K
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        maxFailCaptureCount = 1
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureFails = False, captureCTVs = False, captureAll = False, stopOnMaxFail = True, maxFailCaptureCount = maxFailCaptureCount)
                      
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if captureCount != 0:
                self.Log('error', 'capture count = {} , should be 0'.format(captureCount))
            else:
                self.Log('info', 'capture count = {} , should be 0'.format(captureCount))
                
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus == 0xdeadbeef:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead, fail to stop on max fail'.format(actualEndStatus))
            else:
                self.Log('info', 'Pattern End Status = 0x{:x}'.format(actualEndStatus))
            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
                
                
class CaptureBlock(HpccTest):
    # TotalCaptureBlockCount and TotalCaptureCount will not be stopped by captureLength
    # If captureLength is smaller than TotalCaptureBlockCount, read capture using captureLength, and ignore any missing captures
    def DirectedCaptureLengthStopTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            %repeat 512
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1H1
            %end                                                                    # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)       
            captureLength = 16 # in unit of blocks, has to be multiple of 16
            self.env.RunPattern(slot, slice, pdata, period, captureLength = captureLength)    
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['CaptureDRAMOverflow'], captureDisableRules = ['missing-capture'])
            if 'CaptureDRAMOverflow' not in alarms:
                self.Log('error', 'CaptureDRAMOverflow is not set in AlarmControl')
            
    def DirectedCaptureLengthWrapTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            %repeat 523
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1H1
            %end                                                                    # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice) 
            hpcc = self.env.instruments[slot]            
            captureLength = 16 # in unit of blocks, has to be multiple of 16
            self.env.RunPattern(slot, slice, pdata, period, captureLength = captureLength, dramOverflowWrap = True)    
            firstFailAddress = hpcc.ac[slice].Read('FirstFailAddress').Pack()
            if firstFailAddress != 0x308:
                self.Log('error', 'firstFailAddress = 0x{:X}, expect 0x308'.format(firstFailAddress))
            #self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            
    def DirectedCapturePinEnableTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            %repeat 512
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1
            %end                                                                    # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)       
            captureLength = 16 # in unit of blocks, has to be multiple of 16
            self.env.RunPattern(slot, slice, pdata, period, captureType = 2)    
            self.env.RunCheckers(slot, slice)


class CTV(HpccTest):
    def DirectedFirstFailAddressTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
            Drive0CompareLVectors 200                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = True, captureFails = False, captureCTVs = False)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
        
    def DirectedCTVBitCaptureAllTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = True, captureFails = False, captureCTVs = False)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            
    def DirectedCTVBitCaptureFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = False, captureFails = True, captureCTVs = False)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)        
        
    def DirectedCTVAfterPerPatternMaxFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = False, captureFails = True, captureCTVs = True, maxFailCountPerPatern = 1, CaptureCTVAfterMaxFail = True)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
                
    def DirectedCTVAfterMaxCaptureFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = False, captureFails = True, captureCTVs = True, maxFailCaptureCount = 1, CaptureCTVAfterMaxFail = True)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')      
            #self.env.DumpCapture(slot, slice)
            
    def DirectedCTVAfterMaxCaptureFailAbortTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 20000                                                                      
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vH1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureAll = False, captureFails = True, captureCTVs = True, stopOnMaxFail = True, maxFailCaptureCount = 1, CaptureCTVAfterMaxFail = True)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')      
            #self.env.DumpCapture(slot, slice)        
            
    def DirectedCTVCaptureMaskedFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
        PATTERN1:
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  
            V link=0, ctv=1, mtv=1, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  
            RandomPassingEvenToOddVectors length=256            
            Return
        PATTERN_START:
            S stype=MASK,                   data=0b11000000000000000000000000000000000000000000000000000000
            PatternId 0
            PCall `PATTERN1`
        PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        """)
        pattern = asm.Generate()
        period = 50e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.SetSelectableFailMask(slot, slice, 0, 0b00000000000000000000000000000000000000000000000000000000)
            self.env.SetSelectableFailMask(slot, slice, 1, 0b00000000000000000000000000000000000000000000000000001100)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period, start = asm.Resolve('PATTERN_START'), captureType = ac_registers.CaptureControl.MASKED_FAIL, captureAll = False, captureFails = True, captureCTVs = True)
            self.env.RunCheckers(slot, slice)
            
            #self.env.DumpCapture(slot, slice)
        
    def RandomPlistCTVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        
        constraints = {'NUM_PAT': [5, 10], 'LEN_PAT': [1000, 2000], 'CTV': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('ctv.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            
    def RandomPlistCTVPlusTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        
        constraints = {'NUM_PAT': [5, 10], 'LEN_PAT': [1000, 2000], 'CTV': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('ctvPlus.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], stopOnMaxFail = param['stopOnMaxFail'], captureAddress = param['captureAddress'], CaptureCTVAfterMaxFail = param['CaptureCTVAfterMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], maxCaptureCount = param['maxCaptureCount'])
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])    
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')

class CaptureWindow(HpccTest):
    def DirectedCaptureWindowFailCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedperchannelfailcount = 1001          # Total failing vectors in pattern
        expectedtotalcapturecount = 1               # Failing vectors inside Capture window
        expectedtotalfailcount = 1001               # Total failing vectors in pattern
        expectedlastvectoraddress = 503             # Location of last failing vector  
        captureFailsstate = True
        captureCTVsstate = False
        captureAllstate = False
        self.RunEventooddPatternForCapture(expectedtotalcapturecount,expectedlastvectoraddress,captureFailsstate,captureCTVsstate,captureAllstate)
        self.CheckCounterRegister(expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount)
    
    def DirectedCaptureWindowCTVCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedperchannelfailcount = 1001
        expectedtotalcapturecount = 1
        expectedtotalfailcount = 1001
        expectedlastvectoraddress = 503
        captureFailsstate = False
        captureCTVsstate = True
        captureAllstate = False
        self.RunEventooddPatternForCapture(expectedtotalcapturecount,expectedlastvectoraddress,captureFailsstate,captureCTVsstate,captureAllstate)
        self.CheckCounterRegister(expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount)
        
    def DirectedCaptureWindowAllCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedperchannelfailcount = 1001
        expectedtotalcapturecount = 2
        expectedtotalfailcount = 1001
        expectedlastvectoraddress = 504
        captureFailsstate = False
        captureCTVsstate = False
        captureAllstate = True
        self.RunEventooddPatternForCapture(expectedtotalcapturecount,expectedlastvectoraddress,captureFailsstate,captureCTVsstate,captureAllstate)
        self.CheckCounterRegister(expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount)    
    
    def DirectedCaptureWindowMultipatternAllCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedperchannelfailcount = 3500
        expectedtotalcapturecount = 1500
        expectedtotalfailcount = 3500
        captureFailsstate = False
        captureCTVsstate = False
        captureAllstate = True
        self.RunEventooddMultiPatternForCapture(captureFailsstate,captureCTVsstate,captureAllstate)
        self.CheckCounterRegister(expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount)    
    
    def DirectedCaptureWindowMultipatternFailCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedperchannelfailcount = 3500
        expectedtotalcapturecount = 1500
        expectedtotalfailcount = 3500
        captureFailsstate = True
        captureCTVsstate = False
        captureAllstate = False
        self.RunEventooddMultiPatternForCapture(captureFailsstate,captureCTVsstate,captureAllstate)
        self.CheckCounterRegister(expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount)
    
    def DirectedCaptureWindowMultipatternCTVCaptureTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedperchannelfailcount = 3500
        expectedtotalcapturecount = 1000
        expectedtotalfailcount = 3500
        captureFailsstate = False
        captureCTVsstate = True
        captureAllstate = False
        self.RunEventooddMultiPatternForCapture(captureFailsstate,captureCTVsstate,captureAllstate)
        self.CheckCounterRegister(expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount)
    
    def RunEventooddPatternForCapture(self,expectedtotalcapturecount,expectedlastvectoraddress,captureFailsstate,captureCTVsstate,captureAllstate):
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=0
            S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=1
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=0
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """)
        pdata = pattern.Generate()
        period = 50e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureFails = captureFailsstate, captureCTVs = captureCTVsstate, captureAll = captureAllstate)    
            captureData, blockCount, captureCount = hpcc.ReadCapture(slice)
            vectorcount = 0
            for block in hpcc.ac[slice].UnpackCapture(captureData):
                header, data = block
                for i in range(8):
                    if header.CapturedVectorsValidFlags[i] == 1:
                        vaddr = data[i].VectorAddress
                        vectorcount += 1
            if vectorcount > expectedtotalcapturecount:
                self.Log('error', 'Vector count mismatch.Expected only {} vector'.format(expectedtotalcapturecount))
            elif vaddr != expectedlastvectoraddress:
                self.Log('error', 'Vector Captured at wrong address. Expected address {} Actual Address {}'.format(expectedlastvectoraddress,vaddr))
                           
    def RunEventooddMultiPatternForCapture(self,captureFailsstate,captureCTVsstate,captureAllstate):
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=0
            S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=1
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=0
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=1
            %repeat 500
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=0
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=1
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=CAPTURE_I, imm=0
            %repeat 500
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """)
        pdata = pattern.Generate()
        period = 50e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, period, captureFails = captureFailsstate, captureCTVs = captureCTVsstate, captureAll = captureAllstate)
        
    def CheckCounterRegister(self,expectedperchannelfailcount,expectedtotalcapturecount,expectedtotalfailcount):    
        perChannelRegisters = ['ChannelFailCount']
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            ActualTotalCaptureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            self.Log('info', 'Expected Total Capture:{} Actual Total Captures {} vectors for ({}, {})'.format(expectedtotalcapturecount,ActualTotalCaptureCount, hpcc.slot, slice))
            ActualTotalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            self.Log('info', 'Expected Total Failed:{} Actual Total Failed  {} vectors for ({}, {})'.format(expectedtotalfailcount,ActualTotalFailCount, hpcc.slot, slice))
            for ch in range(1,56,2):
                for register in perChannelRegisters:
                    actualperchannel = hpcc.ac[slice].ReadChannel(ch, register).Pack()
                    self.Log('debug', 'per-channel register {}[{}] in HPCC({}, {}), expected={}, actual={}'.format(register, ch, hpcc.slot, slice, expectedperchannelfailcount, actualperchannel))
                    if expectedperchannelfailcount != actualperchannel:
                        self.Log('error', 'per-channel register {}[{}] in HPCC({}, {}), expected={}, actual={}'.format(register, ch, hpcc.slot, slice, expectedperchannelfailcount, actualperchannel))
            if ActualTotalCaptureCount != expectedtotalcapturecount:
                self.Log('error','Mismatch in count.Expected Total Capture:{} Actual Total Captures {} vectors for ({}, {})'.format(expectedtotalcapturecount,ActualTotalCaptureCount, hpcc.slot, slice))
            if ActualTotalFailCount != expectedtotalfailcount:
                self.Log('error','Mismatch in count.Expected Total Failed:{} Actual Total Failed  {} vectors for ({}, {})'.format(expectedtotalfailcount,ActualTotalFailCount, hpcc.slot, slice))