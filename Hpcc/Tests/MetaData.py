################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: MetaData.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for Meta Data 
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 09/18/15
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.rpg import RPG
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest


class MetaData(HpccTest):
    def DirectedMetaDataTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN1:                          
                M data=`rand(126)`   
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                M data=`rand(126)`   
                RandomEvenToOddVectors 50, simple=1
                M data=`rand(126)`   
                I optype=BRANCH, br=RET
                M data=`rand(126)`   
            PATTERN2:                          
                M data=`rand(126)`   
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                M data=`rand(126)`   
                RandomEvenToOddVectors 50, simple=1
                M data=`rand(126)`   
                I optype=BRANCH, br=RET 
                M data=`rand(126)`   
            PATTERN_START:                                                                                 
                M data=`rand(126)`   
                S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
                %repeat 10
                M data=`rand(126)`
                %end
                I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=0       
                M data=`rand(126)`
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN2`
                M data=`rand(126)`
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN1`
                M data=`rand(126)`                
            PATTERN_END:                                                                                   
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                    
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """)
        
        pdata = pattern.Generate()
        period = 50e-9

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pdata, period, start = pattern.Resolve('eval[PATTERN_START]'))
            self.env.RunCheckers(slot, slice)

    def RandomMetaDataTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'Meta': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('rpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)