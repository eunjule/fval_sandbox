################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: MDut.py
#-------------------------------------------------------------------------------
#     Purpose: Multi-DUT tests
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/10/16
#       Group: HDMT FPGA Validation
################################################################################

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class MDut(HpccTest):
    def _GetPattern(self):
        patternFail = PatternAssembler()
        patternFail.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pFail = patternFail.Generate()
        
        patternPass = PatternAssembler()
        patternPass.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.GSE, imm=`LOOP`, invcond=1 
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pPass = patternPass.Generate()
        return pFail, pPass

    def DirectedMDutPassingTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pFail, pPass = self._GetPattern()
        checkList = []
        period = 5e-9
        
        for (slot, slice) in self.env.fpgas:                    
            self.env.LogPerSlice(slot, slice)
            checkList.append({'slot': slot, 'slice': slice})    
            hpcc = self.env.instruments[slot]
            data1 = hpcc.ac[slice].Read('TriggerControl')
            if slice == 0:
                data1.DomainMaster = 1
            else:
                data1.DomainMaster = 0
            data1.DomainID = slot
            data1.DutID = 20
            hpcc.ac[slice].Write('TriggerControl', data1)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            if slice == 0:
                self.env.WritePattern(slot, slice, 0, pFail)
            else:
                self.env.WritePattern(slot, slice, 0, pPass)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # CTV
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern(seMask = 1 << slot)

        # Send sync pulse    
        self.env.rc.send_sync_pulse()
        
        # Wait for all slices to complete
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                if slice == 0:
                    self.Log('error', 'Pattern execution did not complete')
                else:
                    self.Log('warning', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                
            #self.env.DumpCapture(slot, slice)            
            # check capture
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if slice == 0 and captureCount != 1:
                self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))                
            
            # check trigger and flag
            triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
            self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
            flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            if slice == 0:
                if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                    self.Log('error', flags)
            else:
                if flags.LocalStickyError != 0 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                    self.Log('error', flags)
        
        # cannot use sim since pattern has a non-deterministic loop
        #self.env.RunMultiSliceCheckers(checkList)    
        
    def DirectedMDutDifferentDutIDSameDomainTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pFail, pPass = self._GetPattern()
        period = 5e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            data1 = hpcc.ac[slice].Read('TriggerControl')
            data1.DomainMaster = 1
            data1.DomainID = slot
            data1.DutID = slice + 1
            hpcc.ac[slice].Write('TriggerControl', data1)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            if slice == 0:
                self.env.WritePattern(slot, slice, 0, pFail)
            else:
                self.env.WritePattern(slot, slice, 0, pPass)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # CTV
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern(seMask = 1 << slot)

        # Send sync pulse    
        self.env.rc.send_sync_pulse()
        
        # Wait for all slices to complete
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                if slice == 0:
                    self.Log('error', 'Pattern execution did not complete')
                else:
                    self.Log('warning', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                
            #self.env.DumpCapture(slot, slice)            
            # check capture
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if slice == 0 and captureCount != 1:
                self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))                
            
            # check trigger and flag
            triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
            self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
            flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            if slice == 0:
                if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                    self.Log('error', flags)
            else:
                if flags.LocalStickyError != 0 or flags.DomainStickyError != 0 or flags.GlobalStickyError != 0:
                    self.Log('error', flags)
                    
    def DirectedMDutDifferentDomainTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pFail, pPass = self._GetPattern()
        period = 5e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            data1 = hpcc.ac[slice].Read('TriggerControl')
            data1.DomainMaster = 1
            data1.DomainID = slice + 1
            data1.DutID = 30
            hpcc.ac[slice].Write('TriggerControl', data1)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            if slice == 0:
                self.env.WritePattern(slot, slice, 0, pFail)
            else:
                self.env.WritePattern(slot, slice, 0, pPass)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # CTV
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern(seMask = 1 << 1) # fail at domainID 1

        # Send sync pulse    
        self.env.rc.send_sync_pulse()
        
        # Wait for all slices to complete
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                if slice == 0:
                    self.Log('error', 'Pattern execution did not complete')
                else:
                    self.Log('warning', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                
            #self.env.DumpCapture(slot, slice)            
            # check capture
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if slice == 0 and captureCount != 1:
                self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))                
            
            # check trigger and flag
            triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
            self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
            flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            if slice == 0:
                if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                    self.Log('error', flags)
            else:
                if flags.LocalStickyError != 0 or flags.DomainStickyError != 0 or flags.GlobalStickyError != 1:
                    self.Log('error', flags)
                    
    def DirectedMDutNoDomainMasterTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pFail, pPass = self._GetPattern()
        period = 5e-9
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            data1 = hpcc.ac[slice].Read('TriggerControl')
            data1.DomainMaster = 0
            data1.DomainID = slot
            data1.DutID = 20
            hpcc.ac[slice].Write('TriggerControl', data1)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            if slice == 0:
                self.env.WritePattern(slot, slice, 0, pFail)
            else:
                self.env.WritePattern(slot, slice, 0, pPass)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pFail))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # CTV
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern()

        # Send sync pulse    
        self.env.rc.send_sync_pulse()
        
        # Wait for all slices to complete
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                self.Log('warning', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                
            # check trigger and flag
            triggerEvent = hpcc.ac[slice].Read('TriggerEvent')
            self.Log('info', 'trigger received = 0x{:X}'.format(triggerEvent.Pack()))
            flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            if slice == 0:
                if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 0:
                    self.Log('error', flags)
            else:
                if flags.LocalStickyError != 0 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 0:
                    self.Log('error', flags)        
    
