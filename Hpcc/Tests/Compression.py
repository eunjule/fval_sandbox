# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
"""Validate Compression features for Gen2E

1. no-op repeat or fixed-word repeat
2. Symbol link mode or Single bit mode.
"""

from Hpcc.Tests.hpcctest import HpccTest
from Common.fval import skip


class BabyStepTests(HpccTest):
    """Test complete flow of data from HPCC instrument to RCTC."""
    
    @skip('Not implemented')
    def DirectedNoopVectorAdditionTest(self):
        """Add NRPT to data vector and make sure vectors defined as start of burst got added after data vector

        Need to update assembler and simulator to support new NRPT.
        """

    @skip('Not implemented')
    def DirectedSymbolLinkModeVectorAdditionTest(self):
        """Enable symbol link feature on particular vector and make sure it decompresses correctly.

        Need to assembler and simulator to support new SLV feature.
        """


class MultipleCompressionTests(HpccTest):
    """Test data received is as expected or not"""

    @skip('Not implemented')
    def DirectedSlvClvCombinationTest(self):
        """Enable SLV and CLV at the same time on single vector and confirm vectors decompresses as expected."""

    @skip('Not implemented')
    def DirectedSlvLrptCombinationTest(self):
        """Enable SLV and LRPT at the same time on single vector and confirm vectors decompressed as expected."""
    
    @skip('Not implemented')
    def DirectedSlvNrptCombinationTest(self):
        """Enable SLV on single vector and add NRPT , confirm vectors decompresses as expected."""

    @skip('Not implemented')
    def DirectedSlvNrptLrptClvCombinationTest(self):
        """Enable SLV CLV on single vector.Add LRPT and NRPT, confirm vectors decompresses as expected."""

    @skip('Not implemented')
    def RandomSlvNrptLrptClvCombinationTest(self):
        """Randomly enable SLV CLV on single vector.Add LRPT and NRPT, confirm vectors decompresses as expected."""