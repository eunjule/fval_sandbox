################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Branch.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for branch instructions, i.e. SUBR
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng / Rodny Rodriguez
#        Date: 04/27/15
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers

class Subroutine(HpccTest):
    def DirectedSubrHeadCallIGlobalTest(self):
        self.SimpleSubrHead("GLOBAL", "CALL_I")
        
    def DirectedSubrTailCallIGlobalTest(self):
        self.SimpleSubrTail("GLOBAL", "CALL_I")
        
    def DirectedSubrHeadCallRRelTest(self):
        self.SimpleSubrHead("RELATIVE", "CALL_R")
        
    def DirectedSubrTailCallRRelTest(self):
        self.SimpleSubrTail("RELATIVE", "CALL_R")
   
    def DirectedSubrTailGapCallICfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "CALL_I")
        
    def DirectedSubrTailGapCallRCfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "CALL_R")
        
    def DirectedSubrTailGapPCallICfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "PCALL_I")
        
    def DirectedSubrTailGapPCallRCfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "PCALL_R")
        
    def DirectedSubrRecursivePCallIPfb10Test(self):
        self.RecursivePCallI_Pfb(10)
        
    def DirectedSubrRecursivePCallIPfb63Test(self):
        self.RecursivePCallI_Pfb(63)
    
    # have 2 subr call each other
    def DirectedSubrNestedPCallIGlobalTest(self):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        callnum = 10
        
        p = RPG()
        p.StartPattern()   
        p.ClearStack()
        p.AddBlock("PRE_RAND", 150, 0)
        p.SetRegFromImm(1, callnum) # r1 = 10
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_1")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        # subroutine
        p.AddBlock("SUBR_1", 50, vectorData)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_2", "ZERO", "1") 
        p.AddBlock("SUBR_1E", 50, vectorData)
        p.Return()
        p.AddBlock("SUBR_2", 60, vectorData)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_1", "ZERO", "1") 
        p.AddBlock("SUBR_2E", 60, vectorData)
        p.Return()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 362 + 110 * (callnum - 1)
        self.ExecuteTest(pattern)
        
    def RecursivePCallI_Pfb(self, callnum):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        
        p = RPG()
        p.StartPattern()   
        p.ClearStack()
        p.AddBlock("PRE_RAND", 150, 0)
        p.SetRegFromImm(1, callnum) # r1 = 10
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        # subroutine
        p.AddBlock("SUBR", 100, vectorData)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("PFB", "PCALL_I", 0, "ZERO", "1", pfb='eval[SUBR]') # should call subr itself
        p.AddBlock("SUBRE", 100, vectorData)
        p.Return()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 452 + 200 * (callnum - 1)
        self.ExecuteTest(pattern)
        
    def SimpleSubrHead(self, branchBase, branchType):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        
        p = RPG()
        p.StartPattern()
        p.Jump("GOTO_I", "eval[PRE_RAND]")
        p.AddSubroutine("SUBR", 50, vectorData)
        p.AddBlock("PRE_RAND", 150, 0)
        p.CallSubr(branchBase, branchType, "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        pattern.LoadString(p.GetPattern())
        
        #print(bin(vectorData))
        expectedCount = 302
        self.ExecuteTest(pattern)
        
    def SimpleSubrTail(self, branchBase, branchType):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        
        p = RPG()
        p.StartPattern()        
        p.AddBlock("PRE_RAND", 150, 0)
        p.CallSubr(branchBase, branchType, "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        p.AddSubroutine("SUBR", 50, vectorData)
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 302
        self.ExecuteTest(pattern)
        
    def SimpleSubrCFB(self, branchBase, branchType):
        CFBAddr = 512
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        patternCfb = PatternAssembler(CFBAddr)
        pCfb = RPG()
        pCfb.AddSubroutine("SUBR", 50, vectorData)  
        patternCfb.LoadString(pCfb.GetPattern())
        self.LoadPattern(patternCfb, CFBAddr)        
        
        pattern = PatternAssembler()
        p = RPG()
        p.StartPattern()  
        p.SetCFB(CFBAddr)        
        p.AddBlock("PRE_RAND", 150, 0)  
        p.CallSubr(branchBase, branchType, 0)        
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 302
        self.ExecuteTest(pattern)
         
         
    def LoadPattern(self, pattern, offset):
        data = pattern.Generate()
        #pattern.SaveObj('test.obj') 
        for (slot, slice) in self.env.fpgas:
            self.env.WritePattern(slot, slice, offset*16, data) # offset should be modular by 16
         
    def ExecuteTest(self, pattern):
        self.env.SetConfig('InternalLoopback')
        patternData = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, patternData, 20e-9)
            self.env.RunCheckers(slot, slice)
            
    def RandomSubroutineTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()        
        
        # randomized parameters         
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CALL': 0.002}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('subroutine1.obj')
        
        for (slot, slice) in self.env.fpgas:
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)
    
    def DirectedGotoAllAddrInOrderTest(self):        
        self.env.SetConfig('InternalLoopback')        
        addressList = [0]
        # 0x8000,0000 is not valid
        for i in range(22):
            addressList.append(0x200 << i)
        addressList.append(0x70000000)
        pattern = {}
        for i in range(len(addressList)):
            addr = addressList[i]
            patternAsm = PatternAssembler()
            if i+1 == len(addressList):
                patternAsm.LoadString("""\
                    S stype=IOSTATEJAM, data=`rand(112)`
                    Drive0CompareLVectors length=200
                    I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={}, imm=0xABCDEF{}
                    StopPattern 0x12345678
                """.format(i, i))
            else:            
                patternAsm.LoadString("""\
                    S stype=IOSTATEJAM, data=`rand(112)`
                    Drive0CompareLVectors length=200
                    I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={}, imm=0xABCDEF{}
                    Goto {}            
                    StopPattern 0x12345678
                """.format(i, i, addressList[i+1]))
            pattern[addr] = patternAsm.Generate()
        
        for (slot, slice) in self.env.fpgas:            
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            for addr in addressList:
                self.env.WritePattern(slot, slice, addr*16, pattern[addr], checkSimMemory = False)
            
            self.env.RunPattern(slot, slice, None, period = 5e-9, captureAll = False, captureAddress = 0x7FFFF000)
            self.env.CheckAlarms(slot, slice)
            
            for i in range(24): # 22+2
                r = hpcc.ac[slice].Read('CentralDomainRegister{}'.format(i)).Pack()
                expR = int('0xABCDEF' + str(i), 0)
                if r != expR:
                    self.Log('error', 'r{} = 0x{:X}, expected 0x{:X}'.format(i, r, expR))
                else:
                    self.Log('debug', 'r{} = 0x{:X}, expected 0x{:X}'.format(i, r, expR))

        
class Unconditional(HpccTest):
    def DirectedGotoImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            Goto L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedGotoImmediateRelativeTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            Goto eval[int32(L17-vecaddr)], base=RELATIVE
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedGotoRegisterGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            SetRegister 0, L23
            GotoR 0
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x23:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x23))
            self.env.RunCheckers(slot, slice)

    def DirectedGotoRegisterRelativeTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            SetRegister 0, eval[int32(L23-vecaddr)]
            GotoR 0, base=RELATIVE
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x23:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x23))
            self.env.RunCheckers(slot, slice)

class Conditional(HpccTest):
    # use LTE for signed operation
    def DirectedPCallLessThanOrEqualCarry1Test(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            SetRegister 0, 0xffffffff # -1
            SetRegister 1, 0xfffffffe # -2
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=2 # -1 - (-2) = 1
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`L17`, cond=LTE
            StopPattern 0x69dead69
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x69dead69:
                self.Log('error', 'Unexpected end status 0x{:X} (probably branch was incorrect), expecting 0x{:X}'.format(endStatus, 0x69dead69))
            self.env.RunCheckers(slot, slice)
            hpcc = self.env.instruments[slot]
            flag = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            if flag.ALUOverflow != 0 or flag.ALUSign != 0:
                self.Log('error', 'flag.ALUOverflow = {}, flag.ALUSign = {}, expect 0 for both'.format(flag.ALUOverflow, flag.ALUSign))
    
    def DirectedPCallLessThanOrEqualCarry0Test(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            SetRegister 0, 0xA0000000  
            SetRegister 1, 0x1 
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=2
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`L17`, cond=LTE
            StopPattern 0x69dead69
            L17:
            AddressVectors length=17
            StopPattern 0x17
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status 0x{:X} (probably branch was incorrect), expecting 0x{:X}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)
            hpcc = self.env.instruments[slot]
            flag = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            #print(hpcc.ac[slice].Read('CentralDomainRegister0'))
            #print(hpcc.ac[slice].Read('CentralDomainRegister1'))
            #print(hpcc.ac[slice].Read('CentralDomainRegister2'))
            #print(flag)
            if flag.ALUOverflow != 0 or flag.ALUSign != 1:
                self.Log('error', 'flag.ALUCarry = {}, expect 0'.format(flag.ALUCarry))
                
    def DirectedPCallGreaterThanOrEqualTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            SetRegister 0, 0xA0000000  
            SetRegister 1, 0x1 
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=2
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`L17`, cond=GTE
            StopPattern 0x69dead69
            L17:
            AddressVectors length=17
            StopPattern 0x17
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x69dead69:
                self.Log('error', 'Unexpected end status 0x{:X} (probably branch was incorrect), expecting 0x{:X}'.format(endStatus, 0x69dead69))
            self.env.RunCheckers(slot, slice)
            hpcc = self.env.instruments[slot]
            flag = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            #print(hpcc.ac[slice].Read('CentralDomainRegister0'))
            #print(hpcc.ac[slice].Read('CentralDomainRegister1'))
            #print(hpcc.ac[slice].Read('CentralDomainRegister2'))
            #print(flag)
            if flag.ALUOverflow != 0 or flag.ALUSign != 1:
                self.Log('error', 'flag.ALUCarry = {}, expect 0'.format(flag.ALUCarry))
    
    # use BE for unsigned operation
    # FPGA bug: Carry should be 0 instead of 1
    def DirectedPCallBelowOrEqualTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            SetRegister 0, 0xA0000000  
            SetRegister 1, 0x1 
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=2
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`L17`, cond=BE
            StopPattern 0x69dead69
            L17:
            AddressVectors length=17
            StopPattern 0x17
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x69dead69:
                self.Log('error', 'Unexpected end status 0x{:X} (probably branch was incorrect), expecting 0x{:X}'.format(endStatus, 0x69dead69))
            self.env.RunCheckers(slot, slice)
            hpcc = self.env.instruments[slot]
            flag = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            #print(hpcc.ac[slice].Read('CentralDomainRegister0'))
            #print(hpcc.ac[slice].Read('CentralDomainRegister1'))
            #print(hpcc.ac[slice].Read('CentralDomainRegister2'))
            #print(flag)
            if flag.ALUCarry != 0:
                self.Log('error', 'flag.ALUCarry = {}, expect 0'.format(flag.ALUCarry))
                
        
    def DirectedGotoIfCarryImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            GotoIfCarry L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedGotoIfOverflowImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the overflow flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            GotoIfOverflow L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedGotoIfSignImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x80000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000000
            # r1 = r0 + 0x00000000 (this should set the sign flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            GotoIfSign L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedGotoIfZeroImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x00000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x00000000
            # r1 = r0 + 0x00000000 (this should set the zero flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            GotoIfZero L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, 10e-9)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

class StickyError(HpccTest):
        
    def RandomDomainStickyErrorTest(self):    
        self.env.SetConfig('EvenToOddLoopback')
        period = random.uniform(1250, 100000) * 1e-12
        PADDING_BEFORE = random.randint(100,20000)
        PADDING_AFTER = 22000 + int(5e-6 / period)
        self.env.Log('info', 'period = {}, padding before = {}, padding after = {}'.format(period, PADDING_BEFORE, PADDING_AFTER))
        asm = PatternAssembler()
        # after error 10k vectors failed, between 10k-15k, 15k failed if error outside pre-stage, need 20k
        # after clear sticky errors, need 20 vector padding, 10 doesn't work
        # Repeat 64
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
            %repeat {} 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %repeat {} 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=BRANCH, br=GOTO_I, cond=COND.DSE, imm=`SUBR`
            StopPatternReturnFlags
            SUBR:
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            StopPatternReturnFlags
        """.format(PADDING_BEFORE, PADDING_AFTER))
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period, captureType = ac_registers.CaptureControl.FAIL_STATUS, captureFails = True, captureCTVs = False, captureAll = False)
            #self.env.RunPattern(slot, slice, pattern, period, captureType = ac_registers.CaptureControl.PIN_STATE)            
            self.env.RunCheckers(slot, slice, endStatusMask = 0xFFFFFFFF)
            #self.env.DumpCapture(slot, slice)
            
            # flags = ac_registers.CentralDomainRegisterFlags()
            #print(flags)
            #if flags.LocalStickyError != 0:
            #    self.Log('error', 'Local Sticky Error bit is set in flags=0x{:x}'.format(endStatus))
