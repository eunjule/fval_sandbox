################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: KeepAlive.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for keep alive features 
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 07/12/16
#       Group: HDMT FPGA Validation
################################################################################

import time

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class Reset(HpccTest):
    # resets capture and capture count, but not fail count    
    def DirectedResetCaptureMemoryAndCountTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            Drive1CompareHVectors 16384
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            LOOP:
                %repeat 200                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 63-113, 
                %end
                I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1   
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 1 fail
            Drive0CompareLVectors 512           
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        period = 5e-9
        domainMaster = False
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]    
            data1 = hpcc.ac[slice].Read('TriggerControl')
            if not domainMaster:
                data1.DomainMaster = 1
                domainMaster = True
            data1.DomainID = 0
            hpcc.ac[slice].Write('TriggerControl', data1)
            
            self.env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            self.env.WritePattern(slot, slice, 0, pdata)
            # Setup capture
            captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pdata))
            hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
            hpcc.ac[slice].SetCaptureCounts()
            # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False)
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, True, False, False) # CTV
            # Clear fail counts
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            hpcc.ac[slice].PrestagePattern()

        # Send sync pulse    
        self.env.rc.send_sync_pulse()
        time.sleep(0.1)
        # reset capture memory and capture count
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            reset = hpcc.ac[slice].Read('ResetsAndStatus')
            reset.ResetCaptureMemory = 1
            hpcc.ac[slice].Write('ResetsAndStatus', reset)
            reset.ResetCaptureMemory = 0
            hpcc.ac[slice].Write('ResetsAndStatus', reset)
        time.sleep(0.1)    
        self.env.rc.send_trigger(0x0) # pattern resume trigger
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                
            hpcc.DumpCapture(slice)
            
            captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            if captureCount != 1:
                self.Log('error', 'captureCount = {}, expect 1'.format(captureCount))  
            totalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if totalFailCount != 2:
                self.Log('error', 'totalFailCount = {}, expect 2'.format(totalFailCount))   
            for ch in range(56):
                channelFailCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount').Pack()
                if ch == 1 or ch == 55:
                    if channelFailCount != 1:
                        self.Log('error', 'ch {}, fail count {}, expect 1'.format(ch, channelFailCount))   
                elif channelFailCount != 0:    
                    self.Log('error', 'ch {}, fail count {}, expect 0'.format(ch, channelFailCount))     
                
              