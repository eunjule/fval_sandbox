################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import gzip
import os
import random
import time

from Hpcc import hpcc_configs as configs
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
from Tools import projectpaths
import Hpcc.instrument.hpccAcRegs as ac_registers

period = configs.INTERNAL_LOOPBACK_PERIOD
class PVCCacheUnderRun(HpccTest):
    def DirectedFVALShortLoopSizeTest(self):
        self.env.SetConfig('InternalLoopback')
        #period = 2.5e-9
        asm = PatternAssembler()
        asm.LoadString("""
            StartPattern
            RandomVectors length=256
            SetRegister 0, 20000
            LOOP:
                RandomVectors length=4
                DecRegister 0
            GotoIfNonZero `LOOP`
            StopPatternReturnRegister reg=0
        """)
        pattern = asm.Generate()
        failed = None
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.Log('info', 'Period'.format(period))
            self.env.RunPattern(slot, slice, pattern, period = period, start = 0, captureFails = True, captureCTVs = False, captureAll = False, maskFailureToComplete = True)
            # NOTE: Checkers will not work in PVCCacheUnderRun scenario
            #self.env.RunCheckers(slot, slice)
            alarms = self.env.CheckAlarms(slot, slice, alarmsToMask = ['PVCCacheUnderRun','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'PVCCacheUnderRun' not in alarms:
                self.Log('error', 'PVCCacheUnderRun is not set in AlarmControl')

    def DirectedFVALHighAluInstructionDensityTest(self):
        self.env.SetConfig('InternalLoopback')
        period = 10e-9
        asm = PatternAssembler()
        asm.symbols['SIZE'] = 448
        asm.LoadString("""
            StartPattern
            ClearRegisters
            RandomVectors length=8192
            %repeat SIZE
                I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, imm=1
            %end
            RandomVectors length=8192
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        failed = None
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = period)
            passed, alarms = self.env.RunCheckers(slot, slice, checkRegisters = True)
           
class StackError(HpccTest):


    def DirectedFVALStackOverflowTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            RandomVectors length=100                                                                        # 1-100
            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm=64                         # 101
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`SUBR`                                            # 102
            StopPattern 0x69696969                                                                          # 103,104
            SUBR:
                RandomVectors length=20100                                                                  # 105-20204
                I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1       # 20205
                I optype=BRANCH, base=PFB, br=PCALL_I, imm=0, cond=ZERO, invcond=1                          # 20206
                RandomVectors length=100                                                                    # 20207-20306
                I optype=BRANCH, br=RET
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StackError', 'IllegalInstruction', 'PatternEndStatusCheck','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'StackError' not in alarms:
                self.Log('error', 'StackError is not set in AlarmControl')
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead'.format(actualEndStatus))
            errorAddress = hpcc.ac[slice].Read('ErrorAddress').Pack()
            if errorAddress != 20206:
                self.Log('error', 'error address = 0x{:x}, expect 0xCE, at PCALL'.format(errorAddress))
                
    def DirectedFVALStackUnderRunTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            RandomVectors length=20100                                                                      # 1-20100
            I optype=BRANCH, br=RET                                                                         # 20101
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, period=period, captureAll = False)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StackError', 'IllegalInstruction', 'PatternEndStatusCheck','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'StackError' not in alarms:
                self.Log('error', 'StackError is not set in AlarmControl')
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead'.format(actualEndStatus))
            errorAddress = hpcc.ac[slice].Read('ErrorAddress').Pack()
            if errorAddress != 20101:
                self.Log('error', 'error address = 0x{:x}, expect 0x65, at Return'.format(errorAddress))

    def DirectedFVALNonEmptyStackAtPatternEndTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            RandomVectors length=20100                                                                      # 1-20100
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`SUBR`                                            # 20101
            StopPattern 0x69696969                                                                          # 20102,20103
            SUBR:
                RandomVectors length=100                                                                    # 20104-20203
                StopPattern 0x69696969                                                                      # 20204
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, period=period, captureAll = False)
            passed, alarms = self.env.RunCheckers(slot, slice)
            errorAddress = hpcc.ac[slice].Read('ErrorAddress').Pack()
            if errorAddress != 20204:
                self.Log('error', 'error address = 0x{:x}, expect 0xCc, at END_I'.format(errorAddress))
                
class Subroutine(HpccTest):
    def DirectedFVALSubrHeadCallIGlobalTest(self):
        self.SimpleSubrHead("GLOBAL", "CALL_I")
        
    def DirectedFVALSubrTailCallIGlobalTest(self):
        self.SimpleSubrTail("GLOBAL", "CALL_I")
        
    def DirectedFVALSubrHeadCallRRelTest(self):
        self.SimpleSubrHead("RELATIVE", "CALL_R")
        
    def DirectedFVALSubrTailCallRRelTest(self):
        self.SimpleSubrTail("RELATIVE", "CALL_R")
   
    def DirectedFVALSubrTailGapCallICfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "CALL_I")
        
    def DirectedFVALSubrTailGapCallRCfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "CALL_R")
        
    def DirectedFVALSubrTailGapPCallICfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "PCALL_I")
        
    def DirectedFVALSubrTailGapPCallRCfbTest(self):
        self.SimpleSubrCFB("BRBASE.CFB", "PCALL_R")
        
    def DirectedFVALSubrRecursivePCallIPfb10Test(self):
        self.RecursivePCallI_Pfb(10)
        
    def DirectedFVALSubrRecursivePCallIPfb63Test(self):
        self.RecursivePCallI_Pfb(63)
    
    def DirectedFVALSubrNestedPCallIGlobalTest(self):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        callnum = 10
        
        p = RPG()
        p.StartPattern()   
        p.ClearStack()
        p.AddBlock("PRE_RAND", 150, 0)
        p.SetRegFromImm(1, callnum) # r1 = 10
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_1")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        # subroutine
        p.AddBlock("SUBR_1", 50, vectorData)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_2", "ZERO", "1") 
        p.AddBlock("SUBR_1E", 50, vectorData)
        p.Return()
        p.AddBlock("SUBR_2", 60, vectorData)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_1", "ZERO", "1") 
        p.AddBlock("SUBR_2E", 60, vectorData)
        p.Return()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 362 + 110 * (callnum - 1)
        self.ExecuteTest(pattern)
        
    def RecursivePCallI_Pfb(self, callnum):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        
        p = RPG()
        p.StartPattern()   
        p.ClearStack()
        p.AddBlock("PRE_RAND", 150, 0)
        p.SetRegFromImm(1, callnum) # r1 = 10
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        # subroutine
        p.AddBlock("SUBR", 100, vectorData)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("PFB", "PCALL_I", 0, "ZERO", "1", pfb='eval[SUBR]') # should call subr itself
        p.AddBlock("SUBRE", 100, vectorData)
        p.Return()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 452 + 200 * (callnum - 1)
        self.ExecuteTest(pattern)
        
    def SimpleSubrHead(self, branchBase, branchType):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        
        p = RPG()
        p.StartPattern()
        p.Jump("GOTO_I", "eval[PRE_RAND]")
        p.AddSubroutine("SUBR", 50, vectorData)
        p.AddBlock("PRE_RAND", 150, 0)
        p.CallSubr(branchBase, branchType, "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        pattern.LoadString(p.GetPattern())
        
        #print(bin(vectorData))
        expectedCount = 302
        self.ExecuteTest(pattern)
        
    def SimpleSubrTail(self, branchBase, branchType):
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        pattern = PatternAssembler()
        
        p = RPG()
        p.StartPattern()        
        p.AddBlock("PRE_RAND", 150, 0)
        p.CallSubr(branchBase, branchType, "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        p.AddSubroutine("SUBR", 50, vectorData)
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 302
        self.ExecuteTest(pattern)
        
    def SimpleSubrCFB(self, branchBase, branchType):
        CFBAddr = 512
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        patternCfb = PatternAssembler(CFBAddr)
        pCfb = RPG()
        pCfb.AddSubroutine("SUBR", 50, vectorData)  
        patternCfb.LoadString(pCfb.GetPattern())
        self.LoadPattern(patternCfb, CFBAddr)        
        
        pattern = PatternAssembler()
        p = RPG()
        p.StartPattern()  
        p.SetCFB(CFBAddr)        
        p.AddBlock("PRE_RAND", 150, 0)  
        p.CallSubr(branchBase, branchType, 0)        
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        pattern.LoadString(p.GetPattern())
        
        expectedCount = 302
        self.ExecuteTest(pattern)
         
    def LoadPattern(self, pattern, offset):
        data = pattern.Generate()
        #pattern.SaveObj('test.obj') 
        for (slot, slice) in self.env.fpgas:
            self.env.WritePattern(slot, slice, offset*16, data) # offset should be modular by 16
         
    def ExecuteTest(self, pattern):
        self.env.SetConfig('InternalLoopback')
        patternData = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, patternData, period=period )
            self.env.RunCheckers(slot, slice)
            
class Unconditional(HpccTest):
    def DirectedFVALGotoImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            Goto L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedFVALGotoImmediateRelativeTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            Goto eval[int32(L17-vecaddr)], base=RELATIVE
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedFVALGotoRegisterGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            SetRegister 0, L23
            GotoR 0
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x23:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x23))
            self.env.RunCheckers(slot, slice)

    def DirectedFVALGotoRegisterRelativeTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            SetRegister 0, eval[int32(L23-vecaddr)]
            GotoR 0, base=RELATIVE
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x23:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x23))
            self.env.RunCheckers(slot, slice)

class Conditional(HpccTest):
    def DirectedFVALGotoIfCarryImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            GotoIfCarry L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedFVALGotoIfOverflowImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the overflow flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            GotoIfOverflow L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedFVALGotoIfSignImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x80000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000000
            # r1 = r0 + 0x00000000 (this should set the sign flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            GotoIfSign L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

    def DirectedFVALGotoIfZeroImmediateGlobalTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            AddressVectors length=1024
            ClearFlags
            # r0 = 0x00000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x00000000
            # r1 = r0 + 0x00000000 (this should set the zero flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            GotoIfZero L17
            StopPattern 0x69dead69
            L11:
            AddressVectors length=11
            StopPattern 0x11
            L13:
            AddressVectors length=13
            StopPattern 0x13
            L17:
            AddressVectors length=17
            StopPattern 0x17
            L19:
            AddressVectors length=19
            StopPattern 0x19
            L23:
            AddressVectors length=23
            StopPattern 0x23
            L31:
            AddressVectors length=31
            StopPattern 0x31
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period)
            if endStatus != 0x17:
                self.Log('error', 'Unexpected end status {} (probably goto was incorrect), expecting {}'.format(endStatus, 0x17))
            self.env.RunCheckers(slot, slice)

class CaptureAll(HpccTest):
    EPSILON = 1E-11

    def SearchPeriod(self, slot, slice, pattern, start, end, resolution, disabledRules):
        end, start = sorted(list([start, end]))
        period = None
        value = start
        while value - end > self.EPSILON:
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period = value, maskFailureToComplete = True)
            passed, alarms = self.env.RunCheckers(slot, slice, endStatusMask = 0xffffffff, alarmsToMask = ['PVCCacheUnderRun'], captureDisableRules = disabledRules)
            passed = passed and completed
            if not(passed):
                break
            period = value
            value = value - resolution
        return period
    def DirectedFVALMinPeriodTest(self):
        # Disabled checker rules that are expected to fail at really fast speeds
        disabledRules = [
            'missing-capture',
            'vecaddr-mismatch',
            'data-mismatch',
            'ucc-mismatch',
            'pcc-mismatch',
            'ctv-mismatch',
            'userlog1-mismatch',
            'userlog2-mismatch',
            'extra-vectors',
            'dropped-vectors',
            'tcc-mismatch',
            'patid-mismatch',
        ]
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=102400
            StopPattern 0x0
        """)
        pattern = asm.Generate()
        start = 3E-9
        end = 1.20E-9
        resolution = 0.10E-9
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            period = self.SearchPeriod(slot, slice, pattern, start, end, resolution, disabledRules)
            self.Log('info', 'For ({},{}), fastest passing period = {}'.format(slot, slice, period))
            if period > 2.5e-9:
                self.Log('error', '({},{}) failed "min period" test, expected<2.5e-9, actual={}'.format(slot, slice, period))

class BasicFlatPattern(HpccTest):
    def MiniFVAL1KVTest(self):
        self.RunInternalLoopbackScenario(1024)

    def MiniFVAL10KVTest(self):
        self.RunInternalLoopbackScenario(10 * 1024)

    def RunInternalLoopbackScenario(self, length):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = length
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            RandomVectors length=REPEATS
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0j11111111111111111111111111111111111111111111111111111111
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period=period)
            self.env.RunCheckers(slot, slice)


class FlatPattern(HpccTest):
    def _DriveWalkingOneAllPattern(self):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 20
        #asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """)
        return pattern.Generate()

    def _FindSubCyclePeriodRange(self):
        overlappingValue = 100
        subCycleOne = 2
        subCycleTwo = 4
        interval = 65
        periodRangeDict = {}
        oneSlicePeriodRangeList = None
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            hpcc.ac[slice]._UpdateFpgaCapabilities()
            subCycleOneLowerBound = hpcc.ac[slice].minQdrMode * subCycleOne
            subCycleOneUpperBound = hpcc.ac[slice].minQdrMode * subCycleTwo 
            print(subCycleOneLowerBound) 
            subCycleInterval = int(  (subCycleOneUpperBound - subCycleOneLowerBound)/ interval )
            periodRangeDict[slot, slice] = range( subCycleOneLowerBound, subCycleOneUpperBound+ overlappingValue, subCycleInterval )  
            oneSlicePeriodRangeList = periodRangeDict[slot, slice]
        
        return oneSlicePeriodRangeList

    def MiniFVALVectorAddrTest(self):
        
        vectorSize = 10*1024
        self.env.SetConfig('InternalLoopback')
        vecAddr = ''
        for i in range(vectorSize):
            vecAddr += 'V link=0, ctv=0, mtv=0, lrpt=0, data=0v{:024b}{:032b}\n'.format(i, i)
        source = """\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            {}
            StopPattern 0x12345678
        """.format(vecAddr) 

        asm = PatternAssembler()
        asm.LoadString(source)
        pattern = asm.Generate()

        for i in range(10):
            period0 = random.uniform(2.5e-9, 25e-9)
            period1 = random.uniform(2.5e-9, 25e-9)
            self.Log('info', 'Testing period = {} for slice 0'.format(period0))
            self.Log('info', 'Testing period = {} for slice 1'.format(period1))
            for (slot, slice) in self.env.fpgas:
                if slice == 0:
                    self.env.LogPerSlice(slot, slice)
                    completed, endstate = self.env.RunPattern(slot, slice, pattern, period0)
                    self.env.RunCheckers(slot, slice)
                    if completed == False:
                        continue
                
                if slice == 1:
                    self.env.LogPerSlice(slot, slice)
                    completed, endstate = self.env.RunPattern(slot, slice, pattern, period1)
                    self.env.RunCheckers(slot, slice)
                    if completed == False:
                        continue

class Misses(HpccTest):
    def ThrFVALTims32KVFlatPatternTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, r'Hpcc\Patterns\flat_long_32k.0.obj.gz'), 'rb').read()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False)
            self.env.RunCheckers(slot, slice)

    def ThrFVALCTVEvery512BlocksTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM, data=`rand(112)`
            RandomVectors length=511
            %repeat 100
                V link=0, ctv=1, mtv=0, lrpt=0, data=`rand(112)`
                %repeat 511
                    V link=0, ctv=0, mtv=0, lrpt=0, data=`rand(112)`
                %end
            %end
            StopPattern `rand(32)`
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, captureCTVs = True, captureAll = False)
            self.env.RunCheckers(slot, slice)

class Alu(HpccTest):
    def DirectedFVALSetCarryTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the carry flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 1:
                self.Log('error', 'ALU carry bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedFVALClearCarryTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the carry flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUCarry != 0:
                self.Log('error', 'ALU carry bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedFVALSetOverflowTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the overflow flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the overflow flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUOverflow != 1:
                self.Log('error', 'ALU overflow bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedFVALClearOverflowTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000001
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000001
            # r1 = r0 + 0x80000000 (this should set the overflow flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x80000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the overflow flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUOverflow != 0:
                self.Log('error', 'ALU overflow bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedFVALSetZeroTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x00000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x00000000
            # r1 = r0 + 0x00000000 (this should set the zero flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the zero flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUZero != 1:
                self.Log('error', 'ALU zero bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags

    def DirectedFVALClearZeroTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x00000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x00000000
            # r1 = r0 + 0x00000000 (this should set the zero flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the zero flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUZero != 0:
                self.Log('error', 'ALU zero bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedFVALSetSignTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000000
            # r1 = r0 + 0x00000000 (this should set the sign flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the sign flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUSign != 1:
                self.Log('error', 'ALU sign bit is not set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags
    def DirectedFVALClearSignTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            # r0 = 0x80000000
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=0x80000000
            # r1 = r0 + 0x00000000 (this should set the sign flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=1, imm=0x00000000
            ClearFlags
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the sign flag
            flags = ac_registers.Flags(endStatus)
            if flags.ALUSign != 0:
                self.Log('error', 'ALU sign bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xE0000000)  # Don't check the 3 most significant bits of the flags

class Misc(HpccTest):
    def SetScenario(self, flagName, flagBit):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['VALUE'] = 0b1 << flagBit
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            I optype=ALU, aluop=OR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=VALUE
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the flag
            flags = ac_registers.Flags(endStatus)
            if getattr(flags, flagName) != 1:
                self.Log('error', '{} bit is not set in flags=0x{:x}'.format(flagName, endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xF0000000)  # Don't check the 4 most significant bits of the flags
    def DirectedFVALSetWiredOrTest(self):
        self.SetScenario('WireOr', 20)
    def DirectedFVALSetLoadActiveTest(self):
        self.SetScenario('LoadActive', 21)
    def DirectedFVALSetSoftwareTriggerTest(self):
        self.SetScenario('SoftwareTrigger', 22)
    def DirectedFVALSetDomainTriggerTest(self):
        self.SetScenario('DomainTrigger', 23)
    def DirectedFVALSetRCTriggerTest(self):
        self.SetScenario('RCTrigger', 24)
    def ClearScenario(self, flagName, flagBit):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['VALUE'] = 0b1 << flagBit
        asm.LoadString("""\
            StartPattern
            RandomVectors length=128
            I optype=ALU, aluop=OR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=VALUE
            RandomVectors length=128
            I optype=ALU, aluop=XOR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=VALUE
            StopPatternReturnFlags
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            completed, endStatus = self.env.RunPattern(slot, slice, pattern, period=period)
            # Check the flag
            flags = ac_registers.Flags(endStatus)
            if getattr(flags, flagName) != 0:
                self.Log('error', '{} bit is set in flags=0x{:x}'.format(flagName, endStatus))
            self.env.RunCheckers(slot, slice, endStatusMask = 0xF0000000)  # Don't check the 4 most significant bits of the flags
    def DirectedFVALClearWiredOrTest(self):
        self.ClearScenario('WireOr', 20)
    def DirectedFVALClearLoadActiveTest(self):
        self.ClearScenario('LoadActive', 21)
    def DirectedFVALClearSoftwareTriggerTest(self):
        self.ClearScenario('SoftwareTrigger', 22)
    def DirectedFVALClearDomainTriggerTest(self):
        self.ClearScenario('DomainTrigger', 23)
    def DirectedFVALClearRCTriggerTest(self):
        self.ClearScenario('RCTrigger', 24)

class Triggers(HpccTest):
    def Scenario(self, trigger, condition):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['CONDITION'] = condition
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            ClearFlags
            LOOP:
                RandomVectors length=256
            GotoIfNon `LOOP`, CONDITION
            StopPattern `rand(32)`
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetDomains({
                (slot, slice): (True, slot),
            })
            hpcc = self.env.instruments[slot]
            self.env.AbortPattern(slot, slice)  # TODO(rodny): Remove this line
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period=period, captureAll = False, captureFails = True, captureCTVs = True, waitForComplete = False)
            self.env.rc.send_trigger(trigger, slot)
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
            # TODO
            #self.env.RunCheckers(slot, slice, endStatusMask = 0xF0000000)  # Don't check the 4 most significant bits of the flags
    def DirectedFVALDomainTriggerTest(self):
        self.Scenario(0x0ff2, 'COND.DMTRIGRCVD')
    def DirectedFVALDomainTriggerPropagateTest(self):
        self.Scenario(0x0ff3, 'COND.DMTRIGRCVD')
    def DirectedFVALCentralRegisterLoadActiveTriggerTest(self):
        self.Scenario(0x0ff9, 'COND.LOADACTIVE')
# FIXME
#    def DirectedClearCentralRegisterLoadActiveTriggerTest(self):
#        self.Scenario(0x0ffa, 'COND.LOADACTIVE')
    def DirectedFVALRCTriggerTest(self):
        self.Scenario(0x0ffb, 'COND.RCTRIGRCVD')
    def DirectedFVALRCTriggerPropagateTest(self):
        self.Scenario(0x0ffc, 'COND.RCTRIGRCVD')

class Abort(HpccTest):
    def DirectedFVALAbortFlatTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            LOOP:
                RandomVectors length=356
            Goto `LOOP`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            self.env.AbortPattern(slot, slice)
            self.env.SetSimAbort(slot, slice, 300)
            self.env.RunCheckers(slot, slice, alarmsToMask = ['MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            
    def DirectedFVALAbortInsidePatternTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN1`            
            StopPattern 0x12345678
            
            PATTERN1:
                RandomVectors length=256
                LOOP:
                    RandomVectors length=256
                Goto `LOOP`
                I optype=BRANCH, br=RET            
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            time.sleep(1)
            self.env.AbortPattern(slot, slice)
            
            hpcc = self.env.instruments[slot]
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'Unexpect end status = 0x{:x}, expected = 0x1800dead.'.format(actualEndStatus))
                
    def DirectedFVALAbortNestedPCallTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN1`            
            StopPattern 0x12345678
            
            PATTERN1:
                RandomVectors length=256
                LOOP:
                    I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
                    I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN2`  
                Goto `LOOP`
                I optype=BRANCH, br=RET   
            PATTERN2:
                RandomVectors length=256
                I optype=BRANCH, br=RET  
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            time.sleep(1)
            self.env.AbortPattern(slot, slice)
            
            hpcc = self.env.instruments[slot]
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'Unexpect end status = 0x{:x}, expected = 0x1800dead.'.format(actualEndStatus))

    def DirectedFVALFlushThenAbortTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            %repeat 313
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            LOOP:
                RandomVectors length=356
            Goto `LOOP`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = True, waitForComplete = False)
            
            data = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            self.Log('debug', 'Before: TotalCaptureCount = {}'.format(data))
            data = hpcc.ac[slice].Read('CaptureControl')
            self.Log('debug', 'Before: CaptureControl.CaptureBufferStatus = {}'.format(data.CaptureBufferStatus))
            
            data = hpcc.ac[slice].Read('CaptureControl')
            data.FlushCapture = 1
            hpcc.ac[slice].Write('CaptureControl', data)
            # Wait for flush to complete
            completed = False
            for i in range(500):
                time.sleep(0.01)
                data = hpcc.ac[slice].Read('CaptureControl')
                completed = data.FlushCapture == 0
                if completed:
                    break
            if completed:
                self.Log('debug', 'Capture flush completed')
            else:
                self.Log('error', 'Capture flush did not complete')
            data = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            self.Log('debug', 'After: TotalCaptureCount = {}'.format(data))
            data = hpcc.ac[slice].Read('CaptureControl')
            self.Log('debug', 'After: CaptureControl.CaptureBufferStatus = {}'.format(data.CaptureBufferStatus))
            # Abort it
            self.env.AbortPattern(slot, slice)
            self.env.SetSimAbort(slot, slice, 350)
            self.env.RunCheckers(slot, slice,alarmsToMask = ['MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])

class ChannelLinking(HpccTest):
    def DirectedFVALLinkMode2xTest(self):
        self.BasicScenario(0, 0, 20e-9, 0)

    def DirectedFVALLinkMode4xTest(self):
        self.BasicScenario(0, 0, 20e-9, 1)

    def DirectedFVALLinkMode8xTest(self):
        self.BasicScenario(0, 0, 20e-9, 2)

    def BasicScenario(self, slice, offset, period, linkMode):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %repeat REPEATS
            V link=1, ctv=0, mtv=0, lrpt=0, data=0v00000000101010101010101010101010101010101010101011111111
            %end
            PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = linkMode
            data = hpcc.ac[slice].Write('PatternControl', data)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period=period)
            self.env.RunCheckers(slot, slice)

class LocalRepeat(HpccTest):
    def DirectedFVAL1KVTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=1, data=0v00000000101010101010101010101010101010101010101011111111
            %end
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period=period)
            self.env.RunCheckers(slot, slice)

    def RandomFVAL1KVTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            S stype=IOSTATEJAM, data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=`rand(4)`, data=`rand(56)`
            %end
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period=period)
            self.env.RunCheckers(slot, slice)

class InstructionRepeat(HpccTest):
    def DirectedFVALImmediateTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            Repeat 13
            RandomVectors length=1
            Repeat 3
            RandomVectors length=1
            Repeat 5
            RandomVectors length=1
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period=period)
            self.env.RunCheckers(slot, slice)

    def DirectedFVALRegisterTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            SetRegister 0, 13
            RepeatR 0
            RandomVectors length=1
            SetRegister 0, 3
            RepeatR 0
            RandomVectors length=1
            SetRegister 1, 5
            RepeatR 1
            RandomVectors length=1
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period=period)
            self.env.RunCheckers(slot, slice)

    def DirectedFVALRepeat64Test(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=32768
            Repeat 64
            RandomVectors length=1
            RandomVectors length=75
            StopPattern 0x69696969
        """)
        #asm.SaveObj('Repeat64.obj')
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False)
            self.env.RunCheckers(slot, slice)
            
