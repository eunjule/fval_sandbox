################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Alarms.py
#-------------------------------------------------------------------------------
#     Purpose: Checks that the alarms actually occur
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 07/13/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import unittest

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest

class PVCCacheUnderRun(HpccTest):
    def DirectedShortLoopSizeTest(self):
        self.env.SetConfig('InternalLoopback')
        period = 2.5e-9
        asm = PatternAssembler()
        asm.LoadString("""
            StartPattern
            RandomVectors length=256
            SetRegister 0, 20000
            LOOP:
                RandomVectors length=4
                DecRegister 0
            GotoIfNonZero `LOOP`
            StopPatternReturnRegister reg=0
        """)
        pattern = asm.Generate()
        failed = None
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = period, start = 0, captureFails = True, captureCTVs = False, captureAll = False, maskFailureToComplete = True)
            # NOTE: Checkers will not work in PVCCacheUnderRun scenario
            #self.env.RunCheckers(slot, slice)
            alarms = self.env.CheckAlarms(slot, slice, alarmsToMask = ['PVCCacheUnderRun','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'PVCCacheUnderRun' not in alarms:
                self.Log('error', 'PVCCacheUnderRun is not set in AlarmControl')

    def DirectedHighAluInstructionDensityTest(self):
        self.env.SetConfig('InternalLoopback')
        period = 10e-9
        asm = PatternAssembler()
        asm.symbols['SIZE'] = 448
        asm.LoadString("""
            StartPattern
            ClearRegisters
            RandomVectors length=8192
            %repeat SIZE
                I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, imm=1
            %end
            RandomVectors length=8192
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        failed = None
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = period)
            passed, alarms = self.env.RunCheckers(slot, slice, checkRegisters = True)
            
class StopOnFirstFail(HpccTest):
    def DirectedTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 20e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            Drive0CompareLVectors `1024*1024`
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = period, stopOnMaxFail = True, captureAll = False, captureFails = True, captureCTVs = True, maxFailCountPerPatern = 1, maxFailCaptureCount = 1)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'StopOnFirstFail' not in alarms:
                self.Log('error', 'StopOnFirstFail is not set in AlarmControl')


class StackError(HpccTest):
    def DirectedStackOverflowTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            RandomVectors length=100                                                                        # 1-100
            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm=64                         # 101
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`SUBR`                                            # 102
            StopPattern 0x69696969                                                                          # 103,104
            SUBR:
                RandomVectors length=20100                                                                  # 105-20204
                I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1       # 20205
                I optype=BRANCH, base=PFB, br=PCALL_I, imm=0, cond=ZERO, invcond=1                          # 20206
                RandomVectors length=100                                                                    # 20207-20306
                I optype=BRANCH, br=RET
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, captureAll = False)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StackError', 'IllegalInstruction', 'PatternEndStatusCheck','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'StackError' not in alarms:
                self.Log('error', 'StackError is not set in AlarmControl')
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead'.format(actualEndStatus))
            errorAddress = hpcc.ac[slice].Read('ErrorAddress').Pack()
            if errorAddress != 20206:
                self.Log('error', 'error address = 0x{:x}, expect 0xCE, at PCALL'.format(errorAddress))        
            
    def DirectedStackUnderRunTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            RandomVectors length=20100                                                                      # 1-20100
            I optype=BRANCH, br=RET                                                                         # 20101
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, captureAll = False)
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StackError', 'IllegalInstruction', 'PatternEndStatusCheck','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            if 'StackError' not in alarms:
                self.Log('error', 'StackError is not set in AlarmControl')
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'pattern end status = 0x{:x}, expect 0x1800dead'.format(actualEndStatus))
            errorAddress = hpcc.ac[slice].Read('ErrorAddress').Pack()
            if errorAddress != 20101:
                self.Log('error', 'error address = 0x{:x}, expect 0x65, at Return'.format(errorAddress))

    def DirectedNonEmptyStackAtPatternEndTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            RandomVectors length=20100                                                                      # 1-20100
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`SUBR`                                            # 20101
            StopPattern 0x69696969                                                                          # 20102,20103
            SUBR:
                RandomVectors length=100                                                                    # 20104-20203
                StopPattern 0x69696969                                                                      # 20204
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pattern, captureAll = False)
            passed, alarms = self.env.RunCheckers(slot, slice)
            errorAddress = hpcc.ac[slice].Read('ErrorAddress').Pack()
            if errorAddress != 20204:
                self.Log('error', 'error address = 0x{:x}, expect 0xCc, at END_I'.format(errorAddress))
