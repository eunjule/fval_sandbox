################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: EdgeCounter.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for Edge Counter 
#-------------------------------------------------------------------------------
#  Created by: Sungjin Ho 
#        Date: 03/29/16
#       Group: HDMT FPGA Validation
################################################################################

import random
import re
import unittest

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class BasicTogglePattern(HpccTest):
    peri = 5e-9
    rpt = 1024

    def MiniFreqCounterSingleRisingEdgeShiftVectorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = 1
        self.basicSingleRisingEdgeShiftPatternSetup(period, repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSingleFallingEdgeShiftVectorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = 1
        self.basicSingleFallingEdgeShiftPatternSetup(period, repeat, drive, compare)
        self.CheckFreCounterRegister(expected)


    def MiniFreqCounterSimpleVectorBothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = repeat*2 
        self.basicTogglingPatternSetup(period, 'BOTH', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)


    @unittest.skip('Fpga Off feature not yet implemented')
    def MiniFreqCounterSimpleVectorOffEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = 0
        self.basicTogglingPatternSetup(period, 'OFF', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleVectorRiseEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = repeat
        self.basicTogglingPatternSetup(period, 'RISE', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleVectorFallEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = repeat
        self.basicTogglingPatternSetup(period, 'FALL', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleShiftedVectorBothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = period/2
        compare = period/4
        expected = (repeat*2 )-1
        self.basicTogglingPatternSetup(period, 'BOTH', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    @unittest.skip('Fpga Off feature not yet implemented')
    def MiniFreqCounterSimpleShiftedVectorOffEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = period/2
        compare = period/4
        expected = 0
        self.basicTogglingPatternSetup(period, 'OFF', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleShiftedVectorRiseEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = period/2
        compare = period/4
        expected = repeat
        self.basicTogglingPatternSetup(period, 'RISE', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleShiftedVectorFallEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = period/2
        compare = period/4
        expected = repeat - 1
        self.basicTogglingPatternSetup(period, 'FALL', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleVectorDoubleInstBothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = (repeat*2  + 1)*2
        self.basicTogglingPatternDoubleEdgeCounterSetup(period, 'BOTH', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    @unittest.skip('Fpga Off feature not yet implemented')
    def MiniFreqCounterSimpleVectorDoubleInstOffEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = 0
        self.basicTogglingPatternDoubleEdgeCounterSetup(period, 'OFF', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleVectorDoubleInstRiseEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = (repeat+1)*2
        self.basicTogglingPatternDoubleEdgeCounterSetup(period, 'RISE', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterSimpleVectorDoubleInstFallEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = self.peri
        repeat = self.rpt
        drive = 0e-9
        compare = period/2
        expected = repeat*2
        self.basicTogglingPatternDoubleEdgeCounterSetup(period, 'FALL', repeat, drive, compare)
        self.CheckFreCounterRegister(expected)

    @unittest.skip('Fpga feature not yet implemented')
    def MiniFreqCounterEnClkRatio1Duty50BothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = 5120
        waveForm = 'E'*repeat
        period = 5e-9
        ratio = 1
        compare = 0.25
        tfall = ratio*0.5
        expected = repeat*2
        self.RunEnabledClockScenario(waveForm, period, ratio, compare, tfall, 'BOTH')
        self.CheckFreCounterRegister(expected)

    @unittest.skip('Fpga feature not yet implemented')
    def MiniFreqCounterEnClkRatio1Duty50OffEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = 5120
        waveForm = 'E'*repeat
        period = 5e-9
        ratio = 1
        compare = 0.25
        tfall = ratio*0.5
        expected = repeat*0
        self.RunEnabledClockScenario(waveForm, period, ratio, compare, tfall, 'OFF')
        self.CheckFreCounterRegister(expected)

    @unittest.skip('Fpga feature not yet implemented')
    def MiniFreqCounterEnClkRatio1Duty50RiseEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = 5120
        waveForm = 'E'*repeat
        period = 5e-9
        ratio = 1
        compare = 0.25
        tfall = ratio*0.5
        expected = repeat*1
        self.RunEnabledClockScenario(waveForm, period, ratio, compare, tfall, 'RISE')
        self.CheckFreCounterRegister(expected)

    @unittest.skip('Fpga feature not yet implemented')
    def MiniFreqCounterEnClkRatio1Duty50FallEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = 5120
        waveForm = 'E'*repeat
        period = 5e-9
        ratio = 1
        compare = 0.25
        tfall = ratio*0.5
        expected = repeat*1
        self.RunEnabledClockScenario(waveForm, period, ratio, compare, tfall, 'FALL')
        self.CheckFreCounterRegister(expected)

    def MiniFreqCounterRatio2Duty50BothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = self.rpt
        waveForm = 'E'*repeat
        period = self.peri
        ratio = 2
        compare = 0.5
        tfall = ratio*0.5
        expected = int( repeat )
        self.RunEdgeCounterScenarioEvenToOddWithoutEnClk(waveForm, period, ratio, compare, tfall, 'BOTH')
        self.CheckFreCounterRegister(expected)
        
    @unittest.skip('Off edgecounter feature not implemented')	
    def MiniFreqCounterRatio2Duty50OffEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = self.rpt
        waveForm = 'E'*repeat
        period = self.peri
        ratio = 2
        compare = 0.5
        tfall = ratio*0.5
        expected = repeat*0
        self.RunEdgeCounterScenarioEvenToOddWithoutEnClk(waveForm, period, ratio, compare, tfall, 'OFF')
        self.CheckFreCounterRegister(expected)
        
    def MiniFreqCounterRatio2Duty50RiseEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = self.rpt
        waveForm = 'E'*repeat
        period = self.peri
        ratio = 2
        compare = 0.5
        tfall = ratio*0.5
        expected = int( repeat*1/2 )
        self.RunEdgeCounterScenarioEvenToOddWithoutEnClk(waveForm, period, ratio, compare, tfall, 'RISE')
        self.CheckFreCounterRegister(expected)
        
    def MiniFreqCounterRatio2Duty50FallEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        repeat = self.rpt
        waveForm = 'E'*repeat
        period = self.peri
        ratio = 2
        compare = 0.25
        tfall = ratio*0.5
        expected = int( repeat*1/2 )
        self.RunEdgeCounterScenarioEvenToOddWithoutEnClk(waveForm, period, ratio, compare, tfall, 'FALL')
        self.CheckFreCounterRegister(expected)
        	

    def MiniMaxMinSampleRatio128BothEdgeDriveSlice0Test(self):
        self.env.SetConfig('LowToHighLoopback')
        repeat = self.rpt
        waveForm = 'E'*repeat
        period = 1330e-12
        Slice2Period = 171e-9
        ratio = 2
        compare = 0.5
        tfall = ratio*0.5
        MTDPadding = 128
        RepeatCount = 1000
        expected = int(RepeatCount*MTDPadding)        # Value calculated based on number of patterns sent and expected edge count in pattern
        self.Log('info', 'Slice1Period: {} Slice2Period: {}'.format(period, Slice2Period))
        self.RunLowTOHighEnabledClockScenario(period, Slice2Period, ratio, compare, tfall, 'BOTH',MTDPadding,RepeatCount, 0)
        self.CheckLowTOHighFreCounterRegister(expected,1)
        
    def MiniMaxMinSampleRatio128BothEdgeDriveSlice1Test(self):
        self.env.SetConfig('LowToHighLoopback')
        repeat = self.rpt
        waveForm = 'E'*repeat
        period = 1330e-12
        Slice2Period = 171e-9
        ratio = 2
        compare = 0.5
        tfall = ratio*0.5
        MTDPadding = 128
        RepeatCount = 1000
        expected = int(RepeatCount*MTDPadding)        # Value calculated based on number of patterns sent and expected edge count in pattern
        self.Log('info', 'Slice1Period: {} Slice2Period: {}'.format(period, Slice2Period))
        self.RunLowTOHighEnabledClockScenario(period, Slice2Period, ratio, compare, tfall, 'BOTH',MTDPadding,RepeatCount, 1)
        self.CheckLowTOHighFreCounterRegister(expected,0)
    
    def RunLowTOHighEnabledClockScenario(self, testPeriod, Slice2Period, ratio, compare, tfall,edgeCounterMode,MTDPaddingRatio,RepeatCount, driveSlice):
        self.env.SetConfig('LowToHighLoopback')
        perChannelRegisters = ['ChannelFrequencyCounts']
        period = testPeriod  # Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        periodX = Slice2Period
        asm = [None, None]
        pattern = [None, None]
        # create pattern of driving one for slice 0
        asm0 = PatternAssembler()

        drivestartpadding = (RepeatCount *MTDPaddingRatio)+(MTDPaddingRatio -1)
        driverepeats = (RepeatCount * MTDPaddingRatio)
        driveendpadding = (RepeatCount * MTDPaddingRatio)
        asm0.LoadString("""\
        %var                           enclk=0b11111111111111111111111111111111111111111111111111111111
        S stype=IOSTATEJAM,             data=0jEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        %repeat {} 
        V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
        %end
        I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
        %repeat {}
        V link=0, ctv=0, mtv=0, lrpt=0, data=0vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        %end
        I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
        %repeat {}
        V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
        %end
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        """.format(drivestartpadding,driverepeats,driveendpadding))

        
        
        asm1 = PatternAssembler()
        asm1.LoadString("""\
        S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        %repeat 400
        V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
        %end
        I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
        %repeat 3000
        V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
        %end
        I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
        %repeat 500
        V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
        %end
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        """)

        # print(source)
        
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlot(slot)
            for slice in [0, 1]:
                if slice == driveSlice:  # Set slice 0 as Enabled clock drive mode.
                    self.Log('info', 'Slice:{} Period: {}'.format(slice,period))
                    pattern[slice] = asm0.Generate()
                    self.env.SetPeriodACandSim(slot, slice, period)
                    #self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': period * 0.5,'compare': 0.0}, True)
                    # set the even pin as EC.
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST','RESET': 'TRANSITION', 'ratio': ratio,'trise': period * 0.5, 'tfall': (period * tfall)+period*0.5}, True)
                else:  # set slice 1 as compare mode
                    self.Log('info', 'Slice:{} Period: {}'.format(slice,periodX))
                    pattern[slice] = asm1.Generate()
                    self.env.SetPeriodACandSim(slot, slice, periodX)
                    self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode},True)  # 'OFF','RISE', "FALL','BOTH'
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0,'compare': 0.0}, True)

                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True,False)  # capture all
                # Clear fail counts
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
        self.env.rc.send_sync_pulse()

        # Wait for all slices to complete
        for slice in [0, 1]:
            completed = hpcc.ac[slice].WaitForCompletePattern()
            if not completed:
                self.Log('error', 'Pattern execution did not complete')

        #self.Log('info', 'Capture for slot {} slice {}'.format(slot, 0))
        #self.env.DumpCapture(slot, 0)  # , 'SHcapture_{}_{}.txt'.format(slot, 0))
        #self.Log('info', 'Capture for slot {} slice {}'.format(slot, 1))
        #self.env.DumpCapture(slot, 1)  # , 'SHcapture_{}_{}.txt'.format(slot, 1))

    def CheckLowTOHighFreCounterRegister(self, expected,countingslice):
        perChannelRegisters = [            'ChannelFrequencyCounts'        ]

        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                #for ch in range(ac.channels):
                for ch in range(56):
                    for register in perChannelRegisters:
                        # Read it back
                        conf3 = ac.ReadChannel(ch, 'PerChannelControlConfig3')
                        actual = ac.ReadChannel(ch, register).Pack()
                        if (ac.slice == countingslice ):
                             self.Log('info','Expected Edge count: {} Actual Edge Count: {} for channel: {}'.format(expected,actual, ch))
                        if (ac.slice == countingslice and expected != actual):
                            self.Log('error', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expected, actual))
    
    def basicTogglingPatternSetup(self, period, edgeCounterMode, repeat, drive, compare):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeat
        pattern.LoadString("""\
                PATTERN_START:
                    S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
                    # S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
                    %repeat 1000
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %repeat 1000
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %end
                PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """)
        
        #pattern.LoadString(source)
        pdata = pattern.Generate()
        #pattern.SaveObj('EdgeCounter.obj')
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': drive, 'compare': compare}, True)
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)

            
    def basicSingleRisingEdgeShiftPatternSetup(self, period, repeat, drive, compare):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeat
        pattern.LoadString("""\
                PATTERN_START:
                    S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
                    %repeat 1024
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
                    %repeat 1024
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """)
        
        #pattern.LoadString(source)
        pdata = pattern.Generate()
        #pattern.SaveObj('EdgeCounter.obj')
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': 'RISE'}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': drive, 'compare': compare}, True)
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)


    def basicSingleFallingEdgeShiftPatternSetup(self, period, repeat, drive, compare):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeat
        pattern.LoadString("""\
                PATTERN_START:
                    S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
                    %repeat 1024
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
                    %repeat 1024
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    %end
                PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """)
        
        #pattern.LoadString(source)
        pdata = pattern.Generate()
        #pattern.SaveObj('EdgeCounter.obj')
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': 'FALL'}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': drive, 'compare': compare}, True)
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            
            
    def basicTogglingPatternDoubleEdgeCounterSetup(self, period, edgeCounterMode, repeat, drive, compare):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeat
        pattern.LoadString("""\
                PATTERN_START:
                    S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
                    %repeat 1024   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
                    %repeat REPEATS   
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    %end 
                PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """)
        
        #pattern.LoadString(source)
        pdata = pattern.Generate()
        #pattern.SaveObj('EdgeCounter.obj')
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': drive, 'compare': compare}, True)
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)

    def RunEnabledClockScenario(self, waveformchars,testPeriod,  ratio, compare, tfall, edgeCounterMode):
        self.env.SetConfig('EvenToOddLoopback')
        period =  testPeriod# Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 256
        source = """\
    %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
    """
        for ch in waveformchars:
            source = source + 'V link=0, ctv=0, mtv=0, lrpt=0, data=0vL{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}\n'.format(ch)
        source = source + """\
    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """
        #print(source)
        pattern.LoadString(source)
        #pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        #pdata = pattern
        pdata = pattern.Generate()
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            self.env.DumpCapture(slot, slice)
    
    def RunEdgeCounterScenarioEvenToOddWithoutEnClk(self, waveformchars,testPeriod,  ratio, compare, tfall, edgeCounterMode):
        self.env.SetConfig('EvenToOddLoopback')
        period =  testPeriod# Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        source = """\
    S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
    %repeat 1000
    V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
    %end
    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
    #%repeat 8 - Enable padding of pulses to debug the pattern
    #V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
    #%end
    %repeat 512
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
    %end
    I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
    V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1
	%repeat 1000
    V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
    %end
    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
    S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
        """
        #print(source)
        pattern.LoadString(source)
        #pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        #pdata = pattern
        pdata = pattern.Generate()
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)
        	
    # def RunEdgeCounterScenarioOddToEvenWithoutEnClk(self, waveformchars,testPeriod,  ratio, compare, tfall, edgeCounterMode):
        # self.env.SetConfig('EvenToOddLoopback')
        # period =  testPeriod# Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        # pattern = PatternAssembler()
        # source = """\
    # S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
    # %repeat 1000
    # V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
    # %end
    # I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
    #%repeat 8 - Enable padding of pulses to debug the pattern
    #V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
    #%end
    # %repeat 512
    # V link=0, ctv=0, mtv=0, lrpt=0, data=0v1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
    # V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
    # %end
    # I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
    # V link=0, ctv=0, mtv=0, lrpt=0, data=0v1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
	# %repeat 1000
    # V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
    # %end
    # I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
    # S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
        # """
        #print(source)
        # pattern.LoadString(source)
        #pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        #pdata = pattern
        # pdata = pattern.Generate()
        #patternSize = len(pdata)

        # for (slot, slice) in self.env.fpgas:
            # self.env.LogPerSlice(slot, slice)
            # self.env.SetPeriodACandSim(slot, slice, period)
            # self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            # self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            # self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            # self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)
        #self.env.TakeSnapshot('Create_Snap_Shot')

	
    def CheckFreCounterRegister(self, expected):    
        perChannelRegisters = [            'ChannelFrequencyCounts'        ]

        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                #for ch in range(ac.channels):
                for ch in range(1,56,2):
                    for register in perChannelRegisters:
                        # Read it back
                        conf3 = ac.ReadChannel(ch, 'PerChannelControlConfig3')
                        actual = ac.ReadChannel(ch, register).Pack()
                        #self.Log('info', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expected, actual))
                        # Compare it
                        if expected != actual:
                            self.Log('error', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expected, actual))
                            #self.Log('error', 'PerChannelControlConfig3.FrequencyCounterMode for channel [{}] is {}'.format(ch,conf3.FrequencyCounterMode) )
                        # Write it
                        #ac.WriteChannel(ch, register, 0x0)
                        #ac._SetFreqCounter(ch, 0)
                        #print( ac.ReadChannel(ch, register).Pack())

    def CheckFreCounterRegisterforeven(self, expected):    
        perChannelRegisters = [            'ChannelFrequencyCounts'        ]

        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                #for ch in range(ac.channels):
                for ch in range(0,55,2):
                    for register in perChannelRegisters:
                        # Read it back
                        conf3 = ac.ReadChannel(ch, 'PerChannelControlConfig3')
                        actual = ac.ReadChannel(ch, register).Pack()
                        #self.Log('info', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expected, actual))
                        # Compare it
                        if expected != actual:
                            self.Log('error', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expected, actual))
                            #self.Log('error', 'PerChannelControlConfig3.FrequencyCounterMode for channel [{}] is {}'.format(ch,conf3.FrequencyCounterMode) )
                        # Write it
                        #ac.WriteChannel(ch, register, 0x0)
                        #ac._SetFreqCounter(ch, 0)
                        #print( ac.ReadChannel(ch, register).Pack())
						

class RandomVectorPattern(HpccTest):
    peri = 5e-9
    rpt = 1024

    def SimplePrbsVectorGenerator(self, patternLength):
        rndVectorList = []
        t3 = 'L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0' 
        rndVectorList.append(t3)
        t3 = 'L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0' 
        rndVectorList.append(t3)
        for count in range(0, patternLength):
            drivePrbs = [ str(random.randint(0,1)) for i in range(28) ]
            capturePrbs = []
            for i in drivePrbs:
                if i=='0': capturePrbs.append('L')
                elif i=='1': capturePrbs.append('H')
            t1 = zip(capturePrbs, drivePrbs)
            t2 = [ ''.join(i) for i in t1]
            t3 = ''.join(t2)
            rndVectorList.append(t3)
        t3 = 'L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0' 
        rndVectorList.append(t3)
        t3 = 'L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0' 
        rndVectorList.append(t3)
        return rndVectorList

    def ChannelVectorRecovery(self, rndVectorList):
        
        channelVectorDict = {}
        for i in range(1, 56, 2):
            channelVectorDict['CaptureVectorChannel{}'.format(i) ] = ''
        
        reversedRndDriveVectorList = [ i[::-2] for i in rndVectorList ] ## Hard Coded. -2 needs to be changed if IOState JAM vector changes.

        for i in range(1, 56, 2):
            channelVectorDict['CaptureVectorChannel{}'.format(i) ] += '0' # Padding Vector 0 just before the EDGECNTR on
            for rndVector in reversedRndDriveVectorList:
                channelVectorDict['CaptureVectorChannel{}'.format(i) ] += rndVector[int(i/2)]
            #channelVectorDict['DriveVectorChannel{}'.format(i) ] += '0' # Padding Vector 0 just after the EDGECNTR off

        return channelVectorDict

    def RiseCounting(self, targetString):
        risingList = re.findall('01', targetString)
        risingCount = len( risingList)
        return risingCount

    def FallingCounting(self, targetString):
        fallingList = re.findall('10', targetString)
        fallingCount = len( fallingList)
        return fallingCount

    def CheckFreCounterRegisterOnCompareChannel(self, edge, expected, mode):    
        perChannelRegisters = [            'ChannelFrequencyCounts'        ]
        expectedEdge = None
        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                for channel in range(0, 56, 2): #range(ac.channels):
                    for register in perChannelRegisters:
                        if mode == 'Vector':
                            ch = channel + 1 # For Vector Pattern, Capture Channels are on Odd Channels
                        elif mode == 'EnClk':
                            ch = channel
                        # Read it back
                        conf3 = ac.ReadChannel(ch, 'PerChannelControlConfig3')
                        actual = ac.ReadChannel(ch, register).Pack()
                        if edge == 'BOTH':
                            expectedEdge = expected['expectedRisingEdgesChannel{}'.format(ch)] + expected['expectedFallingEdgesChannel{}'.format(ch)]
                        elif edge == 'RISE':
                            expectedEdge = expected['expectedRisingEdgesChannel{}'.format(ch)] 
                        elif edge == 'FALL':
                            expectedEdge = expected['expectedFallingEdgesChannel{}'.format(ch)]
                        self.Log('info', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expectedEdge, actual))
                        # Compare it
                        if expectedEdge != actual:
                            self.Log('error', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expectedEdge, actual))

    def MiniFreqCounterRandomVectorBothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedEdges = {}
        edge = 'BOTH'
        period = self.peri
        repeat = self.rpt
        vectorLength = 10240
        randomPattern = self.SimplePrbsVectorGenerator(vectorLength)
        expected = repeat*2  + 1
        self.RandomVectorPatternSetup(randomPattern, period, edge, repeat)

        channelVectors = self.ChannelVectorRecovery(randomPattern)
        for i in range(1, 56, 2):
            expectedEdges['expectedRisingEdgesChannel{}'.format(i)] = self.RiseCounting( channelVectors['CaptureVectorChannel{}'.format(i) ] )
            expectedEdges['expectedFallingEdgesChannel{}'.format(i)] = self.FallingCounting( channelVectors['CaptureVectorChannel{}'.format(i) ] )

        self.CheckFreCounterRegisterOnCompareChannel(edge, expectedEdges, 'Vector')

    def MiniFreqCounterRandomVectorRisingEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedEdges = {}
        edge = 'RISE'
        period = self.peri
        repeat = self.rpt
        vectorLength = 10240
        randomPattern = self.SimplePrbsVectorGenerator(vectorLength)
        self.RandomVectorPatternSetup(randomPattern, period, edge, repeat)

        channelVectors = self.ChannelVectorRecovery(randomPattern)
        for i in range(1, 56, 2):
            expectedEdges['expectedRisingEdgesChannel{}'.format(i)] = self.RiseCounting( channelVectors['CaptureVectorChannel{}'.format(i) ] )
            expectedEdges['expectedFallingEdgesChannel{}'.format(i)] = self.FallingCounting( channelVectors['CaptureVectorChannel{}'.format(i) ] )

        self.CheckFreCounterRegisterOnCompareChannel(edge, expectedEdges, 'Vector')

    def MiniFreqCounterRandomVectorFallingEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedEdges = {}
        edge = 'FALL'
        period = self.peri
        repeat = self.rpt
        vectorLength = 10240
        randomPattern = self.SimplePrbsVectorGenerator(vectorLength)
        expected = repeat*2  + 1
        self.RandomVectorPatternSetup(randomPattern, period, edge, repeat)

        channelVectors = self.ChannelVectorRecovery(randomPattern)
        for i in range(1, 56, 2):
            expectedEdges['expectedRisingEdgesChannel{}'.format(i)] = self.RiseCounting( channelVectors['CaptureVectorChannel{}'.format(i) ] )
            expectedEdges['expectedFallingEdgesChannel{}'.format(i)] = self.FallingCounting( channelVectors['CaptureVectorChannel{}'.format(i) ] )

        self.CheckFreCounterRegisterOnCompareChannel(edge, expectedEdges, 'Vector')

    def RandomVectorPatternSetup(self, rndVectorList, period, edgeCounterMode, repeat):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeat
        source = """\
S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
S stype=IOSTATEJAM,             data=0jX1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1X1
V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
"""
        for rndVector in rndVectorList:
            source = source + 'V link=0, ctv=0, mtv=0, lrpt=0, data=0v{}\n'.format(rndVector)
        source = source + """\
V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """
        #print(source)
        pattern.LoadString(source)
        pdata = pattern.Generate()
        #pattern.SaveObj('EdgeCounter.obj')
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True)
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            # self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)

class RandomEnClkPattern(HpccTest):
    peri = 5e-9
    rpt = 1024
    
    def RiseCounting(self, targetString):
        risingList = re.findall('01', targetString)
        risingCount = len( risingList)
        return risingCount

    def FallingCounting(self, targetString):
        fallingList = re.findall('10', targetString)
        fallingCount = len( fallingList)
        return fallingCount

    def CheckFreCounterRegisterOnCompareChannel(self, edge, expected, mode):    
        perChannelRegisters = [            'ChannelFrequencyCounts'        ]
        expectedEdge = None
        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                for channel in range(0, 56, 2): #range(ac.channels):
                    for register in perChannelRegisters:
                        if mode == 'Vector':
                            ch = channel + 1 # For Vector Pattern, Capture Channels are on Odd Channels
                        elif mode == 'EnClk':
                            ch = channel
                        # Read it back
                        conf3 = ac.ReadChannel(ch, 'PerChannelControlConfig3')
                        actual = ac.ReadChannel(ch, register).Pack()
                        if edge == 'BOTH':
                            expectedEdge = expected['expectedRisingEdgesChannel{}'.format(ch)] + expected['expectedFallingEdgesChannel{}'.format(ch)]
                        elif edge == 'RISE':
                            expectedEdge = expected['expectedRisingEdgesChannel{}'.format(ch)] 
                        elif edge == 'FALL':
                            expectedEdge = expected['expectedFallingEdgesChannel{}'.format(ch)]
                        #self.Log('info', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expectedEdge, actual))
                        # Compare it
                        if expectedEdge != actual:
                            self.Log('error', 'per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(register, ch, hpcc.slot, ac.slice, expectedEdge, actual))

    def EnClkTranslator(self, ratio, duty, enClkPattern, inverted):
        cycle = 0
        dutyDivider = duty
        translatedEnClk = []
        for char in  enClkPattern :
            internalCycle = int( cycle % ratio)
            if char == 'E':
                if internalCycle < dutyDivider and inverted:
                    translatedEnClk.append('0')
                elif internalCycle >= dutyDivider and inverted:
                    translatedEnClk.append('1')
                elif internalCycle < dutyDivider and (not inverted):
                    translatedEnClk.append('1')
                elif internalCycle >= dutyDivider and (not inverted):
                    translatedEnClk.append('0')
            elif char == 'R':
                cycle = 0
                if inverted:
                    translatedEnClk.append('0')
                else:
                    translatedEnClk.append('1')
            elif char == '0':
                translatedEnClk.append('0')
            elif char == '1':
                translatedEnClk.append('1')
            cycle += 1
        translatedEnClkString = ''.join( translatedEnClk)
        return translatedEnClkString

    def SimplePrbsEnClkGenerator(self, patternLength):
        numberOfChannels = 28
        availableRatio = [2, 4, 6, 8]
        random.shuffle(availableRatio)
        availablePattern = ['E']*85 + ['R']*9 + ['0']*3 + ['1']*3 
        random.shuffle(availablePattern)
        rndEnClkList = []
        channelRatio = [ random.choice(availableRatio) for i in range(numberOfChannels) ]
        possilbeNumeratorForDutyCycle = [ random.randint(1, ratio-1) for ratio in channelRatio ] 
        #dutyCycle = [ possilbeNumeratorForDutyCycle/ratio for ratio in channelRatio ] 
        invertedList = [ random.randint(0, 1) for i in range(numberOfChannels) ]
        #print(channelRatio)
        cycleListList = []
        for selectedRatio in channelRatio:
            cycle = 0
            #cycleList = []
            drivePrbsEnClk = []
            for count in range(0, patternLength):
                internalCycle = int( cycle % selectedRatio )
                #cycleList.append(internalCycle)
                enClkPattern = random.choice(availablePattern)
                if enClkPattern == 'E':
                    drivePrbsEnClk.append('E')
                elif enClkPattern == 'R':
                    cycle = 0
                    drivePrbsEnClk.append('R')
                elif (enClkPattern == '0')  and (internalCycle ==0) :
                    cycle += (selectedRatio-1)
                    drivePrbsEnClk.extend('0'*selectedRatio)
                elif (enClkPattern == '1')  and (internalCycle ==0) :
                    cycle += (selectedRatio-1)
                    drivePrbsEnClk.extend('1'*selectedRatio)
                else: drivePrbsEnClk.append('E')
                cycle += 1
                if len(drivePrbsEnClk) > patternLength:
                    #print('break')
                    break
            if len(drivePrbsEnClk) > patternLength:
                excess = len(drivePrbsEnClk) - patternLength
                del drivePrbsEnClk[-excess:]
            rndEnClkList.append( drivePrbsEnClk )
            #cycleListList.append(cycleList)
        return rndEnClkList, channelRatio,possilbeNumeratorForDutyCycle, invertedList #, cycleListList

    def EnClkPatternConverter(self, patternLength):
        convertedEnClkPattern = []
        rndEnClkList, channelRatio, dutyNumeratorList, invertedList = self.SimplePrbsEnClkGenerator(patternLength)
        #print(rndEnClkList)
        capturePatList = ['L']*28 # 28 Channels
        #print(capturePatList)
        for seq in range(patternLength):
            convertedEnClkPattern.append( [ vertPat[seq] for vertPat in rndEnClkList ] )
        channelEnClkOnlyPat = [ ''.join( vertPat ) for vertPat in rndEnClkList ]
        [self.InsertingCapturePattern(enClkList, capturePatList) for enClkList in convertedEnClkPattern]
        horiPat = [ ''.join( verPat ) for verPat in convertedEnClkPattern ]
        return channelEnClkOnlyPat, horiPat, channelRatio, dutyNumeratorList, invertedList 
    
    def InsertingCapturePattern(self, patList, captureList):
        for i, v in enumerate(captureList):
            patList.insert(2*i+1, v)

    def SetRandomEnClkPEAttributes(self, slot, slice, period, ratioList, dutyNumeratorList, invertedList):
        channelList = list( range(55, 0, -2) )
        for channel in channelList: # Hard coding the channel 55 to 1 for setting EnClk PE Attributes
            listIndex = channelList.index(channel)
            tFall = None
            tRise = None
            if invertedList[listIndex]:
                tFall = 0
                tRise = dutyNumeratorList[listIndex]*period
            else:
                tRise = 0
                tFall = dutyNumeratorList[listIndex]*period
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratioList[listIndex], 'trise': tRise, 'tfall': tFall}, True, [channel])

    
    def RandomEnClkPatternSetup(self,  period, rndEnClkList, ratioList, dutyNumeratorList, invertedList, edgeCounterMode):


        pattern = PatternAssembler()
        source = """\
%var                           enclk=0b10101010101010101010101010101010101010101010101010101010
S stype=IOSTATEJAM,             data=0jEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEX
I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
"""
        for rndEnClk in rndEnClkList:
            source = source + 'V link=0, ctv=0, mtv=0, lrpt=0, data=0v{}\n'.format(rndEnClk)
        source = source + """\
I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
V link=0, ctv=0, mtv=0, lrpt=0, data=0vELELELELELELELELELELELELELELELELELELELELELELELELELELELEL
V link=0, ctv=0, mtv=0, lrpt=0, data=0vELELELELELELELELELELELELELELELELELELELELELELELELELELELEL
V link=0, ctv=0, mtv=0, lrpt=0, data=0vELELELELELELELELELELELELELELELELELELELELELELELELELELELEL
V link=0, ctv=0, mtv=0, lrpt=0, data=0vELELELELELELELELELELELELELELELELELELELELELELELELELELELEL
I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """
        #print(source)
        pattern.LoadString(source)
        pdata = pattern.Generate()
        #pattern.SaveObj('EdgeCounter.obj')
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': edgeCounterMode}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True)
            self.SetRandomEnClkPEAttributes(slot, slice, period, ratioList, dutyNumeratorList, invertedList)

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            # self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)

    @unittest.skip('ENCLK SIM NOT UPDATED FOR NEW ENCLK FEATURE ')	
    def MiniFreqCounterRandomEnClkBothEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedEdges = {}
        edge = 'BOTH'
        period = self.peri
        repeat = self.rpt
        vectorLength = 10240
        channelEnClkOnlyPat, randomPattern, channelRatioList, dutyNumeratorList, invertedList = self.EnClkPatternConverter(vectorLength)
        self.RandomEnClkPatternSetup(period, randomPattern, channelRatioList, dutyNumeratorList, invertedList, edge)

        channelList = list( range(54, -1, -2) )
        paddedChannelEnClkOnlyPat = []
        for channel in channelList: 
            listIndex = channelList.index(channel)
            paddedChannelEnClkOnlyPat.append( 'E' + channelEnClkOnlyPat[listIndex] )  # Padding IO State Jam Vector
            translatedEnClk = self.EnClkTranslator( channelRatioList[listIndex], dutyNumeratorList[listIndex], paddedChannelEnClkOnlyPat[listIndex], invertedList[listIndex])
            #print(channelRatioList[listIndex] )
            #print(dutyNumeratorList[listIndex] )
            #print( paddedChannelEnClkOnlyPat[listIndex] )
            #print(translatedEnClk)
            expectedEdges['expectedRisingEdgesChannel{}'.format(channel)] = self.RiseCounting( translatedEnClk )
            expectedEdges['expectedFallingEdgesChannel{}'.format(channel)] = self.FallingCounting( translatedEnClk )

        self.CheckFreCounterRegisterOnCompareChannel(edge, expectedEdges, 'EnClk')

    @unittest.skip('ENCLK SIM NOT UPDATED FOR NEW ENCLK FEATURE ')	        
    def MiniFreqCounterRandomEnClkFallingEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedEdges = {}
        edge = 'FALL'
        period = self.peri
        repeat = self.rpt
        vectorLength =  10240
        channelEnClkOnlyPat, randomPattern, channelRatioList, dutyNumeratorList, invertedList = self.EnClkPatternConverter(vectorLength)
        self.RandomEnClkPatternSetup(period, randomPattern, channelRatioList, dutyNumeratorList, invertedList, edge)

        channelList = list( range(54, -1, -2) )
        paddedChannelEnClkOnlyPat = []
        for channel in channelList: 
            listIndex = channelList.index(channel)
            paddedChannelEnClkOnlyPat.append( 'E' + channelEnClkOnlyPat[listIndex] )  # Padding IO State Jam Vector
            translatedEnClk = self.EnClkTranslator( channelRatioList[listIndex], dutyNumeratorList[listIndex], paddedChannelEnClkOnlyPat[listIndex], invertedList[listIndex])
            #if channel == 42:
            #    print(channelRatioList[listIndex] )
            #    print(dutyNumeratorList[listIndex] )
            #    print( paddedChannelEnClkOnlyPat[listIndex] )
            #    print(translatedEnClk)
            expectedEdges['expectedRisingEdgesChannel{}'.format(channel)] = self.RiseCounting( translatedEnClk )
            expectedEdges['expectedFallingEdgesChannel{}'.format(channel)] = self.FallingCounting( translatedEnClk )

        self.CheckFreCounterRegisterOnCompareChannel(edge, expectedEdges, 'EnClk')
         
    @unittest.skip('ENCLK SIM NOT UPDATED FOR NEW ENCLK FEATURE ')	
    def MiniFreqCounterRandomEnClkRisingEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        expectedEdges = {}
        edge = 'RISE'
        period = self.peri
        repeat = self.rpt
        vectorLength = 10240
        channelEnClkOnlyPat, randomPattern, channelRatioList, dutyNumeratorList, invertedList = self.EnClkPatternConverter(vectorLength)
        self.RandomEnClkPatternSetup(period, randomPattern, channelRatioList, dutyNumeratorList, invertedList, edge)

        channelList = list( range(54, -1, -2) )
        paddedChannelEnClkOnlyPat = []
        for channel in channelList: 
            listIndex = channelList.index(channel)
            paddedChannelEnClkOnlyPat.append( 'E' + channelEnClkOnlyPat[listIndex] )  # Padding IO State Jam Vector
            translatedEnClk = self.EnClkTranslator( channelRatioList[listIndex], dutyNumeratorList[listIndex], paddedChannelEnClkOnlyPat[listIndex], invertedList[listIndex])
            expectedEdges['expectedRisingEdgesChannel{}'.format(channel)] = self.RiseCounting( translatedEnClk )
            expectedEdges['expectedFallingEdgesChannel{}'.format(channel)] = self.FallingCounting( translatedEnClk )

        self.CheckFreCounterRegisterOnCompareChannel(edge, expectedEdges, 'EnClk')
        
        
class Compression(HpccTest):
    def RandomEdgeCounterCompressionTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        # has edge counter in every pattern, randomize later
        constraints = {'NUM_PAT': [5, 5], 'LEN_PAT': [1000, 2000], 'RPT': 0.002, 'LRPT': 0.1, 'CLINK': 0.1, 'EDGE_COUNTER': True}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('linklrpt.obj')
        
		
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode'] # 0,1,2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': param['edgeCounter']}, True)
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            #self.env.WriteExecutePatternAndSetupCapture(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice, captureDisableRules = ['ccwr-mismatch'])  
            
