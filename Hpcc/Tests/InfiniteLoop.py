################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: InfiniteLoop.py
#-------------------------------------------------------------------------------
#     Purpose: Tests that loop a pattern forever
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 07/20/15
#       Group: HDMT FPGA Validation
################################################################################

import time

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest


class Abort(HpccTest):
    def DirectedAbortIRPTTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=16000
            LOOP:
                Repeat 2000
                RandomVectors length=1
            Goto `LOOP`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        period = 20e-9
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            hpcc.ac[slice].AbortPattern()
            self.env.CheckAlarms(slot, slice, alarmsToMask = ['MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
    
    def DirectedAbortFlatTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            LOOP:
                RandomVectors length=256
            Goto `LOOP`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        period = 20e-9
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            self.env.AbortPattern(slot, slice)
            self.env.SetSimAbort(slot, slice, 300)
            self.env.RunCheckers(slot, slice, alarmsToMask = ['MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            
    def DirectedAbortInsidePatternTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN1`            
            StopPattern 0x12345678
            
            PATTERN1:
                RandomVectors length=256
                LOOP:
                    RandomVectors length=256
                Goto `LOOP`
                I optype=BRANCH, br=RET            
        """)
        pattern = asm.Generate()
        period = 20e-9
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            time.sleep(1)
            self.env.AbortPattern(slot, slice)
            
            hpcc = self.env.instruments[slot]
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'Unexpect end status = 0x{:x}, expected = 0x1800dead.'.format(actualEndStatus))
                
    def DirectedAbortNestedPCallTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN1`            
            StopPattern 0x12345678
            
            PATTERN1:
                RandomVectors length=256
                LOOP:
                    I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
                    I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN2`  
                Goto `LOOP`
                I optype=BRANCH, br=RET   
            PATTERN2:
                RandomVectors length=256
                I optype=BRANCH, br=RET  
        """)
        pattern = asm.Generate()
        period = 20e-9
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = False, waitForComplete = False)
            # Abort it
            time.sleep(1)
            self.env.AbortPattern(slot, slice)
            
            hpcc = self.env.instruments[slot]
            actualEndStatus = hpcc.ac[slice].Read('PatternEndStatus').Pack()
            if actualEndStatus != 0x1800dead:
                self.Log('error', 'Unexpect end status = 0x{:x}, expected = 0x1800dead.'.format(actualEndStatus))

    def DirectedFlushThenAbortTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            %repeat 313
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            LOOP:
                RandomVectors length=256
            Goto `LOOP`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        period = 20e-9
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern, period = period, captureAll = False, captureFails = False, captureCTVs = True, waitForComplete = False)
            
            data = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            self.Log('debug', 'Before: TotalCaptureCount = {}'.format(data))
            data = hpcc.ac[slice].Read('CaptureControl')
            self.Log('debug', 'Before: CaptureControl.CaptureBufferStatus = {}'.format(data.CaptureBufferStatus))
            
            data = hpcc.ac[slice].Read('CaptureControl')
            data.FlushCapture = 1
            hpcc.ac[slice].Write('CaptureControl', data)
            # Wait for flush to complete
            completed = False
            for i in range(500):
                time.sleep(0.01)
                data = hpcc.ac[slice].Read('CaptureControl')
                completed = data.FlushCapture == 0
                if completed:
                    break
            if completed:
                self.Log('debug', 'Capture flush completed')
            else:
                self.Log('error', 'Capture flush did not complete')
            data = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
            self.Log('debug', 'After: TotalCaptureCount = {}'.format(data))
            data = hpcc.ac[slice].Read('CaptureControl')
            self.Log('debug', 'After: CaptureControl.CaptureBufferStatus = {}'.format(data.CaptureBufferStatus))
            # Abort it
            self.env.AbortPattern(slot, slice)
            self.env.SetSimAbort(slot, slice, 350)
            self.env.RunCheckers(slot, slice,alarmsToMask = ['MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])

