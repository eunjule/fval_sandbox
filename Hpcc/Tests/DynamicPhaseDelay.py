################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: DynamicPhaseDelay.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for dynamic phase delay
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 09/09/15
#       Group: HDMT FPGA Validation
################################################################################
import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest


class DynamicPhaseDelay(HpccTest):
        
    def _GetWalkingOnePattern(self):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 10

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 17,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 18,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 19,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0  # 20,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0  # 21,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0  # 22,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # 23,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # 24,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # 25,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # 26,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # 27,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 28,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 29,
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        return pattern.Generate()
        
    def _GetEnabledClockPattern(self):
        pattern = PatternAssembler()
        pattern.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat 1001
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """)
        return pattern.Generate()
    
    #Verify that Dynamic Phase Delay on the Drive Channel of Dpin
	#Test Period = 10ns, Tdrive = 21ns
    #Strobe Compare at Tcomp = 22ns, - expected 1 - observe from dump capture both drive and compare strobe
	#occurs in the same test cycle. Note: due to current calibration data, it might not happen 
    #Set Dynamic Phase Delay of -2ns (move forward) keep the same Tcomp
	#Strobe Compare at Tcomp = 15ns, - expected 1.- observe dump capture, the drive channels in one slice 
	# is now occurs in the test cycles in advanced
    def MiniDriveDelayTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 10e-9
        pdata = self._GetWalkingOnePattern()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
			
			
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False,'drive': 21e-9, 'compare': 22e-9}, True)
            hpccModel.acSim[slice].SetDriveDelay(list(range(0,56,2)), 16e-9)
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'compare': 22e-9}, True, 'ODD_PINS')
            hpcc.ac[slice].SetDynamicPhaseDelay({'drive': -2e-9})
            hpccModel.acSim[slice].SetDriveDelay(list(range(0,56,2)), 19e-9)
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
            
    #verify the Dynamic Phase delay on the Compare channel of DPIN
	#Test period = 10 ns, Tdrive = 18ns, and Tcmp = 19ns
    #observe from dump capture: the drive and compare strobe occurs in the same test cycle
	#set dynamic delay on the compare channel to 5 ns delay.
	#observe from dump capture: the compare strobe now occurs in the next test cycle.  Note: the cal data for
	#particular tester might cause this not happen
    def MiniCompareDelayTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 10e-9
        pdata = self._GetWalkingOnePattern()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
            
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 18e-9, 'compare': 19e-9}, True)
           
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
		
            hpcc.ac[slice].SetDynamicPhaseDelay({'compare': 5e-9}) #setup Dynamic Phase Delay on the compare channel
            hpccModel.acSim[slice].SetCompareDelay(list(range(56)), 24e-9)
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
    
    #Verify: Placement of the EC rising edge with Dynamic Phase Delay.
	#Tester Period = 50ns, EC Ratio = 2, Tfall = 0, Trise = 8ns, Even To Odd Loopback
	#Dynamic Delay on the Rise Edge by 5ns
    def MiniTRiseDelayTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        pdata = self._GetEnabledClockPattern()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
			
			#strobe compare right before the rising edge 
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 7e-9}, True)
			#config both FPGA and simulator for CE with the rise edge at 8ns anf fall edge at 0 ns, ratio of 2
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 2, 'trise': 8e-9, 'tfall': 0.0}, True, 'EVEN_PINS')
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #strobe compare right after rising edge.
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 10e-9}, True, 'ODD_PINS')
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #config the Dynamic phase delay
			#strobe compare right after rising edge - expected capture 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 14e-9}, True, 'ODD_PINS')  
            hpcc.ac[slice].SetDynamicPhaseDelay({'trise': 5e-9}, 'EVEN_PINS') # config the FPGA for 5 ns Dynamic Delay
            hpccModel.acSim[slice].SetEnClkTrise(list(range(0,56,2)), 13e-9)  # set up simulator for rising edge at 13ns 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #strobe compare right before rising edge - expect capture 0
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 12e-9}, True, 'ODD_PINS')  
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #strobe compare right before falling edge- expect capture 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 49e-9}, True, 'ODD_PINS')  
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #strobe compare right after falling edge - expected capture 0
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 1e-9}, True, 'ODD_PINS')  
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
    #Verify: The placement of fall edge of EC with Dynamic Phase Delay
	#Tester Period= 50ns, EC Ratio = 1, and Tfall = 8 ns, and Trise = 0 ns, Event to Odd Loopback
	#Dynamic Phase Delay on Fall Edge by 5ns
    def MiniTFallDelayTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        pdata = self._GetEnabledClockPattern()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 10e-9}, True) #strobe compare right after Tfall, expected - 0
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': 8e-9}, True, 'EVEN_PINS')
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
            
            #strobe compare right before the fall edge  - expected = 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 10e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #config dynamic delay phase for 5 ns 
            hpcc.ac[slice].SetDynamicPhaseDelay({'tfall': 5e-9}, 'EVEN_PINS')
            hpccModel.acSim[slice].SetEnClkTfall(list(range(0,56,2)), 13e-9) #config simulator for Tfall at 13ns (8ns + 5ns)
            #strobe compare right before falling edge - expected = 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 12e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
			#strobe compare right before falling edge - expected = 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 14e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #strobe compare right before rising edge - expected = 0
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 49e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #strobe compare right after rising edge - expected = 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 1e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)

    # moving both enabled clock edges together, per Shelby's suggestion
    def MiniECDelayTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 10e-9
        pdata = self._GetEnabledClockPattern()
        
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            hpccModel = self.env.models[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 2e-9}, True)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': 5e-9}, True, 'EVEN_PINS')
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
			
            #config dynamic delay on both edge 
            hpcc.ac[slice].SetDynamicPhaseDelay({'trise': 5e-9}, 'EVEN_PINS')
            hpcc.ac[slice].SetDynamicPhaseDelay({'tfall': 5e-9}, 'EVEN_PINS')
            hpccModel.acSim[slice].SetEnClkTrise(list(range(0,56,2)), 5e-9)
            hpccModel.acSim[slice].SetEnClkTfall(list(range(0,56,2)), 10e-9)
			
            #strobe compare right before the trise at t= 4ns  - expected 0
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 4e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)

            #strobe compare right after the trise at t= 6ns  - expected 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 6e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
            
            #strobe compare right before the tfall at t= 9ns  - expected 1
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 9e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)
            
            #strobe compare right after the tfall at t= 11ns  - expected 0
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 11e-9}, True, 'ODD_PINS') 
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)

