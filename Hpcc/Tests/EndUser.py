################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: EndUser.py
#-------------------------------------------------------------------------------
#     Purpose: All the "end user" and "end user"-like tests go here
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 05/20/15
#       Group: HDMT FPGA Validation
################################################################################

import gzip
import os

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
from Tools import projectpaths


class Misses(HpccTest):
    def ThrTims32KVFlatPatternTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, r'Hpcc\Patterns\flat_long_32k.0.obj.gz'), 'rb').read()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = 5e-9)
            self.env.RunCheckers(slot, slice)
    def ThrRelease802GrossFailsTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, r'Hpcc\Patterns\8_0_2_gross_fails.obj.gz'), 'rb').read()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = 50e-9, start = 528)
            self.env.RunCheckers(slot, slice)
    def ThrCTVEvery512BlocksTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM, data=`rand(112)`
            RandomVectors length=511
            %repeat 100
                V link=0, ctv=1, mtv=0, lrpt=0, data=`rand(112)`
                %repeat 511
                    V link=0, ctv=0, mtv=0, lrpt=0, data=`rand(112)`
                %end
            %end
            StopPattern `rand(32)`
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)
    def ThrCQ13077PVCUnderrunTest(self): 
        self.env.SetConfig('InternalLoopback')
        snapshot = os.path.join(projectpaths.ROOT_PATH, r'Hpcc\Patterns\pvc_flat_pattern_12_16_2015_11_27_11_Part0.tar.gz')
        period = 3e-9    
        for sourceSlot in [7,8,9]:          
            self.ReplaySnapshotNoChecker(snapshot, sourceSlot, period) 
        
    def ThrSKLPVCUnderrunTest(self): 
        self.env.SetConfig('InternalLoopback')
        snapshot = os.path.join(projectpaths.ROOT_PATH, r'Hpcc\Patterns\PVC_Underrun_01_09_2016_11_46_01_Part0.tar.gz')
        period = 2.5e-9 
        for sourceSlot in [2,3,7,8]:             
            self.ReplaySnapshotNoChecker(snapshot, sourceSlot, period) 
    
    # assume loopback already set
    def ReplaySnapshotNoChecker(self, snapshot, sourceSlot, period, expectedAlarm = []):   
        snapshotDir = self.env.UnzipSnapshot(snapshot)              
        
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]

            previousRegSetting = 'temp.csv'
            hpcc.ac[slice].TakeRegistersSnapshot(previousRegSetting)
            path = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\PatternMemory'.format(sourceSlot, slice))
            filename = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\registers.csv'.format(sourceSlot, slice)) 
        
            self.env.LogPerSlice(slot, slice)
            
            self.env.LoadPatternMemoryFromSnapshot(slot, slice, path)
            self.env.LoadRegistersFromSnapshot(slot, slice, filename) 
            
            hpcc.ac[slice].captureBaseAddress = hpcc.ac[slice].Read('CaptureBaseAddress').Pack() 
            hpcc.ac[slice].SetPeriod(period)
            #hpcc.ac[slice].Write('CaptureControl', 0)
            #hpcc.ac[slice].Write('CaptureLength', 0xffffffff)
            #hpcc.ac[slice].Write('MaxCaptureCount', 0xffffffff)
            #hpcc.ac[slice].Write('MaxFailCount', 0xffffffff)
            #hpcc.ac[slice].Write('MaxFailCaptureCount', 0xffffffff) 
            #print(hpcc.ac[slice].Read('CaptureControl'))
            
            start = hpcc.ac[slice].Read('PatternStartAddress').Pack()             
            completed = self.env.ExecutePatternViaSyncPulse(slot, slice, start) 
            
            if not completed:
                self.Log('error', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete = True)
                completed = hpcc.ac[slice].IsPatternComplete()
                self.Log('warning', 'abort pattern, pattern complete = {}'.format(completed))
                
            defaultAlarmToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd']
            defaultAlarmToMask.extend(expectedAlarm)
            actualAlarms = self.env.CheckAlarms(slot, slice, alarmsToMask = defaultAlarmToMask)
            for alarm in expectedAlarm:
                if alarm not in actualAlarms:
                    self.Log('error', 'expected alarm {} did not happen'.format(alarm))
                else:
                    self.Log('info', 'caught expected alarm {}'.format(alarm))
            self.Log('info', 'Pattern completed = {}'.format(completed))
            self.Log('info', 'Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
            self.Log('info', 'New Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl1').Pack()))
            self.Log('info', 'Pattern End Status = 0x{:x}'.format(hpcc.ac[slice].Read('PatternEndStatus').Pack()))
            self.Log('info', 'Capture Address Underrun = 0x{:x}'.format(hpcc.ac[slice].Read('CaptureAddressUnderrun').Pack()))
            
            # restore previous setting
            self.env.LoadRegistersFromSnapshot(slot, slice, previousRegSetting)
            #self.env.TakeRegistersSnapshot('registers8200_{}.csv'.format(slice))


class RandomPlist(HpccTest):

    def RandomPlistManyPatternTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        
        constraints = {'NUM_PAT': [300, 500], 'LEN_PAT': [10000, 20000]}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('test.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 20000)        
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], stopOnMaxFail = param['stopOnMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], maxCaptureCount = param['maxCaptureCount'], captureAddress = param['captureAddress'])
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail','MetaDataFilterMissedEnd','ChannelLinkerMissedEnd','LocalRepeaterMissedEnd','PinDomainMissedEnd','DelayStackMissedEnd','CompareEngineMissedEnd','DeepCaptureEngineMissedEnd'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
        
    def RandomPlistLongPatternTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()        
        
        constraints = {'NUM_PAT': [30, 50], 'LEN_PAT': [100000, 200000]}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('test3.obj')
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 200000)        
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureType = param['captureType'], captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], stopOnMaxFail = param['stopOnMaxFail'], maxFailCountPerPatern = param['maxFailCountPerPatern'], maxFailCaptureCount = param['maxFailCaptureCount'], maxCaptureCount = param['maxCaptureCount'], captureAddress = param['captureAddress'])    
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm')
                
            expectStopOnFailAlarm = self.env.models[slot].acSim[slice].stopOnFailAlarm
            if expectStopOnFailAlarm and (not stopOnFail):
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            elif (not expectStopOnFailAlarm) and stopOnFail:
                self.Log('error', 'Unexpect StopOnFirstFailAlarm.')
            
            
class Special(HpccTest):
        
    # Jared was asking about this pattern structure, which uses pop instruction to alter call/ret stack so that patterns may return to places other than its origin    
    def DirectedAlterReturnStackTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN1:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 500, simple=1
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN2] 
            RandomEvenToOddVectors 500, simple=1
            I optype=BRANCH, br=RET                                                                         
            PATTERN2:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 500, simple=1
            PopRegister 0 
            I optype=BRANCH, br=RET 
            
            PATTERN_START:                                                                                 
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN1] 
            PATTERN_END:                                                                                   
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                    
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 50e-9, start = pattern.Resolve('eval[PATTERN_START]'))    
            self.env.RunCheckers(slot, slice)                
