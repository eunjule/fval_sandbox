################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: Diags.py
# -------------------------------------------------------------------------------
#     Purpose: Smoke tests and other gross reality checks go here
# -------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 04/01/15
#       Group: HDMT FPGA Validation
################################################################################

import gzip
import operator
import os
import random
import tarfile
import tempfile
import time
import unittest

from Common import configs
from Common import hilmon as hil
from Common.instruments.hdmt_trigger_interface import validate_aurora_statuses
from Common.instruments.tester import get_tester as cards_in_tester
from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.loopbackcal import Calibration
import Hpcc.instrument.hpccAcRegs as ac_registers
from Hpcc.Tests.hpcctest import HpccTest

PICO_SECONDS_PER_SECOND = 1000000000000

# configuration for the I2C communication with the main tester power supplies
PORTA_IO_DIR = 0xF5  # 1 = output, 0 = input

PORTA_I2C_MUX_SEL_MASK = 0xC0  # These two bits control the RC MUX
PORTA_I2C_MUX_SEL_FP = 0x00
PORTA_I2C_MUX_SEL_BP = 0x80
PORTA_I2C_MUX_SEL_TIU = 0x40
PORTA_I2C_MUX_SEL_RC = 0xC0

# Indices of supplies
PRIMARY = 0
SECONDARY = 1
# I2C addresses of devices
PCA9548A_U7_ADDR = 0xE2
BULK_ADDR = 0x80, 0x82
# Byte to write to PCA9548A to select the correct path
I2C_BULK = b'\x20', b'\x80'


class CalDebug(HpccTest):
    _myVox = 1.5
    _myTermVref = 1.5
    _myVil = 0.0
    _myVih = 3.0

    @staticmethod
    def _ClearShmoo(estimate, loopCount):
        for loop in range(loopCount):
            for slice in [0, 1]:
                for ch in range(56):
                    estimate[(loop, slice, ch)] = []
        return estimate


    def _clearcaloffset(hpcc):
        for slice in [0, 1]:
            for ch in range(56):
                hpcc.ac[slice].caldata.channelData[ch].inputCal = 0


    def _createetocoarsepat(patdata):
        pattern = PatternAssembler()
        pattern.LoadString("""\
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                    BlockFails 1
                    Drive0CompareLVectors length=1024
                    BlockFails 0
                    %repeat 256
                    V link=0, ctv=0, mtv=1, lrpt=0, data=0vH0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    %end
                    Drive1CompareHVectors length=1024
                    PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                """)
        patdata = pattern.Generate()
        return patdata


    def _createotecoarsepat(patdata):
        pattern = PatternAssembler()
        pattern.LoadString("""\
                    S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                    BlockFails 1
                    Drive0CompareLVectors length=1024
                    BlockFails 0
                    %repeat 256
                    V link=0, ctv=0, mtv=1, lrpt=0, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H
                    %end
                    Drive1CompareHVectors length=1024
                    PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X
                """)
        patdata = pattern.Generate()
        return patdata



    # Create a random passing vectior Even to Odd for ODD PINS
    def _createRamdomEtoOddPat(patdata):
        pattern = PatternAssembler()
        pattern.LoadString("""\
                  S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
                  BlockFails 1
                  Drive0CompareLVectors length=1024
                  BlockFails 0
                  RandomPassingEvenToOddNoSwitchVectors length=102400
                  StopPattern 0x12345678
              """)
        patdata = pattern.Generate()
        return patdata


    # Create a random passing vectior Even to Odd for Even PINS
    def _createRamdomOteEvenPat(patdata):
        pattern = PatternAssembler()
        pattern.LoadString("""\
                  S stype=IOSTATEJAM,             data=0jZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
                  BlockFails 1
                  Drive0CompareLVectors length=1024
                  BlockFails 0
                  RandomPassingEvenToOddNoSwitchVectors length=102400
                  StopPattern 0x12345678
              """)
        patdata = pattern.Generate()
        return patdata



    def _executecoarsepat(hpcc, slice, pdata, period):
        mtv = 1
        mask = 0
        CALIBRATION_PERIOD = period
        hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
        hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
        hpcc.ac[slice].captureAll = False
        hpcc.ac[slice].SetPeriod(CALIBRATION_PERIOD)
        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': 0.0}, True, channels='ALL_PINS')
        hpcc.ac[slice].WritePattern(0, pdata)
        hpcc.ac[slice].ExecutePattern()



    def _executecoarselhpat(hpcc, env, pdata, period):
        mtv = 1
        mask = 0
        CALIBRATION_PERIOD = period
        for slice in [0,1]:
            hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
            hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
            hpcc.ac[slice].captureAll = False
            hpcc.ac[slice].SetPeriod(CALIBRATION_PERIOD)
            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': 0.0}, True, channels='ALL_PINS')
            hpcc.ac[slice].WritePattern(0, pdata)
        hpcc.ac[0].PrestagePattern()  # have to prestage twice, main reason for twice as slow as even to odd cal
        hpcc.ac[1].PrestagePattern()
        env.rc.send_sync_pulse()
        hpcc.ac[0].WaitForCompletePattern()  # Wait for all slices to complete
        hpcc.ac[1].WaitForCompletePattern()





    def _executefinepat(hpcc, slice, pdata, period):
        mtv = 1
        mask = 0
        CALIBRATION_PERIOD = period
        hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
        hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
        hpcc.ac[slice].captureAll = False
        hpcc.ac[slice].SetPeriod(CALIBRATION_PERIOD)
        hpcc.ac[slice].WritePattern(0, pdata)
        hpcc.ac[slice].ExecutePattern()




    def _clearchfailcount(hpcc):
        for slice in [0, 1]:
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                # ADD error checking for suck count


    def _LoadInputCal(filename, coarseresults, offset=0):
        file = open(filename, 'r')
        for line in file:
            if ',' in line:
                data = line.split(',')
                slice = int(data[0])
                channel = int(data[1])
                calValue = int(data[2])
                coarseresults[(slice, channel)] = calValue


    def sweep_slot1_666_pass1_test(self):
        slot = 1
        period = 666
        loops = 20
        periodCount = 10
        stepsPerPeriod = 16
        initialOffset = 4000
        # Create Patterns
        patStr = r'Hpcc\Patterns\CalRandomOddPins_1490218841.018052.obj.gz'
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, patStr), 'rb').read()
        randomcalETOpdata = pattern

        patStr = r'Hpcc\Patterns\CalRandomEvenPins_1490218841.018052.obj.gz'
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, patStr), 'rb').read()
        randomcalOTEpdata = pattern

        self.sweep_from_zero(slot, period, loops, periodCount, stepsPerPeriod, initialOffset, randomcalETOpdata, randomcalOTEpdata)

    def sweep_slot1_666_pass2_test(self):
        slot = 1
        period = 666
        loops = 20
        periodCount = 10
        stepsPerPeriod = 16
        initialOffset = 4000
        # Create Patterns
        patStr = r'Hpcc\Patterns\CalRandomOddPins_1490218841.018052.obj.gz'
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, patStr), 'rb').read()
        randomcalETOpdata = pattern

        patStr = r'Hpcc\Patterns\CalRandomEvenPins_1490218841.018052.obj.gz'
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, patStr), 'rb').read()
        randomcalOTEpdata = pattern

        self.sweep_from_zero(slot, period, loops, periodCount, stepsPerPeriod, initialOffset, randomcalETOpdata, randomcalOTEpdata)

    def sweep_from_zero(self, testSlot, period, loopCount, periodCount, stepsPerPeriod, initialOffset,randomcalETOpdata,randomcalOTEpdata):
        slot = testSlot
        loops = loopCount
        calibrationPeriod = period
        periodCount = periodCount
        stepsPerPeriod = stepsPerPeriod
        initialOffsetDelay = initialOffset
        slices = [0]
        skipEven = 1

        stepSize = int(period / stepsPerPeriod)
        stepCount = periodCount * stepsPerPeriod

        self.Log('info', 'Slot Under Test: {}, Numer of Loops: {}, Periods to Sweep: {}, Calibration Period: {}PS, '.format(slot, loops, periodCount, calibrationPeriod))
        self.Log('info', 'Number of Steps: {}, Step Size: {}PS'.format(stepCount, stepSize))

        hpcc = self.env.instruments[slot]

        # Set even to odd loopback and clear cal data
        # hpcc.cal.SetEvenToOddChannelLoopback()
        # for ac in hpcc.ac:
        #     ac.internalLoopback = False

        CalDebug._clearcaloffset(hpcc)

        # # set up PEC with default values
        # for slice in slices:
        #    hpcc.dc[slice].SetPEAttributes(
        #        {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0, 'VOX': Diags._myVox,
        #         'termVRef': Diags._myTermVref, 'VIL': Diags._myVil, 'VIH': Diags._myVih}, True, channels='ALL_PINS')

        # # set up PEC with default values
        # for slice in slices:
        #     for ch in range(56):
        #         hpcc.dc[slice].SetPEAttributes(
        #             {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0, 'VOX': Diags._myVox,
        #              'termVRef': Diags._myTermVref, 'VIL': Diags._myVil, 'VIH': Diags._myVih}, True, channels=[ch])



        # set up data structure to hold shmoo results
        shmoodat = {}
        CalDebug._ClearShmoo(shmoodat, loops)
        for loop in range(loops):
            for slice in slices:
                for ch in range(56): shmoodat[(loop, slice, ch)] = {'LEFTEDGE': -999, 'RIGHTEDGE': -999, 'EYEWIDTH': -999, 'EYECENTER': -999, 'EYESTEPS': -999, 'SLOT': slot, 'SHMOO': []}

        # create structure to hold intial offset for each pin
        initialOffsets = {}
        CalDebug._ClearShmoo(initialOffsets, loops)
        for slice in slices:
            for ch in range(56):
                initialOffsets[(slice, ch)] = initialOffsetDelay

        # Run pattern and collect data for number of total loops specified
        for loop in range(loops):
            self.Log('info', 'Loop Interation: {}'.format(loop))

            # # Set even to odd loopback
            # hpcc.cal.SetEvenToOddChannelLoopback()
            # for ac in hpcc.ac:
            #     ac.internalLoopback = False

            # ODD PINS
            currentoffset = 0
            for shmooIter in range(stepCount):

                # Set even to odd loopback
                hpcc.cal.SetEvenToOddChannelLoopback()
                for ac in hpcc.ac:
                    ac.internalLoopback = False

                self.Log('info', 'ODD PIN Sweep Interation: {}: Current offset: {}'.format(shmooIter, currentoffset))
                # Loop through each slice and each channel, starting at 1,3,5,7......
                for slice in slices:
                    # set period
                    hpcc.ac[slice].SetPeriod(period / PICO_SECONDS_PER_SECOND)

                    # try all_pins for step setpeatrributes instead of per pin
                    compareOffset = initialOffsets[(slice, 0)] + currentoffset
                    # hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': compareOffset / PICO_SECONDS_PER_SECOND}, True, channels='ALL_PINS')

                    hpcc.dc[slice].SetPEAttributes(
                        {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': compareOffset / PICO_SECONDS_PER_SECOND, 'VOX': CalDebug._myVox,
                         'termVRef': CalDebug._myTermVref, 'VIL': CalDebug._myVil, 'VIH': CalDebug._myVih}, True, channels='ALL_PINS')

                    # for ch in range(56):
                    #     compareOffset = initialOffsets[(slice, ch)] + currentoffset
                    #     hpcc.dc[slice].SetPEAttributes(
                    #         {'IS_ENABLED_CLOCK': False, 'compare': compareOffset / PICO_SECONDS_PER_SECOND}, True, channels=[ch])
                    # write and execute pattern
                    hpcc.ac[slice].WritePattern(0, randomcalETOpdata)
                    hpcc.ac[slice].captureAll = False
                    hpcc.ac[slice].ExecutePattern()
                    # For each odd channel get the channel fail count and save it along with cal offset
                    # sweepresults[(slice,ch)] = [[fail1,offset1],......,[failn,offsetn]]
                    startChannel = 1
                    for ch in range(56):
                        if ch % 2 == startChannel:
                            compareOffset = initialOffsets[(slice, ch)] + currentoffset
                            captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                            shmoodat[(loop, slice, ch)]['SHMOO'].append([captureCount.ChannelFailCaptureCount, compareOffset])
                currentoffset = currentoffset + stepSize

            if not skipEven:
                # EVEN PINS
                currentoffset = 0
                for shmooIter in range(stepCount):
                    self.Log('info', 'EVEN PIN Sweep Interation: {}: Current offset: {}'.format(shmooIter, currentoffset))
                    # Loop through each slice and each channel, starting at 0,2,4,6,......
                    for slice in slices:
                        # set period
                        hpcc.ac[slice].SetPeriod(period / PICO_SECONDS_PER_SECOND)

                        # try all_pins for step setpeatrributes instead of per pin
                        compareOffset = initialOffsets[(slice, 0)] + currentoffset
                        hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': compareOffset / PICO_SECONDS_PER_SECOND}, True, channels='ALL_PINS')

                        # for ch in range(56):
                        #     compareOffset = initialOffsets[(slice, ch)] + currentoffset
                        #     hpcc.dc[slice].SetPEAttributes(
                        #         {'IS_ENABLED_CLOCK': False, 'compare': compareOffset / PICO_SECONDS_PER_SECOND}, True,
                        #         channels=[ch])

                        # write and execute pattern
                        hpcc.ac[slice].WritePattern(0, randomcalOTEpdata)
                        hpcc.ac[slice].captureAll = False
                        hpcc.ac[slice].ExecutePattern()
                        # For each odd channel get the channel fail count and save it along with cal offset
                        # sweepresults[(slice,ch)] = [[fail1,offset1],......,[failn,offsetn]]
                        startChannel = 0
                        for ch in range(56):
                            if ch % 2 == startChannel:
                                compareOffset = initialOffsets[(slice, ch)] + currentoffset
                                captureCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                                shmoodat[(loop, slice, ch)]['SHMOO'].append([captureCount.ChannelFailCaptureCount, compareOffset])
                    currentoffset = currentoffset + stepSize

            for slice in slices:
                for ch in range(56):
                    # reset count used for initial loop
                    count = 0
                    # reset low and high eye values
                    lowval = 0
                    highval = 0
                    for list in shmoodat[(loop, slice, ch)]['SHMOO']:
                        # if there were no fails on the current step
                        if list[0] == 0:
                            # for the first passing step set the low and high to the offset value
                            if count == 0:
                                lowval = list[1]
                                highval = list[1]
                            # find the high value for the eye
                            if list[1] >= highval:
                                highval = list[1]
                            count += 1
                    if count == 0:
                        lowval = -999
                        highval = -999
                    self.Log('info', 'LOOP {}, CH {}, SLICE {}, Low Value: {}, High Value: {}, Width: {}, Width Steps: {}'.format(loop, ch, slice, lowval, highval, highval - lowval, count - 1))
                    shmoodat[(loop, slice, ch)]['LEFTEDGE'] = lowval
                    shmoodat[(loop, slice, ch)]['RIGHTEDGE'] = highval
                    shmoodat[(loop, slice, ch)]['EYEWIDTH'] = highval - lowval
                    shmoodat[(loop, slice, ch)]['EYECENTER'] = lowval + ((highval - lowval) / 2)
                    shmoodat[(loop, slice, ch)]['EYESTEPS'] = count - 1

        # write data to file
        # create files and headers to hssold data
        time_string = time.strftime("%Y%m%d%H%M%S")
        filename = 'shmoo_detail_' + time_string + '.csv'

        with open(filename, 'w') as f:
            # write header to file
            temp_list = []
            temp_list.append('LOOP')
            temp_list.append('SLOT')
            temp_list.append('SLICE')
            temp_list.append('CHAN')
            temp_list.append('OFFSET')
            temp_list.append('FAILS')

            print(','.join(temp_list), file=f)

            for loop in range(loops):
                for slice in slices:
                    for ch in range(56):
                        # Lets dump shmoo data to csv file with cols: loop,slot,slice,ch,offset,fails
                        for list in shmoodat[(loop, slice, ch)]['SHMOO']:
                            temp_list = []
                            temp_list.append(str(loop))
                            temp_list.append(str(shmoodat[(loop, slice, ch)]['SLOT']))
                            temp_list.append(str(slice))
                            temp_list.append(str(ch))
                            temp_list.append(str(list[1]))
                            temp_list.append(str(list[0]))
                            print(','.join(temp_list), file=f)



    def CalVerifyTest(self):

        self.Log('info','fpgas: {}'.format(self.env.fpgas))
        for (slot,slice) in self.env.fpgas:
            if slice == 0:
                hpcc = self.env.instruments[slot]
                period = ((hpcc.ac[0].minQdrMode  * 2) - hpcc.ac[0].nominalFineDelayValue)/ PICO_SECONDS_PER_SECOND

        patlength = 40000
        self.CalEvenToOddVerifyTest(period,patlength)
        self.CalLowToHighVerifyTest(period,patlength)

        patlength = 20000
        self.CalEvenToOddVerifyTest(period,patlength)
        self.CalLowToHighVerifyTest(period,patlength)

        patlength = 10000
        self.CalEvenToOddVerifyTest(period, patlength)
        self.CalLowToHighVerifyTest(period, patlength)

        patlength = 5000
        self.CalEvenToOddVerifyTest(period, patlength)
        self.CalLowToHighVerifyTest(period, patlength)

        patlength = 1000
        self.CalEvenToOddVerifyTest(period, patlength)
        self.CalLowToHighVerifyTest(period, patlength)


    def CalEvenToOddVerifyTest(self,period,patlength):
        self.Log('info', 'Verifying Even to Odd Calibration')
        self.env.SetConfig('EvenToOddLoopback', loadCal=True)
        patLength = patlength

        OddPattern = PatternAssembler()
        EvenPattern = PatternAssembler()

        ioJamForOdd = 'S stype=0, data=0xeeeeeeeeeeeeeeeeeeeeeeeeeeee'
        ioJamForEven = 'S stype=0, data=0xbbbbbbbbbbbbbbbbbbbbbbbbbbbb'

        # Create odd pat

        OddPattern.LoadString("""
                  {}
                  BlockFails 1
                  Drive0CompareLVectors length=1024
                  BlockFails 0
                  RandomPassingEvenToOddNoSwitchVectors length={}
                  StopPattern 0x12345678
              """.format(ioJamForOdd, patLength))
        OddPatData = OddPattern.Generate()

        # even pattern
        EvenPattern.LoadString("""
                  {}
                  BlockFails 1
                  Drive0CompareLVectors length=1024
                  BlockFails 0
                  RandomPassingEvenToOddNoSwitchVectors length={}
                  StopPattern 0x12345678
              """.format(ioJamForEven, patLength))
        EvenPatData = EvenPattern.Generate()

        self.Log('info','Testing Odd Pattern')
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            #period = ((hpcc.ac[0].minQdrMode  * 2) - hpcc.ac[0].nominalFineDelayValue)/ PICO_SECONDS_PER_SECOND
            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period/2}, True, channels='ALL_PINS')
            hpcc.ac[slice].captureAll = True
            self.env.RunPattern(slot, slice, OddPatData, period)
            # self.env.DumpCapture(slot, slice)
            captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if captureCount > 0:
                self.Log('error', 'Pattern Failed: Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))
            else:
                self.Log('info', 'Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))

        self.Log('info', 'Testing Even Pattern')
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            #period = ((hpcc.ac[0].minQdrMode * 2) - hpcc.ac[0].nominalFineDelayValue) / PICO_SECONDS_PER_SECOND
            hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True, channels='ALL_PINS')
            hpcc.ac[slice].captureAll = True
            self.env.RunPattern(slot, slice, EvenPatData, period)
            # self.env.DumpCapture(slot, slice)
            captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if captureCount > 0:
                self.Log('error', 'Pattern Failed: Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))
            else:
                self.Log('info', 'Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))


    def CalLowToHighVerifyTest(self,period,patlength):
        self.Log('info','Verifying Low to High Calibration')
        self.env.SetConfig('LowToHighLoopback', loadCal=True)
        patLength = patlength

        OddPattern = PatternAssembler()
        EvenPattern = PatternAssembler()

        ioJamForOdd = 'S stype=0, data=0xeeeeeeeeeeeeeeeeeeeeeeeeeeee'
        ioJamForEven = 'S stype=0, data=0xbbbbbbbbbbbbbbbbbbbbbbbbbbbb'

        # Create odd pat

        OddPattern.LoadString("""
                  S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
                  BlockFails 1
                  Drive0CompareLVectors length=1024
                  BlockFails 0
                  RandomPassingEvenToOddNoSwitchVectors length={}
                  StopPattern 0x12345678
              """.format(patLength))
        OddPatData = OddPattern.Generate()

        # even pattern
        EvenPattern.LoadString("""
                  S stype=IOSTATEJAM,             data=0jZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
                  BlockFails 1
                  Drive0CompareLVectors length=1024
                  BlockFails 0
                  RandomPassingEvenToOddNoSwitchVectors length={}
                  StopPattern 0x12345678
              """.format(patLength))
        EvenPatData = EvenPattern.Generate()

        self.Log('info', 'Testing Odd Pattern')

        for (slot, hpcc) in self.env.instruments.items():
            hpcc = self.env.instruments[slot]
            mtv = 1
            mask = 0
            #period = ((hpcc.ac[0].minQdrMode * 2) - hpcc.ac[0].nominalFineDelayValue) / PICO_SECONDS_PER_SECOND
            for slice in [0,1]:
                hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
                hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].captureAll = False
                hpcc.ac[slice].SetPeriod(period)
                hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': period / 2}, True, channels='ALL_PINS')
                hpcc.ac[slice].WritePattern(0, OddPatData)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1]:
                hpcc.ac[slice].WaitForCompletePattern()
                captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
                if captureCount > 0:
                    self.Log('error', 'Pattern Failed: Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))
                else:
                    self.Log('info', 'Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))

        self.Log('info', 'Testing Even Pattern')

        for (slot, hpcc) in self.env.instruments.items():
            hpcc = self.env.instruments[slot]
            mtv = 1
            mask = 0
            #period = ((hpcc.ac[0].minQdrMode * 2) - hpcc.ac[0].nominalFineDelayValue) / PICO_SECONDS_PER_SECOND
            for slice in [0, 1]:
                hpcc.ac[slice].Write('SelectableFailMask{}Lower'.format(mtv), mask & 0b11111111111111111111111111111111)
                hpcc.ac[slice].Write('SelectableFailMask{}Upper'.format(mtv), (mask >> 32) & 0b11111111111111111111111111111111)
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, False, False)
                hpcc.ac[slice].captureAll = False
                hpcc.ac[slice].SetPeriod(period)
                hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'compare': period / 2}, True, channels='ALL_PINS')
                hpcc.ac[slice].WritePattern(0, EvenPatData)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1]:
                hpcc.ac[slice].WaitForCompletePattern()
                captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
                if captureCount > 0:
                    self.Log('error', 'Pattern Failed: Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))
                else:
                    self.Log('info', 'Period: {} Slot: {} Slice: {} Size: {} Fails: {}'.format(period, slot, slice, patLength, captureCount))


class Diags(HpccTest):

    @staticmethod
    def _ClearShmoo(estimate,loopCount):
        for loop in range(loopCount):
            for slice in [0, 1]:
                for ch in range(56):
                    estimate[(loop,slice, ch)] = []
        return estimate



class PcieAccess(HpccTest):

    def i2c_status(self, n):
        return 'I2C_SUCCESS I2C_TIMEOUT I2C_NOACK I2C_BUS_ERROR I2C_NOT_READY'.split()[n]


    def MarkRandomSlowPeriodTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddNoSwitchVectors length=2051
            StopPattern 0x12345678
        """)
        resultsList = []
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            if slice == 0:
                self.env.LogPerSlice(slot, slice)
                hpcc = self.env.instruments[slot]
                period = 5000 / PICO_SECONDS_PER_SECOND
                self.Log('info', 'Testing period = {}'.format(period))
                myVox = 1.5

                for x in range(0, 31, 1):
                    myVox = x / 10
                    # print('VOX: {}'.format(myVox))
                    self.env.SetPEAttributesDCandSim(slot, 0, {'IS_ENABLED_CLOCK': False, 'VOX': myVox}, True)
                    self.env.SetPEAttributesDCandSim(slot, 1, {'IS_ENABLED_CLOCK': False, 'VOX': myVox}, True)
                    self.env.RunPattern(slot, slice, pattern, period)
                    # self.env.RunCheckers(slot, slice)

                    # self.Log('info', 'Dump Capture for Slot {}, Slice {}'.format( slot, slice) )
                    # self.env.DumpCapture(slot, slice)
                    captureData = hpcc.ac[slice].ReadCapture()
                    for block in hpcc.ac[slice].UnpackCapture(captureData):
                        header, data = block
                        for i in range(8):
                            if header.CapturedVectorsValidFlags[i] == 1:
                                vaddr = data[i].VectorAddress
                                actual = data[i].PinLaneData
                                # actualbits = bin(actual)[2:].zfill(56)
                                # print(actualbits[10])
                                if vaddr in [1023, 1024, 1025]:
                                    actual = data[i].PinLaneData
                                    actualbits = bin(actual)[2:].zfill(56)
                                    #                                   print(actualbits[1])
                                    a = [slice, vaddr, myVox, actualbits[1]]
                                    resultsList.append(a)
            1
            # print(resultsList)
        i = 0
        for x in resultsList:
            myVaddr = x[1]
            mySlice = x[0]
            myVox = x[2]
            myBit = x[3]
            if myVaddr == 1023:
                if mySlice == 0:
                    if i == 0:
                        previous = myBit
                        i = i + 1
                        #                    print ('{},{}'.format(myVox,myBit))
                    if myBit != previous:
                        print('transition at {} to {}'.format(myVox - 0.1, myVox))

                    previous = myBit

    # function to decode the bit strings read back from the tester power supplies to report out on status and voltage and currents
    def translate(self, idx, data):
        if VENDOR[idx] == 'Delta':
            I_factor = .4706
        else:
            I_factor = .1959

        print('\n{:02X}h Status ='.format(data[0]), end='')
        if data[0] & 0x80: print(' PS_ON', end='')
        if data[0] & 0x40: print(' P_GOOD', end='')
        if data[0] & 0x20: print(' AC_OK', end='')
        if data[0] & 0x10: print(' PS_BAD', end='')
        if data[0] & 0x08: print(' FAN_Fail', end='')
        if data[0] & 0x04: print(' OT_Warn', end='')
        if data[0] & 0x02: print(' OT_Crit', end='')
        if data[0] & 0x01: print(' AC_RANGE', end='')
        print()

        print('{:02X}h Fault'.format(data[1]))
        if data[1] & 0x80: print('   Main Output Overvoltage Fault')
        if data[1] & 0x40: print('   Main Output Undervoltage Fault')
        if data[1] & 0x20: print('   Main Output Overcurrent Fault')
        if data[1] & 0x10: print('   Standby Output Fail Detect')
        if data[1] & 0x08: print('   Fan 1 Stall/Overcurrent Detect')
        if data[1] & 0x04: print('   Fan 2 Stall/Overcurrent Detect')

        # Translation factors from HAL...Voltage looks right (~48V) but currents need to be verified as accurate.
        # Quite likely currents are wrong for Delta.
        print('{:02X}h Main Output Voltage:   {:7.4f}V'.format(data[2], data[2] * .2314))
        print('{:02X}h Main Output Current:   {:7.4f}A ({})'.format(data[3], data[3] * I_factor, VENDOR[idx]))
        if VENDOR[idx] == 'Murata':
            print('{:02X}h Fan1 Filtered Current: {:7.4f}A (need to verify)'.format(data[4], data[4] * .01104))
            print('{:02X}h Fan2 Filtered Current: {:7.4f}A (need to verify)'.format(data[5], data[5] * .01104))
        else:
            print('{:02X}h Fan1 raw byte'.format(data[4]))
            print('{:02X}h Fan2 raw byte'.format(data[5]))


            # create a string from the raw data read from the power supplies

    def marktranslate(self, idx, data, device):
        if VENDOR[idx] == 'Delta':
            I_factor = .4706
        else:
            I_factor = .1959

        outstring = []

        # primary or secondary ps
        outstring.append(device)
        # vendor of board
        outstring.append(VENDOR[idx])
        # status normally see 0xEH
        outstring.append('{:02X}h'.format(data[0]))
        # fault normally see 0x00
        outstring.append('{:02X}h'.format(data[1]))
        # raw voltage
        outstring.append('{:02X}h'.format(data[2]))
        # voltage
        outstring.append('{:7.4f}'.format(data[2] * .2314))

        # ifactor
        outstring.append('{}'.format(I_factor))
        # raw current
        outstring.append('{:02X}h'.format(data[3]))
        # current
        outstring.append('{:7.4f}'.format(data[3] * I_factor))

        # raw fan 1
        outstring.append('{:02X}h'.format(data[4]))
        # raw fan 2
        outstring.append('{:02X}h'.format(data[5]))

        return outstring

    # determine vendor id of system power supplies
    def vendor(self, h):
        v = ['Unknown'] * 2
        if self.write(h, I2C_BULK[PRIMARY]):
            hil.cypI2cWrite(h, 0xB0, b'\x00')
            if hil.cypI2cStatus(h) == 0:
                v[0] = 'Delta'
            else:
                v[0] = 'Murata'
        if self.write(h, I2C_BULK[SECONDARY]):
            hil.cypI2cWrite(h, 0xB2, b'\x00')
            if hil.cypI2cStatus(h) == 0:
                v[1] = 'Delta'
            else:
                v[1] = 'Murata'
        return tuple(v)

    def wait(self, h):
        start = time.time()
        while time.time() - start < 1:
            status = hil.cypI2cStatus(h)
            if status == 0:
                return status
            print('retry, status = {}({})'.format(self.i2c_status(status), status))
            time.sleep(.2)
        return status

    # read status register from system power supplies and print data
    def read(self, h, idx):
        data = hil.cypI2cRead(h, BULK_ADDR[idx], 6)
        status = self.wait(h)
        if status == 0:
            self.translate(idx, data)
        else:
            print('fail, status = {}({})'.format(self.i2c_status(status), status))

    # read status register from system power supplies and return data
    def markread(self, h, idx, device):
        data = hil.cypI2cRead(h, BULK_ADDR[idx], 6)
        status = self.wait(h)
        if status == 0:
            x = self.marktranslate(idx, data, device)
            return (x)
        else:
            print('fail, status = {}({})'.format(self.i2c_status(status), status))

    def write(self, h, data):
        hil.cypI2cWrite(h, PCA9548A_U7_ADDR, data)
        return self.wait(h) == 0

    # @unittest.skip('This test is for debug purposes only')
    def MarkCalTest(self):
        slot = 1
        self.Log('info', 'test')
        # dcCal = hpcctb.dcCal.DcCalibration(slot)
        # dcCal.DcCalibration()
        hpcc = self.env.instruments[slot]
        rc = self.env.rc
        loopbackCal = Calibration(hpcc, rc)
        loopbackCal.LoopbackCalEvenToOdd()
        # loopbackCal.LoopbackCalLowToHigh()


    def MarkRandomTest(self):
        self.env.SetConfig('EvenToOddLoopback', loadCal=True)
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddNoSwitchVectors length=2051
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()

        for (slot, slice) in self.env.fpgas:
            if slice == 0:
                for x in range(0, 1):
                    self.env.LogPerSlice(slot, slice)
                    hpcc = self.env.instruments[slot]
                    period = hpcc.ac[0].minQdrMode / PICO_SECONDS_PER_SECOND
                    self.Log('info', 'Testing period = {}'.format(period))
                    myVox = 1.5
                    myDelay = 0
                    self.env.SetPEAttributesDCandSim(slot, 0,
                                                     {'IS_ENABLED_CLOCK': False, 'VOX': myVox, 'compare': myDelay},
                                                     True)
                    self.env.SetPEAttributesDCandSim(slot, 1,
                                                     {'IS_ENABLED_CLOCK': False, 'VOX': myVox, 'compare': myDelay},
                                                     True)
                    # self.env.SetPEAttributesDCandSim(slot, 0, {'IS_ENABLED_CLOCK': False,'VOX': myVox}, True)
                    # self.env.SetPEAttributesDCandSim(slot, 1, {'IS_ENABLED_CLOCK': False,'VOX': myVox}, True)

                    hpcc.ac[slice].captureAll = True
                    self.env.RunPattern(slot, slice, pattern, period)
                    # self.env.DumpCapture(slot, slice)
                    captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
                    self.Log('info', 'TOTAL FAIL COUNT: {}'.format(captureCount))

    # Run a pattern on all HPCC's specified for a specified about of time to simulate read use running conditions.  CSV data is logged to a file.
    # Logging current, voltage, fan from from both supplies
    # logging junction temperature of HPCC AC
    @unittest.skip('This test is a special test case for mechanical validation team')

    def ThermalStressTest(self):
        run_time = 60
        pre_time = 60
        post_time = 60

        PICO_SECONDS_PER_SECOND = 1000000000000

        h = hil.cypDeviceOpen(hil.CYP_VIDPID_RC, hil.HIL_ANY_SLOT, None)
        hil.cypPortDirectionWrite(h, hil.CP_PORTA, PORTA_IO_DIR)
        data = hil.cypPortRead(h, hil.CP_PORTA)
        hil.cypPortWrite(h, hil.CP_PORTA, (data & ~PORTA_I2C_MUX_SEL_MASK) | PORTA_I2C_MUX_SEL_BP)
        global VENDOR
        VENDOR = self.vendor(h)

        self.write(h, I2C_BULK[PRIMARY])
        x = self.markread(h, PRIMARY, 'Primary')
        print('****')
        print(x)
        print('****')

        print('\nPrimary (3 readings)')
        if self.write(h, I2C_BULK[PRIMARY]):
            self.read(h, PRIMARY)
            self.read(h, PRIMARY)
            self.read(h, PRIMARY)
        print('\nSecondary (3 readings)')
        if self.write(h, I2C_BULK[SECONDARY]):
            self.read(h, SECONDARY)
            self.read(h, SECONDARY)
            self.read(h, SECONDARY)

        for slot, hpcc in self.env.instruments.items():
            self.Log('info', 'test slot {}'.format(slot))
            # dcCal = hpcctb.dcCal.DcCalibration(slot)
            # dcCal.DcCalibration()

            self.Log('info', '****In DCCalEvenToOdd****')

            self.env.SetConfig('EvenToOddLoopback', loadCal=False)

            for slice in [0, 1]:
                hpcc = self.env.instruments[slot]
                # CALIBRATION_PERIOD = hpcc.ac[slice].minQdrMode / PICO_SECONDS_PER_SECOND

                # odd channel
                pattern = PatternAssembler()
                pattern.LoadString("""\
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
                    BlockFails 1
                    Drive0CompareLVectors length=100000
                    %repeat 100000
                    V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    Drive1CompareHVectors length=1
                    %end
                    BlockFails 0
                    Goto 0
                    PATTERN_END:                                                                                     
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
                """)
                pdata = pattern.Generate()
                self.Log('info', '*******Calling RunDcCalEvenToOdd**********')
                self.env.SetSelectableFailMask(slot, slice, 1, 0)
                self.env.WritePattern(slot, slice, 0, pdata)
                hpcc.ac[slice].captureAll = True
                # hpcc.ac[slice].SetupCapture(0x20000, ac_registers.CaptureControl.PIN_STATE)
                # period = self.CALIBRATION_PERIOD
                period = 0.8e-9
                hpcc.ac[slice].SetPeriod(period)
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False}, True)
                hpcc.ac[slice].PrestagePattern()
        temp_list = []

        time_string = time.strftime("%Y%m%d%H%M%S")
        filename = 'temperature_power_' + time_string + '.csv'
        with open(filename, 'w') as f:
            temp_list.append('Supply')
            temp_list.append('Type')
            temp_list.append('Status')
            temp_list.append('Fault')
            temp_list.append('Raw Voltage')
            temp_list.append('Calc Voltage')
            temp_list.append('ifactor')
            temp_list.append('Raw Current')
            temp_list.append('Calc Current')
            temp_list.append('Fan 1')
            temp_list.append('Fan 2')
            temp_list.append('Supply')
            temp_list.append('Type')
            temp_list.append('Status')
            temp_list.append('Fault')
            temp_list.append('Raw Voltage')
            temp_list.append('Calc Voltage')
            temp_list.append('ifactor')
            temp_list.append('Raw Current')
            temp_list.append('Calc Current')
            temp_list.append('Fan 1')
            temp_list.append('Fan 2')

            for slot, hpcc in self.env.instruments.items():
                for slice in [0, 1]:
                    temp_list.append('ACFPGA S{}.{}'.format(slot, slice))

            print(','.join(temp_list), file=f)

            for i in range(pre_time * 2):
                time.sleep(.5)
                temp_list = []
                self.write(h, I2C_BULK[PRIMARY])
                x = self.markread(h, PRIMARY, 'Primary')
                self.write(h, I2C_BULK[SECONDARY])
                y = self.markread(h, SECONDARY, 'Secondary')
                temp_list.extend(x)
                temp_list.extend(y)
                # print (','.join(temp_list),file = f)
                for slot, hpcc in self.env.instruments.items():
                    self.Log('info', 'Slot {}'.format(slot))
                    hpcc = self.env.instruments[slot]
                    for slice in [0, 1]:
                        completed = hpcc.ac[slice].IsPatternComplete()
                        temperature = (hpcc.dc[slice].Read('AcFpgaTemperature').HpccAcFpgaTemperature) / 16
                        temp_list.append(str(temperature))
                        self.Log('info', 'Check complete loop {} status for slice {} is {} and temp {}'.format(i, slice,
                                                                                                               completed,
                                                                                                               temperature))
                print(','.join(temp_list), file=f)

            self.env.rc.send_sync_pulse()

            for i in range(run_time * 2):
                time.sleep(.5)
                # for slot, hpcc in self.env.instruments.items():                
                # for slice in [0, 1]:
                # hpcc.ac[slice].PrestagePattern()
                # self.env.rc.SendSyncPulse()
                temp_list = []
                self.write(h, I2C_BULK[PRIMARY])
                x = self.markread(h, PRIMARY, 'Primary')
                self.write(h, I2C_BULK[SECONDARY])
                y = self.markread(h, SECONDARY, 'Secondary')
                temp_list.extend(x)
                temp_list.extend(y)
                # print (','.join(temp_list),file = f)
                for slot, hpcc in self.env.instruments.items():
                    self.Log('info', 'Slot {}'.format(slot))
                    hpcc = self.env.instruments[slot]
                    for slice in [0, 1]:
                        completed = hpcc.ac[slice].IsPatternComplete()
                        temperature = (hpcc.dc[slice].Read('AcFpgaTemperature').HpccAcFpgaTemperature) / 16
                        temp_list.append(str(temperature))
                        self.Log('info', 'Check complete loop {} status for slice {} is {} and temp {}'.format(i, slice,
                                                                                                               completed,
                                                                                                               temperature))
                print(','.join(temp_list), file=f)

            for slot, hpcc in self.env.instruments.items():
                for slice in [0, 1]:
                    hpcc.ac[slice].AbortPattern()

            for i in range(post_time * 2):
                time.sleep(.5)
                temp_list = []
                self.write(h, I2C_BULK[PRIMARY])
                x = self.markread(h, PRIMARY, 'Primary')
                self.write(h, I2C_BULK[SECONDARY])
                y = self.markread(h, SECONDARY, 'Secondary')
                temp_list.extend(x)
                temp_list.extend(y)
                # print (','.join(temp_list),file = f)
                for slot, hpcc in self.env.instruments.items():
                    self.Log('info', 'Slot {}'.format(slot))
                    hpcc = self.env.instruments[slot]
                    for slice in [0, 1]:
                        completed = hpcc.ac[slice].IsPatternComplete()
                        temperature = (hpcc.dc[slice].Read('AcFpgaTemperature').HpccAcFpgaTemperature) / 16
                        temp_list.append(str(temperature))
                        self.Log('info', 'Check complete loop {} status for slice {} is {} and temp {}'.format(i, slice,
                                                                                                               completed,
                                                                                                               temperature))
                print(','.join(temp_list), file=f)

    @staticmethod
    def BuildPattern(length):
        # Build a pattern of the given length with a bunch of random vectors
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = length - 3  # 3 = len(Initial S, End I, Final S)
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=`rand(112)`
            RandomVectors length=REPEATS
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=`rand(32)`
            S stype=IOSTATEJAM,             data=`rand(112)`
        """)
        return asm.Generate()

    def SavePerChannelRegisters(self, registers):
        perChannelRegisterValues = {}
        for slot, hpcc in self.env.instruments.items():
            perChannelRegisterValues[hpcc.slot] = {}
            for ac in hpcc.ac:
                perChannelRegisterValues[hpcc.slot][ac.slice] = {}
                for ch in range(ac.channels):
                    perChannelRegisterValues[hpcc.slot][ac.slice][ch] = {}
                    for register in registers:
                        perChannelRegisterValues[hpcc.slot][ac.slice][ch][register] = ac.ReadChannel(ch,
                                                                                                     register).Pack()
        return perChannelRegisterValues

    def RestorePerChannelRegisters(self, registers, perChannelRegisterValues):
        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                for ch in range(ac.channels):
                    for register in registers:
                        value = perChannelRegisterValues[hpcc.slot][ac.slice][ch][register]
                        ac.WriteChannel(ch, register, value)

    def MiniDmaWritePatternTest(self):
        K = 1024
        M = 1024 * 1024
        G = 1024 * 1024 * 1024
        addresses = [i for i in [0, 1 * K, 16 * K, 1 * M, 16 * M, 1 * G]]
        patterns = [self.BuildPattern(i) for i in [128, 256, 1024, 4096]]
        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                for pattern in patterns:
                    for address in addresses:
                        ac.DmaWrite(address, pattern)
                        data = ac.DmaRead(address, len(pattern))
                        if data != pattern:
                            self.Log('error',
                                     'Failed to write pattern data to HPCC({}, {}) for address=0x{:x}'.format(hpcc.slot,
                                                                                                              ac.slice,
                                                                                                              address))

    def MiniBarReadWriteTest(self):
        repeats = 10  # TODO: Increase repeats to 1000 or something big and crazy
        registers = [
            'CaptureBaseAddress',
        ]
        perChannelRegisters = [
            # 'PerChannelControlConfig1',  # Reserved bits will return zero
            'PerChannelControlConfig2',
            'PerChannelControlConfig3',
            'PerChannelControlConfig4',
        ]
        # NOTE: If this test crashes before it restores the per-channel registers most of the tests will fail after that
        backup = self.SavePerChannelRegisters(perChannelRegisters)
        for r in range(repeats):
            for slot, hpcc in self.env.instruments.items():
                for ac in hpcc.ac:
                    for register in registers:
                        expected = random.randint(0x0, 0xffffffff)
                        ac.Write(register, expected)
                        actual = ac.Read(register).Pack()
                        if expected != actual:
                            self.Log('error',
                                     'Run {} of {}: Failed Write/Read to register {} in Slot({}) expected=0x{:x}, actual=0x{:x}'.format(
                                         r + 1, repeats, register, hpcc.slot, expected, actual))
                    for ch in range(ac.channels):
                        for register in perChannelRegisters:
                            expected = random.randint(0x0, 0xffffffff)
                            ac.WriteChannel(ch, register, expected)
                            actual = ac.ReadChannel(ch, register).Pack()
                            if expected != actual:
                                self.Log('error',
                                         'Failed to write to per-channel register {}[{}] in HPCC({}, {}), expected=0x{:x}, actual=0x{:x}'.format(
                                             register, ch, hpcc.slot, ac.slice, expected, actual))
        # NOTE: If this test crashes before it restores the per-channel registers most of the tests will fail after that
        self.RestorePerChannelRegisters(perChannelRegisters, backup)

    def MiniProductionReleaseBuildInfoTest(self):
        """
        Checks the BuildInfo registers to see if this was a proper production build.
        """
        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                buildInfo1 = ac.Read('BuildInfo1')
                buildInfo2 = ac.Read('BuildInfo2')
                hgId = (buildInfo2.UpperMqIdDword << 32) | buildInfo1.LowerMqIdDword
                hgClean = buildInfo2.MercurialClean == 1
                fpgaNumber = buildInfo2.FpgaNumber
                if hgId == 0:
                    self.Log('error',
                             'Mercurial id is zero for this HPCC({}, {}). This is not allowed for production releases.'.format(
                                 hpcc.slot, ac.slice))
                if not hgClean:
                    self.Log('error',
                             'Mercurial clean bit is not set for this HPCC({}, {}). This is not allowed for production releases.'.format(
                                 hpcc.slot, ac.slice))
                if fpgaNumber != ac.slice:
                    self.Log('error',
                             'FPGA number is incorrect for HPCC({}, {}). This is not allowed for production releases.'.format(
                                 hpcc.slot, ac.slice))

    @unittest.skip('This test takes too long and it is not very useful')
    def MiniDmaReadFullRangeTest(self):
        for slot, hpcc in self.env.instruments.items():
            for ac in hpcc.ac:
                for i in range(1024 * 1024 * 1024 * 32 // (16 * 4096)):
                    try:
                        ac.DmaRead(i * 4096, 4096)
                    except:
                        self.Log('error',
                                 'Failed vector offset {} for HPCC({}, {})'.format(i * 4096, hpcc.slot, ac.slice))
                        break


class BasicFlatPattern(HpccTest):

    def Mini1KVTest(self):
        self.RunInternalLoopbackScenario(1024)

    def Mini10KVTest(self):
        self.RunInternalLoopbackScenario(10 * 1024)

    def MiniWalkingOne50NSPinStateCKTest(self):
        self.RunWalkingOneScenario(284, 50e-9, 'PIN_STATE')

    def MiniWalkingOne10NSFailStateCKTest(self):
        self.RunWalkingOneScenario(284, 10e-9, 'FAIL_STATUS')

    def _FindSubCyclePeriodRange(self):
        overlappingValue = 100
        subCycleOne = 2
        subCycleTwo = 4
        interval = 65
        periodRangeDict = {}
        oneSlicePeriodRangeList = None
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            hpcc.ac[slice]._UpdateFpgaCapabilities()
            subCycleOneLowerBound = hpcc.ac[slice].minQdrMode * subCycleOne
            subCycleOneUpperBound = hpcc.ac[slice].minQdrMode * subCycleTwo
            print(subCycleOneLowerBound)
            subCycleInterval = int((subCycleOneUpperBound - subCycleOneLowerBound) / interval)
            periodRangeDict[slot, slice] = range(subCycleOneLowerBound, subCycleOneUpperBound + overlappingValue,
                                                 subCycleInterval)
            oneSlicePeriodRangeList = periodRangeDict[slot, slice]

        return oneSlicePeriodRangeList

    def xMiniWalkingOneETOFreqSearchTest(self):

        self.env.SetConfig('EvenToOddLoopback')
        pattern = self._DriveWalkingOneEvenToOddLoopback()

        hangingPeriod = {}
        firstHang = {}
        found = {}
        for peri in self._FindSubCyclePeriodRange():
            period = peri * 1e-12  # random.uniform(2.5e-9, 25e-9)
            self.Log('info', 'Testing period = {} '.format(period))
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                completed, endstate = self.env.RunPattern(slot, slice, pattern, period)
                self.env.RunCheckers(slot, slice)
                if completed == False:
                    if not firstHang.setdefault((slot, slice), False):
                        hangingPeriod[(slot, slice)] = period
                        firstHang[slot, slice] = True
                    continue

            for (slot, slice) in self.env.fpgas:
                if firstHang.setdefault((slot, slice), False) == False:
                    found[(slot, slice)] = False
                elif firstHang[(slot, slice)] == True:
                    found[(slot, slice)] = True

            if all(val for val in found.values()):
                break

        if all(val for val in found.values()):
            for key, value in hangingPeriod.items():
                self.Log('info', 'Hanging period is {} for Slot {} slice {} '.format(value, key[0], key[1]))
                # print(key, value)
                self.env.LogPerSlice(key[0], key[1])
                completed, endstate = self.env.RunPattern(key[0], key[1], pattern=pattern, period=value)
                self.env.RunCheckers(key[0], key[1])

    def MiniWalkingOneEvenToOddShmooTest(self):

        self.env.SetConfig('EvenToOddLoopback')
        pattern = self._DriveWalkingOneEvenToOddLoopback()

        notCompleted = {}
        for period in self._FindSubCyclePeriodRange():
            periodInNs = period * 1e-12
            found = []
            self.Log('info', 'Testing period = {} '.format(periodInNs))
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                completed, endstate = self.env.RunPattern(slot, slice, pattern, periodInNs)
                self.env.RunCheckers(slot, slice)
                if completed == False:
                    notCompleted[(slot, slice)] = True
                    continue
                if completed == True:
                    notCompleted[(slot, slice)] = False
                    continue
            for (slot, slice) in self.env.fpgas:
                if notCompleted[(slot, slice)] == False:
                    found.append(False)
                if notCompleted[(slot, slice)] == True:
                    found.append(True)
            if all(found):
                self.Log('info', 'Finally, found the right period {}'.format(period))
                # for (slot, slice) in self.env.fpgas:
                #    self.Log('info', 'Dump Capture for Slot {}, Slice {}'.format( slot, slice) )
                #    self.env.DumpCapture(slot, slice)
                # self.env.TakeSnapshot('MiniWalkingOnePeriodsSearch_AllSlot_AllSlice', self.env.fpgas, offsets=0, lengths = len(pattern) )
                break

    def MiniWalkingOneInternalShmooTest(self):

        self.env.SetConfig('InternalLoopback')
        pattern = self._DriveWalkingOneAllPattern()

        notCompleted = {}
        for period in self._FindSubCyclePeriodRange():
            periodInNs = period * 1e-12
            found = []
            self.Log('info', 'Testing period = {} '.format(periodInNs))
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                completed, endstate = self.env.RunPattern(slot, slice, pattern, periodInNs)
                self.env.RunCheckers(slot, slice)
                if completed == False:
                    notCompleted[(slot, slice)] = True
                    continue
                if completed == True:
                    notCompleted[(slot, slice)] = False
                    continue
            for (slot, slice) in self.env.fpgas:
                if notCompleted[(slot, slice)] == False:
                    found.append(False)
                if notCompleted[(slot, slice)] == True:
                    found.append(True)
            if all(found):
                self.Log('info', 'Finally, found the right period {}'.format(period))
                # for (slot, slice) in self.env.fpgas:
                #    self.Log('info', 'Dump Capture for Slot {}, Slice {}'.format( slot, slice) )
                #    self.env.DumpCapture(slot, slice)
                # self.env.TakeSnapshot('MiniVecAddrPeriodsSearch_AllSlot_AllSlice', self.env.fpgas, offsets=0, lengths = len(pattern) )
                break

    def RunInternalLoopbackScenario(self, length):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = length
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            RandomVectors length=REPEATS
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0j11111111111111111111111111111111111111111111111111111111
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)

    def RunWalkingOneScenario(self, length, period, captureState):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = (length - 4) // 28  # 1021

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 1
            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 17,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 18,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 19,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0  # 20,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0  # 21,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0  # 22,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # 23,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # 24,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # 25,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # 26,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # 27,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 28,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 29,
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        offset = 0
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            if captureState == 'FAIL_STATUS':
                self.env.RunPattern(slot, slice, pdata, period, captureType=ac_registers.CaptureControl.FAIL_STATUS)
            elif captureState == 'PIN_STATE':
                self.env.RunPattern(slot, slice, pdata, period, captureType=ac_registers.CaptureControl.PIN_STATE)
            else:
                raise Exception('Un-supported capture state {}'.format(captureState))

            self.env.RunCheckers(slot, slice)
            # self.env.DumpCapture(slot, slice)
            # self.env.TakeSnapshot('WalkingOne_Cal_Slot7_Slice0_1', self.env.fpgas, offsets=0, lengths=len(pdata))

    def _DriveWalkingOneEvenToOddLoopback(self):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 20
        # XZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
        # 0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        # L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
        # H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 1
            %repeat REPEATS                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9, 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 17,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 18,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 19,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0  # 20,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0  # 21,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0  # 22,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # 23,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # 24,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # 25,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # 26,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # 27,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 28,
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 29,
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        return pattern.Generate()

    def _DriveWalkingOneAllPattern(self):
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 20
        # asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
        """)
        return pattern.Generate()

    def MiniWalkingOne2p5NSPinStateLowToHighCKTest(self):
        self.env.SetConfig('LowToHighLoopback')
        period = 2.5e-9

        asm = [None, None]
        pattern = [None, None]

        asm[0] = PatternAssembler()
        asm[0].LoadString("""\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v10000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00100000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00010000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00001000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000100000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000010000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000001000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000100000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000010000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000001000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000100000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000010000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000001000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000100000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000010000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000001000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000100000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000010000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000001000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000100000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000010000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000001000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000100000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000010000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000001000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000100000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000010000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000001000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000100000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000010000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000001000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000100000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000010000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000001000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000100000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000010000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000001000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000100000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000010000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000001000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000100000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000010000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000001000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000100000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000010000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000001000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000100000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000010000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000001000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000100000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000010000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000001000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000010
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000001
            %repeat 200
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
        """)
        pattern[0] = asm[0].Generate()

        asm[1] = PatternAssembler()
        asm[1].LoadString("""\
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %repeat 256
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x89ABCDEF
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        """)
        pattern[1] = asm[1].Generate()

        # Get set of slots
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributes(slot, 0, period,
                                              {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            self.env.SetPeriodAndPEAttributes(slot, 1, period,
                                              {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
            for slice in [0, 1]:
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True,
                                                 False)  # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1]:
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')

            # Run checkers
            self.env.RunMultiSliceCheckers([
                {'slot': slot, 'slice': 0},
                {'slot': slot, 'slice': 1},
            ])

    def MiniPassingEvenToOddVectors100KVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=`100*1024`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period)
            self.env.RunCheckers(slot, slice)

    # make sure pulse can last high for 100k cycles        
    def DirectLongPulseTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            Drive0CompareLVectors length=100
            Drive1CompareHVectors length=100000
            Drive0CompareLVectors length=100
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pdata, period=1.25e-9)
            self.env.RunCheckers(slot, slice)

    def MiniBasicKeepModeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            %var                         keepmode=0b01010000000000000000000000000000000000000000000000000000
            S stype=IOSTATEJAM,              data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 512
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1L0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vHKLKH1H1L0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vHKLKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1L0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1 
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vHKLKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vL0LKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vLKL0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXZL0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1LKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vL0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXZH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vL0HKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXZH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1HKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vL0L0H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXZXZH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXZXZH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXKXZH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            V link=0, ctv=0, mtv=0, lrpt=0,  data=0vXKXKH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1L0H1H1H1H1H1
            %end
            
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            self.env.SetKeepModeAttributes(slot, slice, {'IS_KEEP_MODE': True}, True, [54])
            self.env.SetKeepModeAttributes(slot, slice, {'IS_KEEP_MODE': True}, True, [52])

            self.env.RunPattern(slot, slice, pattern, period, captureType=0, captureFails=False, captureAll=True)

            captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            self.Log('info', 'FPGA captureCount is {} while expected is {}'.format(captureCount, 3))
            # print(captureCount)
            # for ch in range(56):
            #    ChannelFail = hpcc.ac[slice].ReadChannel(ch,'ChannelFailCount').Pack()
            #    print('ch {} is # {}'.format(ch, ChannelFail) )
            self.env.RunCheckers(slot, slice)
            # self.env.DumpCapture(slot, slice)
            self.env.SetKeepModeAttributes(slot, slice, {'IS_KEEP_MODE': False}, True, [54])
            self.env.SetKeepModeAttributes(slot, slice, {'IS_KEEP_MODE': False}, True, [52])

    def MiniKeepModeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9
        asm = PatternAssembler()
        asm.LoadString("""\
                SUBROUTINE0:
                        %var                        keepmode=0b10000000000000000000000000000000000000000000000000000000
                        %repeat 512
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vK1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                        %end
                        I optype=BRANCH, br=RET                                  
                PATTERN0:
                        %var                        keepmode=0b10000000000000000000000000000000000000000000000000000000
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0 
                        %repeat 512
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                        %end
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vK1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vK1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1
                        I optype=BRANCH, base=PFB, br=CALL_I, imm=eval[int32(SUBROUTINE0-PATTERN0)], cond=COND.NONE, invcond=0
                        I optype=BRANCH, br=RET                                  
                PATTERN_START:
                    I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm=3790531420
                    I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=0
                    I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`PATTERN0`
                PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            hpcc.ac[slice].Write('TotalFailCount', 0)
            for ch in range(56):
                hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
            self.env.SetKeepModeAttributes(slot, slice, {'IS_KEEP_MODE': True}, True, [55])
            self.env.RunPattern(slot, slice, pattern, period, start=asm.Resolve('eval[PATTERN_START]'),
                                captureAddress=hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern)),
                                captureType=0, captureFails=True, captureAll=True)

            captureCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            self.Log('info', 'FPGA captureCount is {} while expected is {}'.format(captureCount, 515))
            self.env.RunCheckers(slot, slice)
            # self.env.DumpCapture(slot, slice)
            self.env.SetKeepModeAttributes(slot, slice, {'IS_KEEP_MODE': False}, True, [55])


# FlatPattern.MiniRandomFastPeriodTest
class FlatPattern(HpccTest):

    def MiniDrive0CompareLTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 100e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 1024
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            # self.env.EnableIlas(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period, captureType=0, captureFails=True, captureCTVs=False,
                                captureAll=False)
            upper = hpcc.ac[slice].Read('PinInputStatusUpper').Pack()
            lower = hpcc.ac[slice].Read('PinInputStatusLower').Pack()
            pinStatus = upper << 32 | lower
            if pinStatus != 0b00000000000000000000000000000000000000000000000000000000:
                self.Log('error', 'Unexpected pin status = 0b{:056b}'.format(pinStatus))
            else:
                self.Log('debug', 'Expected pin status = 0b{:056b}'.format(pinStatus))
            self.env.RunCheckers(slot, slice)

    def MiniFixedDrive0CompareLTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 100e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 1024
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            # self.env.EnableIlas(slot, slice)

            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice,
                                             {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True,
                                             'ODD_PINS')
            self.env.SetPEAttributesDCandSim(slot, slice,
                                             {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2,
                                              'FIXED_DRIVE_STATE': 'VIL'}, True, 'EVEN_PINS')

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pattern, captureType=0, captureFails=True,
                                                        captureCTVs=False, captureAll=False)
            upper = hpcc.ac[slice].Read('PinInputStatusUpper').Pack()
            lower = hpcc.ac[slice].Read('PinInputStatusLower').Pack()
            pinStatus = upper << 32 | lower
            if pinStatus != 0b00000000000000000000000000000000000000000000000000000000:
                self.Log('error', 'Unexpected pin status = 0b{:056b}'.format(pinStatus))
            else:
                self.Log('debug', 'Expected pin status = 0b{:056b}'.format(pinStatus))
            self.env.RunCheckers(slot, slice)

    def MiniDrive1CompareHTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 100e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 1024
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            # self.env.EnableIlas(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period, captureType=0, captureFails=True, captureCTVs=False,
                                captureAll=False)
            upper = hpcc.ac[slice].Read('PinInputStatusUpper').Pack()
            lower = hpcc.ac[slice].Read('PinInputStatusLower').Pack()
            pinStatus = upper << 32 | lower
            if pinStatus != 0b11111111111111111111111111111111111111111111111111111111:
                self.Log('error', 'Unexpected pin status = 0b{:056b}'.format(pinStatus))
            else:
                self.Log('debug', 'Expected pin status = 0b{:056b}'.format(pinStatus))
            self.env.RunCheckers(slot, slice)

    def MiniFixedDrive1CompareHTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 100e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 1024
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            # self.env.EnableIlas(slot, slice)

            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice,
                                             {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True,
                                             'ODD_PINS')
            self.env.SetPEAttributesDCandSim(slot, slice,
                                             {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2,
                                              'FIXED_DRIVE_STATE': 'VIH'}, True, 'EVEN_PINS')

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pattern, captureType=0, captureFails=True,
                                                        captureCTVs=False, captureAll=False)
            upper = hpcc.ac[slice].Read('PinInputStatusUpper').Pack()
            lower = hpcc.ac[slice].Read('PinInputStatusLower').Pack()
            pinStatus = upper << 32 | lower
            if pinStatus != 0b11111111111111111111111111111111111111111111111111111111:
                self.Log('error', 'Unexpected pin status = 0b{:056b}'.format(pinStatus))
            else:
                self.Log('debug', 'Expected pin status = 0b{:056b}'.format(pinStatus))
            self.env.RunCheckers(slot, slice)

    def MiniRandomSlowPeriodTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddNoSwitchVectors length=`100*1024`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            period = random.uniform(50e-9, hpcc.ac[slice].maxPeriod / PICO_SECONDS_PER_SECOND)
            self.Log('info', 'Testing period = {}'.format(period))
            self.env.RunPattern(slot, slice, pattern, period)
            self.env.RunCheckers(slot, slice)

    def MiniRandomFastPeriodTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddNoSwitchVectors length=`100*1024`
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        period = random.uniform(2.5e-9, 10e-9)
        self.Log('info', 'Testing period = {}'.format(period))
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period)
            self.env.RunCheckers(slot, slice)

    def MiniPassingEvenToOddVectorsWOSwitch100KVTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=`100*1024`, simple=1
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period, captureType=0, captureFails=True, captureCTVs=False,
                                captureAll=False)
            self.env.RunCheckers(slot, slice)

    def _FindSubCyclePeriodRange(self):
        overlappingValue = 100
        subCycleOne = 2
        subCycleTwo = 4
        interval = 65
        periodRangeDict = {}
        oneSlicePeriodRangeList = None
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            hpcc.ac[slice]._UpdateFpgaCapabilities()
            subCycleOneLowerBound = hpcc.ac[slice].minQdrMode * subCycleOne
            subCycleOneUpperBound = hpcc.ac[slice].minQdrMode * subCycleTwo
            print(subCycleOneLowerBound)
            subCycleInterval = int((subCycleOneUpperBound - subCycleOneLowerBound) / interval)
            periodRangeDict[slot, slice] = range(subCycleOneLowerBound, subCycleOneUpperBound + overlappingValue,
                                                 subCycleInterval)
            oneSlicePeriodRangeList = periodRangeDict[slot, slice]

        return oneSlicePeriodRangeList

    def xMiniVectorAddrFreqSearchTest(self):

        vectorSize = 10 * 1024
        self.env.SetConfig('InternalLoopback')
        vecAddr = ''
        for i in range(vectorSize):
            vecAddr += 'V link=0, ctv=0, mtv=0, lrpt=0, data=0v{:024b}{:032b}\n'.format(i, i)
        source = """\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            {}
            StopPattern 0x12345678
        """.format(vecAddr)

        asm = PatternAssembler()
        asm.LoadString(source)
        pattern = asm.Generate()

        hangingPeriod = {}
        firstHang = {}
        found = {}
        # for peri in range(1600, 3300, 25 ):
        for peri in self._FindSubCyclePeriodRange():
            period = peri * 1e-12  # random.uniform(2.5e-9, 25e-9)
            self.Log('info', 'Testing period = {} '.format(period))
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                completed, endstate = self.env.RunPattern(slot, slice, pattern, period)
                self.env.RunCheckers(slot, slice)
                if completed == False:
                    if not firstHang.setdefault((slot, slice), False):
                        hangingPeriod[(slot, slice)] = period
                        firstHang[slot, slice] = True
                    continue

            for (slot, slice) in self.env.fpgas:
                if firstHang.setdefault((slot, slice), False) == False:
                    found[(slot, slice)] = False
                elif firstHang[(slot, slice)] == True:
                    found[(slot, slice)] = True

            # if sum( list( found.values() ) ) == 3:
            #    break
            if all(val for val in found.values()):
                break

        if all(val for val in found.values()):
            # if sum( list( found.values() ) ) == 3:
            for key, value in hangingPeriod.items():
                self.Log('info', 'Hanging period is {} for Slot {} slice {} '.format(value, key[0], key[1]))
                # print(key, value)
                self.env.LogPerSlice(key[0], key[1])
                completed, endstate = self.env.RunPattern(key[0], key[1], pattern=pattern, period=value)
                self.env.RunCheckers(key[0], key[1])
                # self.env.DumpCapture(key[0], key[1])

    def MiniVectorAddrTest(self):

        vectorSize = 10 * 1024
        self.env.SetConfig('InternalLoopback')
        vecAddr = ''
        for i in range(vectorSize):
            vecAddr += 'V link=0, ctv=0, mtv=0, lrpt=0, data=0v{:024b}{:032b}\n'.format(i, i)
        source = """\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            {}
            StopPattern 0x12345678
        """.format(vecAddr)

        asm = PatternAssembler()
        asm.LoadString(source)
        pattern = asm.Generate()

        for i in range(10):
            period0 = random.uniform(2.5e-9, 25e-9)
            period1 = random.uniform(2.5e-9, 25e-9)
            self.Log('info', 'Testing period = {} for slice 0'.format(period0))
            self.Log('info', 'Testing period = {} for slice 1'.format(period1))
            for (slot, slice) in self.env.fpgas:
                if slice == 0:
                    self.env.LogPerSlice(slot, slice)
                    completed, endstate = self.env.RunPattern(slot, slice, pattern, period0)
                    self.env.RunCheckers(slot, slice)
                    if completed == False:
                        continue

                if slice == 1:
                    self.env.LogPerSlice(slot, slice)
                    completed, endstate = self.env.RunPattern(slot, slice, pattern, period1)
                    self.env.RunCheckers(slot, slice)
                    if completed == False:
                        continue
                        # self.env.TakeSnapshot('MiniVectorAddr_Slot7_slice0_1', [(7,0), (7, 1)], offsets=0, lengths = len(pattern) )

    def MiniVecAddrPeriodSerachTest(self):

        vectorSize = 10 * 1024
        self.env.SetConfig('InternalLoopback')
        vecAddr = ''
        for i in range(vectorSize):
            vecAddr += 'V link=0, ctv=0, mtv=0, lrpt=0, data=0v{:024b}{:032b}\n'.format(i, i)
        source = """\
            S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            {}
            StopPattern 0x12345678
        """.format(vecAddr)
        asm = PatternAssembler()
        asm.LoadString(source)
        pattern = asm.Generate()

        notCompleted = {}
        for period in self._FindSubCyclePeriodRange():
            periodInNs = period * 1e-12
            found = []
            self.Log('info', 'Testing period = {} '.format(periodInNs))
            for (slot, slice) in self.env.fpgas:
                self.env.LogPerSlice(slot, slice)
                completed, endstate = self.env.RunPattern(slot, slice, pattern, periodInNs)  # , numberOfTries=1000)
                self.env.RunCheckers(slot, slice)
                if completed == False:
                    notCompleted[(slot, slice)] = True
                    continue
                if completed == True:
                    notCompleted[(slot, slice)] = False
                    continue
            for (slot, slice) in self.env.fpgas:
                if notCompleted[(slot, slice)] == False:
                    found.append(False)
                if notCompleted[(slot, slice)] == True:
                    found.append(True)
            if all(found):
                self.Log('info', 'Finally, found the right period {}'.format(period))
                # for (slot, slice) in self.env.fpgas:
                #    self.Log('info', 'Dump Capture for Slot {}, Slice {}'.format( slot, slice) )
                #    self.env.DumpCapture(slot, slice)
                # self.env.TakeSnapshot('MiniVecAddrPeriodsSearch_AllSlot_AllSlice', self.env.fpgas, offsets=0, lengths = len(pattern) )
                continue

    def MiniRandomPeriodSerachTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=`20*1024`, simple=1
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()

        notCompleted = {}
        for period in self._FindSubCyclePeriodRange():
            found = []
            self.Log('info', 'Testing period = {} '.format(period * 1e-12))
            for (slot, slice) in self.env.fpgas:
                # hpcc = self.env.instruments[slot]
                self.env.LogPerSlice(slot, slice)
                completed, endstate = self.env.RunPattern(slot, slice, pattern, period * 1e-12)
                self.env.RunCheckers(slot, slice)
                if completed == False:
                    notCompleted[(slot, slice)] = True
                    # blockCount = hpcc.ac[slice].Read('TotalCaptureBlockCount').Pack()  # in blocks
                    # captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()  # in vectors
                    # self.Log('info', 'Block Count is {}, Capture Count is {}'.format(blockCount, captureCount ) )
                    continue
                if completed == True:
                    notCompleted[(slot, slice)] = False
                    continue
            for (slot, slice) in self.env.fpgas:
                if notCompleted[(slot, slice)] == False:
                    found.append(False)
                if notCompleted[(slot, slice)] == True:
                    found.append(True)
            if all(found):
                self.Log('info', 'Finally, found the right period {}'.format(period))
                # self.env.TakeSnapshot('MiniRandomPeriodsSearch_AllSlot_AllSlice', self.env.fpgas, offsets=0, lengths = len(pattern) )
                break

    def MiniRandomPeriodsTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddVectors length=`25*1024`, simple=1
            StopPattern 0x12345678
        """)
        pattern = asm.Generate()
        for i in range(10):
            period0 = random.uniform(2.5e-9, 25e-9)
            period1 = random.uniform(2.5e-9, 25e-9)
            self.Log('info', 'Testing period = {} for slice 0'.format(period0))
            self.Log('info', 'Testing period = {} for slice 1'.format(period1))
            for (slot, slice) in self.env.fpgas:
                if slice == 0:
                    self.env.LogPerSlice(slot, slice)
                    completed, endstate = self.env.RunPattern(slot, slice, pattern, period0)
                    self.env.RunCheckers(slot, slice)
                    if completed == False:
                        continue

                if slice == 1:
                    self.env.LogPerSlice(slot, slice)
                    completed, endstate = self.env.RunPattern(slot, slice, pattern, period1)
                    self.env.RunCheckers(slot, slice)
                    if completed == False:
                        continue
                        # self.env.TakeSnapshot('MiniRandomPeriods_Slot7_slice0_1', [(7,0), (7, 1)], offsets=0, lengths = len(pattern) )


class External(HpccTest):

    def DirectedDeskewCalibrationPatternTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        vectors = []
        # Following are the Pattern Vector encodings:
        # startEncoding for Instruction to Set all pins in Compare mode                                                          = 0x8000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        # repeatEncoding for Instruction to repeat the following data vector 20k - 252 = 19748 times                             = 0xc0000000000000210800000000004D24
        # highEncoding for Data vector to set all pins to High state                                                             = 0x0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        # stopEncoding for Instruction to stop pattern and write 'face' to a register to indicate the stop encoding got executed = 0xc000000000000021100000000000face
        startEncoding = 'R data=0x{:x}'.format(0x8000ffffffffffffffffffffffffffff)
        compareHighEncoding = 'R data=0x{:x}'.format(0x0000ffffffffffffffffffffffffffff)
        repeatEncoding = 'R data=0x{:x}'.format(0xc0000000000000210800000000004D24)
        stopEncoding = 'R data=0x{:x}'.format(0xc000000000000021100000000000face)
        vectors.append(startEncoding)
        for i in range(252):
            vectors.append(compareHighEncoding)
        vectors.append(repeatEncoding)
        vectors.append(compareHighEncoding)
        vectors.append(stopEncoding)
        asm.LoadString('\n'.join(vectors))
        pattern = asm.Generate()
        # asm.SaveObj('DeskewCalibration.obj')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            for period in [2.5e-9, 4e-9, 8e-9, 16e-9, 32e-9, 64e-9]:
                self.env.RunPattern(slot, slice, pattern, period=period)
                self.env.RunCheckers(slot, slice)


class Triggers(HpccTest):

    def TriggerHpccToRc(self, slot, slice, value):
        expected = value
        hpcc = self.env.instruments[slot]
        msg = ''
        hpcc.ac[slice].Write('TriggerEvent', expected)
        cnt = 0
        for j in range(0, 100):
            cnt += 1
        self.Log('debug', f'HPCC({slot}, {slice}) sent trigger 0x{expected:08X} to RC')
        dc_trigger = hpcc.dc[slice].Read('UpTrigger').Pack()
        self.Log('info', f'DC UpTrigger 0x{dc_trigger:x}')

        rc_trigger = self.env.rc.read_trigger_up(slot)
        msg = f'DC to RC trigger up (expected, actual): ' \
              f'0x{expected:08X}, 0x{rc_trigger:08X}'

        if rc_trigger != expected:
            self.Log('error', msg)
        else:
            self.Log('debug', msg)

    def TriggerRcToHpcc(self, slot, slice, value):
        expected = value
        hpcc = self.env.instruments[slot]
        msg = ''

        self.env.rc.send_trigger(expected, slot)
        self.Log('debug', 'RC sent trigger 0x{:x}'.format(expected))

        cnt = 0
        for j in range(0, 100):
            cnt += 1
        dc_trigger = hpcc.dc[slice].Read('DownTrigger').Pack()
        self.Log('info', f'DC DownTrigger 0x{dc_trigger:08X}')

        # Try to read the trigger from the HPCC side
        triggerValue = hpcc.ac[slice].Read('TriggerEvent').Pack()
        msg = f'HPPC_AC({slot}, {slice}) expected, actual: 0x{expected:08X},' \
              f' 0x{triggerValue:08X}'
        if triggerValue != expected:
            self.Log('error', msg)
        else:
            self.Log('debug', msg)

    def MiniManualTransmitTest(self):
        values = [0x08FFFFFF, 0x09123aaa, 0x0B456cca, 0x08AAAAAA, 0x09BBBBBB, 0x0BCCCCCC]
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)

            self.Log('info', f'Checking Trigger Status before test...')
            validate_aurora_statuses(slot, enable_log=True)

            for value in values:
                self.TriggerHpccToRc(slot, slice, value)

            self.Log('info', f'Checking Trigger Status after test...')
            validate_aurora_statuses(slot, enable_log=True)

    def MiniManualReceiveTest(self):
        values = [0x00398abc, 0x02456bbb, 0x03123CCC, 0x00AAAAAA, 0x02BBBBBB, 0x03CCCCCC]
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)

            self.Log('info', f'Checking Trigger Status before test...')
            validate_aurora_statuses(slot, enable_log=True)

            for value in values:
                self.TriggerRcToHpcc(slot, slice, value)

            self.Log('info', f'Checking Trigger Status after test...')
            validate_aurora_statuses(slot, enable_log=True)


    def DirectedClockCompensationExhaustiveTriggerTest(self):
        if configs.CONFIGURABLE_AURORA_CLOCK_COMPENSATION == True:
            for (slot, slice) in self.env.fpgas:
                clock_compensation_capability = self.env.rc.read_clock_compensation_capability()
                if clock_compensation_capability == 1:
                    hpcc = self.read_tester_status_before_test(slot)
                    self.send_random_triggers(hpcc, slot, slice)
                    self.disable_clock_compensation()
                    self.reset_aurora_link(hpcc)
                    self.send_random_triggers(hpcc, slot, slice)
                    self.enable_clock_compensation()
                    self.reset_aurora_link(hpcc)
                    self.send_random_triggers(hpcc,slot, slice)
                else:
                    self.Log('info', 'Skipping the test as Clock compensation is not supported by RC FPGA')
        else:
            self.Log('info', 'Skipping the test as Clock Compensation is not enabled in config file.')

    def read_tester_status_before_test(self,slot):
        self.env.rc.read_clock_compensation_status()
        hpcc = self.env.instruments[slot]
        hpcc.clock_compensation_register_status()
        return hpcc

    def send_random_triggers(self, hpcc, slot, slice):
        cnt = 0
        for trigger_sent_count in range(0, 10000):
            value = 0x08000000
            while ((value & 0xFC000000)):
                value = random.getrandbits(32) & 0xFFF7FFFF

            hpcc.ac[slice].Write('TriggerEvent', value)
            self.Log('debug', 'HPCC({}, {}) sent trigger 0x{:x} to DC'.format(slot, slice, value))
            # to create delay between sending trigger and reading results
            for j in range(0, 100):
                cnt = cnt + 1

            trigger_dc_up = hpcc.dc[0].Read('UpTrigger').Pack()
            if trigger_dc_up != value:
                self.Log('error',
                         '(AC->DC)DC did not received correct trigger from AC: expected=0x{:x} actual=0x{:x}'.format(
                             value, trigger_dc_up))
            else:
                self.Log('debug', '(AC->DC)DC received trigger from AC 0x{:x}'.format(trigger_dc_up))

            trigger_rc_up = self.env.rc.read_trigger_up(slot)
            if trigger_rc_up != value:
                self.Log('error',
                         '(DC->RC)RC received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(value,
                                                                                                        trigger_rc_up))
            else:
                self.Log('debug', '(DC->RC)RC received trigger from DC 0x{:x}'.format(trigger_rc_up))

            if hpcc.using_rc2():
                trigger_rc_down = self.env.rc.read_trigger_down(slot)
                if trigger_rc_down != value:
                    self.Log('error',
                             '(RC->RC)RC did not sent correct trigger to DC: expected=0x{:x} actual=0x{:x}'.format(value,
                                                                                                           trigger_rc_down))
                else:
                    self.Log('debug', '(RC->RC)RC sent trigger downstream 0x{:x}'.format(trigger_rc_down))

            trigger_dc_down = hpcc.dc[0].Read('DownTrigger').Pack()
            if trigger_dc_down != value:
                self.Log('error',
                         '(RC->DC)DC did not received correct trigger from RC: expected=0x{:x} actual=0x{:x}'.format(
                             value, trigger_dc_down))
            else:
                self.Log('debug', '(RC->DC)DC received trigger from RC 0x{:x}'.format(trigger_dc_down))

            trigger_rc_rceived = hpcc.ac[slice].Read('TriggerEvent').Pack()
            if trigger_rc_rceived != value:
                self.Log('error',
                         '(DC->AC)HPCC received trigger 0x{:x}, actual=0x{:x}'.format(value, trigger_rc_rceived))
            else:
                self.Log('debug', '(DC->AC)AC received trigger from DC 0x{:x}'.format(trigger_rc_rceived))

    def disable_clock_compensation(self):
        self.change_clock_compensation_status('Disable')

    def change_clock_compensation_status(self, status):
        tester_status = cards_in_tester()
        for device in tester_status.devices:
            self.Log('info', "Current device in device list {}".format(device.name()))
            try:
                if device.name() == 'Hpcc':
                    self.Log('info', "{}ing Clock compensation for HPCC at slot {}".format(status, device.slot))
                    if status == 'Disable':
                        device.disable_dc_fpga_clock_compensation()
                    else:
                        device.enable_dc_fpga_clock_compensation()
                elif device.name() == 'Hddps':
                    self.Log('warning', "{}ing Clock compensation feature for hddps not enabled yet ".format(status))
                elif device.name() == 'Tdaubnk':
                    self.Log('info', "{}ing Clock compensation for TDAUBANK at slot {}".format(status, device.slot))
                    if status == 'Disable':
                        device.disable_tdaubank_fpga_clock_compensation()
                    else:
                        device.enable_tdaubank_fpga_clock_compensation()
                elif device.name() == 'Rc2':
                    if status == 'Disable':
                        self.Log('info', '*********Disabling Clock Compensation from RC ***********')
                        self.env.rc.disable_clock_compensation()
                    else:
                        self.Log('info', '*********Enabling Clock Compensation from RC ***********')
                        self.env.rc.enable_clock_compensation()
                else:
                    self.Log('info', "{}ing Clock compensation not supported for {}".format(status, device.name()))
            except:
                self.Log('info','unable to change Clock compensation for Instrument {} on slot {}'.format(device.name(), device.slot))

    def enable_clock_compensation(self):
        self.change_clock_compensation_status('Enable')

    def reset_aurora_link(self, hpcc):
        tester_status = cards_in_tester()
        for device in tester_status.devices:
            if device.name() == 'Hpcc':
                self.Log('info', "Resetting aurora for device {} in slot {}".format(device.name(), device.slot))
                hpcc.dc[0].Write('Resets',0x800)
                hpcc.dc[0].Write('Resets',0x400)
                hpcc.dc[0].Write('Resets',0x200)
        self.ResetRCAuroraBus()

    def ResetRCAuroraBus(self):
        for slot in range(12):
            self.env.rc.reset_trigger_interface(slot)
            time.sleep(0.05)

        data = self.env.rc.Read('Reset')
        data.AuroraResetCards0_3 = 1
        self.env.rc.Write('Reset', data)
        self.env.rc.wait_for_reset_to_clear()

        data = self.env.rc.Read('Reset')
        data.AuroraResetCards4_7 = 1
        self.env.rc.Write('Reset', data)
        self.env.rc.wait_for_reset_to_clear()

        data = self.env.rc.Read('Reset')
        data.AuroraResetCards8_11 = 1
        self.env.rc.Write('Reset', data)
        self.env.rc.wait_for_reset_to_clear()
        slotUp = self.env.rc.log_aurora_status()

    def DirectedTriggerExhaustiveInFlowTest(self):
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            fail_cnt_at_DC_UP = 0
            fail_cnt_at_RC_UP = 0
            fail_cnt_at_RC_Down = 0
            fail_cnt_at_DC_Down = 0
            fail_cnt_at_AC_Down = 0
            cnt = 0            
            for i in range(0,100000):
                value = 0x08000000
                while ((value & 0xFC000000)):
                    value = random.getrandbits(32) & 0xFFF7FFFF
                
                expected = value
                hpcc = self.env.instruments[slot]
                hpcc.ac[slice].Write('TriggerEvent', expected)
                self.Log('debug','HPCC({}, {}) sent trigger 0x{:x} to DC'.format(slot, slice, expected))
                #to create delay between sending trigger and reading results
                for j in range(0,100):
                    cnt = cnt + 1
                
                triggerdcup = hpcc.dc[0].Read('UpTrigger').Pack()
                if triggerdcup != expected:
                    self.Log('error','(AC->DC)DC did not received correct trigger from AC: expected=0x{:x} actual=0x{:x}'.format(expected,triggerdcup))
                    fail_cnt_at_DC_UP = fail_cnt_at_DC_UP + 1
                    triggererrer = hpcc.dc[0].Read('TriggerErrors').Pack()
                    self.Log('error','(AC->DC) DC FPGA Trigger error register status : {} '.format(triggererrer))
                    self.Log('info','Aurora interface status for (AC->DC)')
                    self.env.rc.log_aurora_status()
                else:
                    self.Log('debug','(AC->DC)DC received trigger from AC 0x{:x}'.format(triggerdcup))
                
                triggerrcup = self.env.rc.read_trigger_up(slot)
                if triggerrcup != expected:
                     self.Log('error','(DC->RC)RC received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected,triggerrcup))
                     fail_cnt_at_RC_UP = fail_cnt_at_RC_UP + 1
                     self.Log('info','Aurora interface status for (DC->RC)')
                     self.env.rc.log_aurora_status()
                else:
                     self.Log('debug', '(DC->RC)RC received trigger from DC 0x{:x}'.format(triggerrcup))

                if self.env.rc.name().upper() == 'RC2':
                    triggerrcdown = self.env.rc.read_trigger_down(slot)
                    if triggerrcdown != expected:
                        self.Log('error', 'RC did not sent correct trigger to DC: expected=0x{:x} actual=0x{:x}'.format(expected,triggerrcdown))
                        fail_cnt_at_RC_Down = fail_cnt_at_RC_Down + 1
                    else:
                        self.Log('debug', '(RC->RC)RC sent trigger downstream 0x{:x}'.format(triggerrcdown))
                    
                triggerdcdown = hpcc.dc[0].Read('DownTrigger').Pack()
                if triggerdcdown != expected:
                    self.Log('error', '(RC->DC)DC did not received correct trigger from RC: expected=0x{:x} actual=0x{:x}'.format(expected,triggerdcdown))
                    fail_cnt_at_DC_Down = fail_cnt_at_DC_Down + 1
                    triggererrer = hpcc.dc[0].Read('TriggerErrors').Pack()
                    self.Log('error','(RC->DC) DC FPGA Trigger error register status : {} '.format(triggererrer))
                    self.Log('info','Aurora interface status for (RC->DC)')
                    self.env.rc.log_aurora_status()
                else:
                    self.Log('debug', '(RC->DC)DC received trigger from RC 0x{:x}'.format(triggerdcdown))
                
                TriggerReceived = hpcc.ac[slice].Read('TriggerEvent').Pack()
                if TriggerReceived != expected:
                    self.Log('error', '(DC->AC)HPCC received trigger 0x{:x}, actual=0x{:x}'.format(expected,TriggerReceived))
                    fail_cnt_at_AC_Down = fail_cnt_at_AC_Down + 1
                    self.Log('info','Aurora interface status for (DC->AC)')
                    self.env.rc.log_aurora_status()
                else:
                    self.Log('debug', '(DC->AC)AC received trigger from DC 0x{:x}'.format(TriggerReceived))               
            
                if fail_cnt_at_DC_UP == 10 or fail_cnt_at_RC_UP == 10 or fail_cnt_at_RC_Down == 10 or  fail_cnt_at_DC_Down == 10 or fail_cnt_at_AC_Down == 10:
                    self.Log('info', 'Slice: {} AC->DC: {} DC->RC: {} RCup->RCDown: {} RC->DC: {} DC->AC: {}'.format(slice,fail_cnt_at_DC_UP,fail_cnt_at_RC_UP,fail_cnt_at_RC_Down,fail_cnt_at_DC_Down,fail_cnt_at_AC_Down))
                    break
            
            self.Log('info', 'Slice: {} AC->DC: {} DC->RC: {} RCup->RCDown: {} RC->DC: {} DC->AC: {}'.format(slice,fail_cnt_at_DC_UP,fail_cnt_at_RC_UP,fail_cnt_at_RC_Down,fail_cnt_at_DC_Down,fail_cnt_at_AC_Down))




class Timing(HpccTest):

    def DumpDelayValue(self):

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            capabilityReg = hpcc.ac[slice].Read('Capability0')

            self.driveOutDelay = capabilityReg.DriveOutDelay
            self.compareInDelay = capabilityReg.CompareInDelay
            print("self.driveoutdelay: {}".format(self.driveOutDelay))
            print("self.compareInDelay: {}".format(self.compareInDelay))

    def SchmooDelayValue(self):
        self.env.SetConfig('EvenToOddLoopback')
        configMode = 'odd'
        # shmooOutput = open('shmooDelay.csv', 'w')
        # estimate[i] =
        driveRange = range(20, 100, 10)
        compareRange = range(20, 100, 10)
        csvLists = {}
        for (slot, slice) in self.env.fpgas:
            # self.env.LogPerSlice(slot, slice)
            csvLists.setdefault((slot, slice), [])
            self._CreatePinShmooCSV(slot, slice, csvLists, configMode)
            hpcc = self.env.instruments[slot]
            # for i in range(10):
            # for dr in driveRange:
            # channelData = {}
            # self._ChannelInitializer(channelData, configMode)
            # for comp in compareRange:
            pattern1 = PatternAssembler()
            pattern1.LoadString("""\
                PATTERN_START:                                                                                   
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
                %end
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1  
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            """)
            pattern = pattern1.Generate()

            # self.env.SetSelectableFailMask(slot, slice, 1, 0)
            self.env.WritePattern(slot, slice, 0, pattern)
            # hpcc.ac[slice].captureAll = False
            # hpcc.ac[slice].captureFails = True
            # hpcc.ac[slice].SetupCapture(0x20000, ac_registers.CaptureControl.PIN_STATE)


            hpcc.ac[slice].SetCaptureBaseAddress(0x20000)
            hpcc.ac[slice].SetCaptureCounts()
            # captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail
            hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, True, True, False, False)
            # print('test1')
            for dr in driveRange:
                channelData = {}
                self._ChannelInitializer(channelData, configMode)
                for comp in compareRange:
                    hpcc.ac[slice]._UpdateFpgaCapabilities()
                    hpcc.ac[slice].driveOutDelay = dr
                    # print(dr, comp)
                    hpcc.ac[slice].compareInDelay = comp
                    # print("---")
                    period = hpcc.ac[slice].minQdrMode * 5 / PICO_SECONDS_PER_SECOND
                    hpcc.ac[slice].SetPeriod(period)
                    # set initial timing for all pins
                    hpcc.dc[slice].SetPEAttributes({'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0}, True)
                    # print('test5')
                    completed = hpcc.ac[slice].ExecutePattern()
                    if not completed:
                        self.Log('error', 'Pattern execution did not complete')

                    # print('test4')
                    hpcc.DumpCapture(slice)
                    estimate = self.GetVectorCycleFromCapture(hpcc, slice)
                    # print(estimate)
                    # self.FineSearchForEachPin(hpcc, slot, slice, file, startChannel, estimate)
                    # channel[i]
                    # shmooOutput.write('DriveOutDelay_{},{}'.format(i, estimate[
                    self._ChannelDataAppend(channelData, configMode, estimate)
                    # print(channelData)
                # print(csvLists)
                self._CSVDataWrite(slot, slice, csvLists, configMode, dr, channelData)
                hpcc.ac[slice].captureAll = True
            self._ClosePinShmooCSV(slot, slice, csvLists, configMode)

    def GetVectorCycleFromCapture(self, hpcc, slice):
        estimate = dict()
        captureData = hpcc.ac[slice].ReadCapture()
        period = hpcc.ac[slice].minQdrMode / PICO_SECONDS_PER_SECOND
        for block in hpcc.ac[slice].UnpackCapture(captureData):
            header, data = block
            for i in range(8):
                if header.CapturedVectorsValidFlags[i] == 1:
                    actual = data[i].PinLaneData
                    vaddr = data[i].VectorAddress
                    actualString = '{:056b}'.format(actual)
                    vectorString = ' '.join(actualString[i:i + 2] for i in range(0, len(actualString), 2))
                    # print(str(vaddr) + "    " + vectorString)
                    if vaddr > 101:
                        # print(str(vaddr) + "    " + vectorString)
                        for i in range(56):
                            # sometimes two vectors may fail, use the first one
                            if (actual & 1 == 1) and (i not in estimate):
                                estimate[i] = vaddr

                            actual = actual >> 1
        return estimate

    def _CSVDataWrite(self, slot, slice, csvList, evenOrOddChannel, drive, channelData):
        if evenOrOddChannel == 'odd':
            startingChannel = 1
        else:
            startingChannel = 0
        # print(channelData)
        i = 0
        for pinIndex in range(startingChannel, 56, 2):
            # print(channelData[pinIndex])
            csvList[(slot, slice)][i].write(
                'Drive_{}, {}\n'.format(drive, ','.join(str(x) for x in channelData[pinIndex])))
            i += 1
            # print(i)

    def _ChannelDataAppend(self, chDict, evenOrOddChannel, failCapture):
        if evenOrOddChannel == 'odd':
            startingChannel = 1
        else:
            startingChannel = 0

        for pinIndex in range(startingChannel, 56, 2):
            chDict[pinIndex].append(failCapture.get(pinIndex, 0))

    def _ChannelInitializer(self, chDict, evenOrOddChannel):
        if evenOrOddChannel == 'odd':
            startingChannel = 1
        else:
            startingChannel = 0

        for pinIndex in range(startingChannel, 56, 2):
            chDict[pinIndex] = []

    # def _
    def _CreatePinShmooCSV(self, slot, slice, csvList, evenOrOddChannel):
        if evenOrOddChannel == 'odd':
            startingChannel = 1
        else:
            startingChannel = 0

        for pinIndex in range(startingChannel, 56, 2):
            csvList[slot, slice].append(open('Slot_{}_Slice{}_pin{}_shmoo.csv'.format(slot, slice, pinIndex), 'w'))

    def _ClosePinShmooCSV(self, slot, slice, csvList, evenOrOddChannel):
        if evenOrOddChannel == 'odd':
            startingChannel = 1
        else:
            startingChannel = 0
        i = 0
        for pinIndex in range(startingChannel, 56, 2):
            csvList[slot, slice][i].close()
            i += 1

    def AverageReturn(self, estimate):
        vectorCycle = estimate.values()
        if all([i > 101 for i in vectorCycle]):
            return sum(vectorCycle) / float(len(vectorCycle))
        else:
            return 0

    def _DrivePulseOneAllPattern(self):
        pattern = PatternAssembler()
        # pattern.symbols['REPEATS'] = 1*ratio
        # asm.symbols['CONDITION'] = condition

        pattern.LoadString("""\
                PATTERN_START:                                                                                   
                S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                %end
                V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
                %repeat 100                                                                               
                V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            """)
        return pattern.Generate()

    def _CaptPulseOneAllPattern(self):

        pattern = PatternAssembler()
        # pattern.symbols['REPEATS'] = 1*ratio
        pattern.symbols['LENGTH'] = 201 * 20  # *ratio

        pattern.LoadString("""\
            PATTERN_START:                                                                                  # 0
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %repeat LENGTH
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            StopPattern 0x89ABCDEF
        """)
        return pattern.Generate()

    def MiniMultiTimeCapabilityCompareMeasurementTest(self):
        self.env.SetConfig('LowToHighLoopback')
        # period = 10e-9
        # hpcc = self.env.instruments[8]
        drivePeriod = 50e-9
        slowSamplePeriod = 25e-9
        fastSamplePeriod = 2.5e-9
        period1 = 10e-9
        period2 = 2.5e-9
        # period1 = hpcc.ac[0].minQdrMode * 52 / PICO_SECONDS_PER_SECOND
        # period2 = hpcc.ac[1].minQdrMode * 2 / PICO_SECONDS_PER_SECOND

        # ratio = 2
        # periods = [ period*ratio, period ]
        periods = [period1, period2]
        pattern = [self._DrivePulseOneAllPattern(), self._CaptPulseOneAllPattern()]

        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 0, period1,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=drivePeriod, zeroDelay=True)
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 1, period2,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=slowSamplePeriod,
                                                           zeroDelay=True)
            for slice in [0, 1]:
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True,
                                                 False)  # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            # self.env.rc.SendTrigger(0x0ffc)

            # Wait for all slices to complete
            for slice in [0, 1]:
                hpcc.ac[slice].WaitForCompletePattern()

            # Run checkers
            for slice in [0, 1]:
                tempSlowSamplingCycle = list(
                    reversed(sorted(self.GetCycleFromCapture(hpcc, slice).items(), key=operator.itemgetter(0))))
                slowSamplingCycle = [i[1] for i in tempSlowSamplingCycle]
                # print(slowSamplingCycle)
                # self.env.DumpCapture(slot, slice)
            # self.env.RunMultiSliceCheckers([{'slot': slot, 'slice': 0, 'ratio': ratio}, {'slot': slot, 'slice': 1, 'ratio': 1},])

            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 0, period1,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=drivePeriod, zeroDelay=True)
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 1, period2,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=fastSamplePeriod,
                                                           zeroDelay=True)
            for slice in [0, 1]:
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True,
                                                 False)  # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            # self.env.rc.SendTrigger(0x0ffc)

            # Wait for all slices to complete
            for slice in [0, 1]:
                hpcc.ac[slice].WaitForCompletePattern()

            # Run checkers
            for slice in [0, 1]:
                tempFastSamplingCycle = list(reversed(
                    sorted(self.GetCycleFromCapture(hpcc, slice, targetVector=2200, rangeValue=600).items(),
                           key=operator.itemgetter(0))))
                fastSamplingCycle = [i[1] for i in tempFastSamplingCycle]
                # print(fastSamplingCycle)
                # self.env.DumpCapture(slot, slice)
            actualDelayMeasurement = [abs(i * fastSamplePeriod - j * slowSamplePeriod) for (i, j) in
                                      zip(fastSamplingCycle, slowSamplingCycle)]
            # print(actualDelayMeasurement )
            actualDelayMeasurementAve = sum(actualDelayMeasurement) / len(actualDelayMeasurement)
            actualDelayMeasurementStd = (
                                        sum((x - actualDelayMeasurementAve) ** 2 for x in actualDelayMeasurement) / len(
                                            actualDelayMeasurement)) ** 0.5
            compareSideDelay = slowSamplePeriod - fastSamplePeriod
            fifoStackCompareDelay = actualDelayMeasurementAve / compareSideDelay
            # print(fifoStackCompareDelay)
            capabilityReg = hpcc.ac[0].Read('Capability0')
            if (capabilityReg.CompareInDelay > (fifoStackCompareDelay - 1)) and (
                capabilityReg.CompareInDelay < (fifoStackCompareDelay + 1)):
                self.Log('info', 'CapabilityRegister for CompareInDelay is {0}, while measurement is {1}'.format(
                    capabilityReg.CompareInDelay, fifoStackCompareDelay))
            else:
                self.Log('error', 'CapabilityRegister for CompareInDelay is {0}, while measurement is {1}'.format(
                    capabilityReg.CompareInDelay, fifoStackCompareDelay))

    def MiniMultiTimeCapabilityDriveMeasurementTest(self):
        self.env.SetConfig('LowToHighLoopback')
        # period = 10e-9
        # hpcc = self.env.instruments[8]
        comparePeriod = 2.5e-9
        slowDrivePeriod = 50e-9
        fastDrivePeriod = 25e-9
        period1 = 10e-9
        period2 = 2.5e-9

        # period1 = hpcc.ac[0].minQdrMode * 52 / PICO_SECONDS_PER_SECOND
        # period2 = hpcc.ac[1].minQdrMode * 2 / PICO_SECONDS_PER_SECOND

        # ratio = 2
        # periods = [ period*ratio, period ]
        periods = [period1, period2]
        pattern = [self._DrivePulseOneAllPattern(), self._CaptPulseOneAllPattern()]

        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 0, period1,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=slowDrivePeriod,
                                                           zeroDelay=True)
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 1, period2,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=comparePeriod,
                                                           zeroDelay=True)
            for slice in [0, 1]:
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True,
                                                 False)  # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            # self.env.rc.SendTrigger(0x0ffc)

            # Wait for all slices to complete
            for slice in [0, 1]:
                hpcc.ac[slice].WaitForCompletePattern()

            # Run checkers
            for slice in [0, 1]:
                tempSlowSamplingCycle = list(reversed(
                    sorted(self.GetCycleFromCapture(hpcc, slice, targetVector=2200, rangeValue=600).items(),
                           key=operator.itemgetter(0))))
                slowSamplingCycle = [i[1] for i in tempSlowSamplingCycle]
                # self.env.DumpCapture(slot, slice)
                # self.env.RunMultiSliceCheckers([{'slot': slot, 'slice': 0, 'ratio': ratio}, {'slot': slot, 'slice': 1, 'ratio': 1},])
                # print(slowSamplingCycle)

                # slots = set()
                # for (slot, slice) in self.env.fpgas:
                #    slots.add(slot)

                # for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 0, period1,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=fastDrivePeriod,
                                                           zeroDelay=True)
            self.env.SetPeriodAndPEAttributesWithZeroDelay(slot, 1, period2,
                                                           {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0},
                                                           subcycleCount=0, internalPeriod=comparePeriod,
                                                           zeroDelay=True)
            for slice in [0, 1]:
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True,
                                                 False)  # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()
            # self.env.rc.SendTrigger(0x0ffc)

            # Wait for all slices to complete
            for slice in [0, 1]:
                hpcc.ac[slice].WaitForCompletePattern()

            # Run checkers
            for slice in [0, 1]:
                tempFastSamplingCycle = list(reversed(
                    sorted(self.GetCycleFromCapture(hpcc, slice, targetVector=1000, rangeValue=400).items(),
                           key=operator.itemgetter(0))))
                fastSamplingCycle = [i[1] for i in tempFastSamplingCycle]
                # self.env.DumpCapture(slot, slice)
                # print(slowSamplingCycle)
            # print(fastSamplingCycle)
            actualDelayMeasurement = [abs(j - i) for (i, j) in zip(fastSamplingCycle, slowSamplingCycle)]
            # print(actualDelayMeasurement)
            actualDelayMeasurementAve = sum(actualDelayMeasurement) / len(actualDelayMeasurement)
            # print(actualDelayMeasurementAve)
            actualDelayMeasurementStd = (
                                        sum((x - actualDelayMeasurementAve) ** 2 for x in actualDelayMeasurement) / len(
                                            actualDelayMeasurement)) ** 0.5
            driveSideDelay = slowDrivePeriod - fastDrivePeriod
            fifoStackCompareDelay = comparePeriod * actualDelayMeasurementAve / driveSideDelay - 102  # 102 coming from (drive pattern 1 + IO Jam Vector + EnCLK Bias)
            # print(fifoStackCompareDelay)
            capabilityReg = hpcc.ac[0].Read('Capability0')

            if (capabilityReg.DriveOutDelay > (fifoStackCompareDelay - 1)) and (
                capabilityReg.DriveOutDelay < (fifoStackCompareDelay + 1)):
                self.Log('info', 'CapabilityRegister for DriveoutDelay is {0}, while measurement is {1}'.format(
                    capabilityReg.DriveOutDelay, fifoStackCompareDelay))
            else:
                self.Log('error', 'CapabilityRegister for DriveoutDelay is {0}, while measurement is {1}'.format(
                    capabilityReg.DriveOutDelay, fifoStackCompareDelay))

    def GetCycleFromCapture(self, hpcc, slice, targetVector=200, rangeValue=50):
        cycleofFirstOne = dict()
        if slice == 1:
            captureData = hpcc.ac[slice].ReadCapture()
            for block in hpcc.ac[slice].UnpackCapture(captureData):
                header, data = block
                for i in range(8):
                    if header.CapturedVectorsValidFlags[i] == 1:
                        actual = data[i].PinLaneData
                        vaddr = data[i].VectorAddress
                        actualString = '{:056b}'.format(actual)
                        vectorString = ' '.join(actualString[i:i + 2] for i in range(0, len(actualString), 2))
                        # print(str(vaddr) + "    " + vectorString)
                        if (vaddr > (targetVector - rangeValue)) and (vaddr < (
                            targetVector + rangeValue)):  # vaddr > 150 & vaddr < 250 :  # Looking for first one between 150 and 250 Cycle.
                            for j in range(56):
                                # sometimes two vectors may fail, use the first one
                                if (actual & 1 == 1) and (j not in cycleofFirstOne):
                                    cycleofFirstOne[j] = vaddr
                                actual = actual >> 1
                                # print(firstOne)
        return cycleofFirstOne

    def DebugSnapshotPerPinRegisterTest(self):
        period = 5e-9
        registerFileName = r'X:\Users\sho10\FpgaValidationRegression\results\registers.csv'
        calFileName = r'X:\Users\sho10\FpgaValidationRegression\results\AcCalibration.csv'
        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.LoadRegistersFromSnapshot(slot, slice, registerFileName)
            hpcc.ac[slice]._ReadAcCalCsvFile(calFileName)
            for i in range(56):
                hpcc.ac[slice]._RecoverEdgeFromSnapshot(hpcc.dc[slice].pe[i])
            hpcc.ac[slice].TakeRegistersSnapshot('Register_slot{}_slice{}.csv'.format(slot, slice))

    def SnapshotMultiSliceTest(self):
        # targetSlot : sourceSlot
        SLOT_MAP = {10: 8}
        '''
        snapshot = 'X:\\Users\\yuanfeng\\FpgaValidation1\\PVC_Underrun_01_09_2016_11_46_01_Part0.tar.gz'     
        #snapshot = 'X:\\Users\\yuanfeng\\FpgaValidation1\\pvc2.tar.gz'
        snapshotDir = tempfile.mkdtemp()
        print('Extracting snapshot to {}'.format(snapshotDir))
        tar = tarfile.open(snapshot, "r:gz")
        tar.extractall(snapshotDir)
        tar.close()

        subdirs = next(os.walk(snapshotDir))[1]
        if len(subdirs) == 1:
            snapshotDir = os.path.join(snapshotDir, subdirs[0])
            print('Updated snapshotDir to {}'.format(snapshotDir))
        '''
        snapshotDir = 'X:\\Users\\yuanfeng\\FpgaValidation1\\debug\\Snapshot_03_15_2016_11_01_46_false_underrun.tar\\Snapshot_03_15_2016_11_01_46'
        print('Updated snapshotDir to {}'.format(snapshotDir))
        for (slot, slice) in self.env.fpgas:
            if slot in SLOT_MAP:
                path = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\PatternMemory'.format(SLOT_MAP[slot], slice))
                filename = os.path.join(snapshotDir,
                                        r'HPCC_Slot{}\AcFpga{}\registers.csv'.format(SLOT_MAP[slot], slice))
                self.env.LoadPatternMemoryFromSnapshot(slot, slice, path)
                self.env.LoadRegistersFromSnapshot(slot, slice, filename)

        self.env.SetConfig('InternalLoopback')
        period = 20e-9
        for repeat in range(1):
            self.env.Log('info', '~~round {}~~'.format(repeat))
            for (slot, slice) in self.env.fpgas:
                if slot in SLOT_MAP:
                    hpcc = self.env.instruments[slot]
                    hpcc.ac[slice].SetPeriod(period)
                    # captureControl = hpcc.ac[slice].Read('CaptureControl')
                    # captureControl.StopOnMaxFailCaptureCountHit = 0
                    # captureControl.CaptureFails = 0
                    # captureControl.StorageCaptureType = 1
                    # hpcc.ac[slice].Write('CaptureControl', captureControl)
                    captureBaseAddr = hpcc.ac[slice].Read('CaptureBaseAddress').Pack()
                    hpcc.ac[slice].captureBaseAddress = captureBaseAddr

                    start = hpcc.ac[slice].Read('PatternStartAddress').Pack()
                    hpcc.ac[slice].PrestagePattern(patternStartAddress=start)

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for (slot, slice) in self.env.fpgas:
                hpcc = self.env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                    hpcc.ac[slice].AbortPattern(waitForComplete=True)
                    isComplete = hpcc.ac[slice].IsPatternComplete()
                    self.env.Log('info', 'abort pattern, pattern complete = {}'.format(isComplete))
                    return
                self.env.Log('info', 'checking slot {} slice {}'.format(slot, slice))
                self.env.CheckAlarms(slot, slice)
                self.env.Log('info', 'Pattern completed = {}'.format(completed))
                self.env.Log('info', 'Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
                self.env.Log('info',
                             'New Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl1').Pack()))
                self.env.Log('info',
                             'Pattern End Status = 0x{:x}'.format(hpcc.ac[slice].Read('PatternEndStatus').Pack()))
                self.env.Log('info', 'Capture Address Underrun = 0x{:x}'.format(
                    hpcc.ac[slice].Read('CaptureAddressUnderrun').Pack()))
                self.env.Log('info', 'ErrorAddress = 0x{:x}'.format(hpcc.ac[slice].Read('ErrorAddress').Pack()))

            # self.env.DumpCapture(7, 1)
            '''
            # Run checkers
            self.env.RunMultiSliceCheckers([
                {'slot': slot, 'slice': 0},
                {'slot': slot, 'slice': 1},
            ])
            '''

    def SnapshotTest(self):
        snapshot = 'X:\\Users\\yuanfeng\\latest\\FpgaValidation1\\results\\VectorCompression_InstructionRepeat_RandomInstructionRPTTest.tar.gz'
        # snapshot = 'X:\\Users\\yuanfeng\\latest\\FpgaValidation1\\results\\VectorCompression_Mix_RandomLrptChannelLinkTest.tar.gz'
        # snapshot = 'X:\\Users\\yuanfeng\\latest\\FpgaValidation1\\results\\pvc_flat_pattern_12_16_2015_11_27_11_Part0.tar.gz'
        snapshotDir = tempfile.mkdtemp()
        print('Extracting snapshot to {}'.format(snapshotDir))
        tar = tarfile.open(snapshot, "r:gz")
        tar.extractall(snapshotDir)
        tar.close()

        subdirs = next(os.walk(snapshotDir))[1]
        if len(subdirs) == 1:
            snapshotDir = os.path.join(snapshotDir, subdirs[0])
            print('Updated snapshotDir to {}'.format(snapshotDir))

        self.env.SetConfig('EvenToOddLoopback')
        sourceSlot = 6
        for (slot, slice) in self.env.fpgas:
            path = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\PatternMemory'.format(sourceSlot, slice))
            filename = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\registers.csv'.format(sourceSlot, slice))

            self.env.LoadPatternMemoryFromSnapshot(slot, slice, path)
            self.env.LoadRegistersFromSnapshot(slot, slice, filename)
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpcc.ac[slice].captureBaseAddress = hpcc.ac[slice].Read('CaptureBaseAddress').Pack()
            period = 62.3e-9
            hpcc.ac[slice].SetPeriod(period)

            start = hpcc.ac[slice].Read('PatternStartAddress').Pack()
            print(hex(start))

            hpcc.ac[slice].LoadAndArmIlas(slot, slice)
            completed = hpcc.ac[slice].ExecutePattern(start)
            hpcc.ac[slice].DumpIlas(slot, slice)
            self.env.RunCheckers(slot, slice)

            if not completed:
                self.env.Log('error', 'Pattern execution did not complete')
                hpcc.ac[slice].AbortPattern(waitForComplete=True)
                isComplete = hpcc.ac[slice].IsPatternComplete()
                self.env.Log('info', 'abort pattern, pattern complete = {}'.format(isComplete))

            self.env.CheckAlarms(slot, slice)
            self.env.Log('info', 'Pattern completed = {}'.format(completed))
            self.env.Log('info', 'Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
            self.env.Log('info', 'New Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl1').Pack()))
            self.env.Log('info', 'Pattern End Status = 0x{:x}'.format(hpcc.ac[slice].Read('PatternEndStatus').Pack()))
            self.env.Log('info', 'Capture Address Underrun = 0x{:x}'.format(
                hpcc.ac[slice].Read('CaptureAddressUnderrun').Pack()))


class VoxVerify(HpccTest):

    def DirectedVihVilShmooTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        cal = {}
        vox_base_voltage = 1.5
        step_size = 0.1
        high_stable_voltage = 2.0
        low_stable_voltage = 1.0
        low_voltage_channel_stable_status = 0x00000000000000
        high_voltage_channel_stable_status = 0xFFFFFFFFFFFFFF

        for (slot, slice) in self.env.fpgas:
            vih_base_voltage = 0
            vil_base_voltage = 3

            for channel in range(56):
                cal[(slice, channel)] = vox_base_voltage

            hpcc = self.env.instruments[slot]
            period = hpcc.ac[slice].minQdrMode / PICO_SECONDS_PER_SECOND
            hpcc.ac[slice].SetPeriod(period)

            all_one_pattern, all_zero_pattern = self.all_one_and_all_zero_pattern_assembler()

            hpcc.cal.SetEvenToOddChannelLoopback()
            for ac in hpcc.ac:
                ac.internalLoopback = False

            self.Log('info', 'VIH increase shmoo for slice: {} slot: {} '.format(slice,slot))
            for vih_low_to_high_shmoo in range(30):
                hpcc.dc[slice].SetPEAttributes(
                    {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0, 'VOX': cal[(slice, channel)],
                     'VIH': vih_base_voltage},
                    True, channels='ALL_PINS')
                self.env.WriteExecutePatternAndSetupCapture(slot, slice, all_one_pattern)
                pin_status = self.read_pin_input_status_register(hpcc, slice)
                if high_stable_voltage == round(vih_base_voltage,10):
                    if pin_status != high_voltage_channel_stable_status:
                        self.print_failing_pin_details(high_stable_voltage, pin_status,high_voltage_channel_stable_status )
                        break
                if low_stable_voltage == round(vih_base_voltage,10):
                    if pin_status != low_voltage_channel_stable_status:
                        self.print_failing_pin_details(low_stable_voltage, pin_status,low_voltage_channel_stable_status )
                        break
                vih_base_voltage = vih_base_voltage + step_size

            self.Log('info','VIL decrease shmoo.  for slice: {} slot: {} '.format(slice, slot))
            for vil_high_to_low_shmoo in range(30):
                hpcc.dc[slice].SetPEAttributes(
                    {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 0.0, 'VOX': cal[(slice, channel)],
                     'VIL': vil_base_voltage}, True, channels='ALL_PINS')
                self.env.WriteExecutePatternAndSetupCapture(slot, slice, all_zero_pattern)
                pin_status = self.read_pin_input_status_register(hpcc, slice)
                if high_stable_voltage == round(vil_base_voltage,10):
                    if pin_status != high_voltage_channel_stable_status:
                        self.print_failing_pin_details(high_stable_voltage, pin_status,high_voltage_channel_stable_status )
                        break
                if low_stable_voltage == round(vil_base_voltage, 10):
                    if pin_status != low_voltage_channel_stable_status:
                        self.print_failing_pin_details(low_stable_voltage, pin_status,low_voltage_channel_stable_status )
                        break
                vil_base_voltage = vil_base_voltage - step_size

    def print_failing_pin_details(self, high_stable_voltage, pin_status, expected_pin_status):
        self.Log('error',
                 'Failed to stabilize at :{}V. Failing pin(s) as shown below -'.format(high_stable_voltage))
        self.Log('info', "Pin               \t" +
                 "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
        actual_string = '{:056b}'.format(pin_status)
        expected_string = '{:056b}'.format(expected_pin_status)
        actual_vector_string = ' '.join(actual_string[i:i + 2] for i in range(0, len(actual_string), 2))
        expected_vector_string = ' '.join(expected_string[i:i + 2] for i in range(0, len(expected_string), 2))
        self.Log('info', "Expected Status   \t" + expected_vector_string)
        self.Log('error', "Actual Status     \t" + actual_vector_string)


    def read_pin_input_status_register(self, hpcc, slice):
        upper = hpcc.ac[slice].Read('PinInputStatusUpper').Pack()
        lower = hpcc.ac[slice].Read('PinInputStatusLower').Pack()
        pin_status = upper << 32 | lower
        return pin_status

    def all_one_and_all_zero_pattern_assembler(self):
        asm = PatternAssembler()
        asm.LoadString("""\
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
                Drive0CompareLVectors length=1024
                %repeat 1024                  
                V link=0, ctv=1, mtv=1, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
            """)
        all_one_pattern = asm.Generate()
        asm = PatternAssembler()
        asm.LoadString("""\
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
                Drive0CompareLVectors length=1024
                %repeat 1024
                V link=0, ctv=1, mtv=1, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000  
                %end
                PATTERN_END:                                                                                     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                S stype=IOSTATEJAM,             data=0j0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X  
                        """)
        all_zero_pattern = asm.Generate()
        return all_one_pattern, all_zero_pattern
