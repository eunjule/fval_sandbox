################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Clocks.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for HPCC AC clocks, eg: 9914, lmk1000... 
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 03/29/16
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest

class HPClock(HpccTest):
        
    # default 9914 HP clock runs @ 8ns
    def DirectedDefault9914HPClockTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 0
            I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=1
            %repeat 1000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            %end
            I optype=VECTOR, vptype=VPOTHER, vpop=EDGECNTR_I, imm=0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        
        pdata = pattern.Generate()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            hpcc.EnableHPClockPin48Output(True)
            
            self.env.SetFreqCounterModeAttributes(slot, slice, {'EDGECOUNTER': 'RISE'}, True) # 'OFF','RISE', "FALL','BOTH'
            self.env.RunPattern(slot, slice, pdata, 2e-9)
            #self.env.DumpCapture(slot, slice)
            pin48Count = hpcc.ac[slice].ReadChannel(48, 'ChannelFrequencyCounts').Pack()
            pin49Count = hpcc.ac[slice].ReadChannel(49, 'ChannelFrequencyCounts').Pack()
            if slice == 0:
                expectedCount = 250
            else:
                expectedCount = 0
            if pin48Count != expectedCount or pin49Count != expectedCount:
                self.Log('error', 'expected count {}, pin 48 count {}, pin 49 count {}'.format(expectedCount, pin48Count, pin49Count))

            hpcc.EnableHPClockPin48Output(False)
            

    