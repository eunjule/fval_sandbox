################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: RegisterOps.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for the misc register op instructions 
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/20/16
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest


class Register(HpccTest):
    def DirectedRegisterPushPopTest(self):
        self.env.SetConfig('InternalLoopback')
        asm = PatternAssembler()
        asm.LoadString("""
            StartPattern
            RandomVectors length=2000
            SetRegister 0, 10   
            PushRegister 0 
            SetRegister 0,3
            LABEL2:
                RandomVectors length=200
                DecRegister 0
                GotoIfNonZero `LABEL2`
            PopRegister 0            
            StopPatternReturnRegister reg=0
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = 2.5e-9)
            self.env.RunCheckers(slot, slice)
            #hpcc = self.env.instruments[slot]
            #print(hpcc.ac[slice].Read('CentralDomainRegister0'))
