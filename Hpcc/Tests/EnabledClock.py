################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: EnabledClock.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for Enabled Clock Patterns
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng / Rodny Rodriguez / Sungjin Ho
#        Date: 06/03/15
#       Group: HDMT FPGA Validation
################################################################################

import gzip
import os

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.Tests.hpcctest import HpccTest
from Tools import projectpaths
import Hpcc.instrument.hpccAcRegs as ac_registers

class CornerCases(HpccTest):
        
    # CQ 12097
	# The previous version of this test case was to drive the R following the falling edge. That is violate
	# Rule B (Trise < Tfall) per D. Armstrong 
	# The test is modified to drive R one clock after the falling edge.
	# EC ration = 8 with duty cycle 3/5
	# Verify: the EC goes high for 3 cycles then down for 5 cycles while on Channel 55, it will go high for 
	# 3 cycles then goes down to 2 cycles then goes up again due to the R on the cycle 5. 
    def DirectedResetCauseExtraEdgeTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 20e-9

        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 99
        pattern.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
			V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE #new pattern vector to add 1 cycle after falling edge
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLRLELELELELELELELELELELELELELELELELELELELELELELELELELELE
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """)
                
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': (period /4)}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 8, 'trise': 0.0, 'tfall': period * 3}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 1)
            self.env.RunCheckers(slot, slice)
            #hpcc.DumpCapture(slice)

class EnabledClock(HpccTest):

    def MiniEnabledClockRatioTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9

        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 1024
        pattern.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """)
                
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)

        ratios = [2,2,3,4,5,6,7,8,9,10,11,13,21,26,34,40,47,55,61,68,72,89,93,100,104,115,127,128]
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratios[i//2], 'trise': 0.0, 'tfall': period}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)

    def MiniEnabledClockBigRatio50DutyCycleTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 50e-9

        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 1024
        pattern.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLRLR
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """)
                
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)

        ratios = [2,3,4,5,6,7,8,9,10,11,13,21,26,34,40,47,55,61,68,74,89,104,115,127,128,152,200,254]
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratios[i//2], 'trise': 0.0, 'tfall': period * (ratios[i//2] // 2)}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            
    def MiniEnabledClockRatioOneCheckHighTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9

        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 1024
        pattern.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """)
                
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': (period / 4)}, True)
            for i in range(0,56,4):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': period / 2}, True, [i])
            for i in range(2,56,4):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 2, 'trise': 0.0, 'tfall': period}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            
    def MiniEnabledClockRatioOneCheckLowTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9

        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 1024
        pattern.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """)
                
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': 1.5e-9}, True)
            for i in range(0,56,4):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': period / 2}, True, [i])
            for i in range(2,56,4):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 2, 'trise': 0.0, 'tfall': period}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata, captureType = 0)
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
    
    ##Regular EnCLK Tests
    def DirectedRatio1Duty50CompareHighExhaustiveTest(self):
        ratio = 1
        patStr = r'Hpcc\Patterns\Ratio2Pattern_UsingRatio4.obj.gz'
        self.RunEnabledClockScenarioRatio1(patStr, ratio=ratio, compare=0.25, tfall=ratio*0.5)
        
    def DirectedRatio1Duty50CompareLowExhaustiveTest(self):
        ratio = 1
        patStr = r'Hpcc\Patterns\Ratio2Pattern_UsingRatio4.obj.gz'
        self.RunEnabledClockScenarioRatio1(patStr, ratio=ratio, compare=0.75, tfall=ratio*0.5)

    ##InvertedTestCases:Start
    def DirectedInvertedRatio1Duty50CompareHighExhaustiveTest(self):
        ratio = 1
        patStr = r'Hpcc\Patterns\Ratio2Pattern_UsingRatio4.obj.gz'
        self.RunInvertedEnabledClockScenarioRatio1(patStr, ratio=ratio, compare=0.75, trise=ratio*0.5)
        
    def DirectedInvertedRatio1Duty50CompareLowExhaustiveTest(self):
        ratio = 1
        patStr = r'Hpcc\Patterns\Ratio2Pattern_UsingRatio4.obj.gz'
        self.RunInvertedEnabledClockScenarioRatio1(patStr, ratio=ratio, compare=0.25, trise=ratio*0.5)

    ##Constrained EnCLK Regular Tests

    def DirectedConstrainedRatio2Duty50ComparehighlowExhaustiveTest(self):
        ratio = 2
        patstr = r'hpcc\patterns\Ratio2Pattern_UsingRatio4.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.5)
        
    def DirectedConstrainedRatio4Duty25ComparehighlowExhaustiveTest(self):
        ratio = 4
        patstr = r'hpcc\patterns\Ratio4Pattern_UsingRatio4.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.25)
        
    def DirectedConstrainedRatio4Duty50ComparehighlowExhaustiveTest(self):
        ratio = 4
        patstr = r'hpcc\patterns\Ratio4Pattern_UsingRatio4.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.5)
        
    def DirectedConstrainedRatio4Duty75ComparehighlowExhaustiveTest(self):
        ratio = 4
        patstr = r'hpcc\patterns\Ratio4Pattern_UsingRatio4.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.75)
        
    def DirectedConstrainedRatio6Duty17ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.16666666666666666)
        
    def DirectedConstrainedRatio6Duty33ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.3333333333333333)
        
    def DirectedConstrainedRatio6Duty50ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.5)
        
    def DirectedConstrainedRatio6Duty67ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.6666666666666666)
        
    def DirectedConstrainedRatio6Duty83ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunEnabledClockScenario(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.8333333333333334)
        

    ##Constained Inverted EnCLK Tests

    def DirectedConstrainedInvertedRatio2Duty50ComparehighlowExhaustiveTest(self):
        ratio = 2
        patstr = r'hpcc\patterns\Ratio2Pattern_UsingRatio4.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.5)
        
    def DirectedConstrainedInvertedRatio4Duty25ComparehighlowExhaustiveTest(self):
        ratio = 4
        patstr = r'hpcc\patterns\Ratio4Pattern_UsingRatio4.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.75)
        
    def DirectedConstrainedInvertedRatio4Duty50ComparehighlowExhaustiveTest(self):
        ratio = 4
        patstr = r'hpcc\patterns\Ratio4Pattern_UsingRatio4.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.5)
        
    def DirectedConstrainedInvertedRatio4Duty75ComparehighlowExhaustiveTest(self):
        ratio = 4
        patstr = r'hpcc\patterns\Ratio4Pattern_UsingRatio4.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.25)
        
    def DirectedConstrainedInvertedRatio6Duty17ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.8333333333333334)
        
    def DirectedConstrainedInvertedRatio6Duty33ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.6666666666666667)
        
    def DirectedConstrainedInvertedRatio6Duty50ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.5)
        
    def DirectedConstrainedInvertedRatio6Duty67ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.33333333333333337)
        
    def DirectedConstrainedInvertedRatio6Duty83ComparehighlowExhaustiveTest(self):
        ratio = 6
        patstr = r'hpcc\patterns\Ratio6Pattern_UsingRatio6.obj.gz'
        self.RunInvertedEnabledClockScenario(patstr, ratio=ratio, compare=0.5, trise=ratio*0.16666666666666663)
        

    def DebugRatio3Duty50ComparepP8Test(self):
        ratio = 3
        patStr = 'EEEEEREEEEEEEREEREREE'
        print('test1')
        self.RunEnabledClockScenarioDebug(patStr, ratio=ratio, compare=0.8, tfall=ratio*0.5)

    def DebugRatio2Duty50ComparehighlowExhaustiveTestDebug(self):
        ratio = 2
        patstr = 'EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE'
        self.RunEnabledClockScenarioDebug(patstr, ratio=ratio, compare=0.5, tfall=ratio*0.5)
        
    def RunEnabledClockScenarioDebug(self, waveformChars,  ratio, compare, tfall):
        self.env.SetConfig('EvenToOddLoopback')
        period = 5e-9
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 10240
        source = """\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            ClearFlags
        """
        for ch in waveformChars:
            source = source + '            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}L{0}\n'.format(ch)
        source = source + """\
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            StopPatternReturnFlags
            #I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """
        #print(source)
        pattern.LoadString(source)
        ##pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        ##pdata = pattern
        pdata = pattern.Generate()
        print('test2')
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            hpcc = self.env.instruments[slot]
            self.env.LogPerSlice(slot, slice)
            print('test5')
            self.env.SetPeriodACandSim(slot, slice, period)
            print('test6')
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            print('test4')
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
            
            completed, endStatus = self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            print('test3')
            flags = ac_registers.CentralDomainRegisterFlags(endStatus)
            if flags.LocalStickyError   == 1:
                self.Log('error', 'Local Sticky Error bit is set in flags=0x{:x}'.format(endStatus))
            if flags.DomainStickyError  == 1:
                self.Log('error', 'Domain Sticky Error bit is set in flags=0x{:x}'.format(endStatus))
            if flags.GlobalStickyError  == 1:
                self.Log('error', 'Global Sticky Error bit is set in flags=0x{:x}'.format(endStatus))
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def RunInvertedEnabledClockScenarioDebug(self, waveformChars,  ratio, compare, trise):
        self.env.SetConfig('EvenToOddLoopback')
        period = 25e-9
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = 256
        source = """\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """
        for ch in waveformChars:
            source = source + '            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L{}\n'.format(ch)
        source = source + """\
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        """
        #print(source)
        pattern.LoadString(source)
        #pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        #pda#ta = pattern
        pdata = pattern.Generate()
        pattern.SaveObj('EnClkRatio3Duty50Period25nS.obj')
        patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': period*tfall, 'tfall': 0}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            hpcc = self.env.instruments[slot]
            hpcc.DumpCapture(slice)

    def RunEnabledClockScenarioRatio1(self, waveformPath,  ratio, compare, tfall):
        self.env.SetConfig('EvenToOddLoopback')
        period = 5e-9 # Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 256
        #source = """\
        #    %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #for ch in waveformChars:
        #    source = source + '            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L{}\n'.format(ch)
        #source = source + """\
        #    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #print(source)
        #pattern.LoadString(source)
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern
        #pdata = pattern.Generate()
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def RunEnabledClockScenario(self, waveformPath,  ratio, compare, tfall):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9 # Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 256
        #source = """\
        #    %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #for ch in waveformChars:
        #    source = source + '            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L{}\n'.format(ch)
        #source = source + """\
        #    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #print(source)
        #pattern.LoadString(source)
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern
        #pdata = pattern.Generate()
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            for i in range(0,56,2):
                self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def RunEnabledClockMixedRegularScenario(self, waveformPath,  ratio, compare, compareRatioOne, tfall, tfallRatioOne):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9 # Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareRatioOne}, True, [55])
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1 , 'trise': 0.0, 'tfall': period*tfallRatioOne}, True, [54])

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)
    
    def RunEnabledClockMixedScenario(self, waveformPath,  ratio, compare, compareRatioOne, tfall, tfallRatioOne):
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9 # Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareRatioOne}, True, [1])
            for i in range(0,56,2):
                if i != 0 :
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
                else:
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1 , 'trise': 0.0, 'tfall': period*tfallRatioOne}, True, [i])

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def RunInvertedEnabledClockMixedScenario(self, waveformPath,  ratio, compare, compareRatioOne, trise, triseRatioOne):
        # Swapping trise and tfall to make tfall 0S while trise as a variable.
        # Inverted one of RunEnabledClockScenario
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9# Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareRatioOne}, True, [1])
            for i in range(0,56,2):
                if i != 0 :
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': period*trise, 'tfall': 0}, True, [i])
                else:
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1 , 'trise': period*triseRatioOne, 'tfall': 0}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def RunInvertedEnabledClockScenarioRatio1(self, waveformPath,  ratio, compare, trise):
        # Swapping trise and tfall to make tfall 0S while trise as a variable.
        # Inverted one of RunEnabledClockScenario
        self.env.SetConfig('EvenToOddLoopback')
        period = 5e-9# Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 256
        #source = """\
        #    %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #for ch in waveformChars:
        #    source = source + '            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L{}\n'.format(ch)
        #source = source + """\
        #    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #print(source)
        #pattern.LoadString(source)
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern
        #pdata = pattern.Generate()
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            for i in range(0,56,2):
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': period*trise, 'tfall': 0}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def RunInvertedEnabledClockScenario(self, waveformPath,  ratio, compare, trise):
        # Swapping trise and tfall to make tfall 0S while trise as a variable.
        # Inverted one of RunEnabledClockScenario
        self.env.SetConfig('EvenToOddLoopback')
        period = 2.5e-9# Fix later to 2.5e-9 once FPGA is fast enough to run at this speed.
        pattern = PatternAssembler()
        #pattern.symbols['REPEATS'] = 256
        #source = """\
        #    %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #for ch in waveformChars:
        #    source = source + '            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L{}\n'.format(ch)
        #source = source + """\
        #    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
        #    S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
        #"""
        #print(source)
        #pattern.LoadString(source)
        pattern = gzip.open(os.path.join(projectpaths.ROOT_PATH, waveformPath), 'rb').read()
        pdata = pattern
        #pdata = pattern.Generate()
        #patternSize = len(pdata)

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            for i in range(0,56,2):
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': period*trise, 'tfall': 0}, True, [i])
            
            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

    def DirectedEnCLKETOBothasmTest(self):
        self.env.SetConfig('EvenToOddLoopback')

        period = 5e-9
        #ratio = 3
        compare = 0.75
        #compareRatioOne = 0.4
        #tfall = 1
        tfallRatioOne = 0.5
        asm1 = PatternAssembler()
        asm1.symbols['REPEATS'] = 70000
        asm1.LoadString("""\
                  # %var                           enclk=0b10101010101010101010101010101010101010101010101010101010
                    %var                           enclk=0b10000000000000000000000000000000000000000000000000000000
                   PATTERN_START:
                   #S stype=IOSTATEJAM,             data=0jEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEX
                    S stype=IOSTATEJAM,             data=0jEXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
                    %repeat REPEATS  
                   #V link=0, ctv=0, mtv=0, lrpt=0, data=0vELELELELELELELELELELELELELELELELELELELELELELELELELELELEL
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vEL1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vRL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
                   #V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
                   #V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                    %end
                    PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    #S stype=IOSTATEJAM,             data=0jZZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
                    """)
##
        #print('x1')
        #pattern[0] = open('SLOT9.SLICE0-fixed.obj', 'rb').read()
        #pattern[1] = open('SLOT9.SLICE1-fixed.obj', 'rb').read()
        pdata = asm1.Generate()
        #asm1.SaveObj('EnClkRatio1Duty50Period5nSDrive.obj')
        #asm.SaveObj('EnClkRatio1Duty50Period5nSCapture.obj')
        #print('x2')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            #self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareRatioOne}, True, [55])
            #for i in range(0,56,2):
            #    if i != 55 :
            #        self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
            #    else:
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1 , 'trise': 0.0, 'tfall': period*tfallRatioOne}, True, [55])

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

            #print('It finished!!!! :)')

    def DirectedEnCLKCompOneTest(self):
        self.env.SetConfig('LowToHighLoopback')

        period = 5e-9
        compareX = 0.5
        ratio = 1
        periods = [None, None]
        periods[0] = period * ratio
        periods[1] = period

        asm = [None, None]
        pattern = [None, None]
		
		#create pattern of driving one for slice 0
        asm0 = PatternAssembler()
        asm0.symbols['REPEATS'] = 10000
        asm0.LoadString("""\
			S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x89ABCDEF
            #S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        """)
		#create pattern for slice 1 - that is set even pins as EC and odd pins as compare strobe
        asm1 = PatternAssembler()
        asm1.symbols['REPEATS'] = 10000
        REPEATS = 10000
        asm1.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS  
			V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELELELELELELELELELELELELELELELELELE
            %end
        
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            #S stype=IOSTATEJAM,             data=0jZZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
        """)

       
        pattern[0] = asm0.Generate()
        pattern[1] = asm1.Generate()
        
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            for slice in [0, 1]:
                if slice == 1:
                    self.env.SetPeriodACandSim(slot, slice, period)
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareX}, True)
					#set the even pin as EC.
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': 0.5 * period}, True, list(range(0, 56, 2) ))
                    #config the even pins slice 1 in compare mode 
                    for ch in range(0, 56, 2):
                        config = hpcc.ac[slice].ReadChannel(ch, 'PerChannelControlConfig1')
                        config.EnabledClockDifferentialCompMode = 1
                        hpcc.ac[slice].WriteChannel(ch, 'PerChannelControlConfig1', config)
                else: # set slice 0 as dpin 
                    self.env.SetPeriodACandSim(slot, slice, period)
                    self.env.SetPEAttributesDCandSim(slot, slice,  {'IS_ENABLED_CLOCK': False, 'drive': 0.0}, True, list(range(56) ) )
                    
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True, False) # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1 ]:
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                for ch in range(56):
                    error = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                    if ((ch %2) == 0 and slice == 1 ):
                        if (error.ChannelFailCaptureCount != REPEATS):
                            self.Log('error', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, REPEATS))
                    elif error.ChannelFailCaptureCount > 0:
                        self.Log('error', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, 0))
				# reset back to EC pin
                if slice == 1:
                    for ch in range(0, 56, 2):
                        config = hpcc.ac[slice].ReadChannel(ch, 'PerChannelControlConfig1')
                        config.EnabledClockDifferentialCompMode = 0
                        hpcc.ac[slice].WriteChannel(ch, 'PerChannelControlConfig1', config)
                        
    def DirectedEnCLKCompZeroTest(self):
        self.env.SetConfig('LowToHighLoopback')

        period = 5e-9
        compareX = 0.5
        ratio = 1
        periods = [None, None]
        periods[0] = period * ratio
        periods[1] = period

        asm = [None, None]
        pattern = [None, None]
		
		#create pattern of driving ZERO for slice 0
        asm0 = PatternAssembler()
        asm0.symbols['REPEATS'] = 10000
        REPEATS = 10000
        asm0.LoadString("""\
			S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x89ABCDEF
            #S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        """)
		#create pattern for slice 1 - that is set even pins as EC and odd pins as compare strobe
        asm1 = PatternAssembler()
        asm1.symbols['REPEATS'] = 10000
        REPEATS = 10000
        asm1.LoadString("""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS  
			V link=0, ctv=0, mtv=0, lrpt=0, data=0vHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHE
            %end
        
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
            #S stype=IOSTATEJAM,             data=0jZZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
        """)

       
        pattern[0] = asm0.Generate()
        pattern[1] = asm1.Generate()
        
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            for slice in [0, 1]:
                if slice == 1:
                    self.env.SetPeriodACandSim(slot, slice, period)
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareX}, True)
					#set the even pin as EC.
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': 0.5 * period}, True, list(range(0, 56, 2) ))
                    #config the even pins slice 1 in compare mode 
                    for ch in range(0, 56, 2):
                        config = hpcc.ac[slice].ReadChannel(ch, 'PerChannelControlConfig1')
                        config.EnabledClockDifferentialCompMode = 1
                        hpcc.ac[slice].WriteChannel(ch, 'PerChannelControlConfig1', config)
                else: # set slice 0 as dpin 
                    self.env.SetPeriodACandSim(slot, slice, period)
                    self.env.SetPEAttributesDCandSim(slot, slice,  {'IS_ENABLED_CLOCK': False, 'drive': 0.0}, True, list(range(56) ) )
                    
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True, False) # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1 ]:
                completed = hpcc.ac[slice].WaitForCompletePattern()
                if not completed:
                    self.Log('error', 'Pattern execution did not complete')
                for ch in range(56):
                    error = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                    configreg = hpcc.ac[slice].ReadChannel(ch,'PerChannelControlConfig1')
                    if ((ch %2) == 0 and slice == 1 ):
                        if (error.ChannelFailCaptureCount != REPEATS):
                            self.Log('error', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, REPEATS))
                        else:
                            self.Log('info', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, REPEATS))
                    else:
                        self.Log('info', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, 0))
                # reset back to EC pin
                if slice == 1:
                    for ch in range(0, 56, 2):
                        config = hpcc.ac[slice].ReadChannel(ch, 'PerChannelControlConfig1')
                        config.EnabledClockDifferentialCompMode = 0
                        hpcc.ac[slice].WriteChannel(ch, 'PerChannelControlConfig1', config)
						
    def DirectedEnCLKCompCaptureZeroFailTest(self):
        self.env.SetConfig('LowToHighLoopback')

        period = 5e-9
        compareX = 0.5
        ratio = 1
        periods = [None, None]
        periods[0] = period * ratio
        periods[1] = period

        asm = [None, None]
        pattern = [None, None]
        endvalue = 0xACED
		#create pattern of driving ZERO for slice 0
        asm0 = PatternAssembler()
        asm0.symbols['REPEATS'] = 10000
        asm0.LoadString(f"""\
			S stype=IOSTATEJAM,             data=0jZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v01010101010101010101010010101010101010101010101010101010
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={endvalue}
            #S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        """)
		#create pattern for slice 1 - that is set even pins as EC and odd pins as compare strobe
        asm1 = PatternAssembler()
        asm1.symbols['REPEATS'] = 10000
        REPEATS = 10000
        asm1.LoadString(f"""\
            %var                           enclk=0b01010101010101010101010101010101010101010101010101010101
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0jXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXE
            %repeat REPEATS  

			V link=0, ctv=0, mtv=0, lrpt=0, data=0vLELELELELELELELELELELELEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHEHE
            %end
        
            PATTERN_END:
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={endvalue}
            #S stype=IOSTATEJAM,             data=0jZZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
        """)

       
        pattern[0] = asm0.Generate()
        pattern[1] = asm1.Generate()
        
        slots = set()
        for (slot, slice) in self.env.fpgas:
            slots.add(slot)

        for slot in slots:
            self.env.LogPerSlot(slot)
            hpcc = self.env.instruments[slot]
            for slice in [0, 1]:
                if slice == 1:
                    self.env.SetPeriodACandSim(slot, slice, period)
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareX}, True)
					#set the even pin as EC.
                    self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1, 'trise': 0.0, 'tfall': 0.5 * period}, True, list(range(0, 56, 2) ))
                    #config the even pins slice 1 in compare mode 
                    for ch in range(0, 56, 2):
                        config = hpcc.ac[slice].ReadChannel(ch, 'PerChannelControlConfig1')
                        config.EnabledClockDifferentialCompMode = 1
                        hpcc.ac[slice].WriteChannel(ch, 'PerChannelControlConfig1', config)
                else: # set slice 0 as dpin 
                    self.env.SetPeriodACandSim(slot, slice, period)
                    self.env.SetPEAttributesDCandSim(slot, slice,  {'IS_ENABLED_CLOCK': False, 'drive': 0.0}, True, list(range(56) ) )
                    
                self.env.WritePattern(slot, slice, 0, pattern[slice])
                # Setup capture
                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern[slice]))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
                hpcc.ac[slice].SetCaptureControl(ac_registers.CaptureControl.PIN_STATE, False, False, True, False) # capture all
                # Clear fail counts
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
                hpcc.ac[slice].PrestagePattern()

            # Send sync pulse    
            self.env.rc.send_sync_pulse()

            # Wait for all slices to complete
            for slice in [0, 1 ]:
                completed = hpcc.ac[slice].WaitForCompletePattern()
                pat_reg = hpcc.ac[slice].Read('PatternEndStatus')
                if not completed or endvalue != pat_reg.Status:
                    self.Log('error', f'Pattern execution did not complete, end_value is {pat_reg.Status}, expected {endvalue}')
                for ch in range(56):
                    error = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount')
                    configreg = hpcc.ac[slice].ReadChannel(ch,'PerChannelControlConfig1')
                    if (error.ChannelFailCaptureCount != 0):
                        self.Log('error', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, 0))
                    else:
                        self.Log('info', 'Slice {} Channel {} capture number of Fail {} while expected {}'.format(slice, ch, error.ChannelFailCaptureCount, 0))
				# Reset back to EC pin 
                if slice == 1:
                    for ch in range(0, 56, 2):
                        config = hpcc.ac[slice].ReadChannel(ch, 'PerChannelControlConfig1')
                        config.EnabledClockDifferentialCompMode = 0
                        hpcc.ac[slice].WriteChannel(ch, 'PerChannelControlConfig1', config)
                    

    def DirectedMisalignedEnCLKETOTest(self):
        self.env.SetConfig('EvenToOddLoopback')

        period = 4.7e-9
        #ratio = 3
        compare = 0.75
        #compareRatioOne = 0.4
        #tfall = 1
        tRiseRatioOne = 0.5
        asm1 = PatternAssembler()
        asm1.symbols['REPEATS'] = 2000
        asm1.LoadString("""\
                  # %var                           enclk=0b10101010101010101010101010101010101010101010101010101010
                    %var                           enclk=0b10000000000000000000000000000000000000000000000000000000
                   PATTERN_START:
                   #S stype=IOSTATEJAM,             data=0jEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEXEX
                    S stype=IOSTATEJAM,             data=0jEXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX
                    %repeat REPEATS  
                   #V link=0, ctv=0, mtv=0, lrpt=0, data=0vELELELELELELELELELELELELELELELELELELELELELELELELELELELEL
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L
                    %end
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vE1111111111111111111111111111111111111111111111111111111
                    PATTERN_END:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    """)
##
        #print('x1')
        #pattern[0] = open('SLOT9.SLICE0-fixed.obj', 'rb').read()
        #pattern[1] = open('SLOT9.SLICE1-fixed.obj', 'rb').read()
        pdata = asm1.Generate()
        #asm1.SaveObj('MisAlignedEnCLK.obj')
        #asm.SaveObj('EnClkRatio1Duty50Period5nSCapture.obj')
        #print('x2')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.SetPeriodACandSim(slot, slice, period)
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compare}, True)
            #self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period*compareRatioOne}, True, [55])
            #for i in range(0,56,2):
            #    if i != 55 :
            #        self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': ratio, 'trise': 0.0, 'tfall': period*tfall}, True, [i])
            #    else:
            self.env.SetPEAttributesDCandSim(slot, slice, {'IS_ENABLED_CLOCK': True, 'DRIVE_TYPE': 'FIRST', 'RESET': 'TRANSITION', 'ratio': 1 , 'trise': tRiseRatioOne*period, 'tfall':0}, True, [55])

            self.env.WriteExecutePatternAndSetupCapture(slot, slice, pdata)
            #hpcc.ac[slice].TakeRegistersSnapshot('MisAlignedEnClk_registers.csv')
            self.env.RunCheckers(slot, slice)
            # TODO(rodny) : Update the simulator
            #self.env.DumpCapture(slot, slice)

            #print('It finished!!!! :)')
