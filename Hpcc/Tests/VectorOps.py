################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: VectorOps.py
#-------------------------------------------------------------------------------
#     Purpose: Tests for the misc vector op instructions 
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/10/16
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers

class Log(HpccTest):
    def DirectedPatternIdUsrLogTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN1:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 50, simple=1
            I optype=BRANCH, br=RET                                                                         
            PATTERN2:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 50, simple=1
            I optype=BRANCH, br=RET 
            PATTERN3:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 50, simple=1
            I optype=BRANCH, br=RET 
            PATTERN4:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 50, simple=1
            I optype=BRANCH, br=RET 
            
            PATTERN_START:                                                                                 
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=0                                             
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xabc                                           
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN1]  
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xef 
            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm=0xc
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_RA, regA=1                                            
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN2]   
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xa   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xa   
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2       
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
            %end
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN3] 
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=4
            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=0, imm=0xb
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_RA, regA=0  
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_RA, regA=1   
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=3                                             
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN4]                                   
            %repeat 10 # 200 fails
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            PATTERN_END:                                                                                   
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                    
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """)
        
        pdata = pattern.Generate()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pdata, 50e-9, start = pattern.Resolve('eval[PATTERN_START]'))
            self.env.RunCheckers(slot, slice)
    
    def DirectedPatternIDUsrLogLinkTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            Drive0CompareLVectors 256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xa   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xa   
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xb   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xb   
            V link=1, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)      
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = 1
            data = hpcc.ac[slice].Write('PatternControl', data)   
            
            self.env.RunPattern(slot, slice, pdata, 1.25e-9, captureFails = False, captureCTVs = True, captureAll = False)    
            passed, alarms = self.env.RunCheckers(slot, slice)
            
    def DirectedPatternIDUsrLogLrptTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            Drive0CompareLVectors 256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xa   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xa   
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xb   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xb   
            V link=0, ctv=1, mtv=0, lrpt=6, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)       
            
            self.env.RunPattern(slot, slice, pdata, 1.25e-9, captureFails = False, captureCTVs = True, captureAll = False)    
            passed, alarms = self.env.RunCheckers(slot, slice)    

    def DirectedPatternIDUsrLogIrptTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            Drive0CompareLVectors 256
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xa   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xa   
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
            I optype=VECTOR, vptype=VPLOG, vpop=LOG1_I, imm=0xb   
            I optype=VECTOR, vptype=VPLOG, vpop=LOG2_I, imm=0xb   
            Repeat 23
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)       
            
            self.env.RunPattern(slot, slice, pdata, 1.25e-9, captureFails = False, captureCTVs = True, captureAll = False)    
            passed, alarms = self.env.RunCheckers(slot, slice)      
    


# block fail, drain pin fifo, capture, reset cycle count, reset capture memory, reset capture counts, 
# NOT HERE: trigger SIB, edge counter (edgeCounter.py), clear sticky error (flags)
class Other(HpccTest):
    # Reset the user cycle counter special register    
    def DirectedResetCycleCountTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive1CompareHVectors 512               
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
            Drive0CompareLVectors 512           
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 5e-9)    
            self.env.RunCheckers(slot, slice)
            
    def DirectedResetCycleCountPlistTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN1:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 50, simple=1
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
            RandomEvenToOddVectors 50, simple=1
            I optype=BRANCH, br=RET                                                                         
            PATTERN2:                          
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomEvenToOddVectors 50, simple=1
            I optype=BRANCH, br=RET 
            
            PATTERN_START:                                                                                 
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end     
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN1] 
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end                                          
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN2]   
            %repeat 10
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end
            PATTERN_END:                                                                                   
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                    
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 50e-9, start = pattern.Resolve('eval[PATTERN_START]'))    
            self.env.RunCheckers(slot, slice)        
            
    # HDMT Ticket 18194: VectorOp ResetCycleCount is Applied to All Compressed Vectors
    def DirectedResetCycleCountCompressionTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive1CompareHVectors 512               
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
            V link=1, ctv=0, mtv=0, lrpt=7, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 512           
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = 2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, pdata, 5e-9)    
            self.env.RunCheckers(slot, slice)     

    # IRPT gets missed
    def DirectedResetCycleCountIRPTTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive1CompareHVectors 512               
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTCYCLCNT
            Repeat 7
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 512           
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = 2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, pdata, 5e-9)    
            self.env.RunCheckers(slot, slice) 
            
    # Resets total fail count and pin fail counters via internal PVC. 
    def DirectedResetCaptureCountTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive1CompareHVectors 512               
            I optype=VECTOR, vptype=VPOTHER, vpop=RSTFAILCNT
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 1 fail
            Drive0CompareLVectors 512           
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 5e-9)    
            self.env.RunCheckers(slot, slice)     
            
            totalFailCount = hpcc.ac[slice].Read('TotalFailCount').Pack()
            if totalFailCount != 1:
                self.Log('error', 'totalFailCount = {}, expect 1'.format(totalFailCount))   
            for ch in range(56):
                channelFailCount = hpcc.ac[slice].ReadChannel(ch, 'ChannelFailCount').Pack()
                if ch == 1:
                    if channelFailCount != 1:
                        self.Log('error', 'ch {}, fail count {}, expect 1'.format(ch, channelFailCount))   
                elif channelFailCount != 0:    
                    self.Log('error', 'ch {}, fail count {}, expect 0'.format(ch, channelFailCount))     


    # Failing cycle captures are blocked from being recorded into capture memory in Store Only Fails mode.  Capture All and Capture This Vector events still capture.    
    # No updates the Total Failing Cycles Counter for any failing pins occur.
    # No updates the Per-Pin Fail Counters for any failing pins occur.
    def DirectedBlockFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            %repeat 512
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end               
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=1
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1   
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 5e-9, captureFails = True, captureCTVs = False, captureAll = False)    
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
            if hpcc.ac[slice].Read('TotalCaptureCount').Pack() != 5:
                self.Log('error', 'TotalCaptureCount = {}, expect 5'.format(hpcc.ac[slice].Read('TotalCaptureCount').Pack()))
            if hpcc.ac[slice].Read('TotalFailCount').Pack() != 5:
                self.Log('error', 'TotalFailCount = {}, expect 5'.format(hpcc.ac[slice].Read('TotalFailCount').Pack()))
            if hpcc.ac[slice].ReadChannel(55, 'ChannelFailCount').Pack() != 5:
                self.Log('error', 'channel 55 fail count = {}, expect 5'.format(hpcc.ac[slice].ReadChannel(55, 'ChannelFailCount').Pack()))
            if hpcc.ac[slice].ReadChannel(0, 'ChannelFailCount').Pack() != 0:
                self.Log('error', 'channel 0 fail count = {}, expect 0'.format(hpcc.ac[slice].ReadChannel(0, 'ChannelFailCount').Pack()))
            if hpcc.ac[slice].ReadChannel(1, 'ChannelFailCount').Pack() != 0:
                self.Log('error', 'channel 1 fail count = {}, expect 0'.format(hpcc.ac[slice].ReadChannel(1, 'ChannelFailCount').Pack()))
            
    # Failing cycle captures are blocked from being recorded into capture memory in Store Only Fails mode.  Capture All and Capture This Vector events still capture.
    # No updates to the Channel Fail Status Register for any failing pins occur.
    # Capture All, fail vector will be captured, but fail flag will be off
    def DirectedBlockFailCaptureAllTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            %repeat 512
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %end               
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=1
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1   
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()
        
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)   
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 5e-9, captureFails = True, captureCTVs = False, captureAll = True)    
            self.env.RunCheckers(slot, slice)
            
            if hpcc.ac[slice].Read('TotalCaptureCount').Pack() != 522:
                self.Log('error', 'TotalCaptureCount = {}, expect 5'.format(hpcc.ac[slice].Read('TotalCaptureCount').Pack()))
            if hpcc.ac[slice].Read('TotalFailCount').Pack() != 5:
                self.Log('error', 'TotalFailCount = {}, expect 5'.format(hpcc.ac[slice].Read('TotalFailCount').Pack()))
            if hpcc.ac[slice].ReadChannel(55, 'ChannelFailCount').Pack() != 5:
                self.Log('error', 'channel 55 fail count = {}, expect 5'.format(hpcc.ac[slice].ReadChannel(55, 'ChannelFailCount').Pack()))
            if hpcc.ac[slice].ReadChannel(0, 'ChannelFailCount').Pack() != 0:
                self.Log('error', 'channel 0 fail count = {}, expect 0'.format(hpcc.ac[slice].ReadChannel(0, 'ChannelFailCount').Pack()))
            if hpcc.ac[slice].ReadChannel(1, 'ChannelFailCount').Pack() != 0:
                self.Log('error', 'channel 1 fail count = {}, expect 0'.format(hpcc.ac[slice].ReadChannel(1, 'ChannelFailCount').Pack()))
            
    # Errors are prevented from stopping pattern execution due to Stop On First Fail mode.    
    def DirectedBlockFailStopOnFirstFailTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=1
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200000                                                                      # 20K > 16K
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=0
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            completed, endstate = self.env.RunPattern(slot, slice, pdata, 5e-9, stopOnMaxFail = False, maxFailCaptureCount = 1, maxFailCountPerPatern = 1, captureAll = False, captureFails = True)
            #print(hex(endstate))
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            #self.env.DumpCapture(slot, slice)
            stopOnFail = 'StopOnFirstFail' in alarms
            if stopOnFail:
                self.Log('warning', 'Caught StopOnFirstFailAlarm, expected')
            else:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
                
    # Errors are prevented from stopping pattern execution due to Stop On First Fail mode. 
    # Sticky errors still occur/propagate in this mode.
    def DirectedBlockFailStopOnFirstFailAbortTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=1
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            Drive0CompareLVectors 200000                                                                      # 20K > 16K
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1 fail
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)
        pdata = pattern.Generate()

        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            data1 = hpcc.ac[slice].Read('TriggerControl')
            data1.DomainMaster = 1
            hpcc.ac[slice].Write('TriggerControl', data1)
            completed, endstate = self.env.RunPattern(slot, slice, pdata, 5e-9, stopOnMaxFail = True, maxFailCaptureCount = 1, maxFailCountPerPatern = 1, captureAll = False, captureFails = True)
            #print(hex(endstate))
            passed, alarms = self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice) 
            flags = hpcc.ac[slice].Read('CentralDomainRegisterFlags')
            if flags.LocalStickyError != 1 or flags.DomainStickyError != 1 or flags.GlobalStickyError != 1:
                self.Log('error', flags)
            
            data1 = hpcc.ac[slice].Read('TriggerControl')
            data1.DomainMaster = 0
            hpcc.ac[slice].Write('TriggerControl', data1)
    
    # resetCycleCount, resetCaptureCount
    def RandomVpopTest(self):    
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [3, 5], 'LEN_PAT': [1000, 2000], 'VPOP': 0.001} 
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('vpop.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice)             
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'))
            self.env.RunCheckers(slot, slice)  
    
    # 12.7.2, drain pin fifo to 2500 may cause pvc cache underrun on slice 1. 3000 is good
    def RandomDrainFIFOUnderrunTest(self):    
        self.env.SetConfig('EvenToOddLoopback')
        for (slot, slice) in self.env.fpgas:  
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            
            period = 1.25e-9   
            PADDING_BEFORE = random.randint(16000,36000)
            PADDING_AFTER = 2000
            PADDING_STEP = 2000            
            while (PADDING_AFTER < 30000):    
                asm = PatternAssembler()
                asm.LoadString("""\
                    S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
                    V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                    I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
                    Drive0CompareLVectors length={} 
                    I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm=2500
                    Drive0CompareLVectors length={} 
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xaaaa                                      
                    S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
                """.format(PADDING_BEFORE, PADDING_AFTER))
                pattern = asm.Generate()
                #asm.SaveObj('drainFIFO.obj')
                
                completed, endStatus = self.env.RunPattern(slot, slice, pattern, period, captureType = ac_registers.CaptureControl.FAIL_STATUS, captureFails = True, captureCTVs = False, captureAll = False)           
                passing, alarm = self.env.RunCheckers(slot, slice, endStatusMask = 0xFFFFFFFF)
                    
                self.env.Log('info', 'period = {}, padding before = {}, padding after = {}, passed = {}'.format(period, PADDING_BEFORE, PADDING_AFTER, passing))
                PADDING_AFTER += PADDING_STEP
        