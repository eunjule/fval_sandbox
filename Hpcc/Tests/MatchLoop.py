################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class BasicFlatPattern(HpccTest):

    def DirectedReadDataGapShmooTest(self):
        '''schmoo the period, checking the read data gap'''
        self.env.SetConfig('InternalLoopback')
        pattern = self._pattern(1000000)
        pattern_data = pattern.Generate()
        pattern_start = pattern.Resolve('eval[PATTERN_START]')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            ac = hpcc.ac[slice]
            step_size = (ac.maximum_tester_period(subcycle=2) - ac.minimum_tester_period(subcycle=2)) / 100
            info = []
            pat_reg = hpcc.ac[slice].Read('PatternEndStatus').value
            for i in range(100):
                period = ac.minimum_tester_period(subcycle=2) + (i * step_size)
                self.Log('info', 'Setting period to {}'.format(period))
                # self.Log('info', ac.read_register(ac_registers.PatternControl))
                self.env.RunPattern(slot, slice, pattern_data, start=pattern_start, period=period, captureAll=False)
                patternStart = hpcc.ac[slice].Read('PatternControl').VectorProcessorStarted
                ac.CheckAlarms()
                request_interval = ac.read_register(ac_registers.PinDdrRequestControl).RequestInterval
                read_data_gap = ac.read_register(ac_registers.PinDdrStatus).ReadDataGap
                info.append((period, request_interval, read_data_gap))
            for d in info:
                self.Log('info', 'period={:3.3f}, request_interval={:3d}, read_data_gap={:3d}'.format(d[0] * 1e9, d[1], d[2]))
            if patternStart == 0:
                self.Log('error', 'pattern not executed')

    def xDirectedRequestDurationShmooTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._pattern(1000000)
        pattern_data = pattern.Generate()
        pattern_start = pattern.Resolve('eval[PATTERN_START]')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            ac = hpcc.ac[slice]
            step_size = (ac.maximum_tester_period(subcycle=3) - ac.minimum_tester_period(subcycle=2)) / 100
            info = []
            for i in range(100):
                period = ac.minimum_tester_period(subcycle=2) + (i * step_size)
                self.Log('info', 'Setting period to {}'.format(period))
                self.Log('info', ac.read_register(ac_registers.PatternControl))
                for duration in reversed(range(56)):
                    self._set_request_duration(ac, duration)
                    self.env.RunPattern(slot, slice, pattern_data, start=pattern_start, period=period, captureAll=False)
                    alarms = ac.CheckAlarms()
                    if set(['PVCCacheUnderRun', 'StackError']) & set(alarms):
                        info.append((period, duration + 1))
                        break
            for d in info:
                self.Log('info', 'period={:3.3f}, request_duration={:3d}'.format(d[0] * 1e9, d[1]))

    def xDirectedRequestSizeTest(self):
        self.env.SetConfig('InternalLoopback')
        pattern = self._pattern(100000)
        pattern_data = pattern.Generate()
        pattern_start = pattern.Resolve('eval[PATTERN_START]')
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            ac = hpcc.ac[slice]
            period = ac.minimum_tester_period(subcycle=3)
            self._request_size_test(ac, pattern_data, pattern_start, period)
    
    def _request_size_test(self, ac, pattern_data, pattern_start, period):
        gaps = []
        for i in reversed(range(30, 100)):
            self._set_request_interval(ac, i)
            self.env.RunPattern(ac.slot, ac.slice, pattern_data, start=pattern_start, period=period, captureAll=False)
            ac.CheckAlarms()
            xy = (i, ac.read_register(ac_registers.PinDdrStatus).ReadDataGap)
            gaps.append(xy)
            if xy[1] < 3:
                break
        for x, y in gaps:
            self.Log('info', 'request interval={:3d}, data gap={:3d} {}*'.format(x, y, ' '*y))
    
    def _set_request_interval(self, ac, interval):
        r = ac.read_register(ac_registers.PinDdrRequestControl)
        r.RequestInterval = interval
        ac.write_register(r)
    
    def _set_request_duration(self, ac, duration):
        r = ac.read_register(ac_registers.PinDdrRequestControl)
        r.RequestDuration = duration
        ac.write_register(r)
    
    def _pattern(self, length):
        # pattern = PatternAssembler()
        # pattern.symbols['REPEATS'] = length
        # pattern.LoadString("""\
                    # S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
                    # %repeat REPEATS
                    # V link=0, ctv=0, mtv=1, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
                    # %end
                    # I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                    # S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
                # """)
        # return pattern.Generate()
        constraints = {'NUM_PAT': [500, 500], 'LEN_PAT': [520, 520], 'Meta': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD')
        
        pattern = PatternAssembler()
        pattern.LoadString(p.GetPattern())
        return pattern
        
    