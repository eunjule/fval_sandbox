################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: VectorCompression.py
#-------------------------------------------------------------------------------
#     Purpose: Vector compression (channel linking, local repeat, etc) tests
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 04/22/15
#       Group: HDMT FPGA Validation
################################################################################

import random

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG
from Hpcc.Tests.hpcctest import HpccTest
import Hpcc.instrument.hpccAcRegs as ac_registers


class ChannelLinking(HpccTest):
    def DirectedLinkMode2xTest(self):
        self.BasicScenario(0, 0, 20e-9, 0)

    def DirectedLinkMode4xTest(self):
        self.BasicScenario(0, 0, 20e-9, 1)

    def DirectedLinkMode8xTest(self):
        self.BasicScenario(0, 0, 20e-9, 2)

    def BasicScenario(self, slice, offset, period, linkMode):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %repeat REPEATS
            V link=1, ctv=0, mtv=0, lrpt=0, data=0v00000000101010101010101010101010101010101010101011111111
            %end
            PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = linkMode
            data = hpcc.ac[slice].Write('PatternControl', data)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)

    def RandomLinkMode2xTest(self):
        self._RandomScenario(0, 0, 20e-9, 0)

    def RandomLinkMode4xTest(self):
        self._RandomScenario(0, 0, 20e-9, 1)

    def RandomLinkMode8xTest(self):
        self._RandomScenario(0, 0, 20e-9, 2)

    def _RandomScenario(self, slice, offset, period, linkMode):
        self.env.SetConfig('EvenToOddLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            S stype=IOSTATEJAM, data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            RandomPassingEvenToOddNoSwitchVectors length=REPEATS
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = linkMode
            data = hpcc.ac[slice].Write('PatternControl', data)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)
        
    # TODO: confirm no switch in link vector
    def RandomChannelLinkingTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CLINK': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('link.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode'] # 0,1,2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)


class LocalRepeat(HpccTest):
    def Directed1KVTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=1, data=0v00000000101010101010101010101010101010101010101011111111
            %end
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)

    def Random1KVTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            S stype=IOSTATEJAM, data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ
            %repeat REPEATS
            V link=0, ctv=0, mtv=0, lrpt=`rand(4)`, data=`rand(56)`
            %end
            StopPattern 0xdeadbeef
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            # Run the pattern
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)

    # TODO: confirm no switch in lrpt vector
    def RandomLocalRepeatTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
        
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'LRPT': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('lrpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice)             
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)

class Mix(HpccTest):
    # found in 12.3.2, fixed in 12.4.2
    def ThrReversedLinkCountTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 100e-9
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 100
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            %end
            Repeat 10                                                                           
            V link=1, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            V link=1, ctv=0, mtv=0, lrpt=0, data=0v1111L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = 1
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, pdata, period, captureType = 0, captureAll = False, captureFails = True)
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)
            
    # HDMT Ticket 18460        
    def DirectedBranchCompressedVectorTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        period = 1.25e-9
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 1000
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            %end            
            Call `PATTERN1`
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
            
            PATTERN1:
            #Drive0CompareLVectors length=1
            V link=1, ctv=1, mtv=0, lrpt=0, data=0vH1H1H1H1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0H1  
            Drive0CompareLVectors length=256
            Return
        """)
        pdata = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = 1
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, pdata, period, captureType = 0, captureAll = False, captureFails = True, captureCTVs = True)
            self.env.RunCheckers(slot, slice)  
            hpcc.DumpCapture(slice)

    def DirectedWorstCase31LrptAnd8LinkTest(self):
        self.env.SetConfig('EvenToOddLoopback')        
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=31, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %repeat 100
            V link=1, ctv=0, mtv=0, lrpt=31, data=0vL1L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            period = 1.25e-9 #hpcc.ac[0].minQdrMode / PICO_SECONDS_PER_SECOND
            
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = 2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, pdata, period, captureType = 1, captureAll = True)
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)

    #basic lrpt + channel link test, 31 lrpt and x2 CL, capture all
    def DirectedBasic31LrptAnd2LinkCapAllTest(self):
        self._BasicLrptAndLinkScenario(31, 0, True, False)

    #basic lrpt + channel link test, 31 lrpt and x4 CL, capture all    
    def DirectedBasic31LrptAnd4LinkCapAllTest(self):
        self._BasicLrptAndLinkScenario(31, 1, True, False) 

    #basic lrpt + channel link test, 31 lrpt and x8 CL, capture all        
    def DirectedBasic31LrptAnd8LinkCapAllTest(self):
        self._BasicLrptAndLinkScenario(31, 2, True, False)

    #basic lrpt + channel link test, random LRPT and random CL, capture all
    def DirectedBasicRandomLrptAndRandomLinkCapAllTest(self):
        self._BasicLrptAndLinkScenario(random.randint(1,30), random.randint(0,2), True, False)

    #basic lrpt + channel link test, 31 lrpt and x2 CL, capture fails
    def DirectedBasic31LrptAnd2LinkCapFailsTest(self):
        self._BasicLrptAndLinkScenario(31, 0, False, True)

    #basic lrpt + channel link test, 31 lrpt and x4 CL, capture fails    
    def DirectedBasic31LrptAnd4LinkCapFailsTest(self):
        self._BasicLrptAndLinkScenario(31, 1, False, True)    

    #basic lrpt + channel link test, 31 lrpt and x8 CL, capture fails    
    def DirectedBasic31LrptAnd8LinkCapFailsTest(self):
        self._BasicLrptAndLinkScenario(31, 2, False, True)

    #basic lrpt + channel link test, random lrpt and random CL, capture fails
    def DirectedBasicRandomLrptAndRandomLinkCapFailsTest(self):
        self._BasicLrptAndLinkScenario(random.randint(1,30), random.randint(0,2), False, True)

    #random vector lrpt + CL test, 31 lrpt and x2 CL, capture all
    def Random31LrptAnd2LinkCapAllTest(self):
        self._RandomLrptAndLinkScenario(31, 0, True, False)

    #random vector lrpt + CL test, 31 lrpt and x4 CL, capture all    
    def Random31LrptAnd4LinkCapAllTest(self):
        self._RandomLrptAndLinkScenario(31, 1, True, False)    

    #random vector lrpt + CL test, 31 lrpt and x8 CL, capture all        
    def Random31LrptAnd8LinkCapAllTest(self):
        self._RandomLrptAndLinkScenario(31, 2, True, False)

    #random vector lrpt + CL test, random lrpt and random CL, capture all 
    def RandomLrptAndRandomLinkCapAllTest(self):
        self._RandomLrptAndLinkScenario(random.randint(1,30), random.randint(0,2), True, False)

    #random vector lrpt + CL test, 31 lrpt and x2 CL, capture fails 
    def Random31LrptAnd2LinkCapFailsTest(self):
        self._RandomLrptAndLinkScenario(31, 0, False, True)

    #random vector lrpt + CL test, 31 lrpt and x4 CL, capture fails     
    def Random31LrptAnd4LinkCapFailsTest(self):
        self._RandomLrptAndLinkScenario(31, 1, False, True)    

    #random vector lrpt + CL test, 31 lrpt and x8 CL, capture fails         
    def Random31LrptAnd8LinkCapFailsTest(self):
        self._RandomLrptAndLinkScenario(31, 2, False, True)

    #random vector lrpt + CL test, random lrpt and random CL, capture fails
    def RandomLrptAndLinkCapFailsTest(self):
        self._RandomLrptAndLinkScenario(random.randint(1,30), random.randint(0,2), False, True)

    def _BasicLrptAndLinkScenario(self, repeats, linkMode, capAll, capFails):
        self.env.SetConfig('EvenToOddLoopback')        
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeats
        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=REPEATS, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %repeat 100
            V link=1, ctv=0, mtv=0, lrpt=REPEATS, data=0vH1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0H1L0
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            period = 1.25e-9 #hpcc.ac[0].minQdrMode / PICO_SECONDS_PER_SECOND

            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = linkMode
            hpcc.ac[slice].Write('PatternControl', data)

            self.env.RunPattern(slot, slice, pdata, period, captureType = 1, captureAll = capAll, captureFails = capFails)
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)

    def _RandomLrptAndLinkScenario(self, repeats, linkMode, capAll, capFails):

        self.env.SetConfig('EvenToOddLoopback')        
        pattern = PatternAssembler()
        pattern.symbols['REPEATS'] = repeats
        pattern.symbols['RANDVECTOR'] = self._GenerateRandomLrptClVector()
        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            V link=0, ctv=0, mtv=0, lrpt=REPEATS, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            %repeat 100
            V link=1, ctv=0, mtv=0, lrpt=REPEATS, data=RANDVECTOR
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            period = 1.25e-9 #hpcc.ac[0].minQdrMode / PICO_SECONDS_PER_SECOND

            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = linkMode
            hpcc.ac[slice].Write('PatternControl', data)

            self.env.RunPattern(slot, slice, pdata, period, captureType = 1, captureAll = capAll, captureFails = capFails)
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)

    def _GenerateRandomLrptClVector(self):

        randchoice = ["0","0","0","0","1"] #choice array for random vector generation, 20% drive high

        #create vector data
        randvectordata = ["1"] #guarantee at least one drive high
        for i in range(27): #remainder of vectors random, 20% chance to drive high
            randvectordata.append(random.choice(randchoice))   

        #randomize the vector drive data
        random.shuffle(randvectordata)

        #assemble the pattern vector, always compare low
        randvector = "0v" 
        for data in randvectordata:
            randvector = randvector + "L" + data

        return randvector

    # TODO: confirm no switch in link vector
    def RandomLrptChannelLinkTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CLINK': 0.1, 'LRPT': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('linklrpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode'] # 0,1,2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)
            
    # CQ13400 Pattern Not Compelte in 12.2.2/12.3.2
    # CQ13808 PVC Cache Underrun in 12.4.2
    def RandomLrptChannelLinkCQTest(self):
        self.env.OverrideTestSeed(2861067085514914222)
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CLINK': 0.1, 'LRPT': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('linklrpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode'] # 0,1,2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = False, captureAll = False, captureFails = False, captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice) 
            self.Log('info', 'Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
            self.Log('info', 'New Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl1').Pack()))
            #self.env.DumpCapture(slot, slice)
            
    # underruns on 24.1.2
    def RandomLrptChannelLinkUnderrunTest(self):
        self.env.OverrideTestSeed(4140094422095092291)
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'CLINK': 0.1, 'LRPT': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('linklrpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode'] # 0,1,2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = False, captureAll = False, captureFails = False, captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice) 
            self.Log('info', 'Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
            self.Log('info', 'New Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl1').Pack()))
            #self.env.DumpCapture(slot, slice)
            
    def RandomIrptLrptChannelLinkTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 500], 'LEN_PAT': [1000, 2000], 'RPT': 0.002, 'LRPT': 0.1, 'CLINK': 0.1}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('linklrpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            # Program channel linking mode
            hpcc = self.env.instruments[slot]
            data = hpcc.ac[slice].Read('PatternControl')
            data.LinkMode = param['linkMode'] # 0,1,2
            hpcc.ac[slice].Write('PatternControl', data)
            
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)
            
    def RandomCompressionCCWRTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [1, 1], 'LEN_PAT': [1000, 2000], 'RPT': 0.002, 'LRPT': 0.1} #'CLINK': 0.1
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('linklrpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice)             
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = True, captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)

class InstructionRepeat(HpccTest):
    def DirectedImmediateTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            Repeat 13
            RandomVectors length=1
            Repeat 3
            RandomVectors length=1
            Repeat 5
            RandomVectors length=1
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)

    def DirectedRegisterTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=256
            SetRegister 0, 13
            RepeatR 0
            RandomVectors length=1
            SetRegister 0, 3
            RepeatR 0
            RandomVectors length=1
            SetRegister 1, 5
            RepeatR 1
            RandomVectors length=1
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern)
            self.env.RunCheckers(slot, slice)

    def DirectedRepeat64Test(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=32768
            Repeat 64
            RandomVectors length=1
            RandomVectors length=75
            StopPattern 0x69696969
        """)
        #asm.SaveObj('Repeat64.obj')
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = 10e-9)
            self.env.RunCheckers(slot, slice)
    
    def DirectedIPRTIOJamTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            %repeat 100     
                Repeat 5
                S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 2, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 3, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 4, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 5, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 6, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 7, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 8, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 9, 
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 10,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 11,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 12,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 13,
                Repeat 5
                S stype=IOSTATEJAM,             data=0jZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZX  
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 14,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 15,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0  # 16,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0  # 17,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0  # 18,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0  # 19,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0  # 20,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0  # 21,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0  # 22,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0  # 23,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0  # 24,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0  # 25,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0  # 26,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0  # 27,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0  # 28,
                V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1  # 29,
            %end
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        offset = 0
        pdata = pattern.Generate()
        patternSize = len(pdata)
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 10e-9, captureType = ac_registers.CaptureControl.FAIL_STATUS)            
            self.env.RunCheckers(slot, slice)
            #self.env.DumpCapture(slot, slice)
    
    # 12.4.2 fails @ 2.5ns
    # 24.0.2 fails @ 5ns
    def DirectedRepeat61PrimeNumberTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=7
            %repeat 1000
            Repeat 61
            RandomVectors length=1
            %end
            RandomVectors length=7
            StopPattern 0x69696969
        """)
        #asm.SaveObj('Repeat64.obj')
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = 5e-9)
            self.env.RunCheckers(slot, slice)
            
    # to test capture block split when repeat addr is the same
    def DirectedRepeatLoopTest(self):
        self.env.SetConfig('InternalLoopback')
        # Create the pattern
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            RandomVectors length=1000
            SetRegister 0, 10  
            LOOP:
                Repeat 61
                RandomVectors length=1
                DecRegister 0
                GotoIfNonZero `LOOP`  
            StopPattern 0x69696969
        """)
        pattern = asm.Generate()
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            self.env.RunPattern(slot, slice, pattern, period = 5e-9)
            self.env.RunCheckers(slot, slice)
            
    def RandomInstructionRPTTest(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()
                    
        constraints = {'NUM_PAT': [5, 100], 'LEN_PAT': [1000, 2000], 'RPT': 0.002}
        p = RPG(constraints = constraints)
        p.GeneratePlist('RANDOM_EVEN_TO_ODD') 
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        patternData = pattern.Generate()
        #pattern.SaveObj('rpt.obj')
        
        for (slot, slice) in self.env.fpgas:
            # randomized parameters
            param = self.env.RandomizeParameters(slot, slice, patternData, 2000)
            
            self.env.LogPerSlice(slot, slice) 
            self.env.RunPattern(slot, slice, patternData, param['period'], start = pattern.Resolve('eval[PATTERN_START]'), captureCTVs = param['captureCTVs'], captureAll = param['captureAll'], captureFails = param['captureFails'], captureAddress = param['captureAddress'])
            self.env.RunCheckers(slot, slice)  
            #self.env.DumpCapture(slot, slice)
            
    def DirectedIRPTFirstFailBeforeRPT(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 62, fail
            %repeat 10                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            Repeat 20000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 5e-9, captureAll = False, captureFails = True, stopOnMaxFail = True, maxFailCaptureCount = 1)            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            #self.env.DumpCapture(slot, slice)
            if 'StopOnFirstFail' not in alarms:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            if hpcc.ac[slice].Read('PatternEndStatus').Pack() != 0x1800dead:
                self.Log('error', 'Expect pattern end status 0x1800dead, actual end status 0x{:X}'.format(hpcc.ac[slice].Read('PatternEndStatus').Pack())) 
            
    def DirectedIRPTFirstFailAtRPT(self):
        self.env.SetConfig('EvenToOddLoopback')
        pattern = PatternAssembler()

        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ  # 0
            %repeat 20000                                                                               
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  # 1-50, 
            %end
            Repeat 20000
            V link=0, ctv=1, mtv=0, lrpt=0, data=0vL1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            PATTERN_END:                                                                                     # 1022
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
        """)
        pdata = pattern.Generate()
        # pattern.SaveObj('eventoodd.obj') 
        patternSize = len(pdata)
        for (slot, slice) in self.env.fpgas:
            self.env.LogPerSlice(slot, slice)
            hpcc = self.env.instruments[slot]
            self.env.RunPattern(slot, slice, pdata, 5e-9, captureAll = False, captureFails = True, stopOnMaxFail = True, maxFailCaptureCount = 1)            
            passed, alarms = self.env.RunCheckers(slot, slice, alarmsToMask = ['StopOnFirstFail'])
            #self.env.DumpCapture(slot, slice)
            if 'StopOnFirstFail' not in alarms:
                self.Log('error', 'Expect StopOnFirstFailAlarm, fail to catch.')
            if hpcc.ac[slice].Read('PatternEndStatus').Pack() != 0x1800dead:
                self.Log('error', 'Expect pattern end status 0x1800dead, actual end status 0x{:X}'.format(hpcc.ac[slice].Read('PatternEndStatus').Pack()))            

