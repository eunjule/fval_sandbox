################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: runcal.py
#-------------------------------------------------------------------------------
#     Purpose: stand alone, run calibration workbench
#-------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 2/2/17
#       Group: HDMT FPGA Validation
################################################################################
if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    

import argparse
from Common import fval
from Common.fval import core
from Common.instruments.tester import get_tester


def Main():

    parser = argparse.ArgumentParser()
    parser.add_argument('slot',   help = 'Slot that dev board is in', type = str, action = "store", default = None)
    parser.add_argument('-v', '--verbosity', help = 'Set verbosity level', type = str, action = "store", default='15')

    args = parser.parse_args()

    # set logging level
    if args.verbosity.isdigit():
        fval.SetLoggerLevel(int(args.verbosity))
    elif args.verbosity.upper() == "CRITICAL":
        fval.SetLoggerLevel(50)
    elif args.verbosity.upper() == "ERROR":
        fval.SetLoggerLevel(40)
    elif args.verbosity.upper() == "WARNING":
        fval.SetLoggerLevel(30)
    elif args.verbosity.upper() == "INFO":
        fval.SetLoggerLevel(20)
    elif args.verbosity.upper() == "DEBUG":
        fval.SetLoggerLevel(10)
    else:
        raise RuntimeError("Verbosity \'{}\' is not supported".format(args.verbosity))

    # enable logger, part of fval core, is inherited through tester.py
    fval.ConfigLogger()

    # Check for valid slot
    validSlots = range(0, 11)
    print(args.slot)
    slots = None
    if not(args.slot is None):
        slots = []
        for slot in args.slot.split(','):
             try:
                 if int(slot) in validSlots:
                     print("Slot under test: {}".format(slot))
                     slots.append(int(slot))
                 else:
                     raise ValueError
             except ValueError:
                 parser.error("Invalid Slot {}, must be between 0-11".format(slot))

    # create a tester hardware instance
    tester = get_tester()
    tester.discover_all_instruments()
    #tester.duts.append('hpcc')
    #dutset = set(tester.duts)
    #tester.duts = list(dutset)

    # Create an Rc2 instance needed for calibration to send sync pulse
    rc_instrument = tester.get_tester_rc()

    # Load RC and set it up
    tester.CommonInitialize()

    # basic init of backplane and cal base
    tester.backplane_calbase_init()


    for slot in slots:

        # Create a HPCC Instrument instance for each card under test
        hpcc_instrument = tester.get_undertest_instruments()[slot]

        # Initialize the calbase
        hpcc_instrument.InitializeCal()

        # Initialize HPCC, load fpga's, set up clocks
        hpcc_instrument.Initialize()

        # # set up structures for AC
        # for ac in hpcc_instrument.ac:
        #     ac.structs = structs

        hpcc_instrument.calibration()

if __name__ == '__main__':
        Main()