################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: runcal.py
#-------------------------------------------------------------------------------
#     Purpose: stand alone, run calibration workbench
#-------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 2/2/17
#       Group: HDMT FPGA Validation
################################################################################
if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
import argparse
import time

from Common import fval
from Hpcc.hpcctb import disassembler
from Hpcc.hpcctb.assembler import PatternAssembler

def Main():

    parser = argparse.ArgumentParser(description = 'PATGEN TOOL')
#    parser.add_argument('type',   help = 'LTH or ETO', type = str, action = "store", default = None)
    parser.add_argument('length',   help = 'length of random vectors in pattern', type = str, action = "store", default = None)

    parser.add_argument('-v', '--verbosity', help = 'Sset verbosity level', type = str, action = "store", default='15')

    args = parser.parse_args()

    # set logging level
    if args.verbosity.isdigit():
        fval.SetLoggerLevel(int(args.verbosity))
    elif args.verbosity.upper() == "CRITICAL":
        fval.SetLoggerLevel(50)
    elif args.verbosity.upper() == "ERROR":
        fval.SetLoggerLevel(40)
    elif args.verbosity.upper() == "WARNING":
        fval.SetLoggerLevel(30)
    elif args.verbosity.upper() == "INFO":
        fval.SetLoggerLevel(20)
    elif args.verbosity.upper() == "DEBUG":
        fval.SetLoggerLevel(10)
    else:
        raise RuntimeError("Verbosity \'{}\' is not supported".format(args.verbosity))

    # enable logger, part of fval core, is inherited through tester.py
    fval.ConfigLogger()

    patLength = int(args.length)

    OddPattern = PatternAssembler()
    EvenPattern = PatternAssembler()


    ioJamForOdd = 'S stype=0, data=0xeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    ioJamForEven = 'S stype=0, data=0xbbbbbbbbbbbbbbbbbbbbbbbbbbbb'


    #Create odd pat

    OddPattern.LoadString("""
              {}
              BlockFails 1
              Drive0CompareLVectors length=1024
              BlockFails 0
              RandomPassingEvenToOddNoSwitchVectors length={}
              StopPattern 0x12345678
          """.format(ioJamForOdd,patLength))
    OddPattern.Generate()
    diassemblerObj = disassembler.Pattern()
    OddPatternPatObj = OddPattern.cachedObj
    vectorCount = int(len(OddPatternPatObj) / 16)
    OddPatternArray = []
    for i in range(vectorCount):
        disVec = diassemblerObj.disassemble(OddPatternPatObj, i * 16)
        OddPatternArray.append(disVec)


    #even pattern
    EvenPattern.LoadString("""
              {}
              BlockFails 1
              Drive0CompareLVectors length=1024
              BlockFails 0
              RandomPassingEvenToOddNoSwitchVectors length={}
              StopPattern 0x12345678
          """.format(ioJamForEven,patLength))
    EvenPattern.Generate()
    diassemblerObj = disassembler.Pattern()
    EvenPatternPatObj = EvenPattern.cachedObj
    vectorCount = int(len(EvenPatternPatObj) / 16)
    EvenPatternArray = []
    for i in range(vectorCount):
        disVec = diassemblerObj.disassemble(EvenPatternPatObj, i * 16)
        EvenPatternArray.append(disVec)

    # print("Even PATTERN")
    # for vec in EvenPatternArray:
    #     print(vec)

    EvenPatternArray[1] =  'BlockFails 1'
    EvenPatternArray[2+1024] = 'BlockFails 0'
    EvenPatternArray[3+1024+patLength] = 'StopPattern    0x12345678'
    EvenPatternArray = EvenPatternArray[:4+1024+patLength]

    for x in range(patLength):
        EvenPatternArray[1024 + 3 + x] = OddPatternArray[1024 + 3 + x]

    EvenPatternString = '\n'.join(EvenPatternArray)
    EvenPatternNew = PatternAssembler()
    EvenPatternNew.LoadString(EvenPatternString)
    EvenPatternNew.Generate()

    #save Patterns to files
    currentTime = time.time()
    OddPattern.SaveObj('Calibration_even_to_odd_odd_pins_{}.obj.gz'.format(currentTime))
    EvenPatternNew.SaveObj('Calibration_even_to_odd_even_pins_{}.obj.gz'.format(currentTime))

    # print("Even PATTERN")
    # for vec in OddPatternArray:
    #      print(vec)





if __name__ == '__main__':
        Main()