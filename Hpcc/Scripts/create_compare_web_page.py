# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import os

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', nargs='+')
    return parser.parse_args()


if __name__ == '__main__':
  try:
    args = parse_args()
    files = set([file for dir in args.directory for file in os.listdir(dir) if file.endswith('.html')])
    file_name = 'compare_' + '_to_'.join([os.path.basename(x) for x in args.directory]) + '.html'
    title = 'Comparison between ' + ' and '.join(args.directory)
    with open(file_name, 'w') as f:
        print('<html>', file=f)
        print('<header>', file=f)
        print(f'<title>{title}</title>', file=f)
        print('</header>', file=f)
        print('<body>', file=f)
        print(f'{title}<br/>', file=f)
        print('<table width="100%" cellspacing = "10">', file=f)
        
        for file in sorted(files):
             print('<tr>', file=f)
             for d in args.directory:
                print(f'<th>{d}</th>', file=f)
             print('</tr>', file=f)
             shmoo_name = file.rsplit('.', maxsplit=1)[0]
             print(f'<tr><td colspan="{len(args.directory)}"><b>{shmoo_name}</b></td></tr>', file=f)
             print('<tr>', file=f)
             for d in args.directory:
                print(f'<td><iframe width="100%", height="800", src="{d}/{file}"></iframe></td>', file=f)
             print('</tr>', file=f)
        
        
        print('</table>', file=f)
        print('</body>', file=f)
        print('</html>', file=f)
        
  except Exception as e:
        print(e)
        import code
        code.interact(local=locals())
        