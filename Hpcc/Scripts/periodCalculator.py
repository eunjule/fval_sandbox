################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: periodCalculator.py
#-------------------------------------------------------------------------------
#     Purpose: domain period and 9914 period, sub cycle count calculator
#              Note: no boundary checking, please be responsible for the values you entered
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 06/25/15
#       Group: HDMT FPGA Validation
################################################################################

import math

def FromDomainPeriod(domainPeriod, minQdrMode):
    subCycleCount = -1
    maxQdrMode = minQdrMode * 2
    maxDdrMode = maxQdrMode * 2
    maxSdrMode = maxQdrMode * 4 
    maxPeriod = minQdrMode * 256
    if domainPeriod < minQdrMode:
        raise Exception('Domain period {} ns too small. Min Qdr = {} ns'.format(domainPeriod, minQdrMode))
    elif domainPeriod <= maxQdrMode:
        domainQuarterCycle = domainPeriod
        subCycleCount = 0
    elif domainPeriod <= maxDdrMode:
        domainQuarterCycle = domainPeriod / 2
        subCycleCount = 1
    elif domainPeriod <= maxSdrMode:
        domainQuarterCycle = domainPeriod / 4
        subCycleCount = 2
    elif domainPeriod <= maxPeriod:
        repeat = math.ceil(domainPeriod / maxQdrMode)
        repeat -= 1
        power2exp = 8
        while power2exp > 3:
            if (repeat >> (power2exp - 1)) == 1:
                break
            power2exp -= 1
        domainQuarterCycle = domainPeriod / (1 << power2exp)
        subCycleCount = power2exp
    else:
        raise Exception('Domain period {} ns too large. Max period = {} ns'.format(domainPeriod, maxPeriod))
        
    print("internal 9914 clock period = {} ns, subcycle count = {}".format(2 * domainQuarterCycle, subCycleCount))
    
def FromInternalPeriod(internalPeriod, subcycle):
    domainQuarterCycle = internalPeriod / 2
    domainPeriod = domainQuarterCycle * (1 << subcycle)
    print("domain period = {} ns".format(domainPeriod))
    
minQdrMode = input('Enter minQdr (ns), typical value 1.18 / 2.5: ')
minQdrMode = float(minQdrMode)
mode = input('choose (1: DomainPeriod to InternalPeriod, 2: InternalPeriod to DomainPeriod) : ')
if mode == '1':
    domainPeriod = input('Enter domain period (ns): ')
    domainPeriod = float(domainPeriod)
    FromDomainPeriod(domainPeriod, minQdrMode)
else:
    subcycle = input('Enter sub cycle count: ')
    subcycle = int(subcycle)
    internalPeriod = input('Enter internal 9914 clock period (ns): ')
    internalPeriod = float(internalPeriod)
    FromInternalPeriod(internalPeriod, subcycle)