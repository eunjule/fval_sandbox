# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Script to consume high DDR peak power

After some experimentation with an HPCC instrumented to measure current
on the DDR power rail, the shmoo included in this test was found to generate
the highest peak power of FVAL tests and shmoos.
"""

import argparse
import os
import sys
import time

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
sys.path.insert(0, os.path.abspath(repo_root_path))
    
from Common.instruments.tester import get_tester
from Common import fval
from Hpcc import hpcc_configs as configs
from Common import configs as global_configs
from Hpcc.hpcctb.multi_slot_env import MultiSlotEnv as Env
from Hpcc.hpcctb.assembler import PatternAssembler


from Hpcc.instrument import hpccAcRegs
hpccAcRegs.critical_alarms = []

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--slots', type=int, nargs='*')
    parser.add_argument('-p', '--period', type=float, default=666e-12)
    parser.add_argument('--pre_soak_time', type=int, default=300, help="Run patterns for this many seconds")
    parser.add_argument('--soak_time', type=int, default=600, help="Run patterns for this many seconds")
    parser.add_argument('--post_soak_time', type=int, default=600, help="Run patterns for this many seconds")
    parser.add_argument('--initialize', action='store_true')
    return parser.parse_args()


def _assemble_pattern():
    asm = PatternAssembler()
    asm.LoadString(Pattern())
    pattern = asm.Generate()
    start = asm.Resolve('START')
    return pattern, start


def Pattern():
    length = 10*1024*1024
    ctv_percent = 34
    non_ctv_percent = 100 - ctv_percent
    source = f"""\
        START:
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        SetRegister 0, 500
        LOOP:
            RandomPassingEvenToOddNoSwitchVectors length={length}, ctv={{0:={non_ctv_percent},1:={ctv_percent}}}
            DecRegister 0
            GotoIfNonZero LOOP
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
    """
    fval.Log('info', source)
    return source


def RunHeatingPattern(period, soak_time):
    env = Env(object())
    env.SetConfig('EvenToOddLoopback')
    capture_address, slice, start = _load_pattern(env)
    fval.Log('info', f'Running heating pattern in a loop for {soak_time} seconds')
    start_time = time.perf_counter()
    while time.perf_counter() - start_time < soak_time:
        print(f'Running pattern at period: {period}')
        _run_multislice_pattern(env, capture_address, period, start)


def _load_pattern(env):
    hpcc_list = list(env.instruments.values())
    pattern, start = _assemble_pattern()
    capture_address = hpcc_list[0].ac[0].SinglePatternCaptureBaseAddress(len(pattern))
    for hpcc in hpcc_list:
        for slice in range(2):
            print(f'Writing pattern to slot {hpcc.slot} slice {slice}')
            env.WritePattern(slot=hpcc.slot, slice=slice, offset=0, pattern=pattern)
    return capture_address, slice, start


def _run_multislice_pattern(env, capture_address, period, start):
    slots = list(env.instruments.keys())
    complete = RunMultiSlicePattern(env, slots, capture_address=capture_address, period=period, start=start)
    if not complete:
        fval.Log('warning', 'Aborting pattern')
        hpcc_list = env.instruments.values()
        for hpcc in hpcc_list:
            for slice in range(2):
                env.AbortPattern(hpcc.slot, slice)


def RunMultiSlicePattern(env, slots, pattern=None, start=0, period=10e-9, capture_address=None):
    for slot in slots:
        for slice in range(2):
            env.SetPeriodAndPEAttributes(slot, slice, period)
    return env.WriteExecuteMultiSlicePatternAndSetupCapture(slots=slots, pattern=pattern, start=start,
                                                            captureAddress=capture_address, captureType=1,
                                                            captureFails=False, captureCTVs=True, captureAll=False,
                                                            stopOnMaxFail=False, dramOverflowWrap=True,
                                                            captureLength=0x00FFFFFF)


def setup_output_directory():
    results_dir = time.strftime('ddr_heater_results_%Y_%m_%d_%H-%M-%S')
    os.mkdir(results_dir)
    os.chdir(results_dir)
    # configure logging last, so the transcript is stored in the results directory
    fval.ConfigLogger('transcript.txt')
    
    
if __name__ == '__main__':
    setup_output_directory()
    args = parse_args()
    tester = get_tester()
    devices = tester.discover_all_instruments()
    hpcc_devices = [d for d in devices if d.name() == 'Hpcc']
    if args.slots:
        hpcc_devices = [h for h in hpcc_devices if h.slot in args.slots]
    for hpcc in hpcc_devices:
        fval.Log('info', f'Using HPCC in slot {hpcc.slot}')
        tester.set_under_test('Hpcc', hpcc.slot)
    if args.initialize:
        tester.Initialize()
    else:
        configs.HPCC_ENV_INIT = 'NONE'
        global_configs.RC2_FPGA_LOAD = False
        global_configs.COMMON_INITIALIZE = False
        tester.Initialize()

    fval.Log('info', f'Waiting {args.pre_soak_time} seconds before starting heating')
    fval.Log('info', f'(This shows that the system is in a stable state before heating)')
    time.sleep(args.pre_soak_time)
    RunHeatingPattern(period=args.period, soak_time=args.soak_time)
    fval.Log('info', f'Waiting {args.post_soak_time} seconds cooldown (Ctrl-C to cancel)')
    try:
        time.sleep(args.post_soak_time)
    except KeyboardInterrupt:
        fval.Log('info', 'User cancelled cooldown')
