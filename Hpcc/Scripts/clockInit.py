################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: clockInit.py
#-------------------------------------------------------------------------------
#     Purpose: script to play with domain and hp clocks
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/18/15
#       Group: HDMT FPGA Validation
################################################################################

import argparse
import os
import sys

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
sys.path.append(os.path.abspath(repo_root_path))

from Common import configs
from Common import fval
from Common.instruments.tester import get_tester
from Common.instruments.hpcc.hpcc import Hpcc


class MockTest(object):
    _softErrors = []
    def Name(self):
        return 'PostReplaySnapshot'

configs.COMMON_INITIALIZE = True
fval.SetLoggerLevel(20)  # DEBUG=10, INFO=20, WARNING=30

# Parse command line options
parser = argparse.ArgumentParser(description = 'Snapshot replay tool')
parser.add_argument('slot', help = 'HPCC card slot', type = int)
#parser.add_argument('period', help = 'Period to execute the snapshot', type = float)
args = parser.parse_args()
slot = args.slot
tester = get_tester()
tester.discover_all_instruments()
tester.CommonInitialize()
hpcc = Hpcc(slot = slot, name = 'Hpcc[{}]'.format(slot), rc = tester.get_tester_rc())
hpcc.Initialize()


''' Customer code starts here '''
hpcc.ac[0].ad9914HP.EnableOutput(False)
hpcc.ac[0].ad9914HP.WritePeriod(10e-9)
hpcc.ac[0].ad9914HP.EnableOutput(True)
''' Customer code ends here '''
