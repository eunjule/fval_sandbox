################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: onlineShmoos.py
#-------------------------------------------------------------------------------
#     Purpose: Script that calculates instruction density shmoos on the tester
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 09/16/15
#       Group: HDMT FPGA Validation
################################################################################

import argparse
import os
import sys
import time
from datetime import datetime

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
sys.path.append(os.path.abspath(repo_root_path))
    
PICO_SECONDS_PER_SECOND = 1000000000000

import random
from string import Template
from Common.fval import core
from Hpcc.instrument.hpccAcRegs import critical_alarms, CaptureControl
from Common.instruments.tester import get_tester
from Common import fval
from Hpcc.Scripts import shmoo

from Hpcc.hpcctb.env import Env
from Hpcc.hpcctb.assembler import PatternAssembler
from _build.bin import hpcctbc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('slot', type=int)
    parser.add_argument('slice', type=int)
    return parser.parse_args()

SIGNATURE_COLOR_MAP = {
    'PVCCacheUnderRun': '#FF0000',
    'Capture has more vectors than expected': '#FF9600',
    'Fail event mismatch': '#FFFF00',
    'Pattern execution did not complete': '#550000',
    'CaptureBufferOverflow': '#800080', 
    'CTV Mismatch' : '#4295f4'
}

MAX_CREDIT = 16 * 1024

fval.SetLoggerLevel(20)  # DEBUG=10, INFO=20, WARNING=30


def ShmooRange(a, b, length, flip = False):
    reverse = False
    lower, upper = None, None
    if a < b:
        lower, upper = a, b
    else:
        lower, upper = b, a
        reverse = True
    result = [int(lower + x * (upper - lower) / length) for x in range(length + 1)]
    if (not reverse and not flip) or (reverse and flip):
        return result
    else:
        return reversed(result)

def InstructionRepeatShmoo(basename, periods, flat = False):
    fval.Log('info', 'Starting Shmoo {}'.format(basename))
    env = Env(object())
    env.SetConfig('EvenToOddLoopback')
    slot, slice = env.fpgas[0]
    s = shmoo.Shmoo()
    hpcc = env.instruments[slot]
    for size in ShmooRange(1, 201, 20):
        asm = PatternAssembler()
        if flat:
            asm.symbols['PADDING'] = size - 1
            asm.LoadString("""\
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                RandomPassingEvenToOddNoSwitchVectors length=32768
                %repeat 2000
                    RandomPassingEvenToOddNoSwitchVectors length=32
                    RandomPassingEvenToOddNoSwitchVectors length=PADDING
                %end
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            """)
        else:
            asm.symbols['PADDING'] = size
            asm.LoadString("""\
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                RandomPassingEvenToOddNoSwitchVectors length=32768
                %repeat 2000
                    Repeat 32
                    RandomPassingEvenToOddNoSwitchVectors length=PADDING
                %end
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            """)
        pattern = asm.Generate()
        #asm.SaveObj('{}_{}.obj'.format(basename, size))

        # Run pattern at a slow speed and make sure it passes
        env.RunPattern(slot, slice, pattern, period = 100e-9, start = 0, captureFails = True, captureCTVs = True, captureAll = False)
        passed, alarms = env.RunCheckers(slot, slice)
        if not passed:
            raise Exception('Pattern did not pass at a slow speed!')
        # Read GOLDEN capture
        goldenCapture = env.ReadAndFlattenCapture(slot, slice)

        for period in periods:
            passed, failSignature = True, ''
            try:
                # Randomize capture address
                captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern)) + 256 * random.randint(0, 1024 * 1024)
                # Execute pattern memory without writing the pattern again and with a random capture address
                complete, status = env.RunPattern(slot, slice, pattern = None, captureAddress = captureAddress, period = period, start = 0, captureFails = True, captureCTVs = True, captureAll = False)
                if not complete:
                    fval.Log('warning', 'Aborting pattern')
                    env.AbortPattern(slot, slice)
                alarms = env.CheckAlarms(slot, slice)
                active_critical_alarms = [x for x in alarms if x in critical_alarms + ['DdrReadDataGapTooSmall']]
                if len(active_critical_alarms) != 0:
                    passed, failSignature = False, ', '.join(active_critical_alarms)
                else:
                    capture = env.ReadAndFlattenCapture(slot, slice)
                    # NOTE: Fail event does not work in internal loopback mode (use checkFailEvent = False)
                    #       Mask driver pins because capture is expected to be incorrect at fast speeds
                    failSignature = env.CaptureEquality(goldenCapture, capture, pinLaneDataMask = 0b01010101010101010101010101010101010101010101010101010101)
                    failSignature = failSignature.split(',')[0]
                    if failSignature != '':
                        passed = False
            except Exception as e:
                passed, failSignature = False, str(e)
            fval.Log('info', '{} {} {} {}'.format(size, period, passed, failSignature))
            s.Add(period, size, passed, failSignature)
    s.SaveHtml('{}.html'.format(basename), signatureColorMap = SIGNATURE_COLOR_MAP)
    s.SaveCsv('{}.csv'.format(basename))

def ParamVsPeriodShmoo(basename, config, patternSource, constants, paramSymbol, paramValues, periods, captureFails, captureCTVs, captureAll, slowPeriod = 100e-9, env = None, paramFormula = None, check_capture=True):
    fval.Log('info', 'Starting Shmoo {}'.format(basename))
    if env is None:
        env = Env(object())
    env.SetConfig(config)
    slot, slice = env.fpgas[0]
    s = shmoo.Shmoo()
    for value in paramValues:
        pattern, start = _assemble_pattern(patternSource, constants, paramSymbol, value)
        if check_capture:
            goldenCapture = _capture_reference(env, slot, slice, pattern, slowPeriod, start, captureFails, captureCTVs, captureAll)
        else:
            env.WritePattern(slot, slice, 0, pattern)

        for period in periods:
            failSignature = None
            _run_pattern(env, slot, slice, pattern, period, start, captureFails, captureCTVs, captureAll)
            try:
                _run_pattern(env, slot, slice, pattern, period, start, captureFails, captureCTVs, captureAll)
            except Exception as e:
                failSignature = str(e)
            else:
                failSignature = _check_alarms(env, slot, slice)
                if not failSignature and check_capture:
                    failSignature = _check_capture(env, slot, slice, goldenCapture)
            passed = failSignature == ''
            if paramFormula is not None:
                translatedValue = paramFormula(value)
            else:
                translatedValue = value
            fval.Log('info', '{} {} {} {}'.format(translatedValue, period, passed, failSignature))
            s.Add(period, translatedValue, passed, failSignature)
    s.SaveHtml('{}.html'.format(basename), signatureColorMap = SIGNATURE_COLOR_MAP)
    s.SaveCsv('{}.csv'.format(basename))
    # env.TakeSnapshot(filename='sc0_new_' + datetime.now().strftime("%Y%m%d_%H%M%S"), fpgas=[(args.slot, args.slice)])

def _assemble_pattern(source, constants, param, value):
    asm = PatternAssembler()
    asm.symbols.update(constants)
    asm.symbols[param] = value
    if callable(source):
        asm.LoadString(source(asm.symbols))
    else:
        asm.LoadString(source)
    pattern = asm.Generate()
    start = asm.Resolve('START')
    return pattern, start

def _capture_reference(env, slot, slice, pattern, period, start, capture_fails, capture_ctvs, capture_all):
    env.RunPattern(slot, slice, pattern, period=period, start=start, captureFails=capture_fails, captureCTVs=capture_ctvs, captureAll=capture_all)
    passed, _ = env.RunCheckers(slot, slice)
    if not passed:
        raise Exception('Pattern did not pass at a slow speed!')
    # If it passes, save its capture as the GOLDEN capture
    return env.ReadAndFlattenCapture(slot, slice)

def _run_pattern(env, slot, slice, pattern, period, start, capture_fails, capture_ctvs, capture_all):
    hpcc = env.instruments[slot]
    captureAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(pattern)) + 256 * random.randint(0, 1024 * 1024)
    # Execute pattern memory without writing the pattern again and with a random capture address
    complete, _ = env.RunPattern(slot, slice, pattern = None, captureAddress=captureAddress, period=period, start=start, captureFails=capture_fails, captureCTVs=capture_ctvs, captureAll=capture_all)
    if not complete:
        fval.Log('warning', 'Aborting pattern')
        env.AbortPattern(slot, slice)

def _check_alarms(env, slot, slice):
    alarms = env.CheckAlarms(slot, slice)
    active_critical_alarms = [x for x in alarms if x in critical_alarms + ['DdrReadDataGapTooSmall']]
    return ', '.join(active_critical_alarms)

def _check_capture(env, slot, slice, reference):
    capture = env.ReadAndFlattenCapture(slot, slice)
    # NOTE: Fail event does not work in internal loopback mode (use checkFailEvent = False)
    #       Mask driver pins because capture is expected to be incorrect at fast speeds
    failSignature = env.CaptureEquality(reference, capture, pinLaneDataMask = 0b01010101010101010101010101010101010101010101010101010101)
    return failSignature.split(',')[0]

def ParamVsPeriodShmoo_Slice2Slice(basename, config, RcvingSource, DrvingSource, shmoo_symbol, shmoo_values, periods, captureFails, captureCTVs, captureAll, slowPeriod = 100e-9):
    fval.Log('info', 'Starting Shmoo {}'.format(basename))
    
   
    env = Env(object())
    env.SetConfig(config)
    s = shmoo.Shmoo()
    
    for value in shmoo_values:
        patternRcving = PatternAssembler()
        patternRcving.symbols[shmoo_symbol] = value 
       
        patternRcving.LoadString(RcvingSource(patternRcving.symbols))
        Rcving = patternRcving.Generate()
      
      
      
        patternDriving = PatternAssembler()
        patternDriving.symbols[shmoo_symbol] = value 
       
        patternDriving.LoadString(DrvingSource(patternDriving.symbols))
        Driving = patternDriving.Generate()
        
        
        for period in periods:
            fval.Log('info','Pattern running at {} and with {} padding'.format(period, value))
            passed, failSignature = True, ''
            error_per_padding_per_period = 0
            env.rc.clear_rc_trigger()
            # set up env to start pattern on both slices. 
            for (slot, slice) in env.fpgas:
                env.LogPerSlice(slot, slice)
                hpcc = env.instruments[slot]
                data1 = hpcc.ac[slice].Read('TriggerControl')
                if slice == 0:
                    data1.DomainMaster = 1
                else:
                    data1.DomainMaster = 0
                data1.DomainID = slot
                hpcc.ac[slice].Write('TriggerControl', data1)     
                
                env.SetPeriodAndPEAttributes(slot, slice, period, {'IS_ENABLED_CLOCK': False, 'drive': 0.0, 'compare': period / 2})
      
                if slice == 0:
                    env.WritePattern(slot, slice, 0, Rcving)
                           
                else:
                    env.WritePattern(slot, slice, 0, Driving)


                captureBaseAddress = hpcc.ac[slice].SinglePatternCaptureBaseAddress(len(Rcving))
                hpcc.ac[slice].SetCaptureBaseAddress(captureBaseAddress)
                hpcc.ac[slice].SetCaptureCounts()
               
                hpcc.ac[slice].SetCaptureControl(CaptureControl.FAIL_STATUS, True, True, False, False)
               
                hpcc.ac[slice].Write('TotalFailCount', 0)
                for ch in range(56):
                    hpcc.ac[slice].WriteChannel(ch, 'ChannelFailCount', 0)
               
                hpcc.ac[slice].PrestagePattern()
            # end setting up env to start pattern on both slices. 
            
            env.rc.send_sync_pulse()
            time.sleep(0.1)
            
            # pattern is done. get capture data for each slot. 
            ctv = []
            for (slot, slice) in env.fpgas:
                hpcc = env.instruments[slot]
                completed = hpcc.ac[slice].WaitForCompletePattern(20000)
                
                if not completed:
                    hpcc.ac[slice].AbortPattern(waitForComplete = True)
                    completed = hpcc.ac[slice].IsPatternComplete()
                    fval.Log('warning', 'abort pattern, pattern complete = {} {} {}'.format(completed, period, value))
                    error_per_padding_per_period = error_per_padding_per_period + 1
                    
                    
                hpcc.DumpCapture(slice)
                captureCount = hpcc.ac[slice].Read('TotalCaptureCount').Pack()
                captureData = hpcc.ac[slice].ReadCapture()
                
                alarms = env.CheckAlarms(slot, slice)
                active_critical_alarms = [x for x in alarms if x in critical_alarms + ['DdrReadDataGapTooSmall']]
                
                if len(active_critical_alarms) != 0:
                    passed, failSignature = False, ', '.join(active_critical_alarms)
                    error_per_padding_per_period = error_per_padding_per_period + 1
                
                indCycleCount = 0
                for block in hpcc.ac[slice].UnpackCapture(captureData):
                    header, data = block
                    for i in range(8):
                        if header.CapturedVectorsValidFlags[i] == 1:
                            totalCycleCount = header.TotalCycleCount 
                            indCycleCount = totalCycleCount + i 
            
                ctv.append(indCycleCount)
            
            # cycle count for slice 0 and slice 1 for current period.
            if ctv[0] != ctv[1]:
                error_per_padding_per_period = error_per_padding_per_period + 1
                if len(failSignature) > 0:
                    failSignature = failSignature + ', ' + 'CTV Mismatch'
                else: 
                    failSignature = 'CTV Mismatch'
                
            if error_per_padding_per_period > 0:
                passed = False
            else: 
                passed = True
                
            s.Add(period, value, passed, failSignature)
            
    s.SaveHtml('{}.html'.format(basename), signatureColorMap = SIGNATURE_COLOR_MAP)
    s.SaveCsv('{}.csv'.format(basename))    


def FlatVectorShmoo(basename, length, periods, iterations):
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """,
        constants = {'LENGTH': length},
        paramSymbol = 'ITERATION',
        paramValues = [i + 1 for i in range(iterations)],
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )

def CaptureDensityShmoo(basename, length, lrpt, periods, values):
    def Pattern(symbols):
        source = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH, ctv={0:=$ctvZero,1:=$ctvOne}, lrpt={$lrpt}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        source = Template(source).substitute(ctvZero = 100 - symbols['DENSITY'], ctvOne = symbols['DENSITY'], lrpt = lrpt)
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {'LENGTH': round(length / (lrpt + 1))},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False,
    )

def _assemble_vector(vector):
    a = PatternAssembler()
    a.LoadString(vector)
    return a.Generate()[0:16]
    

_METADATA_ZERO = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40'
_METADATA_ONE = b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x7F'
_DRIVE_COMPARE_LOW = b'\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\x00\x00'
_SUBTRACT_ONE = _assemble_vector('I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, imm=1')


class PercentShmooGen():
    def __init__(self, context, length, percent, *args, **kwargs):
        vector_address = context.vecaddr
        self.length = int(length)
        self.percent = percent
        self.percent_vector = _METADATA_ONE
        self.other_vector   = _METADATA_ZERO
        
    def Size(self):
        return self.length
    
    def Generate(self, array, offset, *args, **kwargs):
        if self.percent == 0:
            array[offset:offset + self.length * 16] = self.other_vector * self.length
        elif self.percent == 100:
            array[offset:offset + self.length * 16] = self.percent_vector * self.length
        else:
            delta_error = 0
            correct_down = self.percent - 100
            correct_up = self.percent
            for i in range(self.length):
                if delta_error > 0:
                    array[offset:offset+16] = self.percent_vector
                    delta_error += correct_down
                else:
                    array[offset:offset+16] = self.other_vector
                    delta_error += correct_up
                offset += 16
        return self.length

class MetaDataShmooGen(PercentShmooGen):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.percent_vector = _METADATA_ZERO
        self.other_vector   = _DRIVE_COMPARE_LOW

class SubtractShmooGen(PercentShmooGen):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.percent_vector = _SUBTRACT_ONE
        self.other_vector   = _DRIVE_COMPARE_LOW

class InstructionRepeatShmooGen(PercentShmooGen):
    def __init__(self, context, length, percent, count=1, *args, **kwargs):
        super().__init__(context, length, percent, *args, **kwargs)
        self.percent_vector = _assemble_vector(f'Repeat {count}')
        self.other_vector   = _DRIVE_COMPARE_LOW

class SmallCreditRepeatOneShmooGen(PercentShmooGen):
    def Generate(self, array, offset, *args, **kwargs):
        delta_error = 0
        correct_down = self.percent - 100
        correct_up = self.percent
        alu_op_vector = _SUBTRACT_ONE
        repeat_vector = _assemble_vector('Repeat 1')
        data_vector = _DRIVE_COMPARE_LOW
        remaining = self.length
        for i in range(self.length):
            if delta_error > 0 and remaining > 1:
                array[offset:offset+16] = repeat_vector
                array[offset+16:offset+32] = data_vector
                delta_error += correct_down
                offset += 32
                remaining -= 2
            else:
                array[offset:offset+16] = alu_op_vector
                delta_error += correct_up
                offset += 16
                remaining -= 1
        return self.length
    
class GotoShmooGen(PercentShmooGen):
    def Generate(self, array, offset, *args, **kwargs):
        delta_error = 0
        correct_down = self.percent - 100
        correct_up = self.percent
        jump_length = 3000
        goto_vector = _assemble_vector(f'Goto address={jump_length}, base=RELATIVE')
        data_vector = _DRIVE_COMPARE_LOW
        starting_vector = offset // 16
        next_offset = offset + self.length * 16
        while offset < next_offset:
            if delta_error > 0 and (next_offset - offset) > (jump_length * 16):
                array[offset:offset+16] = goto_vector
                offset += 16
                for i in range(jump_length-1):
                    array[offset:offset+16] = data_vector
                    offset += 16
                delta_error += correct_down
            else:
                array[offset:offset+16] = data_vector
                delta_error += correct_up
                offset += 16
        # print(f'generated length = {(offset // 16) - starting_vector}, expected length = {self.length}')
        return self.length

class DrainFifoShmooGen(PercentShmooGen):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        level = kwargs['level']
        self.percent_vector = _assemble_vector(f'I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm={level}')
        self.other_vector   = _DRIVE_COMPARE_LOW
        
hpcctbc.MetaDataShmooGen = MetaDataShmooGen
hpcctbc.SubtractShmooGen = SubtractShmooGen
hpcctbc.InstructionRepeatShmooGen = InstructionRepeatShmooGen
hpcctbc.SmallCreditRepeatOneShmooGen = SmallCreditRepeatOneShmooGen
hpcctbc.GotoShmooGen = GotoShmooGen
hpcctbc.DrainFifoShmooGen = DrainFifoShmooGen
    
def MetadataShmoo(basename, periods, values, fifo_level=16384, slowPeriod=100e-9):
    def Pattern(symbols):
        source = f"""\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm={fifo_level}
            MetaDataShmooGen length=2000000, percent={symbols['DENSITY']}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        slowPeriod = slowPeriod,
        captureCTVs = False,
        captureFails = False,
        captureAll = False,
        check_capture=False
    )
    
def AluShmoo(basename, periods, values, length=2000000, fifo_level=16384, slowPeriod=100e-9):
    def Pattern(symbols):
        source = f"""\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm={fifo_level}
            SubtractShmooGen length={length}, percent={symbols['DENSITY']}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        slowPeriod = slowPeriod,
        captureCTVs = True,
        captureFails = False,
        captureAll = False,
        check_capture=False
    )
    
def InstructionRepeatOneShmoo(basename, periods, values, count=1, fifo_level=16384, slowPeriod=100e-9):
    def Pattern(symbols):
        source = f"""\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm={fifo_level}
            InstructionRepeatShmooGen length=2000000, percent={symbols['DENSITY']}, count={count}
            RandomPassingEvenToOddNoSwitchVectors length=1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        slowPeriod = slowPeriod,
        captureCTVs = False,
        captureFails = False,
        captureAll = False,
        check_capture=False
    )
    
def SmallCreditRepeatOneShmoo(basename, periods, values, slowPeriod=100e-9):
    def Pattern(symbols):
        source = f"""\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm=200
            SmallCreditRepeatOneShmooGen length=2000000, percent={symbols['DENSITY']}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        slowPeriod = slowPeriod,
        captureCTVs = False,
        captureFails = False,
        captureAll = False,
        check_capture=False
    )
    
def GotoShmoo(basename, periods, values, fifo_level=16384, slowPeriod=100e-9):
    def Pattern(symbols):
        source = f"""\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=16384
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm={fifo_level}
            GotoShmooGen length=32000000, percent={symbols['DENSITY']}
            RandomPassingEvenToOddNoSwitchVectors length=3000
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        slowPeriod = slowPeriod,
        captureCTVs = False,
        captureFails = False,
        captureAll = False,
        check_capture=False
    )
    
def DrainFifoShmoo(basename, periods, values, fifo_level=1000, slowPeriod=100e-9):
    def Pattern(symbols):
        source = f"""\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=16384
            DrainFifoShmooGen length=2000000, percent={symbols['DENSITY']}, level={fifo_level}
            RandomPassingEvenToOddNoSwitchVectors length=3000
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        fval.Log('info', source)
        return source
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        slowPeriod = slowPeriod,
        captureCTVs = False,
        captureFails = False,
        captureAll = False,
        check_capture=False
    )

def ChannelLinkingCaptureDensityShmoo(basename, length, linkMode, linkDensity, periods, values):
    def Pattern(symbols):
        source = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH, ctv={0:=$ctvZero,1:=$ctvOne}, link={0:=$linkZero,1:=$linkOne}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        source = Template(source).substitute(ctvZero = 100 - symbols['DENSITY'], ctvOne = symbols['DENSITY'], linkZero = 100 - symbols['LINKDENSITY'], linkOne = symbols['LINKDENSITY'])
        fval.Log('info', source)
        return source

    env = Env(object())
    slot, slice = env.fpgas[0]
    hpcc = env.instruments[slot]
    data = hpcc.ac[slice].Read('PatternControl')
    data.LinkMode = linkMode
    data = hpcc.ac[slice].Write('PatternControl', data)

    ParamVsPeriodShmoo(
        basename = basename,
        config = 'InternalLoopback',
        patternSource = Pattern,
        constants = {'LENGTH': length, 'LINKDENSITY': linkDensity},
        paramSymbol = 'DENSITY',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False,
        env = env,
    )

def InstructionRepeatCountShmoo(basename, periods, values):
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
            %repeat 20000
                Repeat COUNT
                RandomPassingEvenToOddNoSwitchVectors length=1
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """,
        constants = {'PREFIX_SIZE': 16 * 1024},
        paramSymbol = 'COUNT',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )

def HighCountLoopSizeShmoo(basename, periods, values):
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
            SetRegister 0, LOOP_COUNT
            LOOP:
                RandomPassingEvenToOddNoSwitchVectors length=LOOP_SIZE
                DecRegister 0
            GotoIfNonZero `LOOP`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """,
        constants = {'PREFIX_SIZE': 16 * 1024, 'LOOP_COUNT': 20000},
        paramSymbol = 'LOOP_SIZE',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )
    
    
def ConsecutiveBranchShmoo(basename, periods, values, distance, captureRate = 0, invcond=0):    
    def Pattern(symbols):
        source = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE, ctv={0:=$ctvZero,1:=$ctvOne}
            SetRegister 0, LOOP_COUNT
            
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            RandomPassingEvenToOddNoSwitchVectors length=CREDIT, ctv={0:=$ctvZero,1:=$ctvOne}
                %repeat 7000
                    RandomPassingEvenToOddNoSwitchVectors length=1, ctv={0:=$ctvZero,1:=$ctvOne}
                    RandomPassingEvenToOddNoSwitchVectors length=1, ctv={0:=$ctvZero,1:=$ctvOne}
                    I optype=BRANCH, base=RELATIVE, br=GOTO_I, imm= DISTANCE, invcond= $invcond
                    RandomPassingEvenToOddNoSwitchVectors length= LENGTH, ctv={0:=$ctvZero,1:=$ctvOne}
                %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """
        source = Template(source).substitute(ctvZero = 100 - symbols['CAPTURE_RATE'], ctvOne = symbols['CAPTURE_RATE'], invcond = invcond)
        fval.Log('info', source)
        fval.Log('info',datetime.now().strftime("%Y%m%d_%H%M%S"))
        return source
        
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {'PREFIX_SIZE': 20 * 1024, 'LOOP_COUNT': 20000, 'CREDIT':MAX_CREDIT - 2048, 'DISTANCE': distance, 'DISTANCEP1': distance+1, 'CAPTURE_RATE': captureRate, 'UNCAPTURE_RATE': 100 - captureRate},
        paramSymbol = 'LENGTH',
        paramValues = values,
        paramFormula= lambda x: x - distance,
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False
    )    

def HighCountInstructionRepeatLoopSizeShmoo(basename, periods, values):
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
            SetRegister 0, LOOP_COUNT
            LOOP:
                Repeat LOOP_SIZE
                RandomPassingEvenToOddNoSwitchVectors length=1
                DecRegister 0
            GotoIfNonZero `LOOP`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        """,
        constants = {'PREFIX_SIZE': 16 * 1024, 'LOOP_COUNT': 20000},
        paramSymbol = 'LOOP_SIZE',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )

def PatternLengthInPlistShmoo(basename, numberOfPatterns, periods, values):
    source = []
    for i in range(numberOfPatterns):
        source.append("""\
            PATTERN{}:
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH
            Return
        """.format(i))
    source.append("""\
        START:
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
    """)
    for i in range(numberOfPatterns):
        source.append("""\
            PatternId {}
            PCall `PATTERN{}`
        """.format(i, i))
    source.append("""\
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
    """)
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = '\n'.join(source),
        constants = {'PREFIX_SIZE': 16 * 1024},
        paramSymbol = 'LENGTH',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )

def SubroutineDensityShmoo(basename, numberOfSubroutines, subroutineLength, periods, values):
    source = []
    for i in range(numberOfSubroutines):
        source.append("""\
            SUBR{}:
            RandomPassingEvenToOddNoSwitchVectors length=SUBR_LENGTH
            Return
        """.format(i))
    source.append("""\
        START:
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
    """)
    for i in range(numberOfSubroutines):
        source.append("""\
            Call `SUBR{}`
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH
        """.format(i, i))
    source.append("""\
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
    """)
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = '\n'.join(source),
        constants = {'PREFIX_SIZE': 16 * 1024, 'SUBR_LENGTH': subroutineLength},
        paramSymbol = 'LENGTH',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )

def InstructionRepeatPatternLengthInPlistShmoo(basename, numberOfPatterns, periods, values):
    source = []
    for i in range(numberOfPatterns):
        source.append("""\
            PATTERN{}:
            Repeat LENGTH
            RandomPassingEvenToOddNoSwitchVectors length=1
            Return
        """.format(i))
    source.append("""\
        START:
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
        RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
    """)
    for i in range(numberOfPatterns):
        source.append("""\
            PatternId {}
            PCall `PATTERN{}`
        """.format(i, i))
    source.append("""\
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
        S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
    """)
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = '\n'.join(source),
        constants = {'PREFIX_SIZE': 16 * 1024},
        paramSymbol = 'LENGTH',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )

def PlistNestingShmoo(basename, patternLength, periods, values):
    def Pattern(symbols):
        patterns = ''
        for i in range(1):
            patterns = patterns + """\
            PATTERN{0}:
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH
            Return
            """.format(i)
        nestedPlistTargets = []
        for i in range(symbols['NESTING_LEVEL'] - 1):
            nestedPlistTargets.append('SUBR{}'.format(i + 1))
        nestedPlistTargets.append('PATTERN0')
        #fval.Log('info', nestedPlistTargets)
        nestedPlist = ''
        for i, target in enumerate(nestedPlistTargets):
            nestedPlist = nestedPlist + """\
            SUBR{0}:
            RandomPassingEvenToOddNoSwitchVectors length=LENGTH
            Call `{1}`
            Return
            """.format(i, target)
        src = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
            Call `SUBR0`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            {0}
            {1}
        """.format(patterns, nestedPlist)
        #fval.Log('info', src)
        return src
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {'PREFIX_SIZE': 16 * 1024, 'LENGTH': patternLength},
        paramSymbol = 'NESTING_LEVEL',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )
    
    

def SubroutineFetchHitShmoo(basename, periods, values):
    def Pattern(symbols):
        # 100k random vectors are used so that sub routines are located many pages apart.
        src = """\
            subr0:
                RandomPassingEvenToOddNoSwitchVectors length=1
                Return 
            RandomPassingEvenToOddNoSwitchVectors length=100000
            subr1:
                RandomPassingEvenToOddNoSwitchVectors length=1
                Return
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=PREFIX_SIZE
            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
            %repeat 1000
                PCall subr0
                
                PCall subr1
                RandomPassingEvenToOddNoSwitchVectors length=LENGTH
            %end
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            
        """
        fval.Log('info', src)
        return src
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {'PREFIX_SIZE': 16 * 1024},
        paramSymbol = 'LENGTH',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = True,
        captureAll = False,
    )
    
    
def ConsecutiveDrainFifo_NoIrpt(basename, periods, values, with_immd = 0):    
    def Pattern(symbols):
    
        if with_immd == 0:
            source = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=20000
            SetRegister 0, 100
                LOOP:
                    RandomPassingEvenToOddNoSwitchVectors length=DRAIN_FIFO_SIZE
                    I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                    DecRegister 0
                GotoIfNonZero `LOOP`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            """
        else: 
            source = """\
                START:
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                RandomPassingEvenToOddNoSwitchVectors length=20000
                SetRegister 0, 100
                    LOOP:
                        RandomPassingEvenToOddNoSwitchVectors length=DRAIN_FIFO_SIZE
                        I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm=DRAIN_FIFO_SIZE
                        DecRegister 0
                    GotoIfNonZero `LOOP`
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            """
        # source = Template(source)
        fval.Log('info', source)
        fval.Log('info',datetime.now().strftime("%Y%m%d-%H%M%S"))
        return source
        
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {'PREFIX_SIZE': 20 * 1024,  'CREDIT':MAX_CREDIT - 2048,  'CAPTURE_RATE': 0, 'UNCAPTURE_RATE': 100},
        paramSymbol = 'DRAIN_FIFO_SIZE',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False
    )    

    
   
def ConsecutiveDrainFifo_WithIrpt(basename, periods, values, with_immd = 0):    
    def Pattern(symbols):
    
        if with_immd == 0:
            source = """\
            START:
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            RandomPassingEvenToOddNoSwitchVectors length=20000
            SetRegister 0, 100
                LOOP:
                    Repeat DRAIN_FIFO_SIZE
                    RandomPassingEvenToOddNoSwitchVectors length=1
                    I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                    DecRegister 0
                GotoIfNonZero `LOOP`
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
            S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            """
        else: 
            source = """\
                START:
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                RandomPassingEvenToOddNoSwitchVectors length=20000
                SetRegister 0, 100
                    LOOP:
                        Repeat DRAIN_FIFO_SIZE
                        RandomPassingEvenToOddNoSwitchVectors length =1
                        I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I, imm = DRAIN_FIFO_SIZE
                        DecRegister 0
                    GotoIfNonZero `LOOP`
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                S stype=IOSTATEJAM, data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
            """
        # source = Template(source)
        fval.Log('info', source)
        fval.Log('info',datetime.now().strftime("%Y%m%d-%H%M%S"))
        return source
        
    ParamVsPeriodShmoo(
        basename = basename,
        config = 'EvenToOddLoopback',
        patternSource = Pattern,
        constants = {'PREFIX_SIZE': 20 * 1024,  'CREDIT':MAX_CREDIT - 2048,  'CAPTURE_RATE': 0, 'UNCAPTURE_RATE': 100},
        paramSymbol = 'DRAIN_FIFO_SIZE',
        paramValues = values,
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False
    )    
    

def MisMatchLoopShmoo(basename, periods, shmoo_values, shmoo_symbol, fixedloop= 10000, condition = 'COND.GSE'):    
    def RcvingPattern(symbols):
        RcvingSource = """\
                        PATTERN_START:                                                                                   # 0
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 0 all compares are x
                        Repeat 20098                                                                             
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, driving zero. compare x
                       
                        SetRegister 2, flatvectors_padding            
                        SetRegister 5, 1            

                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                       
                        I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                        I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
                        I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=1   # block fail on
                       
                        LOOP:
                           
                            
                            
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, # check for high. compare will fail as i am driving 0
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vL0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            
                            I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                           
                            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                            I optype=BRANCH, br=GOTO_I, cond=$condition, imm=`LOOP`, invcond=1
                            
                        #end loop
                        I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=0
                        
                        V link=0, ctv=1, mtv=0, lrpt=0, data=0vH0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, # this compare will fail as i am driving 0
                        %repeat 20000                                                                               
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        %end            
                        PATTERN_END:                                                                                     # 1022
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
                    """
        RcvingSource = Template(RcvingSource).substitute( condition = condition)
        return RcvingSource
        
        
    def DrivingPattern(symbols):
        DrivingSource = """\
                      PATTERN_START:                                                                                   # 0
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 0
                        Repeat 20098                                                                               
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, DRIVE 0, DO NOT COMPARE
                        SetRegister 0, $fixedloop # loop these many times
                        SetRegister 1, 1
                        SetRegister 2, flatvectors_padding  
                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        
                        #simpleloop
                            SIMPLELOOP:
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                
                                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                
                                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=0 # dec reg 0
                                I optype=BRANCH, br=GOTO_I, cond=COND.ZERO, imm=`SIMPLELOOP`, invcond=1
                        #end simpleloop

                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2   
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0             
                        
                        V link=0, ctv=1, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                        
                        
                        %repeat 20000    
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        %end            
                        PATTERN_END:                                                                                     # 1022
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
                    """
        DrivingSource = Template(DrivingSource).substitute( fixedloop = fixedloop)
        return DrivingSource
   
    ParamVsPeriodShmoo_Slice2Slice(
        basename = basename,
        config = 'LowToHighLoopback',
        RcvingSource = RcvingPattern,
        DrvingSource = DrivingPattern,
       
        shmoo_symbol = shmoo_symbol,
        shmoo_values = shmoo_values,
        
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False
    )    


def MatchLoopShmoo(basename, periods, shmoo_values, shmoo_symbol, fixedloop= 10000, condition = 'COND.GSE'):    
    def RcvingPattern(symbols):
        RcvingSource = """\
                        PATTERN_START:                                                                                   # 0
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 0 all compares are x
                        Repeat 20098                                                                             
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, driving zero. compare x
                        
                        SetRegister 2, flatvectors_padding            
                        SetRegister 5, 1            

                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                       
                        I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                        I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=1   # block fail on
                       
                        LOOP:
                            I optype=VECTOR, vptype=VPOTHER, vpop=CLRSTICKY
                            Repeat 200
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                           
                            
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, # check for high. compare will fail as i am driving 0
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vH0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                            I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                            V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                           
                            I optype=VECTOR, vptype=VPOTHER, vpop=DRAINFIFO_I
                            I optype=BRANCH, br=GOTO_I, cond=$condition, imm=`LOOP`, invcond=0
                            
                        #end loop
                        I optype=VECTOR, vptype=VPOTHER, vpop=BLKFAIL_I, imm=0
                        
                        V link=0, ctv=1, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, # this compare will fail as i am driving 0
                        %repeat 20000                                                                               
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        %end            
                        PATTERN_END:                                                                                     # 1022
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
                    """
        RcvingSource = Template(RcvingSource).substitute( condition = condition)
        return RcvingSource
        
        
    def DrivingPattern(symbols):
        DrivingSource = """\
                      PATTERN_START:                                                                                   # 0
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 0
                        Repeat 20098                                                                               
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, DRIVE 0, DO NOT COMPARE
                        
                        SetRegister 0, $fixedloop # loop these many times
                        SetRegister 1, 1
                        SetRegister 2, flatvectors_padding  
                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        
                        #simpleloop
                            SIMPLELOOP:
                               
                                Repeat 200
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50,
                               

                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                
                                
                                # driving zeros in the loop. these will cause fail in slice 0
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2
                                V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                               
                                
                                
                                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA=0, regB=1, opdest=ALUDEST.REG, dest=0 # dec reg 0
                                I optype=BRANCH, br=GOTO_I, cond=COND.ZERO, imm=`SIMPLELOOP`, invcond=1
                        #end simpleloop
                        # start driving 1 so that LSE is not set. 
                        
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, # DRIVE ONE, DO NOT COMPARE. these will pass on compare and slice 0 will exit loop.
                        Repeat 200 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        
                     
                        
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX1X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA=2   
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0                                                          
                       
                        V link=0, ctv=1, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                        %repeat 20000    
                        V link=0, ctv=0, mtv=0, lrpt=0, data=0vX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1-50, 
                        %end            
                        PATTERN_END:                                                                                     # 1022
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      # 1022
                        S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  # 1023
                    """
        DrivingSource = Template(DrivingSource).substitute( fixedloop = fixedloop)
        return DrivingSource
   
    ParamVsPeriodShmoo_Slice2Slice(
        basename = basename,
        config = 'LowToHighLoopback',
        RcvingSource = RcvingPattern,
        DrvingSource = DrivingPattern,
       
        shmoo_symbol = shmoo_symbol,
        shmoo_values = shmoo_values,
        
        periods = periods,
        captureCTVs = True,
        captureFails = False,
        captureAll = False
    )    


def one_period_per_subcycle():
    # run shmoo from sc0 to sc8
    a = [1,6,14,30,61,126,254,510,1021]
    return [minPeriod + p * 0.25E-9 for p in a] 

def from_min_period(steps, step_size):
    return [minPeriod + p * step_size for p in range(steps + 1)]

def span_subcycle(subcycle, steps):
    if subcycle == 0:
        start = minPeriod * (2**subcycle)
    else:
        start = minPeriod * (2**subcycle) + 1e-12
    stop = ((2 * minPeriod) * (2**subcycle))
    return span(start, stop, steps)

def span(start, stop, steps):
    step_size = (stop - start) / (steps - 1)
    return [start + x * step_size for x in range(steps)]

def span_by_step_size(start, stop, size):
    steps = []
    step = start
    while step <= stop:
        steps.append(step)
        step += size
    return steps

def run(shmoos):
    for method, kwargs in shmoos:
        method(**kwargs)

def shmoo_list():
    shmoo_list = []
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=20),
                       'values'             : span(start=0.2, stop=0.5, steps=31),
                       'fifo_level'         : 1000}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=20),
                       'values'             : span(start=1.0, stop=1.3, steps=31),
                       'fifo_level'         : 1000}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_2',
                       'periods'            : span_subcycle(subcycle=2, steps=20),
                       'values'             : span(start=2.50, stop=3.50, steps=51),
                       'fifo_level'         : 1000}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_3',
                       'periods'            : span_subcycle(subcycle=3, steps=20),
                       'values'             : span(start=5.3, stop=7.3, steps=21),
                       'fifo_level'         : 1000}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_4',
                       'periods'            : span_subcycle(subcycle=4, steps=20),
                       'values'             : span(start=10.0, stop=12.0, steps=21),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_5',
                       'periods'            : span_subcycle(subcycle=5, steps=20),
                       'values'             : span(start=19.0, stop=22.0, steps=31),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_subcycle_6',
                       'periods'            : span_subcycle(subcycle=6, steps=20),
                       'values'             : span(start=32, stop=36, steps=21),
                       'fifo_level'         : 160}))
    
    shmoo_list.append((DrainFifoShmoo, 
                      {'basename'           : 'drain_fifo_500_subcycle_7',
                       'periods'            : span_subcycle(subcycle=7, steps=20),
                       'values'             : range(49, 67),
                       'fifo_level'         : 1000}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=20),
                       'values'             : span(start=0.01, stop=0.20, steps=20),
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=20),
                       'values'             : span(start=0.10, stop=0.30, steps=21),
                       'fifo_level'         : 12800,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_2',
                       'periods'            : span_subcycle(subcycle=2, steps=20),
                       'values'             : span(start=0.20, stop=0.70, steps=26),
                       'fifo_level'         : 6400,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_3',
                       'periods'            : span_subcycle(subcycle=3, steps=20),
                       'values'             : span(start=0.5, stop=1.5, steps=21),
                       'fifo_level'         : 3200,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_4',
                       'periods'            : span_subcycle(subcycle=4, steps=20),
                       'values'             : span(start=1.0, stop=3.0, steps=41),
                       'fifo_level'         : 1600,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_5',
                       'periods'            : span_subcycle(subcycle=5, steps=20),
                       'values'             : span(start=2.0, stop=5.5, steps=36),
                       'fifo_level'         : 800,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_6',
                       'periods'            : span_subcycle(subcycle=6, steps=20),
                       'values'             : span(start=4.0, stop=10.4, steps=33),
                       'fifo_level'         : 400,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((GotoShmoo, 
                      {'basename'           : 'goto_3k_subcycle_7',
                       'periods'            : span_subcycle(subcycle=7, steps=20),
                       'values'             : span(start=8, stop=20, steps=25),
                       'fifo_level'         : 200,
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((SmallCreditRepeatOneShmoo, 
                      {'basename'           : 'repeat_1_small_credit_shmoo',
                       'periods'            : span(minPeriod*65, minPeriod * 250, 20),
                       'values'             : range(25, 60),
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_5_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=2.0, size=0.1),
                       'count'              : 5,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_10_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=2.5, size=0.1),
                       'count'              : 10,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_15_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=3, size=0.1),
                       'count'              : 15,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_20_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=3, size=0.1),
                       'count'              : 20,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_25_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=4, size=0.1),
                       'count'              : 25,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_30_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=5.5, size=0.1),
                       'count'              : 30,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_31_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=5.5, size=0.1),
                       'count'              : 31,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_32_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=5.5, size=0.1),
                       'count'              : 32,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_33_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=5.5, size=0.1),
                       'count'              : 33,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_35_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=5.5, size=0.1),
                       'count'              : 35,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_40_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=10.5, size=0.1),
                       'count'              : 40,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_41_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=10.5, size=0.1),
                       'count'              : 41,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_42_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=0, stop=15, size=0.1),
                       'count'              : 42,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_43_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : range(51),
                       'count'              : 43,
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_1_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : span_by_step_size(start=1.0, stop=4.6, size=0.2),
                       'count'              : 1,
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_5_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : span_by_step_size(start=3, stop=6, size=0.1),
                       'count'              : 5,
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_10_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : span_by_step_size(start=5, stop=7, size=0.1),
                       'count'              : 10,
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_15_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : span_by_step_size(start=9, stop=12, size=0.1),
                       'count'              : 15,
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_20_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : range(20, 51),
                       'count'              : 20,
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_21_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : range(30, 51),
                       'count'              : 21,
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_1_count_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : span_by_step_size(start=0.5, stop=4, size=0.05),
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_2_count_1',
                       'periods'            : span_subcycle(subcycle=2.2, steps=8),
                       'values'             : span_by_step_size(start=2, stop=8.5, size=0.05),
                       'fifo_level'         : 800}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_3_count_1',
                       'periods'            : span_subcycle(subcycle=3, steps=8),
                       'values'             : span_by_step_size(start=5, stop=16, size=0.1),
                       'fifo_level'         : 400}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_4_count_1',
                       'periods'            : span_subcycle(subcycle=4, steps=8),
                       'values'             : span_by_step_size(start=10, stop=28.5, size=0.5),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_5_count_1',
                       'periods'            : span_subcycle(subcycle=5, steps=8),
                       'values'             : span_by_step_size(start=20, stop=44.5, size=0.5),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_6_count_1',
                       'periods'            : span_subcycle(subcycle=6, steps=8),
                       'values'             : span_by_step_size(start=30, stop=50, size=0.5),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((SmallCreditRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_6_count_1_small_credit_shmoo',
                       'periods'            : span_subcycle(subcycle=6, steps=8),
                       'values'             : span_by_step_size(start=50, stop=58, size=0.2),
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((SmallCreditRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_7_count_1_small_credit_shmoo_for_TOS_3.4',
                       'periods'            : span_subcycle(subcycle=7, steps=8),
                       'values'             : span_by_step_size(start=98, stop=99.9, size=0.1),
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((SmallCreditRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_subcycle_7_count_1_small_credit_shmoo_for_TOS_3.',
                       'periods'            : span_subcycle(subcycle=7, steps=8),
                       'values'             : span_by_step_size(start=25, stop=28, size=0.2),
                       'slowPeriod'         : 340.992e-9}))
    
    shmoo_list.append((InstructionRepeatOneShmoo, 
                      {'basename'           : 'instruction_repeat_1_subcycle_0_to_5',
                       'periods'            : span(minPeriod, minPeriod * 64, 100),
                       'values'             : range(0, 50)}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_0',
                       'periods'            : span_subcycle(subcycle=0, steps=8),
                       'values'             : span_by_step_size(start=1.25, stop=3.75, size=0.05),
                       'fifo_level'         : 3200}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_1',
                       'periods'            : span_subcycle(subcycle=1, steps=8),
                       'values'             : span_by_step_size(start=4, stop=7, size=0.1),
                       'fifo_level'         : 1600}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_2',
                       'periods'            : span_subcycle(subcycle=2, steps=8),
                       'values'             : span_by_step_size(start=9.5, stop=13, size=0.1),
                       'fifo_level'         : 800}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_3',
                       'periods'            : span_subcycle(subcycle=3, steps=8),
                       'values'             : span_by_step_size(start=17, stop=23, size=0.2),
                       'length'             : 2000000,
                       'fifo_level'         : 400}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_4',
                       'periods'            : span_subcycle(subcycle=4, steps=8),
                       'values'             : span_by_step_size(start=31, stop=37, size=0.2),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_5',
                       'periods'            : span_subcycle(subcycle=5, steps=8),
                       'values'             : span_by_step_size(start=45, stop=54, size=0.2),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_6',
                       'periods'            : span_subcycle(subcycle=6, steps=8),
                       'values'             : span_by_step_size(start=66, stop=70, size=0.2),
                       'fifo_level'         : 200}))
    
    shmoo_list.append((AluShmoo, 
                      {'basename'           : 'Alu_Density_subcycle_7',
                       'periods'            : span_subcycle(subcycle=7, steps=8),
                       'values'             : span_by_step_size(start=79.5, stop=82.5, size=0.2),
                       'fifo_level'         : 200,
                       'slowPeriod'         : 250e-9}))
                       
    shmoo_list.append((MetadataShmoo, 
                      {'basename' : 'MetaData_Density',
                       'periods'  : from_min_period(steps=50, step_size=100e-12),
                       'values'   : [x for x in range(20, 100, 2)]}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_0',
                       'periods'    : span_subcycle(subcycle=0, steps=32),
                       'fifo_level' :6400,
                       'values'     : span_by_step_size(start=25, stop=50, size=0.5)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_1',
                       'periods'    : span_subcycle(subcycle=1, steps=32),
                       'fifo_level' : 3200,
                       'values'     : span_by_step_size(start=60, stop=80, size=0.5)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_2',
                       'periods'    : span_subcycle(subcycle=2, steps=32),
                       'fifo_level' : 1600,
                       'values'     : span_by_step_size(start=81, stop=88, size=0.2)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_3',
                       'periods'    : span_subcycle(subcycle=3, steps=32),
                       'fifo_level' : 800,
                       'values'     : span_by_step_size(start=91, stop=94, size=0.1)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_4',
                       'periods'    : span_subcycle(subcycle=4, steps=32),
                       'fifo_level' : 400,
                       'values'     : span_by_step_size(start=95, stop=97, size=0.1)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_5',
                       'periods'    : span_subcycle(subcycle=5, steps=32),
                       'fifo_level' : 200,
                       'values'     : span_by_step_size(start=97, stop=99, size=0.1)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_6',
                       'periods'    : span_subcycle(subcycle=6, steps=32),
                       'fifo_level' : 200,
                       'values'     : span_by_step_size(start=98, stop=99.5, size=0.1)}))
    
    shmoo_list.append((MetadataShmoo, 
                      {'basename'   : 'MetaData_Density_subcycle_7',
                       'periods'    : span_subcycle(subcycle=7, steps=32),
                       'values'     : span_by_step_size(start=99.2, stop=99.8, size=.02),
                       'fifo_level' : 200,
                       'slowPeriod' : 250e-9}))
    
    shmoo_list.append((ConsecutiveDrainFifo_NoIrpt, 
                      {'basename' : 'ConsecutiveDrainFifo_NoIrpt_NoImmd',
                       'periods'  : one_period_per_subcycle(),
                       'values'   : ShmooRange(500, 4000, 9, flip=True), 
                       'with_immd': 0}))
    
    #shmoo to analyze drain fifo performance. 
    shmoo_list.append((ConsecutiveDrainFifo_NoIrpt,
                      {'basename' : 'ConsecutiveDrainFifo_NoIrpt_NoImmd',
                       'periods' : one_period_per_subcycle(),
                       'values' : ShmooRange(500, 4000, 9, flip=True), 
                       'with_immd' : 0}))

    shmoo_list.append((ConsecutiveDrainFifo_NoIrpt,
        {'basename' : 'ConsecutiveDrainFifo_NoIrpt_WithImmd',
         'periods' : one_period_per_subcycle(),
         'values' : ShmooRange(500, 4000, 9, flip=True), 
         'with_immd' : 1}))

    shmoo_list.append((ConsecutiveDrainFifo_WithIrpt,
        {'basename' : 'ConsecutiveDrainFifo_WithIrpt_NoImmd',
         'periods' : one_period_per_subcycle(),
         'values' : ShmooRange(500, 4000, 9, flip=True), 
         'with_immd' : 0}))

    shmoo_list.append((ConsecutiveDrainFifo_WithIrpt,
        {'basename' : 'ConsecutiveDrainFifo_WithIrpt_WithImmd',
         'periods' : one_period_per_subcycle(),
         'values' : ShmooRange(500, 4000, 9, flip=True), 
         'with_immd' : 1}))


    # These shmoos helps us figure out our calibration limits
    shmoo_list.append((FlatVectorShmoo,
        {'basename' : 'flat_vectors_1M',
         'length' : 1 * 1024 * 1024,
         'periods' : from_min_period(steps=32, step_size=10e-12),
         'iterations' : 15}))

    shmoo_list.append((FlatVectorShmoo,
        {'basename' : 'flat_vectors_10M',
         'length' : 10 * 1024 * 1024,
         'periods' : from_min_period(steps=32, step_size=10e-12),
         'iterations' : 15}))

    # capture density
    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M_full_range',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 0,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_10M',
         'length' : 10 * 1024 * 1024,
         'lrpt' : 0,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 0,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_0.5M',
         'length' : 512 * 1024,
         'lrpt' : 0,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_0.25M',
         'length' : 256 * 1024,
         'lrpt' : 0,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M_lrpt1_full_range',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 1,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M_lrpt3_full_range',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 3,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M_lrpt7_full_range',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 7,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M_lrpt15_full_range',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 15,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((CaptureDensityShmoo,
        {'basename' : 'capture_density_1M_lrpt31_full_range',
         'length' : 1 * 1024 * 1024,
         'lrpt' : 31,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link2x_100_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 0,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 100,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link2x_75_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 0,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 75,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link2x_50_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 0,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 50,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link2x_25_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 0,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 25,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link4x_100_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 1,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 100,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link4x_75_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 1,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 75,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link4x_50_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 1,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 50,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((ChannelLinkingCaptureDensityShmoo,
        {'basename' : 'capture_density_1M_link4x_25_full_range',
         'length' : 1 * 1024 * 1024,
         'linkMode' : 1,  # 0 = 2x, 1 = 4x, 2 = 8x
         'linkDensity' : 25,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 0, 20)}))

    shmoo_list.append((InstructionRepeatCountShmoo,
        {'basename' : 'instruction_repeat_count',
         'periods' : from_min_period(steps=30, step_size=100e-12),
         'values' : ShmooRange(1, 101, 25)}))

    shmoo_list.append((InstructionRepeatCountShmoo,
        {'basename' : 'instruction_repeat_count_subcycle_0',
         'periods' : span_subcycle(subcycle=0, steps=20),
         'values' : ShmooRange(40, 60, 21)}))

    shmoo_list.append((InstructionRepeatShmoo,
        {'basename' : 'instruction_repeat_density_8_low',
         'periods' : from_min_period(steps=15, step_size=300e-12)}))

    shmoo_list.append((HighCountInstructionRepeatLoopSizeShmoo,
        {'basename' : 'high_count_loop_size_instruction_repeat',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(50, 1050, 20)}))

    shmoo_list.append((HighCountInstructionRepeatLoopSizeShmoo,
        {'basename' : 'high_count_loop_size_zoom_in_instruction_repeat',
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(120, 520, 20)}))

    shmoo_list.append((HighCountLoopSizeShmoo,
        {'basename' : 'high_count_loop_size',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(5, 505, 20)}))

    shmoo_list.append((HighCountLoopSizeShmoo,
        {'basename' : 'high_count_loop_size_zoom_in',
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(120, 520, 20)}))

    shmoo_list.append((ConsecutiveBranchShmoo,
        {'basename' : 'consecutive_far_branch_no_capture',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         # 'periods' : from_min_period(steps=24, step_size=250e-12),
         'values' : ShmooRange(3200, 7000, 20, flip=True), # failed at 3005
         # 'values' : ShmooRange(3100, 3960, 4, flip=True),
         'distance' : 3000,
         'captureRate' : 0, 
         'invcond' : 0}))

    shmoo_list.append((ConsecutiveBranchShmoo,
        {'basename' : 'consecutive_far_branch_50percent_capture',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(3200, 7000, 20, flip=True), # failed at 3005
         'distance' : 3000,
         'captureRate' : 50,
         'invcond' : 0}))

    # shmoo_list.append((ConsecutiveBranchShmoo,
       # {'basename' : 'consecutive_far_branch_75percent_capture',
       #  'periods' : from_min_period(steps=40, step_size=250e-12),
       #  'values' : ShmooRange(3200, 7000, 20, flip=True), # failed at 3005
       #  'distance' : 3000,
       #  'captureRate' : 75,
       #  'invcond' : 0}))

    shmoo_list.append((ConsecutiveBranchShmoo,
        {'basename' : 'consecutive_near_branch_no_capture',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(5, 300, 20, flip=True),
         'distance' : 2,
         'captureRate' : 0, 
         'invcond' : 0}))

    shmoo_list.append((ConsecutiveBranchShmoo,
        {'basename' : 'consecutive_near_branch_50percent_capture',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(5, 300, 20, flip=True),
         'distance' : 2,
         'captureRate' : 50,
         'invcond' : 0}))

    shmoo_list.append((ConsecutiveBranchShmoo,
        {'basename' : 'consecutive_not_taken_branch',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(5, 300, 20, flip=True),
         'distance' : 2,
         'captureRate' : 0, 
         'invcond' : 1}))

    shmoo_list.append((InstructionRepeatPatternLengthInPlistShmoo,
        {'basename' : 'pattern_length_in_plist_full_range_instruction_repeat',
         'numberOfPatterns' : 300,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 2100, 20)}))

    shmoo_list.append((PatternLengthInPlistShmoo,
        {'basename' : 'pattern_length_in_plist',
         'numberOfPatterns' : 300,
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(25, 1025, 20)}))

    shmoo_list.append((PatternLengthInPlistShmoo,
        {'basename' : 'pattern_length_in_plist_zoom_out2',
         'numberOfPatterns' : 300,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(10000, 110000, 10)}))

    shmoo_list.append((PatternLengthInPlistShmoo,
        {'basename' : 'pattern_length_in_plist_zoom_out1',
         'numberOfPatterns' : 300,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(1000, 11000, 10)}))

    shmoo_list.append((PatternLengthInPlistShmoo,
        {'basename' : 'pattern_length_in_plist_full_range',
         'numberOfPatterns' : 300,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(100, 2100, 20)}))

    shmoo_list.append((SubroutineDensityShmoo,
        {'basename' : 'subroutine_density_1',
         'numberOfSubroutines' : 300,
         'subroutineLength' : 1,
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' : ShmooRange(10, 1010, 20)}))

    shmoo_list.append((PlistNestingShmoo,
        {'basename' : 'plist_nesting_512',
         'patternLength' : 512,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(16, 1, 16)}))

    shmoo_list.append((PlistNestingShmoo,
        {'basename' : 'plist_nesting_1K',
         'patternLength' : 1024,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(16, 1, 16)}))

    shmoo_list.append((PlistNestingShmoo,
        {'basename' : 'plist_nesting_1K_full_range',
         'patternLength' : 1024,
         'periods' : from_min_period(steps=53, step_size=25e-12),
         'values' : ShmooRange(16, 1, 16)}))

    shmoo_list.append((PlistNestingShmoo,
        {'basename' : 'plist_nesting_10K',
         'patternLength' : 10 * 1024,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(16, 1, 16)}))

    shmoo_list.append((PlistNestingShmoo,
        {'basename' : 'plist_nesting_100K',
         'patternLength' : 100 * 1024,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(16, 1, 16)}))

    shmoo_list.append((PlistNestingShmoo,
        {'basename' : 'plist_nesting_1M',
         'patternLength' : 1024 * 1024,
         'periods' : from_min_period(steps=20, step_size=50e-12),
         'values' : ShmooRange(16, 1, 16)}))

    shmoo_list.append((SubroutineFetchHitShmoo,
        {'basename' : 'subroutine_fetch_hit_shmoo',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'values' :  ShmooRange(20, 2000, 10, flip=True)}))

    shmoo_list.append((MisMatchLoopShmoo,
        {'basename' : 'MisMatchLoopShmoo_GSE',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'shmoo_values' : ShmooRange(2000, 17001, 15, flip=True),
         'shmoo_symbol' : 'flatvectors_padding', 
         'fixedloop' : 100000,
         'condition' : 'COND.GSE'}))
        
    shmoo_list.append((MisMatchLoopShmoo,
        {'basename' : 'MisMatchLoopShmoo_DSE',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'shmoo_values' : ShmooRange(2000, 17001, 15, flip=True),
         'shmoo_symbol' : 'flatvectors_padding', 
         'fixedloop' : 100000,
         'condition' : 'COND.DSE'}))

    shmoo_list.append((MatchLoopShmoo,
        {'basename' : 'MatchLoopShmoo_GSE',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'shmoo_values' : ShmooRange(2000, 17001, 15, flip=True),
         'shmoo_symbol' : 'flatvectors_padding', 
         'fixedloop' : 100000,
         'condition' : 'COND.GSE'}))

    shmoo_list.append((MatchLoopShmoo,
        {'basename' : 'MatchLoopShmoo_DSE',
         'periods' : from_min_period(steps=40, step_size=250e-12),
         'shmoo_values' : ShmooRange(2000, 17001, 15, flip=True),
         'shmoo_symbol' : 'flatvectors_padding', 
         'fixedloop' : 100000,
         'condition' : 'COND.DSE'}))
    return shmoo_list


def setup_output_directory(prefix):
    results_dir = time.strftime(f'results_{prefix}_%Y_%m_%d_%H-%M-%S')
    os.mkdir(results_dir)
    os.chdir(results_dir)
    # configure logging last, so the transcript is stored in the results directory
    fval.ConfigLogger('transcript.txt')
    
    
if __name__ == '__main__':
    setup_output_directory('online_shmoo')
    args = parse_args()
    slots = [args.slot]
    tester = get_tester()
    tester.discover_all_instruments()
    tester.set_under_test('Hpcc', args.slot)
    tester.Initialize()
    env = Env(object())
    slot, slice = env.fpgas[args.slice]
    hpcc = env.instruments[slot]
    minPeriod = hpcc.ac[slice].minQdrMode / PICO_SECONDS_PER_SECOND
    run(shmoo_list())