################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: shmoo.py
#-------------------------------------------------------------------------------
#     Purpose: Shmoo plotter / analysis module
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/27/15
#       Group: HDMT FPGA Validation
###############################################################################

import copy
import csv
import math
import random

def ToSI(d, decimalPlaces = 2):
    incPrefixes = ['K', 'M', 'G']
    decPrefixes = ['m', 'u', 'n', 'p', 'f']
    degree = int(math.floor(math.log10(math.fabs(d)) / 3))
    prefix = ''
    if degree != 0:
        ds = degree / math.fabs(degree)
        if ds == 1:
            if degree - 1 < len(incPrefixes):
                prefix = incPrefixes[degree - 1]
            else:
                prefix = incPrefixes[-1]
                degree = len(incPrefixes)
        elif ds == -1:
            if -degree - 1 < len(decPrefixes):
                prefix = decPrefixes[-degree - 1]
            else:
                prefix = decPrefixes[-1]
                degree = -len(decPrefixes)
        scaled = float(d * math.pow(1000, -degree))
        f = '{:.' + str(decimalPlaces) + 'f}{}'
        s = f.format(scaled, prefix)
    else:
        s = "{d}".format(d = d)
    return s

def rgbToHex(r, g, b):
    return '#%02X%02X%02X' % (r, g, b)

class Shmoo:
    HTML_TEMPLATE = """\
<!DOCTYPE html>
<html>
<body>
<canvas id="canvas" width="{}" height="{}">Your browser does not support the HTML5 canvas tag.</canvas>
<style>
    * {{ font: 14px Lucida Console; padding-bottom: 5px; }}
    h1 {{ font-size: 16px; font-weight: bold; font-family: Verdana; }}
</style>
<script>
var c = document.getElementById("canvas");
var ctx = c.getContext("2d");
ctx.imageSmoothingEnabled = false;
ctx.font = "14px Lucida Console";
ctx.strokeStyle = "rgb(120, 120, 120)";
ctx.lineWidth = 1;
{}
</script>
<br>
{}
</body>
</html>
"""
    def __init__(self):
        self.Clear()
    def Clear(self):
        self.xAxis = []
        self.yAxis = []
        self.data = {}
    def Add(self, x, y, status, failSignature = None):
        if x not in self.xAxis: self.xAxis.append(x)
        if y not in self.yAxis: self.yAxis.append(y)
        self.data[(x, y)] = (status, failSignature)
    def Compare(self, other):
        if self.xAxis != other.xAxis:
            raise Exception('Can\'t compare shmoos. x-axes don\'t match!')
        if self.yAxis != other.yAxis:
            raise Exception('Can\'t compare shmoos. y-axes don\'t match!')
        mismatches = []
        for y in self.yAxis:
            for x in self.xAxis:
                selfStatus = None
                otherStatus = None
                if (x, y) in self.data:
                    selfStatus = self.data[(x, y)]
                if (x, y) in other.data:
                    otherStatus = other.data[(x, y)]
                if selfStatus != otherStatus:
                    mismatches.append('For cell ({}, {}), status mismatch: self={}, other={}'.format(x, y, selfStatus, otherStatus))
        return mismatches
    def LoadCsv(self, filename):
        fin = open(filename, 'r')
        for row in csv.reader(fin):
            if len(row) == 3:
                y = Number(row[0])
                x = Number(row[1])
                status = row[2]
                self.Add(x, y, Status(status))
            elif len(row) == 4:
                y = Number(row[0])
                x = Number(row[1])
                status = row[2]
                failSignature = row[3]
                self.Add(x, y, Status(status), failSignature)
            else:
                raise Exception('Invalid number of columns. Expecting 3 columns: y, x, pass/fail status, [fail signature,]')
    def SaveCsv(self, filename):
        fout = open(filename, 'w')
        csvout = csv.writer(fout)
        for y in self.yAxis:
            for x in self.xAxis:
                status = None
                failSignature = None
                if (x, y) in self.data:
                    status, failSignature = self.data[(x, y)]
                csvout.writerow([y, x, status, failSignature])
        fout.close()
    
    def SaveHtml(self, filename, signatureColorMap = {}):
        cellWidth  = 20
        cellHeight = 20
        lx = len(self.xAxis)
        ly = len(self.yAxis)
        width  = cellWidth * (lx + 5)
        height = cellHeight * (ly + 3)
        shmooData = []

        colors = [
            rgbToHex(0, 0, 127),
            rgbToHex(0, 127, 127),
            rgbToHex(127, 127, 0),
            rgbToHex(255, 0, 255),
            rgbToHex(255, 0, 127),
            rgbToHex(127, 0, 127),
            rgbToHex(127, 127, 127),
            rgbToHex(0x00, 0x66, 0xFF),
            rgbToHex(0x99, 0xC2, 0xFF),
            rgbToHex(0xFF, 0x33, 0xCC),
        ]

        signatureColors = copy.deepcopy(signatureColorMap)
        i = 0
        for key, value in self.data.items():
            status, failSignature = value
            if not(status) and not(failSignature in signatureColors):
                try:
                    signatureColors[failSignature] = colors[i]
                except IndexError:
                    signatureColors[failSignature] = random.randrange(0xFFFFFF + 1)
                i = i + 1

        for j, y in enumerate(reversed(self.yAxis)):  # Flip y axis
            shmooData.append('// Shmoo row {}'.format(j))
            for i, x in enumerate(self.xAxis):
                status = None
                failSignature = None
                if (x, y) in self.data:
                    status, failSignature = self.data[(x, y)]
                if status is not None:
                    if status:
                        shmooData.append('ctx.fillStyle = "#00FF00";')
                    else:
                        shmooData.append('ctx.fillStyle = "{}";'.format(signatureColors[failSignature]))
                else:
                    shmooData.append('ctx.fillStyle = "#666666";')
                ii = i + 2
                jj = j + 2
                shmooData.append('ctx.fillRect({}, {}, {}, {});'.format(ii * cellWidth, jj * cellHeight, cellWidth, cellHeight))
    
        shmooData.append('ctx.beginPath();')
        for j in range(ly + 1):  # Flip y axis
            ii = 2
            jj = j + 2
            shmooData.append('ctx.moveTo({}+0.5, {}+0.5);'.format(ii * cellWidth, jj * cellHeight))
            ii = 2 + lx
            jj = j + 2
            shmooData.append('ctx.lineTo({}+0.5, {}+0.5);'.format(ii * cellWidth, jj * cellHeight))
        shmooData.append('ctx.stroke();')
        shmooData.append('ctx.closePath();')
    
        shmooData.append('ctx.beginPath();')
        for i in range(lx + 1):
            ii = i + 2
            jj = 2
            shmooData.append('ctx.moveTo({}+0.5, {}+0.5);'.format(ii * cellWidth, jj * cellHeight))
            ii = i + 2
            jj = 2 + ly
            shmooData.append('ctx.lineTo({}+0.5, {}+0.5);'.format(ii * cellWidth, jj * cellHeight))
        shmooData.append('ctx.stroke();')
        shmooData.append('ctx.closePath();')

        shmooData.append('ctx.fillStyle = "#000000";')
        for j, y in enumerate(reversed(self.yAxis)):  # Flip y axis
            ii = 0
            jj = j + 3
            if type(y) == float:
                y = round(y, 2)
            shmooData.append('ctx.fillText("{}", {}, {});'.format(y, ii * cellWidth, jj * cellHeight))
    
        i, x = 0, self.xAxis[0]
        ii = i + 2
        jj = 1.5
        shmooData.append('ctx.fillText("{}", {}, {});'.format(ToSI(float(x)), ii * cellWidth, jj * cellHeight))
    
        i, x = len(self.xAxis) // 2, self.xAxis[len(self.xAxis) // 2]
        ii = i + 2
        jj = 1.5
        shmooData.append('ctx.fillText("{}", {}, {});'.format(ToSI(float(x)), ii * cellWidth, jj * cellHeight))
    
        i, x = len(self.xAxis) - 1, self.xAxis[len(self.xAxis) - 1]
        ii = i + 2
        jj = 1.5
        shmooData.append('ctx.fillText("{}", {}, {});'.format(ToSI(float(x)), ii * cellWidth, jj * cellHeight))
   
        legend = []
        for failSignature, value in signatureColors.items():
            legend.append('<div style="width:17px;height:17px;border:1px solid rgb(120, 120, 120); background-color: {}; float:left; margin-right: 5px;"></div>{}<br><br>'.format(value, failSignature))
        legend.append('<div style="width:17px;height:17px;border:1px solid rgb(120, 120, 120); background-color: #00FF00; float:left; margin-right: 5px;"></div>PASS<br><br>')

        fout = open(filename, 'w')
        fout.write(self.HTML_TEMPLATE.format(width, height, '\n'.join(shmooData), '\n'.join(legend)))
        fout.close()

def Number(s):
    try:
        return int(s)
    except ValueError:
        return float(s)

def Status(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    elif s == 'None':
        return None
    else:
        raise Exception('Invalid shmoo status \'{}\''.format(s))

if __name__ == "__main__":
    # Parse command line options
    import argparse
    parser = argparse.ArgumentParser(description = 'Shmoo csv data plotter')
    parser.add_argument('infile', help = 'Input csv data', type = str)
    parser.add_argument('outfile', help = 'Output html filename', type = str)
    args = parser.parse_args()

    shmoo = Shmoo()
    shmoo.LoadCsv(args.infile)
    shmoo.SaveHtml(args.outfile)

