# INTEL CONFIDENTIAL

# Copyright 2016-2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

'''Runs a subset of online shmoos, using an HPCC model instead of hardware'''

import os
import sys
import time


repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
sys.path.append(os.path.abspath(repo_root_path))

from Hpcc.hpcctb.creditcounter_factory import new_credit_counter
import onlineShmoos


SIMULATOR_VECTOR_LIMIT = 50000000
SLOWER_THAN_POSSIBLE_PERIOD = 1
PVC_CACHE_FULL = 16384
PIN_DATA_VECTOR_TYPE = 0
BYTES_PER_VECTOR = 16

IMMEDIATE_LSB           = 0
IMMEDIATE_BITS          = 32
REGISTER_A_LSB          = 32
REGISTER_A_BITS         = 5
REGISTER_B_LSB          = 37
REGISTER_B_BITS         = 5
DESTINATION_LSB         = 42
DESTINATION_BITS        = 5
BRANCH_TYPE_LSB         = 47
BRANCH_TYPE_BITS        = 5
BRANCH_BASE_LSB         = 52
BRANCH_BASE_BITS        = 2
BRANCH_CONDITION_LSB    = 54
BRANCH_CONDITION_BITS   = 4
BRANCH_INVERT_LSB       = 58
BRANCH_INVERT_BITS      = 1
OP_CODE_LSB             = 59
OP_CODE_BITS            = 7
OP_TYPE_LSB             = 66
OP_TYPE_BITS            = 4

LOCAL_REPEAT_MASK = 0x1F
LINK_ENABLE_MASK = 0x02

FLAGS_RESERVED_MASK = 0x00010000
FLAGS_ZERO_MASK = 0x02000000
FLAGS_CARRY_MASK = 0x04000000
FLAGS_OVERFLOW_MASK = 0x08000000
FLAGS_SIGN_MASK = 0x10000000

SIGNED_MAX_POSITIVE = 0x7FFFFFFF
SIGNED_MAX_NEGATIVE = -0x80000000
SIGNED_SIGN_MASK = 0x80000000
REGISTER_SIZE_MASK = 0xFFFFFFFF
REGISTER_MAX_VALUE_PLUS_ONE = 0x100000000



class OfflineEnv():
    '''Offline environment for running patterns.
    
    Maintains compatibility with normal HPCC Env, as used by onlineShmoos
    '''
    this_row_already_passed_at_period = SLOWER_THAN_POSSIBLE_PERIOD
    
    def __init__(self, test):
        self.fpgas = [(0,0), (0, 1)]
        self.instruments = [Hpcc()]
        self.rc = Rc()
        self.alarms = []
    
    def SetConfig(self, config):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def RunPattern(self, slot, slice, pattern, period, start, captureFails, captureCTVs, captureAll, captureAddress=0):
        '''Conditionally runs a pattern on 1 slice.
        
        Runs the pattern unless the same pattern already passed at a faster speed.
        '''
        if pattern is None and self.this_row_already_passed_at_period < period:
            # will always pass the same pattern at a slower speed
            return True, None
        else:
            self.this_row_already_passed_at_period = SLOWER_THAN_POSSIBLE_PERIOD
        
        self.alarms = []
        try:
            self.instruments[slot].ac[slice].run_pattern(pattern, start, period)
        except RuntimeError as e:
            if 'Pattern will under run' in str(e):
                self.alarms.append('PVCCacheUnderRun')
            else:
                raise
        if not self.alarms:
            self.this_row_already_passed_at_period = period
        return True, None
    
    def RunCheckers(self, slot, slice):
        '''Required part of API, but not used for offline shmoo'''
        return True, None
    
    def ReadAndFlattenCapture(self, slot, slice):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def CheckAlarms(self, slot, slice):
        return self.alarms + self.instruments[slot].ac[slice].get_alarms()
    
    def CaptureEquality(self, expected, observed, pinLaneDataMask=0):
        '''Required part of API, but not used for offline shmoo'''
        return ''
    
    def LogPerSlice(self, slot, slice):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def SetPeriodAndPEAttributes(self, slot, slice, period, attributes):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def WritePattern(self, slot, slice, offset, pattern):
        self.instruments[slot].ac[slice].WritePattern(offset, pattern)
        

class Hpcc():
    '''HPCC Model'''
    
    def __init__(self):
        '''Create a standard HPCC configuration'''
        self.ac = [Ac(), Ac()]
    
    def DumpCapture(self, slice):
        '''Required part of API, but not used for offline shmoo'''
        pass
        

class Ac():
    '''AC FPGA Model'''
    MIN_PERIOD = 666e-12
    
    def __init__(self):
        '''Prepare instance to run a pattern
        
        Pattern vectors have many fields to define behavior.
        Set up method look up tables for possible field values.
        '''
        self._credit_counter = None
        self._period = SLOWER_THAN_POSSIBLE_PERIOD
        self._pattern_end_seen = False
        self._flags = FLAGS_RESERVED_MASK
        self._stack = []
        self._alarms = []
        # self._vector_handler = {0b00: self._pin_data_prestage,
        self._vector_handler = {0b00: self._pin_data,
                                0b01: self._metadata,
                                0b10: self._pin_state,
                                0b11: self._instruction}
        self._instruction_handler = {0b0000: self._branch,
                                     0b0001: self._external_op,
                                     0b0010: self._alu_op,
                                     0b0100: self._register_op,
                                     0b1000: self._vector_op}
        self._vector_op_handler = {0b001: self._vector_op_log,
                                   0b010: self._vector_op_local,
                                   0b100: self._vector_op_other}
        self._vector_op_local_handler = {0b0001: self._repeat_imm,
                                         0b0010: self._end_imm,
                                         0b0100: self._fail_branch_imm,
                                         0b1001: self._repeat_reg,
                                         0b1010: self._end_reg,
                                         0b1100: self._fail_branch_r}
        self._vector_op_other_handler = {0b0001: self._trigger_sib,
                                         0b0010: self._block_fails_imm,
                                         0b0011: self._drain_pin_fifo_imm,
                                         0b0100: self._edge_counter,
                                         0b0101: self._clear_sticky_error,
                                         0b1000: self._capture_imm,
                                         0b1100: self._reset_cycle_count,
                                         0b1010: self._reset_capture_memory,
                                         0b1001: self._reset_fail_counts,
                                         0b1011: self._drain_pin_fifo_reg}
        self._register_source_handler = {0b000: self._get_immediate_unsigned,
                                         0b001: self._reg_op_source_reg,
                                         0b010: self._reg_op_source_tcc,
                                         0b011: self._reg_op_source_vec_addr,
                                         0b111: self._reg_op_source_cfb,
                                         0b111: self._reg_op_source_flags,
                                         0b111: self._reg_op_source_dirty_mask,
                                         0b111: self._reg_op_source_stack}
        self._register_op_handler = {0b0001: self._register_op_reg,
                                     0b0010: self._register_op_cfb,
                                     0b0100: self._register_op_flags,
                                     0b1000: self._register_op_stack}
        self._branch_handler = {0b00000: self._no_branch,
                                0b00001: self._goto_imm,
                                0b00010: self._call_imm,
                                0b00100: self._pcall_imm,
                                0b01000: self._ret,
                                0b10001: self._goto_reg,
                                0b10010: self._call_reg,
                                0b10100: self._pcall_reg,
                                0b11000: self._clear_stack}
        self._goto_imm_handler = {0b00: self._goto_imm_global,
                                  0b01: self._goto_imm_cfb,
                                  0b10: self._goto_imm_relative,
                                  0b11: self._goto_imm_pfb}
        self._alu_op_handler = {0b000: self._alu_op_xor,
                                0b001: self._alu_op_and,
                                0b010: self._alu_op_or,
                                0b011: self._alu_op_lshl,
                                0b100: self._alu_op_lshr,
                                0b101: self._alu_op_add,
                                0b110: self._alu_op_sub,
                                0b111: self._alu_op_mul}
        self._condition_handler = {0b0000: self._condition_always,
                                   0b0001: self._condition_lse,
                                   0b0010: self._condition_dse,
                                   0b0011: self._condition_gse,
                                   0b0100: self._condition_reserved1,
                                   0b0101: self._condition_reserved2,
                                   0b0110: self._condition_sw_trigger,
                                   0b0111: self._condition_domain_trigger,
                                   0b1000: self._condition_rc_trigger,
                                   0b1001: self._condition_zero,
                                   0b1010: self._condition_carry,
                                   0b1011: self._condition_overflow,
                                   0b1100: self._condition_sign,
                                   0b1101: self._condition_below_or_equal,
                                   0b1110: self._condition_less_than_or_equal,
                                   0b1111: self._condition_greater_than_or_equal}
        self._alu_registers = {x:0 for x in range(32)}
        
    def Read(self, name):
        '''Just need a valid register object'''
        return PatternControl()
    
    def Write(self, name, data):
        '''Required part of API, but not used for offline shmoo'''
        if hasattr(data, 'LinkMode'):
            self.link_width = 2**(data.LinkMode+1)
    
    def SinglePatternCaptureBaseAddress(self, offset):
        '''Required part of API, but not used for offline shmoo'''
        return 0
    
    def SetCaptureBaseAddress(self, offset):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def SetCaptureCounts(self):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def SetCaptureControl(self, a, b, c, d, e):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def WriteChannel(self, channel, register_name, value):
        '''Required part of API, but not used for offline shmoo'''
        pass
        
    def WritePattern(self, offset, pattern):
        self._pattern = pattern
    
    def PrestagePattern(self):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def WaitForCompletePattern(self, timeout):
        '''Required part of API, but no wait needed in offline use'''
        return True
    
    def ReadCapture(self):
        '''Required part of API, but not used for offline shmoo'''
        return []
    
    def UnpackCapture(self, data):
        '''Return one valid CaptureHeader for analysis'''
        return [(CaptureHeader(), None)]
    
    def get_alarms(self):
        return self._alarms
    
    def run_pattern(self, pattern, start, period):
        '''Run the pattern, as hardware would'''
        try:
            self._setup_for_burst(pattern, start, period)
        except RuntimeError as e:
            if 'Invalid period' in str(e):
                raise Exception('Period outside credit counter range')
            else:
                raise
        pattern_out_of_bounds = False
        try:
            for vectors in range(SIMULATOR_VECTOR_LIMIT):
                #### PERFORMANCE CRITICAL LOOP ####
                # vector type is top 2 bits of 16 byte vector
                vector_type = self._pattern[self._byte_offset + 15] >> 6
                self._vector_handler[vector_type]()
                if self.pattern_end_executed:
                    break
                self._byte_offset += BYTES_PER_VECTOR
            else:
                raise Ac.SimulatorLimitError('Simulator limit reached')
        except IndexError:
            if len(self._pattern) <= self._byte_offset:
                pattern_out_of_bounds = True
            else:
                raise
        if pattern_out_of_bounds:
            raise Exception('Simulator: Executing outside pattern')
    
    # def _pin_data_prestage(self):
        # '''Checks for PVC cache filled, then updates to method without check'''
        # self._pin_data()
        # if self._credit_counter.GetBalance() == PVC_CACHE_FULL:
            # self._credit_counter.CountAllCredit()
            # self._vector_handler[PIN_DATA_VECTOR_TYPE] = self._pin_data

    def _pin_data(self):
        #### PERFORMANCE CRITICAL METHOD ####
        # local repeat is bits 112-116 of vector
        repeat = self._pattern[self._byte_offset+14] & LOCAL_REPEAT_MASK
        # link enable is bit 121 of vector
        if self._pattern[self._byte_offset+15] & LINK_ENABLE_MASK:
            link_width = self.link_width
        else:
            link_width = 1
        self.pattern_end_executed = self._pattern_end_seen
        self._credit_counter.Vector(repeat, link_width)
    
    def _metadata(self):
        self._credit_counter.Metadata()
    
    def _pin_state(self):
        self.pattern_end_executed = self._pattern_end_seen
        self._credit_counter.Vector(0, 1)
    
    def _instruction(self):
        op_type = self._get_field(lsb=OP_TYPE_LSB, bits=OP_TYPE_BITS)
        self._instruction_handler[op_type]()
    
    def _external_op(self):
        self._credit_counter.Instruction()
        self._unhandled_vector()
    
    def _alu_op(self):
        self._credit_counter.Instruction()
        destination, source, operation = self._get_alu_op_fields()
        result = self._alu_op_handler[operation](source, destination)
    
    def _get_alu_op_fields(self):
        op_code = self._get_op_code()
        destination = op_code >> 5
        source = (op_code >> 3) & 0x03
        operation = op_code & 0x07
        return destination, source, operation
        
    def _alu_op_xor(self, source, destination):
        self._unhandled_vector()
    def _alu_op_and(self, source, destination):
        self._unhandled_vector()
    def _alu_op_or(self, source, destination):
        self._unhandled_vector()
    def _alu_op_lshl(self, source, destination):
        self._unhandled_vector()
    def _alu_op_lshr(self, source, destination):
        self._unhandled_vector()
    def _alu_op_add(self, source, destination):
        self._unhandled_vector()
    def _alu_op_sub(self, source, destination):
        a, b = self._get_signed_alu_sources(source)
        result = a - b
        self._store_signed_alu_result(destination, result)
        self._update_signed_alu_flags(result)
    def _alu_op_mul(self, source, destination):
        self._unhandled_vector()
    
    def _get_signed_alu_sources(self, source):
        if source == 0:
            return self._get_signed_reg_a(), self._get_immediate_signed()
        else:
            print('source =', source)
            self._unhandled_vector()
    
    def _get_signed_reg_a(self):
        reg_a = self._get_field(lsb=REGISTER_A_LSB, bits=REGISTER_A_BITS)
        return self._get_signed_register(reg_a)
    
    def _get_signed_register(self, index):
        if self._alu_registers[index] & SIGNED_SIGN_MASK:
            return self._alu_registers[index] - REGISTER_MAX_VALUE_PLUS_ONE
        else:
            return self._alu_registers[index]
    
    def _store_signed_alu_result(self, destination, result):
        if destination == 0b01:
            reg = self._get_destination_register_index()
            self._alu_registers[reg] = result & REGISTER_SIZE_MASK
        else:
            print('destination =', destination)
            self._unhandled_vector()
            
    def _update_signed_alu_flags(self, result):
        if result == 0:
            self._flags |= FLAGS_ZERO_MASK
        else:
            self._flags &= ~FLAGS_ZERO_MASK
        if result > SIGNED_MAX_POSITIVE:
            self._flags |= FLAGS_CARRY_MASK
        else:
            self._flags &= ~FLAGS_CARRY_MASK
        if result > SIGNED_MAX_POSITIVE or result < SIGNED_MAX_NEGATIVE:
            self._flags |= FLAGS_OVERFLOW_MASK
        else:
            self._flags &= ~FLAGS_OVERFLOW_MASK
        if result < 0:
            self._flags |= FLAGS_SIGN_MASK
        else:
            self._flags &= ~FLAGS_SIGN_MASK
            
    def _register_op(self):
        self._credit_counter.Instruction()
        op_code = self._get_op_code()
        source = op_code & 0x7
        destination = op_code >> 3
        source_data = self._register_source_handler[source]()
        self._register_op_handler[destination](source_data)
    
    def _reg_op_source_reg(self, source_data):
        self._unhandled_vector()
    def _reg_op_source_tcc(self, source_data):
        self._unhandled_vector()
    def _reg_op_source_vec_addr(self, source_data):
        self._unhandled_vector()
    def _reg_op_source_cfb(self, source_data):
        self._unhandled_vector()
    def _reg_op_source_flags(self, source_data):
        self._unhandled_vector()
    def _reg_op_source_dirty_mask(self, source_data):
        self._unhandled_vector()
    def _reg_op_source_stack(self, source_data):
        self._unhandled_vector()
        
    def _register_op_reg(self, source_data):
        dest = self._get_destination_register_index()
        self._alu_registers[dest] = source_data
        
    def _register_op_cfb(self):
        self._unhandled_vector()
        
    def _register_op_flags(self):
        self._unhandled_vector()
        
    def _register_op_stack(self):
        self._unhandled_vector()
    
    def _vector_op(self):
        op_code = self._get_op_code()
        vp_type = op_code >> 4
        vp_op = op_code & 0xF
        self._vector_op_handler[vp_type](vp_op)
    
    def _get_destination_register_index(self):
        return self._get_field(lsb=DESTINATION_LSB, bits=DESTINATION_BITS)
    
    def _get_op_code(self):
        return self._get_field(lsb=OP_CODE_LSB, bits=OP_CODE_BITS)
        
    def _vector_op_log(self, vp_op):
        self._credit_counter.Instruction()
        '''Ignore updates to the log registers'''
        
    def _vector_op_local(self, vp_op):
        self._vector_op_local_handler[vp_op]()
    
    def _vector_op_other(self, vp_op):
        self._credit_counter.Instruction()
        self._vector_op_other_handler[vp_op]()
        
    def _repeat_imm(self):
        count = self._get_immediate_unsigned()
        self._credit_counter.InstructionRepeat(count)
        
    def _end_imm(self):
        balance = self._credit_counter.Instruction()
        if balance < 3000:
            self._alarms.append('Buffer < 3k')
        elif balance < 6000:
            self._alarms.append('Buffer < 6k')
        elif balance < 9000:
            self._alarms.append('Buffer < 9k')
        self._pattern_end_seen = True
        
    def _fail_branch_imm(self):
        self._unhandled_vector()
        self._credit_counter.Instruction()
        
    def _repeat_reg(self):
        self._unhandled_vector()
        self._credit_counter.InstructionRepeat(count)
                
        
    def _end_reg(self):
        self._unhandled_vector()
        self._credit_counter.Instruction()
        
    def _fail_branch_r(self):
        self._unhandled_vector()
        self._credit_counter.Instruction()
    
    def _trigger_sib(self):
        self._unhandled_vector()
        
    def _block_fails_imm(self):
        self._unhandled_vector()
        
    def _drain_pin_fifo_imm(self):
        requested_balance = self._get_immediate_unsigned()
        self._credit_counter.DrainFifo(requested_balance)
    
    def _get_immediate_unsigned(self):
        return self._get_field(lsb=IMMEDIATE_LSB, bits=IMMEDIATE_BITS)
    
    def _get_immediate_signed(self):
        return self._get_field(lsb=IMMEDIATE_LSB, bits=IMMEDIATE_BITS, signed=True)
            
    def _edge_counter(self):
        self._unhandled_vector()
    def _clear_sticky_error(self):
        self._unhandled_vector()
    def _capture_imm(self):
        self._unhandled_vector()
    def _reset_cycle_count(self):
        self._unhandled_vector()
    def _reset_capture_memory(self):
        self._unhandled_vector()
    def _reset_fail_counts(self):
        self._unhandled_vector()
    def _drain_pin_fifo_reg(self):
        self._unhandled_vector()
    
    def _branch(self):
        balance = self._credit_counter.Branch()
        # print(balance)
        if self._branch_condition_is_true():
            branch_type = self._get_field(lsb=BRANCH_TYPE_LSB, bits=BRANCH_TYPE_BITS)
            self._branch_handler[branch_type]()
            # account for pattern loop increment
            self._byte_offset -= BYTES_PER_VECTOR
    
    def _branch_condition_is_true(self):
        condition = self._get_field(lsb=BRANCH_CONDITION_LSB, bits=BRANCH_CONDITION_BITS)
        invert = self._get_field(lsb=BRANCH_INVERT_LSB, bits=BRANCH_INVERT_BITS)
        if invert:
            return not self._condition_handler[condition]()
        else:
            return self._condition_handler[condition]()
        
    def _condition_always(self):
        return True
        
    def _condition_lse(self):
        self._unhandled_vector()
    def _condition_dse(self):
        self._unhandled_vector()
    def _condition_gse(self):
        self._unhandled_vector()
    def _condition_reserved1(self):
        self._unhandled_vector()
    def _condition_reserved2(self):
        self._unhandled_vector()
    def _condition_sw_trigger(self):
        self._unhandled_vector()
    def _condition_domain_trigger(self):
        self._unhandled_vector()
    def _condition_rc_trigger(self):
        self._unhandled_vector()
    def _condition_zero(self):
        return bool(self._flags & FLAGS_ZERO_MASK)
        
    def _condition_carry(self):
        self._unhandled_vector()
    def _condition_overflow(self):
        self._unhandled_vector()
    def _condition_sign(self):
        self._unhandled_vector()
    def _condition_below_or_equal(self):
        self._unhandled_vector()
    def _condition_less_than_or_equal(self):
        self._unhandled_vector()
    def _condition_greater_than_or_equal(self):
        self._unhandled_vector()
        
    def _no_branch(self):
        self._unhandled_vector()
        
    def _goto_imm(self):
        branch_base = self._get_field(lsb=BRANCH_BASE_LSB, bits=BRANCH_BASE_BITS)
        self._goto_imm_handler[branch_base]()
        
    def _goto_imm_global(self):
        imm = self._get_immediate_unsigned()
        self._byte_offset = imm * BYTES_PER_VECTOR
        
    def _goto_imm_cfb(self):
        self._unhandled_vector()
    def _goto_imm_relative(self):
        imm = self._get_immediate_signed()
        self._byte_offset += (imm * BYTES_PER_VECTOR)
    def _goto_imm_pfb(self):
        self._unhandled_vector()
        
    def _call_imm(self):
        self._stack.append(self._byte_offset)
        destination = self._get_immediate_unsigned()
        self._byte_offset = destination * BYTES_PER_VECTOR
    
    def _pcall_imm(self):
        '''same as call'''
        self._call_imm()
        
    def _ret(self):
        self._byte_offset = self._stack.pop() + BYTES_PER_VECTOR
        
    def _goto_reg(self):
        self._unhandled_vector()
    def _call_reg(self):
        self._unhandled_vector()
    def _pcall_reg(self):
        self._unhandled_vector()
    def _clear_stack(self):
        self._unhandled_vector()
    
    def _get_field(self, lsb, bits, signed=False):
        first_byte = self._byte_offset + (lsb // 8)
        last_byte = self._byte_offset + ((lsb + bits - 1) // 8)
        shift = lsb % 8
        mask = 2**bits-1
        return (int.from_bytes(self._pattern[first_byte:last_byte+1], byteorder='little', signed=signed) >> shift) & mask
        
    def _unhandled_vector(self):
        vector = int.from_bytes(self._pattern[self._byte_offset:self._byte_offset+BYTES_PER_VECTOR], byteorder='little')
        raise Ac.UnrecognizedVectorError(f'unhandled vector: 0x{vector:032X}')
        
    def _setup_for_burst(self, pattern, start, period):
        self._credit_counter = new_credit_counter('TOS_3.4', period)
        self._credit_counter.ModelPrestage()
        self._period = period
        self._pattern_end_seen = False
        self.pattern_end_executed = False
        if pattern is not None:
            self._pattern = pattern
        # self._vector_handler[PIN_DATA_VECTOR_TYPE] = self._pin_data_prestage
        self._vector_handler[PIN_DATA_VECTOR_TYPE] = self._pin_data
        self._byte_offset = start * BYTES_PER_VECTOR
    
    class UnrecognizedVectorError(Exception):
        pass
    
    class SimulatorLimitError(Exception):
        pass
    

class Rc():
    '''RC FPGA Model'''
    def ClearRCTrigger(self):
        '''Required part of API, but not used for offline shmoo'''
        pass
    
    def SendSyncPulse(self):
        '''Required part of API, but not used for offline shmoo'''
        pass
        

class PatternControl():
    '''Pattern control register'''
    def __init__(self):
        '''Implement fields needed for offline shmoo'''
        self.LinkMode = None
    
    def Pack(self):
        '''Required part of API, but not used for offline shmoo'''
        return 0
        

class CaptureHeader():
    '''Pattern capture block header'''
    def __init__(self):
        '''Indicate that the first vector is captured'''
        self.CapturedVectorsValidFlags = [1, 0, 0, 0, 0, 0, 0, 0]
        self.TotalCycleCount = 0
        

def credit_counter_shmoo_list():
    '''Filter online shmoo list.

    Match loop shmoos are unsupported by the simulator and irrelevant for the credit counter
    Flat patterns never underrun and increase runtime
    Captures are not relevant to credit counter use cases
    '''
    shmoos = onlineShmoos.shmoo_list()
    shmoos = [x for x in shmoos if 'MatchLoop' not in x[1]['basename']]
    shmoos = [x for x in shmoos if 'flat_vectors' not in x[1]['basename']]
    shmoos = [x for x in shmoos if 'capture_density' not in x[1]['basename']]
    shmoos = [x for x in shmoos if '50percent_capture' not in x[1]['basename']]
    return shmoos
    

if __name__ == '__main__':
    '''Run the offline shmoos'''
    onlineShmoos.Env = OfflineEnv
    onlineShmoos.minPeriod = Ac.MIN_PERIOD
    onlineShmoos.setup_output_directory('offline_shmoo')
    onlineShmoos.run(credit_counter_shmoo_list())