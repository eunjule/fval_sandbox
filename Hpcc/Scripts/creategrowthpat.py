################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: runcal.py
#-------------------------------------------------------------------------------
#     Purpose: stand alone, run calibration workbench
#-------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 2/2/17
#       Group: HDMT FPGA Validation
################################################################################

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
import argparse

from Common import fval
from Hpcc.hpcctb.assembler import PatternAssembler

def Main():

    parser = argparse.ArgumentParser()
    parser.add_argument('slot',   help = 'Slot that dev board is in', type = str, action = "store", default = None)
    parser.add_argument('-v', '--verbosity', help = 'Set verbosity level', type = str, action = "store", default='15')

    args = parser.parse_args()

    # set logging level
    if args.verbosity.isdigit():
        fval.SetLoggerLevel(int(args.verbosity))
    elif args.verbosity.upper() == "CRITICAL":
        fval.SetLoggerLevel(50)
    elif args.verbosity.upper() == "ERROR":
        fval.SetLoggerLevel(40)
    elif args.verbosity.upper() == "WARNING":
        fval.SetLoggerLevel(30)
    elif args.verbosity.upper() == "INFO":
        fval.SetLoggerLevel(20)
    elif args.verbosity.upper() == "DEBUG":
        fval.SetLoggerLevel(10)
    else:
        raise RuntimeError("Verbosity \'{}\' is not supported".format(args.verbosity))

    # enable logger, part of fval core, is inherited through tester.py
    fval.ConfigLogger()

    # Check for valid slot
    validSlots = range(0, 11)
    print(args.slot)
    slots = None
    if not(args.slot is None):
        slots = []
        for slot in args.slot.split(','):
             try:
                 if int(slot) in validSlots:
                     print("Slot under test: {}".format(slot))
                     slots.append(int(slot))
                 else:
                     raise ValueError
             except ValueError:
                 parser.error("Invalid Slot {}, must be between 0-11".format(slot))

                 
    pattern = PatternAssembler()

    patstr =   '\
              S stype=IOSTATEJAM,             data=0jXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZXZ\n\
                BlockFails 1 \n\
               Drive0CompareLVectors length=1024\n \
               BlockFails 0\n'

    repeatCount = 20

    maxAltRepeats = 20
    totalVectors = 0

    for i in range(repeatCount):
        stepCount = 1

        for x in range(maxAltRepeats):
            for y in range(stepCount):
                patstr = patstr + 'Drive0CompareLVectors length=1\n'
                totalVectors = totalVectors + 1

            for z in range(stepCount):
                patstr = patstr + 'Drive1CompareHVectors length=1\n'
                totalVectors = totalVectors + 1

            stepCount = stepCount + 1

    patstr = patstr + '              StopPattern 0x12345678 \n'
    pattern.LoadString(patstr)
    pattern.SaveObj('poo.obj')

    print('Total Vectors: {}'.format(totalVectors))


if __name__ == '__main__':
        Main()