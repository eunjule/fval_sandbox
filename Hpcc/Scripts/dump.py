################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: dump.py
#-------------------------------------------------------------------------------
#     Purpose: Snapshot dump tool
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 11/04/15
#       Group: HDMT FPGA Validation
################################################################################

import argparse
import os
import sys

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
sys.path.append(os.path.abspath(repo_root_path))

from Common import configs
from Common import fval
from Hpcc.hpcctb.env import Env


# Parse command line options
parser = argparse.ArgumentParser(description = 'Snapshot dump tool')
parser.add_argument('filename', help = 'Snapshot filename (must be .tar.gz)', type = str)
parser.add_argument('offset', help = 'First vector address to dump (in bytes). This value may be different from the start address.', type = int)
parser.add_argument('length', help = 'Pattern length (in bytes)', type = int)
args = parser.parse_args()


configs.HPCC_ENV_INIT = 'NONE'
fval.SetLoggerLevel(20)  # DEBUG=10, INFO=20, WARNING=30

class MockTest(object):
    _softErrors = []

fval.set_current_test(MockTest())

fval.ConfigLogger()
Env.Initialize()

env = Env(object())

env.TakeSnapshot(env.fpgas, args.offset, args.length, args.filename)

