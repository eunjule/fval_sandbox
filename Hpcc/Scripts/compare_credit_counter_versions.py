# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import sys

sys.path.append(os.path.abspath('../..'))

from _build.bin.creditcounter import CreditCounter as LegacyCreditCounter
from Hpcc.hpcctb.creditcounter_34 import CreditCounter as Tos34CreditCounter
from Hpcc.hpcctb.creditcounter_36 import CreditCounter as Tos36CreditCounter

def print_credit(function, start_mhz, stop_mhz):
    table = credit_table(function, start_mhz, stop_mhz)
    print_table(table, filename=f'{function.__name__}_{start_mhz}_{stop_mhz}.csv')
    
def print_table(table, filename):
    with open(filename, 'w') as f:
        for row in table:
            print(','.join([str(x) for x in row]), file=f)

def credit_table(function, start_mhz, stop_mhz):
    table = []
    for frequency_mhz in range(start_mhz, stop_mhz + 1):
        period = 1e-6/frequency_mhz
        row = [frequency_mhz,
               function(LegacyCreditCounter, period),
               function(Tos34CreditCounter, period),
               function(Tos36CreditCounter, period)]
        table.append(row)
    return table

def metadata_credit(CreditCounter, period):
    try:
        cc = CreditCounter(period)
    except RuntimeError:
        return ''
    return method_credit(cc, 'Metadata')

def instruction_credit(CreditCounter, period):
    try:
        cc = CreditCounter(period)
    except RuntimeError:
        return ''
    return method_credit(cc, 'Instruction')

def branch_credit(CreditCounter, period):
    try:
        cc = CreditCounter(period)
    except RuntimeError:
        return ''
    return method_credit(cc, 'Branch')

def repeat_credit(CreditCounter, period):
    try:
        cc = CreditCounter(period)
    except RuntimeError:
        return ''
    return method_credit(cc, 'InstructionRepeat', 1)

def drain_fifo_credit(CreditCounter, period):
    try:
        cc = CreditCounter(period)
    except RuntimeError:
        return ''
    return method_credit(cc, 'DrainFifo')

def method_credit(credit_counter, method_name, *args):
    try:
        method = getattr(credit_counter, method_name)
    except AttributeError:
        return ''
        
    try:
        balance = method(*args)
    except RuntimeError:
        return -credit_counter.GetUnderrunAmount()
    else:
        return balance



if __name__ == '__main__':
    try:
        os.mkdir('credit_counter_compare')
    except FileExistsError:
        pass
    os.chdir('credit_counter_compare')
    print_credit(metadata_credit, 500, 1500)
    print_credit(metadata_credit, 250, 500)
    print_credit(metadata_credit, 50, 250)
    print_credit(instruction_credit, 500, 1500)
    print_credit(instruction_credit, 250, 500)
    print_credit(instruction_credit, 50, 250)
    print_credit(branch_credit, 500, 1500)
    print_credit(branch_credit, 250, 500)
    print_credit(branch_credit, 50, 250)
    print_credit(repeat_credit, 500, 1500)
    print_credit(repeat_credit, 250, 500)
    print_credit(repeat_credit, 50, 250)
    print_credit(drain_fifo_credit, 500, 1500)
    print_credit(drain_fifo_credit, 250, 500)
    print_credit(drain_fifo_credit, 50, 250)
    
    


