################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

__author__ = 'Dean Glazeski'

import zlib
import os
import argparse
import struct


class SliceInformation(object):
    def __init__(self, data, slot, slot_slice):
        self.slot = slot
        self.data = data
        self.slice = slot_slice


class PatternInflator:
    def __init__(self, pattern):
        self.pattern = pattern

    def inflate(self, slot, slot_slice):
        raw_data = None
        compressed = 0
        with open(self.pattern, 'rb') as f:
            header_domain = 720  # first domain name
            for i in range(0, 15):
                f.seek(header_domain)
                domain_name_bytes = f.read(64)
                domain_offset_bytes = f.read(8)

                (domain_offset,) = struct.unpack('Q', domain_offset_bytes)
                if domain_offset == 0:
                    break

                domain_name = domain_name_bytes.decode('utf-8').rstrip('\0')

                print('Scanning domain {}...'.format(domain_name))
                f.seek(domain_offset + 592)  # slice compression flag
                (compressed,) = struct.unpack('B', f.read(1))
                slice_header_offset = domain_offset + 172  # first slice offset
                for j in range(0, 24):
                    f.seek(slice_header_offset)

                    slice_offset_bytes = f.read(8)
                    (slice_offset,) = struct.unpack('Q', slice_offset_bytes)
                    if slice_offset == 0:
                        break

                    slice_offset += domain_offset

                    f.seek(slice_offset + 12)  # offset to slot number
                    (header_slot,) = struct.unpack('B', f.read(1))
                    (header_slice,) = struct.unpack('B', f.read(1))
                    if slot == -1 or (slot == header_slot and (slot_slice == -1 or slot_slice == header_slice)):
                        data_offset_bytes = f.read(8)
                        data_size_bytes = f.read(8)

                        (data_offset,) = struct.unpack('Q', data_offset_bytes)
                        data_offset += slice_offset

                        (data_size,) = struct.unpack('Q', data_size_bytes)

                        print('Found slot {} and slice {} at {}'.format(header_slot, header_slice, data_offset))
                        f.seek(data_offset)
                        raw_data = f.read(data_size)
                        if compressed == 0:
                            yield SliceInformation(raw_data, header_slot, header_slice)
                        else:
                            yield SliceInformation(zlib.decompress(raw_data), header_slot, header_slice)

                    slice_header_offset += 16  # skip next size

                header_domain += 84  # jump to next domain

    def inflate_to_file(self, slot, slot_slice, base_output_file):
        for slice_info in self.inflate(slot, slot_slice):
            output_file = '{}.{}.{}'.format(base_output_file, slice_info.slot, slice_info.slice)
            with open(output_file, 'wb') as f:
                f.write(slice_info.data)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Inflates specific slice inside pattern object')
    parser.add_argument('pattern', metavar='PATTERN',
                        help='pattern to read slice data from')
    parser.add_argument('slot', metavar='SLOT', type=int, default=-1, nargs='?',
                        help='slot for data to inflate')
    parser.add_argument('slice', metavar='SLICE', type=int, default=-1, nargs='?',
                        help='slice for data to inflate')
    parser.add_argument('-o', '--output', metavar='OUT', default=None,
                        help='output file to put data into')

    args = parser.parse_args()

    input_pattern = os.path.abspath(args.pattern)
    output = args.output
    if output is None:
        output = input_pattern

    inflator = PatternInflator(input_pattern)
    try:
        inflator.inflate_to_file(args.slot, args.slice, output)
    except RuntimeError as e:
        print('Could not pull out slice: {}'.format(e))


