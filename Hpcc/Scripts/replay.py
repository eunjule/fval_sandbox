################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: replay.py
#-------------------------------------------------------------------------------
#     Purpose: Snapshot runner tool
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 11/04/15
#       Group: HDMT FPGA Validation
################################################################################

import argparse
import os
import sys
import tarfile
import tempfile

from Common import configs
from Common import fval

from Hpcc.hpcctb.env import Env

configs.HPCC_ENV_INIT = 'NONE'
configs.HPCC_ALARM_DEBUG = True
configs.HPCC_TAKE_SNAPSHOT = True
configs.HPCC_NO_PAT_SNAPSHOT = True
fval.SetLoggerLevel(10)  # DEBUG=10, INFO=20, WARNING=30

class MockTest(object):
    _softErrors = []
    def Name(self):
        return 'PostReplaySnapshot'

def StartPattern(env, slot, slice, period):
    hpcc = env.instruments[slot]
    start = hpcc.ac[slice].Read('PatternStartAddress').Pack()
    hpcc.ac[slice].captureBaseAddress = hpcc.ac[slice].Read('CaptureBaseAddress').Pack() 
    hpcc.ac[slice].SetPeriod(period)
    #hpccModel = env.models[slot]
    #hpccModel.acSim[slice].SetDriveDelay(list(range(56)), 0.0)
    #hpccModel.acSim[slice].SetCompareDelay(list(range(56)), period / 2)
    
    if (slot, slice) in env.enabledIlas:
        hpcc.ac[slice].LoadAndArmIlas(slot, slice)

    completed = hpcc.ac[slice].ExecutePattern(start)
    
    if (slot, slice) in env.enabledIlas:
        hpcc.ac[slice].DumpIlas(slot, slice)

    if not completed:
        env.Log('error', 'Pattern execution did not complete')
        
    #env.RunCheckers(slot, slice)
    env.CheckAlarms(slot, slice)
    env.Log('info', 'Pattern completed = {}'.format(completed))
    env.Log('info', 'Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl').Pack()))
    env.Log('info', 'New Alarm Control = 0b{:032b}'.format(hpcc.ac[slice].Read('AlarmControl1').Pack()))
    env.Log('info', 'Pattern End Status = 0x{:x}'.format(hpcc.ac[slice].Read('PatternEndStatus').Pack()))
    env.Log('info', 'Capture Address Underrun = 0x{:x}'.format(hpcc.ac[slice].Read('CaptureAddressUnderrun').Pack()))

fval.set_current_test(MockTest())

import hpcctb

# Parse command line options
parser = argparse.ArgumentParser(description = 'Snapshot replay tool')
parser.add_argument('filename', help = 'Snapshot filename (must be .tar.gz)', type = str)
parser.add_argument('slots_and_slices', help = 'Slots and slices to load and execute (e.g. 9,0)', type = str)
parser.add_argument('period', help = 'Period to execute the snapshot', type = float)
parser.add_argument('-c', '--config', help = 'Loopback configuration', type = str, action = "store", default = 'InternalLoopback')
args = parser.parse_args()

snapshotDir = tempfile.mkdtemp()

print('Extracting snapshot to {}'.format(snapshotDir))
tar = tarfile.open(args.filename, "r:gz")
tar.extractall(snapshotDir)
tar.close()

subdirs = next(os.walk(snapshotDir))[1]
if len(subdirs) == 1:
    snapshotDir = os.path.join(snapshotDir, subdirs[0])
    print('Updated snapshotDir to {}'.format(snapshotDir))

fpgas = set()
slotSliceMap = {}

slotsAndSlices = args.slots_and_slices.split(' ')
for slotSlice in slotsAndSlices:
    slotSlice = slotSlice.split(':')
    if len(slotSlice) == 1:
        slot, slice = [int(x) for x in slotSlice[0].split(',')]
        slotSliceMap[(slot, slice)] = (slot, slice)
        fpgas.add((slot, slice))
    elif len(slotSlice) == 2:
        slot, slice = [int(x) for x in slotSlice[0].split(',')]
        targetSlot, targetSlice = [int(x) for x in slotSlice[1].split(',')]
        slotSliceMap[(slot, slice)] = (targetSlot, targetSlice)
        fpgas.add((targetSlot, targetSlice))

fval.ConfigLogger()
hpcctb.Env.Initialize()

env = Env(MockTest())
env.SetConfig(args.config)
env.enabledCalDumps = []
env.fpgas = list(fpgas)

for source, target in slotSliceMap.items():
    slot, slice = source
    targetSlot, targetSlice = target
    path = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\PatternMemory'.format(slot, slice))
    filename = os.path.join(snapshotDir, r'HPCC_Slot{}\AcFpga{}\registers.csv'.format(slot, slice))
    env.LoadPatternMemoryFromSnapshot(targetSlot, targetSlice, path)
    env.LoadRegistersFromSnapshot(targetSlot, targetSlice, filename)
    StartPattern(env, targetSlot, targetSlice, args.period)

