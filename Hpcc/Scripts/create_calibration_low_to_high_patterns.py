################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: runcal.py
#-------------------------------------------------------------------------------
#     Purpose: stand alone, run calibration workbench
#-------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 2/2/17
#       Group: HDMT FPGA Validation
################################################################################
if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
import argparse
import time

from Common import fval
from Hpcc.hpcctb import disassembler
from Hpcc.hpcctb.assembler import PatternAssembler

def Main():

    parser = argparse.ArgumentParser(description = 'PATGEN TOOL')
#    parser.add_argument('type',   help = 'LTH or ETO', type = str, action = "store", default = None)
    parser.add_argument('length',   help = 'length of random vectors in pattern', type = str, action = "store", default = None)

    parser.add_argument('-v', '--verbosity', help = 'Sset verbosity level', type = str, action = "store", default='15')

    args = parser.parse_args()

    # set logging level
    if args.verbosity.isdigit():
        fval.SetLoggerLevel(int(args.verbosity))
    elif args.verbosity.upper() == "CRITICAL":
        fval.SetLoggerLevel(50)
    elif args.verbosity.upper() == "ERROR":
        fval.SetLoggerLevel(40)
    elif args.verbosity.upper() == "WARNING":
        fval.SetLoggerLevel(30)
    elif args.verbosity.upper() == "INFO":
        fval.SetLoggerLevel(20)
    elif args.verbosity.upper() == "DEBUG":
        fval.SetLoggerLevel(10)
    else:
        raise RuntimeError("Verbosity \'{}\' is not supported".format(args.verbosity))

    # enable logger, part of fval core, is inherited through tester.py
    fval.ConfigLogger()

    patLength = int(args.length)

    receivePattern = PatternAssembler()
    drivePattern = PatternAssembler()


    ioJamForDrive = 'S stype=0, data=0xaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
    ioJamForReceive = 'S stype=0, data=0xffffffffffffffffffffffffffff'


    #Create Low to High Patterns

    drivePattern.LoadString("""
              {}
              BlockFails 1
              %repeat 1024
              V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
              %end
              BlockFails 0
              RandomPassingEvenToOddNoSwitchVectors length={}
              StopPattern 0x12345678
          """.format(ioJamForDrive,patLength))
    drivePattern.Generate()

    diassemblerObj = disassembler.Pattern()
    drivePatternPatObj = drivePattern.cachedObj
    vectorCount = int(len(drivePatternPatObj) / 16)
    drivePatternArray = []
    for i in range(vectorCount):
        disVec = diassemblerObj.disassemble(drivePatternPatObj, i * 16)
        drivePatternArray.append(disVec)

    receivePattern.LoadString("""
              {}
              BlockFails 1
              %repeat 1024
              V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
              %end
              BlockFails 0
              RandomPassingEvenToOddNoSwitchVectors length={}
              StopPattern 0x12345678
          """.format(ioJamForReceive,patLength))
    receivePattern.Generate()

    receivePatternPatObj = receivePattern.cachedObj
    vectorCount = int(len(receivePatternPatObj) / 16)
    receivePatternArray = []
    for i in range(vectorCount):
        disVec = diassemblerObj.disassemble(receivePatternPatObj, i * 16)
        receivePatternArray.append(disVec)

    # print("DRIVE PATTERN")
    # for vec in drivePatternArray:
    #     print(vec)

    receivePatternArray[1] =  'BlockFails 1'
    receivePatternArray[2+1024] = 'BlockFails 0'
    receivePatternArray[3+1024+patLength] = 'StopPattern    0x12345678'
    receivePatternArray = receivePatternArray[:4+1024+patLength]

    for x in range(patLength):
        receivePatternArray[1024 + 3 + x] = drivePatternArray[1024 + 3 + x]

    receivePatternString = '\n'.join(receivePatternArray)
    receivePatternNew = PatternAssembler()
    receivePatternNew.LoadString(receivePatternString)
    receivePatternNew.Generate()

    #save Patterns to files
    currentTime = time.time()
    drivePattern.SaveObj('Calibration_low_to_high_drive_{}.obj.gz'.format(currentTime))
    receivePatternNew.SaveObj('Calibration_low_to_high_receive_{}.obj.gz'.format(currentTime))

    # print("RECEIVE PATTERN")
    # for vec in receivePatternArray:
    #      print(vec)





if __name__ == '__main__':
        Main()