################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: powerVirus.py
#-------------------------------------------------------------------------------
#     Purpose: 
#-------------------------------------------------------------------------------
#  Created by: 
#        Date: 
#       Group: HDMT FPGA Validation
################################################################################

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
import argparse
import time

from Common import configs
from Common import fval
from Common import hilmon as hil
from Hpcc.hpcctb.env import Env

fval.SetLoggerLevel(20)  # DEBUG=10, INFO=20, WARNING=30


class MockTest(object):
    _softErrors = []

fval.set_current_test(MockTest())


TOTAL_CAPTURE_COUNT            = 0x8440
RESET_PLACE                    = 0x8110 
BIST_CONTROL_REG               = 0x8820
BIST_STATUS_REG                = 0x8830
BRAM_1_RESULTS                 = 0xC000
BRAM_2_RESULTS                 = 0xC010
LRAM_RESULTS                   = 0xC020
LUT_RESULTS                    = 0xC030

hpcc = None
env = None
verbose = False

def set_config_flags(args):
    if args.flash_empty:
        configs.HPCC_AC_FPGA_PATH = r'X:\FPGAs\HPCC_AC\debug\FabD\A3_silicon\hpcc_ac_variant0_mini_build_no_dimm__2015-10-30_11.45.38__empty__v.11.0.2'
    else:
        configs.HPCC_AC_FPGA_PATH = r'X:\FPGAs\HPCC_AC\debug\FabD\A3_silicon\hpcc_ac_variant0_power_virus__2015-11-09_01.13.41__pv_all__v.11.0.2'
        
    #hpcc_ac_variant0_power_virus__2015-10-27_20.46.54__pv_all_pcie_debug__v.11.0.2
    #hpcc_ac_variant0_power_virus__2015-10-26_23.50.38__lram_debug_6__v.11.0.2
    #hpcc_ac_variant0_power_virus__2015-11-09_01.13.41__pv_all__v.11.0.2
    #hpcc_ac_variant0_power_virus__2015-10-29_18.14.07__pv_all_10_29__v.11.0.2
    if args.flash or args.flash_empty:
        configs.HPCC_ENV_INIT = 'FULL'
    else: 
        configs.HPCC_ENV_INIT = 'NONE'
    
def Set9914Frequency(args ):
    #args.freq is in mega hertz
    ad9914_period = ( 1e-6 / (2 * args.freq) )
    print(ad9914_period)
   
    for (slot, slice) in env.fpgas:
        hpcc = env.instruments[slot]  
        hpcc.ac[slice].ad9914.SetPeriod(ad9914_period)
        hpcc.ac[slice].ad9914.EnableOutput(False)
        hpcc.ac[slice].ad9914.EnableOutput(True)
    
def read_power_virus_error_flag(slot, slice):
    r = hil.hpccAcBarRead(slot, slice, 0, TOTAL_CAPTURE_COUNT)
    return r
    
    
def read_bist_engine_result(slot, slice):
    r = hil.hpccAcBarRead(slot, slice, 0, BIST_STATUS_REG)
    return r
    
def reset_power_virus(slot, slice):
    #set reset low
    # read reset reg
    r = hil.hpccAcBarRead(slot, slice, 0, RESET_PLACE )
    if verbose:
        print('slot {} slice {} initial value read from reset reg {}'.format(slot, slice, hex(r)))
    #set bit 25 to zero
    r = r & ~ (0x1 << 25)
    if verbose:
        print('slot {} slice {} writing zero to bit 25 in reset reg {}'.format(slot, slice, hex(r)))
    # write back reset reg
    hil.hpccAcBarWrite(slot, slice, 0, RESET_PLACE, r)
    
    time.sleep(5)
    # read total capture to make sure all errors are zero. 
    error_flag = read_power_virus_error_flag(slot, slice)
    r = hil.hpccAcBarRead(slot, slice, 0, RESET_PLACE )
    if verbose:
        print('slot {} slice {} reading from reset reg. {}'.format(slot, slice, hex(r)))
        print('slot {} slice {} power virus result when reset is zero {}'.format(slot, slice, hex(error_flag)))
    
    # come out of reset by setting bit 25 to high. 
    r = r | (0x1 << 25)
    if verbose:
        print('slot {} slice {} writing high to bit 25 in reset reg {}'.format(slot, slice, hex(r)))
    hil.hpccAcBarWrite(slot, slice, 0, RESET_PLACE, r)
    time.sleep(5)
 

def reset_bist_engine(slot, slice):

    #set bit 0 in reset reg to high
    r = hil.hpccAcBarRead(slot, slice, 0, RESET_PLACE )
    r = r | 0x1
    hil.hpccAcBarWrite(slot,slice,0,RESET_PLACE,r)
    time.sleep(1)
    
    
    #set bit 0 in reset reg to low
    r = hil.hpccAcBarRead(slot, slice, 0, RESET_PLACE )
    r = r & ~(0x1)
    hil.hpccAcBarWrite(slot,slice,0,RESET_PLACE,r)
    time.sleep(1)
    
    
    
    
    #write to bist reg
    hil.hpccAcBarWrite(slot,slice,0,BIST_CONTROL_REG,0x0)
    time.sleep(1)
    hil.hpccAcBarWrite(slot,slice,0,BIST_CONTROL_REG,0x03030303)
    time.sleep(1)



 
def parse_arguments():
    parser = argparse.ArgumentParser(description='POWER VIRUS')
    parser.add_argument('freq',         type=int, help='Frequency in Mega Hz.')
    parser.add_argument('-f', '--flash', action='store_true' )
    parser.add_argument('-fe', '--flash_empty', action='store_true' )
    
    return parser.parse_args()


        
if __name__ == '__main__': 
    args = parse_arguments()
    #slot = 7
    #slice = 0
    

    set_config_flags(args)
    
    
    
    fval.ConfigLogger()
    time.sleep(3)
    Env.Initialize()
    time.sleep(3)
    env = Env(object())
    time.sleep(3)
    
    #set freq of ad9914
    Set9914Frequency(args); 
    time.sleep(3)
    
    if verbose:
        print('VALUE IN REG BEFORE RESET')
        for (slot, slice) in env.fpgas:
            if(slot != '2'):
                bram_data_1 = hil.hpccAcBarRead(slot, slice, 0, BRAM_1_RESULTS )
                bram_data_2 = hil.hpccAcBarRead(slot, slice, 0, BRAM_2_RESULTS )
                lram_data = hil.hpccAcBarRead(slot, slice, 0, LRAM_RESULTS )
                lut_data = hil.hpccAcBarRead(slot, slice, 0,  LUT_RESULTS )
                reset_reg = hil.hpccAcBarRead(slot, slice, 0, RESET_PLACE )
                power_virus_result  = read_power_virus_error_flag(slot, slice)
                bist_engine_result  = read_bist_engine_result(slot, slice)
                print('slot {} slice {} bram_data_1 = {}  bram_data_2 = {} lram_data = {}  lut_data = {}'.format(slot, slice, hex(bram_data_1), hex(bram_data_2), hex(lram_data), hex(lut_data)))
                print('slot {} slice {} power_virus_result = {}  bist_engine_result = {}  reset reg = {}'.format(slot, slice, hex(power_virus_result), hex(bist_engine_result), hex(reset_reg)))
        print('#########################################################################################################')
    
    if not args.flash_empty:
        for (slot, slice) in env.fpgas:
            reset_power_virus(slot, slice)
            reset_power_virus(slot, slice)
            reset_bist_engine(slot, slice)       
    else:
        print('Erased Power Virus Image!')

     
    print('VALUE IN  REGISTERS')
    for (slot, slice) in env.fpgas:
        #for i in range(10):
        bram_data_1 = hil.hpccAcBarRead(slot, slice, 0, BRAM_1_RESULTS )
        bram_data_2 = hil.hpccAcBarRead(slot, slice, 0, BRAM_2_RESULTS )
        lram_data = hil.hpccAcBarRead(slot, slice, 0, LRAM_RESULTS )
        lut_data = hil.hpccAcBarRead(slot, slice, 0,  LUT_RESULTS )
        reset_reg = hil.hpccAcBarRead(slot, slice, 0, RESET_PLACE )
        power_virus_result  = read_power_virus_error_flag(slot, slice)
        bist_engine_result  = read_bist_engine_result(slot, slice)
        print('slot {} slice {} bram_data_1 = {}  bram_data_2 = {} lram_data = {}  lut_data = {}'.format(slot, slice, hex(bram_data_1), hex(bram_data_2), hex(lram_data), hex(lut_data)))
        print('slot {} slice {} power_virus_result = {}  bist_engine_result = {}  reset reg = {}'.format(slot, slice, hex(power_virus_result), hex(bist_engine_result), hex(reset_reg)))
                
    
