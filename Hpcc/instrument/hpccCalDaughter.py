################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: hpccCalDaughter.py
#-------------------------------------------------------------------------------
#     Purpose: Class for HPCC Cal Daughter board
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/06/15
#       Group: HDMT FPGA Validation
################################################################################

import os

from Common import fval
from Common import hilmon as hil
from Common.instruments.base import Instrument

PCA9505_OUTPUT_PORT0 = 8
PCA9505_CONFIGURATION_PORT0 = 24
PCA9505_POLARITY_INVERSION_PORT0 = 16
PCA9505_AUTO_INCREMENT_FLAG = 0x80
NUMBER_PCA9505_PORTS = 5
            
HPCC_CAL_DS = os.path.join(os.path.dirname(__file__), 'hpccCal.ds')
HPCC_CAL_STRUCTS = fval.LoadStructs(HPCC_CAL_DS)


class HpccCalDaughter(Instrument):
    def __init__(self, slot):
        super().__init__('')
        self._slot = slot
        self._PCABits = HPCC_CAL_STRUCTS

    @property
    def cardFound(self):
        if not hasattr(self, '_found'):
            for retries in range(3):
                self.Connect()
                if self._found:
                    break    
        return self._found
    
    def Connect(self):  
        try:
            hil.calHpccConnect(self._slot)
            self._found = True
        except RuntimeError:
            self._found = False
    
    def Initialize(self):
        initData = bytes(NUMBER_PCA9505_PORTS)
        self.WriteBytes(PCA9505_OUTPUT_PORT0, initData)
        self.WriteBytes(PCA9505_CONFIGURATION_PORT0, initData)
        self.WriteBytes(PCA9505_POLARITY_INVERSION_PORT0, initData)
    
    def WriteBytes(self, address, bytedata):
        for i, byte in enumerate(bytedata):
            hil.calHpccPca9506Write(self._slot, address+i, byte)
        
    def _GetPortData(self, data):
        result = bytearray(NUMBER_PCA9505_PORTS)
        for i in range(NUMBER_PCA9505_PORTS):
            result[i] = (data >> (8 * i)) & 0xff
        return bytes(result)
        
    def _SelectChannelToTreeTop(self, channel, u36):
        if channel < 0 or channel > 111:
            self.Log('error', 'channel number {} out of the range [0,111]'.format(channel))
        
        if channel & 1:
            u36.SelLevel0 = 1
        else:
            u36.SelLevel0 = 0
            
        if channel & 2:
            u36.SelLevel1 = 1
        else:
            u36.SelLevel1 = 0
            
        if channel & 4:
            u36.SelLevel2 = 1
        else:
            u36.SelLevel2 = 0
            
        if channel & 8:
            u36.SelLevel3 = 1
        else:
            u36.SelLevel3 = 0
            
        if channel & 16:
            u36.SelLevel4 = 1
        else:
            u36.SelLevel4 = 0
            
        if channel & 32:
            u36.SelLevel5 = 1
        else:
            u36.SelLevel5 = 0
            
        if channel & 64:
            u36.SelLevel6 = 1
        else:
            u36.SelLevel6 = 0
            
        return u36
    
    def ClearAll(self):
        if not self.cardFound:
            return
        u36 = self._PCABits.U36(uint = 0, length = 40)         
        u36Data = self._GetPortData(u36.Pack())
        self.WriteBytes(PCA9505_OUTPUT_PORT0, u36Data)
    
    def SelectChannelToSma(self, channel):
        u36 = self._PCABits.U36(uint = 0, length = 40) 
        u36 = self._SelectChannelToTreeTop(channel, u36)
        u36.MeasureSel0Drv = 0
        u36.MeasureSel1Drv = 0
        u36.SelTreeOut = 1
        # Apply
        u36Data = self._GetPortData(u36.Pack())
        self.WriteBytes(PCA9505_OUTPUT_PORT0, u36Data)        
    
    def SetEvenToOddChannelLoopback(self):
        if not self.cardFound:
            return
        u36 = self._PCABits.U36(uint = 0, length = 40) 
        u36.SelBank0Even = 1
        u36.SelBank0Odd = 1
        u36.SelBank1Even = 1
        u36.SelBank1Odd = 1
        u36.SelLb0Even = 1
        u36.SelLb0Odd = 1
        u36.SelLb1Even = 1
        u36.SelLb1Odd = 1
        # Apply
        u36Data = self._GetPortData(u36.Pack())
        self.WriteBytes(PCA9505_OUTPUT_PORT0, u36Data)
        
    def SetLowToHighChannelLoopback(self):
        if not self.cardFound:
            return
        u36 = self._PCABits.U36(uint = 0, length = 40) 
        u36.SelBank0Even = 1
        u36.SelBank0Odd = 1
        u36.SelBank1Even = 1
        u36.SelBank1Odd = 1
        # Apply
        u36Data = self._GetPortData(u36.Pack())
        self.WriteBytes(PCA9505_OUTPUT_PORT0, u36Data)

