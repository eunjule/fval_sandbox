################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: lmk04808.py
#-------------------------------------------------------------------------------
#     Purpose: Class for clock lmk04808
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/16/15
#       Group: HDMT FPGA Validation
################################################################################

from collections import namedtuple
import time

from Common.fval import Log
from Common.instruments.base import Instrument
import Hpcc.instrument.hpccAcRegs as ac_registers

Command = namedtuple('Command', ['address', 'data'])

def create_hpcc_init_sequence(fine_delay_divider, hpcc2):
    '''Creates a list of commands to initialize the LMK04808'''
    seq = [ Command(address=0 , data=0x00020000),
            Command(address=0 , data=0x00140020),
            Command(address=1 , data=0x00140020),
            Command(address=2 , data=0x00140000 | (fine_delay_divider << 5))]
    
    if hpcc2:
        seq.extend([Command(address=3 , data=0x00140300),  #for 125Mhz clock on EU15-clock out 7
                    Command(address=4 , data=0x00140180)])
    else:
        seq.extend([Command(address=3 , data=0x00140180),
        # seq.extend([Command(address=3 , data=0x80140C00),
                    Command(address=4 , data=0x00140300)])
    
    seq.extend([Command(address=5 , data=0x00140300),
                Command(address=6 , data=0x44440000)])
    
    if hpcc2:
        seq.extend([Command(address=7 , data=0x1c010000),
                    Command(address=8 , data=0x00010000)])
    else:
        seq.extend([Command(address=7 , data=0x40010000),
                    Command(address=8 , data=0x01000000)])
    
    seq.append(Command(address=9 , data=0x55555540))
    
    if hpcc2:
        seq.append(Command(address=10, data=0x914249a0))
    else:
        seq.append(Command(address=10, data=0x91424980))
    
    seq.extend([Command(address=11, data=0x14021000),
                Command(address=12, data=0x1b0c0060),
                Command(address=13, data=0x3b249000),
                Command(address=14, data=0xd4000000),
                Command(address=15, data=0x80008000),
                Command(address=16, data=0xc1550400),
                Command(address=24, data=0x00000000),
                Command(address=25, data=0x02c80000),
                Command(address=26, data=0xafa80000),
                Command(address=27, data=0x10001F40),
                Command(address=28, data=0x00201F40),
                Command(address=29, data=0x01800180),
                Command(address=30, data=0x02000180),
                Command(address=31, data=0x001F0000)])
    return seq


class Lmk04808(Instrument):
    def __init__(self, hpcc, name = None):
        Log('debug','Instantiate an Lmk04808 instance')
        super(Lmk04808, self).__init__(name)
        self._hpcc = hpcc # so that the clock can access methods in hpcc class
    
    def Initialize(self, fineDelayDivider, hpcc2=False):
        Log('info','Initializing an Lmk04808 instance')
        # Make sure we aren't in sync mode
        regResetsAndStatus = self._hpcc.Read('ResetsAndStatus')
        regResetsAndStatus.Lmk04808Sync = 0
        self._hpcc.Write('ResetsAndStatus', regResetsAndStatus)
        
        init_sequence = create_hpcc_init_sequence(fineDelayDivider, hpcc2)
        for command in init_sequence:
            self._Write(*command)

        # Sync Outputs
        regResetsAndStatus.Lmk04808Sync = 1
        self._hpcc.Write('ResetsAndStatus', regResetsAndStatus)
        time.sleep(0)
        regResetsAndStatus.Lmk04808Sync = 0
        self._hpcc.Write('ResetsAndStatus', regResetsAndStatus)
            
    # LS5B ignore, suppose to be address
    def _Write(self, address, data):
        regLMK4808WRData = ac_registers.LMK4808WRData()
        regLMK4808WRData.WRData = data >> 5
        self._hpcc.Write('LMK4808WRData', regLMK4808WRData)      
        
        regLMK4808Control = ac_registers.LMK4808Control()
        regLMK4808Control.Address = address
        regLMK4808Control.RdEnable = 0
        self._hpcc.Write('LMK4808Control', regLMK4808Control)
        time.sleep(0.001)
