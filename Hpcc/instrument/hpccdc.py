################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: hpccdc.py
#-------------------------------------------------------------------------------
#     Purpose: Hpcc DC instrument code, one slice
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/04/15
#       Group: HDMT FPGA Validation
################################################################################

import time

from Common import hilmon as hil

from Common import fval
from Common.globals import ALL_PINS, CHANNEL_PER_FPGA, EVEN_PINS, ODD_PINS
from Common.instruments.base import Instrument
from Hpcc.instrument.adate320 import AdAte320
from ThirdParty.SASS.suppress_sensor import suppress_sensor

DC_DATA_CAPTURE_HEADER_SIZE = 32 * 8 # 32 bytes
DcDataCaptureHeadersSegment = 0x1F000000 # 0x1FFF_FFFF - 0x1F00_0000 (16MB)

class HpccDc(Instrument):
    def __init__(self, hpcc, slot, slice):
        super().__init__('')
        self.hpcc = hpcc
        self.dcRegisters = hpcc.dcRegisters
        self.peRegs = hpcc.peRegs
        self.slot = slot
        self.slice = slice
        self.NUMBER_TRIES = hpcc.NUMBER_TRIES
        self.pe = []
        self.CLC_MAX = 7
        for channelNum in range(CHANNEL_PER_FPGA):
            self.pe.append(AdAte320(self, channelNum))
        self._resultMap = dict()
        self._samplingMap = dict()

    def name(self):
        return f'{self.__class__.__name__}_{self.slot}_{self.slice}'

    def FpgaLoad(self, image):
        
        with suppress_sensor(f'FVAL HPCC DC {self.slot}', f'HDMT_HPCCDC:{self.slot}'),\
             suppress_sensor(f'FVAL HPCC AC WHILE PROGRAMMING DC {self.slot}', f'HDMT_HPCCAC:{self.slot}'):
            with HpccDc.FpgaPcieDriverDisable(self.slot):
                result = hil.hpccDcFpgaLoad(self.slot, self.slice, image)
            time.sleep(1)
        return result

    class FpgaPcieDriverDisable():
        def __init__(self, slot):
            self.slot = slot

        def __enter__(self):
            hil.hpccDcDeviceDisable(self.slot, 0)
            hil.hpccDcDeviceDisable(self.slot, 1)
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            hil.hpccDcDeviceEnable(self.slot, 0)
            hil.hpccDcDeviceEnable(self.slot, 1)

    def GetFpgaVersion(self):
        return hil.hpccDcFpgaVersion(self.slot, self.slice)
    
    def BarRead(self, bar, offset):
        return hil.hpccDcBarRead(self.slot, self.slice, bar, offset)

    def BarWrite(self, bar, offset, data):
        return hil.hpccDcBarWrite(self.slot, self.slice, bar, offset, data)

    def DmaRead(self, address, length):
        return hil.hpccDcDmaRead(self.slot, self.slice, address, length)

    def DmaWrite(self, address, data):
        return hil.hpccDcDmaWrite(self.slot, self.slice, address, data)

    def Read(self, registerName):
        register = getattr(self.dcRegisters, registerName)
        return register(uint = self.BarRead(register.BAR, register.ADDR),
                       length = 32)

    def Write(self, registerName, data):
        register = getattr(self.dcRegisters, registerName)
        if isinstance(data, fval.BitStruct):
            return self.BarWrite(register.BAR, register.ADDR, data.Pack())
        else:
            return self.BarWrite(register.BAR, register.ADDR, data)
            
    def ReadPMU(self, channel):
        register = getattr(self.dcRegisters, 'Ad7609PmuMeasurement')
        address = register.BASEADDR + channel * register.BASEMUL
        return register(uint = self.BarRead(register.BAR, address), length = 32)
    
    def _ClearResults(self, clearChannels):
        self.Log('debug', 'HPCC DC {} SLICE{}: Reset Instruction Block and Data Capture.'.format(self.slot, self.slice))
        regResets = self.dcRegisters.Resets(uint = 0, length = 32) 
        regResets.InstructionBlockDomainReset = 1
        regResets.DataCaptureReset = 1
        self.Write('Resets', regResets)
        
        # pull until Resets are cleared or timeout
        timeout = time.time() + 1 # 1 sec timeout
        regResets = self.Read('Resets')
        while (regResets.InstructionBlockDomainReset == 1 or regResets.DataCaptureReset == 1) and (time.time() < timeout):
            regResets = self.Read('Resets')

        if regResets.InstructionBlockDomainReset == 1:
            self.Log('error', 'HPCC DC {} SLICE{}: Reset Instruction Block Failed.'.format(self.slot, self.slice))
        if regResets.DataCaptureReset == 1:
            self.Log('error', 'HPCC DC {} SLICE{}: Reset Data Capture Failed.'.format(self.slot, self.slice))

        if clearChannels:
            self._resultMap = dict()
            self._samplingMap = dict()
                  
    def _InitDCFPGA(self):
        self.Log('info', 'Initializing HPCC DC FPGA slot {} slice {}'.format(self.slot, self.slice))
        # Resets the Instruction Block Execution Registers state
        self._ClearResults(True)
        self._EnableTriggers()

    def _EnableTriggers(self):
        if self.slice == 0:
            self.Log('debug', 'Enabled trigger bus for slot {}'.format(self.slot))
            data = self.Read('Gpio')
            data.DisableUspiInputs = 0
            data.DisableAuroraInput = 0
            self.Write('Gpio', data)

    def _CoreInitDC(self):
        # Zero out trigger block 0 (the Stop Block) as well as both Trigger-to-TriggerBlock mapping tables, so they point to the stop block.
        zeroMemory = bytes(SIZE_OF_TRIGGER_BLOCK)
        self.DmaWrite(DC_TRIGGER_BLOCKS_SEGMENT, zeroMemory)
        self.DmaWrite(DC_TRIGGER_BLOCK_MAPPING_SEGMENT, zeroMemory)
        self.DmaWrite(LTC_TRIGGER_BLOCK_MAPPING_SEGMENT, zeroMemory)
        
        #if self.slice == 0:
        #    self._ResetAuroraInterface()        
        #self._ResetTriggerDomains()
        
    def _ResetAuroraInterface(self):
        MAX_RESETS = 10
        
        regTriggerErrors = self.Read('TriggerErrors')
        self.Log('debug', 'HPCC DC {} SLICE{}: Trigger Error Register 0x{:X}.'.format(self.slot, self.slice, regTriggerErrors.Pack()))
        
        resetCount = 1
        while (regTriggerErrors.TriggerLaneDown == 1 or regTriggerErrors.TriggerChannelDown == 1 or regTriggerErrors.TriggerSerialPllNotLocked == 1 or regTriggerErrors.TriggerSerialTransceiverNotLocked == 1) and (resetCount <= MAX_RESETS):
            self.reset_aurora()
            
            # Poll until Resets are cleared or timeout after 1 second.
            timeout = time.time() + 1 # 1 sec timeout
            regResets = self.Read('Resets')
            while (regResets.Pack() != 0) and (time.time() < timeout):
                regResets = self.Read('Resets')
            if regResets.Pack() != 0:
                self.Log('debug', 'HPCC DC {} SLICE{}: Reset Aurora failed.'.format(self.slot, self.slice))
            
            regTriggerErrors = self.Read('TriggerErrors')
            if regTriggerErrors.TriggerLaneDown == 1 or regTriggerErrors.TriggerChannelDown == 1 or regTriggerErrors.TriggerSerialPllNotLocked == 1 or regTriggerErrors.TriggerSerialTransceiverNotLocked == 1:
                time.sleep(0.025)
                regTriggerErrors = self.Read('TriggerErrors')
            if regTriggerErrors.TriggerLaneDown == 1 or regTriggerErrors.TriggerChannelDown == 1 or regTriggerErrors.TriggerSerialPllNotLocked == 1 or regTriggerErrors.TriggerSerialTransceiverNotLocked == 1:
                self.Log('debug', 'HPCC DC {} SLICE{}: Trigger Error Register 0x{:X}.'.format(self.slot, self.slice, regTriggerErrors.Pack()))
            else:
                self.Log('debug', 'HPCC DC {} SLICE{}: Trigger Error Register Aurora Interface cleared.'.format(self.slot, self.slice))
            
            resetCount += 1
            
        if regTriggerErrors.TriggerLaneDown == 1 or regTriggerErrors.TriggerChannelDown == 1 or regTriggerErrors.TriggerSerialPllNotLocked == 1 or regTriggerErrors.TriggerSerialTransceiverNotLocked == 1:
            self.Log('error', 'HPCC DC {} SLICE{}: Trigger Error Register 0x{:X}.'.format(self.slot, self.slice, regTriggerErrors.Pack()))

    def reset_aurora(self):
        regResets = self.dcRegisters.Resets(uint=0, length=32)
        regResets.AuroraCoreReset = 1
        regResets.AuroraGtReset = 1
        self.Write('Resets', regResets)

    def _ResetTriggerDomains(self):
        MAX_RESETS = 10
        regTriggerErrors = self.Read('TriggerErrors')
        self.Log('debug', 'HPCC DC {} SLICE{}: Trigger Error Register 0x{:X}.'.format(self.slot, self.slice, regTriggerErrors.Pack()))
        
        resetCount = 1
        while (regTriggerErrors.Pack() != 0) and (resetCount <= MAX_RESETS):
            self.reset_trigger_domains()
            
            # Poll until Resets are cleared or timeout after 1 second.
            timeout = time.time() + 1 # 1 sec timeout
            regResets = self.Read('Resets')
            while (regResets.Pack() != 0) and (time.time() < timeout):
                regResets = self.Read('Resets')
            if regResets.Pack() != 0:
                self.Log('debug', 'HPCC DC {} SLICE{}: Reset Trigger Domain failed.'.format(self.slot, self.slice))
            
            regTriggerErrors = self.Read('TriggerErrors')
            if regTriggerErrors.Pack() != 0:
                time.sleep(0.025)
                regTriggerErrors = self.Read('TriggerErrors')
            if regTriggerErrors.Pack() == 0:
                self.Log('debug', 'HPCC DC {} SLICE{}: Trigger Error Register cleared.'.format(self.slot, self.slice))
            else:
                self.Log('debug', 'HPCC DC {} SLICE{}: Trigger Error Register 0x{:X}.'.format(self.slot, self.slice, regTriggerErrors.Pack()))
            
            resetCount += 1
            
        if regTriggerErrors.Pack() != 0:
            self.Log('error', 'HPCC DC {} SLICE{}: Trigger Error Register 0x{:X}.'.format(self.slot, self.slice, regTriggerErrors.Pack()))
            
    def reset_trigger_domains(self):
        regResets = self.dcRegisters.Resets(uint=0, length=32)
        regResets.AuroraDomainReset = 1
        regResets.TriggerDomainReset = 1
        self.Write('Resets', regResets)

    def reset_aurora_domain(self):
        # Clears Sof and Hard trigger errors. Aurora domain bit is self
        #  clearing so it will always read zero
        regResets = self.dcRegisters.Resets(uint=0, length=32)
        regResets.AuroraDomainReset = 1
        self.Write('Resets', regResets)

    def _EnableAnalogPowerRails(self, enable):
        if self.slice == 1: 
            # enable the USB line, only exists after CPLD 3.0
            usbHandler = hil.cypDeviceOpen(hil.CYP_VIDPID_HPCC_DC, self.slot,None)
            hil.cypPortBitWrite(usbHandler, hil.CP_PORTD, 5, int(enable))
            
            regGpio = self.Read('Gpio')
            if enable:
                regGpio.PowerSwitchControl = 1
            else:
                regGpio.PowerSwitchControl = 0
            self.Write('Gpio', regGpio)
            
            timeout = time.time() + 5 # 5 sec timeout
            regGpio = self.Read('Gpio')
            while (regGpio.PowerSwitchStatus != int(enable)) and (time.time() < timeout):
                time.sleep(0.1)
                regGpio = self.Read('Gpio')
                
            usbControl = hil.cypPortBitRead(usbHandler, hil.CP_PORTD, 5)
            if regGpio.PowerSwitchStatus != int(enable):
                self.Log('error', 'Failed to enable = {} analog power rails on HPCC DC FPGA slot {} slice 1, usb control = {}'.format(enable, self.slot, usbControl))
            else:
                self.Log('debug', 'enable = {} analog power rails on HPCC DC FPGA slot {} slice 1, usb control = {}'.format(enable, self.slot, usbControl))
            hil.cypDeviceClose(usbHandler)
        else:
            self.Log('error', 'Cannot enable/disable analog power rails on HPCC DC FPGA slot {} slice 0'.format(self.slot))
            
    def _ResetAdAte320AndAd7609Domains(self, enable):
        holdInReset = 0
        if enable:
            holdInReset = 1
        
        regHoldResetsActive = self.dcRegisters.HoldResetsActive(uint = 0, length = 32) 
        regHoldResetsActive.HoldAdAte320ResetActive = holdInReset
        regHoldResetsActive.HoldAd7609ResetActive = holdInReset
        self.Write('HoldResetsActive', regHoldResetsActive)
        
        regResets = self.dcRegisters.Resets(uint = 0, length = 32) 
        regResets.AdAte320DomainReset = 1
        regResets.Ad7609DomainReset = 1
        self.Write('Resets', regResets)

        # Poll until Resets are cleared or timeout after 1 second.
        timeout = time.time() + 1 # 1 sec timeout
        regResets = self.Read('Resets')
        while (regResets.AdAte320DomainReset == 1 or regResets.Ad7609DomainReset == 1) and (time.time() < timeout):
            regResets = self.Read('Resets')
        if regResets.AdAte320DomainReset == 1:
            self.Log('error', 'HPCC DC {} SLICE{}: Reset AdAte320 domain Failed.'.format(self.slot, self.slice))
        if regResets.Ad7609DomainReset == 1:
            self.Log('error', 'HPCC DC {} SLICE{}: Reset Ad7609 domain Failed.'.format(self.slot, self.slice))
            
    def SetPEAttributesWithZeroDelay(self, attributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peIndex = ALL_PINS
        elif channels == 'EVEN_PINS':
            peIndex = EVEN_PINS
        elif channels == 'ODD_PINS':
            peIndex = ODD_PINS
        elif isinstance(channels, list):
            peIndex = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        
        for i in peIndex:
            for att in attributes:
                self.pe[i].SetAttribute(att, attributes[att])
        
        if apply:
            hasECRatioOne = False
            for i in ALL_PINS:
                if self.pe[i].isEnabledClock:
                    if self.pe[i]._ratio == 1:
                        hasECRatioOne = True
                        break
            update = self.hpcc.ac[self.slice].UpdateRatioOneECDomainPeriod(hasECRatioOne)
            if update:
                peIndex = ALL_PINS
            for i in peIndex:
                if self.pe[i].isEnabledClock:
                    self.hpcc.ac[self.slice]._PlaceClockEdges(self.pe[i])
                else:
                    self.hpcc.ac[self.slice]._PlaceDPinZeroDelay(self.pe[i])
                self.pe[i].SetInitialValue() # TODO: only set it when necessary

    def SetFreqCounterMode(self, attributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peIndex = ALL_PINS
        elif channels == 'EVEN_PINS':
            peIndex = EVEN_PINS
        elif channels == 'ODD_PINS':
            peIndex = ODD_PINS
        elif isinstance(channels, list):
            peIndex = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        
        for i in peIndex:
            for att in attributes:
                self.pe[i].SetAttribute(att, attributes[att])
        
        if apply:
            for i in peIndex:
                self.hpcc.ac[self.slice]._FreqCounterMode(self.pe[i], self.pe[i].counterMode)

    def SetPEKeepMode(self, attributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peIndex = ALL_PINS
        elif channels == 'EVEN_PINS':
            peIndex = EVEN_PINS
        elif channels == 'ODD_PINS':
            peIndex = ODD_PINS
        elif isinstance(channels, list):
            peIndex = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        
        for i in peIndex:
            for att in attributes:
                self.pe[i].SetAttribute(att, attributes[att])
        
        if apply:
            for i in peIndex:
                if self.pe[i].isKeepMode:
                    keepMode = True
                    self.hpcc.ac[self.slice]._KeepModeEnable(self.pe[i], keepMode)
                else:
                    keepMode = False
                    self.hpcc.ac[self.slice]._KeepModeEnable(self.pe[i], keepMode)


    # attributes = {att: value}
    def SetPEAttributes(self, attributes, apply, channels = 'ALL_PINS'):
        if channels == 'ALL_PINS':
            peIndex = ALL_PINS
        elif channels == 'EVEN_PINS':
            peIndex = EVEN_PINS
        elif channels == 'ODD_PINS':
            peIndex = ODD_PINS
        elif isinstance(channels, list):
            peIndex = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))
        
        for i in peIndex:
            for att in attributes:
                self.pe[i].SetAttribute(att, attributes[att])
        
        if apply:
            hasECRatioOne = False
            for i in ALL_PINS:
                if self.pe[i].isEnabledClock:
                    if self.pe[i]._ratio == 1:
                        hasECRatioOne = True
                        break
            update = self.hpcc.ac[self.slice].UpdateRatioOneECDomainPeriod(hasECRatioOne)
            if update:
                peIndex = ALL_PINS
            for i in peIndex:
                if self.pe[i].isEnabledClock:
                    self.hpcc.ac[self.slice]._PlaceClockEdges(self.pe[i])
                else:
                    self.hpcc.ac[self.slice]._PlaceDPinEdges(self.pe[i])
                self.pe[i].SetInitialValue() # TODO: only set it when necessary

            
    def GetDcDataCaptureHeaders(self):
        count = self.Read('DcDataCaptureStatus').Count
        if count > 0:
            headers = self.DmaRead(DcDataCaptureHeadersSegment, (count * DC_DATA_CAPTURE_HEADER_SIZE))
            return headers
        else:
            raise Exception('HPCC DC {} SLICE{}: did not capture any header'.format(self.slot, self.slice))
    
    def GetDcDataCaptureResults(self):
        # Poll until DC Data Capture is no longer active or timeout after 5 seconds.
        timeout = time.time() + 5
        while (self.Read('DcDataCaptureStatus').Active == 1) and (time.time() < timeout):
            time.sleep(0)
        if (self.Read('DcDataCaptureStatus').Active == 1):
            raise Exception('HPCC DC {} SLICE{}: data capture timeout'.format(self.slot, self.slice))
            
        headers = self.GetDcDataCaptureHeaders()
        print(len(headers))
        
    def Ad7609AdcToVoltage(self, adc):
        # AD7609 18-bit two's complement value, converted to 22-bit value by FPGA to provide for calibration smoothing in lower 2-bits and overhead for math.
        # The range is from 0x80000 (-10.0V) to 0x7FFFF (9.9999237060546875V), with 0x00000 = 0.0V, LSB is 0.000019073486328125V (10.0V / 0x80000).
        return (5.0 * adc) / 0x40000
            
    def GetPmuMeasurements(self, channel, sample):
        if channel < 0 or channel > 55:
            raise Exception('HPCC DC {} SLICE{}: Invalid channel number {}'.format(self.slot, self.slice, channel))
            
        measurments = []
        for i in range(sample):
            pmuReg = self.ReadPMU(channel)
            voltage = self.Ad7609AdcToVoltage(pmuReg.ChannelCalibratedAdcData)
            measurments.append(voltage)
            
        #print(measurments)
        return sum(measurments) / len(measurments)
            
    def LoadDcCal(self, filename):
        file = open(filename, 'r')
        for line in file:
            if ',' in line:
                data = line.rstrip().split(',')
                channel = int(data[0])
                calValue = float(data[1])
                self.pe[channel].attribute.vox = calValue        
                self.pe[channel].attribute.termVRef = calValue        

    def clear_trigger_errors(self, num_retries=50, force=False):
        if self.read_trigger_errors() or force:
            for retry in range(num_retries):
                self.reset_aurora_domain()
                time.sleep(0.1)
                if self.read_trigger_errors() == 0:
                    self.Log('info', f'{self.name()}: Cleared trigger errors '
                                     f'in {retry} out of {num_retries} tries')
                    break
            else:
                self.Log('warning',
                         f'{self.name()}: Unable to clear trigger errors')
        self.log_trigger_errors(log_level='info')

    def read_trigger_errors(self):
        return self.Read('TriggerErrors').Pack()

    def log_trigger_errors(self, log_level='info'):
        value = self.read_trigger_errors()
        self.Log(log_level, f'{self.name()} (trigger_errors): '
                            f'0x{value:08X}')

    def aurora_link_is_stable(self):
        trigger_errors = self.read_trigger_errors()
        return (trigger_errors == 0)
