################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: lmk01000.py
#-------------------------------------------------------------------------------
#     Purpose: Class for clock lmk01000
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/15/15
#       Group: HDMT FPGA Validation
################################################################################

import time

from Common.instruments.base import Instrument
import Hpcc.instrument.hpccAcRegs as ac_registers

LMK01000_R0_ADDR = 0x0 #bit31 = reset
LMK01000_R1_ADDR = 0x1
LMK01000_R2_ADDR = 0x2
LMK01000_R3_ADDR = 0x3
LMK01000_R4_ADDR = 0x4
LMK01000_R5_ADDR = 0x5
LMK01000_R6_ADDR = 0x6
LMK01000_R7_ADDR = 0x7
LMK01000_R9_ADDR = 0x9
LMK01000_R14_ADDR = 0xE
LMK01000_R14_VALUE = 0x6800000

class Lmk01000(Instrument):
    def __init__(self, hpcc, isHP, name = None):
        super(Lmk01000, self).__init__(name)
        self._hpcc = hpcc # so that the clock can access methods in hpcc class
        self._slot = hpcc.slot
        self._slice = hpcc.slice
        self._isHP = isHP
        if self._isHP:
            self.CONTROL = 'LMK01000Control_HP'
            self.WR_DATA = 'LMK01000WRData_HP'
        else:
            self.CONTROL = 'LMK01000Control'
            self.WR_DATA = 'LMK01000WRData'
         
    def _Write(self, address, data):
        regLMK01000WRData = ac_registers.LMK01000WRData()
        regLMK01000WRData.WRData = data
        self._hpcc.Write(self.WR_DATA, regLMK01000WRData)
        time.sleep(0.01)        
        
        regLMK01000Control = ac_registers.LMK01000Control()
        regLMK01000Control.Address = address
        self._hpcc.Write(self.CONTROL, regLMK01000Control)
        time.sleep(0.01)
    
    def Initialize(self):
        self.EnableClockOutput(LMK01000_R0_ADDR, 1);
        self.EnableClockOutput(LMK01000_R1_ADDR, 1);
        self.EnableClockOutput(LMK01000_R2_ADDR, 1);
        self.EnableClockOutput(LMK01000_R3_ADDR, 1);
        self.EnableClockOutput(LMK01000_R4_ADDR, 1);
        self.EnableClockOutput(LMK01000_R5_ADDR, 1);
        self.EnableClockOutput(LMK01000_R6_ADDR, 1);
        self.EnableClockOutput(LMK01000_R7_ADDR, 1);
        self._Write(LMK01000_R9_ADDR, 0x00032a0); # enable vboost
        self._Write(LMK01000_R14_ADDR, LMK01000_R14_VALUE); # Clkin_SELECT = 1, choose clkin0
    
    def Reset(self):
        self.Log('debug', 'resetting lmk01000 for hpcc {} slice {}'.format(self._slot, self._slice))
        self._Write(LMK01000_R0_ADDR, 0x8000000) # send reset
        self._Write(LMK01000_R0_ADDR, 0x0) # release reset
        
    def EnableClockOutput(self, clkAddr, div):
        if clkAddr > 7:
            raise RuntimeError("Invalid LMK01000 clock address")
            
        if div <= 1:
            self._Write(clkAddr, 0x1000) # just set the enable clock bit
        else:
            if ((div & 0x1) != 0) or (div > 510):
                raise RuntimeError("Invalid LMK01000 clock div value")
            divVal = div // 2; # Actual clock divider is 2X of ClkDiv
            baseRegister = 0x3000; # ClkEn = 1, ClkMux = 1, mode = divided
            clkDivider = baseRegister | (divVal << 4);
            self._Write(clkAddr, clkDivider);
    
    def EnableOutput(self, enable):
        regLMK01000WRData = ac_registers.LMK01000WRData()
        regLMK01000WRData.WRData = LMK01000_R14_VALUE
        self._hpcc.Write(self.WR_DATA, regLMK01000WRData)
        time.sleep(0.01)    
        
        regLMK01000Control = ac_registers.LMK01000Control()
        regLMK01000Control.Address = LMK01000_R14_ADDR 
        if enable:
            regLMK01000Control.GoeDisable = 0
        else:
            regLMK01000Control.GoeDisable = 1
        self._hpcc.Write(self.CONTROL, regLMK01000Control)
        time.sleep(0.01)
        
        