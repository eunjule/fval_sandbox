################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: accal.py
#-------------------------------------------------------------------------------
#     Purpose: AC FPGA Calibration data
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/10/15
#       Group: HDMT FPGA Validation
################################################################################

from Common.instruments.base import Instrument

class ACCalChannel(Instrument):
    def __init__(self, channel):
        super().__init__('')
        self.fpgaChannel = channel
        self.inputDeskew  = -1
        self.outputDeskew = -1
        self.enableDeskew = -1
        self.inputCal     = 0 #12500 # ps
        self.outputCal    = 0
        self.enableCal    = 0
        self.inputDelays  = -1
        self.enableDelays = -1
        self.outputDelays = -1
        

class ACCal(Instrument):
    def __init__(self):
        super().__init__('')
        self.channelData = dict()
        self.channelNum = 56
        for i in range(self.channelNum):
            self.channelData[i] = ACCalChannel(i)
            
    def LoadNominalFineDelayValues(self, nominalFineDelayValue):
        fineDelays = []
        for i in range(64):
            fineDelays.append(nominalFineDelayValue * i)

        for i in range(self.channelNum):
            self.channelData[i].inputDelays = fineDelays
            self.channelData[i].outputDelays = fineDelays
            self.channelData[i].enableDelays = fineDelays
            
    def LoadInputCal(self, filename,offset=0):
        file = open(filename, 'r')
        for line in file:
            if ',' in line:
                data = line.split(',')
                channel = int(data[0])
                calValue = int(data[1])
                self.channelData[channel].inputCal = calValue - offset
