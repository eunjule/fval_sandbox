################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: ccattribute.py
#-------------------------------------------------------------------------------
#     Purpose: (dpin) Attribute Class for channel card
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/09/15
#       Group: HDMT FPGA Validation
################################################################################

from Common.instruments.base import Instrument

# TODO: hard code to dpin for now
class CCAttribute(Instrument):
    # Ppmu Range Select
    E_2uA =   0 # [0XXb] = PPMU Range E (2uA, 250 KOhm)
    D_10uA =  4 # 100b = PPMU Range D (10uA, 50 KOhm)
    C_100uA = 5 # 101b = PPMU Range C (100uA, 5 KOhm)
    B_1mA =   6 # 110b = PPMU Range B (1mA, 500 Ohm)
    A_40mA =  7 # 111b = PPMU Range A (40mA, 12.5 Ohm)

    def __init__(self):
        super().__init__('')
        
        # common attribute
        self.pinModeSel = 'Digital'

        # Digital Attributes
        self.fixedDriveState  = 'Off' 
        self.ioh = 0.0
        self.iol = 0.0
        self.termMode = 'TermVRef' # TermVref - 50 ohm, Vref - 0 ohm
        self.termVRef = 1.5
        self.vch = 3.0
        self.vcl = 0.0
        self.vih = 3.0
        self.vil = 0.0
        self.vox = 1.5
        self.vRef = 0.0
        
        # PMU attributes
        self.opMode = 'VM' 
        self.iRange = 'IR40mA' # not checked, hard code
        self.samplingCount = 10
        self.samplingRatio = 1
        self.samplingMode = 1 # SamplingMode - 0 = Trace, 1 = Average
        
    
