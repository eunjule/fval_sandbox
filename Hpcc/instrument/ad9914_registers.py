# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from ctypes import LittleEndianStructure, c_uint, memmove, addressof


class Ad9914Register(LittleEndianStructure):
    def __init__(self, uint=0, length=32):
        super().__init__()
        self.from_int(uint)

    def Pack(self):
        return int.from_bytes(self, byteorder='little')

    def from_int(self, i):
        memmove(addressof(self), i.to_bytes(4, byteorder='little'), 4)



class ControlFunction1Register(Ad9914Register):
    _fields_ = [('LsbFirstMode', c_uint, 1),
                ('SdioInputOnly', c_uint, 1),
                ('Reserved1', c_uint, 1),
                ('ExternalPowerDownControl', c_uint, 1),
                ('Reserved2', c_uint, 1),
                ('RefClkInputPowerDown', c_uint, 1),
                ('DacPowerDown', c_uint, 1),
                ('DigitalPowerDown', c_uint, 1),
                ('OskEnable', c_uint, 1),
                ('ExternalOskEnable', c_uint, 1),
                ('Reserved3', c_uint, 1),
                ('ClearPhaseAccumulator', c_uint, 1),
                ('ClearDigitalRampAccumulator', c_uint, 1),
                ('AutoclearPhaseAccumulator', c_uint, 1),
                ('AutoclearDigitalRampAccumulator', c_uint, 1),
                ('LoadLrrAtIoUpdate', c_uint, 1),
                ('EnableSineOutput', c_uint, 1),
                ('ParallelPortStreamingEnable', c_uint, 1),
                ('Reserved4', c_uint, 6),
                ('VcoCalEnable', c_uint, 1),
                ('Reserved5', c_uint, 7)]


class ControlFunction2Register(Ad9914Register):
    _fields_ = [('Reserved1', c_uint, 10),
                ('SyncClkInvert', c_uint, 1),
                ('SyncClkEnable', c_uint, 1),
                ('Reserved2', c_uint, 1),
                ('DrgOverOutputEnable', c_uint, 1),
                ('FrequencyJumpEnable', c_uint, 1),
                ('MatchedLatencyEnable', c_uint, 1),
                ('ProgramModulusEnable', c_uint, 1),
                ('DigitalRampNoDwellLow', c_uint, 1),
                ('DigitalRampNoDwellHigh', c_uint, 1),
                ('DigitalRampEnable', c_uint, 1),
                ('DigitalRampDestination', c_uint, 2),
                ('ParallelDataPortEnable', c_uint, 1),
                ('ProfileModeEnable', c_uint, 1),
                ('Reserved3', c_uint, 8)]


class ControlFunction3Register(Ad9914Register):
    _fields_ = [('MinimumLdw', c_uint, 2),
                ('LockDetectEnable', c_uint, 1),
                ('Icp', c_uint, 3),
                ('ManualIcpSelection', c_uint, 1),
                ('Reserved1', c_uint, 1),
                ('FeedbackDividerN', c_uint, 8),
                ('DoublerClockEdge', c_uint, 1),
                ('PllRefDisable', c_uint, 1),
                ('PllEnable', c_uint, 1),
                ('DoublerEnable', c_uint, 1),
                ('InputDivider', c_uint, 2),
                ('InputDividerReset', c_uint, 1),
                ('Reserved2', c_uint, 9)]


class ControlFunction4Register(Ad9914Register):
    _fields_ = [('Default', c_uint, 24),
                ('DacCalEnable', c_uint, 1),
                ('DacCalClockPowerDown', c_uint, 1),
                ('AuxiliaryDividerPowerDown', c_uint, 1),
                ('Reserved1', c_uint, 5)]


class ProfilePhaseAmplitudeRegister(Ad9914Register):
    """There are one of these registers per profile"""
    _fields_ = [('phase_offset', c_uint, 16),
                ('amplitude_scale_factor', c_uint, 12),
                ('open', c_uint, 4)]

    def offset(self, profile):
        return 0x0C + profile * 2


class UsrRegister(Ad9914Register):
    _fields_ = [('SyncInDelayAdj', c_uint, 3),
                ('SyncOutDelayAdj', c_uint, 3),
                ('CalWithSync', c_uint, 1),
                ('ResetSyncCounter', c_uint, 1),
                ('Default', c_uint, 16),
                ('PllLock', c_uint, 1),
                ('DllAcquired', c_uint, 1),
                ('Reserved1', c_uint, 6)]


CONTROL_FUNCTION1_ADDR = 0x00
CONTROL_FUNCTION2_ADDR = 0x01
CONTROL_FUNCTION3_ADDR = 0x02
CONTROL_FUNCTION4_ADDR = 0x03
DIGITAL_RAMP_LOWER_LIMIT_ADDR = 0x04
DIGITAL_RAMP_UPPER_LIMIT_ADDR = 0x05
RISING_DIGITAL_RAMP_STEP_SIZE_ADDR = 0x06
FALLING_DIGITAL_RAMP_STEP_SIZE_ADDR = 0x07
DIGITAL_RAMP_RATE_ADDR = 0x08
LOWER_FREQUENCY_JUMP_ADDR = 0x09
UPPER_FREQUENCY_JUMP_ADDR = 0x0A
PROFILE0_FREQUENCY_TUNING_WORD_ADDR = 0x0B
PROFILE0_PHASE_AMPLITUDE_ADDR = 0x0C
PROFILE1_FREQUENCY_TUNING_WORD_ADDR = 0x0D
PROFILE1_PHASE_AMPLITUDE_ADDR = 0x0E
PROFILE2_FREQUENCY_TUNING_WORD_ADDR = 0x0F
PROFILE2_PHASE_AMPLITUDE_ADDR = 0x10
PROFILE3_FREQUENCY_TUNING_WORD_ADDR = 0x11
PROFILE3_PHASE_AMPLITUDE_ADDR = 0x12
PROFILE4_FREQUENCY_TUNING_WORD_ADDR = 0x13
PROFILE4_PHASE_AMPLITUDE_ADDR = 0x14
PROFILE5_FREQUENCY_TUNING_WORD_ADDR = 0x15
PROFILE5_PHASE_AMPLITUDE_ADDR = 0x16
PROFILE6_FREQUENCY_TUNING_WORD_ADDR = 0x17
PROFILE6_PHASE_AMPLITUDE_ADDR = 0x18
PROFILE7_FREQUENCY_TUNING_WORD_ADDR = 0x19
PROFILE7_PHASE_AMPLITUDE_ADDR = 0x1A
USR0_ADDR = 0x1B
