# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time
import io
import os
import math

from .ad9914 import Ad9914
from os.path import join, isdir, isfile

SAMPLE_FLOOR_PERCENT = 1 / 24 / 2 # 1/2 of the total number of expected sync states
MINIMUM_DISTINCT_SYNC_STATES = 16
class Ad9914DomainClock(Ad9914):
    def __init__(self, hpcc, isHP, name = None):
        super(Ad9914DomainClock, self).__init__(hpcc, isHP, name)
        self._period = 8e-9
        self._lastPeriod = 8e-9
        
    def SetPeriod(self, period):
        self.Log('debug','****Setting 9914 Domain Clock Period to {}****'.format(period))
        self._period = period

    def EnableOutput(self, enable):
        if not enable:
            # Slow the clock to nearest multiple of 125 (8ns), and keeping doubling it until we hit our target period
            targetPeriod = 8e-9 * 16
            if self._lastPeriod < 2e-9:
                currentPeriod = 2e-9
            elif self._lastPeriod < 4e-9:
                currentPeriod = 4e-9
            else:
                currentPeriod = 8e-9
            #currentPeriod = ((self._lastPeriod / 8e-9) + 1) * 8e-9
            # Slow the clock down
            while (currentPeriod < targetPeriod):
                super(Ad9914DomainClock, self).WritePeriod(currentPeriod)
                currentPeriod *= 2            
            super(Ad9914DomainClock, self).WritePeriod(targetPeriod)
            # Finally, clear the DAC, FPGA will time the IO update correctly
            super(Ad9914DomainClock, self).EnableOutput(False)
            # Now we can set our real frequency back
            super(Ad9914DomainClock, self).WritePeriod(self._period)
            self._lastPeriod = self._period
        else:
            super(Ad9914DomainClock, self).EnableOutput(True)
            
    def GetSyncDistribution(self, samples, lmk01000):
        # self.setup_ad9914()
        distribution = {}
        # todo: setdelay(0) # Zero out phase delay before sync starts
        for i in range(samples):
            state = self.RunAndGetDistribution(i,lmk01000)
            if state > 48 and state < 60:
                return state
        self.Log('error', 'Did not find a valid sync spot in {} runs'.format(samples))
        '''
            if state in distribution:
                distribution[state] += 1
            else:
                distribution[state] = 1
        selectedSyncValue = self.FillOutDistribution(distribution, samples)
        return state
        '''

    def get_sync_state(self,slice,lmk01000):
        sample_list = []
        for loop in range(100):
            self.RunSync(loop,lmk01000)
            time.sleep(0.20)
            sample_list = self.get_sample_delay_tuple_list(sample_list)
            self.reset_ad9914_delay_to_zero()
        sorted_samples = self.sort_step_difference(sample_list)
        sorted_filename = self.get_sorted_distribution_csv_filename()
        self.write_sorted_distribution_to_file(sorted_filename, sorted_samples)
        each_step_range = self.get_each_step_range(sorted_samples)
        filename = self.get_distribution_csv_filename()
        tuple_filename = self.get_tuple_csv_filename()
        self.write_distribution_to_file(filename, sample_list)
        self.write_tuple_to_file(tuple_filename, each_step_range)
        self.bring_edge_to_specific_step(each_step_range,lmk01000)

    def bring_edge_to_specific_step(self, each_step_range,lmk01000):
        sample_list = []
        sample_list = self.get_sample_delay_tuple_list(sample_list)
        rising_edge_delay, rising_edge_sample = self.find_rising_edge(sample_list)
        self.Log('info',f'Falling edge delay {rising_edge_delay}')
        self.Log('info',f'Steps  {each_step_range}')
        for loop in range (1000):
            edge_found = False
            if rising_edge_delay >= 40000:
                self.print_selected_step_reset_ad9914delay(rising_edge_delay)
                edge_found = True
            if edge_found:
                break
            else:
                self.RunSync(loop, lmk01000)
                time.sleep(0.20)
                sample_list = []
                sample_list = self.get_sample_delay_tuple_list(sample_list)
                rising_edge_delay, rising_edge_sample = self.find_rising_edge(sample_list)
                self.Log('info', f'New Rising edge delay {rising_edge_delay} for iteration {loop}')
                self.reset_ad9914_delay_to_zero()

    def print_selected_step_reset_ad9914delay(self,rising_edge_delay):
        self.Log('info', f' Rising edge delay {rising_edge_delay}')
        self.Log('info', 'Clearing delay register')
        self.reset_ad9914_delay_to_zero()

    def get_each_step_range(self, sorted_samples):
        all_steps_list = self.get_24_step_list(sorted_samples)
        return self.get_range_tuple(all_steps_list)

    def get_range_tuple(self,all_steps_list):
        each_step_range_list_tuple = []
        for steps in range (24):
            first_element = all_steps_list[steps][0]
            last_element = all_steps_list[steps][-1]
            if first_element == last_element:
                break
            each_step_range_list_tuple.append((first_element,last_element))
        return each_step_range_list_tuple

    def get_24_step_list(self, sorted_samples):
        step_1, step_2, step_3, step_4, step_5, step_6, step_7, step_8, step_9, step_10, step_11, step_12, step_13,\
        step_14, step_15, step_16, step_17, step_18, step_19, step_20, step_21, step_22, step_23, step_24 =\
        ([] for i in range(24))
        first_item = sorted_samples.pop(0)
        next_item = sorted_samples.pop(0)
        total_samples = len(sorted_samples)
        for step_list in range(24):
            step_name = 'step_' + str(step_list + 1)
            for each_sample in range(total_samples):
                if math.isclose(first_item, next_item, abs_tol=500):
                    vars()[step_name].append(first_item)
                else:
                    vars()[step_name].append(first_item)
                    first_item = next_item
                    next_item = sorted_samples.pop(0)
                    break
                first_item = next_item
                if len(sorted_samples) != 0:
                    next_item = sorted_samples.pop(0)
                else:
                    vars()[step_name].append(first_item)
                    break

        all_steps_list = []
        for step_list in range(24):
            list_name = 'step_' + str(step_list + 1)
            vars()[list_name].sort()
            all_steps_list.append(vars()[list_name])

        return all_steps_list

    def sort_step_difference(self,sample_list):
        sorted_samples = []
        for delays in range(len(sample_list)):
            if sample_list[delays][0] < 1 and sample_list[delays][0] > 0:
                sorted_samples.append(sample_list[delays][1])
        sorted_samples.sort()
        return sorted_samples

    def write_delay_in_ad9914(self, slice, rising_edge_delay):
        actual_rising_edge = rising_edge_delay
        # rising_edge_delay = (rising_edge_delay + hpccdelay) & 0xffff
        self.Log('info', '******************************************************************************')
        # self.Log('info',f'AD9914 delay set to {rising_edge_delay} for slice {slice} actual riing edge {actual_rising_edge} Delay from argument {hpccdelay} ')
        self.Log('info',f'AD9914 delay set to {rising_edge_delay} for slice {slice} actual riing edge {actual_rising_edge}')
        super(Ad9914DomainClock, self).SetDelay(rising_edge_delay)
        self.Log('info', '******************************************************************************')
        return rising_edge_delay

    def find_rising_edge(self, sample_list):
        previous_sample = sample_list[0][0]
        for i in range(len(sample_list)):
            current_sample = sample_list[i][0]
            if current_sample > previous_sample:
                rising_edge_delay = sample_list[i][1]
                rising_edge_sample = sample_list[i][0]
                break
            else:
                previous_sample = current_sample
        return rising_edge_delay,rising_edge_sample

    def read_ad9914_delay(self, rising_edge_delay):
        delay_in_ad9914 = super(Ad9914DomainClock, self).read_delay()
        self.Log('info', f'Delay set in the AD9914 register {delay_in_ad9914}')
        self.Log('info', '******************************************************************************')
        if rising_edge_delay != delay_in_ad9914:
            self.Log('error', f'Set delay {rising_edge_delay} is not equal to received delay {delay_in_ad9914} ')

    def get_distribution_csv_filename(self):
        SaveDir = os. getcwd()
        SaveFileFmt = join(SaveDir, "Ad9914SyncDistribution_slice0.csv")
        if not isfile(SaveFileFmt):
            filename = SaveFileFmt
        else:
            SaveFileFmt = join(SaveDir, "Ad9914SyncDistribution_slice1.csv")
            filename = SaveFileFmt
        return filename

    def get_sorted_distribution_csv_filename(self):
        SaveDir = os. getcwd()
        SaveFileFmt = join(SaveDir, "Ad9914SyncSortedDistribution_slice0.csv")
        if not isfile(SaveFileFmt):
            filename = SaveFileFmt
        else:
            SaveFileFmt = join(SaveDir, "Ad9914SyncSortedDistribution_slice1.csv")
            filename = SaveFileFmt
        return filename

    def get_tuple_csv_filename(self):
        SaveDir = os. getcwd()
        SaveFileFmt = join(SaveDir, "Ad9914_Tuple_slice0.csv")
        if not isfile(SaveFileFmt):
            filename = SaveFileFmt
        else:
            SaveFileFmt = join(SaveDir, "Ad9914_Tuple_slice1.csv")
            filename = SaveFileFmt
        return filename

    def get_sample_delay_tuple_list(self,sample_list):
        number_of_offsets = 2048
        number_of_samples = 100
        for phase_offset in range(number_of_offsets):
            delay_to_set = phase_offset * (2 ** 5)
            super(Ad9914DomainClock, self).SetDelay(delay_to_set)
            latch_state = 0
            for each_sample in range(number_of_samples):
                latch_state += (self._hpcc.Read('SyncStartEdgeResult').Count & 0x2) >> 1
            sample = latch_state / number_of_samples
            sample_list.append((sample, delay_to_set))
        return sample_list

    def reset_ad9914_delay_to_zero(self):
        super(Ad9914DomainClock, self).SetDelay(0)

    def write_distribution_to_file(self, filename, sample_list,mode = 'w'):
        with open(filename, mode) as fp:
            self.write_distribution(fp, sample_list)

    def write_distribution(self, ioStream, sample_list):
        assert isinstance(ioStream, io.IOBase)
        for syncState in sample_list:
            print("{},{}".format(syncState[0],syncState[1]), file=ioStream)

    def write_sorted_distribution_to_file(self, filename, sample_list,mode = 'w'):
        with open(filename, mode) as fp:
            self.write_sorted_distribution(fp, sample_list)

    def write_sorted_distribution(self, ioStream, sample_list):
        assert isinstance(ioStream, io.IOBase)
        for syncState in sample_list:
            print("{}".format(syncState), file=ioStream)

    def write_tuple_to_file(self, filename, each_step_range,mode = 'w'):
        with open(filename, mode) as fp:
            self.write_tuple(fp, each_step_range)

    def write_tuple(self, ioStream, each_step_range):
        assert isinstance(ioStream, io.IOBase)
        for step_start , step_end in each_step_range:
            print("{},{}".format(step_start,step_end), file=ioStream)

    def setup_ad9914(self):
        self.SetPeriod(8e-9)
        super(Ad9914DomainClock, self).SetDelay(0)
        self.EnableOutput(False)
        self.EnableOutput(True)

    def FillOutDistribution(self, distribution, samples):
        sampleFloor = int(samples * SAMPLE_FLOOR_PERCENT)
        if len(distribution) < MINIMUM_DISTINCT_SYNC_STATES:
            self.Log('error', 'Number of distinct sync state = {}, too small'.format(len(distribution)))
        
        # go through and combine peaks that are close together
        combinedDistribution = {}
        peakState = 0
        peakStateCount = 0
        selectedSyncValue = 99999
        MinimumSyncValue = 48
        
        for state in sorted(distribution.keys()):
            # If within 1 PLL click of our last point, add it in
            if (state - 1) < peakState:
                totalCounts = peakStateCount + distribution[state]
                # weighted mid point
                peakState = ((state * distribution[state]) + (peakState * peakStateCount)) / totalCounts
                peakStateCount = totalCounts
            else: # start a new point
                # only add in peaks that have counts against them
                if peakStateCount != 0:
                    combinedDistribution[peakState] = peakStateCount
                    # peak state the minimum sync state bigger than 48??
                    if peakState < selectedSyncValue and peakState > MinimumSyncValue:
                        selectedSyncValue = peakState
                peakState = state
                peakStateCount = distribution[state]
        combinedDistribution[peakState] = peakStateCount
        return selectedSyncValue
        # fill out our distribution, do I need this???
        '''
        syncDistribution = [selectedSyncValue]
        for state in combinedDistribution:
            if combinedDistribution[state] > sampleFloor:
                
            else:
                self.Log('debug', 'Ad9914 sync state {} filtered out'.format(state))
        '''
            
    def RunAndGetDistribution(self, syncDelay, lmk01000):
        self.RunSync(syncDelay, lmk01000)
        time.sleep(0.2)
        syncCounts = []
        syncCounts7f = []
        for i in range(100):
            syncCount = self._hpcc.Read('SyncStartEdgeResult').Count
            syncCounts.append(int(syncCount))
            syncCounts7f.append(int(syncCount & 0x7f))
        #print(syncCounts)
        #print(sum(syncCounts) / len(syncCounts))
        #print(syncCounts7f)
        #print(sum(syncCounts7f) / len(syncCounts7f))
        state = self.CheckSyncState(syncCounts7f)
        return state
    
    def RunSync(self, syncDelay, lmk01000):
        self.EnableOutput(False)
        lmk01000.EnableOutput(False)
        self.EnableOutput(True)
        
        usrReg = self._hpcc.clockRegs.UsrRegister(uint = 0x00000800, length = 32)
        usrReg.ResetSyncCounter = 1
        # Don't care about order of delays, just want a unique delay for each value
        ad9914ControlReg = self._hpcc.Read('AD9914Control')
        ad9914ControlReg.AD9914SyncInFineDelay = syncDelay & 0x1 # The FPGA will advance the fine delay every time this value changes
        usrReg.SyncInDelayAdj = (syncDelay & 0xE) >> 1
        usrReg.SyncOutDelayAdj = (syncDelay & 0xE) >> 1
        ad9914ControlReg.AD9914SyncInQDRPhaseDelay = (syncDelay & 0x3) >> 4
            
        self._Write(self._hpcc.clockRegs.USR0_ADDR, usrReg.Pack())
        usrReg.ResetSyncCounter = 0
        self._Write(self._hpcc.clockRegs.USR0_ADDR, usrReg.Pack())
        
        # Tell FPGA to run sync clock
        statusReg = self._hpcc.Read('ResetsAndStatus')
        statusReg.AD9914Sync = 1
        self._hpcc.Write('ResetsAndStatus', statusReg)
        statusReg.AD9914Sync = 0
        self._hpcc.Write('ResetsAndStatus', statusReg)

        cfr4 = self._hpcc.clockRegs.ControlFunction4Register(uint = 0x00052120, length = 32)
        cfr4.DacCalEnable = 1
        self._Write(self._hpcc.clockRegs.CONTROL_FUNCTION4_ADDR, cfr4.Pack())
        cfr4.DacCalEnable = 0
        self._Write(self._hpcc.clockRegs.CONTROL_FUNCTION4_ADDR, cfr4.Pack())
        
        statusReg.AD9914Sync = 0
        self._hpcc.Write('ResetsAndStatus', statusReg)

        # Ungate output at Lmk01000
        self.EnableOutput(False)
        lmk01000.EnableOutput(True)
        self.EnableOutput(True)
        
        #usrReg = self._Read(self._hpcc.clockRegs.USR0_ADDR)
        #print("ad9914 pll lock bit = {}".format(usrReg.PllLock))   

    def CheckSyncState(self, dist):
        MaxPllRange = 3
        RoundingPoint = 5.0
        wrapAround = 128
        dist.sort()
        #print(dist)
        if (dist[9] + MaxPllRange) >= dist[89]: # in bound
            newdist = dist[9:89]
            average = sum(newdist) / len(newdist)
            state = int(average * RoundingPoint) / RoundingPoint
            return state
        elif (dist[9] <= 2) and (dist[89] >= 126): # wraparound situation
            newdist = dist[9:89]
            sumdist = 0
            for data in newdist:
                sumdist += ((data + MaxPllRange) % wrapAround)
            average = sumdist / len(newdist)
            average -= MaxPllRange
            if average >= wrapAround:
                average -= wrapAround
            elif average < 0:
                average += wrapAround
            state = int(average * RoundingPoint) / RoundingPoint
            #print("wrap around state = {}".format(state))
            return state
        elif (dist[9] + MaxPllRange * 2) >= dist[89]:
            self.Log('warning', 'too much noise')
        else:
            self.Log('error', 'too much error')