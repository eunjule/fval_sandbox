################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: hpccacS1.py
#-------------------------------------------------------------------------------
#     Purpose: Hpcc Ac instrument code slice 1 specific
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng 
#        Date: 04/23/15
#       Group: HDMT FPGA Validation
################################################################################

import time

from .hpccac import HpccAc
from .lmk01000 import Lmk01000
from . import hpccAcRegs as ac_registers


class HpccAcS1(HpccAc):
    def __init__(self, hpcc, slot):
        self.Log('debug',"instantiate an ac slice 1 instance")
        self.slice = 1
        self.slot = slot
        super().__init__(hpcc, slot, self.slice)
        self.lmk01000HP = Lmk01000(self, True)

    def name(self):
        return f'{self.__class__.__name__}_{self.slot}_{self.slice}'

    def _InitializeClockChainFPGA1(self):
        self.Log('info', 'Initializing HPCC clocks on slot {} AC FPGA 1.'.format(self.slot))
        # Prepare for Clock init
        self.lmk01000.Reset()
        self.lmk01000.EnableOutput(False)
        self.lmk01000HP.Reset()
        
        # Toggle AD9914 RESET line
        statusReg = self.Read('ResetsAndStatus')
        statusReg.Ad9914Reset = 1
        self.Write('ResetsAndStatus', statusReg)
        time.sleep(0.01)
        statusReg.Ad9914Reset = 0
        self.Write('ResetsAndStatus', statusReg)
        
        # Program Ad9914 and Ad9914Hp for FPGA0
        self.ad9914.Initialize() 
        # TODO: check whether it is a bad part
        # Enable Lmk01000 and Lmk01000Hp
        self.lmk01000.Initialize()
        self.lmk01000.EnableOutput(True)
        self.lmk01000HP.Initialize()
        self.lmk01000HP.EnableOutput(True)
        # Now enable Ad9914 outputs
        self.ad9914.StartClock()
        
        time.sleep(0.01) # for plls to re-lock        
        
        # Reset FPGA so PLLs can re-lock
        self._Reset()
        # Make sure FPGA stopped internal init pattern
        #m_pFpgaHelper->ResetsAndStatusRegister.PatternRunning = 1
        #StopPatgen();
        # TODO: need update, may need a register class
        statusReg = self.Read('ResetsAndStatus')
        if statusReg.PatternRunning == 1:
            regPatternControl = ac_registers.PatternControl()
            regPatternControl.AbortPattern = 1
            self.Write('PatternControl', regPatternControl)
        #m_pFpgaHelper->ResetsAndStatusRegister.PatternRunning = 0


