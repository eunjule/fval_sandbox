# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os

from Common.fval import Log, LoggedError, Object
from Hpcc.instrument.hpcc import dc_registers, Hpcc
from Hpcc.instrument.hpccac import HpccAc
from Hpcc.instrument.hpccAcRegs import TriggerEvent


def create_interface(instrument):
    attempt_initialization(instrument)
    num_slices = 2
    return [TriggerInterface(instrument.ac[slice], instrument.dc[slice]) for
            slice in range(num_slices)]


def attempt_initialization(instrument):
    num_retries = 3
    name = f'{instrument.name()}_{instrument.slot}'
    Log('info', f'{name}: Initializing for trigger interface')

    for retry in range(num_retries):
        try:
            instrument.ensure_initialized()
            break
        except (Hpcc.SpiAdjustReadTriggerError, HpccAc.PatternRunError,
                HpccAc.InstrumentError, LoggedError, RuntimeError) as e:
            Log('warning', f'{name}: {e}. Re-initializing ({retry+1} of '
                           f'{num_retries})')
    else:
        raise Hpcc.InstrumentError(f'{name}: Unable to initialize FPGA')


class TriggerInterface(Object):
    """
    AC FPGAs
    ----------------------------------------
    Generates triggers.
    hpcc.ac[slice].Write('TriggerEvent', expected)

    BAR 0
    0x00008180 	Trigger Control
    0x00008190 	Trigger Event

    DC FPGAs
    ----------------------------------------
    The Aurora 8B10B interface is a high speed serial link that is used as the
    Trigger Bus Entry Point for the HPCC, and it connects the Resource Card to
    the HPCC DC Lower FPGA. Aurora 8B10B interface is not included in the HPCC
    DC Upper FPGA.

    The Trigger Bus Entry Point forwards all triggers received on the Aurora
    8B10B interface to the Trigger Bus End Points.
    Trigger Bus End Points:
    -HPCC AC Slice 0 and 1
    -HPCC DC Slice 0 and 1

    dc_trigger = hpcc.dc[slice].Read('UpTrigger').Pack()

    2.4.2.16	Trigger Up (32-bit) – Read Only – DCL FPGA Only
    The Trigger Up bits indicate the last Up Trigger that was received by the
    DCL FPGA through the USPI interface (ACU or ACL).
    Note: DCU FPGA does not have the capability of sending an Up Trigger.

    2.4.2.17	Trigger Down (32-bit) – Read Only
    For DCL FPGA, the Trigger Down bits indicate the last Down Trigger that was
    received through the Aurora Interface.
    For DCU FPGA, the Trigger Down bits indicate the last Down Trigger that was
    received through the USPI Interface.

    BAR 0
    0x8010	DCF Resets Register (Bit 9 Aurora Domain Reset)
    BAR 1
    0x0200	Trigger Errors Register
    0x0230	Trigger Down Register
    0x0240	Trigger Up Register

    """
    _MAX_FAIL_COUNT = 10

    def __init__(self, ac_fpga, dc_fpga):
        super().__init__()
        self._slot = dc_fpga.slot
        self._slice = dc_fpga.slice
        self._ac_fpga = ac_fpga
        self._dc_fpga = dc_fpga
        self.fail_count = 0

    def setup_interface(self):
        self.fail_count = 0

    def send_up_trigger(self, trigger):
        self._ac_fpga.BarWrite(TriggerEvent.BAR,
                               TriggerEvent.ADDR,
                               trigger)
        if self.slice_num() == 0:
            num_retries = TriggerInterface._MAX_FAIL_COUNT
            msg = ''
            for retry in range(num_retries):
                dc_trigger = self._dc_fpga.BarRead(dc_registers.UpTrigger.BAR,
                                                   dc_registers.UpTrigger.ADDR)
                msg = f'{self.name()}: Sending up trigger. DC trigger ' \
                      f'UpTrigger value (expected, actual): ' \
                      f'0x{trigger:08X}, 0x{dc_trigger:08X}'
                if trigger != dc_trigger:
                    self.fail_count += 1
                    if self.fail_count < TriggerInterface._MAX_FAIL_COUNT:
                        self.Log('warning', msg)
                else:
                    break
            else:
                self.Log('error', msg)

    def check_for_down_trigger(self, expected_trigger, num_retries=10):
        actual_trigger = 0
        for retry in range(num_retries):
            actual_trigger = self.read_down_trigger()
            if actual_trigger == expected_trigger:
                break

        return actual_trigger

    def log_status(self):
        trigger_errors = self._dc_fpga.BarRead(dc_registers.TriggerErrors.BAR,
                                               dc_registers.TriggerErrors.ADDR)
        self.Log('info', f'{self.name()} (trigger_errors): '
                         f'0x{trigger_errors:08X}')

    def read_down_trigger(self):
        if self.slice_num() == 1:
            return self._ac_fpga.BarRead(TriggerEvent.BAR, TriggerEvent.ADDR)
        else:
            return self._dc_fpga.BarRead(dc_registers.DownTrigger.BAR,
                                         dc_registers.DownTrigger.ADDR)

    def verify_aurora_link_is_stable(self):
        if not self.aurora_link_is_stable():
            self.Log(
                'warning',
                self.create_table_aurora_link_status())
            self.reset_trigger_interface()
        else:
            self.Log('info', f'{self.name()}: Aurora link is stable')

    def aurora_link_is_stable(self):
        """
        struct TriggerErrors:
            BAR = 1
            ADDR = 0x0200
            bit          TriggerSerialTransceiverNotLocked - DCL Only
            bit          TriggerSerialPllNotLocked - DCL Only
            bit          TriggerLaneDown - DCL Only
            bit          TriggerChannelDown - DCL Only
            bit          TriggerSoftError - DCL Only
            bit          TriggerHardError - DCL Only
            bit          TriggerSyncEarly
            bit          TriggerUpOverrunAcl
            bit          TriggerUpOverrunAcu
            bit          TriggerUpOverrunDcl
            bit          TriggerUpOverrunDcu
            bit          TriggerDownEntryPointOverrun
            bit          TriggerDownEndPointOverrun
            bit[18:0]    Reserved
        :return:
        """
        status = self._dc_fpga.BarRead(dc_registers.TriggerErrors.BAR,
                                       dc_registers.TriggerErrors.ADDR)
        status_reg = dc_registers.TriggerErrors(status)
        pll_lock_status = not (status_reg.TriggerSerialTransceiverNotLocked
                               or status_reg.TriggerSerialPllNotLocked)
        error_status = (status_reg.TriggerSoftError or
                        status_reg.TriggerHardError)
        down_status = (status_reg.TriggerChannelDown or
                       status_reg.TriggerLaneDown)

        if not pll_lock_status or error_status or down_status:
            return False
        else:
            return True

    def reset_trigger_interface(self):
        self._dc_fpga._ResetTriggerDomains()

    def create_table_aurora_link_status(self):
        status = self._dc_fpga.BarRead(dc_registers.TriggerErrors.BAR,
                                       dc_registers.TriggerErrors.ADDR)
        status_reg = dc_registers.TriggerErrors(status)
        return self._dc_fpga.create_table_from_register(status_reg)

    def slot(self):
        return self._slot

    def slice_num(self):
        return self._slice

    def name(self):
        return f'Hpcc_{self.slot()}_{self.slice_num()}'
