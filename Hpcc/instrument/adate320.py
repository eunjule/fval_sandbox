################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: adate320.py
#-------------------------------------------------------------------------------
#     Purpose: Class for pin electronics chips adate320
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/05/15
#       Group: HDMT FPGA Validation
################################################################################

import ctypes

from Common.instruments.base import Instrument
from Hpcc.instrument.ccattribute import CCAttribute

VREF = 2.5
PICO_SECONDS_PER_SECOND = 1000000000000

class AdAte320(Instrument):
    def __init__(self, hpccDc, channelNum):
        super().__init__('')
        self._hpcc = hpccDc # so that the clock can access methods in hpcc class
        self._slot = hpccDc.slot
        self._slice = hpccDc.slice
        self.fpgaChannelNum = channelNum
        self.channelNum = channelNum + 56 * self._slice
        self.channelId = -1 # TODO
        self.deviceSelect = (self.fpgaChannelNum // 2) + 1
        self.channelSelect = (self.fpgaChannelNum % 2) + 1
        self.attribute = CCAttribute()
        self._drive = -1 # all the timing parameters should be in ps
        self._compare = -1
        self.isEnabledClock = False
        self._driveType = -1
        self._resetState = -1
        self._ratio = -1
        self._tRise = -1
        self._tFall = -1
        self.dcTriggerBlock = -1 # DcTriggerBlock(self._hpcc)
        self.isKeepMode = False
        self.counterMode = 0
        #self.Log('debug', 'Init AdAte320 for hpcc {} slice {} fpgaChannel {} channel {} deviceSelect {} channelSelect {} Done'.format(self._slot, self._slice, self.fpgaChannelNum, self.channelNum, self.deviceSelect, self.channelSelect))
        
    def Initialize(self):
        # Unlock the ADI hidden registers
        regAdAte320Instruction = AdAte320Instruction()
        # regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
        regAdAte320Instruction.write = 1
        regAdAte320Instruction.deviceSelect = self.deviceSelect        
        regAdAte320Instruction.channelSelect = 0x2        
        regAdAte320Instruction.registerSelect = self._hpcc.peRegs.Register7F # ADI Register - Used for special initialization
        regAdAte320Instruction.data = 0xFEED
        self._Write(regAdAte320Instruction)
        # self._Write(regAdAte320Instruction)

        regAdAte320Instruction.channelSelect = 0x1    
        regAdAte320Instruction.data = 0x0320
        self._Write(regAdAte320Instruction)
        
        regAdAte320Instruction.channelSelect = 0x3    
        regAdAte320Instruction.data = 0xDA7A
        self._Write(regAdAte320Instruction)
        

        regAdAte320Instruction = AdAte320Instruction()
        regAdAte320Instruction.write = 0
        regAdAte320Instruction.deviceSelect = self.deviceSelect
        regAdAte320Instruction.channelSelect = self.channelSelect
        regAdAte320Instruction.registerSelect = self._hpcc.peRegs.Register7D # ADI Register - version
        regAdAte320Instruction.data = 0x0000

        feedback = self._Read(regAdAte320Instruction.value)

        # regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
        # regAdAte320Instruction.write = 0
        # regAdAte320Instruction.deviceSelect = self.deviceSelect
        # regAdAte320Instruction.channelSelect = self.channelSelect
        # regAdAte320Instruction.registerSelect = self._hpcc.peRegs.Register7D # ADI Register - version
        # regAdAte320Instruction.data = 0x0000
        # feedback = self._Read(regAdAte320Instruction.Pack()).Pack()
        
        feedback = (feedback.value & 0x0000FF00) >> 8; # Mask off the upper 8 bits and shift them.
        #print("~~~adate320 version" + str(feedback))
        if feedback <= 110 and feedback > 0:
            self.Log('error', 'AdAte320 version {} for hpcc {} slice {} channel {} too small, need more code'.format(feedback, self._slot, self._slice, self.fpgaChannelNum))
            '''
            # RTC Story 228033: HDMT100004671 : Add ADI microcode patch to ADATE320 to increase to compare voltage
            # Read/Modify/Write 300mV CML value to hidden register 0x7E
            regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
            regAdAte320Instruction.write = 0        
            regAdAte320Instruction.deviceSelect = self.deviceSelect        
            regAdAte320Instruction.channelSelect = self.channelSelect    
            regAdAte320Instruction.registerSelect = self._hpcc.peRegs.Register7E 
            regAdAte320Instruction.data = 0x0000
            feedback = self._Read(regAdAte320Instruction.Pack()).Pack()
            feedback &= 0xFF03; # Mask off the data portion, leaving bits [15:8] and [1:0] unmodified.
            feedback |= 0x00E0; # Set CML to 300mV Terminated. 0x38 or 111000b shifted to bits [7:2]
            
            regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
            regAdAte320Instruction.write = 1        
            regAdAte320Instruction.deviceSelect = self.deviceSelect        
            regAdAte320Instruction.channelSelect = self.channelSelect       
            regAdAte320Instruction.registerSelect = self._hpcc.peRegs.Register7E # ADI Register - Used for special initialization
            regAdAte320Instruction.data = feedback
            self._Write(regAdAte320Instruction)
            '''

        # Lock the ADI hidden registers
        regAdAte320Instruction = AdAte320Instruction()
        # regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
        regAdAte320Instruction.write = 1
        regAdAte320Instruction.deviceSelect = self.deviceSelect        
        regAdAte320Instruction.channelSelect = self.channelSelect        
        regAdAte320Instruction.registerSelect = self._hpcc.peRegs.Register7F # ADI Register - Used for special initialization
        regAdAte320Instruction.data = 0xFFFF
        self._Write(regAdAte320Instruction)
        
    def SetAttribute(self, attribute, value):
        if attribute.upper() == "DRIVE":
            self._drive = value * PICO_SECONDS_PER_SECOND
        elif attribute.upper() == "COMPARE":
            self._compare = value * PICO_SECONDS_PER_SECOND
        elif attribute.upper() == "EDGECOUNTER":
            if value == 'OFF':
                self.counterMode = 0
            elif value == 'RISE':
                self.counterMode = 1
            elif value == 'FALL':
                self.counterMode = 2
            elif value == 'BOTH':
                self.counterMode = 3
            else:
                raise Exception('attribute EDGECOUNTER {} is not properly set'.format(value))
        elif attribute.upper() == "IS_KEEP_MODE":
            if isinstance(value, bool):
                self.isKeepMode = value
            else:
                raise Exception('attribute IS_KEEP_MODE {} is not boolean'.format(value))
        elif attribute.upper() == "IS_ENABLED_CLOCK":
            if isinstance(value, bool):
                self.isEnabledClock = value
                # Every pin can be an enabled clock pin
                #if self.isEnabledClock and (self.fpgaChannelNum % 8 != 0):
                #    raise Exception('channel {} cannot be an enabled clock pin'.format(self.channelNum))
            else:
                raise Exception('attribute IS_ENABLED_CLOCK {} is not boolean'.format(value))
        elif attribute.upper() == "DRIVE_TYPE":
            if value == 'FIRST':
                self._driveType = 1
            #elif value == 'SECOND':   #the following drive type is no longer supported
            #    self._driveType = 0
            #elif value == 'NATIVE':
            #    self._driveType = 2
            else:
                 raise Exception('attribute DRIVE_TYPE {} is not supported'.format(value))
        elif attribute.upper() == "RESET":
            #if value == 'NO_TRANSITION': # no longer support the native reset.
            #    self._resetState = 1
            if value == 'TRANSITION':
                self._resetState = 0
            else:
                 raise Exception('attribute RESET {} is not supported'.format(value))
        elif attribute.upper() == "RATIO":
            self._ratio = value
        elif attribute.upper() == "TRISE":
            self._tRise = value * PICO_SECONDS_PER_SECOND
        elif attribute.upper() == "TFALL":
            self._tFall = value * PICO_SECONDS_PER_SECOND
        elif attribute.upper() == "FIXED_DRIVE_STATE":
            self.attribute.fixedDriveState = value
        elif attribute.upper() == "VOX":
            self.attribute.vox = value
        elif attribute.upper() == "VIH":
            self.attribute.vih = value
        elif attribute.upper() == "VIL":
            self.attribute.vil = value
        elif attribute.upper() == "TERMVREF":
            self.attribute.termVRef = value
        else:
            raise Exception('set attribute {} is not supported'.format(attribute))
    
    def SetInitialValue(self):              
        #self.dcTriggerBlock = DcTriggerBlock(self._hpcc)
        controlRegs = self._SetControlRegisters()
        
        regDacControl = self._hpcc.peRegs.DacControl()
        regDacControl.calEnable = 1
        regDacControl.loadMode = 1
        self._Enqueue(regDacControl.ADDR,  regDacControl.Pack())
        
        # TODO: calibration
        
        # Enqueue control registers for normal channel or differential P channel
        self._Enqueue(controlRegs[0].ADDR,  controlRegs[0].Pack()) # regDrvControl
        self._Enqueue(controlRegs[1].ADDR,  controlRegs[1].Pack()) # regCmpControl
        self._Enqueue(controlRegs[2].ADDR,  controlRegs[2].Pack()) # regLoadControl
        self._Enqueue(controlRegs[3].ADDR,  controlRegs[3].Pack()) # regPpmuControl
        
        # enqueue attributes
        #self._Enqueue(self._hpcc.peRegs.VOHLevel,  2.0)
        #self._Enqueue(self._hpcc.peRegs.VOLLevel,  0.0)
        self._Enqueue(self._hpcc.peRegs.VIHLevel,  self.attribute.vih)
        self._Enqueue(self._hpcc.peRegs.VILLevel,  self.attribute.vil)
        self._Enqueue(self._hpcc.peRegs.VCHLevel,  self.attribute.vch)
        self._Enqueue(self._hpcc.peRegs.VCLLevel,  self.attribute.vcl)
        self._Enqueue(self._hpcc.peRegs.VIOHLevel, self.attribute.ioh)
        self._Enqueue(self._hpcc.peRegs.VIOLLevel, self.attribute.iol)
        
        self._Enqueue(self._hpcc.peRegs.VOHLevel, self.attribute.vox)
        self._Enqueue(self._hpcc.peRegs.VITVCOMLevel, self.attribute.termVRef)  
        
        #print('channel = {}, VOX = {}, termVref = {}'.format(self.fpgaChannelNum, self.attribute.vox, self.attribute.termVRef))
        
        # begin DAC load operation
        regDacControl.load = 1
        self._Enqueue(regDacControl.ADDR,  regDacControl.Pack())
        regDacControl.load = 0
        
        #self.dcTriggerBlock.Commit()
        

    def ExecutePMU_VM(self, preMeasurementDelay):
        # set to PMU mode
        self.attribute.pinModeSel = 'PMU'
        self.SetInitialValue()
        userData = self._hpcc.dcRegisters.UserDefinedData()
        
        # set mask
        userData.ChannelsStartMeasurementMask = 1 << self.fpgaChannelNum
        
        # Enqueue Sampling Attributes
        self._EnqueueDC20('SetCaptureRate', 313 * self.attribute.samplingRatio)
        self._EnqueueDC20('SetCaptureSize', self.attribute.samplingCount)
        self._EnqueueDC20('SetCaptureMode', 0)
        userData.SamplingMode = self.attribute.samplingMode
        
        # Enqueue Blocking Wait, preMeasurementDelay
        instructionDelayStep = 1.6e-8 # 16nS
        data = int(preMeasurementDelay // instructionDelayStep)
        if data > 0xFFFF: # > 1.04856mS
            raise Exception('preMeasurementDelay {} is not supported, too big'.format(preMeasurementDelay))
        self._EnqueueDC16('BlockingWait', data)
        
        # Enqueue Dc Data Capture
        userDataPack = userData.Pack()
        print("----")
        print(bin(userDataPack))
        for i in range(0,8):
            userDataSlice = (userDataPack >> (16*i)) & 0xFFFF
            print(bin((i << 16) | userDataSlice))
            self._EnqueueDC20('SetCaptureUserData', ((i << 16) | userDataSlice))
        self._EnqueueDC16('StartDcDataCapture', 0)
        
        # Get results
        # TODO: WaitForTriggerExecutionComplete
        self._hpcc.GetDcDataCaptureResults()
        
        # revert to digital mode
        self.attribute.pinModeSel = 'Digital'
        self.SetInitialValue()
        
    def _SetControlRegisters(self):
        regDrvControl = self._hpcc.peRegs.DrvControl()
        regCmpControl = self._hpcc.peRegs.CmpControl()
        regLoadControl = self._hpcc.peRegs.LoadControl()
        regPpmuControl = self._hpcc.peRegs.PpmuControl()
        regDrvControl.rcvMux = 0
        regDrvControl.datMux = 0
        
        if self.attribute.fixedDriveState == 'Off':
            regDrvControl.force = 0
            regDrvControl.forceState = 0
        elif self.attribute.fixedDriveState == 'VIL':
            regDrvControl.force = 1
            regDrvControl.forceState = 0
        elif self.attribute.fixedDriveState == 'VIH':
            regDrvControl.force = 1
            regDrvControl.forceState = 1
        elif self.attribute.fixedDriveState == 'HiZ':
            regDrvControl.force = 1
            regDrvControl.forceState = 2
        elif self.attribute.fixedDriveState == 'TermVRef':
            regDrvControl.force = 1
            regDrvControl.forceState = 3
        else:
            self.Log('error', 'AdAte320 fixed drive state {} is currently not supported'.format(self.attribute.fixedDriveStat))
            
        if self.attribute.termMode == 'TermVRef':
            regDrvControl.vtHiZ = 1
            regLoadControl.enable = 0
        else:
            self.Log('error', 'AdAte320 term mode {} is currently not supported'.format(self.attribute.termMode))
        
        regPpmuControl.enable = 1        # PPMU Full Power ON
        regPpmuControl.inputSelect = 2   # PPMU Input from DAC_PPMU Level
        regPpmuControl.clampEnable = 1   # PPMU Clamps Enabled
        regPpmuControl.measureSelect = 0 # PPMU Channel x to PPMU_MEASx Output Pin
        regPpmuControl.rangeSelect = CCAttribute.A_40mA
        
        if self.attribute.pinModeSel == 'Digital':
            regDrvControl.enable = 1
            regDrvControl.clc = self._hpcc.CLC_MAX
            regCmpControl.dmcEnable = 0 # TODO: check if is differential pin
            regPpmuControl.standby = 1
        elif self.attribute.pinModeSel == 'PMU':
            regDrvControl.enable = 0
            regDrvControl.clc = 0
            regCmpControl.dmcEnable = 0 
            regLoadControl.enable = 0
            regPpmuControl.standby = 0
        else:
            self.Log('error', 'AdAte320 pin mode select {} is currently not supported'.format(self.attribute.pinModeSel))
        
        # TODO: hard code to VM
        if self.attribute.opMode == 'VM':
            regPpmuControl.standby = 1 
            regPpmuControl.forceMode = 0  # PPMU Force V Mode (but not forcing because of standby)
            regPpmuControl.clampEnable = 0  # PPMU Clamps Disabled
            regPpmuControl.measureMode = 0 # PPMU Measure V Mode
            regPpmuControl.measureEnable = 1  # PPMU Measure Out Pin on Channel x is Enabled
        else:
            self.Log('error', 'AdAte320 op mode {} is currently not supported'.format(self.attribute.opMode))
        
        return [regDrvControl, regCmpControl, regLoadControl, regPpmuControl]
        
    def _CheckDacValue(self, value, reg):
        if value < 0 or value >= 65536:
            self.Log('error', 'HPCC slot {} channel {}: dacValue {} outside range (0,65536]'.format(self.slot, self.channelNum, dacValue))
    
    def _VoltageLevelToDac(self, voltage, reg):
        dacValue = int((voltage + VREF) / (4.0 * VREF) * 65536)
        self._CheckDacValue(dacValue, reg)
        return dacValue
        
    def _ActiveLoadCurrentLevelToDac(self, current, reg):
        dacValue = int(((current * (5.0 / 0.025)) + VREF) / (4.0 * VREF) * 65536)
        self._CheckDacValue(dacValue, reg)
        return dacValue
    
    def _EncodeValue(self, reg, value):
        VoltageToDacRegs = [self._hpcc.peRegs.VIHLevel, self._hpcc.peRegs.VILLevel, self._hpcc.peRegs.VCHLevel, self._hpcc.peRegs.VCLLevel, self._hpcc.peRegs.VOHLevel, self._hpcc.peRegs.VOLLevel, self._hpcc.peRegs.VITVCOMLevel]
        controlRegs = [self._hpcc.peRegs.DrvControl.ADDR, self._hpcc.peRegs.CmpControl.ADDR, self._hpcc.peRegs.LoadControl.ADDR, self._hpcc.peRegs.PpmuControl.ADDR, self._hpcc.peRegs.DacControl.ADDR]
        dac = 0
        if reg in VoltageToDacRegs:
            dac = self._VoltageLevelToDac(value, reg)
        elif reg == self._hpcc.peRegs.VIOHLevel:
            dac = self._ActiveLoadCurrentLevelToDac(value*-1, reg)
        elif reg == self._hpcc.peRegs.VIOLLevel:
            dac = self._ActiveLoadCurrentLevelToDac(value, reg)
        elif reg in controlRegs:
            dac = value
        else:
            raise Exception('AdAte320 _EncodeValue: register {} is not supported'.format(hex(reg)))
        return dac
    
    # TODO: enqueue to DC trigger block, then execute the whole block, probably faster that way
    # currently each enqueue will apply the value to the register
    def _Enqueue(self, reg, value):
        regAdAte320Instruction = AdAte320Instruction()
        # regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
        regAdAte320Instruction.write = 1
        regAdAte320Instruction.deviceSelect = self.deviceSelect        
        regAdAte320Instruction.channelSelect = self.channelSelect        
        regAdAte320Instruction.registerSelect = reg 
        regAdAte320Instruction.data = self._EncodeValue(reg, value)
        self._Write(regAdAte320Instruction)
        #self.dcTriggerBlock.Enqueue(regAdAte320Instruction.Pack())
        
    def _EnqueueDC20(self, dcInstr, value):
        regDC20Instruction = AdAte320InstructionDC20()
        # regDC20Instruction = self._hpcc.dcRegisters.AdAte320InstructionDC20()
        if dcInstr == 'SetCaptureRate':
            regDC20Instruction.write = 1 
            regDC20Instruction.writeSelect = 1 
            regDC20Instruction.deviceSelect = 0
            regDC20Instruction.data = value
        elif dcInstr == 'SetCaptureSize':
            regDC20Instruction.write = 1 
            regDC20Instruction.writeSelect = 2
            regDC20Instruction.deviceSelect = 0
            regDC20Instruction.data = value
        elif dcInstr == 'SetCaptureMode':
            regDC20Instruction.write = 1 
            regDC20Instruction.writeSelect = 3
            regDC20Instruction.deviceSelect = 0
            regDC20Instruction.data = value
        elif dcInstr == 'SetCaptureUserData':
            regDC20Instruction.write = 1 
            regDC20Instruction.writeSelect = 5
            regDC20Instruction.deviceSelect = 0
            regDC20Instruction.data = value
        else:
            raise Exception('DC instruction {} is not supported'.format(dcInstr))

        self._Write(regDC20Instruction)
        
    def _EnqueueDC16(self, dcInstr, value):
        # regDC16Instruction = self._hpcc.dcRegisters.AdAte320InstructionDC16()
        regDC16Instruction = AdAte320InstructionDC16()
        if dcInstr == 'BlockingWait':
            regDC16Instruction.write = 0
            regDC16Instruction.deviceSelect = 0
            regDC16Instruction.deviceFunction = 3
            regDC16Instruction.data = value
        elif dcInstr == 'StartDcDataCapture':
            regDC16Instruction.write = 0
            regDC16Instruction.deviceSelect = 0
            regDC16Instruction.deviceFunction = 0
            regDC16Instruction.data = 1
        else:
            raise Exception('DC instruction {} is not supported'.format(dcInstr))

        self._Write(regDC16Instruction)
        
    def _Read(self, instruction):
        AdAte320FeedbackReg = AdAte320Feedback()
        AdAte320FeedbackReg.value = 0
        self.write_register(AdAte320FeedbackReg)
        # self._hpcc.Write('AdAte320Feedback', 0)

        regAdAte320Instruction = AdAte320Instruction()
        regAdAte320Instruction.value = instruction
        regAdAte320Instruction.write = 0
        self.write_register(regAdAte320Instruction)

        # regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction(uint=instruction, length=32)
        # regAdAte320Instruction.write = 0
        # self._Write(regAdAte320Instruction)

        tries = 10
        while (self.read_register(AdAte320Feedback).validData != 0) and (tries > 0):
        # while (self._hpcc.Read('AdAte320Feedback').validData != 1) and (tries > 0):
            tries -= 1
            
        if tries > 0:
            #self.Log('debug', 'Read AdAte320 for hpcc {} slice {} channel {} Done'.format(self._slot, self._slice, self.fpgaChannelNum))
            return self.read_register(AdAte320Feedback)
            # return self._hpcc.Read('AdAte320Feedback')
        else:
            self.Log('error', 'Read AdAte320 for hpcc {} slice {} channel {} Failed'.format(self._slot, self._slice, self.fpgaChannelNum))
            # return self._hpcc.Read('AdAte320Feedback')
            return self.read_register(AdAte320Feedback)


    def _Write(self, instruction):        
        self.write_register(instruction)
        AdAte320InstructionEnableReg = AdAte320InstructionEnable()
        #AdAte320InstructionEnableReg = self.read_register(AdAte320InstructionEnableReg())
        AdAte320InstructionEnableReg.executeInstruction = 1
        self.write_register(AdAte320InstructionEnableReg)
        
        # regAdAte320InstructionEnable = self._hpcc.Read('AdAte320InstructionEnable')
        # regAdAte320InstructionEnable.executeInstruction = 1
        # self._hpcc.Write('AdAte320InstructionEnable', regAdAte320InstructionEnable)
        
        tries = 10
        # while (self._hpcc.Read('AdAte320InstructionEnable').executeInstruction != 0) and (tries > 0):
        while (self.read_register(AdAte320InstructionEnable).executeInstruction != 0) and (tries > 0):
            tries -= 1
        if tries <= 0:
            self.Log('error', 'Write Instruction {} to AdAte320 for hpcc {} slice {} channel {} Failed'.format(hex(instruction.Pack()), self._slot, self._slice, self.fpgaChannelNum))
        #else:
        #    self.Log('debug', 'Write Instruction {} to AdAte320 for hpcc {} slice {} channel {} Succeeded'.format(hex(instruction.Pack()), self._slot, self._slice, self.fpgaChannelNum))
        
    def DumpRegisters(self):
        registers = {0x19: "DRVControl", 0x1A: "CMPControl", 0x1B: "LOADControl", 0x1C: "PPMUControl", 0x21: "VIHGain", 0x31: "VIHOffset", 0x01: "VIHLevel", 0x23: "VILGain", 0x33: "VILOffset", 0x03: "VILLevel", 0x24: "VCHGain", 0x34: "VCHOffset", 0x04: "VCHLevel", 0x25: "VCLGain", 0x35: "VCLOffset", 0x05: "VCLLevel", 0x26: "VOHGain", 0x36: "VOHOffset", 0x06: "VOHLevel", 0x28: "VIOHGain", 0x38: "VIOHOffset", 0x08: "VIOHLevel", 0x29: "VIOLGain", 0x39: "VIOLOffset", 0x09: "VIOLLevel", 0x22: "VITGain", 0x32: "VITOffset", 0x42: "VCOMGain", 0x52: "VCOMOffset", 0x02: "VITVCOMLevel", 0x2A: "PPMUFVGain", 0x3A: "PPMUFVOffset", 0x4A: "PPMUFIAGain", 0x4B: "PPMUFIBGain", 0x4C: "PPMUFICGain", 0x4D: "PPMUFIDGain", 0x4E: "PPMUFIEGain", 0x5A: "PPMUFIOffset", 0x0A: "PPMULevel", 0x2B: "PCHVGain", 0x3B: "PCHVOffset", 0x44: "PCHIGain", 0x54: "PCHIOffset", 0x0B: "PCHLevel", 0x2C: "PCLVGain", 0x3C: "PCLVOffset", 0x45: "PCLIGain", 0x55: "PCLIOffset", 0x0C: "PCLLevel"}
        for reg in registers:
            regAdAte320Instruction = self._hpcc.dcRegisters.AdAte320Instruction()
            regAdAte320Instruction.write = 0        
            regAdAte320Instruction.deviceSelect = self.deviceSelect        
            regAdAte320Instruction.channelSelect = self.channelSelect    
            regAdAte320Instruction.registerSelect = reg
            regAdAte320Instruction.data = 0x0000
            feedback = self._Read(regAdAte320Instruction.Pack())
            if "Offset" in registers[reg] and feedback.data != 0x8000:
                self.Log('warning', 'slot: {}, slice: {}, valid: {}, device: {}, channel: {}, register: 0x{:X}, data: 0x{:X}, {}'.format(self._slot, self._slice, feedback.validData, feedback.deviceSelect, feedback.channelSelect, feedback.registerSelect, feedback.data, registers[reg]))
            else:
                self.Log('debug', 'slot: {}, slice: {}, valid: {}, device: {}, channel: {}, register: 0x{:X}, data: 0x{:X}, {}'.format(self._slot, self._slice, feedback.validData, feedback.deviceSelect, feedback.channelSelect, feedback.registerSelect, feedback.data, registers[reg]))
        

    def read_register(self, RegisterType, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        value = self._hpcc.BarRead(RegisterType.BAR, RegisterType.address(index))
        return RegisterType(value=value)


    def write_register(self, register):
        """Writes a register of the given type, the index is for register arrays"""
        self._hpcc.BarWrite(register.BAR, register.ADDR, register.value)

class Register(ctypes.LittleEndianStructure):
    """Register base class - defines setter/getter for integer value of register"""
    BAR = 0
    BASEADDR = 0
    BASEMUL = 16
    REGCOUNT = 1

    def init(self, value=None):
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i.to_bytes(4, byteorder='little'), 4)

    @classmethod
    def address(cls, index=0):
        return cls.BASEADDR + index * cls.BASEMUL


class PatternControl(Register):
    BAR = 0
    ADDR = 0x00008230
    CHANNEL_LINKING_2X = 0
    CHANNEL_LINKING_4X = 1
    CHANNEL_LINKING_8X = 2
    _fields_ = [('EnableDisableOutputs', ctypes.c_uint, 1),
                ('PreStagePattern', ctypes.c_uint, 1),
                ('Reserved1', ctypes.c_uint, 1),
                ('SubCycleCount', ctypes.c_uint, 4),
                ('StartOnNextRCSyncPulse', ctypes.c_uint, 1),
                ('AbortPattern', ctypes.c_uint, 1),
                ('StickyErrorDomainMask', ctypes.c_uint, 15),
                ('PRBSEnable', ctypes.c_uint, 1),
                ('ToggleEnable', ctypes.c_uint, 1),
                ('LinkMode', ctypes.c_uint, 2),
                ('VectorProcessorStarted', ctypes.c_uint, 1),
                ('PerVectorControlStarted', ctypes.c_uint, 1),
                ('PinRingAcknowledge', ctypes.c_uint, 1),
                ('SyncStartBypass', ctypes.c_uint, 1)]


# ADATE320 Debug Access Registers (0x0300-0x03FF)
class AdAte320Instruction(Register):
    BAR = 1
    ADDR = 0x00000300
    _fields_ = [('data',ctypes.c_uint,16),
                ('registerSelect',ctypes.c_uint,7),
                ('channelSelect',ctypes.c_uint,2),
                ('deviceSelect',ctypes.c_uint,6),
                ('write',ctypes.c_uint,1)]


class AdAte320InstructionDC16(Register):
    BAR = 1
    ADDR = 0x00000300
    _fields_ = [('data',ctypes.c_uint,16),
                ('deviceFunction',ctypes.c_uint,9),
                ('deviceSelect',ctypes.c_uint,6),
                ('bit',ctypes.c_uint,1),
                ('write',ctypes.c_uint,1)]

class AdAte320InstructionDC20(Register):
    BAR = 1
    ADDR = 0x00000300
    _fields_ = [('data',ctypes.c_uint,20),
                ('writeSelect',ctypes.c_uint,5),
                ('deviceSelect',ctypes.c_uint,6),
                ('bit', ctypes.c_uint, 1),
                ('write', ctypes.c_uint, 1)]


class AdAte320InstructionEnable(Register):
    BAR = 1
    ADDR = 0x00000310
    _fields_ = [('reserved',ctypes.c_uint,31),
                ('executeInstruction', ctypes.c_uint, 1)]



class AdAte320Feedback(Register):
    BAR = 1
    ADDR = 0x00000320
    _fields_ = [('data',ctypes.c_uint,16),
                ('registerSelect',ctypes.c_uint,7),
                ('channelSelect',ctypes.c_uint,2),
                ('deviceSelect',ctypes.c_uint,6),
                ('validData',ctypes.c_uint,1)]


