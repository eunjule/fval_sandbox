################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: vcd.py
#-------------------------------------------------------------------------------
#     Purpose: VCD Creator
#-------------------------------------------------------------------------------
#  Created by: Jared Pager (HpccGui)
#        Date: 09/24/15
#       Group: HDMT FPGA Validation
################################################################################

from time import strftime

class Signal:
    def __init__(self, num):
        self.identifier = chr(34 + num)
        self.name = 'ila_data[{}]'.format(num)
        self.reset()
    def reset(self):
        self.last_value = 'x'
    def print_var(self, file):
        print('$var wire 1 ' + self.identifier + ' ' + self.name + ' $end', file=file)
    def print(self, value, file):
        value = str(value)
        if (value != self.last_value):
            print(value + self.identifier, file=file)
        self.last_value = value
    def set_name(self, name):
        self.name = name

class VcdCreator:
    def __init__(self, numSignals, moduleName, version):
        self.signals = [Signal(i) for i in range(numSignals)]
        self.moduleName = moduleName
        self.version = version
    def SetSignalName(self, num, name):
        self.signals[num].set_name(name)
    def CreateVcd(self, fileName, signalData, timescale):
        with open(fileName, 'w') as file:
            clock = Signal(-1)
            clock.set_name('clock')

            print("$date", file=file)
            print(strftime("%a %b %d %H:%M:%S %Y"), file=file)
            print("$end", file=file)
            print("$version", file=file)
            print(self.version, file=file)
            print("$end", file=file)
            print("$timescale {} $end".format(timescale), file=file)
            print("$scope module {} $end".format(self.moduleName), file=file)
            clock.print_var(file)
            for sig in reversed(self.signals):
                sig.reset()
                sig.print_var(file)
            print("$upscope $end", file=file)
            print("$enddefinitions $end", file=file)
            print("$dumpvars", file=file)
            print("$end", file=file)
            simulation_time = 0
            for s in signalData:
                clock.print(1, file)
                for (i, sig) in enumerate(self.signals):
                    sig.print((s >> i) & 0x1, file)
                simulation_time += 1
                print('#' + str(simulation_time), file=file)
                clock.print(0, file)
                simulation_time = simulation_time + 1
                print('#' + str(simulation_time), file=file)
