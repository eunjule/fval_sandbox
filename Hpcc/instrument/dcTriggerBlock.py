################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: dcTriggerBlock.py
#-------------------------------------------------------------------------------
#     Purpose: code for HPCC DC software block/trigger block, doesn't work yet
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/20/15
#       Group: HDMT FPGA Validation
################################################################################

import time

from Common.instruments.base import Instrument

SOFTWARE_BLOCK_INDEX = 15358
NUM_TRIGGER_BLOCKS = 15356
STOP_DC_EXECUTION_INSTRUCTION = 0x00000000
DC_TRIGGER_BLOCKS_SEGMENT     = 0x10000000
DC_TRIGGER_BLOCK_MAPPING_SEGMENT  = 0x1EFF0000
LTC_TRIGGER_BLOCK_MAPPING_SEGMENT = 0x1EFF4000
SOFTWARE_BLOCK_SEGMENT = 0x1EFF8000
SOFTWARE_BLOCK_FEEDBACK_SEGMENT  = 0x1EFFC000
INSTRUCTIONS_PER_TRIGGER_BLOCK = 4096
SIZE_OF_DC_INSTRUCTION = 4
SIZE_OF_TRIGGER_BLOCK = SIZE_OF_DC_INSTRUCTION * INSTRUCTIONS_PER_TRIGGER_BLOCK

class DcTriggerBlock(Instrument):
    def __init__(self, hpccdc):
        super().__init__('')
        self._hpcc = hpccdc
        self.index = SOFTWARE_BLOCK_INDEX
        self.instructions = bytes()
        
    def Enqueue(self, instruction):
        byteI = bytes.fromhex('{:08x}'.format(instruction))
        self.instructions += byteI
        
    def Commit(self):
        if len(self.instructions) > 0:
            self.Enqueue(STOP_DC_EXECUTION_INSTRUCTION)
            self.Log('debug', 'DC Trigger Block - commiting {}'.format(self.instructions))
            #print(len(self.instructions))
            self._hpcc.DmaWrite(SOFTWARE_BLOCK_SEGMENT, self.instructions)
            #self.Log('debug', 'DC Trigger Block - start execution')
            self.Execute()
            #self.Log('debug', 'DC Trigger Block - processing feedback')
            self.ProcessFeedback()
        else:
            raise Exception('Empty dc trigger block, fail to commit')
            
    def Execute(self):
        # Reset the Instruction Block Domain and the Data Capture.
        self._hpcc._ClearResults(False)
        
        # Clear Register and set RequestStart so the FPGA executes the DC Instructions.
        regSoftwareBlockManagement = self._hpcc.dcRegisters.SoftwareBlockManagement()
        regSoftwareBlockManagement.RequestStart = 1
        self._hpcc.Write('SoftwareBlockManagement', regSoftwareBlockManagement)    
        
        # Poll until RequestStart is cleared or timeout 
        timeout = time.time() + 1 # 1 sec timeout
        while (self._hpcc.Read('SoftwareBlockManagement').RequestStart == 1) and (time.time() < timeout):
            time.sleep(0)
        if self._hpcc.Read('SoftwareBlockManagement').RequestStart == 1:
            self.Log('error', 'DC Trigger block not start')
            #m_pFpgaRegisters->ReadRegister(m_pFpgaRegisters->TriggerBlockStatusRegister);
            #m_pFpgaRegisters->ReadRegister(m_pFpgaRegisters->ActiveTriggerRegister);

        # Poll until FeedbackComplete is set or timeout after 10 seconds.
        timeout = time.time() + 10
        while (self._hpcc.Read('SoftwareBlockManagement').FeedbackComplete == 0) and (time.time() < timeout):
            time.sleep(0)
        if self._hpcc.Read('SoftwareBlockManagement').FeedbackComplete == 0:
            self.Log('error', 'DC Trigger block not complete execution')
            
    def ProcessFeedback(self):
        regSoftwareBlockManagement = self._hpcc.Read('SoftwareBlockManagement')
        feedbackCount = regSoftwareBlockManagement.FeedbackCount
        #self.Log('debug', 'DC Trigger Block - received {} feedback'.format(feedbackCount))
        if feedbackCount > 0:
            self._hpcc.DmaRead(SOFTWARE_BLOCK_FEEDBACK_SEGMENT, feedbackCount * SIZE_OF_DC_INSTRUCTION)

            
    

    

