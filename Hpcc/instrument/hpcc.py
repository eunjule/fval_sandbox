################################################################################
################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: hpcc.py
#-------------------------------------------------------------------------------
#     Purpose: Class for one HPCC card
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/13/15
#       Group: HDMT FPGA Validation
################################################################################

import collections
import glob
import os
import random
import re
import string
import time
from Tools.projectpaths import ROOT_PATH

from . import adate320Reg as adate320_registers
from . import ad9914_registers
from Common import configs
from Common import fval
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo
from Common.instruments.base import Instrument
from Common.triggers import CardType, generate_trigger
from Hpcc import hpcc_configs as hpccConfigs
from .hpccacS0 import HpccAcS0
from .hpccacS1 import HpccAcS1
from .hpccCalDaughter import HpccCalDaughter
from .hpccdc import HpccDc
from Hpcc.hpcctb.loopbackcal import Calibration
from Hpcc.hpcctb.dcCal import DcCalibration


CAL_FILE_NAMES = collections.namedtuple('CAL_FILE_NAMES','EvenToOddSlice0 EvenToOddSlice1 LowToHighSlice0 LowToHighSlice1 DcSlice0 DcSlice1')

DC_REG_DS = os.path.join(os.path.dirname(__file__), 'hpccDcRegs.ds')
dc_registers = fval.LoadStructs(DC_REG_DS)


class Hpcc(Instrument):
    CHECK_LINK_STABILITY_NUM_ITERATIONS = 10000

    dcRegisters = dc_registers
    clockRegs = ad9914_registers
    peRegs = adate320_registers
    configs = configs
    hpccConfigs = hpccConfigs
    calFile = None
    cal = None

    def __init__(self, slot, rc, name=None):
        super().__init__(name)
        self.rc = rc
        self.cal = HpccCalDaughter(slot)
        self.slot = slot
        self.under_test = False
        self.NUMBER_TRIES = 100
        self._is_initialized = False
        self.instrumentblt = CachingBltInfo(callback_function=self.read_instrument_blt,
                                            name='HPCC Instrument slot {}'.format(slot))
        self.acboardblt = CachingBltInfo(callback_function=self.read_board_acblt,
                                         name='HPCC Daughter Board slot {}'.format(slot))
        self.dcboardblt = CachingBltInfo(callback_function=self.read_board_dcblt,
                                         name='HPCC Main Board slot {}'.format(slot))

        # Create HPCC AC instances
        self.ac = []
        ac0 = HpccAcS0(self, slot)
        self.ac.append(ac0)
        if not hpccConfigs.HPCC_SLICE0_ONLY:
            ac1 = HpccAcS1(self, slot)
            self.ac.append(ac1)
        # Create HPCC DC instances
        self.dc = []
        dc0 = HpccDc(self, slot, 0)
        self.dc.append(dc0)
        dc1 = HpccDc(self, slot, 1)
        self.dc.append(dc1)

    def discover(self):
        instruments = []
        if self._ac_is_present() and self._dc_is_present():
            instruments.append(self)
        else :
            return []

        return instruments

    def _ac_is_present(self):
        try:
            # 1132016 Change to fix issue with HPCCAC Card not detect - Start
            hil.hpccAcDeviceEnable(self.slot, 0)
            hil.hpccAcDeviceEnable(self.slot, 1)
            # 1132016 Change to fix issue with HPCCAC Card not detect - End

            hil.hpccAcConnect(self.slot)
            hil.hpccAcDisconnect(self.slot)
            return True
        except RuntimeError:
            pass

    def _dc_is_present(self):
        try:
            hil.hpccDcConnect(self.slot)
            hil.hpccDcDisconnect(self.slot)
            return True
        except RuntimeError:
            pass


    def _PerformClockSync(self):
        slice = 0 # FPGA0 controls the process
        # TODO: UpdateSoftwareScratchpad();
        # TODO: program lmk04808 only if it is not initialized!!
        statusReg = self.ac[slice].Read('ResetsAndStatus')
        self.Log('info', 'Pre initializing HPCC AC0 LMK04808 on slot {} AC FPGA 0. Lmk04808StatusLd: 0x{:X}, Lmk04808StatusClkIn0: 0x{:X}, Lmk04808StatusClkIn1: 0x{:X}'.format(self.slot, statusReg.Lmk04808StatusLd, statusReg.Lmk04808StatusClkIn0, statusReg.Lmk04808StatusClkIn1))

        if statusReg.Lmk04808StatusLd == 1 and statusReg.Lmk04808StatusClkIn0 == 1 and statusReg.Lmk04808StatusClkIn1 == 1 and hpccConfigs.HPCC_AC_FPGA_LOAD == False:
            self.Log('info', 'Lmk04808 is already programmed, skip it.')
        else:
            # Disable Lmk01000 output (Prevent any runt pulses from getting to FPGA during programming)
            self.ac[slice].lmk01000.Reset()
            self.ac[slice].lmk01000.EnableOutput(False)

            # Program Lmk04808 using FPGA0
            self.ac[slice].lmk04808S0.Initialize(self.ac[slice].fineDelayDivider, self.is_hpcc2)
            time.sleep(10)

            # TODO: fix work around
            for i in range(20):
                statusReg = self.ac[slice].Read('ResetsAndStatus')
                self.Log('debug', 'Initialized HPCC AC0 LMK04808 on slot {} AC FPGA 0. Lmk04808StatusLd: 0x{:X}, Lmk04808StatusClkIn0: 0x{:X}, Lmk04808StatusClkIn1: 0x{:X}'.format(self.slot, statusReg.Lmk04808StatusLd, statusReg.Lmk04808StatusClkIn0, statusReg.Lmk04808StatusClkIn1))
                time.sleep(0.02)

            tries = self.NUMBER_TRIES
            while (self.ac[slice].Read('ResetsAndStatus').Lmk04808StatusLd != 1) and (tries > 0):
                tries -= 1
                time.sleep(0.02)
            statusReg = self.ac[slice].Read('ResetsAndStatus')
            if tries > 0 and statusReg.Lmk04808StatusLd == 1 and statusReg.Lmk04808StatusClkIn0 == 1 and statusReg.Lmk04808StatusClkIn1 == 1:
                self.Log('debug', 'Initialized HPCC AC0 LMK04808 on slot {} AC FPGA 0. Lmk04808StatusLd: 0x{:X}, Lmk04808StatusClkIn0: 0x{:X}, Lmk04808StatusClkIn1: 0x{:X}'.format(self.slot, statusReg.Lmk04808StatusLd, statusReg.Lmk04808StatusClkIn0, statusReg.Lmk04808StatusClkIn1))
                self.Log('info', 'Program Lmk04808 Done')
            else:
                self.Log('info', 'Initialized HPCC AC0 LMK04808 on slot {} AC FPGA 0. Lmk04808StatusLd: 0x{:X}, Lmk04808StatusClkIn0: 0x{:X}, Lmk04808StatusClkIn1: 0x{:X}. Total times to try {}. Tried {} times.'.format(self.slot, statusReg.Lmk04808StatusLd, statusReg.Lmk04808StatusClkIn0, statusReg.Lmk04808StatusClkIn1, self.NUMBER_TRIES, self.NUMBER_TRIES - tries))
                self.Log('error', 'Program Lmk04808 Failed')

        self.ac[0]._InitializeClockChainFPGA0()
        if not hpccConfigs.HPCC_SLICE0_ONLY:
            self.ac[1]._InitializeClockChainFPGA1()

    def ensure_initialized(self):
        if not self._is_initialized:
            self.Initialize()

    def Initialize(self):

        self.log_blt_info()

        self.Log('info', 'Hpcc DC {} CPLD Version = {}'.format(self.slot, self.mainboard_cpld_version()))
        self.Log('info', 'Hpcc AC {} CPLD Version = {}'.format(self.slot, self.hpccac_cpld_version()))

        if configs.SKIP_INIT:
            self.Log('info', f'Skipping initialization for {self.name()}')
        else:
            self.load_dc_fpgas()
            load_ac_fpgas(self)

            # Update DC Cable Loss Compensation
            if "G97918" in self.dcboardblt.PartNumberCurrent:
                self.Log('info', "Update CLC = 6 for HPCC DC board G97918")
                self.dc[0].CLC_MAX = 6
                self.dc[1].CLC_MAX = 6

            # Update AC FPGA Capabilities, including FineDelayDivider
            if hpccConfigs.HPCC_SLICE0_ONLY:
                activeSlice = [0]
            else:
                activeSlice = [0, 1]
            for slice in activeSlice:
                self.ac[slice]._UpdateFpgaCapabilities()

            # init DC FPGA
            self.dc[0]._InitDCFPGA()
            self.dc[1]._InitDCFPGA()

            # init Clock Chain, and AC FPGA
            self._PerformClockSync()
            self.train_aurora_link()

            for slice in activeSlice:
                self.ac[slice]._InitACFPGA()

                # check reset and status reg after init
                statusReg = self.ac[slice].Read('ResetsAndStatus')
                if (statusReg.PLLLocks != 0b1111):
                    self.Log('error', "Incorrect PLLLocks bits after initialize hpcc {} slice {} (actual = 0x{:X}, expected = 0x1111).".format(self.slot, slice, statusReg.PLLLocks))
                if (statusReg.DDR3ControllerInitialized != 0b1111):
                    self.Log('error', "Incorrect DDR3ControllerInitialized bits after initialize hpcc {} slice {} (actual = 0x{:X}, expected = 0x1111).".format(self.slot, slice, statusReg.DDR3ControllerInitialized))
                if (slice == 0) and ((statusReg.Lmk04808StatusLd != 1) or (statusReg.Lmk04808StatusClkIn0 != 1) or (statusReg.Lmk04808StatusClkIn1 != 1)):
                    self.Log('error', "Incorrect Lmk04808Status bits after initialize hpcc {} slice {} (actual = 0x{:X}).".format(self.slot, slice, statusReg.Pack()))

            self.SPIAdjustmentForTrigger(activeSlice)# Enable the PMU analog power rails on Fab D+ (no effect on Fab C)
            self._EnableAnalogPowerRails(True)

            # init PEs on HPCC DC
            for slice in activeSlice:
                for physicalChannel in self.dc[slice].pe:
                    physicalChannel.Initialize()

            # TODO: m_dcFpgas[0]->EnableAuroraInput(true);

            # After Applying and Executing the SoftwareBlock, DcTriggerBlocks will correctly return measurements (but not before).
            # set init value for all PEs
            for slice in activeSlice:
                for physicalChannel in self.dc[slice].pe:
                    physicalChannel.SetInitialValue()

        # validate_aurora_statuses(self.slot)
        self._is_initialized = True

    def train_aurora_link(self):
        if not self.using_rc2():
            num_retries = 20
            name = f'{self.name()}_{self.slot}'

            self.Log('info', f'{name}: Attempting to train Aurora link...')
            for retry in range(num_retries):
                self.dc[0].reset_aurora()
                self.dc[0].reset_trigger_domains()

                self.rc.reset_aurora_bus(self.slot)
                self.rc.clear_aurora_error_counts(self.slot)

                rc_aurora_status = self.rc.aurora_link_is_stable(self.slot)
                dc_aurora_status = self.dc[0].aurora_link_is_stable()
                if rc_aurora_status and dc_aurora_status:
                    self.Log('info',
                             f'{name}: Aurora link trained in {retry} tries')
                    hil.hpccDcBarWrite(self.slot, 0, 0, 0x8050, 0x4)
                    break
            else:
                raise Hpcc.InstrumentError(f'{name}: Unable to train aurora '
                                           f'link')

    def SPIAdjustmentForTrigger(self, activeSlice):
        for slice in activeSlice:
            if self.check_link_stability(slice):
                SPIAdjustmentRegister = self.dc[0].Read('SPIAdjustmentRegister').Pack()
                self.Log('info', f'SPIAdjustmentRegister Status {hex(SPIAdjustmentRegister)} for slice {slice}')
            else:
                SPIAdjustmentRegister = self.dc[0].Read('SPIAdjustmentRegister').Pack()
                self.Log('debug', f'SPIAdjustmentRegister Status Before {hex(SPIAdjustmentRegister)}')
                self.dc[0].Write('SPIAdjustmentRegister', 0x2)
                SPIAdjustmentRegister = self.dc[0].Read('SPIAdjustmentRegister').Pack()
                self.Log('info', f'SPIAdjustmentRegister Status modified to {hex(SPIAdjustmentRegister)} for slice {slice}')
                if self.check_link_stability(slice):
                    pass
                else:
                    raise Hpcc.SpiAdjustReadTriggerError(
                        'Trigger failed with both SPI Adjustment values')

    def check_link_stability(self, slice):
        for i in range(0, self.CHECK_LINK_STABILITY_NUM_ITERATIONS):
            expected = self._generate_trigger()

            self.send_trigger_from_hpccac(expected, slice)
            self.verify_trigger_at_rctc_up(expected)
            trigger_received = self.wait_on_trigger_at_hpccac(slice, expected)

            if trigger_received != expected:
                self.Log('warning',
                         self.trigger_fail_at_hpccac_msg(expected,
                                                         trigger_received))
                self.verify_trigger_at_hpccdc_up(expected)
                self.verify_trigger_at_rctc_up(expected)
                self.verify_trigger_at_rctc_down(expected)
                self.verify_trigger_hpccdc_down(expected)
                return False
        return True

    def _generate_trigger(self):
        return generate_trigger(card_type=CardType.INVALID.name,
                                payload=random.getrandbits(19),
                                is_software_trigger=False)

    def wait_on_trigger_at_hpccac(self, slice, expected, num_retries=50):
        actual_trigger = 0
        for retry in range(num_retries):
            actual_trigger = self.read_trigger_at_hpccac(slice)
            if expected == actual_trigger:
                break
        return actual_trigger

    def trigger_fail_at_hpccac_msg(self, expected, actual):
        return f'(AC->DC->RC->DC-AC) AC back to AC (expected, actual): ' \
               f'0x{expected:08X}, 0x{actual:08X}'

    def read_trigger_at_hpccac(self, slice):
        trigger_received = self.ac[slice].Read('TriggerEvent').Pack()
        return trigger_received

    def verify_trigger_hpccdc_down(self, expected):
        trigger_dc_down = self.dc[0].Read('DownTrigger').Pack()
        if trigger_dc_down != expected:
            self.Log('warning',
                     self.trigger_fail_hpccdc_down_msg(expected,
                                                       trigger_dc_down))

    def trigger_fail_hpccdc_down_msg(selfself, expected, actual):
        return f'(RC->DC) DC received trigger from RC (expected, actual): ' \
               f'0x{expected:08X}, 0x{actual:08X}'

    def verify_trigger_at_rctc_down(self, expected):
        if self.using_rc2():
            trigger_rc_down = self.rc.read_trigger_down(self.slot)
            if trigger_rc_down != expected:
                self.Log('warning',
                         self.trigger_fail_at_rctc_down_msg(expected,
                                                            trigger_rc_down))

    def trigger_fail_at_rctc_down_msg(self, expected, actual):
        return f'(RC->RC) RC sent trigger downstream (expected, actual): ' \
               f'0x{expected:08X}, 0x{actual:08X}'

    def verify_trigger_at_rctc_up(self, expected, num_retries=50):
        trigger_rc_up = 0
        for retry in range(num_retries):
            trigger_rc_up = self.rc.read_trigger_up(self.slot)
            if trigger_rc_up == expected:
                break
        else:
            self.Log('warning',
                     self.trigger_fail_at_rctc_up_msg(expected, trigger_rc_up))

    def trigger_fail_at_rctc_up_msg(self, expected, actual):
        return f'(DC->RC) RC received trigger from DC (expected, actual): ' \
               f'0x{expected:08X}, 0x{actual:08X}'

    def verify_trigger_at_hpccdc_up(self, expected):
        trigger_dc_up = self.dc[0].Read('UpTrigger').Pack()
        if trigger_dc_up != expected:
            self.Log('warning',
                     self.trigger_fail_at_hpccdc_up_msg(expected,
                                                        trigger_dc_up))

    def trigger_fail_at_hpccdc_up_msg(self, expected, actual):
        return f'(AC->DC) DC received trigger from AC (expected, actual): ' \
               f'0x{expected:08X}, 0x{actual:08X}'

    class SpiAdjustReadTriggerError(Exception):
        pass

    def send_trigger_from_hpccac(self, expected, slice):
        self.ac[slice].Write('TriggerEvent', expected)
        self.Log('debug', 'HPCC({}, {}) sent trigger 0x{:x} to DC'.format(self.slot, slice, expected))

    def using_rc2(self):
        return self.rc.name().upper() == 'RC2'

    def ReadCapture(self, slice):
        CAPTURE_BLOCK_SIZE = 128
        blockCount = self.ac[slice].Read('TotalCaptureBlockCount').Pack()  # in blocks
        captureLength = self.ac[slice].Read('CaptureLength').Pack()  # in blocks
        if captureLength < blockCount:
            self.Log('warning', 'Capture length {} is limiting block count {}'.format(captureLength, blockCount))
            blockCount = captureLength
        captureData = self.ac[slice].ReadCapture(blockCount)
        if len(captureData) < blockCount * CAPTURE_BLOCK_SIZE:
            new_block_count = len(captureData) // CAPTURE_BLOCK_SIZE
            self.Log('warning', 'Modifying reported block count from {} to {}'.format(blockCount, new_block_count))
            blockCount = new_block_count
        captureCount = self.ac[slice].Read('TotalCaptureCount').Pack()  # in vectors, HAL doesn't use this variable any more
        self.Log('debug', 'Captured {} vectors for ({}, {})'.format(captureCount, self.slot, slice))
        return (captureData, blockCount, captureCount)

    def _EnableAnalogPowerRails(self, enable):
        # Put the ADATE320 and AD7609 devices into reset state that will be held while turning analog power rails on or off.
        self.dc[0]._ResetAdAte320AndAd7609Domains(True)
        self.dc[1]._ResetAdAte320AndAd7609Domains(True)
        self.dc[1]._EnableAnalogPowerRails(enable)
        if enable:
            time.sleep(2)
            # Now bring the ADATE320 and AD7609 devices out of reset.
            self.dc[0]._ResetAdAte320AndAd7609Domains(False)
            self.dc[1]._ResetAdAte320AndAd7609Domains(False)
            time.sleep(0.01)
            # TODO: do we need an isIdle flag for DC FPGA?

    def calibration(self):
        # need to get RC in instruments
        rc2 = self.rc
        slot = self.slot
        hpcc = self
        if self.cal.cardFound:
            # calibration, force cal if cal data doesn't exist
            CALIBRATION_PATH = os.path.join(ROOT_PATH, hpccConfigs.CALIBRATION_DIR)
            if not os.path.exists(CALIBRATION_PATH):
                os.makedirs(CALIBRATION_PATH)
            EvenToOddSlice0_filename = os.path.join(CALIBRATION_PATH,
                                        r"{}_{}_{}_ac_cal_eo.txt".format(hpcc.instrumentblt.SerialNumber, slot, 0))
            EvenToOddSlice1_filename = os.path.join(CALIBRATION_PATH,
                                        r"{}_{}_{}_ac_cal_eo.txt".format(hpcc.instrumentblt.SerialNumber, slot, 1))
            LowToHighSlice0_filename = os.path.join(CALIBRATION_PATH,
                                        r"{}_{}_{}_ac_cal_lh.txt".format(hpcc.instrumentblt.SerialNumber, slot, 0))
            LowToHighSlice1_filename = os.path.join(CALIBRATION_PATH,
                                        r"{}_{}_{}_ac_cal_lh.txt".format(hpcc.instrumentblt.SerialNumber, slot, 1))
            DcSlice0_filename = os.path.join(CALIBRATION_PATH,
                                        r"{}_{}_{}_dc_cal.txt".format(hpcc.instrumentblt.SerialNumber, slot, 0))
            DcSlice1_filename = os.path.join(CALIBRATION_PATH,
                                        r"{}_{}_{}_dc_cal.txt".format(hpcc.instrumentblt.SerialNumber, slot, 1))
            self.calFile = CAL_FILE_NAMES(EvenToOddSlice0_filename, EvenToOddSlice1_filename, LowToHighSlice0_filename,
                                       LowToHighSlice1_filename, DcSlice0_filename, DcSlice1_filename)

            if hpccConfigs.HPCC_ENV_INIT in ['ZEROCAL']:
                # create 1.5V DC cal and 0ns AC cal
                self.Log('info', 'ZEROCAL: creating 1.5V DC cal files and 0ns AC cal files for slot {}'.format(slot))
                for calFilename in self.calFile:
                    fout = open(calFilename, 'w')
                    for ch in range(56):
                        if "_dc" in calFilename:
                            fout.write("{},{}\n".format(ch, 1.5))
                        else:
                            fout.write("{},{}\n".format(ch, 0))
                    fout.close()
            else:
                # DC calibration
                forceDCCal = False
                if not hpccConfigs.HPCC_SLICE1_ONLY:
                    forceDCCal = forceDCCal or (
                            not os.path.isfile(self.calFile.DcSlice0) and hpccConfigs.HPCC_ENV_INIT in ['LOADCAL'])
                if not hpccConfigs.HPCC_SLICE0_ONLY:
                    forceDCCal = forceDCCal or (
                            not os.path.isfile(self.calFile.DcSlice1) and hpccConfigs.HPCC_ENV_INIT in ['LOADCAL'])
                if ((not os.path.isfile(self.calFile.DcSlice0)) or (
                not os.path.isfile(self.calFile.DcSlice1))) and hpccConfigs.HPCC_ENV_INIT in ['LOADCAL']:
                    self.Log('warning',
                            'Calibration option selected as Loadcal, but calibration files doesnt exists so running the calibration')
                if hpccConfigs.HPCC_ENV_INIT in ['CAL', 'FULL'] or forceDCCal:
                    dcCal = DcCalibration(hpcc, rc2)
                    dcCal.DcCalibration()
                if ((not os.path.isfile(self.calFile.DcSlice0)) or (
                not os.path.isfile(self.calFile.DcSlice1))) and hpccConfigs.HPCC_ENV_INIT in ['NONE']:
                    self.Log('warning', 'Calibration option selected as NONE, so skipping calibration')
                else:
                    hpcc.dc[0].LoadDcCal(self.calFile.DcSlice0)
                    if not hpccConfigs.HPCC_SLICE0_ONLY:
                        hpcc.dc[1].LoadDcCal(self.calFile.DcSlice1)

                # AC calibration
                forceACCal = False
                if not hpccConfigs.HPCC_SLICE1_ONLY:
                    forceACCal = forceACCal or (
                            not (os.path.isfile(self.calFile.EvenToOddSlice0)) and hpccConfigs.HPCC_ENV_INIT in ['LOADCAL'])
                if not hpccConfigs.HPCC_SLICE0_ONLY:
                    forceACCal = forceACCal or (
                            not (os.path.isfile(self.calFile.EvenToOddSlice1)) and hpccConfigs.HPCC_ENV_INIT in ['LOADCAL'])
                if (not hpccConfigs.HPCC_SLICE1_ONLY) and (not hpccConfigs.HPCC_SLICE0_ONLY):
                    forceACCal = forceACCal or (
                    not (os.path.isfile(self.calFile.LowToHighSlice0) and os.path.isfile(self.calFile.LowToHighSlice1)) and hpccConfigs.HPCC_ENV_INIT in [
                        'LOADCAL'])
                if hpccConfigs.HPCC_ENV_INIT in ['CAL', 'FULL'] or forceACCal:
                    loopbackCal = Calibration(hpcc, rc2)
                    loopbackCal.LoopbackCal()
        else:
            self.Log('warning', 'Cal card for slot {} not found. Skipping calibration for this slot.'.format(hpcc.slot))

    def InitializeCal(self):
        if self.cal.cardFound:
            self.cal.Initialize()
            self.cal.SetEvenToOddChannelLoopback()
        else:
            self.Log('warning', 'Cal card for slot {} not found. Skipping InitializeCal() for this slot.'.format(self.slot))
            
    # High Purity clock goes to pin on DC FPGA 0, but mux is controlled by DC FPGA 1
    def EnableHPClockPin48Output(self, enable):
        gpio = self.dc[1].Read('Gpio')
        if enable:
            gpio.HighPurityClockSelect = 1
            self.Log('info', 'Route 9914 HP Clock to pin 48')
        else:
            gpio.HighPurityClockSelect = 0
            self.Log('info', 'Disable 9914 HP Clock to pin 48')
        self.dc[1].Write('Gpio', gpio)

    def DumpCapture(self, slice, limit=5000):
        captureData, blockCount, captureCount = self.ReadCapture(slice)
        vectorCount = 0
        self.Log('info',
                 "pin\t" + "54 52 50 48 46 44 42 40 38 36 34 32 30 28 26 24 22 20 18 16 14 12 10  8  6  4  2  0")
        for block in self.ac[slice].UnpackCapture(captureData):
            header, data = block
            for i in range(8):
                if header.CapturedVectorsValidFlags[i] == 1:
                    actual = data[i].PinLaneData
                    vaddr = data[i].VectorAddress
                    actualString = '{:056b}'.format(actual)
                    vectorString = ' '.join(actualString[i:i + 2] for i in range(0, len(actualString), 2))
                    if vectorCount < limit:
                        self.Log('info', str(vaddr) + "\t" + vectorString)
                    vectorCount += 1

    def read_instrument_blt(self):
        return hil.hpccBltInstrumentRead(self.slot)

    def read_board_acblt(self):
        return hil.hpccAcBltBoardRead(self.slot)

    def read_board_dcblt(self):
        return hil.hpccDcBltBoardRead(self.slot)

    @property
    def is_hpcc2(self):
        return self.acboardblt.PartNumberCurrent.startswith('AAJ49483')

    def mainboard_cpld_version(self):
        return hil.hpccDcCpldVersionString(self.slot)

    def hpccac_cpld_version(self):
        return hil.hpccAcCpldVersionString(self.slot)

    def log_blt_info(self):
        self.instrumentblt.write_to_log()
        self.acboardblt.write_to_log()
        self.dcboardblt.write_to_log()

    def load_dc_fpgas(self):
        dcl_configs_attribute = f'{self.main_board_type()}_DCL_FPGA_MDUT_Fab{self.main_board_fab()}_IMAGE'
        dcu_configs_attribute = f'{self.main_board_type()}_DCU_FPGA_MDUT_Fab{self.main_board_fab()}_IMAGE'

        fpga_configs = [('HPCC_DCL_FPGA_LOAD', dcl_configs_attribute, 0),
                        ('HPCC_DCU_FPGA_LOAD', dcu_configs_attribute, 1)]

        if not hpccConfigs.HPCC_DCL_FPGA_LOAD and not hpccConfigs.HPCC_DCU_FPGA_LOAD:
            self.Log('info', 'Skipping DC FPGA image loading due to hpccConfigs.HPCC_DCL_FPGA_LOAD={} and hpccConfigs.HPCC_DCU_FPGA_LOAD={}'.format(
                hpccConfigs.HPCC_DCL_FPGA_LOAD, hpccConfigs.HPCC_DCU_FPGA_LOAD))
            return

        for config in fpga_configs:
            load = getattr(hpccConfigs, config[0], True)
            if load:
                try:
                    image = getattr(hpccConfigs, config[1])
                except AttributeError:
                    self.Log('error', '\'{}\' is not defined in configs.py'.format(config[1]))
                slice = config[2]

                try:
                    image_id = int(image.split('fpga_rev')[1].split('_')[0])
                except IndexError:
                    self.Log('error', f'DC image name is not valid: "{image}"')

                self.Log('info', f'Loading slot {self.slot} slice {slice} with DC image: \'{image}\'')
                actual = CompatibilityEntry(part_number=self.main_board_type(), fab=self.main_board_fab(), image_id=image_id)
                if actual not in mainboard_compatibility_table:
                    self.Log('info', 'HPCC Main Board / FPGA image compatibility table:')
                    print_compatibility_table(self.Log, mainboard_compatibility_table)
                    self.Log('error', f'{self.main_board_type()} fab {self.main_board_fab()} in '
                                      f'slot {self.slot} is not compatible with rev{image_id} DC FPGA images')

                self.Log('debug', 'Previous DC image version: 0x{:X}'.format(self.dc[slice].GetFpgaVersion()))
                try:
                    self.dc[slice].FpgaLoad(image)
                except RuntimeError as e:
                    self.Log('error',
                             f'HPCC_DC_{slice}: HIL raised a RuntimeError '
                             f'with message "{e}"')
                self.Log('info', 'Current DC Image: 0x{:X}'.format(self.dc[slice].GetFpgaVersion()))

    def clock_compensation_register_status(hpcc):
        AuroraClockCompensationCtrlRegister = hpcc.dc[0].Read('AuroraClockCompensationCtrlRegister').Pack()
        hpcc.Log('info', 'Clock Compensation Status register at slice:0 HPCC-DC : {}'.format(
            hex(AuroraClockCompensationCtrlRegister)))

    def enable_dc_fpga_clock_compensation(hpcc):
        hpcc.dc[0].Write('AuroraClockCompensationCtrlRegister', 0x1)
        AuroraClockCompensationCtrlRegister = hpcc.dc[0].Read('AuroraClockCompensationCtrlRegister').Pack()
        hpcc.Log('info', 'Clock Compensation Status register at HPCC-DC slice :0 after CC Enable: {}'.format(
            hex(AuroraClockCompensationCtrlRegister)))

    def disable_dc_fpga_clock_compensation(hpcc):
        hpcc.dc[0].Write('AuroraClockCompensationCtrlRegister', 0x0)
        AuroraClockCompensationCtrlRegister = hpcc.dc[0].Read('AuroraClockCompensationCtrlRegister').Pack()
        hpcc.Log('info', 'Clock Compensation Status register at HPCC-DC slice :0 after CC disable: {}'.format(
            hex(AuroraClockCompensationCtrlRegister)))

    def daughter_board_type(self):
        """HPCC1 or HPCC2"""
        blt_part_number = self.acboardblt.PartNumberCurrent.strip()
        try:
            part_number = blt_part_number.split('-')[0]
        except ValueError:
            self.Log('error', 'Part number with wrong format ({}) for DB in slot {}'.format(blt_part_number, self.slot))
        try:
            board_type = {'AAG70252': 'HPCC1', 'AAJ49483': 'HPCC2'}[part_number]
        except KeyError:
            board_type = 'Unknown'
            self.Log('error', f'Unknown daughter board part number: {part_number}')
        return board_type

    def daughter_board_fab(self):
        blt_part_number = self.acboardblt.PartNumberCurrent.strip()
        try:
            _, revision = blt_part_number.split('-')
        except ValueError:
            self.Log('error', 'Part number with wrong format ({}) for DB in slot {}'.format(blt_part_number, self.slot))
        fab_number = int(revision) // 100
        return string.ascii_uppercase[fab_number - 1]

    def main_board_type(self):
        """HPCC1 or HPCC2"""
        blt_part_number = self.dcboardblt.PartNumberCurrent.strip()
        try:
            part_number = blt_part_number.split('-')[0]
        except ValueError:
            self.Log('error', 'Part number with wrong format ({}) for MB in slot {}'.format(blt_part_number, self.slot))
        try:
            board_type = {'AAH78904': 'HPCC1', 'AAJ49481': 'HPCC2'}[part_number]
        except KeyError:
            board_type = 'Unknown'
            self.Log('error', f'Unknown main board part number: {part_number}')
        return board_type

    def main_board_fab(self):
        blt_part_number = self.dcboardblt.PartNumberCurrent.strip()
        try:
            _, revision = blt_part_number.split('-')
        except ValueError:
            self.Log('error', 'Part number with wrong format ({}) for MB in slot {}'.format(blt_part_number, self.slot))
        fab_number = int(revision) // 100
        return string.ascii_uppercase[fab_number - 1]


class CompatibilityEntry():
    def __init__(self, part_number, fab, image_id, note=''):
        self.part_number = part_number
        self.fab = fab
        self.image_id = image_id
        self.note = note

    def __eq__(self, other):
        if self.part_number != other.part_number:
            return False
        if self.fab != other.fab:
            return False
        if self.image_id != other.image_id:
            return False
        return True


mainboard_compatibility_table = [
    # 15-bit trigger images
    CompatibilityEntry(part_number='HPCC1', fab='C', image_id=5, note='HPCC  DC 16-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='D', image_id=5, note='HPCC  DC 16-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='F', image_id=7, note='HPCC  DC 16-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='G', image_id=9, note='HPCC  DC 16-bit trigger'),
    CompatibilityEntry(part_number='HPCCC2', fab='A', image_id=7, note='HPCC2 DC 16-bit trigger'),

    # 32-bit trigger images
    CompatibilityEntry(part_number='HPCC1', fab='C', image_id=6, note='HPCC DC 32-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='D', image_id=6, note='HPCC DC 32-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='D', image_id=10, note='HPCC DC 32-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='F', image_id=8, note='HPCC DC 32-bit trigger'),
    CompatibilityEntry(part_number='HPCC1', fab='G', image_id=10, note='HPCC DC 32-bit trigger'),
    CompatibilityEntry(part_number='HPCC2', fab='A', image_id=8, note='HPCC2 DC 32-bit trigger')]

daughterboard_compatibility_table = [
    CompatibilityEntry(part_number='HPCC1', fab='D', image_id=2),
    CompatibilityEntry(part_number='HPCC1', fab='E', image_id=2),
    CompatibilityEntry(part_number='HPCC1', fab='F', image_id=4),
    CompatibilityEntry(part_number='HPCC1', fab='G', image_id=4),
    CompatibilityEntry(part_number='HPCC1', fab='H', image_id=2),
    CompatibilityEntry(part_number='HPCC2', fab='A', image_id=6)]


def print_compatibility_table(log, table):
    log('info', '  | {:^12}---{:^5}---{:^10}|-{:<30}|'.format('-' * 12, '-' * 5, '-' * 10, '-' * 30))
    log('info', '  | {:^12} | {:^5} | {:^10}| {:<30}|'.format('Part Number', 'Fab', 'Image Id', 'Note'))
    log('info', '  | {:^12}---{:^5}---{:^10}|-{:<30}|'.format('-' * 12, '-' * 5, '-' * 10, '-' * 30))
    for entry in table:
        log('info', '  | {:^12} | {:^5} | {:^10}| {:<30}|'.format(entry.part_number, entry.fab, entry.image_id, entry.note))
    log('info', '  | {:^12}---{:^5}---{:^10}| {:<30}|'.format('-'*12, '-'*5, '-'*10, '-'*30))
    

def load_ac_fpgas(hpcc):
    ac0Image = None
    ac1Image = None

    configs_attribute = f'{hpcc.daughter_board_type()}_AC_FPGA_FAB{hpcc.daughter_board_fab()}'
    image_path = getattr(hpccConfigs, configs_attribute)

    if not os.path.isdir(image_path):
        hpcc.Log('error', 'Directory "{}" does not exist'.format(image_path))

    # Search HPCC_AC_FPGA_PATH for HPCC images
    hpcc.Log('debug', 'Searching \'{}\' for FPGA images'.format(image_path))
    AC0_REGEX = re.compile('hpcc_ac_variant0_fpga0_(dimms_|mini_build_|mini_build_no_dimm_|power_virus_)?(32b_)?v\.\d+\.\d+\.\d+\.bin(\.gz)?')
    AC1_REGEX = re.compile('hpcc_ac_variant0_fpga1_(dimms_|mini_build_|mini_build_no_dimm_|power_virus_)?(32b_)?v\.\d+\.\d+\.\d+\.bin(\.gz)?')
    compressed_bin_files = [x for x in filter(os.path.isfile, glob.glob(os.path.join(image_path, '*.bin.gz')))]

    if not compressed_bin_files:
        hpcc.Log('error', 'Directory "{}" does not contain AC FPGA images'.format(image_path))

    for fn in compressed_bin_files:
        bfn = os.path.basename(fn)
        if AC0_REGEX.match(bfn):
            if ac0Image is None:
                ac0Image = (fn, bfn)
            else:
                if not(ac0Image[1] in bfn):
                    hpcc.Log('error', 'HPCC_AC_FPGA_PATH contains multiple *.bin.gz files: {}, {}, ...'.format(ac0Image[1], os.path.basename(fn)))
        if AC1_REGEX.match(bfn):
            if ac1Image is None:
                ac1Image = (fn, bfn)
            else:
                if not(ac1Image[1] in bfn):
                    hpcc.Log('error', 'HPCC_AC_FPGA_PATH contains multiple *.bin.gz files: {}, {}, ...'.format(ac1Image[1], os.path.basename(fn)))
    
    if ac0Image is None:
        hpcc.Log('error', 'No image found for AC FPGA0')

    if ac1Image is None:
        if not hpccConfigs.HPCC_SLICE0_ONLY:
            hpcc.Log('error', 'No image found for AC FPGA1')

    hpcc.ac0Image = ac0Image
    hpcc.ac1Image = ac1Image

    hardware_revision = get_ac_hardware_revision_from_filename(ac0Image[0])
    actual = CompatibilityEntry(part_number=hpcc.daughter_board_type(), fab=hpcc.daughter_board_fab(), image_id=hardware_revision)
    if actual in daughterboard_compatibility_table:
        hpcc.Log('info', 'Programming slot {} with images from {}'.format(hpcc.slot, image_path))
    else:
        hpcc.Log('info', 'HPCC Daughter Board / FPGA image compatibility table:')
        print_compatibility_table(hpcc.Log, daughterboard_compatibility_table)
        hpcc.Log('error', f'{hpcc.daughter_board_type()} fab '
                          f'{hpcc.daughter_board_fab()} in slot {hpcc.slot}'
                          f' is not compatible with AC FPGA image type'
                          f' ({hardware_revision})')

    if hpccConfigs.HPCC_AC_FPGA_LOAD:
        hpcc.Log('info', 'Loading AC image')
        hpcc.Log('debug', 'Previous AC Image slot {} slice{}: {}'.format(hpcc.slot, 0, hpcc.ac[0].GetFpgaVersion()))
        hpcc.Log('info', 'Flashing HPCC AC0 FPGA with image from \'{}\''.format(ac0Image[0]))
        hpcc.ac[0].FpgaLoad(ac0Image[0])
    else:
        hpcc.Log('warning', f'{hpcc.ac[0].name()} FPGA was not loaded due to configs setting')

    hpcc.Log('info', 'Current AC Image slot {} slice{} version: {}'.format(hpcc.slot, 0, hpcc.ac[0].GetFpgaVersion()))
    hpcc.Log('info', 'Current AC Image slot {} slice{} node: {}'.format(hpcc.slot, 0, hpcc.ac[0].get_source_control_node()))
    
    if hpcc.is_hpcc2:
        hpcc.Log('info', 'Initializing LMK04808 so HPCC2 slice 1 FPGA can be configured')
        hpcc.ac[0].lmk04808S0.Initialize(fineDelayDivider=0, hpcc2=True)

    if hpccConfigs.HPCC_SLICE0_ONLY is True:
        hpcc.Log('info', 'Skipping load of AC image in slice 1 (hpccConfigs.HPCC_SLICE0_ONLY = {})'.format(
            hpccConfigs.HPCC_SLICE0_ONLY))
    else:
        hpcc.Log('debug', 'Previous AC Image slot {} slice{}: {}'.format(hpcc.slot, 1, hpcc.ac[1].GetFpgaVersion()))
        if hpccConfigs.HPCC_AC_FPGA_LOAD:
            hpcc.Log('info', 'Flashing HPCC AC1 FPGA with image from \'{}\''.format(ac1Image[0]))
            retries = 5
            for attempt in range(retries):
                try:
                    hpcc.ac[1].FpgaLoad(ac1Image[0])
                    break
                except RuntimeError as e:
                    hpcc.Log('warning',
                             f'HPCC_AC_1: Failed to load FPGA (attempt '
                             f'{attempt+1} of {retries}). {e}')
            else:
                raise Hpcc.InstrumentError(
                    f'{hpcc.ac[1].name()}: Could not load FPGA ({retries} '
                    f'attempts)')
        else:
            hpcc.Log('warning', f'{hpcc.ac[1].name()} FPGA was not loaded due to configs setting')

        hpcc.Log('info', 'Current AC Image slot {} slice{}: {}'.format(hpcc.slot, 1, hpcc.ac[1].GetFpgaVersion()))


def get_ac_hardware_revision_from_filename(filename):
    just_the_filename = os.path.split(filename)[-1]
    filename_split = just_the_filename.split('.')
    index = filename_split.index('bin') - 1
    return int(filename_split[index])
