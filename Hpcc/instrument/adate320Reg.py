################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: adate320Reg.ds
#-------------------------------------------------------------------------------
#     Purpose: registers for AdAte320
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 05/07/15
#       Group: HDMT FPGA Validation
################################################################################
  
import ctypes


class AdateReg(ctypes.LittleEndianStructure):
    def Pack(self):
        return int.from_bytes(self, byteorder='little')


class DrvControl(AdateReg):
    ADDR = 0x019
    _fields_ = [('enable',        ctypes.c_uint, 1),
                ('force',         ctypes.c_uint, 1),
                ('forceState',    ctypes.c_uint, 2),
                ('vtHiZ',         ctypes.c_uint, 1),
                ('reserved',      ctypes.c_uint, 4),
                ('clc',           ctypes.c_uint, 3),
                ('rcvMux',        ctypes.c_uint, 2),
                ('datMux',        ctypes.c_uint, 2)]

class CmpControl(AdateReg):
    ADDR = 0x01A
    _fields_ = [('dmcEnable',     ctypes.c_uint, 1),
                ('dmcClc',        ctypes.c_uint, 3),
                ('nwcClc',        ctypes.c_uint, 3),
                ('reserved',      ctypes.c_uint, 1),
                ('dmcHyst',       ctypes.c_uint, 4),
                ('nwcHyst',       ctypes.c_uint, 4)]
    
class LoadControl(AdateReg):
    ADDR = 0x01B
    _fields_ = [('enable',     ctypes.c_uint, 1),
                ('force',      ctypes.c_uint, 1),
                ('reserved',   ctypes.c_uint, 14)]

class PpmuControl(AdateReg):
    ADDR = 0x01C
    _fields_ = [('enable',        ctypes.c_uint, 1),
                ('standby',       ctypes.c_uint, 1),
                ('rangeSelect',   ctypes.c_uint, 3),
                ('forceMode',     ctypes.c_uint, 1),
                ('measureMode',   ctypes.c_uint, 1),
                ('inputSelect',   ctypes.c_uint, 2),
                ('sensePath',     ctypes.c_uint, 1),
                ('clampEnable',   ctypes.c_uint, 1),
                ('reserved',      ctypes.c_uint, 2),
                ('measureEnable', ctypes.c_uint, 1),
                ('measureSelect', ctypes.c_uint, 2)]
    
class DacControl(AdateReg):
    ADDR = 0x011
    _fields_ = [('calEnable',   ctypes.c_uint, 1),
                ('loadMode',    ctypes.c_uint, 1),
                ('load',        ctypes.c_uint, 1),
                ('reserved',    ctypes.c_uint, 13)]
    
NOP             = 0x000 # NOP
VIHLevel        = 0x001 # VIH (Driver high level)
VITVCOMLevel    = 0x002 # VIT (Driver term level) / VCOM (Load commutation voltage)
VILLevel        = 0x003 # VIL (Driver low level)
VCHLevel        = 0x004 # VCH (Reflection Clamp high level)
VCLLevel        = 0x005 # VCL (Reflection Clamp low level)
VOHLevel        = 0x006 # VOH (Comparator high level)
VOLLevel        = 0x007 # VOL (Comparator low level)
VIOHLevel       = 0x008 # VIOH (Load IOH level)
VIOLLevel       = 0x009 # VIOL (Load IOL level)
PPMULevel       = 0x00A # PPMU (PPMU VIN FV level / PPMU VIN FI level)
PCHLevel        = 0x00B # PCH (PPMU Current Clamp FV high level / PPMU Voltage Clamp FI high level)
PCLLevel        = 0x00C # PCL (PPMU Current Clamp FV low level / PPMU Voltage Clamp FV low level)
POHLevel        = 0x00D # POH (PPMU Go/Nogo MV high level / PPMU Go/Nogo MI high level)
POLLevel        = 0x00E # POL (PPMU Go/Nogo MV low level / PPMU Go/Nogo MI low level)
OVDLLevel       = 0x08F # OVDL (CH:01b, Overvoltage Detect low level)
OVDHLevel       = 0x10F # OVDH (CH:10b, Overvoltage Detect high level)

# DACControl      = 0x011 # DAC Control Register
SPIControl      = 0x092 # SPI Control Register (CH:01b)
# DRVControl      = 0x019 # DRV Control Register
# CMPControl      = 0x01A # CMP Control Register
# LOADControl     = 0x01B # LOAD Control Register
# PPMUControl     = 0x01C # PPMU Control Register
AlarmMask       = 0x01D # ALARM Mask Register
AlarmState      = 0x01E # ALARM State Register
ProdSerialCode  = 0x01F # Product Serialization Code Register

VIHGain         = 0x021 # VIH Gain (m-coefficient)
VITGain         = 0x022 # VIT Gain
VCOMGain        = 0x042 # VCOM Gain
VILGain         = 0x023 # VIL Gain
VCHGain         = 0x024 # VCH Gain
VCLGain         = 0x025 # VCL Gain
VOHGain         = 0x026 # VOH Gain (Normal Window Comparator)
VOHDiffGain     = 0x046 # VOH Gain (Differential Mode Comparator)
VOLGain         = 0x027 # VOL Gain (Normal Window Comparator)
VOLDiffGain     = 0x047 # VOL Gain (Differential Mode Comparator)
VIOHGain        = 0x028 # VIOH Gain
VIOLGain        = 0x029 # VIOL Gain
PPMUFVGain      = 0x02A # PPMU Gain (PPMU Force-V)
PPMUFIAGain     = 0x04A # PPMU FI Range-A Gain
PPMUFIBGain     = 0x04B # PPMU FI Range-B Gain
PPMUFICGain     = 0x04C # PPMU FI Range-C Gain
PPMUFIDGain     = 0x04D # PPMU FI Range-D Gain
PPMUFIEGain     = 0x04E # PPMU FI Range-E Gain
PCHVGain        = 0x02B # PCH FI Voltage Clamp Gain
PCHIGain        = 0x044 # PCH FV Current Clamp Gain
PCLVGain        = 0x02C # PCL FI Voltage Clamp Gain
PCLIGain        = 0x045 # PCL FV Current Clamp Gain
POHMVGain       = 0x02D # POH Gain (PPMU Go/Nogo MV)
POHMIAGain      = 0x061 # POH Gain (PPMU Go/Nogo MI Range-A)
POHMIBGain      = 0x062 # POH Gain (PPMU Go/Nogo MI Range-B)
POHMICGain      = 0x063 # POH Gain (PPMU Go/Nogo MI Range-C)
POHMIDGain      = 0x064 # POH Gain (PPMU Go/Nogo MI Range-D)
POHMIEGain      = 0x065 # POH Gain (PPMU Go/Nogo MI Range-E)
POLMVGain       = 0x02E # POL Gain (PPMU Go/Nogo MV)
POLMIAGain      = 0x066 # POL Gain (PPMU Go/Nogo MI Range-A)
POLMIBGain      = 0x067 # POL Gain (PPMU Go/Nogo MI Range-B)
POLMICGain      = 0x068 # POL Gain (PPMU Go/Nogo MI Range-C)
POLMIDGain      = 0x069 # POL Gain (PPMU Go/Nogo MI Range-D)
POLMIEGain      = 0x06A # POL Gain (PPMU Go/Nogo MI Range-E)
OVDLGain        = 0x0AF # OVDL Gain (CH:01b)
OVDHGain        = 0x12F # OVDH Gain (CH:10b)

VIHOffset       = 0x031 # VIH Offset (c-coefficient)
VITOffset       = 0x032 # VIT Offset
VCOMOffset      = 0x052 # VCOM Offset
VILOffset       = 0x033 # VIL Offset
VCHOffset       = 0x034 # VCH Offset
VCLOffset       = 0x035 # VCL Offset
VOHOffset       = 0x036 # VOH Offset (Normal Window Comparator)
VOHDiffOffset   = 0x056 # VOH Offset (Differential Mode Comparator)
VOLOffset       = 0x037 # VOL Offset (Normal Window Comparator)
VOLDiffOffset   = 0x057 # VOL Offset (Differential Mode Comparator)
VIOHOffset      = 0x038 # VIOH Offset
VIOLOffset      = 0x039 # VIOL Offset
PPMUFVOffset    = 0x03A # PPMU FV Offset
PPMUFIOffset    = 0x05A # PPMU FI Offset
PCHVOffset      = 0x03B # PCH FI Voltage Clamp Offset
PCHIOffset      = 0x054 # PCH FV Current Clamp Offset
PCLVOffset      = 0x03C # PCL FI Voltage Clamp Offset
PCLIOffset      = 0x055 # PCL FV Current Clamp Offset
POHMVOffset     = 0x03D # POH Offset (PPMU Go/Nogo MV)
POHMIOffset     = 0x05D # POH Offset (PPMU Go/Nogo MI)
POLMVOffset     = 0x03E # POL Offset (PPMU Go/Nogo MV)
POLMIOffset     = 0x05E # POL Offset (PPMU Go/Nogo MI)
OVDLOffset      = 0x0BF # OVDL Offset (CH:01b)
OVDHOffset      = 0x13F # OVDH Offset (CH:10b)

Register7D      = 0x07D # ADI Register 0x7D - Used for special initialization, to help determine if >= R1.3 or earlier revisions.
Register7E      = 0x07E # ADI Register 0x7E - Used for special initialization
Register7F      = 0x07F # ADI Register 0x7F - Used for special initialization