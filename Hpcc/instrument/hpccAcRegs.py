################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

import Common.register as register


def get_register_types_sorted_by_name():
    return sorted(get_register_types(), key=lambda x: x.__name__)

def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HpccRegister):
                if attribute is not HpccRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types
    
    
class HpccRegister(register.Register):
    BASEMUL = 16
    
    def __str__(self):
        l = []
        l.append(type(self).__name__)
        try:
            l.append('ADDR=0x{:04X}'.format(self.ADDR))
        except AttributeError:
            pass
        try:
            l.append('REGCOUNT={}'.format(self.REGCOUNT))
        except AttributeError:
            pass
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)
    
    
class HpccStruct(register.Register):
    ADDR = None
    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)

    
class Version(HpccRegister):
    ADDR = 0x8100
    _fields_ = [('HwVersion', ctypes.c_uint, 8),
                ('MinorVersion', ctypes.c_uint, 8),
                ('MajorVersion', ctypes.c_uint, 16)]
    
    
class PatternControl(HpccRegister):
    ADDR = 0x8230
    CHANNEL_LINKING_2X = 0
    CHANNEL_LINKING_4X = 1
    CHANNEL_LINKING_8X = 2
    _fields_ = [('EnableDisableOutputs', ctypes.c_uint, 1),
                ('PreStagePattern', ctypes.c_uint, 1),
                ('Reserved1', ctypes.c_uint, 1),
                ('SubCycleCount', ctypes.c_uint, 4),
                ('StartOnNextRCSyncPulse', ctypes.c_uint, 1),
                ('AbortPattern', ctypes.c_uint, 1),
                ('StickyErrorDomainMask', ctypes.c_uint, 15),
                ('PRBSEnable', ctypes.c_uint, 1),
                ('ToggleEnable', ctypes.c_uint, 1),
                ('LinkMode', ctypes.c_uint, 2),
                ('VectorProcessorStarted', ctypes.c_uint, 1),
                ('PerVectorControlStarted', ctypes.c_uint, 1),
                ('PinRingAcknowledge', ctypes.c_uint, 1),
                ('SyncStartBypass', ctypes.c_uint, 1)]


class FirstFailAddress(HpccRegister):
    BAR = 0
    ADDR = 0x8840
    _fields_ = [('Address', ctypes.c_uint, 32)]

    
class CaptureAddressUnderrun(HpccRegister):
    BAR = 0
    ADDR = 0x8880
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ECCEventAddress(HpccRegister):
    BAR = 0
    ADDR = 0x8130
    _fields_ = [('ECCEventAddress', ctypes.c_uint, 32)]


class ECCEventCounts(HpccRegister):
    ADDR = 0x8140
    _fields_ = [('ECCCorrectedErrorCount', ctypes.c_uint, 16),
                ('ECCUnCorrectedErrorCount', ctypes.c_uint, 16)]


class SingleChannelDRAMAccessAcceleratorControl(HpccRegister):
    ADDR = 0x8150
    _fields_ = [('SingleChannelModifyEnable', ctypes.c_uint, 1),
                ('SingleChannelModifyNumber', ctypes.c_uint, 6),
                ('SingleChannelCaptureEnable', ctypes.c_uint, 1),
                ('SingleChannelCaptureNumber', ctypes.c_uint, 6),
                ('Length', ctypes.c_uint, 18)]


class TriggerControl(HpccRegister):
    ADDR = 0x8180
    _fields_ = [('CardType', ctypes.c_uint, 4),
                ('DomainID', ctypes.c_uint, 4),
                ('DomainMaster', ctypes.c_uint, 1),
                ('SynchModifier', ctypes.c_uint, 4),
                ('CardTypeExtended', ctypes.c_uint, 2),
                ('DutID', ctypes.c_uint, 6),
                ('Unused', ctypes.c_uint, 11)]


class TriggerEvent(HpccRegister):
    ADDR = 0x8190
    _fields_ = [('TriggerType', ctypes.c_uint, 4),
                ('SyncCount', ctypes.c_uint, 4),
                ('DomainID', ctypes.c_uint, 4),
                ('Reserved', ctypes.c_uint, 8),
                ('DutID', ctypes.c_uint, 6),
                ('CardType', ctypes.c_uint, 6)]

class ChannelFailStatusLower(HpccRegister):
    ADDR = 0x8200
    _fields_ = [('ChannelFailStatusLower', ctypes.c_uint, 32)]

class ChannelFailStatusUpper(HpccRegister):
    ADDR = 0x8210
    _fields_ = [('ChannelFailStatusUpper', ctypes.c_uint, 32)]

class AD9914Control(HpccRegister):
    ADDR = 0x8240
    _fields_ = [('Address', ctypes.c_uint, 7),
                ('RdEnable', ctypes.c_uint, 1),
                ('EnableAction', ctypes.c_uint, 1),
                ('ReadValueReturned', ctypes.c_uint, 1),
                ('AD9914Select', ctypes.c_uint, 1),
                ('AD9914SyncInQDRPhaseDelay', ctypes.c_uint, 2),
                ('AD9914SyncInFineDelay', ctypes.c_uint, 6),
                ('AD9914IOUpdateQDRPhaseDelay', ctypes.c_uint, 2),
                ('AD9914IOUpdateFineDelay', ctypes.c_uint, 6),
                ('Ad9914AccessComplete', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 4)]


class AD9914WRData(HpccRegister):
    ADDR = 0x8250
    _fields_ = [('WRData', ctypes.c_uint, 32)]


class AD9914RDData(HpccRegister):
    ADDR = 0x8260
    _fields_ = [('RDData', ctypes.c_uint, 32)]


class AD9914Control_HP(HpccRegister):
    ADDR = 0x82D0
    _fields_ = [('Address', ctypes.c_uint, 7),
                ('RdEnable', ctypes.c_uint, 1),
                ('EnableAction', ctypes.c_uint, 1),
                ('ReadValueReturned', ctypes.c_uint, 1),
                ('AD9914Select', ctypes.c_uint, 1),
                ('AD9914SyncInQDRPhaseDelay', ctypes.c_uint, 2),
                ('AD9914SyncInFineDelay', ctypes.c_uint, 6),
                ('AD9914IOUpdateQDRPhaseDelay', ctypes.c_uint, 2),
                ('AD9914IOUpdateFineDelay', ctypes.c_uint, 6),
                ('Ad9914AccessComplete', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 4)]


class AD9914WRData_HP(HpccRegister):
    ADDR = 0x82E0
    _fields_ = [('WRData', ctypes.c_uint, 32)]


class AD9914RDData_HP(HpccRegister):
    ADDR = 0x82F0
    _fields_ = [('RDData', ctypes.c_uint, 32)]


class LMK4808Control(HpccRegister):
    ADDR = 0x8270
    _fields_ = [('Address', ctypes.c_uint, 5),
                ('RdEnable', ctypes.c_uint, 1),
                ('Reserved1', ctypes.c_uint, 1),
                ('ReadValueReturned', ctypes.c_uint, 1),
                ('Reserved2', ctypes.c_uint, 24)]


class LMK4808WRData(HpccRegister):
    ADDR = 0x8280
    _fields_ = [('WRData', ctypes.c_uint, 27),
                ('Reserved', ctypes.c_uint, 5)]


class LMK4808RDData(HpccRegister):
    ADDR = 0x8290
    _fields_ = [('RDData', ctypes.c_uint, 27),
                ('Reserved', ctypes.c_uint, 5)]


class LMK01000Control(HpccRegister):
    ADDR = 0x8400
    _fields_ = [('Address', ctypes.c_uint, 4),
                ('RdEnable', ctypes.c_uint, 1),
                ('GoeDisable', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 26)]


class LMK01000WRData(HpccRegister):
    ADDR = 0x8410
    _fields_ = [('WRData', ctypes.c_uint, 28),
                ('Reserved', ctypes.c_uint, 4)]


class LMK01000Control_HP(HpccRegister):
    ADDR = 0x8460
    _fields_ = [('Address', ctypes.c_uint, 4),
                ('RdEnable', ctypes.c_uint, 1),
                ('GoeDisable', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 26)]


class LMK01000WRData_HP(HpccRegister):
    ADDR = 0x8470
    _fields_ = [('WRData', ctypes.c_uint, 28),
                ('Reserved', ctypes.c_uint, 4)]


class PinInputStatusLower(HpccRegister):
    ADDR = 0x84E0
    _fields_ = [('PinInputStatusLower', ctypes.c_uint, 32)]


class PinInputStatusUpper(HpccRegister):
    ADDR = 0x84F0
    _fields_ = [('PinInputStatusUpper', ctypes.c_uint, 32)]


class SelectableFailMask0Lower(HpccRegister):
    ADDR = 0x8300
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask0Upper(HpccRegister):
    ADDR = 0x8310
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask1Lower(HpccRegister):
    ADDR = 0x8320
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask1Upper(HpccRegister):
    ADDR = 0x8330
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask2Lower(HpccRegister):
    ADDR = 0x8340
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask2Upper(HpccRegister):
    ADDR = 0x8350
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask3Lower(HpccRegister):
    ADDR = 0x8360
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask3Upper(HpccRegister):
    ADDR = 0x8370
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask4Lower(HpccRegister):
    ADDR = 0x8380
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask4Upper(HpccRegister):
    ADDR = 0x8390
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask5Lower(HpccRegister):
    ADDR = 0x83A0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask5Upper(HpccRegister):
    ADDR = 0x83B0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask6Lower(HpccRegister):
    ADDR = 0x83C0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask6Upper(HpccRegister):
    ADDR = 0x83D0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask7Lower(HpccRegister):
    ADDR = 0x83E0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SelectableFailMask7Upper(HpccRegister):
    ADDR = 0x83F0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TotalCaptureCount(HpccRegister):
    ADDR = 0x8440
    _fields_ = [('CaptureCount', ctypes.c_uint, 32)]


class ChannelFailCount(HpccRegister):
    ADDR = 0xC000
    REGCOUNT = 56
    _fields_ = [('ChannelFailCaptureCount', ctypes.c_uint, 32)]


class BuildInfo1(HpccRegister):
    ADDR = 0x82B0
    _fields_ = [('LowerMqIdDword', ctypes.c_uint, 32)]


class BuildInfo2(HpccRegister):
    ADDR = 0x82C0
    _fields_ = [('UpperMqIdDword', ctypes.c_uint, 16),
                ('Reserved', ctypes.c_uint, 14),
                ('FpgaNumber', ctypes.c_uint, 1),
                ('MercurialClean', ctypes.c_uint, 1)]

                
class SyncStartEdgeResult(HpccRegister):
    ADDR = 0x8530
    _fields_ = [('Count', ctypes.c_uint, 8),
                ('WidePulseDetected', ctypes.c_uint, 1),
                ('HpCount', ctypes.c_uint, 8),
                ('HpWidePulseDetected', ctypes.c_uint, 1),
                ('Reserved1', ctypes.c_uint, 10),
                ('ClockNoiseAlarm', ctypes.c_uint, 1),
                ('StopState', ctypes.c_uint, 3)]

# generate the 32 central domain register types
module_dict = globals()
for i in range(32):
    type_name = 'CentralDomainRegister{}'.format(i)
    module_dict[type_name] = type(type_name,
                                  (HpccRegister,),
                                  {'ADDR': 0x8600 + i*16, '_fields_': [('Data', ctypes.c_uint, 32)]})

class CentralDomainRegisterFlags(HpccRegister):
    ADDR = 0x8800
    _fields_ = [('Reserved', ctypes.c_uint,16),
                ('AlwaysSet', ctypes.c_uint,1),
                ('LocalStickyError', ctypes.c_uint,1),
                ('DomainStickyError', ctypes.c_uint,1),
                ('GlobalStickyError', ctypes.c_uint,1),
                ('WireOr', ctypes.c_uint,1),
                ('LoadActive', ctypes.c_uint,1),
                ('SoftwareTrigger', ctypes.c_uint,1),
                ('DomainTrigger', ctypes.c_uint,1),
                ('RCTrigger', ctypes.c_uint,1),
                ('ALUZero', ctypes.c_uint,1),
                ('ALUCarry', ctypes.c_uint,1),
                ('ALUOverflow', ctypes.c_uint,1),
                ('ALUSign', ctypes.c_uint,1),
                ('Internal1', ctypes.c_uint,1),
                ('Internal2', ctypes.c_uint,1),
                ('Internal3', ctypes.c_uint,1)]


class ErrorAddress(HpccRegister):
    ADDR = 0x8810
    _fields_ = [('Address', ctypes.c_uint, 32)]
    
    
class PinDdrRefreshControl(HpccRegister):
    '''Control register for pin domain DDR refresh control.
    
    Sets DDR refresh interval in pin domain clocks, sets DDR refresh
    duration in pin domain clocks
    '''
    ADDR = 0x8850
    _fields_ = [('RefreshInterval', ctypes.c_uint, 14),
                ('RefreshDuration', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 10)]
    
    
class PinDdrRequestControl(HpccRegister):
    '''Control register for pin domain control of DDR requests.
    
    Sets DDR request interval in pin domain clocks, sets DDR request
    duration in pin domain clocks
    '''
    ADDR = 0x8860
    _fields_ = [('RequestInterval', ctypes.c_uint, 8),
                ('RequestDuration', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 16)]


class PinDdrStatus(HpccRegister):
    '''Status register for pin domain control of DDR.
    
    Reports smallest observed gap between DDR reads, reports number of
    pattern fetches seen
    '''
    ADDR = 0x8870
    _fields_ = [('PatternFetchCount', ctypes.c_uint, 8),
                ('ReadDataGap', ctypes.c_uint, 7),
                ('Reserved', ctypes.c_uint, 17)]

class AllTrigSentCount(HpccRegister):
    ADDR = 0x8950
    _fields_ = [('count', ctypes.c_uint, 32)]

class PerChannelControlConfig1(HpccRegister):
    ADDR = 0x9000
    REGCOUNT = 56
    _fields_ = [('OutputQdrPhaseSelect', ctypes.c_uint, 2),
                ('OutputSubCycleDelay', ctypes.c_uint, 8),
                ('OutputDynamicPhaseDelay', ctypes.c_uint, 4),
                ('EnableQdrPhaseSelect', ctypes.c_uint, 2),
                ('EnableSubCycleDelay', ctypes.c_uint, 8),
                ('EnableDynamicPhaseDelay', ctypes.c_uint, 4),
                ('PinModeSelect', ctypes.c_uint, 1),
                ('EnabledClockModeOn', ctypes.c_uint, 1),
                ('EnabledClockDifferentialCompMode', ctypes.c_uint, 1),
                ('Reserve', ctypes.c_uint, 1)]


class PerChannelControlConfig2(HpccRegister):
    ADDR = 0xA000
    REGCOUNT = 56
    _fields_ = [('EnabledClockTesterCyclesHigh', ctypes.c_uint, 7),
                ('EnabledClockTesterCyclesLow', ctypes.c_uint, 7),
                ('LogicCombinationSelect', ctypes.c_uint, 2),
                ('EcDirectDriveEventSelect', ctypes.c_uint, 2),
                ('EcREventSelect', ctypes.c_uint, 2),
                ('OutputFineDelay', ctypes.c_uint, 6),
                ('OutputAlternateFineDelay', ctypes.c_uint, 6)]


class PerChannelControlConfig3(HpccRegister):
    ADDR = 0xB000
    REGCOUNT = 56
    _fields_ = [('InputSubCycleDelay', ctypes.c_uint, 10),
                ('InputDynamicPhaseDelay', ctypes.c_uint, 4),
                ('Reserved', ctypes.c_uint, 1),
                ('EnableHighPurityClock', ctypes.c_uint, 1),
                ('FrequencyCounterMode', ctypes.c_uint, 2),
                ('EnableFineDelay', ctypes.c_uint, 6),
                ('InputFineDelay', ctypes.c_uint, 6),
                ('DriveCompareSwitchResponse', ctypes.c_uint, 2)]


class PerChannelControlConfig4(HpccRegister):
    ADDR = 0xE000
    REGCOUNT = 56
    _fields_ = [('OutputTesterCycleDelay', ctypes.c_uint, 10),
                ('EnableTesterCycleDelay', ctypes.c_uint, 10),
                ('InputTesterCycleDelay', ctypes.c_uint, 10),
                ('OutputDuplicateDelay', ctypes.c_uint, 1),
                ('OutputDuplicateAdvance', ctypes.c_uint, 1)]


class ChannelFrequencyCounts(HpccRegister):
    ADDR = 0xD000
    REGCOUNT = 56
    _fields_ = [('Count', ctypes.c_uint, 32)]


class DPinCompareEdge(HpccStruct):
    _fields_ = [('TesterCycleDelay', ctypes.c_uint, 10),
                ('FineDelay', ctypes.c_uint, 6),
                ('CompareSubCycleDelay', ctypes.c_uint, 14),
                ('EdgeDelay', ctypes.c_uint, 1),
                ('EdgeAdvance', ctypes.c_uint, 1)]


class DPinDriveEdge(HpccStruct):
    _fields_ = [('TesterCycleDelay', ctypes.c_uint, 10),
                ('FineDelay', ctypes.c_uint, 6),
                ('QdrDelay', ctypes.c_uint, 2),
                ('DriveSubCycleDelay', ctypes.c_uint, 12),
                ('EdgeDelay', ctypes.c_uint, 1),
                ('EdgeAdvance', ctypes.c_uint, 1)]


class Capability0(HpccRegister):
    ADDR = 0x85F0
    _fields_ = [('SyncSupported', ctypes.c_uint, 1),
                ('HP_clock_enabled', ctypes.c_uint, 1),
                ('DelayStackConfiguration', ctypes.c_uint, 4),
                ('TimingVersion', ctypes.c_uint, 4),
                ('DriveOutDelay', ctypes.c_uint, 8),
                ('CompareInDelay', ctypes.c_uint, 8),
                ('FineDelayClockSpeed', ctypes.c_uint, 3),
                ('Triggers32b', ctypes.c_uint, 1),
                ('TDRCountVersion', ctypes.c_uint, 1),
                ('MoreCapabilities', ctypes.c_uint, 1)]


class Capability1(HpccRegister):
    ADDR = 0x88E0
    _fields_ = [('PinClkControlledDDR', ctypes.c_uint, 1),
                ('RequestDurationConstant', ctypes.c_uint, 8),
                ('RequestIntervalConstant', ctypes.c_uint, 8),
                ('ClearDSEOnGSEControl', ctypes.c_uint, 1),
                ('AD9914NoPLLPhaseDetect', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 13)]


class ResetsAndStatus(HpccRegister):
    ADDR = 0x8110
    _fields_ = [('ResetDDR3IpBlocks', ctypes.c_uint, 1),
                ('ResetCaptureMemory', ctypes.c_uint, 1),
                ('DDR3PLLLock', ctypes.c_uint, 1),
                ('CaptureBufferOverflow', ctypes.c_uint, 1),
                ('PatternRunning', ctypes.c_uint, 1),
                ('DMAReset', ctypes.c_uint, 1),
                ('Lmk04808Sync', ctypes.c_uint, 1),
                ('Reserved1', ctypes.c_uint, 1),
                ('Ad9914Reset', ctypes.c_uint, 1),
                ('PinDomainReset', ctypes.c_uint, 1),
                ('DDR3ControllerInitialized', ctypes.c_uint, 4),
                ('PLLLocks', ctypes.c_uint, 4),
                ('Reserved2', ctypes.c_uint, 1),
                ('Lmk04808StatusLd', ctypes.c_uint, 1),
                ('Lmk04808StatusClkIn0', ctypes.c_uint, 1),
                ('Lmk04808StatusClkIn1', ctypes.c_uint, 1),
                ('AD9914Sync', ctypes.c_uint, 1),
                ('AD9914SyncHPClock', ctypes.c_uint, 1),
                ('ECCCountReset', ctypes.c_uint, 1),
                ('Reserved3', ctypes.c_uint, 2),
                ('ACFPGAIdentifier', ctypes.c_uint, 1),
                ('SlotNumber', ctypes.c_uint, 4)]


class TotalFailCount(HpccRegister):
    ADDR = 0x8220
    _fields_ = [('TotalFailCount', ctypes.c_uint, 32)]



class PatternEndStatus(HpccRegister):
    ADDR = 0x84C0
    _fields_= [('Status', ctypes.c_uint,32)]

class CaptureControl(HpccRegister):
    ADDR = 0x81A0
    FAIL_STATUS = 0
    PIN_STATE = 1
    PIN_DIR = 2
    MASKED_FAIL = 3
    _fields_= [('StorageCaptureType', ctypes.c_uint,2),
               ('CaptureFails', ctypes.c_uint,1),
               ('CaptureCTVs', ctypes.c_uint,1),
               ('CaptureAll', ctypes.c_uint,1),
               ('DramOverflowAction', ctypes.c_uint,1),
               ('CaptureBufferStatus', ctypes.c_uint,1),
               ('CaptureIndexMode', ctypes.c_uint,1),
               ('StopOnMaxFailCaptureCountHit', ctypes.c_uint,1),
               ('CaptureVectorData', ctypes.c_uint,1),
               ('FlushCapture', ctypes.c_uint,1),
               ('CaptureCTVAfterMaxFail', ctypes.c_uint,1),
               ('Reserved', ctypes.c_uint,20)]




class AlarmControl(HpccRegister):
    ADDR = 0x8120
    _fields_ = [('StopOnFirstFail', ctypes.c_uint,1),
                 ('DRAMCacheUnderRun', ctypes.c_uint,1),
                 ('PVCCacheUnderRun', ctypes.c_uint,1),
                 ('InstructionBufferOverflow', ctypes.c_uint,1),
                 ('CaptureBufferOverflow', ctypes.c_uint,1),
                 ('CaptureDRAMOverflow', ctypes.c_uint,1),
                 ('ECCError', ctypes.c_uint,1),
                 ('IllegalInstruction', ctypes.c_uint,1),
                 ('UserLog1Overflow', ctypes.c_uint,1),
                 ('UserLog2Overflow', ctypes.c_uint,1),
                 ('PatternInstanceOverflow', ctypes.c_uint,1),
                 ('ResetUserCycleCounterOverflow', ctypes.c_uint,1),
                 ('ResetCaptureMemoryOverflow', ctypes.c_uint,1),
                 ('PinStateWordOverflow', ctypes.c_uint,1),
                 ('EdgeCounterControlOverflow', ctypes.c_uint,1),
                 ('BlockFailsOverflow', ctypes.c_uint,1),
                 ('FailMaskOverflow', ctypes.c_uint,1),
                 ('CaptureOnOffOverflow', ctypes.c_uint,1),
                 ('ResetPinFailCountersOverflow', ctypes.c_uint,1),
                 ('ResetFailCountOverflow', ctypes.c_uint,1),
                 ('StackError', ctypes.c_uint,1),
                 ('MetaDataFilterMissedEnd', ctypes.c_uint,1),
                 ('ChannelLinkerMissedEnd', ctypes.c_uint,1),
                 ('LocalRepeaterMissedEnd', ctypes.c_uint,1),
                 ('PinDomainMissedEnd', ctypes.c_uint,1),
                 ('DelayStackMissedEnd', ctypes.c_uint,1),
                 ('CompareEngineMissedEnd', ctypes.c_uint,1),
                 ('DeepCaptureEngineMissedEnd', ctypes.c_uint,1),
                 ('MetaDataFilterTimeout', ctypes.c_uint,1),
                 ('PVCUnitTimeout', ctypes.c_uint,1),
                 ('PinDomainTimeout', ctypes.c_uint,1),
                 ('ExtendedAlarmsPresent', ctypes.c_uint,1)]


class AlarmControl1(HpccRegister):
    ADDR = 0x8820
    _fields_ = [('PinCacheOverflow', ctypes.c_uint,1),
                ('PinCacheUnderrun', ctypes.c_uint,1),
                ('CaptureEngineExpectOverflow', ctypes.c_uint,1),
                ('CaptureEngineExpectUnderrun', ctypes.c_uint,1),
                ('CaptureEngineDataOverflow', ctypes.c_uint,1),
                ('CaptureEngineDataUnderrun', ctypes.c_uint,1),
                ('FinalOutputFIFOTimeout', ctypes.c_uint,1),
                ('VectorProcessorStartFail', ctypes.c_uint,1),
                ('MetadataFilterStartFail', ctypes.c_uint,1),
                ('LocalRepeatStartFail', ctypes.c_uint,1),
                ('ChannelLinkerStartFail', ctypes.c_uint,1),
                ('PinDomainStartFail', ctypes.c_uint,1),
                ('CompareEngineStartFail', ctypes.c_uint,1),
                ('DeepCaptureStartFail', ctypes.c_uint,1),
                ('CompareHeaderFifoOverflow', ctypes.c_uint,1),
                ('NotEnoughDdrRefreshes', ctypes.c_uint,1),
                ('TooManyDdrRefreshes', ctypes.c_uint,1),
                ('TriggerWindowMissed', ctypes.c_uint,1),
                ('UnexpectedClockEvent', ctypes.c_uint,1),
                ('TriggerInFIFOUnderRun', ctypes.c_uint,1),
                ('TriggerInFIFOOverFlow', ctypes.c_uint,1),
                ('TriggerOutFIFOOverFlow', ctypes.c_uint,1),
                ('IncompleteSCEEventBeforeSCSEvent', ctypes.c_uint,1),
                ('SCEOverFlowAlarm', ctypes.c_uint,1),
                ('UnassignedAlarmBit56', ctypes.c_uint,1),
                ('UnassignedAlarmBit57', ctypes.c_uint,1),
                ('UnassignedAlarmBit58', ctypes.c_uint,1),
                ('UnassignedAlarmBit59', ctypes.c_uint,1),
                ('UnassignedAlarmBit60', ctypes.c_uint,1),
                ('UnassignedAlarmBit61', ctypes.c_uint,1),
                ('UnassignedAlarmBit62', ctypes.c_uint,1),
                ('MoreAlarmBits', ctypes.c_uint,1)]

noncritical_alarms = ['CaptureEngineDataOverflow',
                      'CaptureEngineDataUnderrun',
                      'CaptureEngineExpectOverflow',
                      'CaptureEngineExpectUnderrun',
                      'ChannelLinkerMissedEnd',
                      'ChannelLinkerStartFail',
                      'CompareEngineMissedEnd',
                      'CompareEngineStartFail',
                      'DeepCaptureEngineMissedEnd',
                      'DeepCaptureStartFail',
                      'DelayStackMissedEnd',
                      'FinalOutputFIFOTimeout',
                      'LocalRepeaterMissedEnd',
                      'LocalRepeatStartFail',
                      'ExtendedAlarmsPresent',
                      'MetaDataFilterMissedEnd',
                      'MetadataFilterStartFail',
                      'MetaDataFilterTimeout',
                      'PinCacheOverflow',
                      'PinCacheUnderrun',
                      'PinDomainMissedEnd',
                      'PinDomainStartFail',
                      'PinDomainTimeout',
                      'PVCUnitTimeout',
                      'VectorProcessorStartFail']

critical_alarms = [x for x in register.get_field_names(AlarmControl, AlarmControl1) if x not in noncritical_alarms]


class CorrectableECCCount(HpccRegister):
    ADDR = 0x8540
    REGCOUNT = 4
    _fields_ = [('Count', ctypes.c_uint, 32)]


class UncorrectableECCCount(HpccRegister):
    ADDR = 0x8580
    REGCOUNT = 4
    _fields_ = [('Count', ctypes.c_uint, 32)]


# Number of capture blocks captured.
class TotalCaptureBlockCount(HpccRegister):
    ADDR = 0x8450
    _fields_ = [('CaptureBlockCount', ctypes.c_uint,32)]


class IlaControl(HpccRegister):
    ADDR = 0x8480
    _fields_ = [('WordInSample', ctypes.c_uint,4),
                 ('Sample', ctypes.c_uint,15),
                 ('BypassWriteEnable', ctypes.c_uint,1),
                 ('BypassTrigger', ctypes.c_uint,1),
                 ('Select', ctypes.c_uint,3),
                 ('Reset', ctypes.c_uint,8)]

                 
class IlaStatus(HpccRegister):
    ADDR = 0x8490
    _fields_ = [('CaptureCount', ctypes.c_uint,15),
                ('Reserved', ctypes.c_uint,17)]

                
class IlaData(HpccRegister):
    ADDR = 0x84A0
    _fields_ = [('Data', ctypes.c_uint,32)]


class SoftwareScratchpad(HpccRegister):
    ADDR = 0x85D0
    _fields_ = [('MagicByte', ctypes.c_uint,8),
                ('Lmk04808Programmed', ctypes.c_uint,1),
                ('DdrControllerReset', ctypes.c_uint,1),
                ('MemoryInitialized', ctypes.c_uint,1),
                ('MemoryAddressLength', ctypes.c_uint,7),
                ('ForceFpgaFlash', ctypes.c_uint,1),
                ('PinDomainReset', ctypes.c_uint,1),
                ('WidePulseDetected', ctypes.c_uint,1),
                ('Reserved', ctypes.c_uint,11)]


# Lowest address to legally write capture data to. Capture begins at lowest address and grows up.
class CaptureBaseAddress(HpccRegister):
    ADDR = 0x81B0
    _fields_ = [('CaptureBaseAddress', ctypes.c_uint,32)]

    
# Window size of capture records allowed starting at the capture base address.
class CaptureLength(HpccRegister):
    ADDR = 0x8420
    _fields_ = [('CaptureLength', ctypes.c_uint,32)]

    
class MaxFailCaptureCount(HpccRegister):
    ADDR = 0x8430
    _fields_ = [('MaxFailCaptureCount', ctypes.c_uint,32)]


class MaxFailCount(HpccRegister):
    ADDR = 0x81D0
    _fields_ = [('MaxFailCountPerPattern', ctypes.c_uint,32)]


class MaxCaptureCount(HpccRegister):
    ADDR = 0x81C0
    _fields_ = [('MaxCaptureCount', ctypes.c_uint,32)]

 
class PatternStartAddress(HpccRegister):
    ADDR = 0x82A0
    _fields_ = [('PatternStartAddress', ctypes.c_uint,32)]
    
    
class Flags(HpccStruct):
    _fields_ = [('Reserved', ctypes.c_uint,16),
                ('AlwaysSet', ctypes.c_uint,1),
                ('LocalStickyError', ctypes.c_uint,1),
                ('DomainStickyError', ctypes.c_uint,1),
                ('GlobalStickyError', ctypes.c_uint,1),
                ('WireOr', ctypes.c_uint,1),
                ('LoadActive', ctypes.c_uint,1),
                ('SoftwareTrigger', ctypes.c_uint,1),
                ('DomainTrigger', ctypes.c_uint,1),
                ('RCTrigger', ctypes.c_uint,1),
                ('ALUZero', ctypes.c_uint,1),
                ('ALUCarry', ctypes.c_uint,1),
                ('ALUOverflow', ctypes.c_uint,1),
                ('ALUSign', ctypes.c_uint,1),
                ('Internal1', ctypes.c_uint,1),
                ('Internal2', ctypes.c_uint,1),
                ('Internal3', ctypes.c_uint,1)]


def print_registers():
    for RegisterType in get_register_types():
        print(RegisterType())

if __name__ == '__main__':
    print_registers()