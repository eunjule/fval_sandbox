################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: exceptions.py
#-------------------------------------------------------------------------------
#     Purpose: HPCC Exceptions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/22/15
#       Group: HDMT FPGA Validation
################################################################################

class PrestageIsAlreadySet(Exception):
    pass

class PrestageNeverFinished(Exception):
    pass

class PatternAbortTimeout(Exception):
    pass

class UncorrectableECCErrorAlarm(Exception):
    pass

