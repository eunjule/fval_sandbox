# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import re
import csv
import math
import gzip
import tempfile
import time
import shutil
from fractions import Fraction

from collections import defaultdict
from collections import namedtuple

from Common import hilmon as hil
from Common.register import create_field_dictionary
from Common.instruments.base import Instrument
import Hpcc.instrument.hpccAcRegs as ac_registers

from Common import configs, fval
from Common import globals
from .lmk01000 import Lmk01000
from .ad9914DomainClock import Ad9914DomainClock
from .accal import ACCal
from Hpcc.instrument import vcd
from Hpcc.hpcctb.assembler import PatternAssembler
from Tools import projectpaths
from ThirdParty.SASS.suppress_sensor import suppress_sensor
from .. import hpcc_configs as hpccConfigs

MINIMUM_VECTOR_DMA = 16
VECTOR_SIZE = 16
DMA_WRITE_ALIGNMENT = 16
CAPTURE_BLOCK_SIZE = 128

PICO_SECONDS_PER_SECOND = 1000000000000
EIGHT_THOUSAND_PICOSECONDS = 8000
MINIMUM_PULSE_WIDTH_VALUE = 600
DYNAMIC_PHASE_DELAY_BIAS = 8
DDR_MEMORY_BOUNDARY = 0x7FFFF800
MINIMUM_PRECISION = 10
INITIAL_DDR_MEMORY_SIZE_VALUE = 2048

HPCC_STRUCTS_DS = os.path.join(os.path.dirname(__file__), '..', 'hpcctb', 'structs.ds')
hpcc_structs = fval.LoadStructs(HPCC_STRUCTS_DS)

class HpccAc(Instrument):
    def __init__(self, hpcc, slot, slice):
        self.Log('debug', "instantiate an ac instance")
        super().__init__('')
        self.slot = slot
        self.slice = slice
        self.channels = 56
        self.clockRegs = hpcc.clockRegs
        self.NUMBER_TRIES = hpcc.NUMBER_TRIES
        self.hpcc = hpcc
        self.lmk01000 = Lmk01000(self, False)
        self.ad9914 = Ad9914DomainClock(self, False)
        # AC FPGA Capability
        self.minQdrMode = -1
        self.maxQdrMode = -1
        self.maxDdrMode = -1
        self.maxSdrMode = -1
        self.maxPeriod = -1
        self.maxCompareDelayCycles = -1
        self.maxDriveDelayCycles = -1
        self.driveOutDelay = -1
        self.compareInDelay = -1
        self.nominalFineDelayValue = -1
        self.fineDelayDivider = -1
        self.caldata = ACCal()
        # Timing Events
        self.driveDomainPeriod = -1
        self.compareDomainPeriod = -1
        self.domainQuarterCycle = -1
        self.driveAdjust = -1
        self.CompareAdjust = -1
        self.ratioOneTiming = False
        # CaptureControls fields and default values
        self.internalLoopback = True
        self.captureFails = True
        self.captureCTVs = True
        self.captureAll = True
        self.stopOnMaxFail = False
        self.dramOverflowWrap = False
        # Imported Snapshot Cal Value
        self.importedCal = defaultdict(namedtuple)
        self.CALIBRATION_PATH = os.path.join(projectpaths.ROOT_PATH, hpccConfigs.CALIBRATION_DIR)
        self.ddr_memory_size = INITIAL_DDR_MEMORY_SIZE_VALUE
        self.structs = hpcc_structs
        self._name = f'{self.__class__.__name__}_{slot}_{slice}'

    def name(self):
        return self._name

    def FpgaLoad(self, image):
        GZIP_REGEX = re.compile('(.*)\.gz')
        gzipped = GZIP_REGEX.match(os.path.basename(image))
        if gzipped:
            fin = gzip.open(image, 'rb')
            fout = tempfile.NamedTemporaryFile('wb', delete=False)
            fout.write(fin.read())
            fin.close()
            fout.close()
            self.Log('info', 'Extracted \'{}\' to \'{}\''.format(image, fout.name))
            image = fout.name
            self._perform_load(image)
            time.sleep(1)

    def _perform_load(self, image, num_retries=5):
        for retry in range(num_retries):
            with suppress_sensor(f'FVAL HPCC AC {self.slot}',
                                 f'HDMT_HPCCAC:{self.slot}'), \
                 suppress_sensor(
                     f'FVAL HPCC DC WHILE PROGRAMMING AC {self.slot}',
                     f'HDMT_HPCCDC:{self.slot}'):
                with HpccAc.FpgaPcieDriverDisable(self.slot):
                    try:
                        hil.hpccAcFpgaLoad(self.slot, self.slice, image)
                        os.remove(image)
                        break
                    except RuntimeError as e:
                        self.Log('warning',
                                 f'{self.name()}: Failed to load FPGA (attempt '
                                 f'{retry+1} of {num_retries}). {e}')
        else:
            os.remove(image)
            raise HpccAc.InstrumentError(
                f'{self.name()}: Could not load FPGA ({num_retries} '
                f'attempts)')

    class FpgaPcieDriverDisable():
        def __init__(self, slot):
            self.slot = slot

        def __enter__(self):
            hil.hpccAcDeviceDisable(self.slot, 0)
            hil.hpccAcDeviceDisable(self.slot, 1)
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            hil.hpccAcDeviceEnable(self.slot, 0)
            hil.hpccAcDeviceEnable(self.slot, 1)

    class InstrumentError(Exception):
        pass

    def GetFpgaVersion(self):
        version = self.read_register(ac_registers.Version)
        return '{}.{}.{}'.format(version.MajorVersion, version.MinorVersion, version.HwVersion)

    def BarRead(self, bar, offset):
        return hil.hpccAcBarRead(self.slot, self.slice, bar, offset)

    def BarWrite(self, bar, offset, data):
        #self.Log('debug', 'HPCCAC BarWrite: SLOT {} SLICE {} BAR {} OFFSET {} DATA {}'.format(self.slot, self.slice, bar, offset, data))
        return hil.hpccAcBarWrite(self.slot, self.slice, bar, offset, data)

    def DmaRead(self, address, length):
        return hil.hpccAcDmaRead(self.slot, self.slice, address, length)

    def DmaWrite(self, address, data):
        return hil.hpccAcDmaWrite(self.slot, self.slice, address, data)

    def Read(self, registerName):
        register = getattr(ac_registers, registerName)
        return register(self.BarRead(register.BAR, register.ADDR))

    def Write(self, registerName, data):
        register = getattr(ac_registers, registerName)
        if type(data) is int:
            return self.BarWrite(register.BAR, register.ADDR, data)
        else:
            return self.BarWrite(register.BAR, register.ADDR, data.value)
            

    def read_register(self, RegisterType, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        value = self.BarRead(RegisterType.BAR, RegisterType.address(index))
        return RegisterType(value=value)

    def write_register(self, register, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        self.BarWrite(register.BAR, register.address(index), register.value)

    def ReadChannel(self, channel, registerName):
        register = getattr(ac_registers, registerName)()
        register.value = self.BarRead(register.BAR, register.address(channel))
        return register

    def WriteChannel(self, channel, registerName, data):
        RegisterType = getattr(ac_registers, registerName)
        if type(data) is int:
            self.BarWrite(RegisterType.BAR, RegisterType.address(channel), data)
        else:
            self.BarWrite(RegisterType.BAR, RegisterType.address(channel), data.value)
        
    def get_alarms(self):
        '''returns an object with all alarms from both alarm registers'''
        alarm_register = self.read_register(ac_registers.AlarmControl)
        extended_alarm_register = self.read_register(ac_registers.AlarmControl1)
        alarms = create_field_dictionary(alarm_register, extended_alarm_register)
        alarms.pop('ExtendedAlarmsPresent', None)
        return alarms
    
    def log_all_active_alarms(self):
        alarms = [name for name, status in self.get_alarms().items() if status == 1]
        if alarms:
            self.Log('info', '  \n'.join(['Alarms:'] + sorted(alarms)))
        
    def ecc_correctable_error_count(self):
        count = 0
        for i in range(ac_registers.CorrectableECCCount.REGCOUNT):
            count += self.read_register(ac_registers.CorrectableECCCount, index=i).Count
        return count

    def ecc_uncorrectable_error_count(self):
        count = 0
        for i in range(ac_registers.UncorrectableECCCount.REGCOUNT):
            count += self.read_register(ac_registers.UncorrectableECCCount, index=i).Count
        return count

    def log_ecc_errors(self):
        for i in range(ac_registers.CorrectableECCCount.REGCOUNT):
            count = self.read_register(ac_registers.CorrectableECCCount, index=i).Count
            if count > 0:
                log_level = 'warning'
            else:
                log_level = 'info'
            self.Log(log_level, 'Correctable ECC Error Count {}   = {:13d}'.format(i, count))
        
        for i in range(ac_registers.UncorrectableECCCount.REGCOUNT):
            count = self.read_register(ac_registers.UncorrectableECCCount, index=i).Count
            if count > 0:
                log_level = 'error'
            else:
                log_level = 'info'
            self.Log(log_level, 'Uncorrectable ECC Error Count {} = {:13d}'.format(i, count))
    
    def get_source_control_node(self):
        build_info1 = self.read_register(ac_registers.BuildInfo1)
        build_info2 = self.read_register(ac_registers.BuildInfo2)
        node = (build_info2.UpperMqIdDword << 32) + build_info1.LowerMqIdDword
        if build_info2.MercurialClean:
            flag = ''
        else:
            flag = '+'
        return '{:012X}{}'.format(node, flag)
    
    def _CalculateFineDelay(self, remaining, fineDelayCal):
        guess = min(int(remaining / self.nominalFineDelayValue), len(fineDelayCal) - 1)

        # print("remaining = {}, initGuess = {}".format(remaining, fineDelayCal[guess]))
        # guessed too large, check if the smaller one is a better fit
        # shouldn't happen because int always round down
        # print( fineDelayCal[guess] )
        if (fineDelayCal[guess] > remaining):
            smallerDelay = fineDelayCal[guess - 1]
            if math.fabs(fineDelayCal[guess] - remaining) > math.fabs(remaining - smallerDelay):
                guess -= 1
        # guessed too small, check if a larger one fits better, unless it's already the largest
        elif guess != len(fineDelayCal) - 1:
            largerDelay = fineDelayCal[guess + 1]
            if math.fabs(remaining - fineDelayCal[guess]) > math.fabs(largerDelay - remaining):
                guess += 1
        # print("final guess = {}".format(fineDelayCal[guess]))
        if guess < 0:
            raise Exception('fine delay index < 0, should not happen.')

        return guess

    def _PlaceInputDPinEdge(self, inputEdge, inputDelay):
        halfFineDelay = self.nominalFineDelayValue / 2
        dpinEdge = ac_registers.DPinCompareEdge()
        # Input edge is different:
        # When we add fine delays, we take longer to sample our expected data, meaning our expected data ends up later in our captures
        # When we add subcycle delays, we sample later in time, meaning we effectly decrease the time data ends up in our capture, making it earlier in capture
        # When we add tester delays, we take longer before we compare our data, meaning data has extra time before it is sampled, making it earlier in capture
        # Our algorithm then becomes:
        # Remove all time, rounding up, with tester cycles
        # We then delays with sub cycle and fine delay to line up with a tester cycle

        # If we're an input edge, adding taps actually adds to our remaining time
        # Thus, we need to over-calculate with real tester events and then subtract the remaining with taps

        remaining = inputEdge + self.domainQuarterCycle - halfFineDelay + self.compareAdjust  # Force us to round down
        testerCyclesExact = remaining / self.compareDomainPeriod
        dpinEdge.TesterCycleDelay = int(testerCyclesExact)
        # print("inputEdge = {}, TesterCycleDelay = {}, remaining = {}".format(inputEdge, dpinEdge.TesterCycleDelay, remaining))
        remaining -= dpinEdge.TesterCycleDelay * self.compareDomainPeriod  # We will be either 0 or negative now
        # print("TesterCycleDelay = {}, remaining = {}".format(dpinEdge.TesterCycleDelay, remaining))
        # In input mode, subcycle is equal to a QDR
        quarterCyclesExact = remaining / self.domainQuarterCycle
        dpinEdge.CompareSubCycleDelay = int(quarterCyclesExact)
        remaining -= dpinEdge.CompareSubCycleDelay * self.domainQuarterCycle
        # print("CompareSubCycleDelay = {}, remaining = {}".format(dpinEdge.CompareSubCycleDelay, remaining))

        if dpinEdge.CompareSubCycleDelay >= (1 << self.read_register(ac_registers.PatternControl).SubCycleCount):
            self.Log('error', 'Timing Calculation Overflow on slot {} slice {}. '.format(self.slot, self.slice))

        remaining += halfFineDelay - self.domainQuarterCycle  # Remove rounding mechanism
        remaining *= -1  # Make positive, fill remaining
        dpinEdge.FineDelay = self._CalculateFineDelay(remaining, inputDelay)
        # print("FineDelay = {}, remaining = {}".format(dpinEdge.FineDelay, remaining))
        return dpinEdge

    def _PlaceOutputDPinEdge(self, outputEdge, outputDelay):
        dpinEdge = ac_registers.DPinDriveEdge()
        dpinEdge.value = 0
        halfFineDelay = self.nominalFineDelayValue / 2
        remaining = outputEdge + halfFineDelay + self.driveAdjust  # Round to nearest whole tester event

        # Preliminary calculations
        # Using the rational library requires careful organization of math, so as to not overflow
        # First, we divide our remaining time by our event type
        # We then figure out how many whole events we have (use boost::rational_cast<long long> to prevent overflow before even doing division)
        # We then subtract from our remaining time how much time we dedicated to that event type
        testerCyclesExact = remaining / self.driveDomainPeriod
        self.Log('debug','DriveDomainPeriod: {}'.format(self.driveDomainPeriod))
        dpinEdge.TesterCycleDelay = int(testerCyclesExact)
        # print("OutputEdge = {}, TesterCycleDelay = {}, remaining = {}".format(outputEdge, dpinEdge.TesterCycleDelay, remaining))
        remaining -= dpinEdge.TesterCycleDelay * self.driveDomainPeriod
        # print("TesterCycleDelay = {}, remaining = {}".format(dpinEdge.TesterCycleDelay, remaining))
        quarterCyclesExact = remaining / self.domainQuarterCycle
        CompareSubCycleDelay = int(quarterCyclesExact)
        dpinEdge.QdrDelay = CompareSubCycleDelay & 0b11
        dpinEdge.DriveSubCycleDelay = CompareSubCycleDelay >> 2
        remaining -= CompareSubCycleDelay * self.domainQuarterCycle
        # print("DriveSubCycleDelay = {}, remaining = {}".format(dpinEdge.DriveSubCycleDelay, remaining))
        # Only calculate sub cycles when we have enough of them (More than 4, which is 2^2, or 8 for ec ratio 1 mode)

        subCycleCount = self.read_register(ac_registers.PatternControl).SubCycleCount
        if ((subCycleCount > 2) and (not self.ratioOneTiming)) or ((subCycleCount > 1) and self.ratioOneTiming):
            if CompareSubCycleDelay >= (1 << subCycleCount):
                raise Exception('TimingCalculationOverflow, slot {} slice {}: edge = {}ps'.format(self.slot, self.slice, outputEdge))
        elif dpinEdge.QdrDelay > 3:
            raise Exception('TimingCalculationOverflow, slot {} slice {}: edge = {}ps'.format(self.slot, self.slice, outputEdge))

        remaining -= halfFineDelay  # Remove rounding mechanism
        dpinEdge.FineDelay = self._CalculateFineDelay(remaining, outputDelay)
        return dpinEdge

    def _KeepModeEnable(self, channel, keepModeSwitch):
        channelNum = channel.fpgaChannelNum

        conf1 = self.read_register(ac_registers.PerChannelControlConfig1, channelNum)

        if keepModeSwitch:
            conf1.PinModeSelect = 1
        else:
            conf1.PinModeSelect = 0
        # write to registers
        self.write_register(conf1, channelNum)
        '''
        print(hex(conf1.PinModeSelect))
        '''

    def _FreqCounterMode(self, channel, counterMode):
        self.Log('debug','_FreqCounterMode: CounterMode {}'.format(counterMode))
        channelNum = channel.fpgaChannelNum
        conf3 = self.read_register(ac_registers.PerChannelControlConfig3, channelNum)
        conf3.FrequencyCounterMode = counterMode
        # write to registers
        self.write_register(conf3, channelNum)

    def _PlaceDPinZeroDelay(self, channel):
        channelNum = channel.fpgaChannelNum
        conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
        conf2 = self.ReadChannel(channelNum, 'PerChannelControlConfig2')
        conf3 = self.ReadChannel(channelNum, 'PerChannelControlConfig3')
        conf4 = self.ReadChannel(channelNum, 'PerChannelControlConfig4')

        conf1.EnabledClockModeOn = 0
        if self.maxCompareDelayCycles < 1000:
            raise Exception('Place dpin edge not supported for delay cycle = {}'.format(self.maxCompareDelayCycles))
        # Input Edge
        conf4.InputTesterCycleDelay = 0  # dpinEdge.TesterCycleDelay
        conf3.InputSubCycleDelay = 0  # dpinEdge.CompareSubCycleDelay
        conf3.InputFineDelay = 0  # dpinEdge.FineDelay
        # print("Compare TCD = {} SCD = {} FD = {}".format(dpinEdge.TesterCycleDelay, dpinEdge.CompareSubCycleDelay, dpinEdge.FineDelay))

        # Output Edge
        conf4.OutputTesterCycleDelay = 0  # dpinEdge.TesterCycleDelay
        conf1.OutputSubCycleDelay = 0  # dpinEdge.DriveSubCycleDelay
        conf1.OutputQdrPhaseSelect = 0  # dpinEdge.QdrDelay
        conf2.OutputFineDelay = 0  # dpinEdge.FineDelay
        conf2.OutputAlternateFineDelay = 0  # dpinEdge.FineDelay
        # print("Drive TCD = {} SCD = {} QDR = {} FD = {} AFD = {}".format(dpinEdge.TesterCycleDelay, dpinEdge.DriveSubCycleDelay, dpinEdge.QdrDelay, dpinEdge.FineDelay, dpinEdge.FineDelay))
        # Enable Edge
        # dpinEdge.EdgeDelay = 0
        # dpinEdge.EdgeAdvance = 0
        conf4.EnableTesterCycleDelay = 0  # dpinEdge.TesterCycleDelay
        conf1.EnableSubCycleDelay = 0  # dpinEdge.DriveSubCycleDelay
        conf1.EnableQdrPhaseSelect = 0  # dpinEdge.QdrDelay
        conf3.EnableFineDelay = 0  # dpinEdge.FineDelay
        conf1.EnableEdgeAdvance = 0  # dpinEdge.EdgeAdvance
        conf1.EnableEdgeDelay = 0  # dpinEdge.EdgeDelay

        # Dynamic Phase Delay Bias
        conf1.OutputDynamicPhaseDelay = 0  # DYNAMIC_PHASE_DELAY_BIAS
        conf1.EnableDynamicPhaseDelay = 0  # DYNAMIC_PHASE_DELAY_BIAS
        conf3.InputDynamicPhaseDelay = 0  # DYNAMIC_PHASE_DELAY_BIAS

        # write to registers
        self.WriteChannel(channelNum, 'PerChannelControlConfig1', conf1)
        self.WriteChannel(channelNum, 'PerChannelControlConfig2', conf2)
        self.WriteChannel(channelNum, 'PerChannelControlConfig3', conf3)
        self.WriteChannel(channelNum, 'PerChannelControlConfig4', conf4)

    def _PlaceInputDPinEdgesZeroCal(self, channel):
        channelNum = channel.fpgaChannelNum
        cal = self.caldata.channelData[channelNum]
        conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
        conf3 = self.ReadChannel(channelNum, 'PerChannelControlConfig3')
        conf4 = self.ReadChannel(channelNum, 'PerChannelControlConfig4')

        conf1.EnabledClockModeOn = 0
        if self.maxCompareDelayCycles < 1000:
            raise Exception('Place dpin edge not supported for delay cycle = {}'.format(self.maxCompareDelayCycles))
        # Input Edge
        dpinEdge = self._PlaceInputDPinEdge(channel._compare + 0, cal.inputDelays)
        conf4.InputTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf3.InputSubCycleDelay = dpinEdge.CompareSubCycleDelay
        conf3.InputFineDelay = dpinEdge.FineDelay
        # print("Compare TCD = {} SCD = {} FD = {}".format(dpinEdge.TesterCycleDelay, dpinEdge.CompareSubCycleDelay, dpinEdge.FineDelay))


        # write to registers
        self.WriteChannel(channelNum, 'PerChannelControlConfig3', conf3)
        self.WriteChannel(channelNum, 'PerChannelControlConfig4', conf4)

    def _PlaceDPinEdges(self, channel):
        channelNum = channel.fpgaChannelNum
        cal = self.caldata.channelData[channelNum]
        #self.Log('debug','Cal Value for {}: {}'.format(channel,cal))
        conf1 = self.read_register(ac_registers.PerChannelControlConfig1, channelNum)
        conf2 = self.read_register(ac_registers.PerChannelControlConfig2, channelNum)
        conf3 = self.read_register(ac_registers.PerChannelControlConfig3, channelNum)
        conf4 = self.read_register(ac_registers.PerChannelControlConfig4, channelNum)

        conf1.EnabledClockModeOn = 0
        if self.maxCompareDelayCycles < 1000:
            raise Exception('Place dpin edge not supported for delay cycle = {}'.format(self.maxCompareDelayCycles))
        # Input Edge
        dpinEdge = self._PlaceInputDPinEdge(channel._compare + cal.inputCal, cal.inputDelays)
        conf4.InputTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf3.InputSubCycleDelay = dpinEdge.CompareSubCycleDelay
        conf3.InputFineDelay = dpinEdge.FineDelay
        # print("Compare TCD = {} SCD = {} FD = {}".format(dpinEdge.TesterCycleDelay, dpinEdge.CompareSubCycleDelay, dpinEdge.FineDelay))

        # Output Edge
        dpinEdge = self._PlaceOutputDPinEdge(channel._drive + cal.outputCal, cal.outputDelays)
        conf4.OutputTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf1.OutputSubCycleDelay = dpinEdge.DriveSubCycleDelay
        conf1.OutputQdrPhaseSelect = dpinEdge.QdrDelay
        conf2.OutputFineDelay = dpinEdge.FineDelay
        conf2.OutputAlternateFineDelay = dpinEdge.FineDelay
        # print("Drive TCD = {} SCD = {} QDR = {} FD = {} AFD = {}".format(dpinEdge.TesterCycleDelay, dpinEdge.DriveSubCycleDelay, dpinEdge.QdrDelay, dpinEdge.FineDelay, dpinEdge.FineDelay))
        # Enable Edge
        dpinEdge = self._PlaceOutputDPinEdge(channel._drive + cal.enableCal, cal.enableDelays)
        dpinEdge.EdgeDelay = 0
        dpinEdge.EdgeAdvance = 0
        conf4.EnableTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf1.EnableSubCycleDelay = dpinEdge.DriveSubCycleDelay
        conf1.EnableQdrPhaseSelect = dpinEdge.QdrDelay
        conf3.EnableFineDelay = dpinEdge.FineDelay
        conf1.EnableEdgeAdvance = dpinEdge.EdgeAdvance
        conf1.EnableEdgeDelay = dpinEdge.EdgeDelay

        # Dynamic Phase Delay Bias
        conf1.OutputDynamicPhaseDelay = DYNAMIC_PHASE_DELAY_BIAS
        conf1.EnableDynamicPhaseDelay = DYNAMIC_PHASE_DELAY_BIAS
        conf3.InputDynamicPhaseDelay = DYNAMIC_PHASE_DELAY_BIAS

        # # write to registers
        self.write_register(conf1, channelNum)
        self.write_register(conf2, channelNum)
        self.write_register(conf3, channelNum)
        self.write_register(conf4, channelNum)


    def _PlaceClockEdges(self, channel):
        channelNum = channel.fpgaChannelNum
        cal = self.caldata.channelData[channelNum]
        conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
        conf2 = self.ReadChannel(channelNum, 'PerChannelControlConfig2')
        conf3 = self.ReadChannel(channelNum, 'PerChannelControlConfig3')
        conf4 = self.ReadChannel(channelNum, 'PerChannelControlConfig4')

        if self.ratioOneTiming:
            enabledClockRatio = channel._ratio * 2
        else:
            enabledClockRatio = channel._ratio

        if enabledClockRatio > 254:
            raise Exception('channel {}, enabledClockRatio = {}, > 254 or 127'.format(channelNum, channel._ratio))
        if channel._tRise == channel._tFall:
            raise Exception('tRise = tFall = {} ps, should not happen'.format(channel._tRise))

        enabledClockPeriod = self.driveDomainPeriod * enabledClockRatio
        # If the trailing edge is falling, reset transition to 0 (enable cycle start with low state)
        # because we cannot control the signal before t0, the first trailing edge is skipped
        resetLow = (channel._tFall < channel._tRise)
        if resetLow:
            leadingEdge = channel._tRise
            trailingEdge = channel._tFall
        else:
            leadingEdge = channel._tFall
            trailingEdge = channel._tRise
        maxTesterCycles = self.maxCompareDelayCycles / 2
        trailingEdgeLimit = maxTesterCycles * self.driveDomainPeriod
        leadingEdgeLimit = trailingEdge + enabledClockPeriod - MINIMUM_PULSE_WIDTH_VALUE

        if (trailingEdge > trailingEdgeLimit) or (trailingEdge < 0):
            raise Exception('trailingEdge = {} ps, out of range (0, {}) ps'.format(trailingEdge, trailingEdgeLimit))
        if (leadingEdge > leadingEdgeLimit):
            raise Exception('leadingEdge = {} ps, bigger than upper limit {} ps'.format(leadingEdge, leadingEdgeLimit))

        if resetLow:
            enabledClockHighCycle = enabledClockPeriod - (leadingEdge - trailingEdge)
        else:
            enabledClockHighCycle = leadingEdge - trailingEdge
        highTesterCycles = enabledClockHighCycle / self.driveDomainPeriod
        highCycles = int(highTesterCycles)
        # If the duty cycle is <50%, use 'AND' logic. Otherwise, use 'OR' logic.
        orLogic = (2 * enabledClockHighCycle) > enabledClockPeriod
        # For "AND" logic, round up the high cycles.
        if (not orLogic) and (highTesterCycles != highCycles):
            highCycles += 1
        if (highCycles >= enabledClockRatio):
            raise Exception('highCycles = {}, >= enabledClockRatio {}'.format(highCycles, enabledClockRatio))
        lowCycles = enabledClockRatio - highCycles
        if (highCycles > 127) or (lowCycles > 127):
            raise Exception(
                'channel {}, highCycles = {}, lowCycles = {}, expect <= 127. For ratio > 128, duty cycle choice is limited'.format(
                    channelNum, highCycles, lowCycles))

        # Jeff : remove the CalculateAdvanceDelay function for advance or delay Edge in all the situations
        # since there is no need for the the issue.
        if resetLow and orLogic:
            dataEdge = self._PlaceOutputDPinEdge(channel._tFall + cal.outputCal, cal.outputDelays)
            calcEdge = channel._tRise - (lowCycles * self.driveDomainPeriod)
            # print('calEdge = {}'.format(calcEdge))
            # [enableAdvance, calcEdge] = self._CalculateAdvanceDelay(calcEdge, enabledClockPeriod)
            # print('calEdge = {}, enabledAdvance = {}'.format(calcEdge, enableAdvance))
            enableEdge = self._PlaceOutputDPinEdge(calcEdge + cal.enableCal, cal.enableDelays)
        elif orLogic:  # and resetHigh
            calcEdge = channel._tFall - (highCycles * self.driveDomainPeriod)
            # print('calEdge = {}'.format(calcEdge))
            # [enableAdvance, calcEdge] = self._CalculateAdvanceDelay(calcEdge, enabledClockPeriod)
            # print('calEdge = {}, enabledAdvance = {}'.format(calcEdge, enableAdvance))
            enableEdge = self._PlaceOutputDPinEdge(calcEdge + cal.enableCal, cal.enableDelays)
            dataEdge = self._PlaceOutputDPinEdge(channel._tRise + cal.outputCal, cal.outputDelays)
        elif resetLow:  # and andLogic, same as the first one
            dataEdge = self._PlaceOutputDPinEdge(channel._tFall + cal.outputCal, cal.outputDelays)
            calcEdge = channel._tRise - (lowCycles * self.driveDomainPeriod)
            # print('calEdge = {}'.format(calcEdge))
            # [enableAdvance, calcEdge] = self._CalculateAdvanceDelay(calcEdge, enabledClockPeriod)
            # print('calEdge = {}, enabledAdvance = {}'.format(calcEdge, enableAdvance))
            enableEdge = self._PlaceOutputDPinEdge(calcEdge + cal.enableCal, cal.enableDelays)
        else:  # resetHigh and andLogic, same as the second one
            calcEdge = channel._tFall - (highCycles * self.driveDomainPeriod)
            # print('calEdge = {}'.format(calcEdge))
            # [enableAdvance, calcEdge] = self._CalculateAdvanceDelay(calcEdge, enabledClockPeriod)
            # print('calEdge = {}, enabledAdvance = {}'.format(calcEdge, enableAdvance))
            enableEdge = self._PlaceOutputDPinEdge(calcEdge + cal.enableCal, cal.enableDelays)
            dataEdge = self._PlaceOutputDPinEdge(channel._tRise + cal.outputCal, cal.outputDelays)

        conf1.EnabledClockModeOn = 1
        if channel._ratio == 1:
            conf2.EnabledClockTesterCyclesHigh = 0
            conf2.EnabledClockTesterCyclesLow = 0
        else:
            conf2.EnabledClockTesterCyclesHigh = highCycles
            conf2.EnabledClockTesterCyclesLow = lowCycles
        if orLogic:
            conf2.LogicCombinationSelect = 1
        else:
            conf2.LogicCombinationSelect = 0
        conf2.EcDirectDriveEventSelect = channel._driveType
        if resetLow:
            rEvent = 0
        else:
            rEvent = 1

        # Note: the channel._resetState is set in the adate320.py, now only set to 0.
        conf2.EcREventSelect = rEvent | (channel._resetState << 1)

        # input edge
        dpinEdge = self._PlaceInputDPinEdge(channel._compare + cal.inputCal, cal.inputDelays)
        conf4.InputTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf3.InputSubCycleDelay = dpinEdge.CompareSubCycleDelay
        conf3.InputFineDelay = dpinEdge.FineDelay
        # Output Edge
        conf4.OutputTesterCycleDelay = dataEdge.TesterCycleDelay
        conf1.OutputSubCycleDelay = dataEdge.DriveSubCycleDelay
        conf1.OutputQdrPhaseSelect = dataEdge.QdrDelay
        conf2.OutputFineDelay = dataEdge.FineDelay
        # Enable Edge
        # enableEdge.EdgeAdvance = (enableAdvance & 0x2) >> 1
        # enableEdge.EdgeDelay = (enableAdvance & 0x1)
        conf4.EnableTesterCycleDelay = enableEdge.TesterCycleDelay
        conf1.EnableSubCycleDelay = enableEdge.DriveSubCycleDelay
        conf1.EnableQdrPhaseSelect = enableEdge.QdrDelay
        conf2.OutputAlternateFineDelay = enableEdge.FineDelay
        # conf1.EnableEdgeAdvance = enableEdge.EdgeAdvance
        # conf1.EnableEdgeDelay = enableEdge.EdgeDelay
        conf1.EnabledClockDifferentialCompMode = 0

        # Dynamic Phase Delay Bias
        conf1.OutputDynamicPhaseDelay = DYNAMIC_PHASE_DELAY_BIAS
        conf1.EnableDynamicPhaseDelay = DYNAMIC_PHASE_DELAY_BIAS
        conf3.InputDynamicPhaseDelay = DYNAMIC_PHASE_DELAY_BIAS

        # write to registers        
        self.WriteChannel(channelNum, 'PerChannelControlConfig1', conf1)
        self.WriteChannel(channelNum, 'PerChannelControlConfig2', conf2)
        self.WriteChannel(channelNum, 'PerChannelControlConfig3', conf3)
        self.WriteChannel(channelNum, 'PerChannelControlConfig4', conf4)

        # TODO: need to update calcEdge - Jeff 02/28.27: this function might no long needed.

    def _CalculateAdvanceDelay(self, calcEdge, enabledClockPeriod):
        if calcEdge < 0:
            calcEdge += self.driveDomainPeriod
            if calcEdge < 0:
                raise Exception('calcEdge = {} too small'.format(calcEdge - self.driveDomainPeriod))
            return [2, calcEdge]  # 2 will cause an edge advance
        elif calcEdge > enabledClockPeriod:
            calcEdge -= self.driveDomainPeriod
            return [1, calcEdge]  # 1 will cause an edge delay
        return [0, calcEdge]  # 0 will cause no advance or delay

    def _ClearSoftwareScratchpad(self):
        regSoftwareScratchpad = ac_registers.SoftwareScratchpad()
        regSoftwareScratchpad.MagicByte = 0xAC
        self.write_register(regSoftwareScratchpad)

    def _UpdateFpgaCapabilities(self):
        capabilityReg = self.read_register(ac_registers.Capability0)
        self.Log('info', 'Current Timing Version from Capability Register: {}'.format(capabilityReg.TimingVersion))
        if capabilityReg.TimingVersion == 1:
            self.minQdrMode = 1180  # ps 850Mbps
        elif capabilityReg.TimingVersion == 0:
            self.minQdrMode = 2500  # ps
        elif capabilityReg.TimingVersion == 2:
            self.minQdrMode = 960  # ps 1040MBps
        elif capabilityReg.TimingVersion == 3:
            self.minQdrMode = 800  # ps
        elif capabilityReg.TimingVersion == 4:
            self.minQdrMode = 686  # ps
        elif capabilityReg.TimingVersion == 5:
            self.minQdrMode = 666  # ps
        else:
            raise Exception('TimingVersion = {} not supported'.format(capabilityReg.TimingVersion))
        self.maxQdrMode = self.minQdrMode * 2
        self.maxDdrMode = self.maxQdrMode * 2
        self.maxSdrMode = self.maxQdrMode * 4
        self.maxPeriod = self.maxQdrMode * 256
        self.fineDelayDivider = 8 - capabilityReg.FineDelayClockSpeed
        # print('FineDelayClockSpeed is {}'.format(capabilityReg.FineDelayClockSpeed) )
        self.driveOutDelay = capabilityReg.DriveOutDelay
        self.compareInDelay = capabilityReg.CompareInDelay
        self.nominalFineDelayValue = ((1000 / 3) * self.fineDelayDivider) / 64  # TODO: may need to write in Fraction
        if capabilityReg.DelayStackConfiguration == 2:
            self.maxCompareDelayCycles = 1023
            self.maxDriveDelayCycles = 1023
        elif capabilityReg.DelayStackConfiguration == 1:
            self.maxCompareDelayCycles = 1023
            self.maxDriveDelayCycles = 1023
        elif capabilityReg.DelayStackConfiguration == 0:
            self.maxCompareDelayCycles = 31
            self.maxDriveDelayCycles = 15
        else:
            raise Exception('DelayStackConfiguration = {} not supported'.format(capabilityReg.DelayStackConfiguration))

        self.Log('info',
                 'HPCC slot {} slice {} Capabilities: MinQdr={}ps, MaxQdr={}ps, MaxPeriod={}ps, DriveOutDelay={}, CompareInDelay={}, NominalFineDelayValue={}, MaxDelayCycle={}, Capability0=0x{:x}'.format(
                     self.slot, self.slice, self.minQdrMode, self.maxQdrMode, self.maxPeriod, self.driveOutDelay,
                     self.compareInDelay, self.nominalFineDelayValue, self.maxCompareDelayCycles, capabilityReg.value))

        # manually initialize ac cal data
        self.caldata.LoadNominalFineDelayValues(self.nominalFineDelayValue)

        # Update domain period with something, so that it isn't 0
        self.SetDomainPeriod(self.minQdrMode / PICO_SECONDS_PER_SECOND)
    
    def minimum_tester_period(self, subcycle=0):
        if self.minQdrMode == -1:
            self._UpdateFpgaCapabilities()
        if subcycle == 0:
            period = self.minQdrMode / PICO_SECONDS_PER_SECOND
        else:
            period = (self.minQdrMode * 2**subcycle + 1) / PICO_SECONDS_PER_SECOND
        return period
    
    def maximum_tester_period(self, subcycle=None):
        if self.maxPeriod == -1:
            self._UpdateFpgaCapabilities()
        if subcycle is None:
            period = self.maxPeriod
        else:
            period = self.maxQdrMode * 2**subcycle / PICO_SECONDS_PER_SECOND
        return period

    def _Reset(self):
        self._initialize_ddr_controllers()
        self._reset_dma_engines()
        self._reset_ecc_count()
        self._clear_pin_domain_reset()
    
    def _initialize_ddr_controllers(self):
        for retries in range(3):
            self._reset_ddr_controllers()
            if self._ddr_is_initialized():
                break
        else:
            self._ClearSoftwareScratchpad()
            self.Log('critical', 'DDR initialization failed')
        
        self.Log('info', 'DDR initialization complete on slot {} slice {}'.format(self.slot, self.slice))
    
    def _reset_ddr_controllers(self):
        statusReg = self.read_register(ac_registers.ResetsAndStatus)
        statusReg.ResetDDR3IpBlocks = 1
        self.write_register(statusReg)
        time.sleep(0.001)
        statusReg.ResetDDR3IpBlocks = 0
        self.write_register(statusReg)
    
    def _ddr_is_initialized(self):
        for retries in range(1000):
            statusReg = self.read_register(ac_registers.ResetsAndStatus)
            if statusReg.PLLLocks == 0xF and statusReg.DDR3ControllerInitialized == 0xF:
                return True
            time.sleep(0.001)
        else:
            if statusReg.PLLLocks != 0xF:
                self.Log('warning', 'Not all PLLs locked on slot {} slice {}. '\
                                    'Expected: 0xF, Actual: 0x{:X}'.format(self.slot,
                                                                           self.slice,
                                                                           statusReg.PLLLocks))
            if statusReg.DDR3ControllerInitialized != 0xF:
                self.Log('warning', 'Not all DDR blocks initialized on slot'\
                                    ' {} slice {}. Expected: 0xF, Actual: 0x{:X}'.format(
                                    self.slot, self.slice, statusReg.DDR3ControllerInitialized))
            return False
    
    def _reset_dma_engines(self):
        statusReg = self.read_register(ac_registers.ResetsAndStatus)
        statusReg.DMAReset = 1
        self.write_register(statusReg)
        time.sleep(0.001)
        statusReg.DMAReset = 0
        self.write_register(statusReg)
        
    def _clear_pin_domain_reset(self):
        statusReg = self.read_register(ac_registers.ResetsAndStatus)
        statusReg.PinDomainReset = 0
        self.write_register(statusReg)
        
    def _CalculateMemorySize(self):
        # TODO: check if is calculated
        self.Log('debug', 'Calculating memory size on slot {} AC FPGA {}.'.format(self.slot, self.slice))
        self._Reset()  # TODO: train DDR for 800MHz

        # Calculating memory size needs to be done in two ways, since we're using DIMMs
        # Our DIMM can have up to 4 ranks of memory, or just a single rank
        # If we try to write/read a non-existing rank of memory, then it won't alias but be junk data
        # If we try to write/read a rank greater than 4, then it will alias to mod 4 rank
        # Thus we need to make two checks:
        # 1. The data we wrote comes back valid
        # 2. The data we wrote didn't go to address 0
        ORIGINAL_FIRST_BYTE = bytes(
            [0xff, 0xaa, 0x00, 0x55, 0xaa, 0xff, 0x55, 0x00])  # Set first 64 bits to a known value
        NOT_FIRST_BYTE = bytes([0xaa, 0xaa, 0x55, 0x55, 0x00, 0x00, 0xff, 0xff])
        # MemoryBufferSize = (sizeof(VectorType) / sizeof(size_t)) * MinimumVectorDma
        MemoryBufferSize = (VECTOR_SIZE // 8) * MINIMUM_VECTOR_DMA  # 32, unit = 8bytes
        length = 8 * 32  # unit = byte
        memoryBuffer = ORIGINAL_FIRST_BYTE + bytes(length - 8)
        addressLine = 17  # TODO: Start at BRAMs size...in future, can probably optimize this to a larger start point.

        self.DmaWrite(0, memoryBuffer)
        newFirstByte = ORIGINAL_FIRST_BYTE
        newNotFirstByte = NOT_FIRST_BYTE
        # Now go through and test each address line until we fail one of the two checks above.
        # TODO: catch cal mem size exception
        while (newFirstByte == ORIGINAL_FIRST_BYTE and newNotFirstByte == NOT_FIRST_BYTE):
            memoryBuffer = NOT_FIRST_BYTE + bytes(length - 8)
            addressLine += 1
            if addressLine > 40:
                break
            offset = 1 << addressLine
            self.DmaWrite(offset, memoryBuffer)
            memoryBufferData = self.DmaRead(offset, length)
            newNotFirstByte = memoryBufferData[0:8]
            memoryBufferData = self.DmaRead(0, length)
            newFirstByte = memoryBufferData[0:8]
        memSize = (1 << addressLine >> 10) / 1024.0
        self.Log('info',
                 'Calculated memory size on slot {} AC FPGA {} is {} MB.'.format(self.slot, self.slice, memSize))
        self._Reset()
        return (1 << addressLine)

    # TODO: read memSize from software scratchpad
    def _InitializeEccBytes(self):
        # TODO: check software scratchpad        
        BUFFER_SIZE = 1024 * 1024 * 64
        tempBuffer = bytes([0xff] * BUFFER_SIZE)  # 64MB buffer
        self.Log('debug',
                 'Initializing {} MB of memory on slot {} AC FPGA {} .'.format(((self.ddr_memory_size >> 10) / 1024.0), self.slot,
                                                                               self.slice))

        addr = 0
        while (addr < self.ddr_memory_size):
            self.DmaWrite(addr, tempBuffer)
            addr += BUFFER_SIZE
        self.Log('info', 'ECC Memory Initialization finished on slot {} AC FPGA {} .'.format(self.slot, self.slice))

    def _reset_ecc_count(self):
        # Reset the ECC count
        statusReg = self.read_register(ac_registers.ResetsAndStatus)
        statusReg.ECCCountReset = 1
        self.write_register(statusReg)
        # Unset the EccCountReset to detect ECC errors
        statusReg.ECCCountReset = 0
        self.write_register(statusReg)

    def _ResetPinDomain(self):
        # TODO: check sw scratchpad
        # Set clock to 62.5MHz due to limitations in FPGA IO cells
        self.ad9914.SetPeriod(16e-9)
        self.ad9914.EnableOutput(False)

        statusReg = self.read_register(ac_registers.ResetsAndStatus)
        statusReg.PinDomainReset = 1
        self.write_register(statusReg)

        self.ad9914.EnableOutput(True)
        self.ad9914.EnableOutput(False)
        statusReg.PinDomainReset = 0
        self.write_register(statusReg)

        self.ad9914.EnableOutput(True)

        '''
        # if need to call pin domain reset for debug purpose, may want to uncomment this block
        # pin domain reset will reset fine delays, but delay stack doesn't know, therefore, need to force reset to 0
        for i in range(56):
            config2 = self.ReadChannel(i, 'PerChannelControlConfig2')
            config3 = self.ReadChannel(i, 'PerChannelControlConfig3')
            config2.OutputFineDelay = 0 
            config2.OutputAlternateFineDelay = 0 
            config3.EnableFineDelay = 0 
            config3.InputFineDelay = 0    
            self.WriteChannel(i, 'PerChannelControlConfig2', config2)
            self.WriteChannel(i, 'PerChannelControlConfig3', config3)
        '''

    # TODO: RunDummyPattern()
    #TODO: NOT RUN ANYWHERE
    def _RunDummyPattern(self):
        pattern = PatternAssembler()
        # pattern.symbols['REPEATS'] = length  # 1021
        pattern.symbols['IoJamCompare'] = 16  # 1021
        pattern.symbols['AllHighLow'] = 54  # (124-16)/2
        pattern.symbols['IoJamDrive'] = 16  # 1021
        pattern.symbols['AllHighLowDontCare'] = 35  # 1021

        '''
        pattern.LoadString("""\
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
            BlockFails 1
            Drive0CompareLVectors length=1024
            BlockFails 0
            %repeat 256
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
            %end
            V link=0, ctv=1, mtv=1, lrpt=0, data=0vH1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1  
            %repeat 256                                                                               
            V link=0, ctv=0, mtv=1, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0  
            %end
            PATTERN_END:                                                                                     
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            S stype=IOSTATEJAM,             data=0jX0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0  
        """)        
        '''
        pattern.LoadString("""\
            PATTERN_START:                                                                                   # 0
            
            %repeat IoJamCompare
            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            %end


            %repeat AllHighLow
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
            %end
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm=102400
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm=102400
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

            %repeat IoJamDrive
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            %end

            %repeat AllHighLowDontCare
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111111111111111111111111
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000000000000000000000000
            %end

            I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm=102400
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ

            S stype=IOSTATEJAM,             data=0jXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
            V link=0, ctv=0, mtv=0, lrpt=0, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        """)
        # '''
        pdata = pattern.Generate()

        self.Log('info', 'Running Dummy Pattern for  slot {} slice {}'.format(self.slot, self.slice))

        self.WritePattern(0, pdata)
        # Setup capture
        captureBaseAddress = self.SinglePatternCaptureBaseAddress(len(pdata))
        self.SetCaptureBaseAddress(captureBaseAddress)
        self.SetCaptureCounts()
        # SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail, dramOverflowWrap = False):
        self.SetCaptureControl(ac_registers.CaptureControl.FAIL_STATUS, False, False, True, False)  # capture all

        self.SetDomainPeriodSubcycleZeroAndInternal(2, 20e-9)
        # self.SetPeriod(5e-9)

        # Clear fail counts
        TotalFailCountReg = ac_registers.TotalFailCount()
        TotalFailCountReg.value = 0
        self.write_register(TotalFailCountReg)
        for ch in range(56):
            self.WriteChannel(ch, 'ChannelFailCount', 0)

        self.ExecutePattern(patternStartAddress=0, waitForComplete=True, numberOfTries=500)

        totalFailCount = self.read_register(ac_registers.TotalFailCount).value
        if totalFailCount <= 0:
            raise HpccAc.PatternRunError(
               f'After Pattern Running, Total Fail Count is {totalFailCount} '
               f'for slot {self.slot} slice {self.slice}')
        endStatus = self.read_register(ac_registers.PatternEndStatus).value
        if hex(endStatus) != '0xdeadbeef':
            raise HpccAc.PatternRunError(
                f'After Pattern Running, endstatus is {hex(endStatus)} '
                f'for slot {self.slot} slice {self.slice}')

    class PatternRunError(Exception):
        pass

    def _InitACFPGA(self):
        if not configs.SKIP_MEMORY_INIT and not hpccConfigs.HPCC_SC_SKIP_MEM_INIT:
            # TODO: check if device exists
            memSize = self._CalculateMemorySize()
            # TODO: figure out why it can be < 1 MB
            if (memSize >> 20) < 1:  # less than 1 MB
                self.Log('warning',
                         'HPCC {} SLICE{}: Memory size less than 1 MB, try again.'.format(self.slot, self.slice))
                memSize = self._CalculateMemorySize()
                if ((memSize >> 10) / 1024.0) < 1:
                    self.Log('error',
                             'HPCC {} SLICE{}: Memory size less than 1 MB, fail the second time.'.format(self.slot,
                                                                                                         self.slice))
            # m_patternMemoryMap.Initialize(memSize / VECTOR_SIZE)
            self.ddr_memory_size = memSize
            self._InitializeEccBytes()
            self._reset_ecc_count()
        else:
            self.Log('info', 'Skipping DDR memory initialize')
        self._ResetPinDomain()
        self._RunDummyPattern()
        self.ad9914.setup_ad9914()
        self._sync_domain_clock()
        self._ResetPinDomain()

    def _sync_domain_clock(self,):
        if self._new_domain_clock_sync_algorithm_required():
            self.Log('info', 'This FPGA implements a new domain clock sync algorithm!')
            self._sync_domain_clock_using_new_algorithm()
        else:
            self.Log('info', 'This FPGA implements a legacy domain clock sync algorithm!')
            self._sync_domain_clock_using_legacy_algorithm()

    def _sync_domain_clock_using_legacy_algorithm(self):
        state = self.ad9914.GetSyncDistribution(100, self.lmk01000)
        self.Log('info', 'ad9914 sync state {}'.format(state))

    def _sync_domain_clock_using_new_algorithm(self):
        self.ad9914.get_sync_state(self.slice,self.lmk01000)

    def _new_domain_clock_sync_algorithm_required(self):
        """Check capability bit, when implemented, for now assume True"""
        if self.read_register(ac_registers.Capability1).AD9914NoPLLPhaseDetect:
            return True
        else:
            return False

    def SetDomainPeriod(self, period):
        self.period = period
        period_in_picoseconds = period * PICO_SECONDS_PER_SECOND
        self.Log('debug',
                 'HPCC {} SLICE{}: setting domain period to {} ps.'.format(self.slot, self.slice, period_in_picoseconds))
        
        if period_in_picoseconds < self.minQdrMode:
            raise self.DomainPeriodError('{} ps is less than minimum ({} ps)'.format(period_in_picoseconds, self.minQdrMode))
        
        if period_in_picoseconds > self.maxPeriod:
            raise self.DomainPeriodError('{} ps is greater than maximum ({} ps)'.format(period_in_picoseconds, self.maxPeriod))
        
        subCycleCount = -1
        if period_in_picoseconds <= self.maxQdrMode:
            self.domainQuarterCycle = period_in_picoseconds
            subCycleCount = 0
        elif period_in_picoseconds <= self.maxDdrMode:
            self.domainQuarterCycle = period_in_picoseconds / 2
            subCycleCount = 1
        elif period_in_picoseconds <= self.maxSdrMode:
            self.domainQuarterCycle = period_in_picoseconds / 4
            subCycleCount = 2
        elif period_in_picoseconds <= self.maxPeriod:
            repeat = math.ceil(period_in_picoseconds / self.maxQdrMode)
            repeat -= 1
            power2exp = 8
            while power2exp > 3:
                if (repeat >> (power2exp - 1)) == 1:
                    break
                power2exp -= 1
            self.domainQuarterCycle = period_in_picoseconds / (1 << power2exp)
            subCycleCount = power2exp
            

        self._set_ddr_request_interval(subcycle_count=subCycleCount)
        
        self.compareDomainPeriod = period_in_picoseconds
        # self.Log('info','domain period: {}, pin ring clock div2: {}, subCycleCount: {}'.format(period_in_picoseconds, 4 * self.domainQuarterCycle, subCycleCount))
        self.driveDomainPeriod = period_in_picoseconds
        self.driveAdjust = self._UpdateDriveAdjust()
        self.compareAdjust = self._UpdateCompareAdjust()

        self.ad9914.SetPeriod(2 * self.domainQuarterCycle / PICO_SECONDS_PER_SECOND)
        self._write_subcycle_count_to_fpga(subCycleCount)

        self.Log('debug',
                 'HPCC {} SLICE {}: setting sub cycle count to {}, domain quarter cycle to {} ps, ad9914 period to {} ps. driveAdjust = {}, compareAdjust = {}'.format(
                     self.slot, self.slice, subCycleCount, self.domainQuarterCycle, 2 * self.domainQuarterCycle,
                     self.driveAdjust, self.compareAdjust))
    
    class DomainPeriodError(Exception):
        pass
        
    def _set_ddr_request_interval(self, subcycle_count):
        pin_ddr_refresh_control = self.read_register(ac_registers.PinDdrRefreshControl)
        pin_ddr_request_control = self.read_register(ac_registers.PinDdrRequestControl)
        request_interval_constant = self.read_register(ac_registers.Capability1).RequestIntervalConstant
        request_duration_constant = self.read_register(ac_registers.Capability1).RequestDurationConstant
        internal_pin_clock_period = ((self.period * PICO_SECONDS_PER_SECOND) / 2**subcycle_count) * 4
        ratio = (self.minQdrMode / internal_pin_clock_period) * 4
        pin_ddr_refresh_control.RefreshInterval = int(7750000 / internal_pin_clock_period)
        pin_ddr_refresh_control.RefreshDuration = int(380000 / internal_pin_clock_period)
        pin_ddr_request_control.RequestInterval = int(request_interval_constant * ratio)
        pin_ddr_request_control.RequestDuration = int(request_duration_constant * ratio)
        self.write_register(pin_ddr_refresh_control)
        self.write_register(pin_ddr_request_control)

    def SetDomainPeriodSubcycleZeroAndInternal(self, subCycleCount, internalPeriod):
        self.domainQuarterCycle = internalPeriod * PICO_SECONDS_PER_SECOND / 2
        domainPeriod = self.domainQuarterCycle  # * (1 << subCycleCount)
        self.period = domainPeriod / PICO_SECONDS_PER_SECOND  # FIXME: Is this how we want to store the per-slice period?

        self.Log('debug',
                 'HPCC {} SLICE{}: setting domain period to {} ps.'.format(self.slot, self.slice, domainPeriod))
        if domainPeriod < self.minQdrMode:
            raise Exception('Domain period {} ps too small. Min Qdr = {} ps'.format(domainPeriod, self.minQdrMode))
        elif domainPeriod > self.maxPeriod:
            raise Exception('Domain period {} ps too large. Max period = {} ps'.format(domainPeriod, self.maxPeriod))

        self.compareDomainPeriod = domainPeriod
        self.driveDomainPeriod = domainPeriod
        self.driveAdjust = 0  # self._UpdateDriveAdjust()
        self.compareAdjust = 0  # self._UpdateCompareAdjust()

        self.ad9914.SetPeriod(2 * self.domainQuarterCycle / PICO_SECONDS_PER_SECOND)
        self._write_subcycle_count_to_fpga(subCycleCount)
        self.Log('debug',
                 'HPCC {} SLICE {}: setting sub cycle count to {}, domain quarter cycle to {} ps, ad9914 period to {} ps. driveAdjust = {}, compareAdjust = {}'.format(
                     self.slot, self.slice, subCycleCount, self.domainQuarterCycle, 2 * self.domainQuarterCycle,
                     self.driveAdjust, self.compareAdjust))

    def SetDomainPeriodSubcycleAndInternal(self, subCycleCount, internalPeriod):
        period = internalPeriod * (1 << subCycleCount) / 2
        self.SetDomainPeriod(period)

    def _write_subcycle_count_to_fpga(self, subcycle_count):
        patternControlReg = self.read_register(ac_registers.PatternControl)
        patternControlReg.SubCycleCount = subcycle_count
        self.write_register(patternControlReg)
        
    # If we set a Ratio 1 EC, the FPGA will automatically subtract one subcycle on the drive side and double the number of symbols
    # This will cause the domain subcycle and domain period to be 1/2 of what the user programmed
    # We need to update our drive events to reflect this change in the FPGA
    def UpdateRatioOneECDomainPeriod(self, isECRatioOne):
        update = False
        if self.ratioOneTiming and (not isECRatioOne):
            self.Log('debug',
                     'HPCC {} SLICE {}: double drive domain period since there is none ratio one EC'.format(self.slot,
                                                                                                            self.slice))
            self.driveDomainPeriod *= 2
            self.driveAdjust = self._UpdateDriveAdjust()
            self.compareAdjust = self._UpdateCompareAdjust()
            update = True
        elif (not self.ratioOneTiming) and isECRatioOne:
            self.Log('debug', 'HPCC {} SLICE {}: reduce drive domain period by half since there is ratio one EC'.format(
                self.slot, self.slice))
            patternControlReg = self.read_register(ac_registers.PatternControl)
            if patternControlReg.SubCycleCount == 0:
                raise Exception('Invalid domain period {} ps for Ratio One EC, should be bigger than {} ps'.format(
                    self.driveDomainPeriod, self.maxQdrMode))
            self.driveDomainPeriod /= 2
            self.driveAdjust = self._UpdateDriveAdjust()
            self.compareAdjust = self._UpdateCompareAdjust()
            update = True
        self.ratioOneTiming = isECRatioOne
        return update

    def _UpdateDriveAdjust(self):
        # TODO: longPulseDetected, from run dummy pattern
        longPulseDetected = 0
        # Delay of FPGA is 28 FPGA cycles, 
        # minus 1 9914 cycle due to sync start, plus 1 user cycle due to FPGA bias
        # Finally, long pulse adjustment is minus 1 9914 cycle (minus the 8ns the pulse adds in the first place)

        adjust = (self.driveOutDelay * self.maxQdrMode + self.maxPeriod)
        adjust -= (((self.driveOutDelay - longPulseDetected) * self.domainQuarterCycle) + self.driveDomainPeriod)
        adjust += EIGHT_THOUSAND_PICOSECONDS * longPulseDetected
        adjust += (self.minQdrMode * self.maxDriveDelayCycles / 16)
        adjust -= DYNAMIC_PHASE_DELAY_BIAS * self.domainQuarterCycle  # compensate drive DPD bias
        return adjust

    def _UpdateCompareAdjust(self):
        adjust = self._UpdateDriveAdjust()
        adjust -= (((self.compareInDelay - self.driveOutDelay) * (
            self.domainQuarterCycle - self.minQdrMode)) + EIGHT_THOUSAND_PICOSECONDS)
        adjust += DYNAMIC_PHASE_DELAY_BIAS * self.domainQuarterCycle * 2  # undo drive DPD bias compensation + compensate compare DPD bias
        return adjust

    def WritePattern(self, offset, data, checkSimMemory=True):
        patternSize = len(data)
        if patternSize % VECTOR_SIZE != 0:
            raise Exception('Pattern size of {} bytes is not a multiple of {}'.format(patternSize, VECTOR_SIZE))
        vectorCount = patternSize // VECTOR_SIZE
        # FPGA pre-fetches up to 4 pages, exceed memory boundary could cause memory corruption, which leads to uncorrectable ECC error
        if (offset // VECTOR_SIZE) + vectorCount >= DDR_MEMORY_BOUNDARY:
            raise Exception(
                'Pattern write to address 0x{:X} (0x{:X} vectors) exceeds pattern memory boundary 0x{:X}'.format(offset,
                                                                                                                 vectorCount,
                                                                                                                 DDR_MEMORY_BOUNDARY))
        if (offset // VECTOR_SIZE) + vectorCount > hpccConfigs.HPCC_AC_SIM_MEM:
            if checkSimMemory:
                raise Exception(
                    'Pattern write to address 0x{:X} (0x{:X} vectors) exceeds simulator memory (0x{:X} vectors)'.format(
                        offset, vectorCount, hpccConfigs.HPCC_AC_SIM_MEM))
            else:
                self.Log('warning',
                         'Pattern write to address 0x{:X} (0x{:X} vectors) exceeds simulator memory (0x{:X} vectors), do not run sim!'.format(
                             offset, vectorCount, hpccConfigs.HPCC_AC_SIM_MEM))
        remainder = vectorCount % DMA_WRITE_ALIGNMENT
        padding = None
        if remainder == 0:
            padding = 0
        else:
            padding = DMA_WRITE_ALIGNMENT - remainder
            vectorCount = vectorCount + padding
            patternSize = vectorCount * VECTOR_SIZE
            paddedData = bytearray(patternSize)
            for i in range(len(data)):
                paddedData[i] = data[i]
            data = bytes(paddedData)
            self.Log('warning',
                     'Padded pattern data with {} vectors because original vector count of {} is not a multiple of {}'.format(
                         padding, vectorCount, DMA_WRITE_ALIGNMENT))
        # Write pattern to memory
        self.DmaWrite(offset, data)
        self.Log('debug', 'Wrote {} bytes of pattern data ({} vectors) to memory (address=0x{:x})'.format(patternSize,
                                                                                                          vectorCount,
                                                                                                          offset))

    def ReadPattern(self, offset, length):
        return self.DmaRead(offset, length)

    @staticmethod
    def SinglePatternCaptureBaseAddress(patternSize):
        return (
                   patternSize + (
                       4096 - (patternSize % 4096))) // VECTOR_SIZE  # No alignment needed (no need to randomize)

    def SetCaptureBaseAddress(self, captureBaseAddressValue):
        # Setup capture address (this is here the capture data will go)
        # if captureBaseAddress % VECTOR_SIZE != 0:
        #    raise Exception('CaptureBaseAddress must be a multiple of 16, {} is not valid'.format(captureBaseAddress))
        # captureBaseAddressReg = captureBaseAddress()
        self.captureBaseAddress = captureBaseAddressValue
        captureBaseAddressReg = ac_registers.CaptureBaseAddress()
        captureBaseAddressReg.value = captureBaseAddressValue
        self.write_register(captureBaseAddressReg)
        self.Log('debug', 'Setting capture address to 0x{:x} vector'.format(self.captureBaseAddress))

    def SetCaptureCounts(self, captureLength=0xFFFFFFFF, maxCaptureCount=0xFFFFFFFF, maxFailCountPerPatern=0xFFFFFFFF,
                         maxFailCaptureCount=0xFFFFFFFF):
        self.Log('debug', 'CaptureLength = {}'.format(captureLength))
        self.Log('debug', 'MaxCaptureCount = {}'.format(maxCaptureCount))
        self.Log('debug', 'MaxFailCountPerPattern = {}'.format(maxFailCountPerPatern))
        self.Log('debug', 'MaxFailCaptureCount = {}'.format(maxFailCaptureCount))

        captureLengthReg = ac_registers.CaptureLength()
        captureLengthReg.value = captureLength
        self.write_register(captureLengthReg)

        maxCaptureCountReg = ac_registers.MaxCaptureCount()
        maxCaptureCountReg.value = maxCaptureCount
        self.write_register(maxCaptureCountReg)

        maxFailCountPerPaternReg = ac_registers.MaxFailCount()
        maxFailCountPerPaternReg.value = maxFailCountPerPatern
        self.write_register(maxFailCountPerPaternReg)

        maxFailCaptureCountReg = ac_registers.MaxFailCaptureCount()
        maxFailCaptureCountReg.value = maxFailCaptureCount
        self.write_register(maxFailCaptureCountReg)


    def SetCaptureControl(self, captureType, captureFails, captureCTVs, captureAll, stopOnMaxFail,
                          dramOverflowWrap=False, CaptureCTVAfterMaxFail=False):
        # Setup capture mode
        data = self.read_register(ac_registers.CaptureControl)
        data.StorageCaptureType = captureType  # 0: fail status, 1: Pin state, 2: pin direction, 3: masked fail
        if captureFails:
            data.CaptureFails = 1
            self.Log('debug', 'Enabled capture fails')
        else:
            data.CaptureFails = 0
            self.Log('debug', 'Disabled capture fails')
        if captureCTVs:
            data.CaptureCTVs = 1
            self.Log('debug', 'Enabled capture CTVs')
        else:
            data.CaptureCTVs = 0
            self.Log('debug', 'Disabled capture CTVs')
        if captureAll:
            data.CaptureAll = 1
            self.Log('debug', 'Enabled capture all')
        else:
            data.CaptureAll = 0
            self.Log('debug', 'Disabled capture all')
        if stopOnMaxFail:
            data.StopOnMaxFailCaptureCountHit = 1
            self.Log('debug', 'Enable stop on max fail')
        else:
            data.StopOnMaxFailCaptureCountHit = 0
            self.Log('debug', 'Disabled stop on max fail')
        if dramOverflowWrap:
            data.DramOverflowAction = 1
            self.Log('debug', 'DramOverflowAction = 1, wrap results')
        else:
            data.DramOverflowAction = 0
            self.Log('debug', 'DramOverflowAction = 0, stop and alarm if over flow')
        if CaptureCTVAfterMaxFail:
            data.CaptureCTVAfterMaxFail = 1
            self.Log('debug', 'Enabled CaptureCTVAfterMaxFail')
        else:
            data.CaptureCTVAfterMaxFail = 0
            self.Log('debug', 'Disabled CaptureCTVAfterMaxFail')

        if self.internalLoopback:
            data.CaptureVectorData = 1  # Enable internal loopback
            self.Log('debug', 'Enabled internal loopback mode')
        else:
            data.CaptureVectorData = 0  # Disable internal loopback
            self.Log('debug', 'Disable internal loopback mode')

        self.write_register(data)

    def SetupCapture(self, captureBaseAddress, captureType, maxFailCountPerPatern=0xFFFFFFFF,
                     maxFailCaptureCount=0xFFFFFFFF):
        self.SetCaptureBaseAddress(captureBaseAddress)
        self.SetCaptureCounts(maxFailCountPerPatern=maxFailCountPerPatern, maxFailCaptureCount=maxFailCaptureCount)
        self.SetCaptureControl(captureType, self.captureFails, self.captureCTVs, self.captureAll, self.stopOnMaxFail,
                               self.dramOverflowWrap)

    def SetPeriod(self, period):
        if self.minQdrMode == -1:
            self.Log('warning',
                     'It looks like the FPGA capabilities were not loaded (was Initialize() skipped?). Loading them now...')
            self._UpdateFpgaCapabilities()

        self.SetDomainPeriod(period)

    def SetPeriodSubcycleAndInternal(self, subCycleCount, internalPeriod):
        if self.minQdrMode == -1:
            self.Log('warning',
                     'It looks like the FPGA capabilities were not loaded (was Initialize() skipped?). Loading them now...')
            self._UpdateFpgaCapabilities()

        self.SetDomainPeriodSubcycleAndInternal(subCycleCount, internalPeriod)

    def SetPeriodSubcycleZeroAndInternal(self, subCycleCount, internalPeriod):
        if self.minQdrMode == -1:
            self.Log('warning',
                     'It looks like the FPGA capabilities were not loaded (was Initialize() skipped?). Loading them now...')
            self._UpdateFpgaCapabilities()

        self.SetDomainPeriodSubcycleZeroAndInternal(subCycleCount, internalPeriod)

    def PrestagePattern(self, patternStartAddress=0, numberOfTries=500, seMask=0x7FFF):
        self.Log('debug', 'Setting pattern start address to 0x{:x} vectors'.format(patternStartAddress))

        if hpccConfigs.HPCC_ALARM_DEBUG:
            # Print out Alarm Control and Pattern Control
            self.Log('info', 'Before PreStage Alarm Control bits are 0b{0:032b} for ({1}, {2})'.format(
                self.read_register(ac_registers.AlarmControl).value, self.slot, self.slice))
            self.Log('info', 'Before PreStage New Alarm Control bits are 0b{0:032b} for ({1}, {2})'.format(
                self.read_register(ac_registers.AlarmControl).value, self.slot, self.slice))
            patternControl = self.read_register(ac_registers.PatternControl).value
            self.Log('info', 'Before PreStage Pattern Control bits are 0b{0:032b} for ({1}, {2})'.format(patternControl,
                                                                                                         self.slot,
                                                                                                         self.slice))
            dataResetAndStatus = self.read_register(ac_registers.ResetsAndStatus).value
            self.Log('info',
                     'Before PreStage ResetAndStatus bits are 0b{0:032b} for ({1}, {2})'.format(dataResetAndStatus,
                                                                                                self.slot, self.slice))
        patternStartAddressReg = ac_registers.PatternStartAddress()
        patternStartAddressReg.value = patternStartAddress
        self.write_register(patternStartAddressReg)

        # Reset ECC count register
        data = self.read_register(ac_registers.ResetsAndStatus)
        data.ECCCountReset = 1
        self.write_register(data)
        time.sleep(0.01)
        data.ECCCountReset = 0
        self.write_register(data)

        # Reset capture memory (ask Jared)
        data = self.read_register(ac_registers.ResetsAndStatus)
        data.ResetCaptureMemory = 1
        self.write_register(data)
        data.ResetCaptureMemory = 0
        self.write_register(data)

        # Disable pattern control bits
        data = self.read_register(ac_registers.PatternControl)
        if data.PreStagePattern != 0:
            dataAlarm = self.read_register(ac_registers.AlarmControl).value
            self.Log('warning',
                     'Prestage bit already set ({}, {}). Alarm Control bits are {:032b}'.format(self.slot, self.slice,
                                                                                                dataAlarm))
            raise HpccAc.PrestageIsAlreadySet('Prestage bit is already set')
        data.PreStagePattern = 0
        data.StartOnNextRCSyncPulse = 0  # Starts pattern execution (this is the one that we should set)
        data.EnableDisableOutputs = 0
        data.AbortPattern = 0
        data.SyncStartBypass = 0  # debug feature
        data.StickyErrorDomainMask = seMask  # enable GSE for all by default
        self.write_register(data)

        # Toggle prestage
        data.PreStagePattern = 1
        self.write_register(data)
        data.PreStagePattern = 0
        self.write_register(data)
        self.Log('debug', 'Toggling prestage bit to start prestage')

        for i in range(numberOfTries):
            time.sleep(0.01)
            data = self.read_register(ac_registers.PatternControl)
            if data.PreStagePattern == 0:
                break
        if data.PreStagePattern == 0:
            self.Log('debug', 'Prestage finished')
        else:
            self.Log('warning', 'Prestage never finished on slot {}, slice {}).'.format(self.slot, self.slice))
            self.log_all_active_alarms()
            self.Log('error',
                     'Prestage never finished: VectorProcessorStarted={}, PerVectorControlStarted={}, PinRingAcknowledge={}'.format(
                         data.VectorProcessorStarted, data.PerVectorControlStarted, data.PinRingAcknowledge))
            # raise PrestageNeverFinished('Prestage never finished: VectorProcessorStarted={}, PerVectorControlStarted={}, PinRingAcknowledge={}'.format(data.VectorProcessorStarted, data.PerVectorControlStarted, data.PinRingAcknowledge))

        # Stop the clock
        self.ad9914.EnableOutput(False)
        self.Log('debug', 'Stopped the clocks')

        # Enable outputs
        data = self.read_register(ac_registers.PatternControl)
        data.PreStagePattern = 0
        data.StartOnNextRCSyncPulse = 0
        data.EnableDisableOutputs = 1
        data.AbortPattern = 0
        data.SyncStartBypass = 0
        self.write_register(data)
        self.Log('debug', 'Enabled outputs')

        # Enable sync listen
        data = self.read_register(ac_registers.PatternControl)
        data.PreStagePattern = 0
        data.StartOnNextRCSyncPulse = 1
        data.EnableDisableOutputs = 0
        data.AbortPattern = 0
        data.SyncStartBypass = 0
        self.write_register(data)

        # Start the clock
        self.ad9914.EnableOutput(True)
        self.Log('debug', 'Started the clocks')

        if hpccConfigs.HPCC_ALARM_DEBUG:
            # Print out Alarm Control and Pattern Control
            self.Log('info', 'After PreStage Alarm Control bits are 0b{0:032b} for ({1}, {2})'.format(
                self.read_register(ac_registers.AlarmControl).value, self.slot, self.slice))
            self.Log('info', 'After PreStage New Alarm Control bits are 0b{0:032b} for ({1}, {2})'.format(
                self.read_register(ac_registers.AlarmControl1).value, self.slot, self.slice))
            patternControl = self.read_register(ac_registers.PatternControl).value
            self.Log('info', 'After PreStage Pattern Control bits are 0b{0:032b} for ({1}, {2})'.format(patternControl,
                                                                                                        self.slot,
                                                                                                        self.slice))
            dataResetAndStatus = self.read_register(ac_registers.ResetsAndStatus).value

            self.Log('info',
                     'After PreStage ResetAndStatus bits are 0b{0:032b} for ({1}, {2})'.format(dataResetAndStatus, self.slot, self.slice))

    class PrestageIsAlreadySet(Exception):
        pass

    def ExecutePattern(self, patternStartAddress=0, waitForComplete=True, numberOfTries=500):
        self.PrestagePattern(patternStartAddress, numberOfTries)
        # Arm ILAs after pre-stage
        # for i in range(8):
        #    self.ArmIla(i)

        # Send bypass trigger to start pattern execution
        data = self.read_register(ac_registers.PatternControl)
        data.PreStagePattern = 0
        data.StartOnNextRCSyncPulse = 0
        data.EnableDisableOutputs = 0
        data.AbortPattern = 0
        data.SyncStartBypass = 1
        self.write_register(data)

        # Block until pattern execution finishes
        if waitForComplete:
            return self.WaitForCompletePattern(numberOfTries)

    def WaitForCompletePattern(self, numberOfTries=500):
        completed = False
        # print('number of Tries are {}'.format(numberOfTries) )
        for i in range(numberOfTries):
            time.sleep(0.01)
            completed = self.IsPatternComplete()
            if completed:
                break
        if completed:
            self.Log('debug', 'Pattern execution completed for ({}, {})'.format(self.slot, self.slice))
        else:
            self.Log('warning', 'Pattern execution did not complete for ({}, {})'.format(self.slot, self.slice))
            self.CheckAlarms()
        
        read_data_gap = self.read_register(ac_registers.PinDdrStatus).ReadDataGap
        if read_data_gap == 3:
            self.Log('error', 'DDR read data gap = {} (3 or less is an error)'.format(read_data_gap))
        return completed

    def IsPatternComplete(self):
        data = self.read_register(ac_registers.ResetsAndStatus)
        return data.PatternRunning == 0

    def AbortPattern(self, waitForComplete=True, numberOfTries=500):
        self.Log('info', 'Sending pattern abort command')
        self._send_abort_pattern_command()
        if waitForComplete:
            self.WaitForCompletePattern(numberOfTries)
    
    def _send_abort_pattern_command(self):
        '''write to self-clearing pattern abort bit, check that it clears'''
        pattern_control = self.read_register(ac_registers.PatternControl)
        pattern_control.AbortPattern = 1
        self.write_register(pattern_control)
        for retry in range(100):
            pattern_control = self.read_register(ac_registers.PatternControl)
            if pattern_control.AbortPattern == 0:
                break
        else:
            raise HpccAc.PatternAbortError()

    class PatternAbortError(Exception):
        pass

    def CheckAlarms(self, alarmsToMask=[]):
        alarms = self.get_alarms()
        warning_alarms = [name for name, state in alarms.items() if state and name in ac_registers.noncritical_alarms]
        error_alarms = [name for name, state in alarms.items() if state and name in ac_registers.critical_alarms and name not in alarmsToMask]
        
        read_data_gap = self.read_register(ac_registers.PinDdrStatus).ReadDataGap
        read_data_gap_implemented = read_data_gap != 0
        if read_data_gap_implemented and read_data_gap < 8:
            alarms['DdrReadDataGapTooSmall'] = 1
            error_alarms.append('DdrReadDataGapTooSmall')
        
        if alarms['ECCError'] and self.ecc_uncorrectable_error_count() != 0:
            pass
        elif alarms['ECCError'] and self.ecc_correctable_error_count() < 10:
            warning_alarms.append('ECCError')
            error_alarms.remove('ECCError')

        if warning_alarms:
            warning_alarms_string = '\n'.join([' '*20 + x for x in sorted(warning_alarms)])
            self.Log('warning', 'Non-critical alarms:\n{}'.format(warning_alarms_string))
        
        if error_alarms:
            error_alarms_string = '\n'.join([' '*20 + x for x in sorted(error_alarms)])
            self.Log('error', 'Critical alarms:\n{}'.format(error_alarms_string))
        
        if alarms['ECCError']:
            self.log_ecc_errors()
        
        return [name for name, state in alarms.items() if state]

    def ReadCapture(self, block_count=None):
        if block_count is None:
            block_count = self.read_register(ac_registers.TotalCaptureBlockCount).value
        capture_base_address = self.read_register(ac_registers.CaptureBaseAddress).value
        length = block_count * CAPTURE_BLOCK_SIZE
        self.Log('debug', 'Reading {} bytes of capture memory at address=0x{:x}'.format(length,
                                                                                        capture_base_address * VECTOR_SIZE))
        if length > 2*1024*1024*1024:
            self.Log('warning', 'Capture memory exceeds 2GB, reducing capture read to 1MB')
            length = 1024*1024
        if (capture_base_address + (length // VECTOR_SIZE)) > DDR_MEMORY_BOUNDARY:
            raise Exception(
                'Reading {} bytes of capture memory at address=0x{:x}, out of boundary, can cause ECC errors'.format(
                    length, capture_base_address * VECTOR_SIZE))
            return None
        else:
            data = self.DmaRead(capture_base_address * VECTOR_SIZE, length)
            return data

    def UnpackCapture(self, captureData):

        if len(captureData) % CAPTURE_BLOCK_SIZE != 0:
            raise Exception('Invalid capture data length={}, expecting a multiple of {}'.format(len(captureData),
                                                                                                CAPTURE_BLOCK_SIZE))
        blockCount = len(captureData) // CAPTURE_BLOCK_SIZE
        blocks = []
        # Quick and dirty code to decode capture data
        for i in range(blockCount):
            offset = i * CAPTURE_BLOCK_SIZE
            blockData = self._reverse_bytes(captureData[offset:offset + CAPTURE_BLOCK_SIZE])
            # Capture block = (256 bits Capture Header) + (96 bits Capture Data) * 8
            #               = (32 bytes Capture Header) + (12 bytes Capture Data) * 8
            #               = 128 bytes
            header = self.structs.CaptureHeader(bytes=blockData[0:32], length=256)
            data = []
            for j in range(8):
                k = 7 - j
                start = 32 + k * 12
                end = 32 + (k + 1) * 12
                data.append(self.structs.CaptureData(bytes=blockData[start:end], length=96))
            blocks.append([header, data])
        return blocks

    def _reverse_bytes(self, b):
        return b[::-1]

    def ArmIla(self, ilaNumber):
        # Assert reset for the given ILA
        data = self.read_register(ac_registers.IlaControl)
        data.Reset = data.Reset | (1 << ilaNumber)
        self.write_register(data)
        # De-assert reset for the given ILA
        data.Reset = data.Reset & ~(1 << ilaNumber)
        self.write_register(data)
        self.Log('debug', 'Armed ILA[{}] for ({}, {})'.format(ilaNumber, self.slot, self.slice))

    def FireIla(self, ilaNumber):
        data = self.read_register(ac_registers.IlaControl)
        data.BypassTrigger = 1
        data.Select = ilaNumber
        self.write_register(data)

    def UnFireIla(self, ilaNumber):
        data = self.read_register(ac_registers.IlaControl)
        data.BypassTrigger = 0
        data.Select = ilaNumber
        self.write_register(data)

    def GetIlaSamples(self, ilaNumber):
        # Select given ILA
        data = self.read_register(ac_registers.IlaControl)
        data.Select = ilaNumber
        self.write_register(data)
        # Get number of samples to read
        sampleCount = self.read_register(ac_registers.IlaStatus).CaptureCount
        # Read the samples
        samples = []
        self.Log('debug', 'Reading {} samples for ILA[{}]'.format(sampleCount, ilaNumber))
        for i in range(sampleCount):
            data.Sample = i
            # Read low word
            data.WordInSample = 0
            self.write_register(data)
            low = self.read_register(ac_registers.IlaData).Data
            # Read high word
            data.WordInSample = 1
            self.write_register(data)
            high = self.read_register(ac_registers.IlaData).Data
            # Combine high and low
            sample = (high << 32) | low
            samples.append(sample)
        return samples


    def LoadAndArmIlas(self, slot, slice):
        path = None
        if slice == 0:
            path = os.path.dirname(self.hpcc.ac0Image[0])
        elif slice == 1:
            path = os.path.dirname(self.hpcc.ac1Image[0])
        self.LoadIlaNames(path)
        self.Log('debug', 'Loaded ILA names for ({},{}) from {}'.format(slot, slice, path))
        # Comment out to not arm ILAs here
        for i in range(8):
            self.ArmIla(i)

    def DumpIlas(self, slot, slice):
        for i in range(8):
            filename = 'ila_{}_{}_{}.vcd'.format(slot, slice, i)
            self.DumpIla(i, filename)
        self.Log('debug', 'Dumped ILAs for ({},{})'.format(slot, slice))

        
        
    def LoadIlaNames(self, path):
        self.ilaNames = []
        for i in range(8):
            signalNames = {}
            filename = os.path.join(path, 'ila{}.ila'.format(i))
            with open(filename, 'r') as fin:
                reader = csv.reader(fin)
                for row in reader:
                    if len(row) > 0:
                        signalNames[int(row[0])] = row[1]
            self.ilaNames.append(signalNames)
            self.Log('debug',
                     'Loaded {} signal names for ILA[{}]({}, {}) from {}'.format(len(signalNames), i, self.slot,
                                                                                 self.slice, filename))

    def DumpIla(self, ilaNumber, filename):
        creator = vcd.VcdCreator(40, 'ila', 'AC logic analyzer version 0.1')
        for num, signalName in self.ilaNames[ilaNumber].items():
            creator.SetSignalName(num, signalName)
        samples = self.GetIlaSamples(ilaNumber)
        creator.CreateVcd(filename, samples, '1ns')
        self.Log('debug', 'Dumped ILA[{}] for ({}, {}) to {}'.format(ilaNumber, self.slot, self.slice, filename))

    def SetDynamicPhaseDelay(self, attributes, channels='ALL_PINS'):
        if channels == 'ALL_PINS':
            peIndex = globals.ALL_PINS
        elif channels == 'EVEN_PINS':
            peIndex = globals.EVEN_PINS
        elif channels == 'ODD_PINS':
            peIndex = globals.ODD_PINS
        elif isinstance(channels, list):
            peIndex = channels
        else:
            raise Exception('channels {} is not supported.'.format(channels))

        for att in attributes:
            if att.upper() == 'DRIVE':
                self._SetDynamicDriveDelay(attributes[att], peIndex)
            elif att.upper() == 'COMPARE':
                self._SetDynamicCompareDelay(attributes[att], peIndex)
            elif att.upper() == 'TRISE':
                self._SetDynamicTRiseDelay(attributes[att], peIndex)
            elif att.upper() == 'TFALL':
                self._SetDynamicTFallDelay(attributes[att], peIndex)
            else:
                raise Exception('Dynamic Phase Delay attribute {} is not supported'.format(att))

    def _SetDynamicDriveDelay(self, delay, peIndex):
        delayPS = delay * PICO_SECONDS_PER_SECOND
        self.Log('debug', 'set dynamic drive delay = {}ps'.format(delayPS))
        upperLimit = (0xF - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        lowerLimit = (0 - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        self.Log('debug', 'dynamic drive delay limit {} ~ {} ps'.format(lowerLimit, upperLimit))
        if delayPS > upperLimit or delayPS < lowerLimit:
            raise Exception('dynamic drive delay {}ps outside limit {} ~ {} ps'.format(delayPS, lowerLimit, upperLimit))

        for i in peIndex:
            channel = self.hpcc.dc[self.slice].pe[i]
            channelNum = channel.fpgaChannelNum
            cal = self.caldata.channelData[channelNum]
            if channel.isEnabledClock:
                raise Exception('Cannot set dynamic drive delay for enabled clock pin {}'.format(channelNum))

            conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
            conf2 = self.ReadChannel(channelNum, 'PerChannelControlConfig2')
            conf3 = self.ReadChannel(channelNum, 'PerChannelControlConfig3')

            self.Log('debug', 'original fine delay = {}'.format(conf2.OutputFineDelay))
            [dpd, fineDelay] = self._CalculateDPDFineDelay(delayPS, conf2.OutputFineDelay, cal.outputDelays)

            conf1.OutputDynamicPhaseDelay += dpd
            conf1.EnableDynamicPhaseDelay += dpd
            conf2.OutputFineDelay = fineDelay
            conf2.OutputAlternateFineDelay = fineDelay
            conf3.EnableFineDelay = fineDelay

            self.Log('debug', 'drive dpd = {}, fine delay = {}'.format(dpd, fineDelay))

            self.WriteChannel(channelNum, 'PerChannelControlConfig1', conf1)
            self.WriteChannel(channelNum, 'PerChannelControlConfig2', conf2)
            self.WriteChannel(channelNum, 'PerChannelControlConfig3', conf3)

    def _SetDynamicTRiseDelay(self, delay, peIndex):
        delayPS = delay * PICO_SECONDS_PER_SECOND
        self.Log('debug', 'set dynamic tRise delay = {}ps'.format(delayPS))
        upperLimit = (0xF - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        lowerLimit = (0 - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        self.Log('debug', 'dynamic tRise delay limit {} ~ {} ps'.format(lowerLimit, upperLimit))
        if delayPS > upperLimit or delayPS < lowerLimit:
            raise Exception('dynamic tRise delay {}ps outside limit {} ~ {} ps'.format(delayPS, lowerLimit, upperLimit))

        for i in peIndex:
            channel = self.hpcc.dc[self.slice].pe[i]
            channelNum = channel.fpgaChannelNum
            cal = self.caldata.channelData[channelNum]
            if not channel.isEnabledClock:
                raise Exception('Cannot set dynamic tRise delay for non enabled clock pin {}'.format(channelNum))

            conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
            conf2 = self.ReadChannel(channelNum, 'PerChannelControlConfig2')

            reset = conf2.EcREventSelect & 1  # bit 0
            if reset == 0:  # tRise defined by enabled path
                self.Log('debug', 'original alt fine delay = {}'.format(conf2.OutputAlternateFineDelay))
                [dpd, fineDelay] = self._CalculateDPDFineDelay(delayPS, conf2.OutputAlternateFineDelay,
                                                               cal.enableDelays)
                conf1.EnableDynamicPhaseDelay += dpd
                conf2.OutputAlternateFineDelay = fineDelay
            else:
                self.Log('debug', 'original output fine delay = {}'.format(conf2.OutputFineDelay))
                [dpd, fineDelay] = self._CalculateDPDFineDelay(delayPS, conf2.OutputFineDelay, cal.outputDelays)
                conf1.OutputDynamicPhaseDelay += dpd
                conf2.OutputFineDelay = fineDelay

            self.Log('debug', 'tRise dpd = {}, fine delay = {}'.format(dpd, fineDelay))

            self.WriteChannel(channelNum, 'PerChannelControlConfig1', conf1)
            self.WriteChannel(channelNum, 'PerChannelControlConfig2', conf2)

    def _SetDynamicTFallDelay(self, delay, peIndex):
        delayPS = delay * PICO_SECONDS_PER_SECOND
        self.Log('debug', 'set dynamic tFall delay = {}ps'.format(delayPS))
        upperLimit = (0xF - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        lowerLimit = (0 - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        self.Log('debug', 'dynamic tFall delay limit {} ~ {} ps'.format(lowerLimit, upperLimit))
        if delayPS > upperLimit or delayPS < lowerLimit:
            raise Exception('dynamic tFall delay {}ps outside limit {} ~ {} ps'.format(delayPS, lowerLimit, upperLimit))

        for i in peIndex:
            channel = self.hpcc.dc[self.slice].pe[i]
            channelNum = channel.fpgaChannelNum
            cal = self.caldata.channelData[channelNum]
            if not channel.isEnabledClock:
                raise Exception('Cannot set dynamic tFall delay for non enabled clock pin {}'.format(channelNum))

            conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
            conf2 = self.ReadChannel(channelNum, 'PerChannelControlConfig2')

            reset = conf2.EcREventSelect & 1  # bit 0
            if reset == 1:  # tFall defined by enabled path
                self.Log('debug', 'original alt fine delay = {}'.format(conf2.OutputAlternateFineDelay))
                [dpd, fineDelay] = self._CalculateDPDFineDelay(delayPS, conf2.OutputAlternateFineDelay,
                                                               cal.enableDelays)
                conf1.EnableDynamicPhaseDelay += dpd
                conf2.OutputAlternateFineDelay = fineDelay
            else:
                self.Log('debug', 'original output fine delay = {}'.format(conf2.OutputFineDelay))
                [dpd, fineDelay] = self._CalculateDPDFineDelay(delayPS, conf2.OutputFineDelay, cal.outputDelays)
                conf1.OutputDynamicPhaseDelay += dpd
                conf2.OutputFineDelay = fineDelay

            self.Log('debug', 'tFall dpd = {}, fine delay = {}'.format(dpd, fineDelay))

            self.WriteChannel(channelNum, 'PerChannelControlConfig1', conf1)
            self.WriteChannel(channelNum, 'PerChannelControlConfig2', conf2)

    def _SetDynamicCompareDelay(self, delay, peIndex):
        delayPS = delay * PICO_SECONDS_PER_SECOND * -1
        self.Log('debug', 'set dynamic compare delay = {}ps (inversed already)'.format(delayPS))
        upperLimit = (0xF - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        lowerLimit = (0 - DYNAMIC_PHASE_DELAY_BIAS) * self.domainQuarterCycle
        self.Log('debug', 'dynamic compare delay limit {} ~ {} ps'.format(lowerLimit, upperLimit))
        if delayPS > upperLimit or delayPS < lowerLimit:
            raise Exception(
                'dynamic compare delay {}ps outside limit {} ~ {} ps'.format(delayPS, lowerLimit, upperLimit))

        for i in peIndex:
            channel = self.hpcc.dc[self.slice].pe[i]
            channelNum = channel.fpgaChannelNum
            cal = self.caldata.channelData[channelNum]

            conf3 = self.ReadChannel(channelNum, 'PerChannelControlConfig3')
            self.Log('debug', 'original fine delay = {}'.format(conf3.InputFineDelay))
            [dpd, fineDelay] = self._CalculateDPDFineDelay(delayPS, conf3.InputFineDelay, cal.inputDelays)
            conf3.InputDynamicPhaseDelay += dpd
            conf3.InputFineDelay = fineDelay

            self.Log('debug', 'compare dpd = {}, fine delay = {}'.format(dpd, fineDelay))

            self.WriteChannel(channelNum, 'PerChannelControlConfig3', conf3)

            # The method to break down the dynamic phase delay into domainQuarterCycle and Fine delay cycles
            # Arguments: 1. dynamics phase delay time
        #           2. Fine delay cycles set in the delay stacks (input, output and alternate stacks)
        #           3. Cal is the table used to map the fine delay cycles above to delay times (not quite sure)
        # Return: 1. Number of the Domain Quarter Cycles delay for dynamics phase delay
        #        2. Number of the fine delay cycles for dynamic phase delay

    def _CalculateDPDFineDelay(self, delayPS, orginalFineDelay, cal):
        dpd = int(delayPS / self.domainQuarterCycle)
        remaining = delayPS - dpd * self.domainQuarterCycle
        currentFineDelay = cal[orginalFineDelay]
        totalFineDelay = currentFineDelay + remaining
        if totalFineDelay > self.domainQuarterCycle:
            dpd += 1
            totalFineDelay -= self.domainQuarterCycle
        elif totalFineDelay < 0:
            dpd -= 1
            totalFineDelay += self.domainQuarterCycle
        fineDelay = self._CalculateFineDelay(totalFineDelay, cal)
        return [dpd, fineDelay]

    def _ReadAcCalCsvFile(self, filePath):
        with open(filePath) as csvFile:
            csvReader = csv.reader(csvFile)

            # Read CSV Headings into namedtuple Field
            csvHeading = next(csvReader)
            # print('csvHeading is %s' % csvHeading)
            channelNumberIndex = csvHeading.index('ChannelNumber')
            csvHeading.remove('ChannelNumber')
            AcCalVar = namedtuple('AcCalVar', csvHeading)

            for row in csvReader:
                splitedRow = [float(i) for i in row]
                splitedRowExceptChannelCol = [val for idx, val in enumerate(splitedRow) if idx != channelNumberIndex]
                channelNum = int(splitedRow[channelNumberIndex])  # Read Only first Column Value
                self.importedCal[channelNum] = AcCalVar(*splitedRowExceptChannelCol)  # Read rest of columns
                # print('importedCal is {}'.format( self.importedCal ) )

    def _ReadTimingValue(self, filePath):
        result = defaultdict(list)
        with open(filePath) as inf:
            for line in inf:
                name, score = line.split(":", 1)
                score1 = score.split("/", 1)
                for i in score1:
                    result[name].append(float(i))
        self.compareDomainPeriod = result['Domain Period'][0]
        self.driveDomainPeriod = result['Domain Period'][0]
        self.domainQuarterCycle = result['QDR Value'][0]
        self.importedDomainPeriod = result['Domain Period'][0]
        self.importedQDR = result['QDR Value'][0]
        self.importedDriveAdjustValue = result['Drive Adjust Value'][0]
        self.importedCompareAdjustValue = result['Compare Adjust Value'][0]
        self.importedSubCycle = result['Subcyle'][0]
        self.imprtedRatioOne = result['Ratio1'][0]

        # def _GetInputEdge(self, channel):
        # Input Edge deCal and embedding our own CAL value in the input edge

    #    inputEdge = conf4.InputTesterCycleDelay*self.compareDomainPeriod + conf3.InputSubCycleDelay*self.domainQuarterCycle + conf3.InputFineDelay * self.nominalFineDelayValue

    def _RecoverEdgeFromSnapshot(self, channel):
        channelNum = channel.fpgaChannelNum
        cal = self.caldata.channelData[channelNum]

        ## TODO : Read Snapshot Reigster Value from registers.csv first 
        conf1 = self.ReadChannel(channelNum, 'PerChannelControlConfig1')
        conf2 = self.ReadChannel(channelNum, 'PerChannelControlConfig2')
        conf3 = self.ReadChannel(channelNum, 'PerChannelControlConfig3')
        conf4 = self.ReadChannel(channelNum, 'PerChannelControlConfig4')

        # Input Edge deCal and embedding our own CAL value in the input edge
        inputEdge = conf4.InputTesterCycleDelay * self.compareDomainPeriod + conf3.InputSubCycleDelay * self.domainQuarterCycle + conf3.InputFineDelay * self.nominalFineDelayValue
        print(self.importedCal[(channelNum)])
        deCalInputEdge = inputEdge - self.importedCal[channelNum][0]
        emCalInputEdge = inputEdge + cal.inputCal

        # Input Edge recalculation with our own CAL value
        print('channel number is {}'.format(channelNum))
        print('input')
        dpinEdge = self._PlaceInputDPinEdge(emCalInputEdge, cal.inputDelays)
        conf4.InputTesterCycleDelay = dpinEdge.TesterCycleDelay
        print('Input Tester Cycle Delay is {}'.format(conf4.InputTesterCycleDelay))
        conf3.InputSubCycleDelay = dpinEdge.CompareSubCycleDelay
        print('Input Sub Cycle Delay is {}'.format(conf3.InputSubCycleDelay))
        conf3.InputFineDelay = dpinEdge.FineDelay
        print('Input Fine Delay is {}'.format(conf3.InputFineDelay))

        # Output Edge decal and embedding our own Cal Value in the input edge
        outputEdge = conf4.OutputTesterCycleDelay * self.driveDomainPeriod + conf1.OutputSubCycleDelay * self.domainQuarterCycle * 4 + conf1.OutputQdrPhaseSelect * self.domainQuarterCycle + conf2.OutputFineDelay * self.nominalFineDelayValue  # conf2.OutputAlternateFineDelay
        deCalOutputEdge = outputEdge - self.importedCal[channelNum][1]
        emCalOutputEdge = outputEdge + cal.outputCal

        # Output Edge
        print('output')
        dpinEdge = self._PlaceOutputDPinEdge(emCalOutputEdge, cal.outputDelays)
        conf4.OutputTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf1.OutputSubCycleDelay = dpinEdge.DriveSubCycleDelay
        conf1.OutputQdrPhaseSelect = dpinEdge.QdrDelay
        conf2.OutputFineDelay = dpinEdge.FineDelay
        conf2.OutputAlternateFineDelay = dpinEdge.FineDelay

        # Enable Edge, TODO: Check EdgeAdvance and EdgeDelay
        print('enable')
        enableEdge = conf4.EnableTesterCycleDelay * self.driveDomainPeriod + conf1.EnableSubCycleDelay * self.domainQuarterCycle * 4 + conf1.EnableQdrPhaseSelect * self.domainQuarterCycle + conf3.EnableFineDelay * self.nominalFineDelayValue + conf1.EnableEdgeAdvance - conf1.EnableEdgeDelay  # conf2.OutputAlternateFineDelay
        deCalEnableEdge = enableEdge - self.importedCal[channelNum][2]
        emCalEnableEdge = enableEdge + cal.enableCal

        # Enable Edge
        dpinEdge = self._PlaceOutputDPinEdge(emCalEnableEdge, cal.enableDelays)
        dpinEdge.EdgeDelay = 0
        dpinEdge.EdgeAdvance = 0
        conf4.EnableTesterCycleDelay = dpinEdge.TesterCycleDelay
        conf1.EnableSubCycleDelay = dpinEdge.DriveSubCycleDelay
        conf1.EnableQdrPhaseSelect = dpinEdge.QdrDelay
        conf3.EnableFineDelay = dpinEdge.FineDelay
        conf1.EnableEdgeAdvance = dpinEdge.EdgeAdvance
        conf1.EnableEdgeDelay = dpinEdge.EdgeDelay

        # write to registers
        self.WriteChannel(channelNum, 'PerChannelControlConfig1', conf1)
        self.WriteChannel(channelNum, 'PerChannelControlConfig2', conf2)
        self.WriteChannel(channelNum, 'PerChannelControlConfig3', conf3)
        self.WriteChannel(channelNum, 'PerChannelControlConfig4', conf4)
        '''
        print('Compare: {} Drive: {}'.format(channel._compare, channel._drive))
        print(hex(conf1.Pack()))
        print(hex(conf2.Pack()))
        print(hex(conf3.Pack()))
        print(hex(conf4.Pack()))
        '''


    def TakeRegistersSnapshot(self, filename):
        self.Log('debug', 'Taking registers snapshot: \'{}\''.format(filename))
        # NOTE: The following two lines that clear Cal break the replay.py script
        #       because replay.py runs with no Cal
        #for i in range(56):
        #    self._PlaceInputDPinEdgesZeroCal( self.hpcc.dc[self.slice].pe[i] )
        with open(filename, 'w') as fout:
            print('Offset,Data,Name', file=fout)
            for RegisterType in ac_registers.get_register_types_sorted_by_name():
                if RegisterType.REGCOUNT == 1:
                    data = self.read_register(RegisterType).value
                    print('0x{:0x},0x{:0x},{}'.format(RegisterType.ADDR, data, RegisterType.__name__), file=fout)
                else:
                    for ch in range(RegisterType.REGCOUNT):
                        data = self.read_register(RegisterType, ch).value
                        print('0x{:0x},0x{:0x},{}[{}]'.format(RegisterType.address(ch), data, RegisterType.__name__, ch), file=fout)
        
        
    def TakeTimingSnapshot(self, slot, slice, filename):
        self.Log('debug', 'Taking timing snapshot: \'{}\''.format(filename))
        fout = open(filename, 'w')
        hpcc = self.hpcc

        ac = hpcc.ac[slice]
        domainPeriod = Fraction(int(ac.compareDomainPeriod * MINIMUM_PRECISION), MINIMUM_PRECISION)
        QDR = Fraction(int(ac.domainQuarterCycle * MINIMUM_PRECISION), MINIMUM_PRECISION)
        driveAdjust = Fraction(int(ac.driveAdjust * MINIMUM_PRECISION), MINIMUM_PRECISION)
        compareAdjust = Fraction(int(ac.compareAdjust * MINIMUM_PRECISION), MINIMUM_PRECISION)
        fout.write('Domain Period: {}/{}\n'.format(domainPeriod.numerator, domainPeriod.denominator))        
        fout.write('QDR Value: {}/{}\n'.format(QDR.numerator, QDR.denominator))
        fout.write('Drive Adjust Value: {}/{}\n'.format(driveAdjust.numerator, driveAdjust.denominator))
        fout.write('Compare Adjust Value: {}/{}\n'.format(compareAdjust.numerator, compareAdjust.denominator))
        fout.write('Subcycle: {}\n'.format(ac.Read('PatternControl').SubCycleCount))
        fout.write('Ratio1: {}\n'.format(int(ac.ratioOneTiming)))
                
        fout.close()
        
        
    def TakeCalSnapshot(self, slot, slice, fileName) :
        hpcc = self.hpcc
        fileLH0 = os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_ac_cal_lh.txt".format(hpcc.instrumentblt.SerialNumber, slot, 0))
        fileLH1 = os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_ac_cal_lh.txt".format(hpcc.instrumentblt.SerialNumber, slot, 1))
        fileEO0 = os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_ac_cal_eo.txt".format(hpcc.instrumentblt.SerialNumber, slot, 0))
        fileEO1 = os.path.join(self.CALIBRATION_PATH, r"{}_{}_{}_ac_cal_eo.txt".format(hpcc.instrumentblt.SerialNumber, slot, 1))
        fileSnapshot = fileName
        # User Story 25393:[FVAL]: FVAL failed to generate Snapshot file - Start
        self.ReturnCalCsvFromMemory(slot, slice, fileName)  # self.ReturnCalCsv( fileEO1 , fileSnapshot )

        
    def ReturnCalCsvFromMemory(self, slot,slice, fileName):
        with open(fileName, 'w') as csvfile:
            hpcc = self.hpcc
            spam = csv.writer(csvfile, lineterminator='\n')
            spam.writerow(['ChannelNumber','InputCal','OutputCal','EnableCal'] )
            for i in range(56):
                inputCal1 = (hpcc.ac[slice].caldata.channelData[i].inputCal)
                outputCal1 = (hpcc.ac[slice].caldata.channelData[i].outputCal)
                enableCal1 = (hpcc.ac[slice].caldata.channelData[i].enableCal)
                spam.writerow([i, inputCal1, outputCal1, enableCal1])
    
    def TakePatternMemorySnapshot(self, slot, slice, offset, length, path):  # offset and length are in bytes
        self.Log('debug', 'Taking pattern memory snapshot: \'{}\''.format(path))
        filename = 'Pattern_Type0x{:x}_Id0x{:x}_Offset0x{:x}.bin'.format(0, 0, offset // VECTOR_SIZE)
        hpcc = self.hpcc
        data = hpcc.ac[slice].ReadPattern(offset, length)
        fout = open(os.path.join(path, filename), 'wb')
        fout.write(data)
        fout.close()
        
        
    def TakeCaptureMemorySnapshot(self, slot, slice, path):
        hpcc = self.hpcc
        self.Log('debug', 'Taking capture memory snapshot: \'{}\''.format(path))
        captureData, blockCount, captureCount = hpcc.ReadCapture(slice)
        filename = 'Pattern_Type0x2_Id0x0_Offset0x{:x}.bin'.format(hpcc.ac[slice].captureBaseAddress) # in vector addr
        fout = open(os.path.join(path, filename), 'wb')
        fout.write(captureData)
        fout.close()        
        
        
    def TransferSimTraces(self, slot, slice, path):
        if os.path.isfile('io_state_jam_trace_{}_{}.txt'.format(slot, slice)):
            shutil.move('io_state_jam_trace_{}_{}.txt'.format(slot, slice), os.path.join(path, 'io_state_jam_trace.txt'))
        if os.path.isfile('instruction_trace_{}_{}.txt'.format(slot, slice)):
            shutil.move('instruction_trace_{}_{}.txt'.format(slot, slice), os.path.join(path, 'instruction_trace.txt'))
        if os.path.isfile('capture_trace_{}_{}.txt'.format(slot, slice)):
            shutil.move('capture_trace_{}_{}.txt'.format(slot, slice), os.path.join(path, 'capture_trace.txt'))

    def TransferIlas(self, slot, slice, path):
        for i in range(8):
            if os.path.isfile('ila_{}_{}_{}.vcd'.format(slot, slice, i)):
                shutil.move('ila_{}_{}_{}.vcd'.format(slot, slice, i), os.path.join(path, 'ila_{}.vcd'.format(i)))