################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: ad9914.py
#-------------------------------------------------------------------------------
#     Purpose: Class for clock ad9914
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/17/15
#       Group: HDMT FPGA Validation
################################################################################

from fractions import Fraction

from Common.instruments.base import Instrument
import Hpcc.instrument.hpccAcRegs as ac_registers
from Hpcc.instrument import ad9914_registers

PICO_SECONDS_PER_SECOND = 1000000000000
MINIMUM_PRECISION = 10

class Ad9914(Instrument):
    def __init__(self, hpcc, isHP, name = None):
        super(Ad9914, self).__init__(name)
        self._hpcc = hpcc # so that the clock can access methods in hpcc class
        self._slot = hpcc.slot
        self._slice = hpcc.slice
        self._isHP = isHP # is high purity clock or not
        if self._isHP:
            self.CONTROL = 'AD9914Control_HP'
            self.WR_DATA = 'AD9914WRData_HP'
            self.RD_DATA = 'AD9914RDData_HP'
        else:
            self.CONTROL = 'AD9914Control'
            self.WR_DATA = 'AD9914WRData'
            self.RD_DATA = 'AD9914RDData'
        
    def Initialize(self):
        cfr1 = ad9914_registers.ControlFunction1Register(uint = 0x00010008, length = 32)
        cfr1.ExternalPowerDownControl = 1
        cfr1.AutoclearPhaseAccumulator = 0
        cfr1.ClearPhaseAccumulator = 1
        cfr1.EnableSineOutput = 1
        cfr1.VcoCalEnable = 1
        self._Write(ad9914_registers.CONTROL_FUNCTION1_ADDR, cfr1)
        cfr1.VcoCalEnable = 0
        self._Write(ad9914_registers.CONTROL_FUNCTION1_ADDR, cfr1)

        usrReg = ad9914_registers.UsrRegister(uint = 0x00000800, length = 32)
        usrReg.CalWithSync = 0
        self._Write(ad9914_registers.USR0_ADDR, usrReg)

        cfr4 = ad9914_registers.ControlFunction4Register(uint = 0x00052120, length = 32)
        cfr4.DacCalEnable = 1 # Initiate an auto DAC calibration
        self._Write(ad9914_registers.CONTROL_FUNCTION4_ADDR, cfr4)
        cfr4.DacCalEnable = 0 # Manually clear
        self._Write(ad9914_registers.CONTROL_FUNCTION4_ADDR, cfr4)

        cfr2 = ad9914_registers.ControlFunction2Register(uint = 0x00000900, length = 32)
        cfr2.ProfileModeEnable = 1 
        cfr2.ProgramModulusEnable = 1 
        cfr2.MatchedLatencyEnable = 0
        cfr2.FrequencyJumpEnable = 0
        cfr2.DrgOverOutputEnable = 0
        cfr2.SyncClkEnable = 1        
        cfr2.SyncClkInvert = 0
        cfr2.DigitalRampDestination = 0x0
        cfr2.DigitalRampEnable = 1
        self._Write(ad9914_registers.CONTROL_FUNCTION2_ADDR, cfr2)
        
        cfr3 = ad9914_registers.ControlFunction3Register(uint = 0x0000191C, length = 32)
        self._Write(ad9914_registers.CONTROL_FUNCTION3_ADDR, cfr3)
        
        self._Write(ad9914_registers.PROFILE0_FREQUENCY_TUNING_WORD_ADDR, 0)
        self._Write(ad9914_registers.PROFILE0_PHASE_AMPLITUDE_ADDR, 0)
        
        self.WritePeriod(8e-9)
        
    def WritePeriod(self, period):
        PERIOD_SOURCE = Fraction(1000,3) # 3 GHz, 333.33ps
        periodFraction = Fraction(int(period * PICO_SECONDS_PER_SECOND * MINIMUM_PRECISION), MINIMUM_PRECISION)
        # FTW + A/B = (fo*2^32)/fs
        # Since A/B < 1, the first term is just the integer and fraction part of the equation
        # First, find fo/fs (round fo to nearest hz) (fo is 1/period)
        # That is equivalent to ps/po * 2^32
        ratio = Fraction((PERIOD_SOURCE / periodFraction) * (1 << 32))
        ftw = int(ratio)
        ratio -= ftw
        a = ratio.numerator
        b = ratio.denominator

        #Write frequency registers
        self.Log('debug', 'period = {}, ftw/lowLimit = {}, b/upLimit = {}, a/stepSize = {}'.format(period, ftw, b, a))
        self._Write(ad9914_registers.DIGITAL_RAMP_LOWER_LIMIT_ADDR, ftw);
        #self._Read(self._hpcc.clockRegs.DIGITAL_RAMP_LOWER_LIMIT_ADDR)
        self._Write(ad9914_registers.DIGITAL_RAMP_UPPER_LIMIT_ADDR, b);
        self._Write(ad9914_registers.RISING_DIGITAL_RAMP_STEP_SIZE_ADDR, a);
        
    def StartClock(self):
        self.WritePeriod(8e-9)
        self.EnableOutput(True)
        
    def EnableOutput(self, enable):
        cfr1 = ad9914_registers.ControlFunction1Register(uint = 0x00010008, length = 32)
        cfr1.ExternalPowerDownControl = 1
        # By disabling auto clear phase accumulator and enabling clear phase accumulator, we lock the phase low
        # Once we de-assert the clear, the phase will start again, cleanly
        cfr1.AutoclearPhaseAccumulator = 0        
        if enable:
            cfr1.ClearPhaseAccumulator = 0
        else:
            cfr1.ClearPhaseAccumulator = 1
        cfr1.EnableSineOutput = 1
        cfr1.VcoCalEnable = 0
        self._Write(ad9914_registers.CONTROL_FUNCTION1_ADDR, cfr1)
        
    def SetDelay(self, delay):
        r = ad9914_registers.ProfilePhaseAmplitudeRegister()
        r.phase_offset = delay
        r.amplitude_scale_factor = 0 # not used except in OSK mode
        self._Write(r.offset(profile=0), r)
    def read_delay(self):
        r = ad9914_registers.ProfilePhaseAmplitudeRegister()
        return self._Read(r.offset(profile=0))
    def _Read(self, address):
        regAD9914Control = self._hpcc.Read('AD9914Control')
        regAD9914Control.Address = address
        regAD9914Control.RdEnable = 1
        regAD9914Control.EnableAction = 1        
        self._hpcc.Write(self.CONTROL, regAD9914Control)
        
        for attempt in range(self._hpcc.NUMBER_TRIES):
            if self._hpcc.Read(self.CONTROL).ReadValueReturned:
                break
        else:
            self.Log('error', 'Read Ad9914 address 0x{:X} for hpcc {} slice {} Failed'.format(address, self._slot, self._slice))
            return -1
        return self._hpcc.Read(self.RD_DATA)
    
    def _Write(self, address, data):
        if not isinstance(data, int):
            data = data.Pack()
        regAD9914WRData = ac_registers.AD9914WRData(data)
        self._hpcc.Write(self.WR_DATA, regAD9914WRData)      
        
        regAD9914Control = self._hpcc.Read('AD9914Control')
        self.Log('debug','Data in AD9914Control register: {}'.format(hex(regAD9914Control.Pack())))
        regAD9914Control.Address = address
        regAD9914Control.RdEnable = 0
        regAD9914Control.EnableAction = 1
        
        #self.Log('debug','Writing to register: {}'.format(self.CONTROL))
        
        self._hpcc.Write(self.CONTROL, regAD9914Control)
        
        for attempt in range(self._hpcc.NUMBER_TRIES):
            if self._hpcc.Read(self.CONTROL).Ad9914AccessComplete:
                break
        else:
            self.Log('error', 'Write 0x{:X} to Ad9914 address 0x{:X} for hpcc {} slice {} Failed. Tried {} times.'.format(data, address, self._slot, self._slice, attempt))
            
    
