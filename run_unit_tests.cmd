@REM INTEL CONFIDENTIAL
@REM
@REM Copyright 2020 Intel Corporation.
@REM
@REM This software and the related documents are Intel copyrighted materials,
@REM and your use of them is governed by the express license under which they
@REM were provided to you ("License"). Unless the License provides otherwise,
@REM you may not use, modify, copy, publish, distribute, disclose or transmit
@REM this software or the related documents without Intel's prior written
@REM permission.
@REM
@REM This software and the related documents are provided as is, with no express
@REM or implied warranties, other than those that are expressly stated in the
@REM License.

@REM Run unit tests using the python included with FVAL
@REM Must be run from directory this script is in

ThirdParty\python\python.exe run_tests.py %*
