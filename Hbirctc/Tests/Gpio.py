# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""FPGA has General Purpose Input/Output pins

FPGA has Pcie Bar registers to provide status or control of GPIO pins
"""

from math import ceil, isclose
from random import getrandbits
from time import clock, sleep, time

from Common.fval import skip, SkipTest
from Common.utilities import compliment_bits, format_docstring,\
    negative_one_bits
from Hbirctc.Tests.HbirctcTest import HbirctcTest


class Diagnostics(HbirctcTest):
    """Test communication of registers"""
    TEST_ITERATIONS = 1000

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Diagnostics.TEST_ITERATIONS
        self.max_fail_count = 10

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedGeneralPurposeInputRegisterReadTest(self):
        """Read register and confirm stable value

        Register fields (bit number - description):
        8 - System 1.8V Power Good: 1 for up, 0 for down
        7 - Run 1.8V-48V BST: Value of the FPGA input pin "Run 1.8V-48V BST".
        6 - Handler Present In: Value of the FPGA input pin
        "Handler Present In".
        5:4	- Spare GPI: Value of bits 2 and 3 of the FPGA input/output bus
        "Spare GPIO"
        3:0	LV BM Ver: Value of the FPGA input bus "LV BM Ber".

        Note: This is a read only register
        Re-read register {num_iterations} times and verify its stable
        """
        initial_value = self.hbirctc.general_purpose_input()

        for iteration in range(self.test_iterations):
            temp = self.hbirctc.general_purpose_input()
            if temp != initial_value:
                self.Log('error', self.error_msg(iteration, initial_value,
                                                 temp))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedGeneralPurposeOutputRegisterReadTest(self):
        """Verify default value of register fields

        Register fields (bit number - description):
        5 - System Fail Safe: Controls the state of the FPGA output pin
        "System Fail Safe".
        4 - RCTC Ground Fault Detection: Controls the state of the FPGA output
        pin "RCTC Ground Fault Detection".
        3 - RCTC TOS Ready: Controls the state of the FPGA output pin
        "RCTC TOS Ready".
        2 - Handler Present Out: Controls the state of the FPGA output pin
        "Handler Present Out".
        1:0 - Spare GPO: Controls the state of bits 0 and 1 of the FPGA
        input/output bus "Spare GPIO".

        Test steps:
        1) Verify default value of register
        2) Repeat {num_iterations} times
        """
        expected_default = 0
        initial_value = self.hbirctc.general_purpose_output()
        if initial_value != expected_default:
            self.Log('error', self.error_msg_default(
                name=self.hbirctc.registers.GENERAL_PURPOSE_OUTPUT().name(),
                expected=expected_default,
                actual=initial_value))

        for iteration in range(self.test_iterations):
            temp = self.hbirctc.general_purpose_output()
            if temp != initial_value:
                self.Log('error', self.error_msg(iteration, initial_value,
                                                 temp))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def error_msg_default(self, name, expected, actual):
        return f'{name} default value failure (expected, actual) : ' \
               f'0x{expected:08X}, 0x{actual:08X}'

    def error_msg(self, iteration, expected, actual):
        return f'Iteration {iteration}) Invalid read (expected, actual): ' \
               f'0x{expected:08X}, 0x{actual:08X}'


class Functional(HbirctcTest):
    """Test operation of registers"""
    TEST_ITERATIONS = 100

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Functional.TEST_ITERATIONS
        self.max_fail_count = 10

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedFpgaDebugFlippingBitsTest(self):
        """ Verify operation of bi-directional interface

        The control field sets the direction of the fpga_debug bus one bit at a
        time.
        Control bit is 1: Output field bit is reflected on input field bit
        Control bit is 0: Input field bit reflects external connection

        Test steps:
        1) Get the default input field value by setting control to all input
        2) Set output field to inverse of default input
        3) Perform a walking one's on the control field. Sets bit to output one
        bit at a time
        4) Verify corresponding input field bits toggle
        5) Repeat steps 3 and 4 a total of {num_iterations} times
        """
        debug_num_bits = 8

        # get default input field's value when control is set to all input
        self.hbirctc.write_fpga_debug_control(0x00)
        self.fpga_debug_test_wait_seconds()
        default_input = self.hbirctc.fpga_debug_input()

        if default_input != negative_one_bits(debug_num_bits):
            raise SkipTest(f'Unable to test due to floating input signal(s). '
                           f'0x{default_input:02X}')

        # set the output field's value to compliment of default input
        output_value = compliment_bits(default_input, debug_num_bits)
        self.hbirctc.write_fpga_debug_data(output_value)

        # calculate expected input field values
        expected_values = self.flipping_bits_expected_input_values(
            default_input, debug_num_bits)

        # send control field to output one bit at a time
        for iteration in range(self.test_iterations):
            success = True

            for bit_num in range(debug_num_bits):
                self.hbirctc.write_fpga_debug_control(1 << bit_num)
                self.fpga_debug_test_wait_seconds()

                actual = self.hbirctc.fpga_debug_input()
                expected = expected_values[bit_num]
                if actual != expected:
                    control = self.hbirctc.fpga_debug_control()
                    self.Log('error',
                             self.error_msg_flipping_bits(
                                 iteration, expected, actual, control)
                             )
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def flipping_bits_expected_input_values(self, default_input, num_bits):
        out_compliment = compliment_bits(default_input, num_bits)

        expected = []
        for i in range(num_bits-1, -1, -1):
            if (out_compliment >> i) & 1:
                expected.append(default_input | (out_compliment & (1 << i)))
            else:
                f_s = negative_one_bits(num_bits)
                expected.append(default_input & (f_s ^ 1 << i))
        expected.reverse()
        return expected

    def error_msg_flipping_bits(self, iteration, expected, actual, control):
        return f'Iteration {iteration}: Invalid input field ' \
               f'(expected, actual) 0b{expected:08b}, 0b{actual:08b}, ' \
               f'control: 0b{control:08b}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedFpgaDebugOutputToInputTest(self):
        """ Verify transfer of data from output to input

        1) Set direction control to all output
        2) Write randomized data to output data
        3) Verify input data matches output data
        4) Repeat steps 2 and 3 {num_iterations} times
        """
        for iteration in range(self.test_iterations):
            success = True
            success &= self.output_control_enabled_scenarion(iteration)
            success &= self.output_control_disabled_scenario(iteration)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def generate_random_byte(self, exclude):
        data = getrandbits(8)
        while data == exclude:
            data = getrandbits(8)
        return data

    def output_control_enabled_scenarion(self, iteration):
        self.hbirctc.write_fpga_debug_control(0xFF)
        self.fpga_debug_test_wait_seconds()
        expected_data = self.generate_random_byte(0xFF)
        self.hbirctc.write_fpga_debug_data(expected_data)
        self.fpga_debug_test_wait_seconds()
        actual = self.hbirctc.fpga_debug_input()
        if actual != expected_data:
            self.Log('error', self.error_msg_output_enabled(
                iteration, expected_data, actual))
            return False
        else:
            return True

    def error_msg_output_enabled(self, iteration, expected, actual):
        return f'Iteration {iteration}: Output to Input should match ' \
               f'(expected, actual): 0x{expected:02X}, 0x{actual:02X}'

    def output_control_disabled_scenario(self, iteration):
        self.hbirctc.write_fpga_debug_control(0x00)
        self.fpga_debug_test_wait_seconds()
        exclude = self.hbirctc.fpga_debug_input()

        # expected_data should be a random number with exclude bits masked out
        expected_data = self.generate_random_byte(exclude)
        expected_data = self.clear_bits(expected_data,
                                        data_length=8, bits_to_clear=exclude)

        self.hbirctc.write_fpga_debug_data(expected_data)
        self.fpga_debug_test_wait_seconds()
        actual = self.hbirctc.fpga_debug_input()
        if actual == expected_data:
            self.Log('info', f'exclude: 0x{exclude:02X}')
            self.Log('error', self.error_msg_output_disabled(
                iteration, expected_data, actual))
            return False
        else:
            return True

    def clear_bits(self, data, data_length, bits_to_clear):
        data &= ~bits_to_clear
        data &= int(str('1') * data_length, 2)  # data &= 0xFFFF
        return data

    def error_msg_output_disabled(self, iteration, expected, actual):
        return f'Iteration {iteration}: Output to Input should not match ' \
               f'(expected, actual): 0x{expected:02X}, 0x{actual:02X}'

    def fpga_debug_test_wait_seconds(self):
        sleep(0.025)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedFpgaLedWriteReadTest(self):
        """Verify FPGA Led BAR register's R/W access

        Record default value of register.
        Write random data to the register, read it back, and verify match.
        Repeat a total of {num_iterations} times.
        """
        default = self.hbirctc.fpga_led()

        try:
            for iteration in range(self.test_iterations):
                expected = self.generate_random_led_control()
                self.hbirctc.write_fpga_led(expected)
                actual = self.hbirctc.fpga_led()

                if actual != expected:
                    self.Log('error', self.error_msg_mismtach(
                        iteration, expected, actual))
                    self.update_failed_iterations(success=False)

                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()
        finally:
            self.hbirctc.write_fpga_led(default)
            actual = self.hbirctc.fpga_led()
            if actual != default:
                self.Log('error', self.error_msg_default('FPGA_LED',
                                                         default, actual))

    def generate_random_led_control(self):
        led_num_bits = 8
        return getrandbits(led_num_bits)

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTriggerInterruptEnableRandomFifoCountRegisterTest(self):
        """Verify Trigger interrupts by sending random number of triggers

        NOTE: Enable Trigger Interrupts from Interrupt Control Register. Also,
        FPGA Software FIFO size is 1024
        1) Send random number, between 0 and 1024, of software triggers to FPGA
        2) Verify trigger interrupt bit is set
        3) Empty software FIFO and verify trigger interrupt is cleared
        4) Repeat above steps a total of {num_iterations} times
        """

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTriggerInterruptDisableStatusRegisterTest(self):
        """Verify Trigger interrupts with interrupts disabled

        NOTE: Disable Trigger Interrupts from Interrupt Control Register
        1) Send a random number of software trigger to FPGA
        2) Verify trigger interrupt bit is clear
        3) Empty software FIFO
        4) Repeat above steps a total of {num_iterations} times
        """

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPowerOnCountRegisterTest(self):
        """Validate Power On Count against FPGA image load time

        1) Read the Power On Count register
        2) Calculate time since calling Fpga image load and convert to a count
        3) Validate counts from above steps are within acceptable range
        4) Wait 10 ms
        5) Repeat above steps a total of {num_iterations} times
        """

        self.skip_test_if_no_image_load()

        time_2_count_factor = 0.01
        self.count_delta = 120 + 10  # about 1.2 seconds of delta

        for iteration in range(self.test_iterations):
            actual_count = self.hbirctc.power_on_count()
            expected_count = self.expected_power_on_count()
            self.sleep_seconds(time_2_count_factor)
            if not isclose(a=expected_count, b=actual_count,
                           abs_tol=self.count_delta):
                self.Log('error',
                         self.error_msg_power_on(iteration=iteration,
                                                 expected=expected_count,
                                                 actual=actual_count))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def skip_test_if_no_image_load(self):
        if self.hbirctc._time_after_fpga_load is None:
            raise SkipTest(f'Image load was not called. Skipping test.')

    def expected_power_on_count(self):
        time_2_count_factor = 0.01
        time_after_fpga_load = self.hbirctc.time_after_fpga_load()
        current_time = time()
        expected_count = ceil(abs(time_after_fpga_load - current_time) /
                              time_2_count_factor)
        return expected_count

    def sleep_seconds(self, seconds):
        start_time = clock()
        while (clock() - start_time) < seconds:
            pass

    def error_msg_power_on(self, iteration, expected, actual):
        actual_delta = abs(expected - actual)
        return f'Iteration {iteration}) ' \
               f'Expected Power On Count delta exceeds expected range.\n' \
               f'\tPower On Count (expected, actual):' \
               f'{expected}, {actual}.\n' \
               f'\tDelta (expected, actual): {self.count_delta}, {actual_delta}'

    def error_msg_mismtach(self, iteration, expected, actual):
        return f'Iteration {iteration}) Mismatch (expected, actual): ' \
               f'{hex(expected)}, {hex(actual)}'

    def error_msg_default(self, name, expected, actual):
        return f'{name} default value failure (expected, actual) : ' \
               f'{hex(expected)}, {hex(actual)}'
