# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

""" FPGA Alarms interface monitors system alarms

Interface is made up of PCIe BAR registers:
- Backplane BPS Alarms register: Monitors the SMALT_L alarm signal of the power
supplies
"""

# from Common.fval import skip
# from Common.utilities import format_docstring
# from Hbirctc.Tests.HbirctcTest import HbirctcTest
#
#
# TEST_ITERATIONS = 100
#
#
# class BackplaneBpsDiagnostics(HbirctcTest):
#     """Test alarm signal monitoring of the BPS"""
#
#     def setUp(self, tester=None):
#         super().setUp(tester)
#         self.test_iterations = TEST_ITERATIONS
#         self.max_fail_count = 10
#
#     @skip('Unable to implement test because limit registers can not be
#     written to.')
#     @format_docstring(num_iterations=TEST_ITERATIONS)
#     def DirectedMonitor_SMALT_L_Test(self):
#         """Validate each BPS alarm signal
#
#         1) Trigger an alarm at the BPS by adjusting a sensor setting limit
#         2) Verify receipt of triggered alarm at the alarms register
#         3) Repeat test a total of {num_iterations} times
#         """
#
#         # 1) Read VOUT_OV_FAULT_LIMIT = 0x40 and record default value
#         # 2) Read READ_VOUT = 0x8B and record current vout
#         # 3) Set VOUT_OV_FAULT_LIMIT < READ_VOUT
#         # 4) Verfiy alarm
