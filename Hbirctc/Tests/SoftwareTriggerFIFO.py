# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""FPGA has a Software Trigger FIFO Interface

Trigger link partners can send the FPGA software type triggers. These triggers
are received at the Software Trigger FIFO interface.
"""

from Common.fval import skip
from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 100
SW_TRIGGER_FIFO_DEPTH = 1024


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 2

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS,
                      fifo_depth=SW_TRIGGER_FIFO_DEPTH)
    def DirectedSendSoftwareTriggerReadTest(self):
        """Verify receipt of Software Triggers

        1) Send random number of software type triggers, or RTOS card type,
        from available trigger link instruments
        2) Verify count, and all data sent with its instrument source from FIFO
        3) Repeat above steps a total of {num_iterations} times
        """
