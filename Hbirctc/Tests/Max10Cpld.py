# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBI has a MAX10 CPLD

FPGA has a SPI interface to the CPLD

Interface register definitions:
- Command Data: Sets up Read/Write transaction along with address and data fields
- Read Data: Holds data received after a read transaction
- Status: Provides Busy and Interrupt signals, and RX an TX FIFO counters
- Control: Provides a reset to the interface. Clears Status register fields
"""

import random
from time import sleep

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common.utilities import format_docstring
import Hbirctc.instrument.max10_register as max10_register
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 10000
MAX_FAIL_COUNT = 10


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedCpldReadExhaustiveTest(self):
        """Test communication interface by reading CPLD revision

        Verify receive data by reading a known value repeatedly
        1) Perform an initial read of the CPLD revision register
        2) Read revision again and compare it to initial read
        3) Repeat step 2 a total of {num_iterations} times
        """

        reference_maj, reference_min, reference_pcb = \
            self.hbirctc.max10.read_fw_revision()

        for iteration in range(self.test_iterations):
            observed_maj, observed_min, observed_pcb = \
                self.hbirctc.max10.read_fw_revision()

            if [reference_maj, reference_min, reference_pcb] != \
                    [observed_maj, observed_min, observed_pcb]:
                self.Log('error',
                         f'Iteration_{iteration} reference(maj, min, pcb), '
                         f'observed(maj, min, pcb): '
                         f'(0x{reference_maj:02X}, 0x{reference_min:0X}, '
                         f'0x{reference_pcb:0X}), '
                         f'(0x{observed_maj:02X}, 0x{observed_min:0X}, '
                         f'0x{observed_pcb:0X})')
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedCpldWriteReadScratchRegisterTest(self):
        """Write random data and read it back

        1) Write random data to MAX10 scratch register.
        2) Read data back and compare with data written
        3) Repeat above steps a total of {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            expected_data = random.randint(0, 0xFFFF)
            self.hbirctc.max10.write_rctc_scratch_pad(expected_data)
            actual_data = self.hbirctc.max10.rctc_scratch_pad()

            if expected_data != actual_data:
                self.fail_count += 1
                self.Log('error',
                         f'Iteration_{iteration} (expected, actual): '
                         f'0x{expected_data:04X}, 0x{actual_data:04X}')

        self.validate_iterations()


class Functional(HbirctcTest):
    """Test operation of communication interface"""
    TEST_ITERATIONS = 100
    MAX_FAIL_COUNT = 10

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Functional.TEST_ITERATIONS
        self.max_fail_count = Functional.MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedCpldInterruptSignalTest(self):
        """Test CPLD interrupt signal

        The interface monitors INT0 of the CPLD and reports its value at the
        STATUS register
        1) Trigger an INT0 via MAX10 Interrupt register
        2) Make sure Status Register detected interrupt
        3) Clear interrupt and make sure Status Register clears error status
        4) Repeat above steps a total of {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            RCTC_INT_LINES = max10_register.RCTC_INT_LINES(value=0)
            RCTC_INT_LINES.debug_set_INT0 = 1
            RCTC_INT_LINES.debug_drive_INT0 = 1

            self.hbirctc.max10.write_word(max10_register.RCTC_INT_LINES.ADDR,
                                          RCTC_INT_LINES.value)
            interrupt_on = self.hbirctc.get_cpld_status_interrupt_bit() == 1

            RCTC_INT_LINES.value = 0
            self.hbirctc.max10.write_word(max10_register.RCTC_INT_LINES.ADDR,
                                          RCTC_INT_LINES.value)
            interrupt_off = self.hbirctc.get_cpld_status_interrupt_bit() == 0

            if not (interrupt_on and interrupt_off):
                self.fail_count += 1
                self.Log('error',
                         f'Iteration_{iteration} interrupt enable: '
                         f'{interrupt_on}, interrupt cleared: {interrupt_off}')

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedCpldRxFifoCountTest(self):
        """Verify interface's RX FIFO counter

        1) Perform a directed number of reads (0, max/2, max) from the CPLD
        2) Make sure RX FIFO counter field of the Status Register reflects
        number of data bytes sent from step 1
        3) Reset interface and confirm counter is zero
        4) Repeat above steps a total of {num_iterations} times
        """
        max_fifo_count = 1024

        for num_reads in [0, max_fifo_count / 2, max_fifo_count - 1]:
            self.fail_count = 0
            num_reads = int(num_reads)

            with self.subTest(FIFO_COUNT=num_reads):
                for iteration in range(self.test_iterations):
                    success = self.run_rx_fifo_count_test(iteration,
                                                        num_reads)
                    self.update_failed_iterations(success)
                    if self.fail_count >= self.max_fail_count:
                        break

                self.validate_iterations()

    def run_rx_fifo_count_test(self, iteration, num_reads):
        scratch_offset = max10_register.RCTC_SCRATCH_PAD.ADDR
        success = True

        with suppress_sensor('FVAL DirectedCpldRxFifoCountTest',
                             sensor_names='HBIMainBoard'):
            for read in range(num_reads):
                self.hbirctc.max10.send_read_command(scratch_offset)
            self.hbirctc.max10.send_nop_command()
            self.hbirctc.max10.wait_on_cpld_busy()
            rx_fifo_count = self.hbirctc.max10.rx_fifo_count()
            self.hbirctc.max10.reset_fifos()
            rx_fifo_count_after_reset = self.hbirctc.max10.rx_fifo_count()

        if rx_fifo_count != num_reads:
            success = False
            self.Log('error', self.error_msg_fifo_count_mismatch(
                iteration, num_reads, rx_fifo_count))

        if rx_fifo_count_after_reset != 0:
            success = False
            self.Log('error', self.error_msg_fifo_reset(iteration,
                                                        rx_fifo_count))
        return success

    def error_msg_fifo_count_mismatch(self, iteration, expected_count,
                                      actual_count):
        return f'Iteration {iteration}) RX FIFO count mismatch ' \
               f'(expected, actual): {expected_count}, {actual_count}'

    def error_msg_fifo_reset(self, iteration, rx_fifo_count):
        return f'Iteration {iteration}) RX FIFO did not reset (count): ' \
               f'{rx_fifo_count}'
