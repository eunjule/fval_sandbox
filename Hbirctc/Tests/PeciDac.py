# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBI has a Platform Environment Control Interface (PECI) DAC

RCTC FPGA has a SPI interface to the DAC (AD5684RBRUZ)

Interface register definitions:
- Command Data: Contains 24-bit command and data that needs to be sent to the DAC
- Status: Provides Busy signals, and a 24-bit Read Data field
- Control: Provides a reset to the DAC. Also can drive the DAC's LDAC pin
"""

from random import getrandbits

from Common.utilities import format_docstring
from Hbirctc.instrument.peci_dac import PECI_DAC_CHANNELS
from Hbirctc.Tests.HbirctcTest import HbirctcTest


class Diagnostics(HbirctcTest):
    """Test communication robustness"""
    TEST_ITERATIONS = 100

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Diagnostics.TEST_ITERATIONS
        self.max_fail_count = 10
        self.peci_dac = self.hbirctc.peci_dac

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPeciDacWriteReadExhaustiveTest(self):
        """Write data to the DAC registers and read it back

        1) Record default values of the registers
        2) Write random data to all four registers, read it back and confirm
        match
        3) Repeat above steps {num_iterations} times
        """
        default_values = [self.peci_dac.dac_output(channel)
                          for channel in PECI_DAC_CHANNELS]

        try:
            for channel in PECI_DAC_CHANNELS:
                self.fail_count = 0

                with self.subTest(PECI_DAC_CHANNEL=channel):
                    for iteration in range(self.test_iterations):
                        expected_data = self.generate_random_dac_value()
                        self.peci_dac.write_dac(channel, expected_data)
                        self.peci_dac.wait_on_busy()
                        actual_data = self.peci_dac.dac_output(channel)

                        if expected_data != actual_data:
                            self.Log('error', self.error_msg_data_mismatch(
                                iteration, expected_data, actual_data))
                            self.update_failed_iterations(success=False)

                        if self.fail_count >= self.max_fail_count:
                            break

                    self.validate_iterations()
        finally:
            for i in range(len(default_values)):
                self.peci_dac.write_dac(PECI_DAC_CHANNELS[i],
                                        default_values[i])

    def generate_random_dac_value(self):
        dac_bit_size = 12
        return getrandbits(dac_bit_size)

    def error_msg_data_mismatch(self, iteration, expected_data, actual_data):
        return f'Iteration {iteration}) Update DAC mismatch ' \
               f'(expected, actual): 0x{expected_data:03X}, 0x{actual_data:03X}'
