# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""FPGA has a Trigger Sync Interface

FPGA can generate synchronized pulses to trigger link partners 0 to 18.
Link Partners:
    Trigger Link 18 - Debug Trigger Link. 
    Trigger Link 17 - Ring Multiplier FPGA.
    Trigger Link 16 - Pattern Generator FPGA.
    Trigger Link 15 - PSDB1 DPS 7.
    Trigger Link 14 - PSDB1 DPS 6.
    Trigger Link 13 - PSDB1 DPS 5.
    Trigger Link 12 - PSDB1 DPS 4.
    Trigger Link 11 - PSDB1 DPS 3.
    Trigger Link 10 - PSDB1 DPS 2.
    Trigger Link 9 - PSDB1 DPS 1.
    Trigger Link 8 - PSDB1 DPS 0.
    Trigger Link 7 - PSDB0 DPS 7.
    Trigger Link 6 - PSDB0 DPS 6.
    Trigger Link 5 - PSDB0 DPS 5.
    Trigger Link 4 - PSDB0 DPS 4.
    Trigger Link 3 - PSDB0 DPS 3.
    Trigger Link 2 - PSDB0 DPS 2.
    Trigger Link 1 - PSDB0 DPS 1.
    Trigger Link 0 - PSDB0 DPS 0.
"""

from Common.fval import skip
from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest


class Functional(HbirctcTest):
    """Test operation of the interface"""
    TEST_ITERATIONS = 10000

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Functional.TEST_ITERATIONS

    @skip('Not implemented')
    @format_docstring(times=TEST_ITERATIONS)
    def DirectedTriggerSyncPulseTest(self):
        """Send sync pulses to trigger link partners

        1) Send a sync pulse to one partner at a time and verify receipt at the
           partner
        2) Repeat above steps a total of {times} times
        """


class TimeStampReset(HbirctcTest):
    """Test operation of the time stamp reset of the trigger sync register"""
    TEST_ITERATIONS = 1000

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TimeStampReset.TEST_ITERATIONS

    @format_docstring(times=TEST_ITERATIONS)
    def DirectedTimeStampResetTest(self):
        """Reset the Live Stamp Timer

        1) Reset Live Stamp Timer
        2) Wait for Live Stamp Timer to be greater than 1 second
        3) Send a reset via the Trigger Sync register
        4) Verify the Live Stamp Timer register has a value < 100 us
        5) Repeat above steps a total of {times} times
        """
        initial_timer_value_us = self.generate_initial_timer_value_us()
        timer_after_reset_us = self.generate_timer_value_after_reset_max_us()

        for iteration in range(self.test_iterations):
            success = True

            self.hbirctc.reset_live_time_stamp_timer()

            if not self._wait_on_timer_value(initial_timer_value_us):
                success = False
                self.Log('error',
                         self.error_msg_wait_on_timer_value(
                             iteration, initial_timer_value_us))

            self.hbirctc.reset_live_time_stamp_timer()

            time_stamp = self.hbirctc.live_time_stamp()
            if time_stamp >= timer_after_reset_us:
                success = False
                self.Log('error',
                         self.error_msg_time_stamp_reset(iteration,
                                                         time_stamp,
                                                         timer_after_reset_us))

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def generate_initial_timer_value_us(self):
        return 1000

    def generate_timer_value_after_reset_max_us(self):
        return 200

    def error_msg_wait_on_timer_value(self, iteration, wait_on_value):
        current_time_stamp = self.hbirctc.live_time_stamp()
        return f'Iteration {iteration}: Time stamp value timed out at ' \
               f'{current_time_stamp:0.2f}us while waiting to increment to ' \
               f'{wait_on_value} us.'

    def error_msg_time_stamp_reset(self, iteration, time_stamp, max_value):
        return f'Iteration {iteration}: Time stamp did not reset ' \
               f'(current value, allowable read value range after reset): ' \
               f'{time_stamp}us, 0 to {max_value}us'

    def _wait_on_timer_value(self, value, num_retries=1000):
        for retry in range(num_retries):
            time_stamp = self.hbirctc.live_time_stamp()
            if time_stamp >= value:
                return True
        else:
            return False