# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBI has 12 fan controllers (MAX6650) accessed by three FPGA i2c masters

Fan Controller  | MAX6650
Interface #     | Chip #
------------------------------------------------
0               | 0, 1, 2, 3
1               | 4, 5, 6, 7
2               | 8, 9, 10, 11
"""

import random

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common.fval import skip
from Common.testbench.i2c_tester import address_nak_test,\
    tx_fifo_count_error_bit_test, tx_fifo_count_test
from Common.utilities import format_docstring
from Hbirctc.instrument.fan_controller_i2c_interface import MAX_NUM_CHIPS,\
    create_interfaces
from Hbirctc.instrument.max6650_register import SPEED_REG, CONFIG_REG,\
    ConfigModes
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 10


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedWriteReadExhaustiveTest(self):
        """Write random value to a scratch register and read it back

        1) Write random value to a scratch register of a MAX6650
        2) Read the scratch register and compare with value written
        3) Repeat steps 1 and 2 for all MAX6650 chips
        4) Repeat steps above steps {num_iterations} times
        """
        self.validate_chip_modes()
        data_width = 8

        for iteration in range(self.test_iterations):
            success = True

            with suppress_sensor(
                    'FVAL DirectedFanControllerWriteReadExhaustiveTest',
                    sensor_names=['HBIMainBoard', 'HBIDTB']):
                for chip_num in range(MAX_NUM_CHIPS):
                    expected_data = random.getrandbits(data_width)
                    self.write_speed_reg(chip_num, expected_data)
                    actual_data = self.read_speed_reg(chip_num)

                    if expected_data != actual_data:
                        self.Log('error',
                                 f'Iteration_{iteration} Fan({chip_num}). '
                                 f'RW (Expected, Actual):'
                                 f' {expected_data:X}, {actual_data:X}')
                        success = False

                self.update_failed_iterations(success)

                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()

    def validate_chip_modes(self):
        # Verify Max6650 configuration register is not in closed-loop mode
        # before using SPEED register as a scratch pad.
        # When in closed mode, SPEED register is used for setting the period
        # of the tach signal that controls the interface
        # speed
        for chip_num in range(len(self.interfaces)):
            if self.read_fan_mode(chip_num) == ConfigModes.CLOSED_LOOP:
                self.Log('error', self.invalid_chip_mode_msg(chip_num))

    def invalid_chip_mode_msg(self, chip_num):
        return f'Fan({chip_num}): Unable to perform read/write test because ' \
               f'configuration mode is closed-loop'

    def write_speed_reg(self, chip_num, data):
        self.interfaces[chip_num].write(SPEED_REG.ADDR, data)

    def read_speed_reg(self, chip_num):
        return self.interfaces[chip_num].read(SPEED_REG.ADDR)

    def read_fan_mode(self, chip_num):
        return self.interfaces[chip_num].read(CONFIG_REG.ADDR)


class Functional(HbirctcTest):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than
        two bytes
        1) Create an invalid byte sized message and verify error bit is set
        2) Create a valid byte sized message verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor(
                        'FVAL '
                        'DirectedFanControllerTxFifoCountErrorBitCheckingTest',
                        sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msg = tx_fifo_count_error_bit_test(interface)
                    if error_msg:
                        [self.Log('error', msg) for msg in error_msg]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        1) Create a message with an invalid address and verify error bit is set
        2) Create a message with an valid address and verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor(
                        'FVAL DirectedFanControllerAddressNakTest',
                        sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msgs = address_nak_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        1) Perform 20 random number of data pushes onto the FIFO
        2) Perform a push of corner cases 0 and max fifo size.
        3) Verify Transmit FIFO Count matches total data sent at each push
        4) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            with suppress_sensor('FVAL DirectedFanControllerTxFifoCountTest',
                                 sensor_names=['HBIMainBoard', 'HBIDTB']):
                for interface in self.interfaces:
                    error_msgs = tx_fifo_count_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedResetTxFifoTest(self):
        """Check FIFO reset feature of the interface

        1) Fill TX FIFO with data and verify appropriate fifo count
        2) Reset FIFO, and verify FIFO count is zero
        3) Repeat a total of {num_iterations} times
        """
