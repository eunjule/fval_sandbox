# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBI has three LTM4680 Power Regulators

The FPGA has an i2c master for each LTM4680

Monitor | LTM4680    | Description
Index   | Designator |
----------------------------------
0       | U198       | RCTC Power Regulator
1       | U8         | Patgen Power Regulator
2       | U87        | RM Power Regulator
"""

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common.fval import expected_failure, skip
from Common.testbench.i2c_tester import\
    address_nak_test, tx_fifo_count_error_bit_test, tx_fifo_count_test
from Common.utilities import format_docstring
from Hbirctc.instrument.pmbus_monitor_i2c_interface import create_interfaces,\
    PmBusMonitorInterface
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 10


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @expected_failure('HDMT Ticket 110498:Hbi MB Fab_D LTM4680 i2c access '
                      'issue using FPGA i2c bus access')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedReadExhaustiveTest(self):
        """Read a subset of known value registers

        Read value of read-only PMBus device registers like Capability and
        Revision and compare them to expected value
        1) Read first read-only register and compare to expected value
        2) Repeat step one for remaining read-only registers
        3) Repeat a total of {num_iterations} times
        """
        registers = PmBusMonitorInterface.KNOWN_VALUE_REGISTERS
        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor(
                        'FVAL DirectedPmbusMonitorWriteReadExhaustiveTest',
                        sensor_names=['HBIMainBoard', 'HBIDTB']):
                    for command, expected_data in registers.items():
                        actual_data = interface.read(command)

                        if expected_data != actual_data:
                            self.Log(
                                'error',
                                self.exhaustive_read_error_msg(
                                    iteration,
                                    interface.interface_name,
                                    expected_data,
                                    actual_data))
                            success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def exhaustive_read_error_msg(self, iteration, name, expected_data,
                                  actual_data):
        return f'Iteration_{iteration}: {name} failed read test ' \
               f'(Expected, Actual): 0x{expected_data:02X}, ' \
               f'0x{actual_data:02X}'


class Functional(HbirctcTest):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than
        two bytes
        1) Create an invalid byte sized message and verify error bit is set
        2) Create a valid byte sized message verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor(
                        'FVAL DirectedPmbusMonitorTxFifoCountErrorBitChecking'
                        'Test', sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msg = tx_fifo_count_error_bit_test(interface)
                    if error_msg:
                        [self.Log('error', msg) for msg in error_msg]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        1) Create a message with an invalid address and verify error bit is set
        2) Create a message with an valid address and verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor('FVAL DirectedPmbusMonitorAddressNakTest',
                                     sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msgs = address_nak_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        1) Perform 20 random number of data pushes onto the FIFO
        2) Perform a push of corner cases 0 and max fifo size.
        3) Verify Transmit FIFO Count matches total data sent at each push
        4) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            with suppress_sensor('FVAL DirectedPmbusMonitorTxFifoCountTest',
                                 sensor_names=['HBIMainBoard', 'HBIDTB']):
                for interface in self.interfaces:
                    error_msgs = tx_fifo_count_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedResetTxFifoTest(self):
        """Check FIFO reset feature of the interface

        1) Fill TX FIFO with data and verify appropriate fifo count
        2) Reset FIFO, and verify FIFO count is zero
        3) Repeat a total of {num_iterations} times
        """
