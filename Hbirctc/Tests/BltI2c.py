# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBI connects to two PSDBs each with four I2C masters.

The FPGA contains the 8 i2c masters. This module will use the TIU PCA9505
devices to test I2C communication.

BLT Device  | BLT Device
Index       |
-------------------------
0           | PSDB0: BLT 0
1           | PSDB0: BLT 1
2           | PSDB0: BLT 2
3           | PSDB0: BLT 3
4           | PSDB1: BLT 0
5           | PSDB1: BLT 1
6           | PSDB1: BLT 2
7           | PSDB1: BLT 3
"""

from random import getrandbits
from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common.fval import skip
from Common.testbench.i2c_tester import tx_fifo_count_error_bit_test,\
    tx_fifo_count_test
from Common.utilities import format_docstring
from Hbirctc.instrument.blt_i2c_interface import create_interfaces,\
    MAX_NUM_BLT
from Hbirctc.Tests.HbirctcTest import HbirctcTest


class Diagnostics(HbirctcTest):
    """Test communication robustness"""
    TEST_ITERATIONS = 1000

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Diagnostics.TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedWriteReadExhaustiveTest(self):
        """Test I2C write/read transactions

        Write a random byte to each of eight PCA9505 on the TIUs. Read it back
        and compare with data written.
        Repeat test {num_iterations} times.
        """
        with suppress_sensor(
                'FVAL BltI2c DirectedWriteReadExhaustiveTest',
                sensor_names=['HBIMainBoard', 'HBIDTB']):
            for interface in self.interfaces:
                psdb_num = int(interface.index / MAX_NUM_BLT)
                blt_num = interface.index % MAX_NUM_BLT
                self.fail_count = 0
                default_value = interface.read_pca9505_output_0()

                with self.subTest(INTERFACE=f'PSDB{psdb_num}_BLT{blt_num}'):
                    try:
                        for iteration in range(self.test_iterations):
                            expected = self.generate_random_byte()
                            interface.write_pca9505_output_0(expected)
                            actual = interface.read_pca9505_output_0()

                            if actual != expected:
                                self.Log('error', self.error_msg(iteration,
                                                                 expected, actual))
                                self.update_failed_iterations(success=False)

                            if self.fail_count >= self.max_fail_count:
                                break

                        self.validate_iterations()

                    finally:
                        interface.write_pca9505_output_0(default_value)

    def generate_random_byte(self):
        return getrandbits(8)

    def error_msg(self, iteration, expected, actual):
        return f'Iteration {iteration}) Data mismatch (expected, actual): ' \
               f'{hex(expected)}, {hex(actual)}'


class Functional(HbirctcTest):
    """Test operation of the i2c master"""
    TEST_ITERATIONS = 10

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Functional.TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than
        two bytes
        1) Create an invalid byte sized message and verify error bit is set
        2) Create a valid byte sized message verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor('FVAL DirectedPsdbBltAddressNakTest',
                                     sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msg = tx_fifo_count_error_bit_test(interface)
                    if error_msg:
                        [self.Log('error', msg) for msg in error_msg]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        1) Create a message with an invalid address and verify error bit is set
        2) Create a message with an valid address and verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """
        # 10/24/2020: Why am I not using address_nak_test(interface)?

        # num_blts = self.hbirctc.registers.BLT_I2C_STATUS.REGCOUNT
        # invalid_i2c_addr = 1
        #
        # for iteration in range(self.test_iterations):
        #     success = True
        #
        #     for blt_num in range(num_blts):
        #         with suppress_sensor('FVAL DirectedPsdbBltAddressNakTest',
        #                              sensor_names=['HBIMainBoard', 'HBIDTB']):
        #             # Clear status
        #             self.reset_status(blt_num)
        #             if self.is_address_naked(blt_num):
        #                 success = False
        #                 self.Log(
        #                     'error',
        #                     self.error_msg_address_nak(
        #                         iteration, blt_num,
        #                         expected=False, actual=True))
        #
        #             # Write invalid address
        #             self.push_data_onto_fifo(invalid_i2c_addr, blt_num)
        #
        #             # Write don't care data
        #             self.push_data_onto_fifo(1, blt_num)
        #
        #             # transmit packet
        #             self.transmit_data(blt_num)
        #
        #             # Validate address_nak
        #             if not self.is_address_naked(blt_num):
        #                 success = False
        #                 self.Log(
        #                     'error',
        #                     self.error_msg_address_nak(
        #                         iteration, blt_num,
        #                         expected=True, actual=False))
        #
        #             # Clear status
        #             self.reset_status(blt_num)
        #
        #     self.update_failed_iterations(success)
        #     if self.fail_count >= self.max_fail_count:
        #         break
        #
        # self.validate_iterations()

    def reset_status(self, blt_num):
        self.hbirctc.write_bar_register(
            self.hbirctc.registers.BLT_I2C_RESET(reset=1), blt_num)
        self.hbirctc.write_bar_register(
            self.hbirctc.registers.BLT_I2C_RESET(reset=0), blt_num)
        self.wait_on_busy(blt_num)

    def push_data_onto_fifo(self, data, blt_num):
        self.hbirctc.write_bar_register(
            self.hbirctc.registers.BLT_I2C_TX_FIFO(data=data), blt_num)

    def transmit_data(self, blt_num):
        self.hbirctc.write_bar_register(
            self.hbirctc.registers.BLT_I2C_CONTROL(transmit=1), blt_num)
        self.wait_on_busy(blt_num)

    def is_address_naked(self, blt_num):
        address_nak = self.hbirctc.read_bar_register(
            self.hbirctc.registers.BLT_I2C_STATUS, blt_num).address_nak
        return address_nak == 1

    def wait_on_busy(self, blt_num, num_retries=10000):
        for retry in range(num_retries):
            reg = self.hbirctc.read_bar_register(
                self.hbirctc.registers.BLT_I2C_STATUS, blt_num)
            if not reg.i2c_busy and (reg.transmit_fifo_count == 0):
                break
        else:
            raise Functional.WaitOnBusyException(
                f'PSDB{blt_num % 2}_BLT{blt_num % 4}: '
                f'Timed out waiting on busy signal')

    def error_msg_address_nak(self, iteration, blt_num, expected, actual):
        return f'Iteration {iteration}: PSDB{blt_num % 2}_BLT{blt_num % 4} - '\
               f'Address Nak (expected, actual) {expected}, {actual}'

    class WaitOnBusyException(Exception):
        pass

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        1) Perform 20 random number of data pushes onto the FIFO
        2) Perform a push of corner cases 0 and max fifo size.
        3) Verify Transmit FIFO Count matches total data sent at each push
        4) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            with suppress_sensor('FVAL DirectedPsdbBltTxFifoCountTest',
                                 sensor_names=['HBIMainBoard', 'HBIDTB']):
                for interface in self.interfaces:
                    error_msgs = tx_fifo_count_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedResetTxFifoTest(self):
        """Check FIFO reset feature of the interface

        1) Fill TX FIFO with data and verify appropriate fifo count
        2) Reset FIFO, and verify FIFO count is zero
        3) Repeat a total of {num_iterations} times
        """
