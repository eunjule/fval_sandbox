# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The FPGA provides 8 scratch registers.

The PCIe hard IP in the FPGA has a 256-bit data bus. Registers are accessed
by DWORD (32-bits). Based on the offset of the PCIe register, different
ranges of wires in the 256-bit bus will be used for the 32-bit data.
"""

from random import getrandbits

from Common.utilities import format_docstring
from Hbirctc.instrument.hbirctc_register import SCRATCH_REG
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 10000
MAX_FAIL_COUNT = 10


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedScratchRegisterWriteReadExhaustiveTest(self):
        """Verify write/read capability of all eight Scratch Registers

        1) Write 32-bit random data to a register and read it back
        2) Verify data written is equal to data read
        3) Repeat above test {num_iterations} times for each register
        """

        expected_list = self.generate_expected_list()

        for reg_num in range(SCRATCH_REG.REGCOUNT):
            with self.subTest(SCRATCH_REG=str(reg_num)):
                self.validate_scratch_register(expected_list, reg_num)

    def validate_scratch_register(self, expected_list, reg_num):
        self.fail_count = 0
        for iteration in range(self.test_iterations):
            expected = expected_list[iteration]
            reg = self.hbirctc.registers.SCRATCH_REG(value=expected)

            self.hbirctc.write_bar_register(reg, reg_num)
            actual = self.read_scratch_register(reg_num)

            if expected != actual:
                self.Log('error',
                         self.error_msg(iteration, reg_num, expected, actual))
                self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def generate_expected_list(self):
        expected_list = [getrandbits(32) for i in range(self.test_iterations)]
        expected_list[0] = 0xFFFF_FFFF
        expected_list[self.test_iterations - 1] = 0
        return expected_list

    def read_scratch_register(self, reg_num):
        return self.hbirctc.read_bar_register(
            self.hbirctc.registers.SCRATCH_REG, reg_num).value

    def error_msg(self, iteration, reg_num, expected_value, actual_value):
        return f'Iteration {iteration}. Scratch_{reg_num} (expected, actual):'\
               f' 0x{expected_value:08X}, 0x{actual_value:08X}'
