# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Monitor the power output of the Backplane Bulk Power Supplies

The HBI tester has a maximum power threshold.  If the tester sustains a power
draw greater than that threshold for an extended time period, the bulk power
supplies will shut down without warning.  The System Power Monitoring (SPM)
feature provides the HBI tester the ability to gracefully handle this
situation. The goal of this feature is to detect this scenario before it
happens and safe state DUTs to keep the tester from powering off.

This module will test the following System Power Monitor (SPM) RC interfaces:
- SPM Interrupt
- SPM Streaming
- SPM Configuration and Status
- SPM Data
- SPM Alarms
"""

from itertools import product
from math import isclose, isnan
from random import getrandbits, shuffle
from threading import Thread
from time import perf_counter

from ThirdParty.SASS.suppress_sensor import sass

from Common.fval import skip, SkipTest
from Common.instruments.s2h_interface import S2hBufferIndex, S2hInterface
from Common.Tests.Sensor2HostStreaming import Functional as S2hFunctional
from Common.utilities import format_docstring, negative_one_bits,\
    performance_sleep
from Hbirctc.instrument.spm_data_types import SPM_DATA_TYPES
from Hbirctc.instrument.spm_interface import SpmInterface
from Hbirctc.instrument.spm_scopeshot_interface import SpmScopeshotInterface
from Hbirctc.Tests.HbirctcTest import HbirctcTest


def skip_test_if_fpga_not_spm_capable():
    if not SpmInterface.is_system_power_monitoring_present():
        raise SkipTest(f'Skipping test due to FPGA not being System '
                       f'Power Monitoring capable.')


class SpmInterrupt(HbirctcTest):
    """SPM interrupt is a PCIe interrupt that is triggered on an SPM Alarm

    The interface to be tested consists of two BAR registers:
    - Interrupt Control: SPM interrupt enabling
    - Interrupt Status: Provides the alert that the interrupt was triggered
    """
    TEST_ITERATIONS = 100

    def setUp(self, tester=None):
        super().setUp(tester)
        skip_test_if_fpga_not_spm_capable()
        self.test_iterations = SpmData.TEST_ITERATIONS
        self.max_fail_count = SpmData.MAX_FAIL_COUNT
        self.spm = SpmInterface(self.Log)
        self.spm.initialize_limits(bps_2=1, bps_1=1, bps_0=1)
        self.sass_api = sass.SassApi()
        self.sass_api.Connect('FVAL SpmInterrupt')
        self.sass_api.SuppressSensor('HBIPowerSupply')

    def tearDown(self):
        self.sass_api.ResumeSensor('HBIPowerSupply')
        self.sass_api.Disconnect()
        super().tearDown()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPoutAlarmPCIeInterruptTest(self):
        """Validate SPM Pout PCIe interrupt alarm

        Set POUT Limit threshold to an appropriate value to trigger a POUT
        alarm. Verify SPM PCIe interrupt was set at the Interrupt Status
        FPGA BAR register and at the PCIe driver. Repeat test but with
        interrupts disabled and verify interrupt is not set.

        Repeat the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for present_choice in present_choices:
                    interrupt_enabled = True
                    expected_alarm = \
                        (present_choice[2] << 2 | present_choice[1] << 1 |
                         present_choice[0]) if interrupt_enabled else 0

                    self.run_pcie_interrupt_alarm_scenario(
                        present_choice, 'pout', interrupt_enabled)
                    success &= self.validate_pcie_interrupt_alarm(
                        iteration, present_choice, interrupt_enabled,
                        expected_alarm)

                for present_choice in present_choices:
                    interrupt_enabled = False
                    expected_alarm = 0

                    self.run_pcie_interrupt_alarm_scenario(
                        present_choice, 'pout', interrupt_enabled)
                    success &= self.validate_pcie_interrupt_alarm(
                        iteration, present_choice, interrupt_enabled,
                        expected_alarm)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPoutSumAlarmPCIeInterruptTest(self):
        """Validate SPM Pout Sum PCIe interrupt alarm

        Set POUT SUM Limit threshold to an appropriate value to trigger a POUT
        SUM alarm. Verify SPM PCIe interrupt was set at the Interrupt Status
        FPGA BAR register and at the PCIe driver. Repeat test but with
        interrupts disabled and verify interrupt is not set.

        Repeat the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for present_choice in present_choices:
                    interrupt_enabled = True
                    expected_alarm = 1 << 3

                    self.run_pcie_interrupt_alarm_scenario(
                        present_choice, 'pout_sum', interrupt_enabled)
                    success &= self.validate_pcie_interrupt_alarm(
                        iteration, present_choice, interrupt_enabled,
                        expected_alarm)

                for present_choice in present_choices:
                    interrupt_enabled = False
                    expected_alarm = 0

                    self.run_pcie_interrupt_alarm_scenario(
                        present_choice, 'pout_sum', interrupt_enabled)
                    success &= self.validate_pcie_interrupt_alarm(
                        iteration, present_choice, interrupt_enabled,
                        expected_alarm)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def run_pcie_interrupt_alarm_scenario(self, present_choice,
                                          sensor_name,
                                          interrupt_enabled=True,
                                          timeout_secs=0.001):
        self.spm.initialize_limits(present_choice[0], present_choice[1],
                                   present_choice[2])
        self.spm.clear_spm_pout_alarm()
        self.spm.enable_pcie_interrupt(int(interrupt_enabled))

        default_limit = getattr(self.spm, f'{sensor_name}_limit')()
        fault_limit = default_limit / 2
        getattr(self.spm, f'set_{sensor_name}_limit')(fault_limit)
        self.spm.wait_on_data_refresh()

        Thread(target=self.spm.pcie_driver_alarm_wait).start()

        self.spm.pcie_alarm_lock.acquire(blocking=True, timeout=timeout_secs)
        self.spm.pcie_driver_alarm_wait_cancel()

        if self.spm.pcie_alarm_lock.locked():
            self.spm.pcie_alarm_lock.release()

        self.spm.wait_on_data_refresh()
        self.spm.initialize_limits(present_choice[0], present_choice[1],
                                   present_choice[2])

    def validate_pcie_interrupt_alarm(self, iteration, present_choice,
                                      interrupt_enabled, expected_alarm):
        success = True

        self.spm.wait_on_data_refresh()
        interrupt_status = bool(self.spm.pcie_interrupt())
        self.spm.clear_spm_pout_alarm()

        if bool(self.spm.pcie_interrupt()):
            success = False
            self.Log('error', self.error_msg_clear_interrupt(iteration,
                                                             present_choice))

        if present_choice == (0, 0, 0) or not interrupt_enabled:
            if interrupt_status:
                success = False
                self.Log('error', self.error_msg_interrupt_status(
                    iteration, present_choice, False, True))

            pcie_alarm = self.spm_pcie_alarm()
            if pcie_alarm != 0:
                success = False
                self.Log('error', self.error_msg_pcie_driver_alarm(
                    iteration, present_choice, 0, pcie_alarm))

        else:
            if not interrupt_status:
                success = False
                self.Log('error', self.error_msg_interrupt_status(
                    iteration, present_choice, True, False))

            pcie_alarm = self.spm_pcie_alarm()
            if pcie_alarm != expected_alarm:
                success = False
                self.Log('error', self.error_msg_pcie_driver_alarm(
                    iteration, present_choice, expected_alarm, pcie_alarm))

        return success

    def spm_pcie_alarm(self):
        return self.spm.pcie_alarm

    def error_msg_clear_interrupt(self, iteration, present_choice):
        return f'Iteration {iteration}) BPS Present (0, 1, 2): {present_choice}'\
                f' Unable to clear interrupt'

    def error_msg_interrupt_status(self, iteration, present_choice, expected,
                                   actual):
        return f'Iteration {iteration}) BPS Present (0, 1, 2): {present_choice}'\
               f' Interrupt Status (expected, actual): {expected}, {actual}'

    def error_msg_pcie_driver_alarm(self, iteration, present_choice, expected,
                                    actual):
        return f'Iteration {iteration}) BPS Present (0, 1, 2): {present_choice}'\
               f' PCIe Driver Alarm (expected, actual): {expected}, {actual}'


class SpmStreaming(HbirctcTest):
    """SPM has a Sensor to Host streaming interface

    The interface consists of a host of streaming configuration and
    characterization BAR registers.
    """
    TEST_ITERATIONS = int(1365 / 2)  # total packets in stream is 1365

    def setUp(self, tester=None):
        super().setUp(tester)
        skip_test_if_fpga_not_spm_capable()
        self.test_iterations = SpmStreaming.TEST_ITERATIONS
        self.max_fail_count = 10
        self.spm = SpmInterface(self.Log)
        self.s2h = S2hInterface(self.hbirctc, S2hBufferIndex.SPM)
        self.test_class = S2hFunctional(
            stream=S2hInterface(self.hbirctc, S2hBufferIndex.SPM),
            rc_test=self)
        self.sass_api = sass.SassApi()
        self.sass_api.Connect('FVAL SpmStreaming')
        self.sass_api.SuppressSensor('HBIPowerSupply')

    def tearDown(self):
        self.sass_api.ResumeSensor('HBIPowerSupply')
        self.sass_api.Disconnect()
        super().tearDown()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedValidatePacketTest(self):
        """Validate packet contents within the stream

        Reset stream and enable. While reading stream packets:
        - Confirm packet header contents update accordingly
        - Confirm POUT SUM is approximately the sum of the POUT DATA entries
        - Confirm nan pout values if BPS disabled
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """
        present_choices = self.generate_present_choices()

        try:
            self.s2h.reset_stream()
            self.s2h.enable_stream()
            self.spm.enable_spm()

            for present_choice in present_choices:
                with self.subTest(BPS_PRESENT=present_choice,
                                  SPM_ENABLE=1):
                    self.spm.initialize_limits(bps_0=present_choice[0],
                                               bps_1=present_choice[1],
                                               bps_2=present_choice[2])
                    self.test_class.DirectedValidatePacketTest()

            with self.subTest(BPS_PRESENT=present_choice, SPM_ENABLE=0):
                self.spm.disable_spm()
                self.spm.initialize_limits(bps_0=present_choice[0],
                                           bps_1=present_choice[1],
                                           bps_2=present_choice[2])
                self.test_class.DirectedValidatePacketTest()

        finally:
            self.s2h.disable_stream()
            self.spm.disable_spm()

    def generate_present_choices(self):
        return list(product([0, 1], repeat=SpmInterface.NUM_BPS))

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedStreamEnableUsingBarRegisterTest(self):
        """Validate enabling of S2H stream using the S2H Control Bar register

        Enable stream using S2H Control register. Validate stream is collecting
        data by confirming incrementing of the Valid Packet Index register.
        Poll for Valid Packet Index increment and verify expected wait time is
        within 4 ms between each attempt of reading index register.
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.s2h.reset_stream()
            self.s2h.enable_stream()
            self.spm.enable_spm()

            for present_choice in present_choices:
                with self.subTest(BPS_PRESENT=present_choice,
                                  SPM_ENABLE=1):
                    self.spm.initialize_limits(bps_0=present_choice[0],
                                               bps_1=present_choice[1],
                                               bps_2=present_choice[2])
                    self.test_class.DirectedEnableStreamTest()
        finally:
            self.spm.disable_spm()

        with self.subTest(BPS_PRESENT=present_choice, SPM_ENABLE=0):
            self.spm.initialize_limits(bps_0=present_choice[0],
                                       bps_1=present_choice[1],
                                       bps_2=present_choice[2])
            self.test_class.DirectedEnableStreamTest()

        self.s2h.disable_stream()

    @skip('Not implemented')
    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedStreamEnableUsingEventTriggerTest(self):
        """Validate enabling of S2H stream using the Event Triggers

        Enable stream using an S2H Start Trigger event. Validate stream is
        collecting data by confirming incrementing of the Valid Packet Index
        register.
        Poll for Valid Packet Index increment and verify expected wait time is
        within 4 ms between each attempt of reading index register.
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedStreamDisableUsingBarRegisterTest(self):
        """Validate disabling of S2H stream using the S2H Control Bar register

        Disable stream using S2H Control register. Validate stream is not
        collecting data by confirming the Valid Packet Index register does not
        increment. Wait packet cycle time between each attempt of reading index
        register.
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.s2h.reset_stream()
            self.s2h.enable_stream()
            self.spm.enable_spm()

            for present_choice in present_choices:
                with self.subTest(BPS_PRESENT=present_choice,
                                  SPM_ENABLE=1):
                    self.spm.initialize_limits(bps_0=present_choice[0],
                                               bps_1=present_choice[1],
                                               bps_2=present_choice[2])
                    self.test_class.DirectedDisableStreamTest()
        finally:
            self.spm.disable_spm()

        with self.subTest(BPS_PRESENT=present_choice, SPM_ENABLE=0):
            self.spm.initialize_limits(bps_0=present_choice[0],
                                       bps_1=present_choice[1],
                                       bps_2=present_choice[2])
            self.test_class.DirectedDisableStreamTest()

        self.s2h.disable_stream()

    @skip('Not implemented')
    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedStreamDisableUsingEventTriggerTest(self):
        """Validate disabling of S2H stream using the Event Triggers

        Disable stream using an S2H Start Trigger event. Validate stream is not
        collecting data by confirming the Valid Packet Index register does not
        increment. Wait packet cycle time between each attempt of reading index
        register.
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedStreamResetUsingBarRegisterTest(self):
        """Validate S2H stream reset using the S2H Control Bar register

        Disable and reset stream using S2H Controller register. Validate stream
        reset by confirming Valid Packet Index value is invalid. Wait
        packet cycle time between each attempt of reading index register.
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.s2h.reset_stream()
            self.s2h.enable_stream()
            self.spm.enable_spm()

            for present_choice in present_choices:
                with self.subTest(BPS_PRESENT=present_choice,
                                  SPM_ENABLE=1):
                    self.spm.initialize_limits(bps_0=present_choice[0],
                                               bps_1=present_choice[1],
                                               bps_2=present_choice[2])
                    self.test_class.DirectedResetStreamTest()
        finally:
            self.spm.disable_spm()

        with self.subTest(BPS_PRESENT=present_choice, SPM_ENABLE=0):
            self.spm.initialize_limits(bps_0=present_choice[0],
                                       bps_1=present_choice[1],
                                       bps_2=present_choice[2])
            self.test_class.DirectedResetStreamTest()

        self.s2h.disable_stream()

    @skip('Not implemented')
    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedStreamResetUsingEventTriggerTest(self):
        """Validate S2H stream reset using the Event Triggers

        Disable and reset stream using S2H event triggers. Validate stream
        reset by confirming Valid Packet Index has a value of 0xFFFF. Wait
        packet cycle time between each attempt of reading index register.
        Use all permutations of BPS present bits and for each choice, repeat
        the test {num_iterations} times.
        """


class SpmConfigurationAndStatus(HbirctcTest):
    """This interface consists of SPM POUT polling, filter count and limiting
    """
    TEST_ITERATIONS = 100
    MAX_FAIL_COUNT= 2
    FILTER_COUNT_TEST_ITERATIONS = 4
    FILTER_COUNT_MAX_FAIL_COUNT = 2
    FILTER_COUNT_LIST = [0, 10, 100, 1000]
    FILTER_COUNT_BPS_PRESENT_LIST = [(0, 0, 0), (0, 0, 1), (1, 1, 1)]

    def setUp(self, tester=None):
        super().setUp(tester)
        skip_test_if_fpga_not_spm_capable()
        self.test_iterations = SpmConfigurationAndStatus.TEST_ITERATIONS
        self.max_fail_count = SpmConfigurationAndStatus.MAX_FAIL_COUNT
        self.spm = SpmInterface(self.Log)
        self.spm.initialize_limits(bps_0=1, bps_1=1, bps_2=1)
        self.sass_api = sass.SassApi()
        self.sass_api.Connect('FVAL SpmConfigurationAndStatus')
        self.sass_api.SuppressSensor('HBIPowerSupply')

    def tearDown(self):
        self.sass_api.ResumeSensor('HBIPowerSupply')
        self.sass_api.Disconnect()
        super().tearDown()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPresentEnableTest(self):
        """Validate Bulk Power Supply Present SPM Configuration

        Cycle through all possible choices of the Present bits in the SPM
        configuration register. At each choice of present bits, verify the data
        registers values of the BPS are NaN if its present bit is set to zero.
        Also, confirm that the Pout Sum data register is approximately the sum
        of the non NaN Pout data registers.

        Repeat a total of {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for present_choice in present_choices:
                    self.spm.set_bps_present(bps_0=present_choice[0],
                                             bps_1=present_choice[1],
                                             bps_2=present_choice[2])
                    self.spm.wait_on_data_refresh()

                    success &= self.compare_present_choice_against_data(
                        iteration, present_choice)

                    # POUT SUM does not need testing when all BPS POUT are
                    # NaN
                    if present_choice != (0, 0, 0):
                        success &= self.approximate_sum(iteration,
                                                        present_choice)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def compare_present_choice_against_data(self, iteration, present_choice):
        success = True
        for bps_num in range(SpmInterface.NUM_BPS):
            is_data_all_nan = self.is_data_all_nan(bps_num)
            is_present = bool(present_choice[bps_num])
            if is_present == is_data_all_nan:
                success = False
                self.Log('error', self.error_msg_non_nan_data(
                    iteration, bps_num, is_present, is_data_all_nan))
                self.spm.log_sensor_data(bps_num)
        return success

    def is_data_all_nan(self, bps_num):
        success = True
        for data_type in SPM_DATA_TYPES:
            if data_type != 'pout_sum':
                value = self.spm.bps_data(data_type, bps_num)
                success &= isnan(value)
        return success

    def error_msg_non_nan_data(self, iteration, bps_num, is_present,
                               is_data_all_nan):
        return f'Iteration {iteration}) Found non-Nan data values at ' \
               f'BPS_{bps_num}. ' \
               f'is_present: {is_present}, is_data_all_nan: {is_data_all_nan}'

    def approximate_sum(self, iteration, present_choice):
        sum_total = 0
        for bps_num in range(SpmInterface.NUM_BPS):
            if present_choice[bps_num] != 0:
                sum_total += self.spm.bps_pout_data(bps_num)
        pout_sum = self.spm.bps_pout_sum_data()
        if not isclose(sum_total, pout_sum, rel_tol=SpmData.RELATIVE_TOLERANCE):
            self.Log('error', self.error_msg_sum(iteration, sum_total,
                                                 pout_sum))
            return False
        else:
            return True

    def error_msg_sum(self, iteration, sum_total, pout_sum):
        return f'Iteration {iteration}) (sum_totals, pout_sum): ' \
               f'{sum_total}, {pout_sum}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedEnablePollingTest(self):
        """Validate polling enabling using the SPM Control register

        Enable polling, read IOUT values of all Bulk Power Supplies and make
        sure data is not stale. Continue test by disabling polling, reading
        IOUT values and making sure data is stale. Check the SMP Status
        register and verify if the polling mode is active or not accordingly.

        Repeat a total of {num_iterations} times.
        """

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = self.run_polling_enabled_scenario(iteration)
                success &= self.run_polling_disabled_scenario(iteration)

                self.update_failed_iterations(success=success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def run_polling_enabled_scenario(self, iteration):
        self.spm.set_spm_enable(enable=1)
        self.spm.wait_on_data_refresh()

        success = True
        for bps_num in range(SpmInterface.NUM_BPS):
            success &= self.wait_on_iout_data_change(
                bps_num, enable_logging=True, num_retries=150)

        is_active = self.spm.bps_polling_active()
        if is_active != [1, 1, 1]:
            success = False
            self.Log('error', self.error_msg_polling_active(
                iteration, [1, 1, 1], is_active))

        if not success:
            self.Log('error', self.error_msg_polling_enable(iteration,
                                                           enable=1))
        return success

    def run_polling_disabled_scenario(self, iteration):
        self.spm.set_spm_enable(enable=0)
        self.spm.wait_on_data_refresh()

        success = True
        for bps_num in range(SpmInterface.NUM_BPS):
            success &= not self.wait_on_iout_data_change(bps_num,
                                                         num_retries=20)

        is_active = self.spm.bps_polling_active()
        if is_active != [0, 0, 0]:
            success = False
            self.Log('error', self.error_msg_polling_active(
                iteration, [0, 0, 0], is_active))

        if not success:
            self.Log('error', self.error_msg_polling_enable(iteration,
                                                           enable=0))
        return success

    def wait_on_iout_data_change(self, bps_num=0, enable_logging=False,
                                 num_retries=50):
        success = True
        iout = self.spm.bps_iout_data(bps_num)
        temp = iout
        for retry in range(num_retries):
            self.spm.wait_on_data_refresh()
            temp = self.spm.bps_iout_data(bps_num)
            if isnan(iout) or isnan(temp):
                if isnan(iout) != isnan(temp):
                    break
            elif temp != iout:
                break
        else:
            if enable_logging:
                self.Log('warning', f'IOUT_{bps_num}: No data change obtained. '
                                    f'{iout}, {temp}')
            success = False
        return success

    def error_msg_polling_active(self, iteration, expected, actual):
        return f'Iteration {iteration}) Polling active check failed ' \
               f'(expected, actual): {expected}, {actual}'

    def error_msg_polling_enable(self, iteration, enable):
        text = 'enable' if enable else 'disable'
        return f'Iteration {iteration}) Failed to {text} polling.'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPollingStatusTest(self):
        """Validate polling status using the SPM Status register

        Cycle through all possible combinations of the Bulk Power Supply
        present bits of the SPM Configuration register. Verify that the
        corresponding "mode active" bit of the polling status is set if the
        present bit for that supply is set.

        Repeat a total of {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for present_choice in present_choices:
                    present_choice = list(present_choice)
                    self.spm.set_bps_present(bps_0=present_choice[0],
                                             bps_1=present_choice[1],
                                             bps_2=present_choice[2])
                    is_active = self.wait_on_polling_status(present_choice)
                    if is_active != present_choice:
                        success = False
                        self.Log('error', self.error_msg_polling_status(
                            iteration, present_choice, is_active))

                if self.fail_count >= self.max_fail_count:
                    break

                self.update_failed_iterations(success)

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def wait_on_polling_status(self, expected_status, num_retries=10):
        status = self.spm.bps_polling_active()
        for retry in range(num_retries):
            if status == expected_status:
                break
            self.spm.wait_on_data_refresh()
            status = self.spm.bps_polling_active()
        return status

    def error_msg_polling_status(self, iteration, expected, actual):
        return f'Iteration {iteration}) Invalid active mode ' \
               f'(expected, actual): {expected}, {actual}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPoutAlarmTriggeringTest(self):
        """Validate POUT alarm triggering

        Set POUT Limit threshold to an appropriate value to trigger a POUT
        alarm. Verify POUT alarm(s) is triggered via the SPM Alarms and
        Global Alarms registers. Reset limit and clear the POUT Alarm.

        Repeat the test {num_iterations} times.
        """
        self.validate_spm_alarm_triggering(sensor_name='pout')

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedPoutSumAlarmTriggeringTest(self):
        """Validate POUT Sum alarm triggering

        Set POUT Sum Limit threshold to an appropriate value to trigger a POUT
        Sum alarm. Verify POUT Sum alarm is triggered via the SPM Alarms
        register. Reset Sum limit and clear the POUT Sum Alarm.
        Repeat the test {num_iterations} times.
        """
        self.validate_spm_alarm_triggering(sensor_name='pout_sum')

    def validate_spm_alarm_triggering(self, sensor_name):
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for present_choice in present_choices:
                    self.spm.initialize_limits(bps_0=present_choice[0],
                                               bps_1=present_choice[1],
                                               bps_2=present_choice[2])
                    self.spm.wait_on_data_refresh()

                    current_limit = getattr(
                        self.spm, f'{sensor_name}_limit')()
                    fail_limit = getattr(
                        self, f'generate_failing_{sensor_name}_limit')()
                    expected_alarm = self.generate_expected_spm_alarm(
                        sensor_name, present_choice)
                    success &= self.run_spm_alarm_trigger_scenario(
                        iteration, sensor_name, fail_limit, expected_alarm)

                    success &= self.run_spm_alarm_clear_scenario(
                        iteration, sensor_name, current_limit)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def generate_failing_pout_limit(self):
        pouts = [self.spm.bps_pout_data(bps_num)
                 for bps_num in range(SpmInterface.NUM_BPS)]

        non_nan_pouts = [pout for pout in pouts if not isnan(pout)]

        if not non_nan_pouts:
            return float('nan')

        non_nan_pouts.sort()
        return non_nan_pouts[0] / 2.0

    def generate_failing_pout_sum_limit(self):
        limit = float('nan') if self.spm.bps_present() == [0, 0, 0] else \
            self.spm.pout_sum_limit()
        return limit / 2

    def run_spm_alarm_trigger_scenario(self, iteration, sensor_name, limit,
                                       expected_alarm):
        success = True

        if not isnan(limit):
            getattr(self.spm, f'set_{sensor_name}_limit')(limit)
            self.spm.wait_on_data_refresh()

            actual_alarm = self.spm.spm_alarm()
            global_alarm = self.spm.global_spm_alarm()

            if actual_alarm != expected_alarm:
                success = False
                self.Log('error', self.error_msg_spm_alarm(
                    iteration, sensor_name, expected_alarm, actual_alarm))

            if global_alarm != 1:
                success = False
                self.Log('error', self.error_msg_global_spm_alarm(
                    iteration, sensor_name, expected=1, actual=global_alarm))

        return success

    def run_spm_alarm_clear_scenario(self, iteration, sensor_name, limit):
        success = True

        getattr(self.spm, f'set_{sensor_name}_limit')(limit)
        getattr(self.spm, f'clear_spm_{sensor_name}_alarm')()
        self.spm.wait_on_data_refresh()

        actual_alarm = self.spm.spm_alarm()
        global_alarm = self.spm.global_spm_alarm()

        if actual_alarm != 0:
            success = False
            self.Log('error', self.error_msg_spm_alarm(
                iteration, sensor_name, expected=0, actual=actual_alarm))

        if global_alarm != 0:
            success = False
            self.Log('error', self.error_msg_global_spm_alarm(
                iteration, sensor_name, expected=0, actual=global_alarm))

        return success

    def error_msg_spm_alarm(self, iteration, sensor_name, expected, actual):
        return f'Iteration {iteration}) {sensor_name.upper()} - SPM Alarm ' \
               f'failed (expected, actual): {expected}, {actual}'

    def error_msg_global_spm_alarm(self, iteration, sensor_name, expected,
                                   actual):
        return f'Iteration {iteration}) {sensor_name.upper()} - SPM Global ' \
               f'Alarm failed (expected, actual): {expected}, {actual}'

    @format_docstring(
        num_iterations=FILTER_COUNT_TEST_ITERATIONS,
        count_list=FILTER_COUNT_LIST,
        bps_present_list=FILTER_COUNT_BPS_PRESENT_LIST,
        sensor_refresh_rate=SpmInterface.POUT_REFRESH_RATE_SECS * 1e6)
    def DirectedPoutFilterCountTest(self):
        """Validate POUT filter count

        Validate filter counts {count_list}:
        1. Set filter count to next value in above list.
        2. Set POUT sensor limit register and wait on expected spm alarm.
        3. Compare time elapsed in step 2 against expected time. Expected time
        is sensor refresh rate * filter count. Refresh rate is
        {sensor_refresh_rate:.02f} micro-seconds.
        4. Reset limit to default, clear alarm and verify alarm is cleared.
        5. Repeat steps 1 to 4 for BPS present combinations {bps_present_list}
        6. Repeat all steps a total of {num_iterations} times.
        """
        self.validate_filter_count(sensor_name='pout')

    @format_docstring(
        num_iterations=FILTER_COUNT_TEST_ITERATIONS,
        count_list=FILTER_COUNT_LIST,
        bps_present_list=FILTER_COUNT_BPS_PRESENT_LIST,
        sensor_refresh_rate=SpmInterface.POUT_REFRESH_RATE_SECS * 1e6)
    def DirectedPoutSumFilterCountTest(self):
        """Validate POUT Sum filter count

        Validate filter counts {count_list}:
        1. Set filter count to next value in above list.
        2. Set POUT SUM sensor limit register and wait on expected spm alarm.
        3. Compare time elapsed in step 2 against expected time. Expected time
        is sensor refresh rate * filter count. Refresh rate is
        {sensor_refresh_rate:.02f} micro-seconds.
        4. Reset limit to default, clear alarm and verify alarm is cleared.
        5. Repeat steps 1 to 4 for BPS present combinations {bps_present_list}
        6. Repeat all steps a total of {num_iterations} times.
        """
        self.validate_filter_count(sensor_name='pout_sum')

    def validate_filter_count(self, sensor_name):
        self.test_iterations = self.FILTER_COUNT_TEST_ITERATIONS
        self.max_fail_count = self.FILTER_COUNT_MAX_FAIL_COUNT
        present_choices = self.FILTER_COUNT_BPS_PRESENT_LIST

        for iteration in range(self.test_iterations):
            success = True

            for present_choice in present_choices:
                try:
                    self.spm.enable_spm()
                    success &= self.run_filter_count_scenario(
                        iteration, sensor_name, present_choice)
                finally:
                    self.initialize_filter_count_scenario(
                        iteration, sensor_name, present_choice)
                    self.spm.disable_spm()

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.initialize_filter_count_scenario(sensor_name=sensor_name)

        self.validate_iterations()

    def run_filter_count_scenario(self, iteration, sensor_name, present_choice):
        success = self.validate_default_filter_count(iteration, sensor_name)
        self.spm.initialize_limits(bps_0=present_choice[0],
                                   bps_1=present_choice[1],
                                   bps_2=present_choice[2])
        current_limit = getattr(self.spm, f'{sensor_name}_limit')()
        expected_spm_alarm = self.generate_expected_spm_alarm(sensor_name,
                                                              present_choice)
        filter_count = self.FILTER_COUNT_LIST

        for count in filter_count:
            getattr(self.spm, f'set_{sensor_name}_filter_count')(count)
            getattr(self.spm, f'set_{sensor_name}_limit')(current_limit / 2)

            start_time = self.spm.perf_counter()
            try:
                self.spm.wait_on_spm_alarm(expected=expected_spm_alarm,
                                           num_retries=2 if count == 0
                                           else count+1)
            except self.spm.SpmAlarmWaitError:
                success = False
                self.Log('error', self.error_msg_spm_alarm_timeout(
                    iteration, expected_spm_alarm, count))
            actual_time_us = self.calculate_filter_count_time_elapsed_us(
                start_time)

            success &= self.evaluate_time_elapsed(
                iteration, sensor_name, present_choice, count, actual_time_us)

            success &= self.initialize_filter_count_scenario(
                iteration, sensor_name, present_choice)

        return success

    def calculate_filter_count_time_elapsed_us(self, start_time_sec):
        return (self.spm.perf_counter() - start_time_sec) * 1e6

    def validate_default_filter_count(self, iteration, sensor_name):
        filter_count = self.spm.filter_count(sensor_name)
        if filter_count != self.spm.DEFAULT_FILTER_COUNT:
            self.Log('error', self.error_msg_default_filter_count(
                iteration, sensor_name, filter_count))
            return False
        else:
            return True

    def error_msg_default_filter_count(self, iteration, sensor_name,
                                       filter_count):
        return f'Iteration {iteration}) Invalid {sensor_name} filter default ' \
               f'value (expected, actual): {self.spm.DEFAULT_FILTER_COUNT}, ' \
               f'{filter_count}'

    def generate_expected_spm_alarm(self, sensor_name, present_choice):
        self.spm.set_bps_present(bps_0=present_choice[0],
                                 bps_1=present_choice[1],
                                 bps_2=present_choice[2])

        for retry in range(10):
            if self.spm.bps_present() == self.spm.bps_polling_active():
                break
            self.spm.wait_on_pout_refresh()
        else:
            self.Log('warning', f'BPS not active (expected, actual): '
                                f'{self.spm.bps_present()}, '
                                f'{self.spm.bps_polling_active()}')

        if sensor_name == 'pout':
            expected_spm_alarm = self.hbirctc.registers.SPM_ALARMS(
                bps_0_spm_pout=present_choice[0],
                bps_1_spm_pout=present_choice[1],
                bps_2_spm_pout=present_choice[2]).value
        else:
            expected_spm_alarm = self.hbirctc.registers.SPM_ALARMS(
                spm_pout_sum= 0 if present_choice == (0, 0, 0) else 1).value

        return expected_spm_alarm

    def error_msg_spm_alarm_timeout(self, iteration, expected_spm_alarm,
                                    filter_count):
        return f'Iteration {iteration}) Timed out waiting on an spm alarm ' \
               f'value of 0x{expected_spm_alarm:08X} using a filter count of ' \
               f'{filter_count}'

    def initialize_filter_count_scenario(self, iteration=None, sensor_name=None,
                                         present_choice=(1, 1, 1)):
        self.spm.initialize_limits(present_choice[0], present_choice[1],
                                   present_choice[2])
        self.spm.initialize_filter_count(sensor_name)
        self.spm.wait_on_pout_refresh()
        self.spm.clear_spm_pout_alarm()
        alarm = self.spm.spm_alarm()
        if alarm != 0:
            self.Log('error', self.error_msg_spm_alarm(
                iteration, sensor_name, expected=0, actual=alarm))
            return False
        else:
            return True

    def evaluate_time_elapsed(self, iteration, sensor_name, present_choice,
                              filter_count, time_elapsed_us):
        expected_time_us = self.expected_filter_count_time_elapsed(
            filter_count, present_choice)
        expected_spm_alarm = self.generate_expected_spm_alarm(sensor_name,
                                                              present_choice)
        success = True
        if filter_count != 0 and present_choice != (0, 0, 0):
            if not isclose(time_elapsed_us, expected_time_us,
                           rel_tol=self.spm.filter_count_relative_tolerance):
                success = False
        else:
            if not isclose(time_elapsed_us, expected_time_us,
                           abs_tol=self.spm.filter_count_absolute_tolerance):
                success = False

        if not success:
            self.Log('error', self.error_msg_filter_count(
                iteration, sensor_name, expected_spm_alarm, filter_count,
                expected_time_us, time_elapsed_us))

        return success

    def expected_filter_count_time_elapsed(self, filter_count, present_choice):
        return (0 if filter_count == 0 or present_choice == (0, 0, 0) else
                self.spm.POUT_REFRESH_RATE_SECS * filter_count) * 1e6

    def error_msg_filter_count(self, iteration, sensor_name, expected_spm_alarm,
                               filter_count, expected_time_us, actual_time_us):

        abs_tol, rel_tol = self.spm.filter_count_tolerances()
        bps_present = self.spm.bps_present()
        if filter_count != 0 and bps_present != (0, 0, 0):
            actual_tol = abs(1 - (expected_time_us / actual_time_us))
            tol_msg = f'Relative tolerance between times ' \
                      f'(expected, actual): {rel_tol:.02f}%, {actual_tol:.02f}%'
        else:
            actual_tol = abs(expected_time_us - actual_time_us)
            tol_msg = f'Absolute tolerance between times ' \
                      f'(expected, actual): {abs_tol:.02f}, {actual_tol:.02f}'

        return f'Iteration {iteration}) Waiting on triggering of spm alarm ' \
               f'with a filter count of {filter_count} and BPS present of ' \
               f'{bps_present} was invalid.\n' \
               f'\tSensor name, Expected Alarm: ' \
               f'{sensor_name}, {expected_spm_alarm}\n' \
               f'\tElapsed time in \"us\" to trigger alarm ' \
               f'(expected, actual): {expected_time_us}, {actual_time_us}\n' \
               f'\t{tol_msg}'


class SpmData(HbirctcTest):
    """This interfaces collects sensor data from the Bulk Power Supplies

    The sensor data consists of POUT, VOUT, IOUT, TEMP0:2 and FanSpeed0:1. This
    data can be read from the SPM Data registers, a set of 8 for each of the
    three Bulk Power Supplies (BPS).
    There is also one register, POUT Sum Data register, that populates the sum
    of each BPSs POUT readings.
    """
    TEST_ITERATIONS = 100
    MAX_FAIL_COUNT = 10
    RELATIVE_TOLERANCE = 0.10

    def setUp(self, tester=None):
        super().setUp(tester)
        skip_test_if_fpga_not_spm_capable()
        self.test_iterations = SpmData.TEST_ITERATIONS
        self.max_fail_count = SpmData.MAX_FAIL_COUNT
        self.spm = SpmInterface(self.Log)
        self.spm.initialize_limits(bps_0=1, bps_1=1, bps_2=1)
        self.sass_api = sass.SassApi()
        self.sass_api.Connect('FVAL SpmData')
        self.sass_api.SuppressSensor('HBIPowerSupply')

    def tearDown(self):
        self.sass_api.ResumeSensor('HBIPowerSupply')
        self.sass_api.Disconnect()
        super().tearDown()

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedPoutDataStabilityTest(self):
        """ Verify stability of Bulk Power Supply POUT sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads. Check Vout * Iout is approximately
        Pout.
        Read the data {num_iterations} times.
        """

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = self.run_pout_data_stability_test(iteration)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def run_pout_data_stability_test(self, iteration):
        success = True

        for bps_num in range(self.spm.NUM_BPS):
            pout = self.spm.poll_data_using_refresh_rate('pout', bps_num,
                                                         poll_total=2)
            vout = self.spm.bps_vout_data(bps_num)
            iout = self.spm.bps_iout_data(bps_num)

            if not isclose(pout[0], pout[1], rel_tol=SpmData.RELATIVE_TOLERANCE):
                success = False
                self.Log('error', self.error_msg('pout', iteration, pout))

            data = [pout[0], vout * iout]
            if not isclose(data[0], data[1], rel_tol=SpmData.RELATIVE_TOLERANCE):
                success = False
                self.Log('error', self.error_msg('pout=v*i', iteration, data))

        return success

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedVoutDataStabilityTest(self):
        """ Verify stability of Bulk Power Supply VOUT sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('vout')

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedIoutDataStabilityTest(self):
        """ Verify stability of Bulk Power Supply IOUT sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('iout')

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedTemp1DataStabilityTest(self):
        """ Verify stability of Bulk Power Supply Temperature 1 sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('temp1')

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedTemp2DataStabilityTest(self):
        """ Verify stability of Bulk Power Supply Temperature 2 sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('temp2')

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedTemp3DataStabilityTest(self):
        """ Verify stability of Bulk Power Supply Temperature 3 sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('temp3')

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedFanSpeed1DataStabilityTest(self):
        """ Verify stability of Bulk Power Supply Fan Speed 1 sensor reads

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('fan_speed_1')

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      tolerance=int(RELATIVE_TOLERANCE * 100))
    def DirectedPoutSumDataStabilityTest(self):
        """ Verify stability of Bulk Power Supply Power Sum data read

        Read the data register and verify value is within a {tolerance}%
        relative tolerance between reads.
        Read the data {num_iterations} times.
        """
        self.validate_data_stability('pout_sum')

    def validate_data_stability(self, data_type):
        try:
            self.spm.enable_spm()
            performance_sleep(secs=0.5)

            for iteration in range(self.test_iterations):
                success = self.run_data_stability_test(iteration,
                                                       data_type)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.spm.disable_spm()

    def run_data_stability_test(self, iteration, data_type):
        success = True

        num_bps = 1 if data_type == 'pout_sum' else SpmInterface.NUM_BPS
        for bps_num in range(num_bps):
            data = self.spm.poll_data_using_refresh_rate(data_type, bps_num,
                                                         poll_total=2)
            if data[0] == 0 or data[1] == 0:
                self.Log('error', self.error_msg_zero_value(data_type,
                                                            iteration, data))
                success = False
            elif not isclose(data[0], data[1],
                             rel_tol=SpmData.RELATIVE_TOLERANCE):
                self.Log('error', self.error_msg(data_type, iteration, data))
                success = False

        return success

    def error_msg_zero_value(self, data_type, iteration, data):
        return f'Iteration {iteration}): {data_type.upper()} has a value of ' \
               f'zero {data}'

    def error_msg(self, data_type, iteration, data):
        data.sort()
        current_tolerance = ((data[1] / data[0]) - 1) * 100
        expected_tolerance = SpmData.RELATIVE_TOLERANCE * 100
        return f'Iteration {iteration}): {data_type.upper()} out of tolerance'\
               f' (data_1, data_0, current_tolerance, expected_tolerance): ' \
               f'{data[1]:.01f}, {data[0]:.01f}, ' \
               f'{current_tolerance:.02f}%, {expected_tolerance:.02f}%'


@skip('Not implemented')
class SpmAlarms(HbirctcTest):
    """Interface for SPM related alarms

    Consists of the following registers:
    - SPM Alarms: Data and Address NAK, POUT over limit and POUT Sum over limit
    POUT SPM alarms are tested in DirectedPoutAlarmTriggeringTest.
    - Global Alarms: Active SPM Alarm.
    Tested in DirectedPoutAlarmTriggeringTest
    - Error Information: Stores a DATA NAk alarm's address
    """
    TEST_ITERATIONS = 100

    def setUp(self, tester=None):
        super().setUp(tester)
        skip_test_if_fpga_not_spm_capable()
        self.test_iterations = SpmAlarms.TEST_ITERATIONS
        self.max_fail_count = 2
        self.spm = SpmInterface(self.Log)
        self.spm.initialize_limits(bps_0=1, bps_1=1, bps_2=1)
        self.sass_api = sass.SassApi()
        self.sass_api.Connect('FVAL SpmAlarms')
        self.sass_api.SuppressSensor('HBIPowerSupply')

    def tearDown(self):
        self.sass_api.ResumeSensor('HBIPowerSupply')
        self.sass_api.Disconnect()
        super().tearDown()

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedDataNakTest(self):
        """Validate data NAKs

        Introduce a data NAK condition to the I2C transaction and verify
        receipt at the corresponding Data NAK Alarm register. Verify register
        address of the transaction is available in the appropriate Error
        Information register.
        Repeat the test {num_iterations} times.
        """

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedAddressNakTest(self):
        """Validate address NAKs

        Introduce an address NAK condition to the I2C transaction and verify
        receipt at the corresponding Address NAK Alarm register.
        Repeat the test {num_iterations} times.
        """


class SpmScopeshot(HbirctcTest):
    """Interface provides the HBI system the ability to debug power-related
    failures.  This feature involves the FPGA periodically writing the 
    Power Data values to the FPGA DDR4.

    The user has access to the following registers:
    - Latest SPM Scopeshot DDR4 Address: indicates the next DDR4 Memory Address
    at which the SPM Scopeshot Engine in the FPGA placed the last sample.

    - SPM Scopeshot DDR4 Ending Address: indicates the ending address in the
    DDR4 Memory for the SPM Scopeshot feature. After this limit is reached, the
    SPM Scopeshot Engine wraps around to write the next sample to the address
    configured in the SPM Scopeshot DDR4 Memory Starting Address Register.
    The initial value of this field is 0x7FFF_FFFF.  This combined with the
    SPM Scopeshot DDR4 Memory Starting Address Register  ensures that 0.5GiB of
    DDR4 memory is allotted to the SPM Scopeshot feature, by default.

    - SPM Scopeshot DDR4 Starting Address: indicates the starting address in
    the DDR4 Memory for the SPM Scopeshot feature.
    The initial value of this field is 0x6000_0000.

    - SPM Scopeshot Control: two bits of control.
    Bit #   Description
    1       SPM Scopeshot Stop: Writing a 1 to this bit stops the SPM Scopeshot
            data collection.
    0       SPM Scopeshot Start: Writing a 1 to this bit starts the SPM
            Scopeshot data collection.

    - SPM Scopeshot Configuration: four fields of configuration.
    Bit #   Description
    31      SPM Scopeshot Enable: (0: Disable "default value", 1: Enable)
            enables or disables the SPM Scopeshot feature.  When SPM Scopeshot
            is enabled, the Scopeshot Engine will react to Scopeshot Triggers
            when in Trigger Mode, and react to SPM POUT Alarms when in
            SPM Power Alarm Mode.
            When SPM Scopeshot is enabled, the Scopeshot Engine will also react
            to SPM Scopeshot Start/Stop requests from the SPM Scopeshot Control
            Register.  When SPM Scopeshot is disabled, the Scopeshot Engine
            will abandon any active data collection and thereafter,  the
            Scopeshot Engine will not react to any of the data collection
            events mentioned above.
    30      SPM Scopeshot Mode: (0: Trigger Mode "default value", 1: SPM Power
            Alarm Mode) determines what mode the SPM Scopeshot operates in.
    15:0    Sample Count: indicates the number of samples that need to be
            captured, after scopeshot is stopped, either due to SPM Scopeshot
            Stop bit being written in the SPM Scopeshot Control Register, or a
            Scopeshot Stop Trigger when Trigger Mode is enabled, or due to an
            SPM POUT Alarm being received when SPM Power Alarm Mode is enabled.

    - SPM Scopeshot Status: one bit "active" field which indicates that the
    SPM Scopeshot Engine is busy writing SPM packets to the DDR4 memory.
    """
    TEST_ITERATIONS = 100
    MAX_FAIL_COUNT = 2
    NUM_PACKETS = 10

    def setUp(self, tester=None):
        super().setUp(tester)
        skip_test_if_fpga_not_spm_capable()
        self.skip_if_non_ddr_board()
        self.scopeshot = SpmScopeshotInterface(self.hbirctc)
        self.scopeshot.set_defaults()
        self.spm = SpmInterface(self.Log)
        self.spm.initialize_limits(bps_0=1, bps_1=1, bps_2=1)
        self.max_fail_count = SpmScopeshot.MAX_FAIL_COUNT
        self.test_iterations = SpmScopeshot.TEST_ITERATIONS
        self.num_packets = SpmScopeshot.NUM_PACKETS
        self.sass_api = sass.SassApi()
        self.sass_api.Connect('FVAL SpmScopeshot')
        self.sass_api.SuppressSensor('HBIPowerSupply')

    def tearDown(self):
        self.sass_api.ResumeSensor('HBIPowerSupply')
        self.sass_api.Disconnect()
        super().tearDown()

    def skip_if_non_ddr_board(self):
        fpga_ddr_capable = self.hbirctc.is_fpga_ddr_capable()
        mainboard_ddr_capable = self.hbirctc.is_mainboard_ddr_capable()
        if not (fpga_ddr_capable and mainboard_ddr_capable):
            raise SkipTest(
                f'Mainboard and image are not DDR4 ready (MB ready, '
                f'Image ready): {mainboard_ddr_capable}, {fpga_ddr_capable}')

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedWriteReadBarRegisterTest(self):
        """Validate all R/W access registers

        Write then read back the data written and verify a match. Perform for
        all register fields that have R/W access. Repeat test {num_iterations} 
        times. Also validates default values of registers.
        """
        try:
            self.validate_default_values()

            for iteration in range(self.test_iterations):
                success = True

                self.scopeshot.enable()
                if not self.scopeshot.is_enabled():
                    success = False
                    self.Log('error', self.error_msg_wr_fail(
                        iteration, 'enable', True, False))

                self.scopeshot.disable()
                if self.scopeshot.is_enabled():
                    success = False
                    self.Log('error', self.error_msg_wr_fail(
                        iteration, 'enable', False, True))

                self.scopeshot.set_mode(SpmScopeshotInterface.ALARM_MODE)
                mode = self.scopeshot.mode()
                if mode != SpmScopeshotInterface.ALARM_MODE:
                    success = False
                    self.Log('error', self.error_msg_wr_fail(
                        iteration, 'mode',
                        SpmScopeshotInterface.ALARM_MODE, mode))


                self.scopeshot.set_mode(SpmScopeshotInterface.TRIGGER_MODE)
                mode = self.scopeshot.mode()
                if mode != SpmScopeshotInterface.TRIGGER_MODE:
                    success = False
                    self.Log('error', self.error_msg_wr_fail(
                        iteration, 'mode',
                        SpmScopeshotInterface.TRIGGER_MODE, mode))

                fail_count = 0
                sample_count_field_size = 16
                for i in [negative_one_bits(sample_count_field_size),
                          getrandbits(sample_count_field_size),
                          0]:
                    self.scopeshot.set_sample_count(i)
                    sample_count = self.scopeshot.sample_count()
                    if sample_count != i:
                        success = False
                        fail_count += 1
                        self.Log('error', self.error_msg_wr_fail(
                            iteration, 'sample_count', i, sample_count))
                    if fail_count >= self.max_fail_count:
                        break

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()
        finally:
            self.scopeshot.set_defaults()

    def error_msg_wr_fail(self, iteration, name, expected, actual):
        return f'Iteration {iteration}) Failed to set scopeshot {name} ' \
               f'(expected, actual): {expected}, {actual}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedCircularBufferWriteReadExhaustiveTest(self):
        """ Validate circular buffer aspect of the scopeshot DDR

        Verify that scopeshot capture packets are stored in a circular buffer
        fashion. This means that once a packet is saved at the ending address,
        the next packet will overwrite the packet at the starting address and
        will continue incrementing from there until the ending address and
        repeat.

        The test steps are as follows:
        1. Set a starting and ending address. Will be using one of three
        combinations per test: [0x6000_0000, 0x6000_01B0],
         [0x7000_0000, 0x7000_01B0], [0x7FFF_FE30, 0x7FFF_FFE0]
        2. Start scopeshot and makes sure that the buffer rolls over at least
        three times. Observe the latest address register and confirm.
        3. Stop scopeshot capture midway through buffer by observing the latest
        address.
        4. Cycle through all captured packets by reading memory starting at the
        address location after the latest address up until the memory location
        at the current latest address. The latest address contains the most
        recent capture.
        5. Compare each timestamp read from step 4 and validate an increment of
        700 us

        Repeat circular buffer test for each address combination
        {num_iterations} times.
        """
        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for addressing in [[0x6000_0000, 0x6000_01B0],
                                   [0x7000_0000, 0x7000_01B0],
                                   [0x7FFF_FE30, 0x7FFF_FFE0]]:
                    self.initialize_scopeshot(start_address=addressing[0],
                                              end_address=addressing[1])

                    success &= self.run_circular_buffer_test(iteration)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.scopeshot.disable()
            self.spm.disable_spm()

    def run_circular_buffer_test(self, iteration):
        """
        1) Start scopeshot capture
        2) Repeat until latest address value wraps around buffer 3 times
        3) Stop
        4) Validate timestamp increment of 700 across capture data in DDR
        starting at latest_address +1 and wrapping around to latest_address
        :param iteration: current test run
        :return: True if no fails
        """

        sa = self.scopeshot.ddr4_starting_address()
        ea = self.scopeshot.ddr4_ending_address()
        total_packets = (ea - sa) // self.scopeshot.DDR4_ENTRY_BYTE_SIZE

        memory_wrap_count = 0
        self.num_memory_wraps = 3
        enable_count = True
        mid_address = sa + ((ea - sa) // 2)

        packet_refresh_sec = self.scopeshot.PACKET_REFRESH_RATE_SEC
        self.start_and_sync_scopeshot(sa)
        for i in range(total_packets * self.num_memory_wraps):
            start = perf_counter()

            latest_address = self.scopeshot.ddr4_latest_address()

            if enable_count and (latest_address > mid_address):
                memory_wrap_count += 1
                enable_count = False

            if memory_wrap_count == self.num_memory_wraps:
                break

            if latest_address < mid_address:
                enable_count = True

            performance_sleep(packet_refresh_sec - (perf_counter() - start))

        self.scopeshot.assert_stop_control()
        self.scopeshot.wait_on_active(asserted=False)
        self.scopeshot.disable()

        fail_count = 0
        expected_time_diff = \
            int(SpmScopeshotInterface.PACKET_REFRESH_RATE_SEC * 1e6)
        address = self.scopeshot.ddr4_latest_address()
        for i in range(total_packets - 1):
            address = self.scopeshot.ddr4_next_address(address)
            address_0 = address
            packet_0 = self.scopeshot.read_scopeshot_entries_from_memory(
                address)
            address = self.scopeshot.ddr4_next_address(address)
            packet_1 = self.scopeshot.read_scopeshot_entries_from_memory(
                address)

            diff = packet_1.time_stamp_us - packet_0.time_stamp_us

            if not isclose(diff, expected_time_diff, abs_tol=1):
                fail_count += 1
                self.Log('error', self.error_msg_memory_wrap_time_stamp(
                    iteration, address, address_0, packet_1.time_stamp_us,
                    packet_0.time_stamp_us))
                if fail_count >= self.max_fail_count:
                    break

        if memory_wrap_count < self.num_memory_wraps:
            fail_count += 1
            self.Log('error', self.error_msg_memory_wrap_count(
                iteration, memory_wrap_count))

        return (fail_count == 0)

    def error_msg_memory_wrap_count(self, iteration, memory_wrap_count):
        return f'Iteration {iteration}) Captured data rollover was not ' \
               f'observed for required number of times (expected, actual): ' \
               f'{self.num_memory_wraps}, {memory_wrap_count}'

    def error_msg_memory_wrap_time_stamp(self, iteration, address_1, address_0,
                                         stamp_1, stamp_0):
        return f'Iteration {iteration}) Expected time stamp increment of ' \
               f'{SpmScopeshotInterface.PACKET_REFRESH_RATE_SEC * 1e6}us ' \
               f'between consecutive packets not achieved.' \
               f'\n\tTime stamps from address {hex(address_0)} to ' \
               f'{hex(address_1)} (stamp_0, stamp_1, actual diff): ' \
               f'{stamp_0}, {stamp_1}, {stamp_1 - stamp_0}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedValidatePacketTest(self):
        """Validate packet contents as entered to DDR

        - Confirm packet header contents update accordingly
        - Confirm POUT SUM is approximately the sum of the POUT DATA entries
        - Confirm nan pout values if BPS disabled
        Fill up DDR with multiple entries and stop. Cycle through each captured
        entry and verify contents. Use all permutations of BPS present bits
        and for each choice, repeat the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for bps_present in present_choices:
                    self.initialize_scopeshot(bps_present=bps_present)

                    success &= self.run_valid_packet_test(
                        iteration, bps_present,
                        enable=int(self.spm.is_spm_enabled()))

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.scopeshot.disable()
            self.spm.disable_spm()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedValidatePacketWithSpmDisabledTest(self):
        """Validate packet contents as entered to DDR while SPM is disabled

        Similar to DirectedValidatePacketTest, but expecting POUT and POUT Sum
        to always be nan value, and Packet Header SPM Enable to always be 0
        - Confirm packet header contents update accordingly
        - Confirm POUT SUM is approximately the sum of the POUT DATA entries
        - Confirm nan pout values if BPS disabled
        Fill up DDR with multiple entries and stop. Cycle through each captured
        entry and verify contents. Use all permutations of BPS present bits
        and for each choice, repeat the test {num_iterations} times.
        """
        present_choices = list(product([0, 1], repeat=SpmInterface.NUM_BPS))

        try:
            self.spm.disable_spm()

            for iteration in range(self.test_iterations):
                success = True

                for bps_present in present_choices:
                    self.initialize_scopeshot(bps_present=bps_present)

                    success &= self.run_valid_packet_test(
                        iteration, bps_present,
                        enable=int(self.spm.is_spm_enabled()))

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.scopeshot.disable()

    def run_valid_packet_test(self, iteration, bps_present, enable):
        packets = self.capture_packets()
        self.scopeshot.disable()

        fail_count = 0
        previous_time_stamp = 0
        address = self.scopeshot.ddr4_starting_address()
        for i in range(len(packets)):
            success = True

            data = packets[i]
            actual_bps = [data.bps_0_present, data.bps_1_present,
                          data.bps_2_present]

            if actual_bps != list(bps_present):
                success = False
                self.Log('error', self.error_msg_validate_packet(
                    iteration, address, expected_bps=list(bps_present),
                    actual_bps=actual_bps))

            if data.spm_enable != enable:
                success = False
                self.Log('error', self.error_msg_validate_packet(
                    iteration, address, expected_enable=enable,
                    actual_enable=data.spm_enable))

            if i != 0:
                expected_time_stamp = \
                    previous_time_stamp + \
                    int(SpmScopeshotInterface.PACKET_REFRESH_RATE_SEC * 1e6)
                if not isclose(data.time_stamp_us, expected_time_stamp,
                               abs_tol=1):
                    success = False
                    self.Log('error', self.error_msg_validate_packet(
                        iteration, address,
                        expected_time_stamp=expected_time_stamp,
                        actual_time_stamp=data.time_stamp_us))
                previous_time_stamp = data.time_stamp_us
            else:
                previous_time_stamp = data.time_stamp_us

            if bps_present == (0, 0, 0) or not enable:
                if not isnan(data.pout_bps_0) or not isnan(data.pout_bps_1) or\
                        not isnan(data.pout_bps_2):
                    success = False
                    self.Log('error', self.error_msg_validate_packet(
                        iteration, address, expected_pout_012=f'[nan, nan, nan]',
                        actual_pout_012=f'[{data.pout_bps_0}, {data.pout_bps_1},'
                                        f' {data.pout_bps_2}]'))
                if not isnan(data.pout_sum):
                    success = False
                    self.Log('error', self.error_msg_validate_packet(
                        iteration, address, expected_pout_sum=f'nan',
                        actual_pout_sum=f'{data.pout_sum}'))
            else:
                expected_sum = data.pout_bps_0 if bps_present[0] else 0
                expected_sum += data.pout_bps_1 if bps_present[1] else 0
                expected_sum += data.pout_bps_2 if bps_present[2] else 0
                if data.pout_sum != expected_sum:
                    success = False
                    self.Log('error', self.error_msg_validate_packet(
                        iteration, address, expected_pout_sum=expected_sum,
                        actual_pout_sum=data.pout_sum))

            address = self.scopeshot.ddr4_next_address(address)

            if not success:
                fail_count += 1
            if fail_count >= self.max_fail_count:
                break

        return (fail_count == 0)

    def capture_packets(self):
        sa = self.scopeshot.ddr4_starting_address()

        zero_bytes = bytes(SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE *
                           self.num_packets)
        self.hbirctc.dma_write(sa, zero_bytes)

        # Run scopeshot engine until expected number of packets are collected
        packet_refresh_sec = self.scopeshot.PACKET_REFRESH_RATE_SEC
        self.start_and_sync_scopeshot(sa)
        for i in range(self.num_packets):
            start = perf_counter()
            self.advance_latest_address()
            performance_sleep(packet_refresh_sec - (perf_counter() - start))
        self.scopeshot.assert_stop_control()
        self.scopeshot.wait_on_active(asserted=False)

        # Get packets from memory and clean up no data packets
        address = sa
        packets = []
        for i in range(self.num_packets):
            packet = self.scopeshot.read_scopeshot_entries_from_memory(address)
            if packet.value == 0:
                break
            packets.append(packet)
            address = self.scopeshot.ddr4_next_address(address)

        return packets

    def start_and_sync_scopeshot(self, starting_address, num_retries=5):
        for retry in range(num_retries):
            try:
                self.scopeshot.assert_start_control()
                self.scopeshot.wait_on_latest_address(starting_address)
                break
            except SpmScopeshotInterface.RetryError:
                self.Log('warning', self.warn_msg_retry_latest_address(
                    retry+1, num_retries))
                self.scopeshot.assert_stop_control()
        else:
            raise SpmScopeshotInterface.RetryError(
                'Failed to start and sync Scopeshot engine')

    def warn_msg_retry_latest_address(self, retry, num_retries):
        return f'Unable to start and sync Scopeshot engine. Attempt ' \
               f'{retry+1} of {num_retries}'

    def advance_latest_address(self):
        # DO NOT REMOVE
        # This is mainly needed for simulation, but does not affect online
        # regression.
        self.scopeshot.ddr4_latest_address()

    def error_msg_validate_packet(self, iteration, address, **kwargs):
        args = list(kwargs.items())
        expected_name = args[0][0]
        actual_name = args[1][0]
        expected_data = args[0][1]
        actual_data = args[1][1]
        return f'Iteration {iteration}) Packet validation failed at address ' \
               f'{hex(address)}. ' \
               f'({expected_name}, {actual_name}): ' \
               f'{expected_data}, {actual_data}'

    def initialize_scopeshot(self, **kwargs):
        bps_present = kwargs.get('bps_present')
        start_address = kwargs.get('start_address')
        end_address = kwargs.get('end_address')
        mode = kwargs.get('mode')
        sample_count = kwargs.get('sample_count')
        spm_enable = kwargs.get('spm_enable')

        if bps_present:
            self.spm.initialize_limits(bps_0=bps_present[0],
                                       bps_1=bps_present[1],
                                       bps_2=bps_present[2])
        if spm_enable == 1:
            self.spm.enable_spm()
        elif spm_enable == 0:
            self.spm.disable_spm()

        if start_address:
            self.scopeshot.set_ddr4_starting_address(start_address)
        if end_address:
            self.scopeshot.set_ddr4_ending_address(end_address)
        if mode:
            self.scopeshot.set_mode(mode)
        if sample_count:
            self.scopeshot.set_sample_count(sample_count)
        self.scopeshot.enable()
        self.scopeshot.wait_on_packet_refresh()

    def validate_default_values(self):
        sa = self.scopeshot.ddr4_starting_address()
        ea = self.scopeshot.ddr4_ending_address()
        mode = self.scopeshot.mode()
        is_enabled = self.scopeshot.is_enabled()
        sample_count = self.scopeshot.sample_count()
        success = True

        default = SpmScopeshotInterface.DDR4_STARTING_ADDRESS_DEFAULT
        if sa != default:
            success = False
            self.Log('error', f'Invalid starting default address '
                              f'(expected, actual): {hex(default)}, {hex(sa)}')

        default = SpmScopeshotInterface.DDR4_ENDING_ADDRESS_DEFAULT
        if ea != default:
            success = False
            self.Log('error', f'Invalid ending default address '
                              f'(expected, actual): {hex(default)}, {hex(ea)}')

        default = SpmScopeshotInterface.ENABLE_DEFAULT
        if is_enabled != default:
            success = False
            self.Log('error', f'Invalid enable default (expected, actual): '
                              f'{default}, {is_enabled}')

        default = SpmScopeshotInterface.MODE_DEFAULT
        if mode != default:
            success = False
            self.Log('error', f'Invalid mode default (expected, actual): '
                              f'{default}, {mode}')

        default = SpmScopeshotInterface.SAMPLE_COUNT_DEFAULT
        if sample_count != default:
            success = False
            self.Log('error', f'Invalid sample count (expected, actual): '
                              f'{hex(default)}, {hex(sample_count)}')

        if success:
            self.Log('info', f'Default values validation successful')

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedScopeshotStartStopUsingBarRegisterTest(self):
        """Verify start/stop scopeshot capture using control register

        Setup starting and ending addresses to capture 100 packets. Start
        capture using start bit of control register. As packets get captured,
        use the latest address register to calculate receipt of 10 packets and
        stop capture using stop bit of control register. Verify the remaining
        allocated DDR space has not been populated by observing no change on
        the latest address register.

        Repeat test {num_iterations} times.
        """

        try:
            self.spm.enable_spm()

            for iteration in range(self.test_iterations):

                self.initialize_scopeshot()
                success = self.run_start_stop_test(iteration)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.scopeshot.disable()
            self.spm.disable_spm()

    def run_start_stop_test(self, iteration):
        self.num_packets = 100
        num_capture_packets = self.num_packets // 2
        packet_byte_size = self.scopeshot.DDR4_ENTRY_BYTE_SIZE
        packet_refresh_secs = SpmScopeshotInterface.PACKET_REFRESH_RATE_SEC
        zero_bytes = bytes(self.num_packets * packet_byte_size)

        sa = self.scopeshot.ddr4_starting_address()
        self.scopeshot.set_ddr4_ending_address(sa + len(zero_bytes))
        self.clear_memory(iteration, sa, self.num_packets, packet_byte_size)

        self.start_and_sync_scopeshot(sa)
        for i in range(num_capture_packets-1):
            start = perf_counter()
            self.advance_latest_address()
            performance_sleep(packet_refresh_secs - (perf_counter() - start))
        self.scopeshot.assert_stop_control()
        self.scopeshot.wait_on_active(asserted=False)

        address = self.scopeshot.ddr4_next_address(
            self.scopeshot.ddr4_latest_address())

        actual_num_capture_packets = (address - sa) // packet_byte_size
        absolute_tolerance = 10
        if not isclose(actual_num_capture_packets, num_capture_packets,
                       abs_tol=absolute_tolerance):
            self.Log('error', self.error_msg_start_stop_packet_count(
                iteration, num_capture_packets, actual_num_capture_packets,
                absolute_tolerance))

        address = sa
        fail_count = 0
        for i in range(actual_num_capture_packets):
            data = self.hbirctc.dma_read(address, packet_byte_size)
            if data == bytes(packet_byte_size):
                fail_count += 1
                self.Log('error', self.error_msg_start_stop_data_value(
                    iteration, address, expecting_zero=False))

            if fail_count >= self.max_fail_count:
                break

            address = self.scopeshot.ddr4_next_address(address)

        fail_count = 0
        for i in range(actual_num_capture_packets, self.num_packets):
            data = self.hbirctc.dma_read(address, packet_byte_size)
            if data != bytes(packet_byte_size):
                fail_count += 1
                self.Log('error', self.error_msg_start_stop_data_value(
                    iteration, address, expecting_zero=True))

            if fail_count >= self.max_fail_count:
                break

            address = self.scopeshot.ddr4_next_address(address)

        return (fail_count == 0)

    def error_msg_start_stop_packet_count(self, iteration, expected, actual,
                                          absolute_tolerance):
        return f'Iteration {iteration}) Invalid number of packets '\
               f'captured before stop command (expected, actual, tolerance): '\
               f'{expected}, {actual}, {absolute_tolerance}'

    def error_msg_start_stop_data_value(self, iteration, address,
                                        expecting_zero):
        msg = 'zero value' if expecting_zero else 'non zero value'
        return f'Iteration {iteration}) Invalid data read at address ' \
               f'{hex(address)}. Expected {msg}.'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedScopeshotEnableAndActiveTest(self):
        """Verify enable/disable and active status of scopeshot

        Run a cycle of packet capture while scopeshot is enabled and observe
        incremental changes in latest address register and a logic-1 at the
        status register's active bit.

        Run a cycle of packet capture while scopeshot is disabled and observe
        no change in latest address register, and a logic-0 at the status
        register's active bit.

        Repeat above two cycles {num_iterations} times.
        """

        try:
            self.spm.enable_spm()
            self.scopeshot.set_defaults()

            for iteration in range(self.test_iterations):

                success = self.run_enable_test(iteration)
                success &= self.run_disable_test(iteration)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

        finally:
            self.scopeshot.disable()
            self.spm.disable_spm()

    def run_enable_test(self, iteration):
        sa = self.scopeshot.ddr4_starting_address()
        num_packets = 100
        success = True
        actual_address = 0
        packet_refresh_sec = self.scopeshot.PACKET_REFRESH_RATE_SEC

        self.scopeshot.enable()
        self.start_and_sync_scopeshot(sa)
        for i in range(num_packets):
            start = perf_counter()
            if not self.scopeshot.is_active():
                success = False
                self.Log('error', self.error_msg_enable_active_is_active(
                    iteration, expected_is_active=True))
            actual_address = self.scopeshot.ddr4_latest_address()
            performance_sleep(packet_refresh_sec - (perf_counter() - start))
        self.scopeshot.assert_stop_control()
        self.scopeshot.wait_on_active(asserted=False)

        if actual_address <= sa:
            success = False
            self.Log('error', self.error_msg_enable_active_latest_address(
                iteration, sa, actual_address, is_enabled=True))

        self.scopeshot.disable()
        return success

    def run_disable_test(self, iteration):
        num_packets = 100

        self.scopeshot.disable()
        expected_address = self.scopeshot.ddr4_latest_address()
        self.scopeshot.assert_start_control()
        self.scopeshot.wait_on_packet_refresh()
        success = True
        for i in range(num_packets):
            if self.scopeshot.is_active():
                success = False
                self.Log('error', self.error_msg_enable_active_is_active(
                    iteration, expected_is_active=False))
            self.scopeshot.wait_on_packet_refresh()
        self.scopeshot.assert_stop_control()
        self.scopeshot.wait_on_active(asserted=False)

        actual_address = self.scopeshot.ddr4_latest_address()
        if expected_address != actual_address:
            success = False
            self.Log('error', self.error_msg_enable_active_latest_address(
                iteration, expected_address, actual_address, is_enabled=False))

        return success

    def error_msg_enable_active_is_active(self, iteration, expected_is_active):
        return f'Iteration {iteration}) Active status of ' \
               f'{expected_is_active} is invalid.'

    def error_msg_enable_active_latest_address(self, iteration, expected,
                                               actual, is_enabled):
        msg = 'During enabled test, latest address did not increment' \
            if is_enabled else 'During disabled test, latest address should ' \
                               'not have incremented'
        return f'Iteration {iteration}) {msg}' \
               f'\n\tLatest address (expected, actual): {hex(expected)}, ' \
               f'{hex(actual)}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedScopeshotStopInAlarmModeTest(self):
        """Verify a stop of packet capture upon POUT or POUT SUM alarm

        1. Start a cycle of scopeshot capture using a scopeshot mode of
        "SPM Power Alarm Mode". Trigger a POUT or POUT SUM alarm and verify
        capture has stopped by observing latest address register and an
        active status of 0.

        2. Repeat the test of trigger an alarm, but while in Trigger Mode. Verify
        a stop capture does not occur after an alarm trigger by observing
        increments at latest address register and an active status of 1. Send
        a stop command via control register once test is done.

        Perform steps 1 and 2 above using all possible combinations of
        sensors (pout and pout_sum) against trigger or alarm modes.

        Repeat test {num_iterations} times.
        """
        try:
            self.spm.enable_spm()
            self.scopeshot.enable()

            params = list(product(['pout', 'pout_sum'], [True, False]))
            shuffle(params)

            for sensor_name, enable_alarm_mode in params:
                with self.subTest(SENSOR_NAME=sensor_name,
                                  ENABLE_ALARM_MODE=enable_alarm_mode):
                    self.fail_count = 0
                    for iteration in range(self.test_iterations):
                        success = self.run_stop_in_alarm_mode_test(
                            iteration, sensor_name, enable_alarm_mode)

                        self.update_failed_iterations(success)
                        if self.fail_count >= self.max_fail_count:
                            break

                    self.validate_iterations()

        finally:
            self.scopeshot.set_defaults()
            self.spm.initialize_limits()
            self.spm.clear_spm_pout_alarm()
            self.spm.clear_spm_pout_sum_alarm()
            self.spm.disable_spm()

    def run_stop_in_alarm_mode_test(self, iteration=0, sensor_name='pout',
                                    enable_alarm_mode=True):
        fault_limit = getattr(self.spm, f'{sensor_name}_limit')() / 2

        if enable_alarm_mode:
            self.scopeshot.set_spm_power_alarm_mode()
        else:
            self.scopeshot.set_trigger_mode()

        alarms_before_test_string = self.spm.spm_pout_alarms_to_string()
        self.num_packets = 100
        packet_byte_size = self.scopeshot.DDR4_ENTRY_BYTE_SIZE
        sa = self.scopeshot.ddr4_starting_address()
        self.clear_memory(iteration, sa, self.num_packets, packet_byte_size)

        packet_refresh_sec = self.scopeshot.PACKET_REFRESH_RATE_SEC
        self.num_packet_alarm_trigger = self.num_packets // 4
        address = sa
        self.start_and_sync_scopeshot(sa)
        for i in range(self.num_packets):
            start = perf_counter()
            if i == self.num_packet_alarm_trigger and enable_alarm_mode:
                getattr(self.spm, f'set_{sensor_name}_limit')(fault_limit)
            address = self.scopeshot.ddr4_latest_address()
            performance_sleep(packet_refresh_sec - (perf_counter() - start))

        success = True
        if enable_alarm_mode:
            if self.scopeshot.is_active():
                success = False
                self.Log('error', self.error_msg_enable_active_is_active(
                    iteration, expected_is_active=False))

            actual_num_packets_captured = (address - sa) // packet_byte_size
            max_num_packets_allowed = self.num_packets - 10
            if not (self.num_packet_alarm_trigger <=
                    actual_num_packets_captured <= max_num_packets_allowed):
                success = False
                self.Log('error', self.error_msg_address_in_alarm_mode(
                    iteration, actual_num_packets_captured,
                    max_num_packets_allowed))
        else:
            if not self.scopeshot.is_active():
                success = False
                self.Log('error', self.error_msg_enable_active_is_active(
                    iteration, expected_is_active=True))

            address = self.scopeshot.ddr4_latest_address()
            expected_address = sa + (self.num_packets * packet_byte_size)
            if expected_address > address:
                success = False
                self.Log('error', self.error_msg_address_in_trigger_mode(
                    iteration, expected_address, address))

        if not success:
            self.Log('info', f'POUT Alarms before test: '
                             f'{alarms_before_test_string}')
            self.Log('info', f'POUT Alarms after test: '
                             f'{self.spm.spm_pout_alarms_to_string()}')

        self.scopeshot.assert_stop_control()
        self.scopeshot.wait_on_active(asserted=False)

        self.spm.initialize_limits()
        getattr(self.spm, f'clear_spm_{sensor_name}_alarm')()
        assert self.spm.spm_alarm() == 0

        return success


    def clear_memory(self, iteration, address, num_packets, byte_length):
        zero_bytes = bytes(byte_length)
        fail_count = 0
        for i in range(num_packets):
            self.hbirctc.dma_write(address, zero_bytes)
            if self.hbirctc.dma_read(address, byte_length) != zero_bytes:
                self.Log('error',
                         f'Iteration {iteration}) Unable to clear memory')
                fail_count += 1
                if fail_count >= self.max_fail_count:
                    break
            address += byte_length

    def error_msg_address_in_alarm_mode(self, iteration,
                                        actual_num_packets_captured,
                                        max_num_packets_allowed):
        return f'\nIteration {iteration}) Number of packets captured ' \
               f'{actual_num_packets_captured} failed to be within expected ' \
               f'range of {self.num_packet_alarm_trigger} to ' \
               f'{max_num_packets_allowed}.' \
               f'\nTotal packets if no alarm would be {self.num_packets}' \
               f'\nAlarm was set to trigger after the ' \
               f'{self.num_packet_alarm_trigger} capture.' \
               f'\nActual number of packets captured were ' \
               f'{actual_num_packets_captured}'

    def error_msg_address_in_trigger_mode(self, iteration, expected_address,
                                          address):
        return f'Iteration {iteration}) Invalid latest address of ' \
               f'{hex(address)}. Expecting address to be greater than or ' \
               f'equal to {hex(expected_address)}' \
               f'\n\tExpecting scopeshot to continue capture regardless of ' \
               f'pout alarm and only stop when commanded.'

    def DirectedScopeshotSampleCountTest(self):
        """Verify packet capture for a given sample count

        Using sample counts of 1, 20, 0xFF and max value 0xFFFF, run scopeshot
        capture nd after executing a stop command, check for active status and
        number of samples collected. Make sure status is not active at end of
        test.

        A successful test consists of verifying samples collected by
        comparing the latest address value right after the stop command and
        after completing the sample count test. Difference between the two
        should be close the the sample count used.
        """
        try:
            self.spm.enable_spm()
            self.scopeshot.enable()

            self.sample_counts = [1, 20, 0xFF, 0xFFFF]
            shuffle(self.sample_counts)

            for count in self.sample_counts:
                with self.subTest(SAMPLE_COUNT=hex(count)):
                    self.run_sample_count_test(count)

        finally:
            self.scopeshot.set_defaults()
            self.spm.disable_spm()

    def run_sample_count_test(self, sample_count):
        num_packets = 100
        self.scopeshot.set_sample_count(sample_count)
        sa = self.scopeshot.ddr4_starting_address()

        packet_refresh_sec = self.scopeshot.PACKET_REFRESH_RATE_SEC
        self.start_and_sync_scopeshot(sa)
        address_after_stop = 0
        for i in range(num_packets):
            start = perf_counter()
            address_after_stop = self.scopeshot.ddr4_latest_address()
            performance_sleep(packet_refresh_sec - (perf_counter() - start))
        self.scopeshot.assert_stop_control()

        address_after_sample_count = 0
        for i in range(sample_count):
            self.scopeshot.wait_on_packet_refresh()
            address_after_sample_count = self.scopeshot.ddr4_latest_address()

        success = True
        self.scopeshot.wait_on_packet_refresh()

        if self.scopeshot.is_active():
            success = False
            self.Log('error', self.error_msg_sample_count_status())

        packet_byte_size = self.scopeshot.DDR4_ENTRY_BYTE_SIZE
        actual_sample_count = int((address_after_sample_count -
                                   address_after_stop) // packet_byte_size)

        tolerance = 1
        if not isclose(sample_count, actual_sample_count, abs_tol=tolerance):
            success = False
            self.Log('error', self.error_msg_sample_count(sample_count,
                                                          actual_sample_count,
                                                          tolerance))

        self.scopeshot.set_sample_count(self.scopeshot.SAMPLE_COUNT_DEFAULT)

        return success

    def error_msg_sample_count_status(self):
        return 'Status is still active after sample count period.'

    def error_msg_sample_count(self, expected, actual, absolute_tolerance):
        return f'Invalid number of samples captured ' \
               f'after stop command (expected, actual, absolute tolerance): ' \
               f'{hex(expected)}, {hex(actual)}, {absolute_tolerance}'
