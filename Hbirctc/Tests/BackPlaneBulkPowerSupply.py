# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBI has three Bulk Power Supplies (BPS)

FPGA has an i2c master for each BPS
"""

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common.fval import skip
from Common.testbench.i2c_tester import address_nak_test,\
    tx_fifo_count_error_bit_test, tx_fifo_count_test
from Common.utilities import format_docstring
from Hbirctc.instrument.bps_i2c_interface import CAPABILITY_VALUE,\
    create_interfaces
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS =10


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1
        self.bps = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedReadExhaustiveTest(self):
        """Read BPS Capability value

        1) Read BPS Capability register
        2) Verify value read is valid
        3) Repeat {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for bps in self.bps:
                actual_value = bps.read_capability()

                if actual_value != CAPABILITY_VALUE:
                    self.Log('error',
                             self.read_capability_error_msg(
                                 iteration, bps.index, CAPABILITY_VALUE,
                                 actual_value))
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def read_capability_error_msg(self, iteration, interface_num,
                                  expected_value, actual_value):
        return f'Iteration {iteration}, BPS_{interface_num} ' \
               f'(expected, actual): 0x{expected_value:02X}, ' \
               f'0x{actual_value:02X}'


class Functional(HbirctcTest):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.hbirctc)

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than
        two bytes
        1) Create an invalid byte sized message and verify error bit is set
        2) Create a valid byte sized message verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor('FVAL DirectedBpsAddressNakTest',
                                     sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msg = tx_fifo_count_error_bit_test(interface)
                    if error_msg:
                        [self.Log('error', msg) for msg in error_msg]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        1) Create a message with an invalid address and verify error bit is set
        2) Create a message with an valid address and verify error bit is clear
        3) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor('FVAL DirectedBpsAddressNakTest',
                                     sensor_names=['HBIMainBoard', 'HBIDTB']):
                    error_msgs=  address_nak_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        1) Perform 20 random number of data pushes onto the FIFO
        2) Perform a push of corner cases 0 and max fifo size.
        3) Verify Transmit FIFO Count matches total data sent at each push
        4) Repeat above steps for all three controller interfaces a total of
        {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True

            with suppress_sensor('FVAL DirectedBpsTxFifoCountTest',
                                 sensor_names=['HBIMainBoard', 'HBIDTB']):
                for interface in self.interfaces:
                    error_msgs = tx_fifo_count_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedResetTxFifoTest(self):
        """Check FIFO reset feature of the interface

        1) Fill TX FIFO with data and verify appropriate fifo count
        2) Reset FIFO, and verify FIFO count is zero
        3) Repeat a total of {num_iterations} times
        """
