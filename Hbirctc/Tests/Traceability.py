# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Traceability tests

FPGA bitstream and the FPGA itself have unique identifiers. The following
sources will be tested:
- FPGA Version
- HG ID
- Compatibility (Fab Version)
- Chip ID
- Capability
"""

from random import getrandbits

from Common.fval import skip
from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 1000
MAX_FAIL_COUNT = 10


class Image(HbirctcTest):
    """Verify image related traceability (FPGA Version, HG ID, etc.)"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedHgCleanBitTest(self):
        """Determine if FPGA source code is committed

        1) Match HG Clean bit between BAR register and per_build_params file
        2) Read HG Clean bit from BAR register and verify its clean (bit == 1)
        3) Repeat step 2 {num_iterations} times
        """
        hg_clean = self.hbirctc.get_hg_clean_bit()
        from_file = self.hbirctc.hg_clean_from_file()
        if from_file != hg_clean:
            self.Log('error', self.error_msg_hg_clean(from_file, hg_clean))

        for iteration in range(self.test_iterations):
            if self.hbirctc.get_hg_clean_bit() != 1:
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def error_msg_hg_clean(self, per_build_params, bar_register):
        return f'HG Clean bit does not match (from per_build_params file, ' \
               f'from bar register): {per_build_params}, {bar_register}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedHgIdTest(self):
        """Match HG ID between BAR register and per_build_params file

        HG ID value:
        From BAR register:
        Bits    | Register
        ------------------
        47:32   | HG ID Upper (15:0)
        31:0    | HG ID Lower (31:0)

        From image (example):
        hg_id 240c11b31397

        Test steps:
        1) Read HG ID from both the BAR registers and the per_build_params file
        found in the FPGA image release directory
        2) Verify IDs from both sources match
        3) Re-read HG ID BAR register and verify stable value
        4) Repeat step 3 {num_iterations} times
        """
        fpga_hg_id = self.hbirctc.hg_id()
        file_hg_id = self.hbirctc.hg_id_from_file()
        if fpga_hg_id != file_hg_id:
            self.Log('error', self.error_msg_hg_id(file_hg_id, fpga_hg_id))

        for iteration in range(self.test_iterations):
            temp = self.hbirctc.hg_id()
            if fpga_hg_id != temp:
                self.Log('error', self.error_msg(iteration,
                                                 expected=fpga_hg_id,
                                                 actual=temp))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def error_msg_hg_id(self, per_build_params, bar_register):
        return f'HG ID does not match (from per_build_params file, ' \
               f'from bar register): 0x{per_build_params:016X}, ' \
               f'0x{bar_register:016X}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedFpgaVersionTest(self):
        """Match FPGA Version between BAR register and per_build_params file

        1) Read Version from BAR register and per_build_params file
        2) Verify read data from both sources match
        3) Re-read register and verify stable value
        4) Repeat step 3 {num_iterations} times
        """
        version = self.hbirctc.fpga_version()
        from_file = self.hbirctc.fpga_version_from_file()

        if from_file != version:
            self.Log('error', self.error_msg_fpga_version(from_file, version))

        for iteration in range(self.test_iterations):
            temp = self.hbirctc.fpga_version()
            if temp != version:
                self.Log('error',
                         self.error_msg(iteration, version, temp))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def error_msg_fpga_version(self, from_file, from_register):
        return f'FPGA Version mismatch (file name, bar register): ' \
               f'0x{from_file:08X}, 0x{from_register:08X}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedHwCompatibilityTest(self):
        """Match Compatibility between BAR register and per_build_params file

        FPGA Compatibility Register:
        Bits    | Description
        -----------------------
        31:4    | Reserved
        3:0     | 0: Fab A, 1: Fab B, 2: Fab C, Fab D, Fab E, 3: Fab D, Fab E


        1) Read Compatibility from BAR register and per_build_params file
        2) Verify read data from both sources match
        3) Re-read register and verify stable value
        4) Repeat step 3 {num_iterations} times
        """

        from_file = self.hbirctc.compatibility_id_from_file()
        from_bar = self.hbirctc.hardware_compatibility()
        if from_file != from_bar:
            self.Log('error', self.error_msg_hw_compatibility(from_file,
                                                              from_bar))

        for iteration in range(self.test_iterations):
            temp = self.hbirctc.hardware_compatibility()
            if temp != from_bar:
                self.Log('error', self.error_msg(iteration, from_bar, temp))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_msg_hw_compatibility(self, from_image, from_bar):
        return f'Hardware compatibility mismatch ' \
               f'(from file name, from BAR register) {from_image}, {from_bar}'

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedDdrCapabilitiesTest(self):
        """Verify DDR4 Controller present status against Compatibility

        1) Read the status bit from the Capabilities BAR register and the
        hardware compatibility (hc) field from the Compatibility BAR register
        2) Verify status is correct for the appropriate hc value. Status is 1
        if and only if hc is greater than or equal to 3
        3) Re-read status bit and verify stability
        4) Repeat step 3 {num_iterations} times
        """
        hw_compatibility = self.hbirctc.hardware_compatibility()
        ddr_capabilities = self.hbirctc.is_fpga_ddr_capable()

        if ddr_capabilities:
            expected_compatibility = 3
            if hw_compatibility != expected_compatibility:
                self.Log('error',
                         self.error_msg_ddr_capabilities(
                             expected_compatibility, hw_compatibility))

        for iteration in range(self.test_iterations):
            temp = self.hbirctc.is_fpga_ddr_capable()
            if temp != ddr_capabilities:
                self.Log('error', self.error_msg(iteration, ddr_capabilities,
                                                 temp))
                self.update_failed_iterations(False)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_msg_ddr_capabilities(self, expected, actual):
        return f'Invalid Hardware Compatibility ' \
               f'(expected, actual): {expected}, {actual}'

    def error_msg(self, iteration, expected, actual):
        return f'Iteration_{iteration}: expected={expected}, actual={actual}'


class Chip(HbirctcTest):
    """Verify chip related traceability (Chip ID)"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedChipIdTest(self):
        """Determine if Chip identification is a valid number

        Chip ID value:
        Bits    | Register
        ------------------
        63:32   | Chip ID Upper (31:0)
        31:0    | Chip ID Lower (31:0)

        Test steps:
        1) Read Chip ID Upper and Upper registers
        2) Concatenate both values from step one and verify value is not 0 or
        0xFFFF_FFFF_FFFF_FFFF
        3) Repeat test {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True
            upper, lower = self.hbirctc.chip_id()
            chip_id = upper << 32 | lower
            if chip_id == 0:
                self.Log('error', f'Iteration_{iteration}: '
                                  f'Invalid Chip ID value of 0x0')
                success = False

            if upper == 0xFFFFFFFF:
                self.Log('error', f'Iteration_{iteration}: '
                                  f'Invalid Chip ID Upper of 0xFFFFFFFF')
                success = False

            if lower == 0xFFFFFFFF:
                self.Log('error', f'Iteration_{iteration}: '
                                  f'Invalid Chip ID Lower of 0xFFFFFFFF')
                success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()


class Functional(HbirctcTest):
    """Test operation of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedHpsCommunicationsRegWriteReadExhaustiveTest(self):
        """Validate R/W register access of the Hps Communications register

        1) Write random data and read it back
        2) Verify data written is equal to data read
        3) Repeat above steps a total of {num_iterations} times
        """
        default = self.hbirctc.hps_communications()

        try:
            for iteration in range(self.test_iterations):
                expected = self.generate_dword()
                self.hbirctc.write_hps_communications(expected)
                actual = self.hbirctc.hps_communications()

                if actual != expected:
                    self.Log('error', self.error_msg_mismatch(
                        iteration, expected, actual))
                    self.update_failed_iterations(success=False)

                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()
        finally:
            self.hbirctc.write_hps_communications(default)
            temp = self.hbirctc.hps_communications()
            if temp != default:
                self.Log('error',
                         self.error_msg_default('HPS_COMMUNICATIONS',
                                                default, temp))

    def generate_dword(self):
        return getrandbits(32)

    def error_msg_mismatch(self, iteration, expected, actual):
        return f'Iteration {iteration}) Mismatch (expected, actual): ' \
               f'{hex(expected)}, {hex(actual)}'

    def error_msg_default(self, name, expected, actual):
        return f'{name} default value failure (expected, actual) : ' \
               f'{hex(expected)}, {hex(actual)}'
