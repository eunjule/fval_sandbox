# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""FPGA uses an Aurora Trigger interface

FPGA can communicate information using the Aurora Trigger to up to 19 trigger
link partners

Link Partners:
    Trigger Link 18 - Debug Trigger Link.
    Trigger Link 17 - Ring Multiplier FPGA.
    Trigger Link 16 - Pattern Generator FPGA.
    Trigger Link 15 - PSDB1 DPS 7.
    Trigger Link 14 - PSDB1 DPS 6.
    Trigger Link 13 - PSDB1 DPS 5.
    Trigger Link 12 - PSDB1 DPS 4.
    Trigger Link 11 - PSDB1 DPS 3.
    Trigger Link 10 - PSDB1 DPS 2.
    Trigger Link 9 - PSDB1 DPS 1.
    Trigger Link 8 - PSDB1 DPS 0.
    Trigger Link 7 - PSDB0 DPS 7.
    Trigger Link 6 - PSDB0 DPS 6.
    Trigger Link 5 - PSDB0 DPS 5.
    Trigger Link 4 - PSDB0 DPS 4.
    Trigger Link 3 - PSDB0 DPS 3.
    Trigger Link 2 - PSDB0 DPS 2.
    Trigger Link 1 - PSDB0 DPS 1.
    Trigger Link 0 - PSDB0 DPS 0.
"""

from Common.fval import skip
from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 100000
MAX_FAIL_COUNT = 2


class Diagnostics(HbirctcTest):
    """Test communication robustness between trigger link partners"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedRcToInstrumentTest(self):
        """Verify receipt of a non-software trigger sent by Rc to instrument

        1) Send a randomized payload trigger from Rc to an instrument
        2) Read data at corresponding instrument and confirm
        3) Repeat steps 1 and 2 a total of {num_iterations} times
        4) Repeat above steps for all available instruments
        """

    @skip('Not implemented')
    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedInstrumentToRcTest(self):
        """Verify receipt of a non-software trigger sent by instrument to Rc

        1) Send a randomized payload trigger from an instrument to the Rc
        2) Read data at Rc and confirm
        3) Verify broadcast of trigger if applicable
        4) Repeat steps 1 to 3 a total of {num_iterations} times
        5) Repeat above steps for all available instruments
        """
