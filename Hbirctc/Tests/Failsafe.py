# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""FPGA controls Failsafe Reset and Failsafe Limits

Users can set a specific failsafe limit in seconds and when
timer reaches zero, the FPGA shuts down the tester via the failsafe
control register.

NOTE:
    FVAL can not test the execution of failsafe. In order to prevent
    the tester from shutting down during a failsafe event, the user must
    use the failsafe bypass switch. It's not feasible for FVAL to do this
    therefore will not be testing the execution of a failsafe.

    The validation of the actual failsafe and subsequent tester shutdown
    will be tested elsewhere.
"""

from time import sleep

from Common.fval import skip
from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest

FAILSAFE_LIMIT_DEFAULT = 360


@skip('Need to debug further. Talk to Ken Ito who worked on Max10Cpld and '
      'verify test is appropriate')
class Diagnostics(HbirctcTest):
    """Test communication robustness"""
    FAILSAFE_LIMIT_TIME = 1
    TEST_ITERATIONS = 5

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Diagnostics.TEST_ITERATIONS
        self.max_fail_count = 2

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      wait_seconds=FAILSAFE_LIMIT_TIME)
    def DirectedTriggerSystemFailsafeTest(self):
        """Validate a system failsafe fault

        1) Mask failsafe power down at the FrontPanel (Max10) CPLD
        2) Set the failsafe limit register to {wait_seconds} second
        3) Wait on failsafe at CPLD
        4) Clear failsafe and verify at CPLD
        5) Repeat steps 3 and 4 {num_iterations} times
        """

        self.hbirctc.max10.enable_sys_fail_safe_mask()
        self.hbirctc.write_failsafe_limit(Diagnostics.FAILSAFE_LIMIT_TIME)
        self.hbirctc.reset_failsafe_limit()
        self.hbirctc.disable_fail_safe()

        enable_pass = {}
        disable_pass = {}
        for i in range(self.test_iterations):
            self.hbirctc.enable_fail_safe()
            enable_pass[i] = self.wait_on_sys_fail_safe(is_enabled=True)
            self.hbirctc.disable_fail_safe()
            disable_pass[i] = self.wait_on_sys_fail_safe(is_enabled=False)

        self.reset_failsafe_to_default()
        self.hbirctc.disable_fail_safe()
        self.hbirctc.max10.clear_int0_error_latch()
        self.hbirctc.max10.enable_sys_fail_safe_mask(False)

        for i in range(self.test_iterations):
            success = True

            if not enable_pass[i]:
                success = False
                self.Log('error', self.error_msg_sys_fail_safe(i,
                                                               expected=True))
            if not disable_pass[i]:
                success = False
                self.Log('error', self.error_msg_sys_fail_safe(i,
                                                               expected=False))
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def wait_on_sys_fail_safe(self, is_enabled=True, num_retries=3000):
        status = False
        for retry in range(num_retries):
            if self.hbirctc.max10.sys_fail_safe() == is_enabled:
                status = True
                break
            sleep(0.001)  # 0.001 * 3000, will run a max of 3 seconds
        else:
            self.Log('warning', f'wait_on_sys_fail_safe did not complete.')
        return status

    def error_msg_sys_fail_safe(self, iteration, expected):
        return f'Iteration {iteration}) sys_fail_safe (expected, actual): ' \
               f'{int(expected)}, {int(not expected)}'

    def reset_failsafe_to_default(self):
        default = self.hbirctc.failsafe_limit_default()
        if default is None:
            default = 360
        self.hbirctc.write_failsafe_limit(default)
        self.hbirctc.reset_failsafe_limit()
