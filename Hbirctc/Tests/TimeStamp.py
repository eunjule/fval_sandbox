# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""FPGA has a Time Stamp Interface

Interface components:
- Live Time Stamp PCIe BAR Register: This read-only register indicates a timer
value in microseconds. This timer runs on the MS Clock.
"""

from time import perf_counter

from Common import hilmon as hil
from Common.utilities import format_docstring
from Hbirctc.instrument.hbirctc_register import LIVE_TIME_STAMP
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = int(1e6)


class Functional(HbirctcTest):
    """Test operation of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 10
        self.fpga_index = 0
        self.pcie_bar_num = 1
        self.time_stamp_bar_addr = LIVE_TIME_STAMP.ADDR

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedLiveTimeStampReadTest(self):
        """Verify incremental count of the Live Time Stamp register

        1) Read the register at constant intervals
        2) Verify value read increments accordingly
        3) Repeat above steps a total of {num_iterations} times
        """

        stamps = []
        elapsed_times = []
        for iteration in range(self.test_iterations):
            start_time = perf_counter()
            time_stamp = self.read_time_stamps()
            time_diff = self.calculate_elapsed_time_us(start_time)
            stamps.append(time_stamp)
            elapsed_times.append(time_diff)

            success = self.validate_time_stamp_range(iteration, time_stamp,
                                                     time_diff)
            success &= self.validate_time_stamp_increment(iteration,
                                                          time_stamp)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def read_time_stamps(self):
        stamp_0 = hil.hbiMbBarRead(self.fpga_index, self.pcie_bar_num,
                                   self.time_stamp_bar_addr)
        stamp_1 = hil.hbiMbBarRead(self.fpga_index, self.pcie_bar_num,
                                   self.time_stamp_bar_addr)
        return [stamp_0, stamp_1]

    def calculate_elapsed_time_us(self, start_time):
        return (perf_counter() - start_time) * 1e6

    def validate_time_stamp_range(self, iteration, time_stamp, elapsed_time):
        time_stamp_diff = int(time_stamp[1] - time_stamp[0])
        max_time_stamp_diff = int(elapsed_time)

        if not 0 < time_stamp_diff <= max_time_stamp_diff:
            self.Log('error', self.error_msg_time_stamp_range(
                iteration, time_stamp, elapsed_time))
            return False
        else:
            return True

    def error_msg_time_stamp_range(self, iteration, time_stamp, elapsed_time):
        time_stamp_diff = int(time_stamp[1] - time_stamp[0])
        max_time_stamp_diff = int(elapsed_time)

        return f'Iteration {iteration}) Failed time stamp range ' \
               f'(stamp_1, stamp_0, time_stamp_diff, max_time_stamp_diff, ' \
               f'time_elapsed_us): {time_stamp[1]}, {time_stamp[0]}, ' \
               f'{time_stamp_diff}, {max_time_stamp_diff}, {elapsed_time}'

    def validate_time_stamp_increment(self, iteration, time_stamp):
        if time_stamp[1] <= time_stamp[0]:
            self.Log('error', self.error_msg_time_stamp_increment(iteration,
                                                                  time_stamp))
            return False
        else:
            return True

    def error_msg_time_stamp_increment(self, iteration, time_stamp):
        return f'Iteration {iteration}) Failed time stamp increment ' \
               f'(stamp_0, stamp_1): {time_stamp[0]}, {time_stamp[1]}'
