# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC uses PCIe DMA into memory

Two memory types available:
- 2GB of DDR: Available on Fab D with hardware compatibility of 3
- 512B of RAM: Available on Fab C or lower Fab
"""

import os

from Common.fval import SkipTest
from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest

TEST_ITERATIONS = 5
RAM_TEST_ITERATIONS = 100


class Diagnostics(HbirctcTest):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 1

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedDMAWriteReadDDRTest(self):
        """Write random data to the DDR and read it back

        1) Verify DDR has passed calibration. If passed, continue, otherwise
        fail test
        2) DMA write random data to first half of DDR, read it back and verify
        it matches
        3) DMA write random data to second half of DDR, read it back and verify
        it matches
        4) Repeat steps 2 to 3 a total of {num_iterations} times
        """
        self.skip_if_non_ddr_board()

        if not self.hbirctc.ddr_is_ready():
            self.Log('error', self.ddr4_calibration_fail_msg())
            self.update_failed_iterations(success=False)
        else:
            memory_chunk_count = 2
            chunk_size = 1 << 30

            for iteration in range(self.test_iterations):
                last_memory_address = 0
                success = True

                for total_memory in range(memory_chunk_count):
                    memory_start_address = last_memory_address
                    random_test_data = os.urandom(chunk_size)

                    read_back_data = self.dma_write_read_transaction(
                        memory_start_address, random_test_data)

                    if random_test_data != read_back_data:
                        self.Log('error', self.dma_mismatch_msg(
                            iteration, memory_start_address,
                            memory_start_address + chunk_size))
                        success = False

                    last_memory_address = last_memory_address + chunk_size

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()

    def ddr4_calibration_fail_msg(self):
        status = self.hbirctc.read_bar_register(
            self.hbirctc.registers.FPGA_DDR4_STATUS).value
        return f'DDR is not ready. FPGA_DDR4_STATUS status: 0x{status:08X}'

    @format_docstring(num_iterations=RAM_TEST_ITERATIONS)
    def DirectedDMAWriteReadRAMTest(self):
        """Write random data in all of RAM and read it back

        1) DMA write 512B of random data at address 0
        2) DMA read 512B from address 0
        3) Verify written data matches read data
        4) Repeat steps 1 to 3 a total of {num_iterations} times
        """
        fab_letter = self.hbirctc.get_fab_letter()
        if fab_letter > 'C':
            raise SkipTest(f'Only Fab C or less FPGA\'s are using internal '
                           f'RAM with DMA access. Current Fab is {fab_letter}')

        self.test_iterations = RAM_TEST_ITERATIONS
        chunk_size = 512
        start_address = 0

        for iteration in range(self.test_iterations):
            random_test_data = os.urandom(chunk_size)

            read_back_data = self.dma_write_read_transaction(start_address,
                                                             random_test_data)

            if random_test_data != read_back_data:
                self.Log('error', self.dma_mismatch_msg(
                    iteration, start_address, start_address + chunk_size))
                self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def dma_mismatch_msg(self, iteration, start_address, end_address):
        return f'Iteration {iteration}. Data compare mismatch for memory ' \
               f'range : 0x{start_address:08X} - 0x{end_address:08X}'

    def dma_write_read_transaction(self, start_address, write_data):
        self.hbirctc.dma_write(start_address, write_data)
        return self.hbirctc.dma_read(start_address, len(write_data))

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedMemoryRecalibrationTest(self):
        """Verify re-calibration of memory by resetting controller

        After re-calibration, data starting at address 0 will contain
        calibration data and any data will be over-written
        1) Write 1KB of randomized data to memory at address 0
        2) Read the data back and make sure it matches
        3) Perform DDR re-calibration
        4) Read 1KB of data at address 0
        5) Make sure data does not match that written at step 1
        6) Repeat above steps a total of {num_iterations} times
        """
        self.skip_if_non_ddr_board()

        byte_size = 1 << 10

        for iteration in range(self.test_iterations):
            success = True

            expected = os.urandom(byte_size)
            self.hbirctc.dma_write(address=0, data=expected)
            actual = self.hbirctc.dma_read(address=0, num_bytes=byte_size)
            if actual != expected:
                success = False
                self.Log('error', self.error_msg_ddr_reset(iteration,
                                                           is_mismatch=False))
            self.reset_ddr()
            actual = self.hbirctc.dma_read(address=0, num_bytes=byte_size)
            if actual == expected:
                success = False
                self.Log('error', self.error_msg_ddr_reset(iteration,
                                                           is_mismatch=True))

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def reset_ddr(self):
        self.hbirctc.issue_ddr_reset()
        if not self.hbirctc.is_ddr_reset_done_after_wait():
            raise Diagnostics.DdrResetException(
                f'Reset sequence for DDR4 Controller did NOT complete.')
        if not self.hbirctc.is_status_calibration_after_wait():
            raise Diagnostics.DdrResetException(
                f'DDR4 calibration did NOT complete after a reset.')

    class DdrResetException(Exception):
        pass

    def error_msg_ddr_reset(self, iteration, is_mismatch):
        if is_mismatch:
            return f'Iteration {iteration}: Data did not clear after ' \
                   f're-calibration'
        else:
            return f'Iteration {iteration}: Data written did not match data ' \
                   f'read before re-calibration'

    def skip_if_non_ddr_board(self):
        fpga_ddr_capable = self.hbirctc.is_fpga_ddr_capable()
        mainboard_ddr_capable = self.hbirctc.is_mainboard_ddr_capable()
        if not (fpga_ddr_capable and mainboard_ddr_capable):
            raise SkipTest(
                f'Mainboard and image are not DDR4 ready (MB ready, '
                f'Image ready): {mainboard_ddr_capable}, {fpga_ddr_capable}')
