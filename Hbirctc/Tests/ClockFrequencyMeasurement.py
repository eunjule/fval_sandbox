# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""FPGA has 8 Clock Frequency Measurement registers

FPGA has measurement logic for calculating observed frequencies.
"""

from enum import Enum
from math import isclose

from Common.utilities import format_docstring
from Hbirctc.Tests.HbirctcTest import HbirctcTest


TEST_ITERATIONS = 1000
DELTA_PERCENTAGE = 2 / 100.0


class FrequencyMeasurement(HbirctcTest):
    """Test clock frequencies

    The following clocks are measured:
    Clock Name      | Expected frequency (MHz)
    ------------------------------------------
    PCIe ref clk    | 100
    PCIe user clk   | 125
    FPGA DDR ref clk| 33.33
    HPS DDR ref clk | 33.33
    MS Clk_0        | 125
    MS Clk_1        | 125
    MS Clk_2        | 125
    HPS user clk    | 125
    """

    class Clocks(Enum):
        PCIE_REF_CLK = 0
        PCIE_USER_CLK = 1
        FPGA_DDR4_REF_CLK = 2
        HPS_DDR4_REF_CLK = 3
        MS_CLK_0 = 4
        MS_CLK_1 = 5
        MS_CLK_2 = 6
        HPS_USER_CLK = 7

    EXPECTED_FREQS = {Clocks.PCIE_REF_CLK.value: 100.0,
                      Clocks.PCIE_USER_CLK.value: 125.0,
                      Clocks.FPGA_DDR4_REF_CLK.value: 33.33,
                      Clocks.HPS_DDR4_REF_CLK.value: 33.33,
                      Clocks.MS_CLK_0.value: 125.0,
                      Clocks.MS_CLK_1.value: 125.0,
                      Clocks.MS_CLK_2.value: 125.0,
                      Clocks.HPS_USER_CLK.value: 125.0}

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 10

    @format_docstring(num_iterations=TEST_ITERATIONS,
                      delta=int(DELTA_PERCENTAGE*100))
    def DirectedFrequencyReadTest(self):
        """Verify each clock measurement has a valid frequency

        1) Read measurement register and calculate corresponding frequency
        2) Verify measure is within +/- {delta}% of expected
        3) Repeat above steps {num_iterations} times
        4) Repeat until all frequencies are measured
        """

        for clock in FrequencyMeasurement.Clocks:
            with self.subTest(CLOCK=clock.name):
                self.fail_count = 0
                for iteration in range(self.test_iterations):
                    measured_freq = \
                        self.hbirctc.read_clock_frequency_mhz(clock.value)
                    expected_freq = \
                        FrequencyMeasurement.EXPECTED_FREQS[clock.value]
                    delta_compare = DELTA_PERCENTAGE * expected_freq

                    if not isclose(a=measured_freq, b=expected_freq,
                                   abs_tol=delta_compare):
                        self.Log('error', self.error_msg_frequency_msimatch(
                            iteration, clock.name,
                            expected_freq, measured_freq))
                        self.update_failed_iterations(success=False)

                    if self.fail_count >= self.max_fail_count:
                        break
                self.validate_iterations()

    def error_msg_frequency_msimatch(self, iteration, name, expected, measured):
        return f'Iteration {iteration}) {name} MHz is not within ' \
               f'{int(DELTA_PERCENTAGE * 100)}% ' \
               f'(expected, actual): {expected:0.02f}, {measured:0.02f}'
