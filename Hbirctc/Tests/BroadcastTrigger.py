# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""FPGA has a Broadcast Trigger Interface

FPGA can generate Aurora Trigger Down packets to all connected trigger link
partners simultaneously
"""
from random import getrandbits

from Common.fval import skip
from Common.utilities import format_docstring, negative_one_bits
from Hbirctc.Tests.HbirctcTest import HbirctcTest


class Diagnostics(HbirctcTest):
    """Test communication of the interface"""
    TEST_ITERATIONS = 100000

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Diagnostics.TEST_ITERATIONS
        self.max_fail_count = 2

    @skip('Not implemented')
    @format_docstring(times=TEST_ITERATIONS)
    def DirectedSendBroadcastTriggerTest(self):
        """Send a trigger to all connected trigger link partners

        1) Set the Broadcast Trigger Down register with a 32-bit random number
        2) Verify receipt on all connected partners
        3) Repeat above steps a total of {times} times
        """


class Functional(HbirctcTest):
    """Test operation of the interface"""
    TEST_ITERATIONS = 10000

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = Functional.TEST_ITERATIONS

    def DirectedLastBroadcastedTriggerTest(self):
        """Validate function of Last Broadcasted Trigger PCie register

        1) Set the Broadcast Trigger Down register with a 32-bit random number
        2) Verify Last Broadcasted Trigger register matches trigger sent
        3) Repeat above steps a total of {times} times
        """

        for iteration in range(self.test_iterations):
            trigger = self.generate_trigger(iteration)
            self.hbirctc.send_broadcast_trigger(trigger)

            if not self.validate_last_broadcast_trigger(iteration, trigger):
                self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def generate_trigger(self, iteration, trigger_bit_size=32):
        if iteration == 0:
            trigger = 0
        elif iteration == 1:
            trigger = negative_one_bits(trigger_bit_size)
        else:
            trigger = getrandbits(trigger_bit_size)
        return trigger

    def validate_last_broadcast_trigger(self, iteration, expected_trigger):
        last_trigger = self.hbirctc.read_last_broadcasted_trigger()
        if expected_trigger != last_trigger:
            self.Log('error', self.error_msg_trigger_mismatch(
                iteration, expected_trigger, last_trigger))
            return False
        else:
            return True

    def error_msg_trigger_mismatch(self, iteration, expected, actual):
        return f'Iteration {iteration}) Last Broadcasted trigger mismatch ' \
               f'(expected, actual): 0x{expected:08X}, 0x{actual:08X}'
