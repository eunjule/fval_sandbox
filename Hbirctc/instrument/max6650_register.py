################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
from enum import Enum

import Common.register as register

MAX_ADDRESS = 0x16
READONLY_ADDRESS = [0x0A, 0x0C, 0x0E, 0x10, 0x12, 0x1E]


class SPEED_REG(register.Register):
    ADDR = 0x00
    _fields_ =  [('data', ctypes.c_uint, 8)] #6-7


class CONFIG_REG(register.Register):
    ADDR = 0x02
    _fields_ =  [('scale', ctypes.c_uint, 3), #0-2
                 ('voltage', ctypes.c_uint, 1), #1
                 ('mode', ctypes.c_uint, 2), #4-5
                 ('reserved', ctypes.c_uint, 2)] #6-7


class ConfigModes(Enum):
    FULL_ON = 0
    OFF = 1
    CLOSED_LOOP = 2
    OPEN_LOOP = 3
