# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from enum import Enum

from Common.fval import Object
from Hbirctc.instrument.hbirctc_register import PECI_DAC_COMMAND_DATA, \
    PECI_DAC_STATUS

PECI_DAC_CHANNELS = [1, 2, 4, 8]
class PeciDacCommands(Enum):
    nop = 0
    update_dac = 0b0011
    read_back = 0b1001


class PeciDac(Object):
    def __init__(self, rc):
        super().__init__()
        self.rc = rc

    def dac_output(self, channel):
        if self.is_valid_channel(channel):
            self.send_read_back_command(channel)
            self.send_nop_command()
            self.wait_on_busy()
            return self.read_data()

    def send_read_back_command(self, channel):
        reg = PECI_DAC_COMMAND_DATA(command=PeciDacCommands.read_back.value,
                                    channel=channel)
        self.rc.write_bar_register(reg)

    def send_nop_command(self):
        reg = PECI_DAC_COMMAND_DATA(command=PeciDacCommands.nop.value)
        self.rc.write_bar_register(reg)

    def wait_on_busy(self):
        num_retries = 100
        for retry in range(num_retries):
            if not self.status_reg().busy:
                break
        else:
            self.Log('error', self.error_msg_busy_timeout())

    def error_msg_busy_timeout(self):
        return f'PeciDac: Timed out waiting on busy bit'

    def read_data(self):
        dont_care_bits = 4
        reg = self.status_reg()
        return reg.data >> dont_care_bits

    def status_reg(self):
        return self.rc.read_bar_register(PECI_DAC_STATUS)

    def write_dac(self, channel, data):
        if self.is_valid_channel(channel):
            reg = PECI_DAC_COMMAND_DATA(
                command=PeciDacCommands.update_dac.value, channel=channel,
                data=data)
            self.rc.write_bar_register(reg)

    def is_valid_channel(self, channel):
        if channel not in PECI_DAC_CHANNELS:
            self.Log('warning', self.warning_msg_invalid_channel(channel))
            return False
        else:
            return True

    def warning_msg_invalid_channel(self, channel):
        return f'PECI DAC read error. Invalid channel {channel} chosen. ' \
               f'Acceptable channels {PECI_DAC_CHANNELS}'
