# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import ctypes
from ctypes import c_uint

import Common.register as register

module_dict = globals()


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbirctcRegister):
                if attribute is not HbirctcRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbirctcRegister):
                if attribute is not HbirctcRegister:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbirctcStruct):
                if attribute is not HbirctcStruct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbirctcStruct):
                if attribute is not HbirctcStruct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class HbirctcRegister(register.Register):
    BASEMUL = 4
    BAR = 1


class HbirctcStruct(register.Register):
    ADDR = None

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class SCRATCH_REG(HbirctcRegister):
    BAR = 1
    ADDR = 0x0
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_VERSION(HbirctcRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Minor', ctypes.c_uint, 16),
                ('Major', ctypes.c_uint, 16)]


class HG_ID_LOWER(HbirctcRegister):
    BAR = 1
    ADDR = 0x24
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_UPPER(HbirctcRegister):
    BAR = 1
    ADDR = 0x28
    _fields_ = [('Data', ctypes.c_uint, 16),  # 0
                ('reserved', ctypes.c_uint, 15),
                ('hg_clean', ctypes.c_uint, 1)]  # 31


class TARGET_HARDWARE(HbirctcRegister):
    BAR = 1
    ADDR = 0x2C
    _fields_ = [('hw_compatibility', ctypes.c_uint, 4),
                ('reserved', c_uint, 28)]


class CHIP_ID_UPPER(HbirctcRegister):
    BAR = 1
    ADDR = 0x30
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_LOWER(HbirctcRegister):
    BAR = 1
    ADDR = 0x34
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HPS_COMMUNICATIONS(HbirctcRegister):
    BAR = 1
    ADDR = 0x038
    _fields_ = [('communications', ctypes.c_uint, 32)]


class FPGA_CAPABILITY(HbirctcRegister):
    BAR = 1
    ADDR = 0x03C
    _fields_ = [('ddr_capable', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 1),
                ('system_power_monitoring_capable', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 29)]


class MAX10_CPLD_COMMAND_DATA(HbirctcRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('data', c_uint, 16),
                ('offset', c_uint, 15),
                ('read_write', c_uint, 1)]


class MAX10_CPLD_READ_DATA(HbirctcRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('data', c_uint, 16),
                ('reserved', c_uint, 16)]


class MAX10_CPLD_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('tx_fifo_count', c_uint, 10),
                ('reserved', c_uint, 6),
                ('rx_fifo_count', c_uint, 10),
                ('reserved', c_uint, 4),
                ('interrupt', c_uint, 1),
                ('busy', c_uint, 1)]


class MAX10_CPLD_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x4c
    _fields_ = [('controller_and_fifo_reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class HPS_SLAVE_TX_DATA(HbirctcRegister):
    BAR = 1
    ADDR = 0x60
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HPS_SLAVE_RX_DATA(HbirctcRegister):
    BAR = 1
    ADDR = 0x64
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRIGGER_SYNC(HbirctcRegister):
    BAR = 1
    ADDR = 0x80
    _fields_ = [('trigger_sync_to_PSDB0_DPS0', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS1', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS2', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS3', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS4', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS5', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS6', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB0_DPS7', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS0', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS1', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS2', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS3', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS4', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS5', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS6', ctypes.c_uint, 1),
                ('trigger_sync_to_PSDB1_DPS7', ctypes.c_uint, 1),
                ('trigger_sync_to_pattern_generator', ctypes.c_uint, 1),
                ('trigger_sync_to_ring_multiplier', ctypes.c_uint, 1),
                ('trigger_sync_to_debug', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 12),
                ('time_stamp_reset', c_uint, 1)]


class BROADCAST_TRIGGER_DOWN(HbirctcRegister):
    BAR = 1
    ADDR = 0xE0
    _fields_ = [('broadcast_trigger_down', ctypes.c_uint, 32)]


class AURORA_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x100
    REGCOUNT = 19
    BASEMUL = 32
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('clear_fifos', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 29)]


class AURORA_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x104
    REGCOUNT = 19
    BASEMUL = 32
    _fields_ = [('transceiver_pll_locked', ctypes.c_uint, 1),
                ('lane_up', ctypes.c_uint, 1),
                ('channel_up', ctypes.c_uint, 1),
                ('soft_error', ctypes.c_uint, 1),
                ('hard_error', ctypes.c_uint, 1),
                ('tx_pll_lock', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 2),
                ('rx_fifo_count', ctypes.c_uint, 11),
                ('rx_fifo_full', ctypes.c_uint, 1),
                ('tx_fifo_count', ctypes.c_uint, 11),
                ('tx_fifo_full', ctypes.c_uint, 1)]


class AURORA_ERROR_COUNTS(HbirctcRegister):
    BAR = 1
    ADDR = 0x108
    REGCOUNT = 19
    BASEMUL = 32
    _fields_ = [('soft_error_count', ctypes.c_uint, 16),
                ('hard_error_count', ctypes.c_uint, 16)]


class AURORA_TRIGGER_UP(HbirctcRegister):
    BAR = 1
    ADDR = 0x10c
    REGCOUNT = 19
    BASEMUL = 32
    _fields_ = [('trigger_up', ctypes.c_uint, 32)]


class AURORA_TRIGGER_DOWN(HbirctcRegister):
    BAR = 1
    ADDR = 0x110
    REGCOUNT = 19
    BASEMUL = 32
    _fields_ = [('trigger_down', ctypes.c_uint, 32)]


class GENERAL_PURPOSE_INPUT(HbirctcRegister):
    BAR = 1
    ADDR = 0x360
    _fields_ = [('lv_bm_ver', ctypes.c_uint, 4),
                ('spare_gpi', ctypes.c_uint, 2),
                ('handler_present_in', ctypes.c_uint, 1),
                ('run_1p8V_48V_bst', ctypes.c_uint, 1),
                ('system_1p8V_power_good', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 23)]


class FPGA_DEBUG(HbirctcRegister):
    BAR = 1
    ADDR = 0x364
    _fields_ = [('output', ctypes.c_uint, 8),
                ('control', ctypes.c_uint, 8),
                ('input', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 8)]


class FPGA_LED(HbirctcRegister):
    BAR = 1
    ADDR = 0x368
    _fields_ = [('led_control', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 24)]


class GENERAL_PURPOSE_OUTPUT(HbirctcRegister):
    BAR = 1
    ADDR = 0x36C
    _fields_ = [('spare_gpo', ctypes.c_uint, 2),
                ('handler_present_out', ctypes.c_uint, 1),
                ('rctc_tos_ready', ctypes.c_uint, 1),
                ('rctc_ground_fault_detection', ctypes.c_uint, 1),
                ('system_fail_safe', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 26)]


class INTERRUPT_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x370
    _fields_ = [('trigger_interrupt', c_uint, 1),
                ('spm_interrupt', c_uint, 1),
                ('reserved', c_uint, 30)]


class INTERRUPT_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x374
    _fields_ = [('trigger_interrupt_enable', c_uint, 1),
                ('spm_interrupt_enable', c_uint, 1),
                ('reserved', c_uint, 30)]


class POWER_ON_COUNT(HbirctcRegister):
    BAR = 1
    ADDR = 0x378
    _fields_ = [('data', c_uint, 32)]


class FPGA_DDR4_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x380
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 31)]


class FPGA_DDR4_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x384
    _fields_ = [('calibration_fail', ctypes.c_uint, 1),
                ('calibration_success', ctypes.c_uint, 1),
                ('reset_done', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 29)]


class FAN_CONTROLLER_I2C_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x600
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class FAN_CONTROLLER_I2C_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x604
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]


class FAN_CONTROLLER_I2C_TX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0x608
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class FAN_CONTROLLER_I2C_RX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0x60c
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class FAN_CONTROLLER_I2C_RESET(HbirctcRegister):
    BAR = 1
    ADDR = 0x610
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class BPS_I2C_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x660
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class BPS_I2C_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x664
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]


class BPS_I2C_TX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0x668
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class BPS_I2C_RX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0x66c
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class BPS_I2C_RESET(HbirctcRegister):
    BAR = 1
    ADDR = 0x670
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class PMBUS_MONITOR_I2C_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0xB20
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class PMBUS_MONITOR_I2C_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0xB24
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]


class PMBUS_MONITOR_I2C_TX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0xB28
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class PMBUS_MONITOR_I2C_RX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0xB2c
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class PMBUS_MONITOR_I2C_RESET(HbirctcRegister):
    BAR = 1
    ADDR = 0xB30
    REGCOUNT = 3
    BASEMUL = 32
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class PECI_DAC_COMMAND_DATA(HbirctcRegister):
    BAR = 1
    ADDR = 0x6C0
    _fields_ = [('reserved_0', c_uint, 4),
                ('data', c_uint, 12),
                ('channel', c_uint, 4),
                ('command', c_uint, 4),
                ('reserved_1', c_uint, 8)]


class PECI_DAC_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x6C4
    _fields_ = [('data', ctypes.c_uint, 24),
                ('reserved', ctypes.c_uint, 7),
                ('busy', ctypes.c_uint, 1)]


class PECI_DAC_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x6C8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class BP_ALARMS(HbirctcRegister):
    BAR = 1
    ADDR = 0x6E0
    _fields_ = [('bps_0_alarm', ctypes.c_uint, 1),
                ('bps_1_alarm', ctypes.c_uint, 1),
                ('bps_2_alarm', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 29)]


class SPM_ALARMS(HbirctcRegister):
    BAR = 1
    ADDR = 0x6E4
    _fields_ = [('bps_0_spm_pout', ctypes.c_uint, 1),
                ('bps_1_spm_pout', ctypes.c_uint, 1),
                ('bps_2_spm_pout', ctypes.c_uint, 1),
                ('spm_pout_sum', ctypes.c_uint, 1),
                ('bps_0_spm_i2c_addr_nak', ctypes.c_uint, 1),
                ('bps_0_spm_i2c_data_nak', ctypes.c_uint, 1),
                ('bps_1_spm_i2c_addr_nak', ctypes.c_uint, 1),
                ('bps_1_spm_i2c_data_nak', ctypes.c_uint, 1),
                ('bps_2_spm_i2c_addr_nak', ctypes.c_uint, 1),
                ('bps_2_spm_i2c_data_nak', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 22)]


class GLOBAL_ALARMS(HbirctcRegister):
    BAR = 1
    ADDR = 0x6E8
    _fields_ = [('active_bp_alarm', ctypes.c_uint, 1),
                ('active_spm_alarm', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class BLT_I2C_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0x700
    REGCOUNT = 8
    BASEMUL = 32
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class BLT_I2C_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0x704
    REGCOUNT = 8
    BASEMUL = 32
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]


class BLT_I2C_TX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0x708
    REGCOUNT = 8
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class BLT_I2C_RX_FIFO(HbirctcRegister):
    BAR = 1
    ADDR = 0x70c
    REGCOUNT = 8
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class BLT_I2C_RESET(HbirctcRegister):
    BAR = 1
    ADDR = 0x710
    REGCOUNT = 8
    BASEMUL = 32
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class TEMPERATURE_SENSOR_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0xa00
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 27),
                ('channel_select', c_uint, 4)]


class TEMPERATURE_SENSOR_DATA(HbirctcRegister):
    BAR = 1
    ADDR = 0xa04
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FAILSAFE_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0xB00
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('reserved',c_uint, 31)]


class FAILSAFE_LIMIT(HbirctcRegister):
    BAR = 1
    ADDR = 0xB04
    _fields_ = [('limit', ctypes.c_uint, 32)]


class FPGA_TO_THERM_DATA_EN(HbirctcRegister):
    BAR = 1
    ADDR = 0xf00
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_TO_THERM_DATA_OUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xf04
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_TO_THERM_DATA_IN(HbirctcRegister):
    BAR = 1
    ADDR = 0xf08
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_TO_THERM_CLK_OUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xf0c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class S2H_BD_PACKET_BYTE_COUNT(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA40
    _fields_= [ ('count',   c_uint, 16),
                ('reserved',c_uint, 16)]


class S2H_BD_VALID_PACKET_INDEX(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA44
    _fields_= [ ('index',   c_uint, 16),
                ('reserved',c_uint, 16)]


class S2H_BD_ADDRESS_LOWER(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA48
    _fields_= [('address', ctypes.c_uint, 32)]


class S2H_BD_ADDRESS_UPPER(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA4C
    _fields_= [('address', ctypes.c_uint, 32)]


class S2H_BD_PACKET_COUNT(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA50
    _fields_= [ ('count',   c_uint, 16),
                ('reserved',c_uint, 16)]


class S2H_BD_STATUS(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA54
    #          [lsb,
    #           msb]
    _fields_= [ ('streaming_enabled',   c_uint, 1),
                ('reserved',            c_uint, 31)]


class S2H_BD_CONTROL(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA58
    _fields_= [ ('streaming_start', c_uint, 1),
                ('streaming_stop',  c_uint, 1),
                ('streaming_reset', c_uint, 1),
                ('reserved_1',      c_uint, 1),
                ('controller_reset',c_uint, 1),
                ('reserved',        c_uint, 27)]


class S2H_BD_CONFIG(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xA5C
    _fields_= [ ('done',    c_uint, 1),
                ('reserved',c_uint, 31)]


class SW_TRIGGER_FIFO_SOURCE(HbirctcRegister):
    BAR = 1
    ADDR = 0xA2C
    _fields_ = [('data', c_uint, 32)]


class SW_TRIGGER_FIFO_READ(HbirctcRegister):
    BAR = 1
    ADDR = 0xA28
    _fields_ = [('data', c_uint, 32)]


class SW_TRIGGER_FIFO_COUNT(HbirctcRegister):
    BAR = 1
    ADDR = 0xA24
    _fields_ = [('data', c_uint, 32)]


class SW_TRIGGER_FIFO_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0xA20
    _fields_ = [('trigger_reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class CLOCK_FREQUENCY_MEASUREMENT(HbirctcRegister):
    BAR = 1
    ADDR = 0xA60
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('meas_counter', c_uint, 12),
                ('ref_window_size', c_uint, 12),
                ('divider_ratio', c_uint, 8)]


class LIVE_TIME_STAMP(HbirctcRegister):
    BAR = 1
    ADDR = 0xA80
    _fields_ = [('data', c_uint, 32)]


class BROADCAST_TRIGGER_DOWN(HbirctcRegister):
    BAR = 1
    ADDR = 0x0E0
    _fields_ = [('trigger', c_uint, 32)]


class LAST_BROADCASTED_TRIGGER(HbirctcRegister):
    BAR = 1
    ADDR = 0x0E4
    _fields_ = [('trigger', c_uint, 32)]


class SPM_CONTROL(HbirctcRegister):
    BAR = 1
    ADDR = 0xB80
    _fields_ = [('spm_enable', c_uint, 1),
                ('reserved', c_uint, 31)]

class SPM_CONFIG(HbirctcRegister):
    BAR = 1
    ADDR = 0xB84
    _fields_ = [('bps_0_present', c_uint, 1),
                ('bps_1_present', c_uint, 1),
                ('bps_2_present', c_uint, 1),
                ('reserved', c_uint, 29)]


class SPM_POUT_FILTER_COUNT(HbirctcRegister):
    BAR = 1
    ADDR = 0xB88
    _fields_ = [('count', c_uint, 32)]


class SPM_POUT_SUM_FILTER_COUNT(HbirctcRegister):
    BAR = 1
    ADDR = 0xB8C
    _fields_ = [('count', c_uint, 32)]


class SPM_POUT_LIMIT(HbirctcRegister):
    BAR = 1
    ADDR = 0xB90
    _fields_ = [('limit', c_uint, 32)]


class SPM_POUT_SUM_LIMIT(HbirctcRegister):
    BAR = 1
    ADDR = 0xB94
    _fields_ = [('limit', c_uint, 32)]


class SPM_STATUS(HbirctcRegister):
    BAR = 1
    ADDR = 0xB98
    _fields_ = [('bps_0_polling_active', c_uint, 1),
                ('bps_1_polling_active', c_uint, 1),
                ('bps_2_polling_active', c_uint, 1),
                ('reserved', c_uint, 29)]


class SPM_BPS_0_POUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBA0
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_0_VOUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBA4
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_0_IOUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBA8
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_0_TEMP1(HbirctcRegister):
    BAR = 1
    ADDR = 0xBAC
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_0_TEMP2(HbirctcRegister):
    BAR = 1
    ADDR = 0xBB0
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_0_TEMP3(HbirctcRegister):
    BAR = 1
    ADDR = 0xBB4
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_0_FAN_SPEED_1(HbirctcRegister):
    BAR = 1
    ADDR = 0xBB8
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_POUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBC0
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_VOUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBC4
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_IOUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBC8
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_TEMP1(HbirctcRegister):
    BAR = 1
    ADDR = 0xBCC
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_TEMP2(HbirctcRegister):
    BAR = 1
    ADDR = 0xBD0
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_TEMP3(HbirctcRegister):
    BAR = 1
    ADDR = 0xBD4
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_1_FAN_SPEED_1(HbirctcRegister):
    BAR = 1
    ADDR = 0xBD8
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_POUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBE0
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_VOUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBE4
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_IOUT(HbirctcRegister):
    BAR = 1
    ADDR = 0xBE8
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_TEMP1(HbirctcRegister):
    BAR = 1
    ADDR = 0xBEC
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_TEMP2(HbirctcRegister):
    BAR = 1
    ADDR = 0xBF0
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_TEMP3(HbirctcRegister):
    BAR = 1
    ADDR = 0xBF4
    _fields_ = [('data', c_uint, 32)]


class SPM_BPS_2_FAN_SPEED_1(HbirctcRegister):
    BAR = 1
    ADDR = 0xBF8
    _fields_ = [('data', c_uint, 32)]


class SPM_POUT_SUM(HbirctcRegister):
    BAR = 1
    ADDR = 0xC00
    _fields_ = [('data', c_uint, 32)]
	

class SPM_SCOPESHOT_LATEST_DDR4_ADDRESS(HbirctcRegister):
    ADDR = 0xC40
    _fields_ = [('ddr4_address', c_uint, 31),
                ('reserved', c_uint, 1)]


class SPM_SCOPESHOT_DDR4_ENDING_ADDRESS(HbirctcRegister):
    ADDR = 0xC44
    _fields_ = [('ddr4_address', c_uint, 31),
                ('reserved', c_uint, 1)]


class SPM_SCOPESHOT_DDR4_STARTING_ADDRESS(HbirctcRegister):
    ADDR = 0xC48
    _fields_ = [('ddr4_address', c_uint, 31),
                ('reserved', c_uint, 1)]


class SPM_SCOPESHOT_CONTROL(HbirctcRegister):
    ADDR = 0xC4C
    _fields_ = [('start', c_uint, 1),
                ('stop', c_uint, 1),
                ('reserved', c_uint, 30)]


class SPM_SCOPESHOT_CONFIG(HbirctcRegister):
    ADDR = 0xC50
    _fields_ = [('sample_count', c_uint, 16),
                ('reserved', c_uint, 14),
                ('mode', c_uint, 1),
                ('enable', c_uint, 1)]


class SPM_SCOPESHOT_STATUS(HbirctcRegister):
    ADDR = 0xC54
    _fields_ = [('active', c_uint, 1),
                ('reserved', c_uint, 31)]


class S2H_SPM_PACKET_BYTE_COUNT(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAE0
    _fields_= [ ('count',   c_uint, 16),
                ('reserved',c_uint, 16)]


class S2H_SPM_VALID_PACKET_INDEX(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAE4
    _fields_= [ ('index',   c_uint, 16),
                ('reserved',c_uint, 16)]


class S2H_SPM_ADDRESS_LOWER(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAE8
    _fields_= [('address', ctypes.c_uint, 32)]


class S2H_SPM_ADDRESS_UPPER(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAEC
    _fields_= [('address', ctypes.c_uint, 32)]


class S2H_SPM_PACKET_COUNT(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAF0
    _fields_= [ ('count',   c_uint, 16),
                ('reserved',c_uint, 16)]


class S2H_SPM_STATUS(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAF4
    #          [lsb,
    #           msb]
    _fields_= [ ('streaming_enabled',   c_uint, 1),
                ('reserved',            c_uint, 31)]


class S2H_SPM_CONTROL(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAF8
    _fields_= [ ('streaming_start', c_uint, 1),
                ('streaming_stop',  c_uint, 1),
                ('streaming_reset', c_uint, 1),
                ('reserved_1',      c_uint, 1),
                ('controller_reset',c_uint, 1),
                ('reserved',        c_uint, 27)]


class S2H_SPM_CONFIG(HbirctcRegister):
    BAR     = 1
    ADDR    = 0xAFC
    _fields_= [ ('done',    c_uint, 1),
                ('reserved',c_uint, 31)]
