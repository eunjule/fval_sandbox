# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from threading import Lock
from time import perf_counter, sleep

from . import hbirctc_register
from .spm_data_types import SPM_DATA_TYPES
from Common import hbi_simulator, hilmon
import Common.hilmon as hil
import Common.hbi_simulator
from Common.register import create_field_dictionary
from Common.utilities import dword_to_float, float_to_dword


class SpmInterface():
    NUM_BPS = 3
    POUT_LIMIT = 300.0
    POLLING_RETRIES = 200000
    POUT_REFRESH_RATE_SECS = 700 / 1e6  # 700us
    DATA_REFRESH_RATE_SECS = 4.2 / 1e3  # 4.2ms
    DEFAULT_FILTER_COUNT = 1

    def __init__(self, log=None):
        super().__init__()
        self.Log = log
        self.fpga_index = 0
        self.bar = 1
        self.init_addresses()
        self.pcie_alarm = 0
        self.pcie_alarm_lock = Lock()

        self.filter_count_absolute_tolerance = 400
        self.filter_count_relative_tolerance = 0.30
        if isinstance(hilmon.hil, Common.hbi_simulator.HbiSimulator):
            self.perf_counter = self.perf_counter_sim
        else:
            self.perf_counter = perf_counter

    def init_addresses(self):
        self._pout_addr = {i: getattr(hbirctc_register,
                                      f'SPM_BPS_{i}_POUT').ADDR
                           for i in range(SpmInterface.NUM_BPS)}
        self._vout_addr = {i: getattr(hbirctc_register,
                                      f'SPM_BPS_{i}_VOUT').ADDR
                           for i in range(SpmInterface.NUM_BPS)}
        self._iout_addr = {i: getattr(hbirctc_register,
                                      f'SPM_BPS_{i}_IOUT').ADDR
                           for i in range(SpmInterface.NUM_BPS)}
        self._pout_sum_addr = hbirctc_register.SPM_POUT_SUM.ADDR
        self._pout_limit_addr = hbirctc_register.SPM_POUT_LIMIT.ADDR
        self._pout_sum_limit_addr = hbirctc_register.SPM_POUT_SUM_LIMIT.ADDR
        self._control_addr = hbirctc_register.SPM_CONTROL.ADDR
        self._status_addr = hbirctc_register.SPM_STATUS.ADDR
        self._config_addr = hbirctc_register.SPM_CONFIG.ADDR
        self._temp1_addr = {i: getattr(hbirctc_register,
                                       f'SPM_BPS_{i}_TEMP1').ADDR
                            for i in range(SpmInterface.NUM_BPS)}
        self._temp2_addr = {i: getattr(hbirctc_register,
                                       f'SPM_BPS_{i}_TEMP2').ADDR
                            for i in range(SpmInterface.NUM_BPS)}
        self._temp3_addr = {i: getattr(hbirctc_register,
                                       f'SPM_BPS_{i}_TEMP3').ADDR
                            for i in range(SpmInterface.NUM_BPS)}
        self._fan_speed_1_addr = {i: getattr(hbirctc_register,
                                             f'SPM_BPS_{i}_FAN_SPEED_1').ADDR
                                  for i in range(SpmInterface.NUM_BPS)}
        self._pout_filter_count_addr = \
            hbirctc_register.SPM_POUT_FILTER_COUNT.ADDR
        self._pout_sum_filter_count_addr = \
            hbirctc_register.SPM_POUT_SUM_FILTER_COUNT.ADDR
        self._spm_alarm_addr = hbirctc_register.SPM_ALARMS.ADDR

    def initialize_limits(self, bps_0=1, bps_1=1, bps_2=1):
        self.set_bps_present(bps_0, bps_1, bps_2)

        self.set_pout_limit(SpmInterface.POUT_LIMIT)
        num_bps = bps_0 + bps_1 + bps_2
        sum_limit = SpmInterface.POUT_LIMIT * num_bps
        self.set_pout_sum_limit(sum_limit)

    def wait_on_data_availability(self):
        num_retries = 100

        for retry in range(num_retries):
            success = True
            for bps_num in range(SpmInterface.NUM_BPS):
                for data_type in SPM_DATA_TYPES:
                    success &= self.bps_data(data_type, bps_num) != 0
            if success:
                break
            self.wait_on_data_refresh()
        else:
            self.Log('warning', self.error_msg_non_zero_data())
            for bps_num in range(SpmInterface.NUM_BPS):
                self.log_sensor_data(bps_num)
            value = self.bps_pout_sum_data()
            self.Log('info', f'POUT_SUM: {value}')

    def error_msg_non_zero_data(self):
        return f'Unable to get non-zero sensor data reads'

    def disable_spm(self, post_delay_seconds=1):
        self.set_spm_enable(0)
        sleep(post_delay_seconds)

    def enable_spm(self, pre_delay_seconds=1):
        sleep(pre_delay_seconds)
        self.set_spm_enable(1)
        self.wait_on_data_availability()

    def set_spm_enable(self, enable):
        hil.hbiMbBarWrite(self.fpga_index, self.bar, self._control_addr,
                          enable)

    def is_spm_enabled(self):
        data = hil.hbiMbBarRead(self.fpga_index, self.bar, self._control_addr)
        return bool(data & 1)

    def set_bps_present(self, bps_0=1, bps_1=1, bps_2=1):
        reg = hbirctc_register.SPM_CONFIG(
            value=hil.hbiMbBarRead(self.fpga_index, self.bar,
                                   self._config_addr))
        reg.bps_2_present = bps_2
        reg.bps_1_present = bps_1
        reg.bps_0_present = bps_0
        hil.hbiMbBarWrite(self.fpga_index, self.bar, self._config_addr,
                          reg.value)

    def bps_present(self):
        reg = hbirctc_register.SPM_CONFIG(value=hil.hbiMbBarRead(
            self.fpga_index, self.bar, self._config_addr))
        is_present = [reg.bps_0_present, reg.bps_1_present, reg.bps_2_present]
        return is_present

    def bps_polling_active(self):
        reg = hbirctc_register.SPM_STATUS(hil.hbiMbBarRead(
            self.fpga_index, self.bar, self._status_addr))
        is_active = [reg.bps_0_polling_active, reg.bps_1_polling_active,
                     reg.bps_2_polling_active]
        return is_active

    def set_pout_limit(self, limit):
        hil.hbiMbBarWrite(self.fpga_index, self.bar, self._pout_limit_addr,
                          float_to_dword(limit))

    def pout_limit(self):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._pout_limit_addr))

    def set_pout_sum_limit(self, limit):
        hil.hbiMbBarWrite(self.fpga_index, self.bar, self._pout_sum_limit_addr,
                          float_to_dword(limit))

    def pout_sum_limit(self):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._pout_sum_limit_addr))

    def bps_data(self, data_type, bps_num):
        return getattr(self, f'bps_{data_type}_data')(bps_num)

    def bps_pout_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._pout_addr[bps_num]))

    def bps_vout_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._vout_addr[bps_num]))

    def bps_iout_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._iout_addr[bps_num]))

    def bps_pout_sum_data(self, bps_num=None):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._pout_sum_addr))

    def bps_temp1_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._temp1_addr[bps_num]))

    def bps_temp2_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._temp2_addr[bps_num]))

    def bps_temp3_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._temp3_addr[bps_num]))

    def bps_fan_speed_1_data(self, bps_num):
        return dword_to_float(hil.hbiMbBarRead(self.fpga_index, self.bar,
                                               self._fan_speed_1_addr[bps_num]))

    def poll_data_using_refresh_rate(self, data_type, bps_num, poll_total):
        out_data = []

        if data_type not in SPM_DATA_TYPES:
            self.Log('error', self.error_msg_invalid_data_type(data_type))
            return out_data

        wait = self.wait_on_pout_refresh if data_type == 'pout' else \
            self.wait_on_data_refresh

        for i in range(poll_total):
            out_data.append(self.bps_data(data_type, bps_num))
            wait()

        return out_data

    def error_msg_invalid_data_type(self, data_type):
        return f'Invalid data type {data_type}. Acceptable types ' \
               f'{SPM_DATA_TYPES}'

    def log_sensor_data(self, bps_num):
        for data_type in SPM_DATA_TYPES:
            if data_type != 'pout_sum':
                value = self.bps_data(data_type, bps_num)
                self.Log('info', f'{data_type.upper()}_{bps_num}: {value}')

    def wait_on_pout_refresh(self):
        start_time = self.perf_counter()
        while self.perf_counter() - start_time < SpmInterface.POUT_REFRESH_RATE_SECS:
            pass

    def wait_on_data_refresh(self):
        start_time = self.perf_counter()
        while self.perf_counter() - start_time < SpmInterface.DATA_REFRESH_RATE_SECS:
            pass

    def spm_alarm(self):
        return hil.hbiMbBarRead(self.fpga_index, self.bar,
                                self._spm_alarm_addr)

    def clear_spm_pout_alarm(self, bps_0=1, bps_1=1, bps_2=1):
        reg = hbirctc_register.SPM_ALARMS(self.spm_alarm())
        reg.bps_0_spm_pout = bps_0
        reg.bps_1_spm_pout = bps_1
        reg.bps_2_spm_pout = bps_2
        hil.hbiMbBarWrite(self.fpga_index, self.bar, self._spm_alarm_addr,
                          reg.value)

    def clear_spm_pout_sum_alarm(self):
        reg = hbirctc_register.SPM_ALARMS(self._spm_alarm_addr)
        reg.spm_pout_sum = 1
        hil.hbiMbBarWrite(self.fpga_index, self.bar, self._spm_alarm_addr,
                          reg.value)

    def pcie_driver_alarm_wait(self):
        self.pcie_alarm = 0
        self.pcie_alarm_lock.acquire()
        try:
            self.pcie_alarm = hil.hbiMbAlarmWait()
        except RuntimeError:
            pass

        if self.pcie_alarm_lock.locked():
            self.pcie_alarm_lock.release()

    def pcie_driver_alarm_wait_cancel(self):
        hil.hbiMbAlarmWaitCancel()

    def global_spm_alarm(self):
        reg = hbirctc_register.GLOBAL_ALARMS(value=hil.hbiMbBarRead(
            self.fpga_index, self.bar, hbirctc_register.GLOBAL_ALARMS.ADDR))
        return reg.active_spm_alarm

    def enable_pcie_interrupt(self, enable=1):
        address = hbirctc_register.INTERRUPT_CONTROL.ADDR
        reg = hbirctc_register.INTERRUPT_CONTROL(value=hil.hbiMbBarRead(
            self.fpga_index, self.bar, address))
        reg.spm_interrupt_enable = enable
        hil.hbiMbBarWrite(self.fpga_index, self.bar, address, reg.value)

    def pcie_interrupt(self):
        reg = hbirctc_register.INTERRUPT_STATUS(value=hil.hbiMbBarRead(
            self.fpga_index, self.bar, hbirctc_register.INTERRUPT_STATUS.ADDR))
        return reg.spm_interrupt

    def set_pout_filter_count(self, count):
        hil.hbiMbBarWrite(self.fpga_index, self.bar,
                          self._pout_filter_count_addr, count)

    def pout_filter_count(self):
        return hil.hbiMbBarRead(self.fpga_index, self.bar,
                                self._pout_filter_count_addr)

    def set_pout_sum_filter_count(self, count):
        hil.hbiMbBarWrite(self.fpga_index, self.bar,
                          self._pout_sum_filter_count_addr, count)

    def pout_sum_filter_count(self):
        return hil.hbiMbBarRead(self.fpga_index, self.bar,
                                self._pout_sum_filter_count_addr)

    def filter_count(self, sensor_name):
        count = getattr(self, f'{sensor_name}_filter_count')()
        return count

    def initialize_filter_count(self, sensor_name):
        getattr(self, f'set_{sensor_name}_filter_count')(
            SpmInterface.DEFAULT_FILTER_COUNT)

    def wait_on_spm_alarm(self, expected, num_retries=10):
        if self.spm_alarm() != expected:
            data = 0
            retry = 0
            for retry in range(num_retries):
                data = self.spm_alarm()
                if data == expected:
                    break
                self.wait_on_pout_refresh()
            else:
                raise SpmInterface.SpmAlarmWaitError(
                    f'Spm Alarm timed out '
                    f'(expected alarm, actual alarm, num_retries): '
                    f'0x{expected:08X}, 0x{data:08X}, {retry}')

    class SpmAlarmWaitError(Exception):
        pass

    def perf_counter(self):
        """ Place holder function

        :return: time.perf_counter or perf_counter_sim
        """

    def perf_counter_sim(self):
        if not hasattr(self, 'perf_count'):
            self.perf_count = 0
        current_count = self.perf_count
        self.perf_count += (65 / 1e6)
        return current_count

    def filter_count_tolerances(self):
        return self.filter_count_absolute_tolerance,\
               self.filter_count_relative_tolerance

    @staticmethod
    def is_system_power_monitoring_present():
        addr = hbirctc_register.FPGA_CAPABILITY.ADDR
        bar = hbirctc_register.FPGA_CAPABILITY.BAR
        fpga_index=0
        reg = hbirctc_register.FPGA_CAPABILITY(hil.hbiMbBarRead(fpga_index,
                                                                bar, addr))
        return (reg.system_power_monitoring_capable == 1)

    def spm_pout_alarms_to_string(self):
        reg = hbirctc_register.SPM_ALARMS(self.spm_alarm())
        return f'bps_0_spm_pout; {reg.bps_0_spm_pout}, ' \
               f'bps_1_spm_pout: {reg.bps_1_spm_pout}, ' \
               f'bps_2_spm_pout: {reg.bps_2_spm_pout}, ' \
               f'spm_pout_sum: {reg.spm_pout_sum}'
