# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import ctypes

import Common.register as register


# Offsets taken from HBI-MB-FRONTPANEL-DESIGN-SPEC document

class RCTC_SCRATCH_PAD(register.Register):
    ADDR = 2
    _fields_ = [('data', ctypes.c_uint, 16)]


class RCTC_FW_REV_MAJ(register.Register):
    ADDR = 3
    _fields_ = [('data', ctypes.c_uint, 16)]


class RCTC_FW_REV_MINOR(register.Register):
    ADDR = 4
    _fields_ = [('data', ctypes.c_uint, 16)]


class RCTC_FW_REV_PCB(register.Register):
    ADDR = 5
    _fields_ = [('data', ctypes.c_uint, 16)]


class RCTC_IO_STATUS(register.Register):
    ADDR = 6
    _fields_ = [('ltc3787_12v_to_48v_pgood', ctypes.c_uint, 1),  # 0 RD Only
                ('run_48v_bst', ctypes.c_uint, 1),  # 1 RD Only
                ('sys_power_down', ctypes.c_uint, 1),  # 2 RD Only
                ('fail_safe_mask', ctypes.c_uint, 1),  # 3 RW
                ('sys_fail_safe', ctypes.c_uint, 1),  # 4 RD Only
                ('p12v_stdby_3a', ctypes.c_uint, 1),  # 5 RD Only
                ('power_button', ctypes.c_uint, 1),  # 6 RD Only
                ('reserved', ctypes.c_uint, 9)]  # 8-15


class RCTC_INT_LINES(register.Register):
    ADDR = 7
    _fields_ = [('INT0', ctypes.c_uint, 1),  # 0, ERROR
                ('INT1', ctypes.c_uint, 1),  # 1, SPI Busy
                ('unlatched_limit_error', ctypes.c_uint, 1),  # 2
                ('clear_INT0_error_latch', ctypes.c_uint, 1),  # 3
                ('debug_drive_INT0', ctypes.c_uint, 1),  # 4
                ('debug_drive_INT1', ctypes.c_uint, 1),  # 5
                ('debug_set_INT0', ctypes.c_uint, 1),  # 6
                ('debug_set_INT1', ctypes.c_uint, 1),  # 7
                ('reserved', ctypes.c_uint, 8)]  # 8-15


class RCTC_BPS_MASK_SHUTDOWN_FABC(register.Register):
    # Set bit pos to high to disable shutdown for testing.
    ADDR = 48
    _fields_ = [('mask_bps_AC_INPUT_OK_N_error', ctypes.c_uint, 3),  # 2,1,0
                ('mask_bps_PRSNT_N_error', ctypes.c_uint, 3),  # 2,1,0
                ('mask_bps_PWR_OUT_OK_N_error', ctypes.c_uint, 3),  # 2,1,0
                ('mask_PGOOD_48V_error', ctypes.c_uint, 1),
                ('mask_sys_fail_safe', ctypes.c_uint, 1),
                ('mask_leak_detect_3', ctypes.c_uint, 1),
                ('mask_leak_detect_1', ctypes.c_uint, 1),
                ('mask_leak_detect_0', ctypes.c_uint, 1),
                ('mask_leak_detect_2', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 17)]


class RCTC_MASK_LIMIT_SHUTDOWN(register.Register):
    # Set bit pos to high to disable shutdown for testing.
    ADDR = 49
    _fields_ = [('mask_GND_limit_error', ctypes.c_uint, 1),  # 2,1,0
                ('maks_I2C_errors', ctypes.c_uint, 1),  # 2,1,0
                ('mask_H20_INT_TEMP_error', ctypes.c_uint, 1),  # 2,1,0
                ('mask_limit_errors_&_enable_error_mask', ctypes.c_uint, 1),
                ('mask_anaolog_leak_error', ctypes.c_uint, 4),  #3,1,2,0
                ('mask_H20_TEMP_error', ctypes.c_uint, 1),
                ('mask_H20_FLOW_RATE_error', ctypes.c_uint, 1),
                ('reserved_0', ctypes.c_uint, 1),
                ('mask_SHT3X_HUMIDITY_error', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 21)]
