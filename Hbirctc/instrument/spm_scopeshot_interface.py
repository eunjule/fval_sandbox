# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from ctypes import addressof, c_float, c_uint32, LittleEndianStructure, memmove
from random import getrandbits, randint
import sys

from Common import get_device_class
from Common.fval import Object
from Common.instruments import hdmt_trigger_interface
from Common.triggers import SCSMemoryEncoding
from Common.utilities import performance_sleep


class SpmScopeshotInterface(Object):
    DDR4_NUM_BYTES = int((1 << 30) * 0.5)  # 0.5GB DDR space
    DDR4_STARTING_ADDRESS_DEFAULT = 0x6000_0000
    DDR4_ENDING_ADDRESS_DEFAULT = \
        (DDR4_NUM_BYTES - 1) + DDR4_STARTING_ADDRESS_DEFAULT
    DDR4_ENTRY_BYTE_SIZE = int((32 * 6) / 8)
    MAX_NUM_DATA_ENTRIES = int(DDR4_NUM_BYTES / DDR4_ENTRY_BYTE_SIZE)
    SAMPLE_COUNT_DELAY_TIME_US = [20, 20]
    SAMPLE_COUNT_DEFAULT = 0
    WAIT_ON_ACTIVE_RETRY_COUNT = 100
    TRIGGER_MODE = 0
    ALARM_MODE = 1
    ENABLE_DEFAULT = False
    MODE_DEFAULT = TRIGGER_MODE
    PACKET_REFRESH_RATE_SEC= 700 / 1e6

    def __init__(self, rc=None):
        super().__init__()
        if rc is None:
            self.rc = get_device_class.get_device_class('hbirctc')()
        else:
            self.rc = rc

    def enable(self):
        reg = self.config_reg()
        reg.enable = 1
        self.rc.write_bar_register(reg)

    def is_enabled(self):
        reg = self.config_reg()
        return bool(reg.enable)

    def disable(self):
        reg = self.config_reg()
        reg.enable = 0
        self.rc.write_bar_register(reg)

    def mode(self):
        reg = self.config_reg()
        return reg.mode

    def set_mode(self, mode):
        reg = self.config_reg()
        reg.mode = mode
        self.rc.write_bar_register(reg)

    def set_spm_power_alarm_mode(self):
        self.set_mode(1)

    def set_trigger_mode(self):
        self.set_mode(0)

    def config_reg(self):
        return self.rc.read_bar_register(
            self.rc.registers.SPM_SCOPESHOT_CONFIG)

    def ddr4_latest_address(self):
        ddr4_address = self.rc.read_bar_register(
            self.rc.registers.SPM_SCOPESHOT_LATEST_DDR4_ADDRESS).ddr4_address
        return ddr4_address

    def ddr4_starting_address(self):
        return self.rc.read_bar_register(
            self.rc.registers.SPM_SCOPESHOT_DDR4_STARTING_ADDRESS).ddr4_address

    def set_ddr4_starting_address(self, address):
        reg = self.rc.registers.SPM_SCOPESHOT_DDR4_STARTING_ADDRESS(address)
        self.rc.write_bar_register(reg)

    def ddr4_ending_address(self):
        return self.rc.read_bar_register(
            self.rc.registers.SPM_SCOPESHOT_DDR4_ENDING_ADDRESS).ddr4_address

    def set_ddr4_ending_address(self, address):
        reg = self.rc.registers.SPM_SCOPESHOT_DDR4_ENDING_ADDRESS(address)
        self.rc.write_bar_register(reg)

    def start_scopeshot(self, use_event_trigger=False,
                       force_wait=True):
        assert not use_event_trigger, 'FVAL has not implemented a handle to ' \
                                      'Event Triggers yet'
        self.assert_start_control()
        if force_wait:
            self.wait_on_active(asserted=True)

    def stop_scopeshot(self, use_event_trigger=False,
                       force_wait=True):
        assert not use_event_trigger, 'FVAL has not implemented a handle to ' \
                                      'Event Triggers yet'
        self.assert_stop_control()
        if force_wait:
            self.wait_on_active(asserted=False)

    def send_start_event(self, slot, bypass=True):
        trigger = hdmt_trigger_interface.generate_scs_start_trigger()
        if bypass:
            self.rc.send_up_trigger_bypass(slot, trigger)
        else:
            self.rc.send_broadcast_trigger(trigger)

    def send_stop_event(self, slot, bypass=True):
        trigger = hdmt_trigger_interface.generate_scs_stop_trigger()
        if bypass:
            self.rc.send_up_trigger_bypass(slot, trigger)
        else:
            self.rc.send_broadcast_trigger(trigger)

    def assert_start_control(self):
        # start bit self clears upon a write
        reg = self.rc.registers.SPM_SCOPESHOT_CONTROL(start=1)
        self.rc.write_bar_register(reg)

    def assert_stop_control(self):
        # stop bit self clears upon a write
        reg = self.rc.registers.SPM_SCOPESHOT_CONTROL(stop=1)
        self.rc.write_bar_register(reg)

    def ddr4_next_address(self, current_address):
        ddr4_entry_byte_size = SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE
        end_addr = self.ddr4_ending_address()
        start_addr = self.ddr4_starting_address()
        if current_address < (end_addr - ddr4_entry_byte_size):
            return current_address + ddr4_entry_byte_size
        else:
            return start_addr

    def read_scopeshot_entries_from_memory(self, address, num_entries=1):
        entry_byte_size = SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE
        total_bytes = entry_byte_size * num_entries
        ddr4_data = self.rc.dma_read(address, total_bytes)

        encoding = []
        for i in range(num_entries):
            data_lsb = entry_byte_size * i
            data_msb = data_lsb + entry_byte_size
            data = ddr4_data[data_lsb:data_msb]
            packet = int.from_bytes(data, byteorder=sys.byteorder)
            encoding.append(SpmScopeshotPacket(value=packet))

        if num_entries == 1:
            return encoding[0]
        else:
            return encoding

    def compare_sensor_data(self, a, b):
        return (a.sensor_data == b.sensor_data and a.user_id == b.user_id and
                a.pin_id == b.pin_id and a.ac_slice_id == b.ac_slice_id and
                a.link_num == b.link_num)

    def generate_random_scs_ddr4_data(self, num_entries, link_num=None):
        data = [SCSMemoryEncoding(
                sensor_data=getrandbits(10),
                user_id=getrandbits(10),
                pin_id=getrandbits(2),
                ac_slice_id=getrandbits(2),
                link_num=randint(0, 13) if link_num is None else link_num)
            for i in range(num_entries)]
        return data

    def convert_ddr4_data_to_trigger(self, entry):
        trigger = hdmt_trigger_interface.generate_scs_trigger(
            slice=entry.ac_slice_id, pin=entry.pin_id, user=entry.user_id,
            data=entry.sensor_data
        )
        return trigger

    def convert_trigger_to_ddr4_data(self, trigger, link_num):
        entry = SCSMemoryEncoding(
                    sensor_data=trigger.data,
                    user_id=trigger.user,
                    pin_id=trigger.pin,
                    ac_slice_id=trigger.slice,
                    link_num=link_num)
        return entry

    def initiate_scopeshot(self, use_event_trigger=False):
        self.enable()
        self.start_scopeshot(use_event_trigger=use_event_trigger)

    def terminate_scopeshot(self, use_event_trigger=False):
        self.stop_scopeshot(use_event_trigger=use_event_trigger)
        self.set_defaults()

    def set_defaults(self):
        self.set_ddr4_starting_address(
            SpmScopeshotInterface.DDR4_STARTING_ADDRESS_DEFAULT)
        self.set_ddr4_ending_address(
            SpmScopeshotInterface.DDR4_ENDING_ADDRESS_DEFAULT)
        self.set_sample_count(SpmScopeshotInterface.SAMPLE_COUNT_DEFAULT)
        self.set_mode(SpmScopeshotInterface.MODE_DEFAULT)
        self.disable()

    def is_active(self):
        active = self.rc.read_bar_register(
            self.rc.registers.SPM_SCOPESHOT_STATUS).active
        return bool(active)

    def sample_count(self):
        count = self.rc.read_bar_register(
            self.rc.registers.SPM_SCOPESHOT_CONFIG).sample_count
        return count

    def set_sample_count(self, count):
        reg = self.rc.read_bar_register(self.rc.registers.SPM_SCOPESHOT_CONFIG)
        reg.sample_count = count
        self.rc.write_bar_register(reg)

    def wait_on_active(self, asserted=True):
        for retry in range(SpmScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT):
            if asserted == self.is_active():
                break
            else:
                performance_sleep(10 / 1e6)
        else:
            raise SpmScopeshotInterface.TimeoutError(
                f'Timed out waiting on active status of {asserted}')

    class TimeoutError(Exception):
        pass

    def wait_on_packet_refresh(self):
        performance_sleep(SpmScopeshotInterface.PACKET_REFRESH_RATE_SEC)

    def wait_on_latest_address(self, address, num_retries=50):
        for retry in range(num_retries):
            current_address = self.ddr4_latest_address()
            if address == current_address:
                return current_address
            performance_sleep(10 / 1e6)
        else:
            raise SpmScopeshotInterface.RetryError(
                self.exception_msg_wait_on_latest_address(address))

    def exception_msg_wait_on_latest_address(self, address):
        return f'Failed to find latest address {hex(address)}'

    def wait_on_latest_address_change(self, address, num_retries=1000):
        for retry in range(num_retries):
            current_address = self.ddr4_latest_address()
            if address != current_address:
                return current_address
            performance_sleep(10 / 1e6)
        else:
            raise SpmScopeshotInterface.RetryError(
                self.exception_msg_wait_on_latest_address_change(address))

    def exception_msg_wait_on_latest_address_change(self, address):
        return f'Failed to obtain a latest address update from address ' \
               f'{hex(address)}'

    class RetryError(Exception):
        pass


class SpmScopeshotPacket(LittleEndianStructure):
    _num_bytes = 24
    # Total of 6*4 bytes
    _fields_ =[('bps_0_present', c_uint32, 1),  # 0
               ('bps_1_present', c_uint32, 1),
               ('bps_2_present', c_uint32, 1),
               ('spm_enable', c_uint32, 1),
               ('sample_count_mode', c_uint32, 1),
               ('reserved', c_uint32, 27),
               ('time_stamp_us', c_uint32),
               ('pout_bps_0', c_float),
               ('pout_bps_1', c_float),
               ('pout_bps_2', c_float),
               ('pout_sum', c_float)]

    def __init__(self, value=0):
        super().__init__()
        self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')

    @value.setter
    def value(self, i):
        """setter - fills fields from integer value"""
        dst = addressof(self)
        src = i.to_bytes(SpmScopeshotPacket._num_bytes, byteorder='little')
        count = SpmScopeshotPacket._num_bytes
        memmove(dst, src, count)

    def to_string(self):
        msg = f'SpmScopeshotPacket:\n'
        msg += f'\tbps_present (0, 1, 2): {self.bps_0_present}, ' \
               f'{self.bps_1_present}, {self.bps_2_present}\n'
        msg += f'\t           smp_enable: {self.spm_enable}\n'
        msg += f'\t    sample_count_mode: {self.sample_count_mode}\n'
        msg += f'\t        time_stamp_us: {self.time_stamp_us}\n'
        msg += f'\t    pout_bps(0, 1, 2): {self.pout_bps_0:.02f}, ' \
               f'{self.pout_bps_1:.02f}, {self.pout_bps_2:.02f}\n'
        msg += f'\t             pout_sum: {self.pout_sum:.02f}'

        return msg
