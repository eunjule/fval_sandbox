# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.instruments.i2c_test_interface import I2cTestInterface
from Hbirctc.instrument.hbirctc_register import BLT_I2C_CONTROL
from Hbirctc.instrument.hbirctc_register import BLT_I2C_RESET
from Hbirctc.instrument.hbirctc_register import BLT_I2C_RX_FIFO
from Hbirctc.instrument.hbirctc_register import BLT_I2C_STATUS
from Hbirctc.instrument.hbirctc_register import BLT_I2C_TX_FIFO

TIU_PCA9505_GPIO_0_I2C_ADDR = 0x40
I2C_ADDR = TIU_PCA9505_GPIO_0_I2C_ADDR  # If read, I2C_ADDR | 1
MAX_NUM_PSDB = 2
MAX_NUM_BLT = 4
MAX_NUM_CHIPS = MAX_NUM_BLT * MAX_NUM_PSDB

PCA9505_TOTAL_OUTPUT_REG = 5
PCA9505_OUTPUT_REG_NUM = {i: 0x08 + i for i in range(PCA9505_TOTAL_OUTPUT_REG)}


def create_interfaces(instrument):
    interfaces = [BltI2cInterface(instrument, index)
                  for index in range(MAX_NUM_BLT * MAX_NUM_PSDB)]
    return interfaces


def get_psdb_num(index):
    return int(index / MAX_NUM_BLT)


def get_blt_num(index):
    return index % MAX_NUM_BLT


class BltI2cInterface(I2cTestInterface):

    def __init__(self, instrument, index):
        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR,
                         interface_name=f'PSDB{get_psdb_num(index)}: '
                                        f'BLT{get_blt_num(index)}',
                         interface_index=index)

        self.update_register_table({'STATUS': BLT_I2C_STATUS,
                                    'CONTROL': BLT_I2C_CONTROL,
                                    'TX_FIFO': BLT_I2C_TX_FIFO,
                                    'RX_FIFO': BLT_I2C_RX_FIFO,
                                    'RESET': BLT_I2C_RESET})

    def write_pca9505_output_0(self, data):
        self.write(PCA9505_OUTPUT_REG_NUM[0], data)

    def read_pca9505_output_0(self):
        return self.read(PCA9505_OUTPUT_REG_NUM[0])
