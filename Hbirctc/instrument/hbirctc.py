# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import random
import time

from ThirdParty.HPS.hps_diskless_boot import BootHps
from ThirdParty.SASS.suppress_sensor import suppress_sensor

from . import hbirctc_register
from Common import configs
from Common import hilmon as hil
from Common import pcie
from Common import stratix10
from Common.cache_blt_info import CachingBltInfo
from Common.instruments.hbi_instrument import HbiInstrument
from Common.instruments.s2h_interface import S2hBufferIndex, S2hInterface
from Hbirctc.instrument.max10 import Max10
from Hbirctc.instrument.peci_dac import PeciDac
from Hbirctc.testbench.fan_chip_simulator import MB_FAN_INDEXES,\
    DPS_FAN_INDEXES


class Hbirctc(HbiInstrument):
    aurora_link_test_loop = 100
    DDR4_MAINBOARD_FAB_NUMBER_MINIMUM = 403
    HBIRCTC_FPGA_INDEX = 0

    class S2HStreamError(Exception):
        pass

    def __init__(self, mainboard=None):
        super().__init__()
        self.mainboard = mainboard
        self.fpga_index = Hbirctc.HBIRCTC_FPGA_INDEX
        self.registers = hbirctc_register
        self.fpga_load = configs.HBIRCTC_FPGA_IMAGE_LOAD
        self.instrumentblt = CachingBltInfo(
            callback_function=self.read_board_blt, name='HBIMB')

        self.max10 = Max10(self)
        self.bulk_data_s2h_stream = S2hInterface(self, S2hBufferIndex.BD)
        self.peci_dac = PeciDac(self)

        self.fab_letter = None
        self._is_initialized = False
        self._failsafe_limit_default = None
        self._time_after_fpga_load = None

    def Initialize(self):
        if configs.SKIP_INIT:
            self.Log('warning',f'Skipping {self.name()} instrument '
                               f'initialization')
        else:
            self.initialize_fpga()
        self.log_traceability()

    def initialize_fpga(self):
        self.load_fpga()

        if self.get_fab_letter().upper() >= 'C':
            # HPS diskless initialization only used from FabC MB and up
            BootHps(self.name())

        self.bulk_data_s2h_stream.reset_stream()
        self.bulk_data_s2h_stream.enable_stream()

        self._is_initialized = True

    def log_traceability(self):
        self.log_fpga_version()
        self.log_blt()
        self.log_cpld()
        self.log_max10_cpld()
        self.log_chip_id()
        self.log_build()
        self.log_fan_rps()

    def get_fab_letter(self):
        if configs.HBICC_IN_HDMT:
            self.fab_letter = configs.HBICC_IN_HDMT_FAB
            self.warn_user_fab_detection_is_bypassed(self.fab_letter)
        else:
            if self.fab_letter is None:
                self.fab_letter = self.mainboard.blt.fab_letter()
        return self.fab_letter

    def ensure_initialized(self):
        if not self._is_initialized:
            self.Initialize()

    def discover_device(self):
        if self.discover():
            return self
        else:
            return None

    def attach_model(self):
        pass

    def fpga_image_path(self):
        fab_letter = self.get_fab_letter()
        return getattr(configs, f'HBIRCTC_FPGA_IMAGE_FAB_{fab_letter}')

    def load_fpga_using_hil(self):
        num_retries = 5
        with suppress_sensor('FVAL HBI RCTC', 'HBI_RCTC'):
            for retry in range(num_retries):
                try:
                    with self.FpgaPcieDriverDisable(self.fpga_index):
                        hil.hbiMbFpgaLoad(self.fpga_index,
                                          self.fpga_image_path())
                        self._time_after_fpga_load = time.time()
                    self.Log('info', f'{self.name()} FPGA Version: '
                                     f'{self.fpga_version_string()}')
                    self._failsafe_limit_default = self.failsafe_limit()
                    break
                except RuntimeError as e:
                    self.Log('warning',
                             f'{self.name()} failed to properly load '
                             f'image and re-enable driver {retry + 1} of '
                             f'{num_retries} times')

        # Do not remove. Hbirctc requires this every time a new image is loaded
        self.disable_fail_safe()

    class FpgaPcieDriverDisable():
        def __init__(self, fpga_index):
            self.fpga_index = fpga_index

        def __enter__(self):
            hil.hbiMbDeviceDisable(self.fpga_index)
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            hil.hbiMbDeviceEnable(self.fpga_index)

    def failsafe_limit_default(self):
        return self._failsafe_limit_default

    def time_after_fpga_load(self):
        return self._time_after_fpga_load

    def enable_device(self):
        hil.hbiMbDeviceEnable(self.fpga_index)

    def disable_device(self):
        hil.hbiMbDeviceDisable(self.fpga_index)

    def open_pcie_device(self):
        handle = hil.pciDeviceOpen(pcie.hbirctc_guid, pcie.hbirctc_viddid, -1,
                                   None)
        hil.pciDeviceClose(handle)

    def hg_id(self):
        lower = self.read_bar_register(self.registers.HG_ID_LOWER).Data
        upper = self.read_bar_register(self.registers.HG_ID_UPPER).Data
        return upper << 32 | lower

    def hg_id_from_file(self):
        per_build_params_file = self.fpga_per_build_param_file()
        return self.find_in_param_build_file(per_build_params_file, 'hg_id')

    def fpga_version_from_file(self):
        file_path = self.fpga_per_build_param_file()
        major = self.find_in_param_build_file(file_path, 'major_version')
        minor = self.find_in_param_build_file(file_path, 'minor_version')
        return (major << 16) | minor

    def hg_clean_from_file(self):
        file_path = self.fpga_per_build_param_file()
        return self.find_in_param_build_file(file_path, 'hg_clean')

    def compatibility_id_from_file(self):
        file_path = self.fpga_per_build_param_file()
        return self.find_in_param_build_file(file_path, 'compatibility_id')

    def fpga_per_build_param_file(self):
        image_file = self.fpga_image_path()
        image_folder = os.path.dirname(image_file)
        per_build_params_file = os.path.join(image_folder,
                                             'per_build_params.txt')
        return per_build_params_file

    def find_in_param_build_file(self, per_build_params_file, name):
        value = 0
        with open(per_build_params_file, 'r') as build_params:
            lines = build_params.readlines()
            for line in lines:
                key_value = line.strip().split(' ')
                if name == key_value[0].lower():
                    value = key_value[1]
                    break
            else:
                self.Log('error', f'{name.upper()} could not be found in '
                                  f'per_build_params.txt. Defaulting to 0')
        return int(value, 16)

    def chip_id(self):
        upper = self.read_bar_register(self.registers.CHIP_ID_UPPER).Data
        lower = self.read_bar_register(self.registers.CHIP_ID_LOWER).Data
        return upper, lower

    def disable_fail_safe(self):
        hil.hilFailsafeDisable(1)

    def enable_fail_safe(self):
        hil.hilFailsafeDisable(0)

    def dma_read(self, address, num_bytes):
        return hil.hbiMbDmaRead(self.fpga_index, address, num_bytes)

    def dma_write(self, address, data):
        return hil.hbiMbDmaWrite(self.fpga_index, address, data)

    def fpga_version_string(self):
        return hil.hbiMbFpgaVersionString(self.fpga_index)

    def log_board_blt(self):
        self.instrumentblt.write_to_log()

    def read_board_blt(self):
        return self.mainboard.read_blt()

    def read_bar_register(self, register_type, index=0):
        reg_value = self.bar_read(register_type.BAR,
                                  register_type.address(index))
        return register_type(value=reg_value)

    def bar_read(self, bar, offset):
        return hil.hbiMbBarRead(self.fpga_index, bar, offset)

    def write_bar_register(self, register, index=0):
        self.bar_write(register.BAR, register.address(index), register.value)

    def bar_write(self, bar, offset, data):
        return hil.hbiMbBarWrite(self.fpga_index, bar, offset, data)

    def cpld_version_string(self,):
        return hil.hbiMbCpldVersionString(self.fpga_index)

    def log_max10_cpld(self):
        cpld_index = 3
        self.Log('info', f'MAX10 CPLD Version '
                         f'{hil.hbiMbCpldVersionString(cpld_index)}')

    def get_hg_clean_bit(self):
        return self.read_bar_register(self.registers.HG_ID_UPPER).hg_clean

    def fpga_version(self):
        return self.read_bar_register(self.registers.FPGA_VERSION).value

    def read_rctc_aurora_control(self, trigger_link_index):
        return self.read_bar_register(self.registers.AURORA_CONTROL,
                                      trigger_link_index)

    def aurora_status(self, trigger_link_index):
        return self.read_bar_register(self.registers.AURORA_STATUS,
                                      trigger_link_index)

    def aurora_error_count(self, trigger_link_index):
        return self.read_bar_register(self.registers.AURORA_ERROR_COUNTS,
                                      trigger_link_index)

    def send_broadcast_trigger(self, trigger_data):
        self.write_bar_register(
            self.registers.BROADCAST_TRIGGER_DOWN(value=trigger_data))

    def send_trigger(self, trigger_data, trigger_link_index):
        self.write_bar_register(
            self.registers.AURORA_TRIGGER_DOWN(value=trigger_data),
            trigger_link_index)

    def read_trigger(self, trigger_link_index):
        return self.read_bar_register(self.registers.AURORA_TRIGGER_UP,
                                      trigger_link_index).value

    def reset_sw_trigger_fifo(self):
        sw_fifo_trigger_control = self.read_bar_register(
            self.registers.SW_TRIGGER_FIFO_CONTROL)
        sw_fifo_trigger_control.trigger_reset = 1
        self.write_bar_register(sw_fifo_trigger_control)
        sw_fifo_trigger_control = self.read_bar_register(
            self.registers.SW_TRIGGER_FIFO_CONTROL)
        sw_fifo_trigger_control.trigger_reset = 0
        self.write_bar_register(sw_fifo_trigger_control)

    def read_sw_trigger_fifo_count(self):
        current_count = \
            self.read_bar_register(self.registers.SW_TRIGGER_FIFO_COUNT).value
        return current_count
    
    def disable_aurora(self, link):
        r = self.read_bar_register(self.registers.AURORA_CONTROL, link)
        r.reset = 1
        r.clear_error_count = 1
        r.clear_fifos = 1
        self.write_bar_register(r, link)
    
    def enable_aurora(self, link):
        r = self.read_bar_register(self.registers.AURORA_CONTROL, link)
        r.reset = 0
        r.clear_error_count = 0
        r.clear_fifos = 0
        self.write_bar_register(r, link)

    def reset_rctc_aurora_link(self, trigger_link_index, register = None):
        rctc_link_up = False
        if register == None:
            register = self.registers.AURORA_CONTROL()
        for link_reset_loop in range(self.aurora_link_test_loop):
            self.Log('info', f'link_reset_loop = {link_reset_loop}')
            link_status, rctc_aurora_status, rctc_aurora_error_count = \
                self.is_aurora_link_up(trigger_link_index)
            fifo_not_full = self.is_fifo_not_full(trigger_link_index)
            fifo_count = self.is_fifo_count_empty(trigger_link_index)
            error_count = self.is_error_count_empty(trigger_link_index)
            if link_status and fifo_not_full and fifo_count and error_count:
                rctc_link_up = True
                break
            else:
                self.reset_rctc_link(trigger_link_index)
                time.sleep(0.01)
        if rctc_link_up:
            self.Log('info', f'Aurora link up from rctc.Rctc_aurora_status: '
                             f'0x{rctc_aurora_status.value:x} '
                             f'rctc_aurora_error_count '
                             f'0x{rctc_aurora_error_count.value:x}')
        else:
            self.Log('warning', f'Aurora link failed to come up from RC. '
                                f'rc_aurora_status: '
                                f'0x{rctc_aurora_status.value:x} '
                                f'rc_aurora_error_count: '
                                f'0x{rctc_aurora_error_count.value:x}')
        time.sleep(0.015)
        return rctc_link_up

    def is_fifo_not_full(self, trigger_link_index):
        for fifo_empty_loop in range(10):
            status = self.aurora_status(trigger_link_index)
            if status.rx_fifo_full == 0x1 or status.tx_fifo_full == 0x1:
                self.clear_trigger_fifo(trigger_link_index)
            else:
                return True
        else:
            return False

    def is_fifo_count_empty(self, trigger_link_index):
        for count_empty_loop in range(10):
            status = self.aurora_status(trigger_link_index)
            if status.rx_fifo_count != 0x0 or status.tx_fifo_count != 0x0:
                self.clear_trigger_fifo(trigger_link_index)
            else:
                return True
        else:
            return False

    def is_error_count_empty(self, trigger_link_index):
        for error_count in range(10):
            count = self.aurora_error_count(trigger_link_index)
            if count.soft_error_count or count.soft_error_count:
                self.clear_trigger_error_count(trigger_link_index)
            else:
                return True
        else:
            return False

    def clear_trigger_fifo(self, trigger_link_index):
        control = self.read_rctc_aurora_control(trigger_link_index)
        control.clear_fifos = 0x1
        self.write_bar_register(control, trigger_link_index)
        time.sleep(0.001)
        control.clear_fifos = 0x0
        self.write_bar_register(control, trigger_link_index)
        # time.sleep(0.001)

    def clear_trigger_error_count(self, trigger_link_index):
        control = self.read_rctc_aurora_control(trigger_link_index)
        control.clear_error_count = 0x1
        self.write_bar_register(control, trigger_link_index)
        time.sleep(0.001)
        control.clear_error_count = 0x0
        self.write_bar_register(control, trigger_link_index)

    def reset_rctc_link(self, trigger_link_index):
        for poll in range(1, 1000):
            control = self.read_rctc_aurora_control(trigger_link_index)
            control.reset = 0x1
            control.clear_error_count = 0x1
            control.clear_fifos = 0x1
            self.write_bar_register(control, trigger_link_index)
            time.sleep(0.001)
            control.reset = 0x0
            control.clear_error_count = 0x0
            control.clear_fifos = 0x0
            self.write_bar_register(control, trigger_link_index)
            time.sleep(0.001)
            status = self.aurora_status(trigger_link_index).value
            if status == 0x27:
                self.Log('info', f'Aurora link {trigger_link_index} up '
                                 f'(polled {poll} times)')
                break
        else:
            self.Log('warning', f'Aurora link {trigger_link_index} did not '
                                f'come up. status = 0x{status:08X}')

    def is_aurora_link_up(self,trigger_link_index):
        rctc_link_up = False
        status = self.aurora_status(trigger_link_index)
        rctc_aurora_error_count = self.aurora_error_count(trigger_link_index)
        if status.value == 0x27 and rctc_aurora_error_count.value == 0:
            rctc_link_up = True
        return rctc_link_up,status,rctc_aurora_error_count

    def ddr_is_ready(self):
        reg = self.read_bar_register(self.registers.FPGA_DDR4_STATUS)
        return (reg.calibration_success and not reg.calibration_fail and
                reg.reset_done)

    def is_fpga_ddr_capable(self):
        reg = self.read_bar_register(self.registers.FPGA_CAPABILITY)
        return (reg.ddr_capable == 1)

    def is_mainboard_ddr_capable(self):
        # As of WW35.2 we are adding DDR4 back onto the mainboard. Part numbers
        # XXXXXXXX-403 and XXXXXXXX-404 are currently assigned to FabD boards
        # with DDR4. All future boards will have DDR4, use 403 as a
        # check for minimum part number.

        part_number = self.mainboard.blt.PartNumberCurrent
        fab_number = int(part_number[9:len(part_number)])
        return (fab_number >= Hbirctc.DDR4_MAINBOARD_FAB_NUMBER_MINIMUM)

    def temperature_using_hil(self):
        return hil.hbiMbTmonRead(11)

    def temperature_using_bar_read(self, channel=0):
        self.write_bar_register(
            self.registers.TEMPERATURE_SENSOR_CONTROL(channel))
        raw_temperature = self.read_bar_register(
            self.registers.TEMPERATURE_SENSOR_DATA).value
        return stratix10.convert_raw_temp_to_degrees_celcius(raw_temperature)

    def get_cpld_status_interrupt_bit(self):
        return self.read_bar_register(
            self.registers.MAX10_CPLD_STATUS).interrupt

    def clear_sw_trigger_fifo(self):
        current_count = \
            self.read_bar_register(self.registers.SW_TRIGGER_FIFO_COUNT).value
        for i in range(current_count):
            rc_trigger_read = self.registers.SW_TRIGGER_FIFO_READ
            self.read_bar_register(rc_trigger_read)

    def check_sw_trigger_fifo_source(self, source):
        current_count = \
            self.read_bar_register(self.registers.SW_TRIGGER_FIFO_COUNT).value
        for i in range(current_count):
            sw_trigger_source = self.read_bar_register(self.registers.SW_TRIGGER_FIFO_SOURCE).value
            if sw_trigger_source != source:
                self.Log('error', f'Software trigger source mismatch. Expected : {source}')
            rc_trigger_read = self.registers.SW_TRIGGER_FIFO_READ
            self.read_bar_register(rc_trigger_read)

    def log_fan_rps(self):
        for chip_num in MB_FAN_INDEXES:
            try:
                rps = hil.hbiMbFanRead(chip_num)
                self.Log('info', f'MB_Fan_{chip_num} rps: {rps}')
            except RuntimeError:
                self.Log('warning', f'MB_Fan_{chip_num} is unresponsive.')
        for chip_num in DPS_FAN_INDEXES:
            try:
                rps = hil.hbiMbFanRead(chip_num)
                self.Log('info', f'DPS_Fan_{chip_num} rps: {rps}')
            except RuntimeError:
                self.Log('warning', f'DPS_Fan_{chip_num} is unresponsive.')

    def read_sw_trigger_fifo_source(self):
        rc_trigger_source = self.registers.SW_TRIGGER_FIFO_SOURCE
        return self.read_bar_register(rc_trigger_source).value

    def read_sw_trigger_from_rc(self):
        rc_trigger_read = self.registers.SW_TRIGGER_FIFO_READ
        return self.read_bar_register(rc_trigger_read).value

    def reset_link(self, link_partner):
        link = self.get_aurora_link_number(link_partner)
        rc_status, link_partner_status = \
            self.get_aurora_link_status_with_partner(link, link_partner)
        if rc_status == link_partner_status == 0x27:
            self.Log('info',
                     f'{self.name()} <-> {link_partner.name()} '
                     f'Aurora link Pass. {self.name()} '
                     f'status = 0x{rc_status:08X}, {link_partner.name()} '
                     f'status = 0x{link_partner_status:08X}')
        else:
            for attempts in range(100):
                self.reset_aurora_link_with_partner(link, link_partner)
                self.reset_trigger_fifo_error_count_for_link_with_partner(
                    link, link_partner)
                rc_status, link_partner_status = \
                    self.get_aurora_link_status_with_partner(link,
                                                             link_partner)
                if rc_status == link_partner_status == 0x27:
                    if self.link_is_stable(link, link_partner):
                        break
                    else:
                        self.Log('warning', 'Link unstable, retrying')
            else:
                self.Log('error', f'{self.name()} <-> {link_partner.name()} '
                                  f'Aurora link failure. {self.name()} '
                                  f'status = 0x{rc_status:08X}, '
                                  f'{link_partner.name()} '
                                  f'status = 0x{link_partner_status:08X}')

    def reset_link_error_count_before_link_reset(self, link_partner):
        link = self.get_aurora_link_number(link_partner)
        rc_status, link_partner_status = \
            self.get_aurora_link_status_with_partner(link, link_partner)
        if rc_status == link_partner_status == 0x27:
            self.Log('info', f'{self.name()} <-> {link_partner.name()} '
                             f'Aurora link Pass. {self.name()} '
                             f'status = 0x{rc_status:08X}, '
                             f'{link_partner.name()} '
                             f'status = 0x{link_partner_status:08X}')
        else:
            self.Log('info', 'reset link fifo error count')
            self.reset_trigger_fifo_error_count_for_link_with_partner(
                link, link_partner)
            time.sleep(0.5)  # wait for aurora link to update its status
            self.reset_link(link_partner)

    def reset_trigger_fifo_error_count_for_link_with_partner(self, link,
                                                             link_partner):
        self.clear_trigger_fifo(link)
        self.clear_trigger_error_count(link)
        link_partner.clear_trigger_fifo(link)
        link_partner.clear_trigger_error_count(link)
        time.sleep(0.015)

    def reset_aurora_link_with_partner(self, link, link_partner):
        self.disable_aurora(link)
        link_partner.disable_aurora(link)
        self.enable_aurora(link)
        link_partner.enable_aurora(link)
        time.sleep(0.015)

    @staticmethod
    def get_aurora_link_number(link_partner):
        if 'PATGEN' == link_partner.name():
            return 16
        else:
            return 17

    def get_aurora_link_status_with_partner(self, link, link_partner):
        rc_status = self.aurora_status(link).value
        link_partner_status = link_partner.aurora_status(link).value
        return rc_status, link_partner_status

    def link_is_stable(self, link, link_partner):
        for i in range(100):
            expected = random.getrandbits(32)
            received_at_link_partner, polls = \
                self.send_trigger_from_sender_to_receiver(self, link, expected,
                                                          link_partner)
            observed, polls = self.send_trigger_from_sender_to_receiver(
                link_partner, link, received_at_link_partner, self)
            if expected != observed:
                return False
        return True

    def send_trigger_from_sender_to_receiver(self, sender, link, value,
                                             receiver):
        sender.send_trigger(value, link)
        i = 0
        received_trigger = receiver.read_trigger(link)
        for i in range(100):
            if self.is_expected_trigger_seen_regardless_of_cycle_count(
                    value, received_trigger):
                break
            received_trigger = receiver.read_trigger(link)
        return received_trigger, i + 1

    def verify_trigger_received(self, link, receiver, expected_value):
        i = 0
        received_trigger = receiver.read_trigger(link)
        for i in range(100):
            if self.is_expected_trigger_seen_regardless_of_cycle_count(
                    expected_value, received_trigger):
                break
            received_trigger = receiver.read_trigger(link)
        else:
            self.Log('error', f'expected trigger: {hex(expected_value)} != '
                              f'received_trigger: {hex(received_trigger)}')
        return i, received_trigger

    def is_expected_trigger_seen_regardless_of_cycle_count(
            self, expected_trigger, observed_trigger):
        return (observed_trigger & 0xffffff0f) == \
               (expected_trigger & 0xffffff0f)

    def read_clock_frequency_mhz(self, index):
        # See HBI_RCTC_FPGA-HPS_HLD, Revision 4.0, Section 7.18.2
        reference_clock_period= 1/125.0  # 8ns

        reg = self.read_bar_register(
            self.registers.CLOCK_FREQUENCY_MEASUREMENT, index)

        # HBI_RCTC_FPGA-HPS_HLD, Revision 4.0, Section 7.18
        if reg.meas_counter == 0 or reg.meas_counter == 0:
            measured_clock_frequency = 0
        else:
            measured_clock_period = \
                ((reference_clock_period * reg.ref_window_size) /
                 (reg.meas_counter * reg.divider_ratio))
            measured_clock_frequency = 1.0 / measured_clock_period

        return measured_clock_frequency

    def ddr_controller_reset(self):
        return self.read_bar_register(self.registers.FPGA_DDR4_CONTROL).reset

    def ddr_status_register(self):
        return self.read_bar_register(self.registers.FPGA_DDR4_STATUS)

    def is_ddr_reset_done_after_wait(self, retries=0xFFFFFF):
        for retry in range(retries):
            if self.is_controller_reset_done():
                return True
        else:
            return False

    def is_controller_reset_done(self):
        reset_done = self.ddr_status_register().reset_done
        return True if reset_done else False

    def issue_ddr_reset(self):
        self.write_ddr_controller_reset(1)
        self.write_ddr_controller_reset(0)

    def write_ddr_controller_reset(self, data):
        controller = self.registers.FPGA_DDR4_CONTROL()
        controller.reset = data
        self.write_bar_register(controller)

    def is_status_calibration_after_wait(self, retries=0xFFFFFF):
        for retry in range(retries):
            if self.is_ddr_calibrated():
                return True
        else:
            return False

    def is_ddr_calibrated(self):
        calibration_success = self.ddr_status_register().calibration_success
        return calibration_success == 1

    def hardware_compatibility(self):
        reg = self.read_bar_register(self.registers.TARGET_HARDWARE)
        return reg.hw_compatibility

    def write_fpga_debug_control(self, control):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        fpga_debug.control = control
        self.write_bar_register(fpga_debug)

    def fpga_debug_control(self):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        return fpga_debug.control

    def write_fpga_debug_data(self, data):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        fpga_debug.output = data
        self.write_bar_register(fpga_debug)

    def fpga_debug_data(self):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        return fpga_debug.output

    def fpga_debug_input(self):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        return fpga_debug.input

    def failsafe_limit(self):
        reg = self.read_bar_register(self.registers.FAILSAFE_LIMIT)
        return reg.limit

    def write_failsafe_limit(self, limit):
        reg = self.registers.FAILSAFE_LIMIT(limit=limit)
        self.write_bar_register(reg)

    def failsafe_reset(self):
        reg = self.read_bar_register(self.registers.FAILSAFE_CONTROL)
        return reg.reset

    def reset_failsafe_limit(self):
        self.write_failsafe_control(reset=1)
        self.write_failsafe_control(reset=0)

    def write_failsafe_control(self, reset):
        reg = self.registers.FAILSAFE_CONTROL(reset=reset)
        self.write_bar_register(reg)

    def power_on_count(self):
        return self.read_bar_register(self.registers.POWER_ON_COUNT).value

    def general_purpose_input(self):
        reg = self.read_bar_register(self.registers.GENERAL_PURPOSE_INPUT)
        return reg.value

    def general_purpose_output(self):
        reg = self.read_bar_register(self.registers.GENERAL_PURPOSE_OUTPUT)
        return reg.value

    def fpga_led(self):
        reg = self.read_bar_register(self.registers.FPGA_LED)
        return reg.led_control

    def write_fpga_led(self, control):
        self.write_bar_register(self.registers.FPGA_LED(led_control=control))

    def hps_communications(self):
        reg = self.read_bar_register(self.registers.HPS_COMMUNICATIONS)
        return reg.communications

    def write_hps_communications(self, data):
        self.write_bar_register(self.registers.HPS_COMMUNICATIONS(
            communications=data))

    def system_fail_safe(self):
        reg = self.read_bar_register(self.registers.GENERAL_PURPOSE_OUTPUT)
        return reg.system_fail_safe

    def enable_system_fail_safe(self, enable=True):
        reg = self.read_bar_register(self.registers.GENERAL_PURPOSE_OUTPUT)
        reg.system_fail_safe = int(enable)
        self.write_bar_register(reg)

    def live_time_stamp(self):
        reg = self.read_bar_register(self.registers.LIVE_TIME_STAMP)
        return reg.value

    def reset_live_time_stamp_timer(self):
        reg = self.read_bar_register(self.registers.TRIGGER_SYNC)
        reg.time_stamp_reset = 0
        self.write_bar_register(reg)
        reg.time_stamp_reset = 1
        self.write_bar_register(reg)
        reg.time_stamp_reset = 0
        self.write_bar_register(reg)

    def send_broadcast_trigger(self, trigger):
        self.write_bar_register(self.registers.BROADCAST_TRIGGER_DOWN(
            value=trigger))

    def read_last_broadcasted_trigger(self):
        return self.read_bar_register(
            self.registers.LAST_BROADCASTED_TRIGGER).trigger
			
    def backplane_alarms(self):
        reg = self.read_bar_register(self.registers.BP_ALARMS)
        return [reg.bps_0_alarm, reg.bps_1_alarm, reg.bps_2_alarm]
		
