# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from time import sleep

from Common import hilmon as hil
from Common.fval import Log
from Common.register import create_field_dictionary
from Hbirctc.instrument.hbirctc_register import MAX10_CPLD_COMMAND_DATA,\
    MAX10_CPLD_CONTROL, MAX10_CPLD_READ_DATA, MAX10_CPLD_STATUS
import Hbirctc.instrument.max10_register as registers


class Max10():
    def __init__(self, rc):
        self.rc = rc

    def read_word(self, address):
        num_words = 1
        return hil.hbiMbMax10CpldRead(address, num_words)[0]

    def write_word(self, address, data):
        data_list = [data]
        hil.hbiMbMax10CpldWrite(address, data_list)

    def read_fw_revision(self):
        num_words = 3
        # [major, minor, pcb]
        return hil.hbiMbMax10CpldRead(registers.RCTC_FW_REV_MAJ.ADDR,
                                      num_words)

    def status_reg(self):
        reg = registers.RCTC_IO_STATUS(
            value=self.read_word(registers.RCTC_IO_STATUS.ADDR))
        return reg

    def sys_fail_safe(self):
        status = self.status_reg()
        return status.sys_fail_safe

    def log_status_reg(self, log_level='info'):
        status_dict = create_field_dictionary(self.status_reg())
        Log(log_level, status_dict)

    def interrupt_reg(self):
        reg = registers.RCTC_INT_LINES(
            value=self.read_word(registers.RCTC_INT_LINES.ADDR))
        return reg

    def clear_int0_error_latch(self):
        reg = self.interrupt_reg()
        reg.clear_INT0_error_latch = 1
        self.write_word(registers.RCTC_INT_LINES.ADDR, reg.value)
        sleep(0.25)
        reg.clear_INT0_error_latch = 0
        self.write_word(registers.RCTC_INT_LINES.ADDR, reg.value)
        sleep(0.25)

    def rctc_bps_mask_shutdown_reg(self):
        reg = registers.RCTC_BPS_MASK_SHUTDOWN_FABC(
            value=self.read_word(registers.RCTC_BPS_MASK_SHUTDOWN_FABC.ADDR))
        return reg

    def enable_sys_fail_safe_mask(self, enable=True):
        reg = self.rctc_bps_mask_shutdown_reg()
        reg.mask_sys_fail_safe = int(enable)
        self.write_word(reg.ADDR, reg.value)

    def log_rctc_bps_mask_shutdown_reg(self, log_level='info'):
        mask_dict = create_field_dictionary(self.rctc_bps_mask_shutdown_reg())
        Log(log_level, mask_dict)

    def enable_limit_errors_mask(self, enable=True):
        reg = registers.RCTC_INT_LINES(
            value=self.read_word(registers.RCTC_INT_LINES.ADDR))
        reg.mask_limit_errors_ = int(enable)
        self.write_word(reg.ADDR, reg.value)

    def log_rctc_int_lines_reg(self, log_level='info'):
        reg = registers.RCTC_INT_LINES(
            value=self.read_word(registers.RCTC_INT_LINES.ADDR))
        mask_dict = create_field_dictionary(reg)
        Log(log_level, mask_dict)

    def write_rctc_scratch_pad(self, data):
        self.write_word(registers.RCTC_SCRATCH_PAD.ADDR, data)

    def rctc_scratch_pad(self):
        data = self.read_word(registers.RCTC_SCRATCH_PAD.ADDR)
        return data

    def send_read_command(self, address):
        read_spi_command = 1
        reg = MAX10_CPLD_COMMAND_DATA(read_write=read_spi_command,
                                      offset=address)
        self.rc.write_bar_register(reg)

    def send_nop_command(self):
        self.rc.write_bar_register(MAX10_CPLD_COMMAND_DATA(value=0))

    def read_hbirctc_status_reg(self):
        return self.rc.read_bar_register(MAX10_CPLD_STATUS)

    def wait_on_cpld_busy(self, num_retries=100):
        for retry in range(num_retries):
            if not self.read_hbirctc_status_reg().busy:
                return
        else:
            Log('warning', f'Timed out waiting on Max10 CPLD busy bit')

    def rx_fifo_count(self):
        reg = self.read_hbirctc_status_reg()
        return reg.rx_fifo_count

    def read_data(self):
        return self.rc.read_bar_register(MAX10_CPLD_READ_DATA).data

    def reset_fifos(self):
        self.rc.write_bar_register(MAX10_CPLD_CONTROL(
            controller_and_fifo_reset=1))
        self.rc.write_bar_register(MAX10_CPLD_CONTROL(
            controller_and_fifo_reset=0))
