# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import Common.hilmon as hil
from Common.instruments.i2c_test_interface import I2cTestInterface
from Hbirctc.instrument.hbirctc_register import BPS_I2C_STATUS,\
    BPS_I2C_CONTROL, BPS_I2C_TX_FIFO, BPS_I2C_RX_FIFO, BPS_I2C_RESET

I2C_ADDR = 0x80
CAPABILITY_CMD = 0x19
CAPABILITY_VALUE = 0xB0
MAX_NUM_INTERFACES = BPS_I2C_STATUS.REGCOUNT


def create_interfaces(instrument):
    interfaces = [BpsInterface(instrument, interface_num) for interface_num in
                  range(MAX_NUM_INTERFACES)]
    return interfaces


class BpsInterface(I2cTestInterface):
    def __init__(self, instrument, interface_num):
        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR,
                         interface_index=interface_num,
                         interface_name=f'BPS_{interface_num}')
        self.update_register_table({'STATUS': BPS_I2C_STATUS,
                                    'CONTROL': BPS_I2C_CONTROL,
                                    'TX_FIFO': BPS_I2C_TX_FIFO,
                                    'RX_FIFO': BPS_I2C_RX_FIFO,
                                    'RESET': BPS_I2C_RESET})

    def read_capability(self):
        data = hil.hbiPsPmbusRead(self.index, CAPABILITY_CMD, byte_length=1)
        return int(bytes.hex(data), 16)
