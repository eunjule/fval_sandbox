# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.instruments.i2c_test_interface import I2cTestInterface
from Hbirctc.instrument.hbirctc_register import PMBUS_MONITOR_I2C_CONTROL
from Hbirctc.instrument.hbirctc_register import PMBUS_MONITOR_I2C_RESET
from Hbirctc.instrument.hbirctc_register import PMBUS_MONITOR_I2C_RX_FIFO
from Hbirctc.instrument.hbirctc_register import PMBUS_MONITOR_I2C_STATUS
from Hbirctc.instrument.hbirctc_register import PMBUS_MONITOR_I2C_TX_FIFO


LTM4680_I2C_ADDR = 0x80
MAX_NUM_INTERFACES = PMBUS_MONITOR_I2C_STATUS.REGCOUNT


def create_interfaces(instrument):
    interfaces = [PmBusMonitorInterface(instrument, index)
                  for index in range(MAX_NUM_INTERFACES)]
    return interfaces


class PmBusMonitorInterface(I2cTestInterface):
    """I2C Interface to the LTM4680"""

    KNOWN_VALUE_REGISTERS = {0x19: 0xB0,
                             0x98: 0x22}

    def __init__(self, instrument, index):
        super().__init__(instrument=instrument,
                         i2c_addr=LTM4680_I2C_ADDR,
                         interface_name=f'PmBus Monitor {index}',
                         interface_index=index)

        self.update_register_table({'STATUS': PMBUS_MONITOR_I2C_STATUS,
                                    'CONTROL': PMBUS_MONITOR_I2C_CONTROL,
                                    'TX_FIFO': PMBUS_MONITOR_I2C_TX_FIFO,
                                    'RX_FIFO': PMBUS_MONITOR_I2C_RX_FIFO,
                                    'RESET': PMBUS_MONITOR_I2C_RESET})
