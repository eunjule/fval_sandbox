################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from Common import hilmon as hil
from Hbirctc.instrument.max6650_register import CONFIG_REG

class Max6650():
    def __init__(self, chip_num):
        self.chip_num = chip_num

    def read_register(self, register):
        return hil.hbiMbMax665xRead(self.chip_num, register)

    def write_register(self, register, data):
        hil.hbiMbMax665xWrite(self.chip_num, register, data)

    def get_configuration_reg(self):
        data = self.read_register(CONFIG_REG.ADDR)
        return CONFIG_REG(value=data)

    def get_configuration_mode(self):
        return self.get_configuration_reg().mode

    def set_configuration_mode(self, mode):
        reg = self.get_configuration_reg()
        reg.mode = mode
        self.write_register(reg.ADDR, reg.value)

    def get_rps(self):
        return hil.hbiMbFanRead(self.chip_num)