# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.instruments.i2c_test_interface import I2cTestInterface
from Hbirctc.instrument.hbirctc_register import FAN_CONTROLLER_I2C_CONTROL
from Hbirctc.instrument.hbirctc_register import FAN_CONTROLLER_I2C_RESET
from Hbirctc.instrument.hbirctc_register import FAN_CONTROLLER_I2C_RX_FIFO
from Hbirctc.instrument.hbirctc_register import FAN_CONTROLLER_I2C_STATUS
from Hbirctc.instrument.hbirctc_register import FAN_CONTROLLER_I2C_TX_FIFO


I2C_ADDR = [0x90, 0x96, 0x36, 0x3E]  # If read, I2C_ADDR | 1
MAX_NUM_INTERFACES = 3
NUM_CHIPS_PER_CONTROLLER = 4
MAX_NUM_CHIPS = NUM_CHIPS_PER_CONTROLLER * MAX_NUM_INTERFACES


def create_interfaces(instrument):
    interfaces = [FanControllerI2cInterface(instrument, chip_num)
                  for chip_num in range(NUM_CHIPS_PER_CONTROLLER *
                                        MAX_NUM_INTERFACES)]
    return interfaces


def get_controller_index(chip_num):
    return int(chip_num / NUM_CHIPS_PER_CONTROLLER)


def get_chip_num(i2c_addr, controller_index):
    i2c_addr &= 0xFE  # remove read bit
    chip_dict =  {0x90: 0 + (controller_index * NUM_CHIPS_PER_CONTROLLER),
                  0x96: 1 + (controller_index * NUM_CHIPS_PER_CONTROLLER),
                  0x36: 2 + (controller_index * NUM_CHIPS_PER_CONTROLLER),
                  0x3E: 3 + (controller_index * NUM_CHIPS_PER_CONTROLLER)}
    return chip_dict[i2c_addr]


# chip_num,   Fan_controller_num, i2c_addr
# 0:          0                   0x90
# 1:          0                   0x96
# 2:          0                   0x36
# 3:          0                   0x3E
# 4:          1                   0x90
# 5:          1                   0x96
# 6:          1                   0x36
# 7:          1                   0x3E
# 8:          2                   0x90
# 9:          2                   0x96
# 10:         2                   0x36
# 11:         2                   0x3E
class FanControllerI2cInterface(I2cTestInterface):
    def __init__(self, instrument, chip_num):
        self.fan_controller_index = get_controller_index(chip_num)
        self.chip_num = chip_num
        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR[self.i2c_addr_index()],
                         interface_name=f'Fan Chip {chip_num}',
                         interface_index=self.fan_controller_index)

        self.update_register_table({'STATUS': FAN_CONTROLLER_I2C_STATUS,
                                    'CONTROL': FAN_CONTROLLER_I2C_CONTROL,
                                    'TX_FIFO': FAN_CONTROLLER_I2C_TX_FIFO,
                                    'RX_FIFO': FAN_CONTROLLER_I2C_RX_FIFO,
                                    'RESET': FAN_CONTROLLER_I2C_RESET})

    def i2c_addr_index(self):
        return self.chip_num % NUM_CHIPS_PER_CONTROLLER
