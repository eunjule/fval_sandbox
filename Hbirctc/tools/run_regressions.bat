@echo off
setlocal enabledelayedexpansion

if %1==-h (
    echo Two arguments required:
    echo 1: any name for these regressions
    echo 2: number of iterations to repeat
    goto end
)

set argCount=0
for %%x in (%*) do (
   set /A argCount+=1
   set "argVec[!argCount!]=%%~x"
)

set REGRESSION_NAME=!argVec[1]!
set NUM_ITERATIONS=!argVec[2]!


py ./Tools/regress.py hbirctc -r 10 -o !REGRESSION_NAME!_hbirctc_repeat10
for /L %%i in (1, 1, !NUM_ITERATIONS!) do (
    echo ##################################### Iteration %%i #####################################
    py ./Tools/regress.py hbirctc -o !REGRESSION_NAME!_hbirctc_%%i
    py ./Tools/regress.py hbicc -tn Triggers.*.* -o !REGRESSION_NAME!_hbirctc_hbicc_triggers_%%i
    py ./Tools/regress.py hbidps -tn Trigger.*.* -o !REGRESSION_NAME!_hbirctc_hbidps_triggers_%%i
    echo -----------------------------------------------------------------------------------------
    echo.
)


:end
EXIT /B %ERRORLEVEL%
