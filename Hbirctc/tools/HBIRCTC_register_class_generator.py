################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import argparse
import sys
import os.path


def extractPageData(fileData):
    page_data = []
    for page_info in fileData:
        # PAGE_00            = 'h0000,
        page_info_regex = re.compile(r'\s*([0-9a-zA-Z_]+)\s*\=\s\'h(\w*)', re.IGNORECASE)
        page_info = page_info_regex.search(page_info)
        try:
            page_number = page_info.group(1)
            page_address = page_info.group(2)
            page_data.append((page_number, page_address))
        except:
            pass
    return page_data

def extractRegisterData(fileData,page_data):
    register_data = []
    print(page_data)
    for register_info in fileData:
        # BAR1_SCRATCH = PAGE_00 + 'h00,  // (0x000)
        # register_info_regex = re.compile(r'\s*([0-9a-zA-Z_]+)\s*\=\s([0-9a-zA-Z_]+)\s\+\s\'h(\w*)', re.IGNORECASE)
        register_info_regex = re.compile(r'\s*([0-9a-zA-Z_]+)\s*\=\s([0-9a-zA-Z_]+)\s\+\s\'h([0-9A-Fa-f]+)', re.IGNORECASE)
        register_info = register_info_regex.search(register_info)
        try:
            register_name = register_info.group(1)
            register_page_address = register_info.group(2)
            register_address_final = add_page_base_address(page_data, register_info, register_page_address)
            register_data.append((register_name,register_address_final))
            print(register_data)
            print('')
        except:
            pass
    return register_data

def printRegisterClasses(bar_regs, registerClass, registerBar):
    for registerName, registerAddress in bar_regs:
        print('class {}({}):'.format(registerName, registerClass))
        print('    BAR = {}'.format(registerBar))
        print('    ADDR = {}'.format((registerAddress)))
        print('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]')
        print('')

def writeRegisterClasses(bar_regs, registerClass, registerBar, outputFileName):
    with open(outputFileName, 'w') as registerFile:
        for registerName, registerAddress in bar_regs:
            registerFile.write('class {}({}):\n'.format(registerName, registerClass))
            registerFile.write('    BAR = {}\n'.format(registerBar))
            registerFile.write('    ADDR = {}\n'.format((registerAddress)))
            registerFile.write('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]\n\n')

def read_and_clean_input_file(fileName):
    fileData = []
    with open(fileName) as f:
        for line in f:
            fileData.append(line)
    return fileData

def output_register_classes(OutputFileName, bar_regs, args):
    if OutputFileName == None:
        printRegisterClasses(bar_regs, args.regclass, args.bar)
    else:
        if os.path.isfile(OutputFileName):
            print('Output file already exists:')
            overwrite = input('\tWould you like to overwrite existing output file {}?\n'.format(OutputFileName))
            if overwrite.lower() == 'y' or overwrite.lower == 'yes':
                print('\n\toverwriting existing register output file')
            else:
                print('\tFile already exists: Exiting')
                sys.exit(1)
        writeRegisterClasses(bar_regs, args.regclass, args.bar, OutputFileName)

def add_page_base_address(page_data, register_info, register_page_address):
    if register_page_address == 'SCRATCH_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[0][1],16))
    if register_page_address == 'VERSION_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[1][1],16))
    if register_page_address == 'MAX10_CPLD_COMM_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[2][1],16))
    if register_page_address == 'HPS_TO_FPGA_SPI_COMM_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[3][1],16))
    if register_page_address == 'TRIGGER_SYNC_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[4][1],16))
    if register_page_address == 'TRIGGER_PAGE_0':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[5][1],16))
    if register_page_address == 'TRIGGER_PAGE_1':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[6][1],16))
    if register_page_address == 'TRIGGER_PAGE_2':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[7][1],16))
    if register_page_address == 'TRIGGER_PAGE_3':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[8][1],16))
    if register_page_address == 'TRIGGER_PAGE_4':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[9][1],16))
    if register_page_address == 'TRIGGER_PAGE_5':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[10][1],16))
    if register_page_address == 'TRIGGER_PAGE_6':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[11][1],16))
    if register_page_address == 'TRIGGER_PAGE_7':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[12][1],16))
    if register_page_address == 'TRIGGER_PAGE_8':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[13][1],16))
    if register_page_address == 'TRIGGER_PAGE_9':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[14][1], 16))
    if register_page_address == 'TRIGGER_PAGE_10':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[15][1], 16))
    if register_page_address == 'TRIGGER_PAGE_11':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[16][1], 16))
    if register_page_address == 'TRIGGER_PAGE_12':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[17][1], 16))
    if register_page_address == 'TRIGGER_PAGE_13':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[18][1],16))
    if register_page_address == 'TRIGGER_PAGE_14':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[19][1],16))
    if register_page_address == 'TRIGGER_PAGE_15':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[20][1], 16))
    if register_page_address == 'TRIGGER_PAGE_16':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[21][1], 16))
    if register_page_address == 'TRIGGER_PAGE_17':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[22][1], 16))
    if register_page_address == 'TRIGGER_PAGE_18':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[23][1], 16))
    if register_page_address == 'GPIO_PAGE':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[24][1], 16))
    if register_page_address == 'FPGA_DDR4_PAGE':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[25][1], 16))
    if register_page_address == 'TDAU_I2C_PAGE_0':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[26][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_0':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[27][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_2':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[28][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_3':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[29][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_4':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[30][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_5':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[31][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_6':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[32][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_7':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[33][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_8':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[34][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_9':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[35][1], 16))
    if register_page_address == 'TDAU_I2C_PAGE_10':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[36][1], 16))
    if register_page_address == 'TDAU_I2C_PAGE_11':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[37][1], 16))
    if register_page_address == 'TDAU_I2C_PAGE_12':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[38][1], 16))
    if register_page_address == 'TDAU_I2C_PAGE_13':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[39][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_14':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[40][1],16))
    if register_page_address == 'TDAU_I2C_PAGE_15':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[41][1], 16))
    if register_page_address == 'FAN_I2C_PAGE_0':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[42][1],16))
    if register_page_address == 'FAN_I2C_PAGE_1':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[43][1],16))
    if register_page_address == 'FAN_I2C_PAGE_2':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[44][1],16))
    if register_page_address == 'BP_I2C_PAGE_0':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[45][1],16))
    if register_page_address == 'BP_I2C_PAGE_1':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[46][1],16))
    if register_page_address == 'BP_I2C_PAGE_2':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[47][1],16))
    if register_page_address == 'PECI_DAC_COMM_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[48][1],16))
    if register_page_address == 'ALARMS_PAGE':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[49][1],16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_0':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[50][1],16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_1':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[51][1], 16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_2':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[52][1], 16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_3':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[53][1], 16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_4':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[54][1], 16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_5':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[55][1],16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_6':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[56][1],16))
    if register_page_address == 'PSDB_BLT_I2C_PAGE_7':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[57][1], 16))
    if register_page_address == 'PECI_PAGE_0':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[58][1], 16))
    if register_page_address == 'PECI_PAGE_1':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[59][1],16))
    if register_page_address == 'PECI_PAGE_2':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[60][1],16))
    if register_page_address == 'PECI_PAGE_3':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[61][1],16))
    if register_page_address == 'PECI_PAGE_4':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[62][1],16))
    if register_page_address == 'PECI_PAGE_5':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[63][1],16))
    if register_page_address == 'PECI_PAGE_6':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[64][1],16))
    if register_page_address == 'PECI_PAGE_7':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[65][1],16))
    if register_page_address == 'PECI_PAGE_8':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[66][1],16))
    if register_page_address == 'PECI_PAGE_9':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[67][1],16))
    if register_page_address == 'PECI_PAGE_10':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[68][1], 16))
    if register_page_address == 'PECI_PAGE_11':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[69][1], 16))
    if register_page_address == 'PECI_PAGE_12':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[70][1], 16))
    if register_page_address == 'PECI_PAGE_13':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[71][1], 16))
    if register_page_address == 'PECI_PAGE_14':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[72][1],16))
    if register_page_address == 'PECI_PAGE_15':
        register_address_final = hex(int(register_info.group(3),16) + int(page_data[73][1],16))
    if register_page_address == 'TEMPERATURE_SENSOR_PAGE':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[74][1], 16))
    if register_page_address == 'FAILSAFE_PAGE':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[75][1], 16))
    if register_page_address == 'DEBUG_PAGE':
        register_address_final = hex(int(register_info.group(3), 16) + int(page_data[76][1], 16))
    return register_address_final

def parse_args(args):
    # Parse command line options
    parser = argparse.ArgumentParser(description='VH to CTYPE register generator')
    parser.add_argument('inputFile', help='Input .vh file that contains register definitions', type=str, action="store")
    parser.add_argument('-bar', help='Bar for registers', type=int, action="store", required=True)
    parser.add_argument('-regclass', help='Register class that registers will derive from', type=str, action="store",
                        required=True)
    parser.add_argument('-o', '--outputfilename', help='optional output filename, otherwise prints to screen', type=str,
                        action="store", default=None)
    args = parser.parse_args(args)
    return args

def Main(user_args=None):
    args = parse_args(user_args)

    outputFileName = args.outputfilename
    inputFileName = args.inputFile

    try:
        with open(inputFileName):
            pass
    except FileNotFoundError:
        print('Input File \'{}\' does not exist!'.format(inputFileName))
        sys.exit(1)

    fileData = read_and_clean_input_file(inputFileName)
    page_data = extractPageData(fileData)
    register_data = extractRegisterData(fileData,page_data)
    output_register_classes(outputFileName, register_data, args)


if __name__ == '__main__':
    Main()
