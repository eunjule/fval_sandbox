# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from math import nan

from Common.utilities import dword_to_float
from Hbirctc.instrument import spm_scopeshot_interface
from Hbirctc.instrument.hbirctc_register import \
    SPM_SCOPESHOT_LATEST_DDR4_ADDRESS as LATEST_ADDR,\
    SPM_SCOPESHOT_DDR4_ENDING_ADDRESS as ENDING_ADDR,\
    SPM_SCOPESHOT_DDR4_STARTING_ADDRESS as STARTING_ADDR,\
    SPM_SCOPESHOT_CONTROL as CONTROL,\
    SPM_SCOPESHOT_CONFIG as CONFIG,\
    SPM_SCOPESHOT_STATUS as STATUS


class SpmScopeshotInterface():
    DDR4_STARTING_ADDRESS_DEFAULT = spm_scopeshot_interface.\
        SpmScopeshotInterface.DDR4_STARTING_ADDRESS_DEFAULT
    DDR4_ENDING_ADDRESS_DEFAULT = spm_scopeshot_interface.\
        SpmScopeshotInterface.DDR4_ENDING_ADDRESS_DEFAULT
    DDR4_ENTRY_BYTE_SIZE = spm_scopeshot_interface.\
        SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE
    PACKET_REFRESH_RATE_USEC = int(spm_scopeshot_interface.\
        SpmScopeshotInterface.PACKET_REFRESH_RATE_SEC * 1e6)

    def __init__(self, rc):
        self.write_actions = {
            ENDING_ADDR.ADDR: self.bar_write,
            STARTING_ADDR.ADDR: self.bar_write,
            CONTROL.ADDR: self.write_control,
            CONFIG.ADDR: self.write_config
        }

        self.read_actions = {
            LATEST_ADDR.ADDR: self.read_latest_addr,
            ENDING_ADDR.ADDR: self.bar_read,
            STARTING_ADDR.ADDR: self.bar_read,
            CONFIG.ADDR: self.bar_read,
            STATUS.ADDR: self.bar_read
        }

        self.registers = {
            LATEST_ADDR.ADDR:
                SpmScopeshotInterface.DDR4_STARTING_ADDRESS_DEFAULT,
            ENDING_ADDR.ADDR:
                SpmScopeshotInterface.DDR4_ENDING_ADDRESS_DEFAULT,
            STARTING_ADDR.ADDR:
                SpmScopeshotInterface.DDR4_STARTING_ADDRESS_DEFAULT,
            CONTROL.ADDR: 0,
            CONFIG.ADDR: 0,
            STATUS.ADDR: 0
        }
        self.rc = rc
        self.time_stamp_us = 0
        self.refresh_rate_us = SpmScopeshotInterface.PACKET_REFRESH_RATE_USEC
        self.sample_count_counter = 0

    def bar_write(self, offset, data):
        self.registers[offset] = data

    def bar_read(self, offset):
        return self.registers[offset]

    def assert_stop(self):
        self.write_control(offset=None, data=CONTROL(stop=1).value)

    def write_control(self, offset, data):
        data = CONTROL(value=data)
        if data.start and self.config_reg().enable:
            self.registers[STATUS.ADDR] = STATUS(active=1).value
            self.registers[LATEST_ADDR.ADDR] = \
                self.registers[STARTING_ADDR.ADDR] - \
                SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE
            self.sample_count_counter = self.sample_count()
        if data.stop:
            self.registers[STATUS.ADDR] = STATUS(active=0).value

    def write_config(self, offset, data):
        self.registers[CONFIG.ADDR] = data

    def read_latest_addr(self, offset=None):
        is_sampling = bool(self.sample_count_counter)
        if self.is_active() or is_sampling:
            self.registers[LATEST_ADDR.ADDR] = self.next_address(
                self.registers[LATEST_ADDR.ADDR])
            self.add_sensor_data_to_memory(self.registers[LATEST_ADDR.ADDR])
            if is_sampling and not self.is_active():
                self.sample_count_counter -= 1
        return self.registers[LATEST_ADDR.ADDR]

    def add_sensor_data_to_memory(self, address):
        bps = [self.rc.spm._bps_present(i) for i in range(self.rc.spm.NUM_BPS)]
        spm_enable = self.rc.spm._spm_enable()
        packet = self.generate_packet(bps, spm_enable, self.time_stamp_us)

        self.time_stamp_us += self.refresh_rate_us
        self.rc.dma_write(address, bytes(packet))

    def config_reg(self):
        return CONFIG(value=self.registers[CONFIG.ADDR])

    def is_active(self):
        return bool(STATUS(value=self.registers[STATUS.ADDR]).active)

    def generate_packet(self, bps, spm_enable, time_stamp_us):
        packet = spm_scopeshot_interface.SpmScopeshotPacket(value=0)
        packet.bps_0_present = bps[0]
        packet.bps_1_present = bps[1]
        packet.bps_2_present = bps[2]
        packet.spm_enable = spm_enable
        packet.time_stamp_us = time_stamp_us

        if not spm_enable or bps == [0, 0, 0]:
            packet.pout_sum = packet.pout_bps_0 = packet.pout_bps_1 = \
                packet.pout_bps_2 = nan
        else:
            pout_default = dword_to_float(self.rc.spm.DEFAULT_POUT)
            packet.pout_bps_0 = nan if not bps[0] else pout_default
            packet.pout_bps_1 = nan if not bps[1] else pout_default
            packet.pout_bps_2 = nan if not bps[2] else pout_default
            packet.pout_sum = pout_default * bps.count(1)

        return packet

    def next_address(self, address):
        sa = self.registers[STARTING_ADDR.ADDR]
        ea = self.registers[ENDING_ADDR.ADDR]
        ddr4_entry_byte_size = SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE
        if address < (ea - ddr4_entry_byte_size):
            return address + ddr4_entry_byte_size
        else:
            return sa

    def sample_count(self):
        return CONFIG(self.registers[CONFIG.ADDR]).sample_count

    def in_alarm_mode(self):
        return self.config_reg().mode == 1
