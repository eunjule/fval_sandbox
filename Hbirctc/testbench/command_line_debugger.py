# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import inspect
method_name = lambda : inspect.stack()[1][3]

import logging

from Common.fval import ConfigLogger, Log, SetLoggerLevel
from Common.hilmon import hil
from Common.utilities import performance_sleep
from Hbicc.instrument.mainboard import MainBoard
from Hbirctc.instrument.hbirctc import Hbirctc

def prog_fpga(image=None):
    temp = hbirctc.fpga_image_path
    if image is not None:
        hbirctc.fpga_image_path = lambda : image

    Log('info', f'Loading FPGA using \"{hbirctc.fpga_image_path()}\"...')
    hbirctc.load_fpga_using_hil()
    Log('info', f'Loading complete!')
    Log('info', f'{hbirctc.name()} FPGA Version '
                f'{hbirctc.fpga_version_string()}')
    hbirctc.fpga_image_path = temp


def scopeshot_latest_address(iteration_num=0, n=None, sa=None, ea = None, dbg_print=False):
    from Hbirctc.instrument.spm_interface import SpmInterface
    from Hbirctc.instrument.spm_scopeshot_interface import \
        SpmScopeshotInterface

    packet_byte_size = SpmScopeshotInterface.DDR4_ENTRY_BYTE_SIZE

    spm = SpmInterface(Log)
    scopeshot = SpmScopeshotInterface(hbirctc)

    spm.initialize_limits()
    assert spm.bps_present() == [1, 1, 1]
    spm.enable_spm()

    scopeshot.set_defaults()

    if sa:
        assert (SpmScopeshotInterface.DDR4_STARTING_ADDRESS_DEFAULT <= sa <=
                SpmScopeshotInterface.DDR4_ENDING_ADDRESS_DEFAULT)
        scopeshot.set_ddr4_starting_address(sa)
    sa = scopeshot.ddr4_starting_address()

    if ea:
        assert sa < ea <= SpmScopeshotInterface.DDR4_ENDING_ADDRESS_DEFAULT
        scopeshot.set_ddr4_ending_address(ea)
    ea = scopeshot.ddr4_ending_address()

    if not n:
        n = (ea - sa) // packet_byte_size
    zero_bytes = bytes(n * packet_byte_size)
    hbirctc.dma_write(sa, zero_bytes)
    assert zero_bytes == hbirctc.dma_read(sa, len(zero_bytes))

    scopeshot.enable()

    if dbg_print:
        print(f'Before starting Scopeshot:')
        print(f'\t          Is SPM enabled: {spm.is_spm_enabled()}')
        print(f'\t    Is Scopeshot enabled: {scopeshot.is_enabled()}')
        print(f'\t Scopeshot active status: {scopeshot.is_active()}')
        print(f'\t  Scopeshot sample count: {scopeshot.sample_count()}')
        print(f'\t          Scopeshot mode: {scopeshot.mode()}')
        print(f'\t        starting address: {hex(scopeshot.ddr4_starting_address())}')
        print(f'\t          ending address: {hex(scopeshot.ddr4_ending_address())}')
        print(f'\t       Number of packets: {hex(n)}, 0d{n}')
        print()

    try:
        global addresses
        addresses = []

        scopeshot.assert_start_control()

        address = wait_on_latest_address(sa)
        if sa != address:
            print(f'Failure: Latest address value after starting '
                  f'(expected, actual): {hex(sa)}, {hex(address)}')
            scopeshot.assert_stop_control()
            assert False

        success = True
        for i in range(n):
            next_address = wait_on_latest_address_change(address)
            # print(f'Address (previous, current, diff) : '
            #       f'{hex(next_address)}, {hex(address)},'
            #       f' {hex(abs(next_address - address))}')
            diff = abs(next_address - address)
            if diff != packet_byte_size:
                success = False
                print(f'Failure ({i}): Latest address value did not increment '
                          f'accordingly (addr_1, addr_0, diff): '
                          f'{hex(next_address)}, {hex(address)}, '
                          f'{diff}')
            address = next_address
            # scopeshot.wait_on_packet_refresh()
            # print(hex(ddr4_latest_address()))


            # next_address = wait_on_latest_address_change(address)
            # # addresses.append(next_address)
            # address = next_address
            # if (next_address - address) != packet_byte_size:
            #     print(f'Failure: Latest address value did not increment '
            #           f'accordingly (addr_1, addr_0, diff): '
            #           f'{hex(next_address)}, {hex(address)}, '
            #           f'{abs(next_address - address)}')
            #     break

        scopeshot.assert_stop_control()
        if success:
            out_msg = f'{iteration_num}: Test completed successfully! {n} ' \
                      f'packets sent and latest address incremented ' \
                      f'accordingly!'
        else:
            out_msg = f'{iteration_num}: Test completed failed.'

        [scopeshot.wait_on_packet_refresh() for i in range(10)]
        address = ddr4_latest_address()
        expected = sa + (n * packet_byte_size)
        if address != expected:
            print(f'Failure: Expected latest address (expected, actual): '
                  f'{hex(expected)}, {hex(address)}')

        if dbg_print:
            print('After stopping Scopeshot:')
            print(f'\tScopeshot latest address: {hex(address)}')
            print(f'\t    Is Scopeshot enabled: {scopeshot.is_enabled()}')
            print(f'\t Scopeshot active status: {scopeshot.is_active()}')
            print()

    finally:
        scopeshot.disable()
        spm.disable_spm()

        if dbg_print:
            print('Exiting test:')
            print(f'\t    Is Scopeshot enabled: {scopeshot.is_enabled()}')
            print(f'\t Scopeshot active status: {scopeshot.is_active()}')
            print(f'\t             SPM enabled: {spm.is_spm_enabled()}')
            print()

    return out_msg

addresses = []
def print_addresses():
    print('-------------')
    for address in addresses:
        print(hex(address))
    print()


def ddr4_latest_address():
    return hil.hbiMbBarRead(0, 1, 0xC40) - 0x10


def wait_on_latest_address_change(address, num_retries=100):
    for retry in range(num_retries):
        current_address = ddr4_latest_address()
        if address != current_address:
            # print(retry)
            return current_address
        performance_sleep(10 / 1e6)
    else:
        assert False, f'Failed to obtain a latest address update from ' \
                      f'address {hex(address)}'


def wait_on_latest_address(address, num_retries=100):
    current_address = 0
    for retry in range(num_retries):
        current_address = ddr4_latest_address()

        if address == current_address:
            return current_address
        performance_sleep(10 / 1e6)
    else:
        assert False, f'Failed to obtain latest address (expected, actual): ' \
                      f'{hex(address)}, {hex(current_address)}'


def DirectedCircularBufferWriteReadExhaustiveTest():
    from Common.instruments.tester import get_tester
    from Hbirctc.Tests.SystemPowerMonitoring import SpmScopeshot
    test = SpmScopeshot()
    t = get_tester()
    t.discover_all_instruments()
    test.setUp(tester=t)
    test.test_iterations = 1
    test.DirectedCircularBufferWriteReadExhaustiveTest()
    assert test.fail_count == 0


def DirectedScopeshotStartStopUsingBarRegisterTest():
    from Common.instruments.tester import get_tester
    from Hbirctc.Tests.SystemPowerMonitoring import SpmScopeshot
    test = SpmScopeshot()
    t = get_tester()
    t.discover_all_instruments()
    test.setUp(tester=t)
    test.test_iterations = 1
    test.DirectedScopeshotStartStopUsingBarRegisterTest()
    assert test.fail_count == 0


def DirectedScopeshotEnableAndActiveTest():
    from Common.instruments.tester import get_tester
    from Hbirctc.Tests.SystemPowerMonitoring import SpmScopeshot
    test = SpmScopeshot()
    t = get_tester()
    t.discover_all_instruments()
    test.setUp(tester=t)
    test.test_iterations = 1
    test.DirectedScopeshotEnableAndActiveTest()
    assert test.fail_count == 0


def DirectedScopeshotStopInAlarmModeTest():
    from Common.instruments.tester import get_tester
    from Hbirctc.Tests.SystemPowerMonitoring import SpmScopeshot
    test = SpmScopeshot()
    t = get_tester()
    t.discover_all_instruments()
    test.setUp(tester=t)
    test.test_iterations = 1
    test.DirectedScopeshotStopInAlarmModeTest()
    assert test.fail_count == 0


def DirectedScopeshotSampleCountTest():
    from Common.instruments.tester import get_tester
    from Hbirctc.Tests.SystemPowerMonitoring import SpmScopeshot
    test = SpmScopeshot()
    t = get_tester()
    t.discover_all_instruments()
    test.setUp(tester=t)
    test.test_iterations = 1
    test.DirectedScopeshotSampleCountTest()
    assert test.fail_count == 0

# ----------------------------------------------------------------------------

ConfigLogger()
SetLoggerLevel(logging.INFO)  # Use logging.DEBUG for D, I and W

mb = MainBoard()
hbirctc = Hbirctc(mainboard=mb)

Log('info', f'{hbirctc.name()} FPGA Version '
            f'{hbirctc.fpga_version_string()}')
