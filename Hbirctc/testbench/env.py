# Copyright 2018 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.


from Common import fval


class HbirctcEnv(fval.Env):

    def __init__(self, test, tester):
        super().__init__(test)
        self.hbirctc = tester.get_hbicc_undertest('Hbirctc')
        self.Log('info', 'Creating new Hbirctc test environment instance')