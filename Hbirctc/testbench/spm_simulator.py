# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from math import nan
from random import choice

from Common import hbi_simulator, hilmon
from Common.utilities import compliment_bytes, dword_to_float, float_to_dword
from Hbirctc.instrument import hbirctc_register as register
from Hbirctc.instrument.spm_data_types import SPM_DATA_TYPES

nan_dword = float_to_dword(nan)


class SpmInterface():
    NUM_BPS = 3
    DEFAULT_VOUT = float_to_dword(12.0)
    DEFAULT_IOUT = float_to_dword(20.0)
    DEFAULT_POUT = float_to_dword(12.0 * 20.0)
    DEFAULT_TEMP1 = float_to_dword(27.0)
    DEFAULT_TEMP2 = float_to_dword(52.0)
    DEFAULT_TEMP3 = float_to_dword(33.0)
    DEFAULT_FAN_SPEED_1 = float_to_dword(4000.0)
    DEFAULT_POUT_SUM = DEFAULT_POUT * NUM_BPS

    RELATIVE_TOLERANCE = 0.05  # See SystemPowerMonitoring.SpmData.
                               # RELATIVE_TOLERANCE

    def __init__(self):
        self.write_actions = {
            register.SPM_ALARMS.ADDR: self.clear_alarms,
            register.SPM_CONFIG.ADDR: self.write_config,
            register.SPM_CONTROL.ADDR: self.write_control,
            register.SPM_POUT_LIMIT.ADDR: self.update_pout_limit,
            register.SPM_POUT_SUM_LIMIT.ADDR: self.update_pout_sum_limit,
            register.SPM_POUT_FILTER_COUNT.ADDR: self.bar_write,
            register.SPM_POUT_SUM_FILTER_COUNT.ADDR: self.bar_write}

        self.read_actions = {
            register.SPM_ALARMS.ADDR: self.read_alarms,
            register.SPM_STATUS.ADDR: self.bar_read,
            register.SPM_CONFIG.ADDR: self.bar_read,
            register.SPM_CONTROL.ADDR: self.bar_read,
            register.SPM_POUT_LIMIT.ADDR: self.bar_read,
            register.SPM_POUT_SUM_LIMIT.ADDR: self.bar_read,
            register.SPM_POUT_FILTER_COUNT.ADDR: self.bar_read,
            register.SPM_POUT_SUM_FILTER_COUNT.ADDR: self.bar_read}

        self.registers = {
            register.SPM_CONTROL.ADDR: 0,
            register.SPM_CONFIG.ADDR: 0,
            register.SPM_POUT_LIMIT.ADDR: 0,
            register.SPM_POUT_SUM_LIMIT.ADDR: 0,
            register.SPM_STATUS.ADDR: 0,
            register.SPM_ALARMS.ADDR: 0,
            register.SPM_POUT_FILTER_COUNT.ADDR: 1,
            register.SPM_POUT_SUM_FILTER_COUNT.ADDR: 1
        }
        self.init_data_registers()
        self._alarms_pending = False

    def init_data_registers(self):
        for bps_num in range(SpmInterface.NUM_BPS):
            for data_type in SPM_DATA_TYPES:
                addr = self._get_data_addr(data_type, bps_num)
                self.registers[addr] = nan_dword
                self.read_actions[addr] = self.read_data

    def bar_write(self, offset, data):
        self.registers[offset] = data

    def bar_read(self, offset):
        return self.registers[offset]

    def write_config(self, offset, data):
        self.registers[offset] = data
        sum_total = 0
        is_active = []
        for bps_num in range(SpmInterface.NUM_BPS):
            is_present = (data >> bps_num) & 1 and self._spm_enable()
            is_active.append(is_present)

            for data_type in SPM_DATA_TYPES:
                addr = self._get_data_addr(data_type, bps_num)
                if data_type != 'pout_sum':
                    default_data = getattr(self, f'DEFAULT_{data_type.upper()}')
                    self.registers[addr] = default_data if is_present else \
                        nan_dword
                if data_type == 'pout' and is_present:
                    sum_total += dword_to_float(self.registers[addr])

        self.update_spm_status(is_active)

        sum_addr = self._get_data_addr('pout_sum')
        if sum_total != 0:
            self.registers[sum_addr] = float_to_dword(sum_total)
        else:
            self.registers[sum_addr] = nan_dword

    def _get_data_addr(self, name, bps_num=None):
        name = name.upper()
        if name == 'POUT_SUM':
            return getattr(register,'SPM_POUT_SUM').ADDR
        else:
            return  getattr(register, f'SPM_BPS_{bps_num}_{name}').ADDR

    def update_spm_status(self, is_active):
        reg = register.SPM_STATUS(bps_0_polling_active=is_active[0],
                                  bps_1_polling_active=is_active[1],
                                  bps_2_polling_active=is_active[2])
        self.registers[reg.ADDR] = reg.value

    def read_data(self, offset):
        bps_num = self._bps_num_from_offset(offset)
        if bps_num != -1:
            if not self._spm_enable() or not self._bps_present(bps_num):
                return nan_dword
            else:
                return self.generate_pout_data(offset)
        elif not self._spm_enable() and offset != register.SPM_POUT_SUM.ADDR:
            return nan_dword
        else:
            return self.generate_pout_data(offset)

    def generate_pout_data(self, offset):
        current_data = dword_to_float(self.registers[offset])
        delta = current_data * (SpmInterface.RELATIVE_TOLERANCE / 8)
        adjusted_data = current_data + choice([-1 * delta, delta, 0])
        return float_to_dword(adjusted_data)

    def _bps_num_from_offset(self, offset):
        if register.SPM_BPS_0_POUT.ADDR <= offset <= \
                register.SPM_BPS_0_FAN_SPEED_1.ADDR:
            return 0
        elif register.SPM_BPS_1_POUT.ADDR <= offset <= \
                register.SPM_BPS_1_FAN_SPEED_1.ADDR:
            return 1
        elif register.SPM_BPS_2_POUT.ADDR <= offset <= \
                register.SPM_BPS_2_FAN_SPEED_1.ADDR:
            return 2
        else:
            return -1

    def _spm_enable(self):
        reg = register.SPM_CONTROL(
            value=self.registers[register.SPM_CONTROL.ADDR])
        return reg.spm_enable

    def _bps_present(self, bps_num):
        if 0 > bps_num >= SpmInterface.NUM_BPS:
            raise SpmInterface.InvalidBpsNum()

        reg = register.SPM_CONFIG(
            value=self.registers[register.SPM_CONFIG.ADDR])

        if bps_num == 0:
            return reg.bps_0_present
        else:
            return reg.bps_1_present if bps_num == 1 else reg.bps_2_present

    class InvalidBpsNum(Exception):
        pass

    def write_control(self, offset, data):
        self.registers[offset] = data
        config_addr = register.SPM_CONFIG.ADDR
        self.write_config(config_addr, self.registers[config_addr])

    def read_alarms(self, offset=None):
        self._update_alarm_reg()
        return self.registers[register.SPM_ALARMS.ADDR]

    def _update_alarm_reg(self):
        if self._spm_enable():
            pout_limit = dword_to_float(
                self.registers[register.SPM_POUT_LIMIT.ADDR])
            pout_sum_limit = dword_to_float(
                self.registers[register.SPM_POUT_SUM_LIMIT.ADDR])

            reg = register.SPM_ALARMS(
                value=self.registers[register.SPM_ALARMS.ADDR])
            if self.registers[register.SPM_POUT_FILTER_COUNT.ADDR] <= 1:
                for bps_num in range(SpmInterface.NUM_BPS):
                    pout_address = getattr(register,
                                           f'SPM_BPS_{bps_num}_POUT').ADDR
                    pout_data = dword_to_float(self.registers[pout_address])
                    alarm = 1 if pout_data > pout_limit else 0
                    setattr(reg, f'bps_{bps_num}_spm_pout', alarm)
            if self.registers[register.SPM_POUT_SUM_FILTER_COUNT.ADDR] <= 1:
                pout_sum_address = register.SPM_POUT_SUM.ADDR
                pout_sum_data = dword_to_float(self.registers[pout_sum_address])
                reg.spm_pout_sum = 1 if pout_sum_data > pout_sum_limit else 0
            self.registers[register.SPM_ALARMS.ADDR] = reg.value

            self.decrement_filter_counts()

        self._alarms_pending = self.registers[register.SPM_ALARMS.ADDR] != 0

    def clear_alarms(self, offset, data):
        mask = compliment_bytes(data, num_bytes=4)
        self.registers[offset] = data & mask
        self._alarms_pending = self.registers[register.SPM_ALARMS.ADDR] != 0

    def alarms_pending(self):
        return self._alarms_pending

    def decrement_filter_counts(self):
        pout_addr = register.SPM_POUT_FILTER_COUNT.ADDR
        pout_sum_addr = register.SPM_POUT_SUM_FILTER_COUNT.ADDR

        # Filter Count default values are 1, so reset to 1
        if self.registers[pout_addr] > 1:
            self.registers[pout_addr] -= 1
        else:
            self.registers[pout_addr] = 1
        if self.registers[pout_sum_addr] > 1:
            self.registers[pout_sum_addr] -= 1
        else:
            self.registers[pout_sum_addr] = 1

    def alarms_reg(self):
        return register.SPM_ALARMS(self.registers[register.SPM_ALARMS.ADDR])

    def update_pout_limit(self, offset, data):
        self.registers[offset] = data
        if isinstance(hilmon.hil, hbi_simulator.HbiSimulator):
            simulator = hilmon.hil
            if simulator.rc.spm_scopeshot.in_alarm_mode():
                self._update_alarm_reg()
                reg = self.alarms_reg()
                if reg.bps_0_spm_pout or reg.bps_1_spm_pout or \
                        reg.bps_2_spm_pout:
                    simulator.rc.spm_scopeshot.assert_stop()

    def update_pout_sum_limit(self, offset, data):
        self.registers[offset] = data
        if isinstance(hilmon.hil, hbi_simulator.HbiSimulator):
            simulator = hilmon.hil
            if simulator.rc.spm_scopeshot.in_alarm_mode():
                self._update_alarm_reg()
                reg = self.alarms_reg()
                if reg.spm_pout_sum:
                    simulator = hilmon.hil
                    simulator.rc.spm_scopeshot.assert_stop()
