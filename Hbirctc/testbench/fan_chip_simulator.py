# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval
from Hbirctc.instrument.fan_controller_i2c_interface import MAX_NUM_CHIPS
import Hbirctc.instrument.max6650_register as max6650_register


def create_fan_chips():
    fans = {}
    for channel in MB_FAN_INDEXES:
        fans[channel] = MbFanChip()
    for channel in DPS_FAN_INDEXES:
        fans[channel] = DpsFanChip()
    return fans


class _FanChip(fval.Object):
    def __init__(self):
        super().__init__()
        self.registers = {max6650_register.SPEED_REG.ADDR:
                              max6650_register.SPEED_REG(value=0),
                          max6650_register.CONFIG_REG.ADDR:
                              max6650_register.CONFIG_REG(value=0x0A)}

    def get_rps(self):
        return (self.FAN_MAX_RPS + self.FAN_MIN_RPS) / 2.0

    def read(self, address):
        address = self.trim_address(address)
        return self.registers[address].value

    def write(self, address, data):
        address = self.trim_address(address)
        if address not in READONLY_ADDRESS:
            self.registers[address].value = data & 0xFF

    class AddressError(Exception):
        pass

    def trim_address(self, address):
        # Max address is 0x1F, and divisible by 2. Trim the address accordingly
        address &= 0x1E
        return address


class MbFanChip(_FanChip):
    FAN_MAX_RPS = 60
    FAN_MIN_RPS = 40


class DpsFanChip(_FanChip):
    FAN_MAX_RPS = 210
    FAN_MIN_RPS = 120


MB_FAN_INDEXES = [4, 8, 9, 10, 11]
DPS_FAN_INDEXES = [0, 1, 2, 3, 5, 6, 7]
READONLY_ADDRESS = [0x0A, 0x0C, 0x0E, 0x10, 0x12, 0x1E]

assert len(MB_FAN_INDEXES) + len(DPS_FAN_INDEXES) == MAX_NUM_CHIPS
