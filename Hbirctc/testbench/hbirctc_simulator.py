# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time

from ThirdParty.HPS.hps_diskless_boot import BootHps

from Common import fval
from Common.instruments.s2h_interface import S2hBufferIndex
from Common.testbench import dma_simulator, s2h_simulator
from Common.testbench.i2c_controller_simulator import I2CInterfaceSimulator
from Common.testbench.pmbus_simulator import create_pmbus_devices
from Common.triggers import is_software_trigger
from Hbirctc.instrument import hbirctc_register
from Hbirctc.instrument.bps_i2c_interface import \
    MAX_NUM_INTERFACES as MAX_NUM_BPS_INTERFACES
from Hbirctc.instrument.bps_i2c_interface import I2C_ADDR as BPS_I2C_ADDR
from Hbirctc.instrument.fan_controller_i2c_interface import \
    MAX_NUM_INTERFACES as MAX_NUM_FAN_INTERACES
from Hbirctc.instrument.fan_controller_i2c_interface import \
    I2C_ADDR as FAN_I2C_ADDR
from Hbirctc.instrument.blt_i2c_interface import \
    MAX_NUM_CHIPS as MAX_NUM_PSDB_BLT
from Hbirctc.instrument.blt_i2c_interface import \
    I2C_ADDR as BLT_I2C_ADDR
from Hbirctc.instrument.pmbus_monitor_i2c_interface import \
    MAX_NUM_INTERFACES as MAX_NUM_PMBUS_MONITOR_INTERFACES
from Hbirctc.instrument.pmbus_monitor_i2c_interface import \
    LTM4680_I2C_ADDR as LTM4680_I2C_ADDR
from Hbirctc.testbench import spm_scopeshot_simulator
from Hbirctc.testbench.blt_i2c_simulator import create_blt_i2c_interfaces
from Hbirctc.testbench.fan_chip_simulator import create_fan_chips
from Hbirctc.testbench.max10_simulator import Max10
from Hbirctc.testbench.peci_dac_simulator import PeciDac
from Hbirctc.testbench.spm_simulator import SpmInterface

def simulate_from_file_method_calls(rc):
    rc.hg_id_from_file = lambda: Rc.HG_ID_SIM
    rc.hg_clean_from_file = lambda: Rc.HG_CLEAN_SIM
    rc.fpga_version_from_file = lambda: Rc.FPGA_VERSION_SIM
    rc.compatibility_id_from_file = lambda: Rc.COMPATIBILITY_SIM


class Rc(fval.Object):
    POWER_ON_TIME = 0
    HG_ID_SIM = 0x46D3_9E3FF415
    HG_CLEAN_SIM = 1
    FPGA_VERSION_SIM = 0x0008_0001
    COMPATIBILITY_SIM = 3
    TIME_STAMP_COUNT_STEP = 2 / 1e6

    def __init__(self, system):
        super().__init__()

        self.hbi_simulator = system
        self.max10 = Max10(self)
        self.power_supplies = create_pmbus_devices(MAX_NUM_BPS_INTERFACES)
        self.ltm4680 = create_pmbus_devices(MAX_NUM_PMBUS_MONITOR_INTERFACES)
        self.blt_i2c = create_blt_i2c_interfaces()
        self.fan_chips = create_fan_chips()
        self._software_trigger_fifo = SoftwareTriggerFifo()
        self.s2h_bd = s2h_simulator.S2h(hbirctc_register, S2hBufferIndex.BD)
        self.s2h_spm = s2h_simulator.S2h(hbirctc_register, S2hBufferIndex.SPM)
        self.peci_dac = PeciDac()
        self.spm = SpmInterface()
        self.spm_scopeshot = \
            spm_scopeshot_simulator.SpmScopeshotInterface(self)
        self.is_fail_safe_disable = None
        self._live_time_stamp_counter = Rc.TIME_STAMP_COUNT_STEP
        self._alarm_wait_cancel = False

        self.registers = {
            0x20: Rc.FPGA_VERSION_SIM,
            0x24: Rc.HG_ID_SIM & 0xFFFFFFFF,
            0x28: (Rc.HG_CLEAN_SIM << 31) | (Rc.HG_ID_SIM >> 32) & 0xFFFF,
            0x2C: Rc.COMPATIBILITY_SIM,
            0x30: 0xAAAAAAAA,
            0x34: 0xAAAAAAAA,
            0x3C: 0x5,
            0x44: 0XFFFFFFFF,
            0x48: 0x10000,
            0x304: 0x27,
            0x324: 0x27,
            0x364: 0,
            0x360: 0x10F,
            0x36C: 0,
            0x384: 0x6,
            0x6E8: 0,
            0xA04: 0x1D11,
            0xA54: 0x1,
            0xB04: 360,
            0x4000: BootHps.HPS2SYS_SYNC_0}
        self._initialize_clock_frequency_measurement_registers()

        self.write_actions = {
            0x80: self.write_trigger_sync,
            0xA20: self._write_software_trigger_control,
            0x310: self.patgen_trigger_write,
            0x330: self.ring_multiplier_trigger_write,
            0x380: self.reset_ddr,
            0xB00: self.write_failsafe_control,
            0x4000: self.write_hps_msg_mailbox
        }
        self.read_actions = {
            0x364: self.update_debug_register_input_field,
            0x378: self.power_on_count_read,
            0x6E8: self.read_global_alarms,
            0xA24: self._read_sw_trigger_fifo_count,
            0xA28: self._read_software_trigger_fifo_data,
            0xA2C: self._read_software_trigger_fifo_source,
            0xA80: self.read_live_time_stamp_us
        }

        self._setup_fan_controller_interfaces()
        self._setup_bps_interfaces()
        self._setup_ltm4680_interfaces()
        self._setup_blt_i2c_interfaces()
        self._setup_s2h()
        self._setup_max10()
        self._setup_peci_dac()
        self._setup_spm()
        self._setup_interrupts()
        self._setup_trigger_actions()
        self._setup_software_trigger_interface()
        self._setup_dma()
        self._setup_spm_scopeshot()

    def _setup_blt_i2c_interfaces(self):
        self.blt_i2c_interfaces = [I2CInterfaceSimulator(
            i2c_addr=BLT_I2C_ADDR,
            base_offset=hbirctc_register.BLT_I2C_STATUS.address(i),
            write_cmd=self.blt_i2c[i].write,
            read_cmd=self.blt_i2c[i].read) for i in range(MAX_NUM_PSDB_BLT)]
        self._setup_blt_i2c_write_actions()
        self._setup_blt_i2c_read_actions()

    def _setup_blt_i2c_write_actions(self):
        for i in range(MAX_NUM_PSDB_BLT):
            self.write_actions[0x704 + (i * 0x20)] = self.blt_i2c_interfaces[
                i].transmit
            self.write_actions[0x708 + (i * 0x20)] = self.blt_i2c_interfaces[
                i].push_data
            self.write_actions[0x710 + (i * 0x20)] = self.blt_i2c_interfaces[
                i].reset

    def _setup_blt_i2c_read_actions(self):
        for i in range(MAX_NUM_PSDB_BLT):
            self.read_actions[0x700 + (i * 0x20)] = self.blt_i2c_interfaces[
                i].update_status_reg
            self.read_actions[0x70C + (i * 0x20)] = self.blt_i2c_interfaces[
                i].pop_data

    def _setup_fan_controller_interfaces(self):
        self.fan_controller_i2c_interfaces = [I2CInterfaceSimulator(
            i2c_addr=FAN_I2C_ADDR,
            base_offset=hbirctc_register.FAN_CONTROLLER_I2C_STATUS.address(i),
            write_cmd=self.fan_chips[i].write,
            read_cmd=self.fan_chips[i].read) for i in range(
            MAX_NUM_FAN_INTERACES)]
        self._setup_fan_controller_write_actions()
        self._setup_fan_controller_read_actions()

    def _setup_fan_controller_write_actions(self):
        for i in range(MAX_NUM_FAN_INTERACES):
            self.write_actions[0x604 + (i * 0x20)] = \
                self.fan_controller_i2c_interfaces[i].transmit
            self.write_actions[0x608 + (i * 0x20)] = \
                self.fan_controller_i2c_interfaces[i].push_data
            self.write_actions[0x610 + (i * 0x20)] = \
                self.fan_controller_i2c_interfaces[i].reset

    def _setup_fan_controller_read_actions(self):
        for i in range(MAX_NUM_FAN_INTERACES):
            self.read_actions[0x600 + (i * 0x20)] = \
                self.fan_controller_i2c_interfaces[i].update_status_reg
            self.read_actions[0x60C + (i * 0x20)] = \
                self.fan_controller_i2c_interfaces[i].pop_data

    def _setup_bps_interfaces(self):
        self.bps_interfaces = [I2CInterfaceSimulator(
            i2c_addr=BPS_I2C_ADDR,
            base_offset=hbirctc_register.BPS_I2C_STATUS.address(i),
            write_cmd=self.power_supplies[i].pmbus_write,
            read_cmd=self.power_supplies[i].pmbus_read) for i in range(
            MAX_NUM_BPS_INTERFACES)]
        self._setup_bps_write_actions()
        self._setup_bps_read_actions()

    def _setup_bps_write_actions(self):
        for i in range(MAX_NUM_BPS_INTERFACES):
            self.write_actions[0x664 + (i * 0x20)] = self.bps_interfaces[
                i].transmit
            self.write_actions[0x668 + (i * 0x20)] = self.bps_interfaces[
                i].push_data
            self.write_actions[0x670 + (i * 0x20)] = self.bps_interfaces[
                i].reset

    def _setup_bps_read_actions(self):
        for i in range(MAX_NUM_BPS_INTERFACES):
            self.read_actions[0x660 + (i * 0x20)] = self.bps_interfaces[
                i].update_status_reg
            self.read_actions[0x66C + (i * 0x20)] = self.bps_interfaces[
                i].pop_data

    def _setup_ltm4680_interfaces(self):
        self.ltm4680_interfaces = [I2CInterfaceSimulator(
            i2c_addr=LTM4680_I2C_ADDR,
            base_offset=hbirctc_register.PMBUS_MONITOR_I2C_STATUS.address(i),
            write_cmd=self.ltm4680[i].pmbus_write,
            read_cmd=self.ltm4680[i].pmbus_read) for i in range(
            MAX_NUM_PMBUS_MONITOR_INTERFACES)]
        self._setup_ltm4680_write_actions()
        self._setup_ltm4680_read_actions()

    def _setup_ltm4680_write_actions(self):
        for i in range(MAX_NUM_PMBUS_MONITOR_INTERFACES):
            self.write_actions[0xB24 + (i * 0x20)] = self.ltm4680_interfaces[
                i].transmit
            self.write_actions[0xB28 + (i * 0x20)] = self.ltm4680_interfaces[
                i].push_data
            self.write_actions[0xB30 + (i * 0x20)] = self.ltm4680_interfaces[
                i].reset

    def _setup_ltm4680_read_actions(self):
        for i in range(MAX_NUM_PMBUS_MONITOR_INTERFACES):
            self.read_actions[0xB20 + (i * 0x20)] = self.ltm4680_interfaces[
                i].update_status_reg
            self.read_actions[0xB2C + (i * 0x20)] = self.ltm4680_interfaces[
                i].pop_data

    def _setup_s2h(self):
        self.read_actions.update(self.s2h_bd.read_actions)
        self.write_actions.update(self.s2h_bd.write_actions)
        self.read_actions.update(self.s2h_spm.read_actions)
        self.write_actions.update(self.s2h_spm.write_actions)

    def _setup_max10(self):
        self.write_actions.update({0x40: self.max10.write_command_data,
                                   0x4C: self.max10.write_control})
        self.read_actions.update({0x44: self.max10.read_data,
                                  0x48: self.max10.cpld_status})

    def _setup_peci_dac(self):
        self.read_actions.update(self.peci_dac.read_actions)
        self.write_actions.update(self.peci_dac.write_actions)
		
    def _setup_spm(self):
        self.write_actions.update(self.spm.write_actions)
        self.read_actions.update(self.spm.read_actions)

    def _setup_interrupts(self):
        self.registers[0x370] = 0
        self.registers[0x374] = 0
        self.read_actions[0x370] = self.read_interrupt_status

    def _setup_trigger_actions(self):
        num_links = hbirctc_register.AURORA_TRIGGER_DOWN.REGCOUNT

        # actions = {hbirctc_register.AURORA_TRIGGER_DOWN.address(link):
        #                self.write_trigger_down_by_offset
        #            for link in range(num_links)}
        # self.write_actions.update(actions)
        #
        # actions = {hbirctc_register.AURORA_TRIGGER_DOWN.address(link):
        #                self.read_trigger_down_by_offset
        #            for link in range(num_links)}
        # self.read_actions.update(actions)
        #
        # actions = {hbirctc_register.AURORA_TRIGGER_UP.address(link):
        #                self.write_trigger_up_by_offset
        #            for link in range(num_links)}
        # self.write_actions.update(actions)
        #
        # actions = {hbirctc_register.AURORA_TRIGGER_UP.address(link):
        #                self.read_trigger_up_by_offset
        #            for link in range(num_links)}
        # self.read_actions.update(actions)
        #
        # actions = {hbirctc_register.AURORA_CONTROL.address(link):
        #                self.write_aurora_control
        #            for link in range(num_links)}
        # self.write_actions.update(actions)

        self.dps_down_trigger_buffer = {i: [] for i in range(num_links)}

        self.write_actions.update({0xE0: self.send_broadcast_trigger_down})
        self.read_actions.update({0xE4: self.read_last_broadcasted_trigger})

    def _setup_software_trigger_interface(self):
        self.sw_trigger_fifo = []
        self.sw_trigger_source = []
        # self.read_actions.update(
        #     {0xA28: self.read_sw_trigger_data,
        #      0xA2C: self.read_sw_trigger_source,
        #      0xA24: self.read_sw_trigger_count})
        # self.write_actions.update(
        #     {0xA20: self.write_sw_trigger_control})

    def _setup_dma(self):
        self.dma = dma_simulator.DmaSimulator()
        self.dma_write = self.dma.dma_write
        self.dma_read = self.dma.dma_read

    def _setup_spm_scopeshot(self):
        self.registers.update(self.spm_scopeshot.registers)
        self.read_actions.update(self.spm_scopeshot.read_actions)
        self.write_actions.update(self.spm_scopeshot.write_actions)

    def bar_write(self, bar, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def bar_read(self, bar, offset):
        data =  self.read_actions.get(offset, self.default_read_action)(offset)
        return data

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def default_read_action(self, offset):
        if offset not in self.registers:
            self.registers[offset] = 0x0
        return self.registers[offset]

    def patgen_trigger_write(self, offset, data):
        self.hbi_simulator.patgen.trigger_down(data)

    def ring_multiplier_trigger_write(self, offset, data):
        self.hbi_simulator.ring_multiplier.trigger(data)

    def write_hps_msg_mailbox(self, offset, data):
        if data == BootHps.SYS2HPS_SYNC_1:
            self.registers[offset] = BootHps.HPS2SYS_UBOOT_WAIT
        elif data == BootHps.SYS2HPS_UBOOT_DONE:
            self.registers[offset] = BootHps.HPS2SYS_LINUX_WAIT
        else:
            self.registers[offset] = data

    def fpga_version(self):
        return self.registers[0x20]

    def fpga_version_string(self):
        version = self.fpga_version()
        return f'{version >> 16}.{version & 0xFFFF}'

    def fan_rps(self, chip_num):
        return self.fan_chips[chip_num].get_rps()

    def trigger_up(self, trigger, link):
        self._update_trigger_up_register(link, trigger)
        if self._is_software_trigger(trigger):
            self._software_trigger_fifo.push(trigger, link)

    def _is_software_trigger(self, trigger):
        return trigger & 0x00080000

    def _update_trigger_up_register(self, link, trigger):
        debug_trigger_up_offset = 0x10c + link * 32
        self.registers[debug_trigger_up_offset] = trigger

    def _read_sw_trigger_fifo_count(self, offset):
        self.registers[offset] = self._software_trigger_fifo.count()
        return self.registers[offset]

    def _read_software_trigger_fifo_data(self, offset):
        self.registers[offset] = self._software_trigger_fifo.pop()
        return self.registers[offset]

    def _read_software_trigger_fifo_source(self, offset):
        self.registers[offset] = self._software_trigger_fifo.source()
        return self.registers[offset]

    def _write_software_trigger_control(self, offset, data):
        self._software_trigger_fifo.reset_state(data & 1)

    def update_debug_register_input_field(self, offset):
        reg = hbirctc_register.FPGA_DEBUG(
            value=self.registers[hbirctc_register.FPGA_DEBUG.ADDR])
        control_binary_string = f'{reg.control:08b}'
        output_binary_string = f'{reg.output:08b}'

        temp = ''
        for i in range(len(control_binary_string)):
            if control_binary_string[i] == '1':  # output direction
                temp += output_binary_string[i]
            else:  # input direction
                temp += '1'

        reg.input = int(temp, 2)
        self.registers[hbirctc_register.FPGA_DEBUG.ADDR] = reg.value
        return reg.value

    def _initialize_clock_frequency_measurement_registers(self):
        base_offset = hbirctc_register.CLOCK_FREQUENCY_MEASUREMENT.ADDR

        self.registers[base_offset + 0x00] = 0x1fffcc5
        self.registers[base_offset + 0x04] = 0x1fffff6
        self.registers[base_offset + 0x08] = 0x1fff444
        self.registers[base_offset + 0x0C] = 0x1fff444
        self.registers[base_offset + 0x10] = 0x1ffffff
        self.registers[base_offset + 0x14] = 0x1ffffff
        self.registers[base_offset + 0x18] = 0x1ffffff
        self.registers[base_offset + 0x1C] = 0x1ffffff

    def reset_ddr(self, offset, data):
        if data == 1:
            self.dma.reset_ddr()

    def write_failsafe_control(self, offset, data):
        self.registers[offset] = data

    def hbirctc_fpga_load(self, image=None):
        Rc.POWER_ON_TIME = time.time()

    def power_on_count_read(self, offset):
        current_time = time.time()
        total_time_seconds = current_time - Rc.POWER_ON_TIME
        number_of_10ms_increments = int(total_time_seconds / 0.01)
        return number_of_10ms_increments

    def read_live_time_stamp_us(self, offset=0):
        second_to_microsecond_factor = 1e6
        self._live_time_stamp_counter += Rc.TIME_STAMP_COUNT_STEP
        converted_time = int(self._live_time_stamp_counter *
                             second_to_microsecond_factor)
        return converted_time

    def reset_live_time_stamp(self):
        self._live_time_stamp_counter = Rc.TIME_STAMP_COUNT_STEP

    def write_trigger_sync(self, offset, data):
        if data & 0x7FFF_FFFF:
            num_dps_links = 16
            for i in range(num_dps_links):
                if (data >> i) & 1:
                    try:
                        hbidps = self.get_dps_sim(i)
                    except Rc.GetInstrumentError:
                        pass
                    else:
                        hbidps.receive_sync_pulse()
            for name in ['patgen', 'ring_multiplier']:
                instrument = getattr(self.hbi_simulator, name)
                instrument.receive_sync_pulse()

        time_stamp_reset_bit = (data >> 31) & 1
        if time_stamp_reset_bit:
            self.reset_live_time_stamp()


    def get_dps_sim(self, slot):
        for key, instrument in self.hbi_simulator.dps.items():
            if instrument.slot == slot:
                return instrument
        else:
            raise Rc.GetInstrumentError(f'No instrument found at slot {slot}')

    class GetInstrumentError(Exception):
        pass

    def send_broadcast_trigger_down(self, offset, data):
        self.registers[offset] = data

        # Update LAST_BROADCASTED_TRIGGER register
        self.registers[hbirctc_register.LAST_BROADCASTED_TRIGGER.ADDR] = data

        # Update individual trigger down links so that sim instruments can read
        # it
        for slot in range(hbirctc_register.AURORA_TRIGGER_DOWN.REGCOUNT):
            self.write_trigger_down(slot, data)

        # # process if trigger is a Star/Stop scopeshot event
        # self.scopeshot_engine.process_trigger(link_num=None, trigger=data)

    def write_trigger_down(self, slot, trigger):
        offset = hbirctc_register.AURORA_TRIGGER_DOWN.address(slot)
        self.write_trigger_down_by_offset(offset, trigger)

    def write_trigger_down_by_offset(self, offset, data):
        self.registers[offset] = data

        if not is_software_trigger(data):
            self.write_trigger_up_by_offset(offset - 4, data)

    def write_trigger_up_by_offset(self, offset, data):
        self.registers[offset] = data
        dps_fifo_depth = 256  # HDDPS HLD V0.15 "Trigger Down"

        # If software data, update software trigger fifo
        if is_software_trigger(data):
            self.update_software_trigger_fifo(offset, data)
        else:  # if not software data, redirect to down trigger for broadcast
            self.registers[offset + 4] = data
            slot = hbirctc_register.AURORA_TRIGGER_DOWN.index(offset + 4)
            if len(self.dps_down_trigger_buffer[slot]) < dps_fifo_depth:
                self.dps_down_trigger_buffer[slot].append(data)
            self.registers[0xE4] = data  # last broadcast trigger register

    def update_software_trigger_fifo(self, offset, trigger):
        fifo_depth = 1024
        if len(self.sw_trigger_fifo) < fifo_depth:
            slot = hbirctc_register.AURORA_TRIGGER_UP.index(offset)
            self.sw_trigger_fifo.append(trigger)
            self.sw_trigger_source.append(slot)

    def read_last_broadcasted_trigger(self, offset):
        return self.registers[0xE4]

    def read_global_alarms(self, offset):
        reg = hbirctc_register.GLOBAL_ALARMS(value=self.registers[0x6E8])
        reg.active_spm_alarm = self.spm.read_alarms() != 0
        return reg.value

    def alarm_wait(self):
        self._alarm_wait_cancel = False

        while True:
            is_enabled = hbirctc_register.INTERRUPT_CONTROL(
                value=self.registers[0x374]).spm_interrupt_enable
            if self._alarm_wait_cancel or not is_enabled:
                raise RuntimeError()

            spm_alarm = self.spm.read_alarms()
            if spm_alarm > 0:
                return spm_alarm

    def alarm_wait_cancel(self):
        self._alarm_wait_cancel = True

    def read_interrupt_status(self, offset):
        reg = hbirctc_register.INTERRUPT_STATUS(value=self.registers[0x370])
        reg.spm_interrupt = int(self.spm.alarms_pending())
        return reg.value


class SoftwareTriggerFifo():
    MAX_SIZE = 1024

    def __init__(self):
        self._data = []
        self._link = []
        self._last_data = 0
        self._last_link = 0
        self._reset = False

    def push(self, trigger, link):
        if self._reset:
            pass
        elif len(self._data) < SoftwareTriggerFifo.MAX_SIZE:
            self._data.append(trigger)
            self._link.append(link)

    def pop(self):
        try:
            trigger = self._data.pop(0)
            _ = self._link.pop(0)
        except IndexError:
            trigger = self._last_data
            self._last_data = trigger
        return trigger

    def reset_state(self, state):
        self._data = []
        self._link = []
        self._reset = bool(state)

    def count(self):
        return len(self._data)

    def source(self):
        try:
            s = self._link[0]
        except IndexError:
            s = self._last_link
            self._last_link = s
        return s
