# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval
from Hbirctc.instrument.hbirctc_register import MAX10_CPLD_CONTROL, \
    MAX10_CPLD_COMMAND_DATA, MAX10_CPLD_READ_DATA, MAX10_CPLD_STATUS
import Hbirctc.instrument.max10_register as max10_register


class Max10(fval.Object):
    def __init__(self, rc):
        self.rc = rc
        self.registers = {max10_register.RCTC_SCRATCH_PAD.ADDR: 0x0000,
                          max10_register.RCTC_FW_REV_MAJ.ADDR: 0x0030,
                          max10_register.RCTC_FW_REV_MINOR.ADDR: 0x0050,
                          max10_register.RCTC_FW_REV_PCB.ADDR: 0x0000,
                          max10_register.RCTC_INT_LINES.ADDR: 0x0000,
                          max10_register.RCTC_IO_STATUS.ADDR: 0x0000,
                          max10_register.RCTC_BPS_MASK_SHUTDOWN_FABC.ADDR: 0}
        self.write_actions = {
            max10_register.RCTC_INT_LINES.ADDR: self.update_interrupt,
            max10_register.RCTC_FW_REV_MAJ.ADDR: self.bypass_read_only_reg,
            max10_register.RCTC_FW_REV_MINOR.ADDR: self.bypass_read_only_reg,
            max10_register.RCTC_FW_REV_PCB.ADDR: self.bypass_read_only_reg}
        self.read_actions = {
            max10_register.RCTC_IO_STATUS.ADDR: self.read_status}
        self.interrupt = False
        self._cpld_control = MAX10_CPLD_CONTROL()
        self._cpld_command = MAX10_CPLD_COMMAND_DATA()
        self._cpld_read = MAX10_CPLD_READ_DATA()
        self._cpld_status = MAX10_CPLD_STATUS()

    def read_word(self, offset):
        if offset not in self.registers.keys():
            self.Log('warning', f'Address {offset:X} is currently not '
                                f'supported in simulation.')
        data = self.read_actions.get(offset, self.default_read_action)(offset)
        return data

    def write_word(self, offset, data):
        if offset not in self.registers.keys():
            self.Log('warning', f'Address {offset:X} is currently not '
                                f'supported in simulation.')
        else:
            self.write_actions.get(offset,
                                   self.default_write_action)(offset, data)

    def default_read_action(self, offset):
        if offset not in self.registers:
            self.registers[offset] = 0x0
        return self.registers[offset]

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def update_interrupt(self, offset, data):
        self.registers[offset] = data
        reg = max10_register.RCTC_INT_LINES(value=data)
        if reg.debug_drive_INT0 and reg.debug_set_INT0:
            reg.INT0 = 1
            self.interrupt = True
        else:
            reg.INT0 = 0
            self.interrupt = False

    def bypass_read_only_reg(self, offset, data):
        pass

    def read_status(self, offset):
        reg = max10_register.RCTC_IO_STATUS(
            sys_fail_safe=not self.rc.is_fail_safe_disable)
        return reg.value

    def write_command_data(self, offset, data):
        self._cpld_command.value = data
        if self._cpld_command.read_write:
            self._cpld_status.rx_fifo_count += 1
        else:
            self._cpld_status.tx_fifo_count += 1

    def write_control(self, offset, data):
        self._cpld_control.value = data
        if self._cpld_control.controller_and_fifo_reset:
            self._cpld_status.value = 0

    def read_data(self, offset):
        return 0

    def cpld_status(self, offset):
        self._cpld_status.interrupt = self.interrupt
        return self._cpld_status.value
