# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval
from Hbirctc.instrument.blt_i2c_interface import MAX_NUM_CHIPS

def create_blt_i2c_interfaces():
    return [BltI2cInterface(index) for index in range(MAX_NUM_CHIPS)]


class BltI2cInterface(fval.Object):
    def __init__(self, index):
        super().__init__()
        self.registers = {}

    def read(self, address):
        register = self.registers.get(address, [])
        if not register:
            return 0
        else:
            return register.pop(0)

    def write(self, address, data):
        if address not in self.registers.keys():
            self.registers[address] = []
        self.registers[address].append(data)
