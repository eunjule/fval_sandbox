# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Hbirctc.instrument.hbirctc_register import PECI_DAC_COMMAND_DATA, \
    PECI_DAC_STATUS
from Hbirctc.instrument.peci_dac import PECI_DAC_CHANNELS, PeciDacCommands


class PeciDac(Object):
    def __init__(self):
        super().__init__()

        self.read_actions = {PECI_DAC_STATUS.ADDR: self.read_status}
        self.write_actions = {PECI_DAC_COMMAND_DATA.ADDR: self.send_command}

        self.status = PECI_DAC_STATUS()
        self.command_data = PECI_DAC_COMMAND_DATA()

        self.dac_output = {i:0 for i in PECI_DAC_CHANNELS}

    def read_status(self, offset=None):
        return self.status.value

    def send_command(self, offset=None, data=0):
        self.command_data.value = data

        command = self.command_data.command
        channel = self.command_data.channel
        data = self.command_data.data
        if command == PeciDacCommands.update_dac.value:
            self.dac_output[channel] = data
        elif command == PeciDacCommands.read_back.value:
            dont_care_bits = 4
            self.status.data = self.dac_output[channel] << dont_care_bits
