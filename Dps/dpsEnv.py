################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random

from _build.bin import fvalc
from _build.bin import hddpstbc
from _build.bin import rctbc
from Common import fval
from Common.instruments.tester import get_tester
import unittest
import os
import json


class LogProxy(hddpstbc.Logger):
    def __init__(self):
        super().__init__()

    def Log(self, file, line, function, verbosity, message):
        fval.Log(verbosity, message)


logProxy = LogProxy()
hddpstbc.SetCommonLogger(logProxy)
rctbc.SetCommonLogger(logProxy)


class DpsEnv(fval.Env):
    def __init__(self, test):
        super().__init__(test)
        self.duts = []
        self.fpgas = []
        self.instruments = []
        self.tester = get_tester()
        self.under_test_dps_devices = self.tester.get_undertest_dps_instruments()
        self.duts = self.get_duts()
        self.fpgas = self.get_fpgas()
        self.instruments = self.get_instruments_dictionary()
        # FIXME: Workaround
        self.root_path = os.path.dirname(__file__)
        fvalc.SetSeed(random.randint(0, 1 << 64 - 1))
        for slot, instance in self.instruments.items():
            if instance.__class__.__name__.lower() == 'hddps':
                self.setAD5764_AD5560_DefaultRegisterValues()

    def duts_or_skip_if_no_vaild_rail(self,railTypesToTest):
        reason = 'skipping test, the requested rails: {} not available'.format(railTypesToTest)
        dutsToTest = []
        for dut in self.duts:
            for railType in dut.railTypes:
                if railType in railTypesToTest:
                    dutsToTest.append(dut)
        dutsToTest = list(set(dutsToTest))
        if(len(dutsToTest) == 0):
            self.Log('warning',reason)
            raise fval.SkipTest(reason)
        return dutsToTest

    def get_instruments_dictionary(self):
        instruments = {}
        for dps in self.under_test_dps_devices:
            instruments[dps.slot] = dps
        return instruments

    def get_fpgas(self):
        fpgas = []
        for dps in self.under_test_dps_devices:
            for subslot in dps.subslots:
                fpgas.append((dps.slot,subslot.subslot))
        return fpgas

    def get_duts(self):
        duts = []
        for dps in self.under_test_dps_devices:
            for subslot in dps.subslots:
                duts.append(subslot)
        return duts


    def send_trigger(self, triggerValue):
        self.tester.rc.send_trigger(triggerValue)

    def RcBarRead(self,bar,address):
        return self.tester.rc.BarRead(bar,address)

    def read_trigger_up(self,slot):
        return self.tester.rc.read_trigger_up(slot)

    def setAD5764_AD5560_DefaultRegisterValues(self):
        registervalues = json.load(open(os.path.join(self.root_path, 'AD5764_AD5560_DefaultRegisterValues.json')))
        for rail_type in ['HC', 'LC']:
            self.Log('debug', 'Setting Default AD5560/AD5764 Values for {} rail.'.format(rail_type))
            for board in self.duts_or_skip_if_no_vaild_rail(rail_type):
                for default in registervalues[str(board.subslot)]["AD5764"]:
                    index, reg, dac, data = default
                    board.WritetoAd5764(index, reg, dac, data)
                    self.Log('debug', 'index {} , reg {} , dac {}  , data {}'.format(index, reg, dac, data))
                for default in registervalues[str(board.subslot)]["AD5560"]:
                    address, data, rail = default
                    board.WriteAd5560Register(address, data, rail)
                    self.Log('debug', 'address {}, data {}, rail {}'.format(address, data, rail))
