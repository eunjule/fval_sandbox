////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: samplechecker.cc
//------------------------------------------------------------------------------
//    Purpose: HDDPS SAmple Data Checker
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/20/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>     // std::vector

#include "samplechecker.h"
#include "defines.h"
#include "hddpstbc.h"
#include "logging.h"

#define MAX_ERROR 0.2

namespace hddpstbc {

SampleChecker::SampleChecker(size_t maxErrors)
{
    this->maxErrors = maxErrors;
    Clear();
}

SampleChecker::~SampleChecker()
{
    if (disabledRules.size() > 0) {
        std::stringstream ss;
        ss << "SampleChecker disabled rules: ";
        for (size_t i = 0; i < disabledRules.size(); i++) {
            ss << disabledRules[i] << " ";
        }
        LOG("debug") << ss.str();
    }
}

bool SampleChecker::DisableRule(const std::string& id)
{
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter == disabledRules.end()) {
        disabledRules.push_back(id);
        return true;
    } else {
        return false;
    }
}

bool SampleChecker::EnableRule(const std::string& id)
{
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter != disabledRules.end()) {
        disabledRules.erase(iter);
        return true;
    } else {
        return false;
    }
}

bool SampleChecker::LogError(const std::string& id, const std::string& s)
{
    totalErrorsFound++;
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter != disabledRules.end()) {
        // The rule is disabled
        disabledErrorsFound++;
        return false;
    } else {
        // The rule is enabled
        enabledErrorsFound++;
        bool logError = true;
        if (maxErrors > 0) {  // 0 means log all errors
            if (enabledErrorsFound > maxErrors) {
                logError = false;
            }
        }
        if (logError) {
            // LOG("error") << "[" << id << "] " << s;
            LOG("error") << s;
        }
        if (enabledErrorsFound == maxErrors) {
            LOG("error") << "Logged max capture data errors (maxErrors=" << maxErrors << "). Additional errors will not be logged.";
        }
        return logError;
    }
}

bool SampleChecker::LogInfo(const std::string& id, const std::string& s)
{
    totalErrorsFound++;
    auto iter = std::find(disabledRules.begin(), disabledRules.end(), id);
    if (iter != disabledRules.end()) {
        // The rule is disabled
        disabledErrorsFound++;
        return false;
    } else {
        // The rule is enabled
        enabledInfoFound++;
        bool logInfo = true;
        if (maxErrors > 0) {  // 0 means log all errors
            if (enabledInfoFound > maxErrors) {
                logInfo = false;
            }
        }
        if (logInfo) {
            // LOG("error") << "[" << id << "] " << s;
            LOG("info") << s;
        }
        if (enabledInfoFound == maxErrors) {
            LOG("info") << "Logged max capture data info (maxErrors=" << maxErrors << "). Additional errors will not be logged.";
        }
        return logInfo;
    }
}

void SampleChecker::Clear()
{
    enabledErrorsFound = 0;
    enabledInfoFound = 0;
    disabledErrorsFound = 0;
    totalErrorsFound = 0;
}


void SampleChecker::Run(SampleCheckerContext* context, HddpsSimulator* sim)
{
    // Run the simulator until it is done
    sim->Run();
    //bool result = true;
    //while (result) {
    //    result = sim->StepTriggerQueueProcessor();
    //}

    std::ofstream railLog;
    railLog.open( "railLog.csv" );
    railLog << "Cycle, Sample Time, Rail Number, Expected, Actual, Voltage/Current,\n";
    for (auto it = context->regions.begin(); it != context->regions.end(); it++) {
        int dutid = it->first;
        SampleEngineRegion* region = it->second;
        if (region->HasData()) {
            LOG("info") << "dut domain id is " << dutid;
            // Get actual sample region size
            size_t actualSampleSizeInByte = region->actualSampleRegionSize;
            // Raise sample engine related alarms
            // Sample engine related Fpga alarms raise exception preventing any simulator run
            // However, it's here in case that user uses the wrong sample registers.
            sim->SetSampleRegionSize(dutid, actualSampleSizeInByte);
            // Get expected samples from simulator
            std::vector<float>& expectedSamples = *(sim->GetSampleData(dutid));
            // Get rail vector corresponding to Dut Id or Uhc Rails
            std::unordered_map<size_t, std::vector<size_t>> uhcRails = sim->GetUhcRails(dutid); 
            // Get Sample Counts vector for multirun in the same uhc sample engine
            std::vector<uint16_t> sampleCounts = sim->GetSampleCounts(dutid);
            // Get Valid Cycle number to avoid comparision when rail is folded.
            std::unordered_map<size_t, uint64_t> validCycles = sim->GetValidCycle(dutid);
            // Actual sample count is the length of sample data region divided by 4 (1 samples = 1 dword = 4 bytes)
            size_t fpgaSampleCount = ( (region->sampleLength / 4) );
            LOG("info") << "Got " << fpgaSampleCount << " actual samples from Fpga";
            LOG("info") << "Got " << expectedSamples.size() << " expected samples from simulator";
            // Init the sample start count for multiple run in the same uhc or dutid sample engine
            // iStart is the first sample of each run under same uhc sample engine
            //size_t iStart = 0;
            // r is multi-run count in the same uhc sample engine
            //for (size_t r = 0; r <  sampleCounts.size(); r++) {
                // (sample count * number of rails * 2 ) per a run for multi-runs of the same uhc sample engine
                //size_t sampleCountsWithRails = sampleCounts[r]*uhcRails[r].size()*2; // 2 includes both voltage/current
                // for (size_t i = iStart; i < std::min(fpgaSampleCount, iStart + sampleCountsWithRails); i++) {

                    // //////////////////////////////////////////////////
                    // // Logging either error or actual output for debug
                    // //////////////////////////////////////////////////
                    // std::stringstream ss;
                    // size_t moduloDivider = (uhcRails[r].size()*2); // 2 is coming from considering both voltage and current
                    // size_t modulus = i%moduloDivider;   // Getting modulus 
                    // size_t railModulus = modulus%2;     // Getting either 0 or 1 for railIdx calculation
                    // size_t railIdx;
                    // if (railModulus == 1) {
                        // railIdx = ( (modulus-1)/2 );    // if railModulus equal to 1, then subtract 1 to get number that can be divided by 2 for railIdx.
                    // } else {
                        // railIdx = (modulus/2);
                    // }

                // } // inner For Loop
                // Adding the sample Count for the next sample engine run start
                //iStart = sampleCountsWithRails;
            //} // Sample count For loop
        } // outermost if 
    }
}

}  // namespace hddpstbc

