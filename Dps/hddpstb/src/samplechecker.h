////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: samplechecker.h
//------------------------------------------------------------------------------
//    Purpose: HDDPS Sample Data Checker
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 04/20/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __SAMPLE_CHECKER_H__
#define __SAMPLE_CHECKER_H__

#include <unordered_map>

#include "hddpssim.h"
#include "defines.h"
#include "hddpstbc.h"
#include "logging.h"

namespace hddpstbc {

class SampleEngineRegion
{
public:
    SampleEngineRegion()
    {
        headerArray = nullptr;
        headerLength = 0;
        sampleArray = nullptr;
        sampleLength = 0;
        actualSampleRegionSize = 0;

    }
    virtual ~SampleEngineRegion() {};
    void SetHeader(uint8_t* array, size_t length)
    {
        headerArray = array;
        headerLength = length;
    }
    void SetData(uint8_t* array, size_t length)
    {
        sampleArray = array;
        sampleLength = length;
    }
    bool HasData()
    {
        return headerLength > 0 && sampleLength > 0;
    }
    void SetSize( size_t length)
    {
        actualSampleRegionSize = length;
    }
    uint8_t* headerArray;
    size_t headerLength;
    uint8_t* sampleArray;
    size_t sampleLength;
    size_t actualSampleRegionSize;
};

class SampleCheckerContext
{
public:
    SampleCheckerContext() {};
    virtual ~SampleCheckerContext() {};
    virtual void AddRegion(int dutid, SampleEngineRegion* region)
    {
        regions[dutid] = region;
    }
    std::unordered_map<int, SampleEngineRegion*> regions;
};

class SampleChecker
{
public:
    SampleChecker(size_t maxErrors);  // maxErrors == 0 means log all errors
    virtual ~SampleChecker();

    virtual bool DisableRule(const std::string& id);
    virtual bool EnableRule(const std::string& id);

    virtual bool LogError(const std::string& id, const std::string& s);

    virtual bool LogInfo(const std::string& id, const std::string& s);

    void Clear();

    virtual void Run(SampleCheckerContext* context, HddpsSimulator* sim);

    std::vector<std::string> disabledRules;

    size_t maxErrors;  // 0 means log all errors
    size_t enabledInfoFound;
    size_t enabledErrorsFound;
    size_t disabledErrorsFound;
    size_t totalErrorsFound;
};

}  // namespace hddpstbc

#endif  // __SAMPLE_CHECKER_H__

