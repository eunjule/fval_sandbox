////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hddpsbfms.h
//------------------------------------------------------------------------------
//    Purpose: HDDPS Bus Functional Models
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 01/27/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __BFMS_H__
#define __BFMS_H__

#include <unordered_map>

#include "hddpssim.h"
#include "waveform.h"

namespace hddpstbc {

class ResistiveLoadBfm : public Bfm
{
public:
    //ResistiveLoadBfm(int slot, int subslot, float R)
    ResistiveLoadBfm(int slot, int subslot, float R)
    {
        for( size_t railNum=0; railNum < 26; railNum++) {
            size_t vRail = (slot * 2 * HDDPS_MAX_RAILS_PER_BOARD) + (subslot * HDDPS_MAX_RAILS_PER_BOARD) + railNum;
            railR[vRail] = R;
        }
    }
    virtual size_t MapRail(int slot, int subslot, size_t rail) const
    {
        // Every rail must map to a unique value
        return (slot * 2 * HDDPS_MAX_RAILS_PER_BOARD) + (subslot * HDDPS_MAX_RAILS_PER_BOARD) + rail;
    }
    //virtual void SetUp(int slot, int subslot, std::vector<size_t> uhcRails);
    virtual void Start(int slot, int subslot);
    virtual void DriveVoltage(int slot, int subslot, size_t rail, float time, float value);
    virtual void DriveCurrent(int slot, int subslot, size_t rail, float time, float value);
    virtual float SampleVoltage(int slot, int subslot, size_t rail, float time);
    virtual float SampleCurrent(int slot, int subslot, size_t rail, float time);
    virtual void SetResistance(int slot, int subslot, size_t rail, float resistance);
    virtual  fvalc::Waveform<float, 120000>* GetVoltageWaveform(int slot, int subslot, size_t rail)
    { 
        return &voltage[MapRail(slot, subslot, rail)];
    }
    virtual  fvalc::Waveform<float, 120000>* GetCurrentWaveform(int slot, int subslot, size_t rail)
    { 
        return &current[MapRail(slot, subslot, rail)];
    }
    //bfm.GetVoltageWaveform(8,0,16).SampleAt(0)  // SampleAt usage for Python
protected:
    std::unordered_map<size_t, fvalc::Waveform<float, 120000> > voltage;
    std::unordered_map<size_t, fvalc::Waveform<float, 120000> > current;
    //float R;
    std::unordered_map<size_t, float> railR;
};

}  // namespace hddpstbc

#endif  // __BFMS_H__

