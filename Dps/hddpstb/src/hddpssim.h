////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hddpssim.h
//------------------------------------------------------------------------------
//    Purpose: HDDPS Trigger Queue Simulator
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 01/21/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __HDDPSSIM_H__
#define __HDDPSSIM_H__

#include <cstdint>
#include <unordered_map>
#include <chrono>
#include <cmath>
#include <set>
#include <utility>

#include "hddpstbc.h"
#include "rcsim.h"
#include "defines.h"
#include "waveform.h"

#define HDDPS_SAMPLING_ENGINES 8
#define HDDPS_HCLC_RAIL_START    16
#define HDDPS_HCLC_RAILS           10

#define HDDPS_MOTHERBOARD_VLC_RAILS          16
#define HDDPS_MOTHERBOARD_LC_RAILS           10
#define HDDPS_MOTHERBOARD_SAMPLING_ENGINES   8
#define HDDPS_MOTHERBOARD_AD5560_ICS         10

#define HDDPS_DAUGHTERBOARD_HC_RAILS         10
#define HDDPS_DAUGHTERBOARD_HC_RAIL_START    16
#define HDDPS_DAUGHTERBOARD_SAMPLING_ENGINES 8
#define HDDPS_DAUGHTERBOARD_AD5560_ICS       10
#define HDDPS_DAUGHTERBOARD_ISL55180_ICS     2
#define HDDPS_DAUGHTERBOARD_HC_SLEW_RAMP_STEP 0.00305f  // mV

#define HDDPS_UHCS                           8
#define HDDPS_MAX_RAILS_PER_BOARD            26
//#define PERIOD                               exp(-6)

namespace hddpstbc {

struct TriggerQueueWord
{
    uint8_t b[TRIGGER_QUEUE_WORD_SIZE];
};

class Ad5560
{
public:
    virtual ~Ad5560() {};

};


class Isl55180
{
public:
    virtual ~Isl55180() {};


};

class HeaderSampleRegionSet
{
public:
    uint32_t DutHeaderRegionBase[HDDPS_UHCS];
    uint32_t DutHeaderRegionSize[HDDPS_UHCS];
    uint32_t DutSampleRegionBase[HDDPS_UHCS];
    uint32_t DutSampleRegionSize[HDDPS_UHCS];
    size_t ActualSampleRegionSize[HDDPS_UHCS];
};

class UhcSetSamplingEngine 
{
public:
    // UHC Number, UHC Rail Config, Actual Rails
    
    // size_t: Sample Engine Run Number, std::vector<size_t>: vector containing rails
    std::unordered_map<size_t, std::vector<size_t>> rails[HDDPS_UHCS];
    size_t dutDomainId[HDDPS_UHCS];
    size_t dutDomainIdValid[HDDPS_UHCS];
    //uint32_t railConfig;    // UHC Rail Config

};

class UhcSetFpga 
{
public:
    // UHC Number, UHC Rail Config, Actual Rails
    
    std::vector<size_t> rails[HDDPS_UHCS];
    size_t dutDomainId[HDDPS_UHCS];
    size_t dutDomainIdValid[HDDPS_UHCS];
    //uint32_t railConfig;    // UHC Rail Config

};

class SamplingEngineConfig
{
public:
    // Create a vector for multiple runs per sample engine.
    virtual ~SamplingEngineConfig() {};
    bool sampleEngineReset;
    std::vector<uint16_t> sampleDelay;
    std::vector<uint16_t> sampleCount;
    std::vector<uint16_t> sampleRate;
    std::vector<uint16_t> sampleMetaHi;
    std::vector<uint16_t> sampleMetaLo;
    std::vector<uint16_t> sampleRailSelect;
    std::vector<uint16_t> sampleStart;
    //virtual void paramVectorNorm( std::vector<uint16_t> sampleRunRef, std::vector<uint16_t>& sampleParam );
    virtual void SampleEngineConfig();
};

class HddpsSimulator;   // Forward declaration
class MotherboardSimulator;   // Forward declaration
class DaughterboardSimulator;   // Forward declaration

class SamplingEngine
{
public:
    SamplingEngine(int slot, int subslot, HddpsSimulator* sim, size_t samplePeriod, UhcSetFpga* fpgaUhc);
    virtual ~SamplingEngine() {};

    virtual void SampleEngineWaveform(int slot, int subslot, size_t rail, int sampleDelay, int sampleCount, int sampleRate, int sampleStart);  // Collecting Samples based on Sample Engine Configuration
    virtual void configureSampleEngineWaveformSet();
    virtual void createMultipleSampleEngineWaveformSet();
    virtual void SampleDataCreation(size_t sampleEngineNumber, size_t sampleRunNumber);
    virtual void MultipleSampleDataCreation(size_t sampleEngineNumber);
    virtual void SampleTimeCreation(size_t sampleEngineNumber, size_t sampleRunNumber);
    virtual void MultipleSampleTimeCreation(size_t sampleEngineNumber);
    virtual void ValidCycleCreation(size_t sampleEngineNumber);

    std::vector<size_t> sampleEngineInUseVector;
    std::set<size_t> sampleEngineInUseSet;
    std::vector<float> sampleEngineData[HDDPS_SAMPLING_ENGINES];
    std::vector<size_t> sampleEngineTime[HDDPS_SAMPLING_ENGINES];
    std::unordered_map<size_t, uint64_t> validCycleDict[HDDPS_SAMPLING_ENGINES];; // rail number, valid sample cycle for rail number

    // sampleEngineVoltage/Current/Time: size_t => Rail Number, std::vector<type> ==> Data
    std::unordered_map<size_t, std::vector<float>> sampleEngineVoltage;
    std::unordered_map<size_t, std::vector<float>> sampleEngineCurrent;
    std::unordered_map<size_t, std::vector<size_t>> sampleEngineTimePerRail;

    // Uhc Config from BARWRITE
    UhcSetFpga* fpgaUhc;
    // Uhc Config after filtering BARWRITE config using sample engine Rail Select
    UhcSetSamplingEngine uhcSampleEngine;
    // Multi-Run sample engine config
    SamplingEngineConfig sampleEngineConfig [HDDPS_SAMPLING_ENGINES];
    HddpsSimulator* sim;
    std::unordered_map<size_t, uint64_t> validSampleCycle;
    int m_slot;
    int m_subslot;
    size_t samplePeriod;
    size_t m_sampleCycle;
    size_t sampleTime;
    virtual size_t SampleTime(size_t sampleCycle) const
    {
        return sampleCycle * samplePeriod;
    }
    void ForceInit(size_t railStart, size_t railEnd);
};
class Rail
{
public:
    Rail(size_t rail = 999, HddpsSimulator* sim = nullptr);
    virtual ~Rail() {};

    virtual void SetVoltage(float value);
    virtual void SetCurrent(float value);
    virtual void SetCurrentRange(uint16_t value) {} ;
    virtual void EnableDisableRail(bool value) ;
    virtual void SetOverVoltage(float value);
    virtual void SetUnderVoltage(float value);
    virtual void SetCurrentClampHigh(float value);
    virtual void SetCurrentClampLow(float value);
    virtual void EnableDisableCurrentClamp(uint16_t value) {};
    virtual void SetSafeState(uint16_t value) {};
    virtual void SetMode(uint16_t value) {};
    virtual void SetSlewRate(uint16_t value) {};
    virtual void EnableVoltageCompareAlarm(uint16_t value);
    virtual void StartFreeDrive(uint16_t value) {};
    virtual void EndFreeDrive(uint16_t value) {};
    virtual void ForceClampFlush(uint16_t value) {};
    virtual void EnableDisableTriggerSlaves(uint16_t value) {};
    virtual void SetFreeDriveHighCurrent(uint16_t value);
    virtual void SetFreeDriveLowCurrent(uint16_t value);
    virtual void EndFreeDriveEnable(uint16_t value) {};
    virtual void RailCurrentClampAlarm(uint16_t value) {};
    virtual void TriggerQueueComplete(uint16_t value) {};
    virtual void TqNotify(uint16_t value) {};
    virtual void SetSlaveCards(uint16_t value) {};
    virtual void SetSlaveCount(uint16_t value) {};
    virtual void SetRailDirty(uint16_t value) {};
    virtual void EnableDisablHcDriver(uint16_t value) {};
    virtual void DisableCurrentClamp(uint16_t value) {};
    virtual void AssertTestRail(uint16_t value) {};
    virtual void SetTesterPeriod(float period);
    
    virtual float GetCurrentVoltage(int slot, int subslot, size_t rail); 
    virtual float Time() const
    {
        return cycle * m_period;
    }

    /// Tester cycle period in seconds
    // Test Period is set to 1um while sample period is set to 10us for Hc/Lc and 800us for Vlc.
    float m_period;

    // Rail Number
    size_t m_rail;
    uint64_t cycle;
    bool railUnfoldedBit;
    bool lastValidCycle;
    bool railEnabledBit;
    bool freeDriveEnable;
    bool slewUp;
    bool slewDown;
    float freeDriveVoltage;
    float overVoltage;
    float underVoltage;
    float currentClampHigh;
    float currentClampLow;
    float freeDriveCurrentHigh;
    float freeDriveCurrentLow;
    float initialVoltage;
    bool initialVoltageExist;
    HddpsSimulator* sim;
};


class HcLcRail : public Rail
{
public:
    HcLcRail(size_t rail = 999, HddpsSimulator* sim = nullptr);
    void SetVoltage(float value) override;
    void SetCurrent(float value) override;
};

class HcRail : public HcLcRail
{
public:
    HcRail(size_t rail = 999, HddpsSimulator* sim = nullptr);
};

class LcRail : public HcLcRail
{
public:
    LcRail(size_t rail = 999, HddpsSimulator* sim = nullptr);
    //void SetVoltage(float value) override;
    //void SetCurrent(float value) override;
};


class VlcRail : public Rail
{
public:
    VlcRail(size_t rail = 999, HddpsSimulator* sim = nullptr);
    void SetVoltage(float value) override;
    void SetCurrent(float value) override;
};

class Alarms
{
public:
    Alarms(): alarm(0x0) {};
    virtual ~Alarms() {};
    uint32_t alarm;
    virtual void AlarmClear(uint32_t alarmBitLoc) {
        alarm &= ~(1 << (alarmBitLoc));
    }  
    virtual void AlarmSet(uint32_t alarmBitLoc) {
        alarm |= 1 << alarmBitLoc;
    }
    virtual void AlarmsReset() {
        alarm = 0x0;
    } 
};

class HclcAlarms : public Alarms
{
public:
    virtual void AlarmClear(uint32_t alarmBitLoc) {
        alarm &= ~(1 << ( alarmBitLoc - HDDPS_DAUGHTERBOARD_HC_RAIL_START ));            // 16 is
    }  
    virtual void AlarmSet(uint32_t alarmBitLoc) {
        alarm |= ( 1 << (alarmBitLoc - HDDPS_DAUGHTERBOARD_HC_RAIL_START  ));
    }
};

class HddpsAlarms
{
public:
    Alarms globalAlarms;
    Alarms hclcSampleAlarms;
    Alarms vlcSampleAlarms;
    Alarms hclcCfoldAlarms[HDDPS_DAUGHTERBOARD_HC_RAILS] ;
    Alarms vlcCfoldAlarms[HDDPS_MOTHERBOARD_VLC_RAILS];
    Alarms hclcCfoldAlarmsContents[HDDPS_DAUGHTERBOARD_HC_RAILS] ;
    Alarms vlcCfoldAlarmsContents[HDDPS_MOTHERBOARD_VLC_RAILS];
    HclcAlarms hclcAlarms;
    Alarms vlcAlarms;
    Alarms hclcAlarmsContents[HDDPS_DAUGHTERBOARD_HC_RAILS] ;
    Alarms vlcAlarmsContents[HDDPS_MOTHERBOARD_VLC_RAILS] ;
    void AlarmsReset(); 
};

/// FPGA Output/Pin Electronics/TIU/DUT BFM = Bus Functional Model (See http://en.wikipedia.org/wiki/Bus_Functional_Model)
class Bfm
{
public:
    /// Called by the test environment for every (slot, subslot) in the system
    //virtual void SetUp(int slot, int subslot, std::vector<size_t> uhcRails) = 0;
    /// Called by each simulator before executing a trigger queue
    virtual void Start(int slot, int subslot) = 0;
    /// Called by the simulator to write voltage values for BFM's input rails
    virtual void DriveVoltage(int slot, int subslot, size_t rail, float time, float value) = 0;
    /// Called by the simulator to write current values for BFM's input rails
    virtual void DriveCurrent(int slot, int subslot, size_t rail, float time, float value) = 0;
    /// Called by the simulator to read voltage values from the BFM
    virtual float SampleVoltage(int slot, int subslot, size_t rail, float time) = 0;
    /// Called by the simulator to read current values from the BFM
    virtual float SampleCurrent(int slot, int subslot, size_t rail, float time) = 0;
    virtual ~Bfm() { };
};

class HddpsSimulator : public rctbc::InstrumentSimulator
{
public:
    HddpsSimulator(int slot, int subslot, size_t memorySize);
    virtual ~HddpsSimulator();

    virtual void BarRead(uint32_t bar, uint32_t offset, uint32_t data);
    // address and length are in bytes
    virtual void DmaRead(uint64_t address, const uint8_t* data, uint64_t length);
    // address and length are in bytes
    virtual void DmaWrite(uint64_t address, const uint8_t* data, uint64_t length);

    virtual void DumpMemory(std::string filename, uint64_t triggerQueueAddress, uint64_t triggerQueueLength) const;

    virtual std::vector<float>* GetSampleData(size_t sampleEngineNumber) = 0 ;

    virtual std::vector<size_t>* GetSampleTime(size_t sampleEngineNumber) = 0 ;

    virtual std::unordered_map<size_t, uint64_t> GetValidCycle(size_t sampleEngineNumber) = 0 ;
    
    virtual std::unordered_map<size_t, std::vector<size_t>> GetUhcRails(size_t uhcNumber) = 0; 

    virtual std::vector<uint16_t> GetSampleCounts(size_t uhcNumber) = 0; 

    virtual std::set<size_t> GetSampleEngineInUse() = 0;
    
    // if there is sample bounds alarm, then it's caught by python first and raise exception.
    // This part made for possible any future improvement in FPGA for out of bound case handling.
    // Setting also out of bounds alarm bit too.
    virtual void SetSampleRegionSize( size_t uhcIdx, size_t sampleRegionSize) = 0;

    /// Sets the period of this simulator instance
    virtual void SetPeriod(float period);

    virtual void SetBfm(Bfm* bfm);

    virtual void ForcedInit();

    virtual void Run();

    virtual bool StepTriggerQueueProcessor();
    /// Execute one tester cycle. This function returns false after the pattern ends.
    /// This function calls StepVectorProcessor() and StepInternalVectorProcessor() when required.
    //virtual bool StepCycle(TesterCycleContext* context);

    virtual void ExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) = 0;

    //virtual float Time() const
    //{
    //    return cycle * period;
    //}

    virtual void ProcessTrigger(uint32_t trigger);
    virtual uint32_t GetHddpsAlarms(std::string subAlarm, size_t railNumber);

    /// Subslot of this simulator (slot is already part of base class)
    int subslot;
    /// Main menory size
    size_t memorySize;
    // Offset Memory
    uint32_t offsetAddr;
    /// Main memory
    TriggerQueueWord* memory;

    /// Simulator period in seconds
    float m_period;

    uint32_t tqaddr;  // Instruction pointer

    std::chrono::time_point<std::chrono::high_resolution_clock> startTime;

    //uint64_t cycle;

    bool isStopped;
    bool stop;
    // Voltage Compare Enable Unmask for true.
    std::unordered_map<size_t, bool>  voltageCompareEnable;

    HddpsAlarms hddpsAlarms;

    Bfm* m_bfm;

    //std::unordered_map<size_t, HcLcRail> hcLc;
    // Uhc Config from BARWRITE
    UhcSetFpga hcLcUhc;
    // Sampling Engine
    SamplingEngine hcLcSampleEngine;
};


class TqExecutionEngine          // refactoring the HcLc/VLC rail's common Tq Processing unit.
{
public:
    TqExecutionEngine(int slot, int subslot, HddpsSimulator* sim);
    virtual ~TqExecutionEngine(){};
    
    virtual void CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) = 0;

    float voltageSlewRampStepSize;

    int slot;
    int subslot;
    bool stop;
    HddpsSimulator* sim;
    std::unordered_map<size_t, Rail*> virtualRail;
};

class VlcHcLcTqExecutionEngine : public TqExecutionEngine
{
public:
    VlcHcLcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim);
    void CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) override;
};

class HcLcTqExecutionEngine : public VlcHcLcTqExecutionEngine
{
public:
    HcLcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim);
    void CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) override;
    std::unordered_map<size_t, std::vector<size_t>> masterSlaveGanging;    // Only for HcLc. Data for Master-Slave Relation from BAR2 Rail Master Mask Register. Key is master rail. value is slaves vector.
    bool gangEnabled;
};


class LcTqExecutionEngine : public HcLcTqExecutionEngine
{
public:
    LcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim);
    void CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) override;
};

class HcTqExecutionEngine : public HcLcTqExecutionEngine
{
public:
    HcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim);
    void CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) override;

};

class VlcTqExecutionEngine : public VlcHcLcTqExecutionEngine
{
public:
    VlcTqExecutionEngine(int slot, int subslot, MotherboardSimulator* sim);
    void CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) override;
    MotherboardSimulator* sim;
};

class MotherboardSimulator : public HddpsSimulator
{
public:
    MotherboardSimulator(int slot, int subslot, size_t memorySize);

    virtual std::string Name() const;
    
    void Run() override;
    void ExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write) override;
    virtual void BarWrite(uint32_t bar, uint32_t offset, uint32_t data) ;
    virtual bool BoardSelection(size_t& vlcCount, size_t& hcLcCount );
    void ForcedInit() override;

    std::vector<float>* GetSampleData(size_t sampleEngineNumber) override;  
    std::vector<size_t>* GetSampleTime(size_t sampleEngineNumber) override;
    std::unordered_map<size_t, uint64_t> GetValidCycle(size_t sampleEngineNumber) override;
    std::unordered_map<size_t, std::vector<size_t>> GetUhcRails(size_t uhcNumber) override; 
    std::vector<uint16_t> GetSampleCounts(size_t uhcNumber) override; 
    std::set<size_t> GetSampleEngineInUse() override;
    void SetSampleRegionSize( size_t uhcIdx, size_t sampleRegionSize) override;

    // Uhc Config from BARWRITE
    UhcSetFpga vlcUhc;
    // Trigger Queue Execution Engine
    LcTqExecutionEngine mbLcTqEngine;
    VlcTqExecutionEngine mbVlcTqEngine;
    // Sampling Engine
    SamplingEngine mbVlcSampleEngine;
    // Rail Instantiation
    std::unordered_map<size_t, LcRail> lc;
    std::unordered_map<size_t, VlcRail> vlc;
    // Header Sample Region
    HeaderSampleRegionSet vlcHeaderSampleRegionSet;
    HeaderSampleRegionSet hcLcHeaderSampleRegionSet;

    std::string whichBoard;
    // Unused Class object
    Ad5560 ad5560[HDDPS_MOTHERBOARD_AD5560_ICS];
    Isl55180 isl55180[HDDPS_DAUGHTERBOARD_ISL55180_ICS];
};

class DaughterboardSimulator : public HddpsSimulator
{
public:
    DaughterboardSimulator(int slot, int subslot, size_t memorySize);

    virtual std::string Name() const;

    virtual void ExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write);
    virtual void BarWrite(uint32_t bar, uint32_t offset, uint32_t data);
    void ForcedInit() override;

    std::vector<float>* GetSampleData(size_t sampleEngineNumber) override;  
    std::vector<size_t>* GetSampleTime(size_t sampleEngineNumber) override ;
    std::unordered_map<size_t, uint64_t> GetValidCycle(size_t sampleEngineNumber) override;
    std::unordered_map<size_t, std::vector<size_t>> GetUhcRails(size_t uhcNumber) override;
    std::vector<uint16_t> GetSampleCounts(size_t uhcNumber) override; 
    std::set<size_t> GetSampleEngineInUse() override;
    void SetSampleRegionSize(size_t uhcIdx, size_t sampleRegionSize) override;

    // Trigger Queue Execution Engine
    HcTqExecutionEngine dbHcTqEngine;
    // Header Sample Region
    HeaderSampleRegionSet hcLcHeaderSampleRegionSet;
    //HcRail hcRail[HDDPS_DAUGHTERBOARD_HC_RAILS]; 
    std::unordered_map<size_t, HcRail> hc;

    Ad5560 ad5560[HDDPS_DAUGHTERBOARD_AD5560_ICS];
};


}  // namespace hddpstbc

#endif  // __HDDPSSIM_H__

