////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hddpsbfms.cc
//------------------------------------------------------------------------------
//    Purpose: HDDPS Bus Functional Models
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 01/27/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include "hddpsbfms.h"
#include "logging.h"

namespace hddpstbc {

//void ResistiveLoadBfm::SetUp(int slot, int subslot, std::vector<size_t> uhcRails)
//{
//    if (subslot == 0) {
//        for (size_t rail = 0; rail != uhcRails.size(); rail++) {
//            size_t vrail = MapRail(slot, subslot, uhcRails[rail]);
//            voltage[vrail].Clear();
//            current[vrail].Clear();
//        }
//        for (size_t rail = 0; rail != uhcRails.size(); rail++) {
//            size_t vrail = MapRail(slot, subslot, uhcRails[rail]);
//            voltage[vrail].Clear();
//            current[vrail].Clear();
//        }
//    } else if (subslot == 1) {
//        for (size_t rail = 0; rail != uhcRails.size(); rail++) {
//            size_t vrail = MapRail(slot, subslot, uhcRails[rail]);
//            voltage[vrail].Clear();
//            current[vrail].Clear();
//        }
//    } else {
//        std::stringstream ss;
//        ss << "Invalid subslot " << subslot;
//        throw std::runtime_error(ss.str());
//    }
//
//}

void ResistiveLoadBfm::Start(int slot, int subslot)
{
    if (subslot == 0) {
        for (size_t rail = 0; rail < HDDPS_MOTHERBOARD_VLC_RAILS; rail++) {
            size_t vrail = MapRail(slot, subslot, rail);
            voltage[vrail].Clear();
            current[vrail].Clear();
        }
        for (size_t rail = 16; rail < 16 + HDDPS_MOTHERBOARD_LC_RAILS; rail++) {
            size_t vrail = MapRail(slot, subslot, rail);
            voltage[vrail].Clear();
            current[vrail].Clear();
        }
    } else if (subslot == 1) {
        for (size_t rail = 16; rail < 16 + HDDPS_DAUGHTERBOARD_HC_RAILS; rail++) {
            size_t vrail = MapRail(slot, subslot, rail);
            voltage[vrail].Clear();
            current[vrail].Clear();
        }
    } else {
        std::stringstream ss;
        ss << "Invalid subslot " << subslot;
        throw std::runtime_error(ss.str());
    }
}


void ResistiveLoadBfm::DriveVoltage(int slot, int subslot, size_t rail, float time, float value)
{
    size_t vrail = MapRail(slot, subslot, rail);
    float I = value / railR[vrail];
    bool success;
    success = voltage[vrail].DriveAt(time, value);
    if (!success) {
        LOG("error") << "Failed to voltage of " << value << "V for rail " << rail << " of (" << slot << "," << subslot << ")";
    }
    success = current[vrail].DriveAt(time, I);
    if (!success) {
        LOG("error") << "Failed to current of " << I << "A for rail " << rail << " of (" << slot << "," << subslot << ")";
    }
}

void ResistiveLoadBfm::DriveCurrent(int slot, int subslot, size_t rail, float time, float value)
{
    size_t vrail = MapRail(slot, subslot, rail);
    float V = value * railR[vrail];
    bool success;
    success = voltage[vrail].DriveAt(time, V);
    if (!success) {
        LOG("error") << "Failed to voltage of " << V << "V for rail " << rail << " of (" << slot << "," << subslot << ")";
    }
    success = current[vrail].DriveAt(time, value);
    if (!success) {
        LOG("error") << "Failed to current of " << value << "A for rail " << rail << " of (" << slot << "," << subslot << ")";
    }
}

float ResistiveLoadBfm::SampleVoltage(int slot, int subslot, size_t rail, float time)
{
    size_t vrail = MapRail(slot, subslot, rail);
    return voltage[vrail].SampleAt(time);
}

float ResistiveLoadBfm::SampleCurrent(int slot, int subslot, size_t rail, float time)
{
    size_t vrail = MapRail(slot, subslot, rail);
    return current[vrail].SampleAt(time);
}

void ResistiveLoadBfm::SetResistance(int slot, int subslot, size_t rail, float resistance)
{
    size_t vrail = MapRail(slot, subslot, rail);
    railR[vrail] = resistance;
}

}  // namespace hddpstbc

