////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hddpstbc.h
//------------------------------------------------------------------------------
//    Purpose: HDDPS High-Performance Testbench Methods and Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 12/15/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#ifndef __HDDPSTBC_H__
#define __HDDPSTBC_H__

#ifdef _MSC_VER
    #pragma warning(disable:4100) // unreferenced formal parameter
#endif

#include <cstdint>

#define TRIGGER_QUEUE_WORD_SIZE 4

namespace hddpstbc {

uint16_t EncodeSFP(double value);

double DecodeSFP(uint16_t sfp);

// Encodes floating point into 16-bit Qm.n format
int16_t EncodeFixedPoint16(size_t n, double value);

// Decodes number in 16-bit Qm.n into floating point
double DecodeFixedPoint16(size_t n, int16_t fp);

uint32_t EncodeFloatingPoint32(double value);

double DecodeFloatingPoint32(uint32_t fp);

void WriteTriggerCommand
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint8_t cmd,
    uint8_t arg,
    uint16_t data,
    uint8_t write
);

void WriteRawCommand
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint32_t data
);

void ReadTriggerCommand
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint8_t* cmd,
    uint8_t* arg,
    uint16_t* data,
    uint8_t* write
);

}  // namespace hddpstbc

#endif  // __HDDPSTBC_H__
