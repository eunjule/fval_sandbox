////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hddpssim.cc
//------------------------------------------------------------------------------
//    Purpose: HDDPS Trigger Queue Simulator
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 01/21/16
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <cstring>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <fstream>

#include "hddpssim.h"
#include "logging.h"

#include "symbols.h"

#define  ExecuteTrigger             0x00000014
#define  HclcDutId0RailConfig       0x000000A0
#define  HclcDutId7RailConfig       0x000000BC
#define  VlcDutId0RailConfig        0x000000C0
#define  VlcDutId7RailConfig        0x000000DC
#define  VirtualDutId0              0x00000080
#define  VirtualDutId7              0x0000009C
#define  HclcDut0HeaderRegionBase   0x00000880
#define  HclcDut7SampleRegionSize   0x000008FC

#define  VlcDut0HeaderRegionBase   0x00000900
#define  VlcDut7SampleRegionSize   0x0000097C

#define  RailMasterMask             0x00000100
#define  Mask16Bit                  0xFFFF
#define  Mask10Bit                  0x3FF
#define  HCLCSAMPLEPERIOD            10     // in microSecond
#define  VLCSAMPLEPERIOD            800     // in microSecond
#define  TESTERPERIOD                 1e-6f
#define  Enabled                    true
#define  Disabled                   false
#define  FreeDriveJumpVoltage       0.4f
#define  SLEWTOLERANCE              0.0f
#define  VOLTAGESTEPTIME            128
#define  SAMPLEHEADERSIZE           16  // In Bytes


namespace hddpstbc {

Rail::Rail(size_t rail, HddpsSimulator* sim)
{
    this->m_rail = rail;
    this->sim = sim;
    m_period = TESTERPERIOD;
    cycle = 0;
    railUnfoldedBit = false;
    railEnabledBit = false;
    freeDriveEnable = false;
    slewUp = false;
    slewDown = false;
    freeDriveCurrentHigh = 0;
    freeDriveCurrentLow = 0;
    initialVoltage = 0;
    initialVoltageExist = false;
}


void SamplingEngineConfig::SampleEngineConfig()
{
	;
	// Assuming that SAMPLESTART must be at the end of a set of sample engine commands to run.
    // Assuming SAMPLESTART must be non zero value to make it effective.
    // Creating vector for each sample engine config based on multi-runs per sample engine
    //paramVectorNorm( sampleStart, sampleDelay);
    //paramVectorNorm( sampleStart, sampleCount);
   // paramVectorNorm( sampleStart, sampleRate);
    //paramVectorNorm( sampleStart, sampleMetaHi);
    //paramVectorNorm( sampleStart, sampleMetaLo);
    //paramVectorNorm( sampleStart, sampleRailSelect);
}

//void SamplingEngineConfig::paramVectorNorm( std::vector<uint16_t> sampleRunRef, std::vector<uint16_t>& sampleParam)
//{
//    size_t totalRuns = sampleRunRef.size();
//    size_t paramSize = sampleParam.size();
//    size_t paramDelta = abs( static_cast<int>(paramSize) - static_cast<int>(totalRuns) );
//    //LOG("info") << " Delta is " << paramDelta;
//    // If delay had more elements than total runs, then delete the last elements in amount of difference.
//    if ( paramSize > totalRuns ) {
//        sampleParam.erase( sampleParam.end() - paramDelta, sampleParam.end()-1 );
//    // if delay is smaller than total runs, then add the last delay element.
//    } else if ( paramSize < totalRuns ) {
//        while ( paramDelta != 0 ) {
//            sampleParam.push_back( sampleParam[paramSize -1] ); // Adding the last element
//            paramDelta--  ;
//        }
//    }
//
//}

void Rail::SetVoltage(float value)
{
    // Setting Rail Voltage
    sim->m_bfm->DriveVoltage(sim->slot, sim->subslot, m_rail, Time(), value);
}

void HcLcRail::SetVoltage(float value) 
{
    if (railUnfoldedBit && railEnabledBit) {
        // Setting Rail Voltage
        Rail::SetVoltage(value);
        // Polling Rail Current
        float sampleCurrent = sim->m_bfm->SampleCurrent(sim->slot, sim->subslot, m_rail, Time() );
        // Setting Alarm Bit for overvoltage, undervoltage
        if ( value > overVoltage && !freeDriveEnable) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );// Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 3 ); // Set Comparator High Alarm, CRITICAL: Here rail is from 16, but all alarms has a starting index 0. Need to subtract 16.
        } else if ( value < underVoltage && !freeDriveEnable) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );// Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 2 );                  // Set Comparator Low Alarm
        }
        // Checking whether current is more or less than clamp values in simulation
        if ( sampleCurrent > currentClampHigh && !freeDriveEnable) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) ); // Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 0 );                  // 0 is clamp alarm in hclc rail alarms
        } else if ( sampleCurrent < currentClampLow && !freeDriveEnable) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 0 );                  // 0 is clamp alarm in hclc rail alarms
        }
    }
    // Handling the Slew Down case with initial voltage before rail enabled.
    if (railUnfoldedBit && !railEnabledBit) {
        // Setting initialVoltage 
        initialVoltage = value;
        initialVoltageExist = true;
    }
    
}

void VlcRail::SetVoltage(float value) 
{
    if (railUnfoldedBit && railEnabledBit) {
        // Setting Rail Voltage
        Rail::SetVoltage(value);
        // Polling Rail Current
        float sampleCurrent = sim->m_bfm->SampleCurrent(sim->slot, sim->subslot, m_rail, Time() );
        // Checking whether current is more or less than clamp values in simulation
        if ( sampleCurrent > currentClampHigh ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 11 );    // 11 is vlc_rail_alarm in global alarms
            sim->hddpsAlarms.vlcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );
            sim->hddpsAlarms.vlcAlarmsContents[m_rail].AlarmSet( 1 ); // 1 is clamp alarm high in vlc rail alarms
        } else if ( sampleCurrent < currentClampLow ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 11 );    // 11 is vlc_rail_alarm in global alarms
            sim->hddpsAlarms.vlcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );
            sim->hddpsAlarms.vlcAlarmsContents[m_rail].AlarmSet( 0 ); // 0 is clamp alarm low in vlc rail alarms
        }
    }
}

void Rail::SetCurrent(float value)
{
    sim->m_bfm->DriveCurrent(sim->slot, sim->subslot, m_rail, Time(), value);
}

void HcLcRail::SetCurrent(float value) 
{
    if (railUnfoldedBit && railEnabledBit) {
        // Setting Rail Current
        Rail::SetCurrent(value);
        // Polling Rail Voltage
        float sampleVoltage = sim->m_bfm->SampleVoltage(sim->slot, sim->subslot, m_rail, Time() );
        // Setting Alarm Bit for overvoltage, undervoltage
        if ( sampleVoltage > overVoltage) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );// Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 3 ); // Set Comparator High Alarm, CRITICAL: Here rail is from 16, but all alarms has a starting index 0. Need to subtract 16.
        } else if ( sampleVoltage < underVoltage ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );// Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 2 );                  // Set Comparator Low Alarm
        }
        // Checking whether current is more or less than clamp values in simulation
        if ( value > currentClampHigh ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );// Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 0 );                  // 0 is clamp alarm in hclc rail alarms
        } else if ( value < currentClampLow ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
            sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );// Indexing taken care by class itself
            sim->hddpsAlarms.hclcAlarmsContents[m_rail - HDDPS_HCLC_RAIL_START].AlarmSet( 0 );                  // 0 is clamp alarm in hclc rail alarms
        }
    }
}

void VlcRail::SetCurrent(float value) 
{
    if (railUnfoldedBit && railEnabledBit) {
        // Setting Rail Current
        Rail::SetCurrent(value);
        // Checking whether current is more or less than clamp values in simulation
        if ( value > currentClampHigh ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 11 );    // 11 is vlc_rail_alarm in global alarms
            sim->hddpsAlarms.vlcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );
            sim->hddpsAlarms.vlcAlarmsContents[m_rail].AlarmSet( 1 ); // 1 is clamp alarm high in vlc rail alarms
        } else if ( value < currentClampLow ) {
            sim->hddpsAlarms.globalAlarms.AlarmSet( 11 );    // 11 is vlc_rail_alarm in global alarms
            sim->hddpsAlarms.vlcAlarms.AlarmSet( static_cast<uint32_t>(m_rail) );
            sim->hddpsAlarms.vlcAlarmsContents[m_rail].AlarmSet( 0 ); // 0 is clamp alarm low in vlc rail alarms
        }
    }
}

void Rail::SetOverVoltage(float value)
{
    overVoltage = value;
    //LOG("debug") << " Over Voltage is " << overVoltage;
}

void Rail::SetUnderVoltage(float value)
{
    underVoltage = value;
    //LOG("debug") << " Under Voltage is " << underVoltage;
}

void Rail::SetCurrentClampHigh(float value)
{
    currentClampHigh = value;
}

void Rail::SetCurrentClampLow(float value)
{
    currentClampLow = value;
    //LOG("debug") << " Current Clamp Low is " << currentClampLow;
}


void Rail::EnableVoltageCompareAlarm(uint16_t value)
{
    if ( railEnabledBit == true && railUnfoldedBit == true) { 
        sim->voltageCompareEnable[m_rail] = true;
    }
}

float Rail::GetCurrentVoltage(int slot, int subslot, size_t rail) 
{
    return sim->m_bfm->SampleVoltage(sim->slot, sim->subslot, rail, Time());
}

void Rail:: SetFreeDriveHighCurrent(uint16_t value) 
{
    freeDriveCurrentHigh = static_cast<float>(DecodeSFP(value)); 
    LOG("debug") << " Free drive current high is " << freeDriveCurrentHigh;
}

void Rail::SetFreeDriveLowCurrent(uint16_t value) 
{
    freeDriveCurrentLow = static_cast<float>(DecodeSFP(value)); 
    LOG("debug") << " Free drive current low is " << freeDriveCurrentLow;
}

//void Rail::SetCurrentRange(float value)
//{
    // Chnages the current range on a rail
    // 3-0 Current Range
//}

void Rail::EnableDisableRail(bool value)
{
    // Break into Four Types of instructuons: Enable(Master), Disable(Master), Enable(Slave), Disable(Slave)
    // Associated bit in the Rail Master Mask Register(BAR2) to determin if the rail is a master or slave.
    // Bits 15-2 Reserved
    // Bit 1 Cascade Power On Bit
    //       0: Do not cascade enable to slave
    //       1: Cascade enable to slave
    // Bit 0 Enalbe/Disable bit
    //       0: Disable
    //       1: Enable
    railEnabledBit = value;
}

void Rail::SetTesterPeriod(float period)
{
    this->m_period = period;
}

HcLcRail::HcLcRail(size_t rail, HddpsSimulator* sim) : Rail(rail, sim)
{
}

HcRail::HcRail(size_t rail, HddpsSimulator* sim) : HcLcRail(rail, sim)
{
}

LcRail::LcRail(size_t rail, HddpsSimulator* sim) : HcLcRail(rail, sim)
{
}

VlcRail::VlcRail(size_t rail, HddpsSimulator* sim) : Rail(rail, sim)
{
}

HddpsSimulator::HddpsSimulator(int slot, int subslot, size_t memorySize) : hcLcSampleEngine(slot, subslot, this, HCLCSAMPLEPERIOD, &(this->hcLcUhc))
{
    m_bfm = nullptr;
    //period = 0;
    cardType = rctbc::DPS_CARD;
    this->slot = slot;
    this->subslot = subslot;
    this->memorySize = memorySize;
    memory = new TriggerQueueWord[this->memorySize];  // allocate trigger queue memory

    tqaddr = 0; //INIT
    offsetAddr = 0;//INIT Added
    stop = false;   //INIT
    // Initializing voltageCompareEnable to be false for all 26 rails.
    for (size_t rail =HDDPS_HCLC_RAIL_START; rail < HDDPS_HCLC_RAIL_START + HDDPS_HCLC_RAILS; rail++) {
        voltageCompareEnable[rail] = false; // INIT
    }
    //cycle = 0;
    //for (size_t rail =HDDPS_HCLC_RAIL_START; rail < HDDPS_HCLC_RAIL_START + HDDPS_HCLC_RAILS; rail++) {
    //    hcLc[rail] = HcLcRail(rail, this);
    //}
}

HddpsSimulator::~HddpsSimulator()
{
    if (memory != nullptr) {
        delete[] memory;
    }
}

uint32_t HddpsSimulator::GetHddpsAlarms(std::string subAlarm, size_t railNumber)
{
    std::string global("GlobalAlarms");
    std::string hclcSample("HclcSampleAlarms");
    std::string vlcSample("VlcSampleAlarms");
    std::string hclcCfold("HclcCfoldAlarms");
    std::string vlcCfold("VlcCfoldAlarms");
    std::string vlcCfoldData("VlcCfoldData");
    std::string hclcCfoldData("HclcCfoldData");
    std::string hclcRails("HclcRailAlarms");
    std::string vlcRails("VlcRailAlarms");
    std::string hclcRailData("HclcRailAlarmsData");
    std::string vlcRailData("VlcRailAlarmsData");

    if ( subAlarm == global ) {
        //hddpsAlarms.globalAlarms.AlarmSet(7);
        return hddpsAlarms.globalAlarms.alarm;

    } else if ( subAlarm == hclcSample ) {
        
        return hddpsAlarms.hclcSampleAlarms.alarm;

    } else if ( subAlarm == vlcSample ) {
        
        return hddpsAlarms.vlcSampleAlarms.alarm;

    } else if ( subAlarm == hclcCfold ) {

        return hddpsAlarms.hclcCfoldAlarms[railNumber].alarm;

    } else if ( subAlarm == vlcCfold ) {

        return hddpsAlarms.vlcCfoldAlarms[railNumber].alarm;

    } else if ( subAlarm == vlcCfoldData ) {

        return hddpsAlarms.vlcCfoldAlarmsContents[railNumber].alarm;

    } else if ( subAlarm == hclcCfoldData ) {

        return hddpsAlarms.hclcCfoldAlarmsContents[railNumber].alarm;

    } else if ( subAlarm == hclcRails ) {

        return hddpsAlarms.hclcAlarms.alarm;

    } else if ( subAlarm == vlcRails ) {

        return hddpsAlarms.vlcAlarms.alarm;

    } else if ( subAlarm == hclcRailData ) {

        return hddpsAlarms.hclcAlarmsContents[railNumber].alarm;

    } else if ( subAlarm == vlcRailData ) {

        return hddpsAlarms.vlcAlarmsContents[railNumber].alarm;
    
    } else {
        LOG("error") << "Alarm Name isn't set properly : " << subAlarm;
        return 2147483647;         // This large number equals to all bits set to 1 except 31th bit.
    }
}

void HddpsSimulator::BarRead(uint32_t bar, uint32_t offset, uint32_t data)
{
    // Nothing to do here
}


void HddpsSimulator::DmaRead(uint64_t address, const uint8_t* data, uint64_t length)
{
    // Nothing to do here
}

void HddpsSimulator::DmaWrite(uint64_t address, const uint8_t* data, uint64_t length)
{
    if (address % TRIGGER_QUEUE_WORD_SIZE != 0) {
        std::stringstream ss;
        ss << "DmaWrite address " << address << " is not a multiple of " << TRIGGER_QUEUE_WORD_SIZE;
        throw std::runtime_error(ss.str());
    }
    if (length % TRIGGER_QUEUE_WORD_SIZE != 0) {
        std::stringstream ss;
        ss << "DmaWrite length " << address << " is not a multiple of " << TRIGGER_QUEUE_WORD_SIZE;
        throw std::runtime_error(ss.str());
    }

    size_t triggerQueueAddress = address / TRIGGER_QUEUE_WORD_SIZE;  // Convert to trigger queue address
    size_t triggerQueueLength  = length / TRIGGER_QUEUE_WORD_SIZE;  // Convert to trigger queue length

    if (triggerQueueAddress >= memorySize) {
        // Drop the write, target address is outside our reach
        return;
    }
    // At this point we know that address < memorySize
    if (triggerQueueAddress + triggerQueueLength > memorySize) {
        // Truncate it, we can't write the entire thing but can write some of it
        triggerQueueLength = memorySize - triggerQueueAddress;
    }
    // Copy the data
    memcpy(&memory[triggerQueueAddress], data, triggerQueueLength * TRIGGER_QUEUE_WORD_SIZE);
}

void HddpsSimulator::DumpMemory(std::string filename, uint64_t triggerQueueAddress, uint64_t triggerQueueLength) const
{
    std::ofstream fout;
    fout.open(filename.c_str(), std::ios::out | std::ios::binary);
    fout.write(reinterpret_cast<char*>(memory + triggerQueueAddress), triggerQueueLength * sizeof(TriggerQueueWord));
    fout.close();
}

void HddpsSimulator::SetBfm(Bfm* bfm)
{
    this->m_bfm = bfm;
}

void HddpsAlarms::AlarmsReset()
{
     globalAlarms.AlarmsReset();
     hclcSampleAlarms.AlarmsReset();
     vlcSampleAlarms.AlarmsReset();
     hclcAlarms.AlarmsReset();
     vlcAlarms.AlarmsReset();
     for(size_t railNum=0; railNum < HDDPS_DAUGHTERBOARD_HC_RAILS; railNum++) {
         hclcCfoldAlarms[railNum].AlarmsReset();
         hclcCfoldAlarmsContents[railNum].AlarmsReset();
         hclcAlarmsContents[railNum].AlarmsReset();
     }
     for(size_t railNum=0; railNum <HDDPS_MOTHERBOARD_VLC_RAILS; railNum++) {
         vlcCfoldAlarms[railNum].AlarmsReset();
         vlcCfoldAlarmsContents[railNum].AlarmsReset();
         vlcAlarmsContents[railNum].AlarmsReset();
     }
}

void HddpsSimulator::ForcedInit()
{
    // Alarm Reset
    hddpsAlarms.AlarmsReset(); //INIT  Clearing tq_notify_alarm_uhc alarm 
    // HCLC Sample Engine Reset
    hcLcSampleEngine.ForceInit( HDDPS_HCLC_RAIL_START, HDDPS_HCLC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS );
    // HCLC Rail Cycle Reset
    //tqaddr = 0; //INIT
}

void DaughterboardSimulator::ForcedInit()
{
    HddpsSimulator::ForcedInit();
    for( size_t railNumber= HDDPS_HCLC_RAIL_START; railNumber < HDDPS_HCLC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ; railNumber++) {
        hc[railNumber].cycle = 0;
    }
}

void MotherboardSimulator::ForcedInit()
{
    HddpsSimulator::ForcedInit();
    // VLC Sample Engine Reset
    mbVlcSampleEngine.ForceInit(0, HDDPS_MOTHERBOARD_VLC_RAILS );
    // VLC Rail Cycle Reset
    for( size_t railNumber= 0; railNumber <  HDDPS_MOTHERBOARD_VLC_RAILS ; railNumber++) {
        vlc[railNumber].cycle = 0;
    }
    for( size_t railNumber= HDDPS_HCLC_RAIL_START; railNumber < HDDPS_HCLC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ; railNumber++) {
        lc[railNumber].cycle = 0;
    }
}

void HddpsSimulator::Run()
{
    //HddpsSimulator is used for Daughterboard without change for now.
    ForcedInit();
    bool result = true;
    while (result) {
        result = StepTriggerQueueProcessor();
    }
    // Create Sample Engine Data
    hcLcSampleEngine.createMultipleSampleEngineWaveformSet();
}

void MotherboardSimulator::Run()
{
    size_t vlcCount = 0;
    size_t hcLcCount = 0;

    ForcedInit();
    bool checkResult = true;
    while (checkResult) {
        checkResult = BoardSelection(vlcCount, hcLcCount);
    }
    //LOG("info") << " vlc Count is  " << vlcCount;
    //LOG("info") << " hclc Count is  " << hcLcCount;
    if ( vlcCount > hcLcCount ) {
        whichBoard = "VLC";
    } else whichBoard = "HCLC";
    bool result = true;
    while (result) {
        result = StepTriggerQueueProcessor();
    }
    hcLcSampleEngine.createMultipleSampleEngineWaveformSet();
    mbVlcSampleEngine.createMultipleSampleEngineWaveformSet();
}

bool MotherboardSimulator::BoardSelection(size_t& vlcCount, size_t& hcLcCount)
{
    // Check address bounds
    if (tqaddr >= memorySize) {
        std::stringstream ss;
        ss << "Invalid tqaddr=" << tqaddr << ", memorySize=" << memorySize;
        throw std::runtime_error(ss.str());
    }

    
    // Update trigger queue pointer
    uint8_t* tq = (uint8_t*) &memory[tqaddr];
    // Read trigger queue command
    uint8_t cmd;
    uint8_t arg;
    uint16_t data;
    uint8_t write;
    //LOG("info") << " offset addresss is " << offsetAddr;
    ReadTriggerCommand(tq, memorySize, 0, &cmd, &arg, &data, &write);

    //LOG("info") << "Tqaddr is " << tqaddr;
    if ( cmd == TRIGGER_Q_COMPLETE ) {
        stop = true; 
    } else if ( cmd == TQ_NOTIFY ) {
    } else if ( arg <= 0x3C && cmd == 1 ) {
        hcLcCount++;
    } else if ( arg >= 0x40 && cmd == 1 ) {
        vlcCount++;
    } else if ( arg >= 16 ) {
        hcLcCount++;
    } else if ( arg < 16 ) {
        vlcCount++;
    }

    if (stop) {
        std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
        LOG("debug") << "Stopped trigger queue simulator (" << slot << "," << subslot << ") at address=" << Hex(tqaddr);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(now - startTime).count();
        LOG("debug") << "Simulator took " << elapsed; //<< " wall clock seconds to simulate " << Time() << " seconds";
        isStopped = true;
        tqaddr = 0; // INIT added
        //tqaddr = offsetAddr;   //INIT Comment out
        stop = false; //INIT
        return false;
    }
    tqaddr += 1;

    return true;

}

bool HddpsSimulator::StepTriggerQueueProcessor()
{
    // Check address bounds
    if (tqaddr >= memorySize) {
        std::stringstream ss;
        ss << "Invalid tqaddr=" << tqaddr << ", memorySize=" << memorySize;
        throw std::runtime_error(ss.str());
    }

    // Update trigger queue pointer
    uint8_t* tq = (uint8_t*) &memory[tqaddr];

    // Read trigger queue command
    uint8_t cmd;
    uint8_t arg;
    uint16_t data;
    uint8_t write;
    //LOG("info") << " offset addresss is " << offsetAddr;
    ReadTriggerCommand(tq, memorySize, 0, &cmd, &arg, &data, &write);

    //LOG("info") << "Tqaddr is " << tqaddr;
    ExecuteTriggerCommand(cmd, arg, data, write);

    if (stop) {
        std::chrono::time_point<std::chrono::high_resolution_clock> now = std::chrono::high_resolution_clock::now();
        LOG("debug") << "Stopped trigger queue simulator (" << slot << "," << subslot << ") at address=" << Hex(tqaddr);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(now - startTime).count();
        LOG("debug") << "Simulator took " << elapsed; //<< " wall clock seconds to simulate " << Time() << " seconds";
        isStopped = true;
        tqaddr = 0;   //INIT
        stop = false; //INIT
        return false;
    }
    tqaddr += 1;

    return true;
}

void HddpsSimulator::SetPeriod(float period)
{
    this->m_period = period;
}

void HddpsSimulator::ProcessTrigger(uint32_t trigger)
{

}

/*
bool HddpsSimulator::StepCycle(TesterCycleContext* context)
{
    while (!isStopped) {
        if (vectors.size() == 0) {
            // Keep executing pattern words / instructions until one or more output vectors are generated
            while (vectors.size() == 0) {
                bool success = StepVectorProcessor();
                if (!success) {
                    break;
                }
            }
        } else {
            // Execute first internal vector
            return StepInternalVectorProcessor(context);
        }
    }
    // Increase cycle count
    cycle++;
    return false;
}
*/

TqExecutionEngine::TqExecutionEngine(int slot, int subslot, HddpsSimulator* sim)
{
    this->slot = slot;
    this->subslot = subslot;
    this->sim = sim;
    voltageSlewRampStepSize = 0.0f;  // Default Value if RAMP_STEP_SIZE not defined.
    stop = false;

}

VlcHcLcTqExecutionEngine::VlcHcLcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim ):TqExecutionEngine(slot, subslot, sim)
{
}

HcLcTqExecutionEngine::HcLcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim) : VlcHcLcTqExecutionEngine(slot, subslot, sim)
{
    //for (size_t rail =HDDPS_HCLC_RAIL_START; rail < 16 + HDDPS_HCLC_RAILS; rail++) {
    //    hcLc[rail] = HcLcRail(rail, this->sim);
    //}
    // MasterSlaveGanging
    gangEnabled = false;
}

LcTqExecutionEngine::LcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim ) : HcLcTqExecutionEngine(slot, subslot, sim)
{
}

HcTqExecutionEngine::HcTqExecutionEngine(int slot, int subslot, HddpsSimulator* sim ) : HcLcTqExecutionEngine(slot, subslot, sim)
{
    //this->sim = sim;
    //this->virtualRail = virtualRail;
}

VlcTqExecutionEngine::VlcTqExecutionEngine(int slot, int subslot, MotherboardSimulator* sim ) : VlcHcLcTqExecutionEngine(slot, subslot, sim)
{
    this->sim = sim;
}

SamplingEngine::SamplingEngine(int slot, int subslot, HddpsSimulator* sim, size_t samplePeriod, UhcSetFpga* fpgaUhc ) 
{
    this->m_slot = slot;
    this->m_subslot = subslot;
    this->sim = sim;
    this->samplePeriod = samplePeriod;
    this->fpgaUhc = fpgaUhc;
    m_sampleCycle = 0;
    sampleTime = 0;
}

void SamplingEngine::SampleEngineWaveform(int slot, int subslot, size_t railNum, int sampleDelay, int sampleCount, int sampleRate, int sampleStart)
{
    bool sampleDelayDefined = true;
    float sampleTimeFloat = 0.0f;

    if (sampleStart != 0) {
        for ( int sampleIdx=0; sampleIdx < sampleCount; sampleIdx++) {
            if ( sampleDelayDefined ) {
            sampleTime += (sampleDelay);  // Converting us in sampleDelay to second using TesterPeriod
            sampleDelayDefined = false;
            } else {
            m_sampleCycle = 1 + sampleRate; 
            sampleTime += SampleTime(m_sampleCycle);
            }
            //LOG("info") << "Sample Time is " << sampleTime;
            // Recording sample Time per rail
            sampleEngineTimePerRail[railNum].push_back( sampleTime );
            sampleTimeFloat = static_cast<float>( sampleTime*TESTERPERIOD );
            // Getting sample voltage from BFM
            float samplevoltage = sim->m_bfm->SampleVoltage(sim->slot, sim->subslot, railNum, sampleTimeFloat );
            //LOG("info") << "Sample voltage is " << samplevoltage;
            // 0 volt if samplevoltage is a default 999.0f. Otherwise, voltage set by rail commands
            if (samplevoltage != 999.0f)  {
                sampleEngineVoltage[railNum].push_back( samplevoltage );
            } else sampleEngineVoltage[railNum].push_back( 0.0f );
            // Getting sample current from BFM
            float samplecurrent = sim->m_bfm->SampleCurrent(sim->slot, sim->subslot, railNum, sampleTimeFloat );
            // 0 A if sampleecurrent is a default 999.0f. Otherwise, current set by rail commands
            if (samplecurrent != 999.0f) {
                sampleEngineCurrent[railNum].push_back( samplecurrent );
            } else sampleEngineCurrent[railNum].push_back( 0.0f );
        } // for

    } // if (sampleRun)
}

void SamplingEngine::configureSampleEngineWaveformSet()
{
    // Removing the redudant sample Engine number in use and create a unique set in sampleEngineInUseSet
    sampleEngineInUseSet.insert( sampleEngineInUseVector.begin(), sampleEngineInUseVector.end() );
    std::set<size_t>::iterator uhcIdx;
    // Looping over sample Engine in use
    for(uhcIdx = sampleEngineInUseSet.begin(); uhcIdx != sampleEngineInUseSet.end(); ++uhcIdx) {
        size_t sampleEngineNumber = *uhcIdx;
        // Sample Engine Variable Initialization.
        sampleEngineConfig[sampleEngineNumber].SampleEngineConfig();
        //LOG("info") << "sample Engine Number is " << sampleEngineNumber;
        // Uhc Rails from BARWRITE
        std::vector<size_t> uhcRailBits = fpgaUhc->rails[ sampleEngineNumber ];
        // sample Engine Config from Trigger Queue
        SamplingEngineConfig engineConfig = sampleEngineConfig[sampleEngineNumber];
        // Looping over sample engine run number
        for( size_t runNum=0; runNum < engineConfig.sampleStart.size(); runNum++) {
            // Looping over UHC Rail config
            for( size_t railIdx = 0 ; railIdx !=  uhcRailBits.size() ; railIdx++) {
                // Different calculation for selectedRail for HC/LC and VLC. HDDPS_HCLC_RAIL_START
                size_t selectedRail;
                if ( uhcRailBits[railIdx] >=  HDDPS_HCLC_RAIL_START) {
                    selectedRail = (engineConfig.sampleRailSelect[runNum] >> ( uhcRailBits[railIdx] -HDDPS_HCLC_RAIL_START ) ) & 0x1;
                } else {
                    selectedRail = (engineConfig.sampleRailSelect[runNum] >> ( uhcRailBits[railIdx] ) ) & 0x1;
                }
                size_t railNum = uhcRailBits[railIdx];
                //LOG("info") << "uhcRailBit is " << railNum;
                //LOG("info") << "selectedRail is " << selectedRail;
                if ( selectedRail == 1) {
                    uhcSampleEngine.rails[sampleEngineNumber][runNum].push_back(railNum);
                }
            }
        }
    }
}

void SamplingEngine::createMultipleSampleEngineWaveformSet()
{
    configureSampleEngineWaveformSet();
    std::set<size_t>::iterator uhcIdx;
    for(uhcIdx = sampleEngineInUseSet.begin(); uhcIdx != sampleEngineInUseSet.end(); ++uhcIdx) {
        size_t sampleEngineNumber = *uhcIdx;
        SamplingEngineConfig engineConfig = sampleEngineConfig[sampleEngineNumber];
        for( size_t runNum=0; runNum < engineConfig.sampleStart.size(); runNum++) {
            std::vector<size_t> sampleEngineRails = uhcSampleEngine.rails[sampleEngineNumber][runNum];
            for( size_t idx=0; idx < sampleEngineRails.size(); idx++) {
                size_t railNum = sampleEngineRails[idx];
                //LOG("info") << "Sample Engine Count is "<< engineConfig.sampleCount[runNum] << " for run Number "<< runNum;
                //INIT for multiple Sample engine run
                m_sampleCycle = 0;
                sampleTime = 0;
                SampleEngineWaveform(m_slot, m_subslot, railNum, engineConfig.sampleDelay[runNum], engineConfig.sampleCount[runNum], engineConfig.sampleRate[runNum], engineConfig.sampleStart[runNum] );
            }
        }
    }
}

void  SamplingEngine::SampleDataCreation(size_t sampleEngineNumber, size_t sampleRunNumber)
{
    std::vector<size_t> uhcSampleEngineRails = uhcSampleEngine.rails[sampleEngineNumber][sampleRunNumber];
    size_t sampleCnt = sampleEngineConfig[sampleEngineNumber].sampleCount[sampleRunNumber];
    size_t sampleCntStart=0;
    // Adjusting sample start number depending on run number
    if ( sampleRunNumber > 0 ) {
        for( size_t runNum=0 ; runNum < sampleRunNumber; runNum++) {
            sampleCntStart += sampleEngineConfig[sampleEngineNumber].sampleCount[runNum];
        }
    }
    //LOG("info") << "Sample Engine Rail size is  " << uhcSampleEngineRails.size();
    //LOG("info") << " Sample start " << sampleCntStart;
    //LOG("info") << " Sample CNT " << sampleCnt;
    
    // Sample Engine stores data in the following format
    // Rail 1 voltage
    // Rail 1 current
    // Rail 2 voltage
    // Rail 2 current
    // Looping through all the sample voltage
    for( size_t sampleIdx = sampleCntStart; sampleIdx < (sampleCntStart + sampleCnt); sampleIdx++) {  
        // Looping through enabled sample engine rails corrsponding Uhc.
        for(size_t railIdx = 0; railIdx != uhcSampleEngineRails.size() ; railIdx++) {           
            size_t railNum = uhcSampleEngineRails[railIdx];
            sampleEngineData[sampleEngineNumber].push_back( sampleEngineVoltage[railNum][sampleIdx] );
            sampleEngineData[sampleEngineNumber].push_back( sampleEngineCurrent[railNum][sampleIdx] );

        }
    }
}

void SamplingEngine::MultipleSampleDataCreation(size_t sampleEngineNumber)
{
    SamplingEngineConfig engineConfig = sampleEngineConfig[sampleEngineNumber];
    for( size_t runNum=0; runNum < engineConfig.sampleStart.size(); runNum++) {
        SampleDataCreation(sampleEngineNumber, runNum );
    }
}

void  SamplingEngine::SampleTimeCreation(size_t sampleEngineNumber, size_t sampleRunNumber)
{
    std::vector<size_t> uhcSampleEngineRails = uhcSampleEngine.rails[sampleEngineNumber][sampleRunNumber];
    size_t sampleCnt = sampleEngineConfig[sampleEngineNumber].sampleCount[sampleRunNumber];
    size_t sampleCntStart=0;
    if ( sampleRunNumber > 0 ) {
        for( size_t runNum=0 ; runNum < sampleRunNumber; runNum++) {
            sampleCntStart += sampleEngineConfig[sampleEngineNumber].sampleCount[runNum];
        }
    }
    //LOG("info") << "Sample Engine Rail size is  " << hcLcUhcSampleEngineRails.size();
    for( size_t sampleIdx = sampleCntStart; sampleIdx < (sampleCntStart + sampleCnt); sampleIdx++) {  // Looping through all the sample voltage
        for(size_t railIdx = 0; railIdx != uhcSampleEngineRails.size() ; railIdx++) {           // Looping through enabled sample engine rails corrsponding Uhc.
            size_t railNum = uhcSampleEngineRails[railIdx];
            sampleEngineTime[sampleEngineNumber].push_back( sampleEngineTimePerRail[railNum][sampleIdx] );
            sampleEngineTime[sampleEngineNumber].push_back( sampleEngineTimePerRail[railNum][sampleIdx] );
        }
    }
}

void SamplingEngine::MultipleSampleTimeCreation(size_t sampleEngineNumber)
{
    SamplingEngineConfig engineConfig = sampleEngineConfig[sampleEngineNumber];
    for( size_t runNum=0; runNum < engineConfig.sampleStart.size(); runNum++) {
        SampleTimeCreation(sampleEngineNumber, runNum );
    }
}

void  SamplingEngine::ValidCycleCreation(size_t sampleEngineNumber)
{
    //LOG("info") << " I am VLC Valid Cycle 0";
    std::vector<size_t> uhcEngineRails = fpgaUhc->rails[sampleEngineNumber];
    //LOG("info") << "Sample Engine Rail size is  " << hcLcUhcEngineRails.size();
    for(size_t railIdx = 0; railIdx != uhcEngineRails.size() ; railIdx++) {           // Looping through enabled sample engine rails corrsponding Uhc.
        //LOG("info") << " I am VLC Valid Cycle 1";
        // Manually adjust rail number using HDDPS_HCLC_RAIIL_START
        size_t railNum = uhcEngineRails[railIdx] ;
        //validCycleDict[sampleEngineNumber].insert( std::make_pair(railNum, hcLc[railNum].validSampleCycle ) );
        validCycleDict[sampleEngineNumber][railNum] = validSampleCycle[railNum];
        LOG("info") << "rail " << railNum << " has " << validSampleCycle[railNum] << " valid sample cycles";
    }
}

void SamplingEngine::ForceInit(size_t railStart, size_t railEnd)
{
    
    for( size_t engineNumber=0; engineNumber < HDDPS_UHCS; engineNumber++) {    //INIT
        uhcSampleEngine.rails[engineNumber].clear();
        sampleEngineData[engineNumber].clear();                         // DEBUG: Clearing Sample Engine Data set. Without this, more sample data was created. 
        sampleEngineTime[engineNumber].clear();                         // DEBUG: Clearing Sample Engine Data set. Without this, more sample data was created. 
        sampleEngineConfig[engineNumber].sampleDelay.clear();
        sampleEngineConfig[engineNumber].sampleCount.clear();
        sampleEngineConfig[engineNumber].sampleRate.clear();
        sampleEngineConfig[engineNumber].sampleMetaHi.clear();
        sampleEngineConfig[engineNumber].sampleMetaLo.clear();
        sampleEngineConfig[engineNumber].sampleRailSelect.clear();
        sampleEngineConfig[engineNumber].sampleStart.clear();
    }


    // cleare stored data in sample engine voltag/current in rails
    for( size_t railNumber=railStart; railNumber <  railEnd; railNumber++) {
        sampleEngineVoltage[railNumber].clear(); 
        sampleEngineCurrent[railNumber].clear(); 
        sampleEngineTimePerRail[railNumber].clear(); 
        m_sampleCycle=0; 
        sampleTime=0; 

    }
    sampleEngineInUseVector.clear();    // Need to clear sampleEngine In use vector for Next run. INIT
    sampleEngineInUseSet.clear();       // Need to clear sampleEngine In use vector for Next run. INIT
    validSampleCycle.clear();           // Need to clear validSampleCycle
}

MotherboardSimulator::MotherboardSimulator(int slot, int subslot, size_t memorySize) : HddpsSimulator(slot, subslot, memorySize), mbLcTqEngine(slot, subslot, this), mbVlcTqEngine(slot, subslot, this), mbVlcSampleEngine(slot, subslot, this, VLCSAMPLEPERIOD, &(this->vlcUhc))
{
    for (size_t rail = 0; rail < HDDPS_MOTHERBOARD_VLC_RAILS; rail++) {
        //vlcRail[rail] = VlcRail(rail, this);
        vlc[rail] = (VlcRail(rail, this));
        mbVlcTqEngine.virtualRail[rail] = &(vlc[rail]) ;
    }
    for (size_t rail = 16; rail < 16 + HDDPS_MOTHERBOARD_LC_RAILS; rail++) {
        lc[rail] = LcRail(rail, this);
        mbLcTqEngine.virtualRail[rail] = &(lc[rail]) ;
    }
}

std::string MotherboardSimulator::Name() const
{
    std::stringstream ss;
    ss << "HDDPS Motherboard Simulator (" << slot << ", " << subslot << ")";
    return ss.str();
}

std::set<size_t> MotherboardSimulator::GetSampleEngineInUse()
{
    return hcLcSampleEngine.sampleEngineInUseSet;
}

std::vector<float>* MotherboardSimulator::GetSampleData(size_t sampleEngineNumber)
{
    if (whichBoard == "VLC") {
        mbVlcSampleEngine.MultipleSampleDataCreation( sampleEngineNumber );
        return &(mbVlcSampleEngine.sampleEngineData[sampleEngineNumber]);
    } else  {
        hcLcSampleEngine.MultipleSampleDataCreation( sampleEngineNumber );
        return &(hcLcSampleEngine.sampleEngineData[sampleEngineNumber]);
    }
}

std::vector<size_t>* MotherboardSimulator::GetSampleTime(size_t sampleEngineNumber)
{
    if (whichBoard == "VLC") {
        mbVlcSampleEngine.MultipleSampleTimeCreation( sampleEngineNumber );
        return &(mbVlcSampleEngine.sampleEngineTime[sampleEngineNumber]);
    } else  {
        hcLcSampleEngine.MultipleSampleTimeCreation( sampleEngineNumber );
        return &(hcLcSampleEngine.sampleEngineTime[sampleEngineNumber]);
    }
}

std::unordered_map<size_t, uint64_t> MotherboardSimulator::GetValidCycle(size_t sampleEngineNumber)
{
    if (whichBoard == "VLC") {
        mbVlcSampleEngine.ValidCycleCreation( sampleEngineNumber );
        return (mbVlcSampleEngine.validCycleDict[sampleEngineNumber]);
    } else  {
        hcLcSampleEngine.ValidCycleCreation( sampleEngineNumber );
        return (hcLcSampleEngine.validCycleDict[sampleEngineNumber]);
    }
}

std::unordered_map<size_t, std::vector<size_t>> MotherboardSimulator::GetUhcRails(size_t uhcNumber)
{
    if (whichBoard == "VLC") {
        return mbVlcSampleEngine.uhcSampleEngine.rails[uhcNumber];
    } else {
        return hcLcSampleEngine.uhcSampleEngine.rails[uhcNumber];
    }
}

std::vector<uint16_t> MotherboardSimulator::GetSampleCounts(size_t uhcNumber)
{
    if (whichBoard == "VLC") {
        return mbVlcSampleEngine.sampleEngineConfig[uhcNumber].sampleCount;
    } else {
        return hcLcSampleEngine.sampleEngineConfig[uhcNumber].sampleCount;
    }
}

void MotherboardSimulator::SetSampleRegionSize( size_t uhcIdx, size_t sampleRegionSize)
{
    if (whichBoard == "VLC") {
        vlcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx] = sampleRegionSize;
        // Sample out of bounds alarm from actual sample region size beyond the user defined sample region size 
        if ( vlcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx] > vlcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx] ) {
            hddpsAlarms.globalAlarms.AlarmSet( 13 ); 
            hddpsAlarms.vlcSampleAlarms.AlarmSet( 2*static_cast<uint32_t>(uhcIdx) + 1); 
        // Sample out of bounds alarm from actual sample header region size beyond the user defined sample header region size 
        } else if (mbVlcSampleEngine.sampleEngineConfig[uhcIdx].sampleStart.size()*SAMPLEHEADERSIZE > vlcHeaderSampleRegionSet.DutHeaderRegionSize[uhcIdx] ) {
            hddpsAlarms.globalAlarms.AlarmSet( 13 ); 
            hddpsAlarms.vlcSampleAlarms.AlarmSet( 2*static_cast<uint32_t>(uhcIdx) + 1); 
        }
    } else {
        hcLcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx] = sampleRegionSize;
        // Sample out of bounds alarm from actual sample region size beyond the user defined sample region size 
        if ( hcLcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx] > hcLcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx] ) {
            hddpsAlarms.globalAlarms.AlarmSet( 10 ); 
            hddpsAlarms.hclcSampleAlarms.AlarmSet( 2*static_cast<uint32_t>(uhcIdx) + 1); 
        // Sample out of bounds alarm from actual sample header region size beyond the user defined sample header region size 
        } else if (hcLcSampleEngine.sampleEngineConfig[uhcIdx].sampleStart.size()*SAMPLEHEADERSIZE > hcLcHeaderSampleRegionSet.DutHeaderRegionSize[uhcIdx] ) {
            hddpsAlarms.globalAlarms.AlarmSet( 10 ); 
            hddpsAlarms.hclcSampleAlarms.AlarmSet( 2*static_cast<uint32_t>(uhcIdx) + 1); 
        }
    }
}

void MotherboardSimulator::ExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write)
{
    if ( whichBoard=="HCLC") { // LC Rail Commands Process
        mbLcTqEngine.CommonExecuteTriggerCommand(cmd, arg, data, write);
        if (mbLcTqEngine.stop == true) {
            stop = true;                //INIT
            mbLcTqEngine.stop = false;  //INIT
        }
    } else if (whichBoard=="VLC") {   // TODO Change Sample Engine Register in symbols.py VLC Rail Commands Process
        mbVlcTqEngine.CommonExecuteTriggerCommand(cmd, arg, data, write);
        if (mbVlcTqEngine.stop == true) {
            stop = true;                //INIT
            mbVlcTqEngine.stop = false;  //INIT
        }
    } else {
        LOG("error") << " Neither LC or VLC Trigger Execution was not carried out" ;
    }
}

void MotherboardSimulator::BarWrite(uint32_t bar, uint32_t offset, uint32_t data)
{
    if (bar == 1) {
        if ( (offset >= HclcDutId0RailConfig) && (offset <= HclcDutId7RailConfig) ) {
            size_t uhcIndex = (offset -  HclcDutId0RailConfig)/4;
             hcLcUhc.rails[uhcIndex].clear();
            for( size_t idx=0 ; idx <=  HDDPS_DAUGHTERBOARD_HC_RAILS; idx++) {
                uint32_t railBit = (data & 0x1) ;
                data = data >> 1;
                if ( railBit == 1 )  {
                    // Adding RAIL Start Number 16
                    hcLcUhc.rails[uhcIndex].push_back( idx + HDDPS_HCLC_RAIL_START );
                    //LOG("debug") << "hcUhc.rails[uhcIndex] " << hcUhc.rails[uhcIndex][hcUhc.rails[uhcIndex].size()-1];
                } 
            }
        } else if ( (offset >= VirtualDutId0) && (offset <= VirtualDutId7) ) {
            size_t dutDomainIdIndex = ( offset - VirtualDutId0 )/4;
            hcLcUhc.dutDomainId[ dutDomainIdIndex ] = data & 0x3F ;                   // First 6 bits are Dut Domain Id
            vlcUhc.dutDomainId[ dutDomainIdIndex ] = data & 0x3F ;                   // First 6 bits are Dut Domain Id
            data = data >> 6 ;                                              // Left shifting 6 bits to get 7th bit DUT Domain Id valid
            hcLcUhc.dutDomainIdValid[ dutDomainIdIndex ] = data & 0x1;
            //LOG("debug") << "hcUhc.dutDomainId" << dutDomainIdIndex << " is " << hcUhc.dutDomainId[ dutDomainIdIndex ];
            //LOG("debug") << "hcUhc.dutDomainIdValid" << dutDomainIdIndex << " is " << hcUhc.dutDomainIdValid[ dutDomainIdIndex ];
            vlcUhc.dutDomainIdValid[ dutDomainIdIndex ] = data & 0x1;
        } else if (offset == ExecuteTrigger) {
            offsetAddr = (data & 0x7FFFFFFF)/4;   // First 30 bit are offset address, 4 is 4 bytes which is one dword.
            tqaddr += offsetAddr; // moving tqaddr to the offset address as a tq start adreess
            //LOG("info") << " TQADDRESS is " << tqaddr;
        } else if ( offset == RailMasterMask ) {
            size_t railNum = 0;
            std::vector<size_t> masterGroup;
            std::vector<size_t> slaveGroup;
            
            while( railNum < HDDPS_DAUGHTERBOARD_HC_RAILS ) {               // Putting all the master rail number into masterGroup vector
                size_t railBit = ( data &  0x1 ) ;  
                data = data >> 1;
                if (railBit == 1) {
                    masterGroup.push_back(railNum);        // FInd all the Master rails
                }
                railNum += 1;
            }
            if ( masterGroup.empty() ) LOG("error") << "No Master Rail is Set" ;
            for( size_t railIdx=0 ; railIdx != masterGroup.size() ; railIdx++ ) {   // Looping over all the master rail number to calculate slave rails.
                size_t currentRail = 0;
                size_t nextRail = 1;
                size_t slaveRails = 1; 
                size_t masterGroupSize = masterGroup.size();
                if ( railIdx != (masterGroupSize-1) ) {                             // case except the last rail.
                    currentRail = masterGroup[railIdx];
                    nextRail = masterGroup[railIdx+1];
                } else if ( masterGroup[ masterGroupSize - 1] != 9 ) {   // 9 is here rail 9 which is last rail. case when the last master rail is not rail 9.
                    currentRail = masterGroup[railIdx] ;
                    nextRail = 10; //
                } else if ( masterGroup[ masterGroupSize - 1] == 9 ) {  // case when last master rail is rail 9.
                    currentRail = masterGroup[railIdx];
                    nextRail = 10; //Ficticious Rail just to make slaveRails 1
                }
                slaveRails = nextRail - currentRail;                    // slave rails are between master rails.
                if ( slaveRails > 1 ) {
                    for( size_t slaveRailNum=(currentRail + 1); slaveRailNum < nextRail ; slaveRailNum++ ) {  // CurrentRail + 1 ==> Start of Slave Rail
                        slaveGroup.push_back( slaveRailNum + HDDPS_DAUGHTERBOARD_HC_RAIL_START     );
                    }
                    mbLcTqEngine.masterSlaveGanging.insert( std::make_pair(currentRail + HDDPS_DAUGHTERBOARD_HC_RAIL_START, slaveGroup ) );
                    //MasterSlaveGanging
                    mbLcTqEngine.gangEnabled = true;
                    slaveGroup.clear();                 // Clear the vector for next master-slave dict.
                } else if (slaveRails == 1) {
                    slaveGroup.clear();
                    mbLcTqEngine.masterSlaveGanging.insert( std::make_pair(currentRail + HDDPS_DAUGHTERBOARD_HC_RAIL_START, slaveGroup ) );
                    //MasterSlaveGanging
                    //mbLcTqEngine.gangEnabled = true;
                }
            }
            /*
            // Printing out Master and slave rails. 
            for( auto it=mbLcTqEngine.masterSlaveGanging.begin() ; it != mbLcTqEngine.masterSlaveGanging.end() ; ++it) {
                LOG("info") << "Master Rail is " << ( it->first ) ;
                LOG("info") << "gangEnabled is " << mbLcTqEngine.gangEnabled;
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                        LOG("info") << "Slave Rail is " << *iter; 
                    }
                }
            }
            */
        } else if ( (offset >= HclcDut0HeaderRegionBase) && (offset <= HclcDut7SampleRegionSize) ) {
            size_t uhcIdx = ( offset -  HclcDut0HeaderRegionBase ) / 16;
            size_t selection = ( offset -  HclcDut0HeaderRegionBase ) % 16;
            // Sample Region Base
            if ( selection == 0 ) {
                hcLcHeaderSampleRegionSet.DutHeaderRegionBase[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Header Region Base is " << data;
            // Header Region Size
            } else if ( selection == 4) {
                hcLcHeaderSampleRegionSet.DutHeaderRegionSize[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Header Region Size is " << data;
            // Sample Region Base
            } else if ( selection == 8) {
                hcLcHeaderSampleRegionSet.DutSampleRegionBase[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Sample Region Base is " << data;
            // Sample Region Size
            } else if ( selection == 12) {
                hcLcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Sample Region Size is " << data;
            }

        // VLC BAR WRITE START
        } else if ( (offset >= VlcDutId0RailConfig) && (offset <= VlcDutId7RailConfig) ) {
            size_t uhcIndex = (offset -  VlcDutId0RailConfig)/4;
            vlcUhc.rails[uhcIndex].clear();
            for( size_t idx=0 ; idx <=  HDDPS_MOTHERBOARD_VLC_RAILS; idx++) {
                uint32_t railBit = (data & 0x1) ;
                data = data >> 1;
                if ( railBit == 1 )  {
                    vlcUhc.rails[uhcIndex].push_back( idx );
                    //LOG("info") << "vlcUhc.rails[uhcIndex] " << mbVlcTqEngine.vlcUhc.rails[uhcIndex][mbVlcTqEngine.vlcUhc.rails[uhcIndex].size()-1];
                } 
            }
        }  else if ( (offset >= VlcDut0HeaderRegionBase) && (offset <= VlcDut7SampleRegionSize ) ) {
            size_t uhcIdx = ( offset -  VlcDut0HeaderRegionBase ) / 16;
            size_t selection = ( offset -  VlcDut0HeaderRegionBase ) % 16;
            // Sample Region Base
            if ( selection == 0 ) {
                vlcHeaderSampleRegionSet.DutHeaderRegionBase[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Header Region Base is " << data;
            // Header Region Size
            } else if ( selection == 4) {
                vlcHeaderSampleRegionSet.DutHeaderRegionSize[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Header Region Size is " << data;
            // Sample Region Base
            } else if ( selection == 8) {
                vlcHeaderSampleRegionSet.DutSampleRegionBase[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Sample Region Base is " << data;
            // Sample Region Size
            } else if ( selection == 12) {
                vlcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Sample Region Size is " << data;
            }
        }
    }
}

DaughterboardSimulator::DaughterboardSimulator(int slot, int subslot, size_t memorySize) : HddpsSimulator(slot, subslot, memorySize), dbHcTqEngine(slot, subslot, this)
{
    for (size_t rail = 16; rail < 16 + HDDPS_DAUGHTERBOARD_HC_RAILS; rail++) {
        //hcRail[rail] = HcRail(rail, this);
        hc[rail] = (HcRail(rail, this));
    }
    for (size_t railNum = 16; railNum < 16 + HDDPS_DAUGHTERBOARD_HC_RAILS; railNum++) {
        dbHcTqEngine.virtualRail[railNum] = &(hc[railNum]) ;
    }
}

std::string DaughterboardSimulator::Name() const
{
    std::stringstream ss;
    ss << "HDDPS Daughterboard Simulator (" << slot << ", " << subslot << ")";
    return ss.str();
}

std::set<size_t> DaughterboardSimulator::GetSampleEngineInUse()
{
    return hcLcSampleEngine.sampleEngineInUseSet;
}

std::vector<float>* DaughterboardSimulator::GetSampleData(size_t sampleEngineNumber)
{
    hcLcSampleEngine.MultipleSampleDataCreation( sampleEngineNumber );
    return &(hcLcSampleEngine.sampleEngineData[sampleEngineNumber]);
}

std::vector<size_t>* DaughterboardSimulator::GetSampleTime(size_t sampleEngineNumber)
{
    hcLcSampleEngine.MultipleSampleTimeCreation( sampleEngineNumber );
    return &(hcLcSampleEngine.sampleEngineTime[sampleEngineNumber]);
}

std::unordered_map<size_t, uint64_t> DaughterboardSimulator::GetValidCycle(size_t sampleEngineNumber)
{
    hcLcSampleEngine.ValidCycleCreation( sampleEngineNumber );
    return (hcLcSampleEngine.validCycleDict[sampleEngineNumber]);
}

std::unordered_map<size_t, std::vector<size_t>> DaughterboardSimulator::GetUhcRails(size_t uhcNumber)
{
    return hcLcSampleEngine.uhcSampleEngine.rails[uhcNumber];
}

std::vector<uint16_t> DaughterboardSimulator::GetSampleCounts(size_t uhcNumber)
{
    return hcLcSampleEngine.sampleEngineConfig[uhcNumber].sampleCount;
}

void DaughterboardSimulator::SetSampleRegionSize( size_t uhcIdx, size_t sampleRegionSize)
{
        hcLcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx] = sampleRegionSize;
        //LOG("info") << " header sample region actual size is " << hcLcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx];
        //LOG("info") << " user defined size is " << hcLcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx];
        // Sample out of bounds alarm from actual sample region size beyond the user defined sample region size 
        if ( hcLcHeaderSampleRegionSet.ActualSampleRegionSize[uhcIdx] > hcLcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx] ) {
            hddpsAlarms.globalAlarms.AlarmSet( 10 ); 
            hddpsAlarms.hclcSampleAlarms.AlarmSet( 2*static_cast<uint32_t>(uhcIdx) + 1); 
        // Sample out of bounds alarm from actual sample header region size beyond the user defined sample header region size 
        } else if (hcLcSampleEngine.sampleEngineConfig[uhcIdx].sampleStart.size()*SAMPLEHEADERSIZE > hcLcHeaderSampleRegionSet.DutHeaderRegionSize[uhcIdx] ) {
            hddpsAlarms.globalAlarms.AlarmSet( 10 ); 
            hddpsAlarms.hclcSampleAlarms.AlarmSet( 2*static_cast<uint32_t>(uhcIdx) + 1); 
        }
}

void DaughterboardSimulator::BarWrite(uint32_t bar, uint32_t offset, uint32_t data)
{
    if (bar == 1) {
        if ( (offset >= HclcDutId0RailConfig) && (offset <= HclcDutId7RailConfig) ) {
            size_t uhcIndex = (offset -  HclcDutId0RailConfig)/4;
            hcLcUhc.rails[uhcIndex].clear();
            for( size_t idx=0 ; idx <=  HDDPS_DAUGHTERBOARD_HC_RAILS; idx++) {
                uint32_t railBit = (data & 0x1) ;
                data = data >> 1;
                if ( railBit == 1 )  {
                    hcLcUhc.rails[uhcIndex].push_back( idx + HDDPS_HCLC_RAIL_START);
                    //LOG("debug") << "hcUhc.rails[uhcIndex] " << hcUhc.rails[uhcIndex][hcUhc.rails[uhcIndex].size()-1];
                } 
            }
        } else if ( (offset >= VirtualDutId0) && (offset <= VirtualDutId7) ) {
            size_t dutDomainIdIndex = ( offset - VirtualDutId0 )/4;
            hcLcUhc.dutDomainId[ dutDomainIdIndex ] = data & 0x3F ;                   // First 6 bits are Dut Domain Id
            data = data >> 6 ;                                              // Left shifting 6 bits to get 7th bit DUT Domain Id valid
            hcLcUhc.dutDomainIdValid[ dutDomainIdIndex ] = data & 0x1;
            //LOG("debug") << "hcUhc.dutDomainId" << dutDomainIdIndex << " is " << hcUhc.dutDomainId[ dutDomainIdIndex ];
            //LOG("debug") << "hcUhc.dutDomainIdValid" << dutDomainIdIndex << " is " << hcUhc.dutDomainIdValid[ dutDomainIdIndex ];
        } else if (offset == ExecuteTrigger) {
            offsetAddr = (data & 0x7FFFFFFF)/4;   // First 30 bit are offset address, 4 is 4 bytes which is one dword.
            tqaddr += offsetAddr; // moving tqaddr to the offset address as a tq start adreess
        } else if ( offset == RailMasterMask ) {
            size_t railNum = 0;
            std::vector<size_t> masterGroup;
            std::vector<size_t> slaveGroup;
            
            while( railNum < HDDPS_DAUGHTERBOARD_HC_RAILS ) {               // Putting all the master rail number into masterGroup vector
                size_t railBit = ( data &  0x1 ) ;  
                data = data >> 1;
                if (railBit == 1) {
                    masterGroup.push_back(railNum);        // FInd all the Master rails
                }
                railNum += 1;
            }
            if ( masterGroup.empty() ) LOG("error") << "No Master Rail is Set" ;
            for( size_t railIdx=0 ; railIdx != masterGroup.size() ; railIdx++ ) {   // Looping over all the master rail number to calculate slave rails.
                size_t currentRail = 0;
                size_t nextRail = 1;
                size_t slaveRails = 1; 
                size_t masterGroupSize = masterGroup.size();
                if ( railIdx != (masterGroupSize-1) ) {                             // case except the last rail.
                    currentRail = masterGroup[railIdx];
                    nextRail = masterGroup[railIdx+1];
                } else if ( masterGroup[ masterGroupSize - 1] != 9 ) {   // 9 is here rail 9 which is last rail. case when the last master rail is not rail 9.
                    currentRail = masterGroup[railIdx] ;
                    nextRail = 10; //
                } else if ( masterGroup[ masterGroupSize - 1] == 9 ) {  // case when last master rail is rail 9.
                    currentRail = masterGroup[railIdx];
                    nextRail = 10; //Ficticious Rail just to make slaveRails 1
                }
                slaveRails = nextRail - currentRail;                    // slave rails are between master rails.
                if ( slaveRails > 1 ) {
                    for( size_t slaveRailNum=(currentRail + 1); slaveRailNum < nextRail ; slaveRailNum++ ) {  // CurrentRail + 1 ==> Start of Slave Rail
                        slaveGroup.push_back( slaveRailNum + HDDPS_DAUGHTERBOARD_HC_RAIL_START     );
                    }
                    dbHcTqEngine.masterSlaveGanging.insert( std::make_pair(currentRail + HDDPS_DAUGHTERBOARD_HC_RAIL_START, slaveGroup ) );
                    //MasterSlaveGanging
                    dbHcTqEngine.gangEnabled = true;
                    slaveGroup.clear();                 // Clear the vector for next master-slave dict.
                } else if (slaveRails == 1) {
                    slaveGroup.clear();
                    dbHcTqEngine.masterSlaveGanging.insert( std::make_pair(currentRail + HDDPS_DAUGHTERBOARD_HC_RAIL_START, slaveGroup ) );
                }
            }
            /*
            // Printing out Master and slave rails. 
            for( auto it=dbHcTqEngine.masterSlaveGanging.begin() ; it != dbHcTqEngine.masterSlaveGanging.end() ; ++it) {
                LOG("info") << "Master Rail is " << ( it->first ) ;
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                        LOG("info") << "Slave Rail is " << *iter; 
                    }
                }
            }
            */
        } else if ( (offset >= HclcDut0HeaderRegionBase) && (offset <= HclcDut7SampleRegionSize ) ) {
            size_t uhcIdx = ( offset -  HclcDut0HeaderRegionBase ) / 16;
            size_t selection = ( offset -  HclcDut0HeaderRegionBase ) % 16;
            // Sample Region Base
            if ( selection == 0 ) {
                hcLcHeaderSampleRegionSet.DutHeaderRegionBase[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Header Region Base is " << data;
            // Header Region Size
            } else if ( selection == 4) {
                hcLcHeaderSampleRegionSet.DutHeaderRegionSize[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Header Region Size is " << data;
            // Sample Region Base
            } else if ( selection == 8) {
                hcLcHeaderSampleRegionSet.DutSampleRegionBase[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Sample Region Base is " << data;
            // Sample Region Size
            } else if ( selection == 12) {
                hcLcHeaderSampleRegionSet.DutSampleRegionSize[uhcIdx] = data;
                //LOG("info") << " BAR WRITE Sample Region Size is " << data;
            }
        }
    }
}

void DaughterboardSimulator::ExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write)
{
        dbHcTqEngine.CommonExecuteTriggerCommand(cmd, arg, data, write);    // HC Rail Commands Process
        if (dbHcTqEngine.stop == true) {
            stop = true;                //INIT
            dbHcTqEngine.stop = false;  //INIT
        }

}

void VlcHcLcTqExecutionEngine::CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write)
{
    // TODO
}

void VlcTqExecutionEngine::CommonExecuteTriggerCommand(uint8_t my_cmd, uint8_t arg, uint16_t data, uint8_t write)
{
    switch (my_cmd) {
        case SET_VOLTAGE: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( rail >= 16 ){
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            //LOG("info") << "I Set Voltage";
            virtualRail[rail]->SetVoltage(static_cast<float>(DecodeFixedPoint16(12, value)));
            break;
        }
        case SET_CURRENT_RANGE: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( rail >= 16 ){
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            //LOG("info") << "I Set Current Range";
            virtualRail[rail]->SetCurrentRange(value);
            break;
        }
        case SET_MODE: {
            // Set the mode of the rail to VFORCE, IFORCE or ISINK
            // 0: VFORCE, 1:IFORCE, 2:ISINK
            uint8_t rail = arg;
            uint16_t value = data;
            if ( rail >= 16 ){
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            //LOG("info") << "I Set Mode";
            virtualRail[rail]->SetMode(value);
            break;
        }
        case ENABLE_DISABLE_RAIL: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( rail >= 16 ){
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            //LOG("info") << "I Enable Rail";
            if ( BIT(value, 0) == 1 ) {
                virtualRail[rail]->EnableDisableRail(Enabled);
            } else if ( BIT(value,0) == 0 ) {
                virtualRail[rail]->EnableDisableRail(Disabled);
            }
            break;
        }
        case SET_CURRENT_CLAMP_HI: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( rail >= 16 ) {
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            if (virtualRail[rail]->railUnfoldedBit) {
                virtualRail[rail]->SetCurrentClampHigh(static_cast<float>(DecodeSFP(value)));
            }
            break;
        }
        case SET_CURRENT_CLAMP_LOW: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( rail >= 16 ) {
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            //LOG("info") << "I Curren Clamp Low";
            if (virtualRail[rail]->railUnfoldedBit) virtualRail[rail]->SetCurrentClampLow(static_cast<float>(DecodeSFP(value)));
            break;
        }
        case SET_SAFE_STATE: {
            // TODO : NOT Implemented in MACRO.rtq
            // Put the rail processor in safe state
            // Rail processor does not process any commands(except general communication command when the PCIE source flag is set)
            // until it receives a TQ notify command
            // Disable the Rail & ignore commands while this state.
            // 0-5 DUT Domain ID
            uint8_t dutDomainId = arg;
            bool dutDomainIdExist = false;
            // Looping over all 8 Uhcs to find the matching Dut domain Id.
            for(size_t uhcIdx=0 ; uhcIdx < HDDPS_UHCS ; uhcIdx++) {
                // Finding matching dut domain id for corresponding Uhc
                if (sim->vlcUhc.dutDomainId[uhcIdx] == dutDomainId) {
                    std::vector<size_t> uhcConfig = sim->vlcUhc.rails[uhcIdx];
                    // Looping over all the rails in matching Dut Domain Id to disable rails.
                    for(size_t railIdx=0 ; railIdx != uhcConfig.size() ; railIdx++) {   
                        size_t railNum = uhcConfig[railIdx];
                        virtualRail[railNum]->railUnfoldedBit = false;
                    }
                    dutDomainIdExist = true;
                }
            }
            if (!dutDomainIdExist) {
                LOG("error") << "DUt Domain Id " << dutDomainId << " doesn't exist in Dut Domain ID register during SET SAFE STATE rail command" ;
            }
            break;
        }
        case TIME_DELAY: {
            // VLC Rails don't have Slew function.
            // Inserts a number of cycles into a sequence of instructions
            // Bit 0-15: Number of microseconds of delay
            // On each cyle of the time delay, if the set safe stae condition triggered in the design(Set Safe Stae, Cross Board Fold,
            // or Alarm Safe State), the delay is stopped to allow the Safe State sequence to begin immediately
            // 0-5 DUT Domain ID
            uint8_t rail = arg;
            uint16_t value = data;  // Total Delay
            //LOG("info") << "I am a Time Delay";
            if ( rail >= 16 ) {
                LOG("error") << "VLC Rail must be between 0 and " << int(HDDPS_MOTHERBOARD_VLC_RAILS-1) ;
                return;
            }
            if ( !virtualRail[rail]->freeDriveEnable ) {
                virtualRail[rail]->cycle += value;
            }
            // Getting number of cycles to avoid sample comparison during rail folded state.
            if ( virtualRail[rail]->railEnabledBit == true ) { 
                //LOG("info") << "Valid Cycle is " << virtualRail[rail]->cycle;
                if (virtualRail[rail]->railUnfoldedBit == true ) {
                    sim->mbVlcSampleEngine.validSampleCycle[rail] = virtualRail[rail]->cycle ;
                } else if (virtualRail[rail]->railUnfoldedBit == false && virtualRail[rail]->lastValidCycle == false) { 
                    sim->mbVlcSampleEngine.validSampleCycle[rail] = virtualRail[rail]->cycle ;
                    virtualRail[rail]->lastValidCycle = true;
                }
            }
            break;
        } // case
        case TQ_NOTIFY: {
            uint8_t rail = arg;
            uint16_t value = data;
            bool dutDomainIdExist = false;
            size_t dutDomainId = SLICE(value, 6, 1);             // Bit 1 to 6 assigned to Dut Domain Id
            size_t triggerAlarm = SLICE(value, 0, 0);            // Bit 0 is for 'Generate Alarm trigger pulse bit'
            // Daughterboard TQ_Notify Implementation
            //LOG("info") << "I am a TQ Notify";
            if (rail < 16) {
                for(size_t uhcIdx=0 ; uhcIdx < HDDPS_UHCS ; uhcIdx++) {                     // Looping over all 8 Uhcs to find the matching Dut domain Id.
                    if (sim->vlcUhc.dutDomainId[uhcIdx] == dutDomainId) {                         // If matching dut domain id with SET_SAFE_STATE found, 
                        std::vector<size_t> uhcConfig = sim->vlcUhc.rails[uhcIdx];
                        for(size_t railIdx=0 ; railIdx != uhcConfig.size() ; railIdx++) {   // Looping over all the rails in matching Dut Domain Id to disable rails.
                            size_t railNum = uhcConfig[railIdx];
                            if ( triggerAlarm == 1) {
                                virtualRail[railNum]->railUnfoldedBit = true;
                                sim->hddpsAlarms.globalAlarms.AlarmClear( static_cast<uint32_t>(uhcIdx) ); // Clearing tq_notify_alarm_uhc alarm
                            } else {
                                virtualRail[railNum]->railUnfoldedBit = false ;
                                sim->hddpsAlarms.globalAlarms.AlarmSet( static_cast<uint32_t>(uhcIdx) ); // Setting tq_notify_alarm_uhc alarm
                            }
                        }
                        dutDomainIdExist = true;
                    }
                }
                if (!dutDomainIdExist) {
                    LOG("error") << "DUt Domain Id " << dutDomainId << " doesn't exist in Dut Domain ID register during TQ_NOTIFY rail command." ;
                }

            }
            break;
        }
        case TRIGGER_Q_COMPLETE: {
            stop = true;
            break;
        }
        case 1: {
            uint16_t cmd;
            uint16_t engineNumber;
            //LOG("info") << "I Sample Engine";
            if (arg == VlcSampleEngineReset) {
                cmd = arg;
                engineNumber = (arg/7); // Same as Uhc Number, 7 here is distance between Registers in symbols.py. Hclc0SampleCount=0x0, HclcSampleStart=0x6
            // Case between sample engine 0 and sample engine 3
            // Sample engine 3 and sample engine 4 registers aren't continuous.
            } else if ( arg >= Vlc0SampleCount && arg <= Vlc3SampleStart){        // Vlc3 ends with 0x5b and Vlc4 starts with 0x60
                uint16_t tempArg = arg - Vlc0SampleCount; 
                cmd = (tempArg % 7); // Using Vlc0 Sampling engine for all other sampling engines
                //engineNumber = static_cast<size_t>(arg / 7); // Same as Uhc Number, 7 here is distance between Registers
                engineNumber = (tempArg/7); // Same as Uhc Number, 7 here is distance between Registers
                sim->mbVlcSampleEngine.sampleEngineInUseVector.push_back(engineNumber);
                LOG("debug") << "Engine number is " << engineNumber;
                LOG("debug") << "Engine command is " << cmd;
            // Covering sample engine 4 to sample engine 7.
            } else {
                cmd = ( (arg - Vlc4SampleCount)%7 );
                engineNumber = ( (arg - Vlc4SampleCount)/7 + 4);  // 4 is sample engine Number 4 as the starting engine number for this group. 
                sim->mbVlcSampleEngine.sampleEngineInUseVector.push_back(engineNumber);
                LOG("debug") << "Hclc4 Engine number is " << engineNumber;
                LOG("debug") << "Hclc4 Engine command is " << cmd;
            }
            uint16_t value = data;
            switch (cmd) {
                case VlcSampleEngineReset: {
                    for( size_t idx=0 ; idx <=  HDDPS_UHCS-1; idx++) {
                        uint32_t uhcBit = (data & 0x1) ;
                        data = data >> 1;
                        if ( uhcBit == 1 )  {
                            sim->mbVlcSampleEngine.sampleEngineConfig[idx].sampleEngineReset = true;
                            //LOG("debug") << "hcUhc.rails[uhcIndex] " << hcUhc.rails[uhcIndex][hcUhc.rails[uhcIndex].size()-1];
                        } 
                    }
                    break;
                    }
                // Insert a delay value in microseconds into sampling engine for the corresponding UHC.
                // Delay inserted before a Sample Start in order to delay when the sampling engine for a particular UHC begins storing samples.
                case (Vlc0SampleDelay-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleDelay.push_back(value & Mask16Bit);  // All 16bits for sample delay
                    //LOG("debug") << "sample Delay is " << samplingEngine[engineNumber].sampleDelay;
                    break;
                    } 
                case (Vlc0SampleCount-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleCount.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                case (Vlc0SampleRate-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleRate.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                case (Vlc0SampleMetadataHi-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleMetaHi.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                case (Vlc0SampleMetadataLo-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleMetaLo.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                // Sample Rail Select has different valid bits for HCLC and VLS
                // HCLC: First 10 bits,  VLC: 16 bits
                case (Vlc0SampleRailSelect-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleRailSelect.push_back( value & Mask16Bit);  // First 10 bits for HCLC
                    // TODO : Something to remove voltage or current objects.
                    // Since tester memory is much bigger and HDDPS sample size small, it looks unnecesary to implement Rail select.
                    break;
                    } 
                // Any value in sample start field causes the Sampling Engine to run.
                case (Vlc0SampleStart-Vlc0SampleCount) : {
                    sim->mbVlcSampleEngine.sampleEngineConfig[engineNumber].sampleStart.push_back(value & Mask16Bit); // All 16 bits
                    break;
                    } 
            }
        break;
        }
    } // Switch
}

void HcLcTqExecutionEngine::CommonExecuteTriggerCommand(uint8_t my_cmd, uint8_t arg, uint16_t data, uint8_t write)
{
    switch (my_cmd) {
        case SET_VOLTAGE: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            virtualRail[rail]->SetVoltage(static_cast<float>(DecodeFixedPoint16(12, value)));
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if ( gangEnabled ) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetVoltage(static_cast<float>( DecodeFixedPoint16(12, value) ));
                    }
                }
            }
            break;
        }
        case SET_CURRENT: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            virtualRail[rail]->SetCurrent(static_cast<float>(DecodeSFP(value)));
            break;
        }
        case SET_CURRENT_RANGE: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            virtualRail[rail]->SetCurrentRange(value);
            break;
        }
        case SET_OVER_VOLTAGE: {
            // TODO
            // Adjust the upper voltage comparator window alarm level.
            // A linear range -8 to 8 V
            // Actual range is from -4.44 V to +8 V
            // Voltage in Q4.12 format
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            if (virtualRail[rail]->railUnfoldedBit) virtualRail[rail]->SetOverVoltage(static_cast<float>(DecodeFixedPoint16(12, value)));
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if (virtualRail[rail]->railUnfoldedBit && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetOverVoltage(static_cast<float>(DecodeFixedPoint16(12, value)));
                    }
                }
            }
            break;
        }
        case SET_UNDER_VOLTAGE: {
            // TODO
            // Same as Set_over_voltage
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            if (virtualRail[rail]->railUnfoldedBit) virtualRail[rail]->SetUnderVoltage(static_cast<float>(DecodeFixedPoint16(12, value)));
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if (virtualRail[rail]->railUnfoldedBit && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetUnderVoltage(static_cast<float>(DecodeFixedPoint16(12, value)));
                    }
                }
            }
            break;
        }
        case ENABLE_DISABLE_RAIL: {
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }

            //std::unordered_map<size_t, std::vector<size_t>>::const_iterator it = masterSlaveGanging.begin();
            //while ( it != masterSlaveGanging.end() ) {
            //    it -> first;
            //    it -> second;
            //    ++it;
            //}
            for( auto it=masterSlaveGanging.begin() ; it != masterSlaveGanging.end() ; ++it) {
                if (it->first == rail && BIT(value,0) == 1 ) {                  // case 1 : master Rail and Bit 0 is ENABLE Bit
                    virtualRail[rail]->EnableDisableRail(Enabled);
                    if ( BIT(value, 1) == 1 ) {                                 // case 2 : Slaves cascaded
                        for(size_t slaveIdx = 0 ; slaveIdx != it->second.size() ; slaveIdx++) {
                            size_t slaveRailNum = it->second[slaveIdx];
                            virtualRail[slaveRailNum]->EnableDisableRail(Enabled);
                         }
                    } 
                    //else if ( BIT(value, 1) == 0 ){                           // case 3 : Slaved not cascaded
                        // for(size_t slaveIdx = 0 ; slaveIdx != it->second.size() ; slaveIdx++) {
                            // size_t slaveRailNum = it->second[slaveIdx];
                            
                            // // TODO : Letting it be default status. What is default status?
                         // }

                    //}
                } else if (it->first == rail && BIT(value,0) == 0 ) {           // case 4: master rail and Bit 0 is is DISABLE Bit
                    //if (virtualRail[rail]->railEnabledBit == true) {
                        //virtualRail[rail]->SetVoltage(0.0);   // Disabled Rail Set Voltage to 0.0
                    //}
                    virtualRail[rail]->EnableDisableRail(Disabled);
                    if ( BIT(value, 1) == 1 ) {                                 // case 5 : Slaves cascaded
                        for(size_t slaveIdx = 0 ; slaveIdx != it->second.size() ; slaveIdx++) {
                            size_t slaveRailNum = it->second[slaveIdx];
                            virtualRail[slaveRailNum]->EnableDisableRail(Disabled);
                         }
                    } 
                    // else if ( BIT(value, 1) == 0 ){                           // case 6 : slaves not cascaded
                        // for(size_t slaveIdx = 0 ; slaveIdx != it->second.size() ; slaveIdx++) {
                            // size_t slaveRailNum = it->second[slaveIdx];
                            // // TODO : Letting it be default status. What is default status?
                         // }
                    // }
                }
            }
            break;
        }
        case SET_CURRENT_CLAMP_HI: {
            uint8_t rail = arg;
            uint16_t value = data;
            std::unordered_map<size_t, std::vector<size_t>>::iterator my_it = masterSlaveGanging.find(rail);
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            if (virtualRail[rail]->railUnfoldedBit && my_it != masterSlaveGanging.end()) {
                virtualRail[rail]->SetCurrentClampHigh(static_cast<float>(DecodeSFP(value)));
            }
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if (virtualRail[rail]->railUnfoldedBit && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetCurrentClampHigh(static_cast<float>( DecodeSFP(value) ));
                    }
                }
            }
            break;
        }
        case SET_CURRENT_CLAMP_LOW: {
            uint8_t rail = arg;
            uint16_t value = data;
            std::unordered_map<size_t, std::vector<size_t>>::iterator t_it = masterSlaveGanging.find(rail);
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(HDDPS_DAUGHTERBOARD_HC_RAIL_START + HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            if (virtualRail[rail]->railUnfoldedBit && t_it != masterSlaveGanging.end()) virtualRail[rail]->SetCurrentClampLow(static_cast<float>(DecodeSFP(value)));
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if (virtualRail[rail]->railUnfoldedBit && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetCurrentClampLow(static_cast<float>( DecodeSFP(value) ));
                    }
                }
            }
            break;
        }
        // case ENABLE_DISABLE_CURRENT_CLAMP: {
            // TODO
            // Updating CLEN(Bit 4) value in AD5560 Device DPS Register 1 to turn clamps on/off
            // break;
        // }
        case GENERAL_COMMUNICATION: {
            // TODO
            // Lower 7 bits of register address
            // associated device register and internally cached register updated with command data.
            break;
        }
        case SET_SAFE_STATE: {
            // TODO : NOT Implemented in MACRO.rtq
            // Put the rail processor in safe state
            // Rail processor does not process any commands(except general communication command when the PCIE source flag is set)
            // until it receives a TQ notify command
            // Disable the Rail & ignore commands while this state.
            // 0-5 DUT Domain ID
            uint8_t dutDomainId = arg;
            bool dutDomainIdExist = false;
            for(size_t uhcIdx=0 ; uhcIdx < HDDPS_UHCS ; uhcIdx++) {                     // Looping over all 8 Uhcs to find the matching Dut domain Id.
                if (sim->hcLcUhc.dutDomainId[uhcIdx] == dutDomainId) {                         // If matching dut domain id with SET_SAFE_STATE found, 
                    std::vector<size_t> uhcConfig = sim->hcLcUhc.rails[uhcIdx];
                    for(size_t railIdx=0 ; railIdx != uhcConfig.size() ; railIdx++) {   // Looping over all the rails in matching Dut Domain Id to disable rails.
                        size_t railNum = uhcConfig[railIdx] + HDDPS_DAUGHTERBOARD_HC_RAIL_START;
                        virtualRail[railNum]->railUnfoldedBit = false;
                    }
                    dutDomainIdExist = true;
                }
            }
            if (!dutDomainIdExist) {
                LOG("error") << "DUt Domain Id " << dutDomainId << " doesn't exist in Dut Domain ID register during SET SAFE STATE rail command" ;
            }
            break;
        }
        case SET_MODE: {
            // Set the mode of the rail to VFORCE, IFORCE or ISINK
            // 0: VFORCE, 1:IFORCE, 2:ISINK
            uint8_t rail = arg;
            uint16_t value = data;
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                LOG("error") << "HC/LC Rail must be between 16 and " << int(16+HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            virtualRail[rail]->SetMode(value);
            break;
        }
        // case SET_SLEW_RATE: {
            // TODO
            // Update the slew rate field of DPS register 2
            // break;
        // }
        case ENABLE_VOLTAGE_COMPARE_ALARM: {
            // TODO
            // Unmask the voltage comparison alarms
            // the mask setting can be read from BAR2 CP Alarm Mask Register
            // Voltage Comparison Alarms masked whenever a command other than TQ Notify executed or
            // Rail goes into Safe State
            uint8_t rail = arg;
            uint16_t value = data;
            virtualRail[rail]->EnableVoltageCompareAlarm(value);
            break;
        }
        case START_FREE_DRIVE: {
            uint8_t rail = arg;
            uint16_t value = data;
            virtualRail[rail]->freeDriveEnable = true;
            virtualRail[rail]->freeDriveVoltage = static_cast<float>(DecodeFixedPoint16(12, value)); // - FreeDriveJumpVoltage;
            // MasterSlaveGanging for setting Free Drive voltage
            if ( gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                        virtualRail[*iter]->freeDriveVoltage = static_cast<float>(DecodeFixedPoint16(12, value)); // - FreeDriveJumpVoltage;
                    }
                }
            }
            // TODO
            // Save active Current Low/High Clamp
            // Set free drive current Low/High Clamp
            // Set CUrrent Range based on Set Free drive Current
            // Assuming Start_Free_Drive always starts with 0V instead of some voltage.
            break;
        }
        case END_FREE_DRIVE: {
            uint8_t rail=arg;

            virtualRail[rail]->freeDriveEnable = false;
            // If Vslew voltage is less than Vslew target voltage, raise a Vslew ramp alarm.
            float currentSampleVoltage = virtualRail[rail]->GetCurrentVoltage(slot, subslot, rail);
            float freeDriveTargetVoltage = virtualRail[rail]->freeDriveVoltage;
            if ( virtualRail[rail]->slewUp && currentSampleVoltage < freeDriveTargetVoltage ) {
                //sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
                //sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(rail) );// Indexing taken care by class itself
                //sim->hddpsAlarms.hclcAlarmsContents[rail - HDDPS_HCLC_RAIL_START].AlarmSet( 4 ); // Set Vslew ramp Alarm
                // MasterSlaveGanging for setting Free Drive voltage
                if ( gangEnabled) {
                    std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                    if ( !it->second.empty() ) {
                        for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                            //sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(*iter) );// Indexing taken care by class itself
                            //sim->hddpsAlarms.hclcAlarmsContents[*iter - HDDPS_HCLC_RAIL_START].AlarmSet( 4 ); // Set Vslew ramp Alarm
                        }
                    }
                }
            }
            if ( virtualRail[rail]->slewDown && currentSampleVoltage > freeDriveTargetVoltage ) {
                //sim->hddpsAlarms.globalAlarms.AlarmSet( 8 );    // 8 is hclc_rail_alarm in global alarms
                //sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(rail) );// Indexing taken care by class itself
                //sim->hddpsAlarms.hclcAlarmsContents[rail - HDDPS_HCLC_RAIL_START].AlarmSet( 4 ); // Set Vslew ramp Alarm
                // MasterSlaveGanging for setting Free Drive voltage
                if ( gangEnabled) {
                    std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                    if ( !it->second.empty() ) {
                        for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                            //sim->hddpsAlarms.hclcAlarms.AlarmSet( static_cast<uint32_t>(*iter) );// Indexing taken care by class itself
                            //sim->hddpsAlarms.hclcAlarmsContents[*iter - HDDPS_HCLC_RAIL_START].AlarmSet( 4 ); // Set Vslew ramp Alarm
                        }
                    }
                }
            }
            // TODO
            break;
        }
        case FORCE_CLAMP_FLUSH: {
            // TODO
            break;
        }
        case ENABLE_DISABLE_TRIGGER_SLAVES: {
            // TODO
            break;
        }
        case SET_FREE_DRIVE_HIGH_CURRENT: {
            uint8_t rail=arg;
            uint16_t value=data;
            virtualRail[rail]->SetFreeDriveHighCurrent(value);
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if (virtualRail[rail]->railUnfoldedBit && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetFreeDriveHighCurrent(value );
                    }
                }
            }
            // TODO
            break;
        }
        case SET_FREE_DRIVE_LOW_CURRENT: {
            uint8_t rail=arg;
            uint16_t value=data;
            virtualRail[rail]->SetFreeDriveLowCurrent(value);
            // MasterSlaveGanging: Implementing Slave Current setting following master rail
            if (virtualRail[rail]->railUnfoldedBit && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->SetFreeDriveLowCurrent(value );
                    }
                }
            }
            // TODO
            break;
        }
        case END_FREE_DRIVE_ENABLE: {
            // TODO
            break;
        }
        // case RAIL_CURRENT_CLAMP_ALARM: {
            // TODO
            // break;
        // }
        case TRIGGER_Q_COMPLETE: {
            stop = true;
            // Checking Voltage Compare Enable is On.
            for(size_t railNum=HDDPS_HCLC_RAIL_START; railNum < HDDPS_HCLC_RAIL_START+HDDPS_HCLC_RAILS; railNum++) {
                if (!sim->voltageCompareEnable[railNum]) {
                    // clearing every Voltage Comparison if voltageCompareEnable False
                    sim->hddpsAlarms.hclcAlarmsContents[railNum - HDDPS_HCLC_RAIL_START].AlarmClear( 2 ); // Set Comparator Low Alarm
                    sim->hddpsAlarms.hclcAlarmsContents[railNum - HDDPS_HCLC_RAIL_START].AlarmClear( 3 ); // Set Comparator High Alarm
                    // Checking hclcAlarmsContents are cleared after clearing comparator alarms.
                    // if zero, then clearing hclcAlarms 
                    if (sim->hddpsAlarms.hclcAlarmsContents[railNum - HDDPS_HCLC_RAIL_START].alarm == 0 ) {
                        sim->hddpsAlarms.hclcAlarms.AlarmClear( static_cast<uint32_t>(railNum) );
                    }
                }
            }
            // Clearing hclc_rail_alarms in global alarm if hclcAlarms is zero
            if (sim->hddpsAlarms.hclcAlarms.alarm == 0 ) {
                // Clearing hclc_rail_alarms in global alarm
                sim->hddpsAlarms.globalAlarms.AlarmClear(8); 
            }
            //for( size_t sampleNum=0 ; sampleNum != sampleEngineData[1].size() ; sampleNum++) {
            //    LOG("debug") << sampleNum /4 ; 
            //    LOG("debug") << sampleEngineData[1][ sampleNum ];
            //}
            /*
            for(size_t uhcIdx=0 ; uhcIdx < HDDPS_UHCS; uhcIdx++) {                                          // Looping through every UHCS
                std::vector<size_t> hcUhcSampleEngineNum = hcUhcSampleEngine.rails[uhcIdx];
                if (hcUhcSampleEngineNum.size() > 0) {                                                      // Checking each Uhc has at least one sample.
                    for(size_t railIdx = 0; railIdx != hcUhcSampleEngineNum.size() ; railIdx++) {           // Looping through enabled sample engine rails corrsponding Uhc.
                        size_t railNum = hcUhcSampleEngineNum[railIdx];
                        for( size_t sampleIdx = 0; sampleIdx != hc[railNum].sampleEngineVoltage.size(); sampleIdx++) {  // Looping through all the sample voltage
                            LOG("debug") << "sample Engine Voltage size for rail Num  " << railNum << " is " <<hc[railNum].sampleEngineVoltage.size() ;
                            LOG("debug") << "hcLcUhc sample data is  " << hc[railNum].sampleEngineVoltage[sampleIdx];
                        }
                    }
                }
            }
            */
            break;
        }
        case TQ_NOTIFY: {
            // TODO
            // Take Rails out of safe state
            // If TQ notify field is 0, send trigger to SW and set alarm bit
            // Re-Enable Commands and Optionally Send an Alarm 
            // Bits 15-7 Reserved
            // Bits 6-1 DUT Domain ID
            // Bit 0  'Generate alarm trigger pulse' bit
            //          0 : Generate alarm trigger pulse
            //          1 : Do not generate alarm trigger pulse
            // Only Execute if 'DUT domain field ID' matches of the DUT Domain ID of the rail processor
            // Alarm Trigger Pulse causes the alarms handler to generate an alarm trigger
            
            uint8_t rail = arg;
            uint16_t value = data;
            bool dutDomainIdExist = false;
            size_t dutDomainId = SLICE(value, 6, 1);             // Bit 1 to 6 assigned to Dut Domain Id
            size_t triggerAlarm = SLICE(value, 0, 0);            // Bit 0 is for 'Generate Alarm trigger pulse bit'
            // Daughterboard TQ_Notify Implementation
            if (rail >= 16) {
                for(size_t uhcIdx=0 ; uhcIdx < HDDPS_UHCS ; uhcIdx++) {                     // Looping over all 8 Uhcs to find the matching Dut domain Id.
                    if (sim->hcLcUhc.dutDomainId[uhcIdx] == dutDomainId) {                         // If matching dut domain id with SET_SAFE_STATE found, 
                        std::vector<size_t> uhcConfig = sim->hcLcUhc.rails[uhcIdx];
                        for(size_t railIdx=0 ; railIdx != uhcConfig.size() ; railIdx++) {   // Looping over all the rails in matching Dut Domain Id to disable rails.
                            size_t railNum = uhcConfig[railIdx] ;
                            if ( triggerAlarm == 1) {
                                virtualRail[railNum]->railUnfoldedBit = true;
                                //virtualRail[railNum]->railEnabledBit = true;
                                sim->hddpsAlarms.globalAlarms.AlarmClear( static_cast<uint32_t>(uhcIdx) ); // Clearing tq_notify_alarm_uhc alarm
                            } else {
                                virtualRail[railNum]->railUnfoldedBit = false ;
                                virtualRail[railNum]->railEnabledBit = false;
                                sim->hddpsAlarms.globalAlarms.AlarmSet( static_cast<uint32_t>(uhcIdx) ); // Setting tq_notify_alarm_uhc alarm
                            }
                        }
                        dutDomainIdExist = true;
                    }
                }
                if (!dutDomainIdExist) {
                    LOG("error") << "DUt Domain Id " << dutDomainId << " doesn't exist in Dut Domain ID register during TQ_NOTIFY rail command." ;
                }

            }
            

            break;
        }
        // case SET_SLAVE_CARDS: {
            // TODO
            // break;
        // }
        // case SET_SLAVE_COUNT: {
            // TODO
            // break;
        // }
        // case SET_RAIL_DIRTY: {
            // TODO
            // break;
        // }
        // case ENABLE_DISABLE_HC_DRIVER: {
            // TODO
            // break;
        // }
        // case DISABLE_CURRENT_CLAMP: {
            // TODO
            // break;
        // }
        case ASSERT_TEST_RAIL: {
            // TODO
            break;
        }
        case 1: {
            uint16_t cmd;
            uint16_t engineNumber;
            if (arg == HclcSampleEngineReset) {
                cmd = arg;
                engineNumber = (arg/7); // Same as Uhc Number, 7 here is distance between Registers in symbols.py. Hclc0SampleCount=0x0, HclcSampleStart=0x6
            // Case between sample engine 0 and sample engine 3
            // Sample engine 3 and sample engine 4 registers aren't continuous.
            } else if ( arg >= Hclc0SampleCount && arg <= Hclc3SampleStart){        // Hclc3 ends with 0x1b and hclc4 starts with 0x20
                cmd = (arg % 7); // Using Hclc0 Sampling engine for all other sampling engines
                //engineNumber = static_cast<size_t>(arg / 7); // Same as Uhc Number, 7 here is distance between Registers
                engineNumber = (arg/7); // Same as Uhc Number, 7 here is distance between Registers
                sim->hcLcSampleEngine.sampleEngineInUseVector.push_back(engineNumber);
                LOG("debug") << "Engine number is " << engineNumber;
                LOG("debug") << "Engine command is " << cmd;
            // Covering sample engine 4 to sample engine 7.
            } else {
                cmd = ( (arg - Hclc4SampleCount)%7 );
                engineNumber = ( (arg - Hclc4SampleCount)/7 + 4);  // 4 is sample engine Number 4 as the starting engine number for this group. 
                sim->hcLcSampleEngine.sampleEngineInUseVector.push_back(engineNumber);
                LOG("debug") << "Hclc4 Engine number is " << engineNumber;
                LOG("debug") << "Hclc4 Engine command is " << cmd;
            }
            uint16_t value = data;
            switch (cmd) {
                case HclcSampleEngineReset: {
                    for( size_t idx=0 ; idx <=  HDDPS_UHCS-1; idx++) {
                        uint32_t uhcBit = (data & 0x1) ;
                        data = data >> 1;
                        if ( uhcBit == 1 )  {
                            sim->hcLcSampleEngine.sampleEngineConfig[idx].sampleEngineReset = true;
                            //LOG("debug") << "hcUhc.rails[uhcIndex] " << hcUhc.rails[uhcIndex][hcUhc.rails[uhcIndex].size()-1];
                        } 
                    }
                    break;
                    }
                // Insert a delay value in microseconds into sampling engine for the corresponding UHC.
                // Delay inserted before a Sample Start in order to delay when the sampling engine for a particular UHC begins storing samples.
                case Hclc0SampleDelay: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleDelay.push_back(value & Mask16Bit);  // All 16bits for sample delay
                    //LOG("debug") << "sample Delay is " << sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleDelay;
                    break;
                    } 
                case Hclc0SampleCount: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleCount.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                case Hclc0SampleRate: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleRate.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                case Hclc0SampleMetadataHi: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleMetaHi.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                case Hclc0SampleMetadataLo: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleMetaLo.push_back(value & Mask16Bit);  // All 16bits
                    break;
                    } 
                // Sample Rail Select has different valid bits for HCLC and VLS
                // HCLC: First 10 bits,  VLC: 16 bits
                case Hclc0SampleRailSelect: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleRailSelect.push_back( value & Mask10Bit);  // First 10 bits for HCLC
                    // TODO : Something to remove voltage or current objects.
                    // Since tester memory is much bigger and HDDPS sample size small, it looks unnecesary to implement Rail select.
                    break;
                    } 
                // Any value in sample start field causes the Sampling Engine to run.
                case Hclc0SampleStart: {
                    sim->hcLcSampleEngine.sampleEngineConfig[engineNumber].sampleStart.push_back(value & Mask16Bit); // All 16 bits
                    break;
                    } 
            }
        break;
        }
        case 9 : case 10 : case 11 : case 12 : case 13 : case 14 : case 15: case 16: case 17: case 18: {
            uint16_t cmd = arg;
            uint16_t value = data;
            switch (cmd) {
                case RAMP_STEP_SIZE: {
                    voltageSlewRampStepSize = HDDPS_DAUGHTERBOARD_HC_SLEW_RAMP_STEP * value;
                    //static_cast<float>(DecodeFixedPoint16(12, value))
                
                break;
                }
            }
        break;                             
        }
//        default: {
//            std::stringstream ss;
//            ss << "Unsupported cmd=" << Hex(cmd);
//            throw std::runtime_error(ss.str());
//        }
    }  // case statement
}      // function

void LcTqExecutionEngine::CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write)
{
    HcLcTqExecutionEngine::CommonExecuteTriggerCommand(cmd, arg, data, write);
    switch (cmd) {
        case TIME_DELAY: {
            // LC Rails don't have Slew function.
            // Inserts a number of cycles into a sequence of instructions
            // Bit 0-15: Number of microseconds of delay
            // On each cyle of the time delay, if the set safe stae condition triggered in the design(Set Safe Stae, Cross Board Fold,
            // or Alarm Safe State), the delay is stopped to allow the Safe State sequence to begin immediately
            // 0-5 DUT Domain ID
            uint8_t rail = arg;
            uint16_t value = data;  // Total Delay
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                // info instead of error to make sequencebreak(Setting time delay to all rails) error free
                LOG("info") << "HC/LC Rail must be between 16 and " << int(16+HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            if ( !virtualRail[rail]->freeDriveEnable ) {
                virtualRail[rail]->cycle += value;
            }
            // MasterSlaveGang: Implementing Slave time delay setting following master rail
            if (!virtualRail[rail]->freeDriveEnable && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->cycle += value;
                    }
                }
            }
            // Free Drive Case 
            if ( virtualRail[rail]->freeDriveEnable ) {                       
                for( size_t idx = 0; idx < value; idx++) {
                    // Advancing cycle before setting voltage.
                    virtualRail[rail]->cycle += 1; 
                    virtualRail[rail]->SetVoltage( virtualRail[rail]->freeDriveVoltage ) ;
                    // Ganging cases for free drive
                    if ( gangEnabled) {
                        std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                        if ( !it->second.empty() ) {
                            for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                                virtualRail[*iter]->cycle += 1; 
                                virtualRail[*iter]->SetVoltage( virtualRail[*iter]->freeDriveVoltage ) ;
                            }
                        }
                    }
                    //LOG("info") << " Free Drive Voltage " << hcLc[rail]->freeDriveVoltage ;
                    //LOG("info") << " Set voltage is " << hcLc[rail]->GetCurrentVoltage(slot, subslot, rail) ;
                    //LOG("info") << " Cycle is " << hcLc[rail]->cycle ;
                }
            }
            // Getting number of cycles to avoid sample comparison during rail folded state.
            if ( virtualRail[rail]->railEnabledBit == true ) { 
                //LOG("info") << "Valid Cycle is " << virtualRail[rail]->cycle;
                if (virtualRail[rail]->railUnfoldedBit == true ) {
                    sim->hcLcSampleEngine.validSampleCycle[rail] = virtualRail[rail]->cycle ;
                    // MasterSlaveGang: Implementing Slave time delay setting following master rail
                    if ( gangEnabled) {
                        std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                        if ( !it->second.empty() ) {
                            for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                                sim->hcLcSampleEngine.validSampleCycle[*iter] = virtualRail[*iter]->cycle ;
                            }
                        }
                    }
                    
                } else if (virtualRail[rail]->railUnfoldedBit == false && virtualRail[rail]->lastValidCycle == false) { 
                    sim->hcLcSampleEngine.validSampleCycle[rail] = virtualRail[rail]->cycle ;
                    virtualRail[rail]->lastValidCycle = true;
                    // MasterSlaveGang: Implementing Slave time delay setting following master rail
                    if ( gangEnabled) {
                        std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                        if ( !it->second.empty() ) {
                            for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                                sim->hcLcSampleEngine.validSampleCycle[*iter] = virtualRail[*iter]->cycle ;
                                virtualRail[*iter]->lastValidCycle = true;
                            }
                        }
                    }
                }
            }
            break;
        } // case
    } // Switch
}       // Function 

void HcTqExecutionEngine::CommonExecuteTriggerCommand(uint8_t cmd, uint8_t arg, uint16_t data, uint8_t write)
{
    HcLcTqExecutionEngine::CommonExecuteTriggerCommand(cmd, arg, data, write);
    switch (cmd) {
        case TIME_DELAY: {
            // TODO
            // Inserts a number of cycles into a sequence of instructions
            // Bit 0-15: Number of microseconds of delay
            // On each cyle of the time delay, if the set safe stae condition triggered in the design(Set Safe Stae, Cross Board Fold,
            // or Alarm Safe State), the delay is stopped to allow the Safe State sequence to begin immediately
            // 0-5 DUT Domain ID
            uint8_t rail = arg;
            uint16_t value = data;  // Total Delay
            if ( ( rail < 16) || ( rail >= 16 +  HDDPS_DAUGHTERBOARD_HC_RAILS) ){
                // info instead of error to make sequencebreak(Setting time delay to all rails) error free
                LOG("info") << "HC/LC Rail must be between 16 and " << int(16+HDDPS_DAUGHTERBOARD_HC_RAILS ) ;
                return;
            }
            if ( !virtualRail[rail]->freeDriveEnable ) {
                virtualRail[rail]->cycle += value;
            }
            // MasterSlaveGang: Implementing Slave time delay setting following master rail
            if (!virtualRail[rail]->freeDriveEnable && gangEnabled) {
                std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                if ( !it->second.empty() ) {
                    for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                       virtualRail[*iter]->cycle += value;
                    }
                }
            }
            float currentSampleVoltage = virtualRail[rail]->GetCurrentVoltage(slot, subslot, rail);
            // When there is initial voltage set before rail enabled.
            if (virtualRail[rail]->initialVoltageExist) {
                currentSampleVoltage = virtualRail[rail]->initialVoltage;
                // Once used, the initialVoltage doesn't exist any more.
                virtualRail[rail]->initialVoltageExist = false;
            } 
            if (currentSampleVoltage == 999.0f) currentSampleVoltage = 0.0f;
            // Ramp up: when current voltage is lower than target free drive voltage.
            if ( virtualRail[rail]->freeDriveEnable && currentSampleVoltage < virtualRail[rail]->freeDriveVoltage) {                       
                virtualRail[rail]->slewUp = true;
                float initialRampUpVoltage = FreeDriveJumpVoltage + currentSampleVoltage;
                for( size_t idx = 0; idx < value; idx++) {
                    float realTimeSampleVoltage = virtualRail[rail]->GetCurrentVoltage(slot, subslot, rail);
                    // current voltage is 999.0f, then set it to 0v.
                    if (realTimeSampleVoltage == 999.0f) realTimeSampleVoltage = 0.0f;
                    // Implementing voltage step function Index.
                    size_t voltageStepIndex = idx/VOLTAGESTEPTIME;
                    if (  realTimeSampleVoltage <= virtualRail[rail]->freeDriveVoltage + SLEWTOLERANCE ) {
                        // Advancing cycle before setting voltage.
                        virtualRail[rail]->cycle += 1; 
                        // Increasing voltage Slew Ramp step size every 128us.
                        virtualRail[rail]->SetVoltage( initialRampUpVoltage + voltageSlewRampStepSize*(voltageStepIndex+1) ) ;
                        //LOG("info") << " Free Drive Jump Voltage " << FreeDriveJumpVoltage ;
                        //LOG("info") << " Free Drive Voltage " << virtualRail[rail]->freeDriveVoltage ;
                        //LOG("info") << " voltage slew ramp step is " << voltageSlewRampStepSize;
                        //LOG("info") << " Set voltage is " << FreeDriveJumpVoltage + voltageSlewRampStepSize*(voltageStepIndex+1);
                        //LOG("info") << " Cycle is " << virtualRail[rail]->cycle ;
                    } else {
                        virtualRail[rail]->cycle += 1;
                        //LOG("info") << "Current voltage is  " << hcLc[rail]->GetCurrentVoltage(slot, subslot, rail);
                    }
                }
            // Ramp down: when current voltage is higher than target free drive voltage.
            } else if ( virtualRail[rail]->freeDriveEnable && currentSampleVoltage > virtualRail[rail]->freeDriveVoltage) {
                virtualRail[rail]->slewDown = true;
                float initialRampDownVoltage =  currentSampleVoltage;
                for( size_t idx = 0; idx < value; idx++) {
                    float realTimeSampleVoltage = virtualRail[rail]->GetCurrentVoltage(slot, subslot, rail);
                    if (realTimeSampleVoltage == 999.0f) realTimeSampleVoltage = 0.0f;
                    // Implementing voltage step function Index.
                    size_t voltageStepIndex = idx/VOLTAGESTEPTIME;
                    //LOG("info") << " real time sample voltage is " << realTimeSampleVoltage ;
                    if ( realTimeSampleVoltage >= virtualRail[rail]->freeDriveVoltage + SLEWTOLERANCE ) {
                        // Advancing cycle before setting voltage.
                        virtualRail[rail]->cycle += 1; 
                        virtualRail[rail]->SetVoltage(  ( initialRampDownVoltage - voltageSlewRampStepSize*(voltageStepIndex+1) ) );
                        //LOG("info") << "Intial Ramp Dow Voltage is" << initialRampDownVoltage;
                        //LOG("info") << " Negative Slew";
                        //LOG("info") << " Free Drive Voltage " << hcLc[rail]->freeDriveVoltage ;
                        //LOG("info") << " voltage slew ramp step is " << voltageSlewRampStepSize;
                        //LOG("info") << " Set voltage is " << ( initialRampDownVoltage - voltageSlewRampStepSize*(voltageStepIndex+1) );
                        //LOG("info") << " Cycle is " << hcLc[rail]->cycle ;
                    } else {
                        virtualRail[rail]->cycle += 1;
                    }
                }
            }
            // Getting number of cycles to avoid sample comparison during rail folded state.
            if ( virtualRail[rail]->railEnabledBit == true ) { 
                //LOG("info") << "Valid Cycle is " << virtualRail[rail]->cycle;
                if (virtualRail[rail]->railUnfoldedBit == true ) {
                    sim->hcLcSampleEngine.validSampleCycle[rail] = virtualRail[rail]->cycle ;
                    // MasterSlaveGang: Implementing Slave time delay setting following master rail
                    if ( gangEnabled) {
                        std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                        if ( !it->second.empty() ) {
                            for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                                sim->hcLcSampleEngine.validSampleCycle[*iter] = virtualRail[*iter]->cycle ;
                            }
                        }
                    }
                } else if (virtualRail[rail]->railUnfoldedBit == false && virtualRail[rail]->lastValidCycle == false) { 
                    sim->hcLcSampleEngine.validSampleCycle[rail] = virtualRail[rail]->cycle ;
                    virtualRail[rail]->lastValidCycle = true;
                    // MasterSlaveGang: Implementing Slave time delay setting following master rail
                    if ( gangEnabled) {
                        std::unordered_map<size_t, std::vector<size_t>>::iterator it = masterSlaveGanging.find(rail);
                        if ( !it->second.empty() ) {
                            for( auto iter=it->second.begin() ; iter != it->second.end() ; ++iter) {
                                sim->hcLcSampleEngine.validSampleCycle[*iter] = virtualRail[*iter]->cycle ;
                                virtualRail[*iter]->lastValidCycle = true;
                            }
                        }
                    }
                }
            }
            break;
        } // case
    } // Switch
}       // Function 


}  // namespace hddpstbc

