////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: hddpstbc.cc
//------------------------------------------------------------------------------
//    Purpose: HDDPS High-Performance Testbench Methods and Components
//------------------------------------------------------------------------------
// Created by: Rodny Rodriguez
//       Date: 12/15/15
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////

#include <sstream>
#include <stdexcept>
#include <cmath>

#include "hddpstbc.h"
#include "defines.h"

namespace hddpstbc {

// S = Sign
// E = Exponent
// M = Mantissa

// double: SEEEEEEE EEEEMMMM MMMMMMMM MMMMMMMM MMMMMMMM MMMMMMMM MMMMMMMM MMMMMMMM
// byte number:   7        6        5        4        3        2        1        0

// float:  SEEEEEEE EMMMMMMM MMMMMMMM MMMMMMMM
// byte number:   3        2        1        0

// sfp:    SMMMMMMM MMMEEEEE
// byte number:   1        0

uint16_t EncodeSFP(double value)
{
    bool valid = (value > -1024.0) && (value < 1024.0);
    if (!valid) {
        std::stringstream ss;
        ss << "SFP value must in the (-1024.0, 1024.0) set, endpoints excluded, but it is " << value;
        throw std::runtime_error(ss.str());
    }
    uint16_t sign = 0;
    if (value < 0.0) {
        sign = 1;
        value = -value;
    }
    // value is now abs(value)
    int exponent;
    double fraction = std::frexp(value, &exponent);
    // cout << "fraction=" << fraction << ", exponent=" << exponent << ", sign=" << sign << endl;
    uint16_t result = 0;
    result |= (10 - exponent) & 0x0001f;
    result |= (static_cast<uint16_t>(fraction * 0x0400) & 0x03ff) << 5;
    result |= sign << 15;
    return result;
}

double DecodeSFP(uint16_t sfp)
{
    uint16_t sign = (sfp >> 15) & 0x0001;      //  [15]
    uint16_t precision = (sfp >> 5) & 0x03ff;  //  [14:5]
    uint16_t exponent = sfp & 0x0001f;         //  [4:0]
    double result;
    result = precision / pow(2, exponent);
    result = sign ? -result : result;
    return result;
}

size_t ipow(size_t base, size_t exp)
{
    size_t result = 1;
    while (exp) {
        if (exp & 1) {
            result *= base;
        }
        exp >>= 1;
        base *= base;
    }
    return result;
}

int16_t EncodeFixedPoint16(size_t n, double value)
{
    return static_cast<int16_t>(ipow(2, n) * value);
}

double DecodeFixedPoint16(size_t n, int16_t fp)
{
    return static_cast<double>(fp) / ipow(2, n);
}

uint32_t EncodeFloatingPoint32(double value)
{
    float x = static_cast<float>(value);
    uint32_t* ptr = reinterpret_cast<uint32_t*>(&x);
    return *ptr;
}

double DecodeFloatingPoint32(uint32_t fp)
{
    float* ptr = reinterpret_cast<float*>(&fp);
    return static_cast<double>(*ptr);
}

void WriteTriggerCommand
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint8_t cmd,
    uint8_t arg,
    uint16_t data,
    uint8_t write
) {
    if (offset + 4 > length) {
        throw std::out_of_range("In WriteTriggerCommand(), offset is out of range");
    }
    // Advance the pointer to the intended trigger queue command
    array += offset;

    array[0x3] = (UNMASK_1(write) << 7) | UNMASK_7(cmd);
    array[0x2] = UNMASK_8(arg);
    array[0x1] = UNMASK_8(data >> 8);
    array[0x0] = UNMASK_8(data);
}

void WriteRawCommand
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint32_t data
) {
    if (offset + 4 > length) {
        throw std::out_of_range("In WriteRawWord(), offset is out of range");
    }
    // Advance the pointer to the intended trigger queue command
    array += offset;

    array[0x3] = UNMASK_8(data >> 24);
    array[0x2] = UNMASK_8(data >> 16);
    array[0x1] = UNMASK_8(data >> 8);
    array[0x0] = UNMASK_8(data);
}


void ReadTriggerCommand
(
    uint8_t* array,
    size_t length,
    size_t offset,
    uint8_t* cmd,
    uint8_t* arg,
    uint16_t* data,
    uint8_t* write
) {
    if (offset + 4 > length) {
        throw std::out_of_range("In ReadTriggerCommand(), offset is out of range");
    }
    // Advance the pointer to the intended trigger queue command
    array += offset;

    *cmd     = UNMASK_7(array[0x3]);
    *arg     = UNMASK_8(array[0x2]);
    *data    = (UNMASK_8(array[0x1]) << 8) | (UNMASK_8(array[0x0]));
    *write   = BIT(array[0x3], 7);
}

}  // namespace hddpstbc

