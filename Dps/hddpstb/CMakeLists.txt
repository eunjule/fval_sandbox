# INTEL CONFIDENTIAL

# Copyright 2016-2018 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

project(hddpstbc)

find_package(SWIG)


include(Helpers)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/src
    ${CMAKE_CURRENT_BINARY_DIR}
    ${fvalc_SOURCE_DIR}/src
    ${rctbc_SOURCE_DIR}/src
)

add_library(hddpssim STATIC
    src/samplechecker.cc
    src/samplechecker.h
    src/hddpsbfms.cc
    src/hddpsbfms.h
    src/hddpssim.cc
    src/hddpssim.h
    src/hddpstbc.cc
    src/hddpstbc.h
    src/logging.cc
    ${CMAKE_CURRENT_BINARY_DIR}/symbols.h
)

add_library(hddpstbc SHARED
    src/hddpstbc.i
    ${CMAKE_CURRENT_BINARY_DIR}/hddpstbc_wrap.cxx
)

target_link_libraries(hddpssim
    rcsim
)

target_link_libraries(hddpstbc
    hddpssim
    fvalc
)

SWIG_TARGET(hddpstbc)

add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/hddpstbc_wrap.cxx
    COMMAND ${SWIG_EXECUTABLE}
    ARGS -c++ -python -py3 -O -builtin -I${CMAKE_CURRENT_SOURCE_DIR}/src -I${fvalc_SOURCE_DIR}/src -outdir ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} -o ${CMAKE_CURRENT_BINARY_DIR}/hddpstbc_wrap.cxx src/hddpstbc.i
    DEPENDS src/hddpstbc.i src/hddpstbc.h src/hddpssim.h 
    COMMENT "[SWIG][hddpstbc.i] Building python swig wrappers with ${SWIG_VERSION}"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)

add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/symbols.h
    COMMAND ${PYTHON_EXECUTABLE}
    ARGS ${CMAKE_SOURCE_DIR}/Common/instruments/dps/symbols.py ${CMAKE_CURRENT_BINARY_DIR}/symbols.h
    DEPENDS ${CMAKE_SOURCE_DIR}/Common/instruments/dps/symbols.py
    COMMENT "[SYMBOLS] Building symbols.h"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
