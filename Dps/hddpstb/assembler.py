################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: assembler.py
#-------------------------------------------------------------------------------
#     Purpose: HDDPS Trigger Queue Assembler
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/18/15
#       Group: HDMT FPGA Validation
################################################################################

import os
import math

from ThirdParty import pyparsing

from Common import fval
from Common.instruments.dps import symbols
from Dps.hddpstb import structs

def CreateTriggerQueueAssemblerContext(assembler):
    #context = hddpstbc.TriggerQueueAssembler()
    #context.startaddr = assembler.Resolve('startaddr', False)
    #context.vecaddr = assembler.Resolve('vecaddr', False)
    return None  # context

class TriggerQueueAssembler(fval.asm.Assembler):
    _ENCODED_WORD_SIZE       = 4  # 4 bytes = 32 bits
    _BUILTIN_DIRECTIVES      = ['Q', 'R']
    _BUILTIN_GENERATORS      = ['SequenceBreak']
    _GENERATOR_CONTEXT_MAKER = CreateTriggerQueueAssemblerContext
    LITERAL_CHARACTERS       = pyparsing.Word('01XZLHSKRE')
    _CUSTOM_LITERALS         = None
    _EXTENDED_EVAL_SYMBOLS   = True

    def Q(self, array, offset, cmd, arg = 0x0, data = 0x0, write = 0x1):
        if cmd == '0x0':
            self.generate_bar2_register_access_command(arg, array, cmd, data, offset, write)
        else:
            self.generate_rail_command(arg, array, cmd, data, offset, write)

    def generate_rail_command(self, arg, array, cmd, data, offset, write):
        tq_rail_command = structs.ChannelRailCommand()
        tq_rail_command.wr_rd_n = self.Resolve(write)
        tq_rail_command.command = self.Resolve(cmd)
        tq_rail_command.rail_number = self.Resolve(arg)
        tq_rail_command.payload = self.Resolve(data)
        array[offset:offset + 4] = tq_rail_command.value

    def generate_bar2_register_access_command(self, arg, array, cmd, data, offset, write):
        tq_rail_command = structs.BarTwoRegisterAccess()
        tq_rail_command.command = self.Resolve(cmd)
        tq_rail_command.register_address = self.Resolve(arg)
        tq_rail_command.payload = self.Resolve(data)
        tq_rail_command.reserved = self.Resolve(write)
        array[offset:offset + 4] = tq_rail_command.value

    class SequenceBreak:
        def __init__(self, asm, context, rail, delay = 60150):
            if rail < 0 or rail > 25:
                raise Exception('Rail {} out of range'.format(rail))
            self.asm = asm
            self.rail = rail
            self.delay = delay
        def Size(self):
            return 25
        def Generate(self, array, offset):
            self._words = 0
            self._offset = offset
            for i in range(26):
                if i != self.rail:
                    self.asm.Q(array, self._offset, cmd = self.asm.symbols['TIME_DELAY'], arg = i, data = self.delay)
                    self._Next()
            return self._words
        def _Next(self):
            self._words = self._words + 1
            self._offset = self._offset + self.asm._ENCODED_WORD_SIZE

    def __init__(self, startAddress = 0):
        super(TriggerQueueAssembler, self).__init__(startAddress)
        self.evalSymbols['sfp'] = self._sfp
        self.evalSymbols['q3_12'] = self._q_3_12
        self.evalSymbols['fp'] = self._q_3_12

    @staticmethod
    def _sfp(current_in_amps):
        if int(current_in_amps) not in range(-1024,1024):
            print('error', 'Current out of range. Expected value is between -1024 to 1024')
        sign = 0
        if current_in_amps < 0.0:
            sign = 1
            current_in_amps = - current_in_amps
        fraction = math.frexp(current_in_amps)
        current_in_sfp = 0
        current_in_sfp |= (10 - fraction[1]) & 0x0001f
        current_in_sfp |= (int(fraction[0] * 0x0400) & 0x03ff) << 5
        current_in_sfp |= sign << 15
        return current_in_sfp

    @staticmethod
    def _q_3_12(value):
        result = TriggerQueueAssembler.calculate_power(value)
        result = int(result * 0xFFFF)
        return result

    @staticmethod
    def calculate_power(value):
        result = 1
        base = 2
        exp = 12
        while exp:
            if exp & 1:
                result *= base
            exp >>= 1
            base *= base
        result = result * value
        return result

    # This method resolves simple expressions, symbols, and literals
    # Supported proto-functions / expressions:
    #   - eval[evalExpr] or `evalExpr`
    #   - LABEL
    #   - startaddr
    #   - curaddr
    def Resolve(self, expr, secondPass = True):
        if isinstance(expr, int) or isinstance(expr, float):
            return expr
        elif isinstance(expr, str):
            if (expr.startswith('eval[') and expr.endswith(']')) or (expr.startswith('`') and expr.endswith('`')):
                evalExpr = None
                if expr.startswith('eval['):
                    evalExpr = expr[5:-1]
                else:
                    evalExpr = expr[1:-1]
                # Build dictionary of symbols that can be used in eval
                self.evalSymbols['startaddr'] = self.Resolve('startaddr')
                if secondPass:
                    if hasattr(self, 'curaddr'):
                        self.evalSymbols['curaddr'] = self.curaddr
                else:
                    self.evalSymbols['curaddr'] = self.sizeStack[-1]
                result = self.Resolve(eval(evalExpr, self.evalSymbols), secondPass)
                return result
            elif expr == 'startaddr':
                return self.startaddr
            elif expr == 'curaddr':
                if secondPass:
                    return self.curaddr
                else:
                    return self.sizeStack[-1]
            elif expr in self.vars:
                return self.Resolve(self.vars[expr], secondPass)
            elif expr in self.symbols:
                return self.Resolve(self.symbols[expr], secondPass)
            elif expr in self.labels:
                return self.Resolve(self.labels[expr], secondPass)
            else:
                # Let the _num() function take care of it. Supported formats:
                #   - Decimal (e.g. 1, 234)
                #   - Binary (e.g. 0b001, 0b000101010100111100)
                #   - Hexadecimal (e.g. 0xAB6969)
                #   - Float numbers
                return TriggerQueueAssembler._num(expr)
        elif hasattr(expr, '__call__'):
            return self.Resolve(expr(), secondPass)
        else:
            raise Exception('Invalid expression \'{}\''.format(expr))

# Load standard symbols
constants, functions = symbols.Load()
standardSymbols = {}
standardSymbols.update(constants)
standardSymbols.update(functions)
TriggerQueueAssembler.classSymbols = standardSymbols

# Load standard macros
macro_file = os.path.join(os.path.dirname(__file__), 'macros.rtq')
TriggerQueueAssembler.classMacros = fval.asm.LoadMacros(macro_file, TriggerQueueAssembler)

if __name__ == "__main__":
    # Parse command line options
    import argparse
    parser = argparse.ArgumentParser(description = 'HDDPS Trigger Q assembler')
    parser.add_argument('infile', help = 'Source code input filename', type = str)
    parser.add_argument('outfile', help = 'Output filename', type = str)
    parser.add_argument('-s', '--symbols', help = 'Create symbols', action = "store_true")
    args = parser.parse_args()
    
    pattern = TriggerQueueAssembler()
    pattern.Load(args.infile)
    pattern.SaveObj(args.outfile)
    if args.symbols:
        pattern.SaveSymbols(args.outfile + '.symbols')

