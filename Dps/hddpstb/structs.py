################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes


class DpsStruct(ctypes.LittleEndianStructure):
    """Structure base class - defines setter/getter for bytes value of register"""
    def __init__(self, value=None):
        super().__init__()
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return bytes(self)

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i, len(i))


class ChannelRailCommand(DpsStruct):
    _fields_ = [('payload', ctypes.c_uint32, 16),
                ('rail_number', ctypes.c_uint32, 5),
                ('reserved', ctypes.c_uint32, 3),
                ('command', ctypes.c_uint32, 7),
                ('wr_rd_n', ctypes.c_uint32, 1)]


class BarTwoRegisterAccess(DpsStruct):
    _fields_ = [('payload', ctypes.c_uint32, 16),
                ('register_address', ctypes.c_uint32, 8),
                ('command', ctypes.c_uint32, 7),
                ('reserved', ctypes.c_uint32, 1)]


class PmBusPayloadRailCommand(DpsStruct):
    _fields_ = [('payload', ctypes.c_uint32,  16),
                ('register_address', ctypes.c_uint32, 8),
                ('reserved', ctypes.c_uint32, 16),
                ('rail_number',  ctypes.c_uint32,  8),
                ('command', ctypes.c_uint32,  8),
                ('reserved',ctypes.c_uint32,  7),
                ('wr_rd_n', ctypes.c_uint32,  1)]
