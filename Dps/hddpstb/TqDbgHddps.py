################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: patdbg.py
#-------------------------------------------------------------------------------
#     Purpose: Pattern debugger
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/14/15
#       Group: HDMT FPGA Validation
################################################################################

import cmd

from _build.bin import hddpstbc
from Dps import hddpstb

HDDPS_SIM_MEM = 100 * 1024 * 1024


class TqDebugger(cmd.Cmd):
    intro = 'Welcome to the HDDPS Trigger Queue Debugger. Type help or ? to list commands.\n'
    prompt = '(tqdb) '
    def Init(self):
        self.sim = hddpstbc.DaughterboardSimulator(8, 0, HDDPS_SIM_MEM)
        #self.expected = hpcctbc.ExpectedCaptureState()
        #self.bfm = hpcctbc.InternalLoopbackBfm()
        self.bfm = hddpstbc.ResistiveLoadBfm(0, 0, 10)
        self.sim.SetBfm(self.bfm)
    def TqGen(self):
        asm = hddpstb.TriggerQueueAssembler()
        #asm.symbols['RAIL'] = 16 + rail
        #asm.symbols['END'] = 2 * dutid
        #asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString("""\
             TqNotify rail=16, value=1
             SetVoltage rail=0, value=2.0    
             TqComplete rail=16, value=0
         """)
        data = asm.Generate()
        return data
    def do_test(self, args):
        self.sim = hddpstbc.DaughterboardSimulator(8, 0, HDDPS_SIM_MEM)
        self.bfm = hddpstbc.ResistiveLoadBfm(0, 0, 10)
        self.sim.SetBfm(self.bfm)
        self.sim.DmaWrite( 0, self.patGen() )
        self.sim.StepTriggerQueueProcessor()
        print(self.sim.tqaddr)
        self.breakpoints = set()
    def do_echo(self, args):
        self.Echo(*parse(args))
    def do_load(self, args):
        self.sim.DmaWrite( 0, self.TqGen() )
    def do_start(self, args):
        self.Start(*parse(args))
    def do_step(self, args):
        self.Step(*parse(args))
    def do_break(self, args):
        self.Breakpoint(*parse(args))
    def do_continue(self, args):
        self.Continue(*parse(args))
    def do_quit(self, args):
        """
        Exit from patdb.
        """
        return True
    def Echo(self, message):
        print(message)

    def Breakpoint(self, address):
        pass
    def Step(self, numberOfSteps = '1'):
        self.sim.StepTriggerQueueProcessor()
        print(self.sim.tqaddr)
    def Continue(self):
        pass
    def do_q(self, args):
        return self.do_quit(args)
    def do_s(self, args):
        return self.do_step(args)
    def do_b(self, args):
        return self.do_break(args)
    def do_c(self, args):
        return self.do_continue(args)
def parse(args):
    return tuple(args.split())

if __name__ == '__main__':
    tqdb = TqDebugger()
    tqdb.Init()
    tqdb.cmdloop()

