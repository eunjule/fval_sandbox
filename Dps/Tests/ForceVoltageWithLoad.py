################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import random
import collections
import time
from Dps.Tests.dpsTest import BaseTest
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString

LoadCombinations = collections.namedtuple('LoadCombinations', ['is_negative', 'rail_type', 'load'])
VoltageLimits = collections.namedtuple('VoltageLimits', ['min', 'max', 'step_size'])

TEST_COMBINATIONS = {LoadCombinations(is_negative=True,rail_type='LC',load='OHM_4'):VoltageLimits(min=-0.5,max=0,step_size=0.05),
                     LoadCombinations(is_negative=True,rail_type='LC',load='OHM_10'):VoltageLimits(min=-2,max=0,step_size=0.25),
                     LoadCombinations(is_negative=True,rail_type='LC',load='OHM_100'):VoltageLimits(min=-2,max=0,step_size=0.25),

                     LoadCombinations(is_negative=False, rail_type='LC', load='OHM_4'): VoltageLimits(min=0, max=1.5,step_size=0.25),
                     LoadCombinations(is_negative=False, rail_type='LC', load='OHM_10'): VoltageLimits(min=0, max=4.5,step_size=0.25),
                     LoadCombinations(is_negative=False, rail_type='LC', load='OHM_100'): VoltageLimits(min=0, max=6,step_size=0.25),

                     LoadCombinations(is_negative=False, rail_type='HC', load='OHM_4'): VoltageLimits(min=0, max=1.25,step_size=0.25),
                     LoadCombinations(is_negative=False, rail_type='HC', load='OHM_10'): VoltageLimits(min=0, max=2.25,step_size=0.25),
                     LoadCombinations(is_negative=False, rail_type='HV', load='10Ohm'): VoltageLimits(min=1, max=16,step_size=0.25)}

VOLTAGE_LIMITS = {'HC':(-3,2.5),'LC':(-3,10),'HV':(-3,18)}
# VOLTAGE_LIMITS = {'HC':(-3,10),'LC':(-3,10),'HV':(-3,18)}
CURRENT_DEVIATION_TEN_PERCENT = {'HC':0.050, 'LC': 0.120,'HV': 0.750}
CLAMP_LIMITS = {'HC': (-0.5,0.5),'LC':(-0.5,0.5),'HV':(-0.5,3)}
CAL_LOAD = {'HC':['OHM_10','OHM_4'],'LC':['OHM_10','OHM_100','OHM_4'],'HV':['10Ohm']}
I_RANGE = {'HC': 'I_500_MA', 'LC': 'I_500_MA', 'HV': 'I_7500_MA'}
TRIGGER_QUEUE_OFFSET = 0x0
ALARM_PROPAGATION_DELAY = 0.1
HV_RISE_VOLTAGE_DELAY = 1.5
HV_RAIL_START_INDEX = 16
LV_RAIL_START_INDEX = 0
HV_OVER_VOLTAGE = 18
UNDER_VOLTAGE = -1

class VariableLoad(BaseTest):
    def RandomForcePositiveVoltageLcTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_with_load_scenario(board, rail_type, iteration_count = 3, negative = False)

    def RandomForceNegativeVoltageLcTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_with_load_scenario(board, rail_type, iteration_count = 3, negative = True)

    def RandomForcePositiveVoltageHcTest(self):
        rail_type = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            # self.setAD5764_AD5560_DefaultRegisterValues(board)
            self.force_voltage_with_load_scenario(board, rail_type,iteration_count = 2,negative = False)

    def RandomForcePositiveVoltageHvTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_with_load_scenario(board, rail_type, iteration_count=1, negative=False)

    def force_voltage_with_load_scenario(self, board, rail_type, iteration_count, negative = False):
        rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
        dutid = random.randint(0, 63)

        for test_iteration in range(iteration_count):
            list_of_loads = CAL_LOAD[rail_type]
            load_selected = random.choice(list_of_loads)
            load_resistance = int(re.sub('\D', '', load_selected))
            pass_count = 0
            calboard = board.create_cal_board_instance_and_initialize()
            board.cal_load_connect(load_selected, calboard)

            for rail, uhc in rail_uhc_tuple_list:
                low_voltage, high_voltage, step_size = TEST_COMBINATIONS[(negative, rail_type, load_selected)]
                force_voltage = board.get_random_voltage(low_voltage, high_voltage, step_size)
                # self.Log('info', 'Force Voltage {}'.format(force_voltage))
                expected_current = force_voltage / load_resistance
                self.set_up(board, uhc, dutid, rail_type, rail)
                board.cal_connect_force_sense_lines(rail,rail_type,calboard)
                self.force_voltage_with_load_trigger_queue(board, dutid, rail, rail_type, uhc, force_voltage)
                if rail_type == 'HV':
                    time.sleep(HV_RISE_VOLTAGE_DELAY)
                time.sleep(ALARM_PROPAGATION_DELAY)
                global_alarms = board.get_global_alarms()
                if global_alarms!=[]:
                    self.Log('error', '{} rail {} Steady State conditions not achieved. Received {}'.format(rail_type, rail,global_alarms))
                else:
                    try:
                        self.verify_rail_voltage_and_current(board, rail, rail_type, uhc, force_voltage,expected_current)
                        pass_count += 1
                    except VariableLoad.CurrentOrVoltageMismatch:
                        pass
                    board.cal_disconnect_force_sense_lines(rail,rail_type,calboard)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info','{} Voltage and current are measured as expected on {} UHC/Rail combinations.'.format(rail_type,pass_count))
            else:
                self.Log('error','{} Voltage and current are measured as expected  in {} of {} UHC/Rail combinations.'.format(rail_type,pass_count,len(rail_uhc_tuple_list)))
            board.create_cal_board_instance_and_initialize()
        board.SetRailsToSafeState()

    class CurrentOrVoltageMismatch(Exception):
        pass

    def force_voltage_with_load_trigger_queue(self, board, dutid, rail, rail_type, uhc, force_voltage):
        trigger_queue_data = self.generate_trigger_queue(board, rail_type, force_voltage, dutid,rail, uhc)
        board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
        board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)

    def generate_trigger_queue(self, board, rail_type, force_voltage, dutid, rail, uhc):
        tracking_voltage = force_voltage

        tq_helper = TriggerQueueString(board, uhc, dutid)

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.open_socket= False
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = tracking_voltage
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]
        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = 0.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 8000

        trigger_queue = TriggerQueueAssembler()
        text_tq = tq_helper.generateForceVoltageTriggerQueue()
        trigger_queue.LoadString(text_tq)
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def verify_rail_voltage_and_current(self,board,rail,rail_type,uhc,expected_voltage,expected_current):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        # self.Log('info', 'voltage {} '.format(rail_voltage))
        rail_current = board.get_rail_current(rail,rail_type)
        # self.Log('info', 'rail current {} '.format(rail_current))
        allowed_voltage_deviation = 0.25
        if rail_type == 'HV' and expected_voltage >= 10:
            allowed_voltage_deviation = 0.5
        allowed_current_deviation = CURRENT_DEVIATION_TEN_PERCENT[rail_type]
        if board.is_in_maximum_allowed_deviation(expected_current, rail_current, allowed_current_deviation) == False or \
                        board.is_in_maximum_allowed_deviation(expected_voltage, rail_voltage,allowed_voltage_deviation) == False:
            self.Log('error','{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V and current {:.2f} is not within the allowable deviation of {:.2f}V,{:.2f}'.format(
                         rail_type, rail, uhc, rail_voltage, rail_current, expected_voltage, expected_current))
            raise VariableLoad.CurrentOrVoltageMismatch

    def set_up(self, board, uhc, dutid, rail_type, rail):
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
        board.SetRailsToSafeState()
        if rail_type == 'HV' or rail_type == 'LVM':
            board.ResetVoltageSoftSpanCode(rail_type, dutid)
            board.ResetCurrentSoftSpanCode(rail_type, dutid)


