################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import time
import re

from Dps.Tests.dpsTest import BaseTest
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString

TRIGGER_QUEUE_OFFSET = 0x0
ALARM_PROPAGATION_DELAY = 0.1

OLD_FAB_REV = ['100','101']
NEW_FAB_REV = ['201']
class BaseAlarms(BaseTest):

    class HvilAlarmException(Exception):
        pass

    def setup_steady_state(self, board, dutId, hvForceVoltage, hvRail, uhc):
        trigger_queue_data = self.generate_force_voltage_trigger_queue(board, dutId, uhc, hvRail, hvForceVoltage)
        board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
        board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, 'HV')

    def generate_force_voltage_trigger_queue(self, board, dutId, uhc, hv_rail, force_voltage):
        hv_i_range = 'I_7500_MA'
        hv_clamp_high = 1.5
        hvClampLow = -0.5
        hv_over_voltage = 18
        hvUnderVoltage = -3

        hv_tq_string = TriggerQueueString(board.hvdps.subslots[0],uhc,dutId)
        # hv_tq_string.open_socket = False

        hv_tq_string.force_rail_type = 'HV'
        hv_tq_string.force_rail = hv_rail
        hv_tq_string.force_rail_irange = hv_i_range
        hv_tq_string._force_voltage = force_voltage
        hv_tq_string.force_clamp_low = hvClampLow
        hv_tq_string.force_clamp_high = hv_clamp_high
        hv_tq_string.tracking_voltage = force_voltage +2
        hv_tq_string.force_low_voltage_limit = hvUnderVoltage
        hv_tq_string.force_high_voltage_limit = hv_over_voltage
        hv_tq_string.free_drive = True
        hv_tq_string.force_free_drive_high = 7.5
        hv_tq_string.force_free_drive_low = -.5
        hv_tq_string.force_free_drive_delay = 6000


        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(hv_tq_string.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def verify_rail_voltage(self,board,rail,rail_type,uhc,expected_voltage):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        allowed_voltage_deviation = abs((30/100)*expected_voltage)
        if board.is_in_maximum_allowed_deviation(expected_voltage, rail_voltage, allowed_voltage_deviation) == False  :
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V is not within the allowable deviation of {:.2f}V'.format(
                         rail_type, rail, uhc, rail_voltage, expected_voltage))
            raise self.HvilAlarmException

    def set_over_voltage_condition(self, board, high_limit, low_limit):
        board.SetVoltageComparatorLimits(low_limit,high_limit)

    def set_over_current_condition(self, board, i_clamp_limit):
        board.SetCurrentClampLimit(i_clamp_limit)


    def verify_fold_status(self, board):
        expected_hclc_rail_busy_fold_value = 0xFF
        board.WriteRegister(board.registers.CHANNELS_FOLDED(value=0xFF))
        hc_lc_rails_busy_register_status_before_test = board.ReadRegister(board.registers.CHANNELS_FOLDED).value

        if hc_lc_rails_busy_register_status_before_test != expected_hclc_rail_busy_fold_value:
            self.Log('error','Unexpected rails unfolded.Fold status {:x}.'.format(hc_lc_rails_busy_register_status_before_test))
            raise self.RailBusyException

    def verify_rails_busy_folded_register(self,board,rail):
        expected_hclc_rail_busy_fold_value = (1<<(rail+16)) | (0xFF ^ (1<<rail))
        board.WriteRegister(board.registers.CHANNELS_FOLDED(value=0xFF))
        hc_lc_rails_busy_register_status_before_test = board.ReadRegister(board.registers.CHANNELS_FOLDED).value

        if hc_lc_rails_busy_register_status_before_test != expected_hclc_rail_busy_fold_value:
            self.Log('error','Unexpected rails busy behavior observed.Rail Busy Fold status {:x}.'.format(hc_lc_rails_busy_register_status_before_test))
            raise self.RailBusyException


class OverVoltage(BaseAlarms):

    def DirectedRloadOverVoltageTest(self):
        dutId = 15
        force_rail_type = 'HV'
        iterations = 1
        sense_rail_type = 'RLOAD'
        force_voltage = 3
        ov_high_limit = 2.5
        ov_low_limit = -0.5

        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            self.over_voltage_scenario(board, dutId, force_voltage, ov_high_limit, ov_low_limit, force_rail_type,
                                       sense_rail_type,iterations)

    def over_voltage_scenario(self, board, dutId, force_voltage, high_limit, low_limit, force_rail_type, sense_rail_type, iterations):
        hvdps = board.hvdps.subslots[0]
        hvil = board
        for test_iteration in range(iterations):
            pass_count = 0
            hvdps.create_cal_board_instance_and_initialize()
            rail_uhc_tuple_list = hvil.randomize_rail_uhc(sense_rail_type)
            hv_rail = random.randint(0,7)
            for channel,uhc in rail_uhc_tuple_list:

                hvdps.SetRailsToSafeState()
                hvdps.ClearDpsAlarms()
                hvdps.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
                hvdps.ConfigureUhcRail(uhc, 0x1 << hv_rail, force_rail_type)
                hvdps.UnGangAllRails()

                hvil.SetRailsToSafeState()
                hvil.SetDefaultVoltageComparatorLimits()
                hvil.SetDefaultCurrentClampLimit()
                hvil.ClearDpsAlarms()
                hvdps.ConnectRailForce(hv_rail)
                hvdps.ConnectRailSense(hv_rail)
                hvdps.ConnectExternalLoad()

                hvil.ConnectRailForce(channel)

                hvil.setup_load_control(channel)
                self.setup_steady_state(hvdps, dutId, force_voltage, hv_rail, uhc)
                time.sleep(ALARM_PROPAGATION_DELAY)
                hvdps_global_alarms = hvdps.get_global_alarms()
                hvil_global_alarms = hvil.get_global_alarms()
                if hvdps_global_alarms != [] or hvil_global_alarms != []:
                    self.Log('error', '{} rail {} /{} Channel {},Steady State conditions not achieved.Received MB{} DB{}'.format(
                                 force_rail_type, hv_rail, sense_rail_type, channel, hvdps_global_alarms, hvil_global_alarms))
                else:
                    try:
                        self.verify_rail_voltage(hvdps, hv_rail, force_rail_type, uhc, force_voltage)
                        self.verify_rail_voltage(hvil, channel, sense_rail_type, uhc, force_voltage)
                        self.set_over_voltage_condition(hvil, high_limit, low_limit)
                        time.sleep(ALARM_PROPAGATION_DELAY)
                        hvdps_alarms = hvdps.get_global_alarms()
                        if hvdps_alarms != []:
                            self.Log('error', 'Unexpected MB received following {} alarms'.format(hvdps_alarms))
                        else:
                            if hvil.check_per_channel_alarm_details(channel, 'ChannelOverVoltage') == True:
                                pass_count += 1
                    except self.HvilAlarmException:
                        pass
                hvil.load_disconnect(channel)
                hvdps.cal_disconnect_force_sense_lines(hv_rail, force_rail_type)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         '{} Expected Over voltage protection Alarm seen on {} UHC/Rail combinations.'.format(sense_rail_type, pass_count))
            else:
                self.Log('error', '{} Expected Over voltage protection Alarm seen on {} of {} UHC/Rail combinations.'.format(sense_rail_type,
                                                                                                           pass_count, len(rail_uhc_tuple_list)))
        hvdps.ConnectExternalLoad(False)
        hvdps.create_cal_board_instance_and_initialize()
        hvdps.SetRailsToSafeState()


class OverCurrent(BaseAlarms):
    def DirectedRloadOverCurrentTest(self):
        dutId = 15
        force_rail_type = 'HV'
        iterations = 1
        sense_rail_type = 'RLOAD'
        force_voltage = 3
        iterations =1
        alarm_condition_i_clamp_limit = 0.2
        expected_alarm = 'ChannelOverCurrent'
        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            self.over_current_scenario(board, dutId, force_voltage, force_rail_type,sense_rail_type,alarm_condition_i_clamp_limit,iterations,expected_alarm)


    def DirectedRloadThermalOverCurrentTest(self):
        dutId = 15
        force_rail_type = 'HV'
        iterations = 1
        sense_rail_type = 'RLOAD'
        force_voltage = 3
        iterations =1
        alarm_condition_i_clamp_limit = 0.2
        expected_alarm = 'ChannelThermalOverCurrent'
        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            self.over_current_scenario(board, dutId, force_voltage, force_rail_type,sense_rail_type,alarm_condition_i_clamp_limit,iterations,expected_alarm)


    def over_current_scenario(self,board, dutId, force_voltage, force_rail_type,sense_rail_type,alarm_condition_i_clamp_limit,iterations,expected_alarm):
        hvdps = board.hvdps.subslots[0]
        hvil = board
        default_over_current_alarm_filter_count = self.get_over_current_alarm_filter_count(hvil)
        over_current_alarm_filter_count = 2
        for test_iteration in range(iterations):
            pass_count = 0
            hvdps.create_cal_board_instance_and_initialize()
            rail_uhc_tuple_list = hvil.randomize_rail_uhc(sense_rail_type)
            hv_rail = random.randint(0, 7)
            for channel, uhc in rail_uhc_tuple_list:
                hvdps.SetRailsToSafeState()
                hvdps.ClearDpsAlarms()
                hvdps.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
                hvdps.ConfigureUhcRail(uhc, 0x1 << hv_rail, force_rail_type)
                hvdps.UnGangAllRails()

                hvil.SetRailsToSafeState()
                hvil.SetDefaultVoltageComparatorLimits()
                hvil.SetDefaultCurrentClampLimit()
                hvil.SetDefaultThermalCurrentClampLimit()
                self.set_over_current_alarm_filter_count(over_current_alarm_filter_count, hvil)
                hvil.ClearDpsAlarms()
                hvil.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
                hvil.ConfigureUhcRail(uhc, 0x1 << channel, sense_rail_type)
                hvil.WriteTQHeaderViaBar2(dutId, channel, sense_rail_type)

                hvdps.ConnectRailForce(hv_rail)
                hvdps.ConnectRailSense(hv_rail)
                hvdps.ConnectExternalLoad()

                hvil.ConnectRailForce(channel)

                hvil.setup_load_control(channel)
                self.setup_steady_state(hvdps, dutId, force_voltage, hv_rail, uhc)
                time.sleep(ALARM_PROPAGATION_DELAY)
                hvdps_global_alarms = hvdps.get_global_alarms()
                hvil_global_alarms = hvil.get_global_alarms()

                if hvdps_global_alarms != [] or hvil_global_alarms != []:
                    self.Log('error',
                             '{} rail {} /{} Channel {},Steady State conditions not achieved.Received MB{} DB{}'.format(
                                 force_rail_type, hv_rail, sense_rail_type, channel, hvdps_global_alarms, hvil_global_alarms))
                else:
                    try:
                        self.verify_rail_voltage(hvdps, hv_rail, force_rail_type, uhc, force_voltage)
                        self.verify_rail_voltage(hvil, channel, sense_rail_type, uhc, force_voltage)
                        clamp_value_16_bit = hvil.get_16bit_encoded_current_value(alarm_condition_i_clamp_limit)

                        self.set_over_current_condition_using_rail_command(board, channel, clamp_value_16_bit,
                                                                           expected_alarm)
                        time.sleep(ALARM_PROPAGATION_DELAY)

                        hvdps_alarms = hvdps.get_global_alarms()
                        if hvdps_alarms != []:
                            self.Log('error', 'Unexpected MB received following {} alarms'.format(hvdps_alarms))
                        else:
                            if expected_alarm == 'ChannelThermalOverCurrent':
                                if hvil.check_per_channel_alarm_details(channel, 'ChannelThermalOverCurrent') == True:
                                    pass_count += 1
                            elif expected_alarm == 'ChannelOverCurrent':
                                if hvil.check_per_channel_alarm_details(channel, 'ChannelOverCurrent') == True:
                                    pass_count += 1
                    except self.HvilAlarmException:
                        pass
                hvil.load_disconnect(channel)
                hvdps.cal_disconnect_force_sense_lines(hv_rail, force_rail_type)
                hvil.ConnectRailForce(channel,False)

            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         '{} Expected {} Alarm seen on {} UHC/Rail combinations.'.format(
                             sense_rail_type,expected_alarm, pass_count))
            else:
                self.Log('error',
                         '{} Expected {} seen on {} of {} UHC/Rail combinations.'.format(
                             sense_rail_type,expected_alarm,
                             pass_count, len(
                                 rail_uhc_tuple_list)))
        self.set_over_current_alarm_filter_count(default_over_current_alarm_filter_count, hvil)
        hvdps.ConnectExternalLoad(False)
        hvdps.create_cal_board_instance_and_initialize()
        hvdps.SetRailsToSafeState()

    def set_over_current_alarm_filter_count(self, default_over_current_alarm_filter_count, hvil):
        hvil.WriteRegister(hvil.registers.OVERCURRENT_FILTER_COUNT(value=default_over_current_alarm_filter_count))

    def get_over_current_alarm_filter_count(self, hvil):
        default_over_current_alarm_filter_count = hvil.ReadRegister(hvil.registers.OVERCURRENT_FILTER_COUNT).value
        return default_over_current_alarm_filter_count

    def set_over_current_condition_using_rail_command(self, board, channel, clamp_value_16_bit, expected_alarm):
        if expected_alarm == 'ChannelThermalOverCurrent':
            board.WriteBar2NonBroadcastRailCommand(board.symbols.RAILCOMMANDS.SET_THERMAL_CURRENT_CLAMP, channel,
                                                   clamp_value_16_bit, 0)
        elif expected_alarm == 'ChannelOverCurrent':
            board.WriteBar2NonBroadcastRailCommand(board.symbols.RAILCOMMANDS.SET_USER_CURRENT_CLAMP, channel,
                                                   clamp_value_16_bit, 0)


class RailBusyAlarm(BaseAlarms):

    def DirectedRloadRailBusyAlarmTest(self):
        rail_type = 'RLOAD'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.rload_rail_busy_scenario(board,rail_type)

    def rload_rail_busy_scenario(self, board, rail_type,test_iterations=1):
        rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
        resourceid = getattr(board.symbols.RESOURCEIDS, rail_type)
        dutid = random.randint(0, 63)
        for iterations in range(test_iterations):
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                rail_mask = 0x1 << rail
                board.ClearDpsAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.SetDefaultVoltageComparatorLimits()
                board.SetDefaultCurrentClampLimit()
                board.SetRailsToSafeState()

                global_alarms = board.get_global_alarms()
                if global_alarms != []:
                    self.Log('error', '{} rail {} Unexpected Alarms received {}'.format(rail_type, rail,global_alarms))
                else:
                    try:
                        self.verify_fold_status(board)
                        board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                        self.verify_rails_busy_folded_register(board,rail)
                        board.WriteBar2RailCommand(board.symbols.RAILCOMMANDS.SET_RAIL_BUSY, rail_mask, resourceid)
                        time.sleep(ALARM_PROPAGATION_DELAY)
                        if board.check_per_channel_alarm_details(rail, 'RailBusy') == True :
                            pass_count+= 1
                        board.WriteTQFooterViaBar2(dutid, rail, rail_type)
                    except self.RailBusyException:
                        pass
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info', 'Expected Rail busy behavior seen on all {} UHC/Rail combinations iteration {} of {}'.format(
                    len(rail_uhc_tuple_list),iterations+1,test_iterations))
            else:
                self.Log('error', 'Unexpected Rail busy behavior seen on {} of {} UHC/Rail combinations during test iteration {} of {}'.format(
                    len(rail_uhc_tuple_list) - pass_count, len(rail_uhc_tuple_list),iterations+1,test_iterations))
        board.SetRailsToSafeState()


    class RailBusyException(Exception):
        pass
