################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from Dps.Tests.dpsTest import BaseTest


class InterfaceTests(BaseTest):

    def DirectedVoltageReadTest(self):
        number_of_iterations = 1000
        hilapi_returned_voltage = 0
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            pass_count = 0
            fail_count = 0
            dut.enableLtc2450AdcPolling()
            for i in range(number_of_iterations):
                hilapi_returned_voltage = dut.Ltc2450AdcVoltageRead()
                ltc2450register = dut.ReadRegister(dut.getLTC2450AdcVoltageRegister())
                raw_voltage = ltc2450register.AdcVoltage
                direct_register_voltage_value = self.convert_raw_adc_voltage_to_volts(raw_voltage)
                if dut.is_close(hilapi_returned_voltage, direct_register_voltage_value, 2):
                    pass_count = pass_count + 1
                else:
                    self.Log('error',
                             'Mismatch in LTC2450 voltage read through HIL api.Hil value {:.6f}V Register value {:.6f}V'
                             .format(hilapi_returned_voltage, direct_register_voltage_value))
                    fail_count = fail_count + 1

            if fail_count == 0 and pass_count >= 1:
                self.Log('info', 'LTC2450 voltage matched the voltage read by HIL api: {:.6f}V for {} iterations'
                         .format(hilapi_returned_voltage, pass_count))
            else:
                self.Log('error', 'LTC2450 voltage matched the voltage read by HIL api {} out of {} times'
                         .format(pass_count, number_of_iterations))

            dut.disableLtc2450AdcPolling()

    def DirectedTemperatureReadTest(self):
        number_of_iterations = 1000
        upper_temp_limit = 100
        lower_temp_limit = 20
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            for i in range(number_of_iterations):
                hilapi_returned_voltage = dut.Ltc2450AdcVoltageRead()
                temperature_in_degC = self.convert_voltage_to_temperature(hilapi_returned_voltage)
                if temperature_in_degC < lower_temp_limit or temperature_in_degC > upper_temp_limit:
                    self.Log('error', 'Observed Temperature {:.2f}C is outside temperature limits: {}C to {}C'
                             .format(temperature_in_degC, lower_temp_limit, upper_temp_limit))
            self.Log('info', 'LT2450 Temperature was withing the limits of {}C to {}C for {} iterations'
                     .format(lower_temp_limit, upper_temp_limit, number_of_iterations))

    def convert_raw_adc_voltage_to_volts(self, raw_voltage):
        ltc2450_vref = 3.3
        adc_resolution = 16
        converted_value = (raw_voltage * ltc2450_vref) / (1 << adc_resolution)
        return converted_value

    def convert_voltage_to_temperature(self, voltage):
        voltage_temperature_slope = -0.002
        voltage_temperature_intercept = 0.65
        temperature = (voltage - voltage_temperature_intercept) / voltage_temperature_slope
        return temperature
