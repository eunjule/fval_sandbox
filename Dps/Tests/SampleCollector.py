################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegisters
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers
# -------------------------------------------------------------------------------
#  Created by: Sungjin Ho
#        Date: 7/7/16
#       Group: HDMT FPGA Validation
###############################################################################

import random
from string import Template

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    def VlcSampleCollectorVoltageBroken(self):
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for rail in range(0, 1):
                dutid = 0
                rail = rail
                # rail type for sample collector index register
                railType = "VLC"
                # measurement type for sample collector index register
                vOrI = "V"
                # FPGA drive value that is also used in sample collector expected value
                driveValue = 1.0
                # FPGA drive mode that is also used in sample collector expected value
                driveType = "VFORCE"
                # cal cage load value. 
                load = 4
                # CAL cage load rail type
                loadRailType = "VLC"
                self.SampleCollectorScenarioVLC(slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                                loadRailType)

    def LcSampleCollectorVoltageTest(self):
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for rail in range(0, 10):
                dutid = 0
                rail = rail
                # rail type for sample collector index register
                railType = "HCLC"
                # measurement type for sample collector index register
                vOrI = "V"
                # FPGA drive value that is also used in sample collector expected value
                driveValue = 1.5
                # FPGA drive mode that is also used in sample collector expected value
                driveType = "VFORCE"
                # cal cage load value. 
                load = 10
                # CAL cage load rail type
                loadRailType = "LC"
                self.SampleCollectorScenario(slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                             loadRailType)

    def HcSampleCollectorVoltageTest(self):
        for slot, subslot in self.env.fpgas:
            if subslot == 0:
                continue
            for rail in range(0, 10):
                dutid = 0
                rail = rail
                # rail type for sample collector index register
                railType = "HCLC"
                # measurement type for sample collector index register
                vOrI = "V"
                # FPGA drive value that is also used in sample collector expected value
                driveValue = 1.5
                # FPGA drive mode that is also used in sample collector expected value
                driveType = "VFORCE"
                # cal cage load value. 
                load = 10
                # CAL cage load rail type
                loadRailType = "HC"
                self.SampleCollectorScenario(slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                             loadRailType)

    def LcSampleCollectorCurrentTest(self):
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for rail in range(0, 10):
                dutid = 0
                rail = rail
                # rail type for sample collector index register
                railType = "HCLC"
                # measurement type for sample collector index register
                vOrI = "I"
                # FPGA drive value that is also used in sample collector expected value
                driveValue = 1.5
                # FPGA drive mode that is also used in sample collector expected value
                driveType = "VFORCE"
                # cal cage load value. 
                load = 10
                # CAL cage load rail type
                loadRailType = "LC"
                self.SampleCollectorScenario(slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                             loadRailType)

    def HcSampleCollectorCurrentTest(self):
        for slot, subslot in self.env.fpgas:
            if subslot == 0:
                continue
            for rail in range(0, 10):
                dutid = 0
                rail = rail
                # rail type for sample collector index register
                railType = "HCLC"
                # measurement type for sample collector index register
                vOrI = "I"
                # FPGA drive value that is also used in sample collector expected value
                driveValue = 1.5
                # FPGA drive mode that is also used in sample collector expected value
                driveType = "VFORCE"
                # cal cage load value. 
                load = 10
                # CAL cage load rail type
                loadRailType = "HC"
                self.SampleCollectorScenario(slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                             loadRailType)

    def SampleCollectorScenario(self, slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                loadRailType):
        self.Log('info', '\nTesting HC rail {} for dutid {}...'.format(rail, dutid))
        board = self.env.instruments[slot].subslots[subslot]
        board.SetRailsToSafeState()

        if load == "NONE":
            board.ConnectCalBoard('{}{}'.format(loadRailType, rail), "NONE")
        else:
            board.ConnectCalBoard('{}{}'.format(loadRailType, rail), "OHM_{}".format(load))

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,railType)
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'LC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.symbols['DRIVETYPE'] = driveType
        asm.LoadString(Template("""\
             TqNotify rail=0, value=BEGIN
             TqNotify rail=16, value=BEGIN
             SetMode rail=RAIL, value=DRIVETYPE             
             #Q cmd=TIME_DELAY, arg=RAIL, data=0
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=1.0
             SetILoFreeDrive rail=RAIL, value=-1.0
             SetIClampHi rail=RAIL, value=1.2 
             SetIClampLo rail=RAIL, value=-1.2
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             EnableDisableRail rail=RAIL, value=1
             SetVoltage rail=RAIL, value=$DRIVEVALUE
             TimeDelay rail=RAIL, value=1000
             SetVCompAlarm rail=RAIL, value=0
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             #EnableDisableRail rail=RAIL, value=0
             #TimeDelay rail=RAIL, value=1000
             TqNotify rail=0, value=END
             TqNotify rail=16, value=END
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'LC'), DRIVEVALUE=driveValue))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, dutid)

        # Reading out sample collector data
        # delay is between index register write and reading out data
        # is hardcoded to 1 sec.
        board.ReadSampleCollectorData(rail, railType, vOrI, 1, driveValue, driveType, load)

        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        sampleVoltage = board.ReturnSampleData(SampleBase + 0x1000 * dutid)
        self.Log('info', 'Sample voltage is  {}'.format(sampleVoltage))

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()
        return sampleVoltage
        # rail is continuously enabled even after TQ is finished.
        # To avoid energized rail, putting rails to safe state.
        board.SetRailsToSafeState()

    def SampleCollectorScenarioVLC(self, slot, subslot, dutid, rail, railType, vOrI, driveValue, driveType, load,
                                   loadRailType):
        self.Log('info', '\nTesting VLC rail {} for dutid {}...'.format(rail, dutid))
        board = self.env.instruments[slot].subslots[subslot]
        board.SetRailsToSafeState()

        if rail < 10:
            board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_4')
            load = 10
        else:
            board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')
            load = 10

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,railType)
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500 * dutid, 0x500 * dutid, 0x1000 * dutid, 0x1000 * dutid, dutid, 'VLC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.symbols['DRIVETYPE'] = driveType
        asm.LoadString(Template("""\
             TqNotify rail=0, value=BEGIN
             TqNotify rail=16, value=BEGIN
             SetMode rail=RAIL, value=DRIVETYPE             
             #Q cmd=TIME_DELAY, arg=RAIL, data=0
             SetCurrentRange rail=RAIL, value=I_256_MA
             EnableDisableRail rail=RAIL, value=0
             #SetIHiFreeDrive rail=RAIL, value=0.250
             #SetILoFreeDrive rail=RAIL, value=-0.250
             SetIClampHi rail=RAIL, value=0.250 
             SetIClampLo rail=RAIL, value=-0.250
             #SetOV rail=RAIL, value=2.5
             #SetUV rail=RAIL, value=-1.5
             EnableDisableRail rail=RAIL, value=1
             SetVoltage rail=RAIL, value=$DRIVEVALUE
             #TimeDelay rail=RAIL, value=1000
             SetVCompAlarm rail=RAIL, value=0
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             #EnableDisableRail rail=RAIL, value=0
             #TimeDelay rail=RAIL, value=1000
             TqNotify rail=0, value=END
             TqNotify rail=16, value=END
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0),
                         DRIVEVALUE=driveValue))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, dutid)

        # Reading out sample collector data
        # delay is between index register write and reading out data
        # is hardcoded to 1 sec.
        board.ReadSampleCollectorData(rail, railType, vOrI, 1, driveValue, driveType, load)

        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))

        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        sampleVoltage = board.ReturnSampleData(SampleBase + 0x1000 * dutid)
        self.Log('info', 'Sample voltage is  {}'.format(sampleVoltage))
        # board.RunCheckers( dutid, 'ResistiveLoopback', 5, maxCaptureErrors=200 )

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()
        return sampleVoltage
        # rail is continuously enabled even after TQ is finished.
        # To avoid energized rail, putting rails to safe state.
        board.SetRailsToSafeState()
