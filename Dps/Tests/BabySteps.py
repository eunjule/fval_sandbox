################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from Common.instruments.dps import symbols
import random
import struct
from string import Template
import time
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString


from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

TRIGGER_QUEUE_OFFSET = 0x0
ALARM_PROPAGATION_DELAY = 0.1

class SimpleBabySteps(BaseTest):

    def MiniTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','LC','VLC','HV','LVM']):
            slot = board.slot
            subslot = board.subslot
            board.SetRailsToSafeState()
            data = board.ReadRegister(board.getResetsRegister()).value
            self.Log('info', 'Version on ({}, {}) is 0x{:x}'.format(slot, subslot, data))
            board.WriteRegister(board.getResetsRegister()(value = 0x0))


class TriggerQueueBabySteps(BaseTest):

    def DirectedTriggerQueueToScratchRegisterTest(self):
        uhc_count = 8
        rail_type_to_test = ['HV']

        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type_to_test):
            for rail_type in board.RAIL_COUNT.keys():
                if rail_type in rail_type_to_test:
                    rail_uhc_tuple_list = []
                    for rail in range(board.RAIL_COUNT[rail_type]):
                        for uhc in range(uhc_count):
                            rail_uhc_tuple_list.append((rail, uhc))
                    random.shuffle(rail_uhc_tuple_list)
                    for rail, uhc in rail_uhc_tuple_list:
                        self.TriggerQueueToScratchRegisterScenerio(rail, uhc, board,rail_type)

    def TriggerQueueToScratchRegisterScenerio(self, rail, uhc, board,rail_type):
        self.Log('info', 'Testing {} rail {} and UHC {}...'.format(rail_type,rail, uhc))
        board.SetRailsToSafeState()

        board.ClearDpsAlarms()
        board.EnableAlarms()
        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
        board.UnGangAllRails()

        board.WriteRegister(board.registers.BAR2_SCRATCH(value=0x0))
        self.Log('info','Scratch REG: 0x{:x}'.format(board.ReadRegister(board.registers.BAR2_SCRATCH).value))


        scratch_register_value = random.randint(0,0xffff)

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, rail_type)
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, rail_type)

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid << 1
        trigger_queue.symbols['BEGIN'] = (dutid << 1) | 1
        trigger_queue.LoadString(Template("""\
              $triggerqueueheader
              Q cmd=0x00, arg=Scratch, data=$data
              $triggerqueuefooter
              TqComplete rail=0, value=0
          """).substitute(triggerqueueheader=trigger_queue_header ,triggerqueuefooter=trigger_queue_footer
                          ,data=scratch_register_value))
        trigger_queue_data = trigger_queue.Generate()

        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)

        current_scratch_value = board.ReadRegister(board.registers.BAR2_SCRATCH).value
        if current_scratch_value == scratch_register_value:
            self.Log('info','Scratch REG expected 0x{:x}, read: 0x{:x}'.format(scratch_register_value,current_scratch_value))
        else:
            self.Log('error',
                     'Scratch REG expected 0x{:x}, read: 0x{:x}'.format(scratch_register_value, current_scratch_value))

        expected_fold_register = ~(0x1 << rail) & 0xFF
        board.WriteRegister(board.registers.HCLC_RAILS_FOLDED(value=0xffffffff))
        hclcfold = board.ReadRegister(board.registers.HCLC_RAILS_FOLDED).value
        if expected_fold_register == hclcfold:
            self.Log('info', 'rails under test {} folded as expected.  Fold register value: 0x{:x}'.format(rail, hclcfold))
        else:
            self.Log('error', 'Not all rails folded as expected. Fold register value: 0x{:x}'.format(hclcfold))

        global_alarms = board.get_global_alarms()
        if global_alarms!= []:
            self.Log('error', 'Received global alarm(s): {}'.format(global_alarms))
            alarm_register = 'Hclc{:02d}Alarms'.format(rail)
            alarm_register_object = getattr(board.registers,alarm_register)
            self.Log('info','HCLC Rail Alarm Register status for rail {}: {:032b}'.format(rail,board.ReadRegister(alarm_register_object).value))


        board.SetRailsToSafeState()



    def generateRloadLoadSelectTriggerQueue(self, tq_helper, channel, rail_type,channel_load_select_value,load_activate_value,channel_connect_value):
        trigger_queue_string = ''
        force_trigger_queue_header = tq_helper.get_trigger_queue_header(channel,rail_type)
        trigger_queue_string =  trigger_queue_string + '\n'+force_trigger_queue_header

        string_line1 = 'Q cmd = SET_CHANNEL_LOAD_SELECT, arg = {}, data = {}'.format(channel,channel_load_select_value)
        trigger_queue_string = trigger_queue_string + '\n' + string_line1
        string_line2 = 'Q cmd = SET_CHANNEL_LOAD_ACTIVATE, arg = {}, data = {}'.format(channel,load_activate_value)
        trigger_queue_string = trigger_queue_string + '\n' + string_line2
        string_line2 = 'Q cmd = CHANNEL_CONNECT, arg = {}, data = {}'.format(channel, channel_connect_value)
        trigger_queue_string = trigger_queue_string + '\n' + string_line2

        time_delay = tq_helper.get_time_delay(channel, rail_type, 700)
        trigger_queue_string = trigger_queue_string + '\n' + time_delay

        force_trigger_queue_footer = tq_helper.get_trigger_queue_footer(channel, rail_type)
        trigger_queue_string = trigger_queue_string + '\n' + force_trigger_queue_footer

        tq_complete = tq_helper.get_tq_complete()
        trigger_queue_string = trigger_queue_string + '\n' + tq_complete
        return trigger_queue_string

    def DirectedRloadChannelLoadSelectviaTriggerQueueTest(self):
        rail_type = 'RLOAD'
        dutid =15
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            master_fet_enable_disable_bit =9
            pass_count =0
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            for channel, uhc in rail_uhc_tuple_list:
                load_select_value = 0x4
                channel_load_select_value = (channel << 13) | load_select_value
                load_activate_value = 0x1 << channel
                channel_connect_value = 0x1 << channel
                expected_load_selected_value = (0x1<<master_fet_enable_disable_bit)| load_select_value

                board.SetRailsToSafeState()
                board.SetDefaultVoltageComparatorLimits()
                board.SetDefaultCurrentClampLimit()
                board.ClearDpsAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << channel, rail_type)
                tq_helper = TriggerQueueString(board, uhc, dutid)
                trigger_queue = TriggerQueueAssembler()
                trigger_queue.LoadString(self.generateRloadLoadSelectTriggerQueue(tq_helper, channel, rail_type,channel_load_select_value,load_activate_value,channel_connect_value))
                trigger_queue_data = trigger_queue.Generate()

                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)
                hvil_global_alarms = board.get_global_alarms()
                if  hvil_global_alarms != []:
                    self.Log('error','Unexpected Alarms received {}'.format(hvil_global_alarms))
                else:
                    actual_load_select_register_value =board.ReadRegister(board.registers.CHANNEL_LOAD_STATE, index=channel).value
                    if expected_load_selected_value == actual_load_select_register_value:
                        pass_count+=1
                    board.load_disconnect(channel)
            if pass_count == (len(rail_uhc_tuple_list)):
                self.Log('info','{} Expected Load Select value seen on {} UHC/Rail combinations.'.format(rail_type, len(
                             rail_uhc_tuple_list)))
            else:
                self.Log('error',
                         '{} Expected Load Select value seen on {} of {} UHC/Rail combinations'.format(rail_type,pass_count,len(rail_uhc_tuple_list)))