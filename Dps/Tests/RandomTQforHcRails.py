###################################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# --------------------------------------------------------------------------------------------------
#    Filename: RandomTQforHcRails
# --------------------------------------------------------------------------------------------------
#     Purpose: Randomizing various attributes of Trigger Queue for more comprehensive test coverage
# --------------------------------------------------------------------------------------------------
#  Created by: Shaariq Shaikh
#        Date: 
#       Group: HDMT FPGA DEVELOPMENT
###################################################################################################

from collections import defaultdict
import math
import random
from string import Template

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest
import unittest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    def randomizehclcsampleengine(self, dutid, rail):
        result = ''
        # Fixed command for Reset
        result = result + 'Q cmd=0x0, arg=HclcSampleEngineReset, data=0x{:x}\n'.format(0x1 << dutid)
        available_sample_engine_commands_per_rail = defaultdict(list)

        metadata_high = 0
        specific_parameters = ['SampleDelay', 'SampleCount', 'SampleRate', 'SampleMetadataHi', 'SampleMetadataLo',
                               'SampleRailSelect']
        sample_commands_specific_to_a_rail = []
        for parameter in specific_parameters:
            if parameter == 'SampleDelay':
                delay_range = random.randint(5000, 10000)
                dword = 'Q cmd=0x0, arg=Hclc{}SampleDelay, data=0x{:x}\n'.format(dutid, delay_range)
                self.Log('info', 'The sample delay is 0x{:x}\n'.format(delay_range))
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleCount':
                count_value = random.randint(4000, 5000)
                dword = 'Q cmd=0x0, arg=Hclc{}SampleCount, data=0x{:x}\n'.format(dutid, count_value)
                self.Log('info', 'The sample count is 0x{:x}\n'.format(count_value))
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleRate':
                rate = random.randint(0, 5)
                dword = 'Q cmd=0x0, arg=Hclc{}SampleRate, data=0x{:x}\n'.format(dutid, rate)
                self.Log('info', 'The sample rate is 0x{:x}\n'.format(rate))
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleMetadataHi':
                metadata_high = random.randint(500, 65535)
                dword = 'Q cmd=0x0, arg=Hclc{}SampleMetadataHi, data=0x{:x}\n'.format(dutid, metadata_high)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleMetadataLo':
                metadata_low = random.randint(0, metadata_high)
                dword = 'Q cmd=0x0, arg=Hclc{}SampleMetadataLo, data=0x{:x}\n'.format(dutid, metadata_low)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleRailSelect':
                rail_select = 0x3ff
                dword = 'Q cmd=0x0, arg=Hclc{}SampleRailSelect, data=0x{:x}\n'.format(dutid, rail_select)
                sample_commands_specific_to_a_rail.append(dword)

        for individual_command in sample_commands_specific_to_a_rail:
            available_sample_engine_commands_per_rail[rail].append(individual_command)

        # Start randomly picking a rail as well as a command associated with that rail
        while len(available_sample_engine_commands_per_rail[rail]) != 0:
            instruction = random.choice(available_sample_engine_commands_per_rail[rail])
            result = result + instruction
            available_sample_engine_commands_per_rail[rail].remove(instruction)

        # Fixed command for sample start
        result = result + 'Q cmd=0x0, arg=Hclc{}SampleStart, data=0x0001\n'.format(dutid)
        return result

    @unittest.skip('This test currently skipped while TQ tests are being audited')
    def RandomSubmodesforVFORCEModeTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            self.SetsubModeHcTestScenario(board)

    def SetsubModeHcTestScenario(self, board):
        # self.Log('info', '\nTesting HC rail {} for dutid {}...'.format(rail, dutid))
        slot = board.slot
        subslot = board.subslot
        board.SetRailsToSafeState()

        # randomly pick a value of load from the of load values
        num_of_possible_values_for_load = ['OHM_0_025', 'OHM_0_05', 'OHM_0_25', 'OHM_0_8', 'OHM_4', 'OHM_10', 'OHM_100',
                                           'OHM_1K',
                                           'OHM_100K', 'OHM_500K', 'OHM_1M', 'NONE']

        selected_value_of_load = 'OHM_10'

        num_of_hc_rails = list(range(0, 10))
        rails_mapped_to_each_uhc = []
        """
        for i in range (0,8):
            rails_mapped_to_each_uhc.append ([])
        """
        # Assign multiple rails to individual MDUTs
        for i in range(0, 8):
            # num_rails = random.randint (1,2)
            num_rails = 1
            for no_rail in range(0, num_rails):
                rail_number = random.choice(num_of_hc_rails)
                self.Log('info', '\nTesting HC rails {}...'.format(rail_number))
                rails_mapped_to_each_uhc.append(rail_number)
                num_of_hc_rails.remove(rail_number)

                # Randomly pick a uhc number
        uhc = list(range(0, 8))
        dutid = random.choice(uhc)
        self.Log('info', '\nTesting dut id randomly selected is {}'.format(dutid))

        # Pick the rails corresponding to that UHC number
        # rail = []
        rail = rails_mapped_to_each_uhc[dutid]

        self.Log('info', '\nTesting HC rails {} for dutid {}...'.format(rail, dutid))
        board.ConnectCalBoard('HC{}'.format(rail), selected_value_of_load)

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        # Configuring rails to a one hot value to have 1's at the location of selected rails
        """
        rail_configuration = 0b0000000000
        for hclc_rail_number in rail:
            rail_configuration = rail_configuration + (0b0000000001 << hclc_rail_number)
        """

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')

        # Only used to support the run checker
        board.UnGangAllRails()

        # Doubt whether this will work
        # board.ConfigureHclcDutRail(dutid, rail_configuration)
        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'HC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'HC')

        asm = TriggerQueueAssembler()
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid

        # Fixed Header for trigger Queue
        src = trigger_queue_header + "\n"

        available_modes = ['VFORCE', 'IFORCE']
        submodes_within_VFORCE = ['none', 'rampvoltage', 'positiveslewvoltage', 'negativeslewvoltage']
        mode_select = random.choice(available_modes)
        submode_select = random.choice(submodes_within_VFORCE)

        # fixed instruction for free drive time in case vforce mode is selected
        if mode_select == 'VFORCE' and submode_select == 'none':
            rail_command = 'TIME_DELAY'
            data = 0x1388
            src = src + 'Q cmd={}, arg={}, data=0x{:x}\n'.format(rail_command, 16 + rail, data)
        elif mode_select == 'VFORCE':
            rail_command = 'TimeDelay'
            data = 5000
            src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)

            # Start randomizing the trigger queue instructions
        current_ranges = ['I_500_MA', 'I_25_MA', 'I_25000_MA']

        # Declaring a dictionary in order to map the rail commands to the appropriate rails
        available_commands_per_rail = defaultdict(list)

        # Taking care of the dependencies between the instructions here before randomizing
        if mode_select == 'VFORCE':
            commands = ['SetIClampHi', 'SetIClampLo', 'SetOV', 'SetUV', 'SetIHiFreeDrive', 'SetILoFreeDrive']
        else:
            commands = ['SetIClampHi', 'SetIClampLo', 'SetOV', 'SetUV']

        commands_specific_to_a_rail = []
        clamp_high_upper_limit = 0
        under_voltage = 0
        over_voltage = 0
        dword = 0

        specific_value_of_current_range = 'I_500_MA'
        rail_command = 'SetCurrentRange'
        if specific_value_of_current_range == 'I_25_MA':
            clamp_high_upper_limit = 0.025 + (0.25 * 0.025)
        elif specific_value_of_current_range == 'I_500_MA':
            clamp_high_upper_limit = 0.5 + (0.25 * 0.5)
        elif specific_value_of_current_range == 'I_25000_MA':
            clamp_high_upper_limit = 25.0 + (25.0 * 25)
        src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, specific_value_of_current_range)
        rail_command = 'EnableDisableRail'
        data = 0
        src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)

        for rail_command in commands:
            if rail_command == 'SetIClampHi':
                clamp_low_lower_limit = (clamp_high_upper_limit * -1)
                data = random.uniform(0, clamp_high_upper_limit)
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetIClampLo':
                data = random.uniform(clamp_low_lower_limit, 0)
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetOV':
                if mode_select == 'VFORCE' and submode_select != 'none':
                    over_voltage = random.uniform(1, 8)
                    dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, over_voltage)
                    commands_specific_to_a_rail.append(dword)
                else:
                    over_voltage = random.uniform(-4, 8)
                    dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, over_voltage)
                    commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetUV':
                under_voltage = random.uniform(-4.44, over_voltage)
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, under_voltage)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetIHiFreeDrive':
                free_drive_current_high = 24
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, free_drive_current_high)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetILoFreeDrive':
                free_drive_current_low = -4
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, free_drive_current_low)
                commands_specific_to_a_rail.append(dword)

        for individual_command in commands_specific_to_a_rail:
            available_commands_per_rail[rail].append(individual_command)

        while len(available_commands_per_rail[rail]) != 0:
            instruction = random.choice(available_commands_per_rail[rail])
            src = src + instruction
            available_commands_per_rail[rail].remove(instruction)

        # Fixed commands here
        ramp_step_voltage = 0
        start_value_for_negative_vslew = 0
        if mode_select == 'IFORCE':
            rail_command = 'FORCE_CLAMP_FLUSH'
            data = 1
            src = src + 'Q cmd={}, arg={}, data={}\n'.format(rail_command, 16 + rail, data)
        elif mode_select == 'VFORCE':
            if submode_select == 'negativeslewvoltage':
                rail_command = 'SetVoltage'
                start_value_for_negative_vslew = 0.85 * over_voltage
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, start_value_for_negative_vslew)
            if submode_select == 'negativeslewvoltage' or submode_select == 'positiveslewvoltage':
                rail_command = 'TimeDelay'
                value = 1
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)
                rail_command = 0x09 + rail
                arg = 'RAMP_STEP_SIZE'
                ramp_step_size = 0x08
                ramp_step_voltage = ramp_step_size * 0.00305
                src = src + 'Q cmd={}, arg={}, data={}\n'.format(rail_command, arg, ramp_step_size)
        rail_command = 'SetMode'
        data = mode_select
        src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
        rail_command = 'EnableDisableRail'
        data = 1
        src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)

        # Randomizing the second structure of instructions
        if mode_select == 'IFORCE':
            commands = ['SetCurrent', 'SetVCompAlarm']
        else:
            commands = ['SetVoltage', 'SetVCompAlarm']
        commands_specific_to_a_rail = []
        mid_value_of_rampvoltage = 0
        final_value_of_ramp_voltage = 0

        for rail_command in commands:
            if rail_command == 'SetVoltage' and submode_select == 'none':
                data = 0.85 * over_voltage
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetVoltage' and submode_select == 'rampvoltage':
                data = 0.85 * over_voltage
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                rail_command = 'TimeDelay'
                value = 1000
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)
                rail_command = 'SetVoltage'
                mid_value_of_rampvoltage = random.uniform(0.6 * over_voltage, 0.75 * over_voltage)
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, mid_value_of_rampvoltage)
                rail_command = 'TimeDelay'
                value = 1000
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)
                rail_command = 'SetVoltage'
                mid_value_of_rampvoltage = random.uniform(0.2 * over_voltage, 0.5 * over_voltage)
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, mid_value_of_rampvoltage)
                rail_command = 'TimeDelay'
                value = 1000
                src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)
            elif rail_command == 'SetVoltage' and submode_select == 'positiveslewvoltage':
                rail_command = 'SetFreeDrive'
                start_value_for_positive_vslew = 0.85 * over_voltage
                value_to_be_used_in_steps = start_value_for_positive_vslew - 0.4
                number_of_steps = (value_to_be_used_in_steps) / (ramp_step_voltage)
                delay = math.floor(number_of_steps * 128)
                dword = '{} rail={}, start={}, delay={}, end={}\n'.format(rail_command, 16 + rail,
                                                                          start_value_for_positive_vslew, delay, 0)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetVoltage' and submode_select == 'negativeslewvoltage':
                rail_command = 'SetFreeDrive'
                number_of_steps = (start_value_for_negative_vslew) / (ramp_step_voltage)
                delay = math.floor(number_of_steps * 128)
                dword = '{} rail={}, start={}, delay={}, end={}\n'.format(rail_command, 16 + rail, 0.0, delay, 0)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetVCompAlarm':
                data = 0
                dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetCurrent':
                if specific_value_of_current_range == 'I_25_MA':
                    data = 0.025
                    dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                elif specific_value_of_current_range == 'I_500_MA':
                    data = 0.5
                    dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                elif specific_value_of_current_range == 'I_25000_MA':
                    data = 25.0
                    dword = '{} rail={}, value={}\n'.format(rail_command, 16 + rail, data)
                commands_specific_to_a_rail.append(dword)

        for individual_command in commands_specific_to_a_rail:
            available_commands_per_rail[rail].append(individual_command)

        while len(available_commands_per_rail[rail]) != 0:
            instruction = random.choice(available_commands_per_rail[rail])
            src = src + instruction
            available_commands_per_rail[rail].remove(instruction)

            # Inserting a fixed time delay and sequence break rail_command
        if submode_select == 'negativeslewvoltage':
            rail_command = 'TimeDelay'
            value = 11000
            src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)

        rail_command = '#SequenceBreak'
        delay = 21031
        src = src + '{} rail={}, delay={}\n'.format(rail_command, 16 + rail, delay)

        # Randomizing the sampling engine configurations
        rail_command = '$sampleengine'
        src = src + '{}\n'.format(rail_command)

        # Inserting fixed commands
        rail_command = 'EnableDisableRail'
        value = 0
        src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)
        rail_command = 'TimeDelay'
        value = 1000
        src = src + '{} rail={}, value={}\n'.format(rail_command, 16 + rail, value)

        # Fixed Footer for trigger queue
        src = src + trigger_queue_footer + "\nTqComplete rail=0, value=0"

        self.Log('info', src)

        # Start loading the trigger queue assembler
        asm.LoadString(Template(src).substitute(sampleengine=self.randomizehclcsampleengine(dutid, rail)))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        rail_type = 'HC'
        board.ExecuteTriggerQueue(offset, dutid, rail_type)
        railcurrent = 'DpsRail' + str(rail) + 'I'
        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} current reg is 0x{:x}'.format(rail, board.Read(railcurrent).Pack()))
        self.Log('info', 'The HCLC rail {} voltage reg is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))

        # self.env.CheckRailBusy(board)
        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        board.ReadHeaderSampleRegion(HeaderBase + 0x500 * dutid, 0x20, SampleBase + 0x1000 * dutid, 0x0200)
        board.RunCheckers( dutid, 'ResistiveLoopback', 10)

        board.ClearHclcRailAlarms()
