################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: DpsAssertionTests.py
#-------------------------------------------------------------------------------
#     Purpose: Test to verify if Correct Alarms are generated.
#-------------------------------------------------------------------------------
#  Created by:
#        Date: 05/23/18
#       Group: HDMT FPGA Validation
################################################################################

import random
import time
import unittest
import collections

from Common.instruments.dps import symbols
from Dps.Tests.dpsTest import BaseTest

OP_MODES = ['VFORCE', 'IFORCE','ISINK','VMEASURE']
HCLCHV_RAIL_BASE = 16
ALARM_PROPAGATION_DELAY = 0.01
ModesToTest = collections.namedtuple('ModesToTest', ['op_mode'])
CommandAssertData = collections.namedtuple('CommandAssertDataCombinations', ['command_data', 'assert_test_data'])

OP_MODE_ASSERT_ALARM_COMBINATIONS = {ModesToTest(op_mode='VFORCE', ):CommandAssertData(command_data=symbols.SETMODECMD.IFORCE, assert_test_data=symbols.SETMODECMD.VFORCE),
                                     ModesToTest(op_mode='IFORCE',):CommandAssertData(command_data=symbols.SETMODECMD.VFORCE, assert_test_data=symbols.SETMODECMD.IFORCE),
                                    ModesToTest(op_mode='ISINK',):CommandAssertData(command_data=symbols.SETMODECMD.IFORCE, assert_test_data=symbols.SETMODECMD.ISINK),
                                     ModesToTest(op_mode='VMEASURE',):CommandAssertData(command_data=symbols.SETMODECMD.VFORCE, assert_test_data=symbols.SETMODECMD.VMEASURE)}

OP_MODE_ASSERT_NO_ALARM_COMBINATIONS = {ModesToTest(op_mode='VFORCE', ):CommandAssertData(command_data=symbols.SETMODECMD.VFORCE, assert_test_data=symbols.SETMODECMD.VFORCE),
                                        ModesToTest(op_mode='IFORCE',):CommandAssertData(command_data=symbols.SETMODECMD.IFORCE, assert_test_data=symbols.SETMODECMD.IFORCE),
                                        ModesToTest(op_mode='ISINK',):CommandAssertData(command_data=symbols.SETMODECMD.ISINK, assert_test_data=symbols.SETMODECMD.ISINK),
                                        ModesToTest(op_mode='VMEASURE',):CommandAssertData(command_data=symbols.SETMODECMD.VMEASURE, assert_test_data=symbols.SETMODECMD.VMEASURE)}

class BaseAssertion(BaseTest):

    def initialize_rail(self, board, rail, rail_type, uhc, dutid):
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
        board.UnGangAllRails()

    def check_for_alarm(self, board, rail_type, rail, pass_count):
        if rail_type == 'VLC':
            if board.check_for_vlc_cfold_actual_and_assert_data(rail):
                pass_count = pass_count + 1
                return pass_count
        else:
            if board.check_for_hclc_cfold_actual_and_assert_data(rail):
                pass_count = pass_count + 1
                return pass_count

class OpMode(BaseAssertion):

    def DirectedVlcOpModeAssertionTest(self):
        rail_type = 'VLC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            if board.verify_if_fpga_supports_vmeasure() != True:
                try:
                    OP_MODES.remove('VMEASURE')
                except:
                    pass
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                random.shuffle(OP_MODES)
                for mode in OP_MODES:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_op_mode_scenario(board, rail , dutid, 'OPMODE', mode, rail_type, pass_count)
            if pass_count == len(OP_MODES)*len(rail_uhc_tuple_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(int(pass_count/len(OP_MODES))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(int(pass_count / len(OP_MODES)),len(rail_uhc_tuple_list)))

    def DirectedHcOpModeAssertionTest(self):
        rail_type = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            if board.verify_if_fpga_supports_vmeasure() != True:
                try:
                    OP_MODES.remove('VMEASURE')
                except:
                    pass
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                random.shuffle(OP_MODES)
                for mode in OP_MODES:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_op_mode_scenario(board, rail + HCLCHV_RAIL_BASE, dutid, 'OPMODE', mode, rail_type, pass_count)
            if pass_count == len(OP_MODES)*len(rail_uhc_tuple_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(int(pass_count/len(OP_MODES))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(int(pass_count / len(OP_MODES)),len(rail_uhc_tuple_list)))

    def DirectedLcOpModeAssertionTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            if board.verify_if_fpga_supports_vmeasure() != True:
                try:
                    OP_MODES.remove('VMEASURE')
                except:
                    pass
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                random.shuffle(OP_MODES)
                for mode in OP_MODES:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_op_mode_scenario(board, rail + HCLCHV_RAIL_BASE, dutid, 'OPMODE', mode, rail_type, pass_count)
            if pass_count == len(OP_MODES)*len(rail_uhc_tuple_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(int(pass_count/len(OP_MODES))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(int(pass_count / len(OP_MODES)),len(rail_uhc_tuple_list)))

    def DirectedHvOpModeAssertionTest(self):
        rail_type = 'HV'
        OP_MODES.remove('ISINK')
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            if board.verify_if_fpga_supports_vmeasure() != True:
                try:
                    OP_MODES.remove('VMEASURE')
                except:
                    pass
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                random.shuffle(OP_MODES)
                for mode in OP_MODES:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count =  self.check_op_mode_scenario(board, rail + HCLCHV_RAIL_BASE, dutid, 'OPMODE', mode, rail_type, pass_count)
            if pass_count == len(OP_MODES)*len(rail_uhc_tuple_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(int(pass_count/len(OP_MODES))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(int(pass_count / len(OP_MODES)),len(rail_uhc_tuple_list)))

    def check_op_mode_scenario(self,board, rail, dutid, assert_test, mode, rail_type,pass_count):
        board.SetRailsToSafeState()
        board.bring_rails_out_of_safe_state(dutid,rail,rail_type)
        command_data, assert_test_data = OP_MODE_ASSERT_NO_ALARM_COMBINATIONS[(mode,)]
        self.setup_and_send_rail_commands(assert_test, assert_test_data, board, command_data, rail)
        global_alarms = board.get_global_alarms()
        if global_alarms != []:
            self.Log('error', 'Global Alarm should be empty.Actual fields set: {}'.format(global_alarms))
        command_data, assert_test_data = OP_MODE_ASSERT_ALARM_COMBINATIONS[(mode,)]
        self.setup_and_send_rail_commands(assert_test, assert_test_data, board, command_data, rail)
        pass_count = self.check_for_alarm(board, rail_type, rail, pass_count)
        board.send_tq_footer_and_check_tq_notify_alarm(dutid,rail,rail_type)
        return pass_count



    def setup_and_send_rail_commands(self, assert_test, assert_test_data, board, command_data, rail):
        command = board.symbols.RAILCOMMANDS.SET_MODE
        board.WriteBar2RailCommand(command, command_data, rail)
        assert_command = getattr(board.symbols.ASSERTTEST, assert_test)
        data = (assert_command << 8) | assert_test_data
        command = board.symbols.RAILCOMMANDS.ASSERT_TEST_RAIL
        board.WriteBar2RailCommand(command, data, rail)
        time.sleep(ALARM_PROPAGATION_DELAY)


class IRange(BaseAssertion):

    def DirectedHvIrangeAssertionTest(self):
        rail_type = 'HV'
        pass_count = 0
        irange_list = ['I_5_UA', 'I_25_UA', 'I_250_UA', 'I_2_5_MA', 'I_25_MA', 'I_500_MA', 'I_7500_MA']
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                for irange in irange_list:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_irange_scenario(board, rail + HCLCHV_RAIL_BASE, dutid, 'IRANGE', irange, rail_type, pass_count)
            if pass_count == len(rail_uhc_tuple_list)*len(irange_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(
                    int(pass_count / len(irange_list))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(
                             int(pass_count / len(irange_list)), len(rail_uhc_tuple_list)))

    def DirectedHcIrangeAssertionTest(self):
        rail_type = 'HC'
        pass_count = 0
        irange_list = ['I_5_UA', 'I_25_UA', 'I_250_UA', 'I_2_5_MA', 'I_25_MA', 'I_500_MA', 'I_1200_MA', 'I_25000_MA',
                 'I_3000_MA']
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                for irange in irange_list:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_irange_scenario(board, rail + HCLCHV_RAIL_BASE, dutid, 'IRANGE', irange, rail_type, pass_count)
            if pass_count == len(rail_uhc_tuple_list)*len(irange_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(
                    int(pass_count / len(irange_list))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(
                             int(pass_count / len(irange_list)), len(rail_uhc_tuple_list)))

    def DirectedLcIrangeAssertionTest(self):
        rail_type = 'LC'
        pass_count = 0
        irange_list = ['I_5_UA', 'I_25_UA', 'I_250_UA', 'I_2_5_MA', 'I_25_MA', 'I_500_MA', 'I_1200_MA', 'I_25000_MA',
                 'I_3000_MA']
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                for irange in irange_list:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_irange_scenario(board, rail + HCLCHV_RAIL_BASE, dutid, 'IRANGE', irange, rail_type, pass_count)
            if pass_count == len(rail_uhc_tuple_list)*len(irange_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(
                    int(pass_count / len(irange_list))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(
                             int(pass_count / len(irange_list)), len(rail_uhc_tuple_list)))

    def DirectedVlcIrangeAssertionTest(self):
        rail_type = 'VLC'
        pass_count = 0
        irange_list = ['I_2_56_UA', 'I_25_6_UA', 'I_256_UA', 'I_2_56_MA', 'I_25_6_MA', 'I_256_MA']
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            for rail, uhc in rail_uhc_tuple_list:
                dutid = random.randint(0, 63)
                for irange in irange_list:
                    self.initialize_rail(board, rail, rail_type, uhc, dutid)
                    pass_count = self.check_irange_scenario(board, rail, dutid, 'IRANGE', irange,rail_type, pass_count)
            if pass_count == len(rail_uhc_tuple_list)*len(irange_list):
                self.Log('info', 'Expected Cfold alarm seen on all {} DUT/Rail combinations '.format(
                    int(pass_count / len(irange_list))))
            else:
                self.Log('error',
                         'Cfold alarm seen on {} DUT/Rail combinations out of {}'.format(
                             int(pass_count / len(irange_list)), len(rail_uhc_tuple_list)))

    def check_irange_scenario(self,board, rail, dutid, assert_test, mode, rail_type,pass_count):
        board.SetRailsToSafeState()
        board.bring_rails_out_of_safe_state(dutid,rail,rail_type)
        if rail_type == 'VLC':
            command_data = getattr(symbols.VLCIRANGE, mode)
            no_alarm_assert_data = getattr(symbols.VLCIRANGE, mode)
            alarm_assert_data = (getattr(symbols.VLCIRANGE, mode)) +1
        else:
            command_data = getattr(symbols.HCLCIRANGE, mode)
            no_alarm_assert_data = getattr(symbols.HCLCIRANGE, mode)
            alarm_assert_data = (getattr(symbols.HCLCIRANGE, mode)) + 1

        self.setup_and_send_irange_rail_command(assert_test, no_alarm_assert_data, board, command_data, rail)
        global_alarms = board.get_global_alarms()
        if global_alarms != []:
            self.Log('error', 'Global Alarm should be empty.Actual fields set: {}'.format(global_alarms))
        self.setup_and_send_irange_rail_command(assert_test, alarm_assert_data, board, command_data, rail)
        pass_count = self.check_for_alarm(board, rail_type, rail, pass_count)
        board.send_tq_footer_and_check_tq_notify_alarm(dutid, rail, rail_type)
        return pass_count

    def setup_and_send_irange_rail_command(self, assert_test, assert_test_data, board, command_data, rail):
        command = board.symbols.RAILCOMMANDS.SET_CURRENT_RANGE
        board.WriteBar2RailCommand(command, command_data, rail)
        assert_command = getattr(board.symbols.ASSERTTEST, assert_test)
        data = (assert_command << 8) | assert_test_data
        command = board.symbols.RAILCOMMANDS.ASSERT_TEST_RAIL
        board.WriteBar2RailCommand(command, data, rail)
        time.sleep(ALARM_PROPAGATION_DELAY)






