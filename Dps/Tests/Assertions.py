################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: 
# -------------------------------------------------------------------------------
#     Purpose: 
# -------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez/Renuka Agrawal
#        Date: 10/29/15
#       Group: HDMT FPGA Validation
################################################################################

import struct
from string import Template
import time
from Common.instruments.dps.symbols import RAILCOMMANDS, SETMODECMD, VLCIRANGE
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest
import unittest

class Conditions(BaseTest):

    def ConfigureHclcSampleEngine(self, slot, subslot, deviceid, dutid, headerbase, headersize, samplebase, samplesize,
                                  count, rate, metahi, metalo, rail, start, rail_type):
        engineresetvalue = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]
        board = self.env.instruments[slot].subslots[subslot]

        self.Log('info',
                 '\nConfiguring sampling engine for Dut-> {}, slot-> {}, subslot-> {}'.format(dutid, slot, subslot))

        SampleCountReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleCount')
        SampleRateReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleRate')
        SampleMetaHiReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleMetadataHi')
        SampleMetaLoReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleMetadataLo')
        SampleRailSelReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleRailSelect')
        SampleStartReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleStart')

        board.InitializeSampleEngine(headerbase, headersize, samplebase, samplesize, dutid, rail_type)

        board.WriteRegister(SampleCountReg(value=count))
        board.WriteRegister(SampleRateReg(value=rate))
        board.WriteRegister(SampleMetaHiReg(value=metahi))
        board.WriteRegister(SampleMetaLoReg(value=metalo))
        board.WriteRegister(SampleRailSelReg(value=rail))
        board.WriteRegister(SampleStartReg(value=start))
        self.Log('info', 'Starting the sampling engine')

    def ConfigureVlcSampleEngine(self, slot, subslot, deviceid, uhc, headerbase, headersize, samplebase, samplesize,
                                 count, rate, metahi, metalo, rail, start):
        engineresetvalue = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]
        self.Log('info',
                 '\nConfiguring sampling engine for Dut-> {}, slot-> {}, subslot-> {}'.format(uhc, slot, subslot))
        board = self.env.instruments[slot].subslots[subslot]

        board.InitializeSampleEngine(headerbase, headersize, samplebase, samplesize, uhc, 'VLC')

        SampleCountReg = getattr(board.registers,'Vlc' + str(uhc) + 'SampleCount')
        SampleRateReg = getattr(board.registers,'Vlc' + str(uhc) + 'SampleRate')
        SampleMetaHiReg = getattr(board.registers,'Vlc' + str(uhc) + 'SampleMetadataHi')
        SampleMetaLoReg = getattr(board.registers,'Vlc' + str(uhc) + 'SampleMetadataLo')
        SampleRailSelReg = getattr(board.registers,'Vlc' + str(uhc) + 'SampleRailSelect')
        SampleStartReg = getattr(board.registers,'Vlc' + str(uhc) + 'SampleStart')

        board.WriteRegister(SampleCountReg(value=count))
        board.WriteRegister(SampleRateReg(value=rate))
        board.WriteRegister(SampleMetaHiReg(value=metahi))
        board.WriteRegister(SampleMetaLoReg(value=metalo))
        board.WriteRegister(SampleRailSelReg(value=rail))
        board.WriteRegister(SampleStartReg(value=start))
        self.Log('info', 'Starting the sampling engine')

    def SimulateNCheckHclcSampleStartAlarm(self, slot, subslot, deviceid, dutid):
        expected = [0x1, 0x4, 0x10, 0x40, 0x100, 0x400, 0x1000, 0x4000]
        board = self.env.instruments[slot].subslots[subslot]

        self.Log('info', 'Starting the sampling engine for Dut -> {}'.format(dutid))
        SampleStartReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleStart')
        board.WriteRegister(SampleStartReg(value=0x0001))

        globalalarm = board.Read('GLOBAL_ALARMS')
        if globalalarm.HclcSampleAlarms != 1:
            self.Log('error',
                     'HclcSampleAlarms should get set in the GLOBAL_ALARMS Register for slot {} , subslot {}. Actual-> 0x{:x}. Expected-> 0x400'.format(
                         slot, subslot, globalalarm.Pack()))
        else:
            self.Log('info', 'GLOBAL_ALARMS Register is set to 0x{:x}'.format(globalalarm.Pack()))
            hclcsamplealarm = board.Read('HclcSampleAlarms')
            expectedbit = 'HclcDut' + str(dutid) + 'SampleStart'
            if getattr(hclcsamplealarm, expectedbit) != 1:
                self.Log('error',
                         'HclcSampleAlarms Register for slot {} , subslot {} is set-> 0x{:x}. Expected-> 0x{}'.format(
                             slot, subslot, hclcsamplealarm.Pack(), expected[dutid]))
            else:
                self.Log('info', 'The HclcSampleAlarms Register is set to 0x{:x}'.format(hclcsamplealarm.Pack()))

    def SimulateNCheckVlcSampleStartAlarm(self, slot, subslot, deviceid, dutid):
        expected = [0x1, 0x4, 0x10, 0x40, 0x100, 0x400, 0x1000, 0x4000]
        board = self.env.instruments[slot].subslots[subslot]
        time.sleep(.1)
        self.Log('info', 'Starting the sampling engine for Dut -> {}'.format(dutid))
        SampleStartReg = getattr(board.registers,'Vlc' + str(dutid) + 'SampleStart')
        board.WriteRegister(SampleStartReg(value=0x0001))

        globalalarm = board.Read('GLOBAL_ALARMS')
        if globalalarm.VlcSampleAlarms != 1:
            self.Log('error',
                     'VlcSampleAlarms should get set in the GLOBAL_ALARMS Register for slot {} , subslot {}. Actual-> 0x{:x}. Expected-> 0x400'.format(
                         slot, subslot, globalalarm.Pack()))
        else:
            self.Log('info', 'GLOBAL_ALARMS Register is set to 0x{:x}'.format(globalalarm.Pack()))
            hclcsamplealarm = board.Read('VlcSampleAlarms')
            expectedbit = 'VlcDut' + str(dutid) + 'SampleStart'
            if getattr(hclcsamplealarm, expectedbit) != 1:
                self.Log('error',
                         'VlcSampleAlarms Register for slot {} , subslot {} is set-> 0x{:x}. Expected-> 0x{}'.format(
                             slot, subslot, hclcsamplealarm.Pack(), expected[dutid]))
            else:
                self.Log('info', 'The VlcSampleAlarms Register is set to 0x{:x}'.format(hclcsamplealarm.Pack()))

    def SimulateNCheckHclcSampleOutOfBoundAlarm(self, slot, subslot, deviceid, dutid, sampleregionsize):
        expected = [0x2, 0x8, 0x20, 0x80, 0x200, 0x800, 0x2000, 0x8000]
        engineresetvalue = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]
        board = self.env.instruments[slot].subslots[subslot]

        self.Log('info', 'Simulating the out of bounds sample alarm for Dut -> {}'.format(dutid))
        board.WriteRegister(board.registers.HclcUhcSampleRegionSize(value=sampleregionsize), index=dutid)
        self.Log('info', '{} is set to 0x{:x}'.format(board.registers.HclcUhcSampleRegionSize.__name__,
                                                      board.ReadRegister(board.registers.HclcUhcSampleRegionSize,index=dutid).value))

        board.WriteRegister(board.registers.HclcSampleEngineReset(value=engineresetvalue[dutid]))

        SampleStartReg = getattr(board.registers,'Hclc' + str(dutid) + 'SampleStart')
        board.WriteRegister(SampleStartReg(value=0x0001))

        globalalarm = board.Read('GLOBAL_ALARMS')
        if globalalarm.HclcSampleAlarms != 1:
            self.Log('error',
                     'HclcSampleAlarms should get set in the GLOBAL_ALARMS Register for slot {} , subslot {}. Actual-> 0x{:x}. Expected-> 0x400'.format(
                         slot, subslot, globalalarm.Pack()))
        else:
            self.Log('info', 'GLOBAL_ALARMS Register is set to 0x{:x}'.format(globalalarm.Pack()))
            hclcsamplealarm = board.Read('HclcSampleAlarms')
            expectedbit = 'HclcDut' + str(dutid) + 'OutOfBounds'
            if getattr(hclcsamplealarm, expectedbit) != 1:
                self.Log('error',
                         'HclcSampleAlarms Register for slot {} , subslot {} is set-> 0x{:x}. Expected-> 0x{:x}'.format(
                             slot, subslot, hclcsamplealarm.Pack(), expected[dutid]))
            else:
                self.Log('info', 'The HclcSampleAlarms Register is set to 0x{:x}'.format(hclcsamplealarm.Pack()))

    def SimulateNCheckVlcSampleOutOfBoundAlarm(self, slot, subslot, deviceid, dutid, sampleregionsize):
        expected = [0x2, 0x8, 0x20, 0x80, 0x200, 0x800, 0x2000, 0x8000]
        engineresetvalue = [0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80]
        board = self.env.instruments[slot].subslots[subslot]

        self.Log('info', 'Simulating the out of bounds sample alarm for Dut -> {}'.format(dutid))

        board.WriteRegister(board.registers.HclcUhcSampleRegionSize(value=sampleregionsize), index=dutid)

        self.Log('info', '{} is set to 0x{:x}'.format(board.registers.HclcUhcSampleRegionSize.__name__,
                                                      board.ReadRegister(board.registers.HclcUhcSampleRegionSize(), index=dutid).value))

        board.WriteRegister(board.registers.VlcSampleEngineReset(value=engineresetvalue[dutid]))

        SampleStartReg = getattr(board.registers,'Vlc' + str(dutid) + 'SampleStart')
        board.WriteRegister(SampleStartReg(value=0x0001))

        globalalarm = board.Read('GLOBAL_ALARMS')
        if globalalarm.VlcSampleAlarms != 1:
            self.Log('error',
                     'VlcSampleAlarms should get set in the GLOBAL_ALARMS Register for slot {} , subslot {}. Actual-> 0x{:x}. Expected-> 0x400'.format(
                         slot, subslot, globalalarm.Pack()))
        else:
            self.Log('info', 'GLOBAL_ALARMS Register is set to 0x{:x}'.format(globalalarm.Pack()))
            hclcsamplealarm = board.Read('VlcSampleAlarms')
            expectedbit = 'VlcDut' + str(dutid) + 'OutOfBounds'
            if getattr(hclcsamplealarm, expectedbit) != 1:
                self.Log('error',
                         'VlcSampleAlarms Register for slot {} , subslot {} is set-> 0x{:x}. Expected-> 0x{:x}'.format(
                             slot, subslot, hclcsamplealarm.Pack(), expected[dutid]))
            else:
                self.Log('info', 'The VlcSampleAlarms Register is set to 0x{:x}'.format(hclcsamplealarm.Pack()))

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedHclcSampleAlarmTest(self):

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','LC']):
            board.SetRailsToSafeState()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            for dutid in range(0, 8):
                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x03ff,'HC')

                self.ConfigureHclcSampleEngine(board.slot, board.subslot, 0x0, dutid, 0x0, 0x10, 0x10000, 0x1000000, 0xffff, 0x0,
                                               0x7654, 0x3210, 0x3ff, 0x0001, 'LC')
                # Set the Hclc Sample Engine 0 Base and Size

                # Writing to scratch register
                board.WriteRegister(board.registers.Scratch(value=0x1111))
                data = board.ReadRegister(board.registers.Scratch).value
                self.Log('debug', 'Scratch register is set to 0x{:x}'.format(data))

                # Read the global alarm register
                globalalarm = board.Read('GLOBAL_ALARMS')
                if globalalarm.Pack() != 0:
                    self.Log('error',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set->0x{:x}. Expected->0x0'.format(board.slot,
                                                                                                                   board.subslot,
                                                                                                                   globalalarm.Pack()))
                else:
                    self.Log('info',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                      globalalarm.Pack()))

                # Simulating Sample start alarm
                self.SimulateNCheckHclcSampleStartAlarm(board.slot, board.subslot, 0x0, dutid)
                board.ClearVlcSampleAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearGlobalAlarms()

                # Simulate Hclc Dut 0 OutOfBounds Alarm
                self.SimulateNCheckHclcSampleOutOfBoundAlarm(board.slot, board.subslot, 0x0, dutid, 0x00000010)
                board.ClearVlcSampleAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearGlobalAlarms()
                
    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedVlcSampleAlarmTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            for dutid in range(8):
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.SetRailsToSafeState()
                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0xffff,'VLC')

                # ConfigureVlcSampleEngine(self,slot,subslot,deviceid,dutid,headerbase, headersize, samplebase, samplesize, count, rate, metahi, metalo, rail, start):
                self.ConfigureVlcSampleEngine(board.slot, board.subslot, 0x0, dutid, 0x200, 0x10, 0x10000, 0x1000000, 0xffff, 0x0,
                                              0x7654, 0x3210, 0xffff, 0x0001)
                # Set the Vlc Sample Engine 0 Base and Size

                # Writing to scratch register
                board.WriteRegister(board.registers.Scratch(value=0xfdec))
                data = board.ReadRegister(board.registers.Scratch).value
                self.Log('debug', 'Scratch register is set to 0x{:x}'.format(data))

                # Read the global alarm register
                globalalarm = board.Read('GLOBAL_ALARMS')
                if globalalarm.Pack() != 0:
                    self.Log('error',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set->0x{:x}. Expected->0x0'.format(board.slot,
                                                                                                                   board.subslot,
                                                                                                                   globalalarm.Pack()))
                else:
                    self.Log('info',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                      globalalarm.Pack()))

                # Simulating Sample start alarm
                self.SimulateNCheckVlcSampleStartAlarm(board.slot, board.subslot, 0x0, dutid)
                board.ClearHclcSampleAlarms()
                board.ClearVlcSampleAlarms()
                board.ClearGlobalAlarms()

                # Simulate Vlc Dut 0 OutOfBounds Alarm
                self.SimulateNCheckVlcSampleOutOfBoundAlarm(board.slot, board.subslot, 0x0, dutid, 0x000100)
                board.ClearVlcSampleAlarms()
                board.ClearGlobalAlarms()
                board.ClearHclcSampleAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedVlcPowerStateAssertionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            board.SetRailsToSafeState()
            board.EnableOnlyOneUhc(0)
            board.ConfigureUhcRail(0, 0xffff,'VLC')
            modes = ['ON', 'OFF']
            for rail in range(0, 16):
                for mode in modes:
                    board.CheckRailAssertTest(rail, 'POWER', mode)
            board.ClearVlcCfoldAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedHclcPowerStateAssertionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','LC']):
            board.SetRailsToSafeState()
            board.EnableOnlyOneUhc(0)
            board.ConfigureUhcRail(0, 0x03ff,'HC')
            modes = ['ON', 'OFF']
            for rail in range(16, 26):
                for mode in modes:
                    board.CheckRailAssertTest(rail, 'POWER', mode)
            board.ClearHclcCfoldAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedVlcOpModeAssertionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            board.SetRailsToSafeState()
            board.EnableOnlyOneUhc(0)
            board.ConfigureUhcRail(0, 0xffff,'VLC')
            modes = ['VFORCE', 'IFORCE', 'ISINK']
            for rail in range(0, 16):
                for mode in modes:
                    board.CheckRailAssertTest(rail, 'OPMODE', mode)
            board.ClearVlcCfoldAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedHclcOpModeAssertionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','LC']):
            board.SetRailsToSafeState()
            board.EnableOnlyOneUhc(0)
            board.ConfigureUhcRail(0, 0x03ff,'HC')
            modes = ['VFORCE', 'IFORCE', 'ISINK']
            for rail in range(16, 26):
                for mode in modes:
                    board.CheckRailAssertTest(rail, 'OPMODE', mode)
            board.ClearHclcCfoldAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedVlcIRangeAssertionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            board.SetRailsToSafeState()
            board.EnableOnlyOneUhc(0)
            board.ConfigureUhcRail(0, 0xffff,'VLC')
            modes = ['I_2_56_UA', 'I_25_6_UA', 'I_256_UA', 'I_2_56_MA', 'I_25_6_MA', 'I_256_MA']
            for rail in range(0, 16):
                for mode in modes:
                    board.CheckRailAssertTest(rail, 'IRANGE', mode)
            board.ClearVlcCfoldAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedHclcIRangeAssertionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','LC']):
            board.SetRailsToSafeState()
            board.EnableOnlyOneUhc(0)
            board.ConfigureUhcRail(0, 0x03ff,'HC')
            modes = ['I_5_UA', 'I_25_UA', 'I_250_UA', 'I_2_5_MA', 'I_25_MA', 'I_500_MA', 'I_1200_MA', 'I_25000_MA',
                     'I_3000_MA']
            for rail in range(16, 26):
                for mode in modes:
                    board.CheckRailAssertTest(rail, 'IRANGE', mode)
            board.ClearHclcCfoldAlarms()

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedVlcPcieVsimTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.SetRailsToSafeState()
            board.ConnectCalBoard('VLC00', 'OHM_4')
            board.ConfigureUhcRail(0, 0xffff,'VLC')
            # Set the Vlc Sample Engine 0 Base and Size
            board.WriteRegister(board.registers.VlcUhcSampleHeaderRegionBase(value=0x00000000), index=0)
            board.WriteRegister(board.registers.VlcUhcSampleHeaderRegionSize(value=0x00000010), index=0)
            board.WriteRegister(board.registers.VlcUhcSampleRegionBase(value=0x00010000), index=0)
            board.WriteRegister(board.registers.VlcUhcSampleRegionSize(value=0x01000000), index=0)

            # Writing to scratch register
            board.WriteRegister(board.registers.Scratch(value=0x1111))
            data = board.ReadRegister(board.registers.Scratch).value
            self.Log('info', 'Scratch register is set to {}'.format(data))

            for rail in range(0, 1):
                self.Log('info', 'Testing rail: {} on slot: {} subslot: {}'.format(rail, board.slot, board.subslot))

                board.WriteTQHeaderViaBar2(0, rail, 'VLC')

                command = RAILCOMMANDS.ENABLE_DISABLE_RAIL
                board.WriteBar2RailCommand(command, 0, rail)

                command = RAILCOMMANDS.SET_MODE
                data = SETMODECMD.VFORCE
                board.WriteBar2RailCommand(command, data, rail)

                command = RAILCOMMANDS.SET_CURRENT_RANGE
                data = VLCIRANGE.I_256_MA
                board.WriteBar2RailCommand(command, data, rail)

                # Set max IClampHi command
                command = RAILCOMMANDS.SET_CURRENT_CLAMP_HI
                board.WriteBar2RailCommand(command, 0x418B, rail)

                # Set max IClampLo command
                command = RAILCOMMANDS.SET_CURRENT_CLAMP_LOW
                board.WriteBar2RailCommand(command, 0xC18B, rail)

                # VForce = 0 command
                command = RAILCOMMANDS.SET_VOLTAGE
                board.WriteBar2RailCommand(command, 0x1000, rail)

                command = RAILCOMMANDS.ENABLE_DISABLE_RAIL
                board.WriteBar2RailCommand(command, 1, rail)

                command = RAILCOMMANDS.TIME_DELAY
                board.WriteBar2RailCommand(command, 10000, rail)

                # Resetting HCLC and VLC sampling engines for UHC 0.
                board.WriteRegister(board.registers.VlcSampleEngineReset(value=0x0001))

                # Set sample count
                board.WriteRegister(board.registers.Vlc0SampleCount(value=0x1000))

                # Skip Count
                board.WriteRegister(board.registers.Vlc0SampleRate(value=0x0000))

                # Set Meta Hi Data
                board.WriteRegister(board.registers.Vlc0SampleMetadataHi(value=0x7654))

                # Set meta Lo Data
                board.WriteRegister(board.registers.Vlc0SampleMetadataLo(value=0x3210))

                # Masking all rails
                board.WriteRegister(board.registers.Vlc0SampleRailSelect(value=0x0001))

                # Start the sampling engine. Any value can start the engine
                board.WriteRegister(board.registers.Vlc0SampleStart(value=0x0001))

                board.WriteTQFooterViaBar2(0, rail, 'VLC')

            globalalarm = board.Read('GLOBAL_ALARMS')
            self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                      globalalarm.Pack()))

            board.CheckVlcSampleAlarms()

            self.Log('info', 'Reading header')
            data = board.DmaRead(0x0, 0x20)
            board.ReadMemory(data)

            self.Log('info', 'Reading sample engine')
            data = board.DmaRead(0x00010000, 0x050)
            board.ReadSamples(data)

    @unittest.skip('This test currently skipped while tests are being audited')
    def DirectedTqnotifyPerUhcTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','VLC']):
            board.SetRailsToSafeState()
            board.ClearTrigExecAlarms()
            board.Write('ENABLE_ALARMS', 0x1)
            for dutid in range(8):
                board.SetRailsToSafeState()
                # The global alarm register as write clear bits, need to clear tq notify alarm for all dut before enabling any dut    
                board.Write('GLOBAL_ALARMS', 0xffff)
                board.WriteRegister(board.registers.Scratch(value=0x1111))
                board.EnableOnlyOneUhc(dutid)
                board.EnableOnlyOneUhc(dutid)
                board.EnableOnlyOneUhc(dutid)
                if board.subslot == 1:
                    rail_type = 'HC'
                    board.ConfigureUhcRail(dutid, 0x03ff,rail_type)
                    trigger_queue_header = board.getTriggerQueueHeaderString(dutid, 'HC')
                    trigger_queue_footer = board.getTriggerQueueFooterString(dutid, 'HC')
                    print(trigger_queue_footer)
                else:
                    trigger_queue_header = board.getTriggerQueueHeaderString(dutid, 'VLC')
                    trigger_queue_footer = board.getTriggerQueueFooterString(dutid, 'VLC')
                    rail_type = 'VLC'
                    board.ConfigureUhcRail(dutid, 0xffff,rail_type)
                    board.ConfigureUhcRail(dutid, 0x03ff,rail_type)
                    print(trigger_queue_footer)

                asm = TriggerQueueAssembler()
                asm.symbols['VID'] = 2 * dutid
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 1 + 2 * dutid
                asm.LoadString(Template("""\
                    Begin
                    $tiggerqueueheader
                    Q cmd=0x01, arg=Scratch, data=0xe0e0
                    $triggerqueuefooter
                    TqComplete rail=0, value=0
                """).substitute(tiggerqueueheader = trigger_queue_header, triggerqueuefooter = trigger_queue_footer))
                data = asm.Generate()
                self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset = 0
                board.WriteTriggerQueue(offset, data)
                data = board.DmaRead(0x0, 0x20)
                board.ReadMemory(data)
                board.ExecuteTriggerQueue(offset, dutid, rail_type)

                data = board.ReadRegister(board.registers.Scratch).value
                self.Log('info', 'Scratch register is set to {}'.format(data))

    @unittest.skip('This test currently skipped while TQ UHC test is being audited')
    def MiniDPSTQforMultipleUHCviaRcTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC','VLC']):
            board = self.env.instruments[board.slot].subslots[board.subslot]
            board.SetRailsToSafeState()
            board.ClearTrigExecAlarms()
            board.ClearTriggerDebugFifo()
            board.Write('ENABLE_ALARMS', 0x1)
            lutdwordaddress = 0x10000000
            for dutid in range(8):
                triggerValue = 0x4000000
                # The global alarm register as write clear bits, need to clear tq notify alarm for all dut before enabling any dut    
                board.Write('GLOBAL_ALARMS', 0xffff)
                board.WriteRegister(board.registers.Scratch(value=0x1111))
                board.EnableOnlyOneUhc(dutid)
                if board.subslot == 1:
                    rail_type = 'HC'
                    board.ConfigureUhcRail(dutid, 0x03ff,rail_type)
                    trigger_queue_header = board.getTriggerQueueHeaderString(dutid, 'HC')
                    trigger_queue_footer = board.getTriggerQueueFooterString(dutid, 'HC')

                else:
                    rail_type = 'VLC'
                    board.ConfigureUhcRail(dutid, 0x0ffff,rail_type)
                    board.ConfigureUhcRail(dutid, 0x03ff,'LC')
                    trigger_queue_header = board.getTriggerQueueHeaderString(dutid, 'VLC')
                    trigger_queue_footer = board.getTriggerQueueFooterString(dutid, 'VLC')

                asm = TriggerQueueAssembler()
                asm.symbols['DUTID'] = 0x1000 + dutid
                asm.symbols['VID'] = 2 * dutid
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 1 + 2 * dutid

                asm.LoadString(Template("""\
                    Begin
                    $tiggerqueueheader
                    Q cmd=0x01, arg=Scratch, data=DUTID
                    TimeDelay rail=16, value=5000
                    $triggerqueuefooter
                    TqComplete rail=0, value=0
                """).substitute(tiggerqueueheader = trigger_queue_header, triggerqueuefooter = trigger_queue_footer))
                data = asm.Generate()
                self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset = 0x100 * dutid

                self.Log('info', 'Offset Address is 0x{:x}'.format(offset))
                board.WriteTriggerQueue(offset, data)
                data = board.DmaRead(offset, 0x20)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000)
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset))
                data = board.DmaRead(lutDwordAddr, 0x20)
                board.ReadMemory(data)

                triggerValue = triggerValue + (0x100000 * dutid)
                board.ExecuteTriggerQueueviaRc(offset, dutid, triggerValue, rail_type)

                data = board.ReadRegister(board.registers.Scratch).value
                self.Log('info', 'Scratch register is set to 0x{:x}'.format(data))
