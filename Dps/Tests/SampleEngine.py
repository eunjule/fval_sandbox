################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: SampleEngine.py
#-------------------------------------------------------------------------------


import random
import struct
import io
import time
import re

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString
from Common.instruments.dps.symbols import RAILCOMMANDS,SETMODECMD,HCLCIRANGE,VRANGE
from Dps.Tests.dpsTest import BaseTest

POSITIVE_VOLTAGE_RANGE = {'HC':(0,2.5),'LC':(0,5),'HV':(1,15),'LVM':(1,4)}
POSITIVE_VOLTAGE_RANGE_LOAD = {'HC':(1,2.5),'LC':(1,4),'HV':(10,15),'LVM':(1,4)}

POSITIVE_VOLTAGE_STEPSIZE = {'HC':0.25,'LC':0.25,'HV':0.25,'LVM':0.125}

NEGATIVE_VOLTAGE_RANGE = {'LC':(-2,0)}
NEGATIVE_VOLTAGE_STEPSIZE = {'LC':0.25}

CLAMP_LIMITS = {'HC':(-0.5,0.5),'LC':(-0.5,0.5),'HV':(-0.5,0.5)}
CLAMP_LIMITS_LOAD = {'HC':(-0.5,0.5),'LC':(-0.5,0.5),'HV':(-0.5,2)}

VOLTAGE_LIMITS = {'HC':(-3,10),'LC':(-3,10),'HV':(-3,18)}
VOLTAGE_LIMITS_LOAD = {'HC':(-3,10),'LC':(-3,10),'HV':(-3,18)}

I_RANGE = {'HC':'I_500_MA','LC':'I_500_MA','HV':'I_500_MA'}
I_RANGE_LOAD = {'HC':'I_500_MA','LC':'I_500_MA','HV':'I_7500_MA'}

FREE_DRIVE_LIMITS = {'HC':(-0.5,0.5),'LC':(-0.5,0.5),'HV':(-0.5,0.5)}
FREE_DRIVE_DELAY = {'HC':6000,'LC':6000,'HV':6000}

CAL_LOAD = {'HC':'OHM_10','HV':'10Ohm','LC':'OHM_10'}
ALARM_PROPAGATION_DELAY = 0.1


HV_SAMPE_DELAY = 30000
HV_SAMPLE_COUNT = 0x4000
HV_HEADER_BASE = 0x00005000
HV_HEADER_SIZE = 0x00000100
HV_SAMPLE_BASE = 0x00010000
HV_SAMPLE_SIZE = 0x00100000

SAMPE_DELAY = 30000
SAMPLE_COUNT = 0x40
SAMPLE_RATE = 0

HEADER_BASE = 0x00005000
HEADER_SIZE = 0x00000100
SAMPLE_BASE = 0x00010000
SAMPLE_SIZE = 0x00100000

class SampleEngineHelper:

    def force_voltage_open_socket_scenario_with_sample(self,board,rail_type,iterations, negative = False,turbo=False):
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, rail_type)
        for test_iteration in range(iterations):
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                board.SetRailsToSafeState()
                dutid = random.randint(0,63)
                force_voltage = self.get_force_voltage(board, negative, rail_type)
                self.initialize_rail(board, rail, rail_type, uhc, dutid)
                self.initialize_sample_engine(board, rail_type, turbo, uhc)

                trigger_queue_data = self.generate_trigger_queue(board,rail_type,force_voltage, dutid, rail,uhc,turbo)
                trigger_queue_offset = 0x0
                board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
                board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)
                try:
                    self.verify_alarm_status(board, rail, rail_type)
                    self.verify_expected_voltage(board, rail, uhc, rail_type, force_voltage)
                    sample_headers = board.GetSampleHeaders(uhc, 1, rail_type,turbo=turbo)
                    self.verify_sample_header(sample_headers,rail, rail_type,turbo=turbo)
                    self.verify_sample_data(board,sample_headers,force_voltage, rail_type,turbo = turbo)
                    pass_count += 1
                except self.sample_engine_exception:
                    pass

            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         'Expected sample engine behavior seen seen on all UHC/Rail combinations: iteration {} of {}'.format(
                             test_iteration+1, iterations ))
            else:
                self.Log('error',
                         'Unexpected sample engine behavior seen on {} of {} UHC/Rail combinations during test iteration {} of {}.'.format(
                             len(rail_uhc_tuple_list)-pass_count, len(rail_uhc_tuple_list), test_iteration+1, iterations))

        board.SetRailsToSafeState()

    def force_voltage_open_socket_scenario_with_current_sample(self,board,rail_type,iterations, negative = False,turbo=False):
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, rail_type)
        for test_iteration in range(iterations):
            pass_count = 0
            calboard = board.create_cal_board_instance_and_initialize()
            board.cal_load_connect(CAL_LOAD[rail_type], calboard)
            for rail, uhc in rail_uhc_tuple_list:
                board.SetRailsToSafeState()
                dutid = random.randint(0,63)
                force_voltage = self.get_force_voltage(board, negative, rail_type,load=True)
                self.initialize_rail(board, rail, rail_type, uhc, dutid)
                board.cal_connect_force_sense_lines(rail, rail_type, calboard)
                cal_card_connect_delay = 0.01
                time.sleep(cal_card_connect_delay)
                self.initialize_sample_engine(board, rail_type, turbo, uhc)

                trigger_queue_data = self.generate_current_trigger_queue(board,rail_type,force_voltage, dutid, rail,uhc,turbo)
                trigger_queue_offset = 0x0
                set_v_range_data = 0x8001
                board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_V_RANGE, data=set_v_range_data, rail_or_resourceid=rail)
                board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
                board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)
                load_resistance = int(re.sub('\D', '', CAL_LOAD[rail_type]))
                expected_current = force_voltage/load_resistance

                try:
                    self.verify_alarm_status(board, rail, rail_type)
                    self.verify_expected_voltage(board, rail, uhc, rail_type, force_voltage)
                    self.verify_expected_current(board, rail, uhc, rail_type, expected_current)
                    sample_headers = board.GetSampleHeaders(uhc, 1, rail_type,turbo=turbo)
                    self.verify_sample_header(sample_headers,rail, rail_type,turbo=turbo)
                    self.verify_sample_data(board,sample_headers,force_voltage, rail_type, turbo = turbo, test_current = True)
                    pass_count += 1
                except self.sample_engine_exception:
                    pass
                board.cal_disconnect_force_sense_lines(rail, rail_type, calboard)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         'Expected sample engine behavior seen seen on all UHC/Rail combinations: iteration {} of {}'.format(
                             test_iteration+1, iterations ))
            else:
                self.Log('error',
                         'Unexpected sample engine behavior seen on {} of {} UHC/Rail combinations during test iteration {} of {}.'.format(
                             len(rail_uhc_tuple_list)-pass_count, len(rail_uhc_tuple_list), test_iteration+1, iterations))
        board.create_cal_board_instance_and_initialize()
        board.SetRailsToSafeState()

    def initialize_rail(self, board, rail, rail_type, uhc, dutid):
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
        board.UnGangAllRails()

    def initialize_sample_engine(self, board, rail_type, turbo, uhc):
        if turbo == True:
            if rail_type == 'HV':
                board.InitializeTurboSampleEngine(HV_HEADER_BASE, HV_HEADER_SIZE, HV_SAMPLE_BASE, HV_SAMPLE_SIZE, uhc,
                                             rail_type)
            else:
                board.InitializeTurboSampleEngine(HEADER_BASE, HEADER_SIZE, SAMPLE_BASE, SAMPLE_SIZE, uhc, rail_type)
        else:
            if rail_type == 'HV':
                board.InitializeSampleEngine(HV_HEADER_BASE, HV_HEADER_SIZE, HV_SAMPLE_BASE, HV_SAMPLE_SIZE, uhc, rail_type)
            else:
                board.InitializeSampleEngine(HEADER_BASE, HEADER_SIZE, SAMPLE_BASE, SAMPLE_SIZE, uhc, rail_type)

    def get_force_voltage(self, board, negative, rail_type,load = False):
        if load == False:
            if negative == True:
                force_voltage = board.get_negative_random_voltage(NEGATIVE_VOLTAGE_RANGE[rail_type][0],
                                                                  NEGATIVE_VOLTAGE_RANGE[rail_type][1],
                                                                  NEGATIVE_VOLTAGE_STEPSIZE[rail_type])
            else:
                force_voltage = board.get_positive_random_voltage(POSITIVE_VOLTAGE_RANGE[rail_type][0],
                                                                  POSITIVE_VOLTAGE_RANGE[rail_type][1],
                                                                  POSITIVE_VOLTAGE_STEPSIZE[rail_type])
        else:
            force_voltage = board.get_positive_random_voltage(POSITIVE_VOLTAGE_RANGE_LOAD[rail_type][0],
                                                              POSITIVE_VOLTAGE_RANGE_LOAD[rail_type][1],
                                                              POSITIVE_VOLTAGE_STEPSIZE[rail_type])

        return force_voltage

    def force_voltage_hv_to_lv_scenario_with_sample(self,board,iterations,turbo=False):
        sense_rail_type = 'LVM'
        force_rail_type = 'HV'
        hvRail = random.randint(0,7)
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, sense_rail_type)
        for test_iteration in range(iterations):
            pass_count = 0
            calboard = board.create_cal_board_instance_and_initialize()
            board.cal_load_connect('10Ohm', calboard)
            board.cal_connect_force_sense_lines(hvRail, force_rail_type, calboard)
            cal_card_connect_delay = 0.1
            time.sleep(cal_card_connect_delay)
            for lvrail, uhc in rail_uhc_tuple_list:
                board.SetRailsToSafeState()
                force_voltage = self.get_force_voltage(board, False, sense_rail_type)
                dutid = random.randint(0,63)
                self.initialize_rail(board, lvrail, sense_rail_type, uhc, dutid)
                board.ConfigureUhcRail(uhc, 0x1 << hvRail, force_rail_type)
                self.initialize_sample_engine(board, sense_rail_type, turbo, uhc)
                self.initialize_sample_engine(board, force_rail_type, turbo, uhc)
                trigger_queue_data = self.generate_lvm_trigger_queue(board,hvRail,lvrail,force_voltage, dutid,uhc,turbo=turbo)

                board.ConnectRailForce(hvRail)
                board.ConnectRailSense(hvRail)
                board.ConnectLvRailSense(lvrail)
                cal_card_connect_delay = 0.01
                time.sleep(cal_card_connect_delay)
                set_v_range_data = 0x8001
                board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_V_RANGE, data=set_v_range_data,rail_or_resourceid=hvRail)
                trigger_queue_offset = 0x0
                board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)

                board.ExecuteTriggerQueue(trigger_queue_offset, uhc, force_rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)
                try:
                    self.verify_alarm_status(board, lvrail,sense_rail_type)
                    self.verify_expected_voltage(board, lvrail, uhc, 'LVM', force_voltage)
                    sample_headers = board.GetSampleHeaders(uhc, 1, sense_rail_type, turbo=turbo)
                    self.verify_sample_header(sample_headers, lvrail, sense_rail_type,turbo=turbo)
                    self.verify_sample_data(board, sample_headers, force_voltage, sense_rail_type, turbo = turbo)


                    pass_count += 1

                except self.sample_engine_exception:
                    pass

                board.ConnectLvRailSense(lvrail,0)
            board.cal_disconnect_force_sense_lines(hvRail, force_rail_type, calboard)
            board.ConnectRailForce(hvRail, 0)
            board.ConnectRailSense(hvRail, 0)


            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         'Expected sample engine behavior seen seen on all UHC/Rail combinations: iteration {} of {}'.format(
                             test_iteration+1, iterations ))
            else:
                self.Log('error',
                         'Unexpected sample engine behavior seen on {} of {} UHC/Rail combinations during test iteration {} of {}.'.format(
                             len(rail_uhc_tuple_list)-pass_count, len(rail_uhc_tuple_list), test_iteration+1, iterations))
        board.create_cal_board_instance_and_initialize()
        board.SetRailsToSafeState()

    class sample_engine_exception(Exception):
        pass

    def verify_sample_data(self,board,sample_headers, expected_voltage, rail_type, dump_results = False, turbo = False, test_current = False):
        if rail_type == 'LVM' or turbo == True:
            sample_pointer = sample_headers[0].SamplePointer|0x20000000
        else:
            sample_pointer = sample_headers[0].SamplePointer

        sample_memory = board.DmaRead(sample_pointer, sample_headers[0].SampleCount * 8)
        sample_data_stream = io.BytesIO(sample_memory)
        sample_data_stream.seek(0)

        for sample in range(sample_headers[0].SampleCount):
            voltage = struct.unpack('<f', sample_data_stream.read(4))[0]
            if rail_type != 'LVM':
                current = struct.unpack('<f', sample_data_stream.read(4))[0]

            if dump_results:
                if rail_type == 'LVM':
                    self.Log('info','Sample Voltage {}'.format(voltage))
                else:
                    self.Log('info','Sample Voltage {}, Sample Current {}'.format(voltage, current))

            allowed_percentage_deviation = 20
            zero_buffer = 0.25
            if board.is_close(expected_voltage, voltage, allowed_percentage_deviation, zero_buffer) == False:
                self.Log('error','Sample voltage {} differs from sample expected voltage: {}'.format(voltage, expected_voltage))
                raise self.sample_engine_exception
            if test_current == True:
                load_resistance = int(re.sub('\D', '', CAL_LOAD[rail_type]))
                expected_current = expected_voltage/load_resistance
                if board.is_close(expected_current, current, allowed_percentage_deviation, zero_buffer) == False:
                    self.Log('error','Sample current {} differs from sample expected current: {}'.format(current, expected_current))
                    raise self.sample_engine_exception
        return True

    def verify_sample_header(self,sample_headers, rail, rail_type,turbo = False):
        fail_count = 0
        if sample_headers != []:
            try:
                self.verify_sample_header_sample_pointer(sample_headers)
                self.verify_sample_header_sample_rate(sample_headers)
                self.verify_sample_header_sample_count(sample_headers)
                self.verify_sample_header_rail_select(rail, rail_type, sample_headers, turbo)
                self.verify_sample_header_meta_data(sample_headers)
            except self.header_verify_exception:
                fail_count += 1
        else:
            raise self.sample_engine_exception

        if fail_count > 0:
            raise self.sample_engine_exception

    class header_verify_exception(Exception):
        pass

    def verify_sample_header_sample_count(self, sample_headers):
        if sample_headers[0].SampleCount != SAMPLE_COUNT:
            self.Log('error', 'Sample count returned by fpga {}, doesnt match expected count {}'.format(
                sample_headers[0].SampleCount, SAMPLE_COUNT))
            raise self.header_verify_exception

    def verify_sample_header_sample_rate(self, sample_headers):
        if sample_headers[0].SampleRate != SAMPLE_RATE:
            self.Log('error', 'Sample rate returned by fpga {}, doesnt match expected rate {}'.format(
                sample_headers[0].SampleRate, SAMPLE_RATE))
            raise self.header_verify_exception

    def verify_sample_header_sample_pointer(self, sample_headers):
        if sample_headers[0].SamplePointer != SAMPLE_BASE:
            self.Log('error', 'Sample pointer returned by fpga {}, doesnt match expected pointer {}'.format(
                sample_headers[0].SamplePointer, SAMPLE_BASE))
            raise (self.header_verify_exception)

    def verify_sample_header_meta_data(self, sample_headers):
        if sample_headers[0].MetaLo != 0x3210:
            self.Log('error', 'Meta Data Low returned by fpga 0x{:x}, doesnt match expected value 0x{:x}'.format(
                sample_headers[0].MetaLo, 0x3210))
            raise self.header_verify_exception

        if sample_headers[0].MetaHi != 0x7654:
            self.Log('error', 'Meta Data High returned by fpga 0x{:x}, doesnt match expected value 0x{:x}'.format(
                sample_headers[0].MetaHi, 0x7654))
            raise self.header_verify_exception


    def verify_sample_header_rail_select(self, rail, rail_type, sample_headers, turbo):
        if rail_type == 'HV':
            if turbo == True:
                if sample_headers[0].RailVISelect != 0x3:
                    self.Log('error', 'Rail {} select returned by fpga {}, doesnt match expected rails {}'.format(
                        rail, sample_headers[0].RailVISelect, 0x3))
                    raise (self.header_verify_exception)

            else:
                rail_mask = (0x3 << (rail * 2))
                if sample_headers[0].RailVISelect != rail_mask:
                    self.Log('error', 'Rail {} select returned by fpga {}, doesnt match expected rails {}'.format(
                        rail, sample_headers[0].RailVISelect, rail_mask))
                    raise (self.header_verify_exception)

        elif rail_type == 'LVM':
            if turbo == True:
                if sample_headers[0].RailVISelect != 0x1:
                    self.Log('error', 'Rail {} select returned by fpga {}, doesnt match expected rails {}'.format(
                        rail, sample_headers[0].RailVISelect, 0x1))
                    raise (self.header_verify_exception)

            else:
                rail_mask = (0x1 << (rail))
                if sample_headers[0].RailVISelect != rail_mask:
                    self.Log('error',
                             'Rail {} select returned by fpga {}, doesnt match expected rails {}'.format(
                                 rail, sample_headers[0].RailVISelect, rail_mask))
                    raise (self.header_verify_exception)

        else:
            if sample_headers[0].RailSelect != (0x3 << (rail * 2)):
                self.Log('error', 'Rail {} select returned by fpga {}, doesnt match expected rails {}'.format(
                    rail, sample_headers[0].RailSelect, (0x3 << (rail * 2))))
                raise (self.header_verify_exception)


    def randomize_rail_uhc(self, board, rail_type):
        rail_uhc_tuple_list = []
        for rail in range(board.RAIL_COUNT[rail_type]):
            for uhc in range(board.UHC_COUNT):
                rail_uhc_tuple_list.append((rail, uhc))
        random.shuffle(rail_uhc_tuple_list)
        return rail_uhc_tuple_list

    def generate_lvm_trigger_queue(self, board,force_rail,sense_rail,force_voltage, dutid,uhc,turbo):
        lvUnderVoltLimit = -2
        lvOverVoltLimit =  6

        tq_helper = TriggerQueueString(board,uhc,dutid)
        tq_helper.open_socket = False

        rail_type = 'HV'

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = force_rail
        tq_helper.force_voltage = force_voltage

        # tq_helper.tracking_voltage = force_voltage + tracking_voltage_offset
        tq_helper.tracking_voltage = force_voltage
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]
        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = 0.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 6000

        rail_type = 'LVM'
        tq_helper.sense_rail_type = rail_type
        tq_helper.sense_rail = sense_rail
        tq_helper.sense_high_voltage_limit = lvOverVoltLimit
        tq_helper.sense_low_voltage_limit = lvUnderVoltLimit

        if turbo == True:
            tq_helper.turbo_sample_sense = True
        else:
            tq_helper.sample_sense = True

        tq_helper.sample_delay = SAMPE_DELAY
        tq_helper.sample_count = SAMPLE_COUNT
        tq_helper.sample_rate = SAMPLE_RATE

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_trigger_queue(self, board,rail_type,force_voltage, dutid, rail,uhc,turbo):

        tq_helper = TriggerQueueString(board,uhc,dutid)
        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]
        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = 0.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 6000

        if turbo == True:
            tq_helper.turbo_sample_force = True
        else:
            tq_helper.sample_force = True

        tq_helper.sample_delay = SAMPE_DELAY
        tq_helper.sample_count = SAMPLE_COUNT
        tq_helper.sample_rate = SAMPLE_RATE

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_current_trigger_queue(self, board,rail_type,force_voltage, dutid, rail,uhc,turbo):
        tq_helper = TriggerQueueString(board,uhc,dutid)
        tq_helper.open_socket= False
        tq_helper.tracking_voltage = force_voltage
        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.force_voltage = force_voltage
        tq_helper.force_clamp_high = CLAMP_LIMITS_LOAD[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS_LOAD[rail_type][0]
        tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS_LOAD[rail_type][0]
        tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS_LOAD[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE_LOAD[rail_type]

        if turbo == True:
            tq_helper.turbo_sample_force = True
        else:
            tq_helper.sample_force = True
        tq_helper.sample_delay = SAMPE_DELAY
        tq_helper.sample_count = SAMPLE_COUNT
        tq_helper.sample_rate = SAMPLE_RATE

        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = FREE_DRIVE_LIMITS[rail_type][1]
        tq_helper.force_free_drive_low = FREE_DRIVE_LIMITS[rail_type][0]
        tq_helper.force_free_drive_delay = FREE_DRIVE_DELAY[rail_type]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data


    def verify_alarm_status(self, board, rail, rail_type):
        global_alarms = board.get_global_alarms()
        if global_alarms != []:
            self.Log('error', 'Received global alarm(s): {}'.format(global_alarms))
            if rail_type != 'LVM':
                hclc_rail_alarm = board.ReadRegister(board.registers.HCLC_PER_RAIL_ALARMS, index=rail)
                rail_alarms = []
                for field in hclc_rail_alarm.fields():
                    if getattr(hclc_rail_alarm, field) == 1:
                        rail_alarms.append(field)
                self.Log('error', 'Hclc Rails Alarm(s) set were {}'.format(rail_alarms))
            else:
                dut_rail_alarm = board.ReadRegister(board.registers.DUT_RAIL_ALARM, index=rail)
                rail_alarms = []
                for field in dut_rail_alarm.fields():
                    if getattr(dut_rail_alarm, field) == 1:
                        rail_alarms.append(field)
                self.Log('error', 'Dut Rails Alarm(s) set were {}'.format(rail_alarms))
            raise self.sample_engine_exception

    def verify_expected_voltage(self, board, rail, uhc, rail_type, expected_voltage):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        allowed_percentage_deviation = 25
        zero_buffer = 0.25
        if board.is_close(expected_voltage, rail_voltage, allowed_percentage_deviation,zero_buffer) == False:
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous voltage is {:.4f}V is not within the allowable deviation of {}% of {:.4f}V'.format(
                         rail_type, rail, uhc, rail_voltage, allowed_percentage_deviation, expected_voltage))
            raise self.sample_engine_exception

    def verify_expected_current(self, board, rail, uhc, rail_type, expected_current):
        rail_current = board.get_rail_current(rail,rail_type)
        allowed_percentage_deviation = 25
        zero_buffer = 0.25
        if board.is_close(expected_current, rail_current, allowed_percentage_deviation,zero_buffer) == False:
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous current is {:.4f}I is not within the allowable deviation of {}% of {:.4f}I'.format(
                         rail_type, rail, uhc, rail_current, allowed_percentage_deviation, expected_current))
            raise self.sample_engine_exception


class SampleEngineHv(BaseTest,SampleEngineHelper):

    def RandomHvVerifyHeaderVoltageCurrentTest(self):
        rail_type = 'HV'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_current_sample(board, rail_type, iterations)

    def RandomHvVerifyHeaderVoltageCurrentTurboTest(self):
        rail_type = 'HV'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_current_sample(board, rail_type, iterations,turbo=True)

    def RandomHvVerifyHeaderAndVoltageTest(self):
        rail_type = 'HV'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_sample(board, rail_type, iterations)

    def RandomHvTurboVerifyHeaderAndVoltageTest(self):
        rail_type = 'HV'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_sample(board, rail_type, iterations,turbo=True)

class SampleEngineLvm(BaseTest,SampleEngineHelper):

    def RandomLvmVerifyHeaderAndVoltageTest(self):
        rail_type = 'LVM'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_hv_to_lv_scenario_with_sample(board, iterations)

    def RandomLvmTurboVerifyHeaderAndVoltageTest(self):
        rail_type = 'LVM'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_hv_to_lv_scenario_with_sample(board, iterations,turbo = True)

class SampleEngineHc(BaseTest,SampleEngineHelper):

    def RandomHcVerifyHeaderAndVoltageTest(self):
        rail_type = 'HC'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_sample(board, rail_type, iterations)

    def RandomHcVerifyHeaderVoltageCurrentTest(self):
        rail_type = 'HC'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_current_sample(board, rail_type, iterations)

class SampleEngineLc(BaseTest,SampleEngineHelper):

    def RandomLcVerifyHeaderVoltageCurrentTest(self):
        rail_type = 'LC'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_current_sample(board, rail_type, iterations)

    def RandomLcVerifyHeaderAndVoltageTest(self):
        rail_type = 'LC'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_sample(board, rail_type, iterations)

    def RandomLcVerifyHeaderAndNegativeVoltageTest(self):
        rail_type = 'LC'
        iterations = 5
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario_with_sample(board, rail_type, iterations, negative=True)
