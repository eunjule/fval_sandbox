###################################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# --------------------------------------------------------------------------------------------------
#    Filename: RandomTQforVlcRails
# --------------------------------------------------------------------------------------------------
#     Purpose: Randomizing various attributes of Trigger Queue for more comprehensive test coverage
# --------------------------------------------------------------------------------------------------
#  Created by: Shaariq Shaikh
#        Date: 
#       Group: HDMT FPGA DEVELOPMENT
###################################################################################################

from collections import defaultdict
import random
from string import Template
import unittest

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    def randomizevlcsampleengine(self, dutid, rail):
        result = ''
        # Fixed command for Reset
        result = result + 'Q cmd=0x0, arg=VlcSampleEngineReset, data=0x{:x}\n'.format(0x1 << dutid)
        available_sample_engine_commands_per_rail = defaultdict(list)

        metadata_high = 0
        specific_parameters = ['SampleDelay', 'SampleCount', 'SampleRate', 'SampleMetadataHi', 'SampleMetadataLo',
                               'SampleRailSelect']
        sample_commands_specific_to_a_rail = []
        for parameter in specific_parameters:
            if parameter == 'SampleDelay':
                delay_range = random.randint(5000, 10000)
                dword = 'Q cmd=0x0, arg=Vlc{}SampleDelay, data=0x{:x}\n'.format(dutid, delay_range)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleCount':
                count_value = random.randint(4000, 5000)
                dword = 'Q cmd=0x0, arg=Vlc{}SampleCount, data=0x{:x}\n'.format(dutid, count_value)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleRate':
                rate = random.randint(0, 5)
                dword = 'Q cmd=0x0, arg=Vlc{}SampleRate, data=0x{:x}\n'.format(dutid, rate)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleMetadataHi':
                metadata_high = random.randint(500, 65535)
                dword = 'Q cmd=0x0, arg=Vlc{}SampleMetadataHi, data=0x{:x}\n'.format(dutid, metadata_high)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleMetadataLo':
                metadata_low = random.randint(0, metadata_high)
                dword = 'Q cmd=0x0, arg=Vlc{}SampleMetadataLo, data=0x{:x}\n'.format(dutid, metadata_low)
                sample_commands_specific_to_a_rail.append(dword)
            elif parameter == 'SampleRailSelect':
                rail_select = 0x3ff
                dword = 'Q cmd=0x0, arg=Vlc{}SampleRailSelect, data=0x{:x}\n'.format(dutid, rail_select)
                sample_commands_specific_to_a_rail.append(dword)

        for individual_command in sample_commands_specific_to_a_rail:
            available_sample_engine_commands_per_rail[rail].append(individual_command)

        # Start randomly picking a rail as well as a command associated with that rail
        while len(available_sample_engine_commands_per_rail[rail]) != 0:
            instruction = random.choice(available_sample_engine_commands_per_rail[rail])
            result = result + instruction
            available_sample_engine_commands_per_rail[rail].remove(instruction)

        # Fixed command for sample start
        result = result + 'Q cmd=0x0, arg=Vlc{}SampleStart, data=0x0001\n'.format(dutid)
        return result

    @unittest.skip('This test currently skipped while TQ tests are being audited')
    def RandomModeSelectforVlcRailsTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            self.SetsubModeforVlcTestScenario(board)

    def SetsubModeforVlcTestScenario(self, board):
        # self.Log('info', '\nTesting HC rail {} for dutid {}...'.format(rail, dutid))

        slot = board.slot
        subslot = board.subslot
        board.SetRailsToSafeState()

        # randomly pick a value of load from the of load values
        num_of_possible_values_for_load = ['OHM_0_025', 'OHM_0_05', 'OHM_0_25', 'OHM_0_8', 'OHM_4', 'OHM_10', 'OHM_100',
                                           'OHM_1K',
                                           'OHM_100K', 'OHM_500K', 'OHM_1M', 'NONE']
        selected_value_of_load = 'OHM_10'
        num_of_vlc_rails = list(range(0, 16))
        rails_mapped_to_each_uhc = []
        """
        for i in range (0,8):
            rails_mapped_to_each_uhc.append ([])
        """
        # Assign multiple rails to individual MDUTs
        for i in range(0, 8):
            num_rails = 1
            for no_rail in range(0, num_rails):
                rail_number = random.choice(num_of_vlc_rails)
                self.Log('info', '\nTesting VLC rails {}...'.format(rail_number))
                rails_mapped_to_each_uhc.append(rail_number)
                num_of_vlc_rails.remove(rail_number)

                # Randomly pick a uhc number
        uhc = [0, 1, 2, 3, 4, 5, 6, 7]
        dutid = random.choice(uhc)
        self.Log('info', '\nTesting dut id randomly selected is {}'.format(dutid))

        # Pick the rails corresponding to that UHC number
        # rail = []
        rail = rails_mapped_to_each_uhc[dutid]

        self.Log('info', '\nTesting VLC rails {} for dutid {}...'.format(rail, dutid))
        if rail < 10:
            board.ConnectCalBoard('VLC0{}'.format(rail), selected_value_of_load)
        else:
            board.ConnectCalBoard('VLC{}'.format(rail), selected_value_of_load)

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearVlcRailAlarm()
        board.ClearVlcSampleAlarms()
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        # Configuring rails to a one hot value to have 1's at the location of selected rails
        """
        rail_configuration = 0b0000000000
        for hclc_rail_number in rail:
            rail_configuration = rail_configuration + (0b0000000001 << hclc_rail_number)
        """

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

        board.InitializeSampleEngine(0x500 * dutid, 0x500 * dutid, 0x1000 * dutid, 0x1000 * dutid, dutid, 'VLC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'VLC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'VLC')

        asm = TriggerQueueAssembler()
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid

        # Fixed Header for trigger Queue
        src = trigger_queue_header + "\n"

        available_modes = ['VFORCE', 'IFORCE']
        mode_select = random.choice(available_modes)

        # Start randomizing the trigger queue instructions
        current_ranges = ['I_256_UA', 'I_2_56_MA', 'I_25_6_MA', 'I_256_MA']

        # Declaring a dictionary in order to map the rail commands to the appropriate rails
        available_commands_per_rail = defaultdict(list)

        # Taking care of the dependencies between the instructions here before randomizing
        # for rails in rails_mapped_to_each_uhc[dutid]:
        if mode_select == 'VFORCE':
            commands = ['SetIClampHi', 'SetIClampLo', 'SetVoltage', 'SetMode']
        else:
            commands = ['SetIClampHi', 'SetIClampLo', 'SetCurrent', 'SetMode']

        commands_specific_to_a_rail = []
        clamp_high_upper_limit = 0
        under_voltage = 0
        over_voltage = 0
        dword = 0

        # specific_value_of_current_range = random.choice (current_ranges)
        specific_value_of_current_range = 'I_256_MA'
        rail_command = 'SetCurrentRange'
        if specific_value_of_current_range == 'I_2_56_UA':
            clamp_high_upper_limit = 0.00000256 + (0.25 * 0.00000256)
        elif specific_value_of_current_range == 'I_25_6_UA':
            clamp_high_upper_limit = 0.0000256 + (0.25 * 0.0000256)
        elif specific_value_of_current_range == 'I_256_UA':
            clamp_high_upper_limit = 0.000256 + (25.0 * 0.000256)
        elif specific_value_of_current_range == 'I_2_56_MA':
            clamp_high_upper_limit = 0.00256 + (0.25 * 0.00256)
        elif specific_value_of_current_range == 'I_25_6_MA':
            clamp_high_upper_limit = 0.0256 + (0.25 * 0.0256)
        elif specific_value_of_current_range == 'I_256_MA':
            clamp_high_upper_limit = 0.256 + (25.0 * 0.256)
        src = src + '{} rail={}, value={}\n'.format(rail_command, rail, specific_value_of_current_range)

        rail_command = 'EnableDisableRail'
        data = 0
        src = src + '{} rail={}, value={}\n'.format(rail_command, rail, data)

        for rail_command in commands:
            if rail_command == 'SetIClampHi':
                clamp_low_lower_limit = (clamp_high_upper_limit * -1)
                data = random.uniform(0, clamp_high_upper_limit)
                dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetIClampLo':
                data = random.uniform(clamp_low_lower_limit, 0)
                dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetVoltage':
                over_voltage = random.uniform(-4, 8)
                under_voltage = random.uniform(-4.44, over_voltage)
                data = 0.75 * over_voltage
                dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetCurrent':
                if specific_value_of_current_range == 'I_2_56_UA':
                    data = 0.00000256
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                elif specific_value_of_current_range == 'I_25_6_UA':
                    data = 0.0000256
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                elif specific_value_of_current_range == 'I_256_UA':
                    data = 0.000256
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                if specific_value_of_current_range == 'I_2_56_MA':
                    data = 0.00256
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                elif specific_value_of_current_range == 'I_25_6_MA':
                    data = 0.0256
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                elif specific_value_of_current_range == 'I_256_MA':
                    data = 0.256
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                commands_specific_to_a_rail.append(dword)
            elif rail_command == 'SetMode':
                if mode_select == 'IFORCE':
                    data = mode_select
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                else:
                    data = mode_select
                    dword = '{} rail={}, value={}\n'.format(rail_command, rail, data)
                commands_specific_to_a_rail.append(dword)

                # available_commands_per_rail [rails].append(commands_specific_to_a_rail)
        for individual_command in commands_specific_to_a_rail:
            available_commands_per_rail[rail].append(individual_command)

        while len(available_commands_per_rail[rail]) != 0:
            instruction = random.choice(available_commands_per_rail[rail])
            src = src + instruction
            available_commands_per_rail[rail].remove(instruction)

        if mode_select == 'IFORCE':
            rail_command = 'FORCE_CLAMP_FLUSH'
            data = 1
            src = src + 'Q cmd={}, arg={}, data={}\n'.format(rail_command, rail, data)

        # Fixed commands here
        rail_command = 'EnableDisableRail'
        value = 1
        src = src + '{} rail={}, value={}\n'.format(rail_command, rail, value)

        rail_command = '$sampleengine'
        src = src + '{}\n'.format(rail_command)

        # Fixed Footer for trigger queue
        src = src + trigger_queue_footer + "\nTqComplete rail=0, value=0"

        self.Log('info', src)

        # Start loading the trigger queue assembler
        asm.LoadString(Template(src).substitute(sampleengine=self.randomizevlcsampleengine(dutid, rail)))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, dutid, 'VLC')
        railvoltage = 'VlcRail' + str(rail) + 'V'
        railcurrent = 'VlcRail' + str(rail) + 'I'
        self.Log('info', 'The VLC rail {} current reg is 0x{:x}'.format(rail, board.Read(railcurrent).Pack()))
        self.Log('info', 'The VLC rail {} voltage reg is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))

        # self.env.CheckRailBusy(board)
        board.CheckVlcSamplingActive(dutid)
        board.CheckVlcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        board.ReadHeaderSampleRegion(HeaderBase + 0x500 * dutid, 0x20, SampleBase + 0x1000 * dutid, 0x0100)
        board.RunCheckers( dutid, 'ResistiveLoopback', 10, "VLC")

        board.ClearVlcRailAlarm()
