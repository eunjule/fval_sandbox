################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegisters
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################

import random
from string import Template
# import time

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    # Program Over voltage Voltage calibration registers on FPGA with random gain and offset values , set the over voltage and compare expected over voltage with value programmed to the AD5560
    def OverVoltageCalibrationLCTest(self):
        railtype = "LC"
        myresult = ['OverVoltageCalibrationLCTest,']
        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000

            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.001

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            voltageCalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            voltageCalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            # self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )


            # adding this for loop to try each gain and offset in the list for debug
            # for selectedGain in gainOffsetList:
            #    for selectedOffset in gainOffsetList:
            #        time.sleep(1)
            #        voltageCalibrationGainReal = (selectedGain/2**16) + 0.5
            #        voltageCalibrationOffsetReal = (selectedOffset/2**17) - 0.25
            # end of added debug code
            #        self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            #        self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )

            # calculate expected cal over voltage based on 2.5V over voltage setting
            expectedCalibratedOverVoltage = voltageCalibrationGainReal * 2.5 + voltageCalibrationOffsetReal
            # self.Log('info', 'Expected overvoltage {}'.format(expectedCalibratedOverVoltage ) )


            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetOverVoltageCalRegister(rail, refGain, refOffset)

                    # get a reference voltage by programming the fpga with the default calibration values
                    refOverVoltage = self.AD5560OverVoltageCalibrationScenario(slot, subslot, dutid, rail, refGain,
                                                                               refOffset, railtype)

                    # get the voltage by programming the fpga with the selected gain and offsets from above
                    calibratedOverVoltageFromAD5560 = self.AD5560OverVoltageCalibrationScenario(slot, subslot, dutid,
                                                                                                rail, selectedGain,
                                                                                                selectedOffset,
                                                                                                railtype)

                    # based on the selected gain and offset calculate the voltage we expect to see
                    expectedCalibratedOverVoltage = voltageCalibrationGainReal * 2.5 + voltageCalibrationOffsetReal

                    # get the difference between the expected and actual voltages from the fpgas
                    delta = abs(expectedCalibratedOverVoltage - calibratedOverVoltageFromAD5560)

                    # reset calibration register with default values
                    board.SetOverVoltageCalRegister(rail, refGain, refOffset)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual over Voltage value from AD5560 is more than the error limit of {}, the acutal value is {}, the expected value is {}".format(
                                     errorMargin, calibratedOverVoltageFromAD5560, expectedCalibratedOverVoltage))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual Over Voltage value from AD5560 is within the error limit of {}, the actual value is {}. the expected value is {}".format(
                                     errorMargin, calibratedOverVoltageFromAD5560, expectedCalibratedOverVoltage))
                    self.Log('info',
                             'The reference overvoltage when default settings are used is {}'.format(refOverVoltage))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append([str(rail), '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset),
                                     calibratedOverVoltageFromAD5560, expectedCalibratedOverVoltage])
            s_myresult = str(myresult)
            self.Log('debug',
                     'The result summary is: Rail, Gain, Offset, Calibrated Voltage, Expected Voltage: {}'.format(
                         s_myresult))

    # Program Over voltage Voltage calibration registers on FPGA with random gain and offset values , set the over voltage and compare expected over voltage with value programmed to the AD5560
    def OverVoltageCalibrationHCTest(self):
        railtype = "HC"
        myresult = ['OverVoltageCalibrationHCTest,']
        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000

            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.001

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            voltageCalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            voltageCalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            # self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )


            # adding this for loop to try each gain and offset in the list for debug
            # for selectedGain in gainOffsetList:
            #    for selectedOffset in gainOffsetList:
            #        time.sleep(1)
            #        voltageCalibrationGainReal = (selectedGain/2**16) + 0.5
            #        voltageCalibrationOffsetReal = (selectedOffset/2**17) - 0.25
            # end of added debug code
            #        self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            #        self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )

            # calculate expected cal over voltage based on 2.5V over voltage setting
            expectedCalibratedOverVoltage = voltageCalibrationGainReal * 2.5 + voltageCalibrationOffsetReal
            # self.Log('info', 'Expected overvoltage {}'.format(expectedCalibratedOverVoltage ) )


            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetOverVoltageCalRegister(rail, refGain, refOffset)

                    # get a reference voltage by programming the fpga with the default calibration values
                    refOverVoltage = self.AD5560OverVoltageCalibrationScenario(slot, subslot, dutid, rail, refGain,
                                                                               refOffset, railtype)

                    # get the voltage by programming the fpga with the selected gain and offsets from above
                    calibratedOverVoltageFromAD5560 = self.AD5560OverVoltageCalibrationScenario(slot, subslot, dutid,
                                                                                                rail, selectedGain,
                                                                                                selectedOffset,
                                                                                                railtype)

                    # based on the selected gain and offset calculate the voltage we expect to see
                    expectedCalibratedOverVoltage = voltageCalibrationGainReal * 2.5 + voltageCalibrationOffsetReal

                    # get the difference between the expected and actual voltages from the fpgas
                    delta = abs(expectedCalibratedOverVoltage - calibratedOverVoltageFromAD5560)

                    # reset calibration register with default values
                    board.SetOverVoltageCalRegister(rail, refGain, refOffset)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual over Voltage value from AD5560 is more than the error limit of {}, the acutal value is {}, the expected value is {}".format(
                                     errorMargin, calibratedOverVoltageFromAD5560, expectedCalibratedOverVoltage))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual Over Voltage value from AD5560 is within the error limit of {}, the actual value is {}. the expected value is {}".format(
                                     errorMargin, calibratedOverVoltageFromAD5560, expectedCalibratedOverVoltage))
                    self.Log('info',
                             'The reference overvoltage when default settings are used is {}'.format(refOverVoltage))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append([str(rail), '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset),
                                     calibratedOverVoltageFromAD5560, expectedCalibratedOverVoltage])
            s_myresult = str(myresult)
            self.Log('debug',
                     'The result summary is: Rail, Gain, Offset, Calibrated Voltage, Expected Voltage: {}'.format(
                         s_myresult))

    # Program under voltage Voltage calibration registers on FPGA with random gain and offset values , set the under voltage and compare expected under voltage with value programmed to the AD5560
    def UnderVoltageCalibrationLCTest(self):
        railtype = "LC"
        myresult = ['UnderVoltageCalibrationLCTest,']
        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000

            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.001

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            voltageCalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            voltageCalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            # self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )


            # adding this for loop to try each gain and offset in the list for debug
            # for selectedGain in gainOffsetList:
            #    for selectedOffset in gainOffsetList:
            #        time.sleep(1)
            #        voltageCalibrationGainReal = (selectedGain/2**16) + 0.5
            #        voltageCalibrationOffsetReal = (selectedOffset/2**17) - 0.25
            # end of added debug code
            #        self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            #        self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )

            # calculate expected cal under voltage based on -1.5V under voltage setting
            expectedCalibratedUnderVoltage = voltageCalibrationGainReal * (-1.5) + voltageCalibrationOffsetReal
            self.Log('info', 'Expected undervoltage {}'.format(expectedCalibratedUnderVoltage))

            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetUnderVoltageCalRegister(rail, refGain, refOffset)

                    # get a reference voltage by programming the fpga with the default calibration values
                    refUnderVoltage = self.AD5560UnderVoltageCalibrationScenario(slot, subslot, dutid, rail, refGain,
                                                                                 refOffset, railtype)

                    # get the voltage by programming the fpga with the selected gain and offsets from above
                    calibratedUnderVoltageFromAD5560 = self.AD5560UnderVoltageCalibrationScenario(slot, subslot, dutid,
                                                                                                  rail, selectedGain,
                                                                                                  selectedOffset,
                                                                                                  railtype)

                    # based on the selected gain and offset calculate the voltage we expect to see
                    expectedCalibratedUnderVoltage = voltageCalibrationGainReal * (-1.5) + voltageCalibrationOffsetReal

                    # get the difference between the expected and actual voltages from the fpgas
                    delta = abs(expectedCalibratedUnderVoltage - calibratedUnderVoltageFromAD5560)

                    # reset calibration register with default values
                    board.SetUnderVoltageCalRegister(rail, refGain, refOffset)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual Under Voltage value from AD5560 is more than the error limit of {}, the actual value is {}, the expected value is {}".format(
                                     errorMargin, calibratedUnderVoltageFromAD5560, expectedCalibratedUnderVoltage))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual under Voltage value from AD5560 is within the error limit of {}, the actual value is {}, the expected value is {}".format(
                                     errorMargin, calibratedUnderVoltageFromAD5560, expectedCalibratedUnderVoltage))
                    self.Log('info',
                             'The reference Undervoltage when default settings are used is {}'.format(refUnderVoltage))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append([str(rail), '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset),
                                     calibratedUnderVoltageFromAD5560, expectedCalibratedUnderVoltage])
            s_myresult = str(myresult)
            self.Log('debug',
                     'The result summary is: Rail, Gain, Offset, Calibrated Voltage, Expected Voltage: {}'.format(
                         s_myresult))

    # Program Under voltage Voltage calibration registers on FPGA with random gain and offset values , set the Under voltage and compare expected Under voltage with value programmed to the AD5560
    def UnderVoltageCalibrationHCTest(self):
        railtype = "HC"
        myresult = ['UnderVoltageCalibrationHCTest,']
        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000

            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.001

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            voltageCalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            voltageCalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            # self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )


            # adding this for loop to try each gain and offset in the list for debug
            # for selectedGain in gainOffsetList:
            #    for selectedOffset in gainOffsetList:
            #        time.sleep(1)
            #        voltageCalibrationGainReal = (selectedGain/2**16) + 0.5
            #        voltageCalibrationOffsetReal = (selectedOffset/2**17) - 0.25
            # end of added debug code
            #        self.Log('info', 'volt_gain_real is {}'.format(voltageCalibrationGainReal ) )
            #        self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )

            # calculate expected cal Under voltage based on -1.5V Under voltage setting
            expectedCalibratedUnderVoltage = voltageCalibrationGainReal * (-1.5) + voltageCalibrationOffsetReal
            self.Log('info', 'Expected Undervoltage {}'.format(expectedCalibratedUnderVoltage))

            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetUnderVoltageCalRegister(rail, refGain, refOffset)

                    # get a reference voltage by programming the fpga with the default calibration values
                    refUnderVoltage = self.AD5560UnderVoltageCalibrationScenario(slot, subslot, dutid, rail, refGain,
                                                                                 refOffset, railtype)

                    # get the voltage by programming the fpga with the selected gain and offsets from above
                    calibratedUnderVoltageFromAD5560 = self.AD5560UnderVoltageCalibrationScenario(slot, subslot, dutid,
                                                                                                  rail, selectedGain,
                                                                                                  selectedOffset,
                                                                                                  railtype)

                    # based on the selected gain and offset calculate the voltage we expect to see
                    expectedCalibratedUnderVoltage = voltageCalibrationGainReal * (-1.5) + voltageCalibrationOffsetReal

                    # get the difference between the expected and actual voltages from the fpgas
                    delta = abs(expectedCalibratedUnderVoltage - calibratedUnderVoltageFromAD5560)

                    # reset calibration register with default values
                    board.SetUnderVoltageCalRegister(rail, refGain, refOffset)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual Under Voltage value from AD5560 is more than the error limit of {}, the actual value is {}, the expected value is {}".format(
                                     errorMargin, calibratedUnderVoltageFromAD5560, expectedCalibratedUnderVoltage))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual under Voltage value from AD5560 is within the error limit of {}, the actual value is {}, the expected value is {}".format(
                                     errorMargin, calibratedUnderVoltageFromAD5560, expectedCalibratedUnderVoltage))
                    self.Log('info',
                             'The reference Undervoltage when default settings are used is {}'.format(refUnderVoltage))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append([str(rail), '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset),
                                     calibratedUnderVoltageFromAD5560, expectedCalibratedUnderVoltage])
            s_myresult = str(myresult)
            self.Log('debug',
                     'The result summary is: Rail, Gain, Offset, Calibrated Voltage, Expected Voltage: {}'.format(
                         s_myresult))

    # write offset and gain calibration values for over voltage to the FPGA, apply and measure a voltage and return the voltage to the calling function (setovervoltage function must be executed to be able to see overvoltage value on the ad5560)
    def AD5560OverVoltageCalibrationScenario(self, slot, subslot, dutid, rail, gain, offset, railtype):

        iRangeQueue = 'I_500_MA'
        iClampLowQueue = '-0.5'
        iClampHighQueue = '0.5'
        vForceQueue = '1.0'
        calBoardRes = 'OHM_1K'

        self.Log('info', '\nTesting HCLC rail {} for dutid {}...'.format(rail, dutid))
        board = self.env.instruments[slot].subslots[subslot]
        board.SetRailsToSafeState()

        # Setup the cal cage depending on the rail type
        if (railtype == "LC"):
            self.Log('info', 'Supply Type is: {}'.format(railtype))
            board.ConnectCalBoard('LC{}'.format(rail), calBoardRes)
        else:
            board.ConnectCalBoard('HC{}'.format(rail), calBoardRes)
            self.Log('info', 'Supply Type is: {}'.format(railtype))

        # clear and enable alarms
        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        # check the calibration registers for default values and then program them with the passed to values
        board.CheckOverVoltageCalRegister()
        board.SetOverVoltageCalRegister(rail, gain, offset)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,railtype)
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'LC')

        # build trigger queue to force 1V and measure voltage
        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             TqNotify rail=0, value=BEGIN
             TqNotify rail=16, value=BEGIN
             SetMode rail=RAIL, value=VFORCE             
             #Q cmd=TIME_DELAY, arg=RAIL, data=0x1388
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=1.0
             SetILoFreeDrive rail=RAIL, value=-1.0
             SetIClampHi rail=RAIL, value=$clamphigh
             SetIClampLo rail=RAIL, value=$clamplow
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             EnableDisableRail rail=RAIL, value=1
             SetVoltage rail=RAIL, value=$forcevoltage 
             TimeDelay rail=RAIL, value=10000
             SetVCompAlarm rail=RAIL, value=0
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=1000
             TqNotify rail=0, value=END
             TqNotify rail=16, value=END
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'LC'),
                         forcevoltage=vForceQueue, clamphigh=iClampHighQueue, clamplow=iClampLowQueue))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)

        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        # execute trigger queue
        board.ExecuteTriggerQueue(offset, dutid)


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        # **print out global alarm register for debug purposes
        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        # read the over voltage directly from the AD5560
        ad5560_OverVolt_reg = board.ad5560regs.CPH_DAC_X1.ADDR
        ad5560_OverVolt_data = board.ReadAd5560Register(ad5560_OverVolt_reg, rail)

        # OFFSET_DAC_CODE is 0x58c0(22720)
        # VREF=2.5
        # Vout = 5.125 * vref * (dac_code/2^16) - 5.125*vref*(offset_dac_code/2^16)
        OverVoltageActual = 5.125 * 2.5 * (ad5560_OverVolt_data / 2 ** 16) - 5.125 * 2.5 * (22720 / 2 ** 16)

        # check if there were alarms and clear them
        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

        # return voltage back to calling function
        return OverVoltageActual

        # write offset and gain calibration values for Under voltage to the FPGA, apply and measure a voltage and return the voltage to the calling function (setUndervoltage function must be executed to be able to see Undervoltage value on the ad5560)

    def AD5560UnderVoltageCalibrationScenario(self, slot, subslot, dutid, rail, gain, offset, railtype):

        iRangeQueue = 'I_500_MA'
        iClampLowQueue = '-0.5'
        iClampHighQueue = '0.5'
        vForceQueue = '1.0'
        calBoardRes = 'OHM_1K'

        self.Log('info', '\nTesting HCLC rail {} for dutid {}...'.format(rail, dutid))
        board = self.env.instruments[slot].subslots[subslot]
        board.SetRailsToSafeState()

        # Setup the cal cage depending on the rail type
        if (railtype == "LC"):
            self.Log('info', 'Supply Type is: {}'.format(railtype))
            board.ConnectCalBoard('LC{}'.format(rail), calBoardRes)
        else:
            board.ConnectCalBoard('HC{}'.format(rail), calBoardRes)
            self.Log('info', 'Supply Type is: {}'.format(railtype))

        # clear and enable alarms
        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        # check the calibration registers for default values and then program them with the passed to values
        board.CheckUnderVoltageCalRegister()
        board.SetUnderVoltageCalRegister(rail, gain, offset)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,railtype)
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'LC')

        # build trigger queue to force 1V and measure voltage
        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             TqNotify rail=0, value=BEGIN
             TqNotify rail=16, value=BEGIN
             SetMode rail=RAIL, value=VFORCE
             #Q cmd=TIME_DELAY, arg=RAIL, data=0x1388
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=1.0
             SetILoFreeDrive rail=RAIL, value=-1.0
             SetIClampHi rail=RAIL, value=$clamphigh
             SetIClampLo rail=RAIL, value=$clamplow
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             EnableDisableRail rail=RAIL, value=1
             SetVoltage rail=RAIL, value=$forcevoltage 
             TimeDelay rail=RAIL, value=10000
             SetVCompAlarm rail=RAIL, value=0
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=1000
             TqNotify rail=0, value=END
             TqNotify rail=16, value=END
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'LC'),
                         forcevoltage=vForceQueue, clamphigh=iClampHighQueue, clamplow=iClampLowQueue))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)

        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        # execute trigger queue
        board.ExecuteTriggerQueue(offset, dutid)


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        # **print out global alarm register for debug purposes
        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        # read the Under voltage directly from the AD5560
        ad5560_UnderVolt_reg = board.ad5560regs.CPL_DAC_X1.ADDR
        ad5560_UnderVolt_data = board.ReadAd5560Register(ad5560_UnderVolt_reg, rail)

        # OFFSET_DAC_CODE is 0x58c0(22720)
        # VREF=2.5
        # Vout = 5.125 * vref * (dac_code/2^16) - 5.125*vref*(offset_dac_code/2^16)
        UnderVoltageActual = 5.125 * 2.5 * (ad5560_UnderVolt_data / 2 ** 16) - 5.125 * 2.5 * (22720 / 2 ** 16)

        # check if there were alarms and clear them
        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

        # return voltage back to calling function
        return UnderVoltageActual
