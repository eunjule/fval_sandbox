################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegistersVoltage
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Voltage Calibration Registers
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################

import random
from string import Template
# import time

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    # Program LC Voltage calibration register(AD5560) with random gain and offset values and compare actual against expected voltage measure readings.
    def LcVoltageForceCalibrationTest(self):

        railtype = "LC"
        myresult = ["LcVoltageForceCalibrationTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000

            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.1

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            voltageCalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            voltageCalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'volt_cal_real is {}'.format(voltageCalibrationGainReal ) )
            # self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )


            # adding this for loop to try each gain and offset in the list for debug
            #            for selectedGain in gainOffsetList:
            #                for selectedOffset in gainOffsetList:
            #                    time.sleep(1)
            #                    voltageCalibrationGainReal = (selectedGain/2**16) + 0.5
            #                    voltageCalibrationOffsetReal = (selectedOffset/2**17) - 0.25
            # end of added debug code

            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetVoltageForceCalRegister(rail, refGain, refOffset)

                    # get a reference voltage by programing the fpga with the default calibration values
                    refVoltage = self.VoltageForceCalibrationScenario(slot, subslot, dutid, rail, refGain, refOffset,
                                                                      railtype)

                    # get the voltage by programing the fpga with the selected gain and offsets from above
                    calibratedVoltageFromFpga = self.VoltageForceCalibrationScenario(slot, subslot, dutid, rail,
                                                                                     selectedGain, selectedOffset,
                                                                                     railtype)
                    # based on the seelected gain and offset calculate the voltage we expect to see
                    expectedCalibratedVoltage = voltageCalibrationGainReal * refVoltage + voltageCalibrationOffsetReal
                    # get the difference between the exepectd and actual voltages from the fpgas
                    delta = abs(expectedCalibratedVoltage - calibratedVoltageFromFpga)

                    # Reset calibration register with default values
                    board.SetVoltageForceCalRegister(rail, refGain, refOffset)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs Actual value from FPGA is more than error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedVoltageFromFpga, expectedCalibratedVoltage))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs Actual value from FPGA is within the error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedVoltageFromFpga, expectedCalibratedVoltage))
                    self.Log('info', 'The reference is {}'.format(refVoltage))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append(
                        [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), calibratedVoltageFromFpga,
                         expectedCalibratedVoltage])
        s_myresult = str(myresult)
        self.Log('debug', 'The result summary is: Rail, Gain, Offset, Calibrated Voltage, Expected Voltage: {}'.format(
            s_myresult))

    # Program HC Voltage calibration register(AD5560) with random gain and offset values and compare actual against expected voltage measure readings.
    def HcVoltageForceCalibrationTest(self):
        railtype = "HC"
        myresult = ["HcVoltageForceCalibrationTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000

            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.1

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            voltageCalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            voltageCalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'volt_cal_real is {}'.format(voltageCalibrationGainReal ) )
            # self.Log('info', 'volr_offset_real is {}'.format(voltageCalibrationOffsetReal ) )


            # adding this for loop to try each gain and offset in the list for debug
            #            for selectedGain in gainOffsetList:
            #                for selectedOffset in gainOffsetList:
            #                    time.sleep(1)
            #                    voltageCalibrationGainReal = (selectedGain/2**16) + 0.5
            #                    voltageCalibrationOffsetReal = (selectedOffset/2**17) - 0.25
            # end of added debug code

            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetVoltageForceCalRegister(rail, refGain, refOffset)

                    # get a reference voltage by programing the fpga with the default calibration values
                    refVoltage = self.VoltageForceCalibrationScenario(slot, subslot, dutid, rail, refGain, refOffset,
                                                                      railtype)

                    # get the voltage by programing the fpga with the selected gain and offsets from above
                    calibratedVoltageFromFpga = self.VoltageForceCalibrationScenario(slot, subslot, dutid, rail,
                                                                                     selectedGain, selectedOffset,
                                                                                     railtype)
                    # based on the seelected gain and offset calculate the voltage we expect to see
                    expectedCalibratedVoltage = voltageCalibrationGainReal * refVoltage + voltageCalibrationOffsetReal
                    # get the difference between the exepectd and actual voltages from the fpgas
                    delta = abs(expectedCalibratedVoltage - calibratedVoltageFromFpga)

                    # reset calibration register with default values
                    board.SetVoltageForceCalRegister(rail, refGain, refOffset)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual value from FPGA is more than error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedVoltageFromFpga, expectedCalibratedVoltage))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual value from FPGA is within the error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedVoltageFromFpga, expectedCalibratedVoltage))
                    self.Log('info', 'The reference is {}'.format(refVoltage))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append(
                        [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), calibratedVoltageFromFpga,
                         expectedCalibratedVoltage])
        s_myresult = str(myresult)
        self.Log('debug', 'The result summary is: Rail, Gain, Offset, Calibrated Voltage, Expected Voltage: {}'.format(
            s_myresult))

    # write offset and gain calibration values to the AD5560, apply and measure a voltage and return the voltage to the calling function
    def VoltageForceCalibrationScenario(self, slot, subslot, dutid, rail, gain, offset, railtype):

        self.Log('info', '\nTesting HCLC rail {} for dutid {}...'.format(rail, dutid))
        board = self.env.instruments[slot].subslots[subslot]
        board.SetRailsToSafeState()

        # Setup the cal cage depending on the rail type
        if (railtype == "LC"):
            self.Log('info', 'Supply Type is: {}'.format(railtype))
            board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')
        else:
            board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')
            self.Log('info', 'Supply Type is: {}'.format(railtype))

        # clear and enable alarms
        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        # check the calibration registers for default values and then program them with the passed to values
        board.CheckVoltageForceCalRegister()
        board.SetVoltageForceCalRegister(rail, gain, offset)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,railtype)
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'LC')

        # build trigger queue to force 1V and measure voltage
        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             TqNotify rail=0, value=BEGIN
             TqNotify rail=16, value=BEGIN
             SetMode rail=RAIL, value=VFORCE
             #Q cmd=TIME_DELAY, arg=RAIL, data=0x1388
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=1.0
             SetILoFreeDrive rail=RAIL, value=-1.0
             SetIClampHi rail=RAIL, value=1.2 
             SetIClampLo rail=RAIL, value=-1.2
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             EnableDisableRail rail=RAIL, value=1
             SetVoltage rail=RAIL, value=1.0    
             TimeDelay rail=RAIL, value=10000
             SetVCompAlarm rail=RAIL, value=0
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=1000
             TqNotify rail=0, value=END
             TqNotify rail=16, value=END
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'LC')))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)

        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        # execute trigger queue
        board.ExecuteTriggerQueue(offset, dutid)

        # **get memory location of voltage for debug only
        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        # **print out global alarm register for debug purposes
        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        # get voltage from sample memory
        sampleVoltage = board.ReturnSampleData(SampleBase + 0x1000 * dutid)
        self.Log('info', 'Sample voltage is  {}'.format(sampleVoltage))

        # check if there were alarms and clear them
        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

        # return voltage back to calling function
        return sampleVoltage
