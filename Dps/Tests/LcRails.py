################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: 
# -------------------------------------------------------------------------------
#     Purpose: 
# -------------------------------------------------------------------------------
#  Created by: Renuka Agrawal
#        Date: 1/27/15
#       Group: HDMT FPGA Validation
################################################################################
import binascii
import ctypes
import io
import random
import re
from string import Template
import struct
import time
import unittest

from Common.instruments.dps.symbols import RAILCOMMANDS
from Common.instruments.dps.symbols import SETMODECMD
from Common.instruments.dps.symbols import DPSTRIGGERS
from Common.instruments.dps.symbols import EVENTTRIGGER
from Common.instruments.dps.symbols import CROSSBOARDTRIGGER
from _build.bin import hddpstbc
from Common.instruments.dps.trigger_queue import TriggerQueueString
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest
from Common import hilmon as hil


TRIGGER_QUEUE_OFFSET = 0x0
class Conditions(BaseTest):

    ## Lc Ramp Voltage End
    @unittest.skip('Skipping all LcRail.py test until further debug')
    def DirectedTriggerQueueSampleEngineTest(self):
        railCount = 10
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['LC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.TriggerQueueSampleEngineScenerio(rail, uhc, board)

    def TriggerQueueSampleEngineScenerio(self, rail, uhc, board):

        HeaderBase = 0x00005000
        HeaderSize = 0x00000100
        SampleBase = 0x00010000
        SampleSize = 0x00100000
        board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, uhc, 'LC')

        self.Log('info', 'Testing LC rail {} and dutid {}...'.format(rail, uhc))

        board.SetRailsToSafeState()

        voltage_range_start = 1
        voltage_range_end = 5
        voltage_range = voltage_range_end - voltage_range_start
        voltage_resolution = .25
        voltage_steps = voltage_range / voltage_resolution
        force_voltage = (random.randint(0,voltage_steps)*voltage_resolution) + voltage_range_start

        # No load is connected
        board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

        board.ClearDpsAlarms()
        board.EnableAlarms()
        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'LC')
        board.UnGangAllRails()

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

        sample_engine_delay = 3000
        sample_engine_count = 0x40
        sample_engine_rate = 0x0

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid << 1
        trigger_queue.symbols['BEGIN'] = (dutid << 1) | 1
        trigger_queue.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE        
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=0.6
             SetILoFreeDrive rail=RAIL, value=-0.6
             SetIClampHi rail=RAIL, value=0.5
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=5.0
             SetUV rail=RAIL, value=-3.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             $sampleengine
             TimeDelay rail=RAIL, value=1
             EnableDisableRail rail=RAIL, value=1
             SetFreeDrive rail=RAIL, start=$forcevoltage, delay=1500, end=0
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=100
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(triggerqueueheader=trigger_queue_header, triggerqueuefooter=trigger_queue_footer, forcevoltage = force_voltage,
                         sampleengine=board.ConfigureAndStartSamplingEngine(uhc, delay=sample_engine_delay, count=sample_engine_count, rate=sample_engine_rate, rail_type='LC', rails=1 << rail)))

        trigger_queue_data = trigger_queue.Generate()
        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'LC')
        global_alarms = board.get_global_alarms()
        if  global_alarms!= []:
            self.Log('error','Received global alarm(s): {}'.format(global_alarms))

        # check header and expected voltages in sample memory
        sample_headers = board.GetSampleHeaders(uhc, 1, 'LC', wait_loops=50000)

        if sample_headers != None:
            # Field: RailConfig, Value: 0x0
            if sample_headers[0].SamplePointer != SampleBase:
                self.Log('error','Sample pointer returned by fpga {}, doesnt match expected pointer {}'.format(sample_headers[0].SamplePointer,SampleBase))

            if sample_headers[0].SampleRate != sample_engine_rate:
                self.Log('error','Sample rate returned by fpga {}, doesnt match expected rate {}'.format(sample_headers[0].SampleRate,sample_engine_rate))

            if sample_headers[0].SampleCount != sample_engine_count:
                self.Log('error','Sample count returned by fpga {}, doesnt match expected count {}'.format(sample_headers[0].SampleCount,sample_engine_count))

            if sample_headers[0].RailSelect != (1 << rail):
                self.Log('error','Rail select returned by fpga {}, doesnt match expected rails {}'.format(sample_headers[0].RailSelect,(1<<rail)))

            if sample_headers[0].MetaLo != 0x3210:
                self.Log('error','Meta Data Low returned by fpga 0x{:x}, doesnt match expected value 0x{:x}'.format(sample_headers[0].MetaLo,0x3210))

            if sample_headers[0].MetaHi != 0x7654:
                self.Log('error','Meta Data High returned by fpga 0x{:x}, doesnt match expected value 0x{:x}'.format(sample_headers[0].MetaHi,0x7654))

            sample_memory = board.DmaRead(sample_headers[0].SamplePointer,sample_headers[0].SampleCount*8)
            sample_data_stream = io.BytesIO(sample_memory)
            sample_data_stream.seek(0)
            sample_engine_count = 0
            for sample in range(sample_headers[0].SampleCount):
                voltage = struct.unpack('<f', sample_data_stream.read(4))[0]
                current = struct.unpack('<f', sample_data_stream.read(4))[0]
                allowed_percentage_deviation = 20
                if board.is_close(voltage, force_voltage, allowed_percentage_deviation) == False:
                    self.Log('error',
                             'The HCLC rail {}, sampled voltage is {}V, sample nummber {}, is not within the allowable deviation of {}% of {}V'.format(
                                 rail, voltage, sample_engine_count, allowed_percentage_deviation, force_voltage))
                    break
                sample_engine_count = sample_engine_count + 1
        else:
            self.Log('error', 'No Sample Data Returned')

        board.SetRailsToSafeState()

    '''DirectedSampleEngineResetTest: Start sample engine running then reset it, we should not get a valid sample 
        header, and we should not get any alarms raised'''

    @unittest.skip('Skipping all LcRail.py test until further debug')
    def DirectedSampleEngineResetTest(self):
        railCount = 10
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['LC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.DirectedSampleEngineResetScenerio(rail, uhc, board)

    @unittest.skip('Skipping all LcRail.py test until further debug')
    def DirectedSampleEngineResetScenerio(self,rail, uhc, board):
        force_voltage= 1.0

        HeaderBase = 0x00005000
        HeaderSize = 0x00000100
        SampleBase = 0x00010000
        SampleSize = 0x00100000
        board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, uhc, 'LC')

        self.Log('info', 'Testing LC rail {} and dutid {}...'.format(rail, uhc))
        board.SetRailsToSafeState()
        # No load is connected
        board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

        board.ClearDpsAlarms()
        board.EnableAlarms()

        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId = dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'LC')
        board.UnGangAllRails()

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid << 1
        trigger_queue.symbols['BEGIN'] = (dutid << 1) | 1
        trigger_queue.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=0.6
             SetILoFreeDrive rail=RAIL, value=-0.6
             SetIClampHi rail=RAIL, value=0.5
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=5.0
             SetUV rail=RAIL, value=-3.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             TimeDelay rail=RAIL, value=1
             $sampleengine
             Q cmd=0x0, arg=$delayregister, data=500
             Q cmd=0x0, arg=HclcSampleEngineReset, data=$uhc
             EnableDisableRail rail=RAIL, value=1
             SetFreeDrive rail=RAIL, start=$forcevoltage, delay=3000, end=0
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=100
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(triggerqueueheader=trigger_queue_header,
                         triggerqueuefooter=trigger_queue_footer,
                         uhc = 1 << uhc,
                         forcevoltage = force_voltage, delayregister = 'Hclc{}SampleDelay'.format(uhc),
                         sampleengine=board.ConfigureAndStartSamplingEngine(uhc, delay=10, count=0x0FFF, rate=0x0, rail_type='LC',rails=1 << rail)
                         ))

        trigger_queue_data = trigger_queue.Generate()
        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'LC')

        global_alarms = board.get_global_alarms()
        if  global_alarms!= []:
            self.Log('error','Received global alarm(s): {}'.format(global_alarms))

        header_byte_size = 16
        header_terminator_byte_size = 4
        header_terminator_location = HeaderBase + header_byte_size
        wait_loops = 2500
        for i in range(wait_loops):
            terminator = board.DmaRead(header_terminator_location,header_terminator_byte_size)
            if '0xffffffff' == hex(int.from_bytes(terminator,'little')):
                self.Log('error', 'Sample engine terminator raised, sample engine should not have completed')
                break

        board.SetRailsToSafeState()


    ## Lc Ramp Voltage End
    @unittest.skip('Skipping all LcRail.py test until further debug')
    def DirectedLcRampVoltageOpenSocketTest(self):
        railCount = 10
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['LC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.LcRampVoltageOpenSocketScenario(rail, uhc, board)

    def LcRampVoltageOpenSocketScenario(self, rail, uhc, board):

        force_voltages= [1.0,1.5,2.0]

        HeaderBase = 0x00005000
        HeaderSize = 0x00000100
        SampleBase = 0x00010000
        SampleSize = 0x00100000
        board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, uhc, 'LC')

        self.Log('info', 'Testing LC rail {} and dutid {}...'.format(rail, uhc))
        board.SetRailsToSafeState()
        # No load is connected
        board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

        board.ClearDpsAlarms()
        board.EnableAlarms()

        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'LC')
        board.UnGangAllRails()

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid << 1
        trigger_queue.symbols['BEGIN'] = (dutid << 1) | 1
        trigger_queue.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE             
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=0.6
             SetILoFreeDrive rail=RAIL, value=-0.6
             SetIClampHi rail=RAIL, value=0.5
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=5.0
             SetUV rail=RAIL, value=-3.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             TimeDelay rail=RAIL, value=1
             $sampleengine
             $sampleenginetwo
             $sampleenginethree
             EnableDisableRail rail=RAIL, value=1
             SetFreeDrive rail=RAIL, start=$forcevoltagezero, delay=3000, end=0
             SetFreeDrive rail=RAIL, start=$forcevoltageone, delay=3000, end=0
             SetFreeDrive rail=RAIL, start=$forcevoltagetwo, delay=3000, end=0
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=100
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(triggerqueueheader=trigger_queue_header,
                         triggerqueuefooter=trigger_queue_footer,
                         forcevoltagezero = force_voltages[0],
                         forcevoltageone = force_voltages[1],
                         forcevoltagetwo = force_voltages[2],
                         sampleengine=board.ConfigureAndStartSamplingEngine(uhc, delay=1500, count=0x1, rate=0x0, rail_type='LC', rails=1 << rail),
                         sampleenginetwo=board.ConfigureAndStartSamplingEngine(uhc, delay=4500, count=0x1, rate=0x0, rail_type='LC', rails=1 << rail),
                         sampleenginethree=board.ConfigureAndStartSamplingEngine(uhc, delay=7500, count=0x1, rate=0x0, rail_type='LC', rails=1 << rail),
                         ))

        trigger_queue_data = trigger_queue.Generate()
        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'LC')

        global_alarms = board.get_global_alarms()
        if  global_alarms!= []:
            self.Log('error','Received global alarm(s): {}'.format(global_alarms))

        # check for expected voltages
        sample_headers = board.GetSampleHeaders(uhc,3,'LC', wait_loops=50000)

        for i in range(len(force_voltages)):
            sampled_voltage = struct.unpack('<f', board.DmaRead(sample_headers[i].SamplePointer,4))[0]
            allowed_percentage_deviation = 20
            if board.is_close(sampled_voltage, force_voltages[i], allowed_percentage_deviation) == True:
                self.Log('info', 'The HCLC rail {}, sampled voltage is {}V'.format(rail, sampled_voltage))
            else:
                self.Log('error',
                     'The HCLC rail {}, sampled voltage is {}V is not within the allowable deviation of {}% of {}V'.format(
                         rail, sampled_voltage, allowed_percentage_deviation, force_voltages[i]))

        board.SetRailsToSafeState()


    def verify_rail_voltage_and_current(self,board,rail,rail_type,uhc,expected_voltage,expected_current):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        rail_current = board.get_rail_current(rail,rail_type)
        allowed_voltage_deviation = abs((30/100)*expected_voltage)
        allowed_current_deviation = abs((40/100)*expected_current)
        if board.is_in_maximum_allowed_deviation(expected_voltage, rail_voltage, allowed_voltage_deviation) == False or \
                        board.is_in_maximum_allowed_deviation(expected_current, rail_current, allowed_current_deviation) ==False:
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V and current {:.2f} is not within the allowable deviation of {:.2f}V,{:.2f}'.format(
                         rail_type, rail, uhc, rail_voltage,rail_current, expected_voltage,expected_current))
            return False
        return True

    def DirectedLcForceCurrentTest(self):
        dutid = 15
        rail_type = 'LC'
        irange = 'I_500_MA'
        settling_time_delay = 0.1
        force_voltage = 3
        iforce_list = [0.2, 0.1, 0.075, 0.05, 0.025]
        for board in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            pass_count = 0
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            load_selected = 'OHM_10'
            load_resistance = int(re.sub('\D', '',load_selected))
            calboard = board.create_cal_board_instance_and_initialize()
            board.cal_load_connect(load_selected, calboard)
            for rail, uhc in rail_uhc_tuple_list:
                for iforce in iforce_list:
                    iforce_raw_dac_code = board.conversion_to_ad5764daccode(iforce,irange,rail_type)
                    expected_voltage = iforce*load_resistance
                    board.ClearDpsAlarms()
                    board.EnableAlarms()
                    board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                    board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                    board.SetRailsToSafeState()
                    if rail_type == 'HV' or rail_type == 'LVM':
                        board.ResetVoltageSoftSpanCode(rail_type, dutid)
                        board.ResetCurrentSoftSpanCode(rail_type, dutid)
                    board.cal_connect_force_sense_lines(rail, rail_type, calboard)

                    self.iforce_trigger_queue(board, dutid, rail, rail_type, uhc, force_voltage, iforce_raw_dac_code)
                    time.sleep(settling_time_delay)

                    global_alarms = board.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error',
                                 '{} rail {} Unexpected alarms received {}'.format(rail_type, rail,
                                                                                                       global_alarms))
                    else:
                        if self.verify_rail_voltage_and_current(board,rail,rail_type,uhc,expected_voltage,iforce) == True:
                            pass_count +=1

                    board.cal_disconnect_force_sense_lines(rail, rail_type, calboard)
            if pass_count == len(rail_uhc_tuple_list)*len(iforce_list):
                self.Log('info', 'Expected LC IForce mode seen on all UHC/Rail for {} combinations '.format(len(rail_uhc_tuple_list)* len(iforce_list)))
            else:
                self.Log('error',
                         'Unexpected LC IForce mode seen on {} of {} UHC/Rail combinations.'.format(
                             (len(rail_uhc_tuple_list)* len(iforce_list)) - pass_count, len(rail_uhc_tuple_list)* len(iforce_list),))
            board.create_cal_board_instance_and_initialize()
            board.SetRailsToSafeState()

    def iforce_trigger_queue(self, board, dutid, rail, rail_type, uhc, force_voltage, force_current):
        trigger_queue_data = self.generate_trigger_queue(board, rail_type, force_voltage, dutid,rail, uhc,force_current)
        board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
        board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)

    def generateIForceTriggerQueue(self,tq_helper,force_current):
        enable_local_sense = 1
        trigger_queue = ''

        force_trigger_queue_header = tq_helper.get_trigger_queue_header(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + force_trigger_queue_header

        test_mode = tq_helper.get_test_mode(tq_helper.force_rail, tq_helper.force_rail_type, 'IFORCE')
        trigger_queue = trigger_queue + '\n' + test_mode

        if tq_helper.open_socket:
            enable_sense = tq_helper.get_trigger_queue_enable_disable_sense(tq_helper.force_rail, tq_helper.force_rail_type,enable_local_sense)
            trigger_queue = trigger_queue + '\n' + enable_sense

        current_range = tq_helper.get_current_range(tq_helper.force_rail, tq_helper.force_rail_type, tq_helper.force_rail_irange)
        trigger_queue = trigger_queue + '\n' + current_range

        disable_rail = tq_helper.get_disable_rail(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + disable_rail

        current_clamps = tq_helper.get_current_clamp_limits_string(tq_helper.force_rail, tq_helper.force_rail_type,
                                                                   tq_helper.force_rail_irange,
                                                                   tq_helper.force_clamp_low, tq_helper.force_clamp_high)
        trigger_queue = trigger_queue + '\n' + current_clamps

        if tq_helper.force_rail_type=='HV' or tq_helper.force_rail_type=='HC' or tq_helper.force_rail_type=='LC':
            voltage_limits = tq_helper.get_force_voltage_limits_string(tq_helper.force_rail, tq_helper.force_rail_type,
                                                                       tq_helper.force_low_voltage_limit, tq_helper.force_high_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        if tq_helper.force_rail_type == 'HC' or tq_helper.force_rail_type == 'LC':
            set_thermal_limit = 'Q cmd = SET_THERMAL_I_H_CLAMP, arg = {}, data = {}'.format(tq_helper.force_rail+16, 0xFFFF)
            trigger_queue = trigger_queue + '\n' + set_thermal_limit

        flush_clamps = tq_helper.get_flush_clamps(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + flush_clamps

        force_current = 'Q cmd = SET_CURRENT, arg = {}, data = {}'.format(tq_helper.force_rail + 16, force_current)
        trigger_queue = trigger_queue + '\n' + force_current

        if tq_helper.force_rail_type != 'HV':
            enable_rail = tq_helper.get_enable_rail(tq_helper.force_rail, tq_helper.force_rail_type)
            trigger_queue = trigger_queue + '\n' + enable_rail

        time_delay = tq_helper.get_time_delay(tq_helper.force_rail, tq_helper.force_rail_type, 700)
        trigger_queue = trigger_queue + '\n' + time_delay

        force_trigger_queue_footer = tq_helper.get_trigger_queue_footer(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + force_trigger_queue_footer

        tq_complete = tq_helper.get_tq_complete()
        trigger_queue = trigger_queue + '\n' + tq_complete
        return trigger_queue

    def generate_trigger_queue(self, board, rail_type, force_voltage, dutid, rail, uhc,force_current):
        tracking_voltage_offset = 2
        tracking_voltage = force_voltage+tracking_voltage_offset

        tq_helper = TriggerQueueString(board, uhc, dutid)

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.open_socket= False
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = tracking_voltage
        tq_helper.force_clamp_high = 0.5
        tq_helper.force_clamp_low = -0.5
        tq_helper.force_low_voltage_limit = -3
        tq_helper.force_high_voltage_limit = 10
        tq_helper.force_rail_irange = 'I_500_MA'
        tq_helper.free_drive = False
        tq_helper.force_free_drive_high = 7.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 8000

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generateIForceTriggerQueue(tq_helper,force_current))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data


    def LcForceCurrentScenario(self, rail, uhc, board):

        allowed_percentage_deviation = 20
        load_ohms = 10
        force_current_amps = 0.3
        expected_current_amps = force_current_amps
        expected_voltage = expected_current_amps * load_ohms

        self.Log('info', 'Testing LC rail {} for dutid {}...'.format(rail, uhc))
        board.SetRailsToSafeState()
        board.ConnectCalBoard('LC{}'.format(rail), 'OHM_{}'.format(load_ohms))

        board.ClearDpsAlarms()
        board.EnableAlarms()

        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'LC')
        board.UnGangAllRails()

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid << 1
        trigger_queue.symbols['BEGIN'] = (dutid << 1) | 1
        trigger_queue.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=IFORCE             
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIClampHi rail=RAIL, value=1.0
             SetIClampLo rail=RAIL, value=-1.0
             SetOV rail=RAIL, value=5.0
             SetUV rail=RAIL, value=-4.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             EnableDisableRail rail=RAIL, value=1
             SetCurrent rail=RAIL, value=$forcecurrent
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=1000
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header,
                         forcecurrent = force_current_amps
                         ))

        trigger_queue_data = trigger_queue.Generate()
        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'LC')

        #check for alarms
        global_alarms = board.get_global_alarms()
        if  global_alarms!= []:
            self.Log('error','Received global alarm(s): {}'.format(global_alarms))

        # check for expected voltage
        rail_voltage_regsiter = 'DpsRail' + str(rail) + 'V'
        rail_voltage = board.ReadRegister(getattr(board.registers, rail_voltage_regsiter)).value
        rail_voltage = struct.unpack('f', struct.pack('L', rail_voltage))[0]
        if board.is_close(expected_voltage, rail_voltage, allowed_percentage_deviation) == True:
            self.Log('info', 'The HCLC rail {}, instantaneous voltage is {}V'.format(rail, rail_voltage))
        else:
            self.Log('error',
                     'The HCLC rail {}, instantaneous voltage is {}V is not within the allowable deviation of {}% of {}V'.format(
                     rail, rail_voltage, allowed_percentage_deviation, expected_voltage))

        # check for expected current
        rail_current_regsiter = 'DpsRail' + str(rail) + 'I'
        rail_current = board.ReadRegister(getattr(board.registers, rail_current_regsiter)).value
        rail_current = struct.unpack('f', struct.pack('L', rail_current))[0]
        if board.is_close(expected_current_amps, rail_current, allowed_percentage_deviation) == True:
            self.Log('info', 'The HCLC rail {}, instantaneous current is {}A'.format(rail, rail_current))
        else:
            self.Log('error',
                     'The HCLC rail {}, instantaneous current is {}A is not within the allowable deviation of {}% of {}A'.format(
                     rail, rail_current, allowed_percentage_deviation, expected_current_amps))

        board.SetRailsToSafeState()



    ## Lc ComparatorHighAlarm
    @unittest.skip('Skipping this test as already implemented in VerifyAlarms.py')
    def DirectedComparatorHighAlarmTest(self):
        railCount = 10
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['LC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.DirectedComparatorHighAlarmScenario(rail, uhc, board)

    def DirectedComparatorHighAlarmScenario(self,rail, uhc, board):
        self.Log('info', 'Testing LC rail {} and uhc {}...'.format(rail, uhc))
        force_voltage = 1.8
        board.SetRailsToSafeState()

        # No load is connected
        board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

        board.ClearDpsAlarms()
        board.EnableAlarms()

        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'LC')
        board.UnGangAllRails()

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid <<  0x1
        trigger_queue.symbols['BEGIN'] = (dutid << 0x1 ) | 1
        trigger_queue.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE             
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=0.6
             SetILoFreeDrive rail=RAIL, value=-0.6
             SetIClampHi rail=RAIL, value=0.5
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=5.0
             SetUV rail=RAIL, value= -3.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             SetOV rail=RAIL, value=1.0
             SetUV rail=RAIL, value= -3.0
             TimeDelay rail=RAIL, value=1
             EnableDisableRail rail=RAIL, value=1
             SetFreeDrive rail=RAIL, start=$forcevoltage, delay=20000, end=0
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=100
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(triggerqueueheader=trigger_queue_header, triggerqueuefooter=trigger_queue_footer,
                         forcevoltage=force_voltage))
        trigger_queue_data = trigger_queue.Generate()
        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'LC')

        self.check_for_single_rail_alarm(board, rail, 'ComparatorHighAlarm')

        # check that all rails folded
        expected_fold_register = 0x3ff
        hclcfold = board.ReadRegister(board.registers.HclcRailsFolded).value
        if expected_fold_register == hclcfold:
            self.Log('info', 'All rails folded as expected.  Fold register value: 0x{:x}'.format(hclcfold))
        else:
            self.Log('error', 'Not all rails folded as expected. Fold register value: 0x{:x}'.format(hclcfold))

        board.SetRailsToSafeState()


    def check_for_single_rail_alarm(self, board,rail,alarm):
        global_alarms = board.get_global_alarms()
        if global_alarms == ['HclcRailAlarms']:
            hclc_alarms = board.ReadRegister(board.registers.HCLC_ALARMS)
            rail_mask = 1 << rail
            if rail_mask == hclc_alarms.value:
                hclc_rail_alarm = board.ReadRegister(board.registers.HCLC_PER_RAIL_ALARMS, index=rail)
                rail_alarms = []
                for field in hclc_rail_alarm.fields():
                    if getattr(hclc_rail_alarm, field) == 1:
                        rail_alarms.append(field)
                if rail_alarms == [alarm]:
                    self.Log('info','Expected {} Alarm recived on rail {}'.format(alarm, rail))
                    return True
                else:
                    self.Log('error',
                             'Expected only {} to be set in Hclc rail {} specific alarm register, actual alarms set were {}'.format(
                                 alarm, rail, rail_alarms))
            else:
                hclc_alarms_set = []
                for field in hclc_alarms.fields():
                    if getattr(hclc_alarms, field) == 1:
                        hclc_alarms_set.append(field)
                self.Log('error','Expected only Rail {} bit to be set in HCLC_alarms register, but got the following: {}'.format(rail,hclc_alarms_set))
        else:
            self.Log('error','Expected only \'HclcRailAlarms\' in GLOBAL_ALARMS to be set, actual fields set: {}'.format(global_alarms))
        return False


    ## Lc ComparatorLowAlarm
    @unittest.skip('Skipping this test as already implemented in VerifyAlarms.py')
    def DirectedComparatorLowAlarmTest(self):
        railCount = 10
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['LC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.DirectedComparatorLowAlarmScenerio(rail, uhc, board)

    def DirectedComparatorLowAlarmScenerio(self, rail, uhc, board):
        self.Log('info', 'Testing LC rail {} and uhc {}...'.format(rail, uhc))
        force_voltage = 1.8
        board.SetRailsToSafeState()

        # No load is connected
        board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

        board.ClearDpsAlarms()
        board.EnableAlarms()

        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'LC')
        board.UnGangAllRails()

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')
        trigger_queue = TriggerQueueAssembler()
        trigger_queue.symbols['RAIL'] = 16 + rail
        trigger_queue.symbols['END'] = dutid <<  0x1
        trigger_queue.symbols['BEGIN'] = (dutid << 0x1 ) | 1
        trigger_queue.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE             
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=0.6
             SetILoFreeDrive rail=RAIL, value=-0.6
             SetIClampHi rail=RAIL, value=0.5
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=5.0
             SetUV rail=RAIL, value= -3.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             SetOV rail=RAIL, value=3.0
             SetUV rail=RAIL, value= 2.5
             TimeDelay rail=RAIL, value=1
             EnableDisableRail rail=RAIL, value=1
             SetFreeDrive rail=RAIL, start=$forcevoltage, delay=20000, end=0
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=100
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(triggerqueueheader=trigger_queue_header, triggerqueuefooter=trigger_queue_footer,
                         forcevoltage=force_voltage))
        trigger_queue_data = trigger_queue.Generate()
        trigger_queue_offset = 0x0
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'LC')

        self.check_for_single_rail_alarm(board, rail, 'ComparatorLowAlarm')

        # check that all rails folded
        expected_fold_register = 0x3ff
        hclcfold = board.ReadRegister(board.registers.HclcRailsFolded).value
        if expected_fold_register == hclcfold:
            self.Log('info', 'All rails folded as expected.  Fold register value: 0x{:x}'.format(hclcfold))
        else:
            self.Log('error', 'Not all rails folded as expected. Fold register value: 0x{:x}'.format(hclcfold))

        board.SetRailsToSafeState()


    @unittest.skip('This test is no stable and needs to be understood and repaired. Previous mention was simluator mismatch, however passes using tc looping image')
    def DirectedClampHighAlarmwithlocalfoldpolicyTest(self):
        dutid = 0
        rail = 8
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            board = self.env.instruments[slot].subslots[subslot]
            board.SetRailsToSafeState()

            board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcRailAlarm()
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)
            # Reading the Aurora Block Receive Reset bit of resets register to make sure that it's blocked in order to validate local fold policy
            resetsregister = board.ReadRegister(board.registers.RESETS)
            self.Log('info', 'The resets register has a value set to Actual->0x{:x}'.format(resetsregister.value))
            resetsregister.BlockAuroraReceive = 1
            board.Write('RESETS', resetsregister.value)
            resetsregister = board.ReadRegister(board.registers.RESETS)
            self.Log('info',
                     'The resets register after blocking aurora receive bit has a value set to Actual->0x{:x}'.format(
                         resetsregister.value))

            board.EnableOnlyOneUhc(dutid)
            board.ConfigureHclcDutRail(dutid, 0x1 << rail)
            board.UnGangAllRails()

            # In order to check the local rail fold policy, sending an extra TQ notify
            command = RAILCOMMANDS.TQ_NOTIFY
            data = 1 + 2 * dutid
            board.WriteBar2RailCommand(command, data, 0)
            board.WriteBar2RailCommand(command, data, 16)

            hclctqfold = board.ReadRegister(board.registers.HclcRailsFolded)
            self.Log('info', 'Hclcrailfold Register has a previous value of. Actual->0x{:x}'.format(hclctqfold.value))
            board.Write('HclcRailsFolded', 0x3ff)
            hclctqfold = board.ReadRegister(board.registers.HclcRailsFolded)
            self.Log('info',
                     'Hclcrailfold Register clearing after initial TQ notify has a value. Actual->0x{:x}'.format(
                         hclctqfold.value))

            HeaderBase = 0x00005000
            HeaderSize = 0x00000100
            SampleBase = 0x00010000
            SampleSize = 0x00100000
            board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

            trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
            trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

            asm = TriggerQueueAssembler()
            asm.symbols['RAIL'] = 16 + rail
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 1 + 2 * dutid
            asm.LoadString(Template("""\
                $triggerqueueheader
                SetMode rail=RAIL, value=VFORCE
                SetCurrentRange rail=RAIL, value=I_1200_MA
                SetIClampHi rail=RAIL, value=0.4
                SetIClampLo rail=RAIL, value=-1.2
                SetVoltage rail=RAIL, value=5
                EnableDisableRail rail=RAIL, value=1
                $sampleengine
                $triggerqueuefooter
                TqComplete rail=0, value=0
             """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 5000, 0x1000, 0x0, 'LC'),
                             triggerqueuefooter=trigger_queue_footer,
                             triggerqueueheader=trigger_queue_header))

            data = asm.Generate()
            self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
            offset = 0x100 * dutid
            board.WriteTriggerQueue(offset, data)
            data = board.DmaRead(offset, 0x100)
            board.ExecuteTriggerQueue(offset, dutid, 'LC')

            railvoltage = 'DpsRail' + str(rail) + 'V'
            self.Log('info', 'The HCLC rail {} voltage is 0x{:x}'.format(rail, board.ReadRegister(getattr(board.registers,railvoltage)).value))

            globalalarm = board.ReadRegister(board.registers.GLOBAL_ALARMS)
            if globalalarm.HclcRailAlarms == 1:
                self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                          globalalarm.value))
                hclcrailalarm = board.ReadRegister(board.registers.HCLC_ALARMS)
                hclcrails = 'Hclc' + str(rail) + 'Alarms'
                hclcrailRegister = 'Hclc{:02d}Alarms'.format(rail)
                if getattr(hclcrailalarm, hclcrails) == 1:
                    hclcrail = board.ReadRegister(getattr(board.registers,hclcrailRegister))
                    self.Log('info', 'The register {} is set to 0x{:x}'.format(hclcrails, hclcrail.value))
                    if hclcrail.ClampAlarm == 1:
                        self.Log('info', 'The Clamp Alarm got set as expected for rail {}.'.format(rail))
                        clamp_alarm_current_register = 'Hclc' + str(rail) + 'ClampAlarmCurrent'
                        clamp_alarm_value = board.ReadRegister(getattr(board.registers,clamp_alarm_current_register))
                        self.Log('info',
                                 'The clamp alarm current register value in case of clamp alarm is 0x{:x}'.format(
                                     clamp_alarm_value.value))
                        # checking for local fold capability
                        hclctqfold = board.ReadRegister(board.registers.HclcRailsFolded)
                        self.Log('info',
                                 'Hclcrailfold Register due to clamp alarm has a value of. Actual->0x{:x}'.format(
                                     hclctqfold.value))
                        board.Write('HclcRailsFolded', 0x3ff)
                        hclctqfold = board.ReadRegister(board.registers.HclcRailsFolded)
                        self.Log('info',
                                 'Hclcrailfold Register clearing after clamp alarm has a value. Actual->0x{:x}'.format(
                                     hclctqfold.value))
                    else:
                        self.Log('error',
                                 'Only the Clamp Alarm for rail {} should be set. Actual -> 0x{:x}'.format(rail,
                                                                                                           hclcrail.value))
                else:
                    self.Log('error', 'The Hclc rail {} should have an alarm set.')
            else:
                self.Log('error',
                         'The HclcRailAlarm bit in globalalarm register should get set as testing for Clamp Alarm. Actual -> 0x{:x}'.format(
                             globalalarm.value))

            board.RunCheckers( 0, 'ResistiveLoopback', 10)

            board.ClearHclcRailAlarms()
            # Clearing the resets register for clean test exit
            resetsregister = board.ReadRegister(board.registers.RESETS)
            self.Log('info', 'The resets register at the end of test has a value set to Actual->0x{:x}'.format(
                resetsregister.value))
            resetsregister.BlockAuroraReceive = 0
            board.Write('RESETS', resetsregister.Pack())
            resetsregister = board.ReadRegister(board.registers.RESETS)
            self.Log('info',
                     'The resets register after enabling aurora receive bit has a value set to Actual->0x{:x}'.format(
                         resetsregister.value))
    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedClampHighAlarmwithVFORCEmodeTest(self):
        dutid = 0
        rail = 5
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            board = self.env.instruments[slot].subslots[subslot]
            board.SetRailsToSafeState()

            board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcRailAlarm()
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)

            board.EnableOnlyOneUhc(dutid)
            board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
            board.UnGangAllRails()

            HeaderBase = 0x00005000
            HeaderSize = 0x00000100
            SampleBase = 0x00010000
            SampleSize = 0x00100000
            board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

            trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
            trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

            asm = TriggerQueueAssembler()
            asm.symbols['RAIL'] = 16 + rail
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 1 + 2 * dutid
            asm.LoadString(Template("""\
                $triggerqueueheader
                SetMode rail=RAIL, value=VFORCE                
                SetCurrentRange rail=RAIL, value=I_1200_MA
                SetIClampHi rail=RAIL, value=0.4
                SetIClampLo rail=RAIL, value=-1.2
                SetOV rail=RAIL, value=7.9
                SetUV rail=RAIL, value=-4.4
                Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
                SetVoltage rail=RAIL, value=5
                EnableDisableRail rail=RAIL, value=1
                $triggerqueuefooter
                TqComplete rail=0, value=0
             """).substitute(triggerqueuefooter=trigger_queue_footer,
                             triggerqueueheader=trigger_queue_header))

            data = asm.Generate()
            self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
            offset = 0x100 * dutid
            board.WriteTriggerQueue(offset, data)
            data = board.DmaRead(offset, 0x100)
            board.ReadMemory(data)
            board.ExecuteTriggerQueue(offset, dutid, 'LC')

            railvoltage = 'DpsRail' + str(rail) + 'V'
            self.Log('info', 'The HCLC rail {} voltage is 0x{:x}'.format(rail, board.ReadRegister(getattr(board.registers,railvoltage)).value))

            globalalarm = board.ReadRegister(board.registers.GLOBAL_ALARMS)
            if globalalarm.HclcRailAlarms == 1:
                self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                          globalalarm.value))
                hclcrailalarm = board.ReadRegister(board.registers.HCLC_ALARMS)
                hclcrails = 'Hclc' + str(rail) + 'Alarms'
                hclcrailRegister = 'Hclc{:02d}Alarms'.format(rail)
                if getattr(hclcrailalarm, hclcrails) == 1:
                    hclcrail = board.ReadRegister(getattr(board.registers,hclcrailRegister))
                    self.Log('info', 'The register {} is set to 0x{:x}'.format(hclcrails, hclcrail.value))
                    if hclcrail.ClampAlarm == 1:
                        self.Log('info', 'The Clamp Alarm got set as expected for rail {}.'.format(rail))
                        clamp_alarm_current_register = 'Hclc' + str(rail) + 'ClampAlarmCurrent'
                        clamp_alarm_value = board.ReadRegister(getattr(board.registers,clamp_alarm_current_register))
                        self.Log('info',
                                 'The clamp alarm got set due to current clamp high and clamp current register value in single precision float is 0x{:x}'.format(
                                     clamp_alarm_value.value))
                    else:
                        self.Log('error',
                                 'Only the Clamp Alarm for rail {} should be set. Actual -> 0x{:x}'.format(rail,
                                                                                                           hclcrail.value))
                else:
                    self.Log('error', 'The Hclc rail {} should have an alarm set.')
            else:
                self.Log('error',
                         'The HclcRailAlarm bit in globalalarm register should get set as testing for Clamp Alarm. Actual -> 0x{:x}'.format(
                             globalalarm.value))
            board.RunCheckers( 0, 'ResistiveLoopback', 10)

    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedClampLowAlarmwithVFORCEmodeTest(self):
        dutid = 0
        rail = 5
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            board = self.env.instruments[slot].subslots[subslot]
            board.SetRailsToSafeState()

            board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcRailAlarm()
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)

            board.EnableOnlyOneUhc(dutid)
            board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
            board.UnGangAllRails()

            HeaderBase = 0x00005000
            HeaderSize = 0x00000100
            SampleBase = 0x00010000
            SampleSize = 0x00100000
            board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

            trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
            trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

            asm = TriggerQueueAssembler()
            asm.symbols['RAIL'] = 16 + rail
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 1 + 2 * dutid
            asm.LoadString(Template("""\
                $triggerqueueheader
                SetMode rail=RAIL, value=VFORCE                
                SetCurrentRange rail=RAIL, value=I_1200_MA
                SetIClampHi rail=RAIL, value=1.2
                SetIClampLo rail=RAIL, value=-0.15
                SetOV rail=RAIL, value=7.9
                SetUV rail=RAIL, value=-4.4
                Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
                SetVoltage rail=RAIL, value=-2.0
                EnableDisableRail rail=RAIL, value=1
                $triggerqueuefooter
                TqComplete rail=0, value=0
             """).substitute(triggerqueuefooter=trigger_queue_footer,
                             triggerqueueheader=trigger_queue_header))

            data = asm.Generate()
            self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
            offset = 0x100 * dutid
            board.WriteTriggerQueue(offset, data)
            data = board.DmaRead(offset, 0x100)
            board.ReadMemory(data)
            board.ExecuteTriggerQueue(offset, dutid, 'LC')

            railvoltage = 'DpsRail' + str(rail) + 'V'
            self.Log('info', 'The HCLC rail {} voltage is 0x{:x}'.format(rail, board.ReadRegister(getattr(board.registers,railvoltage)).value))

            globalalarm = board.ReadRegister(board.registers.GLOBAL_ALARMS)
            if globalalarm.HclcRailAlarms == 1:
                self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                          globalalarm.value))
                hclcrailalarm = board.ReadRegister(board.registers.HCLC_ALARMS)
                hclcrails = 'Hclc' + str(rail) + 'Alarms'
                hclcrailRegister = 'Hclc{:02d}Alarms'.format(rail)
                if getattr(hclcrailalarm, hclcrails) == 1:
                    hclcrail = board.ReadRegister(getattr(board.registers,hclcrailRegister))
                    self.Log('info', 'The register {} is set to 0x{:x}'.format(hclcrails, hclcrail.value))
                    if hclcrail.ClampAlarm == 1:
                        self.Log('info', 'The Clamp Alarm got set as expected for rail {}.'.format(rail))
                        clamp_alarm_current_register = 'Hclc' + str(rail) + 'ClampAlarmCurrent'
                        clamp_alarm_value = board.ReadRegister(getattr(board.registers,clamp_alarm_current_register))
                        self.Log('info',
                                 'The clamp alarm got set due to current clamp low and clamp current register value in single precision float is 0x{:x}'.format(
                                     clamp_alarm_value.value))
                    else:
                        self.Log('error',
                                 'Only the Clamp Alarm for rail {} should be set. Actual -> 0x{:x}'.format(rail,
                                                                                                           hclcrail.value))
                else:
                    self.Log('error', 'The Hclc rail {} should have an alarm set.')
            else:
                self.Log('error',
                         'The HclcRailAlarm bit in globalalarm register should get set as testing for Clamp Alarm. Actual -> 0x{:x}'.format(
                             globalalarm.value))
            board.RunCheckers( 0, 'ResistiveLoopback', 10)


    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLocalsenseEnableDisableTest(self):
        dutid = 0
        rail = 5
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            board = self.env.instruments[slot].subslots[subslot]
            board.SetRailsToSafeState()

            board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcRailAlarm()
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)

            board.EnableOnlyOneUhc(dutid)
            board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
            board.UnGangAllRails()
            DPS_REGISTER2 = board.ad5560regs.DPS_REGISTER2.ADDR
            ALARM_SETUP = board.ad5560regs.ALARM_SETUP.ADDR

            dps_register2_initial_value = board.ReadAd5560Register(DPS_REGISTER2, rail)
            alarm_setup_register_initial_value = board.ReadAd5560Register(ALARM_SETUP, rail)

            if dps_register2_initial_value == (dps_register2_initial_value & 0xfeff):
                self.Log('info',
                         'The DPS_REGSITER_2 for rail {} is initially set to a correct value 0x{:x}'.format(rail,
                                                                                                            dps_register2_initial_value))
            else:
                self.Log('error',
                         'The DPS_REGSITER_2 for rail {} is initially set to an incorrect value 0x{:x}'.format(rail,
                                                                                                               dps_register2_initial_value))
            trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
            trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

            asm = TriggerQueueAssembler()
            asm.symbols['RAIL'] = 16 + rail
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 1 + 2 * dutid
            asm.LoadString(Template("""\
                $triggerqueueheader
                EnableDisableLocalSense rail=RAIL, value=1
                EnableDisableRail rail=RAIL, value=1
                $triggerqueuefooter
                TqComplete rail=0, value=0
             """).substitute(triggerqueuefooter=trigger_queue_footer,
                             triggerqueueheader=trigger_queue_header))

            data = asm.Generate()
            self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
            offset = 0x100 * (dutid)
            board.WriteTriggerQueue(offset, data)
            data = board.DmaRead(offset, 0x100)
            board.ReadMemory(data)
            board.ExecuteTriggerQueue(offset, dutid, 'LC')

            dps_register2_value_after_local_sense = board.ReadAd5560Register(DPS_REGISTER2, rail)
            alarm_setup_register_after_local_sense = board.ReadAd5560Register(ALARM_SETUP, rail)

            # Checking whether bit 8 is set high or not which indicates the local sense mode to connect an internal resistor of 10K bw force and sense lines
            if dps_register2_value_after_local_sense == ((dps_register2_initial_value & 0xfff) | 0x100):
                self.Log('info',
                         'The DPS_REGSITER_2 for rail {} after enabling the local sense is set to a correct value of 0x{:x}'.format(
                             rail, dps_register2_value_after_local_sense))
            else:
                self.Log('error',
                         'The DPS_REGSITER_2 for rail {} after enabling the local sense is set to 0x{:x}'.format(rail,
                                                                                                                 dps_register2_value_after_local_sense))

            # Checking whether bit 10 is set high or not which indicates the DUTALM bit high to disable 50uA pullup
            if alarm_setup_register_after_local_sense == 0x0400:
                self.Log('info',
                         'The ALARM_SETUP for rail {} after enabling the local sense is set to a correct value of 0x{:x}'.format(
                             rail, alarm_setup_register_after_local_sense))
            else:
                self.Log('error',
                         'The ALARM_SETUP for rail {} after enabling the local sense is set to 0x{:x}'.format(rail,
                                                                                                              alarm_setup_register_after_local_sense))

            asm = TriggerQueueAssembler()
            asm.symbols['RAIL'] = 16 + rail
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 1 + 2 * dutid
            asm.LoadString(Template("""\
                $triggerqueueheader
                EnableDisableLocalSense rail=RAIL, value=0
                EnableDisableRail rail=RAIL, value=1
                $triggerqueuefooter
                TqComplete rail=0, value=0
             """).substitute(triggerqueuefooter=trigger_queue_footer,
                             triggerqueueheader=trigger_queue_header))

            data = asm.Generate()
            self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
            offset = 0x100 * (dutid)
            board.WriteTriggerQueue(offset, data)
            data = board.DmaRead(offset, 0x100)
            board.ReadMemory(data)
            board.ExecuteTriggerQueue(offset, dutid, 'LC')

            dps_register2_value_after_remote_sense = board.ReadAd5560Register(DPS_REGISTER2, rail)
            alarm_setup_register_after_remote_sense = board.ReadAd5560Register(ALARM_SETUP, rail)

            if dps_register2_value_after_remote_sense == (dps_register2_value_after_local_sense & 0xeff):
                self.Log('info',
                         'The DPS_REGSITER_2 for rail {} after enabling the remote sense is set to a correct value of 0x{:x}'.format(
                             rail, dps_register2_value_after_remote_sense))
            else:
                self.Log('error',
                         'The DPS_REGSITER_2 for rail {} after enabling the remote sense is set to 0x{:x}'.format(rail,
                                                                                                                  dps_register2_value_after_remote_sense))

            if alarm_setup_register_after_remote_sense == 0x0:
                self.Log('info',
                         'The ALARM_SETUP for rail {} after enabling the remote sense is set to a correct value of 0x{:x}'.format(
                             rail, alarm_setup_register_after_remote_sense))
            else:
                self.Log('error',
                         'The ALARM_SETUP for rail {} after enabling the remote sense is set to 0x{:x}'.format(rail,
                                                                                                               alarm_setup_register_after_remote_sense))

    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLcRailTcLoopingTest(self):
        dutid = 0
        lutdwordaddress = 0x10000000

        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for rail in range(10):
                self.Log('info', '\nTesting LC rail {} on dutid {}...'.format(rail, dutid))
                board = self.env.instruments[slot].subslots[subslot]

                board.SetRailsToSafeState()
                board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
                board.UnGangAllRails()

                HeaderBase = 0x00005000
                HeaderSize = 0x00000100
                SampleBase = 0x00010000
                SampleSize = 0x00100000
                board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

                trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'LC')
                trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'LC')

                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                # Start loading the start block, Loop block, Stop block, Complete block for TC Looping

                # Start Block
                asm.LoadString(Template("""\
                    $triggerqueueheader
                    LoopControl rail=RAIL, value=0
                    LoopTrigger rail=RAIL, value=1
                    SoftwareTrigger rail=RAIL, value=0
                    TqComplete rail=0, value=0    
                """).substitute(triggerqueueheader=trigger_queue_header))

                data = asm.Generate()
                self.Log('info', 'Testing start block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_start_block = 0x1000
                board.WriteTriggerQueue(offset_start_block, data)
                data = board.DmaRead(offset_start_block, 0x100)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000)
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_start_block))
                board_2 = self.env.instruments[slot].subslots[1]
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                # Looping Block
                asm.LoadString("""\
                    TimeDelay rail=RAIL, value=1
                    TimeDelay rail=RAIL, value=2
                    TimeDelay rail=RAIL, value=3
                    LoopTrigger rail=RAIL, value=1
                    TqComplete rail=0, value=0    
                """)

                data = asm.Generate()
                self.Log('info', 'Testing looping block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_looping_block = 0x2000
                board.WriteTriggerQueue(offset_looping_block, data)
                data = board.DmaRead(offset_looping_block, 0x100)
                # board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x04
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_looping_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                # Stop Block
                asm.LoadString("""\
                    LoopTrigger rail=RAIL, value=3
                    LoopControl rail=RAIL, value=1
                    TqComplete rail=0, value=0    
                """)

                data = asm.Generate()
                self.Log('info', 'Testing the stop block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_stop_block = 0x4000
                board.WriteTriggerQueue(offset_stop_block, data)
                data = board.DmaRead(offset_stop_block, 0x100)
                # board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x08
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_stop_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                # Done/Complete Block
                asm.LoadString(Template("""\
                    LoopControl rail=RAIL, value=2
                    SoftwareTrigger rail=RAIL, value=3
                    $triggerqueuefooter
                    TqComplete rail=0, value=0    
                """).substitute(triggerqueuefooter=trigger_queue_footer))

                data = asm.Generate()
                self.Log('info', 'Testing the stop block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_complete_block = 0x8000
                board.WriteTriggerQueue(offset_complete_block, data)
                data = board.DmaRead(offset_complete_block, 0x100)
                # board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x0c
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_complete_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                # Read the initial value out of the hclcrailsloopingregister
                HclcRailsLoopingregister = 'HclcRailsLooping'
                self.Log('info',
                         'The HCLC Rails Looping Register before executing TC Looping set to a value = 0x{:x}'.format(
                             board.ReadRegister(board.registers.HclcRailsLooping).value))

                # Send the trigger for the start block with a LUT Address of 14'h0
                self.env.send_trigger(0x04000000)
                self.Log('info', 'RC sent trigger 0x04000000')

                # Read out of the hclcrailslooping register
                hclc_looping_register_initial_value = 0x3ff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0x3ff & ~(1 << rail))
                looping_register_new_value = looping_bits | complete_bits

                time.sleep(0.01)
                if board.ReadRegister(board.registers.HclcRailsLooping).value != looping_register_new_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after executing start block is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing start block is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))

                    # wait for couple of looping triggers to be received at the HDDPS side
                ExpectedTrigVal = 0x04000001
                for i in range(0, 4):
                    for count in range(5):
                        time.sleep(0.000001)
                        triggerValue = hil.dpsBarRead(slot, subslot, 1, 0x54)
                        if triggerValue == ExpectedTrigVal:
                            self.Log('info', 'Received the looping trigger at the HDDPS Side')
                            break
                        elif count == 4 and triggerValue != ExpectedTrigVal:
                            self.Log('error',
                                     'Correct Trigger value not received for Looping Block, Actual Value -> {:x}'.format(
                                         triggerValue))

                # Send the trigger to the stop block to terminate the tc looping
                self.env.send_trigger(0x04000002)

                # Read out of HclcRailsLoopingregister to make sure that the rail complete bit goes high and rail looping bit goes low
                time.sleep(0.001)
                if board.ReadRegister(board.registers.HclcRailsLooping).value != hclc_looping_register_initial_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after executing the complete block is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing the complete block is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))

                    # Try reading multiple times from trigger down register to ensure that the fifo is empty
                for num_reads in range(10):
                    hil.dpsBarRead(slot, 0, 1, 0x54)
                    hil.dpsBarRead(slot, 1, 1, 0x54)

                alarm = board.CheckHclcRailAlarm()
                if alarm:
                    board.ClearHclcRailAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLcRailTcLoopingAlarmConditionTest(self):
        dutid = 0

        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue

            for rail in range(10):
                self.Log('info', '\nTesting LC rail {} on dutid {}...'.format(rail, dutid))
                board = self.env.instruments[slot].subslots[subslot]

                board.SetRailsToSafeState()
                board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
                board.UnGangAllRails()

                HeaderBase = 0x00005000
                HeaderSize = 0x00000100
                SampleBase = 0x00010000
                SampleSize = 0x00100000
                board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

                # Sending a TQ Notify to bring the rail 0 out of safe state
                board.WriteTQHeaderViaBar2(dutid, rail, 'LC')

                # Sending a loop control command to rail 0 to start looping
                command = RAILCOMMANDS.LOOP_CONTROL
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail + 16)

                # Read out of the hclcrailslooping register
                hclc_looping_register_initial_value = 0x3ff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0x3ff & ~(1 << rail))
                looping_register_new_value = looping_bits | complete_bits

                HclcRailsLoopingregister = 'HclcRailsLooping'
                time.sleep(0.00001)

                if board.ReadRegister(board.registers.HclcRailsLooping).value != looping_register_new_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after executing loop control rail command is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing loop control rail command is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))


                    # Sending another loop control command to rail 0 to create a looping alarm condition
                command = RAILCOMMANDS.LOOP_CONTROL
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail + 16)

                # In this case both looping and complete bits for a particular rail should be 1 to satisy alarm condition
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0x3ff & ~(1 << rail)) | (1 << rail)
                looping_register_new_value = looping_bits | complete_bits

                HclcRailsLoopingregister = 'HclcRailsLooping'
                time.sleep(0.001)

                if board.ReadRegister(board.registers.HclcRailsLooping).value != looping_register_new_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after second consecutive loop control rail command is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing second consecutive loop control rail command is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))

                globalalarm = board.ReadRegister(board.registers.GLOBAL_ALARMS)
                if globalalarm.HclcRailAlarms == 1:
                    self.Log('info',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                      globalalarm.value))
                    hclcrailalarm = board.ReadRegister(board.registers.HCLC_ALARMS)
                    hclcrails = 'Hclc' + str(rail) + 'Alarms'
                    hclcrailRegister = 'Hclc{:02d}Alarms'.format(rail)
                    if getattr(hclcrailalarm, hclcrails) == 1:
                        hclcrail = board.ReadRegister(getattr(board.registers, hclcrailRegister))
                        self.Log('info', 'The register {} is set to 0x{:x}'.format(hclcrails, hclcrail.value))
                        if hclcrail.LoopinginProgressAlarm == 1:
                            self.Log('info',
                                     'The Looping in Progress Alarm got set as expected for rail {}.'.format(rail))
                        else:
                            self.Log('error',
                                     'Only the Looping in Progress Alarm for rail {} should be set. Actual -> 0x{:x}'.format(
                                         rail, hclcrail.value))
                    else:
                        self.Log('error', 'The Hclc rail {} should have an alarm set.')
                else:
                    self.Log('error',
                             'The HclcRailAlarm bit in globalalarm register should get set as testing for Looping in Progress Alarm. Actual -> 0x{:x}'.format(
                                 globalalarm.value))

                # Clear the value of looping bit to bring the TC looping Mode to default state by wrting to HclcRailsLoopingRegister
                HclcRailsLoopingregister = 'HclcRailsLooping'
                board.Write(HclcRailsLoopingregister, (0x3ff << (16)))
                VlcRailsLoopingRegister = 'VlcRailsLooping'
                board.Write(VlcRailsLoopingRegister, (0xffff << (16)))
                time.sleep(0.00001)
                vlc_looping_register_initial_value = 0xffff

                if board.ReadRegister(board.registers.HclcRailsLooping).value != hclc_looping_register_initial_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.HclcRailsLooping).value))

                if board.ReadRegister(board.registers.VlcRailsLooping).value != vlc_looping_register_initial_value:
                    self.Log('error',
                             'The VLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.VlcRailsLooping).value))
                else:
                    self.Log('info',
                             'The VLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.ReadRegister(board.registers.VlcRailsLooping).value))

                board.ClearHclcRailAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLcRailIsinkFeatureTest(self):
        dutid = 0

        x1_value_for_3_volts = 0x94b1
        x1_value_for_2p5_volts = 0x8ab3
        x1_value_for_neg_1_volts = 0x44c5
        x1_value_for_neg_0p5_volts = 0x4ec2
        x1_value_for_2_volts = 0x80b6
        x1_value_for_neg_0p2_volts = 0x54c1

        vforce_mode = 0b00
        isink_mode = 0b10

        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue

            for rail in range(10):
                self.Log('info', '\nTesting LC rail {} on dutid {}...'.format(rail, dutid))
                board = self.env.instruments[slot].subslots[subslot]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
                board.UnGangAllRails()
                CPL_DAC_X1 = board.ad5560regs.CPL_DAC_X1.ADDR
                CPH_DAC_X1 = board.ad5560regs.CPH_DAC_X1.ADDR

                cpol_initial_value = board.ReadAd5560Register(CPL_DAC_X1, rail)
                cpoh_initial_value = board.ReadAd5560Register(CPH_DAC_X1, rail)

                self.Log('info', 'The CPL_DAC_X1 for rail {} is initially set to a value 0x{:x}'.format(rail,
                                                                                                        cpol_initial_value))
                self.Log('info', 'The CPH_DAC_X1 for rail {} is initially set to a value 0x{:x}'.format(rail,
                                                                                                        cpoh_initial_value))

                # Update the rail mode to VFORCE initially
                command = RAILCOMMANDS.SET_MODE
                data = SETMODECMD.VFORCE
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_MODE is set to mode {}'.format(data))

                time.sleep(0.000001)

                # Verify that the rail mode got set to VFORCE
                hclc_rail_mode_reg_initial_value = board.ReadRegister(board.registers.HclcRailModeVal).value

                if hclc_rail_mode_reg_initial_value == (hclc_rail_mode_reg_initial_value | (vforce_mode << 2 * rail)):
                    self.Log('info', 'The Hclc Rail Mode register is initially set to correct value 0x{:x}'.format(
                        hclc_rail_mode_reg_initial_value))
                else:
                    self.Log('error',
                             'The Hclc Rail Mode register is initially set to an incorrect value 0x{:x}'.format(
                                 hclc_rail_mode_reg_initial_value))

                # Sending a TQ Notify to bring the rails out of safe state
                board.WriteTQHeaderViaBar2(dutid, rail, 'LC')

                # In default VFORCE mode set OV and UV value greater than 2.5v and lesser than -0.5V respectively
                # Setting an OV value of 3V and making sure that it doesn't get clamped to 2.5V
                command = RAILCOMMANDS.SET_OVER_VOLTAGE
                data = 0x3000
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_OVER_VOLTAGE is set to mode {}'.format(data))

                # Setting an UV value of -1V and making sure that it doesn't get clamped to -0.5V
                command = RAILCOMMANDS.SET_UNDER_VOLTAGE
                data = 0xF000
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_OVER_VOLTAGE is set to mode {}'.format(data))

                # Send a flush command to make sure the values from the cached registers get written to ad5560 device
                command = RAILCOMMANDS.FORCE_CLAMP_FLUSH
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_OVER_VOLTAGE is set to mode {}'.format(data))

                time.sleep(0.1)

                # Reading the X1 values from the AD5560 cpoh and cpol registers
                unclamped_cpol_value = board.ReadAd5560Register(CPL_DAC_X1, rail)
                unclamped_cpoh_value = board.ReadAd5560Register(CPH_DAC_X1, rail)

                if unclamped_cpoh_value == x1_value_for_3_volts:
                    self.Log('info',
                             'The ad5560 cpoh register got updated to correct unclamped value after OV/UV during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpoh_value))
                else:
                    self.Log('error',
                             'The ad5560 cpoh register got updated to an incorrect value after OV/UV during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpoh_value))

                if unclamped_cpol_value == x1_value_for_neg_1_volts:
                    self.Log('info',
                             'The ad5560 cpol register got updated to correct unclamped value after OV/UV during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpol_value))
                else:
                    self.Log('error',
                             'The ad5560 cpol register got updated to an incorrect unclamped value after OV/UV during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpol_value))

                # Send a general communication command to the AD5560 cpol and cpoh registers with X1 values greater than clamp limits of 2.5V and -0.5V respectively
                data = x1_value_for_3_volts
                board.WriteAd5560Register(CPH_DAC_X1, data, rail)

                data = x1_value_for_neg_1_volts
                board.WriteAd5560Register(CPL_DAC_X1, data, rail)

                time.sleep(0.1)

                # Reading the X1 values from the AD5560 cpoh and cpol registers after general comm
                unclamped_cpol_value_after_general_comm = board.ReadAd5560Register(CPL_DAC_X1, rail)
                unclamped_cpoh_value_after_general_comm = board.ReadAd5560Register(CPH_DAC_X1, rail)

                if unclamped_cpoh_value_after_general_comm == x1_value_for_3_volts:
                    self.Log('info',
                             'The ad5560 cpoh register got updated to correct unclamped value after general comm during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpoh_value_after_general_comm))
                else:
                    self.Log('error',
                             'The ad5560 cpoh register got updated to an incorrect value after general comm during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpoh_value_after_general_comm))

                if unclamped_cpol_value_after_general_comm == x1_value_for_neg_1_volts:
                    self.Log('info',
                             'The ad5560 cpol register got updated to correct unclamped value after general comm during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpol_value_after_general_comm))
                else:
                    self.Log('error',
                             'The ad5560 cpol register got updated to an incorrect value after general comm during VFORCE mode 0x{:x}'.format(
                                 unclamped_cpol_value_after_general_comm))

                # Update the rail mode to ISINK to validate LC ISINK feature
                command = RAILCOMMANDS.SET_MODE
                data = SETMODECMD.ISINK
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_MODE is set to mode {}'.format(data))

                time.sleep(0.1)

                # Verify that the rail mode got set to ISINK
                hclc_rail_mode_reg_value_after_isink = board.ReadRegister(board.registers.HclcRailModeVal).value

                if hclc_rail_mode_reg_value_after_isink == (
                    hclc_rail_mode_reg_value_after_isink | (isink_mode << 2 * rail)):
                    self.Log('info',
                             'The Hclc Rail Mode register got updated to correct value during ISINK mode 0x{:x}'.format(
                                 hclc_rail_mode_reg_value_after_isink))
                else:
                    self.Log('error',
                             'The Hclc Rail Mode register got updated to an incorrect value during ISINK mode 0x{:x}'.format(
                                 hclc_rail_mode_reg_value_after_isink))

                # Reading the X1 values from AD5560 cpoh and cpol registers after ISINK mode and making sure that these values are clamped at 2.5V and -0.5V
                clamped_cpol_value_after_isink_mode = board.ReadAd5560Register(CPL_DAC_X1, rail)
                clamped_cpoh_value_after_isink_mode = board.ReadAd5560Register(CPH_DAC_X1, rail)

                if clamped_cpoh_value_after_isink_mode == x1_value_for_2p5_volts:
                    self.Log('info',
                             'The ad5560 cpoh register got updated to correct clamped value during ISINK mode 0x{:x}'.format(
                                 clamped_cpoh_value_after_isink_mode))
                else:
                    self.Log('error',
                             'The ad5560 cpoh register got updated to an incorrect value during INSINK mode  0x{:x}'.format(
                                 clamped_cpoh_value_after_isink_mode))

                if clamped_cpol_value_after_isink_mode == x1_value_for_neg_0p5_volts:
                    self.Log('info',
                             'The ad5560 cpol register got updated to correct clamped value during ISINK mode 0x{:x}'.format(
                                 clamped_cpol_value_after_isink_mode))
                else:
                    self.Log('error',
                             'The ad5560 cpol register got updated to an incorrect value during ISINK mode 0x{:x}'.format(
                                 clamped_cpol_value_after_isink_mode))

                # Setting an OV value of 3V and making sure that it gets clamped to 2.5V
                command = RAILCOMMANDS.SET_OVER_VOLTAGE
                data = 0x3000
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_OVER_VOLTAGE is set to mode {}'.format(data))

                # Setting an UV value of -1V and making sure that it doesn't get clamped to -0.5V
                command = RAILCOMMANDS.SET_UNDER_VOLTAGE
                data = 0xF000
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_OVER_VOLTAGE is set to mode {}'.format(data))

                # Send a flush command to make sure the values from the cached registers get written to ad5560 device
                command = RAILCOMMANDS.FORCE_CLAMP_FLUSH
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_OVER_VOLTAGE is set to mode {}'.format(data))

                time.sleep(0.1)

                # Reading the X1 values from the AD5560 cpoh and cpol registers
                clamped_cpol_value = board.ReadAd5560Register(CPL_DAC_X1, rail)
                clamped_cpoh_value = board.ReadAd5560Register(CPH_DAC_X1, rail)

                if clamped_cpoh_value == x1_value_for_2p5_volts:
                    self.Log('info',
                             'The ad5560 cpoh register got updated to correct clamped value after OV/UV during ISINK mode 0x{:x}'.format(
                                 clamped_cpoh_value))
                else:
                    self.Log('error',
                             'The ad5560 cpoh register got updated to an incorrect value after OV/UV during ISINK mode 0x{:x}'.format(
                                 clamped_cpoh_value))

                if clamped_cpol_value == x1_value_for_neg_0p5_volts:
                    self.Log('info',
                             'The ad5560 cpol register got updated to correct clamped value after OV/UV during ISINK mode 0x{:x}'.format(
                                 clamped_cpol_value))
                else:
                    self.Log('error',
                             'The ad5560 cpol register got updated to an incorrect value after OV/UV during ISINK mode 0x{:x}'.format(
                                 clamped_cpol_value))

                # Send a general communication command to the AD5560 cpol and cpoh registers with X1 values greater than clamp limits of 2.5V and -0.5V respectively
                # Making sure that the cpoh and cpol limits are clamped to 2.5V and -0.5V since we are in ISINK mode
                data = x1_value_for_3_volts
                board.WriteAd5560Register(CPH_DAC_X1, data, rail)

                data = x1_value_for_neg_1_volts
                board.WriteAd5560Register(CPL_DAC_X1, data, rail)

                # Reading the X1 values from the AD5560 cpoh and cpol registers after general comm
                clamped_cpol_value_after_general_comm = board.ReadAd5560Register(CPL_DAC_X1, rail)
                clamped_cpoh_value_after_general_comm = board.ReadAd5560Register(CPH_DAC_X1, rail)

                if clamped_cpoh_value_after_general_comm == x1_value_for_2p5_volts:
                    self.Log('info',
                             'The ad5560 cpoh register got updated to correct clamped value after general comm during ISINK mode 0x{:x}'.format(
                                 clamped_cpoh_value_after_general_comm))
                else:
                    self.Log('error',
                             'The ad5560 cpoh register got updated to an incorrect value after general comm during ISINK mode 0x{:x}'.format(
                                 clamped_cpoh_value_after_general_comm))

                if clamped_cpol_value_after_general_comm == x1_value_for_neg_0p5_volts:
                    self.Log('info',
                             'The ad5560 cpol register got updated to correct clamped value after general comm during ISINK mode 0x{:x}'.format(
                                 clamped_cpol_value_after_general_comm))
                else:
                    self.Log('error',
                             'The ad5560 cpol register got updated to an incorrect value after general_comm during ISINK mode 0x{:x}'.format(
                                 clamped_cpol_value_after_general_comm))

                # Send a general communication command to the AD5560 cpol and cpoh registers with X1 values lesser than clamp limits of 2.5V and -0.5V respectively
                # Making sure that the cpoh and cpol limits are updated to appropriate values since we are in ISINK mode
                data = x1_value_for_2_volts
                board.WriteAd5560Register(CPH_DAC_X1, data, rail)

                data = x1_value_for_neg_0p2_volts
                board.WriteAd5560Register(CPL_DAC_X1, data, rail)

                # Reading the X1 values from the AD5560 cpoh and cpol registers after general comm
                unclamped_cpol_value_after_general_comm = board.ReadAd5560Register(CPL_DAC_X1, rail)
                unclamped_cpoh_value_after_general_comm = board.ReadAd5560Register(CPH_DAC_X1, rail)

                if unclamped_cpoh_value_after_general_comm == x1_value_for_2_volts:
                    self.Log('info',
                             'The ad5560 cpoh register got updated to correct unclamped value after general comm during ISINK mode 0x{:x}'.format(
                                 unclamped_cpoh_value_after_general_comm))
                else:
                    self.Log('error',
                             'The ad5560 cpoh register got updated to an incorrect value after general comm during ISINK mode 0x{:x}'.format(
                                 unclamped_cpoh_value_after_general_comm))

                if unclamped_cpol_value_after_general_comm == x1_value_for_neg_0p2_volts:
                    self.Log('info',
                             'The ad5560 cpol register got updated to correct unclamped value after general comm during ISINK mode 0x{:x}'.format(
                                 unclamped_cpol_value_after_general_comm))
                else:
                    self.Log('error',
                             'The ad5560 cpol register got updated to an incorrect value after general_comm during ISINK mode 0x{:x}'.format(
                                 unclamped_cpol_value_after_general_comm))

                # Update the rail mode to VFORCE to disable ISINK feature for a clean exit from the test
                command = RAILCOMMANDS.SET_MODE
                data = SETMODECMD.VFORCE
                board.WriteBar2RailCommand(command, data, rail + 16)
                self.Log('debug', 'RailCommand SET_MODE is set to mode {}'.format(data))

                time.sleep(0.00001)

                hclc_rail_mode_reg_value_after_vforce = board.ReadRegister(board.registers.HclcRailModeVal).value

                if hclc_rail_mode_reg_value_after_vforce == (
                    hclc_rail_mode_reg_value_after_vforce | (vforce_mode << 2 * rail)):
                    self.Log('info',
                             'The Hclc Rail Mode register got updated to VFORCE mode during test exit 0x{:x}'.format(
                                 hclc_rail_mode_reg_value_after_vforce))
                else:
                    self.Log('error',
                             'The Hclc Rail Mode register got updated to an incorrect mode during test exit 0x{:x}'.format(
                                 hclc_rail_mode_reg_value_after_vforce))

                board.ClearHclcRailAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLcRailsScopeShotSamplingModeEventTriggerStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[1]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
                board.UnGangAllRails()

                HeaderBase = 0x00005000
                HeaderSize = 0x00000100
                SampleBase = 0x00010000
                SampleSize = 0x00100000
                board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

                event_mask_alarm_bit = 1
                event_mask_trigger_bit = 1

                trigger_value = 0x0

                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x100000
                count = 5
                # Roll over bit will be low since sample size of 0x100000 is too high for rollover to occur
                rollover = 0
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureHclcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                  metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # ScopeShot Stop Trigger
                scope_shot_stop_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.STOP)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_stop_trigger_value)
                stop_due_to_trigger = 0
                stop_due_to_alarm = 0
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.ReadRegister(board.registers.SShotTriggerFifoPayload).value != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLcRailsScopeShotSamplingModeMatchingTriggerPayloadStopTest(self):
        dutid = 0
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[1]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
                board.UnGangAllRails()

                HeaderBase = 0x00005000
                HeaderSize = 0x00000100
                SampleBase = 0x00010000
                SampleSize = 0x00100000
                board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

                event_mask_alarm_bit = 0
                event_mask_trigger_bit = 1

                trigger_value = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x60
                count = 2
                # Roll over bit will be high since sample region buffer size of 0x60 (96 bytes) is too small to accomodate all the samples
                rollover = 1
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureHclcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                  metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # ScopeShot Stop Trigger
                stop_due_to_trigger = 1
                stop_due_to_alarm = 0
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.ReadRegister(board.registers.SShotTriggerFifoPayload).value != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()
    @unittest.skip('This test currently skipped while LC rails is being audited')
    def DirectedLcRailsScopeShotSamplingModeAlarmStopTest(self):
        dutid = 0
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[1]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('LC{}'.format(rail), 'OHM_10')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
                board.UnGangAllRails()

                HeaderBase = 0x00005000
                HeaderSize = 0x00000100
                SampleBase = 0x00010000
                SampleSize = 0x00100000
                board.InitializeSampleEngine(HeaderBase, HeaderSize, SampleBase, SampleSize, dutid, 'LC')

                event_mask_alarm_bit = 1
                event_mask_trigger_bit = 1

                trigger_value = 0x0
                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x30
                count = 2
                # Roll over bit will be high since sample region buffer size of 0x30 (48 bytes) is too small to accomodate all the samples
                rollover = 1
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureHclcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                  metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # Sending a cbf trigger due to UHC folding alarm which will stop the scope shot sampling engine
                cbf_type_trigger = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (
                CROSSBOARDTRIGGER.DPSCTRLTYPE << 15) | CROSSBOARDTRIGGER.CBFOLD)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, cbf_type_trigger)

                # ScopeShot Stop Trigger
                stop_due_to_trigger = 0
                stop_due_to_alarm = 1
                time.sleep(0.1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.ReadRegister(board.registers.SShotTriggerFifoPayload).value != 0x0):
                    pass

                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    def read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(self, slot):
        # Try reading multiple times from trigger down register to ensure that the fifo is empty
        while (hil.dpsBarRead(slot, 0, 1, 0x54) != 0x0 or hil.dpsBarRead(slot, 1, 1, 0x54) != 0x0):
            pass

    def write_poisoned_lut_entry_for_dummy_triggers(self, slot, subslot, num_triggers, lutdwordaddress, dutid):
        uhc_specific_triggers_lut_address = lutdwordaddress + (dutid * 0x10000)
        board = self.env.instruments[slot].subslots[subslot]
        board_2 = self.env.instruments[slot].subslots[1]

        for trigger in range(num_triggers):
            self.Log('info', 'LUT Dword Address is 0x{:x}'.format(uhc_specific_triggers_lut_address))
            board.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
            board_2.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
            time.sleep(0.000001)
            uhc_specific_triggers_lut_address = uhc_specific_triggers_lut_address + 0x4

    def configure_scope_shot_control_register_and_read_values_back(self, slot, subslot, dutid, event_mask_alarm_bit,
                                                                   event_mask_trigger_bit):
        board = self.env.instruments[slot].subslots[subslot]
        SShotControlRegData = board.ReadRegister(board.registers.SShotControl)
        SShotControlRegData.SShotUhcSelect = dutid
        SShotControlRegData.SShotEventMaskAlarm = event_mask_alarm_bit
        SShotControlRegData.SShotEventMaskTrigger = event_mask_trigger_bit
        SShotControlRegData.SShotEnable = 1
        board.Write('SShotControl', SShotControlRegData)

        time.sleep(0.000001)

        SShotControlRegData = board.ReadRegister(board.registers.SShotControl)

        # Masking the sshot reason bits (8-15) as they are don't care at this point
        SShotControlRegValue = (SShotControlRegData.value & 0xFFFF00FF)

        if SShotControlRegValue == (dutid << 28 | 1 << 24 | event_mask_alarm_bit << 1 | event_mask_trigger_bit):
            self.Log('info',
                     'Scope Shot Ctrl Register is set to a correct value of 0x{:x}'.format(SShotControlRegData.value))
        else:
            self.Log('error', 'Scope Shot Ctrl Register is set to an incorrect value of 0x{:x}'.format(
                SShotControlRegData.value))

    def write_to_stop_trigger_register_and_read_values_back(self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]
        SShotStopTriggerRegData = board.ReadRegister(board.registers.SShotStopTrigger)
        SShotStopTriggerRegData.Data = trigger_value
        board.Write('SShotStopTrigger', SShotStopTriggerRegData)

        time.sleep(0.000001)

        SShotStopTriggerRegData = board.ReadRegister(board.registers.SShotStopTrigger)
        if SShotStopTriggerRegData.value == trigger_value:
            self.Log('info', 'Scope Shot Stop Trigger Register is set to a correct value of 0x{:x}'.format(
                SShotStopTriggerRegData.value))
        else:
            self.Log('error', 'Scope Shot Stop Trigger Register is set to an incorrect value of 0x{:x}'.format(
                SShotStopTriggerRegData.value))

    def send_trigger_via_RC_and_read_from_hddps(self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]
        expected = trigger_value
        self.env.send_trigger(trigger_value)
        time.sleep(0.010)
        self.Log('debug', 'RC sent trigger 0x{:x}'.format(trigger_value))

        trigger_value = board.ReadRegister(board.registers.TRIGGERDOWNTEST)
        if trigger_value.value != expected:
            self.Log('error', 'HDDPS received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected,
                                                                                                        trigger_value.value))
        else:
            self.Log('info', 'HDDPS({}, {}) received trigger 0x{:x}'.format(slot, subslot, trigger_value.value))

    def read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(self, slot, subslot,
                                                                                           stop_due_to_trigger,
                                                                                           stop_due_to_alarm, rollover):
        board = self.env.instruments[slot].subslots[subslot]
        SShotControlRegData = board.ReadRegister(board.registers.SShotControl)
        if getattr(SShotControlRegData, 'SShotEventSignatureAlarm') == stop_due_to_alarm and getattr(
                SShotControlRegData, 'SShotEventSignatureTrigger') == stop_due_to_trigger:
            self.Log('info',
                     'Scope Shot Ctrl Register is set to a correct value of 0x{:x}'.format(SShotControlRegData.value))
        else:
            self.Log('error', 'Scope Shot Ctrl Register is set to an incorrect value of 0x{:x}'.format(
                SShotControlRegData.value))

        # Verify that the rollover bit is set to 1 since the count value 10 is higher than sample region size of 0x30 which cn accomodate 6 samples
        SShotEndSampleAddress = board.ReadRegister(board.registers.SShotHclcEndSampleAddress)
        if getattr(SShotEndSampleAddress, 'SShotHclcSampleAddrRollover') == rollover:
            self.Log('info', 'Sample Engine End Address register is set to a correct value of {:x}'.format(
                SShotEndSampleAddress.value))
        else:
            self.Log('error', 'Sample Engine End Address register is set to an incorrect value of {:x}'.format(
                SShotEndSampleAddress.value))
        sshottimestampreg = board.ReadRegister(board.registers.SShotHcLcEndTimestamp)
        self.Log('info', 'Scopeshotregister time stamp {:d}'.format(sshottimestampreg.value))

    def read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
            self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]

        SShotTriggerFifoTimeStampRegister = board.ReadRegister(board.registers.SShotTriggerFifoTimeStamp)
        self.Log('info', 'The scope shot trigger fifo timestamp register is updated with a value of 0x{:x}'.format(
            SShotTriggerFifoTimeStampRegister.value))

        SShotTriggerFifoPayloadRegister = board.ReadRegister(board.registers.SShotTriggerFifoPayload)
        if SShotTriggerFifoPayloadRegister.value == trigger_value:
            self.Log('info',
                     'The scope shot trigger fifo payload register is updated with correct trigger payload value of 0x{:x}'.format(
                         SShotTriggerFifoPayloadRegister.value))
        else:
            self.Log('error',
                     'The scope shot trigger fifo payload register received an incorrect trigger expected=0x{:x}, actual=0x{:x}'.format(
                         trigger_value, SShotTriggerFifoPayloadRegister.value))
