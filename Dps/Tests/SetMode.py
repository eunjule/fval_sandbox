################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: VMeasure Mode
# -------------------------------------------------------------------------------
#     Purpose: To build tests for V Measure Mode
# -------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 01/04/18
#       Group: HDMT FPGA Validation
###############################################################################

import random
import time
import unittest

from Common import configs
from Common.instruments.dps.symbols import RAILCOMMANDS
from Common.instruments.dps.symbols import SETMODECMD
from Dps.Tests.dpsTest import BaseTest

AD5560_VMMODE_DEFAULT = 0x8000
HCLC_RAIL_BASE = 16
DISABLE_RAIL = 0
SET_GANGIMODE_BIT = 0x0400
RESET_SLAVE_BIT = 0xFDFF
RESET_SLAVE_GANGIMODE_BITS = 0xF9FF
SYSTEM_FORCE_SENSE_BIT = 15
GANGIMODE_BIT = 9
GANGI_MODE_BIT_AFTER_SETMODE = 0x0001
SLAVE_BIT = 10
SLAVE_BIT_AFTER_VF_MODE = 0x0000
SLAVE_BIT_AFTER_IF_IS_MODE = 0x0001
SYSTEM_FORCE_SENSE_BIT_AFTER_VMMODE = 0x0000
LSB_BIT_SET = 0x0001
VMODE_REGISTER_STATUS =  0x0003
VFMODE_REGISTER_STATUS =  0x0000
IFODE_REGISTER_STATUS =  0x0001
ISMODE_REGISTER_STATUS =  0x0002
MASK_LAST_TWO_BITS = 0x0003
HCLC_RAIL_COUNT = 10
VLC_RAIL_COUNT = 16
DUT_ID_COUNT = 8


class OperationModes(BaseTest):

    def RandomRailHCAd5560AndRailModeValRegisterImpactTest(self):
        resource_id = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([resource_id]):
            self.skip_test_if_fpga_does_not_support_vm_mode(board)
            board.SetRailsToSafeState()
            for test_iteration in range(5):
                rail_uhc_tuple_list = self.generate_random_rail_dutids(resource_id)
                for rail, uhc in rail_uhc_tuple_list:
                    board.EnableOnlyOneUhc(uhc)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,resource_id)
                    self.run_bar_commands(rail, uhc, board, resource_id)

    def RandomRailLCAd5560AndRailModeValRegisterImpactTest(self):
        resource_id = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([resource_id]):
            self.skip_test_if_fpga_does_not_support_vm_mode(board)
            board.SetRailsToSafeState()
            for test_iteration in range(5):
                rail_uhc_tuple_list = self.generate_random_rail_dutids(resource_id)
                for rail, uhc in rail_uhc_tuple_list:
                    board.EnableOnlyOneUhc(uhc)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,resource_id)
                    self.run_bar_commands(rail, uhc, board, resource_id)

    def RandomRailVLCRailModeValRegisterImpactTest(self):
        resource_id = 'VLC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([resource_id]):
            self.skip_test_if_fpga_does_not_support_vm_mode(board)
            board.SetRailsToSafeState()
            for test_iteration in range(5):
                rail_uhc_tuple_list = self.generate_random_rail_dutids(resource_id)
                for rail, uhc in rail_uhc_tuple_list:
                    board.EnableOnlyOneUhc(uhc)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,resource_id)
                    self.run_bar_commands(rail, uhc, board, resource_id)

    def skip_test_if_fpga_does_not_support_vm_mode(self, dut):
        if dut.LegacyTcLooping:
            reason = 'This FPGA: {} does not support V Measure Mode'.format(hex(dut.GetFpgaVersion()))
            self.Log('warning', reason)
            raise unittest.SkipTest(reason)

    def generate_random_rail_dutids(self, resource_id):
        rail_dutid_tuple_list = []
        if resource_id == 'VLC':
            rail_count = VLC_RAIL_COUNT
        else:
            rail_count = HCLC_RAIL_COUNT
        for rail in range(rail_count):
            for dutid in range(DUT_ID_COUNT):
                rail_dutid_tuple_list.append((rail, dutid))
        random.shuffle(rail_dutid_tuple_list)
        return rail_dutid_tuple_list

    def run_bar_commands(self, rail, dutId, board, resource_id):
        board.WriteTQHeaderViaBar2(dutId, rail, resource_id)
        if resource_id == 'VLC':
            rail_to_test = rail
        else:
            rail_to_test = rail + HCLC_RAIL_BASE
        self.Log('debug', 'Slot:{}{}Rail:{}dut:{}'.format(board.subslot, resource_id, rail_to_test, dutId))
        board.WriteBar2RailCommand(RAILCOMMANDS.ENABLE_DISABLE_RAIL, DISABLE_RAIL, rail_to_test)
        random_set_mode_method_list = self.randomize_set_mode_bar_calls()
        self.call_bartwo_methods(board, rail, random_set_mode_method_list, resource_id)
        board.WriteTQFooterViaBar2(dutId, rail, resource_id)
        board.CheckTqNotifyAlarm(dutId, resource_id)

    def randomize_set_mode_bar_calls(self):
        set_mode_method_list = [self.verify_set_vmmode,
                                self.verify_set_vfmode,
                                self.verify_set_ifmode,
                                self.verify_set_ismode]
        random.shuffle(set_mode_method_list)
        return set_mode_method_list

    def call_bartwo_methods(self, board, rail, set_mode_method_list, resource_id):
        for method_list in set_mode_method_list:
            method_list(board, rail, resource_id)

    def verify_set_vmmode(self, board, rail, resource_id):
        if resource_id == 'VLC':
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.VMEASURE, rail)
        else:
            self.invert_vm_mode_bits_in_ad5560(board, rail)
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.VMEASURE, rail + HCLC_RAIL_BASE)
            self.verify_ad5560_for_vm_mode(board, rail)
        self.verify_vm_mode_railmodeval_register(board, rail, resource_id)

    def verify_vm_mode_railmodeval_register(self, board, rail, resource_id):
        if resource_id == 'VLC':
            railmodeval_register_type = board.getVlcRailModeValRegisterType()
        else:
            railmodeval_register_type = board.getHclcRailModeValRegisterType()
        railmodeval_register = board.ReadRegister(railmodeval_register_type)
        self.Log('debug', 'Register status after VM Mode:{}'.format(hex(railmodeval_register.value)))
        rail_mode_bits_to_lsb = railmodeval_register.value >> (rail * 2) & MASK_LAST_TWO_BITS
        if rail_mode_bits_to_lsb != VMODE_REGISTER_STATUS:
            self.Log('error', 'Rail Mode Val bits in FPGA failed to get expected value.Expected:{} Actual:{}'
                     .format(VMODE_REGISTER_STATUS, rail_mode_bits_to_lsb))

    def verify_ad5560_for_vm_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_after_vm_mode = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status after VM Mode: {} '.format(hex(ad5560_reg_after_vm_mode)))
        system_force_sense_bit = ad5560_reg_after_vm_mode >> SYSTEM_FORCE_SENSE_BIT & LSB_BIT_SET
        if system_force_sense_bit != SYSTEM_FORCE_SENSE_BIT_AFTER_VMMODE:
            self.Log('error', 'System force sense bit in AD5560 failed to get reset after VMeasure mode Status:{}'
                     .format(hex(ad5560_reg_after_vm_mode)))

    def invert_vm_mode_bits_in_ad5560(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_default = board.ReadAd5560Register(ad5560_reg_address, rail)
        ad5560_reg_default = ad5560_reg_default | AD5560_VMMODE_DEFAULT
        board.WriteAd5560Register(ad5560_reg_address, ad5560_reg_default, rail)
        ad5560_reg_default_verify = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status before VM MODE: {}'.format(hex(ad5560_reg_default_verify)))

    def verify_set_vfmode(self, board, rail, resource_id):
        if resource_id == 'VLC':
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.VFORCE, rail)
        else:
            self.default_ad5560_for_vf_mode(board, rail)
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.VFORCE, rail + HCLC_RAIL_BASE)
            self.verify_ad5560_for_vf_mode(board, rail)
        self.verify_vf_mode_railmodeval_register(board, rail, resource_id)

    def verify_vf_mode_railmodeval_register(self, board, rail, resource_id):
        if resource_id == 'VLC':
            railmodeval_register_type = board.getVlcRailModeValRegisterType()
        else:
            railmodeval_register_type = board.getHclcRailModeValRegisterType()
        railmodeval_register = board.ReadRegister(railmodeval_register_type)
        self.Log('debug', 'Register status after VFMode:{}'.format(hex(railmodeval_register.value)))
        rail_mode_bits_to_lsb = railmodeval_register.value >> (rail * 2) & MASK_LAST_TWO_BITS
        if rail_mode_bits_to_lsb != VFMODE_REGISTER_STATUS:
            self.Log('error', 'Rail Mode Val bits in FPGA failed to get expected value.Expected:{} Actual:{}'
                     .format(VFMODE_REGISTER_STATUS, rail_mode_bits_to_lsb))

    def verify_ad5560_for_vf_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_after_vf_mode = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status after VF Mode: {}'.format(hex(ad5560_reg_after_vf_mode)))
        gangi_mode_bit = ad5560_reg_after_vf_mode >> GANGIMODE_BIT & LSB_BIT_SET
        if gangi_mode_bit != GANGI_MODE_BIT_AFTER_SETMODE:
            self.Log('error', 'GANGI Mode bit in AD5560 failed to get set after VForce mode')
        slave_bit_check = ad5560_reg_after_vf_mode >> SLAVE_BIT & LSB_BIT_SET
        if slave_bit_check != SLAVE_BIT_AFTER_VF_MODE:
            self.Log('error', 'Slave bit in AD5560 failed to get reset after VForce mode')

    def default_ad5560_for_vf_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_default = board.ReadAd5560Register(ad5560_reg_address, rail)
        ad5560_reg_default = ad5560_reg_default | SET_GANGIMODE_BIT
        ad5560_reg_default = ad5560_reg_default & RESET_SLAVE_BIT
        board.WriteAd5560Register(ad5560_reg_address, ad5560_reg_default, rail)
        ad5560_reg_default_verify = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status before VF Mode: {}'.format(hex(ad5560_reg_default_verify)))

    def verify_set_ifmode(self, board, rail, resource_id):
        if resource_id == 'VLC':
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.IFORCE, rail)
        else:
            self.default_ad5560_for_if_mode(board, rail)
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.IFORCE, rail + HCLC_RAIL_BASE)
            self.verify_ad5560_for_if_mode(board, rail)
        self.verify_if_mode_railmodeval_register(board, rail, resource_id)

    def verify_if_mode_railmodeval_register(self, board, rail, resource_id):
        if resource_id == 'VLC':
            railmodeval_register_type = board.getVlcRailModeValRegisterType()
        else:
            railmodeval_register_type = board.getHclcRailModeValRegisterType()
        railmodeval_register = board.ReadRegister(railmodeval_register_type)
        self.Log('debug', 'Register status after IFMode:{}'.format(hex(railmodeval_register.value)))
        rail_mode_bits_to_lsb = railmodeval_register.value >> (rail * 2) & MASK_LAST_TWO_BITS
        if rail_mode_bits_to_lsb != IFODE_REGISTER_STATUS:
            self.Log('error', 'Rail Mode Val bits in FPGA failed to get expected value.Expected:{} Actual:{}'
                     .format(IFODE_REGISTER_STATUS, rail_mode_bits_to_lsb))

    def verify_ad5560_for_if_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_after_if_mode = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status after IF Mode:{}'.format(hex(ad5560_reg_after_if_mode)))
        gangi_mode_bit = ad5560_reg_after_if_mode >> GANGIMODE_BIT & LSB_BIT_SET
        if gangi_mode_bit != GANGI_MODE_BIT_AFTER_SETMODE:
            self.Log('error', 'GANGI Mode bit in AD5560 failed to get set after IForce mode')
        slave_bit = ad5560_reg_after_if_mode >> SLAVE_BIT & LSB_BIT_SET
        if slave_bit != SLAVE_BIT_AFTER_IF_IS_MODE:
            self.Log('error', 'Slave bit in AD5560 failed to get reset after IForce mode')

    def default_ad5560_for_if_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_default = board.ReadAd5560Register(ad5560_reg_address, rail)
        ad5560_reg_default = ad5560_reg_default & RESET_SLAVE_GANGIMODE_BITS
        board.WriteAd5560Register(ad5560_reg_address, ad5560_reg_default, rail)
        ad5560_reg_default_verify = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status before start:{}'.format(hex(ad5560_reg_default_verify)))
        return ad5560_reg_address

    def verify_set_ismode(self, board, rail, resource_id):
        if resource_id == 'VLC':
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.ISINK, rail)
        else:
            self.default_ad5560_for_is_mode(board, rail)
            self.send_bar_command_and_wait(board, RAILCOMMANDS.SET_MODE, SETMODECMD.ISINK, rail + HCLC_RAIL_BASE)
            self.verify_ad5560_for_is_mode(board, rail)

        self.verify_is_mode_railmodeval_register(board, rail, resource_id)

    def verify_is_mode_railmodeval_register(self, board, rail, resource_id):
        if resource_id == 'VLC':
            railmodeval_register_type = board.getVlcRailModeValRegisterType()
        else:
            railmodeval_register_type = board.getHclcRailModeValRegisterType()
        railmodeval_register = board.ReadRegister(railmodeval_register_type)
        self.Log('debug', 'Register status after ISMode: {}'.format(hex(railmodeval_register.value)))
        rail_mode_bits_to_lsb = railmodeval_register.value >> (rail * 2) & MASK_LAST_TWO_BITS
        if rail_mode_bits_to_lsb != ISMODE_REGISTER_STATUS:
            self.Log('error', 'Rail Mode Val bits in FPGA failed to get expected value.Expected:{} Actual:{}'
                     .format(ISMODE_REGISTER_STATUS, rail_mode_bits_to_lsb))

    def verify_ad5560_for_is_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_after_is_mode = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status after IS Mode:{} '.format(hex(ad5560_reg_after_is_mode)))
        gangi_mode_bit = ad5560_reg_after_is_mode >> GANGIMODE_BIT & LSB_BIT_SET
        if gangi_mode_bit != GANGI_MODE_BIT_AFTER_SETMODE:
            self.Log('error', 'GANGI Mode bit in AD5560 failed to get set after ISINK mode')
        slave_bit = ad5560_reg_after_is_mode >> SLAVE_BIT & LSB_BIT_SET
        if slave_bit != SLAVE_BIT_AFTER_IF_IS_MODE:
            self.Log('error', 'Slave bit in AD5560 failed to get reset after ISINK mode')

    def default_ad5560_for_is_mode(self, board, rail):
        ad5560_reg_address = board.ad5560regs.DPS_REGISTER2.ADDR
        ad5560_reg_default = board.ReadAd5560Register(ad5560_reg_address, rail)
        ad5560_reg_default = ad5560_reg_default & RESET_SLAVE_GANGIMODE_BITS
        board.WriteAd5560Register(ad5560_reg_address, ad5560_reg_default, rail)
        ad5560_reg_default_verify = board.ReadAd5560Register(ad5560_reg_address, rail)
        self.Log('debug', 'AD5560 status before IS Mode:{}'.format(hex(ad5560_reg_default_verify)))

    def send_bar_command_and_wait(self, board, set_mode_command, mode, rail):
        board.WriteBar2RailCommand(set_mode_command, mode, rail)
        time.sleep(0.01)