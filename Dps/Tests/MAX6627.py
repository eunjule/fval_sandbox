################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import time
from Common.instruments.dps import hvdps_registers as hvdps_regs
from Dps.Tests.dpsTest import BaseTest
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common import hilmon as hil

ALARM_SAMPLING_RATE = 320

class MAX6627TemperatureInterfacetTestsForHvdps(BaseTest):

    def RandomRailForMAX6627ThermalADCTemperatureReadTest(self,read_attempts = 500):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            number_of_rails = 8
            for i in range(0,read_attempts):
                max6627_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max6627_register = max6627_register_type()
                nums = [x for x in range(number_of_rails)]
                random.shuffle(nums)
                for device_id in nums:
                    device_id_name = 'THERMALADC_DEVICE_ID' + str(device_id)
                    max6627_register.MAX6627DeviceSelectField = getattr(max6627_register ,device_id_name)
                    dut.WriteRegister(max6627_register)
                    result = dut.ReadRegister(max6627_register_type)
                    temperature = result.Temperature
                    temperature_in_celcius =dut.MAX6627TemperatureDecimalToCelcius(temperature)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 60):
                        self.Log('debug', 'Temperature of Thermal ADC {} is in a reasonable range, Value {} for iteration {}' .format(device_id,temperature_in_celcius,i))
                    else:
                        self.Log('error', 'Temperature of Thermal ADC {} is not in a reasonable range,  Value {} for iteration {}'.format(device_id,temperature_in_celcius,i))


    
    
    def RandomRailForMAX6627ThermalADCTemperatureReadUsingHilTest(self,read_attempts = 500):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            number_of_rails = 8
            for i in range(0, read_attempts):
                max6627_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max6627_register = max6627_register_type()
                nums = [x for x in range(number_of_rails)]
                random.shuffle(nums)
                for device_id in nums:
                    temperature_in_celcius = dut.MAX6627ThermalAdcTemperatureRead(device_id)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 60):
                        self.Log('debug',
                                 'Temperature of Thermal ADC {} is in a reasonable range, Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))
                    else:
                        self.Log('error',
                                 'Temperature of Thermal ADC {} is not in a reasonable range,  Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))

    def RandomRailForMAX6627BJTSensorTemperatureReadTest(self,read_attempts = 100):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            number_of_rails = 8
            for i in range(0,read_attempts):
                max6627_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max6627_register = max6627_register_type()
                nums = [x for x in range(number_of_rails)]
                random.shuffle(nums)
                for device_id in nums:
                    device_id_name = 'BJTSENSOR_DEVICE_ID' + str(device_id)
                    max6627_register.MAX6627DeviceSelectField = getattr(max6627_register ,device_id_name)
                    dut.WriteRegister(max6627_register)
                    result = dut.ReadRegister(max6627_register_type)
                    temperature = result.Temperature
                    temperature_in_celcius =dut.MAX6627TemperatureDecimalToCelcius(temperature)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 60):
                        self.Log('debug',
                                 'Temperature of BJT {} is in a reasonable range, Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))
                    else:
                        self.Log('error',
                                 'Temperature of BJT {} is not in a reasonable range,  Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))


        
    def RandomRailForMAX6627BJTSensorTemperatureReadUsingHilTest(self,read_attempts = 100):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            number_of_rails = 8
            for i in range(0,read_attempts):
                max6627_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max6627_register = max6627_register_type()
                nums = [x for x in range(number_of_rails)]
                random.shuffle(nums)
                for device_id in nums:
                    temperature_in_celcius = dut.MAX6627BJTTemperatureRead(device_id)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 60):
                        self.Log('debug',
                                 'Temperature of BJT {} is in a reasonable range, Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))
                    else:
                        self.Log('error',
                                 'Temperature of BJT {} is not in a reasonable range,  Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))

                    
                    
    def RandomDeviceForMAX6627VicorTemperatureReadTest(self,read_attempts = 100):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):

            number_of_vicor_devices = 4
            for i in range(0, read_attempts):
                max6627_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max6627_register = max6627_register_type()
                nums = [x for x in range(number_of_vicor_devices)]
                random.shuffle(nums)
                for device_id in nums:
                    device_id_name = 'VICOR_DEVICE_ID' + str(device_id)
                    max6627_register.MAX6627DeviceSelectField = getattr(max6627_register, device_id_name)
                    dut.WriteRegister(max6627_register)
                    result = dut.ReadRegister(max6627_register_type)
                    temperature = result.Temperature
                    temperature_in_celcius = dut.MAX6627TemperatureDecimalToCelcius(temperature)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 60):
                        self.Log('debug',
                                 'Temperature of Vicor {} is in a reasonable range, Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))
                    else:
                        self.Log('error',
                                 'Temperature of Vicor {} is not in a reasonable range,  Value {} for iteration {}'.format(
                                     device_id, temperature_in_celcius, i))


        
        
    def RandomDeviceForMAX6627VicorTemperatureReadUsingHilTest(self,read_attempts = 100):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            number_of_vicor_devices = 4
            for i in range(0, read_attempts):
                max6627_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max6627_register = max6627_register_type()
                nums = [x for x in range(number_of_vicor_devices)]
                random.shuffle(nums)
                for device_id in nums:
                    temperature_in_celcius = dut.MAX6627VicorTemperatureRead(device_id)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 60):
                        self.Log('debug', 'Temperature Of Vicor Device in a reasonable range, Device Id {},Temperature Value {}'.format(device_id,temperature_in_celcius))
                    else:
                        self.Log('error','Temperature Of Vicor Device not in a reasonable range, Device Id {},Temperature Value {}'.format(device_id,temperature_in_celcius))
    
    
    def RandomRailForMAX6627ThermalADCTemperatureStabilityInterfaceTest(self,read_attempts = 100):
        number_of_rails = 8
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            list_of_rails = [x for x in range(number_of_rails)]
            random.shuffle(list_of_rails)
            for device_id in list_of_rails:
                temperature_in_celcius = []
                max_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max_register = max_register_type()
                device_id_name = 'THERMALADC_DEVICE_ID' + str(device_id)
                max_register.MAX6627DeviceSelectField = getattr(max_register, device_id_name)
                dut.WriteRegister(max_register)
                for i in range(read_attempts):
                    result = dut.ReadRegister(max_register_type)
                    temperature = result.Temperature
                    temperature_in_celcius += [dut.MAX6627TemperatureDecimalToCelcius(temperature)]
                max_temperature = max(temperature_in_celcius)
                min_temperature  = min(temperature_in_celcius)
                deviation_in_temperature = max_temperature - min_temperature
                if deviation_in_temperature <= 10:
                    self.Log('debug', 'Temperature of Thermal ADC device is stable, Device Name: {}'.format(device_id_name))
                else:
                    self.Log('error', 'Temperature of Thermal ADC device is unstable, Device Name:{} '.format(device_id_name))



    def RandomRailForMAX6627BJTSensorTemperatureStabilityInterfaceTest(self,read_attempts = 100):
        number_of_rails = 8
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            list_of_rails = [x for x in range(number_of_rails)]
            random.shuffle(list_of_rails)
            for device_id in list_of_rails:
                temperature_in_celcius = []
                max_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max_register = max_register_type()
                device_id_name = 'BJTSENSOR_DEVICE_ID' + str(device_id)
                max_register.MAX6627DeviceSelectField = getattr(max_register, device_id_name)
                dut.WriteRegister(max_register)
                for i in range(read_attempts):
                    result = dut.ReadRegister(max_register_type)
                    temperature = result.Temperature
                    temperature_in_celcius += [dut.MAX6627TemperatureDecimalToCelcius(temperature)]
                max_temperature = max(temperature_in_celcius)
                min_temperature  = min(temperature_in_celcius)
                deviation_in_temperature = max_temperature - min_temperature
                if deviation_in_temperature <= 10:
                    self.Log('debug', 'Temperature of BJT Sensor device is stable, Device Name: {}'.format(device_id_name))
                else:
                    self.Log('error', 'Temperature of BJT Sensor device is unstable, Device Name:{} '.format(device_id_name))

    def RandomDeviceForMAX6627VicorTemperatureStabilityInterfaceTest(self,read_attempts = 100):
        number_of_devices = 4
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['HV']):
            list_of_devices = [x for x in range(number_of_devices)]
            random.shuffle(list_of_devices)
            for device_id in list_of_devices:
                temperature_in_celcius = []
                max_register_type = hvdps_regs.MAX6627_TEMPERATURE
                max_register = max_register_type()
                device_id_name = 'VICOR_DEVICE_ID' + str(device_id)
                max_register.MAX6627DeviceSelectField = getattr(max_register, device_id_name)
                dut.WriteRegister(max_register)
                for i in range(read_attempts):
                    result = dut.ReadRegister(max_register_type)
                    temperature = result.Temperature
                    temperature_in_celcius += [dut.MAX6627TemperatureDecimalToCelcius(temperature)]
                max_temperature = max(temperature_in_celcius)
                min_temperature = min(temperature_in_celcius)
                deviation_in_temperature = max_temperature - min_temperature

                if deviation_in_temperature <= 10:
                    self.Log('debug', 'Temperature of Vicor device is stable, Device Name: {}'.format(device_id_name))
                else:
                    self.Log('error',
                             'Temperature of Vicor Sensor device is unstable, Device Name:{} '.format(device_id_name))


class MAX6627Alarms(BaseTest):

    def RandomDeviceForMAX6627BJTSensorHighAlarmTest(self, iteration_count=50):
        number_of_devices = 8
        rail_type = 'HV'
        pass_count = 0
        filter_count = 1
        wait_time_in_ms = (((filter_count+1) * ALARM_SAMPLING_RATE) * 1e-3)
        for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            dut.SetRailsToSafeState()
            dut.ClearDpsAlarms()
            default_filter_count_value = self.get_max6627alarm_filter_count_value(dut)
            self.set_max6627alarm_filter_count_value(dut, filter_count)
            for iteration in range(iteration_count):
                list_of_devices = self.get_shuffled_device_list(number_of_devices)
                for device_id in list_of_devices:
                    dut.Max6627BjtSensorLimitsSet(low_temperature=10,high_temperature=80)
                    global_alarms = dut.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error','Received global alarm(s){} on device id{}'.format(global_alarms, device_id))
                    dut.Max6627BjtSensorLimitsSet(low_temperature=10,high_temperature=15)
                    actual_no_alarm_status_list=self.get_third_level_alarm_for_all_hclcrails(dut, rail_type)
                    dut.WaitTime(wait_time_in_ms)
                    if self.verify_no_alarm_received(actual_no_alarm_status_list)== False:
                        self.Log('error','MAX6627BJTHighAlarm Alarm received too soon compared to filter wait time')
                    else:
                        if self.verify_alarms(dut, 'MAX6627BJTHighAlarm', rail_type):
                            pass_count += 1
                    dut.ResetThermalAlarmLimitForMAX6627()
                    dut.ClearHclcRailAlarms()
            if pass_count == (iteration_count * number_of_devices):
                self.Log('debug','High Temperature Alarm generated in MAX6627 BJT Sensor on all {} iterations'.format(pass_count))
            else:
                self.Log('error','High Temperature Alarm generated in MAX6627 BJT Sensor for {} iterations out of {} iterations'
                         .format(pass_count,(iteration_count * number_of_devices)))
            self.set_max6627alarm_filter_count_value(dut, default_filter_count_value)
            dut.ClearDpsAlarms()
            dut.ResetThermalAlarmLimitForMAX6627()
            dut.SetRailsToSafeState()

    def get_third_level_alarm_for_all_hclcrails(self, dut, rail_type):
        alarm_status_list =[]
        for rail in range(dut.RAIL_COUNT[rail_type]):
            hclc_rail_alarm = dut.ReadRegister(dut.registers.HCLC_PER_RAIL_ALARMS, index=rail).value
            alarm_status_list.append(hclc_rail_alarm)
        return alarm_status_list


    def verify_no_alarm_received(self,alarm_list):
        expected_no_alarm_list = [0, 0, 0, 0, 0, 0, 0, 0]
        if alarm_list != expected_no_alarm_list:
            return False
        return True

    def verify_alarms(self, dut, expected_alarm, rail_type):
        global_alarms = dut.get_global_alarms()
        if global_alarms == ['HclcRailAlarms']:
            hclc_per_rail_alarms_status = dut.ReadRegister(dut.registers.HCLC_ALARMS)
            for rail in range(dut.RAIL_COUNT[rail_type]):
                per_rail_alarm_status = getattr(hclc_per_rail_alarms_status, 'Hclc{}Alarms'.format(rail))
                if per_rail_alarm_status != 1:
                    self.Log('error','Level 2 : HCLC Per Rail Alarm Status not set for rail {}'.format(rail))
                    return False
                hclc_rail_alarm = dut.ReadRegister(dut.registers.HCLC_PER_RAIL_ALARMS, index=rail)
                rail_alarms = self.get_non_zero_fields(hclc_rail_alarm)
                if rail_alarms != [expected_alarm]:
                    self.Log('error','Level 3:Unexpected alarms received :HCLC Rail Alarms Register: {} on rail {}.'.format(rail_alarms, rail))
                    return False
        else:
            self.Log('error','Unexpected global alarms received: {}'.format(global_alarms))
            return False
        return True


    def get_non_zero_fields(self, register):
        non_zero_fields = []
        for field in register.fields():
            if getattr(register, field) == 1:
                non_zero_fields.append(field)
        return non_zero_fields


    def RandomDeviceForMAX6627BJTSensorLowAlarmTest(self, iteration_count=50):
        number_of_devices = 8
        rail_type = 'HV'
        pass_count = 0
        filter_count = 1
        wait_time_in_ms = (((filter_count+1.5) * ALARM_SAMPLING_RATE) * 1e-3)
        for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            dut.SetRailsToSafeState()
            dut.ClearDpsAlarms()
            default_filter_count_value= self.get_max6627alarm_filter_count_value(dut)
            self.set_max6627alarm_filter_count_value(dut, filter_count)
            for iteration in range(iteration_count):
                list_of_devices = self.get_shuffled_device_list(number_of_devices)
                for device_id in list_of_devices:
                    dut.Max6627BjtSensorLimitsSet(low_temperature=10, high_temperature=80)
                    global_alarms = dut.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error','Received global alarm(s){} on device id{}'.format(global_alarms, device_id))
                    dut.Max6627BjtSensorLimitsSet(low_temperature=60, high_temperature=80)
                    actual_no_alarm_status_list = self.get_third_level_alarm_for_all_hclcrails(dut, rail_type)
                    dut.WaitTime(wait_time_in_ms)
                    if self.verify_no_alarm_received(actual_no_alarm_status_list)== False:
                        self.Log('error','MAX6627BJTLowAlarm Alarm received too soon compared to filter wait time')
                    else:
                        if self.verify_alarms(dut,'MAX6627BJTLowAlarm',rail_type):
                            pass_count += 1
                    dut.ResetThermalAlarmLimitForMAX6627()
                    dut.ClearHclcRailAlarms()
            if pass_count == (iteration_count * number_of_devices):
                self.Log('debug','Low Temperature Alarm generated in MAX6627 BJT Sensor on all {} iterations'.format(pass_count))
            else:
                self.Log('error','Low Temperature Alarm generated in MAX6627 BJT Sensor for {} iterations out of {} iterations'
                         .format(pass_count,(iteration_count * number_of_devices )))
            self.set_max6627alarm_filter_count_value(dut, default_filter_count_value)
            dut.ClearDpsAlarms()
            dut.ResetThermalAlarmLimitForMAX6627()
            dut.SetRailsToSafeState()

    def set_max6627alarm_filter_count_value(self, dut, filter_count):
        dut.WriteRegister(dut.registers.MAX6627_ALARM_FILTER_COUNT(value=filter_count))

    def get_max6627alarm_filter_count_value(self, dut):
        return dut.ReadRegister(dut.registers.MAX6627_ALARM_FILTER_COUNT).value

    def get_shuffled_device_list(self, number_of_devices):
        list_of_devices = list(range(number_of_devices))
        random.shuffle(list_of_devices)
        return list_of_devices

    def RandomDeviceForMAX6627ThermalADCLowAlarmTest(self, iteration_count=50):
        number_of_devices = 8
        rail_type = 'HV'
        pass_count = 0
        filter_count = 1
        wait_time_in_ms = (((filter_count+1) * ALARM_SAMPLING_RATE) * 1e-3)
        for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            dut.SetRailsToSafeState()
            dut.ClearDpsAlarms()
            default_filter_count_value = self.get_max6627alarm_filter_count_value(dut)
            self.set_max6627alarm_filter_count_value(dut, filter_count)
            for iteration in range(iteration_count):
                list_of_devices = self.get_shuffled_device_list(number_of_devices)
                for device_id in list_of_devices:
                    dut.MAX6627AdcLimitsSet(low_temperature=-50, high_temperature=160)
                    global_alarms = dut.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error', 'Received global alarm(s){} on device id{}'.format(global_alarms, device_id))
                    dut.MAX6627AdcLimitsSet(low_temperature=150, high_temperature=160)
                    actual_no_alarm_status_list = self.get_third_level_alarm_for_all_hclcrails(dut, rail_type)
                    dut.WaitTime(wait_time_in_ms)
                    if self.verify_no_alarm_received(actual_no_alarm_status_list)== False:
                        self.Log('error','MAX6627ThermalADCLowAlarm Alarm received too soon compared to filter wait time')
                    else:
                        if self.verify_alarms(dut, 'MAX6627ThermalADCLowAlarm', rail_type):
                            pass_count += 1
                    dut.ResetThermalAlarmLimitForMAX6627()
                    dut.ClearHclcRailAlarms()
            if pass_count == (iteration_count * number_of_devices):
                self.Log('debug','Low Temperature Alarm generated in MAX6627 Thermal ADC on all {} iterations'.format(
                    pass_count).format(pass_count))
            else:
                self.Log('error','Low Temperature Alarm generated in MAX6627 Thermal ADC for {} iterations out of {} iterations'
                         .format(pass_count, (iteration_count * number_of_devices)))
            self.set_max6627alarm_filter_count_value(dut, default_filter_count_value)
            dut.ClearDpsAlarms()
            dut.ResetThermalAlarmLimitForMAX6627()
            dut.SetRailsToSafeState()

    def RandomDeviceForMAX6627ThermalADCHighAlarmTest(self, iteration_count=50):
        number_of_devices = 8
        rail_type = 'HV'
        pass_count = 0
        filter_count = 1
        wait_time_in_ms = (((filter_count+1) * ALARM_SAMPLING_RATE) * 1e-3)
        for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            dut.SetRailsToSafeState()
            dut.ClearDpsAlarms()
            default_filter_count_value = self.get_max6627alarm_filter_count_value(dut)
            self.set_max6627alarm_filter_count_value(dut, filter_count)
            for iteration in range(iteration_count):
                list_of_devices = self.get_shuffled_device_list(number_of_devices)
                for device_id in list_of_devices:
                    dut.MAX6627AdcLimitsSet(low_temperature=-80, high_temperature=160)
                    global_alarms = dut.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error','Received global alarm(s){} on device id{}'.format(global_alarms, device_id))
                    dut.MAX6627AdcLimitsSet(low_temperature=-80, high_temperature=-50)
                    actual_no_alarm_status_list = self.get_third_level_alarm_for_all_hclcrails(dut, rail_type)
                    dut.WaitTime(wait_time_in_ms)
                    if self.verify_no_alarm_received(actual_no_alarm_status_list)== False:
                        self.Log('error','MAX6627ThermalADCHighAlarm Alarm received too soon compared to filter wait time')
                    else:
                        if self.verify_alarms(dut, 'MAX6627ThermalADCHighAlarm', rail_type):
                            pass_count += 1
                    dut.ResetThermalAlarmLimitForMAX6627()
                    dut.ClearHclcRailAlarms()
            if pass_count == (iteration_count * number_of_devices):
                self.Log('debug','High Temperature Alarm generated in MAX6627 Thermal ADC on all {} iterations'.format(
                    pass_count).format(pass_count))
            else:
                self.Log('error','High Temperature Alarm generated in MAX6627 Thermal ADC for {} iterations out of {} iterations'
                         .format(pass_count, (iteration_count * number_of_devices)))
            self.set_max6627alarm_filter_count_value(dut, default_filter_count_value)
            dut.ClearDpsAlarms()
            dut.ResetThermalAlarmLimitForMAX6627()
            dut.SetRailsToSafeState()

    def RandomDeviceForMAX6627VicorHighAlarmTest(self, iteration_count=50):
        number_of_devices = 4
        rail_type = 'HV'
        pass_count = 0
        expected_alarms = ['Vicor0HighTemperatureAlarm', 'Vicor1HighTemperatureAlarm', 'Vicor2HighTemperatureAlarm',
                           'Vicor3HighTemperatureAlarm']
        filter_count = 1
        wait_time_in_ms = (((filter_count+1) * ALARM_SAMPLING_RATE) * 1e-3)
        for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            dut.SetRailsToSafeState()
            dut.ClearDpsAlarms()
            default_filter_count_value = self.get_max6627alarm_filter_count_value(dut)
            self.set_max6627alarm_filter_count_value(dut, filter_count)
            for iteration in range(iteration_count):
                list_of_devices = self.get_shuffled_device_list(number_of_devices)
                for device_id in list_of_devices:
                    dut.Max6627VicorLimitsSet(low_temperature=10, high_temperature=80)
                    global_alarms = dut.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error', 'Received global alarm(s){} on device id{}'.format(global_alarms, device_id))
                    dut.Max6627VicorLimitsSet(low_temperature=10, high_temperature=15)
                    vicor_alarm_status = dut.ReadRegister(dut.registers.MAX6627_VICOR_ALARMS).value
                    dut.WaitTime(wait_time_in_ms)
                    if vicor_alarm_status !=0:
                        self.Log('error','VicorHighTemperatureAlarm Alarm received too soon compared to filter wait time. Vicor alarm status{}'.format(vicor_alarm_status))
                    else:
                        if self.verify_vicor_alarms(dut, expected_alarms):
                            pass_count += 1
                    dut.ResetThermalAlarmLimitForMAX6627()
                    dut.clearMAX6627VicorAlarmRegister()
            if pass_count == (iteration_count * number_of_devices):
                self.Log('debug','High Temperature Alarm generated in MAX6627 Vicor on all {} iterations'.format(
                    pass_count).format(pass_count))
            else:
                self.Log('error','High Temperature Alarm generated in MAX6627 Vicor for {} iterations out of {} iterations'
                         .format(pass_count, (iteration_count * number_of_devices)))
            self.set_max6627alarm_filter_count_value(dut, default_filter_count_value)
            dut.ClearDpsAlarms()
            dut.ResetThermalAlarmLimitForMAX6627()
            dut.SetRailsToSafeState()

    def verify_vicor_alarms(self, dut, expected_alarms):
        global_alarms = dut.get_global_alarms()
        if global_alarms == ['Max6627VicorTempAlarm']:
            vicor_alarm_register = dut.ReadRegister(dut.registers.MAX6627_VICOR_ALARMS)
            vicor_alarms = self.get_non_zero_fields(vicor_alarm_register)
            if vicor_alarms != expected_alarms:
                self.Log('error','Unexpected alarms received {}'.format(vicor_alarms))
                return False
        else:
            self.Log('error','Unexpected global alarms received: {}'.format(global_alarms))
            return False
        return True

    def RandomDeviceForMAX6627VicorLowAlarmTest(self, iteration_count=10):
        number_of_devices = 4
        rail_type = 'HV'
        pass_count = 0
        expected_alarms = ['Vicor0LowTemperatureAlarm', 'Vicor1LowTemperatureAlarm', 'Vicor2LowTemperatureAlarm',
                           'Vicor3LowTemperatureAlarm']
        filter_count = 1
        wait_time_in_ms = (((filter_count+1) * ALARM_SAMPLING_RATE) * 1e-3)
        for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
            dut.SetRailsToSafeState()
            dut.ClearDpsAlarms()
            default_filter_count_value = self.get_max6627alarm_filter_count_value(dut)
            self.set_max6627alarm_filter_count_value(dut, filter_count)
            for iteration in range(iteration_count):
                list_of_devices = self.get_shuffled_device_list(number_of_devices)
                for device_id in list_of_devices:
                    dut.Max6627VicorLimitsSet(low_temperature=10, high_temperature=80)
                    global_alarms = dut.get_global_alarms()
                    if global_alarms != []:
                        self.Log('error', 'Received global alarm(s){} on device id{}'.format(global_alarms, device_id))
                    dut.Max6627VicorLimitsSet(low_temperature=60, high_temperature=80)
                    vicor_alarm_status = dut.ReadRegister(dut.registers.MAX6627_VICOR_ALARMS).value
                    dut.WaitTime(wait_time_in_ms)
                    if vicor_alarm_status !=0:
                        self.Log('error','VicorLowTemperatureAlarm Alarm received too soon compared to filter wait time.Vicor alarm status{}'.format(vicor_alarm_status))
                    else:
                        if self.verify_vicor_alarms(dut, expected_alarms):
                            pass_count += 1
                    dut.ResetThermalAlarmLimitForMAX6627()
                    dut.clearMAX6627VicorAlarmRegister()
            if pass_count == (iteration_count * number_of_devices):
                self.Log('debug','Low Temperature Alarm generated in MAX6627 Vicor on all {} iterations'.format(
                    pass_count).format(pass_count))
            else:
                self.Log('error','Low Temperature Alarm generated in MAX6627 Vicor for {} iterations out of {} iterations'
                         .format(pass_count, (iteration_count * number_of_devices)))
            self.set_max6627alarm_filter_count_value(dut, default_filter_count_value)
            dut.ClearDpsAlarms()
            dut.ResetThermalAlarmLimitForMAX6627()
            dut.SetRailsToSafeState()