################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
import random

from Dps.Tests.dpsTest import BaseTest
from Common.instruments.dps.symbols import RAILCOMMANDS
from Common.instruments.dps.symbols import HCLCIRANGE
from Common.instruments.dps.symbols import SETMODECMD

class Ad5764InterfaceTests(BaseTest):

    def DirectedAd5764InterfaceCheckViaHilCallTest(self):
        number_of_dac = 3
        number_of_dac_channels = 4
        binary_data_code_for_negative_voltage = 0x7FFF
        binary_data_code_for_zero_voltage = 0x8000
        binary_data_code_for_positive_voltage = 0x8001
        rail_type_to_test = ['HC', 'LC', 'HV']
        for dut in self.env.duts_or_skip_if_no_vaild_rail(rail_type_to_test):
            for rail_type in dut.RAIL_COUNT.keys():
                if rail_type in rail_type_to_test:
                    for dac in range(number_of_dac):
                        for channel in range(number_of_dac_channels):
                            self.write_and_verify_ad5764_data_register(dut, binary_data_code_for_zero_voltage, channel, dac,rail_type)
                            self.write_and_verify_ad5764_data_register(dut, binary_data_code_for_positive_voltage, channel, dac,rail_type)
                            self.write_and_verify_ad5764_data_register(dut, binary_data_code_for_negative_voltage, channel, dac,rail_type)

    def write_and_verify_ad5764_data_register(self, dut, binary_data_code, channel, dac,rail_type):
        data_register_offset =2
        intial_value = dut.ReadAd5764Register(dac, data_register_offset, channel)
        dut.WritetoAd5764(dac, data_register_offset, channel, binary_data_code)
        result = dut.ReadAd5764Register(dac, data_register_offset, channel)
        dut.WritetoAd5764(dac, data_register_offset, channel, intial_value)
        self.validate_set_ad5764_value(dut, channel, binary_data_code, dac, result, rail_type=rail_type)

    def DirectedAd5764InterfaceCheckViaRailCommandTest(self):
        uhc_count=8
        rail_type_to_test = ['HC','LC','HV']
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type_to_test):
            for rail_type in board.RAIL_COUNT.keys():
                if rail_type in rail_type_to_test:
                    rail_uhc_tuple_list = []
                    for rail in range(board.RAIL_COUNT[rail_type]):
                        for uhc in range(uhc_count):
                            rail_uhc_tuple_list.append((rail, uhc))
                    random.shuffle(rail_uhc_tuple_list)
                    board.SetRailsToSafeState()
                    for rail, uhc in rail_uhc_tuple_list:
                        self.Ad5764InterfaceCheckViaRailCommandScenario(board,rail,uhc,rail_type)
                    board.SetRailsToSafeState()

    def Ad5764InterfaceCheckViaRailCommandScenario(self,dut,rail,uhc,rail_type):
        data_register_offset = 2
        data_expected_tuple_list = self.get_predefined_raw_ad5764_currents()
        dut.ClearDpsAlarms()
        dutid = 15
        dut.EnableOnlyOneUhc(uhc,dutdomainId=dutid)
        dac_mapping = dut.get_dac_mapping()
        dac_device, channel = dac_mapping[rail]
        for i in range(len(data_expected_tuple_list)):
            current,expected_current = data_expected_tuple_list[i]
            dut.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
            dut.WriteTQHeaderViaBar2(dutid,rail,rail_type)

            dut.WriteBar2RailCommand(command=RAILCOMMANDS.SET_CURRENT_RANGE, data=HCLCIRANGE.I_25_MA,rail_or_resourceid=rail+16)
            dut.WriteBar2RailCommand(command=RAILCOMMANDS.SET_MODE,data=SETMODECMD.IFORCE,rail_or_resourceid=rail+16)
            dut.WriteBar2RailCommand(command =RAILCOMMANDS.SET_CURRENT, data=current, rail_or_resourceid=rail+16)
            dut.WriteTQFooterViaBar2(dutid, rail, rail_type)
            result = dut.ReadAd5764Register(dac_device, data_register_offset, channel)

            self.validate_set_ad5764_value(dut, channel, expected_current, dac_device, result, rail_type, rail)


    def validate_set_ad5764_value(self, dut, channel, raw_dac_value, dac_device, result, rail_type, rail=None):
        if dut.is_close(raw_dac_value,result,5):
            self.Log('debug', 'Read expected value {} for {} rail {} on DAC {} Channel {} '.format(result,rail_type,rail, dac_device, channel))
        else:
            self.Log('error',
                     'Communication with Ad5764 failed on {} rail {} DAC {} Channel {}.Expected --> {}. Actual -->{}'.
                     format(rail_type,rail,dac_device, channel, raw_dac_value, result))

    def get_predefined_sfp_currents_ad5764(self,dut):
        data_expected_tuple_list=[]
        current_list = [0.001,0.002,0.003,0.004,0.005]
        for current in current_list:
            data_expected_tuple_list.append((dut.encode_current_in_amps_to_sfp(current),self.conversion_to_dac(current)))
        random.shuffle(data_expected_tuple_list)
        return data_expected_tuple_list

    def get_predefined_raw_ad5764_currents(self):
        zero_amps = 0x8000
        ad5764_data_register_value = []
        value_list = [zero_amps+x for x in range(5)]
        for value in value_list:
            ad5764_data_register_value.append((value,value))
        random.shuffle(ad5764_data_register_value)
        return ad5764_data_register_value

    def conversion_to_dac(self,current):
        rsense_25mA = 20
        vref = 5.0
        vrsense= current *rsense_25mA
        vdac = (vrsense*10)+2
        dacCode = (vdac *32768)/(2*vref)
        return dacCode

