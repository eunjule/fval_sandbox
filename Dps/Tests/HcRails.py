################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: 
# -------------------------------------------------------------------------------
#     Purpose: 
# -------------------------------------------------------------------------------
#  Created by: Renuka Agrawal
#        Date: 1/27/15
#       Group: HDMT FPGA Validation
###############################################################################

import random
from string import Template
import struct
import time
import unittest

from Common.instruments.dps.symbols import RAILCOMMANDS
from Common.instruments.dps.symbols import HCLCIRANGE
from Common.instruments.dps.symbols import ASSERTTEST
from Common.instruments.dps.symbols import DPSTRIGGERS
from Common.instruments.dps.symbols import EVENTTRIGGER
from Common.instruments.dps.symbols import CROSSBOARDTRIGGER
from Common.instruments.dps.hddps_cal_board_v04 import hddps_cal_board_v04
from Common.instruments.dps import hddpsSubslot
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest
from Common import hilmon as hil

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    ## HcForceCurrent
    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcForceCurrentTest(self):
        railCount = 10
        dutIdCount = 8
        railDutIdTupleList = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            for rail in range(railCount):
                for dutid in range(dutIdCount):
                    railDutIdTupleList.append((rail, dutid))
            random.shuffle(railDutIdTupleList)
            for rail,dutId in railDutIdTupleList:
                self.HcForceCurrentScenario(rail, dutId, board)

    def HcForceCurrentScenario(self, rail, dutid, board):
        self.Log('info', 'Testing HC rail {} for dutid {}...'.format(rail, dutid))
        board.SetRailsToSafeState()
        board.ConnectCalBoard('HC{}'.format(rail), 'OHM_4')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'HC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'HC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             $triggerqueueheader          
             SetMode rail=RAIL, value=IFORCE
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIClampHi rail=RAIL, value=0.5
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=3.0
             SetUV rail=RAIL, value=-3.0
             Q cmd=FORCE_CLAMP_FLUSH, arg=RAIL, data=1
             EnableDisableRail rail=RAIL, value=1
             SetCurrent rail=RAIL, value=0.5
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=10000
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 10000, 0x1500, 0x0, 'HC'),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)

        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, dutid, 'HC')

        railcurrent = 'DpsRail' + str(rail) + 'I'
        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} current reg is 0x{:x}'.format(rail, board.Read(railcurrent).Pack()))
        self.Log('info', 'The HCLC rail {} voltage reg is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                  globalalarm.Pack()))

        board.RunCheckers( dutid, 'ResistiveLoopback', 4)

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()


    ## Directed HcRamp Voltage Test
    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRampVoltageTest(self):
        railCount = 10
        dutIdCount = 8
        railDutIdTupleList = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            for rail in range(railCount):
                for dutid in range(dutIdCount):
                    railDutIdTupleList.append((rail, dutid))
            random.shuffle(railDutIdTupleList)
            for rail,dutId in railDutIdTupleList:
                self.HcRampVoltageScenario(rail, dutId, board)

    def HcRampVoltageScenario(self, rail, dutid, board):
        self.Log('info', 'Testing HC rail {} and dutid {}...'.format(rail, dutid))
        board.SetRailsToSafeState()
        board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'HC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'HC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE
             TimeDelay rail=RAIL, value=2500
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=24.0
             SetILoFreeDrive rail=RAIL, value=-4.0
             SetIClampHi rail=RAIL, value=0.5 
             SetIClampLo rail=RAIL, value=-0.5
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             EnableDisableRail rail=RAIL, value=1
             SetVoltage rail=RAIL, value=2.0
             TimeDelay rail=RAIL, value=1000
             SetVoltage rail=RAIL, value=1.5    
             TimeDelay rail=RAIL, value=1000
             SetVoltage rail=RAIL, value=1.0    
             TimeDelay rail=RAIL, value=1000
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=11000
             SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=11000
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 2500, 0x1000, 0x0, 'HC'),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header
                         ))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(0x0, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, dutid, 'HC')

        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                  globalalarm.Pack()))

        board.RunCheckers( dutid, 'ResistiveLoopback', 1e6)

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

            ## Hc Positive V Slew

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHPositiveVslewTest(self):
        railCount = 10
        dutIdCount = 8
        railDutIdTupleList = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            for rail in range(railCount):
                for dutid in range(dutIdCount):
                    railDutIdTupleList.append((rail, dutid))
            random.shuffle(railDutIdTupleList)
            for rail,dutId in railDutIdTupleList:
                self.HcPositiveVslewScenario(rail, dutId, board)


    # Free drive delay for slew is calculated as below.
    # One step = 3.05mV
    # Time takes for each step is 128us.
    # For Ramp step size of 0x8 - 3.05 * 8 = 24.4 mv/step
    # To reach 1.5V in TQ below - as we know 0.4V is free. So actually time taken should be calculated for 1.1V
    # Number of Steps required ro reach 1.1 V = 1.1V/24.4mV = ~ 45 steps
    # Time taken for 45 steps = 45*128us = 5770Us ~ 6mS. That is the free drive delay is set to in the TQ below.

    def HcPositiveVslewScenario(self, rail, dutid, board):
        self.Log('info', 'Testing HC rail {} for dutid {}...'.format(rail, dutid))
        board.SetRailsToSafeState()
        board.ConnectCalBoard('HC{}'.format(rail), 'NONE')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'HC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'HC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['DEVICE'] = 0x05 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE
             TimeDelay rail=RAIL, value=5000
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=24.0
             SetILoFreeDrive rail=RAIL, value=-4.0
             SetIClampHi rail=RAIL, value=1.0
             SetIClampLo rail=RAIL, value=-1.0
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             TimeDelay rail=RAIL, value=1
			 # Actual value is 8 * 3.05mv/step
             Q cmd=DEVICE, arg=RAMP_STEP_SIZE, data=0x08 
             EnableDisableRail rail=RAIL, value=1
             SetFreeDrive rail=RAIL, start=1.5, delay=6000, end=0
             SetVCompAlarm rail=RAIL, value=0
             #SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=11000
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 8000, 0x1500, 0x0, 'HC'),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header
                         ))

        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, dutid, 'HC')

        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                  globalalarm.Pack()))

        board.RunCheckers( dutid, 'ResistiveLoopback', 1e6)

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()


    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcNegativeVslewTest(self):

        railCount = 10
        dutIdCount = 8
        railDutIdTupleList = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            for rail in range(railCount):
                for dutid in range(dutIdCount):
                    railDutIdTupleList.append((rail, dutid))
            random.shuffle(railDutIdTupleList)
            for rail,dutId in railDutIdTupleList:
                self.HcNegativeVslewScenario(rail, dutId, board)

    def HcNegativeVslewScenario(self, rail, dutId, board):
        self.Log('info', 'Testing HC rail {} and dutid {}...'.format(rail, dutId))
        board.SetRailsToSafeState()
        board.ConnectCalBoard('HC{}'.format(rail), 'NONE')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutId)
        board.ConfigureUhcRail(dutId, 0x1 << rail,'HC')
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutId, 'HC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'HC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'HC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['DEVICE'] = 0x05 + rail
        asm.symbols['END'] = 2 * dutId
        asm.symbols['BEGIN'] = 1 + 2 * dutId
        asm.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE             
             TimeDelay rail=RAIL, value=5000
             SetCurrentRange rail=RAIL, value=I_500_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=24.0
             SetILoFreeDrive rail=RAIL, value=-4.0
             SetIClampHi rail=RAIL, value=1.0
             SetIClampLo rail=RAIL, value=-1.0
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             SetVoltage rail=RAIL, value=1.5
             EnableDisableRail rail=RAIL, value=1
             Q cmd=DEVICE, arg=RAMP_STEP_SIZE, data=0x08
             SetFreeDrive rail=RAIL, start=0.0, delay=8000, end=0
             SetVCompAlarm rail=RAIL, value=0
             TimeDelay rail=RAIL, value=11000
             SequenceBreak rail=RAIL, delay=21031
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=11000
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutId, 8000, 0x1500, 0x0, 'HC'),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header
                         ))

        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        board.ExecuteTriggerQueue(offset, dutId, 'HC')

        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is {}'.format(rail, board.Read(railvoltage)))


        board.CheckHclcSamplingActive(dutId)
        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to {}'.format(board.slot, board.subslot,
                                                                                              globalalarm.Pack()))

        board.RunCheckers( dutId, 'ResistiveLoopback', 1e6)

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()


    ## 24A
    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcForceVoltage24AIrangeFreeDriveOpenSocketTest(self):

        railCount = 10
        dutIdCount = 8
        railDutIdTupleList = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            for rail in range(railCount):
                for dutid in range(dutIdCount):
                    railDutIdTupleList.append((rail, dutid))
            random.shuffle(railDutIdTupleList)
            for rail,dutId in railDutIdTupleList:
                self.HcForceVoltage24AIrangeFreeDriveOpenSocketScenario(rail, dutId, board)

    def HcForceVoltage24AIrangeFreeDriveOpenSocketScenario(self, rail, dutid, board):

        self.Log('info', 'Testing HC rail {} for dutid {}...'.format(rail, dutid))
        board.SetRailsToSafeState()
        board.ConnectCalBoard('HC{}'.format(rail), 'NONE')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x10000, dutid, 'HC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail,'HC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail,'HC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['DEVICE'] = 0x05 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid
        asm.LoadString(Template("""\
             $triggerqueueheader
             SetMode rail=RAIL, value=VFORCE             
             Q cmd=TIME_DELAY, arg=RAIL, data=5000
             SetCurrentRange rail=RAIL, value=I_25000_MA
             EnableDisableRail rail=RAIL, value=0
             SetIHiFreeDrive rail=RAIL, value=24.0
             SetILoFreeDrive rail=RAIL, value=-4.0
             SetIClampHi rail=RAIL, value=1.0 
             SetIClampLo rail=RAIL, value=-1.0
             SetOV rail=RAIL, value=2.5
             SetUV rail=RAIL, value=-1.5
             TimeDelay rail=RAIL, value=1
             Q cmd=DEVICE, arg=RAMP_STEP_SIZE, data=0x08
             Q cmd=START_FREE_DRIVE, arg=RAIL, data=0x0000
             TimeDelay rail=RAIL, value=1
             EnableDisableRail rail=RAIL, value=1
             TimeDelay rail=RAIL, value=11000
             Q cmd=END_FREE_DRIVE, arg=RAIL, data=0
             Q cmd=DEVICE, arg=ALARM_SETUP, data=0x0100
             SetFreeDrive rail=RAIL, start=1.5, delay=6000, end=0
             Q cmd=DEVICE, arg=ALARM_SETUP, data=0x0000
             SetVCompAlarm rail=RAIL, value=0
             SequenceBreak rail=RAIL, delay=21031
             TimeDelay rail=RAIL, value=10000
             $sampleengine
             EnableDisableRail rail=RAIL, value=0
             TimeDelay rail=RAIL, value=1000
             $triggerqueuefooter
             TqComplete rail=0, value=0
         """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 20000, 0x500, 0x0, 'HC'),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header
                         ))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        board.ExecuteTriggerQueue(offset, dutid, 'HC')
        board.CheckHclcSamplingActive(dutid)

        railvoltage = 'DpsRail' + str(rail) + 'V'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))


        board.CheckHclcSampleAlarms()

        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                  globalalarm.Pack()))

        board.RunCheckers( dutid, 'ResistiveLoopback', 1e6)  # Added an extra here

        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRailTcLoopingTest(self):
        dutid = 0
        lutdwordaddress = 0x10000000

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            slot = board.slot
            subslot = board.subslot
            for rail in range(10):
                self.Log('info', 'Testing HC rail {} on dutid {}...'.format(rail, dutid))

                board.SetRailsToSafeState()
                board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')

                trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'HC')
                trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'HC')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
                board.UnGangAllRails()

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

                # Start loading the start block, Loop block, Stop block, Complete block for TC Looping
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                # Start Block
                asm.LoadString(Template("""\
                    $triggerqueueheader
                    LoopControl rail=RAIL, value=0
                    LoopTrigger rail=RAIL, value=1
                    SoftwareTrigger rail=RAIL, value=0
                    TqComplete rail=0, value=0    
                """).substitute(triggerqueueheader=trigger_queue_header))

                data = asm.Generate()
                self.Log('info', 'Testing start block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_start_block = 0x1000
                board.WriteTriggerQueue(offset_start_block, data)
                data = board.DmaRead(offset_start_block, 0x100)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000)
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board_2 = self.env.instruments[slot].subslots[0]
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_start_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                # Looping Block
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                asm.LoadString("""\
                    TimeDelay rail=RAIL, value=1
                    TimeDelay rail=RAIL, value=2
                    TimeDelay rail=RAIL, value=3
                    LoopTrigger rail=RAIL, value=1
                    TqComplete rail=0, value=0    
                """)

                data = asm.Generate()
                self.Log('info', 'Testing looping block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_looping_block = 0x2000
                board.WriteTriggerQueue(offset_looping_block, data)
                data = board.DmaRead(offset_looping_block, 0x100)
                # board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x04
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_looping_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                # Stop Block
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                asm.LoadString("""\
                    LoopTrigger rail=RAIL, value=3
                    LoopControl rail=RAIL, value=1
                    TqComplete rail=0, value=0    
                """)

                data = asm.Generate()
                self.Log('info', 'Testing the stop block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_stop_block = 0x4000
                board.WriteTriggerQueue(offset_stop_block, data)
                data = board.DmaRead(offset_stop_block, 0x100)
                # board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x08
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_stop_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                # Done/Complete Block
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = 16 + rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                asm.LoadString(Template("""\
                    LoopControl rail=RAIL, value=2
                    SoftwareTrigger rail=RAIL, value=3
                    $triggerqueuefooter
                    TqComplete rail=0, value=0    
                """).substitute(triggerqueuefooter=trigger_queue_footer))

                data = asm.Generate()
                self.Log('info', 'Testing the stop block trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset_complete_block = 0x8000
                board.WriteTriggerQueue(offset_complete_block, data)
                data = board.DmaRead(offset_complete_block, 0x100)
                # board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x0c
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_complete_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                # board.ReadMemory(data)

                # Read the initial value out of the hclcrailsloopingregister
                HclcRailsLoopingregister = 'HclcRailsLooping'
                self.Log('info',
                         'The HCLC Rails Looping Register before executing TC Looping set to a value = 0x{:x}'.format(
                             board.Read(HclcRailsLoopingregister).Pack()))

                # Send the trigger for the start block with a LUT Address of 14'h0
                self.env.send_trigger(0x04000000)
                self.Log('info', 'RC sent trigger 0x04000000')

                # Read out of the hclcrailslooping register
                hclc_looping_register_initial_value = 0x3ff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0x3ff & ~(1 << rail))
                looping_register_new_value = looping_bits | complete_bits

                time.sleep(0.00001)

                if board.Read(HclcRailsLoopingregister).Pack() != looping_register_new_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after executing start block is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing start block is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))

                    # wait for couple of looping triggers to be received at the HDDPS side
                ExpectedTrigVal = 0x04000001
                for i in range(0, 4):
                    for count in range(5):
                        time.sleep(0.000001)
                        triggerValue = hil.dpsBarRead(slot, subslot, 1, 0x54)
                        if triggerValue == ExpectedTrigVal:
                            self.Log('info', 'Received the looping trigger at the HDDPS Side')
                            break
                        elif count == 4 and triggerValue != ExpectedTrigVal:
                            self.Log('error',
                                     'Correct Trigger value not received for Looping Block, Actual Value -> {:x}'.format(
                                         triggerValue))

                # Send the trigger to the stop block to terminate the tc looping
                self.env.send_trigger(0x04000002)

                # Read out of HclcRailsLoopingregister to make sure that the rail complete bit goes high and rail looping bit goes low
                time.sleep(0.001)
                if board.Read(HclcRailsLoopingregister).Pack() != hclc_looping_register_initial_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after executing the complete block is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing the complete block is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))

                    # Try reading multiple times from trigger down register to ensure that the fifo gets empty
                for num_reads in range(10):
                    hil.dpsBarRead(slot, 0, 1, 0x54)
                    hil.dpsBarRead(slot, 1, 1, 0x54)

                alarm = board.CheckHclcRailAlarm()
                if alarm:
                    board.ClearHclcRailAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRailTcLoopingAlarmConditionTest(self):
        dutid = 0

        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            slot = board.slot
            subslot = board.subslot
            for rail in range(10):
                self.Log('info', 'Testing HC rail {} on dutid {}...'.format(rail, dutid))
                board.SetRailsToSafeState()
                board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
                board.UnGangAllRails()

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

                board.WriteTQHeaderViaBar2(dutid,rail,'HC')

                # Sending a loop control command to rail 0 to start looping
                command = RAILCOMMANDS.LOOP_CONTROL
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail + 16)

                # Read out of the hclcrailslooping register
                hclc_looping_register_initial_value = 0x3ff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0x3ff & ~(1 << rail))
                looping_register_new_value = looping_bits | complete_bits

                HclcRailsLoopingregister = 'HclcRailsLooping'
                time.sleep(0.00001)

                if board.Read(HclcRailsLoopingregister).Pack() != looping_register_new_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after executing loop control rail command is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing loop control rail command is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))

                    # Sending another loop control command to rail 0 to create a looping alarm condition
                command = RAILCOMMANDS.LOOP_CONTROL
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail + 16)

                # In this case both looping and complete bits for a particular rail should be 1 to satisy alarm condition
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0x3ff & ~(1 << rail)) | (1 << rail)
                looping_register_new_value = looping_bits | complete_bits

                time.sleep(0.001)

                if board.Read(HclcRailsLoopingregister).Pack() != looping_register_new_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after second consecutive loop control rail command is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after executing second consecutive loop control rail command is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))

                globalalarm = board.Read('GLOBAL_ALARMS')
                if globalalarm.HclcRailAlarms == 1:
                    self.Log('info',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                      globalalarm.Pack()))
                    hclcrailalarm = board.Read('HCLC_ALARMS')
                    hclcrails = 'Hclc' + str(rail) + 'Alarms'
                    hclcrailsRegister = 'Hclc{:02d}Alarms'.format(rail)
                    if getattr(hclcrailalarm, hclcrails) == 1:
                        hclcrail = board.Read(hclcrailsRegister)
                        self.Log('info', 'The register {} is set to 0x{:x}'.format(hclcrailsRegister, hclcrail.Pack()))
                        if hclcrail.LoopinginProgressAlarm == 1:
                            self.Log('info',
                                     'The Looping in Progress Alarm got set as expected for rail {}.'.format(rail))
                        else:
                            self.Log('error',
                                     'Only the Looping in Progress Alarm for rail {} should be set. Actual -> 0x{:x}'.format(
                                         rail, hclcrail.Pack()))
                    else:
                        self.Log('error', 'The Hclc rail {} should have an alarm set.')
                else:
                    self.Log('error',
                             'The HclcRailAlarm bit in globalalarm register should get set as testing for Looping in Progress Alarm. Actual -> 0x{:x}'.format(
                                 globalalarm.Pack()))

                # Clear the value of looping bit to bring the TC looping Mode to default state by wrting to HclcRailsLoopingRegister
                board.Write(HclcRailsLoopingregister, (0x3ff << (16)))
                time.sleep(0.00001)

                if board.Read(HclcRailsLoopingregister).Pack() != hclc_looping_register_initial_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))

                board.ClearHclcRailAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcSlavePowerStateTest(self):
        dutid = 0
        rail = 4
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            slot = board.slot
            subslot = board.subslot
            board.SetRailsToSafeState()

            DPS_REGISTER2 = board.ad5560regs.DPS_REGISTER2.ADDR
            masterSlaveConfig = 0x31F
            iterator = board.ReturnPatternIterator(masterSlaveConfig)

            # Selecting multiple channels on calcage for a ganged configuration
            cb = hddps_cal_board_v04(slot)
            cb.init()

            for match in iterator:
                span = match.span()
                for railNum in range(*span):
                    cb.selectChannel('HC{}'.format(railNum))
                    self.Log('info', 'The rail num is {:x}'.format(railNum))

            cb.selectLoad('OHM_10')
            cb.apply()

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcRailAlarm()
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)

            board.EnableOnlyOneUhc(dutid)
            board.ConfigureUhcRail(dutid, 0x3FF,'HC')
            # master_slave_config = 0x3FF
            # Rail 4 is Master and 5, 6 and 7 are slaves
            # Sending a TQ Notify to bring the rails out of safe state

            board.SetMasterSlaveRails(0x31F)
            board.SetInvSlaveCnt(0x31F)
            board.ConfigureAD5560GangingRegister(0x31F)

            board.WriteTQHeaderViaBar2(dutid, rail, 'HC')

            # Setting IRange to 25A for ganging support
            command = RAILCOMMANDS.SET_CURRENT_RANGE
            data = HCLCIRANGE.I_25000_MA
            board.WriteBar2RailCommand(command, data, rail + 16)
            self.Log('debug', 'RailCommand SET_CURRENT_RANGE is set to {}'.format(data))

            # Assert Slave Power State as Off, no alarm
            command = RAILCOMMANDS.ASSERT_TEST_RAIL
            assert_test_signal = (ASSERTTEST.SLAVE_POWER_STATE) << 8
            assert_test_expected_data = 0x0
            data = assert_test_signal | assert_test_expected_data
            board.WriteBar2RailCommand(command, data, rail + 16)
            self.Log('debug', 'RailCommand ASSERT_TEST_RAIL is set to {}'.format(data))

            time.sleep(0.00001)

            hclc_slave_power_state_register = board.Read('HclcSlavePowerState')
            if (hclc_slave_power_state_register.Pack() != 0x0):
                self.Log('error', 'Slave Power State Register should be set to 0, Actual 0x{:x}'.format(
                    hclc_slave_power_state_register.Pack()))
            else:
                self.Log('info', 'Slave Power State Register is set to 0')

            # Turning on the slaves as well
            command = RAILCOMMANDS.ENABLE_DISABLE_RAIL
            data = 0x3
            board.WriteBar2RailCommand(command, data, rail + 16)
            self.Log('debug', 'RailCommand ENABLE_DISABLE_RAIL is set to mode {}'.format(data))

            # Assert Slave Power State as On, no alarm
            command = RAILCOMMANDS.ASSERT_TEST_RAIL
            assert_test_expected_data = 0x1
            data = assert_test_signal | assert_test_expected_data
            board.WriteBar2RailCommand(command, data, rail + 16)
            self.Log('debug', 'RailCommand SET_CURRENT_RANGE is set to slave power state as on {}'.format(data))

            time.sleep(0.01)

            hclc_slave_power_state_register = board.Read('HclcSlavePowerState')
            if (hclc_slave_power_state_register.Pack() != (0x1 << rail)):
                self.Log('error',
                         'Slave Power State Reg is not set to 1 for master rail {} as slaves are turned on, Actual 0x{:x}'.format(
                             rail, hclc_slave_power_state_register.Pack()))
            else:
                self.Log('info',
                         'Slave Power State Reg is set to 1 for master rail {} as slaves are turned on, Actual 0x{:x}'.format(
                             rail, hclc_slave_power_state_register.Pack()))

            # Turning both the master rail and slaves off
            command = RAILCOMMANDS.ENABLE_DISABLE_RAIL
            data = 0x0
            board.WriteBar2RailCommand(command, data, rail + 16)
            self.Log('debug', 'RailCommand ENABLE_DISABLE_RAIL is set to mode {}'.format(data))

            # Assert Slave Power State as On , this will throw a cfold alarm as slaves are off
            command = RAILCOMMANDS.ASSERT_TEST_RAIL
            assert_test_expected_data = 0x1
            data = assert_test_signal | assert_test_expected_data
            board.WriteBar2RailCommand(command, data, rail + 16)
            self.Log('debug', 'RailCommand SET_CURRENT_RANGE is set to slave power state as on {}'.format(data))

            time.sleep(0.001)
            hclc_slave_power_state_register = board.Read('HclcSlavePowerState')
            if (hclc_slave_power_state_register.Pack() != 0x0):
                self.Log('error',
                         'Slave Power State Register should be set to 0 as slaves are turned off, actual value {:x}'.format(
                             hclc_slave_power_state_register.Pack()))
            else:
                self.Log('info', 'Slave Power State Register is set to 0 as slaves are turned off')

            board.CheckForHclcCfoldAlarm(rail + 16)
            board.ClearHclcRailAlarms()
            board.ClearHclcCfoldAlarms()
            board.ClearGlobalAlarms()
            board.ClearTrigExecAlarms()

            board.UnGangAllRails()

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRailsP2PVoltageCurrentReadTest(self):
        ''' Test to verify HDDPS RC peer to peer communication interface
            •	Initialize P2P communication by enabling P2P enable bit from P2P control register.
            •	Start reading voltage and current from RC FPGA 
            •   Repeat above steps multiple times and see if system fails to report voltage and current.'''
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            slot = board.slot
            subslot = board.subslot
            board.SetRailsToSafeState()

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcRailAlarm()
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)

            for rail in range(hddpsSubslot.DPS_NUM_AD5560_RAILS):
                board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

            num_p2p_dwords = 2 * (hddpsSubslot.DPS_NUM_AD5560_RAILS)

            # Get the base physical address of P2P SRAM
            base_mem_addr = board.RCPeerToPeerMemoryBase()
            self.Log('info', 'The P2P base mem addr is {:x}'.format(base_mem_addr))

            # Write P2P Start Addr Hi and Lo with appropriate address
            addr = base_mem_addr + 0x300 * slot
            board.Write('P2PStartAddrLo', addr & 0xFFFFFFFF)
            board.Write('P2PStartAddrHi', ((addr >> 32) & 0xFFFFFFFF))

            enabledisablep2p = 1
            self.configure_p2p_control_register_to_enable__or_disable_p2p_communication_and_read_values_back(slot,
                                                                                                             subslot,
                                                                                                             enabledisablep2p)

            self.read_back_logged_values_of_V_and_I_from_RC_and_compare_it_to_expected_rail_values(slot, subslot,
                                                                                                   num_p2p_dwords)

            enabledisablep2p = 0
            self.configure_p2p_control_register_to_enable__or_disable_p2p_communication_and_read_values_back(slot,
                                                                                                             subslot,
                                                                                                             enabledisablep2p)

            board.ClearHclcRailAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRailsScopeShotSamplingModeEventTriggerStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 0:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[0]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
                board.UnGangAllRails()

                event_mask_alarm_bit = 1
                event_mask_trigger_bit = 1

                trigger_value = 0x0

                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x100000
                count = 5
                # Roll over bit will be low since sample size of 0x1FFFF is too high for rollover to occur
                rollover = 0
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureHclcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                  metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # ScopeShot Stop Trigger
                scope_shot_stop_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.STOP)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_stop_trigger_value)
                stop_due_to_trigger = 0
                stop_due_to_alarm = 0
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.Read('SShotTriggerFifoPayload').Pack() != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()
    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRailsScopeShotSamplingModeMatchingTriggerPayloadStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 0:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[0]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
                board.UnGangAllRails()

                event_mask_alarm_bit = 0
                event_mask_trigger_bit = 1

                trigger_value = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x60
                count = 2
                # Roll over bit will be high since sample region buffer size of 0x60 (96 bytes) is too small to accomodate all the samples
                rollover = 1
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureHclcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                  metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # ScopeShot Stop Trigger
                stop_due_to_trigger = 1
                stop_due_to_alarm = 0
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.Read('SShotTriggerFifoPayload').Pack() != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    @unittest.skip('This test currently skipped while HC rails is being audited')
    def DirectedHcRailsScopeShotSamplingModeAlarmStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 0:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[0]
                board.SetRailsToSafeState()

                board.ConnectCalBoard('HC{}'.format(rail), 'OHM_10')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
                board.UnGangAllRails()

                event_mask_alarm_bit = 1
                event_mask_trigger_bit = 0

                trigger_value = 0x0
                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x30
                count = 2
                # Roll over bit will be high since sample region buffer size of 0x30 (48 bytes) is too small to accomodate all the samples
                rollover = 1
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureHclcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                  metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # Sending a cbf trigger due to UHC folding alarm which will stop the scope shot sampling engine
                cbf_type_trigger = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (
                    CROSSBOARDTRIGGER.DPSCTRLTYPE << 15) | CROSSBOARDTRIGGER.CBFOLD)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, cbf_type_trigger)

                # ScopeShot Stop Trigger
                stop_due_to_trigger = 0
                stop_due_to_alarm = 1
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.Read('SShotTriggerFifoPayload').Pack() != 0x0):
                    pass

                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    def configure_p2p_control_register_to_enable__or_disable_p2p_communication_and_read_values_back(self, slot, subslot,
                                                                                                    enabledisablep2p):
        board = self.env.instruments[slot].subslots[subslot]
        # Initiating P2P Communication by enabling P2P enable bit in P2P Control Reg
        P2PControlRegData = board.Read('P2PCtrl')
        P2PControlRegData.P2PEn = enabledisablep2p
        board.Write('P2PCtrl', P2PControlRegData)

        for numreads in range(10):
            time.sleep(0.000001)
            P2PControlRegData = board.Read('P2PCtrl')
            if getattr(P2PControlRegData, 'P2PEn') == enabledisablep2p:
                self.Log('info',
                         'The P2P Control Register is set to a correct value 0x{:x}'.format(P2PControlRegData.Pack()))
                break
            elif numreads == 9:
                self.Log('error', 'The P2P Control Register is set to 0x{:x}'.format(P2PControlRegData.Pack()))

    def read_back_logged_values_of_V_and_I_from_RC_and_compare_it_to_expected_rail_values(self, slot, subslot,
                                                                                          num_p2p_dwords):
        board = self.env.instruments[slot].subslots[subslot]
        offset = 0x4000 + (0x300 * slot)

        errorMargin = 0.05

        for multiplereads in range(30):
            dword_count = 0
            v_or_i_pair = 0
            num_p2p_dwords_for_voltage = 0
            num_p2p_dwords_for_current = 0

            time.sleep(0.02)
            self.Log('info', 'Voltage and Current readings {}'.format(multiplereads))
            for address in range(offset, offset + 0x300, 8):
                readbackvi = self.env.RcBarRead(1, address)
                if dword_count < num_p2p_dwords + 1:
                    if v_or_i_pair == 0:
                        self.Log('info', '{:04X}: Voltage Value -> {:04X}'.format(address, readbackvi))
                        self.Log('info', '{:04X}: Voltage Value in float from host memory {:.4f}'.format(address,
                                                                                                         struct.unpack(
                                                                                                             '<f',
                                                                                                             struct.pack(
                                                                                                                 '<L',
                                                                                                                 readbackvi))[
                                                                                                             0]))
                        instantaneous_voltage_reg_value = 'DpsRail' + str(num_p2p_dwords_for_voltage) + 'V'
                        expectedRailVoltage = board.Read(instantaneous_voltage_reg_value).Pack()
                        self.Log('info',
                                 'HC Rail {}: Voltage Value in float from instantaneous voltage register in FPGA {:.4f}'.format(
                                     num_p2p_dwords_for_voltage,
                                     struct.unpack('<f', struct.pack('<L', expectedRailVoltage))[0]))
                        delta = abs(struct.unpack('<f', struct.pack('<L', expectedRailVoltage))[0] -
                                    struct.unpack('<f', struct.pack('<L', readbackvi))[0])
                        if delta > errorMargin:
                            self.Log('error',
                                     " The actual voltage value from Rail is {} which fails the limit of {}".format(
                                         struct.unpack('<f', struct.pack('<L', readbackvi))[0], errorMargin))
                        num_p2p_dwords_for_voltage = num_p2p_dwords_for_voltage + 1
                    else:
                        self.Log('info', '{:04X}: Current Value -> {:04X}'.format(address, readbackvi))
                        self.Log('info', '{:04X}: Current Value in float from host memory {:.4f}'.format(address,
                                                                                                         struct.unpack(
                                                                                                             '<f',
                                                                                                             struct.pack(
                                                                                                                 '<L',
                                                                                                                 readbackvi))[
                                                                                                             0]))
                        instantaneous_current_reg_value = 'DpsRail' + str(num_p2p_dwords_for_current) + 'I'
                        expectedRailCurrent = board.Read(instantaneous_current_reg_value).Pack()
                        self.Log('info',
                                 'HC Rail {}: Current Value in float from instantaneous Current register in FPGA {:.4f}'.format(
                                     num_p2p_dwords_for_current,
                                     struct.unpack('<f', struct.pack('<L', expectedRailCurrent))[0]))
                        delta = abs(struct.unpack('<f', struct.pack('<L', expectedRailCurrent))[0] -
                                    struct.unpack('<f', struct.pack('<L', readbackvi))[0])
                        if delta > errorMargin:
                            self.Log('error',
                                     " The actual current value from Rail is {} which fails the limit of {}".format(
                                         struct.unpack('<f', struct.pack('<L', readbackvi))[0], errorMargin))
                        num_p2p_dwords_for_current = num_p2p_dwords_for_current + 1

                dword_count = dword_count + 1
                v_or_i_pair = not v_or_i_pair

    def read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(self, slot):
        # Try reading multiple times from trigger down register to ensure that the fifo is empty
        while (hil.dpsBarRead(slot, 0, 1, 0x54) != 0x0 or hil.dpsBarRead(slot, 1, 1, 0x54) != 0x0):
            pass

    def write_poisoned_lut_entry_for_dummy_triggers(self, slot, subslot, num_triggers, lutdwordaddress, dutid):
        uhc_specific_triggers_lut_address = lutdwordaddress + (dutid * 0x10000)
        board = self.env.instruments[slot].subslots[subslot]
        board_2 = self.env.instruments[slot].subslots[0]

        for trigger in range(num_triggers):
            self.Log('info', 'LUT Dword Address is 0x{:x}'.format(uhc_specific_triggers_lut_address))
            board.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
            board_2.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
            time.sleep(0.000001)
            uhc_specific_triggers_lut_address = uhc_specific_triggers_lut_address + 0x4

    def configure_scope_shot_control_register_and_read_values_back(self, slot, subslot, dutid, event_mask_alarm_bit,
                                                                   event_mask_trigger_bit):
        board = self.env.instruments[slot].subslots[subslot]
        SShotControlRegData = board.Read('SShotControl')
        SShotControlRegData.SShotUhcSelect = dutid
        SShotControlRegData.SShotEventMaskAlarm = event_mask_alarm_bit
        SShotControlRegData.SShotEventMaskTrigger = event_mask_trigger_bit
        SShotControlRegData.SShotEnable = 1
        board.Write('SShotControl', SShotControlRegData)

        time.sleep(0.000001)

        SShotControlRegData = board.Read('SShotControl')

        # Masking the sshot reason bits (8-15) as they are don't care at this point
        SShotControlRegValue = (SShotControlRegData.Pack() & 0xFFFF00FF)

        if SShotControlRegValue == (dutid << 28 | 1 << 24 | event_mask_alarm_bit << 1 | event_mask_trigger_bit):
            self.Log('info',
                     'Scope Shot Ctrl Register is set to a correct value of 0x{:x}'.format(SShotControlRegData.Pack()))
        else:
            self.Log('error', 'Scope Shot Ctrl Register is set to an incorrect value of 0x{:x}'.format(
                SShotControlRegData.Pack()))

    def write_to_stop_trigger_register_and_read_values_back(self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]
        SShotStopTriggerRegData = board.Read('SShotStopTrigger')
        SShotStopTriggerRegData.Data = trigger_value
        board.Write('SShotStopTrigger', SShotStopTriggerRegData)

        time.sleep(0.000001)

        SShotStopTriggerRegData = board.Read('SShotStopTrigger')
        if SShotStopTriggerRegData.Pack() == trigger_value:
            self.Log('info', 'Scope Shot Stop Trigger Register is set to a correct value of 0x{:x}'.format(
                SShotStopTriggerRegData.Pack()))
        else:
            self.Log('error', 'Scope Shot Stop Trigger Register is set to an incorrect value of 0x{:x}'.format(
                SShotStopTriggerRegData.Pack()))

    def send_trigger_via_RC_and_read_from_hddps(self, slot, subslot, trigger_value):
        expected = trigger_value
        self.env.send_trigger(trigger_value)
        time.sleep(0.010)
        self.Log('debug', 'RC sent trigger 0x{:x}'.format(trigger_value))

        board = self.env.instruments[slot].subslots[subslot]

        trigger_value = board.Read('TRIGGERDOWNTEST')
        if trigger_value.Pack() != expected:
            self.Log('error', 'HDDPS received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected,
                                                                                                        trigger_value.Pack()))
        else:
            self.Log('info', 'HDDPS({}, {}) received trigger 0x{:x}'.format(slot, subslot, trigger_value.Pack()))

    def read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(self, slot, subslot,
                                                                                           stop_due_to_trigger,
                                                                                           stop_due_to_alarm, rollover):
        board = self.env.instruments[slot].subslots[subslot]
        SShotControlRegData = board.Read('SShotControl')
        if getattr(SShotControlRegData, 'SShotEventSignatureAlarm') == stop_due_to_alarm and getattr(
                SShotControlRegData, 'SShotEventSignatureTrigger') == stop_due_to_trigger:
            self.Log('info',
                     'Scope Shot Ctrl Register is set to a correct value of 0x{:x}'.format(SShotControlRegData.Pack()))
        else:
            self.Log('error', 'Scope Shot Ctrl Register is set to an incorrect value of 0x{:x}'.format(
                SShotControlRegData.Pack()))

        # Verify that the rollover bit is set to 1 since the count value 10 is higher than sample region size of 0x30 which cn accomodate 6 samples
        SShotEndSampleAddress = board.Read('SShotHclcEndSampleAddress')
        if getattr(SShotEndSampleAddress, 'SShotHclcSampleAddrRollover') == rollover:
            self.Log('info', 'Sample Engine End Address register is set to a correct value of {:x}'.format(
                SShotEndSampleAddress.Pack()))
        else:
            self.Log('error', 'Sample Engine End Address register is set to an incorrect value of {:x}'.format(
                SShotEndSampleAddress.Pack()))

    def read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
            self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]

        SShotTriggerFifoTimeStampRegister = board.Read('SShotTriggerFifoTimeStamp')
        self.Log('info', 'The scope shot trigger fifo timestamp register is updated with a value of 0x{:x}'.format(
            SShotTriggerFifoTimeStampRegister.Pack()))

        SShotTriggerFifoPayloadRegister = board.Read('SShotTriggerFifoPayload')
        if SShotTriggerFifoPayloadRegister.Pack() == trigger_value:
            self.Log('info',
                     'The scope shot trigger fifo payload register is updated with correct trigger payload value of 0x{:x}'.format(
                         SShotTriggerFifoPayloadRegister.Pack()))
        else:
            self.Log('error',
                     'The scope shot trigger fifo payload register received an incorrect trigger expected=0x{:x}, actual=0x{:x}'.format(
                         trigger_value, SShotTriggerFifoPayloadRegister.Pack()))
