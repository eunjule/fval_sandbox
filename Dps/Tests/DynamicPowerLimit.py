################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: Dynamic Power Limit
# -------------------------------------------------------------------------------
#     Purpose: To build tests for Dynamic Power Limit
# -------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 12/20/17
#       Group: HDMT FPGA Validation
###############################################################################

import random
import unittest

from Common import configs
from Common.instruments.dps.symbols import RAILCOMMANDS
from Common.instruments.dps.symbols import HCLCIRANGE
from Dps.Tests.dpsTest import BaseTest

minimum_current = 1
maximum_current = 24
FUDGE_FACTOR = 5
SENSE_REGISTER_RANGE = 12
HCLC_RAIL_INDEX = 16


class CurrentRangeSelect(BaseTest):

    def CheckForDynamicPowerLimitCompatableFpga(self, dut):
        if dut.LegacyTcLooping:
            reason = 'This FPGA: {} does not support Dynamic Power Limit'.format(hex(dut.GetFpgaVersion()))
            self.Log('warning', reason)
            raise unittest.SkipTest(reason)

    def RandomLowCurrentClampSettingSelectTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HC']):
            self.CheckForDynamicPowerLimitCompatableFpga(board)
            board.SetRailsToSafeState()
            for test_loops in range(20):
                current_for_i_clamp_hi, current_for_thermal_i_h_clamp_in_x1_format, input_current_for_i_clamp_hi, input_current_for_thermal_i_h_clamp = self.generate_random_current_values(
                    board)
                self.print_current_values(current_for_i_clamp_hi, current_for_thermal_i_h_clamp_in_x1_format,
                                          input_current_for_i_clamp_hi, input_current_for_thermal_i_h_clamp)
                rail_uhc_tuple_list = self.generate_random_rail_dutids()
                count = 0
                for rail, uhc in rail_uhc_tuple_list:
                    count = count + 1
                    self.Log('debug', 'Count : {} Rail :{} uhc: {}'.format(count,rail + HCLC_RAIL_INDEX, uhc))
                    board.EnableOnlyOneUhc(uhc)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,'HC')
                    self.generate_and_execute_bar2_commands(board, current_for_i_clamp_hi,
                                                            current_for_thermal_i_h_clamp_in_x1_format, rail)
                    current_received_from_ad5560 = self.read_current_clamp_setting_from_ad5560(board, rail)
                    self.verify_if_low_current_value_selected(board, current_received_from_ad5560,
                                                              input_current_for_i_clamp_hi,
                                                              input_current_for_thermal_i_h_clamp)

    def generate_random_rail_dutids(self):
        rail_dutid_tuple_list = []
        rail_count = 10
        dut_id_count = 8
        for rail in range(rail_count):
            for dutid in range(dut_id_count):
                rail_dutid_tuple_list.append((rail, dutid))
        random.shuffle(rail_dutid_tuple_list)
        return rail_dutid_tuple_list

    def generate_random_current_values(self, board):
        input_current_for_i_clamp_hi = random.randint(minimum_current, maximum_current)
        input_current_for_thermal_i_h_clamp = random.randint(minimum_current, maximum_current)
        current_for_i_clamp_hi_x1_format = board.encode_current_in_amps_to_x1(input_current_for_i_clamp_hi, SENSE_REGISTER_RANGE)
        current_for_thermal_i_h_clamp_in_x1_format = board.encode_current_in_amps_to_x1(input_current_for_thermal_i_h_clamp, SENSE_REGISTER_RANGE)
        return current_for_i_clamp_hi_x1_format, current_for_thermal_i_h_clamp_in_x1_format, input_current_for_i_clamp_hi, input_current_for_thermal_i_h_clamp
    
    def print_current_values(self, current_for_i_clamp_hi_sfp_format, current_for_thermal_i_h_clamp_in_x1_format,
                             input_current_for_i_clamp_hi, input_current_for_thermal_i_h_clamp):
        self.Log('debug', 'Input current for i clamp hi {} '.format(input_current_for_i_clamp_hi))
        self.Log('debug', 'current_for_i_clamp_hi_x1_format {} '.format(hex(current_for_i_clamp_hi_sfp_format)))
        self.Log('debug', 'Input current for thermal i h clamp {} '.format(input_current_for_thermal_i_h_clamp))
        self.Log('debug', 'current_for_thermal_i_h_clamp_in_x1_format {} '.format(
            hex(current_for_thermal_i_h_clamp_in_x1_format)))
        
    def generate_and_execute_bar2_commands(self, board, current_for_i_clamp_hi_sfp_format,
                                           current_for_thermal_i_h_clamp_in_x1_format, rail):
        self.Log('debug',
                 ' ********** Reading for slot: {} subslot: {} Rail :{} *********** '.format(board.slot, board.subslot,rail + HCLC_RAIL_INDEX))
        board.WriteTQHeaderViaBar2(dutid = 0,rail = rail,resourceid_string = 'HC')
        board.WriteBar2RailCommand(RAILCOMMANDS.ENABLE_DISABLE_RAIL,0 , rail + HCLC_RAIL_INDEX)
        board.WriteBar2RailCommand(RAILCOMMANDS.SET_CURRENT_RANGE,HCLCIRANGE.I_25000_MA, rail + HCLC_RAIL_INDEX)
        board.WriteBar2RailCommand(RAILCOMMANDS.SET_CURRENT_CLAMP_HI,current_for_i_clamp_hi_sfp_format, rail + HCLC_RAIL_INDEX)
        board.WriteBar2RailCommand(RAILCOMMANDS.SET_THERMAL_I_H_CLAMP,current_for_thermal_i_h_clamp_in_x1_format, rail + HCLC_RAIL_INDEX)
        board.WriteBar2RailCommand(RAILCOMMANDS.FORCE_CLAMP_FLUSH,0x0000, rail + HCLC_RAIL_INDEX)
        board.WriteBar2RailCommand(RAILCOMMANDS.ENABLE_DISABLE_RAIL, 1, rail + HCLC_RAIL_INDEX)
        board.WriteTQFooterViaBar2(dutid = 0,rail = rail,resourceid_string = 'HC')

    def read_current_clamp_setting_from_ad5560(self, board, rail):
        current_read_register_at_ad5560 = board.ad5560regs.CLH_DAC_X1.ADDR
        current_from_ad5560 = board.ReadAd5560Register(current_read_register_at_ad5560, rail)
        self.Log('debug', 'Value Received from 5560: {} '.format(hex(current_from_ad5560)))
        current_from_ad5560_in_amps = board.decode_x1_to_current_in_amps(current_from_ad5560, 12)
        current_in_precision_three_format = float('{:.3f}'.format(current_from_ad5560_in_amps))
        return current_in_precision_three_format
    
    def verify_if_low_current_value_selected(self, board, current_in_precision_three_format,
                                             input_current_for_i_clamp_hi, input_current_for_thermal_i_h_clamp):
        if input_current_for_i_clamp_hi < input_current_for_thermal_i_h_clamp:
            self.compare_low_input_current_and_current_from_5560_with_5_percentage_fudge_factor(
                input_current_for_i_clamp_hi,
                current_in_precision_three_format, board)
        elif input_current_for_i_clamp_hi > input_current_for_thermal_i_h_clamp:
            self.compare_low_input_current_and_current_from_5560_with_5_percentage_fudge_factor(
                input_current_for_thermal_i_h_clamp,
                current_in_precision_three_format, board)
        else:
            self.compare_low_input_current_and_current_from_5560_with_5_percentage_fudge_factor(
                input_current_for_thermal_i_h_clamp,
                current_in_precision_three_format, board)

    def compare_low_input_current_and_current_from_5560_with_5_percentage_fudge_factor(self, expected_current, current_from_ad5560, board):
        current_value_is_close = board.is_close(expected_current, current_from_ad5560, FUDGE_FACTOR)
        if current_value_is_close:
            self.Log('debug', 'Correct low current value selected. Expected {} with Fudge Factor:{}% Actual:{} '.format(
                expected_current, FUDGE_FACTOR, current_from_ad5560))
        else:
            self.Log('error',
                     'Failed to select low current value. Expected {} with Fudge Factor:{}% Actual:{} '.format(
                         expected_current, FUDGE_FACTOR, current_from_ad5560))