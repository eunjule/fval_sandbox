################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegistersAdcCurrent
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers for ADC current
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################

import random
from string import Template
# import time

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):


    # Program LC Current calibration register(AD7609 ADC) with random gain and offset values and compare actual against expected current measure readings.
    def LcCurrentAdc25mACalibrationTest(self):
        myresult = ["LcCurrentAdc25mACalibrationTest,"]
        iRange = "25Ma"
        railType = "LC"
        # For 25mA and 500mA and 1.2A
        # IC = IU * Current ADC Calibration Gainreal + Current ADC Calibration Offsetreal
        # Current ADC Calibration Gain real = (Current ADC Calibration Gain / 2^16) + 0.5
        # Current ADC Calibration Offsetreal for 25mA and 500mA and 1.2A = (Current ADC Calibration Offset / 2^17) – 0.25

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register function
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000
            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.2
            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]
            # gainOffsetList = [0x8000]
            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            currentAdc500mACalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            currentAdc500mACalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'currentadc_cal_real is {}'.format(currentAdc500mACalibrationGainReal ) )
            # self.Log('info', 'currentacd_offset_real is {}'.format(currentAdc500mACalibrationOffsetReal ) )

            # adding this for loop to try each gain and offset in the list for debug
            # for selectedGain in gainOffsetList:
            #    for selectedOffset in gainOffsetList:
            #        time.sleep(1)
            #        currentAdc500mACalibrationGainReal = (selectedGain/2**16) + 0.5
            #        currentAdc500mACalibrationOffsetReal = (selectedOffset/2**17) - 0.25

            # end of added debug code

            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetCurrentAdcCalRegister(rail, refGain, refOffset, iRange)

                    # get a reference current by programming the fpga with the default calibration values
                    refCurrent = self.LcCurrentAdcCalibrationScenario(slot, subslot, dutid, rail, refGain, refOffset,
                                                                      iRange)

                    # get the current by programming the fpga with the selected gain and offsets from above
                    calibratedAdcCurrentFromFpga = self.LcCurrentAdcCalibrationScenario(slot, subslot, dutid, rail,
                                                                                        selectedGain, selectedOffset,
                                                                                        iRange)
                    # based on the selected gain and offset calculate the current we expect to see
                    expectedAdcCalibratedCurrent = currentAdc500mACalibrationGainReal * refCurrent + currentAdc500mACalibrationOffsetReal
                    # get the difference between the exepectd and actual Currents from the fpgas
                    delta = abs(expectedAdcCalibratedCurrent - calibratedAdcCurrentFromFpga)

                    # Reset calibration register with default values
                    board.SetCurrentAdcCalRegister(rail, refGain, refOffset, iRange)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual ADC Current value from FPGA is more than error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedAdcCurrentFromFpga, expectedAdcCalibratedCurrent))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual ADC Current value from FPGA is within the error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedAdcCurrentFromFpga, expectedAdcCalibratedCurrent))
                    self.Log('info', 'The reference is {}'.format(refCurrent))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append(
                        [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), calibratedAdcCurrentFromFpga,
                         expectedAdcCalibratedCurrent])
        s_myresult = str(myresult)
        self.Log('debug', 'The result summary is: Rail Gain, Offset, Calibrated Current, Expected Current: {}'.format(
            s_myresult))

    # Program HC Current calibration register(AD7609 ADC) with random gain and offset values and compare actual against expected current measure readings.
    def HcCurrentAdc25mACalibrationTest(self):
        myresult = ["HcCurrentAdc25mACalibrationTest,"]
        iRange = "25Ma"
        railType = "HC"
        # For 25mA and 500mA
        # IC = IU * Current ADC Calibration Gainreal + Current ADC Calibration Offsetreal
        # Current ADC Calibration Gain real = (Current ADC Calibration Gain / 2^16) + 0.5
        # Current ADC Calibration Offsetreal for 25A = (Current ADC Calibration Offset / 2^17) – 0.25

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register function
            board = self.env.instruments[slot].subslots[subslot]

            # Default values for gain and offset for calibration register
            refGain = 0x8000
            refOffset = 0x8000
            # **kill limit (needs to be updated with something valid)
            errorMargin = 0.2
            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]
            # gainOffsetList = [0x8000]
            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            currentAdc500mACalibrationGainReal = (selectedGain / 2 ** 16) + 0.5
            currentAdc500mACalibrationOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            # self.Log('info', 'currentadc_cal_real is {}'.format(currentAdc500mACalibrationGainReal ) )
            # self.Log('info', 'currentacd_offset_real is {}'.format(currentAdc500mACalibrationOffsetReal ) )

            # adding this for loop to try each gain and offset in the list for debug
            # for selectedGain in gainOffsetList:
            #    for selectedOffset in gainOffsetList:
            #        time.sleep(1)
            #        currentAdc500mACalibrationGainReal = (selectedGain/2**16) + 0.5
            #        currentAdc500mACalibrationOffsetReal = (selectedOffset/2**17) - 0.25

            # end of added debug code
            # **will loop through each dut and then each rail, right now we have rails set to 0 only, need to follow up with fpga team to see if need to do all rails
            for dutid in range(0, 1):
                for rail in range(0, 10):
                    # program calibration register with default values
                    board.SetCurrentAdcCalRegister(rail, refGain, refOffset, iRange)

                    # get a reference current by programming the fpga with the default calibration values
                    refCurrent = self.HcCurrentAdcCalibrationScenario(slot, subslot, dutid, rail, refGain, refOffset,
                                                                      iRange)

                    # get the current by programming the fpga with the selected gain and offsets from above
                    calibratedAdcCurrentFromFpga = self.HcCurrentAdcCalibrationScenario(slot, subslot, dutid, rail,
                                                                                        selectedGain, selectedOffset,
                                                                                        iRange)
                    # based on the selected gain and offset calculate the current we expect to see
                    expectedAdcCalibratedCurrent = currentAdc500mACalibrationGainReal * refCurrent + currentAdc500mACalibrationOffsetReal
                    # get the difference between the exepectd and actual Currents from the fpgas
                    delta = abs(expectedAdcCalibratedCurrent - calibratedAdcCurrentFromFpga)

                    # Reset calibration register with default values
                    board.SetCurrentAdcCalRegister(rail, refGain, refOffset, iRange)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The calibrated vs actual ADC Current value from FPGA is more than error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedAdcCurrentFromFpga, expectedAdcCalibratedCurrent))
                    # print debug info
                    else:
                        self.Log('info',
                                 " The calibrated vs actual ADC Current value from FPGA is within the error margin {}, actual value is {}, while expected value is {}".format(
                                     errorMargin, calibratedAdcCurrentFromFpga, expectedAdcCalibratedCurrent))
                    self.Log('info', 'The reference is {}'.format(refCurrent))
                    self.Log('info', 'The gain is {:x}'.format(selectedGain))
                    self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                    myresult.append(
                        [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), calibratedAdcCurrentFromFpga,
                         expectedAdcCalibratedCurrent])

        s_myresult = str(myresult)
        self.Log('debug', 'The result summary is: Rail, Gain, Offset, Calibrated Current, Expected Current: {}'.format(
            s_myresult))

    # LC program AD7609 ADC with gain and offset calibration values for 5uA or 25uA or 250uA or 2.5mA or 25mA or 500mA or 1.2A current ranges, force a voltage and return it to calling function
    def LcCurrentAdcCalibrationScenario(self, slot, subslot, dutid, rail, gain, offset, iRange):

        # depending on the current range being tested set range specific values which the trigger queue will use to force voltage

        if iRange is '25Ma':
            iRangeQueue = 'I_25_MA'
            iClampLowQueue = '-0.025'
            iClampHighQueue = '0.025'
            vForceQueue = '1.0'
            calBoardRes = 'OHM_100'

        self.Log('info', '\nTesting HCLC rail {} for dutid {}...'.format(rail, dutid))

        board = self.env.instruments[slot].subslots[subslot]

        # set board to safe state before changing voltages
        board.SetRailsToSafeState()

        # **Depending on rail type setup the cal cage appropiatly(might not need this since might need sepecific functions for blocks of ranges, would set based on function)
        board.ConnectCalBoard('LC{}'.format(rail), calBoardRes)

        # clearing and reenabling alarms
        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearGlobalAlarms()
        board.Write(ENABLE_ALARMS, 0x1)

        # Check that the current ADC calibration registers(all rails are checked) are programmed with the default values
        board.CheckCurrentAdcCalRegister(iRange)

        # Set the current ADC calibration register for the specific rail under test
        board.SetCurrentAdcCalRegister(rail, gain, offset, iRange)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'LC')
        board.UnGangAllRails()

        # set up memory locations for trigger queue
        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'LC')

        # create a trigger queue object
        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid

        # configure the trigger queue to force a voltage at the speficied range
        asm.LoadString(Template("""\
            TqNotify rail=0, value=BEGIN
            TqNotify rail=16, value=BEGIN
            SetMode rail=RAIL, value=VFORCE
            #Q cmd=TIME_DELAY, arg=RAIL, data=0x1388
            SetCurrentRange rail=RAIL, value=$irange
            EnableDisableRail rail=RAIL, value=0
            SetIHiFreeDrive rail=RAIL, value=0.025
            SetILoFreeDrive rail=RAIL, value=-0.025
            SetIClampHi rail=RAIL, value=$clamphigh
            SetIClampLo rail=RAIL, value=$clamplow
            SetOV rail=RAIL, value=2.5
            SetUV rail=RAIL, value=-1.5
            EnableDisableRail rail=RAIL, value=1
            SetVoltage rail=RAIL, value=$forcevoltage
            TimeDelay rail=RAIL, value=10000
            SetVCompAlarm rail=RAIL, value=0
            #SequenceBreak rail=RAIL, delay=21031
            $sampleengine
            EnableDisableRail rail=RAIL, value=0
            TimeDelay rail=RAIL, value=1000
            TqNotify rail=0, value=END
            TqNotify rail=16, value=END
            TqComplete rail=0, value=0
        """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'LC'),
                        forcevoltage=vForceQueue, clamphigh=iClampHighQueue, clamplow=iClampLowQueue,
                        irange=iRangeQueue))

        # generate the trigger queue
        data = asm.Generate()

        # write the trigger queue to the board
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)

        # debug data
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        # execute the trigger queue on the board
        board.ExecuteTriggerQueue(offset, dutid)

        # **get memory location of current for debug only
        railcurrent = 'DpsRail' + str(rail) + 'I'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railcurrent).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        # **print out global alarm register for debug purposes
        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        # get current from sample memory
        sampleCurrent = board.ReturnSampleCurrentData( SampleBase + 0x1000 * dutid)

        # check if there were alarms and clear them
        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

        # return current back to calling function
        return sampleCurrent





        # HC program AD7609 ADC with gain and offset calibration values for 25mA or 500mA or 25A current ranges, force a voltage and return it to calling function

    def HcCurrentAdcCalibrationScenario(self, slot, subslot, dutid, rail, gain, offset, iRange):

        # depending on the current range being tested set range specific values which the trigger queue will use to force voltage
        if iRange is '25Ma':
            iRangeQueue = 'I_25_MA'
            iClampLowQueue = '-0.025'
            iClampHighQueue = '0.025'
            vForceQueue = '1.0'
            calBoardRes = 'OHM_100'

        self.Log('info', '\nTesting HCLC rail {} for dutid {}...'.format(rail, dutid))

        board = self.env.instruments[slot].subslots[subslot]

        # set board to safe state before changing voltages
        board.SetRailsToSafeState()

        # **Depending on rail type setup the cal cage appropiatly(might not need this since might need sepecific functions for blocks of ranges, would set based on function)
        board.ConnectCalBoard('HC{}'.format(rail), calBoardRes)

        # clearing and reenabling alarms
        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearGlobalAlarms()
        board.Write(ENABLE_ALARMS, 0x1)

        # Check that the current ADC calibration registers(all rails are checked) are programmed with the default values
        board.CheckCurrentAdcCalRegister(iRange)

        # Set the current ADC calibration register for the specific rail under test
        board.SetCurrentAdcCalRegister(rail, gain, offset, iRange)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
        board.UnGangAllRails()

        # set up memory locations for trigger queue
        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'HC')

        # create a trigger queue object
        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid

        # configure the trigger queue to force a voltage at the speficied range
        asm.LoadString(Template("""\
            TqNotify rail=0, value=BEGIN
            TqNotify rail=16, value=BEGIN
            SetMode rail=RAIL, value=VFORCE
            #Q cmd=TIME_DELAY, arg=RAIL, data=0x1388
            SetCurrentRange rail=RAIL, value=$irange
            EnableDisableRail rail=RAIL, value=0
            SetIHiFreeDrive rail=RAIL, value=0.025
            SetILoFreeDrive rail=RAIL, value=-0.025
            SetIClampHi rail=RAIL, value=$clamphigh
            SetIClampLo rail=RAIL, value=$clamplow
            SetOV rail=RAIL, value=2.5
            SetUV rail=RAIL, value=-1.5
            EnableDisableRail rail=RAIL, value=1
            SetVoltage rail=RAIL, value=$forcevoltage
            TimeDelay rail=RAIL, value=10000
            SetVCompAlarm rail=RAIL, value=0
            #SequenceBreak rail=RAIL, delay=21031
            $sampleengine
            EnableDisableRail rail=RAIL, value=0
            TimeDelay rail=RAIL, value=1000
            TqNotify rail=0, value=END
            TqNotify rail=16, value=END
            TqComplete rail=0, value=0
        """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'HC'),
                        forcevoltage=vForceQueue, clamphigh=iClampHighQueue, clamplow=iClampLowQueue,
                        irange=iRangeQueue))

        # generate the trigger queue
        data = asm.Generate()

        # write the trigger queue to the board
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)

        # debug data
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        # execute the trigger queue on the board
        board.ExecuteTriggerQueue(offset, dutid)

        # **get memory location of current for debug only
        railcurrent = 'DpsRail' + str(rail) + 'I'
        self.Log('info', 'The HCLC rail {} status is 0x{:x}'.format(rail, board.Read(railcurrent).Pack()))


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        # **print out global alarm register for debug purposes
        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        # get current from sample memory
        sampleCurrent = board.ReturnSampleCurrentData( SampleBase + 0x1000 * dutid)

        # check if there were alarms and clear them
        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

        # return current back to calling function
        return sampleCurrent
