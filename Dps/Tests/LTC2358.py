################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
import random
import unittest
from Dps.Tests.dpsTest import BaseTest
from Common.instruments.dps.symbols import RAILCOMMANDS

HV_RAIL_START_INDEX = 16
LV_RAIL_START_INDEX = 0
TURBO_SAMPLE_COLLECTOR_DELAY = 0.00000125
SETTLING_TIME_DELAY = 0.01

class InterfaceTests(BaseTest):

    def DirectedHVRegularModeVoltageTest(self, read_iterations=100):
        uhc = random.randint(0, 7)
        dutdomainId = random.randint(0, 64)
        rail_type = 'HV'
        group = 1
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc, dutdomainId)
                for softspancode in [7, 3]:
                    for rail in range(dut.RAIL_COUNT[rail_type]):
                        expected_channel = (2 * rail) % 8
                        sample_collector_index = 2*rail
                        dut.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                        dut.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, softspancode,rail + HV_RAIL_START_INDEX)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(0, read_iterations):
                            try:
                                SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector(sample_collector_index,group)
                            except dut.SampleCollectorError:
                                self.Log('error', 'No samples logged in {} iteration'.format(read_iterations))
                                break
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error',
                                         'SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead, softspancode))

                            if ChannelIdRead != expected_channel:
                                self.Log('error',
                                         'channel id Read {} , channel Id expected {}'.format(ChannelIdRead,
                                                                                              expected_channel))
                        self.Log('info', 'Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(
                            read_iterations, softspancode, expected_channel, rail))
                        dut.ResetVoltageSoftSpanCode(rail_type, dutdomainId)
                dut.SetRailsToSafeState()


    def DirectedLVMRegularModeVoltageTest(self, read_iterations = 100):
        uhc = random.randint(0,7)
        dutdomainId = random.randint(0,64)
        rail_type = 'LVM'
        group = 3
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc, dutdomainId)
                for rail in range(dut.RAIL_COUNT[rail_type]):
                    sample_collector_index = rail
                    dut.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
                    expected_channel = (rail) % 8
                    for softspancode in [7,3]:
                        dut.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, softspancode,rail)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(0, read_iterations):
                            try:
                                SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector(sample_collector_index,group)
                            except dut.SampleCollectorError:
                                self.Log('error','No samples logged in {} iteration'.format(read_iterations))
                                break
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error','SpanCodeRead {}, Expected SpanCode {}' .format(SoftSpanCodeRead,softspancode))

                            if ChannelIdRead != expected_channel:
                                self.Log('error',
                                         'Channel id Read {} , channel Id expected {}'.format(ChannelIdRead, expected_channel))
                        self.Log('info', 'Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(read_iterations,softspancode,expected_channel,rail))
                        dut.ResetVoltageSoftSpanCode(rail_type,dutdomainId)
                dut.SetRailsToSafeState()

    def DirectedHVRegularModeCurrentTest(self, read_iterations = 100):
        uhc = random.randint(0,7)
        dutdomainId = random.randint(0,64)
        rail_type = 'HV'
        group = 1
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc,dutdomainId)
                for rail in range(dut.RAIL_COUNT[rail_type]):
                    sample_collector_index = 2*rail +1
                    expected_channel = (2 * rail) % 8 + 1
                    dut.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
                    for softspancode in [1]:
                        dut.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, softspancode + 0x8, rail + HV_RAIL_START_INDEX)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(0, read_iterations):
                            try:
                                SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector(sample_collector_index,group)
                            except dut.SampleCollectorError:
                                self.Log('error', 'No samples logged in {} iteration'.format(read_iterations))
                                break
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error', 'SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead,softspancode))

                            if ChannelIdRead != expected_channel:
                                self.Log('error',
                                         'channel id Read {} , channel Id expected {}'.format(ChannelIdRead,
                                                                                              expected_channel))
                        self.Log('info', 'Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(
                            read_iterations, softspancode, expected_channel, rail))
                        dut.ResetCurrentSoftSpanCode(rail_type,dutdomainId)
                dut.SetRailsToSafeState()



    def DirectedHVTurboModeVoltageTest(self, read_iterations=100):
        uhc = random.randint(0,7)
        dutdomainId = random.randint(0,64)
        rail_type ='HV'
        group = 5
        turbo_voltage_index=0
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc,dutdomainId)
                for rail in range(dut.RAIL_COUNT[rail_type]):
                    sample_collector_index = turbo_voltage_index
                    expected_channel = rail
                    for softspancode in [7,3]:
                        dut.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
                        dut.WriteTQHeaderViaBar2(dutdomainId,rail,rail_type)
                        self.enable_only_one_turbo_rail(dut, rail,rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, softspancode, rail + HV_RAIL_START_INDEX)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(0, read_iterations):
                            try:
                                SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector(
                                    sample_collector_index,group)
                            except dut.SampleCollectorError:
                                self.Log('error', 'No samples logged in {} iteration'.format(read_iterations))
                                break
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error', 'SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead,softspancode))
                            if ChannelIdRead != expected_channel:
                                self.Log('error',
                                         'Channel id Read {} , channel Id expected {}'.format(ChannelIdRead,
                                                                                              expected_channel))
                        self.Log('info', 'Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(
                            read_iterations, softspancode, expected_channel, rail))
                        dut.ResetVoltageSoftSpanCode(rail_type, dutdomainId)
                self.clear_turbo_rail_select(dut, 'HV')
                dut.SetRailsToSafeState()


    def DirectedHVTurboModeCurrentTest(self, read_iterations=100):
        uhc = random.randint(0,7)
        dutdomainId = random.randint(0,64)
        rail_type = 'HV'
        group = 5
        turbo_current_index = 1
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc,dutdomainId)
                for rail in range(dut.RAIL_COUNT[rail_type]):
                    sample_collector_index = turbo_current_index
                    expected_channel = rail
                    for softspancode in [1]:
                        dut.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
                        dut.WriteTQHeaderViaBar2(dutdomainId,rail,rail_type)
                        self.enable_only_one_turbo_rail(dut, rail,rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, softspancode + 0x8,rail + HV_RAIL_START_INDEX)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(0, read_iterations):
                            try:
                                SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector(sample_collector_index,group)
                            except dut.SampleCollectorError:
                                self.Log('error', 'No samples logged in {} iteration'.format(read_iterations))
                                break
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error', 'SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead,softspancode))

                            if ChannelIdRead != expected_channel:
                                self.Log('error','Channel id Read {} , channel Id expected {}'.format(ChannelIdRead,expected_channel))
                        self.Log('info', 'Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(
                            read_iterations, softspancode, expected_channel, rail))
                        dut.ResetCurrentSoftSpanCode(rail_type,dutdomainId)
                self.clear_turbo_rail_select(dut,'HV')
                dut.SetRailsToSafeState()


    def DirectedLVMTurboModeVoltageTest(self, read_iterations = 100):
        uhc = random.randint(0,7)
        dutdomainId = random.randint(0,64)
        rail_type = 'LVM'
        group = 7
        turbo_lvm_voltage_index = 0
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc, dutdomainId)
                for rail in range(dut.RAIL_COUNT[rail_type]):
                    sample_collector_index = turbo_lvm_voltage_index
                    # sample_collector_index = group * samples_per_group
                    expected_channel = (rail) % 8
                    dut.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
                    for softspancode in [7,3]:
                        dut.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
                        self.enable_only_one_turbo_rail(dut,rail,rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_SOFTSPAN_CODE, softspancode,rail)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(0, read_iterations):
                            try:
                                SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector(sample_collector_index,group)
                            except dut.SampleCollectorError:
                                self.Log('error', 'No samples logged in {} iteration'.format(read_iterations))
                                break
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error', 'SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead,softspancode))

                            if ChannelIdRead != expected_channel:
                                self.Log('error','Channel id Read {} , channel Id expected {}'.format(ChannelIdRead,expected_channel))
                        self.Log('info', 'Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(
                            read_iterations, softspancode, expected_channel, rail))
                        dut.ResetVoltageSoftSpanCode(rail_type, dutdomainId)
                self.clear_turbo_rail_select(dut, rail_type)
                dut.SetRailsToSafeState()


    def enable_only_one_turbo_rail(self, dut, rail,rail_type):
        if rail_type == 'HV':
            dut.WriteRegister(dut.registers.AD5560_TURBO_SAMPLE_RAIL_SELECT(value=0x1<<rail))
        elif rail_type =='RLOAD':
            dut.WriteRegister(dut.registers.TURBO_MODE_CURRENT_SAMPLE_CHANNEL_SELECT(value=0x1 << rail))

        else:
            dut.WriteRegister(dut.registers.DUT_TURBO_SAMPLE_RAIL_SELECT(value = 0x1<<rail))

    def clear_turbo_rail_select(self,dut,rail_type):
        if rail_type == 'HV':
            dut.WriteRegister(dut.registers.AD5560_TURBO_SAMPLE_RAIL_SELECT(value=0))
        elif rail_type == 'RLOAD':
            dut.WriteRegister(dut.registers.TURBO_MODE_CURRENT_SAMPLE_CHANNEL_SELECT(value=0))
        else:
            dut.WriteRegister(dut.registers.DUT_TURBO_SAMPLE_RAIL_SELECT(value=0))


    def clear_adc_softspan_code(self,dut,rail_type):
        if rail_type == 'HV':
            dut.WriteRegister(dut.registers.HV_V_ADC_SOFTSPAN_CODE(value=0))


    def DirectedRloadRegularModeVoltageTest(self, read_iterations=1000):
        rail_type = 'RLOAD'
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                for channel in range(dut.RAIL_COUNT[rail_type]):
                    expected_channel = (channel)
                    for softspancode in range(1, 8, 1):
                        adc_softspan_code_value_register = dut.registers.ADC_SOFTSPAN_CODE_VALUE_REGISTER()
                        adc_softspan_code_value_register.SoftSpanCode = softspancode
                        dut.WriteRegister(adc_softspan_code_value_register)
                        time.sleep(SETTLING_TIME_DELAY)
                        for i in range(read_iterations):
                            ltc_2358_register_type = dut.registers.CHANNEL_V
                            result = dut.ReadRegister(ltc_2358_register_type, index=channel)
                            SoftSpanCodeRead = result.SoftSpanCode
                            ChannelIdRead = result.ChannelId
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error','SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead, softspancode))
                            if ChannelIdRead != expected_channel:
                                self.Log('error','Channel id Read {} , channel Id expected {}'.format(ChannelIdRead,expected_channel))
                        self.Log('info','Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(read_iterations,softspancode,expected_channel,channel))
                self.SetAdcSoftSpanCodeToDefaultState(dut)
                dut.SetRailsToSafeState()

    def DirectedRloadRegularModeCurrentTest(self, read_iterations=1000):
        rail_type = 'RLOAD'
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                for channel in range(dut.RAIL_COUNT[rail_type]):
                    expected_channel = channel
                    sample_collector_index = channel*2 + 1
                    for softspancode in range(1, 8, 1):
                        adc_softspan_code_value_register = dut.registers.ADC_SOFTSPAN_CODE_VALUE_REGISTER()
                        adc_softspan_code_value_register.SoftSpanCode = softspancode
                        dut.WriteRegister(adc_softspan_code_value_register)
                        # time.sleep(SETTLING_TIME_DELAY)
                        dut.WriteToSampleCollectorIndexRegisterRload(sample_collector_index)
                        for i in range(read_iterations):
                            SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_sample_collector_rload()
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error','SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead, softspancode))
                            if ChannelIdRead != expected_channel:
                                self.Log('error','channel id Read {} , channel Id expected {}'.format(ChannelIdRead,expected_channel))
                            time.sleep(TURBO_SAMPLE_COLLECTOR_DELAY)
                        self.Log('info','Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(read_iterations,softspancode,expected_channel,channel))
                self.SetAdcSoftSpanCodeToDefaultState(dut)
                dut.SetRailsToSafeState()


    def DirectedRloadTurboModeCurrentTest(self, read_iterations=1000):
        uhc = random.randint(0, 7)
        dutdomainId = random.randint(0, 63)
        rail_type = 'RLOAD'
        if read_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc, dutdomainId)
                dut.ConfigureUhcRail(uhc, 0xFF, rail_type)
                dut.ClearRailSafeState(dutdomainId)
                for channel in range(dut.RAIL_COUNT[rail_type]):
                    expected_channel = channel
                    sample_collector_index = 1
                    self.enable_only_one_turbo_rail(dut,channel,rail_type)
                    for softspancode in range(1, 8, 1):
                        adc_softspan_code_value_register = dut.registers.ADC_SOFTSPAN_CODE_VALUE_REGISTER()
                        adc_softspan_code_value_register.SoftSpanCode = softspancode
                        dut.WriteRegister(adc_softspan_code_value_register)
                        dut.write_to_turbo_sample_collector_index_register(sample_collector_index)
                        for i in range(read_iterations):
                            SoftSpanCodeRead, ChannelIdRead = dut.read_raw_spancode_and_channel_id_from_turbo_sample_collector()
                            if SoftSpanCodeRead != softspancode:
                                self.Log('error','SpanCodeRead {}, Expected SpanCode {}'.format(SoftSpanCodeRead, softspancode))
                            if ChannelIdRead != expected_channel:
                                self.Log('error','channel id Read {} , channel Id expected {}'.format(ChannelIdRead,expected_channel))
                            time.sleep(TURBO_SAMPLE_COLLECTOR_DELAY)
                        self.Log('info','Iterated {} times, For SpanCode {} and Channel id {} and rail {}'.format(read_iterations,softspancode,expected_channel,channel))
                self.clear_turbo_rail_select(dut, rail_type)
                self.SetAdcSoftSpanCodeToDefaultState(dut)
                dut.SetRailsToSafeState()

    def SetAdcSoftSpanCodeToDefaultState(self,dut):
        dut.WriteRegister(dut.registers.ADC_SOFTSPAN_CODE_VALUE_REGISTER(value=0x7))



