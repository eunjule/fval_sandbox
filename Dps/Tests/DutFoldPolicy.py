################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: DUTFOLDPOLICY.py
#-------------------------------------------------------------------------------
#     Purpose: Test to verify if Dut fold policy is correct
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 03/20/18
#       Group: HDMT FPGA Validation
################################################################################


from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString
from Dps.Tests.dpsTest import BaseTest

import random
import time


import unittest

ALARM_PROPAGATION_DELAY = 0.1
NO_ALARM_LV_UNDERVOLT_LIMIT = -2
NO_ALARM_LV_OVERVOLT_LIMIT = 6
STEADY_STATE_FORCE_VOLTAGE = 3
ALARM_SAMPLING_RATE = 5

class DutFoldPolicy(BaseTest):

    def DirectedNoFaultOrAlarmWithExternalLoadTest(self):
        uhc = 5
        dutId = 15
        hvRail = 3
        lvRail = 3
        railType = 'LVM'
        hvForceVoltage =  3
        lvUnderVoltLimit = -2
        lvOverVoltLimit =  6
        trigger_queue_offset = 0x0
        allowed_percentage_deviation = 15

        for board in self.env.duts_or_skip_if_no_vaild_rail(railType):
            board.SetRailsToSafeState()
            board.ClearDpsAlarms()
            board.EnableAlarms()
            board.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
            board.ConfigureUhcRail(uhc, 0x1 << hvRail,'HV')
            board.ConfigureUhcRail(uhc, 0x1 << lvRail, 'LVM')
            board.UnGangAllRails()

            board.ConnectRailForce(hvRail)
            board.ConnectRailSense(hvRail)
            board.ConnectExternalLoad()
            board.ConnectLvRailSense(lvRail)
            time.sleep(0.1)
            trigger_queue_data = self.generate_trigger_queue(board, dutId, uhc, hvRail, lvRail, hvForceVoltage, lvUnderVoltLimit, lvOverVoltLimit)
            board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
            board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'HV')
            time.sleep(ALARM_PROPAGATION_DELAY)
            hv_rails_folded, lv_rails_folded = board.get_folded_state()
            hvRail_mask = 0xff ^ 1 << hvRail
            lvRail_mask = 0x3ff ^ 1 << lvRail

            if hv_rails_folded != hvRail_mask or lvRail_mask != lv_rails_folded:
                self.Log('error',
                         'Unexpected Rails folded. Expected HV:0x{:x} LV:0x{:x}'.format(hvRail_mask, lvRail_mask))

            self.verify_global_alarm_status(board)

            dut_ov_alarm_status = self.get_dut_ov_alarm_status(board)
            if dut_ov_alarm_status != []:
                self.Log('error',
                         'Unexpected DUT OV Alarms Received :{}'.format(dut_ov_alarm_status))
            
            lvrail_voltage = board.get_rail_voltage(lvRail, 'LVM')
            self.Log('info','LV Rail {}, Voltage: {}'.format(lvRail,lvrail_voltage))
            if board.is_close(hvForceVoltage, lvrail_voltage, allowed_percentage_deviation) == False:
                self.Log('error',
                                'LVM rail:{} voltage: {:.2f}V is not equal to Force voltage: {:.2f}V'.format(
                                    lvRail, lvrail_voltage, hvForceVoltage))

            hvrail_voltage = board.get_rail_voltage(hvRail, 'HV')
            self.Log('info','HV Rail {}, Voltage: {}'.format(hvRail,hvrail_voltage))
            if board.is_close(hvForceVoltage, hvrail_voltage, allowed_percentage_deviation) == False:
                self.Log('error',
                               'HV rail:{} voltage: {:.2f}V is not equal to Force voltage: {:.2f}V'.format(
                                   hvRail, hvrail_voltage, hvForceVoltage))

            board.ConnectRailForce(hvRail,0)
            board.ConnectRailSense(hvRail,0)
            board.ConnectExternalLoad(0)
            board.ConnectLvRailSense(lvRail,0)
            board.SetRailsToSafeState()

    def DirectedNoFaultOrAlarmWithCalLoadTest(self):
        uhc = 5
        dutId = 15
        hvRail = 3
        lvRail = 3
        railType = 'LVM'
        hvForceVoltage =  3
        lvUnderVoltLimit = -2
        lvOverVoltLimit =  6
        trigger_queue_offset = 0x0
        allowed_percentage_deviation = 15

        for board in self.env.duts_or_skip_if_no_vaild_rail(railType):
            board.SetRailsToSafeState()
            board.ClearDpsAlarms()
            board.EnableAlarms()
            board.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
            board.ConfigureUhcRail(uhc, 0x1 << hvRail,'HV')
            board.ConfigureUhcRail(uhc, 0x1 << lvRail, 'LVM')
            board.UnGangAllRails()

            calboard = board.create_cal_board_instance_and_initialize()
            board.cal_load_connect('10Ohm', calboard)
            board.cal_connect_force_sense_lines(hvRail, 'HV', calboard)
            board.ConnectLvRailSense(lvRail)

            time.sleep(0.1)
           
            trigger_queue_data = self.generate_trigger_queue(board, dutId, uhc, hvRail, lvRail, hvForceVoltage, lvUnderVoltLimit, lvOverVoltLimit)
            board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
            board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'HV')
            time.sleep(ALARM_PROPAGATION_DELAY)
            
            hv_rails_folded, lv_rails_folded = board.get_folded_state()
            hvRail_mask = 0xff ^ 1 << hvRail
            lvRail_mask = 0x3ff ^ 1 << lvRail

            if hv_rails_folded != hvRail_mask or lvRail_mask != lv_rails_folded:
                self.Log('error',
                         'Unexpected Rails folded. Expected HV:0x{:x} LV:0x{:x}'.format(hvRail_mask, lvRail_mask))

            self.verify_global_alarm_status(board)


            dut_ov_alarm_status = self.get_dut_ov_alarm_status(board)
            if dut_ov_alarm_status != []:
                self.Log('error',
                         'Unexpected DUT OV Alarms Received :{}'.format(dut_ov_alarm_status))

            lvrail_voltage = board.get_rail_voltage(lvRail, 'LVM')
            self.Log('info','LV Rail {}, Voltage: {}'.format(lvRail,lvrail_voltage))
            if board.is_close(hvForceVoltage, lvrail_voltage, allowed_percentage_deviation) == False:
                self.Log('error',
                                'LVM rail:{} voltage: {:.2f}V is not equal to Force voltage: {:.2f}V'.format(
                                    lvRail, lvrail_voltage, hvForceVoltage))

            hvrail_voltage = board.get_rail_voltage(hvRail, 'HV')
            self.Log('info','HV Rail {}, Voltage: {}'.format(hvRail,hvrail_voltage))
            if board.is_close(hvForceVoltage, hvrail_voltage, allowed_percentage_deviation) == False:
                self.Log('error',
                               'HV rail:{} voltage: {:.2f}V is not equal to Force voltage: {:.2f}V'.format(
                                   hvRail, hvrail_voltage, hvForceVoltage))

            board.ConnectRailForce(hvRail,0)
            board.cal_disconnect_force_sense_lines(hvRail, 'HV', calboard)
            board.create_cal_board_instance_and_initialize()
            board.ConnectLvRailSense(lvRail,0)

            board.SetRailsToSafeState()

    def DirectedNoFaultUserOverVoltageAlarmTest(self):
        force_rail_type = 'HV'
        sense_rail_type = 'LVM'
        hvForceVoltage = 3
        alarm_lv_undervolt_limit = -2
        alarm_lv_overvolt_limit = 1
        expected_alarm = 'UserOvAlarm'
        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            self.dut_fold_policy_scenario_with_cal_load(board,force_rail_type,sense_rail_type,hvForceVoltage,alarm_lv_overvolt_limit,alarm_lv_undervolt_limit,expected_alarm)

    def dut_fold_policy_scenario_with_cal_load(self,board,force_rail_type,sense_rail_type,hvForceVoltage,alarm_lv_overvolt_limit,alarm_lv_undervolt_limit,expected_alarm,fault_alarm = False):
        dutId = 15
        pass_count = 0
        hvRail = random.randint(0,7)
        rail_uhc_tuple_list = board.randomize_rail_uhc(sense_rail_type)
        trigger_queue_offset = 0x0
        filter_count = 12
        expected_no_alarm_value = 0
        wait_time_in_us = ((filter_count * ALARM_SAMPLING_RATE)) * 1e-6
        for lv_rail, uhc in rail_uhc_tuple_list:
            self.initialize_rail(board, lv_rail, sense_rail_type, uhc, dutId)
            self.initialize_rail(board, hvRail, force_rail_type, uhc, dutId)
            board.ResetVoltageSoftSpanCode(force_rail_type, dutId)
            board.ResetCurrentSoftSpanCode(force_rail_type, dutId)
            board.ResetVoltageSoftSpanCode(sense_rail_type, dutId)
            default_filter_count_value = self.get_ov_uv_alarm_filter_count(board)
            self.set_ov_uv_alarm_filter_count(board, filter_count)

            calboard = board.create_cal_board_instance_and_initialize()
            board.cal_load_connect('10Ohm', calboard)
            board.cal_connect_force_sense_lines(hvRail, 'HV', calboard)
            board.ConnectLvRailSense(lv_rail)
            time.sleep(0.1)

            trigger_queue_data = self.generate_trigger_queue(board, dutId, uhc, hvRail, lv_rail, STEADY_STATE_FORCE_VOLTAGE,
                                                             NO_ALARM_LV_UNDERVOLT_LIMIT, NO_ALARM_LV_OVERVOLT_LIMIT)
            board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
            board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'HV')
            time.sleep(ALARM_PROPAGATION_DELAY)

            global_alarms = board.get_global_alarms()
            if global_alarms != []:
                self.Log('error',
                         '{} rail {} Steady State conditions not achieved.Received {}'.format(sense_rail_type, lv_rail,
                                                                                              global_alarms))
            else:
                trigger_queue_data = self.generate_trigger_queue(board, dutId, uhc, hvRail, lv_rail, hvForceVoltage,
                                                                 alarm_lv_undervolt_limit,
                                                                 alarm_lv_overvolt_limit,fault_alarm)
                board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
                board.ExecuteTriggerQueue(trigger_queue_offset, uhc, 'HV')
                no_alarm_within_filter_time = board.ReadRegister(board.registers.DUT_RAIL_ALARM,index=lv_rail).value
                board.WaitTime(wait_time_in_us)
                if no_alarm_within_filter_time != expected_no_alarm_value:
                    self.Log('error','Dut Alarms received too soon compared to filter wait time on rail {}.DUT_RAIL_ALARM value {:x}'.format(lv_rail,no_alarm_within_filter_time))
                else:
                    if fault_alarm == True:
                        time.sleep(ALARM_PROPAGATION_DELAY)
                        if self.verify_global_alarm_status(board, ['OvAlarms', 'DutRailAlarms']) == True:
                            if self.get_dut_rail_alarms_status(board,lv_rail,expected_alarm) == True:
                                pass_count += 1
                    else:
                        if board.check_for_single_dut_rail_alarm(lv_rail,expected_alarm) == True:
                            pass_count += 1

            board.ConnectRailForce(hvRail, 0)
            board.cal_disconnect_force_sense_lines(hvRail, 'HV', calboard)
            board.ConnectLvRailSense(lv_rail, 0)
            self.set_ov_uv_alarm_filter_count(board, default_filter_count_value)


        if pass_count == len(rail_uhc_tuple_list):
            self.Log('info',  '{} Expected  {} behavior seen on all UHC/Rail combinations for filter count {}.'.format(
                         sense_rail_type,expected_alarm,filter_count))
        else:
            self.Log('error',
                     '{} Expected {} behavior seen on {} of {} UHC/Rail combinations filter count {}.'.format(
                         sense_rail_type,expected_alarm, pass_count, len(rail_uhc_tuple_list),filter_count))
        board.create_cal_board_instance_and_initialize()
        board.SetRailsToSafeState()

    def get_ov_uv_alarm_filter_count(self, board):
        return board.ReadRegister(board.registers.OV_UV_ALARM_FILTER_COUNT).value

    def set_ov_uv_alarm_filter_count(self, board, filter_count):
        board.WriteRegister(board.registers.OV_UV_ALARM_FILTER_COUNT(value=filter_count))

    def DirectedNoFaultUserUnderVoltageAlarmTest(self):
        force_rail_type = 'HV'
        sense_rail_type = 'LVM'
        hvForceVoltage = 3
        alarm_lv_undervolt_limit = 5
        alarm_lv_overvolt_limit = 6
        expected_alarm = 'UserUvAlarm'
        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            self.dut_fold_policy_scenario_with_cal_load(board, force_rail_type, sense_rail_type, hvForceVoltage,
                                                        alarm_lv_overvolt_limit, alarm_lv_undervolt_limit,
                                                        expected_alarm)

    def DirectedFaultHardwareOverVoltageAlarmTest(self):
        force_rail_type = 'HV'
        sense_rail_type = 'LVM'
        hvForceVoltage = 9
        alarm_lv_undervolt_limit = -2
        alarm_lv_overvolt_limit = 6.25
        expected_alarm = 'HwOvAlarm'
        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            self.dut_fold_policy_scenario_with_cal_load(board, force_rail_type, sense_rail_type, hvForceVoltage,
                                                        alarm_lv_overvolt_limit, alarm_lv_undervolt_limit,
                                                        expected_alarm,fault_alarm=True)



    def generate_trigger_queue(self, board, dutId, uhc, hvRail, lvRail, hvForceVoltage, lvUnderVoltLimit, lvOverVoltLimit,fault_alarm= False):
        hvIRange = 'I_7500_MA'
        hvClampHigh = 3
        hvClampLow = -0.5
        hvOverVoltage = 18
        hvUnderVoltage = -3

        hvTqString = TriggerQueueString(board,uhc,dutId)
        hvTqString.open_socket = False

        rail_type = 'HV'

        hvTqString.force_rail_type = rail_type
        hvTqString.force_rail = hvRail
        hvTqString.force_rail_irange = hvIRange
        hvTqString._force_voltage = hvForceVoltage
        hvTqString.tracking_voltage = hvForceVoltage + 2
        hvTqString.force_clamp_low = hvClampLow
        hvTqString.force_clamp_high = hvClampHigh
        hvTqString.force_low_voltage_limit = hvUnderVoltage
        hvTqString.force_high_voltage_limit = hvOverVoltage
        hvTqString.free_drive = True
        hvTqString.force_free_drive_high = 7.5
        hvTqString.force_free_drive_low = -.5
        hvTqString.force_free_drive_delay = 6000

        rail_type = 'LVM'

        hvTqString.sense_rail_type = rail_type
        hvTqString.sense_rail = lvRail
        hvTqString.sense_high_voltage_limit = lvOverVoltLimit
        hvTqString.sense_low_voltage_limit = lvUnderVoltLimit

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(hvTqString.generateDutFoldPolicyTriggerQueue(fault_alarm))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def verify_global_alarm_status(self, board , alarms = []):
        global_alarms = board.get_global_alarms()
        if set(global_alarms) != set(alarms):
            self.Log('error', 'Expected global alarm(s): {}'.format(alarms))
            self.Log('error', 'Received global alarm(s): {}'.format(global_alarms))
            return False
        self.Log('debug', 'Global Alarm are: {}'.format(alarms))
        return True

    def get_dut_comparison_alarm_status(self, board):
        dut_rail_alarm = board.ReadRegister(board.registers.DUT_COMPARISON_ALARMS)
        alarms = []
        for field in dut_rail_alarm.fields():
            if getattr(dut_rail_alarm, field) > 0:
                    alarms.append(field)
        self.Log('info', 'DUT COMPARISON ALARMS are: {}'.format(alarms))
        if 'Reserved' in alarms:
            self.Log('error', 'Unknown bit set in DUT COMPARISON ALARMS')
        return alarms

    def get_dut_ov_alarm_status(self, board):
        dut_rail_alarm = board.ReadRegister(board.registers.OV_ALARMS)
        alarms = []
        for field in dut_rail_alarm.fields():
            if getattr(dut_rail_alarm, field) > 0:
                    alarms.append(field)
        self.Log('info', 'OV_ALARMS Alarm(s) are{}'.format(alarms))
        return alarms


    def get_dut_rail_alarms_status(self, board, rail, expected_rail_alarm):
        dut_alarms = board.ReadRegister(board.registers.DUT_ALARMS)
        dut_per_rail_alarm = board.getDutPerRailAlarmRegister()
        rail_mask = 1 << rail
        if rail_mask == dut_alarms.value:
            hclc_per_rail_alarm = board.ReadRegister(dut_per_rail_alarm, index=rail)
            rail_alarms = []
            for field in hclc_per_rail_alarm.fields():
                if getattr(hclc_per_rail_alarm, field) == 1:
                    rail_alarms.append(field)
            if rail_alarms == [expected_rail_alarm]:
                self.Log('debug', 'Expected {} Alarm received on rail {}'.format(expected_rail_alarm, rail))
                return True
            else:
                self.Log('error',
                         'Expected only {} to be set in Dut rail {} specific alarm register, actual alarms set were {}'.format(
                             expected_rail_alarm, rail, rail_alarms))
        else:
            hclc_alarms_set = []
            for field in dut_alarms.fields():
                if getattr(dut_alarms, field) == 1:
                    hclc_alarms_set.append(field)
            self.Log('error',
                     'Expected only Rail {} bit to be set in Dut_alarms register, but got the following: {}'.format(
                         rail, hclc_alarms_set))
        return False

    def initialize_rail(self, board, rail, rail_type, uhc, dutid):
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
        board.UnGangAllRails()
        



