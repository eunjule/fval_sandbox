################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
from Dps.Tests.dpsTest import BaseTest
from Common.instruments.dps.symbols import RAILCOMMANDS,SETMODECMD,HCLCIRANGE,VRANGE
from Common.instruments.dps import ad5560_registers

AD5560_DEVICE_RAIL_COUNT = 10
HV_RAIL_OFFSET =16
class InterfaceTests(BaseTest):

    def DirectedModifyRegisterViaHilTest(self,number_of_iterations = 1000):
        random_ad5560_register_type = ad5560_registers.CPL_DACX1_5uA
        rail_type_to_test = ['HC', 'LC', 'HV']
        if number_of_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given.')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail(rail_type_to_test):
                for rail_type in dut.RAIL_COUNT.keys():
                    if rail_type in rail_type_to_test:
                        dut.SetRailsToSafeState()
                        dut.ClearDpsAlarms()
                        for rail in range(dut.RAIL_COUNT[rail_type]):
                            pass_count = 0
                            fail_count = 0
                            initial_register_value = dut.ReadAd5560Register(random_ad5560_register_type.ADDR, rail)

                            for i in range(number_of_iterations):
                                random_value = random.randint(0x0,0xFFFF)

                                dut.WriteAd5560Register(random_ad5560_register_type.ADDR, random_value, rail)
                                readback_value = dut.ReadAd5560Register(random_ad5560_register_type.ADDR,rail)
                                if readback_value !=random_value:
                                    fail_count = fail_count +1
                                    self.Log('error','Communication with {} Ad5560 failed for ad5560 rail {}.Register value Expected: {},Actual: {}'.format(rail_type,rail, random_value, readback_value))
                                else:
                                    pass_count = pass_count+1

                                dut.WriteAd5560Register(random_ad5560_register_type.ADDR, initial_register_value, rail)
                            self.summarize_results(fail_count, number_of_iterations, pass_count, rail,
                                                   rail_type)


    def summarize_results(self, fail_count, number_of_iterations, pass_count, rail, rail_type):
        if pass_count == number_of_iterations:
            self.Log('info','Wrote and verified to register random values for {} Ad5560 rail {}, {} times'.format(rail_type,rail,number_of_iterations))
        else:
            self.Log('error','The {} Ad5560 register values read are incorrect for {} times out of {}'.format(rail_type,fail_count,number_of_iterations))



class AutoModeCompensation(BaseTest):


    def DirectedVForceI7p5MACompensationSettingTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            # vrange = [1,0,2]
            vrange = {'V_25_VOLT':0x110,'V_6_VOLT':0x8AE0,'V_22_VOLT':0x110}
            undertest_irange = HCLCIRANGE.I_7500_MA
            pass_count =0
            # expected_ad5560_compensation_value = [0x110,0x8AE0,0x110]
            # expected_ad5560_compensation_value = 0x8AE0
            for rail, uhc in rail_uhc_tuple_list:
                for undertest_vrange,expected_ad5560_compensation_value in vrange.items():
                    vrange_value =  getattr(VRANGE, undertest_vrange)
                    # print(vrange_value,expected_ad5560_compensation_value)
                    board.ClearDpsAlarms()
                    board.EnableAlarms()
                    board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                    board.UnGangAllRails()
                    board.SetRailsToSafeState()
                    if rail_type == 'HV' or rail_type == 'LVM':
                        board.ResetVoltageSoftSpanCode(rail_type, dutid)
                        board.ResetCurrentSoftSpanCode(rail_type, dutid)
                    board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                    board.WriteTQHeaderViaBar2(dutid, rail, rail_type)

                    pass_count = self.compensation_mode_setting_scenario(board, dutid,
                                                            expected_ad5560_compensation_value, rail, rail_type,
                                                                         vrange_value,undertest_irange,pass_count)
            if pass_count == len(rail_uhc_tuple_list)*len(vrange):
                self.Log('info','AutoModeCompensation Setting for all voltage ranges and 7.5MA Current range was successful for all rail uhc combination {}'.format(len(rail_uhc_tuple_list)))

    def Directed6VRangeI7p5MACompensationSettingTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            undertest_vrange = 0
            undertest_irange = HCLCIRANGE.I_7500_MA
            pass_count =0
            expected_ad5560_compensation_value = 0x8AE0
            for rail, uhc in rail_uhc_tuple_list:
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV' or rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type, dutid)
                    board.ResetCurrentSoftSpanCode(rail_type, dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.WriteTQHeaderViaBar2(dutid, rail, rail_type)

                pass_count = self.compensation_mode_setting_scenario(board, dutid,
                                                        expected_ad5560_compensation_value, rail, rail_type,
                                                        undertest_vrange,undertest_irange,pass_count)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info','AutoModeCompensation Setting for 6V voltage range and 7.5MA Current range was successful for all rail uhc combination {}'.format(len(rail_uhc_tuple_list)))


    def compensation_mode_setting_scenario(self, board, dutid, expected_ad5560_compensation_value, rail,
                                           rail_type, undertest_vrange,undertest_irange,pass_count):
        default_vrange =1
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_MODE, data=SETMODECMD.VFORCE,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_CURRENT_RANGE, data=undertest_irange,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_V_RANGE, data=undertest_vrange,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_AUTO_COMPENSATION, data=0,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)

        actual_ad5560_compensation_value = board.ReadAd5560Register(board.ad5560_registers.COMPENSATION2.ADDR, rail)

        if actual_ad5560_compensation_value != expected_ad5560_compensation_value:
            self.Log('error',
                     'AutoCompensation Setting is incorrect. Actual {:x} , Expected {:x}'.format(
                         actual_ad5560_compensation_value, expected_ad5560_compensation_value))
        else:
            pass_count +=1
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_CURRENT_RANGE, data=undertest_irange,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_V_RANGE, data=default_vrange,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_AUTO_COMPENSATION, data=0,
                                   rail_or_resourceid=rail + HV_RAIL_OFFSET)

        board.WriteTQFooterViaBar2(dutid, rail, rail_type)
        return pass_count

    def Directed22VrangeI7p5MACompensationSettingTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            undertest_vrange = 1
            undertest_irange = HCLCIRANGE.I_7500_MA
            expected_ad5560_compensation_value = 0x0110
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV' or rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type, dutid)
                    board.ResetCurrentSoftSpanCode(rail_type, dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                pass_count = self.compensation_mode_setting_scenario(board, dutid,
                                                        expected_ad5560_compensation_value, rail, rail_type,
                                                        undertest_vrange, undertest_irange,pass_count)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         'AutoModeCompensation Setting for 22V voltage range and 7.5MA Current range was successful for all rail uhc combination {}'.format(
                             len(rail_uhc_tuple_list)))

    def Directed25VrangeI7p5MACompensationSettingTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            undertest_irange = HCLCIRANGE.I_7500_MA
            undertest_vrange = 2
            expected_ad5560_compensation_value = 0x0110
            pass_count =0
            for rail, uhc in rail_uhc_tuple_list:
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV' or rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type, dutid)
                    board.ResetCurrentSoftSpanCode(rail_type, dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                pass_count = self.compensation_mode_setting_scenario(board, dutid,
                                                        expected_ad5560_compensation_value, rail, rail_type,
                                                        undertest_vrange, undertest_irange,pass_count)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         'AutoModeCompensation Setting for 25V voltage range and 7.5MA Current range was successful for all rail uhc combination {}'.format(
                             len(rail_uhc_tuple_list)))


    def DirectedI500MACompensationSettingTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            undertest_vrange = 1
            undertest_irange = HCLCIRANGE.I_500_MA
            expected_ad5560_compensation_value = 0x0110
            pass_count= 0
            for rail, uhc in rail_uhc_tuple_list:
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV' or rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type, dutid)
                    board.ResetCurrentSoftSpanCode(rail_type, dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                pass_count = self.compensation_mode_setting_scenario(board, dutid,
                                                        expected_ad5560_compensation_value, rail, rail_type,
                                                        undertest_vrange, undertest_irange,pass_count)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         'AutoModeCompensation Setting for 500MA Current range was successful for all rail uhc combination {}'.format(
                             len(rail_uhc_tuple_list)))

