################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: Dut Domain ID
# -------------------------------------------------------------------------------
#     Purpose: To build tests to verify if correct dut domain id sent to RC
# -------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 01/30/18
#       Group: HDMT FPGA Validation
###############################################################################

import random
import unittest

from Common import configs
from Common.instruments.dps.symbols import RAILCOMMANDS
from Dps.Tests.dpsTest import BaseTest

HCLC_RAIL_BASE = 16
HCLC_RAIL_COUNT = 10
VLC_RAIL_COUNT = 16
DUT_DOMAIN_ID_COUNT = 64
DISABLE_RAIL = 0

class DutDomainIDinRC(BaseTest):

    def CheckForTCLoopingCompatableFpga(self, dut):
        if dut.LegacyTcLooping:
            reason = 'This FPGA: {} does not support New TC looping Feature'.format(hex(dut.GetFpgaVersion()))
            self.Log('warning', reason)
            raise unittest.SkipTest(reason)

    def RandomHCRailDutDomainIDSenttoRCValidationTest(self):
        resource_id = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([resource_id]):
            self.CheckForTCLoopingCompatableFpga(board)
            board.SetRailsToSafeState()
            for uhc in range(8):
                rail_dutdomainid_tuple_list = self.generate_random_rail_dutdomainids(resource_id)
                for rail, dutdomainId in rail_dutdomainid_tuple_list:
                    board.EnableOnlyOneUhc(uhc, dutdomainId)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,resource_id)
                    self.run_tqnotify_bar_commands(uhc, rail, board, resource_id)
                    self.read_rc_verify_dutdomain_id(dutdomainId, board)
            board.SetRailsToSafeState()

    def RandomLCRailDutDomainIDSenttoRCValidationTest(self):
        resource_id = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([resource_id]):
            self.CheckForTCLoopingCompatableFpga(board)
            board.SetRailsToSafeState()
            for uhc in range(8):
                rail_dutdomainid_tuple_list = self.generate_random_rail_dutdomainids(resource_id)
                for rail, dutdomainId in rail_dutdomainid_tuple_list:
                    board.EnableOnlyOneUhc(uhc, dutdomainId)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,resource_id)
                    self.run_tqnotify_bar_commands(uhc, rail, board, resource_id)
                    self.read_rc_verify_dutdomain_id(dutdomainId, board)
            board.SetRailsToSafeState()

    def RandomVLCRailDutDomainIDSenttoRCValidationTest(self):
        resource_id = 'VLC'
        for board in self.env.duts_or_skip_if_no_vaild_rail([resource_id]):
            self.CheckForTCLoopingCompatableFpga(board)
            board.SetRailsToSafeState()
            for uhc in range(8):
                rail_dutdomainid_tuple_list = self.generate_random_rail_dutdomainids(resource_id)
                for rail, dutdomainId in rail_dutdomainid_tuple_list:
                    board.EnableOnlyOneUhc(uhc, dutdomainId)
                    board.ConfigureUhcRail(uhc, 0x1 << rail,resource_id)
                    self.run_tqnotify_bar_commands(uhc, rail, board, resource_id)
                    self.read_rc_verify_dutdomain_id(dutdomainId, board)
            board.SetRailsToSafeState()

    def generate_random_rail_dutdomainids(self, resource_id):
        rail_dutdomainid_tuple_list = []
        if resource_id == 'VLC':
            rail_count = VLC_RAIL_COUNT
        else:
            rail_count = HCLC_RAIL_COUNT
        for rail in range(rail_count):
            for dutdomainid in range(DUT_DOMAIN_ID_COUNT):
                rail_dutdomainid_tuple_list.append((rail, dutdomainid))
        random.shuffle(rail_dutdomainid_tuple_list)
        return rail_dutdomainid_tuple_list

    def run_tqnotify_bar_commands(self, uhc, rail, board, resource_id):
        board.WriteTQHeaderViaBar2(uhc, rail, resource_id)
        board.WriteTQFooterViaBar2(uhc, rail, resource_id)
        board.CheckTqNotifyAlarm(uhc, resource_id)

    def read_rc_verify_dutdomain_id(self ,dutdomainId, board):
        dutdomainid_mask = 0x3f00000
        trigger_at_rc = board.rc.read_trigger_up(board.slot)
        dutdomainid_at_rc = trigger_at_rc & dutdomainid_mask
        if dutdomainId != dutdomainid_at_rc >> 20:
            self.Log('error', 'Incorrect DutDomain ID received at RC. Expected {}: Actual:{}'.format(dutdomainId,dutdomainid_at_rc >> 20))
        else:
            self.Log('debug', 'Correct DutDomain ID received at RC. :{}'.format(dutdomainid_at_rc >> 20))
