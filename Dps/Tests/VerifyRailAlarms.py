################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: VerifyRailAlarm.py
#-------------------------------------------------------------------------------
#     Purpose: Test to verify if Correct Alarms are generated.
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 04/18/18
#       Group: HDMT FPGA Validation
################################################################################

import random
import time
import unittest
import struct

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString
from Dps.Tests.dpsTest import BaseTest

FORCE_VOLTAGE_RANGE = {'HC':(1, 2.5), 'LC':(1, 3), 'HV':(1, 15), 'LVM':(1,2)}
VOLTAGE_STEPSIZE = {'HC':0.25, 'LC':0.25, 'HV':0.25, 'LVM':0.125}

CLAMP_LIMITS = {'HC':(-0.5,0.5),'LC':(-0.5,0.5),'HV':(-0.5,0.5)}

NO_ALARM_VOLTAGE_LIMITS = {'HC':(-3,10),'LC':(-3,10),'HV':(-3,18),'LVM':(-2,6)}
VOLTAGE_LIMITS_FOR_LOW_ALARM_TEST = {'HC':(5, 10), 'LC':(5, 10), 'HV':(20, 18), 'LVM':(4, 6)}
VOLTAGE_LIMITS_FOR_HIGH_ALARM_TEST = {'HC':(-3, -2), 'LC':(-3, -2), 'HV':(-3, -2), 'LVM':(-2, -1)}

I_RANGE = {'HC':'I_500_MA','LC':'I_500_MA','HV':'I_500_MA'}
dutid = 15
ALARM_PROPAGATION_DELAY = 0.1
TRIGGER_QUEUE_OFFSET = 0x0


class ComparatorAlarms(BaseTest):

    def DirectedHCRailComparatorHighAlarmTest(self):
        rail_type = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.comparator_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='ComparatorHighAlarm')

    def DirectedLCRailComparatorHighAlarmTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.comparator_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='ComparatorHighAlarm')

    def DirectedHVRailComparatorHighAlarmTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.comparator_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='ComparatorHighAlarm')

    @unittest.skip('Skipped ,as oscillation detect feature impacts this test')
    def DirectedHCRailComparatorLowAlarmTest(self):
        rail_type = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.comparator_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='ComparatorLowAlarm')

    def DirectedLCRailComparatorLowAlarmTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.comparator_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='ComparatorLowAlarm')

    @unittest.skip('Skipped ,as oscillation detect feature impacts this test')
    def DirectedHVRailComparatorLowAlarmTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.comparator_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='ComparatorLowAlarm')


    def comparator_alarm_scenario(self, board, rail_type, iterations, expected_hclc_alarm):
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, rail_type)
        board.SetRailsToSafeState()
        if rail_type == 'HV':
            board.ResetVoltageSoftSpanCode(rail_type, dutid)
            board.ResetCurrentSoftSpanCode(rail_type, dutid)
        for test_iteration in range(iterations):
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                force_voltage = board.get_positive_random_voltage(FORCE_VOLTAGE_RANGE[rail_type][0],
                                                                  FORCE_VOLTAGE_RANGE[rail_type][1],
                                                                  VOLTAGE_STEPSIZE[rail_type])

                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.UnGangAllRails()

                trigger_queue_data = self.generate_non_alarm_condition_trigger_queue(board, rail_type, force_voltage, dutid, rail, uhc)
                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)
                global_alarms = board.get_global_alarms()
                if global_alarms != []:
                    self.Log('error', 'Global Alarm should be empty.Actual fields set: {}'.format(global_alarms))
                    continue

                trigger_queue_data = self.generate_alarm_condition_trigger_queue(board, rail_type, dutid, rail, uhc, expected_hclc_alarm)

                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)

                if board.check_for_single_hclc_rail_alarm(rail, expected_hclc_alarm):
                    pass_count += 1

            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         '{} Expected {} seen on all UHC/Rail combinations for iteration:{}.'.format(rail_type, expected_hclc_alarm, test_iteration+1))
            else:
                self.Log('error',
                         '{} Expected {} seen on {} of {} UHC/Rail combinations.'.format(
                             rail_type, expected_hclc_alarm, pass_count, len(rail_uhc_tuple_list)))

        board.SetRailsToSafeState()

    def randomize_rail_uhc(self, board, rail_type):
        rail_uhc_tuple_list = []
        for rail in range(board.RAIL_COUNT[rail_type]):
            for uhc in range(board.UHC_COUNT):
                rail_uhc_tuple_list.append((rail, uhc))
        random.shuffle(rail_uhc_tuple_list)
        return rail_uhc_tuple_list


    def generate_non_alarm_condition_trigger_queue(self, board, rail_type, force_voltage, dutid, rail, uhc):
        tracking_voltage_offset = 2
        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage+tracking_voltage_offset
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]
        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_alarm_condition_trigger_queue(self, board, rail_type, dutid, rail, uhc, expected_hclc_alarm):
        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        if expected_hclc_alarm == 'ComparatorHighAlarm':
            tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS_FOR_HIGH_ALARM_TEST[rail_type][0]
            tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS_FOR_HIGH_ALARM_TEST[rail_type][1]
        elif expected_hclc_alarm == 'ComparatorLowAlarm':
            tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS_FOR_LOW_ALARM_TEST[rail_type][0]
            tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS_FOR_LOW_ALARM_TEST[rail_type][1]
        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generate_comparator_alarm_trigger_queue(tq_helper))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_comparator_alarm_trigger_queue(self, tq_helper):
        trigger_queue = ''
        voltage_limits = tq_helper.get_force_voltage_limits_string(tq_helper.force_rail, tq_helper.force_rail_type,
                                                                   tq_helper.force_low_voltage_limit,
                                                                   tq_helper.force_high_voltage_limit)
        trigger_queue = trigger_queue + '\n' + voltage_limits
        flush_clamps = tq_helper.get_flush_clamps(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + flush_clamps

        if  tq_helper.force_rail_type == 'HC' or tq_helper.force_rail_type == 'LC':
            set_vcomp_alarms = tq_helper.get_set_vcomp_alarms(tq_helper.force_rail, tq_helper.force_rail_type)
            trigger_queue = trigger_queue + '\n' + set_vcomp_alarms

        trigger_queue_with_header_footer = tq_helper.add_header_footer_to_trigger_queue(trigger_queue)
        return trigger_queue_with_header_footer

    def dut_comparator_alarm_scenario(self, board, rail_type, iterations, expected_alarm):
        force_rail_type = 'HV'
        dutid = 15
        hvRail = random.randint(0,7)
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, rail_type)

        board.SetRailsToSafeState()
        board.ResetVoltageSoftSpanCode(force_rail_type, dutid)
        board.ResetCurrentSoftSpanCode(force_rail_type, dutid)
        board.ResetVoltageSoftSpanCode(rail_type, dutid)

        for test_iteration in range(iterations):
            pass_count = 0
            for lvrail, uhc in rail_uhc_tuple_list:
                force_voltage = board.get_positive_random_voltage(FORCE_VOLTAGE_RANGE[rail_type][0],
                                                                  FORCE_VOLTAGE_RANGE[rail_type][1],
                                                                  VOLTAGE_STEPSIZE[rail_type])

                if expected_alarm == 'Low':
                    expected_dut_alarm = 'Rail{:02d}UnderVolt'.format(lvrail)
                else:
                    expected_dut_alarm = 'Rail{:02d}OverVolt'.format(lvrail)
                self.initialize_rail(board, lvrail, rail_type, uhc, dutid)
                board.ConfigureUhcRail(uhc, 0x1 << hvRail, force_rail_type)
                trigger_queue_data = self.generate_non_alarm_condition_lvm_trigger_queue(board, hvRail, lvrail, force_voltage, dutid, uhc)

                board.ConnectRailForce(hvRail)
                board.ConnectExternalLoad()
                board.ConnectLvRailSense(lvrail)
                cal_card_connect_delay = 0.01
                time.sleep(cal_card_connect_delay)
                board.initialize_ad5560_hv_force_rail_to_load_or_sense(hvRail)

                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, force_rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)

                global_alarms = board.get_global_alarms()
                if global_alarms != []:
                    self.Log('error', 'Global Alarm should be empty.Actual fields set: {}'.format(global_alarms))

                if expected_alarm == 'Low':
                    trigger_queue_data = self.generate_low_alarm_condition_lvm_trigger_queue(board, hvRail, lvrail, dutid, uhc)
                else:
                    trigger_queue_data = self.generate_high_alarm_condition_lvm_trigger_queue(board, hvRail, lvrail,
                                                                                             dutid, uhc)

                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)

                if board.check_for_single_lvm_rail_comparator_alarm(expected_dut_alarm) == True:
                   pass_count += 1

                board.ConnectLvRailSense(lvrail,0)

            board.ConnectRailForce(hvRail, 0)
            board.ConnectExternalLoad(0)

            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         '{} Expected \'UnderVolt\OverVolt\' seen on all DUT/Rail combinations for iteration:{}.'.format(rail_type, test_iteration+1))
            else:
                self.Log('error',
                         'Unexpected \'UnderVolt\OverVolt\' behavior seen on {} of {} DUR/Rail combinations during test iteration {} of {}.'.format(
                             len(rail_uhc_tuple_list)-pass_count, len(rail_uhc_tuple_list), test_iteration+1, iterations))

        board.create_cal_board_instance_and_initialize()
        board.SetRailsToSafeState()

    def initialize_rail(self, board, rail, rail_type, uhc, dutid):
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
        board.UnGangAllRails()
        if rail_type == 'LVM':
            board.initialize_lvm_rails()

    def generate_non_alarm_condition_lvm_trigger_queue(self, board, force_rail, sense_rail, force_voltage, dutid, uhc):
        tq_helper = TriggerQueueString(board,uhc,dutid)
        tq_helper.open_socket = False
        tracking_voltage_offset = 2
        rail_type = 'HV'

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = force_rail
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage + tracking_voltage_offset
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]

        rail_type = 'LVM'
        tq_helper.sense_rail_type = rail_type
        tq_helper.sense_rail = sense_rail
        tq_helper.sense_high_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][1]
        tq_helper.sense_low_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][0]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_low_alarm_condition_lvm_trigger_queue(self, board, force_rail, sense_rail, dutid, uhc):
        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.open_socket = False

        rail_type = 'HV'

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = force_rail
        tq_helper.force_rail_irange = I_RANGE[rail_type]

        rail_type = 'LVM'
        tq_helper.sense_rail_type = rail_type
        tq_helper.sense_rail = sense_rail
        tq_helper.sense_low_voltage_limit = VOLTAGE_LIMITS_FOR_LOW_ALARM_TEST[rail_type][0]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generate_dut_low_comparator_alarm_trigger_queue(tq_helper))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_high_alarm_condition_lvm_trigger_queue(self, board, force_rail, sense_rail, dutid, uhc):
        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.open_socket = False

        rail_type = 'HV'

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = force_rail
        tq_helper.force_rail_irange = I_RANGE[rail_type]

        rail_type = 'LVM'
        tq_helper.sense_rail_type = rail_type
        tq_helper.sense_rail = sense_rail

        tq_helper.sense_high_voltage_limit = VOLTAGE_LIMITS_FOR_HIGH_ALARM_TEST[rail_type][1]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generate_dut_high_comparator_alarm_trigger_queue(tq_helper))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_dut_low_comparator_alarm_trigger_queue(self, tq_helper):
        trigger_queue = ''
        if tq_helper.sense_rail != None:
            voltage_limits = tq_helper.get_sense_uv_voltage_limits_string(tq_helper.sense_rail, tq_helper.sense_rail_type,
                                                                       tq_helper.sense_low_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        flush_clamps = tq_helper.get_flush_clamps(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + flush_clamps

        trigger_queue_with_header_footer = tq_helper.add_header_footer_to_dut_trigger_queue(trigger_queue)
        return trigger_queue_with_header_footer



    def generate_dut_high_comparator_alarm_trigger_queue(self, tq_helper):
        trigger_queue = ''

        if tq_helper.sense_rail != None:
            voltage_limits = tq_helper.get_sense_ov_voltage_limits_string(tq_helper.sense_rail, tq_helper.sense_rail_type,
                                                                       tq_helper.sense_high_voltage_limit)
            trigger_queue = trigger_queue + '\n' + voltage_limits

        flush_clamps = tq_helper.get_flush_clamps(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + flush_clamps

        trigger_queue_with_header_footer = tq_helper.add_header_footer_to_dut_trigger_queue(trigger_queue)
        return trigger_queue_with_header_footer

class KelvinAlarms(BaseTest):

    def DirectedHCRailKelvinAlarmTest(self):
        rail_type = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.kelvin_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='KelvinAlarm')

    def DirectedLCRailKelvinAlarmTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.kelvin_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='KelvinAlarm')

    def DirectedHVRailKelvinAlarmTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.kelvin_alarm_scenario(board, rail_type, iterations=2, expected_hclc_alarm='KelvinAlarm')

    def kelvin_alarm_scenario(self, board, rail_type, iterations, expected_hclc_alarm):
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, rail_type)
        board.SetRailsToSafeState()
        for test_iteration in range(iterations):
            pass_count = 0
            disable_ad5560_int10k = 0xe280
            for rail, uhc in rail_uhc_tuple_list:
                force_voltage = board.get_positive_random_voltage(FORCE_VOLTAGE_RANGE[rail_type][0],
                                                                  FORCE_VOLTAGE_RANGE[rail_type][1],
                                                                  VOLTAGE_STEPSIZE[rail_type])
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.UnGangAllRails()


                if rail_type == 'HV':
                    dpsregister2_value_before_test = board.ReadAd5560Register(board.ad5560_registers.DPS_REGISTER2.ADDR,
                                                                              rail)
                else:
                    dpsregister2_value_before_test = board.ReadAd5560Register(board.ad5560regs.DPS_REGISTER2.ADDR,
                                                                          rail)

                trigger_queue_data = self.generate_non_alarm_condition_trigger_queue(board, rail_type, force_voltage,
                                                                                     dutid, rail, uhc)

                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)
                global_alarms = board.get_global_alarms()
                if global_alarms != []:
                    self.Log('error', 'Global Alarm should be empty.Actual fields set: {}'.format(global_alarms))
                    continue
                if rail_type == 'HV':
                    board.WriteAd5560Register(board.ad5560_registers.DPS_REGISTER2.ADDR, disable_ad5560_int10k, rail)
                trigger_queue_data = self.generate_kelvin_trigger_queue(board, rail_type, dutid,
                                                                        rail, uhc, kelvin_alarm_condition= 'False')
                board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
                board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, rail_type)
                time.sleep(ALARM_PROPAGATION_DELAY)

                if board.check_for_single_hclc_rail_alarm(rail, expected_hclc_alarm):
                    pass_count += 1

                if rail_type == 'HV':
                    board.WriteAd5560Register(board.ad5560_registers.DPS_REGISTER2.ADDR, dpsregister2_value_before_test,
                                              rail)
                else:
                    board.WriteAd5560Register(board.ad5560regs.DPS_REGISTER2.ADDR, dpsregister2_value_before_test,
                                              rail)

            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         '{} Expected {} seen on all UHC/Rail combinations for iteration:{}.'.format(rail_type,
                                                                                                     expected_hclc_alarm,
                                                                                                     test_iteration+1))
            else:
                self.Log('error',
                         '{} Expected {} seen on {} of {} UHC/Rail combinations.'.format(
                             rail_type, expected_hclc_alarm, pass_count, len(rail_uhc_tuple_list)))

        board.SetRailsToSafeState()

    def randomize_rail_uhc(self, board, rail_type):
        rail_uhc_tuple_list = []
        for rail in range(board.RAIL_COUNT[rail_type]):
            for uhc in range(board.UHC_COUNT):
                rail_uhc_tuple_list.append((rail, uhc))
        random.shuffle(rail_uhc_tuple_list)
        return rail_uhc_tuple_list

    def generate_non_alarm_condition_trigger_queue(self, board, rail_type, force_voltage, dutid, rail, uhc):
        tq_helper = TriggerQueueString(board, uhc, dutid)
        if rail_type == 'HV':
            # tq_helper.tracking_voltage = force_voltage + tracking_voltage_offset
            tq_helper.tracking_voltage = force_voltage
            tq_helper.free_drive = True
            tq_helper.force_free_drive_high = 0.5
            tq_helper.force_free_drive_low = -.5
            tq_helper.force_free_drive_delay = 6000

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.force_voltage = force_voltage
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = NO_ALARM_VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_kelvin_trigger_queue(self, board, rail_type, dutid, rail, uhc, kelvin_alarm_condition):
        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.open_socket = kelvin_alarm_condition
        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generate_trigger_queue_string(tq_helper))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_trigger_queue_string(self, tq_helper):
        trigger_queue = ''
        disable_rail = tq_helper.get_disable_rail(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + disable_rail
        if tq_helper.open_socket == 'True':
            enable_disable_sense = tq_helper.get_trigger_queue_enable_disable_sense(tq_helper.force_rail,
                                                                                    tq_helper.force_rail_type, 1)
        else:
            enable_disable_sense = tq_helper.get_trigger_queue_enable_disable_sense(tq_helper.force_rail,
                                                                                    tq_helper.force_rail_type, 0)
        trigger_queue = trigger_queue + '\n' + enable_disable_sense
        enable_rail = tq_helper.get_enable_rail(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue = trigger_queue + '\n' + enable_rail
        trigger_queue_with_header_footer = tq_helper.add_header_footer_to_trigger_queue(trigger_queue)
        return trigger_queue_with_header_footer
