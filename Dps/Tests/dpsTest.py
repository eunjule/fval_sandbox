# Copyright 2017 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

from Common.fval import TestCase
from Dps.dpsEnv import DpsEnv
import traceback

class BaseTest(TestCase):
    dutName = 'Dps'

    def setUp(self):
        try:
            self.LogStart()
            self.env = DpsEnv(self)
        except Exception as e:
            exceptionMsg = traceback.format_exc().split('\n')
            for line in exceptionMsg:
                self.Log('error', line)
            self.LogEnd()

    def tearDown(self):
        self.LogEnd()
