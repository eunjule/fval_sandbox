################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import time
from Dps.Tests.dpsTest import BaseTest

ALARM_PROPAGATION_DELAY = 0.1

class RailBusyAlarm(BaseTest):

    def DirectedHvRailBusyAlarmTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.rail_busy_scenario(board, rail_type)

    def DirectedLvmRailBusyAlarmTest(self):
        rail_type = 'LVM'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.rail_busy_scenario(board,rail_type)

    def rail_busy_scenario(self, board, rail_type,test_iterations=1):
        rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
        resourceid = getattr(board.symbols.RESOURCEIDS, rail_type)
        dutid = random.randint(0, 63)
        for iterations in range(test_iterations):
            pass_count = 0
            for rail, uhc in rail_uhc_tuple_list:
                rail_mask = 0x1 << rail
                board.ClearDpsAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV':
                    board.ResetVoltageSoftSpanCode(rail_type, dutid)
                    board.ResetCurrentSoftSpanCode(rail_type, dutid)
                elif rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type, dutid)

                global_alarms = board.get_global_alarms()
                if global_alarms != []:
                    self.Log('error', '{} rail {} Unexpected Alarms received {}'.format(rail_type, rail,global_alarms))
                else:
                    try:
                        self.verify_fold_status(board, rail_type)
                        board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                        self.verify_rails_busy_folded_register(board,rail,rail_type)
                        board.WriteBar2RailCommand(board.symbols.RAILCOMMANDS.SET_RAIL_BUSY, rail_mask, resourceid)
                        time.sleep(ALARM_PROPAGATION_DELAY)
                        self.verify_rail_busy_alarm(board,rail_type,rail)
                        pass_count+= 1
                        board.WriteTQFooterViaBar2(dutid, rail, rail_type)
                    except self.RailBusyException:
                        pass
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info', 'Expected Rail busy behavior seen on all {} UHC/Rail combinations iteration {} of {}'.format(
                    len(rail_uhc_tuple_list),iterations+1,test_iterations))
            else:
                self.Log('error', 'Unexpected Rail busy behavior seen on {} of {} UHC/Rail combinations during test iteration {} of {}'.format(
                    len(rail_uhc_tuple_list) - pass_count, len(rail_uhc_tuple_list),iterations+1,test_iterations))
        board.SetRailsToSafeState()

    def verify_fold_status(self, board,rail_type):
        if rail_type == 'HV':
            expected_hclc_rail_busy_fold_value = 0xFF
            board.WriteRegister(board.registers.HCLC_RAILS_FOLDED(value=0xFF))
            hc_lc_rails_busy_register_status_before_test = board.ReadRegister(board.registers.HCLC_RAILS_FOLDED).value
        else:
            expected_hclc_rail_busy_fold_value = 0x3FF
            board.WriteRegister(board.registers.DUT_RAILS_FOLDED(value=0x3FF))
            hc_lc_rails_busy_register_status_before_test = board.ReadRegister(board.registers.DUT_RAILS_FOLDED).value

        if hc_lc_rails_busy_register_status_before_test != expected_hclc_rail_busy_fold_value:
            self.Log('error','Unexpected rails unfolded.Fold status {:x}.'.format(hc_lc_rails_busy_register_status_before_test))
            raise self.RailBusyException

    def verify_rail_busy_alarm(self,board,rail_type,rail):
        if rail_type == 'HV':
            if board.check_for_single_hclc_rail_alarm(rail, 'RailBusyAlarm') == False:
                raise  self.RailBusyException
        else:
            if board.check_for_single_dut_rail_alarm(rail,'DutRailBusyAlarm') == False:
                raise self.RailBusyException

    def verify_rails_busy_folded_register(self,board,rail,rail_type):
        if rail_type == 'HV':
            expected_hclc_rail_busy_fold_value = (1<<(rail+16)) | (0xFF ^ (1<<rail))
            board.WriteRegister(board.registers.HCLC_RAILS_FOLDED(value=0xFF))
            hc_lc_rails_busy_register_status_before_test = board.ReadRegister(board.registers.HCLC_RAILS_FOLDED).value
        else:
            expected_hclc_rail_busy_fold_value = (1 << (rail + 16)) | (0x3FF ^ (1 << rail))
            board.WriteRegister(board.registers.DUT_RAILS_FOLDED(value=0x3FF))
            hc_lc_rails_busy_register_status_before_test = board.ReadRegister(board.registers.DUT_RAILS_FOLDED).value
        if hc_lc_rails_busy_register_status_before_test != expected_hclc_rail_busy_fold_value:
            self.Log('error','Unexpected rails busy behavior observed.Rail Busy Fold status {:x}.'.format(hc_lc_rails_busy_register_status_before_test))
            raise self.RailBusyException


    class RailBusyException(Exception):
        pass

