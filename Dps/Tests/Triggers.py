################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: 
#-------------------------------------------------------------------------------
#     Purpose: 
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 10/29/15
#       Group: HDMT FPGA Validation
################################################################################

import random
from string import Template
import struct
import time
import unittest
from Common import configs
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest
from Common import hilmon as hil

TRIGGER_BUFFER_SIZE = 200
TRIGGER_BUFFER_LAST_10_VALUES = 10
INSERT_DELAY_COUNT = 1000
INITIALIZE_TO_ZERO = 0
TRIGGER_FROM_EXTERNAL_SLOT = 0x0008002
EMPTY_TRIGGER_BUFFER_VALUE = 0x00000000


class TriggerFlow(BaseTest):

    def DirectedExhaustiveTriggerFromDpsToRcTest(self):
        self.empty_trigger_buffer_and_ddr_for_slot_and_subslot()
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HV','LC','HC']):
            board.WriteRegister(board.getResetsRegister()(value = INITIALIZE_TO_ZERO))
            for trigger in range(10000):
                random_trigger_value = self.create_random_software_triggers()
                self.send_trigger_from_dps_verify_at_rc(random_trigger_value, board)

    def create_random_software_triggers(self):
        trigger_bit_7_11_mask = 0x08FFFFFF
        invalid_card_type_trigger = 0xdc000000
        random_trigger_value = random.getrandbits(32) & trigger_bit_7_11_mask  # Trigger from DPS to RC should not have
        random_trigger_value = random_trigger_value | invalid_card_type_trigger  # to avoid valid card type
        random_trigger_value = random_trigger_value | invalid_card_type_trigger  # to avoid valid card type
        return random_trigger_value

    def DirectedExhaustiveTriggerFromRcToDpsQueueTest(self):
        self.empty_trigger_buffer_and_ddr_for_slot_and_subslot()
        for test_loop in range(20):
            for board in self.env.duts_or_skip_if_no_vaild_rail(['HV', 'LC', 'HC']):
                trigger_list = []
                board.WriteRegister(board.getResetsRegister()(value=INITIALIZE_TO_ZERO))
                board.ClearDpsAlarms()
                board.SetRailsToSafeState()
                board.empty_trigger_buffer()
                self.create_and_send_random_triggers_from_rc(trigger_list)
                self.read_and_verify_triggers_at_dps(board, trigger_list)
                self.confirm_trigger_buffer_is_empty(board)

    def empty_trigger_buffer_and_ddr_for_slot_and_subslot(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['HV', 'LC', 'HC']):
            board.empty_trigger_buffer()
            board.empty_ddr_for_uhc_zero()
            board.DisableAllUhcs()

    def create_and_send_random_triggers_from_rc(self, trigger_list):
        for trigger in range(TRIGGER_BUFFER_SIZE):
            random_trigger_value = self.create_random_dps_triggers()
            self.env.send_trigger(random_trigger_value)
            counter = INITIALIZE_TO_ZERO
            for delay in range(INSERT_DELAY_COUNT):
                counter = counter + 1
            self.Log('debug', 'RC sent trigger 0x{:x}'.format(random_trigger_value))
            trigger_list.append(random_trigger_value)

    def create_random_dps_triggers(self):
        trigger_bit_7_to_11_and_26_mask = 0x040F4000
        trigger_bit_26_card_type_mask = 0x04000000
        random_trigger_value = random.getrandbits(32) & trigger_bit_7_to_11_and_26_mask  # Note: Trigger from DPS to RC should not have
        random_trigger_value = random_trigger_value | trigger_bit_26_card_type_mask  # bits[11:7] set to 1F. Trigger wont be sent
        return random_trigger_value

    def read_and_verify_triggers_at_dps(self, board, trigger_list):
        for trigger in trigger_list:
            self.read_trigger_rc_to_dps(trigger, board)

    def confirm_trigger_buffer_is_empty(self, board):
        for trigger in range(TRIGGER_BUFFER_LAST_10_VALUES):
            self.read_trigger_rc_to_dps(EMPTY_TRIGGER_BUFFER_VALUE, board)

    def read_trigger_rc_to_dps (self, expected, board):
        trigger_received_at_dps = board.ReadRegister(board.getTriggerDownTestRegister())
        global_register_value = board.ReadRegister(board.getGlobalAlarmRegisterType())
        self.Log('debug', 'Global Register Value {} Trigger received at Dps {} expected trigger value {}'.format(hex(global_register_value.value),
                                                                                                                 hex(trigger_received_at_dps.value),
                                                                                                                 hex(expected)))
        if trigger_received_at_dps.value == expected:
            self.Log('debug', 'DPS({}, {}) received trigger 0x{:x} expected=0x{:x} '.format(board.slot, board.subslot,
                                                                                           trigger_received_at_dps.value,
                                                                                           expected))
        else:
            if (trigger_received_at_dps.value & TRIGGER_FROM_EXTERNAL_SLOT) == TRIGGER_FROM_EXTERNAL_SLOT:
                if global_register_value.value != 0:
                    self.Log('error', 'Global Register Value {}'.format(hex(global_register_value.value)))
                trigger_received_at_dps = board.ReadRegister(board.getTriggerDownTestRegister())
            if trigger_received_at_dps.value != expected:
                self.Log('error', 'DPS skipped invalid trigger from external board still received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected,
                                                                                                                                                            trigger_received_at_dps.value))
            else:
                self.Log('debug', 'DPS skipped invalid trigger from external board ({}, {}) received trigger 0x{:x} expected=0x{:x} '.format(board.slot, board.subslot,
                                                                                                                                            trigger_received_at_dps.value,
                                                                                                                                            expected))

    def send_trigger_from_dps_verify_at_rc(self, expected_trigger_value, board):
        board.WriteRegister(board.getTriggerUpTestRegister()(value = expected_trigger_value))
        counter = INITIALIZE_TO_ZERO
        for delay in range(0, INSERT_DELAY_COUNT):
            counter = counter + 1
        self.Log('debug', '({}, {}) sent trigger 0x{:x} to RC'.format(board.slot, board.subslot, expected_trigger_value))
        trigger_value = self.env.read_trigger_up(board.slot)
        if trigger_value != expected_trigger_value:
            global_register_value = board.ReadRegister(board.getGlobalAlarmRegisterType())
            self.Log('error', 'RC received incorrect trigger: Trigger received at RC:{} Expected trigger value:{} Global Register Value:{}'.format(
                hex(trigger_value),
                hex(expected_trigger_value),
                hex(global_register_value.value)))
        else:
            self.Log('debug', 'RC received trigger: 0x{:x}'.format(trigger_value))


class TCLooping(BaseTest):

    @unittest.skip('Test needs to be fixed for stability issues')
    def MiniTriggerFilterTest(self):
        for slot, subslot in self.env.fpgas:
            self.Log('debug', 'Testing slot {}, subslot {}'.format(slot, subslot))
            board = self.env.instruments[slot].subslots[subslot]
            board.SetRailsToSafeState()
            board.Write('ENABLE_ALARMS', 0x1)
            lutdwordaddress = 0x10000000
            for dutid in range (8):
                dut_domain_id_register_type = board.getDutDomainIdRegisterType()
                #Virtualdutid = 'DUT_DOMAIN_ID' + str(dutid)
                data = board.ReadRegister(dut_domain_id_register_type,index=dutid)
                dut_domain_id_register_obj = dut_domain_id_register_type()
                #data = board.Read(Virtualdutid)
                data.DutValidBit = 1
                board.WriteRegister(dut_domain_id_register_obj, index=dutid)
                rails = 1 << dutid
                if subslot == 0:
                    board.WriteRegister(board.registers.VlcUhcRailConfig(value =rails),index=dutid)
                board.WriteRegister(board.registers.HclcDutIdRailConfig(value =rails),index=dutid)

                #triggerValue = 0x4000000
                # The global alarm register as write clear bits, need to clear tq notify alarm for all dut before enabling any dut    
                asm = TriggerQueueAssembler()
                asm.symbols['DUTID'] = 0x1000 + dutid
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 1 + 2 * dutid
                asm.LoadString("""\
                    Begin
                    TqNotify rail=0, value=END
                    TqNotify rail=16, value=END
                    TqComplete rail=0, value=0
                """)
                data = asm.Generate()
                self.Log('debug', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
                offset = 0x100 * dutid

                self.Log('debug', 'Offset Address is 0x{:x}'.format(offset))
                board.WriteTriggerQueue(offset, data)
                data =  board.DmaRead(offset, 0x20)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000)
                self.Log('debug', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L',offset))
                data =  board.DmaRead(lutDwordAddr, 0x20)
                board.ReadMemory(data)
        
        for slot, subslot in self.env.fpgas:
            self.Log('debug','Sending the triggers for slot:{} subslot:{}'.format(slot, subslot))
            board = self.env.instruments[slot].subslots[subslot]
            board.ClearGlobalAlarms()

            while (hil.dpsBarRead(slot, 0, 1, 0x54) != 0x0 or hil.dpsBarRead(slot, 1, 1, 0x54) != 0x0):
                pass
            
            triggerValue = hil.dpsBarRead(slot, subslot, 1, 0x54)
            self.Log('debug', 'HDDPS received trigger: expected=0, actual=0x{:x}'.format(triggerValue))

            self.send_trigger_rc_to_dps(0x04000000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04000000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04000000, board)
            board.CheckTqNotifyAlarm(0)
            board.ClearGlobalAlarms()
            self.send_trigger_rc_to_dps(0x04000010)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04000010, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04000010, board)
            self.send_trigger_rc_to_dps(0x04000a00)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04000a00, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04000a00, board)

            self.send_trigger_rc_to_dps(0x04100000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04100000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04100000, board)
            board.CheckTqNotifyAlarm(1)
            board.ClearGlobalAlarms()

            self.send_trigger_rc_to_dps(0x04800000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04800000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04800000, board)
            self.send_trigger_rc_to_dps(0x04200000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04200000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04200000, board)
            board.CheckTqNotifyAlarm(2)
            board.ClearGlobalAlarms()

            self.send_trigger_rc_to_dps(0x04300000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04300000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04300000, board)
            board.CheckTqNotifyAlarm(3)
            board.ClearGlobalAlarms()

            self.send_trigger_rc_to_dps(0x04400000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04400000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04400000, board)
            board.CheckTqNotifyAlarm(4)
            board.ClearGlobalAlarms()

            self.send_trigger_rc_to_dps(0x04500000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04500000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04500000, board)
            board.CheckTqNotifyAlarm(5)
            board.ClearGlobalAlarms()

            self.send_trigger_rc_to_dps(0x07100000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x07100000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x07100000, board)
            self.send_trigger_rc_to_dps(0x04600000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04600000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04600000, board)
            board.CheckTqNotifyAlarm(6)
            board.ClearGlobalAlarms()

            self.send_trigger_rc_to_dps(0x04700000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x04700000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x04700000, board)
            board.CheckTqNotifyAlarm(7)
            board.ClearGlobalAlarms()
            self.send_trigger_rc_to_dps(0x07f00000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0x07f00000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0x07f00000, board)

            self.Log('debug', 'Testing LTC triggers')
            self.send_trigger_rc_to_dps(0xFC000000)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 0, 0xFC000000, board)
            TriggerFlow.read_trigger_rc_to_dps(self, slot, 1, 0xFC000000, board)
            board.CheckTqNotifyAlarm(0)
            board.ClearGlobalAlarms()
        
        for num_reads in range(10):
            hil.dpsBarRead(slot, 0, 1, 0x54)
            hil.dpsBarRead(slot, 1, 1, 0x54)
        
        # Try to read extra triggers and all should be 0 because in this case the FIFO should be empty
        for slot, subslot in self.env.fpgas:
            board = self.env.instruments[slot].subslots[subslot]
            TriggerFlow.read_trigger_rc_to_dps(self, slot, subslot, 0x00000000, board)

    def send_trigger_rc_to_dps(self,value):
        self.env.send_trigger(value)
        counter = INITIALIZE_TO_ZERO
        for delay in range(INSERT_DELAY_COUNT):
            counter = counter + 1
        self.Log('debug', 'RC sent trigger 0x{:x}'.format(value))

    @unittest.skip('Test needs to be fixed for stability issues')
    def MiniTriggerExecAlarmTest(self):
        for slot, subslot in self.env.fpgas:
            self.Log('debug', 'Testing slot {}, subslot {}'.format(slot, subslot))
            board = self.env.instruments[slot].subslots[subslot]
            board.SetRailsToSafeState()
            board.Write('ENABLE_ALARMS', 0x1)
            lutdwordaddress = 0x10000000
            dutid = 0
            rail=0
            virtual_dut_domain_id_register = board.ReadRegister(board.getDutDomainIdRegisterType(), index=dutid)
            virtual_dut_domain_id_register.DutValidBit = 1
            board.WriteRegister(virtual_dut_domain_id_register, index=dutid)
            # Virtualdutid = 'DUT_DOMAIN_ID' + str(dutid)
            # data = board.Read(Virtualdutid)
            # data.DutValidBit = 1
            # board.Write(Virtualdutid, data)
            
            while (hil.dpsBarRead(slot, 0, 1, 0x54) != 0x0 or hil.dpsBarRead(slot, 1, 1, 0x54) != 0x0):
                pass

            if subslot == 1:
                board.ConfigureUhcRail(dutid, 0x1 << rail,'HC')
            else:
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')
            #triggerValue = 0x4000000
            # The global alarm register as write clear bits, need to clear tq notify alarm for all dut before enabling any dut    
            asm = TriggerQueueAssembler()
            asm.symbols['DUTID'] = 0x1000 + dutid
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 1 + 2 * dutid
            if subslot == 1:
               asm.symbols['RAIL'] = 16+rail 
            else:
               asm.symbols['RAIL'] = rail
            asm.LoadString("""\
                Begin
                TimeDelay rail=RAIL, value=1000
                TimeDelay rail=RAIL, value=2000
                TimeDelay rail=RAIL, value=3000
                TqNotify rail=0, value=END
                TqNotify rail=16, value=END
                TqComplete rail=0, value=0
            """)
            data = asm.Generate()
            self.Log('debug', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
            offset = 0x100 * dutid

            self.Log('debug', 'Offset Address is 0x{:x}'.format(offset))
            board.WriteTriggerQueue(offset, data)
            data =  board.DmaRead(offset, 0x20)
            board.ReadMemory(data)

            lutDwordAddr = lutdwordaddress + (dutid * 0x10000)
            self.Log('debug', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
            board.DmaWrite(lutDwordAddr, struct.pack('<L',offset))
            
            data =  board.DmaRead(lutDwordAddr, 0x20)
            board.ReadMemory(data)
        
            self.Log('debug','Sending the triggers for slot:{} subslot:{}'.format(slot, subslot))
            board = self.env.instruments[slot].subslots[subslot]
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
                        
            self.env.send_trigger(0x04000000)
            self.env.send_trigger(0x04000000)
            
            globalalarm = board.Read('GLOBAL_ALARMS')
            if globalalarm.TrigExecAlarm == 1:
                self.Log('debug', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot,subslot,globalalarm.Pack()))
                trigexecalarm = board.Read('TrigExecAlarm')
                trigwhileuhcbusy = 'TrigWhileUHC' + str(dutid) +'BusyAlarm'
                if getattr(trigexecalarm, trigwhileuhcbusy) == 1:
                    self.Log('debug', 'The trigexecalarm register for dut id {} is set to 0x{:x} as expected'.format(dutid, trigexecalarm.Pack()))
                else:
                    self.Log('error', 'The trig while uhc busy is not set for the appropriate dut id')
            else:
                self.Log('error', 'The trig while uhc busy alarm should get set. Actual -> 0x{:x}'.format(globalalarm.Pack()))
            
            for num_reads in range(10):
                hil.dpsBarRead(slot, 0, 1, 0x54)
                hil.dpsBarRead(slot, 1, 1, 0x54)
            
            # Try to read extra triggers and all should be 0 because in this case the FIFO should be empty
            for slot, subslot in self.env.fpgas:
                TriggerFlow.read_trigger_rc_to_dps(self, slot, subslot, 0x00000000, board)
            
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()