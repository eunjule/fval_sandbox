################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import random
import time

from Common.instruments.dps import symbols
from Dps.Tests.dpsTest import BaseTest
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString


trigger_queue_offset = 0x0
ALARM_PROPAGATION_DELAY = 0.1
TRIGGER_QUEUE_OFFSET =0x0
SETTLING_TIME_DELAY = 0.02

class CurrentCalibration(BaseTest):

    class CalibrationException(Exception):
        pass

    def setup_steady_state(self, board, dutId, hvForceVoltage, hvRail, uhc):
        trigger_queue_data = self.generate_force_voltage_trigger_queue(board, dutId, uhc, hvRail, hvForceVoltage)
        board.WriteTriggerQueue(TRIGGER_QUEUE_OFFSET, trigger_queue_data)
        board.ExecuteTriggerQueue(TRIGGER_QUEUE_OFFSET, uhc, 'HV')

    def generate_force_voltage_trigger_queue(self, board, dutId, uhc, hv_rail, force_voltage):
        hv_i_range = 'I_7500_MA'
        hv_clamp_high = 1.5
        hvClampLow = -0.5
        hv_over_voltage = 18
        hvUnderVoltage = -3

        hv_tq_string = TriggerQueueString(board.hvdps.subslots[0],uhc,dutId)

        hv_tq_string.force_rail_type = 'HV'
        hv_tq_string.force_rail = hv_rail
        hv_tq_string.force_rail_irange = hv_i_range
        hv_tq_string._force_voltage = force_voltage
        hv_tq_string.force_clamp_low = hvClampLow
        hv_tq_string.force_clamp_high = hv_clamp_high
        hv_tq_string.tracking_voltage = force_voltage +2
        hv_tq_string.force_low_voltage_limit = hvUnderVoltage
        hv_tq_string.force_high_voltage_limit = hv_over_voltage
        hv_tq_string.free_drive = True
        hv_tq_string.force_free_drive_high = 7.5
        hv_tq_string.force_free_drive_low = -.5
        hv_tq_string.force_free_drive_delay = 6000


        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(hv_tq_string.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data


    def reset_calibration_gain_and_offset(self, board,channel):
        ref_offset = 0x8000
        ref_gain = 0x8000
        turbo_adc_i_cal_data = board.registers.CHANNEL_TURBO_CURRENT_ADC_GAIN_OFFSET()
        turbo_adc_i_cal_data.Offset = ref_offset
        turbo_adc_i_cal_data.Gain = ref_gain
        board.WriteRegister(turbo_adc_i_cal_data, index=channel)
        regular_adc_i_cal_data = board.registers.CHANNEL_REGULAR_CURRENT_ADC_GAIN_OFFSET()
        regular_adc_i_cal_data.Offset = ref_offset
        regular_adc_i_cal_data.Gain = ref_gain
        board.WriteRegister(regular_adc_i_cal_data,index=channel)

    def set_calibration_gain_offset(self, board, gain, offset, channel,turbo=False):
        if turbo == True:
            adc_i_cal_data = board.registers.CHANNEL_TURBO_CURRENT_ADC_GAIN_OFFSET()
        else:
            adc_i_cal_data = board.registers.CHANNEL_REGULAR_CURRENT_ADC_GAIN_OFFSET()
        adc_i_cal_data.Offset = offset
        adc_i_cal_data.Gain = gain
        board.WriteRegister(adc_i_cal_data, index=channel)


    def get_real_current_calibration_gain_and_offset(self, selected_gain, selected_offset):
        calibration_offset_real = (selected_offset - 0x8000) / (2 ** 16)
        calibration_gain_real = (selected_gain + 0x8000) / (2 ** 16)
        return calibration_gain_real, calibration_offset_real


    def get_random_gain_offset_list(self):
        gain_offset_tuple_list = []
        gain_list = [0x0000, 0x4000, 0x8000, 0x3FFF, 0xFFFF]
        random.shuffle(gain_list)
        offset_list = [0x0000, 0x4000, 0x8000, 0x3FFF, 0xFFFF]
        random.shuffle(offset_list)
        for gain in gain_list:
            for offset in offset_list:
                gain_offset_tuple_list.append((gain, offset))
        random.shuffle(gain_offset_tuple_list)
        return gain_offset_tuple_list


    def verify_rail_voltage(self,board,rail,rail_type,uhc,expected_voltage):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        allowed_voltage_deviation = abs((30/100)*expected_voltage)
        if board.is_in_maximum_allowed_deviation(expected_voltage, rail_voltage, allowed_voltage_deviation) == False  :
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V is not within the allowable deviation of {:.2f}V'.format(
                         rail_type, rail, uhc, rail_voltage, expected_voltage))
            raise self.CalibrationException


    def verify_expected_current(self, board, rail, uhc, rail_type, expected_current,actual_current):
        allowed_current_deviation = abs((40 / 100) * expected_current)
        fail_count =0
        expected_fail_count =5
        actual_current = round(actual_current,4)
        expected_current = round(expected_current,4)
        if board.is_in_maximum_allowed_deviation(expected_current, actual_current,allowed_current_deviation) == False:
            fail_count += 1
            if fail_count<= expected_fail_count:
                self.Log('warning','{} rail:{} uhc:{}, instantaneous current is {:.4f}I is not within the allowable deviation of {} of {:.4f}I'.format(
                        rail_type, rail, uhc, actual_current, allowed_current_deviation, expected_current))
            else:
                self.Log('error',
                         '{} rail:{} uhc:{}, instantaneous current is {:.4f}I is not within the allowable deviation of {} of {:.4f}I'.format(
                             rail_type, rail, uhc, actual_current, allowed_current_deviation, expected_current))
                raise self.CalibrationException


    def DirectedRloadRegularCurrentCalibrationTest(self):
        dutId = 15
        force_rail_type = 'HV'
        sense_rail_type = 'RLOAD'
        force_voltage = 3
        self.rload_current_calibration_scenario(dutId, force_rail_type, force_voltage, sense_rail_type)

    def DirectedRloadTurboCurrentCalibrationTest(self):
        dutId = 15
        force_rail_type = 'HV'
        sense_rail_type = 'RLOAD'
        force_voltage = 3
        self.rload_current_calibration_scenario(dutId, force_rail_type, force_voltage, sense_rail_type,turbo=True)

    def rload_current_calibration_scenario(self, dutId, force_rail_type, force_voltage, sense_rail_type,turbo =False):
        for board in self.env.duts_or_skip_if_no_vaild_rail(sense_rail_type):
            hvdps = board.hvdps.subslots[0]
            hvil = board
            pass_count = 0
            rail_uhc_tuple_list = hvil.randomize_rail_uhc(sense_rail_type)
            hv_rail = random.randint(0, 7)
            gain_offset_tuple_list = self.get_random_gain_offset_list()
            for channel, uhc in rail_uhc_tuple_list:
                if turbo == True:
                    hvil.WriteRegister(hvil.registers.TURBO_MODE_CURRENT_SAMPLE_CHANNEL_SELECT(value=0x1 << channel))
                hvdps.SetRailsToSafeState()
                hvdps.ClearDpsAlarms()
                hvdps.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
                hvdps.ConfigureUhcRail(uhc, 0x1 << hv_rail, force_rail_type)
                hvdps.UnGangAllRails()

                hvil.SetRailsToSafeState()
                hvil.SetDefaultVoltageComparatorLimits()
                hvil.SetDefaultCurrentClampLimit()
                hvil.SetDefaultThermalCurrentClampLimit()
                hvil.ClearDpsAlarms()
                hvil.EnableOnlyOneUhc(uhc, dutdomainId=dutId)
                hvil.ConfigureUhcRail(uhc, 0x1 << channel, sense_rail_type)
                hvil.WriteTQHeaderViaBar2(dutId, channel, sense_rail_type)

                hvdps.ConnectRailForce(hv_rail)
                hvdps.ConnectRailSense(hv_rail)
                hvdps.ConnectExternalLoad()

                hvil.ConnectRailForce(channel)

                self.reset_calibration_gain_and_offset(hvil,channel)
                hvil.setup_load_control(channel)
                self.setup_steady_state(hvdps, dutId, force_voltage, hv_rail, uhc)
                time.sleep(ALARM_PROPAGATION_DELAY)
                hvdps_global_alarms = hvdps.get_global_alarms()
                hvil_global_alarms = hvil.get_global_alarms()
                if hvdps_global_alarms != [] or hvil_global_alarms != []:
                    self.Log('error',
                             '{} rail {} /{} Channel {},Steady State conditions not achieved.Received MB{} DB{}'.format(
                                 force_rail_type, hv_rail, sense_rail_type, channel, hvdps_global_alarms,
                                 hvil_global_alarms))
                else:
                    try:
                        uncalibrated_current = board.get_rail_current(channel,turbo)
                        self.verify_rail_voltage(hvdps, hv_rail, force_rail_type, uhc, force_voltage)
                        self.verify_rail_voltage(hvil, channel, sense_rail_type, uhc, force_voltage)
                        for encoded_gain, encoded_offset in gain_offset_tuple_list:
                            self.set_calibration_gain_offset(hvil, encoded_gain, encoded_offset, channel,turbo)
                            time.sleep(SETTLING_TIME_DELAY)
                            calibrated_current = board.get_rail_current(channel,turbo)
                            calibration_gain_real, calibration_offset_real = self.get_real_current_calibration_gain_and_offset(
                                encoded_gain, encoded_offset)
                            expected_calibrated_current = (uncalibrated_current * calibration_gain_real) + calibration_offset_real
                            self.verify_expected_current(board, channel, uhc, sense_rail_type, expected_calibrated_current,calibrated_current)
                            pass_count += 1
                    except self.CalibrationException:
                        pass
                hvil.load_disconnect(channel)
                hvil.ConnectRailForce(channel, False)
                hvdps.cal_disconnect_force_sense_lines(hv_rail, force_rail_type)
            if pass_count == (len(rail_uhc_tuple_list) * len(gain_offset_tuple_list)):
                self.Log('info',
                         '{} Expected Current ADC Calibration seen on {} UHC/Rail combinations.'.format(sense_rail_type, len(
                             rail_uhc_tuple_list)))
            else:
                self.Log('error',
                         '{} Expected Current ADC Calibration seen on {} of {} UHC/Rail combinations'.format(sense_rail_type,
                                                                                                             int(
                                                                                                                 pass_count / (
                                                                                                                 len(
                                                                                                                     gain_offset_tuple_list))),
                                                                                                             len(
                                                                                                                 rail_uhc_tuple_list)))
            hvdps.ConnectExternalLoad(False)
            hvdps.create_cal_board_instance_and_initialize()
            hvdps.SetRailsToSafeState()