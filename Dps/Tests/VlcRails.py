################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: 
# -------------------------------------------------------------------------------
#     Purpose: 
# -------------------------------------------------------------------------------
#  Created by: Renuka Agrawal
#        Date: 1/27/16
#       Group: HDMT FPGA Validation
################################################################################

import random
from string import Template
import struct
import time

from Common.instruments.dps.symbols import RAILCOMMANDS
from Common.instruments.dps.symbols import ISL55180
from Common.instruments.dps import hddpsSubslot
from Common.instruments.dps.symbols import DPSTRIGGERS
from Common.instruments.dps.symbols import EVENTTRIGGER
from Common.instruments.dps.symbols import CROSSBOARDTRIGGER
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest
from Common import hilmon as hil
import unittest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000
DPS_NUM_ISL55180_RAILS = 16
DPS_NUM_AD5560_RAILS = 10


class Conditions(BaseTest):
    ## ForceVoltage

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedForceVoltageonVlcVsimTest(self):
        railCount = 16
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.VlcForceVoltageVsimScenario(rail, uhc, board)

    def VlcForceVoltageVsimScenario(self, rail, uhc, board):

        self.Log('info', '\nTesting VLC rail {} on uhc {}...'.format(rail, uhc))
        board.SetRailsToSafeState()
        if rail < 10:
            self.Log('info', 'Connecting channel VLC0{}'.format(rail))
            board.ConnectCalBoard('VLC0{}'.format(rail), 'NONE')
        else:
            self.Log('info', 'Connecting channel VLC{}'.format(rail))
            board.ConnectCalBoard('VLC{}'.format(rail), 'NONE')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearVlcRailAlarm()
        board.ClearVlcSampleAlarms()
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)
        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'VLC')

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'VLC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'VLC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'VLC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 2 * dutid + 1
        asm.LoadString(Template("""\
            $triggerqueueheader
            SetMode rail=RAIL, value=VFORCE
            EnableDisableRail rail=RAIL, value=0
            SetCurrentRange rail=RAIL, value=I_256_MA
            SetIClampHi rail=RAIL, value=0.25
            SetIClampLo rail=RAIL, value=-0.25
            SetVoltage rail=RAIL, value=1.5
            EnableDisableRail rail=RAIL, value=1
            $sampleengine
            $triggerqueuefooter
            TqComplete rail=0, value=0
        """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(uhc, 0xEB00, 0x200, 0x0),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header))
        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)
        board.ExecuteTriggerQueue(offset, uhc, 'VLC')

        railvoltage = 'VlcRail' + str(rail) + 'V'
        self.Log('info', 'The VLC rail {} voltage reg is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))

        board.CheckVlcSamplingActive(uhc)
        board.CheckVlcSampleAlarms()

        board.RunCheckers( uhc, 'ResistiveLoopback', 1e6, 'VLC')

        alarm = board.CheckVlcRailAlarm()
        if alarm:
            board.ClearVlcRailAlarm()

            ## ForceCurrent

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedForceCurrentVlcTest(self):
        railCount = 16
        uhc_count = 8
        rail_uhc_tuple_list = []

        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            for rail in range(railCount):
                for uhc in range(uhc_count):
                    rail_uhc_tuple_list.append((rail, uhc))
            random.shuffle(rail_uhc_tuple_list)

            for rail, uhc in rail_uhc_tuple_list:
                self.ForceCurrentonVlcScenario(rail, uhc, board)

    def ForceCurrentonVlcScenario(self, rail, uhc, board):

        self.Log('info', '\nTesting VLC rail {} on uhc {}...'.format(rail, uhc))

        board.SetRailsToSafeState()
        if rail < 10:
            board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
        else:
            board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearVlcRailAlarm()
        board.ClearVlcSampleAlarms()
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearTrigExecAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)
        dutid = 15
        board.EnableOnlyOneUhc(uhc, dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail,'VLC')

#        board.InitializeVlcSampleEngine(0x500 * dutid, 0x500 * dutid, 0x1000 * dutid, 0x1000 * dutid, dutid)
        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'VLC')

        trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'VLC')
        trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'VLC')

        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 2 * dutid + 1
        asm.LoadString(Template("""\
            $triggerqueueheader
            SetMode rail=RAIL, value=VFORCE            
            EnableDisableRail rail=RAIL, value=0
            SetCurrentRange rail=RAIL, value=I_256_MA
            SetIClampHi rail=RAIL, value=0.25
            SetIClampLo rail=RAIL, value=-0.25
            EnableDisableRail rail=RAIL, value=1
            SetVoltage rail=RAIL, value=1.0
            $sampleengine
            $triggerqueuefooter
            TqComplete rail=0, value=0
        """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(uhc, 0x100, 0x100, 0x0),
                         triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header))

        data = asm.Generate()
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
        offset = 0x0 + 0x100 * 0
        board.WriteTriggerQueue(offset, data)
        data = board.DmaRead(offset, 0x100)
        # board.ReadMemory(data)

        board.ExecuteTriggerQueue(offset, uhc, 'VLC')
        board.CheckVlcSamplingActive(uhc)
        board.CheckVlcSampleAlarms()

        railvoltage = 'VlcRail' + str(rail) + 'V'
        self.Log('info', 'The VLC rail {} voltage reg is 0x{:x}'.format(rail, board.Read(railvoltage).Pack()))
        railcurrent = 'VlcRail' + str(rail) + 'I'
        self.Log('info', 'The VLC rail {} current reg is 0x{:x}'.format(rail, board.Read(railcurrent).Pack()))

        if rail < 10:
            board.RunCheckers( uhc, 'ResistiveLoopback', 10, 'VLC')
        else:
            board.RunCheckers( uhc, 'ResistiveLoopback', 4, 'VLC')

        alarm = board.CheckVlcRailAlarm()
        if alarm:
            board.ClearVlcRailAlarm()


    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def MiniForceVoltageonVlcVsimTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            dutid = 0
            rail = 2
            if rail < 10:
                board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
            else:
                board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.Write('ENABLE_ALARMS', 0x1)
            board.Write('GLOBAL_ALARMS', 0x00ff)
            board.EnableOnlyOneUhc(dutid)
            board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

            board.WriteRegister(board.registers.VlcUhcSampleHeaderRegionBase(value=0x00005000), index=0)
            board.WriteRegister(board.registers.VlcUhcSampleHeaderRegionSize(value=0x00000010), index=0)
            board.WriteRegister(board.registers.VlcUhcSampleRegionBase(value=0x00010000), index=0)
            board.WriteRegister(board.registers.VlcUhcSampleRegionSize(value=0x01000000), index=0)

            trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'VLC')
            trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'VLC')

            asm = TriggerQueueAssembler()
            asm.symbols['RAIL'] = rail
            asm.symbols['END'] = 2 * dutid
            asm.symbols['BEGIN'] = 2 * dutid + 1
            asm.LoadString(Template("""\
                 $triggerqueueheader
                 SetMode rail=RAIL, value=VFORCE
                 EnableDisableRail rail=RAIL, value=0
                 SetCurrentRange rail=RAIL, value=I_256_MA
                 SetVoltage rail=RAIL, value=1.5
                 EnableDisableRail rail=RAIL, value=1
                 Q cmd=0x0, arg=VlcSampleEngineReset, data=0x0001
                 Q cmd=0x0, arg=Vlc0SampleDelay, data=0xEB00
                 Q cmd=0x0, arg=Vlc0SampleCount, data=0x100
                 Q cmd=0x0, arg=Vlc0SampleRate, data=0x0000
                 Q cmd=0x0, arg=Vlc0SampleMetadataHi, data=0x7654
                 Q cmd=0x0, arg=Vlc0SampleMetadataLo, data=0x3210
                 Q cmd=0x0, arg=Vlc0SampleRailSelect, data=0xffff
                 Q cmd=0x0, arg=Vlc0SampleStart, data=0x0001
                 $triggerqueuefooter
                 TqComplete rail=0, value=0
             """).substitute(triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header))
            data = asm.Generate()
            self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
            offset = 0
            board.WriteTriggerQueue(offset, data)
            data = board.DmaRead(0x0, 0x100)
            board.ReadMemory(data)
            board.ExecuteTriggerQueue(offset, 0, 'VLC')
            self.Log('info', 'The VLC rail 0 status is {}'.format(board.Read('VlcRail0V').Pack()))
            board.CheckVlcSamplingActive(0)
            board.CheckVlcSampleAlarms()

            self.Log('info', 'Reading header')
            data = board.DmaRead(0x00005000, 0x20)
            board.ReadMemory(data)

            self.Log('info', 'Reading sample engine')
            data = board.DmaRead(0x00010000, 0x100)
            board.ReadSamples(data)

            if rail < 10:
                board.RunCheckers( dutid, 'ResistiveLoopback', 10, 'VLC')
            else:
                board.RunCheckers( dutid, 'ResistiveLoopback', 4, 'VLC')

            alarm = board.CheckVlcRailAlarm()
            if alarm:
                board.ClearVlcRailAlarm()
            self.Log('info', 'Successfully executed the MiniForceVoltageonVlcVsimTest for VLC Rails')

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailIrangeTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):

            dutid = 0
            rail = 5

            vlc_iranges = ['I_25_6_UA', 'I_2_56_MA', 'I_25_6_MA', 'I_256_MA']
            vlc_irange_encodings = [0x1, 0x3, 0x4, 0x5]

            for irange, irange_encoding in zip(vlc_iranges, vlc_irange_encodings):
                self.Log('info', '\nTesting irange {}'.format(irange))

                self.Log('info', '\nTesting VLC rail {} on dutid {}...'.format(rail, dutid))


                board.SetRailsToSafeState()
                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'VLC')

                trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'VLC')
                trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'VLC')

                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1
                asm.symbols['IRANGE'] = irange
                asm.LoadString(Template("""\
                    $triggerqueueheader
                    EnableDisableRail rail=RAIL, value=1
                    SetCurrentRange rail=RAIL, value=IRANGE
                    EnableDisableRail rail=RAIL, value=0
                    $triggerqueuefooter
                    TqComplete rail=0, value=0
                """).substitute(triggerqueuefooter=trigger_queue_footer,
                         triggerqueueheader=trigger_queue_header))

                data = asm.Generate()
                self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset = 0x0 + 0x100 * 0
                board.WriteTriggerQueue(offset, data)
                data = board.DmaRead(offset, 0x100)
                # board.ReadMemory(data)

                board.ExecuteTriggerQueue(offset, dutid, 'VLC')
                board.CheckVlcSampleAlarms()

                # Read the value out of last current value register
                vlclastcurrentrange = 'Vlc' + str(rail) + 'LastCurrentRange'
                if board.Read(vlclastcurrentrange).Pack() == irange_encoding:
                    self.Log('info', 'The VLC rail {} last current range reg value is 0x{:x}'.format(rail, board.Read(
                        vlclastcurrentrange).Pack()))
                else:
                    self.Log('error',
                             'The VLC rail {} last current range reg value is not set as expected Actual -> 0x{:x}'.format(
                                 rail, board.Read(vlclastcurrentrange).Pack()))

                # Read the value of irange from isl55180 measurement unit and source selection register and it should be the max irange value
                deviceid = ISL55180.ISL55180_0_REG + (2 * rail)
                irangefromdevice = board.ReadIsl55180Register(deviceid, 'MEAS_UNIT_SRC_SEL')
                if irangefromdevice == 0x802:
                    self.Log('info',
                             'The VLC rail {} current range read from the device after the rail gets disabled is 0x{:x}'.format(
                                 rail, irangefromdevice))
                else:
                    self.Log('error',
                             'The VLC rail {} current range read from the device after the rail gets disabled is not as expected Actual->0x{:x}'.format(
                                 rail, irangefromdevice))

                alarm = board.CheckVlcRailAlarm()
                if alarm:
                    board.ClearVlcRailAlarm()

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailTcLoopingTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            dutid = 0
            lutdwordaddress = 0x10000000

            for rail in range(16):
                self.Log('info', '\nTesting VLC rail {} on dutid {}...'.format(rail, dutid))

                board.SetRailsToSafeState()
                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'VLC')

                trigger_queue_header = board.getTriggerQueueHeaderString(rail, 'VLC')
                trigger_queue_footer = board.getTriggerQueueFooterString(rail, 'VLC')

                # Start loading the start block, Loop block, Stop block, Complete block for TC Looping
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                # Start Block
                asm.LoadString(Template("""\
                    $triggerqueueheader
                    LoopControl rail=RAIL, value=0
                    LoopTrigger rail=RAIL, value=1
                    SoftwareTrigger rail=RAIL, value=0
                    TqComplete rail=0, value=0    
                """).substitute(triggerqueueheader=trigger_queue_header))

                data = asm.Generate()
                self.Log('info', 'Testing start block trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset_start_block = 0x1000
                board.WriteTriggerQueue(offset_start_block, data)
                data = board.DmaRead(offset_start_block, 0x100)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000)
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_start_block))
                board_2 = self.env.instruments[board.slot].subslots[1]
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                board.ReadMemory(data)

                # Looping Block
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                asm.LoadString("""\
                    TimeDelay rail=RAIL, value=1
                    TimeDelay rail=RAIL, value=2
                    TimeDelay rail=RAIL, value=3
                    LoopTrigger rail=RAIL, value=1
                    TqComplete rail=0, value=0    
                """)

                data = asm.Generate()
                self.Log('info', 'Testing looping block trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset_looping_block = 0x2000
                board.WriteTriggerQueue(offset_looping_block, data)
                data = board.DmaRead(offset_looping_block, 0x100)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x04
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_looping_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                board.ReadMemory(data)

                # Stop Block
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                asm.LoadString("""\
                    LoopTrigger rail=RAIL, value=3
                    LoopControl rail=RAIL, value=1
                    TqComplete rail=0, value=0    
                """)

                data = asm.Generate()
                self.Log('info', 'Testing the stop block trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset_stop_block = 0x4000
                board.WriteTriggerQueue(offset_stop_block, data)
                data = board.DmaRead(offset_stop_block, 0x100)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x08
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_stop_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                board.ReadMemory(data)

                # Done/Complete Block
                asm = TriggerQueueAssembler()
                asm.symbols['RAIL'] = rail
                asm.symbols['END'] = 2 * dutid
                asm.symbols['BEGIN'] = 2 * dutid + 1

                asm.LoadString(Template("""\
                    LoopControl rail=RAIL, value=2
                    SoftwareTrigger rail=RAIL, value=3
                    $triggerqueuefooter
                    TqComplete rail=0, value=0    
                """).substitute(triggerqueuefooter=trigger_queue_footer))

                data = asm.Generate()
                self.Log('info', 'Testing the stop block trigger queue for slot:{} subslot:{}'.format(board.slot, board.subslot))
                offset_complete_block = 0x8000
                board.WriteTriggerQueue(offset_complete_block, data)
                data = board.DmaRead(offset_complete_block, 0x100)
                board.ReadMemory(data)

                lutDwordAddr = lutdwordaddress + (dutid * 0x10000) + 0x0c
                self.Log('info', 'LUT Dword Address is 0x{:x}'.format(lutDwordAddr))
                board.DmaWrite(lutDwordAddr, struct.pack('<L', offset_complete_block))
                board_2.DmaWrite(lutDwordAddr, struct.pack('<L', 0xFFFFFFFF))
                data = board.DmaRead(lutDwordAddr, 0x20)
                board.ReadMemory(data)

                # Read the initial value out of the vlcrailsloopingregister
                vlcrailsloopingregister = 'VlcRailsLooping'
                self.Log('info',
                         'The VLC Rails Looping Register before executing TC Looping set to a value = 0x{:x}'.format(
                             board.Read(vlcrailsloopingregister).Pack()))

                # Send the trigger for the start block with a LUT Address of 14'h0
                self.env.send_trigger(0x04000000)
                self.Log('info', 'RC sent trigger 0x04000000')

                # Reading out of the vlcrailslooping register and waiting till looping bits goes high and complete bit goes low
                vlc_looping_register_initial_value = 0xffff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0xffff & ~(1 << rail))
                looping_register_new_value = looping_bits | complete_bits

                time.sleep(0.00001)
                if board.Read(vlcrailsloopingregister).Pack() != looping_register_new_value:
                    self.Log('error',
                             'The VLC Rails Looping Register after executing start block is set to a value = 0x{:x}'.format(
                                 board.Read(vlcrailsloopingregister).Pack()))
                else:
                    self.Log('info',
                             'The VLC Rails Looping Register after executing start block is set to a value = 0x{:x}'.format(
                                 board.Read(vlcrailsloopingregister).Pack()))

                    # wait for couple of looping triggers to be received at the HDDPS side
                ExpectedTrigVal = 0x04000001
                for i in range(0, 4):
                    for count in range(5):
                        time.sleep(0.000001)
                        triggerValue = hil.dpsBarRead(board.slot, board.subslot, 1, 0x54)
                        if triggerValue == ExpectedTrigVal:
                            self.Log('info', 'Received the looping trigger at the HDDPS Side')
                            break
                        elif count == 4 and triggerValue != ExpectedTrigVal:
                            self.Log('error',
                                     'Correct Trigger value not received for Looping Block, Actual Value -> {:x}'.format(
                                         triggerValue))

                # Send the trigger to the stop block to terminate the tc looping
                self.env.send_trigger(0x04000002)

                # Read out of vlcrailsloopingregister to make sure that the rail complete bit goes high and rail looping bit goes low
                time.sleep(0.001)
                if board.Read(vlcrailsloopingregister).Pack() != vlc_looping_register_initial_value:
                    self.Log('error',
                             'The VLC Rails Looping Register after executing the complete block is set to a value = 0x{:x}'.format(
                                 board.Read(vlcrailsloopingregister).Pack()))
                else:
                    self.Log('info',
                             'The VLC Rails Looping Register after executing the complete block is set to a value = 0x{:x}'.format(
                                 board.Read(vlcrailsloopingregister).Pack()))

                    # Try reading multiple times from trigger down register to ensure that the fifo is empty
                for num_reads in range(10):
                    hil.dpsBarRead(board.slot, 0, 1, 0x54)
                    hil.dpsBarRead(board.slot, 1, 1, 0x54)

                alarm = board.CheckVlcRailAlarm()
                if alarm:
                    board.ClearVlcRailAlarm()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailTcLoopingAlarmConditionTest(self):
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):

            dutid = 0

            for rail in range(16):
                self.Log('info', '\nTesting VLC rail {} on dutid {}...'.format(rail, dutid))
                board.SetRailsToSafeState()

                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

                board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'VLC')

                # Sending a TQ Notify to bring the rail 0 out of safe state
                board.WriteTQHeaderViaBar2(dutid, rail, 'VLC')
                # command = RAILCOMMANDS.TQ_NOTIFY
                # data = 2 * dutid + 1
                # board.WriteBar2RailCommand(command, data, rail)

                # Sending a loop control command to rail 0 to start looping
                command = RAILCOMMANDS.LOOP_CONTROL
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail)

                # Reading out of the vlcrailslooping register and waiting till looping bits goes high and complete bit goes low
                vlc_looping_register_initial_value = 0xffff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0xffff & ~(1 << rail))
                looping_register_new_value = looping_bits | complete_bits

                VlcRailsLoopingRegister = 'VlcRailsLooping'
                time.sleep(0.00001)
                if board.Read(VlcRailsLoopingRegister).Pack() != looping_register_new_value:
                    self.Log('error',
                             'The VLC Rails Looping Register after executing loop control rail command is set to a value = 0x{:x}'.format(
                                 board.Read(VlcRailsLoopingRegister).Pack()))
                else:
                    self.Log('info',
                             'The VLC Rails Looping Register after executing loop control rail command is set to a value = 0x{:x}'.format(
                                 board.Read(VlcRailsLoopingRegister).Pack()))

                    # Sending another loop control command to rail 0 to create a looping alarm condition
                command = RAILCOMMANDS.LOOP_CONTROL
                data = 0x0000
                board.WriteBar2RailCommand(command, data, rail)

                # In this case both looping and complete bits for a particular rail should be 1 to satisy alarm condition
                vlc_looping_register_initial_value = 0xffff
                looping_bits = (0x1 << (16 + rail))
                complete_bits = (0xffff & ~(1 << rail)) | (1 << rail)
                looping_register_new_value = looping_bits | complete_bits

                time.sleep(0.001)

                if board.Read(VlcRailsLoopingRegister).Pack() != looping_register_new_value:
                    self.Log('error',
                             'The VLC Rails Looping Register after executing second consecutive rail command is set to a value = 0x{:x}'.format(
                                 board.Read(VlcRailsLoopingRegister).Pack()))
                else:
                    self.Log('info',
                             'The VLC Rails Looping Register after executing second consecutive rail command is set to a value = 0x{:x}'.format(
                                 board.Read(VlcRailsLoopingRegister).Pack()))

                globalalarm = board.Read('GLOBAL_ALARMS')
                if globalalarm.VlcRailAlarms == 1:
                    self.Log('info',
                             'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(board.slot, board.subslot,
                                                                                                      globalalarm.Pack()))
                    vlcrailalarm = board.Read('VlcRailAlarms')

                    if rail < 10:
                        vlcrails = 'Vlc' + str(0) + str(rail) + 'Rail' + 'Alarms'
                    else:
                        vlcrails = 'Vlc' + str(rail) + 'Rail' + 'Alarms'

                    if getattr(vlcrailalarm, vlcrails) == 1:
                        vlcrail = board.Read(vlcrails)
                        self.Log('info', 'The register {} is set to 0x{:x}'.format(vlcrails, vlcrail.Pack()))
                        if vlcrail.VlcLoopinginProgressAlarm == 1:
                            self.Log('info',
                                     'The Looping in Progress Alarm got set as expected for rail {}.'.format(rail))
                        else:
                            self.Log('error',
                                     'Only the Looping in Progress Alarm for rail {} should be set. Actual -> 0x{:x}'.format(
                                         rail, vlcrail.Pack()))
                    else:
                        self.Log('error', 'The Vlc rail {} should have an alarm set.')
                else:
                    self.Log('error',
                             'The vlcrailalarm bit in globalalarm register should get set as testing for Looping in Progress Alarm. Actual -> 0x{:x}'.format(
                                 globalalarm.Pack()))

                # Clear the value of looping bit to bring the TC looping Mode to default state by wrting to HclcRailsLoopingRegister
                HclcRailsLoopingregister = 'HclcRailsLooping'
                board.Write(HclcRailsLoopingregister, (0x3ff << (16)))
                VlcRailsLoopingRegister = 'VlcRailsLooping'
                board.Write(VlcRailsLoopingRegister, (0xffff << (16)))
                time.sleep(0.00001)
                hclc_looping_register_initial_value = 0x3ff

                if board.Read(HclcRailsLoopingregister).Pack() != hclc_looping_register_initial_value:
                    self.Log('error',
                             'The HCLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))
                else:
                    self.Log('info',
                             'The HCLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.Read(HclcRailsLoopingregister).Pack()))

                if board.Read(VlcRailsLoopingRegister).Pack() != vlc_looping_register_initial_value:
                    self.Log('error',
                             'The VLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.Read(VlcRailsLoopingRegister).Pack()))
                else:
                    self.Log('info',
                             'The VLC Rails Looping Register after clearing the looping bit is set to a value = 0x{:x}'.format(
                                 board.Read(VlcRailsLoopingRegister).Pack()))

                board.ClearVlcRailAlarm()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailsP2PVoltageCurrentReadTest(self):
        ''' Test to verify HDDPS RC peer to peer communication interface
            •	Initialize P2P communication by enabling P2P enable bit from P2P control register.
            •	Start reading voltage and current from RC FPGA 
            •   Repeat above steps multiple times and see if system fails to report voltage and current.'''
        for board in self.env.duts_or_skip_if_no_vaild_rail(['VLC']):
            board.SetRailsToSafeState()

            self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
            board.ClearVlcSampleAlarms()
            board.ClearHclcRailAlarms()
            board.ClearHclcSampleAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()
            board.Write('ENABLE_ALARMS', 0x1)

            for rail in range(board.DPS_NUM_AD5560_RAILS):
                board.ConnectCalBoard('LC{}'.format(rail), 'NONE')

            for rail in range(board.DPS_NUM_ISL55180_RAILS):
                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'NONE')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'NONE')

            num_p2p_dwords = 2 * (board.DPS_NUM_AD5560_RAILS + board.DPS_NUM_ISL55180_RAILS)

            for rail in range(board.DPS_NUM_ISL55180_RAILS):
                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'NONE')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'NONE')

            num_p2p_dwords = 2 * (board.DPS_NUM_AD5560_RAILS + board.DPS_NUM_ISL55180_RAILS)

            for rail in range(board.DPS_NUM_ISL55180_RAILS):
                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'NONE')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'NONE')

            num_p2p_dwords = 2 * (board.DPS_NUM_AD5560_RAILS + board.DPS_NUM_ISL55180_RAILS)

            for rail in range(board.DPS_NUM_ISL55180_RAILS):
                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'NONE')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'NONE')

            num_p2p_dwords = 2 * (board.DPS_NUM_AD5560_RAILS + board.DPS_NUM_ISL55180_RAILS)

            # Get the base physical address of P2P SRAM
            base_mem_addr = board.RCPeerToPeerMemoryBase()
            self.Log('info', 'The P2P base mem addr is {:x}'.format(base_mem_addr))

            # Write P2P Start Addr Hi and Lo with appropriate address
            addr = base_mem_addr + 0x300 * board.slot
            board.Write('P2PStartAddrLo', addr & 0xFFFFFFFF)
            board.Write('P2PStartAddrHi', ((addr >> 32) & 0xFFFFFFFF))

            enabledisablep2p = 1
            self.configure_p2p_control_register_to_enable__or_disable_p2p_communication_and_read_values_back(board.slot,
                                                                                                             board.subslot,
                                                                                                             enabledisablep2p)

            self.read_back_logged_values_of_V_and_I_from_RC_and_compare_it_to_expected_rail_values(board.slot, board.subslot,
                                                                                                   num_p2p_dwords)

            enabledisablep2p = 0
            self.configure_p2p_control_register_to_enable__or_disable_p2p_communication_and_read_values_back(board.slot,
                                                                                                             board.subslot,
                                                                                                             enabledisablep2p)

            board.ClearHclcRailAlarms()
            board.ClearTrigExecAlarms()
            board.ClearGlobalAlarms()

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailsScopeShotSamplingModeEventTriggerStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[1]
                board.SetRailsToSafeState()

                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')
                board.UnGangAllRails()

                board.InitializeSampleEngine(0x500 * dutid, 0x500 * dutid, 0x1000 * dutid, 0x1000 * dutid, dutid, 'VLC')

                event_mask_alarm_bit = 1
                event_mask_trigger_bit = 1

                trigger_value = 0x0

                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x0100000
                count = 1
                # Roll over bit will be low since sample size of 0x100000 is too high for rollover to occur
                rollover = 0
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureVlcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                 metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # ScopeShot Stop Trigger
                scope_shot_stop_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.STOP)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_stop_trigger_value)
                stop_due_to_trigger = 0
                stop_due_to_alarm = 0
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.Read('SShotTriggerFifoPayload').Pack() != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailsScopeShotSamplingModeMatchingTriggerPayloadStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[1]
                board.SetRailsToSafeState()

                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

                board.InitializeSampleEngine(0x500 * dutid, 0x500 * dutid, 0x1000 * dutid, 0x1000 * dutid, dutid, 'VLC')

                event_mask_alarm_bit = 0
                event_mask_trigger_bit = 1

                trigger_value = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x6
                count = 2
                # Roll over bit will be high since sample region buffer size of 0x6 (6 bytes) is too small to accomodate all the samples
                rollover = 1
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureVlcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                 metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # ScopeShot Stop Trigger
                stop_due_to_trigger = 1
                stop_due_to_alarm = 0
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.Read('SShotTriggerFifoPayload').Pack() != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    @unittest.skip('This test currently skipped while VLC rails is being audited')
    def DirectedVlcRailsScopeShotSamplingModeAlarmStopTest(self):
        rail = 5
        lutdwordaddress = 0x10000000
        for slot, subslot in self.env.fpgas:
            if subslot == 1:
                continue
            for dutid in range(8):
                board = self.env.instruments[slot].subslots[subslot]
                board_2 = self.env.instruments[slot].subslots[1]
                board.SetRailsToSafeState()

                if rail < 10:
                    board.ConnectCalBoard('VLC0{}'.format(rail), 'OHM_10')
                else:
                    board.ConnectCalBoard('VLC{}'.format(rail), 'OHM_4')

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)

                self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
                board.ClearVlcRailAlarm()
                board.ClearVlcSampleAlarms()
                board.ClearHclcRailAlarms()
                board.ClearHclcSampleAlarms()
                board.ClearTrigExecAlarms()
                board.ClearGlobalAlarms()
                board.Write('ENABLE_ALARMS', 0x1)

                board.EnableOnlyOneUhc(dutid)
                board.ConfigureUhcRail(dutid, 0x1 << rail,'VLC')

                samplesize = 0x6
                count = 2
                # Roll over bit will be high since sample region buffer size of 0x6 (6 bytes) is too small to accomodate all the samples
                rollover = 1
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.InitializeSampleEngine(0x500 * dutid, 0x500 * dutid, 0x1000 * dutid, 0x1000 * dutid, dutid, 'VLC')

                event_mask_alarm_bit = 1
                event_mask_trigger_bit = 1

                trigger_value = 0x0
                self.configure_scope_shot_control_register_and_read_values_back(slot, subslot, dutid,
                                                                                event_mask_alarm_bit,
                                                                                event_mask_trigger_bit)
                self.write_to_stop_trigger_register_and_read_values_back(slot, subslot, trigger_value)

                samplesize = 0x40
                count = 4
                rate = 0
                metahi = 0xF0F0
                metalo = 0xF0F0
                deviceid = 0

                board.ConfigureVlcSamplingEngineforScopeShotMode(deviceid, dutid, samplesize, count, rate, metahi,
                                                                 metalo, rail)

                dps_type_trigger_1 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x0))
                dps_type_trigger_2 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x1))
                dps_type_trigger_3 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x2))
                dps_type_trigger_4 = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (0x3))

                dps_trigger_array = [dps_type_trigger_1, dps_type_trigger_2, dps_type_trigger_3, dps_type_trigger_4]
                num_triggers = len(dps_trigger_array)

                self.write_poisoned_lut_entry_for_dummy_triggers(slot, subslot, num_triggers, lutdwordaddress, dutid)

                # ScopeShot Start Trigger
                scope_shot_start_trigger_value = ((DPSTRIGGERS.EVENTTRIGGERTYPE << 26) | EVENTTRIGGER.START)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, scope_shot_start_trigger_value)

                for dps_type_trigger in dps_trigger_array:
                    self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, dps_type_trigger)
                    # Read the values in scope shot trigger fifo payload and scope shot trigger fifo timestamp registers
                    self.read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
                        slot, subslot, dps_type_trigger)

                # Sending a cbf trigger due to UHC folding alarm which will stop the scope shot sampling engine
                cbf_type_trigger = ((DPSTRIGGERS.DPSTRIGGERTYPE << 26) | (dutid << 20) | (
                    CROSSBOARDTRIGGER.DPSCTRLTYPE << 15) | CROSSBOARDTRIGGER.CBFOLD)
                self.send_trigger_via_RC_and_read_from_hddps(slot, subslot, cbf_type_trigger)

                # ScopeShot Stop Trigger
                stop_due_to_trigger = 0
                stop_due_to_alarm = 1
                time.sleep(1)

                self.read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(slot, subslot,
                                                                                                        stop_due_to_trigger,
                                                                                                        stop_due_to_alarm,
                                                                                                        rollover)

                self.read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(slot)
                while (board.Read('SShotTriggerFifoPayload').Pack() != 0x0):
                    pass
                board.ClearHclcRailAlarms()
                board.ClearGlobalAlarms()
                board.ClearTrigExecAlarms()

    def read_trigger_down_register_multiple_times_to_empty_the_fifo_containing_any_unitended_triggers(self, slot):
        # Try reading multiple times from trigger down register to ensure that the fifo is empty
        while (hil.dpsBarRead(slot, 0, 1, 0x54) != 0x0 or hil.dpsBarRead(slot, 1, 1, 0x54) != 0x0):
            pass

    def configure_p2p_control_register_to_enable__or_disable_p2p_communication_and_read_values_back(self, slot, subslot,
                                                                                                    enabledisablep2p):
        board = self.env.instruments[slot].subslots[subslot]
        # Initiating P2P Communication by enabling P2P enable bit in P2P Control Reg
        P2PControlRegData = board.Read('P2PCtrl')
        P2PControlRegData.P2PEn = enabledisablep2p
        board.Write('P2PCtrl', P2PControlRegData)

        for numreads in range(10):
            time.sleep(0.000001)
            P2PControlRegData = board.Read('P2PCtrl')
            if getattr(P2PControlRegData, 'P2PEn') == enabledisablep2p:
                self.Log('info',
                         'The P2P Control Register is set to a correct value 0x{:x}'.format(P2PControlRegData.Pack()))
                break
            elif numreads == 9:
                self.Log('error', 'The P2P Control Register is set to 0x{:x}'.format(P2PControlRegData.Pack()))

    def read_back_logged_values_of_V_and_I_from_RC_and_compare_it_to_expected_rail_values(self, slot, subslot,
                                                                                          num_p2p_dwords):
        board = self.env.instruments[slot].subslots[subslot]
        offset = 0x4000 + (0x300 * slot)

        errorMargin = 0.1

        for multiplereads in range(30):
            dword_count = 0
            v_or_i_pair = 0
            num_p2p_dwords_for_voltage = 0
            num_p2p_dwords_for_current = 0

            time.sleep(0.02)
            self.Log('info', 'Voltage and Current readings {}'.format(multiplereads))
            for address in range(offset, offset + 0x300, 8):
                readbackvi = self.env.RcBarRead(1, address)
                if dword_count < num_p2p_dwords:
                    if v_or_i_pair == 0:
                        self.Log('info', '{:04X}: Voltage Value -> {:04X}'.format(address, readbackvi))
                        self.Log('info', '{:04X}: Voltage Value in float from host memory {:.4f}'.format(address,
                                                                                                         struct.unpack(
                                                                                                             '<f',
                                                                                                             struct.pack(
                                                                                                                 '<L',
                                                                                                                 readbackvi))[
                                                                                                             0]))
                        if num_p2p_dwords_for_voltage < hddpsSubslot.DPS_NUM_ISL55180_RAILS:
                            instantaneous_voltage_reg_value = 'VlcRail' + str(num_p2p_dwords_for_voltage) + 'V'
                            expectedRailVoltage = board.Read(instantaneous_voltage_reg_value).Pack()
                            self.Log('info',
                                     'VLC Rail {}: Voltage Value in float from instantaneous voltage register from FPGA {:.4f}'.format(
                                         num_p2p_dwords_for_voltage,
                                         struct.unpack('<f', struct.pack('<L', expectedRailVoltage))[0]))
                        else:
                            instantaneous_voltage_reg_value = 'DpsRail' + str(
                                num_p2p_dwords_for_voltage - hddpsSubslot.DPS_NUM_ISL55180_RAILS) + 'V'
                            expectedRailVoltage = board.Read(instantaneous_voltage_reg_value).Pack()
                            self.Log('info',
                                     'LC Rail {}: Voltage Value in float from instantaneous voltage register from FPGA {:.4f}'.format(
                                         num_p2p_dwords_for_voltage - hddpsSubslot.DPS_NUM_ISL55180_RAILS,
                                         struct.unpack('<f', struct.pack('<L', expectedRailVoltage))[0]))
                        delta = abs(struct.unpack('<f', struct.pack('<L', expectedRailVoltage))[0] -
                                    struct.unpack('<f', struct.pack('<L', readbackvi))[0])
                        if delta > errorMargin:
                            self.Log('error',
                                     " The actual voltage value from Rail is {} which fails the limit of {}".format(
                                         struct.unpack('<f', struct.pack('<L', readbackvi))[0], errorMargin))
                        num_p2p_dwords_for_voltage = num_p2p_dwords_for_voltage + 1
                    else:
                        self.Log('info', '{:04X}: Current Value -> {:04X}'.format(address, readbackvi))
                        self.Log('info', '{:04X}: Current Value in float from host memory {:.4f}'.format(address,
                                                                                                         struct.unpack(
                                                                                                             '<f',
                                                                                                             struct.pack(
                                                                                                                 '<L',
                                                                                                                 readbackvi))[
                                                                                                             0]))
                        if num_p2p_dwords_for_current < hddpsSubslot.DPS_NUM_ISL55180_RAILS:
                            instantaneous_current_reg_value = 'VlcRail' + str(num_p2p_dwords_for_current) + 'I'
                            expectedRailCurrent = board.Read(instantaneous_current_reg_value).Pack()
                            self.Log('info',
                                     'VLC Rail {}: Current Value in float from instantaneous Current register from FPGA {:.4f}'.format(
                                         num_p2p_dwords_for_current,
                                         struct.unpack('<f', struct.pack('<L', expectedRailCurrent))[0]))
                        else:
                            instantaneous_current_reg_value = 'DpsRail' + str(
                                num_p2p_dwords_for_current - hddpsSubslot.DPS_NUM_ISL55180_RAILS) + 'I'
                            expectedRailCurrent = board.Read(instantaneous_current_reg_value).Pack()
                            self.Log('info',
                                     'LC Rail {}: Current Value in float from instantaneous Current register from FPGA {:.4f}'.format(
                                         num_p2p_dwords_for_current - hddpsSubslot.DPS_NUM_ISL55180_RAILS,
                                         struct.unpack('<f', struct.pack('<L', expectedRailCurrent))[0]))
                        delta = abs(struct.unpack('<f', struct.pack('<L', expectedRailCurrent))[0] -
                                    struct.unpack('<f', struct.pack('<L', readbackvi))[0])
                        if delta > errorMargin:
                            self.Log('error',
                                     " The actual current value from Rail is {} which fails the limit of {} Expected Rail Current {}".format(
                                         struct.unpack('<f', struct.pack('<L', readbackvi))[0], errorMargin,
                                         struct.unpack('<f', struct.pack('<L', expectedRailCurrent))[0]))
                        num_p2p_dwords_for_current = num_p2p_dwords_for_current + 1

                dword_count = dword_count + 1
                v_or_i_pair = not v_or_i_pair

    def write_poisoned_lut_entry_for_dummy_triggers(self, slot, subslot, num_triggers, lutdwordaddress, dutid):
        uhc_specific_triggers_lut_address = lutdwordaddress + (dutid * 0x10000)
        board = self.env.instruments[slot].subslots[subslot]
        board_2 = self.env.instruments[slot].subslots[1]

        for trigger in range(num_triggers):
            self.Log('info', 'LUT Dword Address is 0x{:x}'.format(uhc_specific_triggers_lut_address))
            board.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
            board_2.DmaWrite(uhc_specific_triggers_lut_address, struct.pack('<L', 0xFFFFFFFF))
            time.sleep(0.000001)
            uhc_specific_triggers_lut_address = uhc_specific_triggers_lut_address + 0x4

    def configure_scope_shot_control_register_and_read_values_back(self, slot, subslot, dutid, event_mask_alarm_bit,
                                                                   event_mask_trigger_bit):
        board = self.env.instruments[slot].subslots[subslot]
        SShotControlRegData = board.Read('SShotControl')
        SShotControlRegData.SShotUhcSelect = dutid
        SShotControlRegData.SShotEventMaskAlarm = event_mask_alarm_bit
        SShotControlRegData.SShotEventMaskTrigger = event_mask_trigger_bit
        SShotControlRegData.SShotEnable = 1
        board.Write('SShotControl', SShotControlRegData)

        time.sleep(0.000001)

        SShotControlRegData = board.Read('SShotControl')

        # Masking the sshot reason bits (8-15) as they are don't care at this point
        SShotControlRegValue = (SShotControlRegData.Pack() & 0xFFFF00FF)

        if SShotControlRegValue == (dutid << 28 | 1 << 24 | event_mask_alarm_bit << 1 | event_mask_trigger_bit):
            self.Log('info',
                     'Scope Shot Ctrl Register is set to a correct value of 0x{:x}'.format(SShotControlRegData.Pack()))
        else:
            self.Log('error', 'Scope Shot Ctrl Register is set to an incorrect value of 0x{:x}'.format(
                SShotControlRegData.Pack()))

    def write_to_stop_trigger_register_and_read_values_back(self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]
        SShotStopTriggerRegData = board.Read('SShotStopTrigger')
        SShotStopTriggerRegData.Data = trigger_value
        board.Write('SShotStopTrigger', SShotStopTriggerRegData)

        time.sleep(0.000001)

        SShotStopTriggerRegData = board.Read('SShotStopTrigger')
        if SShotStopTriggerRegData.Pack() == trigger_value:
            self.Log('info', 'Scope Shot Stop Trigger Register is set to a correct value of 0x{:x}'.format(
                SShotStopTriggerRegData.Pack()))
        else:
            self.Log('error', 'Scope Shot Stop Trigger Register is set to an incorrect value of 0x{:x}'.format(
                SShotStopTriggerRegData.Pack()))

    def send_trigger_via_RC_and_read_from_hddps(self, slot, subslot, trigger_value):
        expected = trigger_value
        self.env.send_trigger(trigger_value)
        time.sleep(0.010)
        self.Log('debug', 'RC sent trigger 0x{:x}'.format(trigger_value))

        board = self.env.instruments[slot].subslots[subslot]

        trigger_value = board.Read('TRIGGERDOWNTEST')
        if trigger_value.Pack() != expected:
            self.Log('error', 'HDDPS received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expected,
                                                                                                        trigger_value.Pack()))
        else:
            self.Log('info', 'HDDPS({}, {}) received trigger 0x{:x}'.format(slot, subslot, trigger_value.Pack()))

    def read_scope_shot_control_register_and_verify_the_reason_for_sampling_engine_to_stop(self, slot, subslot,
                                                                                           stop_due_to_trigger,
                                                                                           stop_due_to_alarm, rollover):
        board = self.env.instruments[slot].subslots[subslot]
        SShotControlRegData = board.Read('SShotControl')
        if getattr(SShotControlRegData, 'SShotEventSignatureAlarm') == stop_due_to_alarm and getattr(
                SShotControlRegData, 'SShotEventSignatureTrigger') == stop_due_to_trigger:
            self.Log('info',
                     'Scope Shot Ctrl Register is set to a correct value of 0x{:x}'.format(SShotControlRegData.Pack()))
        else:
            self.Log('error', 'Scope Shot Ctrl Register is set to an incorrect value of 0x{:x}'.format(
                SShotControlRegData.Pack()))

        # Verify that the rollover bit is set to 1 since the count value 10 is higher than sample region size of 0x30 which cn accomodate 6 samples
        SShotEndSampleAddress = board.Read('SShotVlcEndSampleAddress')
        if getattr(SShotEndSampleAddress, 'SShotVlcSampleAddrRollover') == rollover:
            self.Log('info', 'Sample Engine End Address register is set to a correct value of {:x}'.format(
                SShotEndSampleAddress.Pack()))
        else:
            self.Log('error', 'Sample Engine End Address register is set to an incorrect value of {:x}'.format(
                SShotEndSampleAddress.Pack()))

    def read_scope_shot_trigger_fifo_and_verify_that_it_is_populated_with_correct_trigger_payload_and_timestamp_entries(
            self, slot, subslot, trigger_value):
        board = self.env.instruments[slot].subslots[subslot]

        SShotTriggerFifoTimeStampRegister = board.Read('SShotTriggerFifoTimeStamp')
        self.Log('info', 'The scope shot trigger fifo timestamp register is updated with a value of 0x{:x}'.format(
            SShotTriggerFifoTimeStampRegister.Pack()))

        SShotTriggerFifoPayloadRegister = board.Read('SShotTriggerFifoPayload')
        if SShotTriggerFifoPayloadRegister.Pack() == trigger_value:
            self.Log('info',
                     'The scope shot trigger fifo payload register is updated with correct trigger payload value of 0x{:x}'.format(
                         SShotTriggerFifoPayloadRegister.Pack()))
        else:
            self.Log('error',
                     'The scope shot trigger fifo payload register received an incorrect trigger expected=0x{:x}, actual=0x{:x}'.format(
                         trigger_value, SShotTriggerFifoPayloadRegister.Pack()))
