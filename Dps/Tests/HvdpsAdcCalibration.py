################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import random
import time
import unittest
import collections
from Common.instruments.dps import symbols
from Dps.Tests.dpsTest import BaseTest
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.symbols import HCLCIRANGE
from Common.instruments.dps.trigger_queue import TriggerQueueString
from Common.instruments.dps.symbols import RAILCOMMANDS,SETMODECMD,HCLCIRANGE,VRANGE

VOLTAGE_LIMITS = {'HV': (-3,18)}
CurrentRange = collections.namedtuple('CurrentRange', ['irange'])
ClampLimits_LoadCombination = collections.namedtuple('ClampLimits_LoadCombination', ['iclamp_low', 'iclamp_high', 'load'])
TEST_COMBINATIONS = {CurrentRange(irange='I_7500_MA',):ClampLimits_LoadCombination(iclamp_low=-0.5,iclamp_high=5,load='10Ohm'),
                     CurrentRange(irange='I_500_MA',):ClampLimits_LoadCombination(iclamp_low=-0.5, iclamp_high=0.5, load='10Ohm'),
                     CurrentRange(irange='I_25_MA',):ClampLimits_LoadCombination(iclamp_low=-0.025, iclamp_high=0.025, load='100Ohm')}
trigger_queue_offset = 0x0
ALARM_PROPAGATION_DELAY = 0.1
SETTLING_TIME_DELAY = 0.01


class BaseCalibration(BaseTest):

    class CalibrationException(Exception):
        pass

    def get_real_current_calibration_gain_and_offset(self, selected_gain, selected_offset, irange):
        calibration_gain_real = (selected_gain / 2 ** 16) + 0.5
        if irange == 'I_7500_MA':
            calibration_offset_real = (selected_offset / 2 ** 16) - 0.5
        elif irange == 'I_25_MA' or irange == 'I_500_MA':
            calibration_offset_real = (selected_offset / 2 ** 17) - 0.25
        elif irange == 'I_2_5_MA' or irange == 'I_250_UA':
            calibration_offset_real = (selected_offset / 2 ** 24) - (1 / 512)
        elif irange == 'I_25_UA' or irange == 'I_5_UA':
            calibration_offset_real = (selected_offset / 2 ** 29) - (1 / 16384)
        return calibration_gain_real, calibration_offset_real

    def get_real_voltage_calibration_gain_and_offset(self,selected_gain,selected_offset):
        calibration_gain_real = (selected_gain / 2 ** 16) + 0.5
        calibration_offset_real = (selected_offset / 2 ** 16) - 0.5
        return  calibration_gain_real,calibration_offset_real

    def get_random_gain_offset_list(self):
        gain_offset_tuple_list = []
        gain_list = [0x0000,0x4000,0x8000,0x3FFF,0xFFFF]
        random.shuffle(gain_list)
        offset_list = [0x0000,0x4000,0x8000,0x3FFF,0xFFFF]
        random.shuffle(offset_list)
        for gain in gain_list:
            for offset in offset_list:
                gain_offset_tuple_list.append((gain,offset))
        random.shuffle(gain_offset_tuple_list)
        return  gain_offset_tuple_list

    def get_uhc_random_rail_tuple_list(self,rail_type):
        if rail_type == 'HV':
            rail_uhc_tuple_list = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0)]
        else:
            rail_uhc_tuple_list = [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (8, 0), (9, 0)]
        random.shuffle(rail_uhc_tuple_list)
        return rail_uhc_tuple_list

    def force_voltage_with_cal_load(self, board, dutid, rail, rail_type, uhc, force_voltage, irange,iclamp_low,iclamp_high):
        trigger_queue_data = self.generate_force_voltage_with_load_trigger_queue(board, rail_type, force_voltage,
                                                                                 irange, dutid, rail, uhc,iclamp_low,iclamp_high)
        set_v_range_data = 0x8001
        board.WriteBar2RailCommand(command=RAILCOMMANDS.SET_V_RANGE, data=set_v_range_data, rail_or_resourceid=rail)
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)

    def force_voltage_lvm(self, board,rail_type, dutid, lv_rail,hv_rail, uhc, force_voltage, irange,iclamp_low, iclamp_high):
        trigger_queue_data = self.generate_lvm_trigger_queue( board,irange,hv_rail,lv_rail,force_voltage, dutid,uhc,iclamp_low, iclamp_high)
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)


    def reset_calibration_gain_and_offset(self, board):
        ref_offset = 0x8000
        ref_gain = 0x8000
        adc_i_cal_data = board.registers.ADC_I_CAL_DATA()
        adc_i_cal_data.Offset = ref_offset
        adc_i_cal_data.Gain = ref_gain
        board.WriteRegister(adc_i_cal_data)
        # self.Log('info', 'ICAL Data before test {:x}'.format(board.ReadRegister(board.registers.ADC_I_CAL_DATA).value))

    def generate_force_voltage_with_load_trigger_queue(self, board, rail_type, force_voltage, irange, dutid, rail, uhc,iclamp_low,iclamp_high):
        tracking_voltage_offset = 2

        tq_helper = TriggerQueueString(board, uhc, dutid)

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.open_socket = False
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage
        tq_helper.force_clamp_high = iclamp_high
        tq_helper.force_clamp_low = iclamp_low
        tq_helper.force_low_voltage_limit = -3
        tq_helper.force_high_voltage_limit = 18
        tq_helper.force_rail_irange = irange
        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = 0.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 10000

        trigger_queue = TriggerQueueAssembler()
        # trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        text_trigger_queue = tq_helper.generateForceVoltageTriggerQueue()
        # self.Log('info', f'{text_trigger_queue}')
        trigger_queue.LoadString(text_trigger_queue)
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data


    def generate_lvm_trigger_queue(self, board,irange,force_rail,sense_rail,force_voltage, dutid,uhc,iclamp_low, iclamp_high):
        lvUnderVoltLimit = -2
        lvOverVoltLimit =  6
        tracking_voltage_offset =2

        tq_helper = TriggerQueueString(board,uhc,dutid)
        tq_helper.open_socket = False

        rail_type = 'HV'

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = force_rail
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage
        tq_helper.force_clamp_high = iclamp_high
        tq_helper.force_clamp_low = iclamp_low
        tq_helper.force_low_voltage_limit = -3
        tq_helper.force_high_voltage_limit = 18
        tq_helper.force_rail_irange = irange

        rail_type = 'LVM'
        tq_helper.sense_rail_type = rail_type
        tq_helper.sense_rail = sense_rail
        tq_helper.sense_high_voltage_limit = lvOverVoltLimit
        tq_helper.sense_low_voltage_limit = lvUnderVoltLimit

        trigger_queue = TriggerQueueAssembler()
        # trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        text_trigger_queue = tq_helper.generateForceVoltageTriggerQueue()
        # self.Log('info', f'{text_trigger_queue}')
        trigger_queue.LoadString(text_trigger_queue)
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def configure_current_calibration_ram(self, board, rail, irange, turbo):
        if turbo == True:
            self.set_turbo_current_calibration_ram(board, rail, irange)
        else:
            self.set_regular_current_calibration_ram(board,rail,irange)

    def configure_voltage_calibration_ram(self, board, rail,rail_type,turbo):
        if turbo == True:
            self.set_turbo_voltage_calibration_ram(board, rail, rail_type)
        else:
            self.set_regular_voltage_calibration_ram(board, rail, rail_type)

    def set_regular_current_calibration_ram(self, board, rail, irange):
        regular_current_rail_offset = rail + 8
        adc_i_cal_register = board.registers.ADC_I_CAL_ADDR()
        adc_i_cal_register.RamSelect = regular_current_rail_offset
        adc_i_cal_register.RamAddress = irange
        board.WriteRegister(adc_i_cal_register)

    def set_regular_voltage_calibration_ram(self, board, rail, rail_type):
        softspancode = 7
        dut_regular_voltage_ram_select_offset = 18
        if rail_type == 'LVM':
            regular_voltage_rail_offset = rail+ dut_regular_voltage_ram_select_offset
        else:
            regular_voltage_rail_offset = rail
        adc_i_cal_register = board.registers.ADC_I_CAL_ADDR()
        adc_i_cal_register.RamSelect = regular_voltage_rail_offset
        adc_i_cal_register.RamAddress = softspancode
        board.WriteRegister(adc_i_cal_register)

    def set_calibration_gain_offset(self, board, gain, offset):
        adc_i_cal_data = board.registers.ADC_I_CAL_DATA()
        adc_i_cal_data.Offset = offset
        adc_i_cal_data.Gain = gain
        board.WriteRegister(adc_i_cal_data)

    def set_turbo_current_calibration_ram(self, board, rail, irange):
        turbo_current_mask = 17
        adc_i_cal_register = board.registers.ADC_I_CAL_ADDR()
        adc_i_cal_register.RamSelect = turbo_current_mask
        adc_i_cal_register.RamAddress = (rail << 3 | irange)
        board.WriteRegister(adc_i_cal_register)

    def set_turbo_voltage_calibration_ram(self, board, rail, rail_type):
        if rail_type == 'LVM':
            turbo_voltage_mask = 28
        else:
            turbo_voltage_mask = 16
        softspancode = 7
        adc_i_cal_register = board.registers.ADC_I_CAL_ADDR()
        adc_i_cal_register.RamSelect = turbo_voltage_mask
        adc_i_cal_register.RamAddress = (rail << 3 | softspancode)
        board.WriteRegister(adc_i_cal_register)

    def verify_expected_voltage(self, board, rail, uhc, rail_type, expected_voltage,actual_voltage):
        allowed_voltage_deviation = abs((20 / 100) * expected_voltage)
        if board.is_in_maximum_allowed_deviation(expected_voltage, actual_voltage, allowed_voltage_deviation) == False:
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V is not within the allowable deviation of {:.2f}V'.format(
                         rail_type, rail, uhc, actual_voltage, expected_voltage))
            raise self.CalibrationException

    def verify_expected_current(self, board, rail, uhc, rail_type, expected_current,actual_current):
        allowed_current_deviation = abs((40 / 100) * expected_current)
        if board.is_in_maximum_allowed_deviation(expected_current, actual_current,allowed_current_deviation) == False:
            self.Log('error','{} rail:{} uhc:{}, instantaneous current is {:.4f}I is not within the allowable deviation of {}% of {:.4f}I'.format(
                        rail_type, rail, uhc, actual_current, allowed_current_deviation, expected_current))
            raise self.CalibrationException


class CurrentCalibration(BaseCalibration):


    def Directed7500MARegularCurrentCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 15
        irange = 'I_7500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.current_calibration_scenario(rail_type, board, force_voltage,irange)


    def Directed7500MATurboCurrentCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 15
        irange = 'I_7500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.current_calibration_scenario(rail_type, board, force_voltage,irange, turbo=True)


    def Directed25MATurboCurrentCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 2
        irange = 'I_25_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.current_calibration_scenario(rail_type, board, force_voltage, irange,turbo=True)

    @unittest.skip('Skipping as 500mA range unstable on the current cal fab version with load connected.')
    def Directed500MARegularCurrentCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 2
        irange = 'I_500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.current_calibration_scenario(rail_type, board, force_voltage,irange)


    def Directed25MARegularCurrentCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 2
        irange = 'I_25_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.current_calibration_scenario(rail_type, board, force_voltage,irange)

    def current_calibration_scenario(self, rail_type, board,force_voltage,irange,turbo = False):
        gain_offset_tuple_list = self.get_random_gain_offset_list()
        rail_uhc_tuple_list = self.get_uhc_random_rail_tuple_list(rail_type)
        dutid = random.randint(0, 63)
        pass_count =0
        iclamp_low,iclamp_high,load = TEST_COMBINATIONS[(irange,)]
        load_resistance = int(re.sub('\D', '', load))
        expected_current = force_voltage / load_resistance
        calboard = board.create_cal_board_instance_and_initialize()
        board.cal_load_connect(load, calboard)
        calibration_ram_address = getattr(board.symbols.HCLCIRANGE, irange)
        for rail, uhc in rail_uhc_tuple_list:
            board.EnableAlarms()
            board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
            board.ConfigureUhcRail(uhc, 0x1 << rail,rail_type)
            board.UnGangAllRails()
            board.SetRailsToSafeState()
            board.ResetVoltageSoftSpanCode(rail_type, dutid)
            board.ResetCurrentSoftSpanCode(rail_type, dutid)
            board.ClearDpsAlarms()
            if turbo == True:
                board.WriteRegister(board.registers.AD5560_TURBO_SAMPLE_RAIL_SELECT(value=0x1 << rail))
            board.cal_connect_force_sense_lines(rail, rail_type, calboard)
            self.reset_calibration_gain_and_offset(board)
            self.force_voltage_with_cal_load(board, dutid, rail, rail_type, uhc, force_voltage, irange,iclamp_low,iclamp_high)
            time.sleep(ALARM_PROPAGATION_DELAY)
            global_alarms = board.get_global_alarms()
            if global_alarms != []:
                self.Log('error', '{} rail {} Steady State conditions not achieved.Received {}'.format(rail_type, rail,global_alarms))
            else:
                try:
                    uncalibrated_current = board.get_rail_current(rail, rail_type, turbo)
                    rail_voltage = board.get_rail_voltage(rail, rail_type, turbo)
                    self.verify_expected_voltage(board, rail, uhc, rail_type,force_voltage,rail_voltage)
                    self.verify_expected_current(board, rail, uhc, rail_type,expected_current,uncalibrated_current)
                    self.configure_current_calibration_ram(board, rail, calibration_ram_address, turbo)
                    for encoded_gain,encoded_offset in gain_offset_tuple_list:
                        calibration_gain_real, calibration_offset_real = self.get_real_current_calibration_gain_and_offset(encoded_gain, encoded_offset, irange)
                        self.set_calibration_gain_offset(board, encoded_gain, encoded_offset)
                        time.sleep(SETTLING_TIME_DELAY)
                        global_alarms = board.get_global_alarms()
                        if global_alarms != []:
                            self.Log('error',
                                     '{} rail {} Unexpected Global Alarms received {}'.format(rail_type, rail,
                                                                                                          global_alarms))
                        calibrated_current = board.get_rail_current(rail, rail_type,turbo)
                        expected_adc_calibrated_current = calibration_gain_real * uncalibrated_current + calibration_offset_real
                        self.verify_expected_current(board, rail, uhc, rail_type, expected_adc_calibrated_current, calibrated_current)
                        pass_count += 1
                except self.CalibrationException:
                    pass

            board.cal_disconnect_force_sense_lines(rail, rail_type, calboard)
        if pass_count == (len(rail_uhc_tuple_list)*len(gain_offset_tuple_list)):
            self.Log('info',
                     '{} Expected Current ADC Calibration seen on {} UHC/Rail combinations.'.format(rail_type,len(rail_uhc_tuple_list)))
        else:
            self.Log('error',
                     '{} Expected Current ADC Calibration seen on {} of {} UHC/Rail combinations'.format(rail_type,int(pass_count/(len(gain_offset_tuple_list))), len(rail_uhc_tuple_list)))
        board.create_cal_board_instance_and_initialize()
        self.reset_calibration_gain_and_offset(board)
        board.SetRailsToSafeState()


class VoltageCalibration(BaseCalibration):


    def DirectedHvRegularVoltageCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 10
        irange = 'I_7500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.hv_voltage_calibration_scenario(rail_type, board, force_voltage, irange, turbo=False)


    def DirectedHvTurboVoltageCalibrationTest(self):
        rail_type = 'HV'
        force_voltage = 10
        irange = 'I_7500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.hv_voltage_calibration_scenario(rail_type, board, force_voltage, irange, turbo=True)


    def DirectedLvmRegularVoltageCalibrationTest(self):
        rail_type = 'LVM'
        force_voltage = 5
        irange = 'I_7500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.lvm_voltage_calibration_scenario(rail_type, board,force_voltage,irange,turbo=False)


    def DirectedLvmTurboVoltageCalibrationTest(self):
        rail_type = 'LVM'
        force_voltage = 5
        irange = 'I_7500_MA'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.lvm_voltage_calibration_scenario(rail_type, board,force_voltage,irange,turbo=True)

    def hv_voltage_calibration_scenario(self, rail_type, board, force_voltage, irange, turbo = False):
        gain_offset_tuple_list = self.get_random_gain_offset_list()
        rail_uhc_tuple_list = self.get_uhc_random_rail_tuple_list(rail_type)
        dutid = random.randint(0, 63)
        pass_count = 0
        iclamp_low, iclamp_high, load = TEST_COMBINATIONS[(irange,)]
        load_resistance = int(re.sub('\D', '', load))
        expected_current = force_voltage / load_resistance
        calboard = board.create_cal_board_instance_and_initialize()
        board.cal_load_connect(load, calboard)
        for rail, uhc in rail_uhc_tuple_list:
            board.ClearDpsAlarms()
            board.EnableAlarms()
            board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
            board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
            board.UnGangAllRails()
            board.SetRailsToSafeState()
            board.ResetVoltageSoftSpanCode(rail_type, dutid)
            board.ResetCurrentSoftSpanCode(rail_type, dutid)
            if turbo == True:
                board.WriteRegister(board.registers.AD5560_TURBO_SAMPLE_RAIL_SELECT(value=0x1 << rail))
            board.cal_connect_force_sense_lines(rail, rail_type, calboard)
            self.reset_calibration_gain_and_offset(board)
            self.force_voltage_with_cal_load(board, dutid, rail, rail_type, uhc, force_voltage, irange,iclamp_low, iclamp_high)
            time.sleep(ALARM_PROPAGATION_DELAY)
            global_alarms = board.get_global_alarms()
            if global_alarms != []:
                self.Log('error', '{} rail {} Steady State conditions not achieved.Received {}'.format(rail_type, rail, global_alarms))
            else:
                try:
                    uncalibrated_voltage = board.get_rail_voltage(rail, rail_type, turbo)
                    rail_current = board.get_rail_current(rail,rail_type,turbo)
                    self.verify_expected_voltage(board, rail, uhc, rail_type, force_voltage,uncalibrated_voltage)
                    self.verify_expected_current(board, rail, uhc, rail_type, expected_current, rail_current)
                    self.configure_voltage_calibration_ram(board, rail, rail_type, turbo)
                    for encoded_gain, encoded_offset in gain_offset_tuple_list:
                        calibration_gain_real, calibration_offset_real = self.get_real_current_calibration_gain_and_offset(encoded_gain, encoded_offset, irange)
                        self.set_calibration_gain_offset(board, encoded_gain, encoded_offset)
                        time.sleep(SETTLING_TIME_DELAY)
                        global_alarms = board.get_global_alarms()
                        if global_alarms != []:
                            self.Log('error','{} rail {} Unexpected Global Alarms received {}'.format(rail_type, rail,global_alarms))
                        calibrated_voltage = board.get_rail_voltage(rail, rail_type, turbo)
                        expected_adc_calibrated_voltage = calibration_gain_real * uncalibrated_voltage + calibration_offset_real
                        self.verify_expected_voltage(board, rail, uhc, rail_type, expected_adc_calibrated_voltage,calibrated_voltage)
                        pass_count += 1
                except self.CalibrationException:
                    pass
            board.cal_disconnect_force_sense_lines(rail, rail_type, calboard)
        if pass_count == (len(rail_uhc_tuple_list) * len(gain_offset_tuple_list)):
            self.Log('info',
                     '{} Expected Voltage ADC Calibration seen on {} UHC/Rail combinations.'.format(rail_type, len(
                         rail_uhc_tuple_list)))
        else:
            self.Log('error',
                     '{} Expected Voltage ADC Calibration seen on {} of {} UHC/Rail combinations'.format(rail_type, int(
                         pass_count / (len(gain_offset_tuple_list))), len(rail_uhc_tuple_list)))
        board.create_cal_board_instance_and_initialize()
        self.reset_calibration_gain_and_offset(board)
        board.SetRailsToSafeState()

    def initialize_rail(self, board, rail, rail_type, uhc, dutid):
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
        board.UnGangAllRails()

    def lvm_voltage_calibration_scenario(self,rail_type, board,force_voltage,irange,turbo = False):
        gain_offset_tuple_list = self.get_random_gain_offset_list()
        rail_uhc_tuple_list = self.get_uhc_random_rail_tuple_list(rail_type)
        dutid = random.randint(0, 63)
        pass_count = 0
        iclamp_low, iclamp_high, load = TEST_COMBINATIONS[(irange,)]
        calboard = board.create_cal_board_instance_and_initialize()
        board.cal_load_connect('100Ohm', calboard)
        hv_rail = random.randint(0,7)
        board.cal_connect_force_sense_lines(hv_rail, 'HV', calboard)
        force_rail_type = 'HV'
        for lv_rail, uhc in rail_uhc_tuple_list:
            self.initialize_rail(board, lv_rail, rail_type, uhc, dutid)
            self.initialize_rail(board, hv_rail, force_rail_type, uhc, dutid)
            board.ResetVoltageSoftSpanCode(rail_type, dutid)
            board.ResetVoltageSoftSpanCode(force_rail_type, dutid)
            board.ResetCurrentSoftSpanCode(force_rail_type, dutid)
            if turbo == True:
                board.WriteRegister(board.registers.DUT_TURBO_SAMPLE_RAIL_SELECT(value=0x1 << lv_rail))
            board.ConnectRailForce(hv_rail)

            board.ConnectLvRailSense(lv_rail)
            self.reset_calibration_gain_and_offset(board)
            self.force_voltage_lvm(board,rail_type, dutid, lv_rail,hv_rail, uhc, force_voltage, irange,iclamp_low, iclamp_high)
            time.sleep(ALARM_PROPAGATION_DELAY)

            global_alarms = board.get_global_alarms()
            if global_alarms != []:
                self.Log('error', '{} rail {} Steady State conditions not achieved.Received {}'.format(rail_type, lv_rail,global_alarms))
            else:
                try:
                    uncalibrated_voltage = board.get_rail_voltage(lv_rail, rail_type, turbo)
                    self.verify_expected_voltage(board, lv_rail, uhc, rail_type, force_voltage, uncalibrated_voltage)
                    self.configure_voltage_calibration_ram(board, lv_rail, rail_type, turbo)
                    for encoded_gain, encoded_offset in gain_offset_tuple_list:
                        calibration_gain_real, calibration_offset_real = self.get_real_current_calibration_gain_and_offset(
                            encoded_gain, encoded_offset, irange)
                        self.set_calibration_gain_offset(board, encoded_gain, encoded_offset)
                        time.sleep(SETTLING_TIME_DELAY)
                        global_alarms = board.get_global_alarms()
                        if global_alarms != []:
                            self.Log('error', '{} rail {} Unexpected Global Alarms received {}'.format(rail_type, lv_rail, global_alarms))
                        calibrated_voltage = board.get_rail_voltage(lv_rail, rail_type, turbo)
                        expected_adc_calibrated_voltage = calibration_gain_real * uncalibrated_voltage + calibration_offset_real
                        self.verify_expected_voltage(board, lv_rail, uhc, rail_type, expected_adc_calibrated_voltage,calibrated_voltage)
                        pass_count += 1
                except self.CalibrationException:
                    pass
            board.ConnectLvRailSense(lv_rail, 0)
        if pass_count == (len(rail_uhc_tuple_list) * len(gain_offset_tuple_list)):
            self.Log('info','{} Expected Voltage ADC Calibration seen on {} UHC/Rail combinations.'.format(rail_type, len(rail_uhc_tuple_list)))
        else:
            self.Log('error','{} Expected Voltage ADC Calibration seen on {} of {} UHC/Rail combinations'.format(rail_type, int(pass_count / (len(gain_offset_tuple_list))), len(rail_uhc_tuple_list)))
        board.ConnectRailForce(hv_rail, 0)
        board.cal_disconnect_force_sense_lines(hv_rail, 'HV', calboard)
        board.create_cal_board_instance_and_initialize()
        self.reset_calibration_gain_and_offset(board)
        board.SetRailsToSafeState()
