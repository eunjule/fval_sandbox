################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import random
import time
import unittest
from Common.instruments.dps.symbols import RAILCOMMANDS
from Dps.Tests.dpsTest import BaseTest
from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString

VOLTAGE_LIMITS = {'HC': (-3,10),'LC': (-3,10),'HV': (-3,18)}
FORCE_VOLTAGE = {'HV': 15,'HC': 2.5,'LC':2,'VLC': 2}
NEGATIVE_FORCE_VOLTAGE = {'LC': -2,'VLC': -1}
NEGATIVE_ALARM_CLAMP_LIMITS = {'LC': (-0.1,2),'VLC': (-0.05,0.25)}
CAL_LOAD = {'HC': 'OHM_10','HV': '10Ohm','LC': 'OHM_10','VLC': 'OHM_10'}
ALARM_CLAMP_LIMITS = {'HC': (-0.5,0.1),'HV': (-0.5,1),'LC': (-0.5,0.1),'VLC': (-0.25,0.05)}
NO_ALARM_CLAMP_LIMITS = {'HC': (-0.5,.5),'HV': (-0.5,3),'LC': (-.5,.5),'VLC': (-0.25,0.25)}
I_RANGE = {'HC': 'I_500_MA','LC': 'I_500_MA','HV': 'I_7500_MA','VLC': 'I_256_MA'}
trigger_queue_offset = 0x0
alarm_propagation_delay = 0.1
HV_RAIL_OFFSET = 16


class Clamps(BaseTest):


    def DirectedHvHighClampTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.clamp_alarm_scenario(board, rail_type,negative=False)

    def DirectedHcHighClampTest(self):
        rail_type = 'HC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.clamp_alarm_scenario(board, rail_type,negative=False)

    def DirectedLcLowClampTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.clamp_alarm_scenario(board, rail_type,negative=True)

    def DirectedLcHighClampTest(self):
        rail_type = 'LC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.clamp_alarm_scenario(board, rail_type, negative=False)

    @unittest.skip('skipped until converstion to raw is in place')
    def DirectedVlcHighClampTest(self):
        rail_type = 'VLC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.clamp_alarm_scenario(board, rail_type, negative=False)

    @unittest.skip('This test currently being debugged for very low current readings on some rails')
    def DirectedVlcLowClampTest(self):
        rail_type = 'VLC'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.clamp_alarm_scenario(board, rail_type, negative=True)


    def clamp_alarm_scenario(self, board, rail_type,negative):
        rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
        dutid = random.randint(0, 63)
        load_resistance = int(re.sub('\D','',CAL_LOAD[rail_type]))
        if negative == True:
            force_voltage = NEGATIVE_FORCE_VOLTAGE[rail_type]
        else:
            force_voltage = FORCE_VOLTAGE[rail_type]
        expected_current  = force_voltage /load_resistance
        rail_scenario = RailScenario(board, rail_type, negative, dutid)
        rail_scenario.cal_load_connect()
        for rail, uhc in rail_uhc_tuple_list:
            rail_scenario.setup(rail,uhc)
            rail_scenario.cal_connect_force_sense_lines(rail)
            self.setup_steady_state(board, dutid, rail, rail_type, uhc, force_voltage)
            time.sleep(alarm_propagation_delay)
            global_alarms = board.get_global_alarms()

            if global_alarms!=[]:
                self.Log('error', '{} rail {} Steady State conditions not achieved.Received {}'.format(rail_type, rail,global_alarms))
            else:
                if self.verify_rail_voltage_and_current(board, rail, rail_type, uhc, force_voltage,expected_current) == True:
                    self.setup_current_clamp_condition(board, dutid, rail, rail_type, uhc,negative)
                    time.sleep(alarm_propagation_delay)
                    rail_scenario.check_for_rail_alarm(rail)

            rail_scenario.cal_disconnect_force_sense_lines(rail)
        pass_count_alarm_received = rail_scenario.pass_count
        if pass_count_alarm_received == len(rail_uhc_tuple_list):
            self.Log('info','{} Expected Clamp Alarm seen on {} UHC/Rail combinations.'.format(rail_type,pass_count_alarm_received))
        else:
            self.Log('error','{} Expected Clamp Alarm seen on {} of {} UHC/Rail combinations.'.format(rail_type,pass_count_alarm_received, len(rail_uhc_tuple_list)))

        board.create_cal_board_instance_and_initialize()
        board.SetRailsToSafeState()


    def setup_current_clamp_condition(self, board, dutid, rail, rail_type,uhc,negative):
        trigger_queue_data1 = self.generate_trigger_queue_with_clamp_alarm(board, rail_type, dutid, rail, uhc,negative)
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data1)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)

    def setup_steady_state(self, board, dutid, rail, rail_type, uhc, force_voltage):
        if rail_type == 'VLC':
            trigger_queue_data = self.generate_vlc_trigger_queue_without_clamp_alarm(board, rail_type, force_voltage, dutid, rail, uhc)
        else:
            trigger_queue_data = self.generate_trigger_queue_without_clamp_alarm(board, rail_type,force_voltage,dutid, rail, uhc)
        board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)
        board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)

    def generate_trigger_queue_without_clamp_alarm(self, board, rail_type, force_voltage, dutid, rail, uhc):
        tq_helper = TriggerQueueString(board, uhc, dutid)

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.open_socket = False
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage
        tq_helper.force_clamp_high = NO_ALARM_CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = NO_ALARM_CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]
        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = 0.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 6000

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def generate_vlc_trigger_queue_without_clamp_alarm(self,board, rail_type, force_voltage, dutid, rail, uhc):
        tq_helper = TriggerQueueString(board, uhc, dutid)

        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.open_socket = False
        tq_helper.force_voltage = force_voltage
        tq_helper.force_clamp_high = NO_ALARM_CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = NO_ALARM_CLAMP_LIMITS[rail_type][0]
        tq_helper.force_rail_irange = I_RANGE[rail_type]
        tq_helper.free_drive = True
        tq_helper.force_free_drive_high = 7.5
        tq_helper.force_free_drive_low = -.5
        tq_helper.force_free_drive_delay = 6000

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generateVlcForceVoltageTriggerQueue(tq_helper))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data


    def generate_trigger_queue_with_clamp_alarm(self, board, rail_type, dutid, rail, uhc,negative):

        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.force_rail = rail
        tq_helper.force_rail_type = rail_type
        if negative == True:
            tq_helper.force_clamp_low = NEGATIVE_ALARM_CLAMP_LIMITS[rail_type][0]
        else:
            tq_helper.force_clamp_high = ALARM_CLAMP_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(self.generate_current_clamp_trigger_queue(tq_helper,negative))
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def verify_rail_voltage_and_current(self,board,rail,rail_type,uhc,expected_voltage,expected_current):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        rail_current = board.get_rail_current(rail,rail_type)
        allowed_voltage_deviation = abs((30/100)*expected_voltage)
        allowed_current_deviation = abs((40/100)*expected_current)
        if board.is_in_maximum_allowed_deviation(expected_voltage, rail_voltage, allowed_voltage_deviation) == False or \
                        board.is_in_maximum_allowed_deviation(expected_current, rail_current, allowed_current_deviation) ==False:
            self.Log('error',
                     '{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V and current {:.2f} is not within the allowable deviation of {:.2f}V,{:.2f}'.format(
                         rail_type, rail, uhc, rail_voltage,rail_current, expected_voltage,expected_current))
            return False
        return True

    def generate_current_clamp_trigger_queue(self, tq_helper,negative):
        trigger_queue_string = ''

        current_range = tq_helper.get_current_range(tq_helper.force_rail, tq_helper.force_rail_type, tq_helper.force_rail_irange)
        trigger_queue_string = trigger_queue_string + '\n' + current_range

        if negative == True:
            current_clamps = tq_helper.get_low_current_clamp_limits_string(tq_helper.force_rail,tq_helper.force_rail_type,tq_helper.force_clamp_low,tq_helper.force_rail_irange)
            trigger_queue_string = trigger_queue_string + '\n' + current_clamps
        else:
            current_clamps = tq_helper.get_high_current_clamp_limits_string(tq_helper.force_rail,
                                                                           tq_helper.force_rail_type,
                                                                           tq_helper.force_clamp_high,tq_helper.force_rail_irange)
            trigger_queue_string = trigger_queue_string + '\n' + current_clamps
        flush_clamps = tq_helper.get_flush_clamps(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue_string = trigger_queue_string + '\n' + flush_clamps
        trigger_queue_with_header_footer = tq_helper.add_header_footer_to_trigger_queue(trigger_queue_string)
        return trigger_queue_with_header_footer


    def generateVlcForceVoltageTriggerQueue(self,tq_helper):
        trigger_queue_string = ''

        test_mode = tq_helper.get_test_mode(tq_helper.force_rail, tq_helper.force_rail_type, 'VFORCE')
        trigger_queue_string = trigger_queue_string + '\n' + test_mode

        disable_rail = tq_helper.get_disable_rail(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue_string = trigger_queue_string + '\n' + disable_rail

        trigger_queue_string = trigger_queue_string + '\n' + 'Q cmd=SUPPRESS_ALARMS, arg={}, data=100\n'.format(
            tq_helper.force_rail)

        current_range = tq_helper.get_current_range(tq_helper.force_rail, tq_helper.force_rail_type,
                                                    tq_helper.force_rail_irange)
        trigger_queue_string = trigger_queue_string + '\n' + current_range
        current_clamps = tq_helper.get_current_clamp_limits_string(tq_helper.force_rail, tq_helper.force_rail_type,
                                                                   tq_helper.force_rail_irange,
                                                                   tq_helper.force_clamp_low,
                                                                   tq_helper.force_clamp_high)
        trigger_queue_string = trigger_queue_string + '\n' + current_clamps
        flush_clamps = tq_helper.get_flush_clamps(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue_string = trigger_queue_string + '\n' + flush_clamps
        if tq_helper.free_drive == True:
            free_drive_current = tq_helper.get_free_drive_current_string(tq_helper.force_rail, tq_helper.force_rail_type, tq_helper.force_free_drive_low, tq_helper.force_free_drive_high)
            trigger_queue_string = trigger_queue_string + '\n' + free_drive_current

        force_voltage = tq_helper.get_force_voltage_string(tq_helper.force_voltage, tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue_string = trigger_queue_string + '\n' + force_voltage

        enable_rail = tq_helper.get_enable_rail(tq_helper.force_rail, tq_helper.force_rail_type)
        trigger_queue_string = trigger_queue_string + '\n' + enable_rail

        trigger_queue_string = trigger_queue_string + '\n' + 'Q cmd=ISL55180_{}_REG, arg=DPS_CONT_MISC, data=0xB200\n'.format(tq_helper.force_rail)
        trigger_queue_string = trigger_queue_string + '\n' + 'Q cmd=SUPPRESS_ALARMS, arg={}, data=250\n'.format(
            tq_helper.force_rail)
        trigger_queue_string = trigger_queue_string + 'Q cmd=ISL55180_{}_REG, arg=CLAMP_ALARM_CONT, data=0x2E\n'.format(tq_helper.force_rail)

        trigger_queue_with_header_footer = tq_helper.add_header_footer_to_trigger_queue(trigger_queue_string)
        return trigger_queue_with_header_footer

class FreeDriveClampSetting(BaseTest):

    def DirectedClampEnableDisableTest(self):
        rail_type = 'HV'
        end_free_drive_settling_time = 0.001
        free_drive_zero_volt_raw_code = 0x2100
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            default_clamp_enable_status = 0xFF
            pass_count =0
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            for rail, uhc in rail_uhc_tuple_list:
                expected_clamp_enable_status_after_fd_start = (0x1<<rail) ^ 0xFF
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV' or rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type,dutid)
                    board.ResetCurrentSoftSpanCode(rail_type,dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                clamp_enable_before_test = self.get_clamp_enable_status(board)
                if clamp_enable_before_test != default_clamp_enable_status:
                    self.Log('error','AD5560 Clamp Enable status before Free drive start is not as expected.Actual {:x} Expected {:x}'.format(clamp_enable_before_test,default_clamp_enable_status))
                else:
                    board.WriteBar2RailCommand(command=RAILCOMMANDS.START_FREE_DRIVE, data=free_drive_zero_volt_raw_code,rail_or_resourceid=rail+HV_RAIL_OFFSET)
                    clamp_enable_after_start_freedrive = self.get_clamp_enable_status(board)
                    board.WriteBar2RailCommand(command=RAILCOMMANDS.END_FREE_DRIVE, data=0,  rail_or_resourceid=rail+HV_RAIL_OFFSET)
                    time.sleep(end_free_drive_settling_time)
                    clamp_enable_after_test = self.get_clamp_enable_status(board)
                    if clamp_enable_after_start_freedrive != expected_clamp_enable_status_after_fd_start:
                        self.Log('error', 'AD5560 Clamp Enable status after Free drive start is not as expected.Actual {:x} Expected {:x}'.format(clamp_enable_after_start_freedrive,expected_clamp_enable_status_after_fd_start))
                    elif clamp_enable_after_test !=default_clamp_enable_status:
                            self.Log('error',
                                     'AD5560 Clamp Enable status after Free drive end is not as expected.Actual {:x} Expected {:x}'.format(
                                         clamp_enable_before_test, default_clamp_enable_status))
                    else:
                        pass_count +=1
                board.WriteTQFooterViaBar2(dutid, rail, rail_type)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info', '{} Expected Clamp Enable status seen on {} UHC/Rail combinations.'.format(rail_type,
                                                                                                    pass_count))
            else:
                self.Log('error',
                         '{} Expected Clamp Enable status seen on {} of {} UHC/Rail combinations.'.format(rail_type,
                                                                                                  pass_count,
                                                                                                  len(
                                                                                                      rail_uhc_tuple_list)))

    def get_clamp_enable_status(self, board):
        return board.ReadRegister(board.registers.AD5560_CL_EN_STATUS).value

    def DirectedSafeStateInBetweenClampEnableDisableTest(self):
        rail_type = 'HV'
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            default_clamp_enable_status = 0xFF
            end_free_drive_settling_time = 0.001
            free_drive_zero_volt_raw_code = 0x2100
            pass_count = 0
            rail_uhc_tuple_list = board.randomize_rail_uhc(rail_type)
            dutid = random.randint(0, 63)
            for rail, uhc in rail_uhc_tuple_list:
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.UnGangAllRails()
                board.SetRailsToSafeState()
                if rail_type == 'HV' or rail_type == 'LVM':
                    board.ResetVoltageSoftSpanCode(rail_type,dutid)
                    board.ResetCurrentSoftSpanCode(rail_type,dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                expected_clamp_enable_status_after_fd_start = (0x1 << rail) ^ 0xFF

                board.WriteTQHeaderViaBar2(dutid, rail, rail_type)
                clamp_enable_before_test = self.get_clamp_enable_status(board)
                if clamp_enable_before_test != default_clamp_enable_status:
                    self.Log('error','AD5560 Clamp Enable status before Free drive start is not as expected.Actual {:x} Expected {:x}'.format(clamp_enable_before_test,default_clamp_enable_status))
                else:
                    board.WriteBar2RailCommand(command=RAILCOMMANDS.START_FREE_DRIVE, data=free_drive_zero_volt_raw_code,rail_or_resourceid=rail + 16)
                    clamp_enable_after_fd = self.get_clamp_enable_status(board)
                    board.SetRailsToSafeState()
                    clamp_enable_after_safestate = self.get_clamp_enable_status(board)
                    board.WriteBar2RailCommand(command=RAILCOMMANDS.END_FREE_DRIVE, data=0,rail_or_resourceid=rail + 16)
                    time.sleep(end_free_drive_settling_time)
                    clamp_enable_after_test = self.get_clamp_enable_status(board)
                    if clamp_enable_after_fd != expected_clamp_enable_status_after_fd_start:
                        self.Log('error', 'AD5560 Clamp Enable status after Free drive start is not as expected.Actual {:x} Expected {:x}'.format(clamp_enable_after_fd,expected_clamp_enable_status_after_fd_start))
                    elif clamp_enable_after_safestate != default_clamp_enable_status:
                        self.Log('error', 'AD5560 Clamp Enable status after Safe State is not as expected.Actual {:x} Expected {:x}'.format(clamp_enable_after_fd,expected_clamp_enable_status_after_fd_start))
                    elif clamp_enable_after_test !=default_clamp_enable_status:
                        self.Log('error',
                                 'AD5560 Clamp Enable status after Free drive end is not as expected.Actual {:x} Expected {:x}'.format(
                                     clamp_enable_before_test, default_clamp_enable_status))
                    else:
                        pass_count += 1
                board.WriteTQFooterViaBar2(dutid, rail, rail_type)
            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info', '{} Expected Clamp Enable status with Safe State in between seen on {} UHC/Rail combinations.'.format(rail_type,
                                                                                                            pass_count))
            else:
                self.Log('error',
                         '{} Expected Clamp Enable status Safe State in between seen on {} of {} UHC/Rail combinations.'.format(rail_type,
                                                                                                          pass_count,
                                                                                                          len(
                                                                                                              rail_uhc_tuple_list)))

class RailScenario():
    def __init__(self,board,rail_type,is_negative,dutid):
        self.board = board
        self.rail_type = rail_type
        self.is_negative = is_negative
        self.pass_count =0
        self.dutid = dutid
        self.calboard = board.create_cal_board_instance_and_initialize()

    def setup(self,rail,uhc):
        board = self.board
        board.ClearDpsAlarms()
        board.EnableAlarms()
        board.EnableOnlyOneUhc(uhc, dutdomainId=self.dutid)
        board.ConfigureUhcRail(uhc, 0x1 << rail, self.rail_type)
        board.UnGangAllRails()
        board.SetRailsToSafeState()
        if self.rail_type == 'HV' or self.rail_type == 'LVM':
            board.ResetVoltageSoftSpanCode(self.rail_type,self.dutid)
            board.ResetCurrentSoftSpanCode(self.rail_type,self.dutid)

    def check_for_rail_alarm(self, rail):
        if self.rail_type == 'VLC' and self.is_negative == False:
            if self.board.check_for_single_vlc_rail_alarm(rail, 'ClampHiAlarm') == True:
                self.pass_count += 1
        elif self.rail_type == 'VLC' and self.is_negative == True:
            if self.board.check_for_single_vlc_rail_alarm(rail, 'ClampLoAlarm') == True:
                self.pass_count += 1
        elif self.board.check_for_single_hclc_rail_alarm(rail, 'ClampAlarm') == True:
            self.pass_count += 1

    def cal_load_connect(self):
        self.board.cal_load_connect(CAL_LOAD[self.rail_type], self.calboard)

    def cal_connect_force_sense_lines(self,rail):
        self.board.cal_connect_force_sense_lines(rail, self.rail_type, self.calboard)

    def cal_disconnect_force_sense_lines(self,rail):
        self.board.cal_disconnect_force_sense_lines(rail, self.rail_type, self.calboard)


