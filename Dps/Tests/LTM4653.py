################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import time
import struct
from Dps.Tests.dpsTest import BaseTest
from Common.instruments.dps.symbols import RAILCOMMANDS,HCLCIRANGE

HV_RAIL_START_INDEX = 16


class InterfaceTests(BaseTest):

    def DirectedHVVoltageTest(self, test_iterations = 100):
        uhc = random.randint(0,7)
        dutdomainId = random.randint(0,64)
        rail_type = 'HV'
        voltage_variation = 1
        tracking_voltage_set_delay = 0.02
        if test_iterations == 0:
            self.Log('error', 'Test failed to execute due to 0 number of iterations given')
        else:
            for dut in self.env.duts_or_skip_if_no_vaild_rail([rail_type]):
                dut.SetRailsToSafeState()
                dut.ClearDpsAlarms()
                dut.EnableOnlyOneUhc(uhc,dutdomainId)
                list_of_random_rails = list(range(dut.RAIL_COUNT[rail_type]))
                random.shuffle(list_of_random_rails)
                for i in range(test_iterations):
                    fail_count = 0
                    for rail in list_of_random_rails:
                        expected_tracking_voltage = random.randint(5, 24)
                        cmdData= dut.GetCmdDataForSetTrackingVoltage(expected_tracking_voltage)
                        dut.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                        dut.WriteTQHeaderViaBar2(dutdomainId, rail, rail_type)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_CURRENT_RANGE, HCLCIRANGE.I_7500_MA, rail + HV_RAIL_START_INDEX)
                        dut.WriteBar2RailCommand(RAILCOMMANDS.SET_TRACKING_VOLTAGE, cmdData, rail + HV_RAIL_START_INDEX)
                        dut.WriteTQFooterViaBar2(dutdomainId, rail, rail_type)
                        time.sleep(tracking_voltage_set_delay)
                        LTM_MV_rail_register = dut.ReadRegister(dut.registers.LTM_MV_rail_v,index = rail)
                        soft_span_code = LTM_MV_rail_register.SoftSpanCode
                        if soft_span_code!= 7 :
                            self.Log('error','Incorrect span code {}, Expected span code is 7'.format(soft_span_code))
                        voltage = LTM_MV_rail_register.Data
                        actual_tracking_voltage = dut.HvTrackingVoltageAdcValueConversion(voltage)
                        hv_ad5560_tracking_regulator_offset = 4.6
                        actual_tracking_voltage = actual_tracking_voltage - hv_ad5560_tracking_regulator_offset
                        if dut.actual_tracking_voltage_is_close_to_volt_variation(expected_tracking_voltage, actual_tracking_voltage, voltage_variation) == False:
                            self.Log('error','Tracking Voltage read from ADC is {:.2f}V is not within expected voltage_variation of {}V of Expected Voltage {:.2f} V on rail {}'.format(
                                    actual_tracking_voltage,voltage_variation,expected_tracking_voltage,rail))
                            fail_count += 1
                    self.Log('debug','Tracking Voltage read from ADC successfully on {} out of {} iterations on all rails'.format(test_iterations-fail_count, test_iterations))
                dut.SetRailsToSafeState()




