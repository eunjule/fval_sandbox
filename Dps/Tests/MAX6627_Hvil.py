################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
from Common.instruments.dps import hvil_registers as hvil_regs
from Dps.Tests.dpsTest import BaseTest
from Common import hilmon as hil


class TemperatureInterface(BaseTest):

    def RandomDeviceList(self,number_of_devices):
        device_list = [x for x in range(number_of_devices)]
        random.shuffle(device_list)
        return device_list

    def RandomMAX6627TemperatureReadUsingHilLevelOneTest(self):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['RLOAD']):
            read_attempts = 10
            random_device_list = self.RandomDeviceList(6)
            for i in range(0, read_attempts):
                for device_id in random_device_list:
                    max6627_register_type = hvil_regs.MAX6627_TEMP_VALUE
                    result = dut.ReadRegister(max6627_register_type, index = device_id)
                    temperature = result.TemperatureValue
                    temperature_in_celcius =dut.MAX6627TemperatureDecimalToCelcius(temperature)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 40):
                        self.Log('info', 'Temperature of MAX6627 device in a reasonable range, device_id {} Temperature Value {}' .format(device_id,temperature_in_celcius))
                    else:
                        self.Log('error', 'Temperature of MAX6627 device not in a reasonable range, device_id {} Temperature Value {}'.format(device_id,temperature_in_celcius))

    def RandomDeviceForMAX6627TemperatureReadUsingHilLevelTwoTest(self):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['RLOAD']):
            read_attempts = 10
            random_device_list = self.RandomDeviceList(6)
            for i in range(0, read_attempts):
                for device_id in random_device_list:
                    temperature_in_celcius = dut.MAX6627TemperatureRead(device_id)
                    if (temperature_in_celcius > 10 and temperature_in_celcius <= 40):
                        self.Log('info', 'Temperature of MAX6627 device in a reasonable range, device_id {} Temperature Value {}' .format(device_id,temperature_in_celcius))
                    else:
                        self.Log('error', 'Temperature of MAX6627 device not in a reasonable range, device_id {} Temperature Value {}'.format(device_id,temperature_in_celcius))
    



    def DirectedTemperatureStabilityTest(self):
        for dut in self.env.duts_or_skip_if_no_vaild_rail(['RLOAD']):
            read_attempts = 1000
            max6627_register_type = hvil_regs.MAX6627_TEMP_VALUE
            max6627_register = max6627_register_type()
            random_device_list = self.RandomDeviceList(6)
            for device_id in random_device_list:
                temperature_in_celcius = []
                for i in range(0, read_attempts):
                    temperature = dut.MAX6627TemperatureRead(device_id)
                    temperature_in_celcius.append(temperature)
                max_temperature = max(temperature_in_celcius)
                min_temperature = min(temperature_in_celcius)
                deviation_in_temperature = max_temperature - min_temperature
                if deviation_in_temperature <= 10:
                    self.Log('info','Temperature of MAX6627 device is stable, Device Name: {}'.format(device_id))
                else:
                    self.Log('error','Temperature of MAX6627 device is unstable, Device Name: {}'.format(device_id))

class TemperatureAlarms(BaseTest):

    number_of_devices = 6
    rail_type = 'RLOAD'
    pass_count = 0

    def create_expected_alarms_list(self, random_numbers_list, AlarmType):
        expected_alarms = []
        for device in random_numbers_list:
            expected_alarms += ['MAX6627Device{}{}Alarm'.format(device,AlarmType)]
        return expected_alarms

    def verify_MAX6627Alarms(self, dut, expected_alarms):
        global_alarms = dut.get_global_alarms()
        if global_alarms == ['BrdTempAlarm']:
            max6627_alarm_register = dut.ReadRegister(dut.registers.MAX6627_TEMP_ALARMS_DETAILS)
            max6627_alarms = dut.get_non_zero_fields(max6627_alarm_register)
            if max6627_alarms != [expected_alarms]:
                self.Log('error','Unexpected alarms received {}, Expected Alarm :{}'.format(max6627_alarms, [expected_alarms]))
                raise TemperatureAlarms.UnexpectedAlarm
        else:
            self.Log('error', 'Expected only BoardTempAlarm ,received following alarms:{}'.format(global_alarms))
            raise TemperatureAlarms.UnexpectedAlarm




    def create_random_device_list(self):
        list_length = random.randint(0, 6)
        device_list = []
        for index in range(list_length):
            device_list.append(random.randint(0, 5))
        return device_list


    def RandomLowAlarmTest(self):
        for dut in self.env.duts_or_skip_if_no_vaild_rail([self.rail_type]):
            fail_count = 0
            iteration_count = 50
            for iteration in range(iteration_count):
                random_device_list = self.create_random_device_list()
                dut.ResetDefaultTemperatureLimitForMAX6627()
                dut.ClearDpsAlarms()

                if dut.get_global_alarms()!= []:
                    self.Log('error', 'Expected no global alarms ,Received global alarm(s){}'.format(dut.get_global_alarms()))
                    fail_count += 1
                else:
                    expected_alarms_list = self.create_expected_alarms_list(random_device_list, AlarmType='Low')
                    for index,device_id in enumerate(random_device_list):
                        dut.SetTemperatureLimitForSingleMAX6627(MAX6627_index=device_id, low_temperature=35,high_temperature=40)
                        try:
                            self.verify_MAX6627Alarms(dut, expected_alarms_list[index])
                        except TemperatureAlarms.UnexpectedAlarm:
                            fail_count += 1
                        dut.ResetDefaultTemperatureLimitForMAX6627()
                        dut.ClearMAX6627TemperatureAlarmDetails()
            if fail_count != 0:
                self.Log('error','Low Temperature Alarm not generated on MAX6627 devices for {} iterations out of {} iterations'.format(fail_count, iteration_count))
            else:
                self.Log('info','Low Temperature Alarm generated on all {} iterations'.format(iteration_count))

            dut.ResetDefaultTemperatureLimitForMAX6627()
            dut.ClearDpsAlarms()



    def RandomHighAlarmTest(self):
        for dut in self.env.duts_or_skip_if_no_vaild_rail([self.rail_type]):
            fail_count = 0
            iteration_count = 50
            for iteration in range(iteration_count):
                random_numbers_list = self.create_random_device_list()
                dut.ResetDefaultTemperatureLimitForMAX6627()
                dut.ClearDpsAlarms()
                expected_alarms_list = self.create_expected_alarms_list(random_numbers_list, AlarmType='High')
                if dut.get_global_alarms() != []:
                    self.Log('error', 'Expected no global alarms ,Received global alarm(s){}'.format(dut.get_global_alarms()))
                    fail_count += 1
                else:
                    for index, device_id in enumerate(random_numbers_list):
                        dut.SetTemperatureLimitForSingleMAX6627(MAX6627_index=device_id, low_temperature=5,high_temperature=10)
                        try:
                            self.verify_MAX6627Alarms(dut, expected_alarms_list[index])
                        except TemperatureAlarms.UnexpectedAlarm:
                            fail_count += 1
                        dut.ResetDefaultTemperatureLimitForMAX6627()
                        dut.ClearMAX6627TemperatureAlarmDetails()
            if fail_count != 0:
                self.Log('error','High Temperature Alarm not generated on MAX6627 devices for {} iterations out of {} iterations'.format(
                             fail_count, iteration_count))
            else:
                self.Log('info', 'High Temperature Alarm generated on all {} iterations'.format(iteration_count))

            dut.ResetDefaultTemperatureLimitForMAX6627()
            dut.ClearDpsAlarms()

    class UnexpectedAlarm(Exception):
        pass
