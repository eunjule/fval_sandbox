################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CurrentClamps
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS commands Current Clamps on AD5660
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz/Jeff Nguyen
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################

import random
from string import Template
# import time

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Dps.Tests.dpsTest import BaseTest

HeaderBase = 0x00005000
HeaderSize = 0x00000010
SampleBase = 0x00010000
SampleSize = 0x01000000


class Conditions(BaseTest):

    # set up the Current Clamp High for 24A on Daughter Board
    def HCCurrentClampCalibrationHigh24000MaTest(self):

        railtype = "HC"
        ClampRange = "24000MA"
        IClamp = 24
        Rsense = 0.02
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["HCCurrentClampCalibrationHigh24000MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 0 is LC Motherboard( subslot 1 is HC Daughter Board) if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                        # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768
                    if IClampHigh_DAC_Exp > 62499:
                        IClampHigh_DAC_Exp = 62499

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])

                        # set up the Current Clamp Low for 24A on Daughter Board

    def HCCurrentClampCalibrationLow24000MaTest(self):

        railtype = "HC"
        ClampRange = "24000MA"
        IClamp = -10
        Rsense = 0.02
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["HCCurrentClampCalibrationLow24000MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 0 is LC Motherboard( subslot 1 is HC Daughter Board) if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                        # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768
                    if IClampLow_DAC_Exp < 3037:
                        IClampLow_DAC_Exp = 3037

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])

                        # set up the Current Clamp High for 500mA on Daughter Board

    def HCCurrentClampCalibrationHigh500MaTest(self):

        railtype = "HC"
        ClampRange = "500MA"
        IClamp = 0.5
        Rsense = 1
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["HCCurrentClampCalibrationHigh500MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 0 is LC Motherboard( subslot 1 is HC Daughter Board) if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                        # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768
                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])

                        # Set up Current Clamp Low for 500mA on Daughter Board

    def HCCurrentClampCalibrationLow500MaTest(self):

        railtype = "HC"
        ClampRange = "500MA"
        IClamp = -0.5
        Rsense = 1
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["HCCurrentClampCalibrationLow500MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 0 is LC Motherboard(1 is daughter board), if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                        # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768
                    if IClampLow_DAC_Exp < 3037:
                        IClampLow_DAC_Exp = 3037

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])


                        # set up the Current Clamp High for 500mA

    def LCCurrentClampCalibrationHigh500MaTest(self):

        railtype = "LC"
        ClampRange = "500MA"
        IClamp = 0.5
        Rsense = 1
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationHigh500MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            # selectedGain   = random.choice(gainOffsetList )
            # selectedOffset = random.choice(gainOffsetList )
            selectedGain = 0xC000
            selectedOffset = 0xC000

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                        # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768
                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])

                        # Set up Current Clamp Low for 500mA

    def LCCurrentClampCalibrationLow500MaTest(self):

        railtype = "LC"
        ClampRange = "500MA"
        IClamp = -0.5
        Rsense = 1
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationLow500MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            # selectedGain   = random.choice(gainOffsetList )
            # selectedOffset = random.choice(gainOffsetList )
            selectedGain = 0xC000
            selectedOffset = 0xC000

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                        # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768
                    if IClampLow_DAC_Exp < 2078:
                        IClampLow_DAC_Exp = 2078

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])


                        # set up the Current Clamp High for 1.2A

    def LCCurrentClampCalibrationHigh1200MaTest(self):

        railtype = "LC"
        ClampRange = "1200MA"
        IClamp = 1.2
        Rsense = 0.4
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationHigh1200MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampHigh_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768

                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampHigh_DAC_Exp))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp_500ma = (
                                                   2 ** 16 * IClamp * 0.40 * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                                   5.125 * Vref) + 32768

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])


                        # set up the Current Clamp Low for 1.2A

    def LCCurrentClampCalibrationLow1200MaTest(self):

        railtype = "LC"
        ClampRange = "1200MA"
        IClamp = -1.2
        Rsense = 0.4
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationLow1200MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampLow_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768
                    if IClampLow_DAC_Exp < 2078:
                        IClampLow_DAC_Exp = 2078

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])

                        # set up the Current Clamp High 25mA ON daughter Board

    def HCCurrentClampCalibrationHigh25MaTest(self):

        railtype = "HC"
        ClampRange = "25MA"
        IClamp = 0.025
        Rsense = 20
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["HCCurrentClampCalibrationHigh25MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 0 is LC Motherboard, if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampHigh_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768

                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampHigh_DAC_Exp))

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])


                        # set up the Current Clamp Low 25mA on daughter Board

    def HCCurrentClampCalibrationLow25MaTest(self):

        railtype = "HC"
        ClampRange = "25MA"
        IClamp = -0.025
        Rsense = 20
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["HCCurrentClampCalibrationLow25MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 0 is LC Motherboard, if that card then skip till next loop
            if subslot == 0:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampLow_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768

                    if IClampLow_DAC_Exp < 3037:
                        IClampLow_DAC_Exp = 3037
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampLow_DAC_Exp))

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])


                        # set up the Current Clamp High 25mA

    def LCCurrentClampCalibrationHigh25MaTest(self):

        railtype = "LC"
        ClampRange = "25MA"
        IClamp = 0.025
        Rsense = 20
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationHigh25MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampHigh_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768

                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampHigh_DAC_Exp))

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])


                        # set up the Current Clamp Low 25mA

    def LCCurrentClampCalibrationLow25MaTest(self):

        railtype = "LC"
        ClampRange = "25MA"
        IClamp = -0.025
        Rsense = 20
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationLow25MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampLow_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768

                    if IClampLow_DAC_Exp < 2078:
                        IClampLow_DAC_Exp = 2078
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampLow_DAC_Exp))

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])



                        # set up the Current Clamp High 2.5mA

    def LCCurrentClampCalibrationHigh2p5MaTest(self):

        railtype = "LC"
        ClampRange = "2_5MA"
        IClamp = 0.0025
        Rsense = 200
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationHigh2p5MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampHigh_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768

                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampHigh_DAC_Exp))

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])

                        # set up the Current Clamp Low 2.5mA

    def LCCurrentClampCalibrationLow2p5MaTest(self):

        railtype = "LC"
        ClampRange = "2_5MA"
        IClamp = -0.0025
        Rsense = 200
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationLow2p5MaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampLow_AD5560_DAC))
                    print(IClamp)

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768

                    if IClampLow_DAC_Exp < 2078:
                        IClampLow_DAC_Exp = 2078

                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampLow_DAC_Exp))

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])

                        # set up the Current Clamp High 250UA

    def LCCurrentClampCalibrationHigh250UaTest(self):

        railtype = "LC"
        ClampRange = "250UA"
        IClamp = 0.00025
        Rsense = 2000
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationHigh250UaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampHigh_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768

                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampHigh_DAC_Exp))

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])

                        # set up the Current Clamp Low 250UA

    def LCCurrentClampCalibrationLow250UaTest(self):

        railtype = "LC"
        ClampRange = "250UA"
        IClamp = -0.00025
        Rsense = 2000
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationLow250UaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 10):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampLow_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768

                    if IClampLow_DAC_Exp < 2078:
                        IClampLow_DAC_Exp = 2078
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampLow_DAC_Exp))

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])




                        # set up the Current Clamp High 25UA

    def LCCurrentClampCalibrationHigh25UaTest(self):

        railtype = "LC"
        ClampRange = "25UA"
        IClamp = 0.000025
        Rsense = 19990
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationHigh25UaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLH_RegAddr = board.ad5560regs.CLH_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 8):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IhClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IhClampGainOffset'
                    IClampHigh_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IhClampGainOffset_RegAddr, IClampHigh_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IhClampGainOffset_RegAddr)
                    if IClampHigh_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampHigh_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampHigh_AD5560_DAC = board.ReadAd5560Register(AD5560_CLH_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampHigh_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampHigh_DAC_Exp = (
                                             2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                             5.125 * Vref) + 32768

                    if IClampHigh_DAC_Exp > 65376:
                        IClampHigh_DAC_Exp = 65376
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampHigh_DAC_Exp))

                    delta = abs(IClampHigh_AD5560_DAC - IClampHigh_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampHigh_AD5560_DAC, IClampHigh_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampHigh_AD5560_DAC,
                             IClampHigh_DAC_Exp])

                        # set up the Current Clamp low 25UA

    def LCCurrentClampCalibrationLow25UaTest(self):

        railtype = "LC"
        ClampRange = "25UA"
        IClamp = -0.000025
        Rsense = 19990
        MI_AMP_GAIN = 10
        Vref = 2.5
        errorMargin = 40
        myresult = ["LCCurrentClampCalibrationLow25UaTest,"]

        # cycle through all slots and subslots in HDDPS Card
        for slot, subslot in self.env.fpgas:
            # Subslot 1 is the HC daughterboard(0 is LC Motherboard), if that card then skip till next loop
            if subslot == 1:
                continue

            # get board instance to use for the set register funntion
            board = self.env.instruments[slot].subslots[subslot]
            AD5560_CLL_RegAddr = board.ad5560regs.CLL_DAC_X1.ADDR

            # list of gains and offsets that will be randomly choosen to be used during the test.
            gainOffsetList = [0x0, 0x4000, 0x8000, 0xC000, 0xFFFF]

            # choose a random gain and offset
            selectedGain = random.choice(gainOffsetList)
            selectedOffset = random.choice(gainOffsetList)

            # calculate calibration gain and offset based on selected values (equation from HLD)
            IClampGainReal = (selectedGain / 2 ** 16) + 0.5
            IClampOffsetReal = (selectedOffset / 2 ** 17) - 0.25

            for dutid in range(0, 1):
                for rail in range(0, 8):

                    # check the rail is MasterRail
                    RailMasterMask_Reg = 'RailMasterMask'
                    RailMaster = board.Read(RailMasterMask_Reg)
                    bit_check = (RailMaster.HcRailIndicator >> rail) & (
                        1)  # shift the HcRailIndicator to right by rail # then AND with '1'

                    if bit_check == 0:  # if the rail is not master then skip
                        continue

                        # setup Gain and Offset for Clamp High Current in FPGA
                    HDDPS_IlClampGainOffset_RegAddr = 'Rail0' + str(rail) + 'IlClampGainOffset'
                    IClampLow_GainOffset_Set = (selectedGain << 16) + selectedOffset
                    board.Write(HDDPS_IlClampGainOffset_RegAddr, IClampLow_GainOffset_Set)

                    # execute trigger queue for current clamp
                    self.ClampCurrentScenario(slot, subslot, dutid, rail, ClampRange, railtype)

                    # verify the correct gain/offset in HDDPS
                    IClamp_GainOffset_Rd = board.Read(HDDPS_IlClampGainOffset_RegAddr)
                    if IClampLow_GainOffset_Set != IClamp_GainOffset_Rd.Data:
                        self.Log('error',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))
                        continue
                    else:
                        self.Log('info',
                                 "  The gain/offset write to FPGA: {} and The gain/offset read from FPGA: {} ".format(
                                     IClampLow_GainOffset_Set, IClamp_GainOffset_Rd.Data))

                    # Read AD5560 Current Clamp DAC Code
                    IClampLow_AD5560_DAC = board.ReadAd5560Register(AD5560_CLL_RegAddr, rail)
                    self.Log('info', " The actual DAC Code read from FPGA: {} ".format(IClampLow_AD5560_DAC))

                    # Calculate Expected ad5560 DAC Code via equation (2^16 * I * Rsense * MI_AMP_GAIN * Gain + Offset) / (5.125 * 2.5) + 32768
                    IClampLow_DAC_Exp = (
                                            2 ** 16 * IClamp * Rsense * MI_AMP_GAIN * IClampGainReal + IClampOffsetReal) / (
                                            5.125 * Vref) + 32768

                    if IClampLow_DAC_Exp < 2078:
                        IClampLow_DAC_Exp = 2078
                    self.Log('info', " The expected DAC Code computed from datasheet: {} ".format(IClampLow_DAC_Exp))

                    delta = abs(IClampLow_AD5560_DAC - IClampLow_DAC_Exp)

                    # check that we are within the delta limit, if not flag as error
                    if (delta > errorMargin):
                        self.Log('error',
                                 " The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is more than error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                    else:
                        self.Log('info',
                                 "  The actual DAC Code command from FPGA: {} and The expected DAC Code command: {} is within error margin of {}".format(
                                     IClampLow_AD5560_DAC, IClampLow_DAC_Exp, errorMargin))
                        self.Log('info', 'The gain is {:x}'.format(selectedGain))
                        self.Log('info', 'The offset is {:x}'.format(selectedOffset))
                        myresult.append(
                            [rail, '{:x}'.format(selectedGain), '{:x}'.format(selectedOffset), IClampLow_AD5560_DAC,
                             IClampLow_DAC_Exp])



                        # Setup the Current Clamp Limit in HDDPS FPGA

    def ClampCurrentScenario(self, slot, subslot, dutid, rail, ClampRange, railtype):
        # print("in scenario")

        if ClampRange is '24000MA':
            iRangeQueue = 'I_25000_MA'
            iClampLowQueue = '-10'
            iClampHighQueue = '24'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        if ClampRange is '1200MA':
            iRangeQueue = 'I_1200_MA'
            iClampLowQueue = '-1.2'
            iClampHighQueue = '1.2'
            # vForceQueue = '0.4'
            # calBoardRes = 'OHM_0_8'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        if ClampRange is '500MA':
            iRangeQueue = 'I_500_MA'
            iClampLowQueue = '-0.5'
            iClampHighQueue = '0.5'
            # vForceQueue = '1.0'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        if ClampRange is '25MA':
            iRangeQueue = 'I_25_MA'
            iClampLowQueue = '-0.025'
            iClampHighQueue = '0.025'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        if ClampRange is '2_5MA':
            iRangeQueue = 'I_2_5_MA'
            iClampLowQueue = '-0.0025'
            iClampHighQueue = '0.0025'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        if ClampRange is '250UA':
            iRangeQueue = 'I_250_UA'
            iClampLowQueue = '-0.00025'
            iClampHighQueue = '0.00025'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        if ClampRange is '25UA':
            iRangeQueue = 'I_25_UA'
            iClampLowQueue = '-0.000025'
            iClampHighQueue = '0.000025'
            vForceQueue = '0.001'
            calBoardRes = 'OHM_1K'

        self.Log('info', '\nTesting Currrent Clamp rail {} for dutid {}...'.format(rail, dutid))

        board = self.env.instruments[slot].subslots[subslot]

        # set board to safe state before changing voltages
        board.SetRailsToSafeState()

        # Setup the cal cage depending on the rail type
        if (railtype == "LC"):
            self.Log('info', 'Supply Type is: {}'.format(railtype))
            board.ConnectCalBoard('LC{}'.format(rail), calBoardRes)
        else:
            board.ConnectCalBoard('HC{}'.format(rail), calBoardRes)
            self.Log('info', 'Supply Type is: {}'.format(railtype))

        # clearing and reenabling alarms
        self.Log('info', 'Clearing transient alarms when switching channels or load on cal board')
        board.ClearHclcRailAlarms()
        board.ClearHclcSampleAlarms()
        board.ClearGlobalAlarms()
        board.Write('ENABLE_ALARMS', 0x1)

        board.EnableOnlyOneUhc(dutid)
        board.ConfigureUhcRail(dutid, 0x1 << rail,railtype)
        board.UnGangAllRails()

        board.InitializeSampleEngine(0x500, 0x500, 0x1000, 0x1000, dutid, 'LC')

        # create a trigger queue object
        asm = TriggerQueueAssembler()
        asm.symbols['RAIL'] = 16 + rail
        asm.symbols['END'] = 2 * dutid
        asm.symbols['BEGIN'] = 1 + 2 * dutid

        # configure the trigger queue to force a voltage to cause the current clamp set.
        asm.LoadString(Template("""\
            TqNotify rail=0, value=BEGIN
            TqNotify rail=16, value=BEGIN
            SetMode rail=RAIL, value=VFORCE
            SetCurrentRange rail=RAIL, value=$irange
            EnableDisableRail rail=RAIL, value=0
            SetIHiFreeDrive rail=RAIL, value=$clamphigh
            SetILoFreeDrive rail=RAIL, value=$clamplow
            SetIClampHi rail=RAIL, value=$clamphigh
            SetIClampLo rail=RAIL, value=$clamplow
            SetOV rail=RAIL, value=2.5
            SetUV rail=RAIL, value=-1.5
            EnableDisableRail rail=RAIL, value=1
            SetVoltage rail=RAIL, value=$forcevoltage
            TimeDelay rail=RAIL, value=10000
            SetVCompAlarm rail=RAIL, value=0
            #SequenceBreak rail=RAIL, delay=21031
            $sampleengine
            EnableDisableRail rail=RAIL, value=0
            TimeDelay rail=RAIL, value=1000
            TqNotify rail=0, value=END
            TqNotify rail=16, value=END
            TqComplete rail=0, value=0
        """).substitute(sampleengine=board.ConfigureAndStartSamplingEngine(dutid, 0x0, 0x300, 0x0, 'LC'),
                        forcevoltage=vForceQueue, clamphigh=iClampHighQueue, clamplow=iClampLowQueue,
                        irange=iRangeQueue))

        # generate the trigger queue
        data = asm.Generate()

        # write the trigger queue to the board
        self.Log('info', 'Testing trigger queue for slot:{} subslot:{}'.format(slot, subslot))
        offset = 0x100 * dutid
        board.WriteTriggerQueue(offset, data)

        # debug data
        data = board.DmaRead(offset, 0x100)

        # execute the trigger queue on the board
        board.ExecuteTriggerQueue(offset, dutid)
        # print("end exectue trigger queue")


        board.CheckHclcSamplingActive(dutid)
        board.CheckHclcSampleAlarms()

        # **print out global alarm register for debug purposes
        globalalarm = board.Read('GLOBAL_ALARMS')
        self.Log('info', 'GLOBAL_ALARMS Register for slot {} , subslot {} is set to 0x{:x}'.format(slot, subslot,
                                                                                                  globalalarm.Pack()))

        # get current from sample memory
        sampleCurrent = board.ReturnSampleCurrentData( SampleBase + 0x1000 * dutid)

        # print(sampleCurrent)

        # check if there were alarms and clear them
        alarm = board.CheckHclcRailAlarm()
        if alarm:
            board.ClearHclcRailAlarms()

        board.SetRailsToSafeState()
