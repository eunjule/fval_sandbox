################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: ForceVoltageOpenSocket.py
#-------------------------------------------------------------------------------
#     Purpose: Test to verify if Correct voltage can be forced
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 02/20/18
#       Group: HDMT FPGA Validation
################################################################################

import random

from Dps.hddpstb.assembler import TriggerQueueAssembler
from Common.instruments.dps.trigger_queue import TriggerQueueString
from Dps.Tests.dpsTest import BaseTest

POSITIVE_VOLTAGE_RANGE = {'HC':(0,2.5),'LC':(0,5),'HV':(1,15)}
POSITIVE_VOLTAGE_STEPSIZE = {'HC':0.25,'LC':0.25,'HV':0.25}

NEGATIVE_VOLTAGE_RANGE = {'LC':(-2,0)}
NEGATIVE_VOLTAGE_STEPSIZE = {'LC':0.25}

CLAMP_LIMITS = {'HC':(-0.5,0.5),'LC':(-0.5,0.5),'HV':(-0.5,0.5)}
VOLTAGE_LIMITS = {'HC':(-3,10),'LC':(-3,10),'HV':(-3,18)}

I_RANGE = {'HC':'I_500_MA','LC':'I_500_MA','HV':'I_500_MA'}



class ForceVoltageOpenSocket(BaseTest):

    def RandomHCRailForceVoltageOpenSocketTest(self):
        rail_type = 'HC'
        iterations = 10
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario(board, rail_type, iterations,negative=False)

    def RandomLCRailForceVoltageOpenSocketTest(self):
        rail_type = 'LC'
        iterations = 10
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario(board, rail_type, iterations,negative=False)

    def RandomLCRailNegativeForceVoltageOpenSocketTest(self):
        rail_type = 'LC'
        iterations = 10
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario(board, rail_type, iterations, negative=True)

    def RandomHVRailForceVoltageOpenSocketTest(self):
        rail_type = 'HV'
        iterations = 10
        for board in self.env.duts_or_skip_if_no_vaild_rail(rail_type):
            self.force_voltage_open_socket_scenario(board, rail_type, iterations, negative=False)

    def force_voltage_open_socket_scenario(self,board,rail_type,iterations, negative = False):
        rail_uhc_tuple_list = self.randomize_rail_uhc(board, rail_type)
        board.SetRailsToSafeState()
        for test_iteration in range(iterations):
            pass_count = 0
            if negative == True:
                force_voltage = board.get_negative_random_voltage(NEGATIVE_VOLTAGE_RANGE[rail_type][0],
                                                                  NEGATIVE_VOLTAGE_RANGE[rail_type][1],
                                                                  NEGATIVE_VOLTAGE_STEPSIZE[rail_type])
            else:
                force_voltage = board.get_positive_random_voltage(POSITIVE_VOLTAGE_RANGE[rail_type][0],
                                                                  POSITIVE_VOLTAGE_RANGE[rail_type][1],
                                                                  POSITIVE_VOLTAGE_STEPSIZE[rail_type])

            for rail, uhc in rail_uhc_tuple_list:
                dutid = 15
                board.ClearDpsAlarms()
                board.EnableAlarms()
                board.EnableOnlyOneUhc(uhc, dutdomainId=dutid)
                board.ConfigureUhcRail(uhc, 0x1 << rail, rail_type)
                board.UnGangAllRails()

                trigger_queue_data = self.generate_trigger_queue(board,rail_type,force_voltage, dutid, rail, uhc)

                trigger_queue_offset = 0x0
                board.WriteTriggerQueue(trigger_queue_offset, trigger_queue_data)

                board.ExecuteTriggerQueue(trigger_queue_offset, uhc, rail_type)

                self.verify_alarm_status(board, rail, force_voltage)
                if self.verify_expected_voltage(board, rail, uhc, rail_type, force_voltage) == True:
                    pass_count += 1

            if pass_count == len(rail_uhc_tuple_list):
                self.Log('info',
                         '{} Expected voltage:{:.2f} seen on all UHC/Rail combinations.'.format(
                             rail_type, force_voltage))
            else:
                self.Log('error',
                         '{} Expected voltage:{:.2f} seen on {} of {} UHC/Rail combinations.'.format(
                             rail_type, force_voltage, pass_count, len(rail_uhc_tuple_list)))

        board.SetRailsToSafeState()

    def randomize_rail_uhc(self, board, rail_type):
        rail_uhc_tuple_list = []
        for rail in range(board.RAIL_COUNT[rail_type]):
            for uhc in range(board.UHC_COUNT):
                rail_uhc_tuple_list.append((rail, uhc))
        random.shuffle(rail_uhc_tuple_list)
        return rail_uhc_tuple_list

    def generate_trigger_queue(self, board,rail_type,force_voltage, dutid, rail,uhc):
        tracking_voltage_offset = 2
        tq_helper = TriggerQueueString(board, uhc, dutid)
        tq_helper.force_rail_type = rail_type
        tq_helper.force_rail = rail
        tq_helper.force_voltage = force_voltage
        tq_helper.tracking_voltage = force_voltage + tracking_voltage_offset
        tq_helper.force_clamp_high = CLAMP_LIMITS[rail_type][1]
        tq_helper.force_clamp_low = CLAMP_LIMITS[rail_type][0]
        tq_helper.force_low_voltage_limit = VOLTAGE_LIMITS[rail_type][0]
        tq_helper.force_high_voltage_limit = VOLTAGE_LIMITS[rail_type][1]
        tq_helper.force_rail_irange = I_RANGE[rail_type]

        trigger_queue = TriggerQueueAssembler()
        trigger_queue.LoadString(tq_helper.generateForceVoltageTriggerQueue())
        trigger_queue_data = trigger_queue.Generate()
        return trigger_queue_data

    def verify_alarm_status(self, board, rail, force_voltage):
        global_alarms = board.get_global_alarms()
        if global_alarms != []:
            self.Log('error', 'Failed to receive expected voltage:{:.2f}V.Received global alarm(s) on rail {}: {}'.format(force_voltage, rail, global_alarms))
            hclc_rail_alarms_register = 'Hclc{}Alarms'.format(rail)
            hclc_rail_alarm = board.ReadRegister(board.registers.HCLC_PER_RAIL_ALARMS,rail)
            rail_alarms = []
            for field in hclc_rail_alarm.fields():
                if getattr(hclc_rail_alarm, field) == 1:
                    rail_alarms.append(field)
            self.Log('error', 'Hclc Rails Alarm(s) set were {}'.format(rail_alarms))

    def verify_expected_voltage(self, board, rail, uhc, rail_type, expected_voltage):
        rail_voltage = board.get_rail_voltage(rail, rail_type)
        allowed_percentage_deviation = 15
        zero_buffer = 0.25
        if board.is_close(expected_voltage, rail_voltage, allowed_percentage_deviation,zero_buffer) == False:
            self.Log('error',
                    '{} rail:{} uhc:{}, instantaneous voltage is {:.2f}V is not within the allowable deviation of {}% of {:.2f}V'.format(
                        rail_type, rail, uhc, rail_voltage, allowed_percentage_deviation, expected_voltage))
            return False
        return True