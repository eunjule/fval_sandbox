# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object


class Hddps(Object):

    def __init__(self, rc, slot, subslot):
        super().__init__()
        self.rc = rc
        self.slot = slot
        self.subslot = subslot
        self.registers = {0x00C: 0x00}

        self.write_actions = {0x050: self.send_up_trigger}
        self.read_actions = {0x054: self.read_trigger_down}

    def bar_write(self, bar, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def bar_read(self, bar, offset):
        data = self.read_actions.get(offset, self.default_read_action)(offset)
        return data

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def default_read_action(self, offset):
        if offset not in self.registers:
            self.registers[offset] = 0x0

        return self.registers[offset]

    def read_trigger_down(self, offset):
        data = self.rc.read_hddps_down_trigger_buffer(self.slot)
        return data

    def send_up_trigger(self, offset, data):
        self.rc.write_trigger_up(self.slot, data)
