################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import argparse
import importlib
import os
import time
import unittest

from ThirdParty.teamcity import is_running_under_teamcity
from ThirdParty.teamcity.unittestpy import TeamcityTestRunner

try:
    from Common import hilmon

    # make sure even incorrectly written tests don't touch hardware in connect functions
    hilmon.hil.calHddpsConnect = lambda x: Exception()
    hilmon.hil.calHpccConnect = lambda x: Exception()
    hilmon.hil.calHvdpsConnect = lambda x: Exception()
    hilmon.hil.dpsConnect = lambda x, y: Exception()
    hilmon.hil.hbiDpsConnect = lambda x: Exception()
    hilmon.hil.hbiMbConnect = lambda x: Exception()
    hilmon.hil.hbiMtbConnect = lambda x: Exception()
    hilmon.hil.hbiPsConnect = lambda x: Exception()
    hilmon.hil.hbiPsdbConnect = lambda x: Exception()
    hilmon.hil.hddpsConnect = lambda x, y: Exception()
    hilmon.hil.hpccAcConnect = lambda x: Exception()
    hilmon.hil.hpccDcConnect = lambda x: Exception()
    hilmon.hil.hvdpsConnect = lambda x: Exception()
    hilmon.hil.hvilConnect = lambda x: Exception()
    hilmon.hil.psConnect = lambda x: Exception()
    hilmon.hil.rc3Connect = lambda: Exception()
    hilmon.hil.rc3LbConnect = lambda: Exception()
    hilmon.hil.rcConnect = lambda: Exception()
    hilmon.hil.tdbConnect = lambda x: Exception()
    hilmon.hil.tddConnect = lambda x: Exception()
    hilmon.hil.tiuCalBaseConnect = lambda: Exception()
except ImportError:
    pass


perf_count = 0
def perf_counter():
    global perf_count
    perf_count += 1000000
    return perf_count
        
# eliminate sleep time in all tested and test code!
time.sleep = lambda x: None
time.perf_counter = perf_counter



def get_runner(verbosity=1):
    if is_running_under_teamcity():
        Runner = TeamcityTestRunner
    else:
        Runner = unittest.TextTestRunner
    return Runner(verbosity=verbosity)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('testnames', nargs='*', default=['.'], help = 'Optional test name', type = str)
    parser.add_argument('-v', '--verbosity', default=1, type=int, help = 'Verbosity level')
    return parser.parse_args()


def discover_tests(testnames):
    tests = unittest.TestSuite()
    for testname in testnames:
        tests.addTests(discover_test(testname))
    return tests


def discover_test(testname):
    if os.path.isdir(testname):
        return discover_tests_in_directory(testname)
    else:
        return discover_tests_in_file(testname)


def discover_tests_in_directory(dir):
    return unittest.defaultTestLoader.discover(start_dir=dir, pattern='test_*.py', top_level_dir=os.getcwd())


def discover_tests_in_file(f):
    '''accepts a file name optionally appended with <.TestCase> or <.TestCase.test_name>'''
    testname = f.replace('\\', '.').replace('/', '.').replace('.py', '')
    import_from_test_name(testname)
    # try:
    return unittest.defaultTestLoader.loadTestsFromName(testname)
    # except AttributeError:
        # raise Exception('Failure importing {}'.format(testname)) from None


def import_from_test_name(testname):
    '''If the name is not directly importable, drop the last part of the name and try again'''
    try:
        i = importlib.import_module(testname)
        return
    except :
        pass

    prefix = testname.rsplit('.', 1)[0]
    if prefix == testname:
        raise ImportError('no module named {}'.format(testname))
    else:
        import_from_test_name(prefix)

def ensure_importability(test):
    """unit test directories must have a __init__.py"""
    if os.path.isdir(test):
        for root, dirs, files in os.walk(test):
            _ignore_non_unit_test_directories_in_root(dirs, root)
            _ignore_pycache_directories(dirs)
            if not '__init__.py' in files:
                with open(os.path.join(root, '__init__.py'), 'w') as f:
                    pass
                # raise FileNotFoundError(f'__init__.py not found in folder: {root}')


def _ignore_pycache_directories(dirs):
    try:
        dirs.remove('__pycache__')
    except ValueError:
        pass


def _ignore_non_unit_test_directories_in_root(dirs, root):
    if root == '.':
        dirs.clear()
        dirs.append('unit_tests')


args = parse_args()
for test in args.testnames:
    ensure_importability(test)

suite = discover_tests(args.testnames)
runner = get_runner(verbosity=args.verbosity)
runner.run(suite)
