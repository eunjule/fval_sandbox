
macro(SWIG_TARGET target)
    if (MSVC)
        set_target_properties(${target} PROPERTIES
            OUTPUT_NAME _${target}
            SUFFIX .pyd
        )
    else (MSVC)
        set_target_properties(${target} PROPERTIES
            PREFIX ""
            OUTPUT_NAME _${target}
        )
    endif (MSVC)
    
    target_link_libraries(${target} ${PYTHON_LIBRARIES})
    include_directories(${PYTHON_INCLUDE_DIRS})
endmacro(SWIG_TARGET)
