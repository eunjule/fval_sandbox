@echo off
REM INTEL CONFIDENTIAL
REM
REM Copyright 2015-2020 Intel Corporation.
REM
REM This software and the related documents are Intel copyrighted materials,
REM and your use of them is governed by the express license under which they
REM were provided to you ("License"). Unless the License provides otherwise,
REM you may not use, modify, copy, publish, distribute, disclose or transmit
REM this software or the related documents without Intel's prior written
REM permission.
REM
REM This software and the related documents are provided as is, with no express
REM or implied warranties, other than those that are expressly stated in the
REM License.
@echo on

ThirdParty\python\python.exe -m ThirdParty.coverage run run_tests.py %*
ThirdParty\python\python.exe -m ThirdParty.coverage html
START /B htmlcov\index.html
