################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import time
import threading
from Common import fval

from Hbicc.tools.CaptureDecoder.CaptureExpectUtilities import *


class AddressVectors(fval.Object):
    def __init__(self, context, length, *args, **kwargs):
        vector_address = context.vecaddr
        self.length = int(length)

    def Size(self):
        return self.length

    def Generate(self, array, offset, *args, **kwargs):
        for i in range(self.length):
            address = offset // 16
            address_low_21bits = address & 0x1FFFFF

            array[offset:offset + 8] = int(f'{address_low_21bits:021b}'.translate(ADDRESS_VECTOR_TRANSLATION_TABLE),
                                           8).to_bytes(8, byteorder='little')
            array[offset + 8:offset + 15] = int(f'{address >> 21:014b}'.translate(ADDRESS_VECTOR_TRANSLATION_TABLE),
                                                8).to_bytes(7, byteorder='little')
            array[offset + 15] = 0x80
            offset += 16
            # if i % 100000 == 0:
            #     print(f'Vector Generator Progess: {i/1000:,}k vectors completed')

        return self.length

        # return low_data + high_data + self._control_bytes()

    def _control_bytes(self):
        return bytes([self.lrpt << 3, 0x80 + (self.ctv << 5) + self.mtv])


class AddressVectorsPlusAtt():
    def __init__(self, context, length, ctv=1, mtv=0, lrpt=0, data=None, *args, **kwargs):
        vector_address = context.vecaddr
        self.length = int(length)
        self.ctv = ctv
        self.mtv = mtv
        self.lrpt = lrpt
        self.data = data
        self.ctp = 0b00000000000000000001001000000000010

    def Size(self):
        return self.length

    def Generate(self, array, offset, *args, **kwargs):
        ctp = self.get_active_pins_from_ctp(self.ctp)
        values = self.index_addres_skip_strobe_pins(offset, ctp, self.length)
        div = self.length//4
        for index, address in enumerate(values):
            next_offset = offset + (self.length * 16)
            array[offset:next_offset] = self._vector(address)
            offset += 16
            # if index % div == 0:
            #     print(f'Vector Generator Progess: {index//1000:,}k vectors completed')
        return self.length

    def _vector(self, data):
        low_data = (data & 0x7FFFFFFFFFFFFFFF).to_bytes(8, byteorder='little')
        high_data = (data >> 63).to_bytes(6, byteorder='little')
        return low_data + high_data + self._control_bytes()

    def _control_bytes(self):
        return bytes([self.lrpt << 3, 0x80 + (self.ctv << 5) + self.mtv])

    def index_addres_skip_strobe_pins(self, address, ctp, length):
        values = []
        maps = [[0, 1, 2, 3, 4, 5, 6, 7], [7, 6, 5, 4, 3, 2, 1, 0], [6, 6, 4, 4, 2, 2, 0, 0], [0, 0, 3, 3, 1, 1, 2, 2]]
        for i in range(length// (len(maps) * 8)):
            for map in maps:
                for index in map:
                    values.append(self.one_encoded_vector(index, ctp))
        return values

    def one_encoded_vector(self, address, ctp, compare_default='000'):
        look = {'0': '000', '1': '001'}
        base = ''
        next_address_ = address & int('1' * len(ctp), 2)
        str_address = f'{next_address_:035b}'
        str_address = list(str_address)
        str_address.reverse()
        for i in range(35):
            for index, pin in enumerate(ctp):
                if (34 - pin) == i:
                    base += look[str_address[index]]
                    break
            else:
                if i in [34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23]:
                    base += look[str_address[0]]
                elif i in [21, 20]:
                    base += look[str_address[1]]
                elif i in [18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]:
                    base += look[str_address[2]]
        return int(base, 2)

    def get_active_pins_from_ctp(self, ctp):
        active = []
        for i in range(35):
            pin = ctp & (1 << i)
            if pin != 0:
                active.append(i)
        return active


class BaseGenerator():
    def __init__(self):
        pass

    def construct_vector(self, control_bytes, encoded, expected, key):
        low_data = (encoded & 0x7FFFFFFFFFFFFFFF).to_bytes(8, byteorder='little')
        high_data = (encoded >> 63).to_bytes(6, byteorder='little')
        vector = low_data + high_data + control_bytes
        self.random_1M_vectors[key] = [expected, vector]
        return vector

    def encode_value(self):
        encoded = 0
        expected = random.randint(self.rand_min, self.rand_max)
        # print(f'ex   :  0b{expected:035b}')
        # expectedx = expected ^ self.fix_0
        # print(f'exx  :  0b{expectedx:035b}')
        for index, i in enumerate(self.active_channels):
            x = (expected >> index) & 1
            # x = ((self.fix_0 >> index) & 1) ^ x
            if x:
                encoded |= 1 << (i * 3)
        return encoded, expected

    def _get_list(self, value, max_range):
        values = []
        for i in range(max_range):
            if (value >> i) & 1:
                values.append(i)
        return values


M = 1000000
class RandomAddressEncoder(BaseGenerator):
    def __init__(self, context, length, ctv=1, mtv=0, lrpt=0, data=None, *args, **kwargs):
        super(BaseGenerator).__init__()
        vector_address = context.vecaddr
        self.length = int(length)
        self.ctv = ctv
        self.mtv = mtv
        self.lrpt = lrpt
        self.data = data
        self.ctp = kwargs['ctp']
        self.slices = kwargs['slices']
        self.args = args
        self.kwargs = kwargs
        self.slice_list = []
        self.random_1M_vectors = {x:None for x in range(M)}
        self.b = bytearray(16*self.length)
        self.active_channels = None
        self.rand_max = 0
        self.rand_min = 0
        self.ceb_data = {}

    def Size(self):
        return self.length

    def Generate(self, array, offset, *args, **kwargs):
        self.offset = offset
        dst = self.set_dst_folder()
        ceb = self.set_up_ctv_expected_builder(dst)
        self.set_min_max_for_randomization()
        self.encode_all_vectors(array, ceb)
        ceb.done()
        return self.length

    def encode_all_vectors(self, array, ceb):
        channel_set_values = [-1, -1, -1, -1]

        control_bytes = self.get_control_bytes()
        self.construct_1M_random_vectors(control_bytes)
        self.construct_all_vectors_from_1M_random_vectors(array, channel_set_values)
        self.add_all_cycles_to_ceb(ceb, channel_set_values)

    def add_all_cycles_to_ceb(self, ceb, channel_set_values):
        start_timer = time.time()
        for vector_num in range(self.length):
            expected = self.ceb_data[vector_num]
            self.add_cycle_to_ceb(ceb, channel_set_values, expected, vector_num)
        end_timer = time.time()
        print(f'Matts tool: {end_timer - start_timer}')

    def construct_all_vectors_from_1M_random_vectors(self, array, channel_set_values):
        start_timer = time.time()
        threads = []
        divider = 10
        step_size = self.length // divider
        for i in range(divider):
            start = i * step_size
            end = start + step_size
            threads.append(threading.Thread(target=self.construct_vectors, args=(array, channel_set_values, start, end)))
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        end_timer = time.time()
        print(f'Timer for putting things to array: {end_timer - start_timer}')

    def construct_1M_random_vectors(self, control_bytes):
        start_timer = time.time()
        threads = []
        step_size = M // 5
        for i in range(5):
            start = i * step_size
            end = start + step_size
            threads.append(threading.Thread(target=self.contruct_look_up, args=(control_bytes, start, end)))
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        end_timer = time.time()
        print(f'Timer for ONE M is {end_timer - start_timer}')

    def contruct_look_up(self, control_bytes, min, max):
        for i in range(min, max):
            encoded, expected = self.encode_value()
            vector = self.construct_vector(control_bytes, encoded, expected, i)
            self.random_1M_vectors[i] = [expected, vector]

    def construct_vectors(self, array, channel_set_values, min, max):
        for vector_num in range(min, max):
            key = random.randint(0, M - 1)
            expected, vector = self.random_1M_vectors[key]
            self.ceb_data[vector_num] = expected
            self.add_vector_to_array(array, vector, vector_num)

    def add_vector_to_array(self, array, vector, vector_num):
        start = self.offset + (vector_num * 16)
        end = start + 16
        array[start: end] = vector

    def add_cycle_to_ceb(self, ceb, channel_set_values, expected, vector_num):
        for i in self.channel_sets:
            channel_set_values[i] = expected
        ceb.add_cycle(vector_num, self.ctp, channel_set_values[0], channel_set_values[1], channel_set_values[2], channel_set_values[3])

    def set_min_max_for_randomization(self):
        self.active_channels = self._get_list(self.ctp, 35)
        self.rand_max = (1 << len(self.active_channels)) - 1
        self.rand_min = self.rand_max // 2

    def get_control_bytes(self):
        control_bytes = bytes([self.lrpt << 3, 0x80 + (self.ctv << 5) + self.mtv])
        return control_bytes

    def set_up_ctv_expected_builder(self, dst):
        ceb = CTVExpectBuilder(dst)
        ceb.set_slices(self._get_list(self.kwargs['slices'], 5))
        self.pms_list = self._get_list(self.kwargs['pms'], 4)
        self.channel_sets = self._get_list(self.kwargs['channel_sets'], 4)
        ceb.set_chips(self.pms_list)
        ceb.start()
        return ceb

    def set_dst_folder(self):
        dir = os.getcwd()
        dst = os.path.join(dir, 'STREAM_CAPTURE')
        os.makedirs(dst, exist_ok=True)
        return dst


ADDRESS_VECTOR_TRANSLATION_TABLE = str.maketrans({'0': '4', '1': '5'})


class ConstantVectors():
    def __init__(self, context, length, ctv=0, mtv=0, lrpt=0, data=None, *args, **kwargs):
        vector_address = context.vecaddr
        self.length = int(length)
        self.ctv = ctv
        self.mtv = mtv
        self.lrpt = lrpt
        self.data = data

    def Size(self):
        return self.length

    def Generate(self, array, offset, *args, **kwargs):
        next_offset = offset + (self.length * 16)
        array[offset:next_offset] = self._vector() * self.length
        return self.length

    def _vector(self):
        low_data = (self.data & 0x7FFFFFFFFFFFFFFF).to_bytes(8, byteorder='little')
        high_data = (self.data >> 63).to_bytes(6, byteorder='little')
        return low_data + high_data + self._control_bytes()

    def _control_bytes(self):
        return bytes([self.lrpt << 3, 0x80 + (self.ctv << 5) + self.mtv])


class CompareLowVectors(ConstantVectors):
    def _vector(self):
        return b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' + self._control_bytes()


class CompareHighVectors(ConstantVectors):
    def _vector(self):
        return b'\x49\x92\x24\x49\x92\x24\x49\x12\x49\x92\x24\x49\x92\x00' + self._control_bytes()


class KeepVectors(ConstantVectors):
    def _vector(self):
        return b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x7F\xFF\xFF\xFF\xFF\xFF\x03' + self._control_bytes()


class DriveHighVectors(ConstantVectors):
    def _vector(self):
        return b'\x6D\xDB\xB6\x6D\xDB\xB6\x6D\x5B\x6D\xDB\xB6\x6D\xDB\x02' + self._control_bytes()


class DriveZeroVectors(ConstantVectors):
    def _vector(self):
        return b'\x24\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02' + self._control_bytes()


class NoCompareDriveZVectors(ConstantVectors):
    def _vector(self):
        return b'\x92\x24\x49\x92\x24\x49\x92\x24\x92\x24\x49\x92\x24\x01' + self._control_bytes()


class RandomCompareHighAndLow(ConstantVectors):
    def _vector(self):
        data = random.getrandbits(106) & 0x0499249249249249249
        data = data.to_bytes(14, byteorder='little')
        return data + self._control_bytes()

