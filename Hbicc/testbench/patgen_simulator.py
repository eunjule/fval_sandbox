# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import operator

from Common import fval
from Hbicc.instrument import patgen_register
from Hbicc.instrument.patgen_register import ALARMS1, CTV_DATA_STREAM_END_ADDRESS_REG, \
    CTV_DATA_STREAM_START_ADDRESS_REG, CTV_HEADER_STREAM_END_ADDRESS_REG, CTV_HEADER_STREAM_START_ADDRESS_REG, \
    ERROR_STREAM_PM_END_ADDRESS_REG, ERROR_STREAM_PM_START_ADDRESS_REG
from Hbicc.testbench import capture_structs
from Hbicc.testbench.capturefailures import CTV_DATA_END_OF_BURST_BLOCK_NUM, STREAM_BLOCK, PIN_ERRROR_BLOCK
from Hbicc.testbench.trigger_utility import flag_bit_trigger_map, normal_trigger_list, propagate_trigger_list, \
    TriggerUtility, TriggerWord


BYTES_PER_VECTOR = 16
NUM_PM = 4
REG_BYTE_NUM = 4

IMMEDIATE_LSB           = 0
IMMEDIATE_BITS          = 32
REGISTER_A_LSB          = 32
REGISTER_A_BITS         = 5
REGISTER_B_LSB          = 37
REGISTER_B_BITS         = 5
DESTINATION_LSB         = 42
DESTINATION_BITS        = 5
BRANCH_TYPE_LSB         = 47
BRANCH_TYPE_BITS        = 5
BRANCH_BASE_LSB         = 52
BRANCH_BASE_BITS        = 2
BRANCH_CONDITION_LSB    = 54
BRANCH_CONDITION_BITS   = 4
BRANCH_INVERT_LSB       = 58
BRANCH_INVERT_BITS      = 1
OP_CODE_LSB             = 59
OP_CODE_BITS            = 7
OP_TYPE_LSB             = 66
OP_TYPE_BITS            = 4

LOCAL_REPEAT_MASK = 0x1F
LINK_ENABLE_MASK = 0x02

FLAGS_RESERVED_MASK = 0x00010000
FLAGS_ZERO_MASK = 0x02000000
FLAGS_CARRY_MASK = 0x04000000
FLAGS_OVERFLOW_MASK = 0x08000000
FLAGS_SIGN_MASK = 0x10000000

SIGNED_MAX_POSITIVE = 0x7FFFFFFF
SIGNED_MAX_NEGATIVE = -0x80000000
SIGNED_SIGN_MASK = 0x80000000
REGISTER_SIZE_MASK = 0xFFFFFFFF
REGISTER_MAX_VALUE_PLUS_ONE = 0x100000000

BRANCH_BASE_GLOBAL = 0
BRANCH_BASE_COMMON_FILE = 1
BRANCH_BASE_RELATIVE = 2
BRANCH_BASE_PATTERN = 3

ECC_RESET_MASK = 0x00000002

SIMULATOR_LIMIT = 300000

class PatGen(fval.Object):
    def __init__(self, system):
        super().__init__()

        self.system = system
        self.slices = [PatGenSlice(slice=i, common=self) for i in range(5)]
        self.registers = {0x20: 0x0001_0000}
        self.read_actions = {0x1C: self.resets_and_status}
        self.unittest_registers = {0x178: None}
        self.is_simulator_active = True
        self.si5344_registers = {}
        self.skip_multi_burst_simulation = False

    def bar_write(self, bar, offset, data):
        if bar == 0:
            self.register_write(offset, data)
        else:
            self.slices[bar-1].register_write(offset, data)

    def bar_read(self, bar, offset):
        if bar == 0:
            return self.register_read(offset)
        else:
            return self.slices[bar-1].register_read(offset)
    
    def register_write(self, offset, data):
        self.registers[offset] = data
    
    def register_read(self, offset):
        data = self.registers.get(offset, 0)
        self.do_read_action(offset)
        return data

    def do_read_action(self, offset):
        self.read_actions.get(offset, lambda x: None)(offset)

    def dma_read(self, offset, length):
        slice, slice_offset = self.slice_from_dma_offset(offset)
        return self.slices[slice].dma_read(slice_offset, length)

    def dma_write(self, offset, data):
        slice, slice_offset = self.slice_from_dma_offset(offset)
        self.slices[slice].dma_write(slice_offset, data)

    def slice_from_dma_offset(self, offset):
        slice = (offset >> 36)
        slice_offset = offset & 0xFFFFFFFFF
        return slice, slice_offset

    def trigger_down(self, data):
        self.write_data_to_last_seen_trigger(data)
        trigger_type = data & 0xf
        targeted_domain = TriggerWord(data).target_domain
        if self.trigger_should_be_propagated_in_domain(targeted_domain, trigger_type):
            down_trigger_utility = TriggerUtility(None, None, data)
            data = down_trigger_utility.get_propagate_trigger()
            self.write_data_to_last_seen_trigger(data)
        for slice_simulator in self.slices:
            if targeted_domain == slice_simulator.get_domain_id():
                slice_simulator.trigger_down(data)

    def trigger_should_be_propagated_in_domain(self, domain_id, trigger_type):
        return trigger_type in normal_trigger_list and self.is_master_running_pattern_present_in_domain(domain_id)

    def write_data_to_last_seen_trigger(self, data):
        self.slices[0].registers.update({patgen_register.LAST_TRIGGER_SEEN.ADDR: data})

    def is_master_running_pattern_present_in_domain(self, domain_number):
        for slice_simulator in self.slices:
            slice_domain_number = slice_simulator.get_domain_id()
            if slice_simulator._is_domain_master() and slice_domain_number == domain_number:
                return slice_simulator.running_pattern
        else:
            return False

    def sync(self):
        self.registers[0x1C] = self.registers.get(0x1C, 0) | 0x10
        for slice in self.slices:
            slice.sync()

    def resets_and_status(self, offset):
        if self.registers.get(0x1C, 0) & 0x10:
            self.registers[0x1C] &= ~0x10
    
    def run_pattern(self):
        if self.is_simulator_active:
            if not self.skip_multi_burst_simulation:
                for slice in self.slices:
                    slice.run_pattern()

    def si5344_read(self, register):
        return self.si5344_registers.get(register, 0xFF)

    def si5344_write(self, register, data):
        self.si5344_registers[register] = data

    def fpga_version(self):
        return self.registers[0x20]

    def fpga_version_string(self):
        version = self.fpga_version()
        return f'{version >> 16}.{version & 0xFFFF}'

    def get_pin_error_count(self, pm, slice_index):
        return self.slices[slice_index].register_read(patgen_register.ERROR_STREAM_PM_COUNT.ADDR + pm.slot_index * 4)

    def receive_sync_pulse(self):
        pass


def record_trace(func):
    '''decorator'''
    def wrapper(slice_object, data):
        if slice_object._pattern_id == 0xFFFFFFFF:
            func(slice_object, data)
        else:
            if slice_object._trace_count == 0:
                slice_object.reinitialize_trace_memory()
            trace = capture_structs.TRACE()
            trace.source_address = slice_object.current_address()
            func(slice_object, data)
            trace.destination_address = slice_object.current_address()
            trace.ctp_mask = slice_object._ctp_mask
            trace.event_type = slice_object.branch_type(data)
            trace.pattern_id = slice_object._pattern_id
            offset = slice_object._trace_count * 32
            next_trace_offset = offset + 32
            if slice_object.trace_overwrite_protection:
                store_trace_to_memory_with_overwrite_protection(next_trace_offset, offset, slice_object, trace)
            else:
                store_trace_to_memory_without_overwrite_protection(next_trace_offset, offset, slice_object, trace)
            slice_object.trace_index = slice_object._trace_count
            increment_trace_count_and_update_trace_overflow_alarm(slice_object)

    def increment_trace_count_and_update_trace_overflow_alarm(slice_object):
        slice_object._trace_count += 1
        if slice_object._trace_count >= slice_object.get_trace_block_count_limit():
            slice_object._set_alarms1('trace_capture_overflow')

    def store_trace_to_memory_with_overwrite_protection(next_trace_offset, offset, slice_object, trace):
        if slice_object._trace_count < slice_object.get_trace_block_count_limit():
            slice_object._trace_memory[offset:next_trace_offset] = bytes(trace)
            slice_object._trace_overwrite_protected_count += 1

    def store_trace_to_memory_without_overwrite_protection(next_trace_offset, offset, slice_object, trace):
        slice_object._trace_memory[offset:next_trace_offset] = bytes(trace)
        if slice_object._trace_count < slice_object.get_trace_block_count_limit():
            slice_object._trace_overwrite_protected_count += 1

    return wrapper
            
            
            
class PatGenSlice(fval.Object):
    def __init__(self, slice, common):
        self.system = common.system
        self.device = common
        self.slice = slice
        self.memory_table = {}
        self.ctv_data_memory_not_limited = bytearray()
        self.registers = {0x30: 0x1E01,
                          0x44: 0x27,
                          0x84: 0x3E9,
                          0x8C: 0x10,
                          0x90: 0x10,
                          0x140: 0x5,
                          0x160: 0xDEADBEEF,
                          0x164: 0x00000000,
                          0x168: 0x00000000,
                          0x178: 0x00010000,
                          0x180: 0,
                          0x184: 0,
                          0x188: 0,
                          0x18C: 0,
                          0x190: 0,
                          0x194: 0,
                          0x198: 0,
                          0x19C: 0,
                          0x284: 1,
                          0x29C: 0x6FF,
                          0x0A0C: 0,
                          0x0A10: 0,
                          0x0A14: 0,
                          0x0A18: 0
                          }
        self._stack = []
        self._staged_mask = 0
        self._active_pattern_mask = 0
        self.global_capture_enabled = 1
        self._reset_counters()
        self.processor_reg = {x:0 for x in range(32)}
        self.fail_registers = {}
        self._capture_distribution = 0xFF
        self._capture_block_size_exponent = 6
        self.write_actions = {0x50: self.manual_trigger_up,
                              0x144: self.ddr_control,
                              0x164: self.pattern_control_write,
                              0x29C: self.capture_distribution_control,
                              0xA1C: self._set_un_parked_duts}
        self.read_actions = {0x164: self.pattern_control_read,
                             0x280: self._ctv_header_count_register,
                             0x284: self._ctv_data_count_register,
                             0x288: self._trace_count_register,
                             0x28c: self._error_pm_count,
                             0x290: self._error_pm_count,
                             0x294: self._error_pm_count,
                             0x298: self._error_pm_count}
        self.add_instruction_method_table()
        self.add_external_op_method_table()
        self.add_branch_method_table()
        self.add_condition_table()
        self._add_branch_base_table()
        self.add_pin_state_word_method_table()
        self.add_vector_op_method_table()
        self.setup_clocks()
        self._serialized_global_channel_set = 0
        self._is_first_pcall = True
        self._mem_block_stack = []
        self.ctv_header_overwrite_protection = True
        self.ctv_data_overwrite_protection = True
        self.pin_error_overwrite_protection = True
        self.pin_state_control = 32
        self.trace_overwrite_protection = True
        self.running_pattern = False
        self._un_parked_dut_list = []

    def register_write(self, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)
        
    def default_write_action(self, offset, data):
        self.registers[offset] = data & 0xFFFFFFFF

    def do_read_action(self, offset):
        self.read_actions.get(offset, lambda x: None)(offset)

    def register_read(self, offset):
        if self.fail_registers.get(offset, False):
            if offset == 0x178:
                data = self.registers.get(offset, 0) | self.fail_registers[offset]
            else:
                data = self.fail_registers[offset]
        else:
            self.do_read_action(offset)
            data = self.registers.get(offset, 0)
        return data

    def dma_read(self, offset, length):
        if self._ecc_error_injection_is_enabled():
            self._set_ecc_alarm()
        block_offset, block_data = self.get_memory_block(offset)
        if offset == block_offset and length == len(block_data):
            return bytes(block_data)
        else:
            start = offset - block_offset
            end = start + length
            return bytes(block_data[start:end])

    def _ecc_error_injection_is_enabled(self):
        return self.registers.get(0x144, 0) & 0x30

    def _set_ecc_alarm(self):
        self.registers[0x16C] = self.registers.get(0x16C, 0) | 0x40

    def _clear_ecc_alarm(self):
        self.registers[0x16C] = self.registers.get(0x16C, 0) & ~0x40

    def dma_write(self, offset, data):
        if offset in self.memory_table:
            del self.memory_table[offset]
        self.memory_table[offset] = data
        self._remove_stale_ram_data()
    
    def _remove_stale_ram_data(self):
        '''Avoid using lots of RAM for simulation, becomes slow otherwise'''
        if len(self.memory_table.keys()) > 12:
            oldest_key = list(self.memory_table.keys())[0]
            del self.memory_table[oldest_key]

    def get_memory_block(self, offset):
        for block_offset in reversed(list(self.memory_table.keys())):
            block_data = self.memory_table[block_offset]
            if block_offset <= offset < (block_offset + len(block_data)):
                self._mark_memory_block_least_recently_used(block_offset)
                return block_offset, block_data
        else:
            self.memory_table[offset] = bytearray(1024)
            self._remove_stale_ram_data()
            return offset, self.memory_table[offset]
    
    def _mark_memory_block_least_recently_used(self, block_offset):
        '''Use insertion order for LRU algorithm'''
        block_data = self.memory_table[block_offset]
        del self.memory_table[block_offset]
        self.memory_table[block_offset] = block_data

    def convert_memory_block_to_bytearray(self, offset):
        offset, _ = self.get_memory_block(offset)
        self.memory_table[offset] = bytearray(self.memory_table[offset])
    
    def _ctv_header_count_register(self, offset):
        self.registers[offset] = self._ctv_header_count
    
    def _ctv_data_count_register(self, offset):
        self.registers[offset] = self._ctv_data_count
    
    def _trace_count_register(self, offset):
        self.registers[offset] = self._trace_count

    def _error_pm_count(self, offset):
        pm_index = (offset - 0x28c) // REG_BYTE_NUM
        self.registers[offset] = self._pin_error_count[pm_index] * 2

    def setup_clocks(self):
        self.registers[0xDC] = 0x0303E001
        self.registers[0xC0] = 0x0303E015
        self.registers[0xC4] = 0x030F8015
        self.registers[0xC8] = 0x18060001
        self.registers[0xCC] = 0x0C030001
        self.registers[0xD0] = 0x08060003
        self.registers[0xD4] = 0x18060001
        self.registers[0xD8] = 0x18030001
        self.registers[0xDC] = 0x18030003
        self.registers[0xE0] = 0x0303E001
        self.registers[0xF0] = 0x0303E00A
        
    def trigger_down(self, data):
        if self.running_pattern:
            trigger_type = data & 0xf
            if trigger_type in flag_bit_trigger_map.keys() and trigger_type in propagate_trigger_list:
                flag_bit = flag_bit_trigger_map[trigger_type]
                self.registers[patgen_register.FLAGS.ADDR] |= (1 << flag_bit)

    def get_domain_id(self):
        return (self.registers.get(patgen_register.TRIGGER_CONTROL.ADDR, 0) & 0xf0) >> 4

    def manual_trigger_up(self, offset, data):
        self.registers[offset] = data
        self.trigger_up(data)

    def ddr_control(self, offset, data):
        self.registers[offset] = data & ~ECC_RESET_MASK
        if data & 0x2:
            self.registers[0x148] = 0
            self.registers[0x14C] = 0
            self._clear_ecc_alarm()

    def pattern_control_read(self, offset):
        PRESTAGE_BIT_MASK = 0x00000002
        if (self.registers.get(0x164, 0) & PRESTAGE_BIT_MASK) and self._prestage_delay:
            self.registers[0x164] &= ~PRESTAGE_BIT_MASK
        self._prestage_delay = bool(self.registers.get(0x164, 0) & PRESTAGE_BIT_MASK)

    def pattern_control_write(self, offset, data):
        write_data = patgen_register.PATTERN_CONTROL(value=data)
        reg_data = patgen_register.PATTERN_CONTROL(value=self.registers[offset])
        write_data.PrestageDone = reg_data.PrestageDone
        self.registers[offset] = write_data.value
        self._abort = write_data.AbortPattern
        if write_data.PrestagePattern:
            self._prestage_delay = False
            for i in range(5):
                if write_data.SliceGroupMask & (0b00001 << i):
                    self.device.slices[i].registers[offset] = self.device.slices[i].registers[offset] | 0x40000000
        if reg_data.reset_alarm == 0 and write_data.reset_alarm == 1:
            self.reset_all_alarm()

    def reset_all_alarm(self):
        self.registers.update({patgen_register.ALARMS1.ADDR: 0})
        self.registers.update({patgen_register.ALARMS2.ADDR: 0})

    def capture_distribution_control(self, offset, data):
        self._capture_distribution = data & 0xFF
        self._capture_block_size_exponent = (data >> 8) & 0xFF
        self.registers[offset] = data
    
    def sync(self):
        self.registers[0x164] = self.registers.get(0x164, 0) & ~0x80

    def run_pattern(self):
        if self._active_and_prestaged():
            self.running_pattern = True
            self.Log('debug', f'Simulator running pattern on slice {self.slice}')
            self.global_capture_enabled = 1
            self._get_pin_vector_handlers()
            self._setup_memory_regions()
            self._set_capture_window(True)
            self._set_up_ctv_data_handlers()
            self.reset_counters_and_overflow_alarms()
            self._stack = []
            self._mem_block_stack = []
            self._pattern_byte_offset = self.registers.get(0x168, 0) * 16
            try:
                self._dispatch_vectors()
            except PatGenSlice.StackError:
                self._set_stack_alarm()
                self._set_instruction_processor_error_end_status()
            except PatGenSlice.PatternAbort:
                self._set_instruction_processor_error_end_status()

    def reset_counters_and_overflow_alarms(self):
        self._reset_counters()
        self._reset_alarms1_ctv_row_capture_overflow()
        self._reset_alarms1_ctv_header_capture_overflow()
        self._reset_alarms1_trace_capture_overflow()
        self._reset_alarms1_error_pm_capture_overflow()

    def _dispatch_vectors(self):
        self.pattern_end_instruction_seen = False
        self.pattern_end_pvc_seen = False
        self.setup_loopback_model()
        dispatch_table = self._get_dispatch_table()
        while True:
            ram_read_address = self._pattern_byte_offset - self._ram_start_byte_adress
            vector = self.ram[ram_read_address:ram_read_address + BYTES_PER_VECTOR]
            vector_type = vector[15] >> 4
            dispatch_table[vector_type](vector)
            if self.pattern_end_pvc_seen:
                for pm in range(NUM_PM):
                    self.ctv_data_handlers[pm].commit_outstanding_ctv_data()
                for pm in range(NUM_PM):
                    self.create_ctv_data_header_eob(pm)
                    self.create_pin_error_data_header_eob(pm)
                self.running_pattern = False
                break
            self._pattern_byte_offset += BYTES_PER_VECTOR

    def setup_loopback_model(self):
        for pm_slice in self._pin_vector_handlers:
            pat_control_reg=pm_slice.register_read(offset=0x80)
            freq_exp= (pat_control_reg >> 2)&0x7
            pm_slice.configure_loopback_pins(freq_exp)

    def _get_dispatch_table(self):
        return {0: self._illegal_vector,
                1: self._illegal_vector,
                2: self._illegal_vector,
                3: self._illegal_vector,
                4: self._illegal_vector,
                5: self.pin_state_word,
                6: self.instruction,
                7: self.metadata,
                8: self.data_vector,
                9: self.data_vector,
                10: self.data_vector,
                11: self.data_vector,
                12: self.data_vector,
                13: self.data_vector,
                14: self.data_vector,
                15: self.data_vector}
    
    def _active_and_prestaged(self):
        return (self.registers[0x164] & 0xC0000000) == 0xC0000000
    
    def current_address(self):
        return self._pattern_byte_offset // BYTES_PER_VECTOR

    def _reset_alarms1_ctv_row_capture_overflow(self):
        alarms1 = ALARMS1(self.registers.get(ALARMS1.ADDR, 0))
        alarms1.ctv_row_capture_overflow = 0
        self.registers.update({ALARMS1.ADDR: alarms1.value})

    def _reset_alarms1_ctv_header_capture_overflow(self):
        alarms1 = ALARMS1(self.registers.get(ALARMS1.ADDR, 0))
        alarms1.ctv_header_capture_overflow = 0
        self.registers.update({ALARMS1.ADDR: alarms1.value})

    def _reset_alarms1_trace_capture_overflow(self):
        alarms1 = ALARMS1(self.registers.get(ALARMS1.ADDR, 0))
        alarms1.trace_capture_overflow = 0
        self.registers.update({ALARMS1.ADDR: alarms1.value})

    def _reset_alarms1_error_pm_capture_overflow(self):
        alarms1 = ALARMS1(self.registers.get(ALARMS1.ADDR, 0))
        for i in range(NUM_PM):
            setattr(alarms1, 'error_pm_{}_capture_overflow'.format(i), 0)
        self.registers.update({ALARMS1.ADDR: alarms1.value})

    def _reset_counters(self):
        self._trace_count = 0
        self._trace_overwrite_protected_count = 0
        self._pattern_cycle = 0
        self._user_cycle = 0
        self._previous_pcall_user_cycle = 0
        self._pattern_counter = 0
        self._ctv_header_count = 0
        self._ctv_count = 0
        self._ctv_data_count = 0
        self._pin_error_count = {pm: 0 for pm in range(NUM_PM)}
        self._pin_error_overwrite_protected_count = {pm: 0 for pm in range(NUM_PM)}
        self._last_pcall_address = 0
        self.trace_index = -1
        self._ctp_mask = 0
        self._pattern_id = 0
        self._log1 = 0
        self._pattern_base = self.registers[0x168]
        self._is_first_pcall = True
        self._instruction_repeat = 1
        self._total_cycle_count = 0
        self._clear_pattern_abort()
        self._clear_stack_alarm()
        self._clear_flags_register()

    def _set_up_ctv_data_handlers(self):
        self.ctv_data_handlers = []
        for pm in range(NUM_PM):
            self.ctv_data_handlers.append(CtvDataHandler(self, pm))

    def _clear_pattern_abort(self):
        self.registers[0x164] = self.registers.get(0x164, 0) & ~0x100

    def _setup_memory_regions(self):
        self._setup_pattern_memory()
        self._setup_trace_memory()
        self._setup_ctv_header_memory()
        self._setup_ctv_data_memory()
        self._setup_pin_error_memory()

    def _setup_pattern_memory(self):
        self._pattern_byte_offset = self.registers.get(0x168, 0) * BYTES_PER_VECTOR
        self._ram_start_byte_adress, self.ram = self.get_memory_block(self._pattern_byte_offset)
        self._ram_end_byte_adress = len(self.ram) + self._ram_start_byte_adress

    def _load_new_pattern_memory(self, return_mode):
        if return_mode:
            _, self.ram = self.get_memory_block(self._mem_block_address)
            self._ram_start_byte_adress = self._mem_block_address
        else:
            _, self.ram = self.get_memory_block(self._pattern_byte_offset)
            self._ram_start_byte_adress = self._pattern_byte_offset
        self._ram_end_byte_adress = len(self.ram) + self._ram_start_byte_adress
    
    def _setup_trace_memory(self):
        trace_memory_offset = self.get_trace_memory_offset()
        self.convert_memory_block_to_bytearray(trace_memory_offset)
        self._trace_memory = self.memory_table[trace_memory_offset]

    def get_trace_memory_offset(self):
        trace_start = self.registers[0x18C] << 6
        trace_memory_offset, _ = self.get_memory_block(trace_start)
        return trace_memory_offset

    def reinitialize_trace_memory(self):
        trace_memory_offset = self.get_trace_memory_offset()
        self.memory_table[trace_memory_offset] = bytearray(1024)
        self._trace_memory = self.memory_table[trace_memory_offset]

    def _setup_pin_error_memory(self):
        self._pin_error_memory = {pm: None for pm in range(NUM_PM)}
        self._pin_error_count = {pm: 0 for pm in range(NUM_PM)}
        self._pin_error_overwrite_protected_count = {pm: 0 for pm in range(NUM_PM)}
        for pm in range(NUM_PM):
            pm_memory_offset = self.get_pin_error_offset(pm)
            self.convert_memory_block_to_bytearray(pm_memory_offset)
            self._pin_error_memory[pm] = self.memory_table[pm_memory_offset]

    def get_pin_error_offset(self, pm):
        pm_start = self.registers[0x190 + (REG_BYTE_NUM * pm)] << 6
        pm_memory_offset, _ = self.get_memory_block(pm_start)
        return pm_memory_offset

    def reinitialize_pin_error_memory(self, pm):
        pm_memory_offset = self.get_pin_error_offset(pm)
        self.memory_table[pm_memory_offset] = bytearray(1024)
        self._pin_error_memory[pm] = self.memory_table[pm_memory_offset]

    def pin_error_data(self, pm):
        return bytes(self._pin_error_memory[pm])

    def trace_data(self):
        trace_start = self.register_left_shift(0x18C, 6)
        trace_byte_count = self._trace_overwrite_protected_count*32
        trace_memory_offset, _ = self.get_memory_block(trace_start)
        return bytes(self.memory_table[trace_memory_offset][:trace_byte_count])

    def register_left_shift(self, reg_index, offset):
        return self.registers[reg_index] << offset

    def ctv_headers(self):
        ctv_header_start = self.register_left_shift(0x184, 6)
        ctv_header_count_limit = self.get_ctv_header_block_count_limit()
        ctv_header_byte_count = min(self._ctv_header_count, ctv_header_count_limit) * STREAM_BLOCK
        ctv_header_memory_offset, _ = self.get_memory_block(ctv_header_start)
        return bytes(self.memory_table[ctv_header_memory_offset][:ctv_header_byte_count])
    
    def _setup_ctv_header_memory(self):
        ctv_headers_memory_offset = self.get_ctv_header_memory_offset()
        self.convert_memory_block_to_bytearray(ctv_headers_memory_offset)
        self._ctv_header_memory = self.memory_table[ctv_headers_memory_offset]
        self._ctv_header_count = 0

    def get_ctv_header_memory_offset(self):
        ctv_headers_start = self.register_left_shift(0x184, 6)
        ctv_headers_memory_offset, _ = self.get_memory_block(ctv_headers_start)
        return ctv_headers_memory_offset

    def _setup_ctv_data_memory(self):
        ctv_data_memory_offset = self.get_ctv_data_memory_offset()
        self.convert_memory_block_to_bytearray(ctv_data_memory_offset)
        self._ctv_data_memory = self.memory_table[ctv_data_memory_offset]
        self._ctv_data_count = 0

    def ctv_data(self):
        if self._ctv_data_overflow():
            return bytes(self.ctv_data_memory_not_limited)
        else:
            return bytes(self.get_ctv_data_with_end_address_limit())

    def get_ctv_data_with_end_address_limit(self):
        ctv_start = self.registers[0x188] << 6
        ctv_block_count = min(self._ctv_data_count, self.get_ctv_data_block_number_limit())
        ctv_byte_count = ctv_block_count * 32
        ctv_header_memory_offset, _ = self.get_memory_block(ctv_start)
        return self.memory_table[ctv_header_memory_offset][:ctv_byte_count]

    def _ctv_data_overflow(self):
        return self._ctv_data_count > self.get_ctv_data_block_number_limit()

    def metadata(self, vector):
        pass

    def data_vector(self, vector):
        local_repeat = vector[14] >> 3
        total_vectors = self._instruction_repeat * (local_repeat + 1)
        is_local_repeat = [0] * total_vectors
        if local_repeat > 0:
            is_local_repeat = [0] * self._instruction_repeat
            is_local_repeat.extend([1] * (total_vectors - self._instruction_repeat))
        for i in range(total_vectors):
            if self.is_CTV_enabled(vector) and self.global_capture_enabled:
                self.handle_ctv_header(i)
            for pm_slice in self._pin_vector_handlers:
                channel_sets_per_cycle, ctv_data, errors = pm_slice.handle_vector(vector)
                if ctv_data:
                    self.add_valid_ctv_data(pm_slice, ctv_data, is_local_repeat[i], channel_sets_per_cycle)
                else:
                    self.add_invalid_ctv_data(pm_slice, is_local_repeat[i])

                if errors and self.global_capture_enabled:
                    self._handle_pin_errors(errors, i, pm_slice.chip)

            self._pattern_cycle += 1
            self._user_cycle += 1
            self._total_cycle_count += 1
            if self._total_cycle_count > SIMULATOR_LIMIT:
                raise Exception('Simulator limit reached')
            if self._abort:
                raise PatGenSlice.PatternAbort()
        self._instruction_repeat = 1
        if self.pattern_end_instruction_seen:
            self.pattern_end_pvc_seen = True

    def is_CTV_enabled(self, vector):
        return vector[15] & 0x20

    def all_channel_sets_on(self, code):
        return code >> 5

    def inactive_pin_multiplier(self, code, pm_num):
        return (code & 0x3) != pm_num

    class PatternAbort(Exception):
        pass

    def _handle_pin_errors(self, errors, repeat, chip):
        if self._pin_error_count[chip] == 0:
            self.reinitialize_pin_error_memory(chip)
        if not self.registers[0x180] & 0b100:
            return
        if not errors:
            return
        word_type = 0
        is_done = 0
        is_valid = 1
        for i in range(2):
            a_channel_set = [2, 0][i]
            b_channel_set = [3, 1][i]
            pin_error = capture_structs.PIN_ERROR()
            pin_error.a_ch_set_e_mask_low_bin = errors[a_channel_set][0] & 0xFFFFFFFF
            pin_error.a_ch_set_p_state_low_bin = errors[a_channel_set][1] & 0xFFFFFFFF
            pin_error.a_ch_set_e_mask_up_bin = (errors[a_channel_set][0] >> 32) & 0x7
            pin_error.a_ch_set_p_state_up_bin = (errors[a_channel_set][1] >> 32) & 0x7
            pin_error.a_ch_set_is_valid = is_valid
            pin_error.a_ch_set_clock_cycle = self._total_cycle_count
            pin_error.a_ch_set_alarm = 0
            pin_error.a_ch_set_burst_is_done = is_done
            pin_error.a_ch_set_slice = self.slice
            pin_error.a_ch_set_chip = chip
            pin_error.a_ch_set_word_type = word_type
            pin_error.b_ch_set_e_mask_low_bin = errors[b_channel_set][0] & 0xFFFFFFFF
            pin_error.b_ch_set_p_state_low_bin = errors[b_channel_set][1] & 0xFFFFFFFF
            pin_error.b_ch_set_e_mask_up_bin = (errors[b_channel_set][0] >> 32) & 0x7
            pin_error.b_ch_set_p_state_up_bin = (errors[b_channel_set][1] >> 32) & 0x7
            pin_error.b_ch_set_is_valid = is_valid
            pin_error.b_ch_set_clock_cycle = self._total_cycle_count
            pin_error.b_ch_set_alarm = 0
            pin_error.b_ch_set_burst_is_done = is_done
            pin_error.b_ch_set_slice = self.slice
            pin_error.b_ch_set_chip = chip
            pin_error.b_ch_set_word_type = word_type
            pin_error.last_pcall_address = self._last_pcall_address
            pin_error.vector_address = self.current_address()
            pin_error.pattern_cycle = self._pattern_cycle
            pin_error.repeat_count = repeat
            pin_error.selectable_field = self._log1
            pin_error.user_cycle = self._user_cycle
            pin_error.invalid_pid = self._is_invalid_pid()
            pin_error.reserved = 0
            pin_error.dut_serial_control = self._serialized_global_channel_set
            pin_error.trace_index = self.trace_index
            pin_error.pattern_counter = self._pattern_counter
            if self.pin_error_overwrite_protection:
                self.store_pin_error_with_overwrite_protection(chip, pin_error)
            else:
                self.store_pin_error_without_overwrite_protection(chip, pin_error)
            self._pin_error_count[chip] += 1
            if self._pin_error_count[chip] >= self.get_pin_error_count_limit(chip) - 2:
                self._set_alarms1('error_pm_{}_capture_overflow'.format(chip))

    def store_pin_error_with_overwrite_protection(self, chip, pin_error):
        offset = self._pin_error_count[chip] * PIN_ERRROR_BLOCK
        next_offset = offset + 64
        if self._pin_error_count[chip] < self.get_pin_error_count_limit(chip):
            self._pin_error_memory[chip][offset: next_offset] = bytes(pin_error)
            self._pin_error_overwrite_protected_count[chip] += 1

    def store_pin_error_without_overwrite_protection(self, chip, pin_error):
        offset = self._pin_error_count[chip] * PIN_ERRROR_BLOCK
        next_offset = offset + 64
        self._pin_error_memory[chip][offset: next_offset] = bytes(pin_error)
        self._pin_error_overwrite_protected_count[chip] += 1

    def get_pin_error_count_limit(self, chip):
        chip_offset = chip * 4
        pm_error_end_address = self.register_read(ERROR_STREAM_PM_END_ADDRESS_REG.ADDR + chip_offset) << 6
        pm_error_start_address = self.register_read(ERROR_STREAM_PM_START_ADDRESS_REG.ADDR + chip_offset) << 6
        pm_error_stream_limit = pm_error_end_address - pm_error_start_address
        pm_error_count_limit = pm_error_stream_limit // PIN_ERRROR_BLOCK
        return pm_error_count_limit

    def handle_ctv_header(self, repeat):
        block = (self._ctv_count >> self._capture_block_size_exponent) & 0x7
        mask = 1 << block
        if mask & self._capture_distribution:
            self.create_ctv_header(repeat)
        self._ctv_count += 1

    def _set_un_parked_duts(self, offset, data):
        self._un_parked_dut_list = []
        for bit_index in range(16):
            if (data >> bit_index) & 0b1:
                self._un_parked_dut_list.append(bit_index)

    def create_ctv_header(self, repeat):
        if self._serialized_global_channel_set == 32 or \
                self._serialized_global_channel_set in self._un_parked_dut_list:
            if self._ctv_header_count == 0:
                self.reinitialize_ctv_header_memory()
            ctv_header = capture_structs.CTV_HEADER()
            ctv_header.last_pcall_address = self._last_pcall_address
            ctv_header.vector_address = self.current_address()
            ctv_header.pattern_cycle = self._pattern_cycle
            ctv_header.repeat_count = repeat
            ctv_header.selectable_field = self._log1
            ctv_header.invalid_pid = self._is_invalid_pid()
            ctv_header.user_cycle = self._user_cycle
            ctv_header.trace_index = self.trace_index
            ctv_header.dut_serial_control = self._serialized_global_channel_set
            ctv_header.pattern_counter = self._pattern_counter
            offset = self._ctv_header_count * 32
            next_ctv_header_offset = offset + 32
            self.store_ctv_header_to_memory(ctv_header, next_ctv_header_offset, offset)

    def store_ctv_header_to_memory(self, ctv_header, next_ctv_header_offset, offset):
        if self.ctv_header_overwrite_protection:
            self.store_ctv_header_to_memory_with_overwrite_protection(ctv_header, next_ctv_header_offset, offset)
        else:
            self.store_ctv_header_to_memory_without_overwrite_protection(ctv_header, next_ctv_header_offset, offset)

    def store_ctv_header_to_memory_with_overwrite_protection(self, ctv_header, next_ctv_header_offset, offset):
        ctv_header_count_limit = self.get_ctv_header_block_count_limit()
        if self._ctv_header_count < ctv_header_count_limit:
            self._ctv_header_memory[offset:next_ctv_header_offset] = bytes(ctv_header)
        self._ctv_header_count += 1
        if self._ctv_header_count >= ctv_header_count_limit:
            self._set_alarms1('ctv_header_capture_overflow')

    def store_ctv_header_to_memory_without_overwrite_protection(self, ctv_header, next_ctv_header_offset, offset):
        ctv_header_count_limit = self.get_ctv_header_block_count_limit()
        self._ctv_header_memory[offset:next_ctv_header_offset] = bytes(ctv_header)
        self._ctv_header_count += 1
        if self._ctv_header_count >= ctv_header_count_limit:
            self._set_alarms1('ctv_header_capture_overflow')

    def reinitialize_ctv_header_memory(self):
        ctv_header_memory_offset = self.get_ctv_header_memory_offset()
        self.memory_table[ctv_header_memory_offset] = bytearray(1024)
        self._ctv_header_memory = self.memory_table[ctv_header_memory_offset]

    def get_ctv_header_block_count_limit(self):
        ctv_header_end_address = self.register_read(CTV_HEADER_STREAM_END_ADDRESS_REG.ADDR) << 6
        ctv_header_start_address = self.register_read(CTV_HEADER_STREAM_START_ADDRESS_REG.ADDR) << 6
        ctv_header_stream_limit = ctv_header_end_address - ctv_header_start_address
        ctv_header_count_limit = ctv_header_stream_limit // STREAM_BLOCK
        return ctv_header_count_limit

    def get_trace_block_count_limit(self):
        trace_end_address = self.register_read(patgen_register.TRACE_STREAM_END_ADDRESS_REG.ADDR) << 6
        trace_start_address = self.register_read(patgen_register.TRACE_STREAM_START_ADDRESS_REG.ADDR) << 6
        trace_stream_limit = trace_end_address - trace_start_address
        trace_count_limit = trace_stream_limit // STREAM_BLOCK
        return trace_count_limit

    def add_valid_ctv_data(self, pm_slice, capture_data, is_local_repeat_vector, bits_per_cycle):
        self.update_ctv_data_object(pm_slice, capture_data, is_local_repeat_vector, bits_per_cycle, True)

    def add_invalid_ctv_data(self, pm_slice, is_local_repeat_vector):
        capture_data = [0] * self.get_total_active_ctp()
        bits_per_cycle = 2
        self.update_ctv_data_object(pm_slice, capture_data, is_local_repeat_vector, bits_per_cycle, False)

    def update_ctv_data_object(self, pm_slice, capture_data, is_local_repeat_vector, bits_per_cycle, valid_cycle):
        chip = pm_slice.chip
        address = self._total_cycle_count & 0x1FF
        self.ctv_data_handlers[chip].add_data(capture_data, bits_per_cycle, valid_cycle, address)

    def commit_ctv_data(self, chip):
        block_num_limit = self.get_ctv_data_block_number_limit()
        if self._ctv_data_count == 0:
            self.reinitialize_ctv_data_memory()
        if self.ctv_data_handlers[chip] is not None: # UNCOMMITTED RECORDS EXIST
            for block in self.ctv_data_handlers[chip].blocks():
                if self._ctv_data_count + CTV_DATA_END_OF_BURST_BLOCK_NUM < block_num_limit - 1:
                    self.store_ctv_data_and_increment_ctv_counter(block)
                elif self._ctv_data_count <= block_num_limit - 1:
                    self.store_ctv_data_and_increment_ctv_counter(block)
                    self._set_alarms1('ctv_row_capture_overflow')
                else:
                    if self.ctv_data_overwrite_protection:
                        self.store_only_ctv_limited_data_and_increment_ctv_counter(block)
                    else:
                        self.store_ctv_data_and_increment_ctv_counter(block)
                    self._set_alarms1('ctv_row_capture_overflow')
            self.ctv_data_handlers[chip] = None

    def reinitialize_ctv_data_memory(self):
        ctv_data_memory_offset = self.get_ctv_data_memory_offset()
        self.memory_table[ctv_data_memory_offset] = bytearray(1024)
        self._ctv_data_memory = self.memory_table[ctv_data_memory_offset]

    def get_ctv_data_memory_offset(self):
        ctv_data_start = self.registers[0x188] << 6
        ctv_data_memory_offset, _ = self.get_memory_block(ctv_data_start)
        return ctv_data_memory_offset

    def get_ctv_data_block_number_limit(self):
        ctv_start_address = self.get_start_byte_address_from_register()
        ctv_end_address = self.get_end_byte_address_from_register()
        byte_num_limit = ctv_end_address - ctv_start_address
        block_num_limit = byte_num_limit // STREAM_BLOCK
        return block_num_limit

    def get_end_byte_address_from_register(self):
        ctv_end_address = self.register_read(CTV_DATA_STREAM_END_ADDRESS_REG.ADDR) << 6
        return ctv_end_address

    def get_start_byte_address_from_register(self):
        return self.register_read(CTV_DATA_STREAM_START_ADDRESS_REG.ADDR) << 6

    def store_ctv_data_and_increment_ctv_counter(self, block):
        offset = self._ctv_data_count * 32
        next_ctv_data_offset = offset + 32
        self._ctv_data_memory[offset:next_ctv_data_offset] = block
        self.ctv_data_memory_not_limited[offset:next_ctv_data_offset] = block
        self._ctv_data_count += 1

    def store_only_ctv_limited_data_and_increment_ctv_counter(self, block):
        offset = self._ctv_data_count * 32
        next_ctv_data_offset = offset + 32
        self.ctv_data_memory_not_limited[offset:next_ctv_data_offset] = block
        self._ctv_data_count += 1

    def _set_alarms1(self, alarm_name):
        alarm1 = ALARMS1(self.registers.get(ALARMS1.ADDR, 0))
        setattr(alarm1, alarm_name, 1)
        self.register_write(ALARMS1.ADDR, alarm1.value)

    def create_ctv_data_header_eob(self, chip):
        header = capture_structs.CTV_HEADER_AND_DATA()
        header.relative_cycle_count = (self._total_cycle_count + 1) & 0x1FF
        header.end_of_burst = 1
        header.originating_slice = self.slice
        header.originating_chip = chip
        header.capture_word_type = 1
        if self._ctv_data_count < self.get_ctv_data_block_number_limit():
            self.store_ctv_data_and_increment_ctv_counter(bytes(header))
        else:
            self._ctv_data_count += 1
        valid_cycles = header.valid_cycles

    def create_pin_error_data_header_eob(self, pm):
        if self._pin_error_count[pm] == 0:
            self.reinitialize_pin_error_memory(pm)
        for i in range(2):
            a_channel_set = [3, 1][i]
            b_channel_set = [2, 0][i]
            pin_error = capture_structs.PIN_ERROR()
            pin_error.a_ch_set_is_valid = 1
            pin_error.a_ch_set_clock_cycle = self._total_cycle_count
            pin_error.a_ch_set_alarm = 0
            pin_error.a_ch_set_burst_is_done = 1
            pin_error.a_ch_set_slice = self.slice
            pin_error.a_ch_set_chip = pm
            pin_error.a_ch_set_word_type = 0
            pin_error.b_ch_set_is_valid = 1
            pin_error.b_ch_set_clock_cycle = self._total_cycle_count
            pin_error.b_ch_set_alarm = 0
            pin_error.b_ch_set_burst_is_done = 1
            pin_error.b_ch_set_slice = self.slice
            pin_error.b_ch_set_chip = pm
            pin_error.b_ch_set_word_type = 0
            pin_error.last_pcall_address = self._last_pcall_address
            pin_error.vector_address = self.current_address()
            pin_error.pattern_cycle = self._pattern_cycle
            pin_error.repeat_count = 0
            pin_error.selectable_field = self._log1
            pin_error.user_cycle = self._user_cycle
            pin_error.dut_serial_control = self._serialized_global_channel_set
            pin_error.invalid_pid = self._is_invalid_pid()
            pin_error.reserved = 0
            pin_error.trace_index = self.trace_index
            pin_error.pattern_counter = self._pattern_counter

            if self.pin_error_overwrite_protection:
                self.store_pin_error_with_overwrite_protection(chip=pm, pin_error=pin_error)
            else:
                self.store_pin_error_without_overwrite_protection(chip=pm, pin_error=pin_error)
            self._pin_error_count[pm] += 1

    def _is_invalid_pid(self):
        if self._pattern_id == 0xFFFFFFFF:
            return 1
        return 0

    def _get_pin_vector_handlers(self):
        ''' Store a local copy of handler methods to reduce overhead when processing pin data vectors'''
        self._pin_vector_handlers = [self.system.psdb[psdb].pin_multiplier[pm].slices[self.slice] for psdb in range(2)
                                     for pm in range(2)]
    
    def add_pin_state_word_method_table(self):
        self._pin_state_word_method_table = {0b001: self._plist_mask_pins,
                                             0b010: self._plist_unmask_pins,
                                             0b011: self._plist_apply_mask,
                                             0b100: self._enabled_clock_reset_mask,
                                             0b101: self._dut_serial_control,
                                             0b110: self._set_ctp_mask,
                                             0b111: self._active_channels_mask}
        
    def pin_state_word(self, data):
        pin_state_type = (data[15] >> 1) & 0x7
        self._pin_state_word_method_table[pin_state_type](data)
        if pin_state_type == 5:
            self._set_pin_state_control(data)
            for psdb in self.system.psdb:
                for pm in psdb.pin_multiplier:
                    for slice in pm.slices:
                        slice.pin_state_control = self.pin_state_control

    def _set_pin_state_control(self, data):
        self.pin_state_control = (data[15] & 0x1) << 5 | data[14] >> 3

    def _plist_mask_pins(self, data):
        value = int.from_bytes(data[0:5], byteorder='little')
        last_mask = self._staged_mask
        self._staged_mask |= int.from_bytes(data[0:5], byteorder='little')

    def _plist_unmask_pins(self, data):
        value = int.from_bytes(data[0:5], byteorder='little')
        last_mask = self._staged_mask
        self._staged_mask &= int.from_bytes(data[0:5], byteorder='little')

    def _plist_apply_mask(self, data):
        if type(data) is bytes:
            mask = int.from_bytes(data[0:5], byteorder='little')
        else:
            mask = data
        self._staged_mask = mask
        self._active_pattern_mask = mask
        for pm_slice in self._pin_vector_handlers:
            pm_slice.set_pattern_mask(mask)

    def _enabled_clock_reset_mask(self, data):
        for pm_slice in self._pin_vector_handlers:
            pm_slice.reset_enabled_clock(data)

    def _dut_serial_control(self, data):
        data = int.from_bytes(data, byteorder='little')
        self._serialized_global_channel_set = (data >> 115) & 0x3F
        self.pm_code = (self._serialized_global_channel_set >> 2) & 0x3
        self.channel_set_code = self._serialized_global_channel_set & 0x3
        upper_channels = (data >> 64) & 0x000003FFFFFFFFFF
        lower_channels = data         & 0x7FFFFFFFFFFFFFFF
        temp = (upper_channels << 63) | lower_channels
        self._channel_set_data = {}
        for i in range(35):
            self._channel_set_data[i] = (temp >> (i *3)) & 0b111
        for pm_slice in self._pin_vector_handlers:
            pm_index = pm_slice.index + 2 * pm_slice.slot
            if self.all_channel_sets_on(self._serialized_global_channel_set):
                pm_slice.update_pm_broadcast_settings(4, self._channel_set_data)
            elif self.inactive_pin_multiplier(self.pm_code, pm_index):
                pm_slice.update_pm_broadcast_settings(5, self._channel_set_data)
            else:
                pm_slice.update_pm_broadcast_settings(self.channel_set_code, self._channel_set_data)

    def _set_ctp_mask(self, data):
        self._ctp_mask = int.from_bytes(data[0:5], byteorder='little')
        for psdb in self.system.psdb:
            psdb.set_ctp_mask(self.slice, self._ctp_mask)

    def get_total_active_ctp(self):
        return len([x for x in range(35) if (self._ctp_mask >> x) & 1])

    def _active_channels_mask(self, data):
        mask = []
        lower_half = int.from_bytes(data[0:8], byteorder='little')
        upper_half = int.from_bytes(data[8:16], byteorder='little')
        for i in range(21):
            mask.append(lower_half & 0x7)
            lower_half = lower_half >> 3
        for i in range(14):
            mask.append(upper_half & 0x7)
            upper_half = upper_half >> 3
        for psdb in self.system.psdb:
            psdb.set_active_channels_mask(self.slice, mask)
        # TODO: Verify if this is a good place to reset Block Fails On -
        #  Set all channels active is the first instruction used in Start Pattern (macros.rpat)
        for pm in self._pin_vector_handlers:
            pm._update_block_fails_mode(0)
        
    def add_instruction_method_table(self):
        self.instruction_method_table = {0b0000: self.branch,
                                         0b0001: self.external_op,
                                         0b0010: self.alu_op,
                                         0b0100: self.reg_op,
                                         0b1000: self.vector_op}

    def instruction(self, data):
        if self._instruction_repeat > 1:
            self._set_illegal_instruction_alarm()
        else:
            op = (data[8] >> 2) & 0xF
            self.instruction_method_table[op](data)

    def alu_op(self, data):
        ALU(patgen=self, data=data).execute()
    
    def add_vector_op_method_table(self):
        self.vector_op_table = {0b001: self.vector_op_log,
                                0b010: self.vector_op_local,
                                0b100: self.vector_op_other}

        self.vector_op_log_table = {0b0001: self.log1_immediate,
                                    0b0100: self.pattern_id_immediate,
                                    0b1001: self.log1_register,
                                    0b1100: self.pattern_id_register}
                                    
        self.vector_op_local_table = {0b0001: self.repeat_immediate,
                                      0b0010: self.end_immediate,
                                      0b1001: self.repeat_register,
                                      0b1010: self.end_register}

        self.vector_op_other_table = {0b0001: self.trigger_to_snap_in_board,
                                      0b0010: self.block_fail_enable_immediate,
                                      0b0011: self.drain_pin_fifo_immediate,
                                      0b0100: self.edge_counter_immediate,
                                      0b0101: self.clear_sticky_error,
                                      0b1000: self.capture_immediate,
                                      0b1001: self.reset_fail_counts,
                                      0b1011: self.drain_pin_fifo_register,
                                      0b1100: self.reset_cycle_count,
                                      0b1101: self.clear_domain_sticky_error,
                                      0b1111: self.clear_global_sticky_error}

    def vector_op(self, data):
        vp_type = (int.from_bytes(data[7:9], byteorder='little') >> 7) & 0x7
        self.vector_op_table[vp_type](data)
    
    def vector_op_log(self, data):
        vp_op = (data[7] >> 3) & 0xF
        self.vector_op_log_table[vp_op]()

    def vector_op_local(self, data):
        vp_op = (data[7] >> 3) & 0xF
        self.vector_op_local_table[vp_op](data)

    def vector_op_other(self, data):
        vp_op = (data[7] >> 3) & 0xF
        self.vector_op_other_table[vp_op](data)
    
    def log1_immediate(self):
        self._log1 = self._unsigned_immediate()
        
    def pattern_id_immediate(self):
        self._new_pattern(self._unsigned_immediate())
        
    def log1_register(self):
        self._log1 = self._get_unsigned_register_a()
        
    def pattern_id_register(self):
        self._new_pattern(self._get_unsigned_register_a())
    
    def _new_pattern(self, id):
        self._pattern_id = id
    
    def repeat_immediate(self, data):
        irpt = self._unsigned_immediate()
        if irpt == 0:
            self._instruction_repeat = 32
        else:
            self._instruction_repeat = irpt

    def end_immediate(self, data):
        self.registers[0x160] = int.from_bytes(data[0:4], 'little')
        self.pattern_end_instruction_seen = True
    
    def repeat_register(self, data):
        irpt = self._get_unsigned_register_a()
        if irpt == 0:
            self._instruction_repeat = 32
        else:
            self._instruction_repeat = irpt
    
    def end_register(self, data):
        self.registers[0x160] = self._get_unsigned_register_a()
        self.pattern_end_instruction_seen = True

    def trigger_to_snap_in_board(self, data):
        self._unhandled_vector()

    def block_fail_enable_immediate(self, data):
        for pm in self._pin_vector_handlers:
            pm._update_block_fails_mode(self._unsigned_immediate())

    # Not relevant in HBICC use model
    def drain_pin_fifo_immediate(self, data):
        pass
        # Purposely create a near underrun condition to minimize latency between instructions operations and final
        # output to the DUT. Internal. No need to implement it in the simulator.

    # Not a feature in HBICC
    def edge_counter_immediate(self, data):
        self._unhandled_vector()

    # Not relevant in HBICC use model
    def clear_sticky_error(self, data):
        flags_reg = patgen_register.FLAGS()
        flags_reg.value = self.registers[patgen_register.FLAGS.ADDR]
        flags_reg.Local_Sticky_Error = 0
        flags_reg.Domain_Sticky_Error = 0
        flags_reg.Global_Sticky_Error = 0
        self.registers[patgen_register.FLAGS.ADDR] = flags_reg.value

    def capture_immediate(self, data):
        self.global_capture_enabled = self._unsigned_immediate()
        self._set_capture_window(self._unsigned_immediate())

    def _set_capture_window(self, enable):
        for pm_slice in self._pin_vector_handlers:
            pm_slice.set_capture_window(enable)

    def reset_fail_counts(self, data):
        for pm in self._pin_vector_handlers:
            pm._reset_fail_counts()

    # Not relevant in HBICC use model
    def drain_pin_fifo_register(self, data):
        pass
        # Purposely create a near underrun condition to minimize latency between instructions operations and final
        # output to the DUT. Internal. No need to implement it in the simulator.

    def reset_cycle_count(self, data):
        self._user_cycle = 0

    # Not relevant in HBICC use model
    def clear_domain_sticky_error(self, data):
        flags_reg = patgen_register.FLAGS()
        flags_reg.value = self.registers[patgen_register.FLAGS.ADDR]
        flags_reg.Local_Sticky_Error = 0
        flags_reg.Domain_Sticky_Error = 0
        self.registers[patgen_register.FLAGS.ADDR] = flags_reg.value

    # Not relevant in HBICC use model
    def clear_global_sticky_error(self, data):
        flags_reg = patgen_register.FLAGS()
        flags_reg.value = self.registers[patgen_register.FLAGS.ADDR]
        flags_reg.Global_Sticky_Error = 0
        self.registers[patgen_register.FLAGS.ADDR] = flags_reg.value

    def reg_op(self, data):
        src = (data[7] & 0b00111000) >> 3
        value = self.get_src_from_dict(data, src)
        self.set_value_in_dst(data, value)
        return value

    def set_value_in_dst(self, data, value):
        dst = (data[8] & 0b11) << 2 | (data[7] & 0b11000000) >> 6
        x = ((data[5] & 0b01111100) >> 2)
        if dst == 0b0001:
            self.update_processor_register(x, value),
        elif dst == 0b0010:
            self._common_file_base = value
        elif dst == 0b0100:
            self.update_flags(value)
        elif dst == 0b1000:
            self._stack.append((value, self._active_pattern_mask))
        else:
            raise Exception('Invalid destination')
    
    def update_flags(self, value):
        self.registers[0x178] = (self.registers[0x178] & 0xFFFF0000) | (value & 0x0000FFFF) | 0x00010000

    def _clear_flags_register(self):
        self.registers[0x178] = 0x00010000

    def get_src_from_dict(self, data, src):
        if src==7:
            offset, mask = self._stack.pop()
            return offset
        else:
            options = {0b000: data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0],
                       0b001: data[4] & 0b11111,
                       # 0b010: self.raise_exception(src, 'tcc'),
                       # 0b011: self.raise_exception(src, 'vacadr'),
                       # 0b100: self.raise_exception(src, 'cfb'),
                       0b101: self.registers[0x178],
                       # 0b110: self.raise_exception(src, 'dirtymask'),
                       # 0b111: self.raise_exception(src, 'stack')
                       }
            return options[src]

    def external_op(self, data):
        self.external_op_methods[self._external_action_type()]()

    def _external_action_type(self):
        return self._get_field(lsb=63, bits=3)

    def _external_trigger_action(self):
        return self._get_field(lsb=59, bits=4)

    def add_external_op_method_table(self):
        self.external_op_methods = {0b001: self.external_trigger,
                                    0b010: self.external_other,
                                    0b100: self.external_trigger_now}
        self.trigger_methods = {0b0001: self.trigger_external_from_immediate,
                                0b0100: self.trigger_domain_from_immediate,
                                0b1001: self.trigger_external_from_register_a,
                                0b1100: self.trigger_domain_from_register_a,
                                0b0010: self.trigger_external_sync_from_immediate,
                                0b1010: self.trigger_external_sync_from_register_a}

    def external_trigger(self):
        self.trigger_methods[self._external_trigger_action()]()

    def external_other(self):
        self._unhandled_vector()

    def external_trigger_now(self):
        self.trigger_methods[self._external_trigger_action()]()

    def trigger_external_from_immediate(self):
        if self._is_domain_master():
            self.trigger_up(self._unsigned_immediate())

    def trigger_external_from_register_a(self):
        if self._is_domain_master():
            self.trigger_up(self._get_unsigned_register_a())

    def trigger_domain_from_immediate(self):
        self._unhandled_vector()

    def trigger_domain_from_register_a(self):
        self._unhandled_vector()

    def trigger_external_sync_from_immediate(self):
        self._unhandled_vector()

    def trigger_external_sync_from_register_a(self):
        self._unhandled_vector()

    def _is_domain_master(self):
        '''Bit 8 of the trigger control register is the domain master flag'''
        return bool(self.registers.get(0x58, 0) & 0x00000100)

    def trigger_up(self, trigger):
        self.registers[0x50] = trigger
        self.system.rc.trigger_up(trigger, link=16)

    def add_branch_method_table(self):
        self.branch_methods = {0b00000: self.no_branch,
                               0b00001: self.goto_i,
                               0b00010: self.call_i,
                               0b00011: self.compressed_call_i,
                               0b00100: self.pcall_i,
                               0b01000: self.ret,
                               0b10001: self.goto_r,
                               0b10010: self.call_r,
                               0b10011: self.compressed_call_r,
                               0b10100: self.pcall_r,
                               0b11000: self.clear_stack}
    
    def add_condition_table(self):
        self._condition_handler = {0b0000: self._condition_always,
                                   0b0001: self._condition_lse,
                                   0b0010: self._condition_dse,
                                   0b0011: self._condition_gse,
                                   0b0100: self._condition_reserved1,
                                   0b0101: self._condition_reserved2,
                                   0b0110: self._condition_sw_trigger,
                                   0b0111: self._condition_domain_trigger,
                                   0b1000: self._condition_rc_trigger,
                                   0b1001: self._condition_zero,
                                   0b1010: self._condition_carry,
                                   0b1011: self._condition_overflow,
                                   0b1100: self._condition_sign,
                                   0b1101: self._condition_below_or_equal,
                                   0b1110: self._condition_less_than_or_equal,
                                   0b1111: self._condition_greater_than_or_equal}
                                   
    def branch(self, data):
        if self.branch_condition(data):
            self.branch_methods[self.branch_type(data)](data)
            self._pattern_byte_offset -= BYTES_PER_VECTOR # account for execution loop increment
    
    def branch_condition(self, data):
        '''condition codes 0:15 correspond to bits 16:31 in the flag register'''
        condition = ((data[7] & 0x3) << 2) | (data[6] >> 6)
        flag = bool((self.registers[0x178] >> (16 + condition)) & 0x1)
        invert = bool(data[7] & 0x04)
        return flag ^ invert

    def branch_type(self, vector):
        """Extract bits 51:47 from the vector"""
        return (int.from_bytes(vector[5:7], byteorder='little') >> 7) & 0x1F
    
    def no_branch(self, data):
        pass
    
    @record_trace
    def goto_i(self, data):
        self._goto(self._unsigned_immediate())
    
    @record_trace
    def call_i(self, data):
        self._call(self._unsigned_immediate())
    
    @record_trace
    def compressed_call_i(self, data):
        self._unhandled_vector()
    
    @record_trace
    def pcall_i(self, data):
        self._pcall(self._unsigned_immediate())
    
    @record_trace
    def ret(self, data):
        if len(self._stack) == 0:
            self.Log('warning', f'Stack underrun at vector offset = 0x{self.current_address():08X}')
            raise PatGenSlice.StackError()
        caller_address, mask = self._stack.pop()
        self._pattern_byte_offset = caller_address + BYTES_PER_VECTOR
        self._plist_apply_mask(mask)
        self._mem_block_address = self._mem_block_stack.pop()
        if (self._pattern_byte_offset >= self._ram_end_byte_adress) | (self._pattern_byte_offset < self._ram_start_byte_adress):
            self._load_new_pattern_memory(return_mode=True)

    @record_trace
    def goto_r(self, data):
        self._goto(self._unsigned_destination())
    
    @record_trace
    def call_r(self, data):
        self._call(self._unsigned_destination())
    
    @record_trace
    def compressed_call_r(self, data):
        self._unhandled_vector()
    
    @record_trace
    def pcall_r(self, data):
        self._pcall(self._unsigned_destination())
    
    def clear_stack(self, data):
        self._stack = []
        self._mem_block_stack = []
        self._pattern_byte_offset += BYTES_PER_VECTOR
    
    def _pcall(self, target):
        if self._pattern_cycle != 0 and not self._is_first_pcall:
            self._pattern_counter += 1
        elif self._is_first_pcall:
            self._pattern_counter += 1
            self._is_first_pcall = False
        self._last_pcall_address = self.current_address()
        self._call(target)
        self._plist_apply_mask(self._staged_mask)
        self._pattern_base = self.current_address()
        self._previous_pcall_user_cycle = self._user_cycle
        self._reset_per_pattern_max_fail()
        self._pattern_cycle = 0

    def _call(self, target):
        if len(self._stack) >= 64:
            self.Log('warning', f'Stack overflow at vector offset = 0x{self.current_address():08X}')
            raise PatGenSlice.StackError()
        self._stack.append((self._pattern_byte_offset, self._active_pattern_mask))
        self._mem_block_stack.append(self._ram_start_byte_adress)
        self._goto(target)

    def _goto(self, target):
        branch_base = self._get_field(lsb=BRANCH_BASE_LSB, bits=BRANCH_BASE_BITS)
        destination_address = self._branch_base_table[branch_base](target)
        self._pattern_byte_offset = destination_address * BYTES_PER_VECTOR
        if (self._pattern_byte_offset>=self._ram_end_byte_adress) | (self._pattern_byte_offset<self._ram_start_byte_adress):
            self._load_new_pattern_memory(return_mode=False)

    def _global_branch(self, target):
        return target
    
    def _relative_branch(self, target):
        return self.current_address() + self.to_signed(target)
    
    def _common_file_branch(self, target):
        return self._common_file_base + target
    
    def _pattern_branch(self, target):
        return self._pattern_base + target
    
    def _add_branch_base_table(self):
        self._branch_base_table = {BRANCH_BASE_GLOBAL: self._global_branch,
                                   BRANCH_BASE_RELATIVE: self._relative_branch,
                                   BRANCH_BASE_COMMON_FILE: self._common_file_branch,
                                   BRANCH_BASE_PATTERN: self._pattern_branch}

    class StackError(Exception):
        pass

    def _set_stack_alarm(self):
        self.registers[0x16C] = self.registers.get(0x16C, 0) | (1 << 20)

    def _clear_stack_alarm(self):
        self.registers[0x16C] = self.registers.get(0x16C, 0) & ~(1 << 20)

    def _set_illegal_instruction_alarm(self):
        self.registers[0x16C] = self.registers.get(0x16C, 0) | (1 << 7)

    def _set_illegal_vector_format_alarm(self):
        self.registers[0x170] = self.registers.get(0x170, 0) | (1 << 2)

    def _set_instruction_processor_error_end_status(self):
        self.registers[0x160] = 0x1800dead

    def to_signed(self, dword):
        if dword & 0x80000000:
           return dword - REGISTER_MAX_VALUE_PLUS_ONE
        else:
            return dword
        
    def _branch_condition_is_true(self, data):
        condition = self._get_field(lsb=BRANCH_CONDITION_LSB, bits=BRANCH_CONDITION_BITS)
        invert = self._get_field(lsb=BRANCH_INVERT_LSB, bits=BRANCH_INVERT_BITS)
        if invert:
            return not self._condition_handler[condition]()
        else:
            return self._condition_handler[condition]()
        
    def _condition_always(self):
        return True
        
    def _condition_lse(self):
        self._unhandled_vector()
    def _condition_dse(self):
        self._unhandled_vector()
    def _condition_gse(self):
        self._unhandled_vector()
    def _condition_reserved1(self):
        self._unhandled_vector()
    def _condition_reserved2(self):
        self._unhandled_vector()
    def _condition_sw_trigger(self):
        self._unhandled_vector()
    def _condition_domain_trigger(self):
        self._unhandled_vector()
    def _condition_rc_trigger(self):
        self._unhandled_vector()
    
    def _condition_zero(self):
        return bool(self.patgen.registers[0x178] & FLAGS_ZERO_MASK)
        
    def _condition_carry(self):
        self._unhandled_vector()
    def _condition_overflow(self):
        self._unhandled_vector()
    def _condition_sign(self):
        self._unhandled_vector()
    def _condition_below_or_equal(self):
        self._unhandled_vector()
    def _condition_less_than_or_equal(self):
        self._unhandled_vector()
    def _condition_greater_than_or_equal(self):
        self._unhandled_vector()
    
    def _unsigned_destination(self):
        index = self._get_destination_register_index()
        return self.processor_reg[index]
    
    def _get_unsigned_register_a(self):
        index = self._get_register_a_index()
        return self.processor_reg[index]
    
    def _unsigned_immediate(self):
        ram_read_address = self._pattern_byte_offset - self._ram_start_byte_adress
        imm_bytes = self.ram[ram_read_address:ram_read_address + 4]
        return int.from_bytes(imm_bytes, 'little')

    def _illegal_vector(self, data):
        self._set_illegal_vector_format_alarm()
        
    def _unhandled_vector(self, data=None):
        #for i in range(0, len(self.ram), 16):
        #    print(f'{i//16:08X} {int.from_bytes(self.ram[i:i+BYTES_PER_VECTOR], byteorder="little"):032X}')
        ram_read_address = self._pattern_byte_offset - self._ram_start_byte_adress
        vector = int.from_bytes(self.ram[ram_read_address:ram_read_address + BYTES_PER_VECTOR], byteorder='little')
        raise Exception(f'at offset {self.current_address():08X}, unhandled vector: {vector:032X}')
    
    def _get_destination_register_index(self):
        return self._get_field(lsb=DESTINATION_LSB, bits=DESTINATION_BITS)
    
    def _get_register_a_index(self):
        return self._get_field(lsb=REGISTER_A_LSB, bits=REGISTER_A_BITS)
    
    def _get_field(self, lsb, bits, signed=False):
        byte_offset = self._pattern_byte_offset - self._ram_start_byte_adress
        first_byte = byte_offset + (lsb // 8)
        last_byte = byte_offset + ((lsb + bits - 1) // 8)
        shift = lsb % 8
        mask = 2**bits-1
        return (int.from_bytes(self.ram[first_byte:last_byte+1], byteorder='little', signed=signed) >> shift) & mask

    def update_processor_register(self, reg, value):
        self.processor_reg.update({reg: value})
        self.registers[0x1A0 + (reg * 4)] = value
    
    def pattern_end_status(self):
        return self.registers[0x160]
    
    def trace_count(self):
        return self._trace_count

    def _reset_per_pattern_max_fail(self):
        for pm_slice in self._pin_vector_handlers:
            pm_slice.reset_max_pattern_error_count()


class CtvDataHandler():
    def __init__(self, patgen_slice, chip):
        self._patgen_slice = patgen_slice
        self._slice = patgen_slice.slice
        self._chip = chip
        self._data_blocks = []
        self._quartet_validity = []
        self._quartet_packing = []
        self._packing_width = []
        self._header_valid_cycles = 0
        self._header_entry_is_wide = 0
        self._entry_is_wide = []
        self._valid_cycles = []
        self._adjust_relative_cycle_count = []
        self._final_block = False
        self._channel_sets_per_cycle = None
        self.block_cycles_wide_mode = []

    def add_data(self, data, bits_per_cycle, valid, pattern_cycle):
        if not data:
            return
        self._track_validity(pattern_cycle, valid, bits_per_cycle)
        self._data_blocks.append(data)
        self._channel_sets_per_cycle = bits_per_cycle
        self._adjust_relative_cycle_count.append(pattern_cycle)
        if self._is_ctv_ready_for_packing(pattern_cycle):
            self.commit_ctv_data()

    def _track_validity(self, pattern_cycle, valid, bits_per_cycle):
        self._quartet_validity.append(valid)
        self._quartet_packing.append(bits_per_cycle)
        if (pattern_cycle+1) % 4 == 0 and pattern_cycle != 0:
            self._update_cycle_stats()

    def _update_cycle_stats(self):
        if any(self._quartet_validity):
            if 8 in self._quartet_packing:
                self._packing_width += [8] * len(self._quartet_validity)
                self._entry_is_wide += [1, 1, 1, 1]
            else:
                self._packing_width += [2] * len(self._quartet_validity)
                self._entry_is_wide += [0]
        else:
            self._packing_width += [2] * len(self._quartet_validity)
            self._entry_is_wide += [0]

        self._valid_cycles += self._quartet_validity
        self._quartet_validity = []
        self._quartet_packing = []

    def _is_ctv_ready_for_packing(self, pattern_cycle):
        if (pattern_cycle+1) % 4 == 0:
            if sum(self._packing_width) >= 64:
                return True
        return False

    def blocks(self):
        block_list = []
        active_pins = self._active_ctp_pins()
        for pin in range(active_pins):
            cycle_num, value = self._construct_pin_ctv_data(pin)
            value &= 0xFFFFFFFFFFFFFFFF
            block_list.append(int.to_bytes(value, 8, byteorder='little'))

        self._update_valid_cycles(cycle_num + 1)
        self._update_entry_is_wide()
        block_list.insert(0, self.create_ctv_data_header())
        self._remove_packed_cycles(cycle_num + 1)

        full_blocks = self._pack_data_blocks(block_list)
        return full_blocks

    def _construct_pin_ctv_data(self, pin):
        value = 0
        max_cycles_per_packet = self._cycles_per_block()
        for cycle_num in range(max_cycles_per_packet):
            bit_width = sum(self._packing_width[:cycle_num + 1])
            data = self._data_blocks[cycle_num][pin]
            value |= data << sum(self._packing_width[:cycle_num])
            if bit_width == 64:
                break
        return cycle_num, value

    def _active_ctp_pins(self):
        return len(self._data_blocks[0])

    def _pack_data_blocks(self, block_list):
        while (len(block_list) % 4) != 0:
            block_list.append(bytes([0 for x in range(8)]))
        full_blocks = []
        for i in range(0, len(block_list), 4):
            full_blocks.append(block_list[3 + i] + block_list[2 + i] + block_list[1 + i] + block_list[0 + i])
        return full_blocks

    def _cycles_per_block(self):
        if self._final_block:
            max_cycles_per_packet = len(self._data_blocks)
        else:
            max_cycles_per_packet = 32
        return max_cycles_per_packet

    def _remove_packed_cycles(self, total):
        for i in range(total):
            self._packing_width.pop(0)
            self._data_blocks.pop(0)
            self._valid_cycles.pop(0)
            self._adjust_relative_cycle_count.pop(0)

    def create_ctv_data_header(self):
        header = capture_structs.CTV_DATA_HEADER()
        header.block_count = len(self._data_blocks[0])
        header.relative_cycle_count = self._adjust_relative_cycle_count[0]
        header.valid_cycles = 0
        header.entry_is_wide = 0
        header.valid_cycles = self._header_valid_cycles
        header.entry_is_wide = self._header_entry_is_wide
        header.is_cycle_count = 0
        header.originating_slice = self._slice
        header.originating_chip = self._chip
        header.capture_word_type = 1
        return bytes(header)

    def _update_valid_cycles(self, total):
        self._header_valid_cycles = 0
        pointer = 0
        for index in range(total):
            if self._valid_cycles[index]:
                if self._packing_width[index] == 8:
                    self._header_valid_cycles |= 1 << pointer
                    pointer += 4
                elif self._packing_width[index] == 2:
                    self._header_valid_cycles |= 1 << pointer
                    pointer += 1
            else:
                pointer += 1

    def _update_entry_is_wide(self):
        self._header_entry_is_wide = 0
        entries = self._entry_is_wide[:8]
        for index, value in enumerate(entries):
            self._header_entry_is_wide |= value << index

        del self._entry_is_wide[:8]

    def commit_outstanding_ctv_data(self):
        self._resolve_final_quartet()
        self.commit_ctv_data()

    def commit_ctv_data(self):
        if not self._data_blocks:
            return None

        block_num_limit = self._patgen_slice.get_ctv_data_block_number_limit()
        if self._patgen_slice._ctv_data_count == 0:
            self._patgen_slice.reinitialize_ctv_data_memory()
        if self._patgen_slice.ctv_data_handlers[self._chip] is not None: # UNCOMMITTED RECORDS EXIST
            self.resolve_data_blocks(block_num_limit)
            if self._final_block and self._packing_width:
                self.resolve_data_blocks(block_num_limit)

    def resolve_data_blocks(self, block_num_limit):
        for block in self.blocks():
            if self._header_valid_cycles != 0:
                if self._patgen_slice._ctv_data_count + CTV_DATA_END_OF_BURST_BLOCK_NUM < block_num_limit - 1:
                    self._patgen_slice.store_ctv_data_and_increment_ctv_counter(block)
                elif self._patgen_slice._ctv_data_count <= block_num_limit - 1:
                    self._patgen_slice.store_ctv_data_and_increment_ctv_counter(block)
                    self._patgen_slice._set_alarms1('ctv_row_capture_overflow')
                else:
                    if self._patgen_slice.ctv_data_overwrite_protection:
                        self._patgen_slice.store_only_ctv_limited_data_and_increment_ctv_counter(block)
                    else:
                        self._patgen_slice.store_ctv_data_and_increment_ctv_counter(block)
                    self._patgen_slice._set_alarms1('ctv_row_capture_overflow')

    def _resolve_final_quartet(self):
        self._final_block = True
        self._update_cycle_stats()


class PinErrorDataRecord:
    def __init__(self, header, chip, slice, address):
        self.header = header
        self.chip = chip
        self.slice = slice
        self.relative_cycle_count = address & 0xFFFFFFFFFFFF
        self.valid_cycles = 0
        self.data_blocks = []
        self.word_type = 0
        self.burst_is_done = 0
        self.alarm = 0

    def add_data(self, pin_state=[], error_masks=[]):
        pass

    def blocks(self):
        block_list = [self.create_ctv_data_header()]
        for i in range(len(self.data_blocks[0])):
            block_list.append(bytes([x[i] for x in self.data_blocks]))
        while (len(block_list) % 4) != 0:
            block_list.append(bytes([0 for x in range(8)]))
        full_blocks = []
        for i in range(0, len(block_list), 4):
            full_blocks.append(block_list[3] + block_list[2] + block_list[1] + block_list[0])
        return full_blocks


class ALU(fval.Object):
    ops = { 0b000: operator.xor,
           0b001: operator.and_,
           0b010: operator.or_,
           0b011: operator.lshift,
           0b100: operator.rshift,
           0b101: operator.add,
           0b110: operator.sub,
           0b111: operator.mul}

    def __init__(self, patgen, data):
        self.data = data
        self.patgen = patgen

    def execute(self):
        self.decode_operation_instruction()
        src1, src2 = self.get_src()
        self.execute_alu_op(src1, src2)
        self.set_result()
        self.set_flags()

    def decode_operation_instruction(self):
        self.operation = (self.data[7] & 0b00111000) >> 3
        self.src = (self.data[7] & 0b11000000) >> 6
        self.imm = self.data[3] << 24 | self.data[2] << 16 | self.data[1] << 8 | self.data[0]
        self.dst = self.data[8] & 0b11

    def execute_alu_op(self, src1, src2):
        op = ALU.ops.get(self.operation, self.not_implemented)
        if op in [operator.add, operator.sub]:
            src1 = self.to_python_signed_int(src1)
            src2 = self.to_python_signed_int(src2)
        self.py_int_result = op(src1, src2)

    def not_implemented(self, src1, src2):
        print('ALU Op is not implemented for Operands 0x{:X} and 0x{:X}'.format(src1, src2))

    def set_flags(self):
        self.patgen.registers[0x178] = (self.patgen.registers[0x178] & ~0xFE000000) | self.active_flags()

        if self.is_failing_unnitest_in_place():
            self.patgen.registers[0x178] = self.patgen.registers[0x178] | self.patgen.device.unittest_registers[0x178]

        return

    def is_failing_unnitest_in_place(self):
        if self.patgen.device.unittest_registers[0x178]:
            return True
        return False

    def get_src(self):
        reg_a = self.patgen.processor_reg[self.data[4] & 0b11111]
        reg_b = self.patgen.processor_reg[(self.data[5] & 0b11) << 3 | (self.data[4] & 0b11100000) >> 5]
        flags = self.patgen.registers[0x178]

        sources = {0b00: [reg_a, self.imm], 0b01: [reg_a, reg_b], 0b10: [flags, self.imm], 0b11: [flags, reg_b]}
        return sources[self.src]

    def set_result(self):
        flags_register = 0b10
        destination_register = 0b01
        no_where = 0b00

        result = self.pythong_int_to_signed(self.py_int_result)

        if self.dst == no_where:
            pass
        elif self.dst == destination_register:
            self.patgen.update_processor_register((self.data[5] & 0b1111100) >> 2, result)
        elif self.dst == flags_register:
            self.patgen.registers[0x178] = (self.patgen.registers[0x178] & 0xFE000000) | (result & 0x01FFFFFF) | 0x00010000

    def pythong_int_to_signed(self, result):
        if result < 0:
            result = (abs(result) ^ 0xFFFFFFFF) + 1
        return result & 0xFFFFFFFF

    def to_python_signed_int(self, src):
        if src & 0x80000000:
             src = -1 * ((src ^ 0xFFFFFFFF) + 1)
        return src

    def active_flags(self):
        in_range = -2 ** 31 < self.py_int_result < ((2 ** 31) - 1)
        zero = not self.pythong_int_to_signed(self.py_int_result) & 0xFFFFFFFF
        carry = self.operation in [5, 6] and not in_range
        overflow = self.operation not in [0, 1, 2] and not in_range
        sign = (self.pythong_int_to_signed(self.py_int_result) & 0x80000000) != 0
        below_or_equal = carry | zero
        less_than_or_equal = (sign ^ overflow) | zero
        greater_than_or_equal = (sign == overflow) | zero
        
        active = 0x00010000
        active = active | (zero << 25)
        active = active | (carry << 26)
        active = active | (overflow << 27)
        active = active | (sign << 28)
        active = active | (below_or_equal << 29)
        active = active | (less_than_or_equal << 30)
        active = active | (greater_than_or_equal << 31)
        return active

