# Copyright 2018 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import os
import tarfile
import tempfile
from Common import fval
from Common import configs


class HbiccEnv(fval.Env):
    def __init__(self, test, tester):
        super().__init__(test)
        self.hbicc = tester.get_hbicc_undertest('Hbicc')
        self.tester_name = tester.name()

    def take_snapshot_if_enabled(self):
        if configs.HBICC_SNAP_SHOT or self.test._softErrors:
            test_repeat_number = self.test.repeat_number
            test_name=self.get_test_name_for_snapshot()
            filename = f'R{test_repeat_number}_{test_name}'
            self.take_snapshot(filename)

    def get_test_name_for_snapshot(self):
        if self.test.subtest_file_name:
            test_name = self.test.Name().replace('.', '_') + '_' + f'{ self.test.subtest_file_name}'
        else:
            test_name = self.test.Name().replace('.', '_')
        return test_name

    def process_snapshots(self, explorer_file=True):
        snapshot_explorer = self._get_snapshot_explorer()
        src, folder = self._get_folder_to_process()
        cmd = f'{snapshot_explorer} {folder}'
        try:
            os.system(cmd)
            os.system(f'explorer {src}')
        except:
            self.Log('error', f'Can not process Snapshot {folder}')

    def _get_folder_to_process(self):
        test_name = self.get_test_name_for_snapshot()
        filename =  f'R{self.test.repeat_number}_{test_name}'+ '.tar.gz'
        src = os.path.join(os.getcwd(), 'SNAPSHOTS')
        file = os.path.join(src, filename)
        return src, file

    def _get_snapshot_explorer(self):
        repo_root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
        return os.path.join(repo_root_path, 'tools', 'snapshot_explorer.py')

    def make_tar(self, filename, sourceDir):
        if not (filename.endswith('.tar.gz')):
            filename = '{}.tar.gz'.format(filename)
        self.Log('info', 'Creating snapshot {}'.format(filename))
        cwd = os.getcwd()
        path = os.path.join(cwd, 'SNAPSHOTS')
        os.makedirs(path, exist_ok=True)
        with tarfile.open(os.path.join(path, filename), "w:gz") as tar:
            tar.add(sourceDir, arcname=os.path.basename(sourceDir))

    def take_snapshot(self,filename):
        try:
            directory_name = 'R{}_{}'.format(self.test.repeat_number, self.test.Name().replace('.', '_') + '_')
        except AttributeError:
            directory_name = filename.replace('.', '_') + '_'
            
        with tempfile.TemporaryDirectory(prefix=directory_name) as snapshot_dir: 
            for device in self.hbicc.devices():
                if device:
                    path = os.path.join(snapshot_dir, r'{}_{}'.format(device.name(), device.fpga_index))
                    os.makedirs(path)
                    device.take_snapshot(path)
            self.make_tar(filename, snapshot_dir)

    def read_all_available_fpga_temperature(self):
        data = []
        title = '-----FPGA Temperature -----'
        headers = ['Board', 'Temperature']
        for device in self.hbicc.devices():
            if device:
                fpga_temperature = device.temperature_using_hil()
                data.append({'Board': f'{device.name()}', 'Temperature': f'{fpga_temperature}'})
        table = self.hbicc.contruct_text_table(headers, data, title_in=title)
        self.Log('debug', f'{table}')
        
    def print_pm_version(self):
        list = self.hbicc.check_pin_version()
        self.Log('debug', f'pm0 {list[0]} pm1 {list[1]} pm2 {list[2]} pm3 {list[3]}')

    def print_vccio_vref(self):
        if self.hbicc.psdb_0_pin_1:
            read_all_vmon = self.round_a_list(self.hbicc.psdb_0_pin_0.read_all_vmon())
            self.Log('debug', f'psdb0 vref0 {read_all_vmon[1]} vref1 {read_all_vmon[2]} '
                              f'vref2 {read_all_vmon[3]} vref3 {read_all_vmon[4]} vccio0 {read_all_vmon[8]}')
        if self.hbicc.psdb_1_pin_1:
            read_all_vmon = self.round_a_list(self.hbicc.psdb_1_pin_0.read_all_vmon())
            self.Log('debug', f'psdb1 vref0 {read_all_vmon[1]} vref1 {read_all_vmon[2]} '
                              f'vref2 {read_all_vmon[3]} vref3 {read_all_vmon[4]} vccio0 {read_all_vmon[8]}')

    def round_a_list(self, list):
        return [round(x, 3) for x in list]

    def disable_simulator(self):
        self.hbicc.model.patgen.is_simulator_active = False

    def enable_simulator(self):
        self.hbicc.model.patgen.is_simulator_active = True
