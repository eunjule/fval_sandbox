# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import os
import sys
import time

if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

from ThirdParty import pyparsing

from Common import fval
from Hpcc.hpcctb.assembler import PatternAssembler as HpccPatternAssembler
from Hpcc.hpcctb import symbols
from Hbicc.testbench import generators, structs


class HbiPatternAssembler(HpccPatternAssembler):
    _GENERATOR_NAMESPACE     = generators
    LITERAL_CHARACTERS       = pyparsing.Word('LHXVMF01EK')
    _CUSTOM_LITERALS         = pyparsing.Combine('0v' + LITERAL_CHARACTERS)
    vector_literal_encoding = {'L':0, 'H':1, 'X':2, 'V':3, 'F':3, '0':4, '1':5, 'E':6, 'K':7} # Vector symbols
    # classMacros = []
    
    def __init__(self, startAddress = 0):
        super().__init__(startAddress=startAddress)
        self.symbols['EC_RESET'] = 4
        self.symbols['DUT_SERIAL'] = 5
        self.symbols['CAPTURE_PIN_MASK'] = 6
        self.symbols['ACTIVE_CHANNELS'] = 7
        self.symbols['ALL_CHANNEL_SETS'] = 32
    
    def V(self, array, offset, ctv=None, mtv=None, lrpt=None, data=0):
        p = structs.PatternWord()
        p.pattern_word_type = 1
        if lrpt is not None:
            p.local_repeat = self.Resolve(lrpt)
        if mtv is not None:
            p.mtv = self.Resolve(mtv)
        if ctv is not None:
            p.ctv = self.Resolve(ctv)
        channel_data = self.Resolve(data)
        p.channel_data_20_0 = channel_data & 0x7FFFFFFFFFFFFFFF
        p.channel_data_37_21 = channel_data >> 63
        array[offset:offset+16] = p.value
    
    def CheckLiteral(self, expr):
        if expr[:2] != '0v':
            raise TypeError(f'Invalid vector type {expr[:2]}')
        if len(expr[2:]) != 35:
            raise ValueError(f'Expecting exactly 35 characters after \'{expr[:2]}\' but {len(expr)-2} characters found')
    
    def ResolveLiteralFromString(self, expr):
        self.CheckLiteral(expr)
        value = 0
        for c in expr[2:]:
            value = (value << 3) | self.EncodeData(c)
        return value
    
    def I(self, array, offset, optype, extop=None, action=None, opdest=None, opsrc=None, aluop=None, vptype=None, vpop=None, invcond=None, cond=None, base=None, br=None, dest=None, regB=None, regA=None, imm=None, swrsvd = 0, rsvd = 0):
        i = structs.Instruction()
        i.type = 6
        i.op_type = getattr(symbols.OPTYPE, optype)
        if i.op_type == symbols.OPTYPE.EXT:
            op_code = (self.Resolve(extop) << 4) | self.Resolve(action)
            i.op_code_4_0 = op_code
            i.op_code_6_5 = op_code >> 5
        elif i.op_type == symbols.OPTYPE.ALU:
            i.op_code_4_0 = (self.Resolve(opsrc) << 3) | self.Resolve(aluop)
            i.op_code_6_5 = self.Resolve(opdest)
        elif i.op_type == symbols.OPTYPE.REGISTER:
            opdest = self.symbols[opdest]
            opsrc = self.symbols[opsrc]
            i.op_code_4_0 = (opdest << 3) | opsrc
            i.op_code_6_5 = opdest >> 2
        elif i.op_type == symbols.OPTYPE.VECTOR:
            vptype = getattr(symbols.VPTYPE, vptype)
            if vptype == symbols.VPTYPE.VPLOG:
                vpop = getattr(symbols.VPLOG, vpop)
            elif vptype == symbols.VPTYPE.VPLOCAL:
                vpop = getattr(symbols.VPLOCAL, vpop)
            else:
                vpop = getattr(symbols.VPOTHER, vpop)
            vp = (vptype << 4) | vpop
            i.op_code_4_0 = vp
            i.op_code_6_5 = vp >> 5
        if invcond is not None:
            i.invert_condition = int(invcond, 0)
        if cond is not None:
            i.condition_type = self.Resolve(cond)
        if base is not None:
            i.branch_base = self.Resolve(base)
        if br is not None:
            i.branch_type = getattr(symbols.BRANCH, br)
        if dest is not None:
            i.destination = self.Resolve(dest)
        if regB is not None:
            i.b = int(regB, 0)
        if regA is not None:
            i.a = int(regA, 0)
        if imm is not None:
            i.immediate = self.Resolve(imm)
            
        i.reserved = swrsvd
        i.reserved2 = rsvd
        array[offset:offset+16] = i.value
        
    def S(self, array, offset, stype, data, channel_set=None):
        psw = structs.PinStateWord()
        if data.startswith('0o') and len(data[2:]) != 35:
            raise ValueError(f'Length of octal word ({len(data[2:])}) does not match pin count (35)')
            
        channel_data = self.Resolve(data)
        psw.channel_data_20_0 = channel_data & 0x7FFFFFFFFFFFFFFF
        psw.channel_data_37_21 = channel_data >> 63
        if channel_set is not None:
            psw.pin_state_control  = self.Resolve(channel_set)
        psw.pin_state_type = self.Resolve(stype)
        psw.pattern_word_type = 5
        array[offset:offset+16] = psw.value
     
    def M(self, array, offset, data):
        mdw = structs.MetadataWord()
        data_int = self.Resolve(data)
        mdw.data_63_0 = data_int
        mdw.data_123_64 = data_int >> 64
        mdw.pattern_word_type = 7
        array[offset:offset+16] = mdw.value


# Load standard macros
macro_file = os.path.join(os.path.dirname(__file__), 'macros.rpat')
HbiPatternAssembler.classMacros = fval.asm.LoadMacros(macro_file, HbiPatternAssembler)

if __name__ == "__main__":
    # Parse command line options
    import argparse
    parser = argparse.ArgumentParser(description = 'HPCC AC single slice pattern (.obj) assembler')
    parser.add_argument('infile', help = 'Pattern source code input filename', type = str)
    parser.add_argument('outfile', help = 'Pattern .obj output filename', type = str)
    parser.add_argument('-s', '--symbols', help = 'Create pattern symbols', action = "store_true")
    args = parser.parse_args()
    
    pattern = HbiPatternAssembler()
    pattern.Load(args.infile)
    s = time.perf_counter()
    pattern.Generate()
    print(time.perf_counter() - s)
    pattern.SaveObj(args.outfile)
    if args.symbols:
        pattern.SaveSymbols(args.outfile + '.symbols')