# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
from Common import fval
from .pin_multiplier_simulator import PinMultiplier

phylite = {0: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           1: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           1: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           2: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           3: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303}}


class Psdb(fval.Object):
    def __init__(self, slot, system):
        super().__init__()
        self.slot = slot
        self.pin_multiplier = [PinMultiplier(slot, x, system) for x in range(2)]


    def dc_trigger(self, trigger):
        device = (trigger >> 54) & 0x1
        self.pin_multiplier[device].dc_trigger(trigger)
    
    def set_active_channels_mask(self, slice, mask):
        for pm in self.pin_multiplier:
            pm.set_active_channels_mask(slice, mask)
    
    def set_ctp_mask(self, slice, mask):
        for pm in self.pin_multiplier:
            pm.set_ctp_mask(slice, mask)
