# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import math

from Common import fval
from Hbicc.instrument import patgen_register
from Hbicc.testbench.capture_structs import CTV_HEADER, CTV_HEADER_AND_DATA, TRACE, PIN_ERROR
from Hbicc.tools.CaptureDecoder.CaptureExpectUtilities import CTVExpectBuilder, CaptureDecodeExecuter

ONE_MB = 1024 * 1024
TWO_GB = 0x80000000
SINGLE_BLOCK_SIZE = 16  # bytes
BOUNDARY_MULTIPLE = 32  # BYTES
STREAM_BLOCK = 32
PM_CHIP_ID = {'PSDB_0_PIN_0': 0, 'PSDB_0_PIN_1': 1, 'PSDB_1_PIN_0': 2, 'PSDB_1_PIN_1': 3}
LOG_STREAM = True
MAX_STREAM_LOG = 6
SPLITTER_EXPONENT = 6
PIN_ERRROR_BLOCK = 64

PIN_MULTIPLIER_NUM = 4
CTV_DATA_STREAM_BLOCK_SIZE = PIN_MULTIPLIER_NUM * STREAM_BLOCK
CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM = 1
CTV_DATA_END_OF_BURST_BLOCK_NUM = CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM * PIN_MULTIPLIER_NUM


class Capture(fval.Object):
    def __init__(self, slice):
        self.slice = slice
        self.patgen = slice.hbicc.pat_gen
        self.rm = slice.hbicc.ring_multiplier
        self.pms = {}
        self.ctv_headers = []
        self.pm_error = {'PSDB_0_PIN_0': [], 'PSDB_0_PIN_1': [], 'PSDB_1_PIN_0': [], 'PSDB_1_PIN_1': []}
        self._is_header_set = False  # temp
        self.ctv_collective_data = None

        # self.wait_for_complete = True
        self.mask_failure_to_complete = False
        self.max_fail_count_per_pattern = ONE_MB // SINGLE_BLOCK_SIZE
        self.max_fail_capture_count = ONE_MB // SINGLE_BLOCK_SIZE
        self.max_capture_count = ONE_MB // SINGLE_BLOCK_SIZE
        self.capture_length = ONE_MB
        self.number_of_tries = 500
        self.global_max_fail = 50
        self.per_pattern_max_fail = 10

        # self.capture_type = 1
        self.capture_fails = 0
        self.capture_ctv = 1
        self.capture_all = 0
        self.overflow_action = 0
        self.stop_on_max_fail = 0
        self.capture_vector_data = 0
        self.flush_capture_buffer = 0
        self.capture_ctv_after_max_fail = 0

        self.CAPTURE_STREAM_MIN_ADDRESS = 0x400000000
        self.ctv_header_stream_start_address = 0x400000000
        self.ctv_data_stream_start_address = 0x440000000
        self.trace_stream_start_address = 0x480000000
        self.pm_stream_start_addresses = {0: 0x4c0000000, 1: 0x500000000, 2: 0x540000000, 3: 0x580000000}
        self.ctv_header_stream_end_address = 0x440000000
        self.ctv_data_stream_end_address = 0x480000000
        self.trace_stream_end_address = 0x4c0000000
        self.pm_stream_end_addresses = {0: 0x500000000, 1: 0x540000000, 2: 0x580000000, 3: 0x5c0000000}
        self.dst = None
        self.stream_data_dst = None
        self.stream_data_readable_dst = None
        self.all_errors = None
        self.dir = os.getcwd()
        self.test_module = self.slice.env.test.Module()
        self.test_name = self.slice.env.test.test_name()
        self.repeat_number = self.slice.env.test.repeat_number
        self.splitter_exponent = SPLITTER_EXPONENT
        self.expect_error_stream = {}
        self.first_capture_done = False
        self.check_capture_results = True

    def set_up(self, streams_address={}, attributes={}):
        self.set_streams_address(streams_address)
        self.set_attributes(attributes)
        self.write_attributes_to_registers()
        return self

    def set_streams_address(self, addresses):
        self.check_address_list(addresses)
        self.set_attributes(addresses)

    def write_attributes_to_registers(self, ):
        self.set_capture_start_addresses()
        self.set_capture_end_addresses()
        self.set_capture_controls()
        self.set_pat_gen_capture_control_registers()
        # self.reset_fail_count_registers()

    def set_capture_distribution_control(self):
        r = self.patgen.registers.CAPTURE_DISTRIBUTION_CONTROL()
        r.distribution = self.capture_distribution = self.slice.ctv_header_distribution
        r.block_size_exponent = self.capture_distribution_size = self.splitter_exponent
        self.patgen.write_slice_register(r, self.slice.index)
        self.Log('debug', f'Setting Capture Distribution Control: ' \
                         f'Slice {self.slice.index}, ' \
                         f'distribution: 0x{r.distribution:X} ' \
                         f'block_size_exponent: {r.block_size_exponent}')

    def set_capture_start_addresses(self):
        self.patgen.write_slice_register(
            patgen_register.CTV_HEADER_STREAM_START_ADDRESS_REG(value=self.ctv_header_stream_start_address >> 6),
            self.slice.index)
        self.patgen.write_slice_register(patgen_register.CTV_DATA_STREAM_START_ADDRESS_REG(self.ctv_data_stream_start_address >> 6),
                                         self.slice.index)
        self.patgen.write_slice_register(patgen_register.TRACE_STREAM_START_ADDRESS_REG(self.trace_stream_start_address >> 6),
                                         self.slice.index)
        for index, address in self.pm_stream_start_addresses.items():
            self.patgen.write_slice_register(patgen_register.ERROR_STREAM_PM_START_ADDRESS_REG(address >> 6), self.slice.index, index)

    def set_capture_end_addresses(self):
        self.patgen.write_slice_register(
            patgen_register.CTV_HEADER_STREAM_END_ADDRESS_REG(value=self.ctv_header_stream_end_address >> 6),
            self.slice.index)
        self.patgen.write_slice_register(patgen_register.CTV_DATA_STREAM_END_ADDRESS_REG(self.ctv_data_stream_end_address >> 6),
                                         self.slice.index)
        self.patgen.write_slice_register(patgen_register.TRACE_STREAM_END_ADDRESS_REG(self.trace_stream_end_address >> 6),
                                         self.slice.index)
        for index, address in self.pm_stream_end_addresses.items():
            self.patgen.write_slice_register(patgen_register.ERROR_STREAM_PM_END_ADDRESS_REG(address >> 6), self.slice.index, index)

    def set_capture_controls(self):
        self.set_active_pms()
        for index, pm in self.pms.items():
            self.set_capture_control_registers(pm)
            self.set_max_capture_error_count(pm)
        self.set_capture_counts_registers()

    def set_active_pms(self):
        pms = self.slice.hbicc.get_pin_multipliers()
        for pm in pms:
            if pm.slot_index == 0 or pm.slot_index == 2:
                self.pms.update({pm.slot_index: pm})
            if pm.slot_index == 1 or pm.slot_index == 3:
                self.pms.update({pm.slot_index: pm})

    def set_capture_control_registers(self, pm):
        capture_control = pm.registers.CAPTURE_CONTROL()

        capture_control.CaptureFails = self.capture_fails
        capture_control.CaptureCtv = self.capture_ctv
        capture_control.CaptureAll = self.capture_all
        capture_control.StopOnMaxFail = self.stop_on_max_fail
        capture_control.CaptureCtvAfterMaxFail = self.capture_ctv_after_max_fail
        capture_control.PmChipId = PM_CHIP_ID[pm.name()]

        pm.write_slice_register(capture_control, self.slice.index)
        control_reg = pm.read_slice_register(pm.registers.CAPTURE_CONTROL, self.slice.index)

    def set_max_capture_error_count(self, pm):
        pm.set_per_pattern_max_fail_count(self.per_pattern_max_fail, [self.slice.index])
        pm.set_global_max_fail_count(self.global_max_fail, [self.slice.index])

    def set_pat_gen_capture_control_registers(self):
        capture_control = self.patgen.read_slice_register(self.patgen.registers.CAPTURE_CONTROL, self.slice.index)
        capture_control.capture_fails = self.capture_fails
        capture_control.capture_ctv = self.capture_ctv
        self.patgen.write_slice_register(capture_control, self.slice.index)
        self.patgen.read_slice_register(self.patgen.registers.CAPTURE_CONTROL, self.slice.index)

    def check_capture_size(self):
        if self.capture_length > TWO_GB:
            self.Log('warning', 'Capture memory exceeds 2GB, reducing capture read to 1MB')
            self.length = ONE_MB

    def check_address_list(self, addresses):
        for name, address in addresses.items():
            if name in ['pm_stream_start_addresses', 'pm_stream_end_addresses']:
                self.check_addresses(address)
            else:
                self.address_format_check(address)

    def check_addresses(self, addresses):
        for address in addresses.values():
            self.address_format_check(address)

    def address_format_check(self, address):
        if (address % BOUNDARY_MULTIPLE) != 0:
            raise Exception(f'Slice {self.slice.index}: Address {hex(address)} is not a multiple of {BOUNDARY_MULTIPLE} bytes.')

        if not isinstance(address, int):
            raise Exception('Slice {}: Address is not a number.')
        if 0 > address < self.CAPTURE_STREAM_MIN_ADDRESS:
            self.Log('error', 'Slice {}: Address within allocated memory for Pattern.')

    def set_capture_counts_registers(self):
        self.patgen.write_slice_register(patgen_register.CAPTURE_LENGTH(value=self.capture_length), self.slice.index)
        self.patgen.write_slice_register(patgen_register.MAX_CAPTURE_COUNT(self.max_capture_count), self.slice.index)
        self.patgen.write_slice_register(patgen_register.MAX_FAIL_COUNT(self.max_fail_count_per_pattern), self.slice.index)
        self.patgen.write_slice_register(patgen_register.MAX_FAIL_CAPTURE_COUNT(self.max_fail_capture_count), self.slice.index)

    # TODO: Look up the registers to reset fail counts
    def reset_fail_count_registers(self):
        self.pm.register('TotalFailCount', 0)

    def set_attributes(self, attributes):
        for attribute, value in attributes.items():
            if attribute == 'pm_stream_start_addresses':
                self.set_pm_start_addresses(value)
            elif attribute == 'pm_stream_end_addresses':
                self.set_pm_end_addresses(value)
            else:
                self.set_params(attribute, value)

    def set_params(self, attribute, value):
        if hasattr(self, attribute):
            setattr(self, attribute, value)
        else:
            raise Exception(
                'Slice {}: Capture Class does not have the attribute {}'.format(self.slice.index, attribute))

    def set_pm_start_addresses(self, addresses):
        for index, value in addresses.items():
            self.pm_stream_start_addresses[index] = value

    def set_pm_end_addresses(self, addresses):
        for index, value in addresses.items():
            self.pm_stream_end_addresses[index] = value

    def process_keepalive_capture_data(self):
        self.Log('debug', f'Slice {self.slice.index}: Processing Keep Alive Capture Data now...')
        self.set_stream_file()
        for reg_index, pm in self.pms.items():
            self.process_error_stream_pm_with_expected_data(pm, reg_index)
        self.first_capture_done = True

    def process_capture_data(self):
        self.Log('debug', f'Slice {self.slice.index}: Processing Capture Data now...')
        self.set_stream_file()
        self.process_ctv_header_stream()
        self.process_ctv_data_stream()
        self.process_trace_stream()
        for reg_index, pm in self.pms.items():
            self.process_error_stream_pm(pm, reg_index)

        if self.stream_data_dst:
            self.stream_data_dst.close()
        if self.stream_data_readable_dst:
            self.stream_data_readable_dst.close()
        self.zip_data_files()

        if self.check_capture_results:
            self.capture_decode()

    def set_stream_file(self):
        if self.slice.pattern_helper.is_regression:
            dir = os.getcwd()
            test_module = self.slice.env.test.Module()
            test_name = self.slice.env.test.test_name()
            repeat_number = self.slice.env.test.repeat_number
            self.dst = os.path.join(dir, 'STREAM_CAPTURE', f'{test_module}', f'{test_name}')
            os.makedirs(self.dst, exist_ok=True)
            self.dst_file = os.path.join(self.dst, f"R{repeat_number}_S{self.slice.index}.txt")
            self.stream_data_dst = open(self.dst_file, "w+")

    def process_ctv_header_stream(self):
        actual_count, data = self.get_ctv_header_count_and_headers()
        self.compare_ctv_header(data, actual_count)
        self.write_stream_data_to_file(data, 'CTV_HEADER')

    def get_ctv_header_count_and_headers(self):
        if self.slice.hbicc.model.patgen.is_simulator_active:
            return self.get_ctv_header_block_count_and_bytes()
        else:
            return len(self.ctv_collective_data), self.ctv_collective_data

    def get_ctv_header_block_count_and_bytes(self, register_count=None, offset=0):
        if register_count is None:
            register_count = self.get_ctv_header_capture_register_count()

        ctv_memory_size_limit = self.ctv_header_stream_end_address - self.ctv_header_stream_start_address
        actual_count = ctv_memory_size_limit // STREAM_BLOCK
        total_bytes = min(register_count * STREAM_BLOCK, ctv_memory_size_limit)
        if total_bytes:
            data = self.patgen.dma_read(self.ctv_header_stream_start_address + offset * STREAM_BLOCK, total_bytes,
                                        self.slice.index)
        else:
            data = b''
        return actual_count, data

    def get_ctv_header_capture_register_count(self):
        register_count = self.get_stream_total_size(self.patgen, patgen_register.CTV_CAPTURE_HEADER_COUNT)
        return register_count

    def process_ctv_data_stream(self):
        actual_count, register_count, data = self.get_ctv_block_count_and_data()
        if data:
            if not self.slice.expected_ctv_data:
                checker = CtvDataChecker(register_count)
                checker.run_ctv_data_checker(self, self.slice, data)
            else:
                self.compare_ctv_data(data, actual_count)
                self.write_stream_data_to_file(data, 'CTV_DATA')

    def get_ctv_block_count_and_data(self):
        data = []
        actual_count, register_count, total_bytes = self.get_ctv_data_block_count_and_byte_count()
        self.Log('debug', f'CTV Data Stream Slice {self.slice.index} total_bytes: {total_bytes}')
        if total_bytes:
            data = self.patgen.dma_read(self.ctv_data_stream_start_address, total_bytes, self.slice.index)
        return actual_count, register_count, data

    def get_ctv_data_block_count_and_byte_count(self):
        register_count = self.get_ctv_data_capture_register_count()
        ctv_memory_limit = self.ctv_data_stream_end_address - self.ctv_data_stream_start_address
        if ctv_memory_limit <= 0:
            self.Log('error', 'The ctv data stream end address is not after start address')
        total_bytes = min(register_count * STREAM_BLOCK, ctv_memory_limit)
        actual_count = total_bytes // STREAM_BLOCK
        return actual_count, register_count, total_bytes

    def get_ctv_data_capture_register_count(self):
        register_count = self.get_stream_total_size(self.patgen, patgen_register.CTV_CAPTURE_DATA_COUNT)
        return register_count

    def capture_decode(self):
        if self.slice.pattern_helper.is_regression and not self.slice.hbicc.model.patgen.is_simulator_active:
            cde = CaptureDecodeExecuter(self.dst_file)
            actual_file_name = cde.generate_ctv_decode()

            dir = os.getcwd()
            dst = os.path.join(dir, 'STREAM_CAPTURE')
            os.makedirs(dst, exist_ok=True)

            ceb = CTVExpectBuilder(dst)
            passing = ceb.compare_slice(actual_file_name, self.slice.index)
            if not passing:
                self.Log('error', f'Slice {self.slice.index} CTV DATA not working.')

    def process_trace_stream(self):
        self.get_trace_data()
        if self.slice.hbicc.model.patgen.is_simulator_active:
            self.check_trace_count()
            self.compare_traces()
        self.write_stream_data_to_file(self._trace_data, 'TRACE')

    def check_trace_count(self):
        expected_trace_count = self.slice.hbicc.model.patgen.slices[self.slice.index].trace_count()
        observed_trace_count = self.patgen.trace_count(self.slice.index)
        if expected_trace_count != observed_trace_count:
            self.Log('error',
                     f'Expected trace count: {expected_trace_count}, observed_trace_count: {observed_trace_count}')

    def get_trace_data(self):
        byte_count = self.get_trace_count()
        if byte_count:
            self._trace_data = self.patgen.dma_read(self.trace_stream_start_address, byte_count, self.slice.index)
        elif byte_count == 0:
            self._trace_data = self.patgen.dma_read(self.trace_stream_start_address, 32, self.slice.index)
        else:
            self.Log('error',
                     f'TRACE Stream Count Do not Match: Slice {self.slice.index}: Expected {byte_count}')
            self._trace_data = b''

    def get_trace_count(self):
        register_byte_count = STREAM_BLOCK * self.patgen.trace_count(self.slice.index)
        byte_limit = self.trace_stream_end_address - self.trace_stream_start_address
        if byte_limit < STREAM_BLOCK:
            self.Log('error', 'TRACE Stream memory space is smaller than 32 bytes. '
                              'TRACE end address souldn\'t be setup this way')
        return min(register_byte_count, byte_limit)

    def get_trace_capture_register_count(self):
        return self.patgen.trace_count(self.slice.index)

    def process_error_stream_pm(self, pm, reg_index):
        data, register_count = self.get_error_stream_data_and_count(pm, reg_index)
        if data:
            fail_capture = FailCaptureChecker()
            fail_capture.run_pin_error_checker(self, pm, self.slice, data, register_count)

    def process_error_stream_pm_with_expected_data(self, pm, reg_index):
        data, register_count = self.get_error_stream_data_and_count(pm, reg_index)
        total_bytes = len(data)
        count = total_bytes // PIN_ERRROR_BLOCK
        if total_bytes:
            self.Log('debug', f'Slice {self.slice.index}: PM{pm.slot_index} total_bytes: {total_bytes}')
            data = self.patgen.dma_read(self.pm_stream_start_addresses[reg_index], total_bytes, self.slice.index)
            if self.first_capture_done:
                which_capture = 1
            else:
                which_capture = 0
            data_expects = self.expect_error_stream[which_capture][pm.slot_index]
            data_expect_string = data_expects.to_bytes(total_bytes,'little')
            result = {}
            ignore_parts = ['a_ch_set_clock_cycle','b_ch_set_clock_cycle','user_cycle']
            for index in range(count):
                start = index * PIN_ERRROR_BLOCK
                end = start + PIN_ERRROR_BLOCK
                observed = PIN_ERROR(data[start: end])
                expected = PIN_ERROR(data_expect_string[start: end])
                self.compare_class_fields(expected,observed,result,ignore_parts)
            if result:
                self.report_errors_by_fields(result, PIN_ERROR().name())
            if data is not None:
                fail_capture = FailCaptureChecker()
                fail_capture.run_pin_error_checker(self, pm, self.slice, data, register_count)

    def get_user_cycles_from_error_stream(self, pm, reg_index, section_start, section_end):
        data, register_count = self.get_error_stream_data_and_count(pm, reg_index)
        result = []
        if len(data):
            for index in range(section_start, section_end):
                start = index * PIN_ERRROR_BLOCK
                end = start + PIN_ERRROR_BLOCK
                result.append(PIN_ERROR(data[start: end]).user_cycle)
        return result

    def get_error_stream_data_and_count(self, pm, reg_index):
        count = self.get_pin_error_capture_register_count(reg_index)
        error_stream_byte_limit = self.pm_stream_end_addresses[reg_index] - self.pm_stream_start_addresses[reg_index]
        if error_stream_byte_limit <= 0:
            error_list = [{'pin error stream size': error_stream_byte_limit,
                           'pin error stream start address': self.pm_stream_start_addresses[reg_index],
                           'pin error stream end address': self.pm_stream_end_addresses[reg_index]}]
            table = self.slice.hbicc.contruct_text_table(data=error_list)
            self.Log('error', f'Slice: {self.slice.index}, PM: {reg_index}: pin error stream not having positive size: '
                              f'{table}')
        total_bytes = min(count * STREAM_BLOCK, error_stream_byte_limit)
        if total_bytes:
            self.Log('debug',
                     f'reg_index {reg_index} Slice {self.slice.index}: PM{pm.slot_index} total_bytes: {total_bytes}')
            data = self.patgen.dma_read(self.pm_stream_start_addresses[reg_index], total_bytes, self.slice.index)
            return data, count
        return []

    def get_pin_error_capture_register_count(self, reg_index):
        count = self.get_stream_total_size(self.patgen, patgen_register.ERROR_STREAM_PM_COUNT, reg_index)
        return count

    def get_stream_total_size(self, device, register, reg_index=0):
        count = device.read_slice_register(register, self.slice.index, reg_index).value
        return count

    def get_ctv_stream_total_size(self):
        count = self.patgen.read_slice_register(patgen_register.CTV_CAPTURE_HEADER_COUNT, self.slice.index).value
        return count

    def add_result(self, expected, observed, row):
        if observed.value != expected.value:
            row.update({'Results': 'FAILED'})
        else:
            row.update({'Results': 'PASSED'})

    def get_field_values(self, data, row, prefix='', default=None):
        fieldnames = [field[0] for field in data._fields_]
        if default is not None:
            for fieldname in fieldnames:
                row.update({f'{prefix}{fieldname}': f'{default}'})
        elif data.int_value % 0xCC == 0:
            for fieldname in fieldnames:
                row.update({f'{prefix}{fieldname}': f'0x{getattr(data, fieldname):X}'})
        else:
            for fieldname in fieldnames:
                if fieldname.lower() in ['packet1', 'packet2', 'packet3', 'Reserved0', 'Reserved2', 'Reserved3']:
                    row.update({f'{prefix}{fieldname}': f'0x{getattr(data, fieldname):016X}'})
                elif fieldname.lower() in ['entry_is_wide']:
                    row.update({f'{prefix}{fieldname}': f'0x{getattr(data, fieldname):08b}'})
                elif fieldname.lower() in ['valid_cycles', 'relative_cycle_count']:
                    row.update({f'{prefix}{fieldname}': f'0x{getattr(data, fieldname):08X}'})
                elif fieldname.lower() in ['ctp_mask']:
                    row.update({f'{prefix}{fieldname}': f'0b{getattr(data, fieldname):035b}'})
                elif 'bin' in fieldname.lower():
                    row.update({f'{prefix}{fieldname}': f'0b{getattr(data, fieldname):b}'})
                else:
                    row.update({f'{prefix}{fieldname}': f'{getattr(data, fieldname)}'})

    def convert_fieldname_into_headers(self, object, add_expected_headers=True, add_result_col=True):
        headers = []
        for field in object._fields_:
            if field[0] not in headers:
                headers.append(field[0])
            if add_expected_headers:
                ex_header = f'ex_{field[0]}'
                if ex_header not in headers:
                    headers.append(ex_header)
        if add_expected_headers:
            headers.append('Results')
        return headers

    def write_stream_data_to_file(self, data, stream_name):
        if self.slice.pattern_helper.is_regression:
            count = len(data) // STREAM_BLOCK
            self.stream_data_dst.write(f'{stream_name} {count}\n')
            offset = 0
            for i in range(0, len(data), STREAM_BLOCK):
                start, end = i, i + STREAM_BLOCK
                if data[start: end]:
                    block_data = data[start: end]
                    temp0 = ''
                    for iter, i in enumerate(block_data.hex()):
                        if iter % 32 == 0:
                            temp0 += f'{offset:8} : '
                            offset += 16
                        temp0 += i
                        if iter % 32 == 31 and iter != 0:
                            temp0 += '\n'
                        elif iter % 2 == 1:
                            temp0 += ' '
                    self.stream_data_dst.write(f'{temp0}')

    def compare_ctv_header(self, data, count):
        if not self.slice.hbicc.model.patgen.is_simulator_active:
            return
        fails = 0
        expected_data = self.slice.hbicc.model.patgen.slices[self.slice.index].ctv_headers()
        if expected_data is None or len(expected_data) == 0:
            return
        if expected_data == data:
            self.Log('debug', f'Slice {self.slice.index}: CTV HEADER Bin-2-Bin Compare Passed')
            return
        expected_count = len(expected_data) // 32
        if expected_count != count:
            self.Log('error',
                     f'CTV Header Stream Count Do not Match: Slice {self.slice.index}: Expected {expected_count} vs. Observed {count}')
        else:
            self.Log('debug',
                     f'CTV Header Stream Count Slice {self.slice.index}: Expected {expected_count} vs. Observed {count}')
        if expected_count > 0 and count > 0:
            results = {field[0]: 0 for field in CTV_HEADER()._fields_}
            min_count = min(count, expected_count)
            for i in range(0, min_count):
                start = i * STREAM_BLOCK
                end = start + STREAM_BLOCK
                block = data[start:end]
                expected_block = expected_data[i]
                observed = CTV_HEADER(block)
                expected = CTV_HEADER(expected_data[start:end])
                if block != expected_block:
                    if expected.trace_index == 0xFFFFFF:
                        expected.trace_index = observed.trace_index
                    if expected.value != observed.value:
                        self.compare_ctv_header_fields(expected, observed, results=results)
                        if MAX_STREAM_LOG > fails:
                            self.Log('error', f'Count #: {i} Following CTV Headers Do Not Match...')
                            self.Log('error', f'Expected: {expected.log_console()}')
                            self.Log('error', f'Observed: {observed.log_console()}')
                        fails += 1

            if fails != 0:
                self.Log('error', f'Total CTV Header Failures: {fails} out of {min_count} block comparisons.')
                self.report_errors_by_fields(results, CTV_HEADER().name())
            else:
                self.Log('debug', f'Slice {self.slice.index}: CTV HEADER results are as expected')

    def compare_ctv_header_fields(self, expected, observed, results):
        if expected.last_pcall_address != observed.last_pcall_address:
            results['last_pcall_address'] += 1
        if expected.vector_address != observed.vector_address:
            results['vector_address'] += 1
        if expected.pattern_cycle != observed.pattern_cycle:
            results['pattern_cycle'] += 1
        if expected.repeat_count != observed.repeat_count:
            results['repeat_count'] += 1
        if expected.selectable_field != observed.selectable_field:
            results['selectable_field'] += 1
        if expected.user_cycle != observed.user_cycle:
            results['user_cycle'] += 1
        if expected.dut_serial_control != observed.dut_serial_control:
            results['dut_serial_control'] += 1
        if expected.reserved != observed.reserved:
            results['reserved'] += 1
        if expected.trace_index == 0xFFFFFF:
            # value read from FPGA not predictable
            pass
        elif expected.trace_index != observed.trace_index:
            results['trace_index'] += 1
        if expected.pattern_counter != observed.pattern_counter:
            results['pattern_counter'] += 1

    def compare_ctv_data(self, data, count):
        if not self.slice.hbicc.model.patgen.is_simulator_active:
            return
        self.Log('debug', f'Slice {self.slice.index}: Comparing CTV Data')
        fails = 0
        repeat_number = self.slice.env.test.repeat_number
        eob_packets = {key: False for key, pm in self.pms.items()}
        eob_tally = {key: [] for key, value in self.pms.items()}
        eob_seen = {key: 0 for key, value in self.pms.items()}
        over_expected = 0
        expected_packet_count = 0
        eob_final = 0
        count_exclude_cycle_count = 0
        packets_present = False
        is_cycle_count = 0
        if self.slice.expected_ctv_data:
            if self.slice.expected_ctv_data == data:
                self.Log('debug',
                         f'CTV DATA: Observed matches Expected')
                return
            expected_packet_count = 0
            pm_index = {0: 0, 1: 0, 2: 0, 3: 0}
            results = {field[0]: 0 for field in CTV_HEADER_AND_DATA()._fields_}
            for pin, packets in self.slice.expected_ctv_data.items():
                expected_packet_count += len(packets)

            for i in range(0, count):
                x = i * STREAM_BLOCK
                start, end = x, x + STREAM_BLOCK
                block = data[start: end]
                observed = CTV_HEADER_AND_DATA(block)
                pm = observed.originating_chip
                eob = observed.end_of_burst
                if observed.is_cycle_count == 1:
                    is_cycle_count += 1
                    continue

                packets_present = True
                count_exclude_cycle_count += 1
                if observed.int_value == 0:
                    self.Log('warning', f'PM {pm} Packet {i} is 0')
                    expected = CTV_HEADER_AND_DATA(b'\x00')
                else:
                    try:
                        expected = self.slice.expected_ctv_data[pm][pm_index[pm]]
                    except:
                        if MAX_STREAM_LOG > fails:
                            self.Log('error',
                                     f'CTV DATA: Slice:{self.slice.index} Repeat: {repeat_number}. More observed (None Zero Value) than expected. Defaulting Expected to all 0xCC for packet number {i}')
                        expected = CTV_HEADER_AND_DATA(bytes([0xcc] * 32))
                        over_expected += 1

                if block != expected.value:
                    self.compare_class_fields(expected, observed, results=results)
                    if MAX_STREAM_LOG > fails:
                        self.Log('error',
                                 f'pm_{pm} Block #: {pm_index[pm]} Repeat {repeat_number} Following CTV DATA Do Not Match...')
                        self.Log('error', f'Expected: {expected.log_console()}')
                        self.Log('error', f'Observed: {observed.log_console()}')
                    fails += 1
                pm_index[pm] += 1
                if eob == 1:
                    eob_packets[pm] = True
                    eob_tally[pm].append(observed)
                    eob_final += 1

                if eob_packets[pm] and observed.int_value != 0:
                    eob_seen[pm] += 1
        if data and self.slice.expected_ctv_data:
            if expected_packet_count - 1 <= count_exclude_cycle_count <= expected_packet_count + 1:
                self.Log('debug', f'Slice {self.slice.index}: CTV DATA Stream Count Check Passed')
            else:
                self.Log('error',
                         f'CTV DATA Stream count did not match: Observed {count_exclude_cycle_count} vs. Expected {expected_packet_count}')

            for key, value in eob_seen.items():
                if value > 1:
                    repeat_number = self.slice.env.test.repeat_number
                    self.Log('error',
                             f'Slice {self.slice.index}: Extra Packets after EOB on PM{key}, {value-1} extra packets. Repeat #{repeat_number}')

            if eob_final != len(self.pms) and packets_present:
                self.Log('error', f'CTV DATA: Wrong Number of EOB seen. {eob_seen}')
            for pm, eob_objects in eob_tally.items():
                if len(eob_objects) > 1:
                    self.Log('error', f'CTV DATA: PM {pm} saw {len(eob_objects)} which is more than one EOB:')
                    for i in eob_objects:
                        self.Log('error', f'Slice {self.slice.index}: Extra EOB: {i.log_console()}')

            if over_expected > 0:
                self.Log('error', f'Slice {self.slice.index}: More packets seen than expected. Over by {over_expected}')

            if len(self.pms) != len(eob_packets):
                self.Log('warning', f'EOB was only seen for {len(eob_packets)} PMs.')
            if fails != 0:
                self.Log('error', f'Slice {self.slice.index}: Total CTV DATA Failures: {fails}')
                self.report_errors_by_fields(results, CTV_HEADER_AND_DATA().name())
            else:
                self.Log('debug', f'Slice {self.slice.index}: CTV HEADER and DATA Results Passed.')

            self.Log('debug', f'Slice {self.slice.index}: is_cycle_count packets seen {is_cycle_count} times')
        elif data and not self.slice.expected_ctv_data:
            eob_packets = {key: False for key, pm in self.pms.items()}
            eob_tally = {key: [] for key, value in self.pms.items()}
            eob_seen = {key: 0 for key, value in self.pms.items()}
            none_eob = 0
            for i in range(0, count):
                x = i * STREAM_BLOCK
                start, end = x, x + STREAM_BLOCK
                block = data[start: end]
                observed = CTV_HEADER_AND_DATA(block)
                pm = observed.originating_chip
                if observed.end_of_burst == 1:
                    eob_packets[pm] = True
                    eob_tally[pm].append(observed)
                    eob_final += 1
                else:
                    none_eob += 1
            if eob_final != len(self.pms) and packets_present:
                self.Log('error', f'CTV DATA: Wrong Number of EOB seen. {eob_seen}')
            if none_eob:
                self.Log('error', f'Slice {self.slice.index}: No CTV Data set but none-EOB data present.')
        else:
            self.Log('error', f'Slice {self.slice.index}: No CTV Data set but obrserved data present.')

    def compare_packet(self, byte_data):
        expected = self.slice.pattern_helper.expected_ctv
        if expected:
            packet_value = expected['packet']
            if self.block_count:
                if packet_value != byte_data:
                    self.Log('error',
                             f'CTV data packet fail for Slice {self.slice.index}. Observed {byte_data} != Expected {packet_value}')
            else:
                if packet_value != b'\x00\x00\x00\x00\x00\x00\x00\x00':
                    self.Log('error',
                             f'CTV data packet fail for Slice {self.slice.index}. Observed {byte_data} != Expected 0x0')
            self.block_count -= 1

    def compare_traces(self):
        data = self._trace_data
        expected_data = self.slice.hbicc.model.patgen.slices[self.slice.index].trace_data()
        if data != expected_data:
            self.report_trace_differences(data, expected_data)
        else:
            self.Log('debug', f'Slice {self.slice.index}: TRACE Results Passed')

    def report_trace_differences(self, data, expected_data):
        if len(data) != len(expected_data):
            self.Log('error', f'FPGA trace has {len(data)} bytes, but model trace has {len(expected_data)} bytes')
        count = len(data) // STREAM_BLOCK
        results = {field[0]: 0 for field in TRACE()._fields_}
        for i in range(0, count):
            x = i * STREAM_BLOCK
            start, end = x, x + STREAM_BLOCK
            block = data[start: end]
            expected_block = expected_data[start: end]
            if block != expected_block:
                observed = TRACE(block)
                expected = TRACE(expected_block)
                observed.cache_hit_miss= observed.cache_hit_miss&0
                self.compare_class_fields(expected, observed, results=results)
                if sum(results.values()) > 0:
                    self.Log('error', 'Trace does not match model')
                    self.Log('error', f'Expected: {expected.log_console()}')
                    self.Log('error', f'Observed: {observed.log_console()}')
        if sum(results.values()) > 0:
            self.report_errors_by_fields(results, TRACE().name())

    def report_errors_by_fields(self, results, title):
        headers = ['Slice', 'Field', 'Total Fails']
        data = []
        for key, value in results.items():
            data.append({'Slice': self.slice.index, 'Field': key, 'Total Fails': value})

        table = self.slice.hbicc.contruct_text_table(headers, data)
        self.Log('error', f'{table}')

    def compare_class_fields(self, expected, observed, results={}, ignored=[]):
        if not expected or not observed:
            self.increment_value_in_dict(results, 'NoneType')

        if expected.name() != observed.name():
            self.increment_value_in_dict(results, 'Name Mismatch')

        for field in expected._fields_:
            field_name = field[0]
            expected_value = getattr(expected, field[0])
            observed_value = getattr(observed, field[0])
            if expected_value != observed_value:
                if field_name not in ignored:
                    self.increment_value_in_dict(results, field_name)

    def increment_value_in_dict(self, dict, key, value=1):
        if key not in dict:
            dict[key] = value
        else:
            dict[key] += 1

    def zip_data_files(self):
        return
        if not self.slice.pattern_helper.is_regression:
            return
        self.slice.hbicc.zip_files_in_folder(self.dst)

    def write_data_to_bin_file(self, data):
        dst_bin = os.path.join(self.dst,
                               f"Ctv_Data_Bin_R{self.repeat_number}_S_{self.slice.index}_{self.test_name}.bin")
        with open(dst_bin, 'wb') as outfile:
            outfile.write(data)


class CaptureBase(fval.Object):
    def __init__(self):
        pass

    def _compare_class_fields(self, expected, observed, results={}):
        if not expected or not observed:
            self._increment_value_in_dict(results, 'NoneType')
        if expected.name() != observed.name():
            self._increment_value_in_dict(results, 'Name Mismatch')
        for field in expected._fields_:
            field_name = field[0]
            expected_value = getattr(expected, field[0])
            observed_value = getattr(observed, field[0])
            if expected_value != observed_value:
                self._increment_value_in_dict(results, field_name)

    def _increment_value_in_dict(self, dict, key, value=1):
        if key not in dict:
            dict[key] = value
        else:
            dict[key] += 1

    def _report_errors_by_fields(self, results, title=''):
        headers = ['Slice', 'Field', 'Total Fails']
        data = []
        for key, value in results.items():
            if value > 0:
                data.append({'Slice': self._slice.index, 'Field': key, 'Total Fails': value})
        table = self._slice.hbicc.contruct_text_table(headers, data, title_in=title)
        self.Log('error', f'{table}')

    def _is_binary_equivalent(self, data1, data2):
        if data1 == data2:
            return True
        return False

    def _report_results(self):
        if not self._capture.slice.hbicc.model.patgen.is_simulator_active:
            return
        for pm, fail_results in self._failures.items():
            if fail_results['Failures']:
                self.Log('error', f'Slice {self._slice.index} PM{pm}. Failures: {fail_results["Failures"]}')
                self._report_errors_by_fields(fail_results['Results'])

    def _set_observed_expected(self, block_number, block_size):
        start = block_number * block_size
        end = start + block_size
        self._expected = self._model_data[start: end]
        self._observed = self._data[start: end]

    def _record_failure(self, data_type):
        observed = data_type(self._observed)
        expected = data_type(self._expected)
        self._failures[self._pm.slot_index]['Failures'] += 1
        self._compare_class_fields(expected, observed, self._failures[self._pm.slot_index]['Results'])
        if self._failures[self._pm.slot_index]['Failures'] < MAX_STREAM_LOG:
            self.Log('error', f'Current Block {self._current_block} Expected: {expected.log_console()}')
            self.Log('error', f'Current Block {self._current_block} Observed: {observed.log_console()}')

    def _get_channel_sets(self):
        channel_sets = []
        for channel_set in self._slice.channel_sets:
            if self._is_channel_set_in_pm(channel_set):
                channel_sets.append(channel_set)
        return channel_sets

    def _is_channel_set_in_pm(self, channel_set):
        if self._pm.slot_index % 2 == channel_set // 4:
            return True
        return False

    def _write_stream_data_to_file(self, data, stream_name):
        if self._slice.pattern_helper.is_regression:
            self._capture.stream_data_dst.write(f'{stream_name} {self._blocks}\n')
            offset = 0
            for i in range(0, len(data), STREAM_BLOCK):
                start, end = i, i + STREAM_BLOCK
                block_data = data[start: end]
                if block_data:
                    offset = self._write_entry(block_data, offset)

    def _write_entry(self, block_data, offset):
        entry = ''
        for iter, i in enumerate(block_data.hex()):
            if iter % 32 == 0:
                entry += f'{offset:8} : '
                offset += 16
            entry += i
            if iter % 32 == 31 and iter != 0:
                entry += '\n'
            elif iter % 2 == 1:
                entry += ' '
        self._capture.stream_data_dst.write(f'{entry}')
        return offset


class FailCaptureChecker(CaptureBase):
    def __init__(self):
        super(CaptureBase).__init__()

    def run_pin_error_checker(self, capture, pm, slice, data, register_count):
        self.Log('debug', f'Processing Pin Error Capture')
        self._set_up(capture, pm, slice, data, register_count)
        self._disposition_data()
        self._report_results()
        self._write_stream_data_to_file(data, 'ERROR')

    def _set_up(self, capture, pm, slice, data, register_count):
        self._slice = slice
        self._capture = capture
        self._blocks = len(data) // PIN_ERRROR_BLOCK
        self._current_block = 0
        self.register_count = register_count
        self._eob = 0
        if capture.slice.hbicc.model.patgen.is_simulator_active:
            self._pm = pm
            self._data = data
            self._channel_sets = self._get_channel_sets()
            self._failures = {pm.slot_index: {'Failures': 0, 'Results': {}} for pm in self._capture.pms.values()}
            self._model_data = self._slice.hbicc.model.patgen.slices[self._slice.index].pin_error_data(
                self._pm.slot_index)

    def _disposition_data(self):
        if not self._capture.slice.hbicc.model.patgen.is_simulator_active:
            return
        if not self._is_binary_equivalent(self._data, self._model_data):
            self._check_data()

    # TODO: Fixed to 2 but this might change when masking other channel sets.
    def _check_eob(self):
        if self._eob != 2:
            self.Log('error', f'Incorrect EOBs: Expected 2, Observed {self._eob}')

    def _record_eob(self):
        fail_capture = PIN_ERROR(self._observed)
        if fail_capture.a_ch_set_burst_is_done:
            self._eob += 1

    def _is_eob(self):
        observed = PIN_ERROR(self._observed)
        if observed.a_ch_set_burst_is_done:
            return True
        return False

    def _check_data(self):
        for block_number in range(self._blocks):
            self._current_block = block_number
            self._set_observed_expected(block_number, PIN_ERRROR_BLOCK)
            self._record_eob()
            if not self._is_binary_equivalent(self._observed, self._expected):
                if not self._is_eob():
                    self._record_failure(PIN_ERROR)
        if not self.is_pm_stream_overflow():
            self._check_eob()

    def is_pm_stream_overflow(self):
        return self.register_count * STREAM_BLOCK > len(self._data)


class CtvDataChecker(CaptureBase):
    def __init__(self, ctv_block_count_from_register):
        super(CaptureBase).__init__()
        self.ctv_block_count_from_register = ctv_block_count_from_register

    def run_ctv_data_checker(self, capture, slice, data):
        self.Log('debug', f'Processing CTV DATA Capture')
        self._set_up(capture, slice, data)
        self._disposition_data()
        self._report_results()
        self._write_stream_data_to_file(data, 'CTV DATA')

    def _set_up(self, capture, slice, data):
        self._slice = slice
        self._capture = capture
        self._blocks = len(data) // STREAM_BLOCK
        self._is_cycle_count_tally = 0
        if capture.slice.hbicc.model.patgen.is_simulator_active:
            self._pm = None
            self._data = data
            self._pm_counters = {x: {'index': 0, 'EOB': 0} for x in range(4)}
            self._pm_header_blocks = {x: {'index': None} for x in range(4)}
            self._failures = {pm.slot_index: {'Failures': 0, 'Results': {}} for pm in self._capture.pms.values()}
            self._model_data = self._slice.hbicc.model.patgen.slices[self._slice.index].ctv_data()

    def _get_model_block_count(self):
        total = 0
        for pm in self._capture.pms:
            reg = pm.read_slice_register(pm.registers.CAPTURE_CONTROL, slice=self._slice)
            if reg.ctp_override_active and reg.ctp_override_chip_wide:
                total += 1

        len(self._model_data) // STREAM_BLOCK

    def _disposition_data(self):
        if not self._capture.slice.hbicc.model.patgen.is_simulator_active:
            return
        if not self._is_binary_equivalent(self._data, self._model_data):
            self._check_data()
            if not self._is_ctv_stream_overflow():
                self._check_eob()

    def _check_eob(self):
        for pm, counters in self._pm_counters.items():
            eob = self._pm_counters[pm]['EOB']
            if eob != 1:
                self.Log('error', f'PM{pm} Incorrect EOBs: Expected 1, Observed {eob}')

    def _is_ctv_stream_overflow(self):
        end_address = self._capture.ctv_data_stream_end_address
        start_address = self._capture.ctv_data_stream_start_address
        ctv_data_memory_size = (end_address - start_address) / STREAM_BLOCK
        ctv_data_memory_info = [{'start_address': start_address, 'end_address': end_address,
                                 'data_memory_size': ctv_data_memory_size, 'block_count': self._blocks}]
        ctv_data_memory_info_table = self._slice.hbicc.contruct_text_table(data=ctv_data_memory_info,
                                                                           title_in=f'ctv data memory information')
        self.Log('debug', ctv_data_memory_info_table)
        return ctv_data_memory_size < self.ctv_block_count_from_register

    def _check_data(self):
        self._new_header_block = True
        self._block_tally = 0
        for block_number in range(self._blocks):
            self._current_block = block_number
            self._set_observed_expected(block_number, STREAM_BLOCK)
            if not self._is_binary_equivalent(self._observed, self._expected):
                if not self._is_eob():

                    self._record_failure(CTV_HEADER_AND_DATA)

    def _is_eob(self):
        observed = CTV_HEADER_AND_DATA(self._observed)
        if observed.end_of_burst:
            return True
        return False

    def _set_observed_expected(self, block_number, block_size):
        start = self._current_block * block_size
        end = start + block_size
        self._set_observed(start, end)

    def _set_observed(self, start, end):
        ctv_data = CTV_HEADER_AND_DATA(self._data[start: end])
        if self._new_header_block:
            self._set_next_header_block(ctv_data, end, start)
        elif self._block_tally <= self._block_count:
            self._set_non_header_block(end, start)

        # if self._block_count == 0:
        #     self._new_header_block = True

    def _set_non_header_block(self, end, start):
        self._observed = self._data[start: end]
        self._set_expected()
        if self._block_count == self._block_tally:
            self._new_header_block = True
        self._block_tally += 1

    def _set_next_header_block(self, ctv_data, end, start):
        self._observed = self._data[start: end]
        self._set_current_pm(ctv_data.originating_chip)
        self._set_eob(ctv_data)
        self._block_count = math.ceil((ctv_data.block_count - 3) / 4)
        self._set_expected()
        self._block_tally = 1
        if self._block_count != 0:
            self._new_header_block = False

    def _set_eob(self, ctv_data):
        if ctv_data.end_of_burst:
            self._pm_counters[self._pm.slot_index]['EOB'] += 1
            self._is_current_block_eob = True
        else:
            self._is_current_block_eob = False

    def _set_current_pm(self, pm):
        try:
            self._pm = self._capture.pms[pm]
        except:
            self.Log('error', f'PM {pm} does not exist in Capture PMS Dictionary')

    def _set_expected(self):
        end_, start_ = self._get_start_end_for_current_pm_index()
        self._expected = self._model_data[start_: end_]

    def _get_start_end_for_current_pm_index(self):
        if self._is_block_count():
            start, end = self._get_non_header_block_adress()
        elif self._is_current_block_eob:
            start, end = self._get_eob_block_address()
        else:
            header_index = self._pm_header_blocks[self._pm.slot_index]['index']
            current_header_index = self._get_header_block_number(header_index)
            start, end = self._find_next_header(current_header_index)

        return end, start

    def _is_block_count(self):
        return self._block_tally <= self._block_count and not self._new_header_block

    def _find_next_header(self, block_num):
        while True:
            temp_block, start, end = self._get_data_block(block_num)
            if self._pm.slot_index == temp_block.originating_chip and not temp_block.end_of_burst:
                self._pm_header_blocks[self._pm.slot_index]['index'] = block_num
                break
            elif self._pm.slot_index == temp_block.originating_chip and temp_block.end_of_burst:
                break
            block_num += 1 + math.ceil((temp_block.block_count - 3) / 4)
        return start, end

    def _get_header_block_number(self, block_num):
        if block_num is None:
            block_num = 0
        else:
            temp_block, _, _ = self._get_data_block(block_num)
            block_num += 1 + math.ceil((temp_block.block_count - 3) / 4)
        return block_num

    def _get_data_block(self, block_num):
        start = block_num * 32
        end = start + 32
        temp_block = CTV_HEADER_AND_DATA(self._model_data[start: end])
        return temp_block, start, end

    def _get_eob_block_address(self):
        eob_start = len(self._model_data) - 128
        start = eob_start + (self._pm.slot_index * 32)
        end = start + 32
        return start, end

    def _get_non_header_block_adress(self):
        start = (self._pm_header_blocks[self._pm.slot_index]['index'] + self._block_tally) * 32
        end = start + 32
        return start, end

