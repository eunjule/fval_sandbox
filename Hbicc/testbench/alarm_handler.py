from Common.fval import Object

CRITICAL_ALARM_FIELDS = {'underrun', 'ecc_error', 'Psdb0XcvrCrcError', 'Psdb1XcvrCrcError', 'PatgenXcvrCrcError',
                         'xcvr_crc_error', 'PmXcvrCrcError', 'RmXcvrCrcError', 'capture_this_pin_full',
                         'enable_clock_full', 'active_channel_full', 'dut_serial_control_full', 'apply_mask_full',
                         'incorrect_first_fetched_address', 'illegal_vector_format', 'illegal_instruction',
                         'read_on_empty_last_pcall_addr', 'write_on_full_last_pcall_addr', 'write_on_full_trace_index'}

class AlarmHandler(Object):
    def __init__(self, hbicc, slices):
        self.hbicc = hbicc
        self.slices = slices
        self.critical_alarm_fields = CRITICAL_ALARM_FIELDS.copy()
        self.critical_alarms_causing_auto_abort = {'stack_error', 'illegal_instruction'}

    def log_all_alarms(self, slices, check_correctness, table_title=''):
        self._reset()
        self.table_title = table_title
        self.check_correctness = check_correctness
        self.slices = slices
        self._get_alarms(slices)
        self._log_alarms_simplified()
        if self.total_fails:
            self._log_alarms_by_attributes()

    def _reset(self):
        self.is_exit_test = False
        self.total_fails = []
        self.alarms = []
        self.data = []
        self.entries = {}
        self.has_attribute_failed = {}
        self.headers = ['Device', 'Register']
        self.table_title = ''
        self.check_correctness = True
        self.log_level = 'info'
        self.comments = {}

    def _log_alarms_by_attributes(self):
        self.headers.append('Attribute')
        for alarm in self.alarms:
            for device, values in alarm.items():
                for alarm_name, slice_values in values.items():
                    self.entries = {}
                    self._get_alarm_att_for_all_slices(alarm_name, device, slice_values)
                    self.add_attribute_to_data()
        self._log_data()

    def add_attribute_to_data(self):
        for att_name, key_values in self.entries.items():
            if self.has_attribute_failed.get(att_name, False):
                self._add_comments_to_entry(att_name, key_values)
                self.data.append(key_values)

    def _add_comments_to_entry(self, att_name, key_values):
        if att_name in self.comments.keys():
            comments = ', '.join(self.comments[att_name])
            comments = ', '.join(self.comments[att_name])
            self._add_header('Comments')
        else:
            comments = ''
        key_values.update({'Comments': comments})

    def _log_data(self, log_level='debug'):
        if not self.check_correctness:
            self.log_level = 'debug'
        if self.data:
            table = self.hbicc.contruct_text_table(self.headers, self.data, title_in=self.table_title)
            self.Log(self.log_level, f'Alarms Broken Down by Alarm Bits {table}')

    def _get_alarm_att_for_all_slices(self, alarm_name, device, slice_values):
        for index, alarm_object in slice_values.items():
            for field in alarm_object._fields_:
                self._add_attribute_entry(index, alarm_name, device, field, alarm_object)

    def _add_attribute_entry(self, index, alarm_name, device, field, alarm_object):
        field_name, value = field[0], getattr(alarm_object, field[0])
        if self._is_skip(field_name):
            return
        self.add_field_name_to_keys(alarm_name, device, field_name)
        header = f'Slice {index} (Raw)'
        self._add_header(header)
        self.entries[field_name].update({header: f'{value} (0x{alarm_object.value:08X})'})
        self._exit_test_if_critical_alarm_raised(field_name, value, index)
        self._is_failure(field_name, value)

    def _is_failure(self, field_name, value):
        if value:
            self.has_attribute_failed.update({field_name: True})

    def add_field_name_to_keys(self, alarm_name, device, field_name):
        if field_name not in self.entries.keys():
            one_entry = {field_name: {'Device': device, 'Register': alarm_name, 'Attribute': field_name}}
            self.entries.update(one_entry)

    def _add_header(self, header):
        if header not in self.headers:
            self.headers.append(header)

    def _exit_test_if_critical_alarm_raised(self, field_name, value, index):
        if value == 0:
            return
        if self._is_field_critical(field_name):
            if field_name.lower() == 'ecc_error':
                if self._dispatch_error(field_name.lower())(index, self._get_comments(field_name)):
                    if self.check_correctness:
                        self.is_exit_test = True
            else:
                if self.check_correctness:
                    self.is_exit_test = True

    def _get_comments(self, field):
        if field not in self.comments.keys():
            self.comments.update({field: []})
        return self.comments[field]

    def _is_field_critical(self, field_name):
        if field_name.lower() in self.critical_alarm_fields:
            return True
        return False

    def _dispatch_error(self, error):
        error_map = {'ecc_error': self.hbicc.pat_gen.is_ecc_error_crital}
        return error_map.get(error, self._unhandled_error)

    def _unhandled_error(self, field_name, index):
        raise Exception(f'Alarm Attribute Unhandled Slice {index}: {field_name}')

    def _is_skip(self, field_name):
        if field_name.lower() in ['reserved']:
            return True
        return False

    def _log_alarms_simplified(self):
        data = []
        headers = ['Device', 'Register', ]
        self.get_raw_alarms(data, headers, [self.hbicc.pat_gen.registers.ALARMS1, self.hbicc.pat_gen.registers.ALARMS2],
                            self.hbicc.pat_gen)
        self.get_raw_alarms(data, headers, [self.hbicc.ring_multiplier.registers.ALARMS],
                            self.hbicc.ring_multiplier)
        for pm in self.hbicc.get_pin_multipliers():
            self.get_raw_alarms(data, headers, [pm.registers.ALARMS], pm)

        table = self.hbicc.contruct_text_table(headers, data, title_in=self.table_title)
        self.Log('debug', f'Alarms Raw Values. {table}')

    def get_raw_alarms(self, data, headers, registers, device):
        for register in registers:
            entry = {'Device': device.name(), 'Register': register(0).name()}
            for slice in self.slices:
                header = f'Slice {slice}'
                if header not in headers:
                    headers.append(header)
                value = device.read_slice_register(register, slice).value
                entry.update({header: f'0x{value:08X}'})
            data.append(entry)

    def _add_alarm_entry(self, alarm_name, device, slice_values):
        entry = {'Device': device, 'Register': alarm_name}
        for index, reg in slice_values.items():
            header = f'Slice {index}'
            self._add_header(header)
            entry.update({header: f'0x{reg.value:08X}'})
        self.data.append(entry)

    def _get_alarms(self, slices):
        self.alarms.append(self.hbicc.pat_gen.get_alarm(slices, self.total_fails))
        self.alarms.append(self.hbicc.ring_multiplier.get_alarm(slices, self.total_fails))
        for pm in self.hbicc.get_pin_multipliers():
            self.alarms.append(pm.get_alarm(slices, self.total_fails))

    def set_alarms_as_non_critical(self, non_critical_alarms):
        self.critical_alarm_fields = self.critical_alarm_fields.difference(set(non_critical_alarms))

    def set_auto_abort_alarms_as_non_critical_at_prestage(self, non_critical_alarms):
        self.critical_alarms_causing_auto_abort = self.critical_alarms_causing_auto_abort.difference((non_critical_alarms))

    def is_alarm_set(self, alarm1, by_slice=False, expected_alarm_set_list=[]):
        verdict = []
        for register in [self.hbicc.pat_gen.registers.ALARMS1, self.hbicc.pat_gen.registers.ALARMS2]:
            for i in alarm1:
                if hasattr(register, i):
                    for slice in self.slices:
                        alarm_value = self.hbicc.pat_gen.read_slice_register(register, slice=slice)
                        is_alarm_set = getattr(alarm_value, i)
                        if is_alarm_set!=0:
                            verdict.append(is_alarm_set)
                        if by_slice:
                            if is_alarm_set != expected_alarm_set_list[slice]:
                                verdict.append(True)
                                self.Log('error', f'Slice {slice}: {alarm1} alarm observed: '
                                                  f'{is_alarm_set} not the same as '
                                                  f'expected {expected_alarm_set_list[slice]}')
        return verdict
