################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes


class DataStructure(ctypes.LittleEndianStructure):
    """Structure base class - defines setter/getter for bytes value of register"""

    def __init__(self, value=None):
        super().__init__()
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return bytes(self)

    @property
    def int_value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i, len(i))

    def log_console(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            l.append(f'{field_name:>20}:{field_value}')
        return '\n\t'.join(l)

    def name(self):
        return type(self).__name__.upper()


class BIG_HEADER(DataStructure):
    _fields_ = [('last_pcall_address', ctypes.c_uint64, 32),
                ('vector_address', ctypes.c_uint64, 32),
                ('pattern_cycle', ctypes.c_uint64, 32),
                ('repeat_count', ctypes.c_uint64, 32),
                ('selectable_field', ctypes.c_uint64, 32),
                ('user_cycle', ctypes.c_uint64, 32),
                ('channel_set_fail_bits', ctypes.c_uint64, 16),
                ('trace_index', ctypes.c_uint64, 24),
                ('pattern_counter', ctypes.c_uint64, 24),
                ]


class HEADER_AND_DDATA(DataStructure):
    _fields_ = [('error_masked_0_31', ctypes.c_uint64, 32),
                ('pin_state_0_31', ctypes.c_uint64, 32),
                ('error_masked_32_34', ctypes.c_uint64, 3),
                ('pin_state_32_34', ctypes.c_uint64, 3),
                ('is_valid', ctypes.c_uint64, 1),
                ('clock_cycle', ctypes.c_uint64, 48),
                ('alarm', ctypes.c_uint64, 1),
                ('burst_is_done', ctypes.c_uint64, 1),
                ('originating_slice', ctypes.c_uint64, 3),
                ('originating_chip', ctypes.c_uint64, 2),
                ('capture_word_type', ctypes.c_uint64, 2),
                ]

    def log_console(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            field_name = field[0]
            if 'error_mask' not in field_name or 'pin_state' not in field_name:
                field_value = getattr(self, field[0])
                l.append(f'{field_name:>20}:{field_value}')

        l.append(
            f'error_masked_0_34 : {bin(getattr(self, "error_masked_0_31"))} {bin(getattr(self, "error_masked_32_34"))}')
        l.append(f'pin_state_0_34 : {bin(getattr(self, "pin_state_0_31"))} {bin(getattr(self, "pin_state_32_34"))}')

        return '\n\t'.join(l)


class TRACE(DataStructure):
    _fields_ = [('source_address', ctypes.c_uint64, 32),
                ('destination_address', ctypes.c_uint64, 32),
                ('ctp_mask', ctypes.c_uint64, 38),
                ('cache_hit_miss', ctypes.c_uint64, 1),
                ('Reserved0', ctypes.c_uint64, 20),
                ('event_type', ctypes.c_uint64, 5),
                ('pattern_id', ctypes.c_uint64, 32),
                ('Reserved1', ctypes.c_uint64, 32),
                ('Reserved2', ctypes.c_uint64, 64),
                ]

    def log_console(self):
        l = []
        l.append(f'\n\t{type(self).__name__}')
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            if field_name in ['ctp_mask']:
                l.append(f'{field_name:>20}:0b{field_value:b}')
            else:
                l.append(f'{field_name:>20}:{field_value}')
        return '\n\t'.join(l)


class CTV_HEADER(DataStructure):
    _fields_ = [('last_pcall_address', ctypes.c_uint64, 32),  # 0
                ('vector_address', ctypes.c_uint64, 32),
                ('pattern_cycle', ctypes.c_uint64, 32),
                ('repeat_count', ctypes.c_uint64, 32),
                ('selectable_field', ctypes.c_uint64, 32),
                ('user_cycle', ctypes.c_uint64, 32),
                ('dut_serial_control', ctypes.c_uint64, 6),
                ('instruction_sync', ctypes.c_uint64, 1),
                ('invalid_pid', ctypes.c_uint64, 1),
                ('reserved', ctypes.c_uint64, 8),
                ('trace_index', ctypes.c_uint64, 24),
                ('pattern_counter', ctypes.c_uint64, 24)]  # 256

    def log_console(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            l.append(f'{field_name:>20}:{field_value}')
        return '\n\t\t\t'.join(l)


class CTV_DATA_HEADER(DataStructure):
    _fields_ = [
        ('block_count', ctypes.c_uint64, 6),
        ('relative_cycle_count', ctypes.c_uint64, 9),
        ('valid_cycles', ctypes.c_uint64, 32),
        ('entry_is_wide', ctypes.c_uint64, 8),
        ('end_of_burst', ctypes.c_uint64, 1),
        ('is_cycle_count', ctypes.c_uint64, 1),
        ('originating_slice', ctypes.c_uint64, 3),
        ('originating_chip', ctypes.c_uint64, 2),
        ('capture_word_type', ctypes.c_uint64, 2),
    ]

    def log_console(self):
        l = []
        l.append(f'\n\t{type(self).__name__}')
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            if field_name in ['ctp_mask', 'event_type']:
                l.append(f'{field_name:>20}: 0b{field_value:b}')
            elif field_name in ['entry_is_wide', 'valid_cycles']:
                l.append(f'{field_name:>20}: 0x{field_value:08X}')
            else:
                l.append(f'{field_name:>20}:{field_value}')
        return '\n\t'.join(l)


class CTV_DATA(DataStructure):
    _fields_ = [('packet', ctypes.c_uint64, 64),
                ]

    def log_console(self):
        l = []
        m = []
        l.append(f'\t{type(self).__name__}')
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            if field_name in ['ctp_mask', 'event_type']:
                l.append(f'{field_name:>20}: 0b{field_value:b}')
            else:
                temp = f'{field_value:016X}'
                temp1 = f'{temp[:8]}_{temp[8:]}'
                l.append(f'0x{temp1}')
                m.append(f'{field_name:20}')
        # m.reverse()
        l.reverse()
        return ' '.join(l)

    def convert_pin_state_to_decimal(self):
        x = f'{self.int_value:064b}'
        total = []
        for i in range(0, len(x), 8):
            total.append(x[i])

        total.reverse()
        value = ''.join(total)
        value = int(value, 2)
        value = value ^ 0xFF
        return value


class CTV_HEADER_AND_DATA(DataStructure):
    _fields_ = [
        ('packet3', ctypes.c_uint64, 64),
        ('packet2', ctypes.c_uint64, 64),
        ('packet1', ctypes.c_uint64, 64),
        ('block_count', ctypes.c_uint64, 6),
        ('relative_cycle_count', ctypes.c_uint64, 9),
        ('valid_cycles', ctypes.c_uint64, 32),
        ('entry_is_wide', ctypes.c_uint64, 8),
        ('end_of_burst', ctypes.c_uint64, 1),
        ('is_cycle_count', ctypes.c_uint64, 1),
        ('originating_slice', ctypes.c_uint64, 3),
        ('originating_chip', ctypes.c_uint64, 2),
        ('capture_word_type', ctypes.c_uint64, 2),
    ]

    def log_console(self):
        l = []
        l.append(f'\n\t{type(self).__name__}')
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            if field_name in ['ctp_mask', 'event_type']:
                l.append(f'{field_name:>20}: 0b{field_value:b}')
            elif field_name in ['entry_is_wide', 'valid_cycles', 'packet1', 'packet2', 'packet3']:
                l.append(f'{field_name:>20}: 0x{field_value:08X}')
            else:
                l.append(f'{field_name:>20}:{field_value}')
        return '\n\t'.join(l)

    def convert_pin_state_to_decimal(self, packet):
        final_value = 0
        for i in range(8):
            final_value = (((packet >> i * 8) & 0x1) << i) | final_value


class PIN_ERROR(DataStructure):
    _fields_ = [
                ('a_ch_set_e_mask_low_bin', ctypes.c_uint64, 32),
                ('a_ch_set_p_state_low_bin', ctypes.c_uint64, 32),
                ('a_ch_set_e_mask_up_bin', ctypes.c_uint64, 3),
                ('a_ch_set_p_state_up_bin', ctypes.c_uint64, 3),
                ('a_ch_set_is_valid', ctypes.c_uint64, 1),
                ('a_ch_set_clock_cycle', ctypes.c_uint64, 48),
                ('a_ch_set_alarm', ctypes.c_uint64, 1),
                ('a_ch_set_burst_is_done', ctypes.c_uint64, 1),
                ('a_ch_set_slice', ctypes.c_uint64, 3),
                ('a_ch_set_chip', ctypes.c_uint64, 2),
                ('a_ch_set_word_type', ctypes.c_uint64, 2),
                ('b_ch_set_e_mask_low_bin', ctypes.c_uint64, 32),
                ('b_ch_set_p_state_low_bin', ctypes.c_uint64, 32),
                ('b_ch_set_e_mask_up_bin', ctypes.c_uint64, 3),
                ('b_ch_set_p_state_up_bin', ctypes.c_uint64, 3),
                ('b_ch_set_is_valid', ctypes.c_uint64, 1),
                ('b_ch_set_clock_cycle', ctypes.c_uint64, 48),
                ('b_ch_set_alarm', ctypes.c_uint64, 1),
                ('b_ch_set_burst_is_done', ctypes.c_uint64, 1),
                ('b_ch_set_slice', ctypes.c_uint64, 3),
                ('b_ch_set_chip', ctypes.c_uint64, 2),
                ('b_ch_set_word_type', ctypes.c_uint64, 2),
                ('last_pcall_address', ctypes.c_uint64, 32),  # 0
                ('vector_address', ctypes.c_uint64, 32),
                ('pattern_cycle', ctypes.c_uint64, 32),
                ('repeat_count', ctypes.c_uint64, 32),
                ('selectable_field', ctypes.c_uint64, 32),
                ('user_cycle', ctypes.c_uint64, 32),
                ('dut_serial_control', ctypes.c_uint64, 6),
                ('instruction_sync', ctypes.c_uint64, 1),
                ('invalid_pid', ctypes.c_uint64, 1),
                ('reserved', ctypes.c_uint64, 8),
                ('trace_index', ctypes.c_uint64, 24),
                ('pattern_counter', ctypes.c_uint64, 24),
    ]

    def log_console(self):
        l = []
        l.append(f'\n\t{type(self).__name__}')
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            if field_name in ['ctp_mask', 'event_type', 'ch_set_0_error_masked_0_31', 'ch_set_1_error_masked_0_31',
                              'ch_set_0_error_masked_32_34', 'ch_set_0_error_masked_32_34', 'ch_set_0_pin_state_0_31',
                              'ch_set_1_pin_state_0_31', 'ch_set_1_pin_state_32_34', 'ch_set_0_pin_state_32_34',
                              'ch_set_0_pin_state_32_34', 'ch_set_1_pin_state_32_34']:
                l.append(f'{field_name:>20}: 0b{field_value:b}')
            elif 'bin' in field_name:
                l.append(f'{field_name:>20}: 0b{field_value:b}')
            elif field_name in ['entry_is_wide', 'valid_cycles', 'packet1', 'packet2', 'packet3']:
                l.append(f'{field_name:>20}: 0x{field_value:08X}')
            else:
                l.append(f'{field_name:>20}:{field_value}')
        return '\n\t'.join(l)

    def convert_pin_state_to_decimal(self, packet):
        final_value = 0
        for i in range(8):
            final_value = (((packet >> i * 8) & 0x1) << i) | final_value


class CYCLE_COUNT_REPORT(DataStructure):
    _fields_ = [
        ('reserved', ctypes.c_uint64, 64),
        ('reserved', ctypes.c_uint64, 64),
        ('reserved', ctypes.c_uint64, 19),
        ('block_count', ctypes.c_uint64, 6),
        ('relative_cycle_count', ctypes.c_uint64, 9),
        ('valid_cycles', ctypes.c_uint64, 32),
        ('entry_is_wide', ctypes.c_uint64, 8),
        ('end_of_burst', ctypes.c_uint64, 1),
        ('is_cycle_count', ctypes.c_uint64, 1),
        ('cycle_count', ctypes.c_uint64, 48),
        ('originating_chip', ctypes.c_uint64, 2),
        ('capture_word_type', ctypes.c_uint64, 2),
    ]

    def log_console(self):
        l = []
        l.append(f'\n\t{type(self).__name__}')
        for field in self._fields_:
            field_name = field[0]
            field_value = getattr(self, field[0])
            if field_name in ['ctp_mask', 'event_type']:
                l.append(f'{field_name:>20}: 0b{field_value:b}')
            elif field_name in ['entry_is_wide', 'valid_cycles', 'packet1', 'packet2', 'packet3']:
                l.append(f'{field_name:>20}: 0x{field_value:08X}')
            elif field_name in ['reserved']:
                pass
            else:
                l.append(f'{field_name:>20}:{field_value}')
        return '\n\t'.join(l)

    def convert_pin_state_to_decimal(self, packet):
        final_value = 0
        for i in range(8):
            final_value = (((packet >> i * 8) & 0x1) << i) | final_value

