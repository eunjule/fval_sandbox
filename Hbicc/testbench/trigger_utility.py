# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import ctypes
import random
import time
from enum import Enum

from Common import fval
from Common.register import Register


class TriggerType(Enum):
    ResumeTrigger = 0
    ResumeTriggerPropagate = 1
    DomainTrigger = 2
    DomainTriggerPropagate = 3
    RCTrigger = 11
    RCTriggerPropagate = 12


normal_trigger_list = [TriggerType.ResumeTrigger.value,
                       TriggerType.DomainTrigger.value,
                       TriggerType.RCTrigger.value]

flag_bit_trigger_map = {TriggerType.ResumeTrigger.value: 22,
                        TriggerType.ResumeTriggerPropagate.value: 22,
                        TriggerType.DomainTrigger.value: 23,
                        TriggerType.DomainTriggerPropagate.value: 23,
                        TriggerType.RCTrigger.value: 24,
                        TriggerType.RCTriggerPropagate.value: 24}

propagate_trigger_list = [TriggerType.ResumeTriggerPropagate.value,
                          TriggerType.DomainTriggerPropagate.value,
                          TriggerType.RCTriggerPropagate.value]


class TriggerWord(Register):
    _fields_ = [('trigger_type', ctypes.c_uint, 4),  # 0-3
                ('count_value', ctypes.c_uint, 4),  # 4-7
                ('target_domain', ctypes.c_uint, 4),  # 8-11
                ('reserved', ctypes.c_uint, 7),  # 12-18
                ('software_trigger_up_only', ctypes.c_uint, 1),  # 19
                ('dut_id', ctypes.c_uint, 6),  # 20-25
                ('target_resource', ctypes.c_uint, 6)]  # 26-31


class TriggerUtility(fval.Object):
    class MissedTriggerError(Exception):
        def __init__(self, message):
            Exception.__init__(self, message)

    def __init__(self, hbicc, slice_list, trigger_word_value=0x03f00000):
        self.hbicc = hbicc
        self.slice_list = slice_list
        trigger_word = TriggerWord(trigger_word_value)
        self.target_resource = trigger_word.target_resource
        self.dut_id = trigger_word.dut_id
        self.target_domain = trigger_word.target_domain
        self.count_value = trigger_word.count_value
        self.software_trigger_up_only = trigger_word.software_trigger_up_only
        self.trigger_type = trigger_word.trigger_type
        self.domain_master = 1
        self.master_slice_index = 0
        self.sender = None
        self.receiver = None
        self.sync_modifier = 0
        self.card_type_extended = 0
        self.clear_all_on_gse = 0
        self.errors = []
        self.expected_source = 0x10

        self.attribute_holder = {'target_resource': self.target_resource, 'target_domain': self.target_domain,
                                 'domain_master': self.domain_master, 'dut_id': self.dut_id,
                                 'sync_modifier': self.sync_modifier, 'card_type_extended': self.card_type_extended,
                                 'clear_all_on_gse': self.clear_all_on_gse}

    def construct_trigger_word(self):
        return self._construct_trigger_word_with_trigger_type(self.trigger_type)

    def configure_trigger_control_register(self):
        master = random.choice(self.slice_list)
        self.master_slice_index = master
        kwargs = self.attribute_holder
        for slice_id in self.slice_list:
            if slice_id == master:
                kwargs['domain_master'] = 1
            else:
                kwargs['domain_master'] = 0
            self.hbicc.pat_gen.set_up_trigger_control_register(slice_id, **kwargs)

    def verify_last_trigger_seen_value(self, expected_trigger_payload):
        actual_last_seen_trigger = self.get_last_seen_trigger_from_patgen()
        actual_last_seen_trigger_rc = self.hbicc.rc.read_trigger(16)
        if actual_last_seen_trigger != expected_trigger_payload:
            self.hbicc.Log('error', f'Trigger sent 0x{expected_trigger_payload:0x} does not match actual '
                                    f'0x{actual_last_seen_trigger:0x}')
        else:
            self.hbicc.Log('debug',
                           f'Trigger sent 0x{expected_trigger_payload:0x} matches actual '
                           f'0x{actual_last_seen_trigger:0x}')

    def verify_last_trigger_seen_except_cycle_count(self, expected_trigger_payload):
        actual_last_seen_trigger = self.get_last_seen_trigger_from_patgen()
        if self.mask_out_cycle_and_check_trigger_mismatch(actual_last_seen_trigger, expected_trigger_payload):
            self.hbicc.Log('error', f'Trigger sent 0x{expected_trigger_payload:08x} does not match actual '
                                    f'0x{actual_last_seen_trigger:08x}')
            return False
        else:
            self.hbicc.Log('debug',
                           f'Trigger sent 0x{expected_trigger_payload:0x} matches actual '
                           f'0x{actual_last_seen_trigger:0x}')
            return True

    def get_last_seen_trigger_from_patgen(self):
        actual_last_seen_trigger = self.hbicc.pat_gen.read_trigger()
        return actual_last_seen_trigger

    def mask_out_cycle_and_check_trigger_mismatch(self, actual_last_seen_trigger, expected_trigger_payload):
        actual_last_seen_trigger &= 0xffffff0f
        return actual_last_seen_trigger != expected_trigger_payload

    def is_sw_flag_set(self):
        failing_slice_list = []
        for slice_id in self.slice_list:
            if not self.hbicc.pat_gen.check_if_flag_set(slice_id, 'Software_Trigger'):
                failing_slice_list.append(slice_id)
        return failing_slice_list

    def restore_initial_trigger_control_register_settings(self):
        self.domain_master = 0
        self.attribute_holder.update({'domain_master': self.domain_master})
        kwargs = self.attribute_holder
        for slice_id in self.slice_list:
            self.hbicc.pat_gen.set_up_trigger_control_register(slice_id, **kwargs)

    def log_error(self, expected_trigger, observed_trigger, observed_source):
        if expected_trigger != observed_trigger:
            self.errors.append({'Error Type': 'Trigger Value', 'Expected': f'0x{expected_trigger:08X}',
                                'Observed': f'0x{observed_trigger:08X}'})
        if self.expected_source != observed_source:
            self.errors.append({'Error Type': 'Device ID', 'Expected': f'0x{self.expected_source:08X}',
                                'Observed': f'0x{observed_source:08X}'})

    def report_errors(self):
        if self.errors:
            table = self.hbicc.contruct_text_table(data=self.errors, title_in='Software Up Trigger')
            self.hbicc.Log('error', f'{table}')
        else:
            self.hbicc.Log('info', f'{self.sender.name()} to RC Sofware Up Trigger Passed.')

    def check_for_trigger_entry_in_rc_software_fifo(self, wait_loop=10000):
        for i in range(wait_loop):
            current_count = self.hbicc.rc.read_bar_register(self.hbicc.rc.registers.SW_TRIGGER_FIFO_COUNT).value
            if current_count >= 1:
                return True
        else:
            raise self.MissedTriggerError(f'Trigger not received from Patgen even after {wait_loop} loops')

    def setup_trigger_utility_up(self, trigger_type, dut_id):
        self.trigger_type = trigger_type
        self.target_resource = 0
        self.software_trigger_up_only = 1
        self.dut_id = dut_id
        self.errors = []
        self.sender = self.hbicc.pat_gen
        self.expected_source = 0x10
        self.hbicc.rc.clear_sw_trigger_fifo()
        self.attribute_holder.update(
            {'target_resource': self.target_resource, 'dut_id': self.dut_id})
        self.configure_trigger_control_register()

    def setup_trigger_utility_down(self, trigger_type, dut_id):
        self.trigger_type = trigger_type
        self.target_resource = 0
        self.target_domain = 1
        self.dut_id = dut_id
        self.sender = self.hbicc.rc
        self.receiver = self.hbicc.pat_gen
        self.attribute_holder.update({'target_resource': self.target_resource,
                                      'target_domain': self.target_domain,
                                      'dut_id': self.dut_id})
        self.configure_trigger_control_register()

    def rc_trigger_check(self):
        payload = self.construct_trigger_word()
        trigger_match = False
        start_time = time.perf_counter()
        rctc_trigger_wait_time = 10
        counter = 0
        while 1:
            wait_time = time.perf_counter() - start_time
            try:
                self.check_for_trigger_entry_in_rc_software_fifo()
                observed_source = self.hbicc.rc.read_sw_trigger_fifo_source()
                observed_trigger = self.hbicc.rc.read_sw_trigger_from_rc()
                self.log_error(payload, observed_trigger, observed_source)
                self.report_errors()
            except:
                self.Log('info', f'Trigger not received from Patgen for {wait_time}s')
            else:
                trigger_match = True
                self.Log('info', 'RCTC_Trigger_Find')
                break
            if wait_time > rctc_trigger_wait_time and counter > rctc_trigger_wait_time:
                break
            counter += 1
        return trigger_match

    def software_keep_alive_resume(self):
        payload = self.construct_trigger_word()
        self.hbicc.rc.send_trigger_from_sender_to_receiver(self.sender, 16, payload, self.receiver)
        expected_trigger = self.get_propagate_trigger()
        self.hbicc.rc.verify_trigger_received(16, self.receiver, expected_trigger)

    def get_propagate_trigger(self):
        if self.trigger_type in normal_trigger_list:
            return self._construct_trigger_word_with_trigger_type(self.trigger_type + 1)
        else:
            return self._construct_trigger_word_with_trigger_type(self.trigger_type)

    def _construct_trigger_word_with_trigger_type(self, trigger_type):
        payload = (self.target_resource << 26 | self.dut_id << 20 | self.software_trigger_up_only << 19 |
                   self.target_domain << 8 | self.count_value << 4 | trigger_type)
        return payload

