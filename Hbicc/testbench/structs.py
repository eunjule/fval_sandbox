################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes

from Hpcc.hpcctb.structs import HpccStruct

class HbiStruct(HpccStruct):
    @classmethod
    def from_bytes(cls, data):
        return cls(value=int.from_bytes(data, byteorder="little"))
    
    def __str__(self):
        l = []
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class Instruction(HbiStruct):
    _pack_ = 1
    _fields_ = [('immediate',                   ctypes.c_uint64, 32),  # 31:0
                ('a',                           ctypes.c_uint64,  5),  # 36:32
                ('b',                           ctypes.c_uint64,  5),  # 41:37
                ('destination',                 ctypes.c_uint64,  5),  # 46:42
                ('branch_type',                 ctypes.c_uint64,  5),  # 51:47
                ('branch_base',                 ctypes.c_uint64,  2),  # 53:52
                ('condition_type',              ctypes.c_uint64,  4),  # 57:54
                ('invert_condition',            ctypes.c_uint64,  1),  # 58
                # op code has to be split because it crosses a 64-bit boundary
                ('op_code_4_0',                 ctypes.c_uint64,  5),  # 63:59
                ('op_code_6_5',                 ctypes.c_uint64,  2),  # 65:64
                ('op_type',                     ctypes.c_uint64,  4),  # 69:66
                ('compressed_page_branch',      ctypes.c_uint64,  1),  # 70
                ('compressed_page_cycle_count', ctypes.c_uint64, 32),  # 102:71
                ('reserved',                    ctypes.c_uint64,  5),  # 107:103
                ('sw_reserved',                 ctypes.c_uint64, 16),  # 123:108
                ('type',                        ctypes.c_uint64,  4)]  # 127:124
                
    
class PinStateWord(HbiStruct):
    _fields_ = [('channel_data_20_0',  ctypes.c_uint64, 63),
                ('reserved',           ctypes.c_uint64,  1),
                ('channel_data_37_21', ctypes.c_uint64, 51),
                ('pin_state_control',  ctypes.c_uint64,  6),
                ('pin_state_type',     ctypes.c_uint64,  3),
                ('pattern_word_type',  ctypes.c_uint64,  4)]

 
class PatternWord(HbiStruct):
    _fields_ = [('channel_data_20_0',  ctypes.c_uint64, 63),
                ('reserved',           ctypes.c_uint64,  1),
                ('channel_data_37_21', ctypes.c_uint64, 51),
                ('local_repeat',       ctypes.c_uint64,  5),
                ('mtv',                ctypes.c_uint64,  5), 
                ('ctv',                ctypes.c_uint64,  1), 
                ('reserved2',          ctypes.c_uint64,  1), 
                ('pattern_word_type',  ctypes.c_uint64,  1)] 

 
class MetadataWord(HbiStruct):
    _fields_ = [('data_63_0',          ctypes.c_uint64, 64),
                ('data_123_64',        ctypes.c_uint64, 60),
                ('pattern_word_type',  ctypes.c_uint64,  4)]


