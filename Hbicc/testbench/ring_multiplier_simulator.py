# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import array

from ThirdParty.I2L.Ltc2975 import Ltc2975Commands

from Common import fval
from Hbicc.instrument import ring_multiplier_register

MAX_NUMBER_OF_MEMORY_ELEMENTS = 500
TOTAL_NUMBER_OF_DUT_IDS = 64
ALL_DUT_IDS = range(TOTAL_NUMBER_OF_DUT_IDS)
BROADCAST_DUT_ID = 63
TQ_END_COMMAND_RECORD_PSDB0 = 0x8001000000000000
TQ_END_COMMAND_RECORD_PSDB1 = 0xa001000000000000
LTM4678_VOUT_COMMAND = 0x21
LTM4678_VOUT_READ_COMMAND = 0x8B
DUT_POWER_STATE_MASK_WR_BASE_ADDRESS =0x348
DUT_POWER_STATE_RD_31_0 = 0x340
DUT_POWER_STATE_RD_63_32 = 0x344
DUT_POWER_STATE_MASK_WR_BITS = 16
TQ_ENGINE_BASE = 0x640
REGISTER_SIZE_IN_BYTE = 4

class RingMultiplier(fval.Object):
    def __init__(self, system):
        self.system = system
        self.slices = [RingMultiplierSlice(slice=i, system=system) for i in range(5)]
        self.registers = {0x20: 0x0001_0000}
        self.memory_table = {}
        self.ad5684r = Ad5684r()
        self.ltm4678 = Ltm4678()
        self.ltc2975 = Ltc2975()
        self.broadcast_trigger = False
    
    def connect_psdb(self, psdb_list):
        for slice in self.slices:
            slice.connect_psdb(psdb_list)

    def bar_write(self, bar, offset, data):
        if bar == 0:
            self.register_write(offset, data)
        else:
            self.slices[bar-1].register_write(offset, data)
        
    def register_write(self, offset, data):
        self.registers[offset] = data

    def bar_read(self, bar, offset):
        if bar == 0:
            return self.register_read(offset)
        else:
            return self.slices[bar-1].register_read(offset)

    def register_read(self, offset):
        return self.registers.get(offset, 0)

    def dma_read(self, offset, length):
        return self.memory_table[offset]

    def dma_write(self, offset, data):
        if offset in self.memory_table:
            del self.memory_table[offset]
        self.memory_table[offset] = data
        self._remove_stale_ram_data()

    def _remove_stale_ram_data(self):
        """Avoid using lots of RAM for simulation, becomes slow otherwise"""
        if len(self.memory_table.keys()) >= MAX_NUMBER_OF_MEMORY_ELEMENTS:
            oldest_key = list(self.memory_table.keys())[0]
            del self.memory_table[oldest_key]

    def trigger(self, data):
        self.slices[0].trigger(data)
        aurora_link_status = self.is_aurora_link_enable()
        if aurora_link_status:
            self.process_trigger(data)

    def is_aurora_link_enable(self):
        aurora_enable_status = self.slices[0].registers[0x24c]
        return aurora_enable_status == 2

    def process_trigger(self, trigger):
        target_dut_id = (trigger & 0x3f00000) >> 20
        if self.is_dps_fold_trigger(trigger):
            self.broadcast_trigger = False
            index = self.get_dps_fold_trigger_index()
            trigger_translate = (trigger & 0xfffffc00) + index
            if target_dut_id in self.active_dut_list():
                byte_offset = self.get_byte_offset_from_trigger_for_dut(target_dut_id, trigger_translate)
                self.process_tq(byte_offset)
                self.increase_single_tq_engine_count_with_dut_id(target_dut_id)
            self.inactive_dut(target_dut_id)
        elif self._is_broadcast_id(target_dut_id):
            self.broadcast_trigger = True
            for dut_id in self.active_dut_list():
                byte_offset = self.get_byte_offset_from_trigger_for_dut(dut_id, trigger)
                self.process_tq(byte_offset)
                self.increase_single_tq_engine_count_with_dut_id(dut_id)
        else:
            self.broadcast_trigger = False
            if target_dut_id in self.active_dut_list():
                byte_offset = self.get_byte_offset_from_trigger_for_dut(target_dut_id, trigger)
                self.process_tq(byte_offset)
                self.increase_single_tq_engine_count_with_dut_id(target_dut_id)

    def is_dps_fold_trigger(self, trigger):
        card_type = (trigger & 0xfc000000) >> 26
        trigger_low_16_bit = (trigger & 0x0000ffff)
        if (card_type == 1) & (trigger_low_16_bit == 0x8002):
            return True
        else:
            return False

    def increase_single_tq_engine_count_with_dut_id(self, dut_id):
        if dut_id < 16:
            self.increase_single_tq_engine_count_with_engine_id(dut_id)
        else:
            self.increase_single_tq_engine_count_with_engine_id(16)

    def increase_single_tq_engine_count_with_engine_id(self, engine_id):
        offset = TQ_ENGINE_BASE + 4*engine_id
        self.slices[0].registers[offset] = \
            self.slices[0].registers.get(offset, 0) + 1

    def inactive_dut(self, target_dut_id):
        power_state31_0 = self.slices[0].registers.get(DUT_POWER_STATE_RD_31_0, 0)
        power_state63_32 = self.slices[0].registers.get(DUT_POWER_STATE_RD_63_32, 0)
        power_state63_0 = (power_state63_32 << 32) | power_state31_0
        power_state63_0_update = power_state63_0 | (1 << target_dut_id)
        power_state31_0_update = power_state63_0_update & 0xffffffff
        power_state63_32_update = (power_state63_0_update >> 32)
        self.slices[0].registers[DUT_POWER_STATE_RD_31_0] = power_state31_0_update
        self.slices[0].registers[DUT_POWER_STATE_RD_63_32] = power_state63_32_update

    def active_dut_list(self):
        power_state31_0 = self.slices[0].registers.get(DUT_POWER_STATE_RD_31_0, 0)
        power_state63_32 = self.slices[0].registers.get(DUT_POWER_STATE_RD_63_32, 0)
        power_state63_0 = (power_state63_32 << 32) | power_state31_0
        active_dut_list = []
        for i in range(TOTAL_NUMBER_OF_DUT_IDS):
            if ((power_state63_0 >> i) & 1) == 0:
                active_dut_list.append(i)
        return active_dut_list

    def get_dps_fold_trigger_index(self):
        DPS_FOLD_TRIGGER_INDEX = 0x25C
        return self.slices[0].registers.get(DPS_FOLD_TRIGGER_INDEX, 0)

    def _is_broadcast_id(self, dut_id):
        return dut_id == BROADCAST_DUT_ID

    def process_tq(self, byte_offset):
        tq_records = self._find_tq_records(byte_offset)
        self.process_tq_records(tq_records)

    def _find_tq_records(self, byte_offset):
        tq_memory_address = self.get_tq_memory_address_from_lut(byte_offset)
        _, tq_block_data = self.get_memory_block(tq_memory_address)
        tq_records = self.get_tq_records(tq_block_data)
        return tq_records

    def process_tq_records(self, tq_records):
        for tq_record in tq_records:
            self.slices[0].handle_trigger(tq_record)
            if self.is_end_tq_record_reached(tq_record):
                self.update_tq_processed_count()
                break

    def is_end_tq_record_reached(self, tq_record):
        if tq_record in [TQ_END_COMMAND_RECORD_PSDB0, TQ_END_COMMAND_RECORD_PSDB1]:
            return True
        else:
            return False

    def is_all_records_processed(self, total_number_of_records, tq_record_processed_counter):
        return total_number_of_records == tq_record_processed_counter

    def update_tq_processed_count(self):
        self.slices[0].registers[ring_multiplier_register.DC_TRIGGER_PAGE_PROCESSED_QUEUES.ADDR] = \
            self.slices[0].registers.get(ring_multiplier_register.DC_TRIGGER_PAGE_PROCESSED_QUEUES.ADDR, 0) + 1

    def get_tq_memory_address_from_lut(self, byte_offset):
        _, block_data_index = self.get_memory_block(byte_offset)
        tq_memory_address = self.convert_byte_to_int(block_data_index)
        return tq_memory_address

    def convert_byte_to_int(self, block_data_index):
        actual_address = int.from_bytes(block_data_index, 'little')
        return actual_address

    def get_byte_offset_from_trigger_for_dut(self, dut_id, data):
        index_of_tq_to_fire = data & 0xfff
        byte_offset = (index_of_tq_to_fire + (dut_id << 20)) << 3
        return byte_offset

    def get_tq_records(self, block_data):
        tq_records = array.array('Q')
        tq_records.frombytes(block_data)
        return tq_records

    def get_memory_block(self, offset):
        for block_offset in reversed(list(self.memory_table.keys())):
            block_data = self.memory_table[block_offset]
            if block_offset <= offset < (block_offset + len(block_data)):
                return block_offset, block_data
        else:
            self.memory_table[offset] = bytearray(1024)
            return offset, self.memory_table[offset]

    def fpga_version(self):
        return self.registers[0x20]

    def fpga_version_string(self):
        version = self.fpga_version()
        return f'{version >> 16}.{version & 0xFFFF}'

    def receive_sync_pulse(self):
        pass


class RingMultiplierSlice(fval.Object):
    def __init__(self, slice, system):
        self.system = system
        self.slice = slice
        self.dps_fold_trigger_index = 0
        self.registers = {0x30: 0x1E80,
                          0x44: 0x27,
                          0x8C: 10,
                          0x90: 10,
                          0x100: 10,
                          0x104: 10,
                          0x120: 10,
                          0x124: 10,
                          0x200: 5,
                          0x480: 0xCAFEF00D,
                          0x4C0: 0xCAFEF00D}
        self.write_actions = {ring_multiplier_register.DC_TRIGGER_QUEUE_RECORD_HIGH.ADDR: self.trigger_queue,
                              0x50: self.manual_trigger_up,
                              0x224: self.pattern_control,
                              0x25C: self.fold_trigger_index,
                              0x348: self.write_dut_power_state,
                              0x34C: self.write_dut_power_state,
                              0x350: self.write_dut_power_state,
                              0x354: self.write_dut_power_state}
        self.read_actions = {}
        self.tq_command_actions = {0: self.tq_start,
                                   1: self.tq_end,
                                   2: self.tq_delay,
                                   4: self.tq_delay}
        self.tq_data_actions = {0: self.psdb_spi_vref,
                                5: self.ltc2975_trigger}
        for i in range(1,5):
            self.tq_data_actions[i] = self.ltm4678_trigger
        for i in range(6,16):
            self.tq_data_actions[i] = self.slice_trigger
            
        self.setup_clocks()
        self._setup_transceivers()
        
    def connect_psdb(self, psdb_list):
        self.psdb = psdb_list

    def register_write(self, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def read_dut_power_state(self):
        power_state31_0 = self.registers[DUT_POWER_STATE_RD_31_0]
        power_state63_32 = self.registers[DUT_POWER_STATE_RD_63_32]
        power_state63_0 = (power_state63_32 << 32) | power_state31_0
        dut_power_state_list = []
        for i in range(TOTAL_NUMBER_OF_DUT_IDS):
            dut_power_state_list.append(power_state63_0 & 0b1)
            power_state63_0 = power_state63_0 >> 1
        return dut_power_state_list

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def fold_trigger_index(self, offset, data):
        self.dps_fold_trigger_index = data
        self.registers[offset] = data

    def write_dut_power_state(self, offset, data):
        power_state31_0, power_state63_32 = self.update_power_active_dut(
            offset, data)
        self.registers[DUT_POWER_STATE_RD_31_0] = power_state31_0
        self.registers[DUT_POWER_STATE_RD_63_32] = power_state63_32

    def update_power_active_dut(self, address, data):
        power_state31_0 = self.registers.get(DUT_POWER_STATE_RD_31_0, 0)
        power_state63_32 = self.registers.get(DUT_POWER_STATE_RD_63_32, 0)
        power_state63_0 = (power_state63_32 << 32) | power_state31_0
        power_state63_0 = self.decode_power_active_dut(power_state63_0, address, data)
        return (power_state63_0 & 0xFFFFFFFF), ((power_state63_0 >> 32) & 0xFFFFFFFF)

    def decode_power_active_dut(self, power_state63_0, address, data):
        register_position = (address - DUT_POWER_STATE_MASK_WR_BASE_ADDRESS) // REGISTER_SIZE_IN_BYTE
        power_dut_bit_position = DUT_POWER_STATE_MASK_WR_BITS * register_position
        mask = data & DUT_POWER_STATE_MASK_WR_BITS
        state = data >> DUT_POWER_STATE_MASK_WR_BITS
        return self.calculate_power_active_dut_bit(power_state63_0, power_dut_bit_position, mask, state)

    def calculate_power_active_dut_bit(self, power_state63_0, power_dut_bit_position, mask, state):
        for i in range(16):
            bit_position = i + power_dut_bit_position
            bit_mask = (mask >> i) & 1
            bit_state = (state >> i) & 1
            if (bit_mask == 0) & (bit_state == 0):
                power_state63_0 = power_state63_0 & (0xFFFFFFFFFFFFFFFF - (1 << bit_position))
            elif (bit_mask == 0) & (bit_state == 1):
                power_state63_0 = power_state63_0 | (1 << bit_position)
        return power_state63_0

    def register_read(self, offset):
        return self.read_actions.get(offset, self.default_register_read)(offset)

    def default_register_read(self, offset):
        return self.registers.get(offset, 0)
    
    def setup_clocks(self):
        self.registers[0xA0] = 0x01019020
        self.registers[0xA4] = 0x01019010
        self.registers[0xA8] = 0x01019020
        self.registers[0xAC] = 0x0101902A
        self.registers[0xB0] = 0x01034005

    def _setup_transceivers(self):
        self.registers[0x084] = 0x3E9  # PatGen ring bus status
        self.registers[0x0C4] = 0x3E9  # PSDB0 ring bus status
        self.registers[0x0D0] = 0x3E9  # PSDB1 ring bus status
        self.registers[0x22C] = 0x027  # PSDB0 aurora pattern status
        self.registers[0x238] = 0x027  # PSDB1 aurora pattern status

    def trigger_queue(self, offset, data):
        self.registers[offset] = data
        concatenated_data = (self.registers[0xE4] << 32) | self.registers[0xE0]
        self.handle_trigger(concatenated_data)

    def handle_trigger(self, tq_record):
        if tq_record & 0x8000000000000000:
            self.do_command_action(tq_record)
        else:
            self.do_data_action(tq_record)

    def do_data_action(self, tq_record):
        target = (tq_record >> 48) & 0xF
        self.tq_data_actions[target](tq_record)

    def do_command_action(self, tq_record):
        command = (tq_record >> 48) & 0xF
        self.tq_command_actions[command](tq_record)

    def tq_start(self,data):
        pass
    
    def tq_end(self,data):
        pass
    
    def tq_delay(self,data):
        pass
    
    def pattern_control(self, offset, data):
        self.registers[offset] = data
        if data & 0x00000001:
            self.system.patgen.run_pattern()

    def psdb_spi_vref(self, tq_record_data):
        payload = tq_record_data & 0xffffffff
        psdb = (tq_record_data >> 61) & 0x1
        ldacn   = (tq_record_data >> 55) & 0x7
        self.registers[0xF4 + 4 * psdb] = self.system.ring_multiplier.ad5684r.spi_write_read(payload, ldacn) << 8

    def ltm4678_trigger(self, tq_record_data):
        write = bool(tq_record_data & 0x4000000000000000)
        payload = tq_record_data & 0xffffffff
        if write:
            self.system.ring_multiplier.ltm4678.pmbus_write(payload)
        else:
            self.registers[0xE8] = self.system.ring_multiplier.ltm4678.pmbus_read(payload)

    def ltc2975_trigger(self, tq_record_data):
        write = (tq_record_data >> 62) == 1
        payload = tq_record_data & 0xffffffff
        if write:
            self.system.ring_multiplier.ltc2975.pmbus_write(payload)
        else:
            self.registers[0xE8] = self.system.ring_multiplier.ltc2975.pmbus_read(payload)
    
    def slice_trigger(self, tq_record_data):
        psdb_index = (tq_record_data >> 61) & 0x1
        self.psdb[psdb_index].dc_trigger(tq_record_data)

    def manual_trigger_up(self, offset, data):
        self.system.rc.registers[0x32C] = self.registers[0x50] = data

    def trigger(self, data):
        self.registers[0x4C] = data


class Ad5684r(fval.Object):
    DAC_RESOLUTION = 4096
    VREF = 2.5
    
    def __init__(self):
        self._input_register = {x: 0 for x in range(4)}
        self._dac_value = {x:0 for x in range(4)}
        self._readback_register = 0
        self._command_table = {0b0000: self._no_operation,
		                       0b0001: self._write_to_input_register,
                               0b0010: self._update_dac_register,
                               0b0011: self._write_and_update_dac_channel_n,
                               0b1001: self._set_up_readback_register}

    def spi_write_read(self, input, ldacn):
        command = (input >> 20) & 0xF
        self._command_table[command](input, ldacn)
        return self._readback_register << 4
    
    def voltage(self, channel):
        return (self._dac_value[channel] / Ad5684r.DAC_RESOLUTION) * Ad5684r.VREF
    
    def _no_operation(self, input, ldacn):
        pass

    def _write_to_input_register(self, input, ldacn):
        for dac_address in self._get_addresses(input):
            self._input_register[dac_address] = (input >> 4) & 0xFFF
            if not(ldacn >> 2 & 0x1) or not(ldacn >> 1 & 0x1):
                self._dac_value[dac_address] = self._input_register[dac_address]

    def _update_dac_register(self, input, ldacn):
        for dac_address in self._get_addresses(input):
            self._dac_value[dac_address] = self._input_register[dac_address]  
  
    def _write_and_update_dac_channel_n(self, input, ldacn):
        for dac_address in self._get_addresses(input):
            self._dac_value[dac_address] = (input >> 4) & 0xFFF
    
    def _set_up_readback_register(self, input, ldacn):
        self._readback_register = self._dac_value[self._get_readback_address(input)]
    
    def _get_readback_address(self, input):
        addresses = self._get_addresses(input)
        if len(addresses) != 1:
            self.Log('error', f'Simulator: AD5684r readback must address exactly one DAC (command = 0x{input:06X})')
        return addresses[0]
    
    def _get_addresses(self, input):
        return [x for x in range(4) if input >> (16 + x) & 0x1]


class Ltm4678(fval.Object):
    def __init__(self):
        self.registers = {0x19: 0x00B0,
                          0x8B: 0x1C00, # always reading 1.8V for now
                          0x98: 0x0022}

    def voltage(self):
        vccio = self.registers[LTM4678_VOUT_READ_COMMAND] / (2 ** 12)
        return vccio

    def pmbus_write(self, pmbus_data):
        offset = pmbus_data & 0xFF
        data = (pmbus_data >> 8) & 0xFFFF
        self.registers[offset] = data
        if offset == LTM4678_VOUT_COMMAND:
            self.registers[LTM4678_VOUT_READ_COMMAND] = data

    def pmbus_read(self, pmbus_data):
        offset = pmbus_data & 0xFF
        return_data = self.registers.get(offset, 0)
        return return_data


class Ltc2975(fval.Object):
    def __init__(self):
        self.registers = {0x98: 0x11,
                          0x19: 0xB0}
        self.write_actions = {Ltc2975Commands.VOUT_COMMAND.value: self.vout_write_action}

    def pmbus_write(self, pmbus_data):
        offset = pmbus_data & 0xFF
        data = (pmbus_data >> 8) & 0xFFFF
        self.write_actions.get(offset, self.default_write_action)(data, offset)

    def default_write_action(self, data, offset):
        self.registers[offset] = data

    def vout_write_action(self, data, offset):
        self.default_write_action(data, offset)
        self.registers[Ltc2975Commands.READ_VOUT.value] = data

    def pmbus_read(self, pmbus_data):
        offset = pmbus_data & 0xFF
        return self.registers[offset]

