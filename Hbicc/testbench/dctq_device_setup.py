# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


import time
from time import perf_counter
import random

MAX_ERROR_COUNT = 10
SETTLING_TIME = 0.01
TQ_RECORD_LENGTH_BITS = 64
NUMBER_OF_BITS_IN_A_BYTE = 8
NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK = 100000
WAIT_BETWEEN_RETRIES_IN_SECONDS = 0.00002
WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER = 0.005
NUMBER_OF_MICROSECONDS_IN_A_SECOND = 1000000
TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS = 400
TQ_DELAY_ABSOLUTE_PERCENTAGE_ERROR_THRESHOLD = 2
TRIGGER_SEND_DELAY_MICROSECONDS = 50
MAX_NON_BROADCAST_DUT_ID = 62
TQ_BIRM_START_DUT_ID = 16
TQ_BIRM_END_DUT_ID = 63
NUMBER_OF_NON_BROADCAST_DUT_IDS = 63
NUMBER_OF_BROADCAST_DUT_IDS = 1
TOTAL_NUMBER_OF_DUT_IDS = NUMBER_OF_NON_BROADCAST_DUT_IDS + NUMBER_OF_BROADCAST_DUT_IDS
ALL_DUT_IDS = range(TOTAL_NUMBER_OF_DUT_IDS)
BROADCAST_DUT_ID = 63
NUMBER_OF_BITS_FOR_TQ_INDEX = 12

class TQTestScenario:

    def __init__(self, hbicc, Object):
        self.scenario_functions = {'Read_Version_Via_DDR': self.generate_version_read_tq_via_DDR,
                                   'Read_Capability_Via_DDR': self.generate_capability_read_tq_via_DDR,
                                   'Write_Read_Scratch_Via_DDR': self.generate_scratch_write_read_tq_via_DDR,
                                   'Random_Delay_Via_TQ_DDR': self.generate_random_delay_tq_via_DDR}
        self.tq_post_process_actions = {'Random_Delay_Via_TQ_DDR': self.delay_tq_post_process_action,
                                        'Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Processed_Count_Check':
                                            self.broadcast_delay_tq_processed_count_check_post_process_action,
                                        'Broadcast_Trigger_Via_TQ_DDR_Scratch_Write_Check':
                                            self.broadcast_scratch_write_check_post_process_action,
                                        'Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Some_DUTs_Processed_Count_Check':
                                            self.broadcast_delay_tq_processed_count_check_post_process_action}
        self.execute_scenario_actions = {'Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Processed_Count_Check':
                                             self.broadcast_execute_scenario_for_device_delay_tq_processed_count_check,
                                         'Broadcast_Trigger_Via_TQ_DDR_Scratch_Write_Check':
                                             self.broadcast_execute_scenario_for_device_scratch_write_check,
                                         'Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Some_DUTs_Processed_Count_Check':
                                             self.broadcast_execute_scenario_delay_tq_some_duts_processed_count_check}
        self.psdbTarget = 0
        self.noOfTestIterations = 0
        self.allScenarios = []
        self.scenarioName = ''
        self.deviceId = 0
        self.hbicc = hbicc
        # self.testClass = test_case
        self.rm_device = hbicc.ring_multiplier
        self.rctc = self.hbicc.rc
        self.Log = Object.Log
        self.tqProcessStartTimeMicroseconds = 0
        self.tqProcessEndTimeMicroseconds = 0
        self.tqProcessedCount = 0
        self.currentIteration = 0
        self.numberOfTQsExecutionExpected = 0
        self.expectedDataList = list()
        self.observedDataList = list()
        self.errorList = list()

    def set_psdb_target(self, psdb_target):
        self.psdbTarget = psdb_target

    def get_psdb_target(self):
        return self.psdbTarget

    def set_no_of_test_iterations(self, no_of_test_iterations):
        self.noOfTestIterations = no_of_test_iterations

    def get_no_of_test_iterations(self):
        return self.noOfTestIterations

    def set_all_scenarios_list(self, all_scenarios):
        self.allScenarios = all_scenarios

    def get_all_scenarios_list(self):
        return self.allScenarios

    def set_scenario_name(self, scenario_name):
        self.scenarioName = scenario_name

    def get_scenario_name(self):
        return self.scenarioName

    def set_device_id(self, device_id):
        self.deviceId = device_id

    def get_device_id(self):
        return self.deviceId

    def add_to_expected_data(self, expected_data):
        self.expectedDataList.append(expected_data)

    def get_all_expected_data(self):
        return self.expectedDataList

    def add_to_observed_data(self, observed_data):
        self.observedDataList.append(observed_data)

    def get_all_observed_data(self):
        return self.observedDataList

    def set_tq_process_start_time_microseconds(self, tq_process_start_time):
        self.tqProcessStartTimeMicroseconds = tq_process_start_time

    def get_tq_process_start_time_microseconds(self):
        return self.tqProcessStartTimeMicroseconds

    def set_tq_process_end_time_microseconds(self, tq_process_start_time):
        self.tqProcessEndTimeMicroseconds = tq_process_start_time

    def get_tq_process_end_time_microseconds(self):
        return self.tqProcessEndTimeMicroseconds

    def set_tq_processed_count(self, tq_processed_count):
        self.tqProcessedCount = tq_processed_count

    def get_tq_processed_count(self):
        return self.tqProcessedCount

    def execute(self):
        self.errorList = list()
        for test_iteration in range(self.get_no_of_test_iterations()):
            self.currentIteration = test_iteration
            device_list = self.get_pmbus_device_list_randomized()
            for device in device_list:
                self.set_device_id(device)
                for scenario in self.get_all_scenarios_list():
                    self.expectedDataList = list()
                    self.observedDataList = list()
                    self.set_scenario_name(scenario)
                    self.execute_scenario_actions.get(scenario, self.default_execute_scenario_for_device)()
                    if len(self.errorList) >= MAX_ERROR_COUNT:
                        return

    def broadcast_execute_scenario_for_device_delay_tq_processed_count_check(self):
        random_index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        self.prepare_blank_delay_tqs_all_duts_for_broadcast_trigger_exec(random_index_of_tq_to_fire)
        trigger_register_data = self.generate_aurora_trigger_for_dut(BROADCAST_DUT_ID, random_index_of_tq_to_fire)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def broadcast_execute_scenario_delay_tq_some_duts_processed_count_check(self):
        random_index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        self.prepare_blank_delay_tqs_some_duts_for_broadcast_trigger_exec(random_index_of_tq_to_fire)
        trigger_register_data = self.generate_aurora_trigger_for_dut(BROADCAST_DUT_ID, random_index_of_tq_to_fire)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def broadcast_execute_scenario_for_device_scratch_write_check(self):
        random_index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        self.prepare_scratch_write_and_blank_delay_tqs_for_broadcast_trigger_exec(random_index_of_tq_to_fire)
        trigger_register_data = self.generate_aurora_trigger_for_dut(BROADCAST_DUT_ID, random_index_of_tq_to_fire)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def prepare_blank_delay_tqs_all_duts_for_broadcast_trigger_exec(self, random_index_of_tq_to_fire):
        self.set_up_blank_delay_tqs_for_active_dut_ids_at_byte_offset(ALL_DUT_IDS, random_index_of_tq_to_fire)
        self.numberOfTQsExecutionExpected = TOTAL_NUMBER_OF_DUT_IDS

    def prepare_blank_delay_tqs_some_duts_for_broadcast_trigger_exec(self, random_index_of_tq_to_fire):
        active_duts, inactive_duts = self.select_random_active_and_inactive_duts()
        self.set_up_blank_delay_tqs_for_active_dut_ids_at_byte_offset(active_duts, random_index_of_tq_to_fire)
        self.set_up_blank_memory_for_inactive_dut_ids_at_byte_offset(inactive_duts, random_index_of_tq_to_fire)
        self.numberOfTQsExecutionExpected = len(active_duts)

    def set_up_blank_memory_for_inactive_dut_ids_at_byte_offset(self, inactive_dut_ids, random_index_of_tq_to_fire):
        for dut_id in inactive_dut_ids:
            lut_address = self.calculate_lut_address(random_index_of_tq_to_fire, dut_id)
            self.save_blank_data_in_memory(lut_address)

    def set_up_blank_delay_tqs_for_active_dut_ids_at_byte_offset(self, active_duts, random_index_of_tq_to_fire):
        for dut_id in active_duts:
            self.set_up_tq_for_blank_delay(dut_id, random_index_of_tq_to_fire)
        no_of_trigger_queues_to_be_processed_on_broadcast_trigger_receipt = len(active_duts)
        self.add_to_expected_data(no_of_trigger_queues_to_be_processed_on_broadcast_trigger_receipt)

    def prepare_scratch_write_and_blank_delay_tqs_for_broadcast_trigger_exec(self, random_index_of_tq_to_fire):
        random_dut_ids_for_scratch_write_tq, other_dut_ids_for_blank_delay_tq = \
            self.get_random_dut_ids_for_scratch_write_and_blank_delay_tqs()
        dut_id_for_user_data_03_write = random_dut_ids_for_scratch_write_tq[0]
        self.set_up_tq_for_scratch_write('USER_DATA_03', dut_id_for_user_data_03_write, random_index_of_tq_to_fire)
        dut_id_for_user_data_04_write = random_dut_ids_for_scratch_write_tq[1]
        self.set_up_tq_for_scratch_write('USER_DATA_04', dut_id_for_user_data_04_write, random_index_of_tq_to_fire)
        for dut_id in other_dut_ids_for_blank_delay_tq:
            self.set_up_tq_for_blank_delay(dut_id, random_index_of_tq_to_fire)
        self.numberOfTQsExecutionExpected = TOTAL_NUMBER_OF_DUT_IDS

    def set_up_tq_for_scratch_write(self, scratch_register_name, dut_id, index_of_tq_to_fire):
        trigger_queue_bits = self.generate_scratch_write_tq_via_DDR(scratch_register_name)
        trigger_register_data = self.generate_aurora_trigger_for_dut(dut_id, index_of_tq_to_fire)
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        return trigger_queue_bits

    def set_up_tq_for_blank_delay(self, dut_id, index_of_tq_to_fire):
        trigger_queue_bits = self.generate_blank_delay_tq_via_DDR()
        trigger_register_data = self.generate_aurora_trigger_for_dut(dut_id, index_of_tq_to_fire)
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        return trigger_queue_bits

    def select_random_active_and_inactive_duts(self):
        all_duts = list(ALL_DUT_IDS)
        random.shuffle(all_duts)
        no_of_duts_to_select_as_active = random.randint(0, TOTAL_NUMBER_OF_DUT_IDS)
        return all_duts[:no_of_duts_to_select_as_active], all_duts[no_of_duts_to_select_as_active:]

    def get_random_dut_ids_for_scratch_write_and_blank_delay_tqs(self):
        all_duts = list(ALL_DUT_IDS)
        random.shuffle(all_duts)
        random_dut_ids_for_scratch_write_tq = all_duts[:2]
        other_dut_ids_for_blank_delay_tq = all_duts[2:]
        return random_dut_ids_for_scratch_write_tq, other_dut_ids_for_blank_delay_tq

    def default_execute_scenario_for_device(self):
        trigger_queue_bits = self.generate_trigger_queue_for_scenario()
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def evaluate_results_on_tq_process_completion(self):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.is_tq_processing_complete():
                self.tq_post_process_actions.get(self.get_scenario_name(), self.default_tq_post_process_action)()
                self.verify_tq_response()
                break
        else:
            self.errorList.append(
                {'Iteration': self.currentIteration, 'Device': self.get_device_id(),
                 'Expected': f'TQs Processed: {self.get_all_expected_data()[0]}',
                 'Observed': f'TQs Processed: {self.check_tq_processed_counter() - self.get_tq_processed_count()}'})

    def get_results_on_tq_process_completion(self):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.is_unicast_tq_processing_complete():
                self.get_observed_data_from_tq_execution()
                break
        else:
            self.errorList.append(
                {'Iteration': self.currentIteration, 'Device': self.get_device_id(),
                 'Expected': 'Read TQ Processed', 'Observed': 'Read TQ NOT processed'})

    def broadcast_scratch_write_check_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.read_data_from_scratch_using_trigger_queue_via_DDR('USER_DATA_03')
        self.read_data_from_scratch_using_trigger_queue_via_DDR('USER_DATA_04')

    def read_data_from_scratch_using_trigger_queue_via_DDR(self, scratch_register_name):
        trigger_queue_bits = self.generate_scratch_read_tq_via_DDR(scratch_register_name)
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        self.send_trigger_from_rctc(trigger_register_data)
        self.get_results_on_tq_process_completion()

    # def get_observed_data_from_tq_execution(self):
    #     self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
    #     self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
    #     self.add_to_observed_data(self.testClass.i2c_data_readback(self.rm_device))

    def broadcast_delay_tq_processed_count_check_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.add_to_observed_data(self.check_tq_processed_counter() - self.get_tq_processed_count())

    def delay_tq_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        if (self.calculate_absolute_error_in_observed_delay() < TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS) \
                or (self.calculate_abs_perc_error_in_observed_delay() < TQ_DELAY_ABSOLUTE_PERCENTAGE_ERROR_THRESHOLD):
            self.add_to_observed_data(self.get_all_expected_data()[0])
        else:
            self.add_to_observed_data(self.get_trigger_queue_process_time())

    # def default_tq_post_process_action(self):
    #     self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
    #     self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
    #     self.add_to_observed_data(self.testClass.i2c_data_readback(self.rm_device))

    def calculate_absolute_error_in_observed_delay(self):
        return abs(self.get_trigger_queue_process_time() - self.get_all_expected_data()[0])

    def calculate_abs_perc_error_in_observed_delay(self):
        return self.calculate_absolute_error_in_observed_delay() / self.get_all_expected_data()[0] * 100

    def get_trigger_queue_process_time(self):
        return int(self.get_tq_process_end_time_microseconds() - TRIGGER_SEND_DELAY_MICROSECONDS
                   - self.get_tq_process_start_time_microseconds())

    def wait_for_seconds(self, wait_time_secs):
        start_time = perf_counter()
        while True:
            curr_time = perf_counter()
            if (curr_time - start_time) > wait_time_secs:
                return

    # def report_results(self):
    #     if self.errorList:
    #         self.Log('error', f'PMBUS transaction using trigger queue for PSDB {self.get_psdb_target()} failed for '
    #                           f'{len(self.errorList)} tests.')
    #         self.testClass.report_errors(self.rm_device, self.errorList)
    #     else:
    #         self.Log('info', f'PMBUS transaction using trigger queue for PSDB {self.get_psdb_target()} was successful '
    #                          f'for all {self.get_no_of_test_iterations()} iterations.')

    def get_pmbus_device_list_randomized(self):
        pass

    def generate_trigger_queue_for_scenario(self, scenario_name=None):
        if scenario_name is None:
            scenario_name = self.get_scenario_name()
        scenario_function_to_call = self.scenario_functions.get(scenario_name)
        return scenario_function_to_call()

    def generate_version_read_tq_via_DDR(self):
        trigger_queue = self.version_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(self.get_version_read_expected_data())
        return trigger_queue_bits

    def generate_capability_read_tq_via_DDR(self):
        trigger_queue = self.capability_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(self.get_capability_read_expected_data())
        return trigger_queue_bits

    def capability_read_tq_commands(self):
        trigger_queue = list()
        tq_command_capability_read = self.capability_read_trigger_queue_record()
        trigger_queue.append(tq_command_capability_read)
        return trigger_queue

    def version_read_tq_commands(self):
        trigger_queue = list()
        tq_command_version_read = self.version_read_trigger_queue_record()
        trigger_queue.append(tq_command_version_read)
        return trigger_queue

    def generate_scratch_write_read_tq_via_DDR(self):
        trigger_queue, random_data_to_be_written = self.scratch_write_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(random_data_to_be_written)
        return trigger_queue_bits

    def generate_blank_delay_tq_via_DDR(self):
        trigger_queue = self.delay_tq_commands(delay_us=0)
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        return trigger_queue_bits

    def generate_scratch_write_tq_via_DDR(self, scratch_register_name):
        trigger_queue, random_data_to_be_written = self.scratch_write_tq_commands(scratch_register_name)
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(random_data_to_be_written)
        return trigger_queue_bits

    def generate_scratch_read_tq_via_DDR(self, scratch_register_name):
        trigger_queue = self.scratch_read_tq_commands(scratch_register_name)
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        return trigger_queue_bits

    def generate_random_delay_tq_via_DDR(self):
        trigger_queue, delay_us = self.random_delay_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(delay_us)
        return trigger_queue_bits

    def generate_tq_via_DDR(self, trigger_queue):
        tq_start = self.start_trigger_queue_record()
        tq_end = self.end_trigger_queue_record()
        trigger_queue_bits = self.create_ddr_trigger_queue_bits(tq_end, tq_start, trigger_queue)
        return trigger_queue_bits

    def create_ddr_trigger_queue_bits(self, tq_end, tq_start, trigger_queue):
        trigger_queue_bits = tq_end
        for trigger_record in trigger_queue:
            trigger_queue_bits = trigger_queue_bits << TQ_RECORD_LENGTH_BITS | trigger_record
        trigger_queue_bits = trigger_queue_bits << TQ_RECORD_LENGTH_BITS | tq_start
        return trigger_queue_bits

    def scratch_write_tq_commands(self, scratch_name):
        trigger_queue = list()
        tq_command_scratch_write, random_data_to_be_written = self.scratch_write_trigger_queue_record(scratch_name)
        trigger_queue.append(tq_command_scratch_write)
        return trigger_queue, random_data_to_be_written

    def scratch_read_tq_commands(self, scratch_name):
        trigger_queue = list()
        tq_command_scratch_read = self.scratch_read_trigger_queue_record(scratch_name)
        trigger_queue.append(tq_command_scratch_read)
        return trigger_queue

    def scratch_write_read_tq_commands(self):
        trigger_queue = list()
        tq_command_scratch_write, random_data_to_be_written = self.scratch_write_trigger_queue_record()
        tq_command_scratch_read = self.scratch_read_trigger_queue_record()
        trigger_queue.append(tq_command_scratch_read)
        trigger_queue.append(tq_command_scratch_write)
        return trigger_queue, random_data_to_be_written

    def random_delay_tq_commands(self):
        delay_us = random.randrange(0x0000, 0xFFFF)
        return self.delay_tq_commands(delay_us), delay_us

    def delay_tq_commands(self, delay_us):
        trigger_queue = list()
        tq_command_delay = self.delay_trigger_queue_record(delay=delay_us)
        trigger_queue.append(tq_command_delay)
        return trigger_queue

    def start_trigger_queue_record(self):
        return self.rm_device.start_trigger_queue_record(psdb_target=self.get_psdb_target(), dma=True)

    def end_trigger_queue_record(self):
        return self.rm_device.end_trigger_queue_record(psdb_target=self.get_psdb_target(), dma=True)

    def delay_trigger_queue_record(self, delay):
        return self.rm_device.delay_within_trigger_queue_record(psdb_target=self.get_psdb_target(), dma=True,
                                                                delay=delay, plb_count=2)

    def version_read_trigger_queue_record(self):
        pmbus_command_code = self.get_version_read_command_code()
        return self.rm_device.create_trigger_queue_command_record(payload_lower=pmbus_command_code,
                                                                  target_interface=self.get_device_id(),
                                                                  payload_byte_count=1,
                                                                  psdb_target=self.get_psdb_target(),
                                                                  read_write=0, command_data=0, dma=True)

    def capability_read_trigger_queue_record(self):
        pmbus_command_code = self.get_capability_read_command_code()
        return self.rm_device.create_trigger_queue_command_record(payload_lower=pmbus_command_code,
                                                                  target_interface=self.get_device_id(),
                                                                  payload_byte_count=1,
                                                                  psdb_target=self.get_psdb_target(), read_write=0,
                                                                  command_data=0, dma=True)

    def scratch_write_trigger_queue_record(self, scratch_name='USER_DATA_03'):
        pmbus_write_to_scratch_address = self.get_scratch_write_command_code(scratch_name)
        random_data_to_write = random.randrange(0, 0xFFFF)
        pmbus_write_to_scratch_command_encoding = \
            (random_data_to_write << pmbus_write_to_scratch_address.bit_length()) | pmbus_write_to_scratch_address
        tq_command_scratch_write = self.rm_device.create_trigger_queue_command_record(
            payload_lower=pmbus_write_to_scratch_command_encoding, target_interface=self.get_device_id(),
            payload_byte_count=3, psdb_target=self.get_psdb_target(),
            read_write=1, command_data=0, dma=True)
        return tq_command_scratch_write, random_data_to_write

    def scratch_read_trigger_queue_record(self, scratch_name='USER_DATA_03'):
        pmbus_read_from_scratch_address = self.get_scratch_read_command_code(scratch_name)
        return self.rm_device.create_trigger_queue_command_record(
            payload_lower=pmbus_read_from_scratch_address, target_interface=self.get_device_id(),
            payload_byte_count=2, psdb_target=self.get_psdb_target(), read_write=0,
            command_data=0, dma=True)

    def get_version_read_command_code(self):
        pass

    def get_capability_read_command_code(self):
        pass

    def get_version_read_expected_data(self):
        pass

    def get_capability_read_expected_data(self):
        pass

    def get_scratch_write_command_code(self, scratch_name):
        pass

    def get_scratch_read_command_code(self, scratch_name):
        pass

    def save_trigger_queue_to_memory(self, trigger_queue_record, trigger_register_data):
        lut_address = self.calculate_lut_address(trigger_register_data.index_of_tq_to_fire,
                                                 trigger_register_data.dut_id)
        tq_record_start_address = self.dma_tq_record_address_to_lut(trigger_queue_record, lut_address)
        self.send_trigger_queue_to_memory(tq_record_start_address, trigger_queue_record)

    def calculate_lut_address(self, index_of_tq_to_fire, dut_id):
        return (index_of_tq_to_fire + (dut_id << 20)) << 3

    def send_trigger_from_rctc(self, trigger_register_data):
        rctc_device = self.rctc
        trigger_link_index = 17
        processed_tqs_count_before_trigger_send = self.check_tq_processed_counter()
        self.set_tq_processed_count(processed_tqs_count_before_trigger_send)
        rctc_device.send_trigger(trigger_register_data.value, trigger_link_index)
        self.set_tq_process_start_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.post_trigger_send_action(trigger_register_data)

    def post_trigger_send_action(self, trigger_register_data):
        received_trigger_on_rm = self.rm_device.read_trigger()
        if received_trigger_on_rm != trigger_register_data.value:
            self.Log('error', f'Correct Trigger NOT received at RM. Received 0x{hex(received_trigger_on_rm)} '
                              f'instead of 0x{hex(trigger_register_data.value)}')

    def generate_aurora_trigger_for_random_non_broadcast_dut(self, index_of_tq_to_fire=None):
        random_dut_id = random.randrange(TQ_BIRM_START_DUT_ID, TQ_BIRM_END_DUT_ID)
        return self.generate_aurora_trigger_for_dut(random_dut_id, index_of_tq_to_fire)

    def generate_aurora_trigger_for_dut(self, dut_id, index_of_tq_to_fire=None):
        if index_of_tq_to_fire is None:
            index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        return self.generate_aurora_trigger(dut_id, index_of_tq_to_fire)

    def generate_aurora_trigger(self, dut_id, index_of_tq_to_fire):
        trigger_register_data = self.rm_device.registers.LAST_TRIGGER_SEEN()
        trigger_register_data.target_source = 0x3f
        trigger_register_data.dut_id = dut_id
        trigger_register_data.sw_up_trigger = 0
        trigger_register_data.reserved = 0x1E8  # set field "reserved" to 0x1E8 to facilitate SignalTap capture
        trigger_register_data.index_of_tq_to_fire = index_of_tq_to_fire
        return trigger_register_data

    def dma_tq_record_address_to_lut(self, trigger_queue_record, lut_address):
        no_of_command = (trigger_queue_record.bit_length() + 63) // TQ_RECORD_LENGTH_BITS
        queue_record_start_address = 0x4000008
        queue_record_end_address = (0x7ffffff - no_of_command)
        tq_record_start_address = random.randint(queue_record_start_address, queue_record_end_address)
        start_address_multiple_of_four = tq_record_start_address * 8
        tq_record_start_address_in_byte = start_address_multiple_of_four.to_bytes(8, byteorder='little')
        self.rm_device.dma_write(lut_address, tq_record_start_address_in_byte)
        return start_address_multiple_of_four

    def save_blank_data_in_memory(self, lut_address):
        blank_data_address_byte_space = 0x4000000
        blank_data_address_bits = blank_data_address_byte_space * NUMBER_OF_BITS_IN_A_BYTE
        blank_data_address_bytes = blank_data_address_bits.to_bytes(8, byteorder='little')
        blank_data_bytes = bytes(8)
        self.rm_device.dma_write(lut_address, blank_data_address_bytes)
        self.rm_device.dma_write(blank_data_address_bits, blank_data_bytes)

    def send_trigger_queue_to_memory(self, tq_record_start_address, trigger_queue_record):
        no_of_bytes = (trigger_queue_record.bit_length() + 7) // NUMBER_OF_BITS_IN_A_BYTE
        tq_data_in_byte = trigger_queue_record.to_bytes(no_of_bytes, byteorder='little')
        self.rm_device.dma_write(tq_record_start_address, tq_data_in_byte)
        return tq_data_in_byte

    def is_tq_processing_complete(self):
        if self.is_broadcast_scenario():
            return self.is_broadcast_tq_processing_complete(self.numberOfTQsExecutionExpected)
        else:
            return self.is_unicast_tq_processing_complete()

    def is_broadcast_scenario(self):
        return self.scenarioName[:9] == 'Broadcast'

    def is_broadcast_tq_processing_complete(self, number_of_duts_with_stored_tqs):
        return self.check_tq_processed_counter() >= (self.get_tq_processed_count() + number_of_duts_with_stored_tqs)

    def is_unicast_tq_processing_complete(self):
        return self.check_tq_processed_counter() > self.get_tq_processed_count()

    def check_tq_processed_counter(self):
        return self.rm_device.read_dc_trigger_page_processed_queues()

    def verify_tq_response(self):
        expected_data_list = self.get_all_expected_data()
        observed_data_list = self.get_all_observed_data()
        for i in range(len(expected_data_list)):
            expected_data = expected_data_list[i]
            observed_data = observed_data_list[i]
            if observed_data != expected_data:
                self.errorList.append(
                    {'Iteration': self.currentIteration, 'Device': self.get_device_id(),
                     'Expected': hex(expected_data), 'Observed': hex(observed_data)})

    def setup_aurora_link_and_power_state(self):
        self.enable_aurora_bus(1)
        self.reset_power_state()

    def reset_power_state(self):
        self.hbicc.ring_multiplier.write_dc_trigger_dut_power_state(0, 0)
        time.sleep(0.1)

    def enable_aurora_bus(self, enable=1):
        self.hbicc.ring_multiplier.write_dc_trigger_enable_aurora_data(enable)
        time.sleep(0.1)


class TQTestScenarioLTM4678_VCCIO(TQTestScenario):

    def device_init(self):
        self.tq_command_list = []
        self.voltage_settings = {}
        self.ltm4678_cmd_page = 0x00
        self.ltm4678_cmd_vout = 0x21
        self.ltm4678_cmd_operation = 0x01
        self.ltm4678_cmd_vout_max = 0x24
        self.ltm4678_cmd_vout_margin_low = 0x26
        self.ltm4678_cmd_vout_margin_high = 0x25
        self.ltm4678_cmd_ov_fault = 0x40
        self.ltm4678_cmd_ov_warn = 0x42
        self.ltm4678_cmd_uv_warn = 0x43
        self.ltm4678_cmd_uv_fault = 0x44
        self.ltm4678_cmd_read_vout = 0x8B
        self.trigger_queue_content = 0
        self.ltm4678_vout_val = 0x21
        self.ltm4678_ov_fault_val = 0
        self.ltm4678_ov_warn_val = 0
        self.ltm4678_uv_warn_val = 0
        self.ltm4678_uv_fault_val = 0
        self.ltm4678_voltage_coefficient = 2**12

    def set_and_check_vccio_voltage(self, vccio_voltage=1.8):
        self.setup_aurora_link_and_power_state()
        time.sleep(.1)
        for psdb, pin0, pin1 in self.hbicc.possible_paths:
            if pin0:
                self.update_vccio_setting(psdb, vccio_voltage=vccio_voltage)
        time.sleep(.5)
        for psdb, pin0, pin1 in self.hbicc.possible_paths:
            if pin0:
                self.check_vccio_setting(psdb, expected=vccio_voltage)

    def check_vccio_setting(self, psdb, expected=1.8):
        self.device_init()
        self.set_psdb_target(psdb_target=psdb)
        vccio_channel_list = ['vccio_0_0', 'vccio_1_0', 'vccio_2_0', 'vccio_3_0',
                              'vccio_0_1', 'vccio_1_1', 'vccio_2_1', 'vccio_3_1']
        data = []
        failed = False
        for vccio_channel in vccio_channel_list:
            self.select_vccio_channel(vccio_channel)
            self.Log('debug', f'reading {vccio_channel} setting of psdb{psdb}')
            observed = round(self._read_ltm4678_vout(), 2)
            entry = {'Channel': vccio_channel, 'Expected': expected, 'Observed': observed}
            if (expected + 0.1) >= observed >= (expected - 0.1):
                entry.update({'Result': 'PASS'})
            else:
                entry.update({'Result': 'FAILED'})
                failed = True
            data.append(entry)
        table = self.hbicc.contruct_text_table(data=data, title_in=f'PSDB{psdb} VCCIO SetUp')
        if failed:
            self.Log('critical', f'Failed to set VCCIO correctly {table}')
        else:
            self.Log('debug',f' PSDB {psdb} VCCIO set to {expected}V')
            # self.Log('info',f' PSDB {psdb} VCCIO set to {expected}V')

    def update_vccio_setting(self, psdb, vccio_voltage):
        self.device_init()
        self.set_psdb_target(psdb_target=psdb)
        vccio_channel_list = ['vccio_0_0', 'vccio_1_0', 'vccio_2_0', 'vccio_3_0',
                              'vccio_0_1', 'vccio_1_1', 'vccio_2_1', 'vccio_3_1']
        for vccio_channel in vccio_channel_list:
            self.select_vccio_channel(vccio_channel)
            self.set_uv_ov_vout_value(uv=0, ov=2, vout=vccio_voltage)
            self.write_voltage_setting()


    def set_psdb_target(self, psdb_target):
        self.psdbTarget = psdb_target

    def select_vccio_channel(self, vout_name):
        self._interface_page_decode(vout_name)
        self._write_ltm4678_page_command_list()
        self._send_command_to_ltm4678_dma()

    def read_voltage_settings(self):
        self.voltage_settings = []
        self.voltage_settings.append({'page': self._read_ltm4678_page(),
                                      'vout': self._read_ltm4678_vout(),
                                      'vout_reg': self._read_ltm4678_vout_reg(),
                                      'vout_max': self._read_ltm4678_vout_max(),
                                      'operation': self._read_ltm4678_operation(),
                                      'vout_m_low': self._read_ltm4678_vout_margin_low(),
                                      'vout_m_high': self._read_ltm4678_vout_margin_high(),
                                      'uv_warn': self._read_ltm4678_uv_warn_limit(),
                                      'uv_fault': self._read_ltm4678_uv_fault_limit(),
                                      'ov_warn': self._read_ltm4678_ov_warn_limit(),
                                      'ov_fault': self._read_ltm4678_ov_fault_limit()})
        column_headers = ['page', 'vout', 'vout_reg', 'vout_max', 'operation', 'vout_m_low', 'vout_m_high',
                          'uv_warn', 'uv_fault', 'ov_warn', 'ov_fault']
        table = self.rm_device.contruct_text_table(column_headers, self.voltage_settings)
        self.Log('debug', 'voltage setting: ' + f'\n {table}')

    def write_voltage_setting(self):
        self._write_ltm4678_uv_ov_vout_limit_command_list()
        self._send_command_to_ltm4678_dma()

    def _interface_page_decode(self, vout_name):
        if vout_name == 'vccio_0_0':
            self.interface_target = 1
            self.page_set_value = 0
        elif vout_name == 'vccio_1_0':
            self.interface_target = 1
            self.page_set_value = 1
        elif vout_name == 'vccio_2_0':
            self.interface_target = 2
            self.page_set_value = 0
        elif vout_name == 'vccio_3_0':
            self.interface_target = 2
            self.page_set_value = 1
        elif vout_name == 'vccio_0_1':
            self.interface_target = 3
            self.page_set_value = 0
        elif vout_name == 'vccio_1_1':
            self.interface_target = 3
            self.page_set_value = 1
        elif vout_name == 'vccio_2_1':
            self.interface_target = 4
            self.page_set_value = 0
        elif vout_name == 'vccio_3_1':
            self.interface_target = 4
            self.page_set_value = 1

    def _read_ltm4678_page(self, dma=True):
        self._read_ltm4678_page_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return hex(self._i2c_data_readback())

    def _read_ltm4678_vout_reg(self, dma=True):
        self._read_ltm4678_vout_reg_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_operation(self, dma=True):
        self._read_ltm4678_operation_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return hex(self._i2c_data_readback())

    def _read_ltm4678_vout_max(self, dma=True):
        self._read_ltm4678_vout_max_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_vout_margin_low(self, dma=True):
        self._read_ltm4678_vout_margin_low_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_vout_margin_high(self, dma=True):
        self._read_ltm4678_vout_margin_high_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_vout(self, dma=True):
        self._read_ltm4678_vout_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_uv_fault_limit(self, dma=True):
        self._read_ltm4678_uv_fault_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_uv_warn_limit(self, dma=True):
        self._read_ltm4678_uv_warn_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_ov_fault_limit(self, dma=True):
        self._read_ltm4678_ov_fault_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_ov_warn_limit(self, dma=True):
        self._read_ltm4678_ov_warn_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _i2c_data_readback(self):
        return self.hbicc.ring_multiplier.read_slice_register(
            self.hbicc.ring_multiplier.registers.PSDB_DC_TRIGGER_PAGE_I2C_DATA).value

    def _send_command_to_ltm4678_dma(self):
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        self.save_trigger_queue_to_memory(self.trigger_queue_content, trigger_register_data)
        tq_processed_prev_count = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(trigger_register_data)
        self._evaluate_trigger_processing(tq_processed_prev_count)

    def _evaluate_trigger_processing(self, tq_processed_prev_count):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.check_tq_processed_counter() > tq_processed_prev_count:
                self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
                break
        else:
            self.Log('error', 'Trigger Queue NOT processed')

    def set_uv_ov_vout_value(self, uv, ov, vout):
        self.uv_set_value = int(uv*self.ltm4678_voltage_coefficient)
        self.ov_set_value = int(ov*self.ltm4678_voltage_coefficient)
        self.vout_set_value = int(vout*self.ltm4678_voltage_coefficient)
        self.uv_set_value_byte_low = self.uv_set_value & 0xFFFF
        self.uv_set_value_byte_high = (self.uv_set_value >> 8) & 0xFFFF
        self.ov_set_value_byte_low = self.ov_set_value & 0xFFFF
        self.ov_set_value_byte_high = (self.ov_set_value >> 8) & 0xFFFF
        self.vout_set_value_byte_low = self.vout_set_value & 0xFFFF
        self.vout_set_value_byte_high = (self.vout_set_value >> 8) & 0xFFFF

    def _write_ltm4678_vout_command_list(self, dma=True):
        data_byte_count_write = 2
        direction_write = 1
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout, data_byte_count_write,
                                               self.vout_set_value_byte_low, self.vout_set_value_byte_high)
        write_command = self._create_constants_ltm4678(payload, data_byte_count_write, direction_write)
        self._generate_single_command_list(write_command)
        self._generate_trigger_queue_contant(dma)

    def _write_ltm4678_uv_ov_vout_limit_command_list(self, dma=True):
        data_byte_count_write = 2
        direction_write = 1
        payload_uv_warn = self._create_payload_ltm4678(self.ltm4678_cmd_uv_warn, data_byte_count_write,
                                                       self.uv_set_value_byte_low, self.uv_set_value_byte_high)
        payload_uv_fault = self._create_payload_ltm4678(self.ltm4678_cmd_uv_fault, data_byte_count_write,
                                                        self.uv_set_value_byte_low, self.uv_set_value_byte_high)
        payload_ov_warn = self._create_payload_ltm4678(self.ltm4678_cmd_ov_warn, data_byte_count_write,
                                                       self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_ov_fault = self._create_payload_ltm4678(self.ltm4678_cmd_ov_fault, data_byte_count_write,
                                                        self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_vout_max = self._create_payload_ltm4678(self.ltm4678_cmd_vout_max, data_byte_count_write,
                                                        self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_vout_m_high = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_high, data_byte_count_write,
                                                           self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_vout_m_low = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_low, data_byte_count_write,
                                                          self.uv_set_value_byte_low, self.uv_set_value_byte_high)
        payload_vout = self._create_payload_ltm4678(self.ltm4678_cmd_vout, data_byte_count_write,
                                                    self.vout_set_value_byte_low, self.vout_set_value_byte_high)
        self.uv_warn_write_command = \
            self._create_constants_ltm4678(payload_uv_warn, data_byte_count_write, direction_write)
        self.uv_fault_write_command = \
            self._create_constants_ltm4678(payload_uv_fault, data_byte_count_write, direction_write)
        self.ov_warn_write_command = \
            self._create_constants_ltm4678(payload_ov_warn, data_byte_count_write, direction_write)
        self.ov_fault_write_command = \
            self._create_constants_ltm4678(payload_ov_fault, data_byte_count_write, direction_write)
        self.vout_max_write_command = \
            self._create_constants_ltm4678(payload_vout_max, data_byte_count_write, direction_write)
        self.vout_m_high_write_command = \
            self._create_constants_ltm4678(payload_vout_m_high, data_byte_count_write, direction_write)
        self.vout_m_low_write_command = \
            self._create_constants_ltm4678(payload_vout_m_low, data_byte_count_write, direction_write)
        self.vout_write_command = \
            self._create_constants_ltm4678(payload_vout, data_byte_count_write, direction_write)
        self._generate_multiple_write_command_list()
        self._generate_trigger_queue_contant(dma)

    def _write_ltm4678_page_command_list(self, dma=True):
        data_byte_count_write = 1
        direction_write = 1
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_page, data_byte_count_write, self.page_set_value)
        write_command = self._create_constants_ltm4678(payload, data_byte_count_write, direction_write)
        self._generate_single_command_list(write_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_page_command_list(self, dma=True):
        data_byte_count_read = 1
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_page, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_operation_command_list(self, dma=True):
        data_byte_count_read = 1
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_operation, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_max_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout_max, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_margin_low_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_low, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_margin_high_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_high, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_reg_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_read_vout, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_uv_fault_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_uv_fault, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_uv_warn_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_uv_warn, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_ov_fault_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_ov_fault, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_ov_warn_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_ov_warn, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _generate_trigger_queue_contant(self, dma):
        tq_start = self.hbicc.ring_multiplier.start_trigger_queue_record(self.psdbTarget, dma)
        tq_end = self.hbicc.ring_multiplier.end_trigger_queue_record(self.psdbTarget, dma)
        self.trigger_queue_content = tq_end
        for command in reversed(self.tq_command_list):
            tq_command = self.hbicc.ring_multiplier.\
                create_trigger_queue_command_record(payload_lower=command['payload'],
                                                    target_interface=self.interface_target,
                                                    payload_byte_count=command['payload_byte_count'],
                                                    psdb_target=self.psdbTarget, read_write=command['read_write_direction'],
                                                    command_data=command['command_data'], channel_set=0,
                                                    command_encoding_upper=0, dma=command['dma'],
                                                    spi_target=command['ldac_bar'])
            self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_command
        self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_start

    def _create_payload_ltm4678(self, ltm4678_cmd, data_byte_count, data_byte_low=0, data_byte_high=0):
        if data_byte_count == 0:
            return ltm4678_cmd
        elif data_byte_count == 1:
            return ltm4678_cmd | (data_byte_low << 8)
        elif data_byte_count == 2:
            return ltm4678_cmd | (data_byte_low << 8) | (data_byte_high << 16)
        else:
            self.Log('error', 'this method only support upto 2 byte data')

    def _create_constants_ltm4678(self, ltm4678_payload, data_byte_count, read_write_direction):
        if read_write_direction:
            return {'payload': ltm4678_payload, 'payload_byte_count': (data_byte_count + 1),
                    'read_write_direction': read_write_direction, 'ldac_bar': 0, 'command_data': 0, 'dma': True}
        else:
            return {'payload': ltm4678_payload, 'payload_byte_count': data_byte_count,
                    'read_write_direction': read_write_direction, 'ldac_bar': 0, 'command_data': 0, 'dma': True}

    def _generate_multiple_write_command_list(self):
        self.tq_command_list = []
        self.tq_command_list.append(self.uv_warn_write_command)
        self.tq_command_list.append(self.uv_fault_write_command)
        self.tq_command_list.append(self.ov_warn_write_command)
        self.tq_command_list.append(self.ov_fault_write_command)
        self.tq_command_list.append(self.vout_max_write_command)
        self.tq_command_list.append(self.vout_m_high_write_command)
        self.tq_command_list.append(self.vout_m_low_write_command)
        self.tq_command_list.append(self.vout_write_command)

    def _generate_single_command_list(self, command):
        self.tq_command_list = []
        self.tq_command_list.append(command)