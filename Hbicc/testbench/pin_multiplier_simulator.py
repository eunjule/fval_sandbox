# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import random

from Common import fval
from Hbicc.instrument.data_structures import CHANNEL_SET_DUTS

phylite = {0: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           1: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           2: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303},
           3: {0: 34359738367, 1: 34359738367, 2: 34359738367, 3: 34359738367, 4: 4194303}}

ALLBROADCAST=4
NOBROADCAST=5
DTB_LOOPBACK_CYCLES = 67
LOOPBACK_CYCLES_SETPOINT = 4


class PinMultiplier(fval.Object):
    def __init__(self, slot, index, system):
        self.slot = slot
        self.index = index
        self.slices = [PinMultiplierSlice(slot=slot, index=index, slice=i, system=system) for i in range(5)]
        for channel_set in range(4):
            self.slices[0].channel_set_pins[channel_set].loop_back_partner = self.slices[1].channel_set_pins[channel_set]
            self.slices[1].channel_set_pins[channel_set].loop_back_partner = self.slices[0].channel_set_pins[channel_set]
            self.slices[2].channel_set_pins[channel_set].loop_back_partner = self.slices[3].channel_set_pins[channel_set]
            self.slices[3].channel_set_pins[channel_set].loop_back_partner = self.slices[2].channel_set_pins[channel_set]

        self.registers = {0x20: 0x0001_0000,
                          0x30: 0x1B4,
                          0x44: 0x3E9,
                          0x50: 0x3E9,
                          0xC8: 0x030,
                          0xCC: 0x030,
                          0xD0: 0x02E,
                          0xD4: 0x030,
                          0xE0: 0x10,
                          0xE4: 0x10,
                          0x100: 0x10,
                          0x104: 0x10}

    def bar_write(self, bar, offset, data):
        if bar == 0:
            self.register_write(offset, data)
        else:
            self.slices[bar-1].register_write(offset, data)
        
    def register_write(self, offset, data):
        self.registers[offset] = data
    
    def bar_read(self, bar, offset):
        if bar == 0:
            return self.register_read(offset)
        else:
            return self.slices[bar-1].register_read(offset)
        
    def register_read(self, offset):
        return self.registers.get(offset, 0)
        
    def dc_trigger(self, trigger):
        target_interface = (trigger >> 48) & 0xF
        mask_opeartion = (trigger >> 55) & 0x1
        slice = ((target_interface - 6) // 2)
        if mask_opeartion:
            self.slices[slice].mask_pin(trigger)
        else:
            self.slices[slice].force_pin(trigger)
    
    def set_active_channels_mask(self, slice, mask):
        self.slices[slice].set_active_channels_mask(mask)
    
    def set_ctp_mask(self, slice, mask):
        self.slices[slice].set_ctp_mask(mask)

    def fpga_version_string(self):
        version = self.registers[0x20]
        return f'{version >> 16}.{version & 0xFFFF}'


class PinMultiplierSlice(fval.Object):
    def __init__(self, slot, index, slice, system):
        super().__init__()
        self.slot = slot
        self.index = index
        self.chip = 2 * slot + index
        self.slice = slice
        self.system = system
        self.channel_set_pins = [ChannelSetPins(index, slice, channel_set, dtb_present=False) for channel_set in range(4)]
        self.registers = {0x30: 0x1B4,
                          0xC8: 0x030,
                          0xCC: 0x030,
                          0xD0: 0x02E,
                          0xD4: 0x030,
                          0xE0: 0x10,
                          0xE4: 0x10,
                          0x100: 0x10,
                          0x104: 0x10}
        
        self.write_actions = {0x80: self._pattern_control,
                              0x84: self._set_capture_all}
        self.read_actions = {}
        self.setup_clocks()
        self._setup_transceivers()
        self._setup_raw_pin_state_actions()
        self._setup_error_count_actions()
        self._setup_fail_mask_actions()
        self._setup_oct_codes()
        self._max_pattern_error_count = 0
        self.pin_state_control = 0

    def register_write(self, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def default_write_action(self, offset, data):
        self.registers[offset] = data
        
    def register_read(self, offset):
        return self.read_actions.get(offset, self.default_read_action)(offset)
        
    def default_read_action(self, offset):
        return self.registers.get(offset, 0)
    
    def setup_clocks(self):
        self.registers[0x140] = 0x0303E021
        self.registers[0x144] = 0x0303E005
        self.registers[0x148] = 0x0303E015
        self.registers[0x14C] = 0x0303E02A
        self.registers[0x150] = 0x0303E020

    def _setup_transceivers(self):
        self.registers[0x044] = 0x3E9  # ring multiplier ring bus status
        self.registers[0x050] = 0x3E9  # pin multiplier ring bus status
        self.registers[0xF68] = 0x027  # ring multiplier aurora pattern status
        self.registers[0xF74] = 0x027  # pin multiplier 1 aurora pattern status

    def _setup_raw_pin_state_actions(self):
        for r in range(8):
            self.read_actions[0xA0 + 4*r] = self.raw_pin_state
    
    def _setup_error_count_actions(self):
        for i in range(18):
            self.read_actions[0x900 + i*4] = self._get_fail_count
            self.read_actions[0x960 + i*4] = self._get_fail_count
            self.read_actions[0x9C0 + i*4] = self._get_fail_count
            self.read_actions[0xA20 + i*4] = self._get_fail_count
    
    def _setup_fail_mask_actions(self):
        for i in range(256):
            self.write_actions[0xB00 + i*4] = self._set_fail_mask
    
    def _pattern_control(self, offset, data):
        if data & 0x00000400 and not self.system.patgen.skip_multi_burst_simulation:
            self._reset_fail_counts()
        self.registers[offset] = data

    def _set_active_channel_sets(self):
        active_channel_set = self._get_active_channel_sets()
        for channel_set in self.channel_set_pins:
            if channel_set.channel_set in active_channel_set:
                channel_set._is_pm_capture_active = True
            else:
                channel_set._is_pm_capture_active = False

    def _reset_fail_counts(self):
        for channel_set_pins in self.channel_set_pins:
            channel_set_pins.reset_fail_counts()
        
    def force_pin(self, trigger):
        channel_set = (trigger >> 52) & 0x3
        self.channel_set_pins[channel_set].set_drive(trigger)

    def mask_pin(self, trigger):
        channel_set = (trigger >> 52) & 0x3
        self.channel_set_pins[channel_set].set_mask(trigger)

    def raw_pin_state(self, offset):
        channel_set, start, stop = self._channel_set_pins_from_raw_input_offset(offset)
        return self.channel_set_pins[channel_set].raw_pin_state(start, stop)
    
    def _channel_set_pins_from_raw_input_offset(self, offset):
        channel_set = (offset - 0xA0) // 8
        if offset % 8:
            start, stop = 32,35
        else:
            start, stop = 0, 32
        return channel_set, start, stop

    def configure_loopback_pins(self, freq_exp):
        for i in range(0, 35):
            for channel_set in self.channel_set_pins:
                pin_index = i + channel_set.channel_set * 35
                offset1 = pin_index * 12 + 0x200
                config1 = self.register_read(offset1)
                channel_set.update_pin_freq_exp(i, freq_exp)
                channel_set.config_loopback_pin(i, config1, DTB_LOOPBACK_CYCLES)

    def reset_enabled_clock(self, data):
        for i in range(0, 5):
            if data[i] & 0x01:
                for channel_set in self.channel_set_pins:
                    pin_index = 8 * i + channel_set.channel_set * 35
                    offset1=pin_index*12+0x200
                    offset2=pin_index*12+0x204
                    offset3=pin_index*12+0x208
                    config1=self.register_read(offset1)
                    config2=self.register_read(offset2)
                    config3=self.register_read(offset3)
                    channel_set.reset_EC(8*i, config2, config3)
                    subcycle = self.calculate_subcycle(config1)
                    channel_set.set_EC_subcycle(8*i, subcycle)

    def calculate_subcycle(self, config1):
        TesterCycleDelayRxFromDut = (config1 >> 25) & 0x1F
        SubCycleDelayRxFromDut = (config1 >> 15) & 0x3FF
        TotalCycleDelayTxToDut = config1 & 0x7FFF
        RxDelay = 8 * TesterCycleDelayRxFromDut + SubCycleDelayRxFromDut
        subcycle_delay = RxDelay - TotalCycleDelayTxToDut
        subcycle = subcycle_delay - DTB_LOOPBACK_CYCLES + LOOPBACK_CYCLES_SETPOINT
        return subcycle


    def set_pattern_mask(self, mask):
        for channel_set in self.channel_set_pins:
            channel_set.set_pattern_mask(mask)

    def process_vector_to_update_drive(self, vector):
        low_half = int.from_bytes(vector[0:8], byteorder='little')
        high_half = int.from_bytes(vector[8:16], byteorder='little')
        for channel_set in self.channel_set_pins:
            channel_set._fail_capture = False
            channel_set.inc_EC_cycle()
        self.channel_set_capture_data = []
        for channel_set in self.channel_set_pins:
            channel_set.only_update_pattern_drive(low_half, high_half)

    def resolve_compares(self):
        for channel_set in self.channel_set_pins:
            self.channel_set_capture_data.append(channel_set.resolve_compares())
        errors = []
        verdict = [channel_set._fail_capture for channel_set in self.channel_set_pins]
        if any(verdict):
            for channel_set in self.channel_set_pins:
                x = channel_set.concatinate_values(channel_set._fail_state) & phylite[channel_set.channel_set][self.slice]
                y = channel_set.concatinate_values(channel_set._pin_state) & phylite[channel_set.channel_set][self.slice]
                errors.append([x, y])
        return [self.channel_set_capture_merge(x) for x in zip(*self.channel_set_capture_data)], errors

    def is_broadcast_channel_set(self, channel_set_code, channel_set_index):
        if channel_set_code ==  ALLBROADCAST:
            return True
        elif channel_set_code == NOBROADCAST:
            return False
        else:
            return channel_set_code == channel_set_index

    def update_pm_broadcast_settings(self, channel_set_code, channel_data):
        for channel_set in self.channel_set_pins:
            broadcast_flag = self.is_broadcast_channel_set(channel_set_code, channel_set.channel_set)
            channel_set.update_broadcast_settings(broadcast_flag, channel_data)

    def handle_vector(self, vector, specific_channel_set_code = 7, channel_data = None):
        low_half = int.from_bytes(vector[0:8], byteorder='little')
        high_half = int.from_bytes(vector[8:16], byteorder='little')
        self._set_active_channel_sets()
        channel_set_capture_data = []

        state_control_ch_sets = self._get_ch_sets()
        for channel_set in self.channel_set_pins:
            data = channel_set.pattern_word(low_half, high_half, channel_data)
            if data:
                channel_set_capture_data.append(data)
                state_control_ch_sets.append(channel_set.channel_set)

        errors = self._get_pin_error_capture()

        return self.resolve_capture_values(channel_set_capture_data, errors, state_control_ch_sets)

    def resolve_capture_values(self, channel_set_capture_data, errors, state_control_ch_sets):
        channel_sets_per_cycle, ctv_data, pin_errors = None, None, None

        pm_control_channel_set = self._get_active_channel_sets()
        active_ch_sets = set(state_control_ch_sets)
        active_ch_sets = list(active_ch_sets.intersection(list(pm_control_channel_set)))
        if active_ch_sets:
            channel_sets_per_cycle = len(active_ch_sets) * 2
        if channel_set_capture_data:
            ctv_data = [self.channel_set_capture_merge(x) for x in zip(*channel_set_capture_data)]
        if errors:
            pin_errors = errors
        return channel_sets_per_cycle, ctv_data, pin_errors

    def _get_ch_sets(self):
        if self.pin_state_control == 32:
            ch_sets = [0, 1, 2, 3]
        elif self.pin_state_control//4 == self.chip:
            ch_sets = [self.pin_state_control%4]
        else:
            ch_sets = []
        return ch_sets

    def _get_active_channel_sets(self):
        capture_control = self.registers.get(0x84, 0)
        ctp_override_feature_bit = 11
        if (capture_control >> ctp_override_feature_bit) & 0b1:
            all_inactive_bit = 10
            if (capture_control >> all_inactive_bit) & 0b1:
                return []
            else:
                active_channel_set = (capture_control >> 8) & 0b11
                return [active_channel_set]
        else:
            return range(4)

    def _get_pin_error_capture(self):
        if self._max_pattern_error_count >= self.registers[0x90]:
            return None
        errors = []
        verdict = [channel_set._fail_state for channel_set in self.channel_set_pins]
        if any(verdict):
            for channel_set in self.channel_set_pins:
                x = channel_set._fail_state & phylite[channel_set.channel_set][self.slice]
                y = channel_set._pin_state & phylite[channel_set.channel_set][self.slice]
                errors.append([x, y])
            self._max_pattern_error_count += 1
        return errors

    def channel_set_capture_merge(self, data):
        value = 0
        for i, dut_data in enumerate(data):
            value |= (dut_data << i * 2)
        return value

    def _get_fail_count(self, offset):
        channel_set = (offset - 0x900) // 0x60
        pin = (offset - 0x900 - channel_set * 0x60) // 2
        return (self.channel_set_pins[channel_set].fail_count(pin + 1) << 16) | self.channel_set_pins[channel_set].fail_count(pin)
    
    def _set_fail_mask(self, offset, data):
        channel_set = (offset - 0xB00) >> 8
        mask_set = (offset & 0xFF) >> 3
        upper = (offset >> 2) & 1
        self.channel_set_pins[channel_set].set_fail_mask(mask_set, upper, data)
        self.registers[offset] = data
    
    def set_active_channels_mask(self, mask):
        for channel_set_pins in self.channel_set_pins:
            channel_set_pins.set_active_channels_mask(mask)
    
    def set_ctp_mask(self, mask):
        for channel_set in self.channel_set_pins:
            channel_set.set_ctp_mask(mask)

    def reset_max_pattern_error_count(self):
        self._max_pattern_error_count = 0

    def _update_block_fails_mode(self, block_fails_mode):
        for channel_set in self.channel_set_pins:
            channel_set._block_fails_on = block_fails_mode

    def _setup_oct_codes(self):
        for r in range(16):
            self.registers[0x10e0 + 4*r] = 0x1e32

    def set_capture_window(self, enable):
        for channel_set in self.channel_set_pins:
            channel_set.is_capture_window_enable = enable

    def _set_capture_all(self, offset, data):
        self.registers[offset] = data
        is_capture_all_enable = bool(data & 0b100)
        for channel_set in self.channel_set_pins:
            channel_set.is_capture_all_enable = is_capture_all_enable


PIN_COUNT = 35
FORCE_LOW = 0
FORCE_HIGH = 1
MASK_DISABLE = 0
FORCE_TRISTATE = 2
FORCE_PATTERN = 3

COMPARE_LOW         = 0
COMPARE_HIGH        = 1
DRIVE_Z             = 2
VALID_OR_MIDBAND    = 3
DRIVE_LOW           = 4
DRIVE_HIGH          = 5
ENABLED_CLOCK       = 6
KEEP                = 7

ACTIVE_CHANNEL_SET = {0: [0],
                      1: [1],
                      2: [2],
                      3: [3],
                      4: [0, 3],
                      5: [1, 2],
                      6: [],
                      7: [0, 1, 2, 3]}

MASK_SETS = 32
SATURATED_FAIL_COUNT = 32768

class ChannelSetPins():
    def __init__(self, fpga, slice, channel_set, dtb_present=False):
        self.fpga = fpga
        self.slice = slice
        self.channel_set = channel_set
        self._setup_pin_control()
        self._pin_state = 0
        self._block_fails_on = 0
        self._pattern_drive = {x:DRIVE_Z for x in range(PIN_COUNT)}
        self._channel_active = {x:0 for x in range(PIN_COUNT)}
        self.loopbackpins = {x:LoopBackPin() for x in range(PIN_COUNT)}
        self._drive_state = {x:0 for x in range(PIN_COUNT)}
        self._fail_state = 0
        self._pattern_fail_mask = 0
        self._fail_masks = {x:0 for x in range(PIN_COUNT)}
        self.reset_fail_counts()
        self._reset_half_cache()
        self._active_lower_channels = []
        self._active_upper_channels = []
        self._pin_state_table = {FORCE_LOW: lambda pin:0,
                                 DRIVE_LOW: lambda pin:0,
                                 FORCE_HIGH: lambda pin:1,
                                 DRIVE_HIGH: lambda pin:1,
                                 FORCE_TRISTATE: self._random_bit,
                                 DRIVE_Z: self._random_bit,
                                 ENABLED_CLOCK: self._enabled_clock}
        self.dtb_present=dtb_present
        self.num_EC_cycles=0
        self._EC_pins = {x:EnabledClockPins(x) for x in range(0,35,8)}
        self._capture_pins = []
        self._is_pm_capture_active = True
        self._pin_mask = [0] * 35
        self.loop_back_partner = None
        self.is_capture_window_enable = True
        self.is_capture_all_enable = False

    def config_loopback_pin(self, pin_index, config1, tiu_delay):
        if self._pin_control[pin_index]==FORCE_PATTERN:
            self.loopbackpins[pin_index].config_loopback_buffers(config1, tiu_delay)
            self.dtb_present = True
        else:
            self.loopbackpins[pin_index].clear_buffers()
            self.dtb_present = False

    def update_pin_freq_exp(self, pin_index, freq_exp):
        self.loopbackpins[pin_index].update_freq_exp(freq_exp)

    def update_broadcast_settings(self, broadcast, channel_set_data):
        self._broadcast = broadcast
        self._channel_set_data = channel_set_data

    def reset_EC(self, i, config2, config3 = 0):
        self.num_EC_cycles=0
        self._EC_pins[i].reset(config2,config3)


    def set_EC_subcycle(self, i, subcycle):
        self._EC_pins[i].set_EC_subcycle(subcycle)


    def inc_EC_cycle(self):
            self.num_EC_cycles+=1

    def set_drive(self, trigger):
        '''If the target interface is even, set pins 34:24, else set pins 23:0'''
        if (trigger >> 48) & 1:
            self._set_drive_pin_range(24, 35, trigger)
        else:
            self._set_drive_pin_range(0, 24, trigger)
        self._pattern_drive_pins = [x for x in range(PIN_COUNT) if self._pin_control[x] == FORCE_PATTERN]
        self._force_tristate_pins = [x for x in range(PIN_COUNT) if self._pin_control[x] == FORCE_TRISTATE]

    def set_mask(self, trigger):
        '''If the target interface is even, set pins 34:24, else set pins 23:0'''
        if (trigger >> 48) & 1:
            self._set_mask_pin_range(24, 35, trigger)
        else:
            self._set_mask_pin_range(0, 24, trigger)

    def _set_drive_pin_range(self, start, stop, payload):
        for i in range(start, stop):
            if self._pin_mask[i] == MASK_DISABLE:
                drive_setting = payload & 0x3
                self._pin_control[i] = drive_setting
                if drive_setting == FORCE_LOW:
                    self._pin_state &= ~(1 << i)
                elif drive_setting == FORCE_HIGH:
                    self._pin_state |= 1 << i
            payload = payload >> 2

    def _set_mask_pin_range(self, start, stop, payload):
        for i in range(start, stop):
            mask_setting = payload & 0x1
            self._pin_mask[i] = mask_setting
            payload = payload >> 1

    def _setup_pin_control(self):
        self._pin_control = {x:FORCE_PATTERN for x in range(PIN_COUNT)}
        self._pattern_drive_pins = []
        self._force_tristate_pins = []
    
    def set_active_channels_mask(self, mask):
        self._active_channels = []
        for pin in range(PIN_COUNT):
            if self.channel_set in ACTIVE_CHANNEL_SET[mask[pin]]:
                self._channel_active[pin] = True
                self._active_channels.append(pin)
            else:
                self._channel_active[pin] = False
        self._active_lower_channels = [x for x in self._active_channels if x < 21]
        self._active_upper_channels = [x for x in self._active_channels if x > 20]
        self._reset_half_cache()
    
    def set_ctp_mask(self, mask):
        self._capture_pins = [x for x in range(PIN_COUNT) if (mask >> x) & 1]
    
    def raw_pin_state(self, start, stop):
        '''Get binary state of pins in range(start, stop), as an integer'''
        mask = 0x00000001
        state = 0
        for i in range(start, stop):
            pin_control = self._pin_control[i]
            if pin_control == FORCE_HIGH:
                state |= mask
            elif pin_control == FORCE_LOW:
                pass
            elif pin_control == FORCE_TRISTATE:
                if self.slice in [0, 2, 4]:
                    if i & 1:
                        loopback_pin = self._pin_control[i-1]
                    elif i == 34:
                        if self.slice in [0, 2]:
                            loopback_pin = self.loop_back_partner._pin_control[0]
                        else:
                            loopback_pin = 0
                    else:
                        loopback_pin = self._pin_control[i+1]
                else:
                    if i & 1:
                        loopback_pin = self._pin_control[i + 1]
                    elif i == 0:
                        loopback_pin = self.loop_back_partner._pin_control[34]
                    else:
                        loopback_pin = self._pin_control[i - 1]
                if loopback_pin == FORCE_HIGH:
                    state |= mask
                elif loopback_pin == FORCE_LOW:
                    pass
                else:
                    state |= random.getrandbits(1) << (i - start)
            else:
                state |= random.getrandbits(1) << (i - start)
            mask = mask << 1
        return state
    
    def reset_fail_counts(self):
        self._fail_counts = {x:0 for x in range(PIN_COUNT)}
    
    def fail_count(self, pin):
        count = self._fail_counts.get(pin, 0)
        return min(self._fail_counts.get(pin, 0), SATURATED_FAIL_COUNT)
    
    def set_fail_mask(self, mask_set, upper, data):
        mask = self._fail_masks[mask_set]
        if upper:
            mask &= 0x0FFFFFFFF
            mask |= (data & 0x7) << 32
        else:
            mask &= 0x700000000
            mask |= data & 0x0FFFFFFFF
        self._fail_masks[mask_set] = mask

    def set_pattern_mask(self, mask):
        self._pattern_fail_mask = mask

    def only_update_pattern_drive(self, low_half, high_half):
        self.low_half = low_half
        self.high_half = high_half
        self._update_pattern_drive()

    def resolve_compares(self):
        self._update_inputs()
        if self._compare_channels:
            self._update_fail_counts()
        capture_data = []
        if self.high_half & 0x2000000000000000:
            for pin in self._capture_pins:
                capture_data.append((self._fail_state[pin] << 1) + self._pin_state[pin])
        return capture_data

    def pattern_word(self, low_half, high_half, channel_data = None):
        self.low_half = low_half
        self.high_half = high_half
        self._update_pattern_drive()
        self._update_inputs()
        if self._compare_low_channels or self._compare_high_channels:
            self._update_fail_state()
        else:
            self._fail_state = 0

        capture_data = []
        if self.is_capture_active(high_half) and self._is_pm_capture_active and self._broadcast:
            for pin in self._capture_pins:
                fail_state = (self._fail_state >> pin) & 1
                pin_state = (self._pin_state >> pin) & 1
                capture_data.append((fail_state << 1) + pin_state)
        self.inc_EC_cycle()
        return capture_data

    def is_capture_active(self, high_half):
        is_ctv_enable = bool(high_half & 0x2000000000000000)
        if self.is_capture_window_enable:
            if is_ctv_enable or self.is_capture_all_enable:
                return True
        return False

    def _update_pattern_drive(self):
        if self.dtb_present|(self._last_low_half != self.low_half or self._last_high_half != self.high_half):
            previous_drive = self._pattern_drive
            compare_low_channels = 0
            compare_high_channels = 0

            for i in self._active_lower_channels:
                loopback_pin = self.loopbackpins[i]
                drive = (self.low_half >> i*3) & 0x7
                if drive == 7: #KEEP
                    drive = previous_drive[i]

                self._pattern_drive[i] = drive
                if not self._broadcast:
                    drive = drive if self._channel_set_data[i] == 0x3 else self._channel_set_data[i]
                    self._pattern_drive[i] = drive

                # update_values
                loopback_pin.compare_buffer.append(drive)
                loopback_pin.drive_buffer.append(drive)

                # get_compare
                drive = loopback_pin.compare_buffer[loopback_pin.compare_index]
                loopback_pin.compare_index += 1

                if drive == 0: #COMPARE_LOW:
                    compare_low_channels |= 1 << i
                elif drive == 1: #COMPARE_HIGH:
                    compare_high_channels |= 1 << i

            for i in self._active_upper_channels:
                loopback_pin = self.loopbackpins[i]
                drive = (self.high_half >> (i-21)*3) & 0x7
                if drive == 7: #KEEP:
                    drive = previous_drive[i]

                self._pattern_drive[i] = drive
                if not self._broadcast:
                    drive = drive if self._channel_set_data[i] == 0x3 else self._channel_set_data[i]
                    self._pattern_drive[i] = drive


                # # update_values
                loopback_pin.compare_buffer.append(drive)
                loopback_pin.drive_buffer.append(drive)

                # get_compare
                drive = loopback_pin.compare_buffer[loopback_pin.compare_index]
                loopback_pin.compare_index += 1

                if drive == 0: #COMPARE_LOW:
                    compare_low_channels |= 1 << i
                elif drive == 1: #COMPARE_HIGH:
                    compare_high_channels |= 1 << i
                    
            self._last_low_half = self.low_half
            self._last_high_half = self.high_half
            self._compare_low_channels = compare_low_channels
            self._compare_high_channels = compare_high_channels

    def _reset_half_cache(self):
        self._last_low_half =  0x8000000000000000
        self._last_high_half = 0x0000000000000000
            
    def _update_inputs(self):
        for i in self._pattern_drive_pins:
            if self._channel_active[i]:
                new_state = self._pin_state_table[self._pattern_drive[i]](i)
                self._drive_state[i] = self.loopbackpins[i].drive_buffer[self.loopbackpins[i].drive_index]
                self.loopbackpins[i].drive_index += 1

                if new_state:
                    self._pin_state |= 1 << i
                else:
                    self._pin_state &= ~(1 << i)
        if self._pin_control[0] == 3:
            start_ind = self.slice % 2
            for i in range(start_ind, 34, 2):
                adj_i=i+1
                pinstate1=self._pattern_drive[i]
                pinstate2=self._pattern_drive[adj_i]
                if (pinstate1&6==0) :
                    new_state = self._pin_state_table[self._drive_state[adj_i]](adj_i)
                    if new_state:
                        self._pin_state |= 1 << i
                        self._pin_state |= 1 << adj_i
                    else:
                        self._pin_state &= ~(1 << i)
                        self._pin_state &= ~(1 << adj_i)
                elif (pinstate2&6==0) :
                    new_state = self._pin_state_table[self._drive_state[i]](i)
                    if new_state:
                        self._pin_state |= 1 << i
                        self._pin_state |= 1 << adj_i
                    else:
                        self._pin_state &= ~(1 << i)
                        self._pin_state &= ~(1 << adj_i)
        if len(self._force_tristate_pins) > 0:
            r = random.getrandbits(35)
            for i in self._force_tristate_pins:
                if (r >> i) & 1:
                    self._pin_state |= 1 << i
                else:
                    self._pin_state &= ~(1 << i)

    def _random_bit(self, pin):
        return random.getrandbits(1)

    def _enabled_clock(self, pin):
        return self._EC_pins[pin].resolve_EC(self.num_EC_cycles)


    def _update_fail_state(self):
        mask_set = (self.high_half >> 56) & 0x1F
        mask = self._fail_masks[mask_set] | self._pattern_fail_mask
        low_fails = self._compare_low_channels & self._pin_state & ~mask
        high_fails = self._compare_high_channels & ~self._pin_state & ~mask
        fail_state = low_fails | high_fails
        if fail_state and (not self._block_fails_on):
            self._update_fail_counts(fail_state)
        self._fail_state = fail_state

    def _update_fail_counts(self, fail_state):
        for i in self._active_channels:
            if (1 << i) & fail_state:
                self._fail_counts[i] += 1


class EnabledClockPins():
    def __init__(self, i):
        self.pin=i
        self.EnableClkTesterCycleHigh=0
        self.EnableClkTesterCycleLow=0
        self.EcFirstEdge=0
        self.EcAddHalfCycle=0
        self.ECIsRatio1=0
        self.EC_subcycle = 4

    def set_EC_subcycle(self, subcycle):
        self.EC_subcycle =  subcycle

    def reset(self, config2, config3):
        if(self.is_FRC_pin(config3)):
            self.load_FRC_parameters(config3)
        else:
            self.load_EC_parameters(config2)

    def resolve_half_cycle(self):
        if self.EC_subcycle < 4:
            return self.EcFirstEdge
        else:
            return not self.EcFirstEdge

    def resolve_EC(self, cycles):
        if(self.ECIsRatio1==1):
            period=1
            return self.resolve_half_cycle()
        else:
            period=self.EnableClkTesterCycleHigh+self.EnableClkTesterCycleLow
        if(period):
            subcycle = cycles % period
        else:
            return 0

        if(self.EcFirstEdge):
            if(subcycle<self.EnableClkTesterCycleHigh):
                return 1
            elif(self.EcAddHalfCycle&(subcycle==self.EnableClkTesterCycleHigh)):
                return self.resolve_half_cycle()
            else:
                return 0
        else:
            if(subcycle<self.EnableClkTesterCycleLow):
                return 0
            elif(self.EcAddHalfCycle&(subcycle==self.EnableClkTesterCycleLow)):
                return self.resolve_half_cycle()
            else:
                return 1

    def is_FRC_pin(self, config3):
        return config3 & 0x1

    def load_FRC_parameters(self, config3):
        self.EcFirstEdge = (config3 >> 19) & 0x1
        self.ECIsRatio1 = (config3 >> 20) & 0x1
        FRC_divider = ((config3 >> 1) & 0x3FFFF) + 2
        self.EnableClkTesterCycleHigh = FRC_divider // 2
        self.EnableClkTesterCycleLow = self.EnableClkTesterCycleHigh
        self.EcAddHalfCycle = 0
        if (FRC_divider % 2):
            self.EcAddHalfCycle = 1
            if self.EcFirstEdge:
                self.EnableClkTesterCycleLow += 1
            else:
                self.EnableClkTesterCycleHigh += 1

    def load_EC_parameters(self, config2):
        self.EnableClkTesterCycleHigh = config2 & 0xFF
        self.EnableClkTesterCycleLow = (config2 >> 8) & 0xFF
        self.EcFirstEdge = (config2 >> 16) & 0x1
        self.EcAddHalfCycle = (config2 >> 17) & 0x1
        self.ECIsRatio1 = (config2 >> 18) & 0x1

class LoopBackPin():
    def __init__(self):
        self.drive_buffer=[]
        self.compare_buffer=[]
        self.freq_exp=0
        self.drive_index = 0
        self.compare_index = 0

    def clear_buffers(self):
        self.drive_buffer=[]
        self.compare_buffer=[]
        self.drive_index = 0
        self.compare_index = 0

    def config_loopback_buffers(self, config1, loopback_delay):
        self.drive_buffer=[]
        self.compare_buffer=[]
        self.drive_index = 0
        self.compare_index = 0
        self.delay_mismatch = self.calculate_mismatch(config1, loopback_delay)
        bufsize = round(self.delay_mismatch / 8)
        if bufsize>0:
            self.compare_buffer = [2]*bufsize
        elif bufsize<0:
            self.drive_buffer = [2]*(-bufsize)

    def update_freq_exp(self, freq_exp):
        self.freq_exp=freq_exp

    def update_values(self, pin_state):
        self.compare_buffer.append(pin_state)
        self.drive_buffer.append(pin_state)

    def calculate_mismatch(self, config1, loopback_delay):
        tx = config1 & 0x7FFF
        rx_tester_cycles = (config1 >> 25) & 0x1F
        rx_subcycles = (config1 >> 15) & 0x3FF
        rx = 8*(2**self.freq_exp)*rx_tester_cycles + rx_subcycles
        return rx - tx - loopback_delay
