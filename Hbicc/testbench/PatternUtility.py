# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import csv
import shutil
import time
from threading import Event
import threading

from Common import configs
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.instrument import patgen_register
from Hbicc.testbench.assembler import HbiPatternAssembler
from Hbicc.testbench.capturefailures import Capture

FIFO_PADDING = 16
SLICECOUNT = range(0, 5)
CHANNEL_SET_COUNT_ONE_PSDB = 8

CTV_HEADER_STREAM_START_ADDRESS = 0x400000000
CTV_DATA_STREAM_START_ADDRESS = 0x440000000
TRACE_STREAM_START_ADDRESS = 0x480000000
PM_STREAM_ADDRESSES = {0: 0x4c0000000, 1: 0x500000000, 2: 0x540000000, 3: 0x580000000}

ADDRESSES = [CTV_DATA_STREAM_START_ADDRESS, CTV_HEADER_STREAM_START_ADDRESS, TRACE_STREAM_START_ADDRESS]
ADDRESSES += list(PM_STREAM_ADDRESSES.values())

CTV_HEADER_SLICE_RESOLUTION = 8


class PatternHelper(HbiccTest):
    class CriticalAlarmError(Exception):
        def __init__(self, message):
            Exception.__init__(self, message)

    class PrestageError(Exception):
        def __init__(self, message):
            Exception.__init__(self, message)

    def __init__(self, env):
        self.env = env
        self.hbicc = env.hbicc

        self.active_slice_list = []
        self.default_slice_list = [0, 1, 2, 3, 4]
        self.inactive_slice_list = []
        self.slice_channel_sets_combo = {}
        self.passing_slices_after_pre_stage = []
        self.slice_group ={}
        self.inactive_slice_group ={}

        self.psdb_path = None
        self.track_error_count = True
        self.expected_ctv = None
        self.is_snooper_activated = False
        self.is_xcvr_report_activated = True
        self.device_with_ila_path_list = {}
        self.start_cycle = 0
        self.xcvr_counters = {x: {i: {} for i in range(5)} for x in range(2)}
        self.is_regression = True
        self.single_clock_domain = True
        self.tos_snapshot_path = None
        self.user_mode = None
        self.is_simulator_active = True
        self.is_capture_active = True
        self.end_status = None
        self.enable_burst_event = Event()
        self.subcycle_exponent = 0
        self.e32_interval = 32
        self.inactive_slices = []
        self._is_set_and_check_pin_state = True

    def set_expect_match(self, expect_match = True):
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            slice_obj.set_expect_match(expect_match)

    def set_expected_fail_signature(self, fail_signature):
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            slice_obj.set_expected_fail_signature(fail_signature)

    def create_slice_channel_set_pattern_combo(self, pattern='', fixed_drive_state=None, slices=range(5), channel_sets=range(0, 16), attributes={}, address_and_pattern_list={}, group=0):
        active_slice_list_obj =[]
        for slice in slices:
            address_and_pattern_list_combo = address_and_pattern_list.copy()
            slice_object = Slice(self, self.hbicc, pattern, fixed_drive_state, slice, channel_sets, group)
            slice_object.capture = Capture(slice_object).set_up(attributes=attributes)
            slice_object.address_and_pattern_list = address_and_pattern_list_combo
            self.slice_channel_sets_combo.update({slice: slice_object})
            if slice not in self.active_slice_list:
                self.active_slice_list.append(slice)
            active_slice_list_obj.append(slice_object)
        self.slice_group.update({group:active_slice_list_obj})
        self.Log('debug',f'SLCIE GROUP {self.slice_group},{self.active_slice_list} {self.inactive_slice_group}')
        self.mtv_mask_object = MtvMaskBuilder(self.hbicc)
        self.fix_drive_state = FixDriveStateBuilder(slices=slices, channel_sets=channel_sets, initial_state=self.user_mode)
        self.hbicc.set_up_fval_alarm_logging(slices=slices)
        self.inactive_slice_list = list(set(self.default_slice_list) - set(self.active_slice_list))
        self.inactive_slices = []
        self.inactive_slices = [Slice(self, self.hbicc, pattern, fixed_drive_state, slice, channel_sets, group) for
                                slice in self.inactive_slice_list]


    def update_slice_channel_set_pattern_combo(self, pattern='', fixed_drive_state=None, slices=range(5), channel_sets=range(0, 16), attributes={}, address_and_pattern_list={}, group=0):
        self.active_slice_list = []
        self.slice_channel_sets_combo = {}
        for slice in slices:
            address_and_pattern_list_combo = address_and_pattern_list.copy()
            if slice not in self.active_slice_list:
                self.active_slice_list.append(slice)
            slice_object = Slice(self, self.hbicc, pattern, fixed_drive_state, slice, channel_sets, group)
            slice_object.capture = Capture(slice_object).set_up(attributes=attributes)
            slice_object.address_and_pattern_list = address_and_pattern_list_combo
            self.slice_channel_sets_combo.update({slice: slice_object})
        self.fix_drive_state.set_state_value(new_state = fixed_drive_state, slices = slices, channel_sets = channel_sets)
        self.hbicc.set_up_fval_alarm_logging(slices=slices)
        self.inactive_slice_list = list(set(self.default_slice_list) - set(self.active_slice_list))
        self.inactive_slices = [Slice(self, self.hbicc, pattern, fixed_drive_state, slice, channel_sets, group) for slice in
                                    self.inactive_slice_list]

    def update_capture_start_and_end_address(self, streams_address):
        for slice in self.active_slice_list:
            self.slice_channel_sets_combo[slice].capture.set_up(streams_address=streams_address[slice])

    def set_pin_fixed_drive_mask_dma(self, psdb_target, slices, channel_sets, mask_type, pin=None):
        index = random.randrange(2**12)
        channel_set_id = 63
        number_of_channel_sets = 64
        self.fix_drive_state.set_mask_value(mask_type, channel_sets, slices, pin)
        self.start_tq_content_record_dma()
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            slice_obj.drive_fixed_mask_on_pins_dma(psdb_target)
        self.end_tq_content_record_dma()
        self.store_tq_content_broadcast(index)
        tq_processed_counter_pre = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(channel_set_id, index)
        self.trigger_completion_handshake(number_of_channel_sets, tq_processed_counter_pre)

    def set_capture_expect_steam(self, expect_stream):
        for slice in self.slice_channel_sets_combo:
            self.slice_channel_sets_combo[slice].capture.expect_error_stream = expect_stream[slice]

    def set_ctv_expected(self, expected):
        self.expected_ctv = expected

    def set_pin_fixed_drive_state(self, psdb_target):
        if self.user_mode == 'RELEASE':
            slices = list(self.slice_channel_sets_combo.values())
        else:
            slices = self.inactive_slices + list(self.slice_channel_sets_combo.values())
        for slice in slices:
            slice.drive_fixed_state_on_pins(psdb_target)
        self.Log('debug', 'DC Triggers Completed')

    def set_pin_fixed_drive_state_dma(self, psdb_target, dma_random=False):
        index = random.randrange(2**12)
        channel_set_id = 63
        number_of_channel_sets = 64
        self.start_tq_content_record_dma()
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            slice_obj.drive_fixed_state_on_pins_dma(psdb_target)
        self.end_tq_content_record_dma()
        self.store_tq_content_broadcast(index)
        tq_processed_counter_pre = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(channel_set_id, index)
        self.trigger_completion_handshake(number_of_channel_sets, tq_processed_counter_pre)

    def get_fds_commands_channel_set_list(self):
        fds_commands_list = {}
        for channel_set in self.get_channel_set_from_active_slice():
            fds_commands = self.get_fds_commands_single_channel_set(channel_set)
            fds_commands_list.update({channel_set: fds_commands})
        return fds_commands_list

    def get_channel_set_from_active_slice(self):
        return list(self.slice_channel_sets_combo.values())[0].channel_sets

    def get_fds_commands_single_channel_set(self, channel_set):
        fds_commands = []
        for slice in self.active_slice_list:
            commands_new = self.get_fds_single_channel_set_slice_commands(channel_set, slice)
            fds_commands = fds_commands + commands_new
        return fds_commands

    def get_fds_single_channel_set_slice_commands(self, channel_set, slice):
        psdb_target = channel_set//CHANNEL_SET_COUNT_ONE_PSDB
        payload_last_11_pins, payload_8_pins, payload_16_pins = self.fix_drive_state.get_payloads(channel_set, slice)
        return self.hbicc.ring_multiplier.set_pin_direction(psdb_target, slice, payload_8_pins, payload_16_pins,
                                                            payload_last_11_pins, channel_set, dma=True)

    def trigger_completion_handshake(self, number_of_channel_sets, tq_processed_counter_pre):
        time_pre=time.perf_counter()
        while 1:
            if self.check_tq_processed_counter() - tq_processed_counter_pre == number_of_channel_sets:
                break
            if time.perf_counter() - time_pre > 1:
                counter_delta = self.check_tq_processed_counter() - tq_processed_counter_pre
                self.Log('error', f'DC trigger counter 0xfc check time out for 1s, '
                                  f'expected counter delta is {number_of_channel_sets}, '
                                  f'executed counter delta is {counter_delta} ')
                break

    def trigger_completion_handshake_with_time_report(self, number_of_channel_sets, tq_processed_counter_pre):
        time_pre=time.perf_counter()
        while 1:
            if self.check_tq_processed_counter() - tq_processed_counter_pre == number_of_channel_sets:
                self.Log('info', f'tq_process_time: {time.perf_counter() - time_pre}s')
                break
            if time.perf_counter() - time_pre > 10:
                counter_delta = self.check_tq_processed_counter() - tq_processed_counter_pre
                self.Log('error', f'DC trigger counter 0xfc check time out for 10s, '
                                  f'expected counter delta is {number_of_channel_sets}, '
                                  f'executed counter delta is {counter_delta} ')
                break

    def check_tq_processed_counter(self):
        return self.hbicc.ring_multiplier.read_dc_trigger_page_processed_queues()

    def store_tq_content_broadcast(self, index):
        self.hbicc.ring_multiplier.store_duts_tq_dma_broadcast(index)

    def start_tq_content_record_dma(self):
        self.hbicc.ring_multiplier.start_tq_record_dma()

    def end_tq_content_record_dma(self):
        self.hbicc.ring_multiplier.end_tq_record_dma()

    def prepare_trigger_queue_lut_in_memory(self, tq_content, dma_random):
        if dma_random:
            channel_set_id=random.randrange(63)
            index_of_tq=random.randrange(2**12)
            tq_list_address=random.randrange(0x20000000, 0x40000000, 0x8)
            return self.hbicc.ring_multiplier.ddr_tq_list_operation(channel_set_id, index_of_tq, tq_list_address)
        else:
            # TODO: ddr_tq_list_operation does not exist.
            return self.hbicc.ring_multiplier.ddr_tq_list_operation(dut_id=0, index_of_tq=0, tq_list_address=0x20000000)

    def send_trigger_from_rctc(self, channel_set_id, index):
        rctc_device = self.env.hbicc.rc
        trigger_link_index = 17
        trigger_register_data = self.generate_aurora_trigger(channel_set_id, index)
        rctc_device.send_trigger(trigger_register_data.value,trigger_link_index)
        received_trigger_on_rm = self.hbicc.ring_multiplier.read_trigger()


    def generate_aurora_trigger(self, channel_set_id, index_of_tq):
        trigger_register_data = self.hbicc.ring_multiplier.registers.LAST_TRIGGER_SEEN()
        trigger_register_data.target_source = 0x3f
        trigger_register_data.dut_id = channel_set_id
        trigger_register_data.sw_up_trigger = 0
        trigger_register_data.reserved = 0x1E8  # set field "reserved" to 0x1E8 to facilitate SignalTap capture
        trigger_register_data.index_of_tq_to_fire = index_of_tq
        return trigger_register_data

    def execute_pattern_scenario(self, CAPTURE_CYCLE=3):
        self.start_execution(CAPTURE_CYCLE)
        self.post_burst_processing()

    def start_execution(self, CAPTURE_CYCLE=3):
        self._set_up(CAPTURE_CYCLE)
        self._pre_sync()
        self._enable_burst_start()

    def execute_pattern_scenario_without_burst_start(self, CAPTURE_CYCLE=3):
        self._set_up(CAPTURE_CYCLE)
        self._pre_sync()

    def _set_up(self, capture_cycle):
        self.set_simulator()
        self.hbicc.get_execution_path()
        self.set_up_ctv_header_distribution()
        self._rebase_xcvrs()
        self.set_up_snooper(capture_cycle)
        self.set_up_ila()
        self._set_mtv_mask()
        self.write_pm_subcycle()
        self.set_e32_interval()
        self.set_up_snapshot()
        self.check_all_alarms(check_correctness=False)

    def _set_mtv_mask(self):
        self.mtv_mask_object.set_mtv_mask()

    def post_burst_processing(self, retry_limit=100000000):
        if self.is_pattern_execution_complete_and_end_status_updated(retry_limit=retry_limit):
            self.report_channel_error_count_and_capture_results()
        if self.is_regression:
            self.log_xcvr_count_status()
            self.compare_top_bottom_rm()
        self.check_all_alarms()
        self._register_status_report()
        self._process_ilas()
        self.hbicc.log_snooper(self.is_snooper_activated, list(self.slice_channel_sets_combo.keys()))

    def _process_ilas(self):
        if len(self.env.test._softErrors) == 0 and configs.HBICC_DUMP_REFERENCE_ILA:
            if self.env.test.Name() not in self.hbicc.track_ila:
                file_name = f'{self.env.test.Name()}_repeat{self.env.test.repeat_number}'
                self.Log('info',
                         f'Recording ILAs for first passing test for reference located in ILA Folder: {file_name}')
                self.dump_all_ilas()
                self.hbicc.track_ila.append(self.env.test.Name())
        elif len(self.env.test._softErrors) > 0:
            self.dump_all_ilas()
        else:
            pass

    def _register_status_report(self):
        self.hbicc.pat_gen.log_capture_stats_registers()
        for psdb, pin0, pin1 in self.hbicc.paths:
            pin0.read_under_run_registers()
            pin1.read_under_run_registers()

    def _rebase_xcvrs(self):
        if self.is_xcvr_report_activated:
            self.Log('debug', f'Rebasing Transceiver Counts')
            for slice_index, slice_object in self.slice_channel_sets_combo.items():
                for psdb, pin0, pin1 in self.hbicc.paths:
                    slice_object.base_xcvrs(psdb, self.hbicc.pat_gen, self.hbicc.ring_multiplier, pin0, pin1)

    def check_packet_status(self, pin0, pin1, expected_status):
        for slice in self.active_slice_list:
            for i in range(2):
                pin0_value = pin0.read_slice_register(pin0.registers.PACKET_CHECK_TOP_STATUS, slice=slice, index=i).value
                if pin0_value != expected_status:
                    self.Log('error',
                             f'Packet Check Status: Slice: {slice}: Pin0: idex: {i} 0x{pin0_value:02X} ')
                else:
                    self.Log('info',
                             f'Packet Check Status: Slice: {slice}: Pin0: idex: {i} 0x{pin0_value:02X} ')

            pin1_value = pin1.read_slice_register(pin1.registers.PACKET_CHECK_TOP_STATUS, slice=slice, index=1).value
            if pin1_value != expected_status:
                self.Log('error',
                         f'Packet Check Status: Slice: {slice}: Pin1: 0x{pin1_value:02X}')
            else:
                self.Log('info',
                         f'Packet Check Status: Slice: {slice}:Pin1: 0x{pin1_value:02X}')

    def map_ila_enabled_devices(self):
        for device in self.hbicc.devices():
            self.device_with_ila_path_list.update({device: {}})
            ila_location = os.path.dirname(device.fpga_image_path())
            device.ila_names = {}
            for file in os.listdir(ila_location):
                if file.endswith(".ila"):
                    ila_index = int(file.split('.')[0][-1:])
                    file_size = os.path.getsize(os.path.join(ila_location, file))
                    if file_size:
                        self.construct_ila_filenames(device, file, ila_index, ila_location)

    def construct_ila_filenames(self, device, file, ila_index, ila_location):
        test_name = self.env.test.Name()
        repeat_number = self.env.test.repeat_number
        dst_ila_folder = os.path.join(os.getcwd(), 'ILA', f'{test_name}_repeat{repeat_number}', f'{device.name()}')
        vcd_file = os.path.join(dst_ila_folder, f'{{}}_{device.name().lower()}_ila_{ila_index}.vcd')
        src_ila_file = os.path.join(ila_location, file)
        self.device_with_ila_path_list[device].update({ila_index: {'DST': dst_ila_folder,
                                                                   'SignalMap': src_ila_file,
                                                                   'VCD': vcd_file}})

    def compare_top_bottom_rm(self):
        if self.is_snooper_activated:
            self.Log('debug', f'Processing Snooper...')
            for slice in range(0, 5):
                if slice not in self.active_slice_list:
                    continue
                top_data = self.write_snooper_top(slice)
                bottom_data = self.write_snooper_bottom(slice)
                if len(self.env.hbicc.paths) == 1:
                    if top_data[:-1] != bottom_data[1:253] and self.start_cycle == 0:
                        self.Log('warning',
                                 f'TOP_BOTTOM_RESULTS: Slice {slice} Failed. Bottom Data and Top Data does not match. ')
                    elif self.start_cycle != 0 and top_data[:-1] != bottom_data[226:-1]:
                        self.Log('warning',
                                 f'TOP_BOTTOM_RESULTS: Slice {slice} Failed. Bottom Data and Top Data does not match. ')

    def log_xcvr_count_status(self, title='', slices=None):
        data = []
        type_log = []
        pg, rm = self.hbicc.pat_gen, self.hbicc.ring_multiplier
        if slices is None:
            slices = self.passing_slices_after_pre_stage
        if self.is_xcvr_report_activated:
            for psdb, pin0, pin1 in self.hbicc.paths:
                for slice_index, slice_object in self.slice_channel_sets_combo.items():
                    if slice_index not in slices:
                        continue
                    one_data, log_type = slice_object.read_different_counter_registers(psdb, pg, rm, pin0, pin1,
                                                                                       title=title)
                    type_log.append(log_type)
                    data.append(one_data)
            self.log_xcvr(data, type_log)

    def log_xcvr_count_status_when_abort(self, title='', slices=None):
        type_log = []
        if slices is None:
            slices = self.passing_slices_after_pre_stage
        if self.is_xcvr_report_activated:
            for psdb, pin0, pin1 in self.hbicc.possible_paths:
                if psdb is not None:
                    data = []
                    for slice_index, slice_object in self.slice_channel_sets_combo.items():
                        if slice_index not in slices:
                            continue
                        pg, rm = self.hbicc.pat_gen, self.hbicc.ring_multiplier
                        one_data, log_type = slice_object.read_different_counter_registers(psdb, pg, rm, pin0, pin1,
                                                                                           title=title)
                        type_log.append(log_type)
                        data.append(one_data)
                    self.log_xcvr(data, type_log)

    def set_snooper_cycle(self, CAPTURE_CYCLE):
        for psdb, pin0, pin1 in self.hbicc.paths:
            if self.is_snooper_activated:
                self.hbicc.pat_gen.set_user_cycle_to_snoop(cycle_no=CAPTURE_CYCLE, slice_list=self.active_slice_list)
                self.hbicc.ring_multiplier.set_user_cycle_to_snoop(cycle_no=CAPTURE_CYCLE // 2,
                                                                   slice_list=self.active_slice_list)
                pin0.set_user_cycle_to_snoop(cycle_no=CAPTURE_CYCLE // 2, slice_list=self.active_slice_list)

    def arm_all_ilas(self):
        for device, ilas in self.device_with_ila_path_list.items():
            for slice in list(self.slice_channel_sets_combo.keys()):
                for index, files in ilas.items():
                    device.arm_ila(slice, index)
            self.Log('debug', f'{device.name()}: ILA Armed for Slice(s) {list(self.slice_channel_sets_combo.keys())}')

    def set_up_ila(self):
        if configs.HBICC_DUMP_ILA:
            self.map_ila_enabled_devices()
            self.arm_all_ilas()

    def dump_all_ilas(self):
        for device, indexed_files in self.device_with_ila_path_list.items():
            ila_slices = []
            for slice in list(self.slice_channel_sets_combo.keys()):
                ila_indexes = []
                for index, files in indexed_files.items():
                    if device.is_ila_data_present(slice, index):
                        os.makedirs(files['DST'], exist_ok=True)
                        shutil.copy(files['SignalMap'], files['DST'])
                        device.load_ila_names(index, files['SignalMap'])
                        device.get_samples_and_create_vcd(slice, index, files['VCD'].format(f'S_{slice}'))
                        if slice not in ila_slices:
                            ila_slices.append(slice)
                        if index not in ila_indexes:
                            ila_indexes.append(index)
            if ila_indexes and ila_slices:
                self.Log('debug', f'{device.name()} Slice(s) {ila_slices} ILAs Index {ila_indexes} files dumped.')

    def set_up_snooper(self, capture_cycle):
        if self.is_snooper_activated:
            self.hbicc.ring_multiplier.set_snooper_start_cycle(self.env.test.test_name())
            for psdb, pin0, pin1 in self.hbicc.paths:
                if self.is_snooper_activated:
                    self.hbicc.ring_multiplier.reset_rm_packet_snooper(self.active_slice_list)
                    self.hbicc.pat_gen.reset_rm_packet_snooper(self.active_slice_list)
                    pin0.reset_rm_packet_snooper(self.active_slice_list)
                self.Log('info', f'Snooper Reset Slice(s) {list(self.slice_channel_sets_combo.keys())}')
            self.set_snooper_cycle(capture_cycle)

    def report_channel_error_count_and_capture_results(self):
        if self.is_simulator_active:
            data = self.report_channel_error_count_summary()
            self.write_fails_to_csv(data)
        self.report_fail_capture()

    def report_fail_capture(self):
        if self.is_simulator_active:
            self.process_unstripped_data()
        else:
            if self.is_capture_active:
                self.process_stripped_data()
            else:
                self.Log('info', f'User Deactivated both the Simulator and Capture')

    def process_keepalive_capture_reset(self):
        self.capture_control_check()
        self.capture_control_reset()
        self.capture_control_check()

    def process_keepalive_capture(self):
        for slice_object in self.slice_channel_sets_combo.values():
            if slice_object.capture is not None:
                slice_object.capture.process_keepalive_capture_data()

    def capture_control_check(self):
        for slice in range(5):
            capture_control = self.env.hbicc.pat_gen.read_capture_control_register(slice).value
            self.Log('debug', f' slice {slice}, control status 0b{capture_control:032b}')

    def capture_control_reset(self):
        self.Log('debug', 'Resetting reset_capture_addr_counts in CAPTURE_CONTROL')
        for slice in range(5):
            self.env.hbicc.pat_gen.reset_capture_addr_counts_of_capture_control_register(slice)

    def process_stripped_data(self):
        threads = []
        stitched_data = self.stitch_ctv_headers()
        for slice_object in self.slice_channel_sets_combo.values():
            if slice_object.capture is not None:
                if stitched_data:
                    slice_object.capture.ctv_collective_data = stitched_data
                threads.append(threading.Thread(target=slice_object.capture.process_capture_data))

        self.start_threads(threads)
        self.join_threads(threads)

    def process_unstripped_data(self):
        threads = []
        for slice_object in self.slice_channel_sets_combo.values():
            if slice_object.capture is not None:
                threads.append(threading.Thread(target=slice_object.capture.process_capture_data))

        self.start_threads(threads)
        self.join_threads(threads)

    def join_threads(self, threads):
        for process in threads:
            value = process.join()

    def start_threads(self, threads):
        for process in threads:
            process.start()

    def _get_distribution_att_for_ctv_headers(self):
        distribution_att = {}
        track_count = 0
        highest_count = 0
        for index, slice_object in self.slice_channel_sets_combo.items():
            distribution = slice_object.capture.capture_distribution
            size = slice_object.capture.capture_distribution_size
            count = slice_object.capture.get_ctv_stream_total_size()
            distribution_att.update({slice_object.index: {'distribution': distribution, 'count': count, 'single_block': size, 'offset': 0}})
            if count >= track_count:
                track_count = count
                highest_count = index
        return distribution_att, highest_count

    def _pre_sync(self):
        if self.tos_snapshot_path:
            self.Log('info','Skipping regular pattern dma due to snapshot enabling')
        else:
            self.dma_pattern()

        for psdb, pin0, pin1 in self.hbicc.paths:
            self.pre_sync_pulse_setup(psdb, pin0, pin1)
        for group,slice_obj in self.slice_group.items():
            self.hbicc.pat_gen.toggle_pattern_prestage(slice_obj[0].index)
        if self._is_set_and_check_pin_state:
            self.hbicc.check_pin_states(list(self.slice_channel_sets_combo.values()), user_mode=self.user_mode)

    def set_drive_low_on_all_active_slice_channel_sets(self, psdb_target):
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
                slice_obj.set_drive_low_state(psdb_target)
        for slice_obj in self.inactive_slices:
            slice_obj.set_drive_low_state(psdb_target)

    def initialize_fds(self):
        self.hbicc.get_execution_path()
        for psdb, pin0, pin1 in self.hbicc.paths:
            self.all_slice_set0_phylite_setup(psdb, pin0, pin1)

    def all_slice_set0_phylite_setup(self, psdb_target, pin0, pin1):
        self.hbicc.ring_multiplier.set_vref(psdb_target)
        self.check_vref_voltage(pin0, expected=0.9)
        self.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        time.sleep(0.01)
        self.pms_phylite_reset(pin0, pin1)

    def pre_sync_pulse_setup(self, psdb_target, pin0, pin1):
        self.hbicc.ring_multiplier.set_vref(psdb_target)
        self.check_vref_voltage(pin0, expected=0.9)
        self.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        time.sleep(0.01)
        self.pms_phylite_reset(pin0, pin1)
        self.reset_pm_fifo_to_flush_content(pin0, pin1)
        self.check_all_alarms(check_correctness=False)
        self.set_pin_fixed_drive_state(psdb_target)
        self.pre_stage(pin0, pin1)
        self.Log('debug', f'Printing Alarms Before Start Prestage')
        self.check_all_alarms(check_correctness=False)
        self.hbicc.reset_xcvr_crc_error(pin0, pin1)
        self.start_prestage(pin0, pin1)
        self.check_all_alarms(check_correctness=False)

    def start_prestage(self, pin0, pin1):
        self.hbicc.ring_multiplier.reset_capture_control()
        pin0.start_pattern_prestage(self.active_slice_list)
        pin1.start_pattern_prestage(self.active_slice_list)

    def pms_phylite_reset(self, pin0, pin1):
        pin0.phylite_reset()
        pin1.phylite_reset()

    def pre_stage(self, pin0, pin1):
        mask_dict = self.generate_active_slice_mask()
        for group, slice_obj_list in self.slice_group.items():
            for slice_obj in slice_obj_list:
                slice_obj.perform_pattern_prestage([pin0, pin1],mask_dict)

    def generate_active_slice_mask(self):
        per_group_mask_dict={}
        for group, slice_obj_list in self.slice_group.items():
            active_slice_group_mask = 0
            for slice_obj in slice_obj_list:
                active_slice_group_mask = (1 << slice_obj.index) | active_slice_group_mask
            per_group_mask_dict.update({group:active_slice_group_mask})
            # self.Log('info',f'Inside mask generation for group {group} {per_group_mask_dict}')
        return per_group_mask_dict

    def dma_pattern(self):
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            if slice_obj.is_calibration():
                if self.tos_snapshot_path is None:
                    slice_obj.write_pattern_list_start_address()
                slice_obj.write_pattern_list_to_memory()
            else:
                self.Log('error', f'Slice {slice_index} is not calibrated, failed to DDR Pattern.')

    def check_all_alarms(self, title='', check_correctness=True):
        if not self.is_regression:
            return
        self.hbicc.hbicc_alarms.log_all_alarms(list(self.slice_channel_sets_combo.keys()), check_correctness, table_title=title)
        if self.hbicc.hbicc_alarms.is_exit_test:
            self.hbicc.abort_pattern(SLICECOUNT,'aborting due to alarms')
            self.log_status_report()
            raise self.CriticalAlarmError('\n\n{"*"*100} \n{"*"*45} Check Alarms {"*"*45}\n{"*"*100}')

    def check_alarms(self, pin0, pin1, slices=None):
        if slices is None:
            slices = self.passing_slices_after_pre_stage

        for slice in slices:
            self.get_alarms(pin0, slice)
            self.get_alarms(pin1, slice)
            self.get_alarms(self.hbicc.pat_gen, slice)
            self.get_alarms(self.hbicc.ring_multiplier, slice)

    def get_alarms(self, device, slice):
        alarms = device.read_alarms(slice)
        if alarms:
            self.Log('warning', ' {} Received unexpected alarms {}'.format(device.name(), alarms))
            if 'UnderRun: 1' in alarms:
                self.hbicc.abort_pattern(SLICECOUNT,'abort due to underrun')
                self.log_status_report()
                self.Log('critical', f'\n\n{"*"*100} \n{"*"*45} Underrun {"*"*45}\n{"*"*100}')

    def log_vref(self, pin0):
        vmon_read_all = pin0.read_all_vmon()
        for index in range(1, 5):
            vref = vmon_read_all[index]
            self.Log('info', '{} VREF {} value {}'.format(pin0.name(), index, vref))

    def check_vref_voltage(self, pin0, expected, retry_limit=5, log=True):
        if not self.is_regression:
            return
        failed = False
        expected_list = [expected] * 4
        observed_list = []
        for i in range(retry_limit):
            observed_list = pin0.read_all_vmon()
            if self.vref_expected_observed_list_match(expected_list, observed_list[1:5]):
                data = self.vref_compare_result(expected_list, observed_list[1:5], 'PASS')
                break
            else:
                self.hbicc.ring_multiplier.set_vref(pin0.slot)
        else:
            data = self.vref_compare_result(expected_list, observed_list[1:5], 'FAIL')
            failed = True
        table = self.hbicc.contruct_text_table(data=data, title_in=f'{pin0.name()} Vref SetUp')
        if failed:
            self.Log('error', f'Failed to set Vref correctly {table}')
            self.Log('critical', f'Failed to set vref for following DACS. Aborting further pattern execution.')

    def vref_compare_result(self, expected_list, observed_list, pass_fail):
        data = []
        for index in range(4):
            observed = round(observed_list[index], 3)
            expected = round(expected_list[index], 3)
            entry = {'Channel': index+1, 'Expected': expected, 'Observed': observed, 'Result': pass_fail}
            data.append(entry)
        return data

    def vref_expected_observed_list_match(self, expected_list, observed_list):
        mismatch = False
        for index in range(4):
            if (observed_list[index] > (expected_list[index]+0.1)) | (observed_list[index] < (expected_list[index]-0.1)):
                mismatch = True
        return (not mismatch)

    def is_pattern_execution_complete_and_end_status_updated(self, retry_limit=0xffff0000):
        slices_executed = []
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            if slice_index not in self.passing_slices_after_pre_stage:
                continue
            if self.hbicc.pat_gen.is_pattern_execution_complete(slice_list=[slice_index], retry_limit=retry_limit):
                if self.verify_pattern_end_status(slice_index) and self.track_error_count:
                    slices_executed.append(slice_index)
            else:
                self.hbicc.abort_pattern([slice_index], 'Aborting pattern after failure to complete pattern')
                return False

        self.Log('info', f'{"*"*30} Slice(s) {slices_executed} Burst Completed {"*"*30}')
        return True

    def report_channel_error_count_summary(self):
        data = []
        for slice_index, slice_obj in self.slice_channel_sets_combo.items():
            data += slice_obj.get_error_count_data()
        return data

    def write_fails_to_csv(self, data):
        if data and self.is_regression:
            csv_path = self.construct_csv_file()
            fieldnames = self.get_headers()
            with open(csv_path, 'a', newline='') as out_file:
                writer = csv.DictWriter(out_file, fieldnames=fieldnames)
                writer.writeheader()
                writer.writerows(data)

    def get_headers(self):
        return ['PM', 'Channel Set', 'Slice'] + [f'pin_{pin}' for pin in range(35)]

    def verify_pattern_end_status(self, slice):
        if self.end_status:
            if self.env.tester_name == 'TesterSimulation' and self.is_regression:
                expected_end_status = 0xdeadbeef
            else:
                expected_end_status = self.end_status
        elif self.is_simulator_active:
            expected_end_status = self.hbicc.model.patgen.slices[slice].pattern_end_status()
        else:
            pass

        return self._compare_end_status_to_expected(slice, expected_end_status)

    def _compare_end_status_to_expected(self, slice, expected_end_status):
        actual_pattern_end_status = self.hbicc.pat_gen.read_pattern_end_status(slice)
        if expected_end_status != actual_pattern_end_status:
            self.Log('error',
                     'Pattern End_status not observed for slice {}. Expected 0x{:x} Actual 0x{:x}'.format(slice,expected_end_status,
                                                                                                          actual_pattern_end_status))
            return False
        return True

    def _enable_burst_start(self):
        self.Log('info', f'{"*"*30} Enable Burst Now All Slices {self.active_slice_list} {"*"*30}')
        try:
            self.passing_slices_after_pre_stage,failing_slice_after_prestage = self.hbicc.pat_gen.get_prestage_pass_fail_slices(
                slice_list=self.active_slice_list)
            if failing_slice_after_prestage:
                if self.hbicc.hbicc_alarms.is_alarm_set(alarm1=self.hbicc.hbicc_alarms.critical_alarms_causing_auto_abort):
                    raise self.PrestageError('Prestage failed due to critical alarms. Not enabling burst')
            else:
                 for group,slice_obj in self.slice_group.items():
                     self.hbicc.ring_multiplier.enable_burst_start(slice_obj[0].index)
                     self.enable_burst_event.set()
                     self.env.hbicc.model.patgen.skip_multi_burst_simulation = True
                 self.env.hbicc.model.patgen.skip_multi_burst_simulation = False

        except AssertionError:
            raise
        self.hbicc.ring_multiplier.log_last_tx_rx_seen('After Enable burst')
        self.check_all_alarms(title='After Enable burst')

    def verify_aurora_pattern_status(self):
        pass

    def reset_pm_fifo_to_flush_content(self, pin0, pin1):
        pin0.start_pattern_prestage(self.active_slice_list)
        pin1.start_pattern_prestage(self.active_slice_list)

    def construct_csv_file(self):
        dir = self.create_fail_data_folder()
        csv_path = self.create_csv_file(dir)
        return csv_path

    def create_csv_file(self, dir):
        file_name = self.env.test.Name()
        repeat = self.env.test.repeat_number
        file_name = file_name.replace('.', '_') + '.csv'
        csv_path = os.path.join(dir, f'Repeat_{repeat}_{file_name}')
        return csv_path

    def create_fail_data_folder(self):
        dir = os.path.join(os.getcwd(), 'PATTERN_FAIL_DATA')
        os.makedirs(dir, exist_ok=True)
        return dir

    def write_snooper_bottom(self, slice):
        total_data = []
        Done = False
        if self.is_regression:
            file = self.contruct_dst_dir(slice, 'bottom')
        while not Done:
            data = []
            for i in range(8):
                value = self.hbicc.ring_multiplier.read_slice_register(
                    self.hbicc.ring_multiplier.registers.PACKET_SNAPSHOT_BOTTOM, slice=slice, index=i).value
                data.insert(0, f'{value:08X}')
                if value == 0xCAFEF00D and not Done:
                    Done = True

            if self.is_regression:
                file.write(' '.join(data) + '\n')
            total_data.append(data)
            self.hbicc.ring_multiplier.write_register_rawer(0x464, 1, slice)
            self.hbicc.ring_multiplier.write_register_rawer(0x464, 0, slice)
        if self.is_regression:
            file.close()
        return total_data

    def write_snooper_top(self, slice):
        total_data = []
        Done = False
        if self.is_regression:
            file = self.contruct_dst_dir(slice, 'top')
        while not Done:
            data = []
            for iterate in range(2):
                for i in range(4):
                    value = self.hbicc.ring_multiplier.read_slice_register(
                        self.hbicc.ring_multiplier.registers.PACKET_SNAPSHOT_TOP, index=i,
                        slice=slice).value
                    data.insert(0, f'{value:08X}')
                    if value == 0xCAFEF00D and not Done:
                        Done = True
                self.hbicc.ring_multiplier.write_register_rawer(0x4a4, 1, slice)
                self.hbicc.ring_multiplier.write_register_rawer(0x4a4, 0, slice)
            if self.is_regression:
                file.write(' '.join(data) + '\n')
            total_data.append(data)
        if self.is_regression:
            file.close()
        return total_data

    def contruct_dst_dir(self, slice, name):
        repeat_number = self.env.test.repeat_number
        dir = os.getcwd()
        test_module = self.env.test.Module()
        test_name = self.env.test.Name()
        dst = os.path.join(dir, 'TOP_BOTTOM_RM', test_module, test_name)
        os.makedirs(dst, exist_ok=True)
        file = open(os.path.join(dst, f"Repeat_{repeat_number}_slice_{slice}_rm_snooper_{name}.txt"), "w+")
        return file

    def log_xcvr(self, data, type_log):
        if all(type_log):
            log_type = 'debug'
        else:
            log_type = 'warning'
        stages = ['pg_rm_pg', 'pg_rm_rm', 'rm_pm0_rm', 'rm_pm0_pm0', 'pm0_pm1_pm0', 'pm0_pm1_pm1', 'pm1_pm0_pm1',
                  'pm1_pm0_pm0', 'pm0_rm_pm0', 'pm0_rm_rm', 'rm_pg_rm', 'rm_pg_pg']
        headers = ['Stage']
        stage_results = []
        per_slice_result = {'Stage': ''}
        for stage in stages:
            results = {'Stage': stage}
            for one in data:
                entry = one.get(stage, 'None')
                slice = one.get('Slice', 'None')
                pin = one.get('PM', 'None')
                pin = pin.split('_')[1]
                header = f'{pin}_{slice}'
                pass_fail = one.get('Result', 'None')
                if header not in headers:
                    headers.append(header)
                results.update({header: entry})
                per_slice_result.update({header: pass_fail})
            stage_results.append(results)
        stage_results.append(per_slice_result)

        table = self.hbicc.contruct_text_table(headers, stage_results,
                                               title_in='Transceiver Counters (Headers are PSDB_Slice)')
        self.Log(log_type, f'{table}')

    def set_rm_snooper(self, state):
        self.is_snooper_activated = state

    def set_xcvr_report(self, state):
        self.is_xcvr_report_activated = state

    def set_expected_ctv_data(self, expected, slices=SLICECOUNT):
        for index in slices:
            self.slice_channel_sets_combo[index].expected_ctv_data = expected

    def set_pm_subcycle(self, subcycle, slices=SLICECOUNT):
        for slice in slices:
            if slice in self.slice_channel_sets_combo.keys():
                slice_object = self.slice_channel_sets_combo[slice]
                slice_object.subcycle_exponent = subcycle

    def write_pm_subcycle(self):
        for slice_object in self.slice_channel_sets_combo.values():
            slice_object.write_pm_subcycle()

    def set_per_pattern_fail_count(self,exponent):
        for psdb, pin0, pin1 in self.hbicc.paths:
            pin0.set_per_pattern_max_fail_count(exponent)
            pin1.set_per_pattern_max_fail_count(exponent)

    def set_global_fail_count(self,exponent):
        for psdb, pin0, pin1 in self.hbicc.paths:
            pin0.set_global_max_fail_count(exponent)
            pin1.set_global_max_fail_count(exponent)


    def report_pin_state(self):
        for psdb, pin0, pin1 in self.hbicc.paths:
            for slice_index, slice_obj in self.slice_channel_sets_combo.items():
                slice_obj.report_pin_state_per_slice(pin0, pin1)

    def set_ctp(self, ctp, slices=SLICECOUNT):
        active = self.hbicc.get_active_pins_from_ctp(ctp)
        for index in slices:
            self.slice_channel_sets_combo[index].ctp_mask_list = active

    def set_start_cycle_snooper(self, padding, repeat):
        top, bottom = self.get_start_cycle(padding, repeat)
        self.Log('debug', f'TOP BOTTOM VALUEES: {top} {bottom}')
        self.start_cycle_0_address = top
        self.start_cycle_1_address = bottom

    def get_start_cycle(self, padding, repeat):
        top, bottom = 200, 450
        ctv_packets = repeat // 8
        cycle_count = (padding + repeat) // 512
        count = (padding + repeat) // 32
        eob = 3

        start_top = (ctv_packets + cycle_count + count + eob) - top
        start_bottom = (ctv_packets + cycle_count + count + eob) - bottom
        if start_bottom < 0:
            start_bottom = 450
        if start_top < 0:
            start_top = 200
        return start_top, start_bottom

    def log_status_report(self):
        self.log_xcvr_count_status_when_abort(title='Before Aborting Pattern', slices=range(0, 5))
        self.hbicc.log_xcvrs_status()
        self.hbicc.log_underrun_registers()
        self.hbicc.pat_gen.log_capture_stats_registers()

    def set_up_snapshot(self):
        if self.tos_snapshot_path:
            snapshot_helper = SnapshotHelper(self.hbicc)
            snapshot_helper.root_path = self.tos_snapshot_path
            for slice in self.active_slice_list:
                snapshot_helper.slice= slice
                snapshot_helper.load_pattern_memory_from_snapshot(slice)
            self.load_tos_snapshot_registers(snapshot_helper, list(self.slice_channel_sets_combo.keys()))

    def load_tos_snapshot_registers(self, snapshot_helper=None, slices=SLICECOUNT):
        if snapshot_helper is None:
            snapshot_helper = SnapshotHelper(self.hbicc)
            snapshot_helper.root_path = self.tos_snapshot_path

        for device in [self.hbicc.ring_multiplier, self.hbicc.psdb_0_pin_0, self.hbicc.psdb_0_pin_1,
                       self.hbicc.psdb_1_pin_0, self.hbicc.psdb_1_pin_1]:
            if device:
                for slice in slices:
                    snapshot_helper.slice = slice
                    snapshot_helper.load_registers_from_snapshot(device)

    def set_expected_ctv_header(self, expected, slices=SLICECOUNT):
        for index in slices:
            self.slice_channel_sets_combo[index].expected_ctv_header = expected

    def set_expected_traces(self, expected, slices=SLICECOUNT):
        for index in slices:
            self.slice_channel_sets_combo[index].expected_traces = expected

    def set_up_ctv_header_distribution(self):
        if configs.SPLITTER_FEATURE:
            self.distribute_bits()
            for slice_object in self.slice_channel_sets_combo.values():
                slice_object.capture.set_capture_distribution_control()

    def distribute_bits(self):
        for group, slices in self.slice_group.items():
            distribution_bits = 0
            for i in range(8):
                for slice_object in slices:
                    slice_object.ctv_header_distribution |= 1 << distribution_bits
                    distribution_bits += 1

    def distribute_bits_all_header_stream_to_one_slice(self, slice_index=1):
        for slice_object in self.slice_channel_sets_combo.values():
            if slice_object.index == slice_index:
                slice_object.ctv_header_distribution = 0xff
            else:
                slice_object.ctv_header_distribution = 0x00

    def set_memory_to_known_value(self, slices=range(0, 5), known_value=0xcc):
        self.Log('debug', f'Initializing Stream Memory to 0x{known_value:X}')
        for address in ADDRESSES:
            known_data = bytes([known_value] * (1 << 10))
            for slice in list(self.slice_channel_sets_combo.keys()):
                self.env.hbicc.pat_gen.dma_write(address, known_data, slice)
                read_back_data = self.env.hbicc.pat_gen.dma_read(address, len(known_data), slice)
                if read_back_data != known_data:
                    self.Log('warning',
                             f'Memory initialization failed for Slice {slice}, Address {address}. Retyring...')
                else:
                    self.env.hbicc.pat_gen.dma_write(address, known_data, slice)
                    read_back_data = self.env.hbicc.pat_gen.dma_read(address, len(known_data), slice)
                    if read_back_data != known_data:
                        self.Log('error', f'Memory initialization failed for Slice {slice}, Address {address}')

    def set_pattern_start_address(self, addresses):
        for slice, address in addresses.items():
            if slice in self.slice_channel_sets_combo.keys():
                self.slice_channel_sets_combo[slice].pattern_start_address = address
                self.Log('info', f'Setting address {address}')
            else:
                self.Log('error', f'Slice {slice} can not be found therefore Pattern Start Address can not be set. ')

    def stitch_ctv_headers(self):
        data = bytes(b'')
        distribution_att, highest_count = self._get_distribution_att_for_ctv_headers()
        while distribution_att[highest_count]['count'] > 0:
            for i in range(8):
                for index, slice_object in self.slice_channel_sets_combo.items():
                    if distribution_att[index]['count'] > 0:
                        if distribution_att[index]['distribution'] & (1 << i):
                            total = 2**distribution_att[index]['single_block']
                            count, one = slice_object.capture.get_ctv_header_block_count_and_bytes(total, offset=
                            distribution_att[index]['offset'])
                            data += one
                            distribution_att[index]['count'] -= total
                            distribution_att[index]['offset'] += total
        return data

    def set_simulator(self):
        self.hbicc.model.patgen.is_simulator_active = self.is_simulator_active

    def set_e32_interval(self):
        self.hbicc.pat_gen.set_e32_interval(self.e32_interval)
        for pm in self.hbicc.get_pin_multipliers():
            pm.set_e32_interval(self.e32_interval)

    def get_slice_ctv_header_distribution(self, total_header_block_num):
        header_count_distribution = {}
        if not self.active_slice_list:
            return header_count_distribution
        for slice_index, slice_object in self.slice_channel_sets_combo.items():
            header_count_distribution.update({slice_index: 0})
        count = 0
        while total_header_block_num > 0:
            for slice_index, slice_object in self.slice_channel_sets_combo.items():
                mask = 1 << count
                if mask & slice_object.ctv_header_distribution:
                    section_length = 1 << slice_object.capture.splitter_exponent
                    header_count_distribution[slice_index] += min(section_length, total_header_block_num)
                    total_header_block_num -= section_length
            count = (count + 1) % 8
        return header_count_distribution

    def get_expected_ctv_header_overflow_map(self, ctv_header_num, ctv_header_num_limit, expected_alarm=None):
        header_count_distribution = self.get_slice_ctv_header_distribution(ctv_header_num)
        expected_ctv_header_overflow_alarm_map = {}
        for slice_index, count in header_count_distribution.items():
            if expected_alarm is None:
                expected_ctv_header_overflow_alarm_map.update({slice_index: count >= ctv_header_num_limit})
            else:
                expected_ctv_header_overflow_alarm_map.update({slice_index: expected_alarm})

        return expected_ctv_header_overflow_alarm_map

    def check_ctv_header_capture_size(self, total_count):
        expected_count_map = self.get_slice_ctv_header_distribution(total_count)
        for slice_index, slice_object in self.slice_channel_sets_combo.items():
            capture = slice_object.capture
            observed_count = capture.get_stream_total_size(capture.patgen, patgen_register.CTV_CAPTURE_HEADER_COUNT)
            if observed_count != expected_count_map[slice_index]:
                self.Log('error', f'slice {slice_object.index} has {observed_count} ctv header blocks '
                                  f'!= {expected_count_map[slice_index]} as expected')


PIN_DIRECTION = {'LOW': 0x0000, 'HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE': 0xFFFF}


class Slice(PatternHelper):
    def __init__(self, pattern_helper, hbicc, pattern, fixed_drive_state, slice, channel_sets, group=0):
        self.index = slice
        self.channel_sets = channel_sets
        self.pattern = pattern
        self.hbicc = hbicc
        self.fixed_drive_state = fixed_drive_state
        self.pattern_helper = pattern_helper
        self.env = pattern_helper.env
        self.capture = None
        self.expected_ctv_data = None
        self.ctp_mask_list = None
        self.expected_ctv_header = None
        self.pattern_byte_object = None
        self.ctv_header_distribution = 0b0
        self.pattern_start_address = None
        self.pin_states= {}
        self.ctv_collective_data = 0
        self.address_and_pattern_list = {}
        self.address_and_pattern_object_list = {}
        self.expect_match = True
        self.fail_signature = None
        self.group = group
        self.subcycle_exponent = 0

    def write_pm_subcycle(self):
        for psdb, pin0, pin1 in self.hbicc.paths:
            pin0.set_subcycles_per_cycle_exp(self.subcycle_exponent, slices=[self.index])
            pin1.set_subcycles_per_cycle_exp(self.subcycle_exponent, slices=[self.index])

    def set_expect_match(self, expect_match = True):
        self.expect_match = expect_match

    def set_expected_fail_signature(self, fail_signature):
        self.fail_signature = fail_signature

    def pattern_and_pattern_list_convert(self):
        if len(self.pattern):
            if(len(self.address_and_pattern_list)):
                if (len(self.address_and_pattern_list)>1) | (list(self.address_and_pattern_list.values)[0] != self.pattern) | (list(self.address_and_pattern_list.keys())[0] != 0):
                    self.Log('info', f'??? pattern list item numbner {len(self.address_and_pattern_list)}')
                    self.Log('info', 'pattern content')
                    self.Log('info', f'{self.pattern}')
                    self.Log('info', f'pattern list first item is address {list(self.address_and_pattern_list.keys())[0]}')
                    self.Log('info', f'{list(self.address_and_pattern_list.values)[0]}')
            self.address_and_pattern_list = {}
            self.address_and_pattern_list.update({0:self.pattern})
            self.pattern = ''

    def write_pattern_list_to_memory(self):
        self.pattern_and_pattern_list_convert()
        address_and_pattern_object_list = self.generate_pattern_list_byte_object()
        address_and_pattern_size_list = {}
        for address, pattern_byte_object in address_and_pattern_object_list.items():
            self.hbicc.pat_gen.dma_write(address, pattern_byte_object, self.index)
            address_and_pattern_size_list.update({address: len(pattern_byte_object)})
        self.hbicc.pat_gen.pattern_list_bytes(address_and_pattern_size_list)

    def generate_pattern_list_byte_object(self):
        self.Log('debug', f'Assembler Generating Pattern and DMA')
        start_timer = time.time()

        # if self.pattern_helper.single_clock_domain:
        #     pattern_list_byte_object = self.get_pattern_list_from_other_slice()
        #     if pattern_list_byte_object:
        #         return pattern_list_byte_object

        self.address_and_pattern_object_list = {}
        for key, value in self.address_and_pattern_list.items():
            asm = HbiPatternAssembler(startAddress=(key // 16))
            asm.LoadString(value)
            pattern_byte_object = asm.Generate()
            self.address_and_pattern_object_list.update({key: pattern_byte_object})
        end_timer = time.time()
        self.Log('debug', f'Slice {self.index} Pattern Generation took: {round(end_timer - start_timer, 0)} seconds')
        return self.address_and_pattern_object_list

    def get_pattern_list_from_other_slice(self):
        if self.pattern_helper.single_clock_domain:
            for slice in self.pattern_helper.slice_channel_sets_combo.values():
                if slice.address_and_pattern_object_list:
                    return slice.address_and_pattern_object_list
        return bytes(b'')


    def drive_fixed_state_on_pins(self, psdb_target):
        if self.pattern_helper._is_set_and_check_pin_state:
            self.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target)
            for channel_set in self.channel_sets:
                payload_11, payload_8, payload_16 = self.pattern_helper.fix_drive_state.get_payloads(channel_set, self.index)
                for i in range(FIFO_PADDING):
                    self.hbicc.ring_multiplier.set_pin_direction(psdb_target, self.index, payload_8,
                                                                 payload_16, payload_11, channel_set)
            self.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target)

    def drive_fixed_state_on_pins_dma(self, psdb_target):
        for channel_set in self.channel_sets:
            if self.pattern_helper.user_mode:
                payload_last_11_pins, payload_8_pins, payload_16_pins = self.pattern_helper.fix_drive_state.get_payloads(channel_set, self.index)
            else:
                if channel_set in [0, 1, 2, 3]:
                    payload_8_pins, payload_16_pins, payload_last_11_pins = self.generate_payload_bits('HIGH')
                elif channel_set in [4, 5, 6, 7]:
                    payload_8_pins, payload_16_pins, payload_last_11_pins = self.generate_payload_bits('LOW')
            self.hbicc.ring_multiplier.set_pin_direction(psdb_target, self.index, payload_8_pins,
                                                         payload_16_pins, payload_last_11_pins, channel_set, dma=True)

    def drive_fixed_mask_on_pins_dma(self, psdb_target):
        for channel_set in self.channel_sets:
            if self.pattern_helper.user_mode:
                payload_last_11_pins, payload_8_pins, payload_16_pins = self.pattern_helper.fix_drive_state.get_maskall_payloads(channel_set, self.index)
            else:
                if channel_set in [0, 1, 2, 3]:
                    payload_8_pins, payload_16_pins, payload_last_11_pins = self.generate_payload_bits('HIGH')
                elif channel_set in [4, 5, 6, 7]:
                    payload_8_pins, payload_16_pins, payload_last_11_pins = self.generate_payload_bits('LOW')
            self.hbicc.ring_multiplier.set_pin_direction(psdb_target, self.index, payload_8_pins,
                                                         payload_16_pins, payload_last_11_pins, channel_set, dma=True, spi_target=1)

    def generate_payload_bits(self, state=None):
        if state is None:
            payload_data = PIN_DIRECTION[self.fixed_drive_state]
        else:
            payload_data = PIN_DIRECTION[state]
        payload_16_pins = payload_data << 16 | payload_data
        payload_8_pins = payload_data
        payload_last_11_pins = payload_16_pins
        return payload_8_pins,payload_16_pins,payload_last_11_pins

    def generate_active_slice_mask(self):
        active_slice_group_mask = 0
        for slice_index in self.pattern_helper.active_slice_list:
            active_slice_group_mask = (1 << slice_index) | active_slice_group_mask
        return active_slice_group_mask

    def perform_pattern_prestage(self, pms,mask_dict):
        for pm in pms:
            pm.set_active_slice(self.index, mask_dict[self.group])
        self.hbicc.pat_gen.set_active_slice(self.index, mask_dict[self.group], self.hbicc)

        if self.pattern_helper.inactive_slice_list:
            for pm in pms:
                pm.mask_inactive_slice(self.pattern_helper.inactive_slice_list)
            self.hbicc.pat_gen.mask_inactive_slice(self.pattern_helper.inactive_slice_list)

    def get_error_count_data(self):
        data = []
        for pm in self.hbicc.get_pin_multipliers():
            active_channel_sets = list(set(pm.channel_sets()) & set(self.channel_sets))
            data.extend(self.compare_error_count_on_channel_sets(pm, active_channel_sets))
        return data

    def compare_error_count_on_channel_sets(self, pm, channel_sets):
        data = []
        error_tally = []
        for channel_set in channel_sets:
            pm_channel_set = channel_set % 4
            dict = {}
            dict.update({'Channel Set': pm_channel_set, 'Slice': self.index, 'PM': pm.name()})
            error_counts = pm.channel_error_counts(pm_channel_set, self.index)
            channel_set = self.hbicc.model.psdb[pm.slot].pin_multiplier[pm.fpga_index].slices[self.index].channel_set_pins[pm_channel_set]
            pin_ignore_list = pm.get_pin_ignores(pm_channel_set, self.index)
            for channel, error_count in enumerate(error_counts):
                if channel in pin_ignore_list:
                    dict.update({f'pin_{channel}': 'PhyLite'})
                else:
                    dict.update({f'pin_{channel}': error_count})
                    if self.expect_match:
                        expected_error_count = channel_set.fail_count(channel)
                    else:
                        expected_error_count = self.fail_signature[channel]
                    if error_count != expected_error_count:
                        error_tally.append({'PM': pm.slot_index, 'Channel Set': pm_channel_set, 'Slice': self.index, 'Pin': channel, 'Expected': expected_error_count, 'Observed': error_count})
            data.append(dict)
        if error_tally:
            table = self.hbicc.contruct_text_table(data=error_tally, title_in='Simulations Expected Pin Error Count vs Observed')
            self.Log('error', f'{table}')
        return data

    def base_xcvrs(self, psdb, pg, rm, pin_0, pin_1):
        pg_rm_rm = rm.read_slice_register(rm.registers.PATGEN_XCVR_RX_PATTERN_COUNTER, self.index).value
        rm_pm0_rm = rm.read_slice_register(rm.registers.PSDB0_XCVR_TX_PATTERN_COUNTER, self.index).value
        rm_pm0_pm0 = pin_0.read_slice_register(pin_0.registers.RM_RING_BUS_RX_PATTERN_COUNT, self.index).value
        pm0_pm1_pm0 = pin_0.read_slice_register(pin_0.registers.PM_RING_BUS_TX_PATTERN_COUNT, self.index).value
        pm0_pm1_pm1 = pin_1.read_slice_register(pin_1.registers.PM_RING_BUS_RX_PATTERN_COUNT, self.index).value
        pm1_pm0_pm1 = pin_1.read_slice_register(pin_1.registers.PM_RING_BUS_TX_PATTERN_COUNT, self.index).value
        pm1_pm0_pm0 = pin_0.read_slice_register(pin_0.registers.PM_RING_BUS_RX_PATTERN_COUNT, self.index).value
        pm0_rm_pm0 = pin_0.read_slice_register(pin_0.registers.RM_RING_BUS_TX_PATTERN_COUNT_LOWER, self.index).value
        pm0_rm_rm = rm.read_slice_register(rm.registers.PSDB0_XCVR_RX_PATTERN_COUNTER, self.index).value
        rm_pg_rm = rm.read_slice_register(rm.registers.PATGEN_XCVR_TX_PATTERN_COUNTER, self.index).value
        pg_rm_pg = pg.read_slice_register(pg.registers.PG_TO_RM_AT_PG, self.index).value
        rm_pg_pg = pg.read_slice_register(pg.registers.RM_TO_PG_AT_PG, self.index).value

        self.pattern_helper.xcvr_counters[psdb][self.index].update(
            {
                'pg_rm_rm': pg_rm_rm,
                'rm_pm0_rm': rm_pm0_rm,
                'rm_pm0_pm0': rm_pm0_pm0,
                'pm0_pm1_pm0': pm0_pm1_pm0,
                'pm0_pm1_pm1': pm0_pm1_pm1,
                'pm1_pm0_pm1': pm1_pm0_pm1,
                'pm1_pm0_pm0': pm1_pm0_pm0,
                'pm0_rm_pm0': pm0_rm_pm0,
                'pm0_rm_rm': pm0_rm_rm,
                'rm_pg_rm': rm_pg_rm,
                'pg_rm_pg': pg_rm_pg,
                'rm_pg_pg': rm_pg_pg,
            })

    def rebase_xcvr_count(self, count, link, psdb):
        base = self.pattern_helper.xcvr_counters[psdb][self.index][link]
        return count - base

    def read_different_counter_registers(self, psdb, pg, rm, pin_0, pin_1, msg='', title=''):
        pg_rm_rm = rm.read_slice_register(rm.registers.PATGEN_XCVR_RX_PATTERN_COUNTER, self.index).value
        pg_rm_rm = self.rebase_xcvr_count(pg_rm_rm, 'pg_rm_rm', psdb)
        rm_pm0_rm = rm.read_slice_register(rm.registers.PSDB0_XCVR_TX_PATTERN_COUNTER, self.index).value
        rm_pm0_rm = self.rebase_xcvr_count(rm_pm0_rm, 'rm_pm0_rm', psdb)
        rm_pm0_pm0 = pin_0.read_slice_register(pin_0.registers.RM_RING_BUS_RX_PATTERN_COUNT, self.index).value
        rm_pm0_pm0 = self.rebase_xcvr_count(rm_pm0_pm0, 'rm_pm0_pm0', psdb)
        pm0_pm1_pm0 = pin_0.read_slice_register(pin_0.registers.PM_RING_BUS_TX_PATTERN_COUNT, self.index).value
        pm0_pm1_pm0 = self.rebase_xcvr_count(pm0_pm1_pm0, 'pm0_pm1_pm0', psdb)
        pm0_pm1_pm1 = pin_1.read_slice_register(pin_1.registers.PM_RING_BUS_RX_PATTERN_COUNT, self.index).value
        pm0_pm1_pm1 = self.rebase_xcvr_count(pm0_pm1_pm1, 'pm0_pm1_pm1', psdb)
        pm1_pm0_pm1 = pin_1.read_slice_register(pin_1.registers.PM_RING_BUS_TX_PATTERN_COUNT, self.index).value
        pm1_pm0_pm1 = self.rebase_xcvr_count(pm1_pm0_pm1, 'pm1_pm0_pm1', psdb)
        pm1_pm0_pm0 = pin_0.read_slice_register(pin_0.registers.PM_RING_BUS_RX_PATTERN_COUNT, self.index).value
        pm1_pm0_pm0 = self.rebase_xcvr_count(pm1_pm0_pm0, 'pm1_pm0_pm0', psdb)
        pm0_rm_pm0 = pin_0.read_slice_register(pin_0.registers.RM_RING_BUS_TX_PATTERN_COUNT_LOWER, self.index).value
        pm0_rm_pm0 = self.rebase_xcvr_count(pm0_rm_pm0, 'pm0_rm_pm0', psdb)
        pm0_rm_rm = rm.read_slice_register(rm.registers.PSDB0_XCVR_RX_PATTERN_COUNTER, self.index).value
        pm0_rm_rm = self.rebase_xcvr_count(pm0_rm_rm, 'pm0_rm_rm', psdb)
        rm_pg_rm = rm.read_slice_register(rm.registers.PATGEN_XCVR_TX_PATTERN_COUNTER, self.index).value
        rm_pg_rm = self.rebase_xcvr_count(rm_pg_rm, 'rm_pg_rm', psdb)
        pg_rm_pg = pg.read_slice_register(pg.registers.PG_TO_RM_AT_PG, self.index).value
        pg_rm_pg = self.rebase_xcvr_count(pg_rm_pg, 'pg_rm_pg', psdb)
        rm_pg_pg = pg.read_slice_register(pg.registers.RM_TO_PG_AT_PG, self.index).value
        rm_pg_pg = self.rebase_xcvr_count(rm_pg_pg, 'rm_pg_pg', psdb)

        if all([pg_rm_pg == pg_rm_rm, rm_pm0_rm == rm_pm0_pm0, pm0_pm1_pm0 == pm0_pm1_pm1, pm1_pm0_pm1 == pm1_pm0_pm0,
                pm0_rm_pm0 == pm0_rm_rm, rm_pg_rm == rm_pg_pg]):
            result = 'PASS'
            log_type = True
        else:
            result = 'FAIL'
            log_type = False

        data = {'Slice': self.index,
                'pg_rm_pg': pg_rm_pg, 'pg_rm_rm': pg_rm_rm,
                'rm_pm0_rm': rm_pm0_rm, 'rm_pm0_pm0': rm_pm0_pm0,
                'pm0_pm1_pm0': pm0_pm1_pm0, 'pm0_pm1_pm1': pm0_pm1_pm1,
                'pm1_pm0_pm1': pm1_pm0_pm1, 'pm1_pm0_pm0': pm1_pm0_pm0,
                'pm0_rm_pm0': pm0_rm_pm0, 'pm0_rm_rm': pm0_rm_rm,
                'rm_pg_rm': rm_pg_rm, 'rm_pg_pg': rm_pg_pg,
                'Test': f'{self.env.test.Name():>30}',
                'Run': os.getcwd(), 'Result': result,
                'PM': pin_0.name()}
        return data, log_type

    def set_mask(self, reg_index, pin0, pin1, mask):
        pin0.set_mtv_fail_mask(self.index, self.channel_sets, reg_index, mask)
        pin1.set_mtv_fail_mask(self.index, self.channel_sets, reg_index, mask)

    def is_calibration(self):
        calibrated_slices = self.hbicc.pat_gen.check_ddr_calibration_status(slice_list=[self.index])
        if self.index not in calibrated_slices:
            return False
        return True

    def report_pin_state_per_slice(self, pin0, pin1):
        if pin0:
            active_channel_sets = list(set([0, 1, 2, 3]) & set(self.channel_sets))
            pin_state_high, pin_stae_low = pin0.detect_logic_state(channel_sets=active_channel_sets, slice=self.index,
                                                                   state=self.fixed_drive_state)
            self.Log('debug', ' {}-->Slice :{} High Pins:{} Low pins:{}'.format(pin0.name(), self.index, pin_state_high,
                                                                               pin_stae_low))
        if pin1:
            active_channel_sets = list(set([4, 5, 6, 7]) & set(self.channel_sets))
            pin_state_high, pin_stae_low = pin1.detect_logic_state(channel_sets=active_channel_sets, slice=self.index,
                                                                   state=self.fixed_drive_state)
            self.Log('debug', '{}-->Slice :{}  High Pins:{} Low pins:{}'.format(pin1.name(), self.index, pin_state_high,
                                                                               pin_stae_low))


    def get_user_mode_payload_bits(self, channel_set_index):
        if (self.pattern_helper.user_mode=='RELEASE'):
            payload_8_pins=int('1111111111111111',2)
            payload_16_pins=int('11111111111111111111111111111111',2)
            payload_last_11_pins=int('1111111111111111111111',2)
        elif self.pattern_helper.user_mode == 'TRISTATE':
            payload_8_pins, payload_16_pins, payload_last_11_pins = self.generate_payload_bits(state='TRISTATE')
        else:
            payload_8_pins, payload_16_pins, payload_last_11_pins = self.get_payload(channel_set_index)
        return payload_8_pins, payload_16_pins, payload_last_11_pins

    def get_random_pin_fixed_drive_state(self, channel_set_index):
        random_pin_states = []
        channel_set_pin_state = {}
        for channel in range(35):
            if self.pattern_helper.user_mode == 'RANDOM':
                random_state = random.getrandbits(1)
            elif self.pattern_helper.user_mode == 'HIGH':
                random_state = 1
            elif self.pattern_helper.user_mode == 'LOW':
                random_state = 0
            elif type(self.pattern_helper.user_mode) == int:
                random_state = (self.pattern_helper.user_mode >> channel) & 1
            channel_set_pin_state.update({channel:random_state})
            zero_fill_state = str(random_state).zfill(2)
            random_pin_states.append(zero_fill_state)
        self.pin_states.update({channel_set_index:channel_set_pin_state})
        return random_pin_states

    def get_payload(self, channel_set_index):
        random_states = self.get_random_pin_fixed_drive_state(channel_set_index)
        random_states.reverse()
        payload_8_pins =''
        payload_16_pins =''
        payload_last_11_pins = ''

        for pin_state in random_states[0:11]:
            payload_last_11_pins = payload_last_11_pins + pin_state
        for pin_state in random_states[11:19]:
            payload_8_pins = payload_8_pins +pin_state
        for pin_state in random_states[19:36]:
            payload_16_pins = payload_16_pins +pin_state

        payload_8_pins = (int(payload_8_pins,2))
        payload_16_pins = (int(payload_16_pins,2))
        payload_last_11_pins = (int(payload_last_11_pins,2))

        return payload_8_pins,payload_16_pins,payload_last_11_pins

    def write_pattern_list_start_address(self):
        if self.pattern_start_address is not None:
            address=self.pattern_start_address
            # pattern_start = self.hbicc.pat_gen.registers.PATTERN_START(value=self.pattern_start_address)
            # self.hbicc.pat_gen.write_slice_register(pattern_start, slice=self.index)
        else:
            self.pattern_and_pattern_list_convert()
            address = (list(self.address_and_pattern_list.keys())[0])//16
        self.hbicc.pat_gen.set_pattern_start_address(address, self.index)

    def set_drive_low_state(self, psdb_target):
        if self.pattern_helper._is_set_and_check_pin_state:
            payload_8_pins, payload_16_pins, payload_last_11_pins =self.generate_payload_bits(state='LOW')
            self.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target)
            for channel_set in self.channel_sets:
                channel_set_index = channel_set
                channel_set = channel_set % 8
                for i in range(FIFO_PADDING):
                    self.hbicc.ring_multiplier.set_pin_direction(psdb_target, self.index, payload_8_pins,
                                                                 payload_16_pins, payload_last_11_pins, channel_set)
            self.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target)


import re
class SnapshotHelper(PatternHelper):
    def __init__(self,hbicc):
        self.hbicc = hbicc
        self.load_registers = False
        self.root_path = None
        self.slice = None
        self.hbicc_dir = "HBICC"
        self.instrument_dir = {'PATGEN':'PG_FPGA','RINGMULTIPLIER':'RM_FPGA','PSDB_0_PIN_0':'PSDB0\PM_FPGA0',
                               'PSDB_0_PIN_1': 'PSDB0\PM_FPGA1','PSDB_1_PIN_0':'PSDB1\PM_FPGA0','PSDB_1_PIN_1':'PSDB1\PM_FPGA1'}

    def get_instrument_register_snapshot_dir(self, board):
        device_dir = self.instrument_dir[board.name()]
        filename = os.path.join(self.root_path, self.hbicc_dir,device_dir,"Slice{}".format(self.slice), "registers.csv")
        return filename

    def get_pattern_memory_snapshot_dir(self):
        device_dir = self.instrument_dir[self.hbicc.pat_gen.name()]
        filename = os.path.join(self.root_path, self.hbicc_dir,device_dir,"Slice{}".format(self.slice), "PatternMemory")
        return filename

    def load_registers_from_snapshot(self,board):
        register_file_path = self.get_instrument_register_snapshot_dir(board)
        if not os.path.isfile(register_file_path):
            raise FileNotFoundError(register_file_path)
        self.hbicc.Log('info', 'Loading registers from \'{}\''.format(register_file_path))
        fin = open(register_file_path, 'r')
        firstLine = True
        for row in csv.reader(fin):
            if firstLine:
                firstLine = False
                continue
            offset = int(str(row[0]), 0)
            data = int(str(row[1]), 0)
            board.write_register_direct(self.slice,offset,data)
        fin.close()

    def load_pattern_memory_from_snapshot(self, slice):
        # dummy_transaction_data = bytes(2<<10)
        pattern_memory_dir = self.get_pattern_memory_snapshot_dir()
        self.hbicc.Log('debug', 'Loading pattern memory from \'{}\''.format(pattern_memory_dir))
        PATTERN_FILE_REGEX = re.compile('Pattern_Type(0x.*)_Id(0x.*)_Offset(0x.*?).bin')
        if not os.path.isdir(pattern_memory_dir):
            raise Exception('\'{}\' is not a valid directory'.format(pattern_memory_dir))
        count = 0
        subfiles = next(os.walk(pattern_memory_dir))[2]  # Immediate child files
        for subfile in subfiles:
            filename = os.path.join(pattern_memory_dir, subfile)
            if os.path.isfile(filename) and filename.endswith('.bin'):
                m = PATTERN_FILE_REGEX.match(subfile)
                if m:
                    groups = m.groups()
                    patternType = int(groups[0], 0)
                    patternId = int(groups[1], 0)
                    patternOffset = int(groups[2], 0)
                    if patternType in [0x0, 0x1,0x3,0x4]:  # 0x0 = Pattern, 0x1 = Plist, 0x2 = Capture
                        self.Log('info', 'Loading {} to 0x{:x}'.format(subfile, patternOffset))
                        fin = open(filename, 'rb')
                        # self.hbicc.pat_gen.dma_write(patternOffset*16, dummy_transaction_data, slice)
                        self.hbicc.pat_gen.dma_write(patternOffset*16,fin.read(), slice)
                        fin.close()
                        count = count + 1
        self.Log('info', 'Loaded {} binary objects'.format(count))

CHANNEL_SETS = range(16)
MASK_RANGE = range(32)
PMS_INDEX = range(4)

class MtvMaskBuilder:

    def __init__(self, hbicc):
        self.hbicc = hbicc
        self.masks = {}
        self.default_mask = 0
        self._initialize_mtv_mask()

    def _initialize_mtv_mask(self):
        self.masks = {channel_set: {slice: {x: self.default_mask
                                     for x in MASK_RANGE}
                             for slice in SLICECOUNT}
                       for channel_set in CHANNEL_SETS}

    def modify_mask(self, value=None, mask_index=MASK_RANGE, channel_sets=CHANNEL_SETS, slices=SLICECOUNT):
        for channel_set in channel_sets:
            for slice in slices:
                for index in mask_index:
                    if value is None:
                        value1 = random.randint(0, 0x7ffffffff)
                        self.masks[channel_set][slice][index] = value1
                    else:
                        self.masks[channel_set][slice][index] = value

    def set_mtv_mask(self):
        for pm in self.hbicc.get_pin_multipliers():
            channel_set_base = pm.slot_index * 4
            for offset in range(4):
                channel_set = channel_set_base + offset
                for slice, mask_values in self.values[channel_set].items():
                    for index, mask in mask_values.items():
                        pm.set_mtv_fail_mask(slice,[channel_set] , index, mask)

    def randomize_mask(self):
        self.modify_mask()

    def restore_default_mtv_mask(self):
        self.modify_mask(0)
        self.set_mtv_mask()

    @property
    def values(self):
        return self.masks

    @property
    def string_values(self):
        values = ''
        for channel_set in CHANNEL_SETS:
            for slice in SLICECOUNT:
                mask_value = self.masks[channel_set][slice][0]
                values += f', CHANNEL_SET{channel_set}={mask_value}'
        return values

import random
import unittest


class FixDriveStateBuilder:
    def __init__(self, slices, channel_sets, initial_state=None, initial_mask=None):
        self.states = {}
        self.masks = {}
        self.active_slices = slices
        self.active_channel_sets = channel_sets
        self.states_after_masking = {dut: {slice: 0 for slice in range(5)} for dut in range(16)}
        self.initial_state = initial_state
        self.initial_mask = initial_mask
        self.psdb_slice = -1
        self.psdbslice_highpin = 0
        self._initialize()

    def _initialize(self):
        self.states = {dut: {slice: self._get_initial_state(slice) for slice in range(5)} for dut in range(16)}
        self.masks = {dut: {slice: self._get_initial_mask(slice) for slice in range(5)} for dut in range(16)}
        self._update_states_after_masking(self.active_slices, self.active_channel_sets)
        if self.initial_state is None:
            self.set_state_value('high', channel_sets=range(4))
            self.set_state_value('high', channel_sets=range(8, 12))

    def _get_initial_state(self, slice):
        if self.initial_state is None:
            state_value = {'long': 0, 'short': 0}
        elif isinstance(self.initial_state, int):
            # self._raise_exception(self.initial_state)
            # PSDBLoopback cases
            if self.psdb_slice < 0:
                self.psdb_slice = self.initial_state >> (18*4)
                self.initial_state &= 0X3FFFFFFFFFFFFFFFFF
                temp_state = self.initial_state
                while ((temp_state & 0x000000000000000003) == 0):
                    temp_state >>= 2
                    self.psdbslice_highpin += 1
                if ((temp_state & 0x000000000000000003) != 1):
                    self._raise_exception(self.initial_state)
            state_value = {'long': 0, 'short': 0}
            if slice == self.psdb_slice:
                state = self.initial_state
                short_value = self._collapse_long_value(state)
                state_value = {'long': state, 'short': short_value}
            if slice == (self.psdb_slice + 1):
                if (self.psdbslice_highpin == 34):
                    state = 0x000000000000000002
                    short_value = self._collapse_long_value(state)
                    state_value = {'long': state, 'short': short_value}
        else:
            state_value = self._handle_str_init_state_value(self.initial_state, slice)
        return state_value

    def _get_initial_mask(self, slice):
        if self.initial_mask is None:
            mask_value = 0
        elif isinstance(self.initial_mask, int):
            self._raise_exception(self.initial_mask)
        else:
            mask_value = self._handle_str_mask_value(self.initial_mask, slice)
        return mask_value

    def constrained_34pins_value_random(self):
        state_value = 0
        for index in range(0, 34, 2):
            bit = random.getrandbits(1)
            if(bit):
                state_value += (0x5 << index*2)
        return state_value

    def constrained_34pins_mask_random(self):
        state_value = 0
        for index in range(0, 34, 2):
            bit = random.getrandbits(1)
            if(bit):
                state_value += (0x3 << index)
        return state_value

    def _handle_str_init_state_value(self, str_state, slice=0):
        str_state = str_state.upper()
        state_value = {'LOW': 0,
                       'HIGH': 0x155555555555555555,
                       'TRISTATE': 0x2AAAAAAAAAAAAAAAAA,
                       'RELEASE': 0x3FFFFFFFFFFFFFFFFF,
                       'RANDOM': self.constrained_34pins_value_random(), #random.randint(0, 0x3FFFFFFFFFFFFFFFFF) & 0x155555555555555555,
                       'HIGH_LOOPBACK': 0x199999999999999999,
                       '': 0}
        state = state_value.get(str_state, None)
        if(slice%2 & (str_state == 'RANDOM')):
            state = state << 2
        if (slice%2 & (str_state == 'HIGH_LOOPBACK')):
            state = 0x266666666666666666
        if state is None:
            self._raise_exception(str_state)
        short_value = self._collapse_long_value(state)
        return {'long': state, 'short': short_value}

    def _handle_one_str_init_state_value(self, str_state):
        str_state = str_state.upper()
        state_value = {'LOW': 0b00,
                       'HIGH': 0b01,
                       'TRISTATE': 0b10,
                       'RELEASE': 0b11
                       #'PATTERN': 0b11
                       }
        state = state_value.get(str_state, None)
        if state is None:
            self._raise_exception(str_state)
        return state

    def _handle_str_mask_value(self, str_mask, slice):
        str_mask = str_mask.upper()
        mask_value = {'MASKNON': 0,
                      'MASKALL': 0x7FFFFFFFF,
                      'MASKRANDOM': self.constrained_34pins_mask_random(),
                      '': 0}
        mask = mask_value.get(str_mask, None)
        if (slice %2 & (str_mask == 'MASKRANDOM')):
            mask = mask << 1
        return mask

    def _update_states_after_masking(self, slices, channel_sets):
        for channel_set in channel_sets:
            for slice in slices:
                state_with_masking_original = self.states_after_masking[channel_set][slice]
                state_new = self.states[channel_set][slice]['short']
                mask = self.masks[channel_set][slice]
                mask_bar = mask ^ 0x7FFFFFFFF
                state_with_masking_new = (state_with_masking_original & mask) | (state_new & mask_bar)
                self.states_after_masking[channel_set][slice] = state_with_masking_new

    def _raise_exception(self, state):
        raise Exception(f'Unsupported initial state {state}')

    def set_state_value(self, new_state, channel_sets=CHANNEL_SETS, slices=range(5), pins=None):
        if new_state is 'MASK':
            pass
        elif isinstance(new_state, int):
            self._raise_exception(new_state)
        else:
            for channel_set in channel_sets:
                for slice in slices:
                    if pins is None:
                        self.states[channel_set][slice] = self._handle_str_init_state_value(new_state, slice)
                    else:
                        pin_value = self._handle_one_str_init_state_value(new_state)
                        for pin in pins:
                            tmp = self.states[channel_set][slice]['long'] & (0x3FFFFFFFFFFFFFFFFF ^ (0b11 << pin * 2))
                            self.states[channel_set][slice]['long'] = tmp | (pin_value << pin * 2)
                        self.states[channel_set][slice]['short'] = self._collapse_long_value(self.states[channel_set][slice]['long'])
            self._update_states_after_masking(slices, channel_sets)

    def set_mask_value(self, mask_type, channel_sets=CHANNEL_SETS, slices=range(5), pins=None):
        if isinstance(mask_type, int):
            self._raise_exception(mask_type)
        else:
            for channel_set in channel_sets:
                for slice in slices:
                    self.masks[channel_set][slice] = self.mask_value_assignment(mask_type, slice, pins)

    def mask_value_assignment(self, mask_type, slice, pins):
        whole_pins_mask_set = {'MASKNON', 'MASKALL', 'MASKRANDOM'}
        select_pins_mask = 'MASKPIN'
        pins_mask_value = 0
        if mask_type in whole_pins_mask_set:
            pins_mask_value = self._handle_str_mask_value(mask_type, slice)
        elif mask_type is select_pins_mask:
            if pins:
                for pin in pins:
                    pins_mask_value = pins_mask_value | (0b1 << pin)
        else: self._raise_exception(mask_type)
        return pins_mask_value

    def __str__(self):
        for channel_set, slices in self.states.items():
            for slice_number, value in slices.items():
                long_value = value["long"]

    def get_payloads(self, channel_set, slice):
        payload_16 = self.states[channel_set][slice]['long'] & 0xFFFFFFFF
        payload_8 = (self.states[channel_set][slice]['long'] >> 16 * 2) & 0xFFFF
        payload_11 = (self.states[channel_set][slice]['long'] >> (16 + 8) * 2) & 0x3FFFFF
        return payload_11, payload_8, payload_16

    def get_maskall_payloads(self, channel_set, slice):
        payload_16 = self.masks[channel_set][slice] & 0xFFFFFF
        payload_8 = 0
        payload_11 = (self.masks[channel_set][slice] >> (16 + 8)) & 0x7FF
        return payload_11, payload_8, payload_16

    def get_state_by_pins(self, channel_set, slice, pin):
        if self.initial_state == 'HIGH_LOOPBACK':
            return 1
        if self.psdb_slice >= 0:
            if (slice == self.psdb_slice) & (pin == (self.psdbslice_highpin + 1)):
                return 1
            if (self.psdbslice_highpin == 34):
                if ((slice == (self.psdb_slice + 1)) & (pin == 0)):
                    return 1
        return (self.states_after_masking[channel_set][slice] >> pin) & 1

    def _set_state_with_35_bits(self):
        for channel_set, slice in self.states.items():
            for slice_number, values in slice.items():
                long_value = self.states[channel_set][slice_number]['long']
                self.states[channel_set][slice_number]['short'] = self._collapse_long_value(long_value)

    def _collapse_long_value(self, value):
        final = 0
        for i in range(0, 70, 2):
            x = (value >> i) & 1
            final |= x << (i // 2)
        return final
