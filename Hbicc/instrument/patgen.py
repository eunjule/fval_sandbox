# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
import csv
import os
import sys
import time

from Common import configs
from Common import hilmon as hil
from Common import pcie
from Common import stratix10
from Common.instruments.hbi_instrument import HbiInstrument
from ThirdParty.SASS.suppress_sensor import suppress_sensor
from . import patgen_register
from Tools import projectpaths
import zipfile

SLICECOUNT = range(0, 5)
PER_DIMM_MEMORY_SIZE = 32 << 30  # 32gb 2**5 * 2**30
TOTAL_MEMORY_SIZE = 160 << 30  # 32gb 2**5 * 2**30
NO_OF_DIMMS = 5

slice_offset_64_GB = {0: 0x0, 1: 0x1000000000, 2: 0x2000000000, 3: 0x3000000000, 4: 0x4000000000}
# slice_offset_64_GB = {0: 0x0, 1: 0x300000000, 2: 0x600000000, 3: 0x900000000, 4: 0x4000000000}

STREAM_BLOCK_SIZE = 32


class PatGen(HbiInstrument):

    def __init__(self, rc):
        self.rc = rc
        if rc and rc.mainboard:
            self.mainboard = rc.mainboard
        else:
            self.mainboard = None
        self.fpga_index = 1  # hardcode
        self.registers = patgen_register
        self.fpga_load = configs.HBIPATGEN_FPGA_IMAGE_LOAD
        self.dimm_size = 64 << 30
        self.ila_names = {}
        self.pattern_size_in_bytes = 0
        self.address_and_pattern_size_list = {}
        self.active_slices = []

    def initialize(self):
        self.load_fpga()
        self.log_cpld()  #
        self.log_fpga_version()  #
        self.log_build()  #

    def fpga_image_path(self):
        if configs.HBICC_IN_HDMT:
            fab_letter = configs.HBICC_IN_HDMT_FAB
            self.warn_user_fab_detection_is_bypassed(fab_letter)
        else:
            fab_letter = self.mainboard.blt.fab_letter()
        return getattr(configs, f'HBIPATGEN_FPGA_IMAGE_FAB_{fab_letter}')

    def load_fpga_using_hil(self):
        num_retries = 5
        with suppress_sensor('FVAL PatGen', 'HBI_PATGEN'):
            for retry in range(num_retries):
                try:
                    hil.hbiMbDeviceDisable(self.fpga_index)
                    hil.hbiMbFpgaLoad(self.fpga_index, self.fpga_image_path())
                    self.enable_device()
                    self.Log('info', f'{self.name()} FPGA Version: '
                                     f'{self.fpga_version_string()}')
                    break
                except RuntimeError as e:
                    self.Log('warning',
                             f'{self.name()} failed to properly load '
                             f'image and re-enable driver {retry + 1} '
                             f'of {num_retries} times')

    def enable_device(self):
        hil.hbiMbDeviceEnable(self.fpga_index)

    def open_pcie_device(self):
        handle = hil.pciDeviceOpen(pcie.pat_gen_guid, pcie.pat_gen_viddid, -1, None)
        hil.pciDeviceClose(handle)

    def read_slice_register(self, register_type, slice=0, index=0):
        return self.read_bar_register(register_type=register_type, bar_space=slice + 1, index=index)

    def read_bar_register(self, register_type, bar_space=1, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        regvalue = hil.hbiMbBarRead(self.fpga_index, bar_space, register_type.address(index))
        return register_type(value=regvalue)

    def read_register_raw(self, register_type, slice=0, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarRead(self.fpga_index, slice + 1, register_type.address(index))

    def write_slice_register(self, register, slice=0, index=0):
        self.write_bar_register(register, bar_space=slice + 1, index=index)

    def write_bar_register(self, register, bar_space=1, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarWrite(self.fpga_index, bar_space, register.address(index), register.value)

    def write_register_raw(self, Register, data, bar=1, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarWrite(self.fpga_index, bar, Register.address(index), data)

    def write_register_direct(self, slice, offset, data):
        return hil.hbiMbBarWrite(self.fpga_index, bar=slice + 1, offset=offset, data=data)

    def cpld_version_string(self):
        return hil.hbiMbCpldVersionString(self.fpga_index)

    def fpga_version_string(self):
        return hil.hbiMbFpgaVersionString(self.fpga_index)

    def read_hg_id(self):
        lower = self.read_register_raw(self.registers.HG_ID_LOWER)
        upper = self.read_register_raw(self.registers.HG_ID_UPPER)
        return upper, lower

    def dma_write(self, offset, data, slice):
        offset = slice_offset_64_GB[slice] + offset
        try:
            hil.hbiMbDmaWrite(self.fpga_index, offset, data)
            return
        except RuntimeError as e:
            self.Log('info', '{}: DMA write failed. \n{}'.format(self.name(), e))
            sys.exit()

    def dma_read(self, offset, size, slice):
        if size == 0:
            return b''
        offset = slice_offset_64_GB[slice] + offset
        try:
            return hil.hbiMbDmaRead(self.fpga_index, offset, size)
        except RuntimeError:
            self.Log('info', f'{self.name()}: DMA read failed, offset={offset:09X}, size={size}')
            sys.exit()

    def check_ddr_calibration_status(self, retry_limit=50, slice_list=SLICECOUNT):
        calibrated_slices = []
        retried_slices = []
        for slice in slice_list:
            for tries in range(retry_limit):
                status = self.get_ddr_calibration_status(slice)
                if status.reset_done and status.calibration_passed:
                    calibrated_slices.append(slice)
                    break
                elif slice not in retried_slices:
                    self.Log('warning', f'Resetting Calibration on Slice {slice}')
                    self.reset_ddr(slice)
                    retried_slices.append(slice)
                    time.sleep(5)
            else:
                status_string = f'reset_done = {status.reset_done}, calibration_failed = {status.calibration_failed}, calibration_passed = {status.calibration_passed}'
                self.Log('warning', f'PatGen: DDR Initialization failed on slice {slice}, {status_string}')
        return calibrated_slices

    def initialize_memory_block(self, slices=SLICECOUNT):
        step_size = 1 << 30
        # slices = [0, 1, 2, 3, 4]
        zero_initialised_1gb_data = b'\x00' * (1 << 30)  # 1GB buffer
        for slice in self.check_ddr_calibration_status(retry_limit=50, slice_list=slices):
            start = time.perf_counter()
            for offset in range(0, self.dimm_size, step_size):
                self.dma_write(offset, zero_initialised_1gb_data, slice)
            end = time.perf_counter()
            self.Log('info', f'PatGen: DMA init successful for slice {slice} (time: {end-start:0.2f} seconds)')

    def get_status_register(self, receiver):
        return self.registers.RM_RING_BUS_STATUS

    def get_control_register(self, receiver):
        return self.registers.RM_RING_BUS_CONTROL

    def get_error_count_register(self, receiver):
        return self.registers.RM_RING_BUS_ERROR_COUNT

    def get_error_count_per_lane_register(self, receiver):
        return self.registers.RM_XCVR_ERROR_COUNT_LANE

    def get_transferred_pattern_count_for_ring_multiplier(self):
        transfer_word_size = 64
        lower = self.read_register_raw(self.registers.RM_RING_BUS_PATTERN_COUNT, index=0)
        upper = self.read_register_raw(self.registers.RM_RING_BUS_PATTERN_COUNT, index=1)

        combined_values = self.combined_hex_values([upper, lower], 32)
        return combined_values * transfer_word_size

    def get_transferred_pattern_count(self, receiver):
        return self.get_transferred_pattern_count_for_ring_multiplier

    def reconfig_bridge_for_ring_multiplier(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_RM_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_RM_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_RM_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_RM_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_pll_bridge_for_ring_multiplier(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_RM_PLL_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_RM_PLL_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_RM_PLL_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_RM_PLL_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reset_error_count(self, register, slice_list=None):
        register.ResetErrorCount = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
        register.ResetErrorCount = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def reset_link(self, register, slice_list=None):
        register.Reset = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
        register.Reset = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def check_error_count(self, error_count_register, slice_list=None):
        error_count_value = self.read_register_raw(error_count_register)
        if error_count_value:
            self.Log('error',
                     '{} Transceiver lane error count set to {}'.format(self.__class__.__name__, error_count_value))
        return error_count_value

    def is_register_set(self, slice, value, expected, reg_name, bit_name=''):
        if value == expected:
            return True
        else:
            self.Log('error',
                     '{} Slice {} {} {} register was not set to 1'.format(self.__class__.__name__, slice, reg_name,
                                                                          bit_name))
            return False

    def enable_prbs(self, register, slice_list=None):
        register.EnablePrbs = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def disable_prbs(self, register, slice_list=None):
        register.EnablePrbs = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def temperature_using_hil(self):
        return hil.hbiMbTmonRead(12)

    def temperature_using_bar_read(self):
        raw_temperature = self.read_register_raw(self.registers.TEMPERATURE0_V)
        return stratix10.convert_raw_temp_to_degrees_celcius(raw_temperature)

    def get_ddr_calibration_status(self, bar=None):
        return self.read_slice_register(self.registers.DDR_STATUS, bar)

    def take_snapshot(self, directory):
        self.set_active_slices_from_pattern_control_register()
        self.register_snapshot(directory)
        self.trace_snapshot(directory)
        self.ctv_header_snapshot(directory)
        self.ctv_data_snapshot(directory)
        self.error_stream_snapshot(directory)
        self.pattern_snapshot(directory)
        self.take_raw_register_snapshot(directory)

    def set_active_slices_from_pattern_control_register(self):
        self.active_slices = []
        for slice in range(0, 5):
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            if pattern_control_register.SliceActiveInBurst == 1:
                self.active_slices.append(slice)

    def register_snapshot(self, directory):
        filename = os.path.join(directory, 'registers.csv')
        with open(filename, 'w') as fout:
            writer = csv.writer(fout)
            writer.writerow(["Address", "BAR", "Register value", "Register Name"])
            for register_type in self.registers.get_register_types_sorted_by_name():
                for slice in range(0, 5):
                    if register_type.REGCOUNT == 1:
                        data = self.read_register_raw(register_type, slice)
                        fout.write(
                            '0x{:0x},{},0x{:0x},{}\n'.format(register_type.ADDR, slice, data, register_type.__name__))
                    else:
                        for register_index in range(register_type.REGCOUNT):
                            data = self.read_register_raw(register_type, slice, register_index)
                            fout.write(
                                '0x{:0x},{},0x{:0x},{}[{}]\n'.format(register_type.address(register_index), slice, data,
                                                                     register_type.__name__,
                                                                     register_index))

    def trace_snapshot(self, directory):
        for slice in self.active_slices:
            filename = os.path.join(directory, f'slice_{slice}_trace.bin')
            offset = self.read_register_raw(patgen_register.TRACE_STREAM_START_ADDRESS_REG,
                                            slice) * STREAM_BLOCK_SIZE * 2
            size = self.read_register_raw(patgen_register.TRACE_STREAM_COUNT, slice) * STREAM_BLOCK_SIZE
            trace = self.dma_read(offset, size, slice)
            with open(filename, 'wb') as f:
                f.write(trace)

    def ctv_header_snapshot(self, directory):
        for slice in self.active_slices:
            filename = os.path.join(directory, f'slice_{slice}_ctv_headers.bin')
            offset = self.read_register_raw(patgen_register.CTV_HEADER_STREAM_START_ADDRESS_REG,
                                            slice) * STREAM_BLOCK_SIZE * 2
            size = self.read_register_raw(patgen_register.CTV_CAPTURE_HEADER_COUNT, slice) * STREAM_BLOCK_SIZE
            ctv_headers = self.dma_read(offset, size, slice)
            with open(filename, 'wb') as f:
                f.write(ctv_headers)

    def ctv_data_snapshot(self, directory):
        for slice in self.active_slices:
            filename = os.path.join(directory, f'slice_{slice}_ctv_data.bin')
            offset = self.read_register_raw(patgen_register.CTV_DATA_STREAM_START_ADDRESS_REG,
                                            slice) * STREAM_BLOCK_SIZE * 2
            size = self.read_register_raw(patgen_register.CTV_CAPTURE_DATA_COUNT, slice) * STREAM_BLOCK_SIZE
            ctv_data = self.dma_read(offset, size, slice)
            with open(filename, 'wb') as f:
                f.write(ctv_data)

    def error_stream_snapshot(self, directory):
        for slice in self.active_slices:
            for pm in range(4):
                filename = os.path.join(directory, f'slice_{slice}_pm_{pm}_error_stream.bin')
                offset = self.read_register_raw(patgen_register.ERROR_STREAM_PM_START_ADDRESS_REG, slice,
                                                pm) * STREAM_BLOCK_SIZE * 2
                size = self.read_register_raw(patgen_register.ERROR_STREAM_PM_COUNT, slice, pm) * STREAM_BLOCK_SIZE
                error_stream = self.dma_read(offset, size, slice)
                with open(filename, 'wb') as f:
                    f.write(error_stream)

    def pattern_snapshot(self, directory):
        for slice in self.active_slices:
            for key, value in self.address_and_pattern_size_list.items():
                filename = os.path.join(directory, f'slice_{slice}_pattern_address_{hex(key // 16)}.bin')
                pattern = self.dma_read(key, value, slice)
                with open(filename, 'wb') as f:
                    f.write(pattern)

    def pattern_list_bytes(self, address_and_pattern_size_list):
        self.address_and_pattern_size_list = address_and_pattern_size_list

    def get_pg_aurora_status_register(self):
        return self.registers.AURORA_STATUS

    def get_pg_aurora_control_register(self):
        return self.registers.AURORA_CONTROL

    def get_pg_aurora_error_count_register(self):
        return self.registers.AURORA_ERROR_COUNTS

    def get_pg_trigger_up_register(self):
        return self.registers.SEND_TRIGGER

    def aurora_status(self, index=0):
        return self.read_slice_register(self.registers.AURORA_STATUS)

    def aurora_error_count(self, index=0):
        return self.read_register_raw(self.registers.AURORA_ERROR_COUNTS)

    def read_trigger(self, index=0):
        return self.read_register_raw(self.registers.LAST_TRIGGER_SEEN)

    def send_trigger(self, trigger_data, index=0):
        self.write_register_raw(self.registers.SEND_TRIGGER, trigger_data)

    def reset_pat_gen_arora_link(self):
        pg_link_up = False
        aurora_control_register = self.registers.AURORA_CONTROL()
        for link_reset_loop in range(20):
            pg_aurora_error_count, pg_aurora_status = self.get_pg_aurora_status_and_error_count()
            if pg_aurora_status != 0x27 or pg_aurora_error_count != 0:
                self.reset_aurora_link(aurora_control_register)
                time.sleep(0.01)
            else:
                self.Log('info',
                         'Aurora link up from PatGen at iteration {}. Aurora status :0x{:x} Aurora error count :0x{:x}'.format(
                             link_reset_loop, pg_aurora_status, pg_aurora_error_count))
                pg_link_up = True
                break
            if link_reset_loop == 19:
                self.Log('error',
                         'Aurora link not up from PatGen at iteration {}. Aurora status :0x{:x} Aurora error count :0x{:x}'.format(
                             link_reset_loop, pg_aurora_status, pg_aurora_error_count))
        return pg_link_up

    def get_aurora_status_and_error_count(self):
        return self.aurora_error_count(), self.aurora_status()

    def reset_aurora_link(self, register):
        register.reset = 0x1
        register.clear_error_count = 0x1
        self.write_slice_register(register)
        register.reset = 0x0
        register.clear_error_count = 0x0
        self.write_slice_register(register)

    def disable_aurora(self, index=0):
        r = self.read_slice_register(self.registers.AURORA_CONTROL)
        r.reset = 1
        r.clear_error_count = 1
        r.clear_fifos = 1
        self.write_slice_register(r)

    def enable_aurora(self, index=0):
        r = self.read_slice_register(self.registers.AURORA_CONTROL)
        r.reset = 0
        r.clear_error_count = 0
        r.clear_fifos = 0
        self.write_slice_register(r)

    def is_aurora_link_up(self, index=0):
        rctc_link_up = False
        status = self.aurora_status()
        rctc_aurora_error_count = self.aurora_error_count()
        if status.value == 0x27 and rctc_aurora_error_count == 0:
            rctc_link_up = True
        return rctc_link_up, status, rctc_aurora_error_count

    def clear_trigger_fifo(self, index=0):
        control = self.read_slice_register(self.registers.AURORA_CONTROL)
        control.clear_fifos = 0x1
        self.write_slice_register(control)
        time.sleep(0.001)
        control.clear_fifos = 0x0
        self.write_slice_register(control)

    def clear_trigger_error_count(self, index=0):
        control = self.read_slice_register(self.registers.AURORA_CONTROL)
        control.clear_error_count = 0x1
        self.write_slice_register(control)
        time.sleep(0.001)
        control.clear_error_count = 0x0
        self.write_slice_register(control)

    def start_pattern_prestage(self, slice_index=range(0, 5)):
        for slice in slice_index:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.SliceActiveInBurst = 1
            pattern_control_register.SliceGroupMask = 0x1f
            pattern_control_register.AbortPattern = 0
            self.write_slice_register(pattern_control_register, slice)

    def set_active_slice(self, slice, mask, hbicc):
        pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
        pattern_control_register.SliceActiveInBurst = 1
        pattern_control_register.SliceGroupMask = mask
        pattern_control_register.include_big_header_full = 1
        pattern_control_register.active_pm_indicator = self.get_active_pm_indicator(hbicc)
        self.write_slice_register(pattern_control_register, slice)

    def set_all_slices_inactive(self):
        for slice in range(0, 5):
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.SliceActiveInBurst = 0
            self.write_slice_register(pattern_control_register, slice)

    def get_active_pm_indicator(self, hbicc):
        pms = hbicc.get_execution_path()
        pm_list = 0
        for psdb, pin0, pin1 in pms:
            if psdb == 0:
                pm_list += 0b11
            if psdb == 1:
                pm_list += 0b1100
        return pm_list

    def mask_inactive_slice(self, slice_list):
        for slice in slice_list:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.SliceActiveInBurst = 0
            pattern_control_register.SliceGroupMask = 0
            self.write_slice_register(pattern_control_register, slice)

    def toggle_pattern_prestage(self, slice):
        pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
        pattern_control_register.PrestagePattern = 1
        self.write_slice_register(pattern_control_register, slice)
        pattern_control_register.PrestagePattern = 0
        self.write_slice_register(pattern_control_register, slice)

    def get_prestage_pass_fail_slices(self, retry_limit=0xff000, slice_list=range(0, 5)):
        passing = []
        failing = []
        for slice in slice_list:
            for i in range(retry_limit):
                prestage_status = self.get_prestage_status(slice)
                if prestage_status.PrestageDone == 1:
                    passing.append(slice)
                    break
            else:
                failing.append(slice)
        return passing,failing

    def get_prestage_status(self, slice):
        return self.read_slice_register(self.registers.PATTERN_CONTROL, slice)

    def is_pattern_execution_complete(self, retry_limit=0xffff0000, slice_list=range(0, 5)):
        failed = []
        for slice in slice_list:
            for i in range(retry_limit):
                pattern_running_status = self.get_pattern_running_complete_status(slice)
                if pattern_running_status.PatternRunning == 0:
                    break
            else:
                failed.append(slice)
        if failed:
            self.Log('error', 'Pattern Running Status Check Failed for {} slice {} .'.format(self.name(), failed))
            return False
        return True

    def get_pattern_running_complete_status(self, slice):
        return self.read_slice_register(self.registers.PATTERN_CONTROL, slice)

    def abort_pattern(self, slice_list):
        for slice in slice_list:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.AbortPattern = 1
            self.write_slice_register(pattern_control_register, slice)

    def read_pattern_end_status(self, slice):
        return self.read_register_raw(self.registers.PATTERN_END_STATUS, slice)

    def get_alarm(self, slices, has_failed):
        values = {self.registers.ALARMS1(0).name(): {}, self.registers.ALARMS2(0).name(): {}}
        for alarm in [self.registers.ALARMS1, self.registers.ALARMS2]:
            for slice in slices:
                alarm_object = self.read_slice_register(alarm, slice)
                values[alarm(0).name()].update({slice: alarm_object})
                if alarm_object.value != 0:
                    has_failed.append(alarm_object)
        return {self.name(): values}

    def read_alarms(self, slice):
        alarms = []
        for alarm_type in [self.registers.ALARMS1, self.registers.ALARMS2]:
            alarm_register = self.read_slice_register(alarm_type, slice)
            self.Log('debug',
                     f'{self.name()} slice {slice}: {alarm_register.name()} RAW VALUE: 0x{alarm_register.value:08X}')
            if alarm_register.value != 0:
                for field in alarm_register._fields_:

                    field_name = field[0]
                    field_value = getattr(alarm_register, field[0])
                    if field_value != 0 and 'reserved' not in field_name.lower():
                        alarms.append(field_name + ': ' + str(field_value))
                        if field_name == 'ecc_error':
                            alarm_register = self.read_slice_register(alarm_type, slice)
                            uncorrectable = self.read_register_raw(self.registers.ECC_UNCORRECTABLE, slice)
                            correctable = self.read_register_raw(self.registers.ECC_CORRECTABLE, slice)
                            self.Log('warning',
                                     f'{self.name()} Slice {slice}. ECC Errors. Correctable {correctable} Uncorrectable {uncorrectable}')
        if alarms:
            return ', '.join(alarms)
        return ''

    def log_capture_stream_counters(self):
        headers = ['Slice', 'ctv_head', 'ctv_data', 'trace', 'pm0', 'pm1', 'pm2', 'pm3']
        data = []

        for slice in range(0, 5):
            entry = {'Slice': slice}
            entry.update(self.get_stream_counters(slice))
            entry.update(self.get_pin_stream_counters(slice))
            data.append(entry)

        table = self.contruct_text_table(headers, data, title_in='Stream Counters')
        self.Log('debug', f'{table}')

    def get_stream_counters(self, slice):
        count_map = {'CTV_CAPTURE_HEADER_COUNT': 'ctv_head', 'CTV_CAPTURE_DATA_COUNT': 'ctv_data',
                     'TRACE_STREAM_COUNT': 'trace'}
        counters = [self.registers.CTV_CAPTURE_HEADER_COUNT,
                    self.registers.CTV_CAPTURE_DATA_COUNT,
                    self.registers.TRACE_STREAM_COUNT,
                    ]
        entry = {}
        for counter in counters:
            register = self.read_slice_register(counter, slice)
            entry.update({count_map[register.name()]: register.value})

        return entry

    def get_pin_stream_counters(self, slice):
        pin_counters = [self.registers.ERROR_STREAM_PM_COUNT,
                        self.registers.ERROR_STREAM_PM_COUNT,
                        self.registers.ERROR_STREAM_PM_COUNT,
                        self.registers.ERROR_STREAM_PM_COUNT]
        entry = {}
        for index, counter in enumerate(pin_counters):
            value = self.read_register_raw(counter, slice, index=index)
            entry.update({f'pm{index}': value})

        return entry

    def reset_ddr(self, slice):
        control_reg = self.read_slice_register(self.registers.DDR_CONTROL, slice)
        control_reg.reset_req = 1
        self.write_slice_register(control_reg, slice)
        if control_reg.reset_req != 1:
            self.Log('error', f'Slice {slice} DDR Calibration Reset Failed. Unable to set to 1.')
        control_reg.reset_req = 0
        self.write_slice_register(control_reg, slice)
        control_reg = self.read_slice_register(self.registers.DDR_CONTROL, slice)
        if control_reg.reset_req != 0:
            self.Log('error', f'Slice {slice} DDR Calibration Reset Failed. Unable to set to 0.')

    def trace_count(self, slice):
        return self.read_register_raw(self.registers.TRACE_STREAM_COUNT, slice)

    def log_capture_stats_registers(self, slices=SLICECOUNT):
        registers = [self.registers.CAPTURE_CONTROL,
                     self.registers.CTV_DATA_CNT_CAPTURE_ARB_TOP,
                     self.registers.TRACE_STREAM_CNT_CAPTURE_ARB_TOP,
                     self.registers.CAPTURE_CNT_RCVD_FROM_XCVR,
                     self.registers.CTV_HEADER_COUNT_AT_PVC_PROC,
                     self.registers.CTV_CAPTURE_HEADER_COUNT,
                     self.registers.CTV_HEADER_COUNT_AT_PACKER,
                     self.registers.PATTERN_CONTROL,
                     self.registers.PATTERN_START,
                     self.registers.ALARMS2,
                     self.registers.ALARMS1,

                     self.registers.L_COUNTER,
                     self.registers.H_COUNTER,
                     self.registers.X_COUNTER,
                     self.registers.V_M_COUNTER,
                     self.registers.ZERO_COUNTER,
                     self.registers.ONE_COUNTER,
                     self.registers.E_COUNTER,
                     self.registers.K_COUNTER,

                     self.registers.FAKE_CTV_DATA_AFTER_256_0,
                     self.registers.FAKE_CTV_DATA_AFTER_256_1,
                     self.registers.FAKE_CTV_DATA_AFTER_256_2,
                     self.registers.FAKE_CTV_DATA_AFTER_256_3,
                     self.registers.ERR_CNT_CAPTURE_ARB_TOP_PM_0,
                     self.registers.ERR_CNT_CAPTURE_ARB_TOP_PM_1,
                     self.registers.ERR_CNT_CAPTURE_ARB_TOP_PM_2,
                     self.registers.ERR_CNT_CAPTURE_ARB_TOP_PM_3,
                     self.registers.READ_32_COUNT,
                     self.registers.LAST_INSTRUCTION_ADDRESS,
                     self.registers.ERROR_COUNT_AFTER_FIRST_SORT,
                     self.registers.ERR_CNT_AFTER_SECOND_SORT_PM_0,
                     self.registers.ERR_CNT_AFTER_SECOND_SORT_PM_1,
                     self.registers.ERR_CNT_AFTER_SECOND_SORT_PM_2,
                     self.registers.ERR_CNT_AFTER_SECOND_SORT_PM_3,
                     self.registers.CTV_HEADER_REQUEST_CNT,
                     self.registers.CTV_ROW_REQUEST_CNT,
                     self.registers.TRACE_REQUEST_CNT,
                     self.registers.ERROR_PACKET_0_REQUEST_CNT,
                     self.registers.ERROR_PACKET_1_REQUEST_CNT,
                     self.registers.ERROR_PACKET_2_REQUEST_CNT,
                     self.registers.ERROR_PACKET_3_REQUEST_CNT,
                     self.registers.WAIT_TOWRITE_COUNTER

                     ]
        self.read_log_set_of_registers(registers)

    def force_ecc_reset(self, slices=SLICECOUNT):
        registers = [self.registers.ALARMS1, self.registers.ALARMS2]
        self.read_log_set_of_registers(registers, title=f'Logging Alarms before ECC reset {slices}')
        for slice in slices:
            ddr_control = self.read_slice_register(self.registers.DDR_CONTROL, slice)
            ddr_control.reset_ecc_count = 1
            self.write_slice_register(ddr_control, slice)
            for i in range(0xFF):
                reset_ecc_count = self.read_slice_register(self.registers.DDR_CONTROL, slice).reset_ecc_count
                if reset_ecc_count == 0:
                    break
            else:
                self.Log('error', f'self.name() Slice {slice}: ECC Alarm/Count Reset was not successful.')

            reset_ecc_count = self.read_slice_register(self.registers.DDR_CONTROL, slice).reset_ecc_count
            alarm = self.read_slice_register(self.registers.ALARMS1, slice).ecc_error
            if alarm:
                self.Log('error', f'{self.name()} Slice {slice}: ECC Alarm bit did not clear')
        for slice in slices:
            uncorrectable = self.read_register_raw(self.registers.ECC_UNCORRECTABLE, slice)
            correctable = self.read_register_raw(self.registers.ECC_CORRECTABLE, slice)
            if uncorrectable or correctable:
                self.Log('warning', f'{self.name()} Slice {slice}: ECC Counters were not cleared. Uncorrectable {uncorrectable}, Correctable {correctable}')

        self.read_log_set_of_registers(registers, title='Logging Alarms After ECC reset')

    def is_ecc_error_crital(self, index, comments):
        self.handle_correctable_ecc_errors(index, comments)
        if self.are_uncorrectable_ecc_errors_present(index, comments):
            return True
        return False

    def are_uncorrectable_ecc_errors_present(self, slice, comments):
        uncorrectable = self.read_register_raw(self.registers.ECC_UNCORRECTABLE, slice)
        if uncorrectable > 0:
            self.Log('error',
                     f'{self.name()} Slice {slice}: Uncorrectable ECC Errors Encountered: {uncorrectable} count')
            comments.append(f'S_{slice}: {uncorrectable} Uncorrectables')
            return True
        return False

    def handle_correctable_ecc_errors(self, slice, comments):
        correctable = self.read_register_raw(self.registers.ECC_CORRECTABLE, slice)
        if correctable > 0:
            comment = f'S_{slice}: {correctable} Correctable Error(s)'
            self.Log('warning', f'{self.name()}: {comment}')
            comments.append(comment)

    def si_5344_write(self, register, data):
        hil.hbiMbSi5344Write(register, data)

    def si_5344_read(self, register):
        return hil.hbiMbSi5344Read(register)

    def check_if_flag_set(self,slice,bit_field):
        flag_register = self.read_slice_register(self.registers.FLAGS, slice)
        self.Log('debug', f'{self.name()} slice {slice}: {flag_register.name()} RAW VALUE: 0x{flag_register.value:08X}')
        if flag_register.value != 0:
            for field in flag_register._fields_:
                field_name = field[0]
                field_value = getattr(flag_register, field[0])
                if field_value != 0 and 'reserved' not in field_name.lower():
                    if field_name == bit_field:
                        self.Log('debug', f'{self.name()} {bit_field} set as expected')
                        return True
        else:
            self.Log('error',
                     f'{self.name()} slice {slice}: Flags not set as expected.RAW VALUE: 0x{flag_register.value:08X}')


    def set_up_trigger_control_register(self, slice,**kwargs):
        trigger_control_register = self.read_slice_register(self.registers.TRIGGER_CONTROL, slice)
        for key, value in kwargs.items():
            trigger_control_register.card_type = kwargs['target_resource']
            trigger_control_register.domain_id = kwargs['target_domain']
            trigger_control_register.domain_master = kwargs['domain_master']
            trigger_control_register.dut_id = kwargs['dut_id']
            trigger_control_register.sync_modifier = kwargs['sync_modifier']
            trigger_control_register.card_type_extended = kwargs['card_type_extended']
            trigger_control_register.clear_all_on_gse = kwargs['clear_all_on_gse']
        self.write_slice_register(trigger_control_register, slice)

    def set_e32_interval(self, interval, slices=range(5)):
        for slice in slices:
            self._set_capabilities(slice)
            self._set_e32_interval(interval, slice)

    def _set_e32_interval(self, interval, slice):
        register = self.read_slice_register(self.registers.E32_INTERVAL, slice=slice)
        register.e32_interval = interval
        self.write_slice_register(register, slice=slice)
        self.Log('debug', f'{self.name()} Interval: {register.value}')

    def _set_capabilities(self, slice):
        register = self.read_slice_register(self.registers.CAPABILITIES, slice=slice)
        register.set_e32_interval = 1
        self.write_slice_register(register, slice=slice)

    def enable_induced_ecc_errors(self, slices):
        for slice in slices:
            ddr_control = self.registers.DDR_CONTROL()
            ddr_control.en_single_bit_error = 1
            self.write_slice_register(ddr_control, slice=slice)

    def disable_induced_ecc_errors(self, slices):
        for slice in slices:
            ddr_control = self.read_slice_register(self.registers.DDR_CONTROL, slice=slice)
            ddr_control.en_single_bit_error = 0
            self.write_slice_register(ddr_control, slice=slice)

    def read_capture_control_register(self, slice):
        capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice)
        return capture_control

    def reset_capture_addr_counts_of_capture_control_register(self, slice):
        capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice)
        capture_control.reset_capture_addr_counts = 1
        self.write_slice_register(capture_control, slice)
        capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice)
        capture_control.reset_capture_addr_counts = 0
        self.write_slice_register(capture_control, slice)

    def take_raw_register_snapshot(self,directory):
        startsaddr=0
        end_addr= 0x2004
        for slice in range(0, 5):
            filename = os.path.join(directory, f'registers_raw_slice_{slice}.csv')
            with open(filename, 'w') as fout:
                writer = csv.writer(fout)
                writer.writerow(["Address","Register value_Decimal","Register value_Hex" ])
                for offset in range(startsaddr,end_addr,4):
                    data = hil.hbiMbBarRead(self.fpga_index, bar=slice + 1, offset=offset)
                    fout.write(
                        '0x{:0x},{},0x{:0x},\n'.format(offset, data,data))

    def report_flag_status(self, flag_name, expected=1, slices=SLICECOUNT):
        data = []
        for slice in slices:
            flags = self.read_slice_register(self.registers.FLAGS, slice)
            for field in flags._fields_:
                if field[0].lower() == flag_name.lower():
                    observed = getattr(flags, field[0])
                    if observed != expected:
                        data.append({'Slice': slice, 'Flag': flag_name, 'Expected': expected, 'Observed': observed})

        if data:
            table = self.contruct_text_table(data=data, title_in=f'Results for {flag_name} flag in PatGen')
            self.Log('error', f'{table}')

    def deactive_master_trigger_control_register(self):
        for slice in range(5):
            trigger_control_register = self.read_slice_register(self.registers.TRIGGER_CONTROL, slice)
            trigger_control_register.domain_master = 0
            self.write_slice_register(trigger_control_register, slice)

    def set_pattern_start_address(self, address, slice):
        pattern_start = self.registers.PATTERN_START(value=address)
        self.write_slice_register(pattern_start, slice=slice)

    def reset_alarm_on_slice(self, slice_index):
        pattern_control = self.read_slice_register(patgen_register.PATTERN_CONTROL, slice_index)
        pattern_control.reset_alarm = 1
        self.write_slice_register(pattern_control, slice_index)
        pattern_control = self.read_slice_register(patgen_register.PATTERN_CONTROL, slice_index)
        pattern_control.reset_alarm = 0
        self.write_slice_register(pattern_control, slice_index)

    def check_e32_count(self, expected):
        for pm in range(4):
            observed = self.read_slice_register(self.registers.READ_32_COUNT, index=pm).value
            if observed != expected:
                self.Log('error', f'PM{pm} does not have the correct E32 Count. Expected: {expected} Observed: {observed}')

    def check_ctv_header_count(self, expected, slices=SLICECOUNT):
        for slice in slices:
            header_count = self.registers.CTV_CAPTURE_HEADER_COUNT
            observed_ctv_header_count = self.read_slice_register(header_count, slice=slice).value
            if observed_ctv_header_count != expected:
                self.Log('error', f'Slice: {slice} CTV Header Count Error: Obererved: {observed_ctv_header_count:,} expected: {expected:,}')

    def check_ctv_data_block_count(self, expected, slices=SLICECOUNT):
        for slice in slices:
            ctv_count = self.registers.CTV_CAPTURE_DATA_COUNT
            ctv_data_block_count = self.read_slice_register(ctv_count, slice=slice).value
            if ctv_data_block_count != expected:
                self.Log('error', f'CTV Data Block Count Error: Observed: {ctv_data_block_count:,} expected: {expected:,}')

    def reset_si5344_via_pin(self):
        self.write_bar_register(self.registers.SI5344_CONTROL_REGISTER(value=1))
        time.sleep(5)
        self.write_bar_register(self.registers.SI5344_CONTROL_REGISTER(value=0))
        device_ready_register = 0xFE
        for tries in range(10000):
            device_status = self.si_5344_read(device_ready_register)
            if device_status == 0xF:
                break
        else:
            self.Log('error', f'Si54344 Device Ready Status Failure')

    def reset_si5344_via_register(self):
        reset_register = 0x001E
        device_ready_register=0xFE
        reset_bit=0x02
        self.si_5344_write(reset_register, reset_bit)
        time.sleep(0.1)
        for tries in range(10000):
            device_status = self.si_5344_read(device_ready_register)
            if device_status == 0xF:
                break
        else:
           self.Log('error', f'Si54344 Device Ready Status Failure')

    def set_si5344_frequency_via_configs_file(self, frequency_mhz):
        if type(frequency_mhz) is not float:
            raise Exception(f'Frequency must be of type float and not of type {type(frequency_mhz)}')

        self.reset_si5344_via_pin()
        si5344_clock_settings = self.get_si5344_clock_settings(frequency_mhz)
        for line in si5344_clock_settings:
            if line.startswith('0x'):
                register, data = self.extract_page_addr_data(line)
                hil.hbiMbSi5344Write(register, data)
                if register == 0x540:
                    time.sleep(0.5)

    def extract_page_addr_data(self, line):
        register, data = line.split(',')
        register = int(register, 16)
        data = int(data, 16)
        return register, data

    def get_si5344_clock_settings(self, frequency_mhz):
        zipped_folder = os.path.join(projectpaths.ROOT_PATH, 'ThirdParty', 'ISV', 'si5344Configs.zip')
        configs_file = f'si5344Settings{float(frequency_mhz)}MHz.txt'
        my_zip = zipfile.ZipFile(zipped_folder)

        for file in my_zip.namelist():
            if configs_file in my_zip.getinfo(file).filename:
                file_bytes = my_zip.read(file)
                si5344_clock_settings = file_bytes.decode("utf-8")
                si5344_clock_settings = si5344_clock_settings.replace('\r', '')
                si5344_clock_settings = si5344_clock_settings.split('\n')
                break
        else:
            raise Exception(f'Si5344: Can not find requested Frequency of {frequency_mhz}')

        return si5344_clock_settings

    def set_clock(self, pll=False, si5344=False):
        if pll and si5344:
            raise Exception(f'Can not set both PLL and Si5344 at the same time. Pick one.')

        if pll:
            self.write_slice_register(self.registers.CLOCK_PLL_SI5344(data=0))
        if si5344:
            self.write_slice_register(self.registers.CLOCK_PLL_SI5344(data=1))

    def set_un_parked_duts(self, duts, slices=SLICECOUNT):
        for slice in slices:
            self.write_slice_register(self.registers.UN_PARKED_DUTS(un_parked_duts=duts), slice)

    def set_un_park_duts_to_default(self, slices=SLICECOUNT):
        for slice in slices:
            self.write_slice_register(self.registers.UN_PARKED_DUTS(un_parked_duts=0xFF), slice)

    def read_central_domain_register_value(self, slice, register_index):
        register = self.registers.CENTRAL_DOMAIN_REGISTER
        return self.read_slice_register(register, slice, index=register_index).value

    def check_error_block_count(self, expected, slices=SLICECOUNT):
        for slice in slices:
            ctv_count = self.registers.ERROR_STREAM_PM_COUNT
            for i in range(4):
                count = self.read_slice_register(ctv_count, slice, i).value
                if count != expected:
                    self.Log('error',
                             f'Error stream {i} slice {slice} Data Block Count Error: Observed: {count} expected: {expected}')
