################################################################################
# INTEL CONFIDENTIAL - Copyright 2020. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
import enum


class BitFieldStructure(ctypes.LittleEndianStructure):
    def to_int(self):
        return int.from_bytes(self, byteorder='little')

    def to_bytes(self):
        return bytes(self)


class DcTriggerQueueCommand(BitFieldStructure):
    _fields_ = [('payload',                 ctypes.c_uint64, 48),
                ('command_or_interface',    ctypes.c_uint64,  4),
                ('channel_set',             ctypes.c_uint64,  3),
                ('target',                  ctypes.c_uint64,  3),
                ('byte_count',              ctypes.c_uint64,  3),
                ('psdb',                    ctypes.c_uint64,  1),
                ('is_write',                ctypes.c_uint64,  1),
                ('is_command',              ctypes.c_uint64,  1)]


class ChannelSet(enum.IntEnum):
    SET_0 = 0
    SET_1 = 1
    SET_2 = 2
    SET_3 = 3
    SET_O_3 = 4
    SET_1_2 = 5
    NONE = 6
    ALL = 7


CHANNEL_SET_DUTS = {
    ChannelSet.SET_0: [0],
    ChannelSet.SET_1: [1],
    ChannelSet.SET_2: [2],
    ChannelSet.SET_3: [3],
    ChannelSet.SET_O_3: [0, 3],
    ChannelSet.SET_1_2: [1, 2],
    ChannelSet.NONE: [],
    ChannelSet.ALL: [0, 1, 2, 3]
}