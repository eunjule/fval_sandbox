# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
import csv
import os
import time
import datetime

from Tools import projectpaths
from Common.instruments.hbi_instrument import HbiInstrument
from Common import hilmon as hil
from Common import pcie
from Common import configs
from . import pin_multiplier_register
from ThirdParty.SASS.suppress_sensor import suppress_sensor

BAR0 = 0
SLICECOUNT = range(0, 5)
PM_CHIP_ID = {'PSDB_0_PIN_0': 0, 'PSDB_0_PIN_1': 1, 'PSDB_1_PIN_0': 2, 'PSDB_1_PIN_1': 3}


class PinMultiplier(HbiInstrument):
    PHYLITE_PIN_IGNORE_LIST = {
                                'B': {0:{0:[14, 22, 34], 1:[11, 24, 25], 2:[6, 8],     3:[5,8,10],        4:[2, 16, 29, 31]},
                                      1:{0:[7,18,23],    1:[2,4,32],     2:[7,29,32],  3:[9,13,29],       4:[0,16,31]},
                                      2:{0:[6,9,29],     1:[20,21,27],   2:[10,19,34], 3:[16,18,19],      4:[7,22,26]},
                                      3:{0:[0,3,9,21],   1:[7,26,32],    2:[19,22,24], 3:[6,21,28],       4:[13,32]}},
                                'C': {0:{0:[],           1:[],           2:[],         3:[5,6,7,8,15,16], 4:[22,23,24,25,26,27,28,29,30,31,32,33,34]},
                                      1:{0:[],           1:[],           2:[],         3:[],              4:[22,23,24,25,26,27,28,29,30,31,32,33,34]},
                                      2:{0:[],           1:[],           2:[],         3:[6,17,18,27,29], 4:[22,23,24,25,26,27,28,29,30,31,32,33,34]},
                                      3:{0:[],           1:[],           2:[],         3:[],              4:[22,23,24,25,26,27,28,29,30,31,32,33,34]}}}
    CHANNELS_PER_SLICE = 35
    
    def __init__(self, psdb, fpga, rc=None, hbidtb=None):
        self.psdb = psdb
        self.slot = psdb.slot
        self.fpga_index = fpga
        self.slot_index = (self.slot << 1) + fpga
        self.rc = rc
        self.hbidtb = hbidtb
        self.blt = None
        self.registers = pin_multiplier_register
        self.fpga_load = configs.HBIPINMULTIPLIER_FPGA_IMAGE_LOAD
        self.ila_names = {}

    def name(self):
        return f'PSDB_{self.slot}_PIN_{self.fpga_index}'
    
    def channel_sets(self):
        first = self.slot_index * 4
        return list(range(first, first + 4))
        
    def initialize(self):
        self.load_fpga()
        self.log_cpld()
        self.log_fpga_version()
        self.log_build()

    def is_slot_valid(self):
        try:
            hil.hbiPsdbConnect(self.slot)
        except:
            return False
        return True

    def fpga_image_path(self):
        if configs.HBICC_IN_HDMT:
            fab_letter = configs.HBICC_IN_HDMT_FAB
            self.warn_user_fab_detection_is_bypassed(fab_letter)
        else:
            fab_letter = self.psdb.blt.fab_letter()
        
        attribute_name = f'HBIPINMULTIPLIER_FPGA_IMAGE_{self.fpga_index}_FAB_{fab_letter}'
        return getattr(configs, attribute_name)

    def load_fpga_using_hil(self):
        num_retries = 5
        with suppress_sensor(f'FVAL PSDB{self.slot} PM{self.fpga_index}',
                             'HBI_PIN'):
            for retry in range(num_retries):
                try:
                    hil.hbiPsdbDeviceDisable(self.slot, self.fpga_index)
                    hil.hbiPsdbFpgaLoad(self.slot, self.fpga_index,
                                        self.fpga_image_path())
                    self.enable_device()
                    self.Log('info', f'{self.name()} FPGA Version: '
                                     f'{self.fpga_version_string()}')
                    break
                except RuntimeError as e:
                    self.Log('warning',
                             f'{self.name()} failed to properly load '
                             f'image and re-enable driver {retry + 1} of '
                             f'{num_retries} times')

    def enable_device(self):
        self.Log('debug', f'Enabling FVAL PSDB{self.slot} PM{self.fpga_index}')
        hil.hbiPsdbDeviceEnable(self.slot, self.fpga_index)
        self.Log('debug', f'FVAL PSDB{self.slot} PM{self.fpga_index} enabled')

    def open_pcie_device(self):
        handle = hil.pciDeviceOpen(pcie.pin_multiplier_guid, pcie.pin_multiplier_viddid, self.slot,
                                   pcie.pin_multiplier_additional[self.fpga_index])
        hil.pciDeviceClose(handle)
    
    def read_slice_register(self, register_type, slice=0, index=0):
        return self.read_bar_register(register_type=register_type, bar_space=slice+1, index=index)

    def read_register_raw(self, register_type, slice=0, index=0):
        return hil.hbiPsdbBarRead(self.slot, self.fpga_index, slice+1, register_type.address(index))

    def read_bar_register(self, register_type, bar_space=1, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        reg_value = hil.hbiPsdbBarRead(self.slot, self.fpga_index, bar_space, register_type.address(index))
        return register_type(value=reg_value)

    def write_slice_register(self, register, slice=0, index=0):
        self.write_bar_register(register, bar_space=slice+1, index=index)
        
    def write_bar_register(self, register, bar_space=1, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        return hil.hbiPsdbBarWrite(self.slot, self.fpga_index, bar_space, register.address(index), register.value)

    def cpld_version_string(self):
        return hil.hbiPsdbCpldVersionString(self.slot, self.fpga_index)

    def read_hg_id(self):
        lower = self.read_register_raw(self.registers.HG_ID_LOWER)
        upper = self.read_register_raw(self.registers.HG_ID_UPPER)
        return upper, lower

    # pin <-> pin
    def get_status_register(self, receiver):
        options = {'PSDB_0_PIN_0': self.registers.PM_RING_BUS_STATUS,
                   'PSDB_0_PIN_1': self.registers.PM_RING_BUS_STATUS,
                   'PSDB_1_PIN_0': self.registers.PM_RING_BUS_STATUS,
                   'PSDB_1_PIN_1': self.registers.PM_RING_BUS_STATUS,
                   'RINGMULTIPLIER': self.registers.RM_RING_BUS_STATUS}
        return options[receiver]

    def get_control_register(self, receiver):
        options = {'PSDB_0_PIN_0': self.registers.PM_RING_BUS_CONTROL,
                   'PSDB_0_PIN_1': self.registers.PM_RING_BUS_CONTROL,
                   'PSDB_1_PIN_0': self.registers.PM_RING_BUS_CONTROL,
                   'PSDB_1_PIN_1': self.registers.PM_RING_BUS_CONTROL,
                   'RINGMULTIPLIER': self.registers.RM_RING_BUS_CONTROL}
        return options[receiver]

    def get_error_count_register(self, receiver):
        options = {'PSDB_0_PIN_0': self.registers.PM_RING_BUS_ERROR_COUNT,
                   'PSDB_0_PIN_1': self.registers.PM_RING_BUS_ERROR_COUNT,
                   'PSDB_1_PIN_0': self.registers.PM_RING_BUS_ERROR_COUNT,
                   'PSDB_1_PIN_1': self.registers.PM_RING_BUS_ERROR_COUNT,
                   'RINGMULTIPLIER': self.registers.RM_RING_BUS_ERROR_COUNT}
        return options[receiver]

    def get_error_count_per_lane_register(self, receiver):
        options = {'PSDB_0_PIN_0': self.registers.PM_XCVR_ERROR_COUNT_LANE,
                   'PSDB_0_PIN_1': self.registers.PM_XCVR_ERROR_COUNT_LANE,
                   'PSDB_1_PIN_0': self.registers.PM_XCVR_ERROR_COUNT_LANE,
                   'PSDB_1_PIN_1': self.registers.PM_XCVR_ERROR_COUNT_LANE,
                   'RINGMULTIPLIER': self.registers.RM_XCVR_ERROR_COUNT_LANE}
        return options[receiver]

    def get_transferred_pattern_count(self, receiver):
        options = {'PSDB_0_PIN_0': self.get_transferred_pattern_count_for_pin_multiplier,
                   'PSDB_0_PIN_1': self.get_transferred_pattern_count_for_pin_multiplier,
                   'PSDB_1_PIN_0': self.get_transferred_pattern_count_for_pin_multiplier,
                   'PSDB_1_PIN_1': self.get_transferred_pattern_count_for_pin_multiplier,
                   'RINGMULTIPLIER': self.get_transferred_pattern_count_for_ring_multiplier}
        return options[receiver]

    def get_transferred_pattern_count_for_pin_multiplier(self):
        transfer_word_size = 64
        lower = self.read_register_raw(self.registers.PM_RING_BUS_PATTERN_COUNT, index=0)
        upper = self.read_register_raw(self.registers.PM_RING_BUS_PATTERN_COUNT, index=1)

        combined_values = self.combined_hex_values([upper, lower], 32)
        return combined_values * transfer_word_size

    def get_transferred_pattern_count_for_ring_multiplier(self):
        transfer_word_size = 64
        lower = self.read_register_raw(self.registers.RM_RING_BUS_PATTERN_COUNT, index=0)
        upper = self.read_register_raw(self.registers.RM_RING_BUS_PATTERN_COUNT, index=1)

        combined_values = self.combined_hex_values([upper, lower], 32)
        return combined_values * transfer_word_size

    def set_equalization_reg_value(self, value):
        register = self.registers.XCVR_PM_RECONFIG_ADDRESS()
        register.value = value
        self.write_slice_register(register)

    def reset_error_count(self, register, slice_list=None):
        register.ResetErrorCount = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
        register.ResetErrorCount = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
            # status = self.read_slice_register(self.get_pin_multiplier_ring_bus_control_register(), slice).value
            # self.Log('info', 'Reset error count {:x}'.format(status))

    def reset_link(self, register, slice_list=None):
        register.Reset = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
        register.Reset = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def check_error_count(self, helper, slice):
        return self.read_slice_register(helper.rx_error_count_register, slice)

    def check_data_transfer(self, transfer_rate_register):
        return self.read_register_raw(transfer_rate_register)

    def is_register_set(self, slice, value, expected, reg_name, bit_name=''):
        if value == expected:
            return True
        else:
            return False

    def fpga_version_string(self):
        return hil.hbiPsdbFpgaVersionString(self.slot, self.fpga_index)

    def enable_prbs(self, register, slice_list=None):
        register.EnablePrbs = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
            rx_bus_status_register_value = self.read_slice_register(self.registers.RM_RING_BUS_CONTROL, slice)

    def disable_prbs(self, register, slice_list=None):
        register.EnablePrbs = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def temperature_using_hil(self):
        return hil.hbiPsdbTmonRead(self.slot, self.fpga_index)

    def temperature_using_bar_read(self):
        raw_temperature = self.read_register_raw(self.registers.TEMPERATURE)
        return self.convert_raw_temp_to_degrees_celcius(raw_temperature)

    def convert_raw_temp_to_degrees_celcius(self, register_value):
        """
        Refer to the link below for more information:
        https://www.intel.com/content/www/us/en/programmable/documentation/eis1412131558004.html#wtw1407486022104
        """
        magic_value_a = 693
        magic_value_b = 265
        x = magic_value_a * register_value
        y = x / 1024
        temperature = y - magic_value_b
        return temperature

    def convert_degrees_celcius_to_raw(self, degrees):
        """
        Refer to the link below for more information:
        https://www.intel.com/content/www/us/en/programmable/documentation/eis1412131558004.html#wtw1407486022104
        """
        magic_value_a = 693
        magic_value_b = 265
        return int((((degrees + magic_value_b) * 1024) / magic_value_a))

    def reconfig_bridge_for_ring_multiplier(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_RM_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_RM_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_RM_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_RM_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_pll_bridge_for_ring_multiplier(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_RM_PLL_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_RM_PLL_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_RM_PLL_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_RM_PLL_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_bridge_for_pin_multiplier(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PM_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PM_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PM_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PM_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_pll_bridge_for_pin_multiplier(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PM_PLL_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PM_PLL_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PM_PLL_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PM_PLL_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def take_snapshot(self, directory):
        self.register_snapshot(directory)
        self.take_raw_register_snapshot(directory)

    def register_snapshot(self, directory):
        filename = os.path.join(directory, 'registers.csv')
        with open(filename, 'w') as fout:
            writer = csv.writer(fout)
            writer.writerow(["Address", "Slice", "Register value", "Register Name"])
            for register_type in self.registers.get_register_types_sorted_by_name():
                for slice in range(5):
                    if register_type.REGCOUNT == 1:
                        data = self.read_register_raw(register_type, slice)
                        fout.write(
                            '0x{:0x},{},0x{:0x},{}\n'.format(register_type.ADDR, slice, data, register_type.__name__))
                    else:
                        for register_index in range(register_type.REGCOUNT):
                            data = self.read_register_raw(register_type, slice, register_index)
                            fout.write(
                                '0x{:0x},{},0x{:0x},{}[{}]\n'.format(register_type.address(register_index), slice, data,
                                                                     register_type.__name__,
                                                                     register_index))

    def start_pattern_prestage(self, slice_index=range(0, 5), rm=None):
        for slice in slice_index:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)

            pattern_control_register.NewBurst = 1
            pattern_control_register.AbortPattern = 0
            pattern_control_register.ChannelSetBaseID = self.slot_index
            for i in range(20):
                # writing the same value multiple times ensures NewBurst is high for long enough
                self.write_slice_register(pattern_control_register, slice)
            pattern_control_register.NewBurst = 0
            self.write_slice_register(pattern_control_register, slice)

    def read_error_count_on_channel_sets(self, pattern_string='0'):
        now = datetime.datetime.now()
        date_time = now.strftime("%Y-%m-%d_%H.%M.%S")
        OUTPUT_PATH = os.path.join(projectpaths.ROOT_PATH, configs.RESULTS_DIR,
                                   'DUMP_PATTERN_STRING_AND_ERRORCOUNT_' + date_time + self.name())
        os.makedirs(OUTPUT_PATH)
        path = os.path.join(OUTPUT_PATH, 'PatternString.txt')
        error_count = os.path.join(OUTPUT_PATH, 'ErrorCount_' + date_time + '.csv')
        with open(path, 'w') as f:
            f.write(pattern_string)
        with open(error_count, 'w') as fout:
            writer = csv.writer(fout)
            writer.writerow(["CHANNEL SET", "Slice", "Pin Index Group", "Error Count"])

            for channel_set in range(4):
                channel_set_register = 'CHANNEL_SET_{}'.format(channel_set)
                for slice in range(0, 5):
                    for pin in range(18):
                        error_count_value = self.read_register_raw(getattr(self.registers, channel_set_register),
                                                               bar_space=slice, index=pin)
                        fout.write('{},{},{},0x{:0x}\n'.format(channel_set, slice, pin, error_count_value))

    def channel_error_counts(self, channel_set, slice):
        RegisterType = [self.registers.CHANNEL_SET_0, 
                        self.registers.CHANNEL_SET_1, 
                        self.registers.CHANNEL_SET_2, 
                        self.registers.CHANNEL_SET_3][channel_set]
        error_counts = []
        for index in range(RegisterType.REGCOUNT):
            reg = self.read_slice_register(RegisterType, slice, index=index)
            error_counts.append(reg.lower_fail_count)
            error_counts.append(reg.upper_fail_count)
        return error_counts[:self.CHANNELS_PER_SLICE]

    def construct_csv_file_dir(self):
        now = datetime.datetime.now()
        date_time = now.strftime("%Y-%m-%d_%H.%M.%S")
        OUTPUT_PATH = os.path.join(projectpaths.ROOT_PATH, configs.RESULTS_DIR,
                                   'DUMP_PATTERN_STRING_AND_ERRORCOUNT_' + date_time + self.name())
        return os.makedirs(OUTPUT_PATH)

    def read_vref(self, channel_index):
        return hil.hbiPsdbVmonRead(self.slot, channel_index)

    def read_all_vmon(self):
        return hil.hbiPsdbVmonReadAll(self.slot)

    def abort_pattern(self, slice_list):
        for slice in slice_list:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.AbortPattern = 1
            self.write_slice_register(pattern_control_register, slice)

    def phylite_reset(self):
        pyhlite_reset_slice = 0
        pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, pyhlite_reset_slice)
        pattern_control_register.PhyliteReset = 1
        pattern_control_register.PhyliteAVMMReset = 1
        self.write_slice_register(pattern_control_register, pyhlite_reset_slice)
        pattern_control_register.PhyliteReset = 0
        pattern_control_register.PhyliteAVMMReset = 0
        self.write_slice_register(pattern_control_register, pyhlite_reset_slice)

    def get_alarm(self, slices, has_failed):
        values = {}
        for slice in slices:
            alarm_object = self.read_slice_register(self.registers.ALARMS, slice)
            values.update({slice: alarm_object})
            if alarm_object.value != 0:
                has_failed.append(alarm_object)
        return {self.name(): {self.registers.ALARMS(0).name(): values}}

    def read_alarms(self, slice):
        alarms = []
        alarm_register = self.read_slice_register(self.registers.ALARMS, slice)
        self.Log('debug', f'{self.name()} slice {slice}: ALARM RAW VALUE: 0x{alarm_register.value:08X}')
        if alarm_register.value != 0:
            for field in alarm_register._fields_:

                field_name = field[0]
                field_value = getattr(alarm_register, field[0])
                if field_value != 0 and 'reserved' not in field_name.lower():
                    alarms.append(field_name + ': ' + str(field_value))
            return ', '.join(alarms)
        return ''

    def set_ec_ratio(self):
        for slice in range(0, 5):
            for pin_count in range(35):
                per_channel_config_register = self.read_slice_register(self.registers.PER_CHANNEL_CONFIG2, slice,
                                                                 index=pin_count)
                per_channel_config_register.ECIsRatio1 = 1
                self.write_slice_register(per_channel_config_register, slice, index=pin_count)

    def poll_for_rx_fifo_full(self, retry_limit, slice_list):
        retry_slice_list = []
        if retry_limit == 0:
            self.Log('warning',
                     'Maximum number of retries exceeded for {} slice {} Rx FIFO full.'.format(self.name(), slice_list))
            return False
        for bar in slice_list:
            prestage_status = self.get_ringbus_status(bar)
            if prestage_status.RXFIFOAllMostFull != 1:
                retry_limit = retry_limit - 1
                retry_slice_list.append(bar)
        if retry_slice_list:
            time.sleep(0.1)
            return self.poll_for_rx_fifo_full(retry_limit, retry_slice_list)
        else:
            return True

    def get_ringbus_status(self, slice):
        return self.read_slice_register(self.registers.RM_RING_BUS_STATUS, slice)

    def set_active_slice(self, slice, mask):
        pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
        pattern_control_register.SyncStartSliceMask = mask
        self.write_slice_register(pattern_control_register, slice)

    def mask_inactive_slice(self, slice_list):

        for slice in slice_list:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.SyncStartSliceMask = 0
            self.write_slice_register(pattern_control_register, slice)

    def set_mtv_fail_mask(self, slice, channel_sets, reg_index, mask):
        mask = self._resolve_reserved_phylite_pins(slice, mask)

        lower_mask = mask & 0xffffffff
        upper_mask = mask >> 32
        lower_index = 2 * reg_index
        upper_index = 2 * reg_index + 1

        for channel_set in [channel_set % 4 for channel_set in channel_sets]:
            fail_mask_register = 'CHANNEL_SET_{}_FAILMASK'.format(channel_set)
            mask_register_class = getattr(self.registers, fail_mask_register)

            self.write_slice_register(mask_register_class(lower_mask), slice=slice, index=lower_index)
            self.write_slice_register(mask_register_class(upper_mask), slice=slice, index=upper_index)

    def _resolve_reserved_phylite_pins(self, slice, mask):
        if slice == 4:
            mask |= 0b11111111111110000000000000000000000
        return mask

    def set_subcycles_per_cycle_exp(self, exponent, slices=SLICECOUNT):
        self.Log('debug', f'Setting {self.name()} SubCycle Exponent to {exponent} on slices {slices}')
        for slice in slices:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.SubcyclesPerCycleExp = exponent
            self.write_slice_register(pattern_control_register, slice)
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            if exponent != pattern_control_register.SubcyclesPerCycleExp:
                self.Log('error', f'{self.name()} SUBCycle was not set to the expected exponent of {exponent}')

    def set_active_pm(self,slice_list):
        for slice in slice_list:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            capture_control_register = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice)
            pattern_control_register.ChannelSetBaseID= self.slot_index
            capture_control_register.PmChipId = self.slot_index
            self.write_slice_register(pattern_control_register, slice)
            self.write_slice_register(capture_control_register, slice)

    def set_available_pms(self, slice_list):
        for slice in slice_list:
            pattern_control_register = self.read_slice_register(self.registers.PATTERN_CONTROL, slice)
            pattern_control_register.ChannelSetBaseID = self.slot_index
            self.write_slice_register(pattern_control_register, slice)

    def read_pin_state(self, channel_set, slice):
        slice_channel_set_pin_state_map = {}
        raw_channel_state_register = 'RAW_STATE_CHANNEL_SET_{}'.format(channel_set)
        pin_state_for32_pins = self.read_register_raw(getattr(self.registers, raw_channel_state_register),
                                                  slice, index=0)
        pin_state_for3_pins = self.read_register_raw(getattr(self.registers, raw_channel_state_register),
                                                 slice, index=1)
        total_pin_state_for_35_pins = (pin_state_for3_pins << 32) | pin_state_for32_pins

        slice_channel_set_pin_state_map.update({channel_set: {slice: total_pin_state_for_35_pins}})
        return slice_channel_set_pin_state_map

    def detect_logic_state(self, channel_sets, slice, state):
        fab = self.psdb.blt.fab_letter()
        scoreboard_logic_high = {}
        scoreboard_logic_low = {}
        phylyite_pin_ignore_list = self.PHYLITE_PIN_IGNORE_LIST[fab]
        for channel_set in channel_sets:
            one_pins_per_slice = []
            zero_pins_per_slice = []
            actual_channel_set_no = channel_set
            if channel_set > 3:
                channel_set -= 4
            channel_set_slice_pin_state = self.read_pin_state(channel_set, slice)
            for pin in range(35):
                if pin not in phylyite_pin_ignore_list[channel_set][slice]:
                    if channel_set_slice_pin_state[channel_set][slice] & (1 << pin):
                        one_pins_per_slice.append(pin)
                    else:
                        zero_pins_per_slice.append(pin)
            scoreboard_logic_high.update({actual_channel_set_no: one_pins_per_slice})
            scoreboard_logic_low.update({actual_channel_set_no: zero_pins_per_slice})
        return scoreboard_logic_high, scoreboard_logic_low

    def read_pin_state(self, channel_set, slice):
        slice_channel_set_pin_state_map = {}
        raw_channel_state_register = 'RAW_STATE_CHANNEL_SET_{}'.format(channel_set)
        pin_state_for32_pins = self.read_register_raw(getattr(self.registers, raw_channel_state_register),
                                                  slice, index=0)
        pin_state_for3_pins = self.read_register_raw(getattr(self.registers, raw_channel_state_register),
                                                 slice, index=1)

        total_pin_state_for_35_pins = (pin_state_for3_pins << 32) | pin_state_for32_pins

        slice_channel_set_pin_state_map.update({channel_set: {slice: total_pin_state_for_35_pins}})
        return slice_channel_set_pin_state_map

    def is_phylite_pin(self, channel_set, slice, pin):
        if pin in self.PHYLITE_PIN_IGNORE_LIST[self.fab_version][channel_set][slice]:
            return True
        return False
        
    def get_pin_ignores(self, channel_set, slice):
        fab = self.psdb.blt.fab_letter()
        return self.PHYLITE_PIN_IGNORE_LIST[fab][channel_set][slice]

    def is_dtb_present(self):
        try:
            if hil.hbiDtbFanRead(self.slot_index):
                self.dtb = True
                return {'Device': self.name(), 'DTB': self.dtb}
        except RuntimeError:
            return {'Device': self.name(), 'DTB': self.dtb}

    def read_under_run_registers(self, slice_list=SLICECOUNT):
        registers = [self.registers.L_COUNTER,
                     self.registers.H_COUNTER,
                     self.registers.X_COUNTER,
                     self.registers.ONE_COUNTER,
                     self.registers.ZERO_COUNTER,
                     self.registers.E_COUNTER,
                     self.registers.K_COUNTER,
                     self.registers.DEFAULT_COUNTER,
                     self.registers.LAST_CYCLE_SEEN,
                     self.registers.UNDER_RUN_COUNTER,
                     self.registers.CAPTURE_CONTROL,
                     self.registers.PATTERN_CONTROL,
                     self.registers.TRIGGER_WORD_LAST_CYCLE_COUNTER,
                     self.registers.VECTOR_ASSEMBLER_LAST_CYCLE_COUNTER,
                     self.registers.SYMBOL_DECODER_LAST_CYCLE_COUNTER,
                     self.registers.K_DEBUG_VA_PAGE_7To3,
                     self.registers.K_DEBUG_VA_PAGE_3To7,
                     self.registers.K_DEBUG_VA_PAGE_KSEEN,
                     self.registers.VA_WR_IN,
                     self.registers.VA_WR_OUT,
                     self.registers.K_DEBUG_VA_PAGE_DUTSERIAL_CONFIG,
                     self.registers.SYMB_DEC_WR_EN_SLICE,
                     self.registers.SYMB_DEC_ALL_WR_EN_SLICE,
                     self.registers.PATTERN_CONTROL,
                     self.registers.ALARMS
                     ]
        self.read_log_set_of_registers(registers, slices=slice_list, title=self.name())


    def get_aurora_pattern_control_register(self,receiver):
        options = {'RINGMULTIPLIER': self.registers.PM_AURORA_PATTERN_CONTROL,
                   'PSDB_0_PIN_0': self.registers.PM_AURORA_PATTERN_CONTROL,
                   'PSDB_0_PIN_1': self.registers.RESEND_TO_PM1_AURORA_PATTERN_CONTROL,
                   'PSDB_1_PIN_0': self.registers.PM_AURORA_PATTERN_CONTROL,
                   'PSDB_1_PIN_1': self.registers.RESEND_TO_PM1_AURORA_PATTERN_CONTROL}
        return options[receiver]

    def get_aurora_pattern_status_register(self, receiver):
        options = {'RINGMULTIPLIER': self.registers.PM_AURORA_PATTERN_STATUS,
                   'PSDB_0_PIN_0': self.registers.PM_AURORA_PATTERN_STATUS,
                   'PSDB_0_PIN_1': self.registers.RESEND_TO_PM1_AURORA_PATTERN_STATUS,
                   'PSDB_1_PIN_0': self.registers.PM_AURORA_PATTERN_STATUS,
                   'PSDB_1_PIN_1': self.registers.RESEND_TO_PM1_AURORA_PATTERN_STATUS}
        return options[receiver]

    def reset_pattern_aurora_link(self, register):
        register.reset = 0x1
        register.clear_error_count = 0x1
        self.write_slice_register(register)
        register.reset = 0x0
        register.clear_error_count = 0x0
        self.write_slice_register(register)

    def disable_pattern_aurora(self,register):
        r = self.read_slice_register(register)
        r.reset = 1
        r.clear_error_count = 1
        r.clear_fifos = 1
        self.write_slice_register(r)

    def enable_pattern_aurora(self,register):
        r = self.read_slice_register(register)
        r.reset = 0
        r.clear_error_count = 0
        r.clear_fifos = 0
        self.write_slice_register(r)

    def clear_aurora_pattern_trigger_error_count(self,register):
        r = self.read_slice_register(register)
        r.clear_error_count = 0x1
        self.write_slice_register(r)
        time.sleep(0.001)
        r.clear_error_count = 0x0
        self.write_slice_register(r)

    def is_burst_start_seen(self, retry_limit=5, slice_list=range(0, 5)):
        failed = []
        for slice in slice_list:
            for i in range(retry_limit):
                pattern_running_status = self.read_slice_register(self.registers.ALARMS, slice)
                if pattern_running_status.BurstStartSeen == 1:
                    break
            else:
                failed.append(slice)
        if failed:
            return False
        return True

    def write_register_direct(self, slice, offset, data):
        return hil.hbiPsdbBarWrite(slot=self.slot, index=self.fpga_index, bar=slice+1, offset=offset, data=data)

    def set_per_pattern_max_fail_count(self, fail_count_value, slices=SLICECOUNT):
        for slice in slices:
            per_pattern_max_fail_register = self.read_slice_register(self.registers.MAX_PATTERN_ERROR_COUNT, slice)
            per_pattern_max_fail_register.Count = fail_count_value
            self.write_slice_register(per_pattern_max_fail_register, slice)
            per_pattern_max_fail_register = self.read_slice_register(self.registers.MAX_PATTERN_ERROR_COUNT, slice)
            if fail_count_value != per_pattern_max_fail_register.Count:
                self.Log('error', f'{self.name()} Per Pattern error count {fail_count_value}')

    def set_global_max_fail_count(self, global_fail_count, slices=SLICECOUNT):
        for slice in slices:
            global_max_fail_register = self.read_slice_register(self.registers.MAX_GLOBAL_ERROR_COUNT, slice)
            global_max_fail_register.Count = global_fail_count
            self.write_slice_register(global_max_fail_register, slice)
            global_max_fail_register = self.read_slice_register(self.registers.MAX_GLOBAL_ERROR_COUNT, slice)
            if global_fail_count != global_max_fail_register.Count:
                self.Log('error', f'{self.name()} Global fail error count {global_fail_count}')

    def set_e32_interval(self, interval, slices=range(5)):
        for slice in slices:
            self._set_capabilities(slice)
            self._set_e32_interval(interval, slice)

    def _set_e32_interval(self, interval, slice):
        register = self.read_slice_register(self.registers.E32_INTERVAL, slice=slice)
        register.e32_interval = interval
        self.write_slice_register(register, slice=slice)
        register = self.read_slice_register(self.registers.E32_INTERVAL, slice=slice)
        self.Log('debug', f'{self.name()} Interval: {register.value}')

    def _set_capabilities(self, slice):
        register = self.read_slice_register(self.registers.CAPABILITIES, slice=slice)
        register.set_e32_interval = 1
        self.write_slice_register(register, slice=slice)

    def set_ctp_override(self, ctp_override_chip_wide=True, active_channel_set=0, slices=SLICECOUNT):
        for slice in slices:
            if ctp_override_chip_wide:
                self._set_ctp_override_chip_wide(slice)
            else:
                self._set_ctp_override_channel_set(active_channel_set, slice)

    def _set_ctp_override_channel_set(self, active_channel_set, slice):
        capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice=slice)
        capture_control.ctp_override_active = 1
        capture_control.ctp_channel_sets_active = active_channel_set
        self.write_slice_register(capture_control, slice=slice)

    def _set_ctp_override_chip_wide(self, slice):
        capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice=slice)
        capture_control.ctp_override_active = 1
        capture_control.ctp_override_chip_wide = 1
        self.write_slice_register(capture_control, slice=slice)

    def reset_ctp_override_channel_sets(self, slices=SLICECOUNT):
        for slice in slices:
            capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice=slice)
            capture_control.ctp_override_active = 0
            self.write_slice_register(capture_control, slice=slice)

    def _reset_ctp_override(self, slices=SLICECOUNT):
        for slice in slices:
            capture_control = self.registers.CAPTURE_CONTROL(value=0)
            self.write_slice_register(capture_control, slice=slice)

    def set_delay_stack_on_all_pins(self, RXdelay=67, TXdelay=0, pins=range(140), slices=range(5)):
        for slice in slices:
            for pin in pins:
                self.set_delay_stack_on_single_pin(slice, pin, RXdelay, TXdelay)

    def set_delay_stack_on_single_pin(self, slice, channel, RXdelay=67, TXdelay=0):
        RXsubcycle = RXdelay % 8
        RXcycle = RXdelay // 8
        config1reg = self.registers.PER_CHANNEL_CONFIG1()
        config1reg.SubCycleDelayRxFromDut = RXsubcycle
        config1reg.TesterCycleDelayRxFromDut = RXcycle
        config1reg.TotalCycleDelayTxToDut = TXdelay
        self.write_slice_register(config1reg, slice, channel)

    def set_all_EC_pins_on_pm(self, first_edge=1, HighCycles=0, LowCycles=0, Halfcycledelay=0, ECIsRatio1=0):
        # Function sets all pins to same value (non EC pins will just have their register ignored.
        for slice_ind in range(5):
            for pin_ind in range(0, 35, 8):
                for channel_set_ind in range(4):
                    self.write_single_pin_config(slice_ind, pin_ind, first_edge=first_edge,
                                                 channel_set=channel_set_ind,
                                                 HighCycles=HighCycles, LowCycles=LowCycles,
                                                 Halfcycledelay=Halfcycledelay, ECIsRatio1=ECIsRatio1)

    def write_single_pin_config(self, slice, pin, channel_set=0, ECIsRatio1=0, first_edge=0, HighCycles=0, LowCycles=0,
                                Halfcycledelay=0):
        pin_index = pin + channel_set * 35
        config2reg = self.registers.PER_CHANNEL_CONFIG2()
        config2reg.EnableClkTesterCycleHigh = HighCycles
        config2reg.EnableClkTesterCycleLow = LowCycles
        config2reg.EcFirstEdge = first_edge
        config2reg.EcAddHalfCycle = Halfcycledelay
        config2reg.ECIsRatio1 = ECIsRatio1
        self.write_slice_register(config2reg, slice, index=pin_index)

    def clear_all_EC_pins_on_pm(self):
        # Function sets all pins to same value (non EC pins will just have their register ignored.
        for slice_ind in range(5):
            for pin_ind in range(0, 35, 8):
                for channel_set_ind in range(4):
                    self.clear_single_enabled_clock_register(slice_ind, pin_ind, channel_set_ind)

    def clear_single_enabled_clock_register(self, slice, pin, channel_set):
        pin_index = pin + channel_set * 35
        config2reg = self.registers.PER_CHANNEL_CONFIG2()
        config2reg.EnableClkTesterCycleHigh = 0
        config2reg.EnableClkTesterCycleLow = 0
        config2reg.EcFirstEdge = 0
        config2reg.EcAddHalfCycle = 0
        config2reg.ECIsRatio1 = 0
        self.write_slice_register(config2reg, slice, index=pin_index)

    def set_all_FRC_pins_on_pm(self, first_edge=1, period=2, FRCIsRatio1=0, FRCMode=1):
        # Function sets all pins to same value (non EC pins will just have their register ignored.
        for slice_ind in range(5):
            for pin_ind in range(0, 35, 8):
                for channel_set_ind in range(4):
                    self.write_single_pin_config_FRC(slice_ind, pin_ind, first_edge=first_edge,
                                                     channel_set=channel_set_ind,
                                                     period=period, FRCIsRatio1=FRCIsRatio1, FRCMode=FRCMode)

    def write_single_pin_config_FRC(self, slice, pin, period=2, channel_set=0, FRCIsRatio1=0, first_edge=0, FRCMode=1):
        pin_index = pin + channel_set * 35
        config3reg = self.registers.PER_CHANNEL_CONFIG3()
        config3reg.FRCMode = FRCMode
        config3reg.FRCDivider = period - 2
        config3reg.FRCStartState = first_edge
        config3reg.FRCIsRatio1 = FRCIsRatio1
        self.write_slice_register(config3reg, slice, index=pin_index)

    def clear_all_FRC_pins_on_pm(self):
        # Function sets all pins to same value (non EC pins will just have their register ignored.
        for slice_ind in range(5):
            for pin_ind in range(0, 35, 8):
                for channel_set_ind in range(4):
                    self.clear_single_pin_config_FRC(slice_ind, pin_ind, channel_set=channel_set_ind)

    def clear_single_pin_config_FRC(self, slice, pin, channel_set):
        pin_index = pin + channel_set * 35
        config3reg = self.registers.PER_CHANNEL_CONFIG3()
        config3reg.FRCMode = 0
        config3reg.FRCDivider = 0
        config3reg.FRCStartState = 0
        config3reg.FRCIsRatio1 = 0
        self.write_slice_register(config3reg, slice, index=pin_index)

    def read_oct_code(self,oct_bank_register):
        oct_bank_register =self.read_slice_register(getattr(self.registers, oct_bank_register))
        n_leg_value= oct_bank_register.n_leg_value
        p_leg_value= oct_bank_register.p_leg_value
        return n_leg_value,p_leg_value

    def take_raw_register_snapshot(self,directory):
        startsaddr=0
        end_addr= 0x2004
        for slice in range(0, 5):
            filename = os.path.join(directory, f'registers_raw_slice_{slice}.csv')
            with open(filename, 'w') as fout:
                writer = csv.writer(fout)
                writer.writerow(["Address","Register value_Decimal","Register value_Hex" ])
                for offset in range(startsaddr,end_addr,4):
                    data = hil.hbiPsdbBarRead(self.slot,self.fpga_index, bar=slice + 1, offset=offset)
                    fout.write(
                        '0x{:0x},{},0x{:0x},\n'.format(offset, data,data))

    def set_tester_cycle_delay(self,delay,slices):
        TXcycle = delay // 8
        config1reg = self.registers.PER_CHANNEL_CONFIG1()
        config1reg.TesterCyleDelayTxToDut = TXcycle
        for slice in slices:
            for pin in range(140):
                self.write_slice_register(config1reg, slice, pin)

    def check_h_counter(self, expected, slices=SLICECOUNT):
        for slice in slices:
            h_count_reg = self.registers.H_COUNTER
            h_count = self.read_slice_register(h_count_reg, slice=slice).value
            if h_count != expected:
                self.Log('error', f'Slice: {slice} H Count Error: Observed: {h_count:,} expected: {expected:,}')

