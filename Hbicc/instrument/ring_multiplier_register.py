################################################################################
# INTEL CONFIDENTIAL - Copyright 2018-2020. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
from ctypes import c_uint

import Common.register as register


def get_register_types_sorted_by_name():
    return sorted(get_register_types(), key=lambda x: x.__name__)


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, RingMultiplierRegister):
                if attribute is not RingMultiplierRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, RingMultiplierRegister):
                if attribute is not RingMultiplierRegister:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


class RingMultiplierRegister(register.Register):
    BASEMUL = 4
    BAR = 1

    def name(self):
        return type(self).__name__.upper()


class SCRATCH_REG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x0
    REGCOUNT = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AURORA_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class AURORA_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('pll_locked', ctypes.c_uint, 1),
                ('lane_up', ctypes.c_uint, 1),
                ('channel_up', ctypes.c_uint, 1),
                ('soft_error', ctypes.c_uint, 1),
                ('hard_error', ctypes.c_uint, 1),
                ('tx_pll_lock', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 26)]


class AURORA_ERROR_COUNTS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('soft_error_count', ctypes.c_uint, 16),
                ('hard_error_count', ctypes.c_uint, 16)]


class LAST_TRIGGER_SEEN(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x4c
    _fields_ = [('index_of_tq_to_fire', ctypes.c_uint, 12),
                ('reserved', ctypes.c_uint, 7),
                ('sw_up_trigger', ctypes.c_uint, 1),
                ('dut_id', ctypes.c_uint, 6),
                ('target_source', ctypes.c_uint, 6)]

class SEND_TRIGGER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SEND_SW_TRIGGER_UP(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('index_of_trigger_queue', ctypes.c_uint, 10),
                ('reserved', ctypes.c_uint, 9),
                ('software_trigger_up', ctypes.c_uint, 1),
                ('dut_id', ctypes.c_uint, 6),
                ('target_resource', ctypes.c_uint, 6)]


class ALIGN_ON_TRIGGER_SYNC_DELAY_COMMAND(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x58
    _fields_ = [('sync_delay_mode', ctypes.c_uint, 1),  # bit0, 1 means new sync_delay used, 0 means delay =  sync_delay
                ('reserved', ctypes.c_uint, 31)]


class FPGA_VERSION(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_LOWER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x24
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_UPPER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x28
    _fields_ = [('Data', ctypes.c_uint, 16),  # 0
                ('reserved', ctypes.c_uint, 15),
                ('hg_clean', ctypes.c_uint, 1)]


class TARGET_HARDWARE(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x2c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE0_V(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x30
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x34
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_L(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x38
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_U(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x30
    REGCOUNT = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_RING_BUS_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x80
    _fields_ = [('EnablePrbs', c_uint, 1),
                ('PRBSFilter', c_uint, 1),
                ('ForceRealign', c_uint, 1),
                ('ResetErrorCount', c_uint, 1),
                ('PrbsInsertError', c_uint, 1),
                ('Reset', c_uint, 1),
                ('SerialLoopbackLane0', c_uint, 1),
                ('SerialLoopbackLane1', c_uint, 1),
                ('SerialLoopbackLane2', c_uint, 1),
                ('SerialLoopbackLane4', c_uint, 1),
                ('Reset_CRC_Error', c_uint, 1),
                ('Reserved', c_uint, 21)]


class PATGEN_RING_BUS_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x84
    _fields_ = [('WordAligned', c_uint, 1),
                ('DecoderError', c_uint, 1),
                ('ErrorDeskew', c_uint, 1),
                ('PrbsSuccess', c_uint, 1),
                ('XoffReceived', c_uint, 1),
                ('LinkUp', c_uint, 1),
                ('RxFreqLocked', c_uint, 1),
                ('LaneAligned', c_uint, 1),
                ('PllLocked', c_uint, 1),
                ('PrbsLocked', c_uint, 1),
                ('Reserved', c_uint, 22)]


class PATGEN_RING_BUS_ERROR_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x88
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_XCVR_ERROR_COUNT_LANE(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x94
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_RING_BUS_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xC0
    _fields_ = [('EnablePrbs', c_uint, 1),
                ('PRBSFilter', c_uint, 1),
                ('ForceRealign', c_uint, 1),
                ('ResetErrorCount', c_uint, 1),
                ('PrbsInsertError', c_uint, 1),
                ('Reset', c_uint, 1),
                ('SerialLoopbackLane0', c_uint, 1),
                ('SerialLoopbackLane1', c_uint, 1),
                ('SerialLoopbackLane2', c_uint, 1),
                ('SerialLoopbackLane4', c_uint, 1),
                ('Reset_CRC_Error', c_uint, 1),
                ('Reserved', c_uint, 21)]


class PSDB0_RING_BUS_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xC4
    _fields_ = [('WordAligned', c_uint, 1),
                ('DecoderError', c_uint, 1),
                ('ErrorDeskew', c_uint, 1),
                ('PrbsSuccess', c_uint, 1),
                ('XoffReceived', c_uint, 1),
                ('LinkUp', c_uint, 1),
                ('RxFreqLocked', c_uint, 1),
                ('LaneAligned', c_uint, 1),
                ('PllLocked', c_uint, 1),
                ('PrbsLocked', c_uint, 1),
                ('Reserved', c_uint, 22)]


class PSDB0_RING_BUS_ERROR_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xC8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB1_RING_BUS_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xCC
    _fields_ = [('EnablePrbs', c_uint, 1),
                ('PRBSFilter', c_uint, 1),
                ('ForceRealign', c_uint, 1),
                ('ResetErrorCount', c_uint, 1),
                ('PrbsInsertError', c_uint, 1),
                ('Reset', c_uint, 1),
                ('SerialLoopbackLane0', c_uint, 1),
                ('SerialLoopbackLane1', c_uint, 1),
                ('SerialLoopbackLane2', c_uint, 1),
                ('SerialLoopbackLane4', c_uint, 1),
                ('Reset_CRC_Error', c_uint, 1),
                ('Reserved', c_uint, 21)]


class PSDB1_RING_BUS_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xD0
    _fields_ = [('WordAligned', c_uint, 1),
                ('DecoderError', c_uint, 1),
                ('ErrorDeskew', c_uint, 1),
                ('PrbsSuccess', c_uint, 1),
                ('XoffReceived', c_uint, 1),
                ('LinkUp', c_uint, 1),
                ('RxFreqLocked', c_uint, 1),
                ('LaneAligned', c_uint, 1),
                ('PllLocked', c_uint, 1),
                ('PrbsLocked', c_uint, 1),
                ('Reserved', c_uint, 22)]


class PSDB1_RING_BUS_ERROR_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xD4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DC_TRIGGER_QUEUE_RECORD_LOW(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xE0
    _fields_ = [('PayLoadLower', ctypes.c_uint, 32)]


class DC_TRIGGER_QUEUE_RECORD_HIGH(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xE4
    _fields_ = [('PayLoadUpper', ctypes.c_uint, 16),
                ('InterfaceTarget', ctypes.c_uint, 4),
                ('ChannelSet', ctypes.c_uint, 3),
                ('SPITarget', ctypes.c_uint, 3),
                ('ValidPayloadByteCount', ctypes.c_uint, 3),
                ('PSDBTarget', ctypes.c_uint, 1),
                ('WriteReadn', ctypes.c_uint, 1),
                ('CommandData', ctypes.c_uint, 1)]


class PSDB_DC_TRIGGER_PAGE_I2C_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xE8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB_DC_TRIGGER_PAGE_I2C_META_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xF0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_DC_TRIGGER_PAGE_SPI_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xF4
    _fields_ = [('reserved', ctypes.c_uint, 3),
                ('interface', ctypes.c_uint, 4),
                ('psdb_target', ctypes.c_uint, 1),
                ('data', ctypes.c_uint, 24)]


class PSDB1_DC_TRIGGER_PAGE_SPI_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xF8
    _fields_ = [('reserved', ctypes.c_uint, 3),
                ('interface', ctypes.c_uint, 4),
                ('psdb_target', ctypes.c_uint, 1),
                ('data', ctypes.c_uint, 24)]


class DC_TRIGGER_PAGE_PROCESSED_QUEUES(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xFC
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_RING_BUS_PATTERN_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x8C
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_RING_BUS_PATTERN_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x100
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB1_RING_BUS_PATTERN_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x120
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_XCVR_ERROR_COUNT_LANE(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x108
    REGCOUNT = 4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB1_XCVR_ERROR_COUNT_LANE(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x128
    REGCOUNT = 4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DDR_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x200
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DDR_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x204
    _fields_ = [('Data', ctypes.c_uint, 32)]


class GPIO_REFERENCE_CLOCK(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xA0
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class TRIGGER_FABRIC_CLOCK_FREQUQNECY(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xA4
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class PCIE_FABRIC_CLOCK_FREQUENCY(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xA8
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class DDR4_FABRIC_CLOCK_FREQUENCY(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xAC
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class SPI_CLOCK_FREQUNECY(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xB0
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class PATGEN_TRANSCEIVER_TOP_PLL_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x140
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PATGEN_TRANSCEIVER_TOP_PLL_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x144
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_TRANSCEIVER_TOP_PLL_RECONFIG_READ_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_TRANSCEIVER_TOP_PLL_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x14C
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PATGEN_TRANSCEIVER_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x150
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PATGEN_TRANSCEIVER_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x154
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_TRANSCEIVER_RECONFIG_READ_DATA_(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x158
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_TRANSCEIVER_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x15C
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PATGEN_TRANSCEIVER_BOTTOM_PLL_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x160
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PATGEN_TRANSCEIVER_BOTTOM_PLL_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x164
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_TRANSCEIVER_BOTTOM_PLL_RECONFIG_READ_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x168
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATGEN_TRANSCEIVER_BOTTOM_PLL_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x16C
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PSDB0_TRANSCEIVER_PLL_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x180
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PSDB0_TRANSCEIVER_PLL_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x184
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_TRANSCEIVER_PLL_RECONFIG_READ_DATA_(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x188
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_TRANSCEIVER_PLL_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x18C
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PSDB0_TRANSCEIVER_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x190
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PSDB0_TRANSCEIVER_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x194
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_TRANSCEIVER_RECONFIG_READ_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x198
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_TRANSCEIVER_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x19C
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PSDB1_TRANSCEIVER_PLL_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1C0
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PSDB1_TRANSCEIVER_PLL_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1C4
    __fields__ = [('Data', ctypes.c_uint, 32)]


class PSDB1_TRANSCEIVER_PLL_RECONFIG_READ_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1C8
    __fields__ = [('Data', ctypes.c_uint, 32)]


class PSDB1_TRANSCEIVER_PLL_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1CC
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PSDB1_TRANSCEIVER_RECONFIG_ADDRESS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1D0
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PSDB1_TRANSCEIVER_RECONFIG_WRITE_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1D4
    __fields__ = [('Data', ctypes.c_uint, 32)]


class PSDB1_TRANSCEIVER_RECONFIG_READ_DATA(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1D8
    __fields__ = [('Data', ctypes.c_uint, 32)]


class PSDB1_TRANSCEIVER_RECONFIG_READ_DATA_TAG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x1DC
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class DC_TRIGGER_PAGE_2_PROCESSED(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x240
    _fields_ = [('Data', c_uint, 32)]


class DC_TRIGGER_LAST_WORD_LOWER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x244
    _fields_ = [('Data', c_uint, 32)]


class DC_TRIGGER_LAST_WORD_UPPER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x248
    _fields_ = [('Data', c_uint, 32)]


class PATGEN_XCVR_TX_PATTERN_COUNTER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x260
    REGCOUNT = 2
    _fields_ = [('Data', c_uint, 32)]


class PATGEN_XCVR_RX_PATTERN_COUNTER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x268
    REGCOUNT = 2
    _fields_ = [('Data', c_uint, 32)]


class PSDB0_XCVR_TX_PATTERN_COUNTER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x270
    REGCOUNT = 2
    _fields_ = [('Data', c_uint, 32)]


class PSDB0_XCVR_RX_PATTERN_COUNTER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x278
    REGCOUNT = 2
    _fields_ = [('Data', c_uint, 32)]


class PSDB1_XCVR_TX_PATTERN_COUNTER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x280
    REGCOUNT = 2
    _fields_ = [('Data', c_uint, 32)]


class PSDB1_XCVR_RX_PATTERN_COUNTER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x288
    REGCOUNT = 2
    _fields_ = [('Data', c_uint, 32)]


class PATGEN_XCVR_LAST_RX_SEEN(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x2C0
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PSDB0_XCVR_LAST_RX_SEEN(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x300
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]


class PSDB1_XCVR_LAST_RX_SEEN(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x320
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]


class PSDB0_XCVR_LAST_TX_SEEN(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x340
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]


class PSDB1_XCVR_LAST_TX_SEEN(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x360
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]


class AURORA_PATTERN_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x224
    _fields_ = [('burst_sync_start_valid', c_uint, 1),
                ('sync_start_sliceid', c_uint, 3),
                ('psdb0_present', c_uint, 1),
                ('psdb1_present', c_uint, 1),
                ('reserved', c_uint, 26)]


class PSDB0_AURORA_PATTERN_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x228
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class PSDB0_AURORA_PATTERN_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x22c
    _fields_ = [('pll_locked', c_uint, 1),
                ('lane_up', c_uint, 1),
                ('channel_up', c_uint, 1),
                ('soft_error', c_uint, 1),
                ('hard_error', c_uint, 1),
                ('tx_pll_locked', c_uint, 1),
                ('reserved', c_uint, 27)]


class PSDB0_AURORA_PATTERN_ERROR_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x230
    _fields_ = [('soft_error_count', c_uint, 16),
                ('hard_error_count', c_uint, 16)]


class PSDB1_AURORA_PATTERN_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x234
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class PSDB1_AURORA_PATTERN_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x238
    _fields_ = [('pll_locked', c_uint, 1),
                ('lane_up', c_uint, 1),
                ('channel_up', c_uint, 1),
                ('soft_error', c_uint, 1),
                ('hard_error', c_uint, 1),
                ('tx_pll_locked', c_uint, 1),
                ('reserved', c_uint, 27)]


class PSDB1_AURORA_PATTERN_ERROR_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x23c
    _fields_ = [('soft_error_count', c_uint, 16),
                ('hard_error_count', c_uint, 16)]


class DC_TRIGGER_PAGE_2_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x24C
    _fields_ = [('i2c_reset', c_uint, 1),  # lsb
                ('aurora_data_enable', c_uint, 1),
                ('dctq_reset', c_uint, 1),
                ('reserved', c_uint, 29)]  # msb


class DC_TRIGGER_PAGE_2_AURORA_TRIGGER(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x250
    _fields_ = [('trigger_value', c_uint, 32)]


class DC_TRIGGER_FOLD_EVENT_LUT_INDEX(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x25C
    _fields_ = [('index', c_uint, 32)]


class DUT_POWER_STATE_RD_31__0(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x340
    _fields_ = [('power_state', c_uint, 32)]


class DUT_POWER_STATE_RD_63_32(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x344
    _fields_ = [('power_state', c_uint, 32)]


class DUT_POWER_STATE_MASK_WR_15__0(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x348
    _fields_ = [('mask', c_uint, 16),
                ('power_state', c_uint, 16)]  # msb


class DUT_POWER_STATE_MASK_WR_31_16(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x34C
    _fields_ = [('mask', c_uint, 16),
                ('power_state', c_uint, 16)]  # msb


class DUT_POWER_STATE_MASK_WR_47_32(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x350
    _fields_ = [('mask', c_uint, 16),
                ('power_state', c_uint, 16)]  # msb


class DUT_POWER_STATE_MASK_WR_63_48(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x354
    _fields_ = [('mask', c_uint, 16),
                ('power_state', c_uint, 16)]  # msb


class DC_TRIGGER_DEBUG(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x358
    _fields_ = [('spi_fifo_empty', c_uint, 1),
                ('spi_fifo_full', c_uint, 1),
                ('spi_busy', c_uint, 1),
                ('reserved1', c_uint, 13),
                ('i2c_fifo_empty', c_uint, 1),
                ('i2c_fifo_full', c_uint, 1),
                ('i2c_busy', c_uint, 1),
                ('reserved2', c_uint, 13)]  # msb


DFT_BASE = 0x360


class PSDB0_ARBITER_WRITE_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = DFT_BASE + 0
    _fields_ = [('Data', c_uint, 32)]


class DC_TRIGGER_PAGE_2_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x24C
    _fields_ = [('i2c_reset', c_uint, 1),  # lsb
                ('aurora_data_enable', c_uint, 1),
                ('dctq_reset', c_uint, 1),
                ('reserved', c_uint, 29)]  # msb


class PSDB1_ARBITER_WRITE_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = DFT_BASE + 4
    _fields_ = [('Data', c_uint, 32)]


class PATGEN_FIFO_WRITE_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = DFT_BASE + 8
    _fields_ = [('Data', c_uint, 32)]


class PATGEN_FIFO_READ_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = DFT_BASE + 12
    _fields_ = [('Data', c_uint, 32)]


class MICRO_FIFO_WRITE_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = DFT_BASE + 16
    _fields_ = [('Data', c_uint, 32)]


class MICRO_FIFO_READ_COUNT(RingMultiplierRegister):
    BAR = 1
    ADDR = DFT_BASE + 20
    _fields_ = [('Data', c_uint, 32)]


class RING_INTERCHIP_RESET(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x380
    _fields_ = [('reset', c_uint, 1),
                ('Reserved', c_uint, 31)]


# TODO: RM to PatGen Snooping Registers
PACKET_CONTROL_PAGE = 0x0460
PACKET_SNAPSHOT_PAGE = 0x0480
PACKET_TOTAL_PAGE = 0x04A0
PACKET_EOB_TOTAL_PAGE = 0x04C0
PACKET_INVALID_TOTAL_PAGE = 0x04E0


class RESET_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x00
    _fields_ = [('Data', c_uint, 32)]


class SNAPSHOT_CYCLE_NUMBER(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x04
    _fields_ = [('Data', c_uint, 32)]


class FEATURE_SIGNATURE(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x08
    _fields_ = [('Data', c_uint, 32)]


class PM_E_PACKETS(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_PACKETS(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_E_EOB_PACKETS(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_EOB_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_EOB_PACKETS(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_EOB_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_E_INVALID_PACKETS(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_INVALID_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_INVALID_PACKETS(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_INVALID_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_SNAPSHOT(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_XOR(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


PACKET_CONTROL_0_PAGE = 0x0460
PACKET_SNAPSHOT_0_PAGE = 0x0480
PACKET_CONTROL_1_PAGE = 0x04A0
PACKET_SNAPSHOT_1_PAGE = 0x04C0
ILA_PAGE = 0x0500


class RESET_CONTROL_0(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_0_PAGE + 0x00
    _fields_ = [('Data', c_uint, 32)]


class RESET_CONTROL_1(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_1_PAGE + 0x00
    _fields_ = [('Data', c_uint, 32)]


FEATURE_SIGNATURE_0 = PACKET_CONTROL_0_PAGE + 0x08


class PACKET_SNAPSHOT_BOTTOM(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x00
    REGCOUNT = 8
    _fields_ = [('Data', c_uint, 32)]


FEATURE_SIGNATURE_1 = PACKET_CONTROL_1_PAGE + 0x08


class PACKET_SNAPSHOT_TOP(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x04C0
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class GET_NEXT_CONTROL_0(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_0_PAGE + 0x04
    _field_ = [('Data', c_uint, 32)]


class GET_NEXT_CONTROL_1(RingMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_1_PAGE + 0x04
    _field_ = [('Data', c_uint, 32)]


class CAPTURE_CONTROL(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x520
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


# class START_CYCLE(RingMultiplierRegister):
#     BAR = 1
#     ADDR = PACKET_CONTROL_1_PAGE + 0x0C
#     _field_ = [('Data', c_uint, 32)]


class ILA_CONTROL(RingMultiplierRegister):
    ADDR = 0x0500
    _fields_ = [('word_in_sample', ctypes.c_uint, 4),
                ('sample', ctypes.c_uint, 15),
                ('bypass_write_enable', ctypes.c_uint, 1),
                ('bypass_trigger', ctypes.c_uint, 1),
                ('select', ctypes.c_uint, 3),
                ('reset', ctypes.c_uint, 8)]


class ILA_STATUS(RingMultiplierRegister):
    ADDR = 0x0504
    _fields_ = [('capture_count', ctypes.c_uint, 15),
                ('reserved', ctypes.c_uint, 17)]


class ILA_DATA(RingMultiplierRegister):
    ADDR = 0x0508
    _fields_ = [('Data', ctypes.c_uint, 32)]


# RESET_CONTROL_0 = PACKET_CONTROL_0_PAGE + 0x00
# GET_NEXT_CONTROL_0 = PACKET_CONTROL_0_PAGE + 0x04
# FEATURE_SIGNATURE_0 = PACKET_CONTROL_0_PAGE + 0x08
# START_CYCLE_0 = PACKET_CONTROL_0_PAGE + 0x0C


class PACKET_SNAPSHOT_0_0(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x00
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_1(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x04
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_2(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x08
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_3(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x0C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_4(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x10
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_5(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x14
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_6(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x18
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_0_7(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_0_PAGE + 0x1C
    _fields_ = [('Data', ctypes.c_uint, 32)]




# RESET_CONTROL_1 = PACKET_CONTROL_1_PAGE + 0x00
# GET_NEXT_CONTROL_1 = PACKET_CONTROL_1_PAGE + 0x04
# FEATURE_SIGNATURE_1 = PACKET_CONTROL_1_PAGE + 0x08


class START_CYCLE_0(RingMultiplierRegister):
    ADDR = PACKET_CONTROL_0_PAGE + 0x0C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class START_CYCLE_1(RingMultiplierRegister):
    ADDR = PACKET_CONTROL_1_PAGE + 0x0C
    _fields_ = [('Data', ctypes.c_uint, 32)]




class PACKET_SNAPSHOT_1_0(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x00
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_1(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x04
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_2(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x08
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_3(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x0C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_4(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x10
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_5(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x14
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_6(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x18
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PACKET_SNAPSHOT_1_7(RingMultiplierRegister):
    ADDR = PACKET_SNAPSHOT_1_PAGE + 0x1C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PIN_STATE_CONTROL_WORD_CHANNEL_SET_0(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x360
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_1(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x370
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_2(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x380
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_3(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x390
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_4(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3a0
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_5(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3b0
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_6(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3c0
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_7(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3d0
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_8(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3e0
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_9(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x3f0
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_10(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x400
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_11(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x410
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_12(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x420
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_13(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x430
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_14(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x440
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class PIN_STATE_CONTROL_WORD_CHANNEL_SET_15(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x450
    REGCOUNT = 3
    _fields_ = [('PinState', c_uint, 32)]

class ALARMS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x220
    _fields_ = [('Psdb0XcvrRxFifoFull', c_uint, 1),  # 0
                ('Psdb1XcvrRxFifoFull', c_uint, 1),  # 1
                ('PatgenXcvrRxFifoFull', c_uint, 1),  # 2
                ('Psdb0_ltc_2975_vadj_alert', c_uint, 1),  # 3
                ('Psdb0_ltm_4678_vccio_alert_3', c_uint, 1),  # 4
                ('Psdb0_ltm_4678_vccio_alert_2', c_uint, 1),  # 5
                ('Psdb0_ltm_4678_vccio_alert_1', c_uint, 1),  # 6
                ('Psdb0_ltm_4678_vccio_alert_0', c_uint, 1),  # 7
                ('Psdb1_ltc_2975_vadj_alert', c_uint, 1),  # 8
                ('Psdb1_ltm_4678_vccio_alert_3', c_uint, 1),  # 9
                ('Psdb1_ltm_4678_vccio_alert_2', c_uint, 1),  # 10
                ('Psdb1_ltm_4678_vccio_alert_1', c_uint, 1),  # 11
                ('Psdb1_ltm_4678_vccio_alert_0', c_uint, 1),  # 12
                ('Psdb0XcvrCrcError', c_uint, 1),  # 13
                ('Psdb1XcvrCrcError', c_uint, 1),  # 14
                ('PatgenXcvrCrcError', c_uint, 1),  # 15
                ('Pcie_write_while_full_alarm', c_uint, 1),  # 16
                ('Dps_trigger_dropped_alarm', c_uint, 1),  # 17
                ('Trigger_dropped_alarm', c_uint, 1),  # 18
                ('I2c_error_alarm', c_uint, 1),  # 19
                ('Reserved', c_uint, 12)]  # 20-31


class PROCESSED_TRIGGER_QUEUES_DUT(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x640
    REGCOUNT = 17
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_0_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6A0
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_1_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6A4
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_2_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6A8
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_3_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6AC
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_4_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6B0
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_5_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6B4
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_6_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6B8
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_7_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6BC
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_8_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6C0
    _fields_ = [('Count', c_uint, 32)]

class I2C_DEVICE_9_STATUS(RingMultiplierRegister):
    BAR = 1
    ADDR = 0x6C4
    _fields_ = [('Count', c_uint, 32)]