# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from Common.instruments.hbi_instrument import HbiInstrument
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo


class MainBoard(HbiInstrument):
    def __init__(self):
        self.blt = CachingBltInfo(callback_function=hil.hbiMbBltBoardRead,
                                  name='HBI MB')

    def name(self):
        return f'HBIMAINBOARD'

    def log_board_blt(self):
        self.blt.write_to_log()

    def read_blt(self):
        '''Does not cache, used for tests'''
        return hil.hbiMbBltBoardRead()
