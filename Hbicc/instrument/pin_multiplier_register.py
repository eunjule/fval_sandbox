################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
from ctypes import c_uint

import Common.register as register


def get_register_types_sorted_by_name():
    return sorted(get_register_types(), key=lambda x: x.__name__)


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, PinMultiplierRegister):
                if attribute is not PinMultiplierRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, PinMultiplierRegister):
                if attribute is not PinMultiplierRegister:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


class PinMultiplierRegister(register.Register):
    BASEMUL = 4
    BAR = 1

    def name(self):
        return type(self).__name__.upper()


class SCRATCH_REG(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x0
    REGCOUNT = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_VERSION(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_LOWER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x24
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_UPPER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x28
    _fields_ = [('Data', ctypes.c_uint, 16),  # 0
                ('reserved', ctypes.c_uint, 15),
                ('hg_clean', ctypes.c_uint, 1)]


class TARGET_HARDWARE(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x2c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x30
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_L(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x34
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_U(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x38
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DUT_PATTERN_ENABLE(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x3c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_RING_BUS_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('EnablePrbs', c_uint, 1),
                ('PRBSFilter', c_uint, 1),
                ('ForceRealign', c_uint, 1),
                ('ResetErrorCount', c_uint, 1),
                ('PrbsInsertError', c_uint, 1),
                ('Reset', c_uint, 1),
                ('SerialLoopbackLane0', c_uint, 1),
                ('SerialLoopbackLane1', c_uint, 1),
                ('SerialLoopbackLane2', c_uint, 1),
                ('SerialLoopbackLane4', c_uint, 1),
                ('Reset_CRC_Error', c_uint, 1),
                ('Reserved', c_uint, 21)]


class RM_RING_BUS_STATUS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('WordAligned', c_uint, 1),
                ('DecoderError', c_uint, 1),
                ('ErrorDeskew', c_uint, 1),
                ('PrbsSuccess', c_uint, 1),
                ('XoffReceived', c_uint, 1),
                ('LinkUp', c_uint, 1),
                ('RxFreqLocked', c_uint, 1),
                ('LaneAligned', c_uint, 1),
                ('PllLocked', c_uint, 1),
                ('PrbsLocked', c_uint, 1),
                ('RXFIFOAllMostFull', c_uint, 1),
                ('Reserved', c_uint, 21)]


class RM_RING_BUS_ERROR_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PM_RING_BUS_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x4c
    _fields_ = [('EnablePrbs', c_uint, 1),
                ('PRBSFilter', c_uint, 1),
                ('ForceRealign', c_uint, 1),
                ('ResetErrorCount', c_uint, 1),
                ('PrbsInsertError', c_uint, 1),
                ('Reset', c_uint, 1),
                ('SerialLoopbackLane0', c_uint, 1),
                ('SerialLoopbackLane1', c_uint, 1),
                ('SerialLoopbackLane2', c_uint, 1),
                ('SerialLoopbackLane4', c_uint, 1),
                ('Reset_CRC_Error', c_uint, 1),
                ('Reserved', c_uint, 21)]


class PM_RING_BUS_STATUS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('WordAligned', c_uint, 1),
                ('DecoderError', c_uint, 1),
                ('ErrorDeskew', c_uint, 1),
                ('PrbsSuccess', c_uint, 1),
                ('XoffReceived', c_uint, 1),
                ('LinkUp', c_uint, 1),
                ('RxFreqLocked', c_uint, 1),
                ('LaneAligned', c_uint, 1),
                ('PllLocked', c_uint, 1),
                ('PrbsLocked', c_uint, 1),
                ('Reserved', c_uint, 22)]


class PM_RING_BUS_ERROR_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x54
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VOLTAGE_SENSOR_CHANNEL_0(PinMultiplierRegister):
    ADDR = 0xC0
    _fields_ = [('VSig_0', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_1(PinMultiplierRegister):
    ADDR = 0xC4
    _fields_ = [('VSig_1', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_2(PinMultiplierRegister):
    ADDR = 0xC8
    _fields_ = [('VCC', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_3(PinMultiplierRegister):
    ADDR = 0xCC
    _fields_ = [('VCCP', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_4(PinMultiplierRegister):
    ADDR = 0xD0
    _fields_ = [('VCCPT', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_5(PinMultiplierRegister):
    ADDR = 0xD4
    _fields_ = [('VCCERAM', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_6(PinMultiplierRegister):
    ADDR = 0xD8
    _fields_ = [('VCCL_HPS', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class VOLTAGE_SENSOR_CHANNEL_7(PinMultiplierRegister):
    ADDR = 0xDC
    _fields_ = [('ADC_GND', ctypes.c_uint, 6),
                ('Data', ctypes.c_uint, 26)]


class PM_RING_BUS_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xE0
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_RING_BUS_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x100
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_PM_PLL_RECONFIG_ADDRESS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x60
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_PM_PLL_RECONFIG_WRITE_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x64
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_PM_PLL_RECONFIG_READ_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x68
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_PM_PLL_RECONFIG_READ_DATA_TAG(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x6c
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class XCVR_PM_RECONFIG_ADDRESS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x70
    _fields_ = [('read_write_address', c_uint, 16),
                ('Reserved', c_uint, 16)]


class XCVR_PM_RECONFIG_WRITE_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x74
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_PM_RECONFIG_READ_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x78
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_PM_RECONFIG_READ_DATA_TAG(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x7c
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PM_RING_BUS_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xe0
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PM_XCVR_ERROR_COUNT_LANE(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xe8
    REGCOUNT = 4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_RING_BUS_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x100
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_XCVR_ERROR_COUNT_LANE(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x108
    REGCOUNT = 4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RM_PLL_RECONFIG_ADDRESS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x120
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class XCVR_RM_PLL_RECONFIG_WRITE_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x124
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RM_PLL_RECONFIG_READ_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x128
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RM_PLL_RECONFIG_READ_DATA_TAG(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x12c
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class XCVR_RM_RECONFIG_ADDRESS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x130
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class XCVR_RM_RECONFIG_WRITE_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x134
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RM_RECONFIG_READ_DATA(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x138
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RM_RECONFIG_READ_DATA_TAG(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x13c
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PIN_CLOCK_FREQUENCY(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x140
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class CLOCK_FREQUENCY_30MHZ(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x144
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class PCIE_FABRIC_CLOCK_FREQUENCY(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class TRANSCEIVER_FABRIC_CLOCK_FREQUENCY(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x14C
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class PHYLITE_CLOCK(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x150
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class PATTERN_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x80
    _fields_ = [('EnableOutputs', c_uint, 1),  # 0
                ('PreStagePattern', c_uint, 1),
                ('SubcyclesPerCycleExp', c_uint, 3),
                ('ChannelSetBaseID', c_uint, 3),
                ('AbortPattern', c_uint, 1),
                ('BurstStart', c_uint, 1),
                ('NewBurst', c_uint, 1),  # 10
                ('SyncStartSliceMask', c_uint, 5),  # 15
                ('StandAloneFixedDriveMode', c_uint, 1),
                ('BurstIsDone', c_uint, 1),  # 17
                ('PhyliteReset', c_uint, 1),
                ('PhyliteAVMMReset', c_uint, 1),
                ('Reserved', c_uint, 12)]


class CAPTURE_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x84
    CHECK = True
    _fields_ = [('CaptureFails', c_uint, 1),  # 0
                ('CaptureCtv', c_uint, 1), #1
                ('CaptureAll', c_uint, 1), #2
                ('IndexMode', c_uint, 1),  #3
                ('StopOnMaxFail', c_uint, 1), #4
                ('CaptureCtvAfterMaxFail', c_uint, 1), #5
                ('PmChipId', c_uint, 2), #6:7
                ('ctp_channel_sets_active', c_uint, 2), #8,9
                ('ctp_override_chip_wide', c_uint, 1),  #10
                ('ctp_override_active', c_uint, 1),    #11
                ('Reserved', c_uint, 19)]  #12


class MAX_GLOBAL_ERROR_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x8c
    _fields_ = [('Count', c_uint, 32)]


class MAX_PATTERN_ERROR_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x90
    _fields_ = [('Count', c_uint, 32)]


class CHANNEL_SET_0(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x900
    REGCOUNT = 18
    _fields_ = [('lower_fail_count', c_uint, 16),
                ('upper_fail_count', c_uint, 16)]


class CHANNEL_SET_1(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x960
    REGCOUNT = 18
    _fields_ = [('lower_fail_count', c_uint, 16),
                ('upper_fail_count', c_uint, 16)]


class CHANNEL_SET_2(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x9C0
    REGCOUNT = 18
    _fields_ = [('lower_fail_count', c_uint, 16),
                ('upper_fail_count', c_uint, 16)]


class CHANNEL_SET_3(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xA20
    REGCOUNT = 18
    _fields_ = [('lower_fail_count', c_uint, 16),
                ('upper_fail_count', c_uint, 16)]


class RAW_CHANNEL_STATE(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xA0
    REGCOUNT = 8
    _fields_ = [('Rawinputchannelstate', c_uint, 32)]


class RM_RING_BUS_RX_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF28
    REGCOUNT = 2
    _fields_ = [('PatternCount', c_uint, 32)]


class RM_RING_BUS_TX_PATTERN_COUNT_LOWER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF20
    REGCOUNT = 2
    _fields_ = [('PatternCount', c_uint, 32)]


class PM_RING_BUS_TX_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF00
    REGCOUNT = 2
    _fields_ = [('PatternCount', c_uint, 32)]


class PM_RING_BUS_RX_PATTERN_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF08
    REGCOUNT = 2
    _fields_ = [('PatternCount', c_uint, 32)]


class LAST_SEEN(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF40
    REGCOUNT = 8
    _fields_ = [('LastSeen', c_uint, 32)]


class AURORA_PATTERN_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF60
    _fields_ = [('reserved', c_uint, 1),
                ('sync_start_sliceid', c_uint, 3),
                ('reserved', c_uint, 28)]


class PM_AURORA_PATTERN_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF64
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class PM_AURORA_PATTERN_STATUS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF68
    _fields_ = [('pll_locked', c_uint, 1),
                ('lane_up', c_uint, 1),
                ('channel_up', c_uint, 1),
                ('soft_error', c_uint, 1),
                ('hard_error', c_uint, 1),
                ('tx_pll_locked', c_uint, 1),
                ('reserved', c_uint, 27)]


class PM_AURORA_PATTERN_ERROR_COUNTS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF6c
    _fields_ = [('soft_error_count', c_uint, 16),
                ('hard_error_count', c_uint, 16)]


class RESEND_TO_PM1_AURORA_PATTERN_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF70
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class RESEND_TO_PM1_AURORA_PATTERN_STATUS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF74
    _fields_ = [('Data', c_uint, 32)]


class RESEND_TO_PM1_AURORA_PATTERN_ERROR_COUNTS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xF78
    _fields_ = [('Data', c_uint, 32)]


class ALARMS(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x88
    _fields_ = [('PMXcvrRxFIFOFull', c_uint, 1),  # 0
                ('RMXcvrRxFIFOFull', c_uint, 1),  # 1
                ('UnderRun', c_uint, 1),  # 2
                ('OverFull', c_uint, 1),  # 3
                ('Phylite0locked', c_uint, 1),  # 4
                ('Phylite1locked', c_uint, 1),  # 5
                ('Phylite2locked', c_uint, 1),  # 6
                ('Phylite3locked', c_uint, 1),  # 7
                ('PmXcvrCrcError', c_uint, 1),  # 8
                ('RmXcvrCrcError', c_uint, 1),  # 9
                ('Reserved', c_uint, 22)]  # 10-31


class PER_CHANNEL_CONFIG1(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x200
    REGCOUNT = 140
    BASEMUL = 12
    _fields_ = [('TotalCycleDelayTxToDut', c_uint, 15),
                ('SubCycleDelayRxFromDut', c_uint, 10),
                ('TesterCycleDelayRxFromDut', c_uint, 5),
                ('Reserved', c_uint, 2)]


class PER_CHANNEL_CONFIG2(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x204
    REGCOUNT = 140
    BASEMUL = 12
    _fields_ = [('EnableClkTesterCycleHigh', c_uint, 8),
                ('EnableClkTesterCycleLow', c_uint, 8),
                ('EcFirstEdge', c_uint, 1),
                ('EcAddHalfCycle', c_uint, 1),
                ('ECIsRatio1', c_uint, 1),
                ('Reserved', c_uint, 13)]


class PER_CHANNEL_CONFIG3(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x208
    REGCOUNT = 140
    BASEMUL = 12
    _fields_ = [('FRCMode', c_uint, 1),
                ('FRCDivider', c_uint, 18),
                ('FRCStartState', c_uint, 1),
                ('FRCIsRatio1', c_uint, 1),
                ('Reserved', c_uint, 11)]


class TX_FIFO_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x3c
    _fields_ = [('Count', c_uint, 32)]


class L_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFE0
    _fields_ = [('Count', c_uint, 32)]


class H_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFE4
    _fields_ = [('Count', c_uint, 32)]


class X_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFE8
    _fields_ = [('Count', c_uint, 32)]


class ONE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFEC
    _fields_ = [('Count', c_uint, 32)]


class ZERO_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFF0
    _fields_ = [('Count', c_uint, 32)]


class E_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFF4
    _fields_ = [('Count', c_uint, 32)]


class K_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFF8
    _fields_ = [('Count', c_uint, 32)]


class DEFAULT_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xFFC
    _fields_ = [('Count', c_uint, 32)]


class ERROR_UNMASKED_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1000
    _fields_ = [('Count', c_uint, 32)]


class COMPARE_EVENT_HIGH_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1004
    _fields_ = [('Count', c_uint, 32)]


class COMPARE_EVENT_LOW_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1008
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_DEFAULT_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x100c
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_STATIC_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1020
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH0_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1024
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH1_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1028
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH2_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x102c
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH3_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1030
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH0_3_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1034
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH1_2_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1038
    _fields_ = [('Count', c_uint, 32)]


class PIN_DATA_REPLICATOR_ACTIVE_CH_ALL_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x103c
    _fields_ = [('Count', c_uint, 32)]


class UNDERRUN_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1040
    _fields_ = [('Count', c_uint, 32)]


class VECTOR_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1010
    _fields_ = [('Count', c_uint, 32)]


class LAST_CYCLE_SEEN(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1040
    _fields_ = [('trigger_word_filter', c_uint, 1),
                ('vector_assembler', c_uint, 1),
                ('symbol_decoder', c_uint, 1),
                ('subcycle_in', c_uint, 1),
                ('subcycle_out', c_uint, 1),
                ('reserved', c_uint, 27)]


class TRIGGER_WORD_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1044
    _fields_ = [('Count', c_uint, 32)]


class VECTOR_ASSEMBLER_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1048
    _fields_ = [('Count', c_uint, 32)]


class SYMBOL_DECODER_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x104C
    _fields_ = [('Count', c_uint, 32)]


class CHANNEL_SET_0_FAILMASK(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xb00
    REGCOUNT = 64
    _fields_ = [('FailMask', c_uint, 32)]


class CHANNEL_SET_1_FAILMASK(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xc00
    REGCOUNT = 64
    _fields_ = [('FailMask', c_uint, 32)]


class CHANNEL_SET_2_FAILMASK(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xd00
    REGCOUNT = 64
    _fields_ = [('FailMask', c_uint, 32)]


class CHANNEL_SET_3_FAILMASK(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xe00
    REGCOUNT = 64
    _fields_ = [('FailMask', c_uint, 32)]


class ILA_CONTROL(PinMultiplierRegister):
    ADDR = 0x1080
    _fields_ = [('word_in_sample', ctypes.c_uint, 4),
                ('sample', ctypes.c_uint, 15),
                ('bypass_write_enable', ctypes.c_uint, 1),
                ('bypass_trigger', ctypes.c_uint, 1),
                ('select', ctypes.c_uint, 3),
                ('reset', ctypes.c_uint, 8)]


class ILA_STATUS(PinMultiplierRegister):
    ADDR = 0x1084
    _fields_ = [('capture_count', ctypes.c_uint, 15),
                ('reserved', ctypes.c_uint, 17)]


class ILA_DATA(PinMultiplierRegister):
    ADDR = 0x1088
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MAX_GLOBAL_ERROR_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x8c
    _fields_ = [('Count', c_uint, 32)]


class MAX_PER_PATTERN_ERROR_COUNT(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x90
    _fields_ = [('Count', c_uint, 32)]


ILA_PAGE = 0x1080
PACKET_CONTROL_PAGE = 0x10A0
PACKET_SNAPSHOT_PAGE = 0x10C0
PACKET_TOTAL_PAGE = 0x10E0
PACKET_EOB_TOTAL_PAGE = 0x1100
PACKET_INVALID_TOTAL_PAGE = 0x1120


class RESET_CONTROL(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x00
    _fields_ = [('Data', c_uint, 32)]


class SNAPSHOT_CYCLE_NUMBER(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x04
    _fields_ = [('Data', c_uint, 32)]


class FEATURE_SIGNATURE(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x08
    _fields_ = [('Data', c_uint, 32)]


class PM_E_PACKETS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_PACKETS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_E_EOB_PACKETS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_EOB_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_EOB_PACKETS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_EOB_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_E_INVALID_PACKETS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_INVALID_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_INVALID_PACKETS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_INVALID_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_SNAPSHOT(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_XOR(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


PACKET_CHECK_TOP_PAGE = 0x10A0
PACKET_CHECK_TOP_PAGE = 0x10A0
PACKET_CHECK_COMPRESSOR_PAGE = 0x10C0


class PACKET_CHECK_TOP_STATUS(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CHECK_TOP_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_CHECK_TOP_XOR(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CHECK_TOP_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_CHECK_COMPRESSOR_STATUS_0(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CHECK_COMPRESSOR_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_CHECK_COMPRESSOR_XOR_0(PinMultiplierRegister):
    BAR = 1
    ADDR = PACKET_CHECK_COMPRESSOR_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class RAW_STATE_CHANNEL_SET_0(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xA0
    REGCOUNT = 2
    _fields_ = [('Rawinputchannelstate', c_uint, 32)]


class RAW_STATE_CHANNEL_SET_1(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xA8
    REGCOUNT = 2
    _fields_ = [('Rawinputchannelstate', c_uint, 32)]


class RAW_STATE_CHANNEL_SET_2(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xB0
    REGCOUNT = 2
    _fields_ = [('Rawinputchannelstate', c_uint, 32)]


class RAW_STATE_CHANNEL_SET_3(PinMultiplierRegister):
    BAR = 1
    ADDR = 0xB8
    REGCOUNT = 2
    _fields_ = [('Rawinputchannelstate', c_uint, 32)]


class UNDER_RUN_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1060
    _fields_ = [('Data', c_uint, 32)]


class TRIGGER_WORD_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1044
    _fields_ = [('Data', c_uint, 32)]


class VECTOR_ASSEMBLER_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x1048
    _fields_ = [('Data', c_uint, 32)]


class SYMBOL_DECODER_LAST_CYCLE_COUNTER(PinMultiplierRegister):
    BAR = 1
    ADDR = 0x104c
    _fields_ = [('Data', c_uint, 32)]


K_DEBUG_VA_PAGE = 0x1140
K_DEBUG_DUT_PAGE = 0x1160


class K_DEBUG_VA_PAGE_7To3(PinMultiplierRegister):
    ADDR = K_DEBUG_VA_PAGE + 0x00
    _fields_ = [('pindata', ctypes.c_uint, 3),
                ('dutid', ctypes.c_uint, 6),
                ('dutserconfig', ctypes.c_uint, 3),
                ('acconfig', ctypes.c_uint, 3),
                ('seen7to3', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 16)]


class K_DEBUG_VA_PAGE_3To7(PinMultiplierRegister):
    ADDR = K_DEBUG_VA_PAGE + 0x04
    _fields_ = [('pindata', ctypes.c_uint, 3),
                ('dutid', ctypes.c_uint, 6),
                ('dutserconfig', ctypes.c_uint, 3),
                ('acconfig', ctypes.c_uint, 3),
                ('seen3to7', ctypes.c_uint, 1),
                ('ac_count', ctypes.c_uint, 16)]


class K_DEBUG_VA_PAGE_KSEEN(PinMultiplierRegister):
    ADDR = K_DEBUG_VA_PAGE + 0x08
    _fields_ = [('pindata', ctypes.c_uint, 3),
                ('dutid', ctypes.c_uint, 6),
                ('dutserconfig', ctypes.c_uint, 3),
                ('acconfig', ctypes.c_uint, 3),
                ('kseen', ctypes.c_uint, 1),
                ('txvector', ctypes.c_uint, 4),
                ('reserved', ctypes.c_uint, 12)]


class VA_WR_IN(PinMultiplierRegister):
    ADDR = K_DEBUG_VA_PAGE + 0x0C
    _fields_ = [('Count', ctypes.c_uint, 32)]


class VA_WR_OUT(PinMultiplierRegister):
    ADDR = K_DEBUG_VA_PAGE + 0x10
    _fields_ = [('Count', ctypes.c_uint, 32)]


class K_DEBUG_VA_PAGE_DUTSERIAL_CONFIG(PinMultiplierRegister):
    ADDR = K_DEBUG_VA_PAGE + 0x14
    _fields_ = [('pindata', ctypes.c_uint, 3),
                ('dutid', ctypes.c_uint, 6),
                ('dutserconfig', ctypes.c_uint, 3),
                ('dutstartid', ctypes.c_uint, 5),
                ('pindataoverride', ctypes.c_uint, 3),
                ('dutidmatch', ctypes.c_uint, 1),
                ('overflow_flag', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 10)]


class SYMB_DEC_WR_EN_SLICE(PinMultiplierRegister):
    ADDR = K_DEBUG_DUT_PAGE + 0x00
    _fields_ = [('Count', ctypes.c_uint, 32)]


class SYMB_DEC_ALL_WR_EN_SLICE(PinMultiplierRegister):
    ADDR = K_DEBUG_DUT_PAGE + 0x04
    _fields_ = [('Count', ctypes.c_uint, 32)]


class CAPABILITIES(PinMultiplierRegister):
    ADDR = 0x1200
    _fields_ = [('set_e32_interval', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]


class E32_INTERVAL(PinMultiplierRegister):
    ADDR = 0x94
    _fields_ = [('e32_interval', ctypes.c_uint, 12),
                ('Reserved', ctypes.c_uint, 20)]

OCT_CALIBRATED_VALUE_PAGE0 = 0x10E0
OCT_CALIBRATED_VALUE_PAGE1 = 0x1100

class OCT_CALIBRATED_VALUE_BANK_2L(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x00
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2K(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x04
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2J(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x08
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2I(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x0C
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2H(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x10
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2G(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x14
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2F(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x18
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_2A(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE0 + 0x1C
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3H(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x00
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3G(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x04
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3F(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x08
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3E(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x0C
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3D(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x10
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3C(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x14
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3B(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x18
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]

class OCT_CALIBRATED_VALUE_BANK_3A(PinMultiplierRegister):
    ADDR= OCT_CALIBRATED_VALUE_PAGE1 + 0x1C
    _fields_ = [('n_leg_value', ctypes.c_uint, 8),
                ('p_leg_value', ctypes.c_uint, 8),
                ('reserved', ctypes.c_uint, 16)]


