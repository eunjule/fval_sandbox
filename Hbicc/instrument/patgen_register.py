################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
from ctypes import c_uint

import Common.register as register


def get_register_types_sorted_by_name():
    return sorted(get_register_types(), key=lambda x: x.__name__)


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, PatgenRegister):
                if attribute is not PatgenRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, PatgenRegister):
                if attribute is not PatgenRegister:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


class PatgenRegister(register.Register):
    BASEMUL = 4
    BAR = 1

    def name(self):
        return type(self).__name__.upper()


class SCRATCH_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x0
    REGCOUNT = 8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FPGA_VERSION(PatgenRegister):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_LOWER(PatgenRegister):
    BAR = 1
    ADDR = 0x24
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HG_ID_UPPER(PatgenRegister):
    BAR = 1
    ADDR = 0x28
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TARGET_HARDWARE(PatgenRegister):
    BAR = 1
    ADDR = 0x2c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE0_V(PatgenRegister):
    BAR = 1
    ADDR = 0x30
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE_STATUS(PatgenRegister):
    BAR = 1
    ADDR = 0x34
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_L(PatgenRegister):
    BAR = 1
    ADDR = 0x38
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CHIP_ID_U(PatgenRegister):
    BAR = 1
    ADDR = 0x3c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AURORA_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('reset', ctypes.c_uint, 1),
                ('clear_error_count', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class AURORA_STATUS(PatgenRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('pll_locked', ctypes.c_uint, 1),
                ('lane_up', ctypes.c_uint, 1),
                ('channel_up', ctypes.c_uint, 1),
                ('soft_error', ctypes.c_uint, 1),
                ('hard_error', ctypes.c_uint, 1),
                ('tx_pll_lock', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 26)]


class AURORA_ERROR_COUNTS(PatgenRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('soft_error_count', ctypes.c_uint, 16),
                ('hard_error_count', ctypes.c_uint, 16)]


class LAST_TRIGGER_SEEN(PatgenRegister):
    BAR = 1
    ADDR = 0x4c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMPERATURE(PatgenRegister):
    BAR = 1
    ADDR = 0x120
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SEND_TRIGGER(PatgenRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('trigger_type', c_uint, 4),
                ('count_value', c_uint, 4),
                ('target_domain', c_uint, 4),
                ('reserved', c_uint, 7),
                ('sw_type_trigger', c_uint, 1),
                ('dut_domain_id', c_uint, 6),
                ('card_type', c_uint, 6)]


class LAST_TRIGGER_IN_HBI(PatgenRegister):
    BAR = 1
    ADDR = 0x54
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRIG_FANOUT_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x5C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CAPTURE_TRIG_SENT_COUNT_OUT(PatgenRegister):
    BAR = 1
    ADDR = 0x240
    _fields_ = [('Data', ctypes.c_uint, 32)]


class FIRST_TRIG_SENT_AFTER_RESET_OUT(PatgenRegister):
    BAR = 1
    ADDR = 0x244
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRIGGER_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = 0x58
    _fields_ = [('card_type', ctypes.c_uint, 4),
                ('domain_id', ctypes.c_uint, 4),
                ('domain_master', ctypes.c_uint, 1),  # 8
                ('sync_modifier', ctypes.c_uint, 4),  # 9-12
                ('card_type_extended', ctypes.c_uint, 2),  # 13-14
                ('dut_id', ctypes.c_uint, 6),  # 15-20
                ('clear_all_on_gse', ctypes.c_uint, 1),  # 21
                ('reserved', ctypes.c_uint, 11), ]


class SEND_SW_TRIGGER_UP(PatgenRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('index_of_trigger_queue', ctypes.c_uint, 10),
                ('reserved', ctypes.c_uint, 9),
                ('software_trigger_up', ctypes.c_uint, 1),
                ('dut_id', ctypes.c_uint, 6),
                ('target_resource', ctypes.c_uint, 6)]


class DDR_STATUS(PatgenRegister):
    BAR = 1
    ADDR = 0x140
    _fields_ = [('reset_done', ctypes.c_uint, 1),
                ('calibration_failed', ctypes.c_uint, 1),
                ('calibration_passed', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 29)]


class DDR_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = 0x144
    _fields_ = [('reset_req', ctypes.c_uint, 1),
                ('reset_ecc_count', ctypes.c_uint, 1),
                ('force_extra_refreshes', ctypes.c_uint, 1),
                ('spd_i2c_reset', ctypes.c_uint, 1),
                ('en_single_bit_error', ctypes.c_uint, 1),
                ('en_double_bit_error', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 26)]


class ECC_CORRECTABLE(PatgenRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ECC_UNCORRECTABLE(PatgenRegister):
    BAR = 1
    ADDR = 0x14C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_TOP_PLL_RECONFIG_ADDRESS(PatgenRegister):
    BAR = 1
    ADDR = 0x60
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class XCVR_TOP_PLL_RECONFIG_WRITE_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x64
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_TOP_PLL_RECONFIG_READ_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x68
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_TOP_PLL_RECONFIG_READ_DATA_TAG(PatgenRegister):
    BAR = 1
    ADDR = 0x6c
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class XCVR_RECONFIG_ADDRESS(PatgenRegister):
    BAR = 1
    ADDR = 0x70
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class XCVR_RECONFIG_WRITE_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x74
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RECONFIG_READ_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x78
    _fields_ = [('Data', ctypes.c_uint, 32)]


class XCVR_RECONFIG_READ_DATA_TAG(PatgenRegister):
    BAR = 1
    ADDR = 0x7c
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class RM_RING_BUS_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = 0x80
    _fields_ = [('EnablePrbs', c_uint, 1),
                ('PRBSFilter', c_uint, 1),
                ('ForceRealign', c_uint, 1),
                ('ResetErrorCount', c_uint, 1),
                ('PrbsInsertError', c_uint, 1),
                ('Reset', c_uint, 1),
                ('SerialLoopbackLane0', c_uint, 1),
                ('SerialLoopbackLane1', c_uint, 1),
                ('SerialLoopbackLane2', c_uint, 1),
                ('SerialLoopbackLane4', c_uint, 1),
                ('Reset_CRC_Error', c_uint, 1),
                ('Reserved', c_uint, 21)]


class RM_RING_BUS_STATUS(PatgenRegister):
    BAR = 1
    ADDR = 0x84
    _fields_ = [('WordAligned', c_uint, 1),
                ('DecoderError', c_uint, 1),
                ('ErrorDeskew', c_uint, 1),
                ('PrbsSuccess', c_uint, 1),
                ('XoffReceived', c_uint, 1),
                ('LinkUp', c_uint, 1),
                ('RxFreqLocked', c_uint, 1),
                ('LaneAligned', c_uint, 1),
                ('PllLocked', c_uint, 1),
                ('PrbsLocked', c_uint, 1),
                ('Reserved', c_uint, 22)]


class RM_RING_BUS_ERROR_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x88
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_RING_BUS_PATTERN_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x8C
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_XCVR_ERROR_COUNT_LANE(PatgenRegister):
    BAR = 1
    ADDR = 0x94
    REGCOUNT = 2
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PCIE_FABRIC_CLOCK(PatgenRegister):
    BAR = 1
    ADDR = 0xC0
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class DDR4_CH_FABRIC_CLOCK(PatgenRegister):
    BAR = 1
    ADDR = 0xC4
    REGCOUNT = 5
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class PIN_CLOCK_PSDB(PatgenRegister):
    BAR = 1
    ADDR = 0xDC
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class AURORA_TRIGGER_FABRIC_CLOCK(PatgenRegister):
    BAR = 1
    ADDR = 0xF0
    _fields_ = [('measure_counter', c_uint, 12),
                ('reference_window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]


class RING_BUS_TRANSCEIVER_BOTTOM_PLL_RECONFIG_ADDRESS(PatgenRegister):
    BAR = 1
    ADDR = 0xA0
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class RING_BUS_TRANSCEIVER_BOTTOM_PLL_RECONFIG_WRITE_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0xA4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RING_BUS_TRANSCEIVER_BOTTOM_PLL_RECONFIG_READ_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0xA8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RING_BUS_TRANSCEIVER_BOTTOM_PLL_RECONFIG_READ_DATA_TAG(PatgenRegister):
    BAR = 1
    ADDR = 0xAC
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PSDB0_CLOCK_PLL_RECONFIG_ADDRESS(PatgenRegister):
    BAR = 1
    ADDR = 0x100
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class PSDB0_CLOCK_PLL_RECONFIG_WRITE_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x104
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_CLOCK_PLL_RECONFIG_READ_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x108
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB0_CLOCK_PLL_RECONFIG_READ_DATA_TAG(PatgenRegister):
    BAR = 1
    ADDR = 0x10C
    _fields_ = [('Tag', c_uint, 2),
                ('Reserved', c_uint, 30)]


class PSDB1_CLOCK_RECONFIG_ADDRESS(PatgenRegister):
    BAR = 1
    ADDR = 0x110
    _fields_ = [('Address', ctypes.c_uint, 11),
                ('Reserved', c_uint, 21)]


class CLOCK_PLL_SI5344(PatgenRegister):
    BAR = 1
    ADDR = 0x110
    _fields_ = [('data', ctypes.c_uint, 1),
                ('Reserved', c_uint, 31)]


class PSDB1_CLOCK_RECONFIG_WRITE_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x114
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PSDB1_CLOCK_RECONFIG_READ_DATA(PatgenRegister):
    BAR = 1
    ADDR = 0x118
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATTERN_END_STATUS(PatgenRegister):
    BAR = 1
    ADDR = 0x160
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATTERN_START(PatgenRegister):
    BAR = 1
    ADDR = 0x168
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PATTERN_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = 0x164
    _fields_ = [('EnableOutputs', c_uint, 1),  # 0
                ('PrestagePattern', c_uint, 1),  # 1
                ('DisableDynamicPinCacheDepth', c_uint, 1),  # 2
                ('SubCycleCount', c_uint, 4),  # 3-6
                ('start_on_next_sync', c_uint, 1),  # 7
                ('AbortPattern', c_uint, 1),  # 8
                ('SliceGroupMask', c_uint, 5),  # bt 9-13
                ('PatternRunning', c_uint, 1),  # 14
                ('patgen_to_a10_started', c_uint, 1),  # 15
                ('active_pm_indicator', c_uint, 4),  # 16-19
                ('include_big_header_full', c_uint, 1),  # 20
                ('reset_alarm', c_uint, 1),  # 21
                ('Reserved', c_uint, 2),  # 22-23
                ('prbs_enable', c_uint, 1),  # 24
                ('toggle_enable', c_uint, 1),  # 25
                ('sync_start_bypass', c_uint, 1),  # 26
                ('PinRingAcknowledge', c_uint, 1),  # 27
                ('PVCStarted', c_uint, 1),  # 28
                ('VPStarted', c_uint, 1),  # 29
                ('PrestageDone', c_uint, 1),  # 30
                ('SliceActiveInBurst', c_uint, 1)]  # 31


class ALARMS1(PatgenRegister):
    BAR = 1
    ADDR = 0x16c
    _fields_ = [('trace_capture_overflow', ctypes.c_uint, 1),  # 0
                ('ctv_row_capture_overflow', ctypes.c_uint, 1),  # 1
                ('ctv_header_capture_overflow', ctypes.c_uint, 1),  # 2
                ('incorrect_first_fetched_address', ctypes.c_uint, 1),  # 3
                ('capture_buffer_overflow', ctypes.c_uint, 1),  # 4
                ('capture_dram_overflow', ctypes.c_uint, 1),  # 5
                ('ecc_error', ctypes.c_uint, 1),  # 6
                ('illegal_instruction', ctypes.c_uint, 1),  # 7
                ('user_log1_overflow', ctypes.c_uint, 1),  # 8
                ('mask_update_high_with_repeat_indicator', ctypes.c_uint, 1),  # 9
                ('pattern_instance_overflow', ctypes.c_uint, 1),  # 10
                ('reset_user_cycle_counter_overflow', ctypes.c_uint, 1),  # 11
                ('enable_clock_full', ctypes.c_uint, 1),  # 12
                ('error_pm_0_capture_overflow', ctypes.c_uint, 1),  # 13
                ('error_pm_1_capture_overflow', ctypes.c_uint, 1),  # 14
                ('error_pm_2_capture_overflow', ctypes.c_uint, 1),  # 15
                ('error_pm_3_capture_overflow', ctypes.c_uint, 1),  # 16
                ('capture_on_off_overflow', ctypes.c_uint, 1),  # 17
                ('active_channel_full', ctypes.c_uint, 1),  # 18
                ('dut_serial_control_full', ctypes.c_uint, 1),  # 19
                ('stack_error', ctypes.c_uint, 1),  # 20
                ('mdf_missed_end', ctypes.c_uint, 1),  # 21
                ('patgen_to_a10_missed_end', ctypes.c_uint, 1),  # 22
                ('lrpt_missed_end', ctypes.c_uint, 1),  # 23
                ('pin_lane_missed_end', ctypes.c_uint, 1),  # 24
                ('multiple_addr_from_controller', ctypes.c_uint, 1),  # 25
                ('multiple_wr_en_from_controller', ctypes.c_uint, 1),  # 26
                ('incomplete_burst_alarm', ctypes.c_uint, 1),  # 27
                ('metadata_timeout', ctypes.c_uint, 1),  # 28
                ('pvc_proc_timeout', ctypes.c_uint, 1),  # 29
                ('capture_this_pin_full', ctypes.c_uint, 1),  # 30
                ('extended_alarms_present', ctypes.c_uint, 1)]  # 31


class ALARMS2(PatgenRegister):
    BAR = 1
    ADDR = 0x170
    _fields_ = [('pin_cache_overflow', ctypes.c_uint, 1),  # 0
                ('pin_cache_underrun', ctypes.c_uint, 1),  # 1
                ('illegal_vector_format', ctypes.c_uint, 1),  # 2
                ('compare_engine_expect_underrun', ctypes.c_uint, 1),  # 3
                ('compare_engine_capture_overflow', ctypes.c_uint, 1),  # 4
                ('last_pcall_addr_is_zero', ctypes.c_uint, 1),  # 5
                ('apply_mask_full', ctypes.c_uint, 1),  # 6
                ('vp_never_started', ctypes.c_uint, 1),  # 7
                ('mdf_never_started', ctypes.c_uint, 1),  # 8
                ('lrpt_never_started', ctypes.c_uint, 1),  # 9
                ('read_on_empty_trace_index', ctypes.c_uint, 1),  # 10
                ('write_on_full_trace_index', ctypes.c_uint, 1),  # 11
                ('read_on_empty_last_pcall_addr', ctypes.c_uint, 1),  # 12
                ('write_on_full_last_pcall_addr', ctypes.c_uint, 1),  # 13
                ('compare_header_fifo_overflow', ctypes.c_uint, 1),  # 14
                ('not_enough_refreshes', ctypes.c_uint, 1),  # 15
                ('too_many_refreshes', ctypes.c_uint, 1),  # 16
                ('trigger_window_missed', ctypes.c_uint, 1),  # 17
                ('unexpected_clock_event', ctypes.c_uint, 1),  # 18
                ('trigger_in_fifo_underrun', ctypes.c_uint, 1),  # 19
                ('trigger_in_fifo_overflow', ctypes.c_uint, 1),  # 20
                ('xcvr_rx_fifo_full', ctypes.c_uint, 1),  # 21
                ('pvc_data_width_converter_full', ctypes.c_uint, 1),  # 22
                ('capture_data_sorter_full', ctypes.c_uint, 1),  # 23
                ('big_header_fifo_chain_full', ctypes.c_uint, 1),  # 24
                ('trace_stream_full', ctypes.c_uint, 1),  # 25
                ('capture_header_packer_full_alarm', ctypes.c_uint, 1),  # 26
                ('packer_got_full_first_alarm', ctypes.c_uint, 1),  # 27
                ('multiple_capture_arb_request', ctypes.c_uint, 1),  # 28
                ('xcvr_crc_error', ctypes.c_uint, 1),  # 29
                ('reserved', ctypes.c_uint, 2)]  # 30-31


class FLAGS(PatgenRegister):
    BAR = 1
    ADDR = 0x178
    _fields_ = [('RESERVED', c_uint, 17),
                ('Local_Sticky_Error', c_uint, 1),
                ('Domain_Sticky_Error', c_uint, 1),
                ('Global_Sticky_Error', c_uint, 1),
                ('Wire_OR', c_uint, 1),
                ('LoadActive', c_uint, 1),
                ('Software_Trigger', c_uint, 1),
                ('Domain_Trigger', c_uint, 1),
                ('RC_Trigger', c_uint, 1),
                ('Zero', c_uint, 1),
                ('Carry', c_uint, 1),
                ('OverFlow', c_uint, 1),
                ('Sign', c_uint, 1),
                ('BELOW_OR_EQUAL', c_uint, 1),
                ('LESS_THAN_OR_EQUAL', c_uint, 1),
                ('GREATER_THAN_OR_EQUAL', c_uint, 1)]


class CENTRAL_DOMAIN_REGISTER(PatgenRegister):
    BAR = 1
    ADDR = 0x1A0
    REGCOUNT = 32
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SI5344_CONTROL_REGISTER(PatgenRegister):
    BAR = 1
    ADDR = 0x2EC
    _fields_ = [('reset', c_uint, 1),
                ('Reserved', c_uint, 31), ]


class CAPTURE_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = 0x180
    CHECK = False
    _fields_ = [('capture_type', c_uint, 2), #0, 1
                ('capture_fails', c_uint, 1), #2
                ('capture_ctv', c_uint, 1), #3
                ('capture_all', c_uint, 1), #4
                ('overflow_action', c_uint, 1), #5
                ('buffer_status', c_uint, 1), #6
                ('index_mode', c_uint, 1), #7
                ('stop_on_max_fail', c_uint, 1), #8
                ('internal_loopback', c_uint, 1), #9
                ('flush_buffer', c_uint, 1), #10
                ('capture_ctv_after_max_fail', c_uint, 1),#11
                ('capture_complete', c_uint, 1),#12
                ('replace_trace_stream', c_uint, 1),#13
                ('reset_capture_addr_counts', c_uint, 1), #14
                ('RESERVED', c_uint, 17), ]


# TODO: ADDR for the fail capture are incorrect

class CAPTURE_LENGTH(PatgenRegister):
    BAR = 1
    ADDR = 0xaa8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MAX_CAPTURE_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0xaa8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MAX_FAIL_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0xaa8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MAX_FAIL_CAPTURE_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0xaa8
    _fields_ = [('Data', ctypes.c_uint, 32)]


# TODO: END OF WRONG ADDRESS


class CTV_HEADER_STREAM_START_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x184
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CTV_DATA_STREAM_START_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x188
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRACE_STREAM_START_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x18C
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ERROR_STREAM_PM_START_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x190
    REGCOUNT = 4
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CTV_CAPTURE_HEADER_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x280
    CHECK = False
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CTV_CAPTURE_DATA_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x284
    CHECK = False
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRACE_STREAM_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x288
    CHECK = False
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ERROR_STREAM_PM_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x28C
    REGCOUNT = 4
    CHECK = False
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ILA_CONTROL(PatgenRegister):
    ADDR = 0x400
    _fields_ = [('word_in_sample', ctypes.c_uint, 4),
                ('sample', ctypes.c_uint, 15),
                ('bypass_write_enable', ctypes.c_uint, 1),
                ('bypass_trigger', ctypes.c_uint, 1),
                ('select', ctypes.c_uint, 3),
                ('reset', ctypes.c_uint, 8)]


class ILA_STATUS(PatgenRegister):
    ADDR = 0x404
    _fields_ = [('capture_count', ctypes.c_uint, 15),
                ('reserved', ctypes.c_uint, 17)]


class ILA_DATA(PatgenRegister):
    ADDR = 0x408
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RING_BUS_CAPTURE_DATA_HOLD(PatgenRegister):
    BAR = 1
    ADDR = 0x384
    REGCOUNT = 4
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PG_TO_RM_AT_PG(PatgenRegister):
    BAR = 1
    ADDR = 0x260
    _fields_ = [('Data', ctypes.c_uint, 32)]


class RM_TO_PG_AT_PG(PatgenRegister):
    BAR = 1
    ADDR = 0x268
    _fields_ = [('Data', ctypes.c_uint, 32)]


PACKET_CONTROL_PAGE = 0x0460
PACKET_SNAPSHOT_PAGE = 0x0480
PACKET_TOTAL_PAGE = 0x04A0
PACKET_EOB_TOTAL_PAGE = 0x04C0
PACKET_INVALID_TOTAL_PAGE = 0x04E0


class RESET_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x00
    _fields_ = [('Data', c_uint, 32)]


class SNAPSHOT_CYCLE_NUMBER(PatgenRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x04
    _fields_ = [('Data', c_uint, 32)]


class FEATURE_SIGNATURE(PatgenRegister):
    BAR = 1
    ADDR = PACKET_CONTROL_PAGE + 0x08
    _fields_ = [('Data', c_uint, 32)]


class PM_E_PACKETS(PatgenRegister):
    BAR = 1
    ADDR = PACKET_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_PACKETS(PatgenRegister):
    BAR = 1
    ADDR = PACKET_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_E_EOB_PACKETS(PatgenRegister):
    BAR = 1
    ADDR = PACKET_EOB_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_EOB_PACKETS(PatgenRegister):
    BAR = 1
    ADDR = PACKET_EOB_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


#
# class PM1_C_EOB_PACKETS(PatgenRegister):
#     BAR = 1
#     ADDR = PACKET_EOB_TOTAL_PAGE + 0x14
#     _fields_ = [('Data', c_uint, 32)]
#
#
# class PM2_C_EOB_PACKETS(PatgenRegister):
#     BAR = 1
#     ADDR = PACKET_EOB_TOTAL_PAGE + 0x18
#     _fields_ = [('Data', c_uint, 32)]
#
#
# class PM3_C_EOB_PACKETS(PatgenRegister):
#     BAR = 1
#     ADDR = PACKET_EOB_TOTAL_PAGE + 0x1C
#     _fields_ = [('Data', c_uint, 32)]


# done
class PM_E_INVALID_PACKETS(PatgenRegister):
    BAR = 1
    ADDR = PACKET_INVALID_TOTAL_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PM_C_INVALID_PACKETS(PatgenRegister):
    BAR = 1
    ADDR = PACKET_INVALID_TOTAL_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


# class PM1_C_INVALID_PACKETS(PatgenRegister):
#     BAR = 1
#     ADDR = PACKET_INVALID_TOTAL_PAGE + 0x14
#     _fields_ = [('Data', c_uint, 32)]
#
#
# class PM2_C_INVALID_PACKETS(PatgenRegister):
#     BAR = 1
#     ADDR = PACKET_INVALID_TOTAL_PAGE + 0x18
#     _fields_ = [('Data', c_uint, 32)]
#
#
# class PM3_C_INVALID_PACKETS(PatgenRegister):
#     BAR = 1
#     ADDR = PACKET_INVALID_TOTAL_PAGE + 0x1C
#     _fields_ = [('Data', c_uint, 32)]


class PACKET_SNAPSHOT(PatgenRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_PAGE + 0x00
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class PACKET_XOR(PatgenRegister):
    BAR = 1
    ADDR = PACKET_SNAPSHOT_PAGE + 0x10
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


CAPTURE_PAGE_1 = 0x0180
CAPTURE_PAGE_2 = 0x0280
DEBUG_REGISTERS_PAGE_5 = 0x0320
DEBUG_REGISTERS_PAGE_3 = 0x02A0
DEBUG_REGISTERS_PAGE_4 = 0x02C0
DEBUG_REGISTERS_PAGE_9 = 0x0420
PATTERN_PAGE = 0x0160


class CTV_DATA_CNT_CAPTURE_ARB_TOP(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x04
    _fields_ = [('Data', c_uint, 32)]


class CAPTURE_CNT_RCVD_FROM_XCVR(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_4 + 0x0C
    _fields_ = [('Data', c_uint, 32)]


class CTV_ROW_COUNT_AFTER_FIRST_SORT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_3 + 0x14
    _fields_ = [('Data', c_uint, 32)]


class CTV_DATA_AFTER_256_0(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x00
    _fields_ = [('Data', c_uint, 32)]


class CTV_DATA_AFTER_256_1(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x04
    _fields_ = [('Data', c_uint, 32)]


class CTV_DATA_AFTER_256_2(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x08
    _fields_ = [('Data', c_uint, 32)]


class CTV_DATA_AFTER_256_3(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x0C
    _fields_ = [('Data', c_uint, 32)]


class FAKE_CTV_DATA_AFTER_256_0(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x10
    _fields_ = [('Data', c_uint, 32)]


class FAKE_CTV_DATA_AFTER_256_1(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x14
    _fields_ = [('Data', c_uint, 32)]


class FAKE_CTV_DATA_AFTER_256_2(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x18
    _fields_ = [('Data', c_uint, 32)]


class FAKE_CTV_DATA_AFTER_256_3(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_5 + 0x1C
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_CAPTURE_ARB_TOP_PM_0(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x0C
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_CAPTURE_ARB_TOP_PM_1(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x10
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_CAPTURE_ARB_TOP_PM_2(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x14
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_CAPTURE_ARB_TOP_PM_3(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x18
    _fields_ = [('Data', c_uint, 32)]


class READ_32_COUNT(PatgenRegister):
    BAR = 1
    ADDR = 0x02A4
    REGCOUNT = 4
    _fields_ = [('Data', c_uint, 32)]


class LAST_INSTRUCTION_ADDRESS(PatgenRegister):
    BAR = 1
    ADDR = PATTERN_PAGE + 0x14
    _fields_ = [('Data', c_uint, 32)]


class ERROR_COUNT_AFTER_FIRST_SORT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_3 + 0x18
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_AFTER_SECOND_SORT_PM_0(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_3 + 0x1C
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_AFTER_SECOND_SORT_PM_1(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_4 + 0x00
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_AFTER_SECOND_SORT_PM_2(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_4 + 0x04
    _fields_ = [('Data', c_uint, 32)]


class ERR_CNT_AFTER_SECOND_SORT_PM_3(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_4 + 0x08
    _fields_ = [('Data', c_uint, 32)]


class ERROR_PACKET_0_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x00
    _fields_ = [('Data', c_uint, 32)]


class ERROR_PACKET_1_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x04
    _fields_ = [('Data', c_uint, 32)]


class ERROR_PACKET_2_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x08
    _fields_ = [('Data', c_uint, 32)]


class ERROR_PACKET_3_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x0C
    _fields_ = [('Data', c_uint, 32)]


class CTV_HEADER_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x10
    _fields_ = [('Data', c_uint, 32)]


class CTV_ROW_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x14
    _fields_ = [('Data', c_uint, 32)]


class TRACE_REQUEST_CNT(PatgenRegister):
    BAR = 1
    ADDR = DEBUG_REGISTERS_PAGE_9 + 0x18
    _fields_ = [('Data', c_uint, 32)]


class TRACE_STREAM_CNT_CAPTURE_ARB_TOP(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x08
    _fields_ = [('Data', c_uint, 32)]


class CTV_HEADER_COUNT_AT_PVC_PROC(PatgenRegister):
    BAR = 1
    ADDR = 0x250
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CTV_HEADER_COUNT_AT_PACKER(PatgenRegister):
    BAR = 1
    ADDR = 0x25C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CAPTURE_DISTRIBUTION_CONTROL(PatgenRegister):
    BAR = 1
    ADDR = CAPTURE_PAGE_2 + 0x01C
    _fields_ = [('distribution', ctypes.c_uint, 8),
                ('block_size_exponent', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 16)]


class WAIT_TOWRITE_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x394
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CAPABILITIES(PatgenRegister):
    ADDR = 0x08C0
    _fields_ = [('set_e32_interval', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]


class E32_INTERVAL(PatgenRegister):
    ADDR = PATTERN_PAGE + 0x1C
    _fields_ = [('e32_interval', ctypes.c_uint, 32)]


class CTV_HEADER_STREAM_END_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x0A00
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class CTV_DATA_STREAM_END_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x0A04
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TRACE_STREAM_END_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x0A08
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ERROR_STREAM_PM_END_ADDRESS_REG(PatgenRegister):
    BAR = 1
    ADDR = 0x0A0C
    REGCOUNT = 4
    CHECK = True
    _fields_ = [('Data', ctypes.c_uint, 32)]


class L_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x440
    _fields_ = [('Data', ctypes.c_uint, 32)]


class H_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x444
    _fields_ = [('Data', ctypes.c_uint, 32)]


class X_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x448
    _fields_ = [('Data', ctypes.c_uint, 32)]


class V_M_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x44C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ZERO_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x450
    _fields_ = [('Data', ctypes.c_uint, 32)]


class ONE_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x454
    _fields_ = [('Data', ctypes.c_uint, 32)]


class E_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x458
    _fields_ = [('Data', ctypes.c_uint, 32)]


class K_COUNTER(PatgenRegister):
    BAR = 1
    ADDR = 0x45C
    _fields_ = [('Data', ctypes.c_uint, 32)]


class UN_PARKED_DUTS(PatgenRegister):
    BAR = 1
    ADDR = 0xA1C
    _fields_ = [('un_parked_duts', ctypes.c_uint, 16),
                ('Reserved', ctypes.c_uint, 16)]

