# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
from enum import IntEnum
import csv
import time
import os

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from . import ring_multiplier_register
from Common import configs
from Common import hilmon as hil
from Common import pcie
from Common import stratix10
from Common.instruments.hbi_instrument import HbiInstrument

TOTAL_MEMORY_SIZE = 1 << 30
SLICECOUNT = range(0, 5)
SLICE_TARGET_INTERFACE_MAPPING = {0: (6, 7), 1: (8, 9), 2: (10, 11), 3: (12, 13), 4: (14, 15)}
TQ_ENGINE_COUNT = 17

#TODO: Replace Dictionary with method that calculates starting cycle for snooper
SNOOPER_ADDR = {
    'Directed30kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 23560, 'pm0_rm_rm': 11780},
    'Directed13kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 10464, 'pm0_rm_rm': 5232},
    'Directed12kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 9008, 'pm0_rm_rm': 4504},
    'Directed20kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 15556, 'pm0_rm_rm': 7778},
    'Directed20kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 15558, 'pm0_rm_rm': 7779},
    'Directed15kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 11920, 'pm0_rm_rm': 5960},
    'Directed1kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 1004, 'pm0_rm_rm': 502},
    'Directed12kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 9008, 'pm0_rm_rm': 4504},
    'Directed5kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 3916, 'pm0_rm_rm': 1958},
    'Directed7kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 5372, 'pm0_rm_rm': 2686},
    'Directed7kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 5372, 'pm0_rm_rm': 2686},
    'Directed1kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 1004, 'pm0_rm_rm': 502},
    'Directed30kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 23024, 'pm0_rm_rm': 11512},
    'Directed5kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 3916, 'pm0_rm_rm': 1958},
    'Directed10kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 7552, 'pm0_rm_rm': 3776},
    'Directed13kCtvXtremeChannelSet0Ctp1SubCycle0Test': {'rm_pg_rm': 10464, 'pm0_rm_rm': 5232},
    'Directed15kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 11920, 'pm0_rm_rm': 5960},
    'Directed10kCtvXtremeAllChannelSetCtp3SubCycle0Test': {'rm_pg_rm': 7552, 'pm0_rm_rm': 3776}}


class CommandEncoding(IntEnum):
    START = 0
    END = 1
    DELAY = 2
    SYNC_DELAY = 4


class RingMultiplier(HbiInstrument):

    def __init__(self, rc=None):
        self.rc = rc
        if rc and rc.mainboard:
            self.mainboard = rc.mainboard
        else:
            self.mainboard = None
        self.blt = None
        self.fpga_index = 2
        self.registers = ring_multiplier_register
        self.fpga_load = configs.HBIRINGMULTIPLIER_FPGA_IMAGE_LOAD
        self.ila_names = {}
        self.fab_version = None

    def initialize(self):
        self.load_fpga()
        self.log_cpld()  #
        self.log_fpga_version()  #
        self.log_build()  #

    def fpga_image_path(self):
        if configs.HBICC_IN_HDMT:
            fab_letter = configs.HBICC_IN_HDMT_FAB
            self.warn_user_fab_detection_is_bypassed(fab_letter)
        else:
            fab_letter = self.mainboard.blt.fab_letter()
        return getattr(configs, f'HBIRINGMULTIPLIER_FPGA_IMAGE_FAB_{fab_letter}')

    def load_fpga_using_hil(self):
        num_retries = 5
        with suppress_sensor('FVAL RingMultiplier', 'HBI_RING'):
            for retry in range(num_retries):
                try:
                    hil.hbiMbDeviceDisable(self.fpga_index)
                    hil.hbiMbFpgaLoad(self.fpga_index, self.fpga_image_path())
                    self.enable_device()
                    self.Log('info', f'{self.name()} FPGA Version: '
                                     f'{self.fpga_version_string()}')
                    self.tfs_65232_workaround()
                    break
                except RuntimeError as e:
                    self.Log('warning',
                             f'{self.name()} failed to properly load '
                             f'image and re-enable driver {retry + 1} of '
                             f'{num_retries} times')

    def tfs_65232_workaround(self):
        time.sleep(1)
        self.Log('debug', 'Executing workaround for TFS 65232')
        self.dma_write(0, bytes(2048))
        if hil.hbiMbFpgaVersion(self.fpga_index) == 0xFFFFFFFF:
            self.Log('critical', 'FPGA has become unusable after DMA operation')
        else:
            self.Log('debug', 'TFS 65232 workaround successful')
            
        
    def enable_device(self):
        hil.hbiMbDeviceEnable(self.fpga_index)

    def open_pcie_device(self):
        handle = hil.pciDeviceOpen(pcie.ring_multiplier_guid, pcie.ring_multiplier_viddid, -1, None)
        hil.pciDeviceClose(handle)
    
    def read_slice_register(self, register_type, slice=0, index=0):
        return self.read_bar_register(register_type=register_type, bar_space=slice+1, index=index)

    def read_bar_register(self, register_type, bar_space=1, index=0):
        # print('REgister values: ', register_type, bar_space, index)
        """Reads a register of the given type, the index is for register arrays"""
        regvalue = hil.hbiMbBarRead(self.fpga_index, bar_space, register_type.address(index))
        return register_type(value=regvalue)

    def read_register_raw(self, register_type, slice=0, index=0):
        """Reads a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarRead(self.fpga_index, slice+1, register_type.address(index))
        
    def write_slice_register(self, register, slice=0, index=0):
        self.write_bar_register(register, bar_space=slice+1, index=index)
        
    def write_bar_register(self, register, bar_space=1, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarWrite(self.fpga_index, bar_space, register.address(index), register.value)
        
    def write_register_raw(self, Register, data, bar_space=1, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarWrite(self.fpga_index, bar_space, Register.address(index), data)

    def write_register_rawer(self, address, data, slice=0, index=0):
        """Writes a register of the given type, the index is for register arrays"""
        return hil.hbiMbBarWrite(self.fpga_index, slice+1, address, data)

    def cpld_version_string(self):
        return hil.hbiMbCpldVersionString(self.fpga_index)

    def fpga_version_string(self):
        return hil.hbiMbFpgaVersionString(self.fpga_index)

    def read_hg_id(self):
        lower = self.read_slice_register(self.registers.HG_ID_LOWER).value
        upper = self.read_slice_register(self.registers.HG_ID_UPPER).value
        return upper, lower

    def get_control_register(self, receiver):
        options = {'PATGEN': self.registers.PATGEN_RING_BUS_CONTROL,
                   'PSDB_0_PIN_0': self.registers.PSDB0_RING_BUS_CONTROL,
                   'PSDB_1_PIN_0': self.registers.PSDB1_RING_BUS_CONTROL}
        return options[receiver]

    def get_status_register(self, receiver):
        options = {'PATGEN': self.registers.PATGEN_RING_BUS_STATUS,
                   'PSDB_0_PIN_0': self.registers.PSDB0_RING_BUS_STATUS,
                   'PSDB_1_PIN_0': self.registers.PSDB1_RING_BUS_STATUS}
        return options[receiver]

    def get_error_count_register(self, receiver):
        options = {'PATGEN': self.registers.PATGEN_RING_BUS_ERROR_COUNT,
                   'PSDB_0_PIN_0': self.registers.PSDB0_RING_BUS_ERROR_COUNT,
                   'PSDB_1_PIN_0': self.registers.PSDB1_RING_BUS_ERROR_COUNT}
        return options[receiver]

    def get_error_count_per_lane_register(self, receiver):
        options = {'PATGEN': self.registers.PATGEN_XCVR_ERROR_COUNT_LANE,
                   'PSDB_0_PIN_0': self.registers.PSDB0_XCVR_ERROR_COUNT_LANE,
                   'PSDB_1_PIN_0': self.registers.PSDB1_XCVR_ERROR_COUNT_LANE}
        return options[receiver]

    def get_transferred_pattern_count(self, receiver):
        options = {'PATGEN': self.get_transferred_pattern_count_for_pat_gen,
                   'PSDB_0_PIN_0': self.get_transferred_pattern_count_for_psdb_0_pin_0,
                   'PSDB_1_PIN_0': self.get_transferred_pattern_count_for_psdb_1_pin_0}
        return options[receiver]

    def get_transferred_pattern_count_for_pat_gen(self):
        transfer_word_size = 64
        lower = self.read_slice_register(self.registers.PATGEN_RING_BUS_PATTERN_COUNT, index=0).value
        upper = self.read_slice_register(self.registers.PATGEN_RING_BUS_PATTERN_COUNT, index=1).value

        combined_values = self.combined_hex_values([upper, lower], 32)
        return combined_values * transfer_word_size

    def get_transferred_pattern_count_for_psdb_0_pin_0(self):
        transfer_word_size = 64
        lower = self.read_slice_register(self.registers.PSDB0_RING_BUS_PATTERN_COUNT, index=0).value
        upper = self.read_slice_register(self.registers.PSDB0_RING_BUS_PATTERN_COUNT, index=1).value

        combined_values = self.combined_hex_values([upper, lower], 32)
        return combined_values * transfer_word_size

    def get_transferred_pattern_count_for_psdb_1_pin_0(self):
        transfer_word_size = 64
        lower = self.read_slice_register(self.registers.PSDB1_RING_BUS_PATTERN_COUNT, index=0).value
        upper = self.read_slice_register(self.registers.PSDB1_RING_BUS_PATTERN_COUNT, index=1).value

        combined_values = self.combined_hex_values([upper, lower], 32)
        return combined_values * transfer_word_size

    def reconfig_bridge_for_pat_gen(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PATGEN_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PATGEN_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PATGEN_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PATGEN_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_pll_bridge_for_pat_gen(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PATGEN_PLL_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PATGEN_PLL_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PATGEN_PLL_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PATGEN_PLL_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_bridge_for_psdb_1_pin_multiplier_0(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PSDB0_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PSDB0_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PSDB0_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PSDB0_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_pll_bridge_for_psdb_1_pin_multiplier_0(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PSDB0_PLL_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PSDB0_PLL_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PSDB0_PLL_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PSDB0_PLL_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_bridge_for_psdb_0_pin_multiplier_0(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PSDB1_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PSDB1_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PSDB1_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PSDB1_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def reconfig_pll_bridge_for_psdb_0_pin_multiplier_0(self, target_reg, target_reg_value=None):
        bridge_reg = self.registers.XCVR_PSDB1_PLL_RECONFIG_ADDRESS
        write_reg = self.registers.XCVR_PSDB1_PLL_RECONFIG_WRITE_DATA
        read_reg = self.registers.XCVR_PSDB1_PLL_RECONFIG_READ_DATA
        tag_reg = self.registers.XCVR_PSDB1_PLL_RECONFIG_READ_DATA_TAG
        return self.reconfig_bridge_reg(bridge_reg, write_reg, read_reg, tag_reg, target_reg, target_reg_value)

    def set_aurora_control(self):
        self.write_slice_register(self.registers.AURORA_CONTROL(value=0x1))

    def aurora_status(self):
        return self.read_register_raw(self.registers.AURORA_STATUS)

    def aurora_error_count(self):
        return self.read_register_raw(self.registers.AURORA_ERROR_COUNTS)

    def set_aurora_control(self):
        self.write_slice_register(self.registers.AURORA_CONTROL(value=0x1))

    def aurora_status(self):
        return self.read_register_raw(self.registers.AURORA_STATUS)

    def aurora_error_count(self):
        return self.read_register_raw(self.registers.AURORA_ERROR_COUNTS)

    def reset_error_count(self, register, slice_list=None):
        register.ResetErrorCount = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
        register.ResetErrorCount = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def reset_link(self, register, slice_list=None):
        register.Reset = 0x1
        slices = slice_list if slice_list is not None else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)
        register.Reset = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def check_error_count(self, error_count_register):
        error_count_value = self.read_register_raw(error_count_register)
        if error_count_value:
            self.Log('error',
                     '{} Transceiver lane error count set to {}'.format(self.__class__.__name__, error_count_value))
        return error_count_value

    def is_register_set(self, slice, value, expected, reg_name, bit_name=''):
        if value == expected:
            return True
        else:
            return False

    def enable_prbs(self, register, slice_list=None):
        register.EnablePrbs = 0x1
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def disable_prbs(self, register, slice_list=None):
        register.EnablePrbs = 0x0
        slices = slice_list if slice_list else SLICECOUNT
        for slice in slices:
            self.write_slice_register(register, slice)

    def temperature_using_hil(self):
        return hil.hbiMbTmonRead(13)

    def temperature_using_bar_read(self):
        raw_temperature = self.read_register_raw(self.registers.TEMPERATURE0_V)
        return stratix10.convert_raw_temp_to_degrees_celcius(raw_temperature)

    def dma_write(self, offset, data):
        try:
            return hil.hbiMbDmaWrite(self.fpga_index, offset, data)
        except RuntimeError:
            self.Log('info', '{}: DMA write failed.'.format(self.__class__.__name__))
            return None

    def dma_read(self, offset, size):
        try:
            return hil.hbiMbDmaRead(self.fpga_index, offset, size)
        except RuntimeError:
            self.Log('info', f'{self.name()}: DMA read failed, offset={offset:09X}, size={size}')
            return None

    def check_ddr_calibration_status(self, retry_limit=10):
        if retry_limit == 0:
            self.Log('warning',
                     'Maximum number of retries exceeded for {} DDR calibration.Cannot perform DMA'.format(self.name()))
            return False
        else:
            ddr_calibration_status = self.get_ddr_calibration_status()
            if ddr_calibration_status != 5:
                retry_limit = retry_limit - 1
                time.sleep(0.1)
                return self.check_ddr_calibration_status(retry_limit)
            else:
                return True

    def initialize_memory_block(self):
        step_size = 1 << 30
        zero_initialised_1gb_data = bytes(step_size)
        if self.check_ddr_calibration_status(30):
            for offset in range(0, TOTAL_MEMORY_SIZE, step_size):
                start_address = offset
                self.dma_write(start_address, zero_initialised_1gb_data)
                read_back_data = self.dma_read(start_address, step_size)
                if read_back_data != zero_initialised_1gb_data:
                    self.Log('error', 'DMA init failed for {}'.format(self.name()))
                    break
            else:
                self.Log('info', 'DMA init successful for {}'.format(self.name()))

    def get_ddr_calibration_status(self, bar=None):
        ddr_calibration_status = self.read_register_raw(self.registers.DDR_STATUS)
        return ddr_calibration_status

    def take_snapshot(self, directory):
        filename = os.path.join(directory, 'registers.csv')
        self.take_raw_register_snapshot(directory)
        with open(filename, 'w') as fout:
            writer = csv.writer(fout)
            writer.writerow(["Address", "Slice", "Register value", "Register Name"])
            for register_type in self.registers.get_register_types_sorted_by_name():
                for slice in range(5):
                    if register_type.REGCOUNT == 1:
                        data = self.read_register_raw(register_type, slice)
                        fout.write(
                            '0x{:0x},{},0x{:0x},{}\n'.format(register_type.ADDR, slice, data, register_type.__name__))
                    else:
                        for register_index in range(register_type.REGCOUNT):
                            data = self.read_register_raw(register_type, slice, register_index)
                            fout.write(
                                '0x{:0x},{},0x{:0x},{}[{}]\n'.format(register_type.address(register_index), slice, data,
                                                                     register_type.__name__,
                                                                     register_index))

    def get_rm_aurora_status_register(self):
        return self.registers.AURORA_STATUS

    def get_rm_aurora_control_register(self):
        return self.registers.AURORA_CONTROL

    def get_rm_aurora_error_count_register(self):
        return self.registers.AURORA_ERROR_COUNTS

    def get_rm_trigger_up_register(self):
        return self.registers.SEND_TRIGGER

    def aurora_status(self, index=0):
        return self.read_slice_register(self.registers.AURORA_STATUS)

    def aurora_error_count(self, index=0):
        return self.read_register_raw(self.registers.AURORA_ERROR_COUNTS)

    def read_rm_aurora_error_count(self):
        return self.read_register_raw(self.registers.AURORA_ERROR_COUNTS)

    def send_trigger(self, trigger_value, index=0):
        self.write_register_raw(self.registers.SEND_TRIGGER, trigger_value)

    def read_trigger(self, index=0):
        return self.read_register_raw(self.registers.LAST_TRIGGER_SEEN)

    def read_dc_trigger_page_processed_queues(self):
        return self.read_register_raw(self.registers.DC_TRIGGER_PAGE_PROCESSED_QUEUES)

    def get_aurora_status_and_error_count(self):
        rm_aurora_error_count = self.read_rm_aurora_error_count()
        rm_aurora_status = self.aurora_status()
        return rm_aurora_error_count, rm_aurora_status

    def reset_aurora_link(self, register):
        register.reset = 0x1
        register.clear_error_count = 0x1
        self.write_slice_register(register)
        register.reset = 0x0
        register.clear_error_count = 0x0
        self.write_slice_register(register)
    
    def disable_aurora(self, link=0):
        r = self.read_slice_register(self.registers.AURORA_CONTROL)
        r.reset = 1
        r.clear_error_count = 1
        r.clear_fifos = 1
        self.write_slice_register(r)
    
    def enable_aurora(self, link=0):
        r = self.read_slice_register(self.registers.AURORA_CONTROL)
        r.reset = 0
        r.clear_error_count = 0
        r.clear_fifos = 0
        self.write_slice_register(r)
        
    def is_aurora_link_up(self, index=0):
        link_up = False
        error_count, status = self.get_aurora_status_and_error_count()
        if status.value == 0x27 and error_count == 0:
            link_up = True
        return link_up, status, error_count

    def clear_trigger_fifo(self, index=0):
        control = self.read_slice_register(self.registers.AURORA_CONTROL)
        control.clear_fifos = 0x1
        self.write_slice_register(control)
        time.sleep(0.001)
        control.clear_fifos = 0x0
        self.write_slice_register(control)

    def clear_trigger_error_count(self, index=0):
        control = self.read_slice_register(self.registers.AURORA_CONTROL)
        control.clear_error_count = 0x1
        self.write_slice_register(control)
        time.sleep(0.001)
        control.clear_error_count = 0x0
        self.write_slice_register(control)

    def create_trigger_queue_command_record(self, payload_lower=0, target_interface=0,
                                            payload_byte_count=0, psdb_target=None, read_write=0, command_data=0,
                                            channel_set=0, command_encoding_upper=0, dma=False, spi_target=0):
        dc_trigger_queue_low = self.registers.DC_TRIGGER_QUEUE_RECORD_LOW
        dc_trigger_queue_low_obj = dc_trigger_queue_low()

        dc_trigger_queue_high = self.registers.DC_TRIGGER_QUEUE_RECORD_HIGH
        dc_trigger_queue_high_obj = dc_trigger_queue_high()

        dc_trigger_queue_low_obj.PayLoadLower = payload_lower

        dc_trigger_queue_high_obj.PayLoadUpper = command_encoding_upper
        dc_trigger_queue_high_obj.InterfaceTarget = target_interface
        dc_trigger_queue_high_obj.ValidPayloadByteCount = payload_byte_count
        dc_trigger_queue_high_obj.PSDBTarget = psdb_target
        dc_trigger_queue_high_obj.WriteReadn = read_write
        dc_trigger_queue_high_obj.CommandData = command_data
        dc_trigger_queue_high_obj.ChannelSet = channel_set
        dc_trigger_queue_high_obj.SPITarget = spi_target

        if dma:
            if hasattr(self, 'ddr_tq_list_reversed'):
                self.ddr_tq_list_reversed = (self.ddr_tq_list_reversed << 64) | \
                                        (dc_trigger_queue_high_obj.value << 32) | dc_trigger_queue_low_obj.value
            return (dc_trigger_queue_high_obj.value << 32) | dc_trigger_queue_low_obj.value
        else:
            self.write_slice_register(dc_trigger_queue_low_obj)
            self.write_slice_register(dc_trigger_queue_high_obj)
            pmbus_targets = [1, 2, 3, 4, 5]
            if command_data == 0 and target_interface in pmbus_targets:
                self.precise_wait(0.001)
            else:
                self.precise_wait(0.00001)

    def precise_wait(self, duration):
        start = time.perf_counter()
        while (time.perf_counter() - start) < duration:
            pass
        
    def start_trigger_queue_record(self, psdb_target, dma=False):
        self.ddr_tq_list_reversed = 0
        command_data = 1
        command_encoding = CommandEncoding.START
        return self.create_trigger_queue_command_record(target_interface=command_encoding, psdb_target=psdb_target,
                                                        command_data=command_data, dma=dma)

    def end_trigger_queue_record(self, psdb_target, dma=False):
        command_data = 1
        command_encoding = CommandEncoding.END
        return self.create_trigger_queue_command_record(target_interface=command_encoding, psdb_target=psdb_target,
                                                        command_data=command_data, dma=dma)

    def start_tq_record_dma(self):
        self.ddr_tq_list_reversed = 0

    def end_tq_record_dma(self):
        return self.ddr_tq_list_reversed

    def store_one_dut_tq_dma_non_broadcast(self, index, dut_id, tq_commands, tq_address=0x20000000):
        tq_content = self.create_tq_content_from_command_list(tq_commands)
        self.store_single_dut_tq_dma(index, dut_id, tq_content, tq_address)

    def store_duts_tq_dma_broadcast(self, index, duts_tq_commands_list=None, duts_tq_address_list=None):
        if duts_tq_commands_list is None:
            tq_content = self.ddr_tq_list_reversed
            duts_tq_content_list = self.dut0_to_63_tq_list_creation(tq_content)
        else:
            duts_tq_content_list = self.create_dut0_to_63_tq_content_from_command_list(duts_tq_commands_list)
        self.store_dut0_to_63_tq_dma(index, duts_tq_content_list, duts_tq_address_list)
        if hasattr(self, 'ddr_tq_list_reversed'):
            del self.ddr_tq_list_reversed

    def create_tq_content_from_command_list(self, tq_commands):
        tq_content = 0
        tq_commands_copy = tq_commands.copy()
        tq_commands_copy.reverse()
        for command in tq_commands_copy:
            tq_content = (tq_content << 64) | command
        return tq_content

    def create_dut0_to_63_tq_content_from_command_list(self, duts_tq_commands_list):
        duts_tq_content_list = []
        for tq_commands in duts_tq_commands_list:
            duts_tq_content_list.append(self.create_tq_content_from_command_list(tq_commands))
        return duts_tq_content_list

    def store_single_dut_tq_dma(self, index, dut_id, tq_content, tq_address):
        lut_address = ((dut_id << 20) + index) << 3
        self.store_1tq_content_in_memory(tq_content, tq_address)
        self.store_1lut_in_memory(tq_address, lut_address)

    def store_dut0_to_63_tq_dma(self, index, duts_tq_content_list, duts_tq_address_list=None):
        if duts_tq_address_list is None:
            duts_tq_address_list = self.duts_tq_address_calculation(duts_tq_content_list)
        self.store_64tq_content_in_memory(duts_tq_content_list, duts_tq_address_list)
        self.store_64tq_addrs_in_lut_memory(duts_tq_address_list, index)

    def store_64tq_content_in_memory(self, duts_tq_content_list, duts_tq_address_list):
        for i in range(64):
            tq_content = duts_tq_content_list[i]
            tq_address = duts_tq_address_list[i]
            self.store_1tq_content_in_memory(tq_content, tq_address)

    def store_64tq_addrs_in_lut_memory(self, duts_tq_addrs_list, index):
        for i in range(64):
            dut_id = i
            lut_content = duts_tq_addrs_list[i]
            lut_address = ((dut_id << 20) + index) << 3
            self.store_1lut_in_memory(lut_content, lut_address)

    def store_1tq_content_in_memory(self, tq_content, tq_address):
        tq_content_in_byte = tq_content.to_bytes((tq_content.bit_length() + 7) // 8, byteorder='little')
        self.dma_write(tq_address, tq_content_in_byte)

    def store_1lut_in_memory(self, lut_content, lut_address):
        lut_content_in_byte = lut_content.to_bytes(8, byteorder='little')
        self.dma_write(lut_address, lut_content_in_byte)

    def duts_tq_address_calculation(self, duts_tq_content):
        start_addrs = 0x20000000
        duts_tq_addrs = [None] * 64
        for i in range(64):
            duts_tq_addrs[i] = start_addrs
            command_count = (duts_tq_content[i].bit_length() + 63) // 64
            start_addrs = start_addrs + 8*command_count
        return duts_tq_addrs

    def dut0_to_63_tq_list_creation(self, tq_content):
        start_command_content = self.start_trigger_queue_record(psdb_target=0, dma=True)
        end_command_content = self.end_trigger_queue_record(psdb_target=0, dma=True)
        duts_tq_content_list = [end_command_content] * 64
        command_count = (tq_content.bit_length() + 63) // 64
        for i in range(command_count):
            command = tq_content & 0xFFFFFFFFFFFFFFFF
            dut_id = self.tq_command_dut_decode(command)
            duts_tq_content_list[dut_id] = (duts_tq_content_list[dut_id] << 64) + command
            tq_content = tq_content >> 64
        if tq_content > 0:
            self.Log('error', 'ddr_tq_list_operation failed')
        for i in range(64):
            duts_tq_content_list[i] = (duts_tq_content_list[i] << 64) + start_command_content
        return duts_tq_content_list

    def tq_dut_info_print(self, duts_tq_content_list):
        for i in range(64):
            command_count = (duts_tq_content_list[i].bit_length() + 63) // 64
            copy = duts_tq_content_list[i]
            for x in range(command_count):
                command = copy & 0xFFFFFFFFFFFFFFFF
                copy = copy >> 64
                self.Log('debug',f'dut {i} num {x} command {command:08x}')

    def tq_command_dut_decode(self, command):
        channel_set = (command >> 52) & 7
        psdb = (command >> 61) & 1
        dut = 8*psdb + channel_set
        return dut

    def generate_lut(self, tq_list_address=0x20000000):
        self.tq_list_address = tq_list_address

    def delay_within_trigger_queue_record(self, psdb_target, dma, delay, plb_count):
        command_data = 1
        command_encoding = CommandEncoding.DELAY
        return self.create_trigger_queue_command_record(target_interface=command_encoding, psdb_target=psdb_target,
                                                        command_data=command_data, dma=dma, payload_lower=delay,
                                                        payload_byte_count=plb_count)

    def sync_delay_within_trigger_queue_record(self, delay):
        command_data = 1
        command_encoding = CommandEncoding.SYNC_DELAY
        return self.create_trigger_queue_command_record(target_interface=command_encoding, psdb_target=0,
                                                        command_data=command_data, dma=True, payload_lower=delay,
                                                        payload_byte_count=0)

    def ad5684_command_encoding(self, command_code, dac_select, voltage_to_be_applied):
        vref = 2.5
        voltage_in_dac_code = int(0xFFF * (voltage_to_be_applied / vref))
        dac_command_encoding_24bit = command_code << 20 | dac_select << 16 | voltage_in_dac_code << 4
        return dac_command_encoding_24bit

    def set_pin_direction(self, psdb_target, slice_index, payload_8_pins, payload_16_pins, payload_last_11_pins, channel_set,
                          dma=False, spi_target=0):
        command_data = 0
        command_first = \
            self.create_trigger_queue_command_record(read_write=1, payload_lower=payload_16_pins,
                                                     target_interface=SLICE_TARGET_INTERFACE_MAPPING[slice_index][0],
                                                     channel_set=channel_set, psdb_target=psdb_target,
                                                     command_data=command_data, command_encoding_upper=payload_8_pins,
                                                     dma=dma, spi_target=spi_target)
        command_second = \
            self.create_trigger_queue_command_record(read_write=1, payload_lower=payload_last_11_pins,
                                                     target_interface=SLICE_TARGET_INTERFACE_MAPPING[slice_index][1],
                                                     channel_set=channel_set, psdb_target=psdb_target,
                                                     command_data=command_data, command_encoding_upper=0,
                                                     dma=dma, spi_target=spi_target)
        return [command_first, command_second]

    def enable_burst_start(self, slice):
        aurora_control_register = self.read_slice_register(self.registers.AURORA_PATTERN_CONTROL)
        aurora_control_register.burst_sync_start_valid = 1
        aurora_control_register.sync_start_sliceid = slice
        self.write_slice_register(aurora_control_register)

    def verify_psdb_aurora_pattern_status(self, psdb_target):
        psdb_aurora_status_register = 'PSDB{}_AURORA_PATTERN_STATUS'.format(psdb_target)
        psdb_aurora_status_register_value = self.read_slice_register(getattr(self.registers, psdb_aurora_status_register))

        pll_locked = psdb_aurora_status_register_value.pll_locked
        lane_up = psdb_aurora_status_register_value.lane_up
        channel_up = psdb_aurora_status_register_value.channel_up
        tx_pll_locked = psdb_aurora_status_register_value.tx_pll_locked

        if self.is_register_set(slice=1, value=pll_locked, expected=0x1, reg_name=psdb_aurora_status_register_value,
                                bit_name='pll_locked'):

            if self.is_register_set(slice=1, value=lane_up, expected=0x1, reg_name=psdb_aurora_status_register_value,
                                    bit_name='lane_up'):

                if self.is_register_set(slice=1, value=channel_up, expected=0x1,
                                        reg_name=psdb_aurora_status_register_value,
                                        bit_name='channel_up'):

                    if self.is_register_set(slice=1, value=tx_pll_locked, expected=0x1,
                                            reg_name=psdb_aurora_status_register_value,
                                            bit_name='tx_pll_locked'):
                        return True

        else:
            return False

    def set_vref(self, psdb_target, voltage_to_set=0.9):
        read_write = 1
        command_data = 0
        spi_target = 0
        payload_byte_count = 3
        dac_list = [1, 2, 4, 8]
        for dac_select in dac_list:
            write_and_update_cmdad5684 = 3
            payload_lower = self.ad5684_command_encoding(write_and_update_cmdad5684, dac_select, voltage_to_set)
            self.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count,
                                                     psdb_target,
                                                     read_write, command_data)

    def write_to_rm_internal_register(self,trigger_to_be_sent):
        dc_trigger_aurora_trigger = self.registers.DC_TRIGGER_PAGE_2_AURORA_TRIGGER()
        dc_trigger_aurora_trigger.trigger_value = trigger_to_be_sent
        self.write_slice_register(dc_trigger_aurora_trigger)
        time.sleep(0.01)
        trigger_at_dc_aurora_trigger_register = self.read_register_raw(self.registers.DC_TRIGGER_PAGE_2_AURORA_TRIGGER)

    # power state 0 - DUT is powered ON    1 - DUT is powered off
    # return 64 bit value
    def read_dc_trigger_dut_power_state(self):
        lo = self.read_register_raw(self.registers.DUT_POWER_STATE_RD_31__0)
        hi = self.read_register_raw(self.registers.DUT_POWER_STATE_RD_63_32)
        return (hi << 32) | lo;

    # power state 0 - DUT is powered ON    1 - DUT is powered off
    # return 64 bit value
    def read_dc_trigger_last_written_dut_power_state(self):
        reg_0 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_15__0).power_state
        reg_1 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_31_16).power_state
        reg_2 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_47_32).power_state
        reg_3 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_63_48).power_state
        return (reg_3 << 48 )|(reg_2 << 32)|(reg_1 << 16) | reg_0;

    # mask value meaning 0 - enable        1 - ignore
    # return 64 bit value
    def read_dc_trigger_last_written_dut_power_mask(self):
        reg_0 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_15__0).mask
        reg_1 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_31_16).mask
        reg_2 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_47_32).mask
        reg_3 = self.read_slice_register(self.registers.DUT_POWER_STATE_MASK_WR_63_48).mask
        return (reg_3 << 48 )|(reg_2 << 32)|(reg_1 << 16) | reg_0;

    # power state 0 - DUT is powered ON    1 - DUT is powered off
    # mask value meaning 0 - enable        1 - ignore
    def write_dc_trigger_dut_power_state(self, power_state, mask=0):
        reg_0 = self.registers.DUT_POWER_STATE_MASK_WR_15__0()
        reg_1 = self.registers.DUT_POWER_STATE_MASK_WR_31_16()
        reg_2 = self.registers.DUT_POWER_STATE_MASK_WR_47_32()
        reg_3 = self.registers.DUT_POWER_STATE_MASK_WR_63_48()
        reg_0.power_state = power_state & 0xFFFF
        reg_0.mask = mask & 0xFFFF
        reg_1.power_state = ( power_state >> 16 ) & 0xFFFF
        reg_1.mask = ( mask >> 16 ) & 0xFFFF
        reg_2.power_state = ( power_state >> 32 ) & 0xFFFF
        reg_2.mask = ( mask >> 32 ) & 0xFFFF
        reg_3.power_state = ( power_state >> 48 ) & 0xFFFF
        reg_3.mask = ( mask >> 32 ) & 0xFFFF
        self.write_slice_register(reg_0)
        self.write_slice_register(reg_1)
        self.write_slice_register(reg_2)
        self.write_slice_register(reg_3)

    def read_dc_trigger_enable_aurora_data(self):
        return self.read_slice_register(self.registers.DC_TRIGGER_PAGE_2_CONTROL).aurora_data_enable

    def write_dc_trigger_enable_aurora_data(self, enable=1):
        self.write_slice_register(self.registers.DC_TRIGGER_PAGE_2_CONTROL(aurora_data_enable=enable))


    def reset_rm_packet_snooper(self,slice_list):
        for slice in slice_list:
            reset_control_register = self.read_slice_register(self.registers.RESET_CONTROL_0, slice=slice)
            reset_control_register.Data = 1
            self.write_slice_register(reset_control_register, slice)
            self.write_register_rawer(0x464, 0, slice)

            reset_control_register.Data = 0
            self.write_slice_register(reset_control_register, slice)

        for slice in slice_list:
            reset_control_register = self.read_slice_register(self.registers.RESET_CONTROL_1, slice=slice)
            reset_control_register.Data = 1
            self.write_slice_register(reset_control_register, slice)
            self.write_register_rawer(0x4a4, 0, slice)
            reset_control_register.Data = 0
            self.write_slice_register(reset_control_register, slice)

    def set_user_cycle_to_snoop(self,cycle_no,slice_list):
        self.Log('debug', f'{self.name()} Snooper Cycle Number: {cycle_no}')
        for slice in slice_list:
            reset_control_register = self.read_slice_register(self.registers.SNAPSHOT_CYCLE_NUMBER, slice=slice)
            reset_control_register.Data = cycle_no
            self.write_slice_register(reset_control_register, slice)
            self.write_slice_register(reset_control_register, slice)

    def reset_capture_control(self, slices=SLICECOUNT):
        for slice in slices:
            capture_control = self.read_slice_register(self.registers.CAPTURE_CONTROL, slice=slice)
            capture_control.reset = 1
            for i in range(20):
                # writing the same value multiple times ensures reset is high for long enough
                self.write_slice_register(capture_control, slice)
            capture_control.reset = 0
            self.write_slice_register(capture_control, slice)

    def set_active_psdb(self):
        aurora_control_register = self.read_slice_register(self.registers.AURORA_PATTERN_CONTROL)
        aurora_control_register.psdb0_present = 0
        aurora_control_register.psdb1_present = 0
        self.write_slice_register(aurora_control_register)
        if configs.PSDB_0:
            aurora_control_register.psdb0_present = 1
        if configs.PSDB_1:
            aurora_control_register.psdb1_present = 1
        self.write_slice_register(aurora_control_register)

    def set_snooper_start_cycle(self, test_name, slices=SLICECOUNT, ):
        for slice in slices:
            start_cycle = self.read_slice_register(self.registers.START_CYCLE_0, slice=slice)
            value = SNOOPER_ADDR.get(test_name, {}).get('pm0_rm_rm', 0)
            if value > 450:
                value -= 450
            start_cycle.value = value
            self.write_slice_register(start_cycle, slice)
            self.read_slice_register(self.registers.START_CYCLE_0, slice=slice)
            self.start_cycle = value
            start_cycle = self.read_slice_register(self.registers.START_CYCLE_1, slice=slice)
            value = SNOOPER_ADDR.get(test_name, {}).get('rm_pg_rm', 0)
            if value > 450:
                value -= 450
            start_cycle.value = value
            self.write_slice_register(start_cycle, slice)
            self.read_slice_register(self.registers.START_CYCLE_1, slice=slice)

    def get_psdb_aurora_pattern_status(self,psdb):
        aurora_status_register = 'PSDB{}_AURORA_PATTERN_STATUS'.format(psdb)
        aurora_status = self.read_register_raw(getattr(self.registers, aurora_status_register))
        return aurora_status

    def get_aurora_pattern_control_register(self,receiver):
        options = {'PSDB_0_PIN_0': self.registers.PSDB0_AURORA_PATTERN_CONTROL,
                   'PSDB_1_PIN_0': self.registers.PSDB1_AURORA_PATTERN_CONTROL}
        return options[receiver]

    def get_aurora_pattern_status_register(self,receiver):
        options = {'PSDB_0_PIN_0': self.registers.PSDB0_AURORA_PATTERN_STATUS,
                   'PSDB_1_PIN_0': self.registers.PSDB1_AURORA_PATTERN_STATUS}
        return options[receiver]


    def reset_pattern_aurora_link(self, register):
        register.reset = 0x1
        register.clear_error_count = 0x1
        self.write_slice_register(register)
        register.reset = 0x0
        register.clear_error_count = 0x0
        self.write_slice_register(register)

    def disable_pattern_aurora(self,register):
        r= self.read_slice_register(register)
        r.reset = 1
        r.clear_error_count = 1
        r.clear_fifos = 1
        self.write_slice_register(r)

    def enable_pattern_aurora(self, register):
        r = self.read_slice_register(register)
        r.reset = 0
        r.clear_error_count = 0
        r.clear_fifos = 0
        self.write_slice_register(r)

    def clear_aurora_pattern_trigger_error_count(self,register):
        r = self.read_slice_register(register)
        r.clear_error_count = 0x1
        self.write_slice_register(r)
        time.sleep(0.001)
        r.clear_error_count = 0x0
        self.write_slice_register(r)

    def write_register_direct(self,slice,offset,data):
        return hil.hbiMbBarWrite(self.fpga_index, bar=slice+1, offset=offset, data=data)

    def log_last_tx_rx_seen(self, title=''):
        registers = [self.registers.PATGEN_XCVR_LAST_RX_SEEN,
                     self.registers.PSDB0_XCVR_LAST_TX_SEEN,
                     self.registers.PSDB1_XCVR_LAST_TX_SEEN]

    def get_alarm(self, slices, has_failed):
        values = {}
        for slice in slices:
            alarm_object = self.read_slice_register(self.registers.ALARMS, slice)
            values.update({slice: alarm_object})
            if alarm_object.value != 0:
                has_failed.append(alarm_object)
        return {self.name(): {self.registers.ALARMS(0).name(): values}}

    def read_alarms(self, slice):
        alarms = []
        alarm_register = self.read_slice_register(self.registers.ALARMS, slice)
        self.Log('debug', f'{self.name()} slice {slice}: ALARM RAW VALUE: 0x{alarm_register.value:08X}')
        if alarm_register.value != 0:
            for field in alarm_register._fields_:

                field_name = field[0]
                field_value = getattr(alarm_register, field[0])
                if field_value != 0 and 'reserved' not in field_name.lower():
                    alarms.append(field_name + ': ' + str(field_value))
            return ', '.join(alarms)
        return ''

    def log_i2c_alarm(self):
        alarm_register = self.read_slice_register(self.registers.ALARMS)
        self.Log('info', f'I2C_ALARM: {alarm_register.I2c_error_alarm} '
                         f'RAW_ALARM: 0x{alarm_register.value:08X}')

    def write_dctq_sync_delay_mode(self, sync_delay_enable):
        reg = self.registers.ALIGN_ON_TRIGGER_SYNC_DELAY_COMMAND()
        reg.sync_delay_mode = sync_delay_enable
        self.write_slice_register(reg)

    def read_dctq_sync_delay_mode(self):
        reg = self.read_register_raw(self.registers.ALIGN_ON_TRIGGER_SYNC_DELAY_COMMAND)
        return reg

    def read_dctq_engines_processed_count_list(self):
        count_list = []
        for i in range(TQ_ENGINE_COUNT):
            processed_count = self.read_register_raw(self.registers.PROCESSED_TRIGGER_QUEUES_DUT, slice=0, index=i)
            count_list.append(processed_count)
        return count_list

    def reset_dctq_engines_bit(self):
        reset_reg = self.read_dc_trigger_page_2_control()
        self.Log('debug', f'dctq_reset: {reset_reg:01x}')
        self.write_dc_trigger_page_2_control_reset_bit(1)
        reset_reg = self.read_dc_trigger_page_2_control()
        self.Log('debug', f'dctq_reset: {reset_reg:01x}')
        self.write_dc_trigger_page_2_control_reset_bit(0)
        reset_reg = self.read_dc_trigger_page_2_control()
        self.Log('debug', f'dctq_reset: {reset_reg:01x}')

    def reset_dctq_i2c_bit(self):
        self.Log('info', 'i2c reset')
        reset_reg = self.read_dc_trigger_page_2_control()
        self.Log('debug', f'trigger_control: {reset_reg:01x}')
        self.write_dc_trigger_page_2_control_i2c_reset(1)
        reset_reg = self.read_dc_trigger_page_2_control()
        self.Log('debug', f'trigger_control: {reset_reg:01x}')
        self.write_dc_trigger_page_2_control_i2c_reset(0)
        reset_reg = self.read_dc_trigger_page_2_control()
        self.Log('debug', f'trigger_control: {reset_reg:01x}')

    def read_dc_trigger_page_2_control(self):
        return self.read_slice_register(self.registers.DC_TRIGGER_PAGE_2_CONTROL).value

    def write_dc_trigger_page_2_control_reset_bit(self, reset_bit):
        reg = self.read_slice_register(self.registers.DC_TRIGGER_PAGE_2_CONTROL)
        reg.dctq_reset = reset_bit
        self.write_slice_register(reg)

    def write_dc_trigger_page_2_control_i2c_reset(self, reset_bit):
        reg = self.read_slice_register(self.registers.DC_TRIGGER_PAGE_2_CONTROL)
        reg.i2c_reset = reset_bit
        self.write_slice_register(reg)

    def switch_dctq_sync_delay_mode(self, syncdelay_mode):
        mode = self.read_dctq_sync_delay_mode()
        self.Log('debug', f'dctq_sync_delay_mode: {mode:01x}')
        self.write_dctq_sync_delay_mode(syncdelay_mode)
        self.Log('debug', f'dctq_sync_delay_mode setting')
        mode = self.read_dctq_sync_delay_mode()
        self.Log('debug', f'dctq_sync_delay_mode:{mode:01x}')

    def write_dps_fold_trigger_index(self, index):
        reg = self.read_slice_register(self.registers.
                                       DC_TRIGGER_FOLD_EVENT_LUT_INDEX)
        reg.index = index
        self.write_slice_register(reg)

    def take_raw_register_snapshot(self,directory):
        startsaddr=0
        end_addr= 0x2004
        for slice in range(0, 5):
            filename = os.path.join(directory, f'registers_raw_slice_{slice}.csv')
            with open(filename, 'w') as fout:
                writer = csv.writer(fout)
                writer.writerow(["Address","Register value_Decimal","Register value_Hex" ])
                for offset in range(startsaddr,end_addr,4):
                    data = hil.hbiMbBarRead(self.fpga_index, bar=slice + 1, offset=offset)
                    fout.write(
                        '0x{:0x},{},0x{:0x},\n'.format(offset, data,data))

