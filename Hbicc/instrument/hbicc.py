################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time

from Common import hilmon
from Common import configs
from Common.fval.core import Object
from Common.hbi_simulator import HbiSimulator
from Common.instruments.hbi_instrument import HbiInstrument
from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.pinmultiplier import PinMultiplier
from Hbicc.instrument.psdb import Psdb
from Hbicc.instrument.ringmultiplier import RingMultiplier
from Hbicc.testbench.alarm_handler import AlarmHandler
from Hbicc.testbench.dctq_device_setup import TQTestScenarioLTM4678_VCCIO

PIN_MULTIPLIER_COUNT = 4
REST = .001
MB_FAB = {'201': 'B', '202': 'B', '301': 'C', '300': 'C'}
PSDB_FAB = {'200': 'B', '204': 'B', '301': 'C', '302': 'C'}
FAB_COMPATIBILITY: {'0': 'A', '1': 'B', '2': 'C'}
FAB_REV = {'2': 'B', '3': 'C'}
RESET_TIMER = .1


class Hbicc(HbiInstrument):
    def __init__(self, rc=None, name=None, hbidtbs={0: None, 1: None, 2: None, 3: None}):
        super().__init__()
        self.rc = rc
        self.hbidtb0 = hbidtbs[0]
        self.hbidtb1 = hbidtbs[1]
        self.hbidtb2 = hbidtbs[2]
        self.hbidtb3 = hbidtbs[3]
        if rc and hasattr(rc, 'mainboard'):
            self.mainboard = rc.mainboard
        else:
            self.mainboard = None

        if configs.PSDB_0:
            self.psdb_0 = Psdb(0)
        else:
            self.psdb_0 = None
        if configs.PSDB_1:
            self.psdb_1 = Psdb(1)
        else:
            self.psdb_1 = None

        self.model = None
        self.paths = []
        self.track_ila = []
        self.possible_paths = []

    def discover_all_devices(self):
        """
        Verifies HBI MB/PSDB is present by successfully opening and closing a PCI-E device handle
        (``hil.pciDeviceOpen()``) to each FPGA.

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        if self.mainboard or configs.HBICC_IN_HDMT:
            self.pat_gen = PatGen(rc=self.rc).discover()
            self.ring_multiplier = RingMultiplier(rc=self.rc).discover()
        else:
            self.pat_gen = None
            self.ring_multiplier = None

        if self.psdb_0:
            self.psdb_0_pin_0 = PinMultiplier(psdb=self.psdb_0, fpga=0, hbidtb=self.hbidtb0).discover()
            self.psdb_0_pin_1 = PinMultiplier(psdb=self.psdb_0, fpga=1, hbidtb=self.hbidtb1).discover()
        else:
            self.psdb_0_pin_0 = None
            self.psdb_0_pin_1 = None

        if self.psdb_0_pin_0 == self.psdb_0_pin_1 == None:
            self.psdb_0 = None

        if self.psdb_1:
            self.psdb_1_pin_0 = PinMultiplier(psdb=self.psdb_1, fpga=0, hbidtb=self.hbidtb2).discover()
            self.psdb_1_pin_1 = PinMultiplier(psdb=self.psdb_1, fpga=1, hbidtb=self.hbidtb3).discover()
        else:
            self.psdb_1_pin_0 = None
            self.psdb_1_pin_1 = None

        if self.psdb_1_pin_0 == self.psdb_1_pin_1 == None:
            self.psdb_1 = None

        if any(self.devices()):
            self.model = hilmon.monitor
            return self
        return None

    def devices(self):
        possible_devices = [self.pat_gen, self.ring_multiplier,
                            self.psdb_0_pin_0, self.psdb_0_pin_1,
                            self.psdb_1_pin_0, self.psdb_1_pin_1]
        return [x for x in possible_devices if x is not None]

    def devices_active(self):
        devices = []

        for device in [self.pat_gen, self.ring_multiplier,
                       self.psdb_0_pin_0, self.psdb_0_pin_1, self.psdb_1_pin_0, self.psdb_1_pin_1]:
            if device:
                devices.append(device)
        return devices

    def initialize(self):
        self.get_all_possible_paths()
        self.get_execution_path()
        self.log_instrument_blt()
        self.log_fab_version()
        self.vccio_initialize()
        # self.discover_dtb()
        if configs.SKIP_INIT:
            self.Log('warning', f'Skipping {self.name()} instrument initialization')
            return True
        else:
            self.check_transceiver_paths()
            self.set_active_pms_on_tester()
            self.initialize_memory()
            self.set_available_pms_active_on_tester()
            self.ring_multiplier.set_active_psdb()
            self.check_rm_pm_aurora_pattern_lane()
            self.reset_pm_phylite()
        self.calibrate_loopback_timing()

    def calibrate_loopback_timing(self):
        self.Log('info', f'Applying time domain calibration')
        for pm in self.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins()

    def clear_config2(self):
        for pm in self.get_pin_multipliers():
            pm.clear_all_EC_pins_on_pm()

    def set_all_EC_pins(self, first_edge=1, HighCycles=0, LowCycles=0, Halfcycledelay=0, ECIsRatio1=0):
        # Function sets all pins to same value (non EC pins will just have their register ignored.
        for pm in self.get_pin_multipliers():
            pm.set_all_EC_pins_on_pm(first_edge=first_edge, HighCycles=HighCycles, LowCycles=LowCycles,
                                        Halfcycledelay=Halfcycledelay, ECIsRatio1=ECIsRatio1)

    def clear_config3(self):
        for pm in self.get_pin_multipliers():
            pm.clear_all_FRC_pins_on_pm()

    def set_all_FRC_pins(self, first_edge=1, period=2, FRCIsRatio1=0, FRCMode=1):
        # Function sets all pins to same value (non EC pins will just have their register ignored.
        for pm in self.get_pin_multipliers():
            pm.set_all_FRC_pins_on_pm(first_edge=first_edge, period=period, FRCIsRatio1=FRCIsRatio1, FRCMode=FRCMode)

    def vccio_initialize(self, vccio_voltage=1.8):
        object_for_tq = Object()
        tq_ltm4678 = TQTestScenarioLTM4678_VCCIO(self, object_for_tq)
        tq_ltm4678.set_and_check_vccio_voltage(vccio_voltage=vccio_voltage)

    def set_up_fval_alarm_logging(self, slices=None):
        self.hbicc_alarms = AlarmHandler(self, slices)

    def set_available_pms_active_on_tester(self):
        paths = self.get_execution_path()
        for psdb, pin0, pin1 in paths:
            pin0.set_active_pm(slice_list=range(0, 5))
            pin1.set_active_pm(slice_list=range(0, 5))

    def load_instrument_fpga(self):
        """
        Based on FPGA discovery, test loads the FPGA image to Patgen, Ring-Multiplier, and Pin-Multiplier using
        corresponding HIL API.

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        for device in self.devices():
            if device:
                device.load_fpga()

    def set_active_pms_on_tester(self):
        for psdb, pin0, pin1 in self.paths:
            pin0.set_available_pms(slice_list=range(0, 5))
            pin1.set_available_pms(slice_list=range(0, 5))

    def reset_pm_phylite(self):
        self.Log('info', 'Resetting PM Phylite cells...')
        for psdb, pin0, pin1 in self.paths:
            pin0.phylite_reset()
            pin1.phylite_reset()

    def log_instrument_blt(self):
        """
        Logs BLT for HBI MB/PSDB.

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+

        """
        if self.mainboard:
            self.mainboard.log_blt()
        if self.psdb_0:
            self.psdb_0.log_blt()
            if self.psdb_0_pin_0.hbidtb:
                self.psdb_0_pin_0.hbidtb.log_blt()
            if self.psdb_0_pin_1.hbidtb:
                self.psdb_0_pin_1.hbidtb.log_blt()
        if self.psdb_1:
            self.psdb_1.log_blt()
            if self.psdb_1_pin_0.hbidtb:
                self.psdb_1_pin_0.hbidtb.log_blt()
            if self.psdb_1_pin_1.hbidtb:
                self.psdb_1_pin_1.hbidtb.log_blt()

    def log_instrument_cpld(self):
        """
        Logs the CPLD version on HBI MB (``hil.hbiDpsCpldVersionString()``)

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        for device in self.devices():
            if device:
                device.log_cpld()

    def log_instrument_fpga_version(self):
        """
        Based on FPGA discovery, FPGA version for Patgen, Ring Multiplier, and Pin Multiplier is logged
        via a HIL bar-register.

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        for device in self.devices():
            if device:
                device.log_fpga_version()

    def log_instrument_build(self):
        """
        Based on FPGA discovery, test logs mercurial build number for Patgen, Ring Multiplier, Pin Multiplier FPGA
        via Bar-register.

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        for device in self.devices():
            if device:
                device.log_build()

    def initialize_memory(self):
        """
        Initialize DDR memory on the Mainboard and on the Ring Multiplier FGPA.

        **Test:**

        * Write 0x00 all memory addresses (``hil.hbiMbDmaWrite()``)
        * Read back all DDR content and assert all are 0x00

        **Criteria:** Write/Read 0x00

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Failing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        for device in [self.pat_gen, self.ring_multiplier]:
            if device:
                device.check_ddr_calibration_status()
                if not configs.SKIP_MEMORY_INIT:
                    device.initialize_memory_block()
        self.pat_gen.force_ecc_reset()


    def from_i2l(self):
        i2l_version = i2lpatgen.I2LPatgenVersion()
        self.Log('info', 'From I2L... {}'.format(i2l_version))

    def get_execution_path(self):
        if not self.paths:
            if self.mainboard and self.psdb_0 and configs.PSDB_0:
                self.paths.append([0, self.psdb_0_pin_0, self.psdb_0_pin_1])
            if self.mainboard and self.psdb_1 and configs.PSDB_1:
                self.paths.append([1, self.psdb_1_pin_0, self.psdb_1_pin_1])
        return self.paths

    def get_pin_multipliers(self):
        pm_list = []
        if self.psdb_0 and configs.PSDB_0:
            pm_list.extend([self.psdb_0_pin_0, self.psdb_0_pin_1])
        if self.psdb_1 and configs.PSDB_1:
            pm_list.extend([self.psdb_1_pin_0, self.psdb_1_pin_1])
        return pm_list

    def get_all_possible_paths(self):
        if self.psdb_0:
            self.possible_paths.append([0, self.psdb_0_pin_0, self.psdb_0_pin_1])
        if self.psdb_1:
            self.possible_paths.append([1, self.psdb_1_pin_0, self.psdb_1_pin_1])

    def check_transceiver_paths(self):
        paths = self.get_execution_path()
        for psdb, pin0, pin1 in paths:
            self.check_xcvr_path(psdb, pin0, pin1)

    def check_xcvr_path(self, psdb, pin0, pin1):
        links = self.get_links(pin0, pin1)
        self.check_xcvr_links(links, psdb, pin0, pin1)

    def check_xcvr_links(self, links, psdb, pin0, pin1, retries=3):
        for retry in range(retries):
            self.reset_xcvr_links(links)
            self.check_link_communications(links)
            self.prbs_set_up(links)
            statuses = self.report_transceiver_status(psdb, pin0, pin1)
            path_links = self.are_links_up(statuses)
            if not path_links:
                if retry < (retries - 1):
                    self.Log('warning', 'Transceiver Retry...')
            else:
                break

    def get_links(self, pin0, pin1):
        links = [TransceiverLink(self.pat_gen, self.ring_multiplier),
                 TransceiverLink(self.ring_multiplier, self.pat_gen),
                 TransceiverLink(self.ring_multiplier, pin0),
                 TransceiverLink(pin0, self.ring_multiplier),
                 TransceiverLink(pin0, pin1),
                 TransceiverLink(pin1, pin0)]
        return links

    @staticmethod
    def prbs_set_up(links):
        for link in links:
            link.enable_prbs_and_prbs_filter()
            link.disable_prbs_and_prbs_filter()

    def check_link_communications(self, links):
        status = []
        for link in links:
            status.append(link.check_communication())

        if all(status):
            self.Log('info', 'All Xcvr passed with status 0x1c1 before PRBS Set Up')
        else:
            self.Log('info', 'Xcvr check did NOT passed before PRBS Set Up')

    @staticmethod
    def reset_xcvr_links(links):
        for link in links:
            link.reset_tx_rx_link()
        time.sleep(1)

    def reset_xcvr_crc_error(self, pin0, pin1):
        links = self.get_links(pin0, pin1)
        for link in links:
            link.reset_crc_error()

    def log_xcvrs_status(self):
        for psdb, pin0, pin1 in self.possible_paths:
            self.report_transceiver_status(psdb, pin0, pin1)

    def report_transceiver_status(self, psdb, pin0, pin1):
        title = f'{pin0.name()} {pin1.name()} Transceiver Status'
        headers = ['Slice', 'PG_RM', 'RM_PG', 'RM_PM0', 'PM0_RM', 'PM0_PM1', 'PM1_PM0']
        data = []
        if psdb == 0:
            pm0_rm_status_reg = self.ring_multiplier.registers.PSDB0_RING_BUS_STATUS
        else:
            pm0_rm_status_reg = self.ring_multiplier.registers.PSDB1_RING_BUS_STATUS

        for slice in range(0, 5):
            pg_rm = self.ring_multiplier.read_slice_register(self.ring_multiplier.registers.PATGEN_RING_BUS_STATUS,
                                                             slice).value
            rm_pg = self.pat_gen.read_slice_register(self.pat_gen.registers.RM_RING_BUS_STATUS, slice).value

            rm_pm0 = pin0.read_slice_register(pin0.registers.RM_RING_BUS_STATUS, slice).value
            pm0_rm = self.ring_multiplier.read_slice_register(pm0_rm_status_reg, slice).value

            pm0_pm1 = pin1.read_slice_register(pin1.registers.PM_RING_BUS_STATUS, slice).value
            pm1_pm0 = pin0.read_slice_register(pin0.registers.PM_RING_BUS_STATUS, slice).value

            data.append({'Slice': slice, 'PG_RM': f'0x{pg_rm:X}', 'RM_PG': f'0x{rm_pg:X}',
                         'RM_PM0': f'0x{rm_pm0:X}', 'PM0_RM': f'0x{pm0_rm:X}',
                         'PM0_PM1': f'0x{pm0_pm1:X}', 'PM1_PM0': f'0x{pm1_pm0:X}'})

        table = self.contruct_text_table(headers, data, title_in=title)
        self.Log('info', f'{table}')

        return data

    def are_links_up(self, statuses):
        for slice in statuses:
            for link, status in slice.items():
                if link == 'Slice':
                    continue
                if status not in ['0x3E9', '0x1E1']:
                    self.Log('warning', f'Status: {status} != 0x3E9/0x1E1 Expected')
                    break
            else:
                continue
            return False
        return True

    def log_fab_version(self):
        data = []
        title = '-----TESTER FAB CONFIG-----'
        headers = ['Board', 'Fab-Version']
        boards = [self.mainboard, self.psdb_0, self.psdb_1]
        for board in boards:
            if not board:
                continue
            try:
                data.append({'Board': f'{board.name()}', 'Fab-Version': f'{board.blt.fab_letter()}'})
            except RuntimeError:
                self.Log('warning', '{} FAB read was unsuccessful'.format(board.name()))

        table = self.contruct_text_table(headers, data, title_in=title)
        self.Log('info', f'{table}')

    def discover_dtb(self):
        data = []
        for pm in self.get_pin_multipliers():
            data.append(pm.is_dtb_present())
        table = self.contruct_text_table(data=data)
        self.Log('info', table)

    def abort_pattern(self, slices, title=''):
        self.Log('info', f'Aborting {title}')
        self.pat_gen.abort_pattern(slices)
        for psdb, pin0, pin1 in self.get_execution_path():
            pin0.abort_pattern(slices)
            pin1.abort_pattern(slices)

    def get_aurora_links(self, pin0, pin1):
        links = [AuroraPatternLinks(self.ring_multiplier, pin0),
                 AuroraPatternLinks(pin0, self.ring_multiplier),
                 AuroraPatternLinks(pin0, pin1),
                 AuroraPatternLinks(pin1, pin0)]
        return links

    def check_rm_pm_aurora_pattern_lane(self, retry_limit=15):
        paths = self.get_execution_path()
        status = []
        headers = ['TX-Lane', 'TX-status', 'RX-Lane', 'RX-status']
        for psdb, pin0, pin1 in paths:
            links = self.get_aurora_links(pin0, pin1)
            for link in links:
                if link.check_communication():
                    continue
                else:
                    status.append({'TX-Lane': f'{link.tx_device_name}',
                                   'TX-status': f'{hex(link.tx_device.read_slice_register(link.tx_bus_status_register).value)}',
                                   'RX-Lane': f'{link.rx_device_name}',
                                   'RX-status': f'{hex(link.rx_device.read_slice_register(link.rx_bus_status_register).value)}'})
        if status:
            table = self.contruct_text_table(headers, status, title_in='AURORA PATTERN STATUS')
            self.Log('warning', f'{table}')
        else:
            self.Log('info', 'PATTERN AURORA LANES UP')

    def check_pin_states(self, slices, user_mode=None):
        pin_state_checker = PinStateChecker(self)
        if user_mode:
            if user_mode != 'RELEASE':
                pin_state_checker.user_mode_pin_state = user_mode
                pin_state_checker.user_mode_pin_state_checker(slices)
        else:
            pin_state_checker.run_pin_state_checker(slices)

    def get_pin_state_checker(self):
        return PinStateChecker(self)

    def expect_list_ini(self, pin_state_checker, psdb):
        pin_state_checker.expect_set_ini(psdb)

    def check_all_pin_states(self, pin_state_checker, slices_channel_sets_combo, psdb, check_mode=True):
        pin_state_checker.user_mode_all_pin_state_checker(slices_channel_sets_combo, psdb, check_mode)

    def check_pin_version(self):
        reg = self.psdb_0_pin_0.registers.FPGA_VERSION
        psdb0_pin0 = self.psdb_0_pin_0.read_slice_register(reg).value
        reg = self.psdb_0_pin_1.registers.FPGA_VERSION
        psdb0_pin1 = self.psdb_0_pin_1.read_slice_register(reg).value
        reg = self.psdb_1_pin_0.registers.FPGA_VERSION
        psdb1_pin0 = self.psdb_1_pin_0.read_slice_register(reg).value
        reg = self.psdb_1_pin_1.registers.FPGA_VERSION
        psdb1_pin1 = self.psdb_1_pin_1.read_slice_register(reg).value
        return [hex(psdb0_pin0), hex(psdb0_pin1), hex(psdb1_pin0), hex(psdb1_pin1)]

    def check_rm_all_channel_set_pin_control(self):
        channel_set_list = range(16)
        slice_list = range(5)
        pin_list = range(35)
        pin_control_dic = {}
        for channel_set in channel_set_list:
            channel_set_pin_control_register = 'PIN_STATE_CONTROL_WORD_CHANNEL_SET_{}'.format(channel_set)
            regs = self.ring_multiplier.registers
            reg = getattr(regs, channel_set_pin_control_register)
            pin_control_one_channel_set_dic = {}
            for slice in slice_list:
                pin_control_15_0 = self.ring_multiplier.read_slice_register(reg, slice, 0).value
                pin_control_31_16 = self.ring_multiplier.read_slice_register(reg, slice, 1).value
                pin_control_34_32 = self.ring_multiplier.read_slice_register(reg, slice, 2).value
                pin_control_34_0 = (pin_control_34_32 << 64) + (pin_control_31_16 << 32) + pin_control_15_0
                pin_control_one_channel_set_slice_dic = {}
                for pin in pin_list:
                    pin_control = pin_control_34_0 & 3
                    pin_control_34_0 = pin_control_34_0 >> 2
                    pin_control_one_channel_set_slice_dic.update({pin: pin_control})
                pin_control_one_channel_set_dic.update({slice: pin_control_one_channel_set_slice_dic})
            pin_control_dic.update({channel_set: pin_control_one_channel_set_dic})
        return pin_control_dic


class TransceiverLink:
    def __init__(self, tx_device, rx_device):
        self.tx_device = tx_device
        self.rx_device = rx_device
        self.tx_device_name = tx_device.name()
        self.rx_device_name = rx_device.name()

        self.tx_control_register = tx_device.get_control_register(self.rx_device_name)
        self.tx_bus_status_register = tx_device.get_status_register(self.rx_device_name)

        self.rx_control_register = rx_device.get_control_register(self.tx_device_name)
        self.rx_bus_status_register = rx_device.get_status_register(self.tx_device_name)
        self.rx_error_count_register = rx_device.get_error_count_register(self.tx_device_name)
        self.rx_xcvr_error_count_lane = rx_device.get_error_count_per_lane_register(self.tx_device_name)

        self.pattern_count_transferred = rx_device.get_transferred_pattern_count(self.tx_device_name)

        self.retry_limit = 100
        self.tx_slice_list = None
        self.rx_slice_list = None
        self.tx_lanes_per_slice = None
        self.rx_lanes_per_slice = None

    def setup_communication_link(self):
        if self.check_communication():
            self.enable_prbs_and_prbs_filter()

        else:
            return False

    def enable_prbs_and_prbs_filter(self):
        tx_reg = self.tx_device.read_slice_register(self.tx_control_register)
        rx_reg = self.rx_device.read_slice_register(self.rx_control_register)
        tx_reg.PRBSFilter = 0x1
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx_reg, slice)

        time.sleep(REST)
        rx_reg.PRBSFilter = 0x1
        for slice in range(0, 5):
            self.rx_device.write_slice_register(rx_reg, slice)
        time.sleep(REST)

        tx_reg.EnablePrbs = 0x1
        tx_reg.PRBSFilter = 0x1
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx_reg, slice)
        time.sleep(REST)

    def enable_prbs(self):
        tx = self.tx_control_register()
        rx = self.rx_control_register()
        rx.PRBSFilter = 0x1
        for slice in range(0, 5):
            self.rx_device.write_slice_register(rx, slice)
        tx.EnablePrbs = 0x1
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx, slice)
        time.sleep(REST)

    def disable_prbs_and_prbs_filter(self):
        tx = self.tx_control_register()
        rx = self.rx_control_register()
        tx.EnablePrbs = 0x0
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx, slice)
        time.sleep(REST)
        tx.PRBSFilter = 0x0
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx, slice)
        time.sleep(REST)

        rx.PRBSFilter = 0x0
        for slice in range(0, 5):
            self.rx_device.write_slice_register(rx, slice)
        time.sleep(REST)

    def reset_tx_rx_link(self, slice=None):
        self.tx_device.reset_link(self.tx_control_register(), slice)
        self.rx_device.reset_link(self.rx_control_register(), slice)
        self.rx_device.reset_error_count(self.rx_control_register(), slice)

    def check_communication(self, retry_limit=0xFF, slice_list=range(0, 5)):
        failed = []
        for slice in slice_list:
            for i in range(retry_limit):
                if self.is_lane_status_up(slice):
                    break
                else:
                    self.reset_tx_rx_link([slice])
                    time.sleep(RESET_TIMER)
            else:
                failed.append(slice)

        if failed:
            self.tx_device.Log('error', '{} Maximum number ({}) of retries exceeded for slice {} reset'.format(
                self.link_name(),
                self.retry_limit,
                slice_list))
            return False
        return True

    def is_lane_status_up(self, slice):
        tx_bus_status_register_value = self.tx_device.read_slice_register(self.tx_bus_status_register, slice)
        rx_bus_status_register_value = self.rx_device.read_slice_register(self.rx_bus_status_register, slice)

        tx_lanealigned = tx_bus_status_register_value.LaneAligned
        rx_lanealigned = rx_bus_status_register_value.LaneAligned
        tx_word_aligned = tx_bus_status_register_value.WordAligned
        rx_word_aligned = rx_bus_status_register_value.WordAligned
        tx_error_deskew = tx_bus_status_register_value.ErrorDeskew
        rx_error_deskew = rx_bus_status_register_value.ErrorDeskew

        tx_decoder_error = tx_bus_status_register_value.DecoderError
        rx_decoder_error = rx_bus_status_register_value.DecoderError

        if tx_lanealigned == 0x1 and rx_lanealigned == 0x1:
            word_aligned = tx_word_aligned == rx_word_aligned == 0x1
            error_deskew = tx_error_deskew == rx_error_deskew == 0x0
            decoder_error = tx_decoder_error == rx_decoder_error == 0x0
            if all([word_aligned, error_deskew, decoder_error]):
                return True
        else:
            return False

    def link_name(self, bidirectional=False):
        if bidirectional:
            return '{} <---> {}'.format(self.tx_device.name(), self.rx_device.name())
        else:
            return '{} ---> {}'.format(self.tx_device.name(), self.rx_device.name())

    def is_prbs_set_up(self):
        expected = 0x1
        for slice in range(0, 5):
            tx_bus_status_register_value = self.tx_device.read_slice_register(self.tx_bus_status_register, slice)
            rx_bus_status_register_value = self.rx_device.read_slice_register(self.rx_bus_status_register, slice)

            tx_register_name = tx_bus_status_register_value.__class__.__name__.upper()
            rx_register_name = rx_bus_status_register_value.__class__.__name__.upper()

            tx_pll_locked = tx_bus_status_register_value.PllLocked
            tx_lanealigned = tx_bus_status_register_value.LaneAligned
            tx_freqlocked = tx_bus_status_register_value.RxFreqLocked

            rx_linkup = rx_bus_status_register_value.LinkUp
            rx_wordaligned = rx_bus_status_register_value.WordAligned
            rx_prbs_locked = rx_bus_status_register_value.PrbsLocked
            rx_prbs_success = rx_bus_status_register_value.PrbsSuccess

            if not self.tx_device.is_register_set(slice, tx_pll_locked, expected, tx_register_name,
                                                  'PllLocked'):
                break
            if not self.rx_device.is_register_set(slice, rx_linkup, expected, rx_register_name, 'LinkUp'):
                break
            if not self.tx_device.is_register_set(slice, tx_freqlocked, expected, tx_register_name,
                                                  'RxFreqLocked'):
                break
            if not self.rx_device.is_register_set(slice, rx_wordaligned, expected, rx_register_name,
                                                  'WordAligned'):
                break
            if not self.tx_device.is_register_set(slice, tx_lanealigned, expected, tx_register_name,
                                                  'LaneAligned'):
                break
            if not self.rx_device.is_register_set(slice, rx_prbs_locked, expected, rx_register_name,
                                                  'PrbsLocked'):
                break
            if not self.rx_device.is_register_set(slice, rx_prbs_success, expected, rx_register_name,
                                                  'PrbsSuccess'):
                break
        else:
            return True

    def reset_crc_error(self):
        tx_reg = self.tx_device.read_slice_register(self.tx_control_register)
        rx_reg = self.rx_device.read_slice_register(self.rx_control_register)
        tx_reg.Reset_CRC_Error = 0x1
        rx_reg.Reset_CRC_Error = 0x1
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx_reg, slice)
            self.rx_device.write_slice_register(rx_reg, slice)

        time.sleep(REST)
        tx_reg.Reset_CRC_Error = 0x0
        rx_reg.Reset_CRC_Error = 0x0
        for slice in range(0, 5):
            self.tx_device.write_slice_register(tx_reg, slice)
            self.rx_device.write_slice_register(rx_reg, slice)
        time.sleep(REST)


class AuroraPatternLinks():
    def __init__(self, tx_device, rx_device):
        self.tx_device = tx_device
        self.rx_device = rx_device
        self.tx_device_name = tx_device.name()
        self.rx_device_name = rx_device.name()

        self.tx_control_register = tx_device.get_aurora_pattern_control_register(self.rx_device_name)
        self.tx_bus_status_register = tx_device.get_aurora_pattern_status_register(self.rx_device_name)

        self.rx_control_register = rx_device.get_aurora_pattern_control_register(self.tx_device_name)
        self.rx_bus_status_register = rx_device.get_aurora_pattern_status_register(self.tx_device_name)

        self.retry_limit = 15
        self.tx_slice_list = None
        self.rx_slice_list = None
        self.tx_lanes_per_slice = None
        self.rx_lanes_per_slice = None

    def disable_enable_pattern_aurora_lane(self):
        self.tx_device.disable_pattern_aurora(self.tx_control_register)
        self.tx_device.enable_pattern_aurora(self.tx_control_register)
        self.rx_device.disable_pattern_aurora(self.rx_control_register)
        self.rx_device.enable_pattern_aurora(self.rx_control_register)
        time.sleep(0.5)

    def clear_error_count(self):
        self.tx_device.clear_aurora_pattern_trigger_error_count(self.tx_control_register)
        self.rx_device.clear_aurora_pattern_trigger_error_count(self.rx_control_register)
        time.sleep(0.5)

    def is_lane_status_up(self):
        tx_bus_status_register_value = self.tx_device.read_slice_register(self.tx_bus_status_register).value
        rx_bus_status_register_value = self.rx_device.read_slice_register(self.rx_bus_status_register).value

        if tx_bus_status_register_value == rx_bus_status_register_value == 0x27:
            return True
        else:
            return False

    def check_communication(self):
        for i in range(self.retry_limit):
            if self.is_lane_status_up():
                return True
            else:
                self.disable_enable_pattern_aurora_lane()
                self.clear_error_count()
        else:
            return False

class PinStateChecker(HbiInstrument):
    def __init__(self, hbicc):
        self.pms = hbicc.get_pin_multipliers()
        self.fab = hbicc.psdb_0.blt.fab_letter()
        self._failures = None
        self._expected = {}
        self.user_mode_pin_state = None

    def run_pin_state_checker(self, slices):
        self._slices = slices
        self._reset()
        self._set_expected()
        for pm in self.pms:
            self._pm = pm
            self._run_checker_on_pm()
        self._log_failures()

    def _reset(self):
        self._failures = None

    # ToDo: Temporary hard coded values. Randomization will follow.
    def _set_expected(self):
        for slice in self._slices:
            entry = {}
            for channel_set in slice.channel_sets:
                entry.update({channel_set: {}})
                if channel_set in [0, 1, 2, 3, 8, 9, 10, 11]:
                    for i in range(35):
                        entry[channel_set].update({i: 1})
                else:
                    for i in range(35):
                        entry[channel_set].update({i: 0})
            self._expected.update({slice.index: entry})

    def _run_checker_on_pm(self):
        for slice in self._slices:
            self._current_slice = slice
            self._check_pin_state()

    def _run_readback_on_pm(self, psdb):
        for slice in range(5):
            self._current_slice = slice
            self._readback_pin_state(slice, psdb)

    def _readback_pin_state(self, slice, psdb):
        if psdb:
            channel_sets_list = [8, 9, 10, 11, 12, 13, 14, 15]
        else:
            channel_sets_list = [0, 1, 2, 3, 4, 5, 6, 7]
        for channel_set in channel_sets_list:
            if not self._is_channel_set_in_pm(channel_set):
                continue
            self._set_rebase_channel_set(channel_set)
            self._store_readback_pin_state(slice, channel_set)

    def _store_readback_pin_state(self, slice, channel_set):
        raw_state_channel_set_register = 'RAW_STATE_CHANNEL_SET_{}'.format(self._current_channel_set)
        regs = self._pm.registers
        reg = getattr(regs, raw_state_channel_set_register)
        pin_state_for32_pins = self._pm.read_slice_register(reg, slice, index=0).value
        pin_state_for3_pins = self._pm.read_slice_register(reg, slice, index=1).value
        self._current_channel_set_state = (pin_state_for3_pins << 32) | pin_state_for32_pins
        for i in range(35):
            state = (self._current_channel_set_state >> i) & 0x1
            self._readback[slice][channel_set].update({i: state})

    def _check_pin_state(self):
        for channel_set in self._current_slice.channel_sets:
            if not self._is_channel_set_in_pm(channel_set):
                continue
            self._set_rebase_channel_set(channel_set)
            self._record_pin_states()
            self._dispatch_each_pin()

    def _is_channel_set_in_pm(self, channel_set):
        if self._pm.slot_index % 4 == channel_set // 4:
            return True
        return False

    def _set_rebase_channel_set(self, channel_set):
        self._current_channel_set = channel_set % 4

    def _dispatch_each_pin(self):
        channel_set = self._current_channel_set + self._pm.slot_index * 4
        for channel in range(35):
            if self._is_used(channel):
                self._disposition_state(channel, channel_set)

    def _is_used(self, channel):
        if channel not in self._pm.PHYLITE_PIN_IGNORE_LIST[self.fab][self._current_channel_set][
            self._current_slice.index]:
            return True
        return False

    def _disposition_state(self, channel, channel_set):
        state = (self._current_channel_set_state >> channel) & 1
        expected = self._expected[self._current_slice.index][channel_set][channel]
        if state != expected:
            self._record_failure(channel, channel_set, expected, state)

    def _record_failure(self, channel, channel_set, expected, state):
        self._set_failure_dict()
        if expected == 1 and state == 0:
            self._failures[self._current_slice.index][channel_set]['H2L'].append(channel)
        elif expected == 0 and state == 1:
            self._failures[self._current_slice.index][channel_set]['L2H'].append(channel)
        else:
            self.Log('error', f'Unhandle Pin State Error Type')

    def _set_failure_dict(self):
        if self._failures:
            return
        self._failures = {slice: {channel_set: {'H2L': [], 'L2H': []} for channel_set in range(16)} for slice in
                          range(0, 5)}

    def _record_pin_states(self):
        raw_state_channel_set_register = 'RAW_STATE_CHANNEL_SET_{}'.format(self._current_channel_set)
        regs = self._pm.registers
        reg = getattr(regs, raw_state_channel_set_register)
        pin_state_for32_pins = self._pm.read_slice_register(reg, slice=self._current_slice.index, index=0).value
        pin_state_for3_pins = self._pm.read_slice_register(reg, slice=self._current_slice.index, index=1).value
        self._current_channel_set_state = (pin_state_for3_pins << 32) | pin_state_for32_pins

    def _log_failures(self):
        if self._failures:
            data = []
            for slice_index, channel_set_values in self._failures.items():
                for channel_set, results in channel_set_values.items():
                    H2L = self._get_channel_set_results(results, 'H2L')
                    L2H = self._get_channel_set_results(results, 'L2H')
                    if H2L or L2H:
                        data.append({'Slice': slice_index, 'CHANNEL SET': channel_set, 'H2L': H2L, 'L2H': L2H})

            table = self.contruct_text_table(data=data, title_in='Pin State Checker Results (H=High, L=Low)')
            self.Log('error', f'{table}')

    def _get_channel_set_results(self, results, error_type):
        result = ''
        if len(results[error_type]):
            result = ','.join([str(i) for i in results[error_type]])
        return result

    def user_mode_pin_state_checker(self, slices):
        self._slices = slices
        self._reset()
        self._custom_set_expected()
        for pm in self.pms:
            self._pm = pm
            self._run_checker_on_pm()
        self._log_failures()

    def user_mode_all_pin_state_checker(self, slices_channel_sets_combo, psdb, xor_mode=True):
        self._slices_channel_sets_combo = slices_channel_sets_combo
        self._reset()
        if xor_mode:
            self._all_set_expected()
        self._readback_set_ini(psdb)
        for pm in self.pms:
            self._pm = pm
            self._run_readback_on_pm(psdb)
        self._log_result_xor(psdb, xor_mode)

    def _log_result_xor(self, psdb, xor_mode=True):
        if psdb:
            channel_set_list = [8, 9, 10, 11, 12, 13, 14, 15]
        else:
            channel_set_list = [0, 1, 2, 3, 4, 5, 6, 7]
        for slice in range(5):
            for channel_set in channel_set_list:
                error = 0
                if slice == 4:
                    for pin in range(22):
                        if (self._expected[slice][channel_set][pin] != self._readback[slice][channel_set][pin]):
                            error = error + 1
                else:
                    for pin in range(35):
                        if (self._expected[slice][channel_set][pin] != self._readback[slice][channel_set][pin]):
                            error = error + 1
                if (error > 0) & xor_mode:
                    self.Log('error', f's{slice}d{channel_set}E: {self._expected[slice][channel_set]}')
                    self.Log('info', f's{slice}d{channel_set}R: {self._readback[slice][channel_set]}')

    def _readback_set_ini(self, psdb):
        if psdb:
            channel_set_list = [8, 9, 10, 11, 12, 13, 14, 15]
        else:
            channel_set_list = [0, 1, 2, 3, 4, 5, 6, 7]
        self._readback = {}
        for slice in range(5):
            one_slice_list = {}
            for channel_set in channel_set_list:
                one_channel_set_list = {}
                for pin in range(35):
                    one_channel_set_list.update({pin: 0})
                one_slice_list.update({channel_set: one_channel_set_list})
            self._readback.update({slice: one_slice_list})

    def expect_set_ini(self, psdb):
        self._expected = {}
        if psdb:
            channel_set_list = [8, 9, 10, 11, 12, 13, 14, 15]
        else:
            channel_set_list = [0, 1, 2, 3, 4, 5, 6, 7]
        for slice in range(5):
            one_slice_list = {}
            for channel_set in channel_set_list:
                one_channel_set_list = {}
                for pin in range(35):
                    one_channel_set_list.update({pin: 0})
                one_slice_list.update({channel_set: one_channel_set_list})
            self._expected.update({slice: one_slice_list})

    def _all_set_expected(self):
        for slice in self._slices_channel_sets_combo:
            for channel_set in self._slices_channel_sets_combo[slice].channel_sets:
                self._expected[slice].update(
                    {channel_set: self._slices_channel_sets_combo[slice].pin_states[channel_set]})

    def _custom_set_expected(self):
        for slice in self._slices:
            entry = {}
            for channel_set in slice.channel_sets:
                entry.update({channel_set: {}})
                for i in range(35):
                    entry[channel_set].update({i: slice.pattern_helper.fix_drive_state.get_state_by_pins(channel_set,
                                                                                                          slice.index,
                                                                                                          i)})
                self._expected.update({slice.index: entry})

