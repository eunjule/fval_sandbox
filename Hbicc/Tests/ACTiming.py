# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""
AC Timing delay tests primarily test timing and the delay stack by tuning the config1 register in the pin multiplier.

Tests include delay stack modifications of the compare delay and the drive delay
Requirements:
dtb - Dynamic phase delay tests are loopback tests and so will only
    pass if the dtb is installed
"""
import unittest
import random
from Common.fval import skip
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.Tests.HbiccTest import HbiccTest
from Common.fval import skip, expected_failure

compare_shifted_two_tester_cycles_even_slice = [0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                                                0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
compare_shifted_two_tester_cycles_odd_slice = [0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                                               0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]


class ACTimingBase(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.user_mode = 'RELEASE'
        self.slices = [random.randint(0, 4)]

    def DebugShmooDelayTest(self):
        """Debug test that is used to determine time domain corrections"""
        period = 50e-9
        self.channel_sets = set(range(16))
        self.slices = [0]
        self.ctp = 0b0
        sweep_length = 48
        shmooresults = [0] * sweep_length
        repeat = 1024
        pin_set = range(1, 34, 2)
        fixed_drive_state = 'LOW'
        # self.pattern_helper.user_mode = 'RELEASE'
        delays = self.init_delay_structure()
        pattern_string = self.psuedo_random_pattern()
        self.Log('info', pattern_string)
        log_ind = 0
        base_delay = 48
        for index in range(base_delay, base_delay + sweep_length):
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices)
            for pm in self.env.hbicc.get_pin_multipliers():
                pm.set_delay_stack_on_all_pins(RXdelay=index)
            self.pattern_helper.user_mode = 'RELEASE'
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)

            self.pattern_helper.execute_pattern_scenario()
            self.check_delay_results(index, delays, pin_set)
            log_ind = log_ind + 1
            self.pattern_helper.user_mode = 'LOW'
        self.save_delay_structure(delays)

    def init_delay_structure(self):
        delays = []
        for dut in self.channel_sets:
            slicedelays = []
            for slice in range(5):  # self.slices:
                dict = []
                for channel in range(35):
                    dict.append(0)
                slicedelays.append(dict)
            delays.append(slicedelays)
        return delays

    def save_delay_structure(self, delays, file='..\..\Tools\TimeDomainCal.csv'):
        with open('..\..\Tools\TimeDomainCal.csv', mode='w') as csv_file:
            csv_file.write('PM,Dut,Slice,pin,RxDelay\n')
            for dut in self.channel_sets:
                for slice in self.slices:
                    for channel in range(35):
                        PM = dut // 4
                        PM_dut = dut % 4
                        delay = delays[dut][slice][channel]
                        csv_file.write(f'{PM},{PM_dut},{slice},{channel},{delay}\n')
            csv_file.close()

    def check_delay_results(self, index, delays, pin_set):
        for slice_index, slice_obj in self.pattern_helper.slice_channel_sets_combo.items():
            for pm in self.pattern_helper.hbicc.get_pin_multipliers():
                active_duts = list(set(pm.channel_sets()) & set(self.channel_sets))
                for dut in active_duts:
                    pm_dut = dut % 4
                    error_counts = pm.channel_error_counts(pm_dut, slice_index)
                    for pin in pin_set:
                        value = error_counts[pin]
                        if (value != 'PhyLite') & (int(value) == 0):
                            add_delay = delays[dut][slice_index][pin]
                            if add_delay == 0:
                                delays[dut][slice_index][pin] = index + 3

    def execute_scenario(self, pattern_string,  subcycle_exp=0):
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.set_pm_subcycle(subcycle_exp, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def assign_random_pins(self):
        num_of_pairs = 4
        pinlist = random.sample(range(1, 34, 2), num_of_pairs)
        loopback_pin_delta = -1
        if self.slices[0] % 2:
            loopback_pin_delta = 1
        loopback_pins = [x + loopback_pin_delta for x in pinlist]
        for x in loopback_pins:
            pinlist.append(x)
        return pinlist

    def set_all_delays(self, RxDelay=67, TxDelay=0):
        for pm in self.pattern_helper.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RxDelay, TxDelay)

    def set_delays(self, pinlist, RxDelay=67, TxDelay=0):
        for pm in self.pattern_helper.hbicc.get_pin_multipliers():
            for channel in pinlist:
                for slice in self.slices:
                    pm.set_delay_stack_on_single_pin(slice, channel, RxDelay, TxDelay)

    def check_pin_settings(self, pinlist, RxDelay, TxDelay, base_rx=67, base_tx=0):
        config1reg_modified = self.calculate_register(RxDelay, TxDelay)
        config1reg_base = self.calculate_register(base_rx, base_tx)
        for slice in self.slices:
            for pin in range(4 * 35):
                for pm in self.pattern_helper.env.hbicc.get_pin_multipliers():
                    config1reg_actual = pm.read_slice_register(pm.registers.PER_CHANNEL_CONFIG1, slice, pin)
                    if pin in pinlist:
                        if config1reg_actual.value != config1reg_modified.value:
                            self.Log('error', f'Time delay config mismatch in {pin} on {slice} in {pm.name()}')
                            self.Log('error', f'Expected: {config1reg_modified}, Actual: {config1reg_actual}')
                    else:
                        if config1reg_actual.value != config1reg_base.value:
                            self.Log('error', f'Time delay config mismatch in {pin} on {slice} in {pm.name()}')
                            self.Log('error', f'Expected: {config1reg_base}, Actual: {config1reg_actual}')

    def calculate_register(self, RxDelay, TxDelay):
        RXsubcycle = RxDelay % 8
        RXcycle = RxDelay // 8
        config1reg = self.pattern_helper.env.hbicc.psdb_0_pin_0.registers.PER_CHANNEL_CONFIG1()
        config1reg.SubCycleDelayRxFromDut = RXsubcycle
        config1reg.TesterCycleDelayRxFromDut = RXcycle
        config1reg.TotalCycleDelayTxToDut = TxDelay
        return config1reg

    def walking_ones_pattern(self):
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                        PatternId {id_0}
                        PCall PATTERN1
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                        DriveZeroVectors length=1

                        PATTERN1:
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                        DriveZeroVectors length=500 
                        V ctv=0, mtv=0, lrpt=0, data=0v1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0 
                        V ctv=0, mtv=0, lrpt=0, data=0v0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1L0
                        V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L1
                        DriveZeroVectors length=504
                        Return
                          '''
        return pattern_string

    def psuedo_random_pattern(self):
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b0 #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=1000
                %repeat 16
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                     V ctv=0, mtv=0, lrpt=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                %end

                DriveHighVectors length=1000

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1
                '''
        return pattern_string

    def psuedo_random_pattern_odd_drive(self):
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b0 #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=1000
                %repeat 16
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v00L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L00
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                     V ctv=0, mtv=0, lrpt=0, data=0v11H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H11
                %end

                DriveHighVectors length=1000

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1
                '''
        return pattern_string


class Functional(ACTimingBase):
    """
    Functional tests for AC Timing
    """


    def MiniDriveDelayTest(self):
        """
         Tests shift in the drive delay portion of the delay stack

         First pass: Sets TX delay value to be two full tester cycles away from tdr calibrated value of 0.  Run pattern and compare
            to expected fail signature (different than simulator)
         Second pass: Sets RX celay value back to tdr calibrated value and rerun pattern. Expected fail signature will
            match the simulator response.
         """

        self.pattern_helper.user_mode = 'RELEASE'
        self.slices = [random.randint(0, 4)]
        if (self.slices[0] % 2):
            compare_shifted_two_tester_cycles = compare_shifted_two_tester_cycles_odd_slice
        else:
            compare_shifted_two_tester_cycles = compare_shifted_two_tester_cycles_even_slice
        self.set_all_delays(RxDelay=67, TxDelay=16)
        pattern_string = self.walking_ones_pattern()
        self.execute_scenario(pattern_string)
        self.set_all_delays(RxDelay=67, TxDelay=0)
        self.execute_scenario(pattern_string)

    def MiniCompareDelayTest(self):
        """
        Tests shift in the compare delay portion of the delay stack

        First pass: Sets RX delay value to be two full tester cycles away from tdr calibrated value of 67.  Run pattern and compare
           to expected fail signature (different than simulator)
        Second pass: Sets RX celay value back to tdr calibrated value and rerun pattern. Expected fail signature will
           match the simulator response.
        """

        if (self.slices[0] % 2):
            compare_shifted_two_tester_cycles = compare_shifted_two_tester_cycles_odd_slice
        else:
            compare_shifted_two_tester_cycles = compare_shifted_two_tester_cycles_even_slice
        pattern_string = self.walking_ones_pattern()
        self.set_all_delays(RxDelay=51)
        self.execute_scenario(pattern_string)
        self.set_all_delays(RxDelay=67)
        self.execute_scenario(pattern_string)

    def DirectedAllPinShmooTest(self):
        """
        Tests shift both the compare and drive delay stacks so that the same relative delay is maintained

        Test will pass if there are no failures in pattern execution and read of pin multipler registers
        expected values
        """
        pattern_string = self.psuedo_random_pattern()
        for ind in range(67, 255, 15):
            RxDelay = ind
            TxDelay = ind - 67
            with self.subTest(Delays=f'Rx {RxDelay} TxDelay {TxDelay}'):
                self.set_all_delays(RxDelay=RxDelay, TxDelay=TxDelay)
                self.execute_scenario(pattern_string)
                self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False
        self.set_all_delays(RxDelay=67, TxDelay=0)

    def RandomPinSubsetShmooTest(self):
        """
        Tests shift both the compare and drive delay stacks so that the same relative delay is maintained

        Only a subset of pins will be shifted while other pins will remain at same value.
        Test will pass if there are no failures in pattern execution and read of pin multipler registers
        expected values.
        """
        pattern_string = self.psuedo_random_pattern()
        pinlist = self.assign_random_pins()
        for ind in range(67, 230, 15):
            RxDelay = ind
            TxDelay = ind - 67
            self.set_delays(pinlist, RxDelay=RxDelay, TxDelay=TxDelay)
            self.execute_scenario(pattern_string)
            self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False
        self.check_pin_settings(pinlist, RxDelay, TxDelay, base_rx=67, base_tx=0)
        self.set_all_delays(RxDelay=67, TxDelay=0)

    def DirectedAllPinShmooAt25MHzTest(self):
        """
        Tests shift both the compare and drive delay stacks so that the same relative delay is maintained

        Test pattern will run at a frequency of 25 MHz.  Test will pass if there are no failures in
        pattern execution and read of pin multipler registers expected values
        """
        twenty_five_mhz_exp = 3
        rx_delay_at_25MHz = 11
        max_rx_delay = 255
        two_tester_cycle_delta = 16
        pattern_string = self.psuedo_random_pattern()
        for ind in range(rx_delay_at_25MHz, max_rx_delay, two_tester_cycle_delta):
            RxDelay = ind
            TxDelay = (2 ** twenty_five_mhz_exp) * (ind - rx_delay_at_25MHz)
            self.set_all_delays(RxDelay=RxDelay, TxDelay=TxDelay)
            self.execute_scenario(pattern_string, subcycle_exp=twenty_five_mhz_exp)
            self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False
        self.set_all_delays(RxDelay=67, TxDelay=0)



class CycleSlip(ACTimingBase):

    def DirectedHdmt106897Test(self):
        """
        Directed HDMT 106897 Cycle Slip Test

        Issue:  At run time some channels may slip drive edge placement by ~4ns to DUT input.
        Occurrence/recovery is random.  Needs FPGA reflash to recover. Could result in extra rejects

        Root cause:  Unknown.  It is known that design violates PSG jitter spec for the IO PLL of <120ps (@200ps currently).
        Per PSG, this spec was a historical cut&paste.
        HDMT Ticket 106897:HBICC Patgen: Timing delay stack values not properly applied to all channels.

        Test Method:

        At 3 different subcycles, execute pattern with drive on odd pins and pattern with drive on even pins
        at least 3 different bursts per pattern.

        Limitations:
        Due to cross slice loopback, there will be a small subset of pins that cannot be covered.
        """
        self.slices = range(0, 5)

        even_drive_pattern_string=self.psuedo_random_pattern()
        self.execute_timing_delay_scenario(even_drive_pattern_string, name='Even Drive')

        odd_drive_pattern_string = self.psuedo_random_pattern_odd_drive()
        self.execute_timing_delay_scenario(odd_drive_pattern_string, name='Odd Drive')

        self.set_all_delays(RxDelay=67, TxDelay=0)

    def execute_timing_delay_scenario(self, pattern_string, name=''):
        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False
        for rxdelay in range(65, 70, 2):
            with self.subTest(Running=f'{name} Rx: {rxdelay}, Tx: 0'):
                self.set_all_delays(RxDelay=rxdelay)
                for repeat in range(3):
                    self.execute_scenario(pattern_string)
                    self.env.hbicc.model.patgen.skip_multi_burst_simulation = True
                    self.pattern_helper._is_set_and_check_pin_state = False
        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False