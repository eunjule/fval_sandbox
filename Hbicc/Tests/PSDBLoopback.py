# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
import time
import unittest

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Common.fval import skip,SkipTest

SETTLING_TIME = 0.01
VCCIO_IRANGE = range(180, 112, -17)
VREF_IRANGE = range(98, 79, -6)

class Diagnostics(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.slices = range(5)

    def DirectedDCLoopbackPSDB0Pin0Test(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH_LOOPBACK'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedDCLoopbackPSDB0Pin1Test(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_1 (2nd 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_1 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH_LOOPBACK'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_1, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedDCLoopbackPSDB1Pin0Test(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_1_Pin_0 (3rd 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_1_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH_LOOPBACK'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedDCLoopbackPSDB1Pin1Test(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_1_Pin_1 (last 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_1_Pin_1 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH_LOOPBACK'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_1, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedDCLoopbackPSDB0Pin0Slice0WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x0000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin0Slice1WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x1000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin0Slice2WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x2000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin0Slice3WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x3000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin0Slice4WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x4000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin1Slice0WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x0000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin1Slice1WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x1000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin1Slice2WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x2000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin1Slice3WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x3000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB0Pin1Slice4WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')

        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 0
            fixed_drive_state = base_fixed_drive_state + 0x4000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin0Slice0WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x0000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin0Slice1WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x1000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin0Slice2WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x2000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin0Slice3WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x3000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin0Slice4WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x4000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin1Slice0WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x0000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin1Slice1WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x1000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin1Slice2WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x2000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin1Slice3WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000024
        for pin in range(17):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x3000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16

    def DirectedDCLoopbackPSDB1Pin1Slice4WalkingPinsTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        base_fixed_drive_state = 0x0000000000000000009
        for pin in range(18):
            psdb_target = 1
            fixed_drive_state = base_fixed_drive_state + 0x4000000000000000000
            self.pattern_helper.user_mode = fixed_drive_state
            self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
            self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
            self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
            self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()),
                                            fixed_drive_state)
            base_fixed_drive_state *= 16


    def DirectedPSDB0Pin0SweepTest(self):
        """
        Pin Multiplier driver/comparator VCCIO and VOX rails sweep.
        Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_0 (1st 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_0:
            if self.env.hbicc.psdb_0_pin_0.hbidtb:
                channel_sets = range(4)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_0 not available')
        self.restore_default_vccio_vref()

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 0
                fixed_drive_state = 'LOW'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 0
                fixed_drive_state = 'HIGH'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 0
                fixed_drive_state = 'HIGH_LOOPBACK'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                           fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        self.restore_default_vccio_vref()

    def restore_default_vccio_vref(self):
        self.env.hbicc.vccio_initialize()
        self.env.hbicc.ring_multiplier.set_vref(psdb_target=0)
        self.env.hbicc.ring_multiplier.set_vref(psdb_target=1)

    def DirectedPSDB0Pin1SweepTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_0_Pin_1 (2nd 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_0_Pin_1 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_0_pin_1:
            if self.env.hbicc.psdb_0_pin_1.hbidtb:
                channel_sets = range(4, 8)
            else:
                pm_name = self.env.hbicc.psdb_0_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_0_PinMultiplier_1 not available')
        self.restore_default_vccio_vref()

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 0
                fixed_drive_state = 'LOW'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 0
                fixed_drive_state = 'HIGH'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 0
                fixed_drive_state = 'HIGH_LOOPBACK'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                           fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)
        self.restore_default_vccio_vref()

    def DirectedPSDB1Pin0SweepTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_1_Pin_0 (3rd 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_1_Pin_0 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_0:
            if self.env.hbicc.psdb_1_pin_0.hbidtb:
                channel_sets = range(8, 12)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_0.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_0 not available')

        self.restore_default_vccio_vref()
        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 1
                fixed_drive_state = 'LOW'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)

                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 1
                fixed_drive_state = 'HIGH'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)

                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 1
                fixed_drive_state = 'HIGH_LOOPBACK'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                           fixed_drive_state=fixed_drive_state)

                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)
        self.restore_default_vccio_vref()

    def DirectedPSDB1Pin1SweepTest(self):
        """Test focuses on setting alternate 'HIGH' DC values on pins for PSDB_1_Pin_1 (last 4 channel sets)
        with alternate pins set to 'TRISTATE', and then tests if those 'TRISTATE' pins change to 'HIGH'
        due to DC Loopback.
        **Test:**

        * Select and use ALL 5 slices
        * Check if PSDB_1_Pin_1 is available
        * Then check if the corresponding DTB present and set the channel sets appropriately
        * First set ALL pins on ALL slices to 'LOW'
        * Then set FixedDriveState to 'HIGH_LOOPBACK' which represents the alternate 'HIGH' and 'TRISTATE',
        * with first pin at 'HIGH' for slices 0,2,4 while 'TRISTATE' for slices 1,3
        * Sleep a little to allow the DC Loopback to propagate through the DTB
        * Check if ALL the pins on ALL the slices have turned 'HIGH' including the ones set at 'TRISTATE'

        **Criteria:** Test passes if 'TRISTATE' pins have turned 'HIGH',
        resulting in ALL 175 (35*5) pins 'HIGH', test fails otherwise.
        """

        if self.env.hbicc.psdb_1_pin_1:
            if self.env.hbicc.psdb_1_pin_1.hbidtb:
                channel_sets = range(12, 16)
            else:
                pm_name = self.env.hbicc.psdb_1_pin_1.name()
                self.Log('error', f'No DTB is present on {pm_name}')
        else:
            raise SkipTest('PSDB_1_PinMultiplier_1 not available')

        self.restore_default_vccio_vref()
        
        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 1
                fixed_drive_state = 'LOW'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 1
                fixed_drive_state = 'HIGH'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                            fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

        for vccio_int in VCCIO_IRANGE:
            vccio = vccio_int * 0.01
            self.pattern_helper.hbicc.vccio_initialize(vccio)
            for vref_int in VREF_IRANGE:
                psdb_target = 1
                fixed_drive_state = 'HIGH_LOOPBACK'
                self.pattern_helper.user_mode = fixed_drive_state
                self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=channel_sets,
                                                                           fixed_drive_state=fixed_drive_state)
                vref = vref_int * 0.01
                self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, vref)
                self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
                # self.Log('info', f'VREF set to {vref}')
                self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
                self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
                time.sleep(SETTLING_TIME)
                self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)
        self.restore_default_vccio_vref()

