# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
"""
Meta data are empty cycles that can be used for various software features and as memory placeholders to allow for
vector modification.  Meta data are executed in zero time and should not impact pat gen counters.
"""
import unittest
import random
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Common.fval import skip


class MetaData(HbiccTest):
    """Tests the ability to insert Meta Data into the pattern content and execute pattern"""
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.getrandbits(35)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))

    def DirectedMetaDataTest(self):
        """Meta Data test

         Test executes a basic pattern in drive low mode that has alternating compare and meta data cycles.

         Test will pass if simulated number of fails matches actual number of fails and the assembler cycle count
         matches expected value (Meta Data vectors are not counted)
        """
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = self.create_pattern_string()
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()
        self.compare_cycle_to_expected(expected = 1011)


    def RandomMetaDataTest(self):
        """Meta Data test will pass if simulated number of fails matches actual number of fails.
           Test also checks the assembler cycle count.  Meta data vectors, while in memory, should not show up in
           this counter.
           Additionally this test will include a check of the meta data in pattern memory to ensure that it matches the
           inserted data.
        """
        self.pattern_helper.user_mode = 'LOW'
        metadatavector = random.randint(0, 0xFFFFFFFF)
        rpt_num = random.randint(10,10000)
        pattern_string = self.create_pattern_string(metadata=metadatavector, rpt_num = rpt_num)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()
        expected_cycles = rpt_num + 11
        self.compare_cycle_to_expected(expected = expected_cycles)
        self.check_meta_data_vector(metadatavector)

    def compare_cycle_to_expected(self, expected):
        register_type = self.env.hbicc.psdb_0_pin_0.registers.VECTOR_ASSEMBLER_LAST_CYCLE_COUNTER
        for pm in self.env.hbicc.get_pin_multipliers():
            for slice in self.slices:
                reg=pm.read_slice_register(register_type, slice = slice)
                if reg.value != expected:
                    self.Log('error', f'{pm.name()}, slice {slice}: {reg.value} not equal to {expected}')

    def create_pattern_string(self, metadata = 0xACED, rpt_num = 1000):
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                PatternId {pattern_id_0}
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=10

                PATTERN1:
                DriveZeroVectors length=10
                %repeat {rpt_num}
                V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                M data={metadata}
                %end

                Return                

                '''
        return pattern_string

    def check_meta_data_vector(self, metadata):
        metadata_b=metadata.to_bytes(15, 'little')
        for slice in self.slices:
            for key, value in self.pattern_helper.hbicc.pat_gen.address_and_pattern_size_list.items():
                offset = key+0x1c0
                length = 16 # Min DMA size
                data=self.pattern_helper.hbicc.pat_gen.dma_read(offset, length, slice)
                if data[0:length-1] != metadata_b:
                    self.Log('error', f'Data does not match: {metadata_b} does not equal observed data {data[0:length-1]}')
