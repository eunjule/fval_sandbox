# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""This module focuses on processing vectors in large volumes to test for capture bandwidth.

The two corner cases of interest are as follows:
1. One channel set, 35 pins, all slices, all pin multipliers
2. Sixteen channel sets, 3 pins, all slices

These two corner cases are tested in two different ways.
1. After pattern executes, capture data integrity is checked as well as other things like
end status.
2. After pattern executes, only end status and alarms raised are checked.
"""

import random

from Common.fval import skip_snapshot, deactivate_simulator
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

PIN_DIRECTION = {'DRIVE_LOW': 0x0000, 'DRIVE_HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE_PATTERN_CNTRL': 0xFFFF}

CTV_HEADER_STREAM_START_ADDRESS = 0x400000000
CTV_DATA_STREAM_START_ADDRESS = 0x440000000
TRACE_STREAM_START_ADDRESS = 0x480000000
PM_STREAM_ADDRESSES = {0: 0x4c0000000, 1: 0x500000000, 2: 0x540000000, 3: 0x580000000}

ADDRESSES = [CTV_DATA_STREAM_START_ADDRESS, CTV_HEADER_STREAM_START_ADDRESS, TRACE_STREAM_START_ADDRESS]
ADDRESSES += list(PM_STREAM_ADDRESSES.values())
PADDING = 1024
SLICECOUNT = [1, 2, 3, 4, 5]

MILLION = 1_000_000


class Functional(HbiccTest):
    """Large volumes of CTV vectors with NO capture data check

    Tests focuses on covering the two corner cases mentioned above. Each time,
    the tests execute the pattern and validates correct completion of it. This means
    that no critical alarms were raised and the end status was seen.

    **Criteria: ** All tests within this class should complete with the following:

    - Correct Pattern End Status word
    - No underrun issues
    - No critical alarms

    """
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
        self.slices = [0, 1, 2, 3, 4]
        self.pattern_helper.is_simulator_active = True
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)
        self.pattern_helper.user_mode = random.randint(0, 0b11111111111111111111111111111111111)

    @skip_snapshot
    @deactivate_simulator
    def Directed100MillionCtv3PinsAllChannelSetsAllSlicesSanityCheckNoDataCheckTest(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 100 million vector pattern with CTV. No check on data integrity.
        Mainly checking to assure pattern burst completed without any alarms, especially underruns.

        """
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.e32_interval = 128
        self.slices = [0, 1, 2, 3]
        repeat = 10000
        loops = 10000
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loops}
                
                PatternId {id_0} 
                PCall LOOP

                LOOP:
                    CompareHighVectors length={repeat}, ctv=1
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices, attributes={'check_capture_results': False})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        total_ctv_vectors = repeat * loops
        expected_e32_count = total_ctv_vectors//self.pattern_helper.e32_interval

        expected_ctv_header_count = repeat * loops // 4
        self.env.hbicc.pat_gen.check_ctv_header_count(expected=expected_ctv_header_count, slices=self.slices)

        cycles_per_ctv_block = 8
        default_pg_packing = 4
        expected_ctv_blocks = (repeat * loops // cycles_per_ctv_block) * \
                              len(self.env.hbicc.get_pin_multipliers()) + default_pg_packing
        self.env.hbicc.pat_gen.check_ctv_data_block_count(expected=expected_ctv_blocks, slices=self.slices)
        self.env.hbicc.pat_gen.check_e32_count(expected_e32_count)

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.check_h_counter(expected=total_ctv_vectors, slices=self.slices)


    @skip_snapshot
    @deactivate_simulator
    def Directed100MillionCtvAllPinsOneChannelSetAllSlicesSanityCheckNoDataCheckTest(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 100 million vector pattern with CTV enabled on all pins. No check on data integrity.
        Mainly checking to assure pattern burst completed without any alarms, especially underruns.

        """
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.is_simulator_active=False
        self.pattern_helper.e32_interval = 512
        self.slices = [0, 1, 2, 3]
        repeat = 10000
        loops = 10000
        id_0 = random.randint(0, 100)
        ctp = 0b11111111111111111111111111111111111
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=DUT_SERIAL, channel_set=0               ,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loops}

                        PatternId {id_0} 
                        PCall LOOP

                        LOOP:
                            CompareHighVectors length={repeat}, ctv=1
                            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                            I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices, attributes={
            'check_capture_results': False})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        total_ctv_vectors = repeat * loops
        expected_e32_count = total_ctv_vectors//self.pattern_helper.e32_interval

        expected_ctv_header_count = repeat * loops // 4
        self.env.hbicc.pat_gen.check_ctv_header_count(expected=expected_ctv_header_count, slices=self.slices)

        cycles_per_ctv_block = 32
        ctv_blocks_based_on_ctp = 9
        default_pg_packing = 4
        expected_ctv_blocks = (repeat * loops // cycles_per_ctv_block) * \
                              ctv_blocks_based_on_ctp + default_pg_packing
        self.env.hbicc.pat_gen.check_ctv_data_block_count(expected=expected_ctv_blocks, slices=self.slices)
        self.env.hbicc.pat_gen.check_e32_count(expected_e32_count)

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.check_h_counter(expected=total_ctv_vectors, slices=self.slices)


class Diagnostics(HbiccTest):
    """Large volumes of CTV vectors with data integrity check

    Each test focuses on a single large number of CTV vectors and the data generated
    is checked for correctness. Alarms and end status is also validated.

    **Criteria: ** All tests within this class should complete with the following:

        - Correct Pattern End Status word
        - Correct capture data
        - No underrun issues
        - No critical alarms

    """
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
        self.slices = [0, 1, 2, 3, 4]
        self.pattern_helper.is_simulator_active = True
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)
        self.pattern_helper.user_mode = random.randint(0, 0b11111111111111111111111111111111111)

    def RandomEncodingFDSAllCtp50kCtvOneChannelSetAllSlices(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 50,000 vector pattern with CTV enabled on all pins.
        Check correctness of data, alarms and end status.

        """
        repeat = 50_000
        self.one_channel_set_execution(repeat)

    def RandomEncodingFDSAllCtp1MCtvOneChannelSetAllSlices(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 1 Million vector pattern with CTV enabled on all pins.
        Check correctness of data, alarms and end status.

        """
        repeat = 1 * MILLION
        self.one_channel_set_execution(repeat)

    def RandomEncodingFDSAllCtp5MCtvOneChannelSetAllSlices(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 5 Million vector pattern with CTV enabled on all pins.
        Check correctness of data, alarms and end status.

        """
        repeat = 5 * MILLION
        self.one_channel_set_execution(repeat)

    def RandomEncodingFDSAllCtp25MCtvOneChannelSetAllSlices(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 5 Million vector pattern with CTV enabled on all pins.
        Check correctness of data, alarms and end status.

        """
        repeat = 25 * MILLION
        self.one_channel_set_execution(repeat)

    def RandomEncodingFDSAllCtp50MCtvOneChannelSetAllSlices(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 50 Million vector pattern with CTV enabled on all pins.
        Check correctness of data, alarms and end status.

        """
        repeat = 50 * MILLION
        self.one_channel_set_execution(repeat)

    def RandomEncodingFDSAllCtp100MCtvOneChannelSetAllSlices(self):
        """Runs the pattern with CTV on all pins enabled and one channel set.

        This test creates a 100 Million vector pattern with CTV enabled on all pins.
        Check correctness of data, alarms and end status.

        """
        repeat = 100 * MILLION
        self.one_channel_set_execution(repeat)

    def RandomEncodingFDS3Ctp50kCtvAllChannelSetAllSlices(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 50,000 vector pattern with CTV.
        Check correctness of data, alarms and end status.

        """
        repeat = 50_000
        self.all_channel_sets_execution(repeat)

    def RandomEncodingFDS3Ctp1MCtvAllChannelSetAllSlices(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 1 Million vector pattern with CTV.
        Check correctness of data, alarms and end status.

        """
        repeat = MILLION
        self.all_channel_sets_execution(repeat)

    def RandomEncodingFDS3Ctp5MCtvAllChannelSetAllSlices(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 5 Million vector pattern with CTV.
        Check correctness of data, alarms and end status.

        """
        repeat = 5 * MILLION
        self.all_channel_sets_execution(repeat)

    def RandomEncodingFDS3Ctp25MCtvAllChannelSetAllSlices(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 25 Million vector pattern with CTV.
        Check correctness of data, alarms and end status.

        """
        repeat = 25 * MILLION
        self.all_channel_sets_execution(repeat)

    def RandomEncodingFDS3Ctp50MCtvAllChannelSetAllSlices(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 50 Million vector pattern with CTV.
        Check correctness of data, alarms and end status.

        """
        repeat = 50 * MILLION
        self.all_channel_sets_execution(repeat)

    def Random100MAllChannelSetsCtvEncoding(self):
        """Runs the pattern with 3 CTV pins enabled and all channel sets.

        This test creates a 100 Million vector pattern with CTV.
        Check correctness of data, alarms and end status.

        """
        repeat = 100 * MILLION
        self.all_channel_sets_execution(repeat)

    @skip_snapshot
    @deactivate_simulator
    def one_channel_set_execution(self, repeat):
        active_number_bits = random.randint(16, 34)
        self.ctp, _ = self.env.hbicc.get_int_with_fix_active_bits(35, active_number_bits)
        self.pattern_helper.is_capture_active = True
        self.env.hbicc.model.patgen.is_simulator_active = self.pattern_helper.is_simulator_active
        self.pattern_helper.e32_interval = 128
        self.slices = range(4)
        id_0= random.randint(0,100)
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, attributes={'splitter_exponent': 15})
        mtv_mask_string = self.pattern_helper.mtv_mask_object.string_values
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=0,   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        PatternId {id_0}
                        PCall PATTERN1
                        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        
                        PATTERN1:
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            RandomAddressEncoder length={repeat}, pms=0b1, channel_sets=0b1, slices=0b1111, ctp=0b{self.ctp:035b} {mtv_mask_string}
                        Return
                        '''
        self.Log('info', f'{pattern_string}')
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            slice_object.pattern = pattern_string
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    @skip_snapshot
    @deactivate_simulator
    def all_channel_sets_execution(self, repeat, active_number_bits=3):
        self.ctp, _ = self.env.hbicc.get_int_with_fix_active_bits(35, active_number_bits)
        self.pattern_helper.is_capture_active = True
        self.env.hbicc.model.patgen.is_simulator_active = self.pattern_helper.is_simulator_active
        self.pattern_helper.e32_interval = 128
        self.slices = range(4)
        id_0 = random.randint(0,100)
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, attributes={'splitter_exponent': 6})
        mtv_mask_string = self.pattern_helper.mtv_mask_object.string_values
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        PatternId {id_0}
                        PCall PATTERN1
                        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        
                        PATTERN1:
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            RandomAddressEncoder length={repeat}, pms=0b1111, channel_sets=0b1111, slices=0b1111, ctp=0b{self.ctp:035b} {mtv_mask_string}
                        Return
                        '''
        self.Log('info', f'{pattern_string}')
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            slice_object.pattern = pattern_string
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()
