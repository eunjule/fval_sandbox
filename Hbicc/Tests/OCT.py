# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

""" Tests for Reading On Chip termination codes for different FPGA banks

On chip termination modulation is possible but is out of FVAL scope
as it involves analog measurements.
Instead FVAL is focussed on reading the default codes and ensuring they
are within limits.
"""

from Hbicc.Tests.HbiccTest import HbiccTest, HbiccTestDecorators

OCT_BANK_REGISTER = ['OCT_CALIBRATED_VALUE_BANK_2L',
                     'OCT_CALIBRATED_VALUE_BANK_2K',
                     'OCT_CALIBRATED_VALUE_BANK_2J',
                     'OCT_CALIBRATED_VALUE_BANK_2I',
                     'OCT_CALIBRATED_VALUE_BANK_2H',
                     'OCT_CALIBRATED_VALUE_BANK_2G',
                     'OCT_CALIBRATED_VALUE_BANK_2F',
                     'OCT_CALIBRATED_VALUE_BANK_2A',
                     'OCT_CALIBRATED_VALUE_BANK_3H',
                     'OCT_CALIBRATED_VALUE_BANK_3G',
                     'OCT_CALIBRATED_VALUE_BANK_3F',
                     'OCT_CALIBRATED_VALUE_BANK_3E',
                     'OCT_CALIBRATED_VALUE_BANK_3D',
                     'OCT_CALIBRATED_VALUE_BANK_3C',
                     'OCT_CALIBRATED_VALUE_BANK_3B',
                     'OCT_CALIBRATED_VALUE_BANK_3A']

N_LEG_LOWER_LIMIT = 40
N_LEG_UPPER_LIMIT = 90
P_LEG_LOWER_LIMIT = 10
P_LEG_UPPER_LIMIT = 55


class Diagnostic(HbiccTest):

    def setUp(self, tester=None):
        super().setUp()

    @HbiccTestDecorators.execute_on_all_pms
    def DirectedPMReadDefaultOctCodesForDifferentBanksTest(self):
        oct_value = {}
        error_count = []
        self.read_oct_values(oct_value)
        self.compare_actual_oct_read_with_expected(oct_value, error_count)
        self.report_results(error_count)

    def read_oct_values(self, oct_value):
        for register in OCT_BANK_REGISTER:
            n_leg_value, p_leg_value = self.pm.read_oct_code(register)
            oct_value.update({register: (n_leg_value, p_leg_value)})

    def report_results(self, read_errors):
        if read_errors:
            self.report_errors(self.pm, read_errors)
        else:
            self.Log('info', f'{self.pm.name()} OCT codes for different banks were within range')

    def compare_actual_oct_read_with_expected(self, oct_dct, error_count):
        for register, (n_leg_value, p_leg_value) in oct_dct.items():
            attribute_dict = {}
            if not (N_LEG_LOWER_LIMIT <= n_leg_value <= N_LEG_UPPER_LIMIT):
                attribute_dict.update({'Observed N leg': n_leg_value})
            if not (P_LEG_LOWER_LIMIT <= p_leg_value <= P_LEG_UPPER_LIMIT):
                attribute_dict.update({'Observed P leg': p_leg_value})
            if attribute_dict:
                attribute_dict.update(
                    {'Register': register, 'Expected N Leg LCL/UCL': f'{N_LEG_LOWER_LIMIT}/{N_LEG_UPPER_LIMIT}',
                     'Observed N leg': n_leg_value,
                     'Expected P Leg LCL/UCL': f'{P_LEG_LOWER_LIMIT}/{P_LEG_UPPER_LIMIT}',
                     'Observed P leg': p_leg_value})
                error_count.append(attribute_dict)

    def report_errors(self, device, register_errors):
        column_headers = ['Register', 'Expected N Leg LCL/UCL', 'Observed N leg', 'Expected P Leg LCL/UCL',
                          'Observed P leg']
        table = device.contruct_text_table(column_headers, register_errors)
        self.Log('error', f'OCT codes read incorrect  for following bank/s: \n {table}')



