# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The instruction processor in the PatGen FPGA supports ALU operations.

Operations:
XOR  : bit-wise exclusive OR
AND  : bit-wise AND
OR   : bit-wise OR
LSHL : logical shift left
LSHR : logical shift right
ADD  : integer addition
SUB  : integer subtraction
MUL  : integer multiplication

Sources of operations:
 - 32 general purpose registers
 - Flags register
 - Immediate field in instruction

Destinations of operations
 - 32 general purpose registers
 - Flags register
 - None
"""

import itertools
import operator
import random

from Common.fval import Object
from Hbicc.Tests.HbiccTest import HbiccTest

HEADERS = ['Slice', 'Op', 'Src', 'Operand', 'Dst', 'Expected', 'Observed', 'PreOp Flags', 'Flags Register', 'Expected Flags', 'Reason']


class Flags(HbiccTest):
    """Verify that flag bits that should be settable are settable."""

    def DirectedRegisterOpSettableFlagsTest(self):
        """Verify which flag register bits are settable by register op"""
        
        pattern = f'''
        PATTERN_START:
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length=1
        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm=0x00000000
        I optype=REGISTER, opsrc=REGSRC.FLAGS, opdest=REGDEST.REG, dest=0
        I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm=0xFFFFFFFF
        I optype=REGISTER, opsrc=REGSRC.FLAGS, opdest=REGDEST.REG, dest=1
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xcafef00d
        DriveZeroVectors length=1
        '''
        
        self.run_pattern(pattern)
        
        device = self.env.hbicc.pat_gen
        a = device.read_slice_register(device.registers.CENTRAL_DOMAIN_REGISTER, index=0).value
        b = device.read_slice_register(device.registers.CENTRAL_DOMAIN_REGISTER, index=1).value
        settable = a ^ b
        expected_value = 0xFFFF
        if settable != expected_value:
            self.Log('info', f'result of writing 0x00000000 to flags: 0x{a:08X}')
            self.Log('info', f'result of writing 0xFFFFFFFF to flags: 0x{b:08X}')
            self.Log('info', f'XOR:                                   0x{settable:08X}')
            self.Log('error', f'expected_value: 0x{expected_value:08X}')
        else:
            self.Log('info', f'Settable bits: 0x{settable:08X}')
            
    def DirectedAndOrFlagsTest(self):
        """Verify which flag register bits are settable by AND/OR operations"""
        
        pattern = f'''
        PATTERN_START:
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length=1
        I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0x00000000, dest=ALUDEST.FLAGS
        I optype=REGISTER, opsrc=REGSRC.FLAGS, opdest=REGDEST.REG, dest=0
        I optype=ALU, aluop=ALUOP.OR, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFFFFFFF, dest=ALUDEST.FLAGS
        I optype=REGISTER, opsrc=REGSRC.FLAGS, opdest=REGDEST.REG, dest=1
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xcafef00d
        DriveZeroVectors length=1
        '''
        
        self.run_pattern(pattern)
        
        device = self.env.hbicc.pat_gen
        a = device.read_slice_register(device.registers.CENTRAL_DOMAIN_REGISTER, index=0).value
        b = device.read_slice_register(device.registers.CENTRAL_DOMAIN_REGISTER, index=1).value
        settable = a ^ b
        expected_value = 0xB3FEFFFF
        if settable != expected_value:
            self.Log('info', f'result of writing 0x00000000 to flags: 0x{a:08X}')
            self.Log('info', f'result of writing 0xFFFFFFFF to flags: 0x{b:08X}')
            self.Log('info', f'XOR:                                   0x{settable:08X}')
            self.Log('error', f'expected_value: 0x{expected_value:08X}')
        else:
            self.Log('info', f'Settable bits: 0x{settable:08X}')

    def run_pattern(self, pattern):
        pattern_helper = PatternHelper(self.env)
        pattern_helper._is_set_and_check_pin_state = False
        pattern_helper.track_error_count = False
        pattern_helper.create_slice_channel_set_pattern_combo(pattern=pattern, fixed_drive_state='LOW', slices=[0])
        pattern_helper.execute_pattern_scenario()



class Functional(HbiccTest):
    """Test all permutations of source and destination for each ALU operation"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.alu_ops =  ['XOR', 'AND', 'OR', 'LSHL', 'LSHR', 'ADD', 'SUB', 'MUL']
        self.pattern_helper = PatternHelper(self.env)

    def RandomRegisterImmediateToNoneTest(self):
        """ALU operations from Register A and Immediate, without storing result

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random value is stored in register A
        - The immediate field is random
        - Only the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='RA_I', destOp='NONE').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomRegisterImmediateToRegisterTest(self):
        """ALU operations from Register A and Immediate, storing result in a register

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random value is stored in register A
        - The immediate field is random
        - The result and the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='RA_I', destOp='REG').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomRegisterImmediateToFlagsRegTest(self):
        """ALU operations from Register A and Immediate, storing result in the flags register

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random value is stored in register A
        - The immediate field is random
        - The flags register is checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='RA_I', destOp='FLAGS').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomRegisterRegisterToNoneTest(self):
        """ALU operations from Register A and Register B, without storing result

        For each operation
        - Register A and B are randomly chosen from the 32 available registers
        - Random values are stored in register A and B
        - Only the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='RA_RB', destOp='NONE').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)
    
    def RandomRegisterRegisterToRegisterTest(self):
        """ALU operations from Register A and Register B, storing result in a register

        For each operation
        - Register A and B and DEST are randomly chosen from the 32 available registers
        - Random values are stored in register A and B
        - The result and the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='RA_RB', destOp='REG').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomRegisterRegisterToFlagsTest(self):
        """ALU operations from Register A and Register B, storing result in the flags register

        For each operation
        - Register A and B are randomly chosen from the 32 available registers
        - Random values are stored in register A and B
        - The flags register is checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='RA_RB', destOp='FLAGS').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomFlagsImmediateToNoneTest(self):
        """ALU operations from Flags register and Immediate, without storing result

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random 32-bit value is stored in register A
        - A random 16-bit value is stored in the flags register
        - Only the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='FLAGS_I', destOp='NONE').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomFlagsImmediateToRegisterTest(self):
        """ALU operations from Flags register and Immediate, storing result in a register

        For each operation
        - The destination register is randomly chosen from the 32 available registers
        - A random 32-bit value is stored in the immediate field
        - A random 16-bit value is stored in the flags register
        - The result and the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='FLAGS_I', destOp='REG').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomFlagsImmediateToFlagsTest(self):
        """ALU operations from Flags register and Immediate, storing result in the flags register

        For each operation
        - A random 32-bit value is stored in the immediate field
        - A random 16-bit value is stored in the flags register
        - The flags register is checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='FLAGS_I', destOp='FLAGS').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomFlagsRegisterToNoneTest(self):
        """ALU operations from Flags register and Register A, without storing the result

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random 32-bit value is stored in register A
        - A random 16-bit value is stored in the flags register
        - Only the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='FLAGS_RB', destOp='NONE').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomFlagsRegisterToRegisterTest(self):
        """ALU operations from Flags register and Register A, storing result in a register

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random 32-bit value is stored in register A
        - A random 16-bit value is stored in the flags register
        - The result and the flags are checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='FLAGS_RB', destOp='REG').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def RandomFlagsRegisterToFlagsTest(self):
        """ALU operations from Flags register and Register A, storing result in the flags register

        For each operation
        - Register A is randomly chosen from the 32 available registers
        - A random 32-bit value is stored in register A
        - A random 16-bit value is stored in the flags register
        - The flags register is checked for correctness.
        """
        for op in self.alu_ops:
            with self.subTest(alu_op=op):
                alu = ALU(self.pattern_helper, aluOp=op, sourceOp='FLAGS_RB', destOp='FLAGS').is_present()
                alu.execute_alu_operation()
                self.report_results(alu)

    def report_results(self, alu):
        if alu.errors:
            table = alu.device.contruct_text_table(HEADERS, alu.errors)
            self.Log('error', f'Operation failed: {table}')
        else:
            self.Log('info', 'ALU Operation {} completed successfully.'.format(alu.aluOp))


from Hbicc.testbench.PatternUtility import PatternHelper

PIN_DIRECTION = {'DRIVE_LOW': 0x0000, 'DRIVE_HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE_PATTERN_CNTRL': 0xFFFF}


class ALU(Object):
    def __init__(self, helper, aluOp='XOR', regRange=32, sourceOp='RA_RB', destOp='REG'):
        self.helper = helper
        self.env = helper.env
        self.device = self.env.hbicc.pat_gen
        self.hbicc = self.env.hbicc
        self.aluOp = aluOp
        self.regRange = regRange
        self.sourceOp = sourceOp
        self.destOp = destOp
        self.slice_list = [0]
        self.errors = []
        self.tally = []

    def execute_alu_operation(self):
        # self.check_slice_calibration() #should we check for calibration here or during init?
        for slice in self.slice_list:
            for reg_a, reg_b, reg_c in self.register_permutations:
                self.reg_a, self.reg_b, self.reg_c = reg_a, reg_b, reg_c

                pattern_helper = self.helper
                # pattern_helper = PatternHelper(self.env)
                fixed_drive_state = 'LOW'
                pattern_helper._is_set_and_check_pin_state = False
                self.pattern_end_status = 0xdeadbeef
                self.build_pattern(reg_a, reg_b, reg_c)
                pattern_helper.track_error_count = False
                pattern_helper.set_rm_snooper(False)
                pattern_helper.set_xcvr_report(False)

                pattern_helper.create_slice_channel_set_pattern_combo(self.pattern, fixed_drive_state,
                                                                      slices=[slice])
                pattern_helper.execute_pattern_scenario()

                flags = self.device.read_slice_register(self.device.registers.FLAGS, slice)
                result = self.check_operation(reg_a, reg_b, reg_c, flags)
                self.check_flags(flags, result)

    def check_flags(self, flags, result):
        flag_checker = FlagChecker(self.aluOp, result, flags)
        flag_fails = flag_checker.check_flags()
        for fail in flag_fails:
            self.add_fail_to_error_list(flags, expected=fail['Expected'], reason=fail['Reason'])

    def build_pattern(self, reg_a, reg_b, reg_c):
        pre_update_flags_reg, pre_op_flags_reg, load_flags_copy = random.sample(list(set(range(32)) - set([reg_a, reg_b, reg_c])), 3)
        string_pattern = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o66666666666666666666666666666666666 # no channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        DriveZeroVectors length=500
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
            I optype=REGISTER, opsrc=REGSRC.FLAGS, opdest=REGDEST.REG, dest={pre_update_flags_reg}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={reg_a}, imm={self.reg_a_random_value}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={reg_b}, imm={self.reg_b_random_value}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.FLAGS, imm={self.flags_random_value}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={load_flags_copy}, imm={self.flags_random_value}
            I optype=REGISTER, opsrc=REGSRC.FLAGS, opdest=REGDEST.REG, dest={pre_op_flags_reg}
            I optype=ALU, aluop={self.aluOp}, opsrc=ALUSRC.{self.sourceOp}, opdest=ALUDEST.{self.destOp}, regA={reg_a}, regB={reg_b}, imm={self.imm_random_value}, dest={reg_c}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_end_status}
            DriveZeroVectors length=1
        '''
        self.pattern = string_pattern

    def check_operation(self, reg_a, reg_b, reg_c, flags):
        pre_op_flags_mask = 0x00010000 | (self.flags_random_value & 0x0000FFFF)
        operand_a, operand_b = self.get_operands_from_sources(pre_op_flags_mask, reg_a, reg_b)
        self.operand_a, self.operand_b = operand_a, operand_b
        result = self.alu_operation(operand_a, operand_b)
        return self.check_results(operand_a, operand_b, result, flags)

    def check_results(self, operand_a, operand_b, result, flags):
        if self.destOp in ['NONE']:
            return result
        else:
            self.compare_expected_observed(result, operand_a, operand_b, flags)
            return result

    def compare_expected_observed(self, result, operand_a, operand_b, flags):
        # print('results before: 0x{:X}'.format(result))
        expected = self.python_int_to_signed(result)
        # print('expected before: 0x{:X}'.format(expected))

        if self.destOp == 'FLAGS':
            observed = self.device.read_slice_register(self.device.registers.FLAGS).value & 0x0000FFFF
            # print('results22: 0x{:X} 0x{:X}'.format(expected, observed))
            expected = expected & 0x0000FFFF
            # print('results after: 0x{:X} 0x{:X}'.format(expected, observed))
        elif self.destOp == 'REG':
            observed = self.device.read_slice_register(self.device.registers.CENTRAL_DOMAIN_REGISTER, index=self.reg_c).Data
            # print('results after: 0x{:X} 0x{:X}'.format(expected, observed))
        else:
            raise Exception('ERROR: OPERATION DESTINATION NOT HANDLED')

        if expected != observed:
            self.add_fail_to_error_list(flags, expected=expected, observed=observed, reason='Bad Op')

    def add_fail_to_error_list(self, flags, expected=0xDEAD,observed=0xDEAD, expected_flags=0xDEAD, reason='Undefined'):
        src_a, src_b = self.sourceOp.split('_')
        # flags = self.device.read_slice_register(self.device.registers.ALU_FLAGS)

        self.errors.append({'Slice': 1, 'Op': self.aluOp,
                            'Src': self.get_dst_printout(src_a),
                            'Operand': f'0x{self.operand_a:X}',
                            'Dst': self.get_dst_printout(self.destOp),
                            'Expected': f'0x{expected:X}',
                            'Observed': f'0x{observed:X}',
                            'PreOp Flags': '0x00010000',
                            'Flags Register': f'0x{flags.value:X} (0x{flags.ADDR:X})',
                            'Expected Flags': f'0x{expected_flags:X}',
                            'Reason': reason})
        self.errors.append({'Slice': '', 'Op': '',
                            'Src': self.get_dst_printout(src_b),
                            'Operand': f'0x{self.operand_b:X}',
                            'Dst': '',
                            'Expected': '',
                            'Observed': '',
                            'PreOp Flags': '',
                            'Flags Register': '',
                            'Expected Flags': '',
                            'Reason': reason})

    def get_dst_printout(self, src):
        reg_dict = {'RA': ['Reg A', self.reg_a], 'RB': ['Reg B', self.reg_b],'REG': ['Dst Reg', self.reg_c],
                    'I': ['Imm', f'0x{self.imm_random_value:X}'], 'FLAGS': ['FLAGS', '0x178'], 'NONE': ['None', 0]}
        name, value = reg_dict.get(src, ['Undefined', 'Undefined'])
        return f'{name} ({value})'

    def python_int_to_signed(self, result):
        if result < 0:
            result = ((abs(result) ^ 0xFFFFFFFF) + 1)
        return result & 0xFFFFFFFF

    def signed_to_python_int(self, src):
        if src & 0x80000000:
            src = -1 * ((src ^ 0xFFFFFFFF) + 1)
        return src

    def get_operands_from_sources(self, pre_op_flags, reg_a, reg_b):
        reg_a_value = self.device.read_slice_register(self.device.registers.CENTRAL_DOMAIN_REGISTER, index=reg_a).Data
        reg_b_value = self.device.read_slice_register(self.device.registers.CENTRAL_DOMAIN_REGISTER, index=reg_b).Data
        operands = {'RA_RB': [reg_a_value, reg_b_value],
                    'RA_I': [reg_a_value, self.imm_random_value],
                    'FLAGS_RB': [pre_op_flags, reg_b_value],
                    'FLAGS_I':  [pre_op_flags, self.imm_random_value]}
        return operands[self.sourceOp]

    def alu_operation(self, operand_a, operand_b):
        if self.aluOp in ['ADD', 'SUB']:
            return self.signed_operation(operand_a, operand_b)
        else:
            return self.unsigned_operation(operand_a, operand_b)

    def unsigned_operation(self, operand_a, operand_b):
        ops = {'XOR': operator.xor,
               'AND': operator.and_,
               'OR': operator.or_,
               'LSHL': operator.lshift,
               'LSHR': operator.rshift,
               'MUL': operator.mul}
        return ops.get(self.aluOp, self.not_implemented)(operand_a, operand_b)

    def not_implemented(self, operand_a, operand_b):
        print('ALU Op is not implemented for Operands 0x{:X} and 0x{:X}'.format(operand_a, operand_b))

    def signed_operation(self, src1, src2):
        # print('operand: 0x{:X} 0x{:X}'.format(src1, src2))
        src1 = self.signed_to_python_int(src1)
        src2 = self.signed_to_python_int(src2)
        return self.carry_out_operation(src1, src2)

    def carry_out_operation(self, a, b):
        op = {'ADD': operator.add, 'SUB': operator.sub, 'MUL': operator.mul}
        # op = {'ADD': a + b, 'SUB': a - b, 'MUL': a * b}
        # print('ss: 0x{:X} 0x{:X} 0x{:X}'.format(a,b, op[self.aluOp](a,b)))
        return op.get(self.aluOp, self.not_implemented)(a,b)

    def check_slice_calibration(self):
        for slice in self.operation.slice_list:
            ddr_calibration_status = self.operation.device.get_ddr_calibration_status(slice).value
            if ddr_calibration_status != 5:
                self.operation.device.Log('error', f'DDR calibration failed. Cannot perform DMA for slice {slice}')

    def is_present(self):
        if self.device is None:
            self.Log('warning', f'PatGen Device Unavailable')
            return False
        else:
            GenerateAluValues(self).randomize_operation_parameters()
            return self


class FlagChecker(Object):
    def __init__(self, alu_op, op_result, flags):
        self.unsigned = op_result
        self.signed = None
        self.alu_op = alu_op
        self.flags = flags
        self.errors = []
        self.tally = []

    def check_flags(self):
        if self.alu_op in ['MUL']:
            pass
        else:
            self.check_flags_all_other_operations()
        return self.errors

    def check_flags_all_other_operations(self):
        self.python_int_to_signed()
        self.check_overflow_flag()
        self.check_zero_flag()
        self.check_sign_bit_flag()

    def check_zero_flag(self):
        if (self.signed & 0xFFFFFFFF) == 0:
            if self.flags.Zero != 1:
                reason ='Operation result is Zero, but ALU_Zero flag was not set.'
                # self.add_fail_to_error_list(expected=signed, reason=reason)
                self.errors.append({'Expected': self.signed, 'Reason': reason})
            else:
                # print('Correct Zero Flag')
                self.tally.append('zero')
        else:
            if self.flags.Zero == 1:
                reason = 'Operation result is None-Zero, but ALU_Zero flag is set.'
                # self.add_fail_to_error_list(expected=signed, reason=reason)
                self.errors.append({'Expected': self.signed, 'Reason': reason})

    def check_overflow_flag(self):
        if self.alu_op in ['LSHL', 'LSHR', 'XOR', 'OR', 'AND']:
            return
        # print('overflow: ', result)
        if -2 ** 31 < self.unsigned < ((2 ** 31) - 1):
            if self.flags.OverFlow == 1:
                reason = 'Operation result did not Overflow, but ALU_OverFlow flag was set.'
                self.errors.append({'Expected': self.signed, 'Reason': reason})
                # self.add_fail_to_error_list(expected=signed, reason=reason)
            else:
                # print('Correct OverFlow Flag')
                self.tally.append('overflow')
        else:
            if self.flags.OverFlow != 1:
                reason = 'Operation result in Overflow, but ALU_OverFlow flag was not set.'
                self.errors.append({'Expected': self.signed, 'Reason': reason})
                # self.add_fail_to_error_list(expected=signed, reason=reason)

    def check_sign_bit_flag(self):
        if self.signed & 0x80000000:  # or (result & 0x80000000):
            if self.flags.Sign != 1:
                reason = 'Operation results in a Signed Value, but ALU_Sign flag was not set.'
                self.errors.append({'Expected': self.signed, 'Reason': reason})
                # self.add_fail_to_error_list(expected=signed, reason=reason)
            else:
                # print('Correct Signed Flag')
                self.tally.append('signed')
        else:
            if self.flags.Sign == 1:
                reason = 'Operation results in a Unsigned Value, but ALU_Sign flag was set.'
                self.errors.append({'Expected': self.signed, 'Reason': reason})
                # self.add_fail_to_error_list(expected=signed, reason=reason)

    def python_int_to_signed(self):
        result = self.unsigned
        if result < 0:
            result = ((abs(result) ^ 0xFFFFFFFF) + 1)
        self.signed = result & 0xFFFFFFFF

class GenerateAluValues(Object):
    def __init__(self, operation):
        self.operation = operation

    def randomize_operation_parameters(self):
        self.set_register_a_b_c_permutations()
        self.set_operands_to_random_values()
        self.operation.pattern_end_status = random.randint(0, 0xFFFFFFFF)
        # print('end status: ', self.operation.pattern_end_status)

    def set_operands_to_random_values(self):
        if self.operation.aluOp in ['LSHR', 'LSHL'] and self.operation.destOp != 'NONE':
            self.set_values_for_logical_shifts()
        elif self.operation.aluOp == 'MUL':
            self.set_values_for_multiplication()
        else:
            self.set_values_for_all_other()

    def set_register_a_b_c_permutations(self):
        self.operation.register_set = random.sample(set(range(32)), 3)
        self.operation.register_permutations = list(itertools.permutations(self.operation.register_set, 3))

    def set_values_for_all_other(self):
        self.set_default_values()
        if self.operation.destOp is 'NONE':
            self.change_default_values()

    def change_default_values(self):
        if self.operation.sourceOp == 'RA_RB':
            self.set_values_for_reg_a_reg_b()
        elif self.operation.sourceOp == 'RA_I':
            self.set_values_for_reg_a_imm()
        elif self.operation.sourceOp == 'FLAGS_RB':
            self.set_values_for_flags_reg_b()
        elif self.operation.sourceOp == 'FLAGS_I' and self.operation.aluOp in ['LSHR', 'LSHL']:
            self.operation.imm_random_value = random.randint(0, 32)

    def set_default_values(self):
        self.operation.reg_a_random_value = random.randint(0, 0xFFFFFFFF)
        self.operation.reg_b_random_value = random.randint(0, 0xFFFFFFFF)
        self.operation.imm_random_value = random.randint(0, 0xFFFFFFFF)
        self.operation.flags_random_value = random.randint(0, 0xFFFFFFF)

    def set_values_for_multiplication(self):
        self.operation.reg_a_random_value = random.getrandbits(16)
        self.operation.reg_b_random_value = random.getrandbits(16)
        self.operation.imm_random_value = random.getrandbits(16)
        self.operation.flags_random_value = random.getrandbits(16)

    def set_values_for_logical_shifts(self):
        self.operation.reg_a_random_value = random.randint(0, 0xFFFFFFF)
        self.operation.reg_b_random_value = random.randint(0, 32)
        self.operation.imm_random_value = random.randint(0, 32)
        self.operation.flags_random_value = random.randint(0, 0xFFFFFFF)

    def set_values_for_reg_a_reg_b(self):
        if self.operation.aluOp == 'ADD':
            self.operation.reg_a_random_value = random.randint(0x7FFFFFF0, 0x7FFFFFFF)
            self.operation.reg_b_random_value = random.randint(0x7FFFFFF0, 0x7FFFFFFF)
        elif self.operation.aluOp == 'SUB':
            self.operation.reg_a_random_value = random.randint(0, 0x7FFFFFFF)
            self.operation.reg_b_random_value = self.operation.reg_a_random_value
        elif self.operation.aluOp == 'MUL':
            self.operation.reg_a_random_value = random.getrandbits(16)
            self.operation.reg_b_random_value = random.getrandbits(16)
        elif self.operation.aluOp in ['AND', 'XOR', 'OR', 'LSHL', 'LSHR']:
            self.operation.reg_a_random_value = 0
            self.operation.reg_b_random_value = self.operation.reg_a_random_value

    def set_values_for_reg_a_imm(self):
        if self.operation.aluOp in ['AND', 'XOR', 'OR', 'LSHL', 'LSHR']:
            self.operation.reg_a_random_value = 0
            self.operation.imm_random_value = self.operation.reg_a_random_value
        elif self.operation.aluOp in ['ADD', 'SUB']:
            self.operation.reg_a_random_value = random.randint(0x7FFFFFF0, 0x7FFFFFFF)
            self.operation.imm_random_value = random.randint(0x7FFFFFF0, 0x7FFFFFFF)
        else:  # MUL
            self.operation.reg_a_random_value = random.getrandbits(16)
            self.operation.imm_random_value = random.getrandbits(16)
        # elif self.operation.aluOp == 'ADD':
        #     self.operation.reg_a_random_value = random.randint(0, 0x7FFFFFFF)
        #     self.operation.reg_b_random_value = self.operation.reg_a_random_value

    def set_values_for_flags_reg_b(self):
        if self.operation.aluOp in ['LSHR', 'LSHL']:
            self.operation.reg_b_random_value = random.randint(0, 32)
        else:
            self.operation.reg_b_random_value = random.randint(0, 0xF) << 25


