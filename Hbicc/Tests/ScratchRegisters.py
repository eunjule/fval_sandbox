# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import inspect
from Common.fval import expected_failure
from Hbicc.Tests.HbiccTest import HbiccTest
import random
import unittest

MAX_FAIL_COUNT = 10
REGCOUNT = 8


class Diagnostics(HbiccTest):

    def DirectedBasicPatgenScratchRegisterTest(self, TEST_ITERATIONS=1000):
        """
        Write random values to the Patgen scratch registers (8) and verify the write with a read back.

        **Test:**

        1. Using HIL API (`hil.hbiMbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiMbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        board_name = 'PATGEN'
        board = self.env.hbicc.pat_gen

        if not board:
            raise unittest.SkipTest('HBI {} Not Found'.format(board_name.upper()))

        register_errors = self.scratch_register_test_scenario(TEST_ITERATIONS, board)

        if register_errors:
            self.report_errors(board, register_errors)
        else:
            board.log_device_message(
                'Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(TEST_ITERATIONS), 'info')

    def DirectedBasicRingMultiplierScratchRegisterTest(self, TEST_ITERATIONS=1000):
        """
        Write random values to the RingMultiplier scratch registers (8) and verify the write with a read back.

        **Test:**

        1. Using HIL API (`hil.hbiMbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiMbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        board_name = 'RINGMULTIPLIER'
        board = self.env.hbicc.ring_multiplier

        if not board:
            raise unittest.SkipTest('HBI {} Not Found'.format(board_name.upper()))

        register_errors = self.scratch_register_test_scenario(TEST_ITERATIONS, board)

        if register_errors:
            self.report_errors(board, register_errors)
        else:
            board.log_device_message(
                'Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(TEST_ITERATIONS), 'info')

    def DirectedBasicPSDB0PinMultiplier0ScratchRegisterTest(self, TEST_ITERATIONS=1000):
        """
        Write random values to the PSDB_0_0 scratch registers (8) and verify the write with a read back.

        **Test:**

        1. Using HIL API (`hil.hbiPsdbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiPsdbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        board_name = 'PINMULTIPLIER'
        board = self.env.hbicc.psdb_0_pin_0

        if not board:
            raise unittest.SkipTest('HBI {} Not Found'.format(board_name.upper()))

        register_errors = self.scratch_register_test_scenario(TEST_ITERATIONS, board)

        if register_errors:
            self.report_errors(board, register_errors)
        else:
            board.log_device_message(
                'Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(TEST_ITERATIONS), 'info')

    def DirectedBasicPSDB0PinMultiplier1ScratchRegisterTest(self, TEST_ITERATIONS=1000):
        """
        Write random values to the PSDB_0_1 scratch registers (8) and verify the write with a read back.

        **Test:**

        1. Using HIL API (`hil.hbiPsdbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiPsdbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        board_name = 'PINMULTIPLIER'
        board = self.env.hbicc.psdb_0_pin_1

        if not board:
            raise unittest.SkipTest('HBI {} Not Found'.format(board_name.upper()))

        register_errors = self.scratch_register_test_scenario(TEST_ITERATIONS, board)

        if register_errors:
            self.report_errors(board, register_errors)
        else:
            board.log_device_message(
                'Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(TEST_ITERATIONS), 'info')

    def DirectedBasicPSDB1PinMultiplier0ScratchRegisterTest(self, TEST_ITERATIONS=1000):
        """
        Write random values to the PSDB_1_0 scratch registers (8) and verify the write with a read back.

        **Test:**

        1. Using HIL API (`hil.hbiPsdbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiPsdbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        board_name = 'PINMULTIPLIER'
        board = self.env.hbicc.psdb_1_pin_0

        if not board:
            raise unittest.SkipTest('HBI {} Not Found'.format(board_name.upper()))

        register_errors = self.scratch_register_test_scenario(TEST_ITERATIONS, board)

        if register_errors:
            self.report_errors(board, register_errors)
        else:
            board.log_device_message(
                'Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(TEST_ITERATIONS), 'info')

    def DirectedBasicPSDB1PinMultiplier1ScratchRegisterTest(self, TEST_ITERATIONS=1000):
        """
        Write random values to the PSDB_1_1 scratch registers (8) and verify the write with a read back.

        **Test:**

        1. Using HIL API (`hil.hbiPsdbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiPsdbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        board_name = 'PINMULTIPLIER'
        board = self.env.hbicc.psdb_1_pin_1

        if not board:
            raise unittest.SkipTest('HBI {} Not Found'.format(board_name.upper()))

        register_errors = self.scratch_register_test_scenario(TEST_ITERATIONS, board)

        if register_errors:
            self.report_errors(board, register_errors)
        else:
            board.log_device_message(
                'Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(TEST_ITERATIONS), 'info')

    def scratch_register_test_scenario(self, TEST_ITERATIONS, board):
        register_errors = []
        for index in range(REGCOUNT):
            error_count = []
            for iteration in range(TEST_ITERATIONS):
                expected = self.write_random_value_to_scratch_register(board, index)
                observed = self.read_scratch_register(board, index)

                self.compare_expected_with_observed(index, expected, observed, error_count, iteration)
                register_errors.extend(error_count)

                if len(register_errors) >= MAX_FAIL_COUNT:
                    break
        return register_errors

    def write_random_value_to_scratch_register(self, board, index):
        expected = random.randrange(0, 0xFFFFFFFF)
        board.write_bar_register(board.registers.SCRATCH_REG(value=expected), index=index)
        return expected

    def read_scratch_register(self, board, index):
        return board.read_slice_register(board.registers.SCRATCH_REG, index=index).value

    def compare_expected_with_observed(self, index, expected, observed, error_count, iteration):
        if expected != observed:
            error_count.append(
                {'Iteration': iteration, 'Index': index, 'Expected': hex(expected), 'Observed': hex(observed)})

    def report_errors(self, device, register_errors):
        column_headers = ['Iteration', 'Index', 'Expected', 'Observed']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('Scratchpad writes failed'.format(table), 'error')


class Interface(HbiccTest):

    @expected_failure('HDMT Ticket 126737:HBICC: ScratchRegisters.Interface.DirectedPatGenCheckRegisterTest failure on missing registers in current released image.')
    def DirectedPatGenCheckRegisterTest(self):
        board = self.env.hbicc.pat_gen

        if not board:
            raise unittest.SkipTest(f'HBI Pat Gen Not Found')

        registers = self.get_all_registers(board.registers)
        failures = self.register_read_write_read(board, registers)

        if failures:
            self.log_failures(board, failures, f'{board.name()} Registers Read-Write-Verify Results')

    def DirectedRingMultiplierCheckRegisterTest(self):
        board = self.env.hbicc.ring_multiplier

        if not board:
            raise unittest.SkipTest(f'HBI Ring Multiplier Not Found')

        registers = self.get_all_registers(board.registers)
        failures = self.register_read_write_read(board, registers)

        if failures:
            self.log_failures(board, failures, f'{board.name()} Registers Read-Write-Verify Results')

    def DirectedPinMultiplierCheckRegisterTest(self):
        pms = [self.env.hbicc.psdb_0_pin_0, self.env.hbicc.psdb_0_pin_1, self.env.hbicc.psdb_1_pin_0,
               self.env.hbicc.psdb_1_pin_1]

        for pm in pms:
            if pm:
                registers = self.get_all_registers(pm.registers)
                failures = self.register_read_write_read(pm, registers)

                if failures:
                    self.log_failures(pm, failures, f'{pm.name()} Registers Read-Write-Verify Results')

    def register_read_write_read(self, board, registers):
        failures = []
        for register in registers:
            if register().REGCOUNT > 0:
                for index in range(register().REGCOUNT):
                    failures += self.check_register(board, register, index)
            else:
                failures += self.check_register(board, failures, register)
        return failures

    def check_register(self, board, register, index=0):
        entry = []
        original, rand_value, read_back = self.read_modify_write(board, index, register)
        if rand_value != read_back:
            entry.append({'FPGA': board.name(), 'Name': register().name(), 'ADDR': f'0x{register().ADDR +(index*4):X}',
                          'Read': f'0x{original:X}',
                          'Write': f'0x{rand_value:X}', 'Readback': f'0x{read_back:X}'})

        board.write_bar_register(register(value=original), index=index)
        return entry

    def read_modify_write(self, board, index, register):
        original_value = (board.read_slice_register(register, index=index)).value
        rand_value = random.randint(0, 0xDEAD)
        board.write_bar_register(register(value=rand_value), index=index)
        readback_value = (board.read_slice_register(register, index=index)).value
        return original_value, rand_value, readback_value

    def get_all_registers(self, all_registers):
        registers = []
        for name, obj in inspect.getmembers(all_registers):
            try:
                if obj().CHECK:
                    registers.append(obj)
            except:
                pass
        return registers

    def log_failures(self, board, data, title):
        table = board.contruct_text_table(data=data, title_in=title)
        self.Log('error', f'{table}')
    

