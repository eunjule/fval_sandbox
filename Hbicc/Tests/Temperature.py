# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Hbicc.Tests.HbiccTest import HbiccTest
from time import sleep
from Common import configs
import unittest
import time

PSDB_0_0 = 50
PSDB_0_1 = 50
PSDB_1_0 = 50
PSDB_1_1 = 50

PATGEN = 40
RINGMULTIPLIER = 40
MIN_MULTIPLIER = 30
MAX_MULTIPLIER = 25

MAX_FAIL_COUNT = 10
SAMPLE_SIZE = 11
MAX_DIFF = 2

MEAN_TEMPERATURE = {'PATGEN': {'MEAN': PATGEN,
                               'MIN': PATGEN - MIN_MULTIPLIER,
                               'MAX': PATGEN + MAX_MULTIPLIER},
                    'RINGMULTIPLIER': {'MEAN': RINGMULTIPLIER,
                                       'MIN': RINGMULTIPLIER - MIN_MULTIPLIER,
                                       'MAX': RINGMULTIPLIER + MAX_MULTIPLIER},
                    'PINMULTIPLIER': {
                        0: {'MEAN': PSDB_0_0,
                            'MIN': PSDB_0_0 - MIN_MULTIPLIER,
                            'MAX': PSDB_0_0 + MAX_MULTIPLIER},
                        1: {'MEAN': PSDB_0_1,
                            'MIN': PSDB_0_1 - MIN_MULTIPLIER,
                            'MAX': PSDB_0_1 + MAX_MULTIPLIER},
                        2: {'MEAN': PSDB_1_0,
                            'MIN': PSDB_1_0 - MIN_MULTIPLIER,
                            'MAX': PSDB_1_0 + MAX_MULTIPLIER},
                        3: {'MEAN': PSDB_1_1,
                            'MIN': PSDB_1_1 - MIN_MULTIPLIER,
                            'MAX': PSDB_1_1 + MAX_MULTIPLIER}}
                    }


class Diagnostics(HbiccTest):

    def DirectedPatgenInternalTemperatureReadTest(self):
        """
        Collect temperature data using two different methods, HIL API and direct register read. The mean temperature value
        will be characterized using the data collected from this test.

        **Test:**

        * Using HIL API, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%
        * Using direct register read, poll FPGA temperature 10000 times. Sample to sample variability should be mean value +/- 5%

        **Criteria:** Sample to sample variability and HIL polling method to register polling method should be mean value +/- 5%

        **NOTE:** `HIL API provides decimal temperature in degrees. For Stratix10, the signed 32-bit register value is broken
        into two parts. Signed integer part are bits 31-8 and the remaining bits represent the decimal part. Ex: 0xFFFFE1C0 is -30.25°C.`

        """

        pat_gen = self.env.hbicc.pat_gen

        if not pat_gen:
            raise unittest.SkipTest('Pat Gen Device Not Found')

        hil_samples = []
        bar_read_samples = []
        failures = []

        for i in range(SAMPLE_SIZE):
            hil_temperature = self.poll_hil_temperature(pat_gen, hil_samples)
            bar_read_temperature = self.poll_bar_read_temperature(pat_gen, bar_read_samples)
            self.compare_temperature(pat_gen, hil_temperature, bar_read_temperature, failures, i)
            if len(failures) > MAX_FAIL_COUNT:
                self.log_failures(pat_gen, failures)
                break

        self.report_sample_average(pat_gen, hil_samples, 'HIL')
        self.report_sample_average(pat_gen, bar_read_samples, 'BAR Read')

    def DirectedRingMultiplierInternalTemperatureReadTest(self):
        """
        Collect temperature data using two different methods, HIL API and direct register read. The mean temperature value
        will be characterized using the data collected from this test.

        **Test:**

        * Using HIL API, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%
        * Using direct register read, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%

        **Criteria:** Sample to sample variability and HIL polling method to register polling method should be mean +/- 5%

        **NOTE:** `HIL API provides decimal temperature in degrees. For Stratix10, the signed 32-bit register value is broken
        into two parts. Signed integer part are bits 31-8 and the remaining bits represent the decimal part. Ex: 0xFFFFE1C0 is -30.25°C.`

        """
        ring_multiplier = self.env.hbicc.ring_multiplier

        if not ring_multiplier:
            raise unittest.SkipTest('Pat Gen Device Not Found')

        hil_samples = []
        bar_read_samples = []
        failures = []

        for i in range(SAMPLE_SIZE):
            hil_temperature = self.poll_hil_temperature(ring_multiplier, hil_samples)
            bar_read_temperature = self.poll_bar_read_temperature(ring_multiplier, bar_read_samples)
            self.compare_temperature(ring_multiplier, hil_temperature, bar_read_temperature, failures, i)
            if len(failures) > MAX_FAIL_COUNT:
                self.log_failures(ring_multiplier, failures)
                break

        self.report_sample_average(ring_multiplier, hil_samples, 'HIL')
        self.report_sample_average(ring_multiplier, bar_read_samples, 'BAR Read')

    def DirectedPSDB0PinMultiplier0InternalTemperatureReadTest(self):
        """
        Collect temperature data using two different methods, HIL API and direct register read. The mean temperature value
        will be characterized using the data collected from this test.

        **Test:**

        * Using HIL API, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%
        * Using direct register read, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%

        **Criteria:** Sample to sample variability and HIL polling method to register polling method should be mean +/- 5%

        **NOTE:** `HIL API provides decimal temperature in degrees. For Arria10, the following conversion will be used:
        temperature = {(AxC)÷1024} - B, where A = 693, B = 265, and C = decimal value of register tempout[9..0].`

        """
        psdb_0_pin_0 = self.env.hbicc.psdb_0_pin_0

        if not psdb_0_pin_0:
            raise unittest.SkipTest('Pat Gen Device Not Found')

        hil_samples = []
        bar_read_samples = []
        failures = []

        for i in range(SAMPLE_SIZE):
            time.sleep(.1)
            hil_temperature = self.poll_hil_temperature(psdb_0_pin_0, hil_samples)
            bar_read_temperature = self.poll_bar_read_temperature(psdb_0_pin_0, bar_read_samples)
            self.compare_temperature(psdb_0_pin_0, hil_temperature, bar_read_temperature, failures, i)
            if len(failures) > MAX_FAIL_COUNT:
                self.log_failures(psdb_0_pin_0, failures)
                break

        self.report_sample_average(psdb_0_pin_0, hil_samples, 'HIL')
        self.report_sample_average(psdb_0_pin_0, bar_read_samples, 'BAR Read')

    def DirectedPSDB0PinMultiplier1InternalTemperatureReadTest(self):
        """
        Collect temperature data using two different methods, HIL API and direct register read. The mean temperature value
        will be characterized using the data collected from this test.

        **Test:**

        * Using HIL API, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%
        * Using direct register read, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%

        **Criteria:** Sample to sample variability and HIL polling method to register polling method should be mean +/- 5%

        **NOTE:** `HIL API provides decimal temperature in degrees. For Arria10, the following conversion will be used:
        temperature = {(AxC)÷1024} - B, where A = 693, B = 265, and C = decimal value of register tempout[9..0].`

        """
        psdb_0_pin_1 = self.env.hbicc.psdb_0_pin_1

        if not psdb_0_pin_1:
            raise unittest.SkipTest('Pat Gen Device Not Found')

        hil_samples = []
        bar_read_samples = []
        failures = []

        for i in range(SAMPLE_SIZE):
            hil_temperature = self.poll_hil_temperature(psdb_0_pin_1, hil_samples)
            bar_read_temperature = self.poll_bar_read_temperature(psdb_0_pin_1, bar_read_samples)
            self.compare_temperature(psdb_0_pin_1, hil_temperature, bar_read_temperature, failures, i)
            if len(failures) > MAX_FAIL_COUNT:
                self.log_failures(psdb_0_pin_1, failures)
                break

        self.report_sample_average(psdb_0_pin_1, hil_samples, 'HIL')
        self.report_sample_average(psdb_0_pin_1, bar_read_samples, 'BAR Read')

    def DirectedPSDB1PinMultiplier0InternalTemperatureReadTest(self):
        """
        Collect temperature data using two different methods, HIL API and direct register read. The mean temperature value
        will be characterized using the data collected from this test.

        **Test:**

        * Using HIL API, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%
        * Using direct register read, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%

        **Criteria:** Sample to sample variability and HIL polling method to register polling method should be mean +/- 5%

        **NOTE:** `HIL API provides decimal temperature in degrees. For Arria10, the following conversion will be used:
        temperature = {(AxC)÷1024} - B, where A = 693, B = 265, and C = decimal value of register tempout[9..0].`

        """

        psdb_1_pin_0 = self.env.hbicc.psdb_1_pin_0

        if not psdb_1_pin_0:
            raise unittest.SkipTest('Pat Gen Device Not Found')

        hil_samples = []
        bar_read_samples = []
        failures = []

        for i in range(SAMPLE_SIZE):
            hil_temperature = self.poll_hil_temperature(psdb_1_pin_0, hil_samples)
            bar_read_temperature = self.poll_bar_read_temperature(psdb_1_pin_0, bar_read_samples)
            self.compare_temperature(psdb_1_pin_0, hil_temperature, bar_read_temperature, failures, i)
            if len(failures) > MAX_FAIL_COUNT:
                self.log_failures(psdb_1_pin_0, failures)
                break

        self.report_sample_average(psdb_1_pin_0, hil_samples, 'HIL')
        self.report_sample_average(psdb_1_pin_0, bar_read_samples, 'BAR Read')

    def DirectedPSDB1PinMultiplier1InternalTemperatureReadTest(self):
        """
        Collect temperature data using two different methods, HIL API and direct register read. The mean temperature value
        will be characterized using the data collected from this test.

        **Test:**

        * Using HIL API, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%
        * Using direct register read, poll FPGA temperature 10000 times. Sample to sample variability should be mean +/- 5%

        **Criteria:** Sample to sample variability and HIL polling method to register polling method should be mean +/- 5%

        **NOTE:** `HIL API provides decimal temperature in degrees. For Arria10, the following conversion will be used:
        temperature = {(AxC)÷1024} - B, where A = 693, B = 265, and C = decimal value of register tempout[9..0].`

        """
        psdb_1_pin_1 = self.env.hbicc.psdb_1_pin_1

        if not psdb_1_pin_1:
            raise unittest.SkipTest('Pat Gen Device Not Found')

        hil_samples = []
        bar_read_samples = []
        failures = []

        for i in range(SAMPLE_SIZE):
            hil_temperature = self.poll_hil_temperature(psdb_1_pin_1, hil_samples)
            bar_read_temperature = self.poll_bar_read_temperature(psdb_1_pin_1, bar_read_samples)
            self.compare_temperature(psdb_1_pin_1, hil_temperature, bar_read_temperature, failures, i)
            if len(failures) > MAX_FAIL_COUNT:
                self.log_failures(psdb_1_pin_1, failures)
                break

        self.report_sample_average(psdb_1_pin_1, hil_samples, 'HIL')
        self.report_sample_average(psdb_1_pin_1, bar_read_samples, 'BAR Read')

    def poll_bar_read_temperature(self, device, register_samples):
        bar_read_temperature = device.temperature_using_bar_read()
        register_samples.append(bar_read_temperature)
        return bar_read_temperature

    def poll_hil_temperature(self, device, hil_samples):
        hil_temperature = device.temperature_using_hil()
        hil_samples.append(hil_temperature)
        return hil_temperature

    def compare_temperature(self, device, hil_temp, bar_temp, failures, sample_number):
        max, mean, min = self.get_limits(device)
        if self.is_entry_outside_limits(hil_temp, device):
            failures.append({'Sample': sample_number, 'Expected (C)': mean, 'Min (C)': min, 'Max (C)': max, 'Temperature (C)': hil_temp,
                             'Reason': 'HIL API Read Failure'})
        if self.is_entry_outside_limits(bar_temp, device):
            failures.append({'Sample': sample_number, 'Expected (C)': mean, 'Min (C)': min, 'Max (C)': max, 'Temperature (C)': bar_temp, 'Reason': 'BAR Read Failure'})

        if self.hil_bar_read_exceed_difference(hil_temp, bar_temp):
            failures.append({'Sample': sample_number, 'Expected (C)': mean, 'Min (C)': min, 'Max (C)': max, 'Temperature (C)': f'Bar: {bar_temp} HIL: {hil_temp}', 'Reason': f'Difference Between HIL and BAR Read is greater than {MAX_DIFF} (C)'})

    def hil_bar_read_exceed_difference(self, hil_temp, bar_temp):
        temp_difference = abs(hil_temp - bar_temp)
        if temp_difference > MAX_DIFF:
            return True
        return False

    def is_entry_outside_limits(self, observed, device):
        max, mean, min = self.get_limits(device)
        if not (min < observed < max):
            return True
        return False

    def get_limits(self, device):
        if 'PSDB' in device.name():
            min = MEAN_TEMPERATURE["PINMULTIPLIER"][device.slot_index]['MIN']
            max = MEAN_TEMPERATURE["PINMULTIPLIER"][device.slot_index]['MAX']
            mean = MEAN_TEMPERATURE["PINMULTIPLIER"][device.slot_index]['MEAN']
        else:
            min = MEAN_TEMPERATURE[device.name()]['MIN']
            max = MEAN_TEMPERATURE[device.name()]['MAX']
            mean = MEAN_TEMPERATURE[device.name()]['MEAN']
        return max, mean, min

    def report_sample_average(self, device, samples, method):
        addition = sum(samples)
        size = len(samples)
        average = addition / size
        message = '{} Average temperature is {}.'.format(method, average)
        device.log_device_message(message, 'info')

    def log_failures(self, device, failures):
        table = device.contruct_text_table(data=failures)
        device.log_device_message('FPGA Temperature Failed {}'.format(table), 'error')
