# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

""" Tests for Resetting different values for read 32 interval in PG and PM.

The default value is 32 and it can range upto 1024.
Purpose was to free up bandwidth by minimising the PM traffic.
Test some of the existing ctv/fail capture test (smaller pattern size with read 128,256)
to check if it still functions fine without any issues
Increase pattern size (scale one of the high volume ctv tests x10,x100) and check if it consistently fails with 128
then change the interval to 256/512/1024 and see check if the test passes
The read 32 counts can also be calculated for a given vector size by using the compression formula used in PMs and be
verified against from the actual number received in PG.
"""
import itertools
import random

from Common.fval import skip, skip_snapshot, deactivate_simulator
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

PADDING = 1024
SLICECOUNT = range(0, 5)


class Functional(HbiccTest):

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = random.randint(0, 0xFFFFFFFF)
        self.ctp = random.randint(1, 0b11111111111111111111111111111111111)
        self.id_0= random.randint(0, 100)
        self.slices=  [0, 1, 2, 3, 4]

    def Directed1kCtvRead128Test(self):
        """Test existing CTv test with read 128 as the interval and check for expected behavior"""
        repeat = 1024
        ctp = 0b00000000000000000001001000000000010
        self.pattern_helper.e32_interval=128
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                        PatternId {self.id_0} 
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:
                        NoCompareDriveZVectors length={PADDING}
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            AddressVectorsPlusAtt length={repeat}
                        Return
                        '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()


    def DirectedThreeFailsInterleaveRead256Test(self):
        """Test existing Fail capture test with read 256 as the interval and check for expected behavior"""
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.e32_interval= 256
        ctp = 0b00000000000000000001001000000000010
        random_repeats = [random.randint(1000, 5000) for x in range(11)]
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data={ctp}

                         PatternId {self.id_0}                                                 
                         PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:                
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                            CompareLowVectors length={random_repeats[random.randint(0, 10)]}
                            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                            CompareLowVectors length={random_repeats[random.randint(0, 10)]}
                            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                            CompareLowVectors length={random_repeats[random.randint(0, 10)]}                                        
                        Return
                        '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def RandomIntervalwithCtvFailCaptureTest(self):
        """Test Ctv and  Fail capture test(within bandwidth limitiation) with random read 32 interval """

        self.slices = [0, 1, 2, 3]
        pattern_sizes = [random.randint(1024, 2048) for x in range(4)]
        read32_intervals = random.choices([x for x in range(32, 1025, 32)], k=5)

        for repeat in pattern_sizes:
            self.env.hbicc.model.patgen.skip_multi_burst_simulation = False
            attributes = {'capture_fails': 0,
                          'capture_ctv': 1}

            setting_string = f'Fails: {bool(attributes["capture_fails"])}, ' \
                             f'CTV: {bool(attributes["capture_ctv"])}'

            for e32_interval in read32_intervals:
                with self.subTest(CaptureSettings=f'{setting_string} Capture Vectors: {repeat}, e32: {e32_interval}.'):
                    self.pattern_helper.e32_interval = e32_interval

                    pattern_string = f'''
                            PATTERN_START:
                            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
        
                            PatternId 0x100 
                            PCall PATTERN1
        
                            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                            DriveZeroVectors length=1

                            PATTERN1:
                                NoCompareDriveZVectors length=1024
                                AddressVectorsPlusAtt length={repeat}
                                Return
                            '''
                    self.Log('debug', pattern_string)
                    self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,
                                                                               slices=self.slices,
                                                                           attributes=attributes)
                    self.pattern_helper.execute_pattern_scenario()
                    self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

    def DirectedLargeIntervalwithCtvFailCaptureTest(self):
        """Test Ctv and  Fail capture test(within bandwidth limitiation) with random read 32 interval """

        self.slices = [0, 1, 2, 3]
        e32_interval = random.choice([1 << x for x in range(7, 11)])
        self.pattern_helper.e32_interval = e32_interval

        repeat = 2000
        loops = 100
        self.ctp = 0b00000000000000000001001000000000010

        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loops}

                PatternId 100 
                PCall LOOP

                LOOP:
                    AddressVectorsPlusAtt length={repeat}
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    @skip_snapshot
    @deactivate_simulator
    def DirectedHighVolumeCtvMaxRead32valueTest(self):
        """Test high volume Ctv with a value of read 32 that works without causing under runs"""

        self.pattern_helper.is_capture_active = False
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.user_mode = 'LOW'

        self.slices = [0, 1, 2, 3]
        self.pattern_helper.e32_interval = 1024

        repeat = 10000
        loops = 10000
        total_ctv_vectors = repeat * loops
        expected_e32_count = total_ctv_vectors//self.pattern_helper.e32_interval
        id_0 = random.randint(0, 100)
        self.ctp = 0b00000000000000000001001000000000010
        self.pattern_helper.end_status = self.end_status
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loops}

                PatternId {id_0} 
                PCall LOOP

                LOOP:
                    CompareHighVectors length={repeat}, ctv=1
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

        expected_ctv_header_count = repeat * loops // 4
        self.env.hbicc.pat_gen.check_ctv_header_count(expected=expected_ctv_header_count, slices=self.slices)

        cycles_per_ctv_block = 8
        default_pg_packing = 4
        expected_ctv_blocks = (repeat * loops // cycles_per_ctv_block) * \
                              len(self.env.hbicc.get_pin_multipliers()) + default_pg_packing
        self.env.hbicc.pat_gen.check_ctv_data_block_count(expected=expected_ctv_blocks, slices=self.slices)
        self.env.hbicc.pat_gen.check_e32_count(expected_e32_count)

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.check_h_counter(expected=total_ctv_vectors, slices=self.slices)

    def DirectedVerifyRead32packetsReceivedFor10kPatternlengthTest(self):
        """Calculate the number of read 32 packets received for a given pattern length and verify with the value
        in the PG register"""

        self.slices = [0, 1, 2, 3]
        repeat = 10000
        e32_interval = random.choice([1 << x for x in range(7, 11)])
        self.pattern_helper.e32_interval = e32_interval
        expected_e32_count =  repeat//e32_interval
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                PatternId 100 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length={repeat}

                    Return
                '''

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

        self.env.hbicc.pat_gen.check_e32_count(expected_e32_count)
