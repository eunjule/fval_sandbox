# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
import random
from time import perf_counter
from Hbicc.Tests.HbiccTest import HbiccTest

MAX_FAIL_COUNT = 40
NUMBER_OF_MICROSECONDS_IN_A_SECOND = 1000000


class Diagnostics(HbiccTest):
    TEST_ITERATIONS = 1000000

    def setUp(self, tester=None):
        super().setUp()
        self.rc = self.env.hbicc.rc
        self.pg = self.env.hbicc.pat_gen
        self.rm = self.env.hbicc.ring_multiplier

    def DirectedRCToPatgenTest(self):
        """
        Send down trigger from RC to Patgen with correct payload and ensure Patgen received it.
        Repeat the process 10000 times.

        """
        self.trigger_test(sender=self.env.hbicc.rc, receiver=self.env.hbicc.pat_gen)

    def DirectedRCToRingMultiplierTest(self):
        """
        Send down trigger from RC to RingMultiplier with correct payload and ensure RingMultiplier received it.
        Repeat the process 10000 times

        """
        self.trigger_test(sender=self.env.hbicc.rc, receiver=self.env.hbicc.ring_multiplier)

    def DirectedPatgenToRCTest(self):
        """
        Send up trigger from Patgen to RC and verify if RC received it.
        Repeat the process 10000 times

        """
        self.trigger_test(sender=self.env.hbicc.pat_gen, receiver=self.env.hbicc.rc)
        

    def DirectedRingMultiplierToRCTest(self):
        """
        Send up trigger from RingMultiplier to RC and verify if RC received it.
        Repeat the process 10000 times

        """
        self.trigger_test(sender=self.env.hbicc.ring_multiplier, receiver=self.env.hbicc.rc)

    def DirectedTriggerDeliveryLatencyTest(self):
        """Verify random trigger delivery latency is within threshold for various device pairs and link directions

        Trigger delivery latency (down link) is reported as a mean value of latencies calculated over 1000000
        iterations.
        Trigger delivery latencies are calculated for the device pairs PG-RC and RM-RC in both directions.
        """
        trigger_links = {'PG->RC': {'sender': self.pg, 'receiver': self.rc},
                         'RC->PG': {'sender': self.rc, 'receiver': self.pg},
                         'RM->RC': {'sender': self.rm, 'receiver': self.rc},
                         'RC->RM': {'sender': self.rc, 'receiver': self.rm}}
        trigger_delivery_latency_thresholds_microseconds = {'PG->RC': 100, 'RC->PG': 100, 'RM->RC': 100, 'RC->RM': 100}
        for trigger_link in trigger_links:
            trigger_delivery_latencies_microseconds = self.send_random_triggers_for_latency_measure(
                sender=trigger_links[trigger_link]["sender"], receiver=trigger_links[trigger_link]["receiver"])
            mean_trigger_delivery_latency_microseconds = \
                sum(trigger_delivery_latencies_microseconds) / len(trigger_delivery_latencies_microseconds)
            if mean_trigger_delivery_latency_microseconds <= \
                    trigger_delivery_latency_thresholds_microseconds[trigger_link]:
                self.Log('info',
                         f'Trigger Link: {trigger_link} | '
                         f'Mean Trigger Delivery Latency ({mean_trigger_delivery_latency_microseconds} microseconds) '
                         f'is within expected range')
            else:
                self.Log('error',
                         f'Trigger Link: {trigger_link} | '
                         f'Mean Trigger Delivery Latency ({mean_trigger_delivery_latency_microseconds} microseconds) '
                         f'is outside expected range')
        
    def trigger_test(self, sender, receiver):
        self.env.hbicc.pat_gen.deactive_master_trigger_control_register()
        self.disable_rm_aurora_bus(receiver, sender)
        link_partner = self.get_rc_link_partner(receiver, sender)
        index = self.env.hbicc.rc.get_aurora_link_number(link_partner)
        fail_count = 0
        total_poll_count = 0
        self.env.hbicc.rc.reset_link_error_count_before_link_reset(link_partner)
        for loop in range(self.TEST_ITERATIONS):
            random_trigger_from_rc = self.generate_non_defined_card_type_random_trigger()
            received_trigger, poll_count = self.env.hbicc.rc.send_trigger_from_sender_to_receiver(
                sender, index, random_trigger_from_rc, receiver)
            if received_trigger != random_trigger_from_rc:
                fail_count += 1
                self.Log('error', 'Iteration {} Trigger failed to match.Expected 0x{:x} Received 0x{:x}'.
                         format(loop, random_trigger_from_rc, received_trigger))
            total_poll_count += poll_count
            if fail_count == MAX_FAIL_COUNT:
                self.report_trigger_link_details(index, sender, receiver)
                break
        self.Log('info', f'average poll count = {total_poll_count / (loop + 1)}')

    def send_random_triggers_for_latency_measure(self, sender, receiver):
        self.pg.deactive_master_trigger_control_register()
        rc_device = self.rc
        self.disable_rm_aurora_bus(receiver, sender)
        link_partner = self.get_rc_link_partner(receiver, sender)
        index = rc_device.get_aurora_link_number(link_partner)
        fail_count = 0
        total_poll_count = 0
        trigger_delivery_latencies_microseconds = []
        rc_device.reset_link_error_count_before_link_reset(link_partner)
        for loop in range(self.TEST_ITERATIONS):
            random_trigger = self.get_random_valid_trigger()
            trigger_send_time_microseconds = perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND
            received_trigger, poll_count = rc_device.send_trigger_from_sender_to_receiver(sender, index,
                                                                                          random_trigger,
                                                                                          receiver)
            trigger_received_time_microseconds = perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND
            trigger_delivery_delay_microseconds = trigger_received_time_microseconds - trigger_send_time_microseconds
            if received_trigger != random_trigger:
                fail_count += 1
                self.Log('error', f'Iteration {loop} Trigger sent did not match with trigger received within '
                                  f'{poll_count} polls. Sent: 0x{random_trigger:x} Received: 0x{received_trigger:x}')
            trigger_delivery_latencies_microseconds.append(trigger_delivery_delay_microseconds)
            total_poll_count += poll_count
            if fail_count == MAX_FAIL_COUNT:
                self.report_trigger_link_details(index, sender, receiver)
                break
        self.Log('debug', f'average poll count = {total_poll_count / (loop + 1)}')
        return trigger_delivery_latencies_microseconds

    @staticmethod
    def get_random_valid_trigger():
        return random.getrandbits(32) & 0x03F7FFFF

    def disable_rm_aurora_bus(self, receiver, sender):
        if sender == self.env.hbicc.ring_multiplier or receiver == self.env.hbicc.ring_multiplier:
            if sender == self.env.hbicc.ring_multiplier:
                sender.write_dc_trigger_enable_aurora_data(0)
            else:
                receiver.write_dc_trigger_enable_aurora_data(0)

    def get_rc_link_partner(self, receiver, sender):
        if self.rc == sender:
            link_partner = receiver
        else:
            link_partner = sender
        return link_partner

    def report_trigger_link_details(self, trigger_link_index, sender, receiver):
        self.log_device_trigger_link_details(trigger_link_index, sender, 'Sender')
        self.log_device_trigger_link_details(trigger_link_index, receiver, 'Receiver')

    def log_device_trigger_link_details(self, trigger_link_index, device, device_type):
        device_trigger_link_up, device_aurora_status, device_aurora_error_count = \
            device.is_aurora_link_up(trigger_link_index)
        self.Log('debug', f'{device_type}: {device.name()} | '
                          f'{device_type} Trigger Link Index: {trigger_link_index} | '
                          f'{device_type} Trigger Link Up Status: {device_trigger_link_up} | '
                          f'{device_type} Aurora Status: {device_aurora_status} | '
                          f'{device_type} Aurora Error Count: {device_aurora_error_count}')

    def generate_non_defined_card_type_random_trigger(self):
        random_trigger = self.env.hbicc.pat_gen.registers.SEND_TRIGGER()
        random_trigger.trigger_type = random.getrandbits(4)
        random_trigger.count_value = random.getrandbits(4)
        random_trigger.target_domain = random.getrandbits(4)
        random_trigger.reserved = random.getrandbits(7)
        random_trigger.sw_type_trigger = 0
        random_trigger.dut_domain_id = random.getrandbits(6)
        random_trigger.card_type = 0x3
        return random_trigger.value


class SoftwareTrigger(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.device = None
        self.expected_source = None
        self.TEST_ITERATIONS = 1_000_000
        self.rc = self.env.hbicc.rc
        self.rc.clear_sw_trigger_fifo()
        self.errors = []

    def DirectedPatGenToRCSoftwareTriggerTest(self):
        """
        Send up software trigger from RingMultiplier to RC and verify if RC received it.
        Repeat the process 10000 times

        """
        self.expected_source = 0x10
        self.device = self.env.hbicc.pat_gen
        self.execute_software_triggers(software_trigger_up=1, target_resource=0)
        self.report_errors()

    def DirectedPatGenToRCSoftwareTriggerViaTargetResourceTest(self):
        """
        Send up software trigger from RingMultiplier to RC and verify if RC received it.
        Repeat the process 10000 times

        """
        self.expected_source = 0x10
        self.device = self.env.hbicc.pat_gen
        self.execute_software_triggers(software_trigger_up=0, target_resource=2)
        self.report_errors()

    def DirectedRMToRCSoftwareTriggerTest(self):
        """
        Send up software trigger from RingMultiplier to RC and verify if RC received it.
        Repeat the process 10000 times

        """
        self.expected_source = 0x11
        self.device = self.env.hbicc.ring_multiplier
        self.execute_software_triggers(software_trigger_up=1, target_resource=0)
        self.report_errors()

    def DirectedRMToRCSoftwareTriggerViaTargetResourceTest(self):
        """
        Send up software trigger from RingMultiplier to RC and verify if RC received it.
        Repeat the process 10000 times

        """
        self.expected_source = 0x11
        self.device = self.env.hbicc.ring_multiplier
        self.execute_software_triggers(software_trigger_up=0, target_resource=2)
        self.report_errors()

    def execute_software_triggers(self, software_trigger_up, target_resource):
        for i in range(self.TEST_ITERATIONS):
            expected_trigger = self.send_sw_trigger_to_rc(software_trigger_up, target_resource)
            observed_source = self.rc.read_sw_trigger_fifo_source()
            observed_trigger = self.rc.read_sw_trigger_from_rc()
            self.log_error(expected_trigger, observed_trigger, observed_source)
            if len(self.errors) > 10:
                self.Log('error', f'Incomplete Tests. Reach maxiumum fails allowed (10).')
                break

    def log_error(self, expected_trigger, observed_trigger, observed_source):
        if expected_trigger != observed_trigger:
            self.errors.append({'Error Type': 'Trigger Value', 'Expected': f'0x{expected_trigger:08X}',
                           'Observed': f'0x{observed_trigger:08X}'})
        if self.expected_source != observed_source:
            self.errors.append({'Error Type': 'Device ID', 'Expected': f'0x{self.expected_source:08X}',
                           'Observed': f'0x{observed_source:08X}'})

    def report_errors(self):
        if self.errors:
            table = self.env.hbicc.contruct_text_table(data=self.errors, title_in='Software Up Trigger')
            self.Log('error', f'{table}')
        else:
            self.Log('info', f'{self.device.name()} to RC Sofware Up Trigger Passed {self.TEST_ITERATIONS:,} times.')

    def send_sw_trigger_to_rc(self, software_trigger_up, target_resource, value=None):
        value = self.get_random_value(value)
        trigger_reg = self.device.registers.SEND_SW_TRIGGER_UP()
        trigger_reg.software_trigger_up = software_trigger_up
        trigger_reg.target_resource = target_resource
        trigger_reg.index_of_trigger_queue = value
        self.device.write_slice_register(trigger_reg)
        return trigger_reg.value

    def get_random_value(self, value):
        if value is None:
            value = random.randint(0, 0x1ff)
        return value
