# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
from Hbicc.Tests.HbiccTest import HbiccTest
EXPECTED_VOLTAGE = {'VCC':0.95,'VCCP':0.95,'VCCPT':1.8,'VCCERAM':0.95}
voltage_conversion_factor = 19.531e-3
allowable_voltage_margin = 0.5
voltage_multiplier_factor = 2
register_channel_index = 2
MAX_FAIL_COUNT =10


class Diagnostics(HbiccTest):

    def DirectedPSDB0PinMultiplier0VoltageReadFromRegisterTest(self,test_iterations =1000):
        """
        Read the values from PSDB 0 PinMultipier 0 FPGA registers and ensure that they measured within the known range.

        **Test:**

        1. Read VOLTAGE_SENSOR_CHANNEL_2--> VCC,it should read hex code close to 0.95V.
            Read VOLTAGE_SENSOR_CHANNEL_3--> VCCP,it should read hex code close to 0.95V.
            Read VOLTAGE_SENSOR_CHANNEL_4--> VCCPT,it should read close to 0.9V. To get the
            actual VCCPT voltage multiply by 2 (the VCCPT value read internal to the FPGA is divided by two)
            Read VOLTAGE_SENSOR_CHANNEL_5 FPGA registers-->VCCERAM,it should read close to 0.95V.
        2. Compare the reference value to the read back value
        3. Repeat step 1-2 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        if self.env.hbicc.psdb_0_pin_0 is None:
            raise unittest.SkipTest('MB/PSDB 0 unavailable')
        board = self.env.hbicc.psdb_0_pin_0
        read_errors = []

        for channel_index,voltage_type in enumerate(EXPECTED_VOLTAGE,register_channel_index):
            error_count = []
            for iteration in range(test_iterations):
                actual_voltage = self.read_voltage_sensor_register(voltage_type, board, channel_index)

                self.compare_actual_voltage_read_with_expected(voltage_type,EXPECTED_VOLTAGE[voltage_type],actual_voltage,error_count)
                if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= test_iterations:
                    break
            read_errors.extend(error_count)
        if read_errors:
            self.report_errors(board, read_errors)
        else:
            self.Log('info', 'Voltages as received by FPGA were ' +
                            f'successful for {test_iterations} iterations.')


    def DirectedPSDB0PinMultiplier1VoltageReadFromRegisterTest(self,test_iterations=1000):
        """
        Read the values from PSDB 0 PinMultipier 1 FPGA registers and ensure that they measured within the known range.

        **Test:**

        1. Read VOLTAGE_SENSOR_CHANNEL_2--> VCC,it should read hex code close to 0.95V
            Read VOLTAGE_SENSOR_CHANNEL_3--> VCCP,it should read hex code close to 0.95V
            Read VOLTAGE_SENSOR_CHANNEL_4--> VCCPT,it should read close to 0.9V. To get the
            actual VCCPT voltage multiply by 2 (the VCCPT value read internal to the FPGA is divided by two)
            Read VOLTAGE_SENSOR_CHANNEL_5 FPGA registers-->VCCERAM,it should read close to 0.95V.
        2. Compare the reference value to the read back value
        3. Repeat step 1-2 for 50 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise unittest.SkipTest('MB/PSDB 0 unavailable')
        board = self.env.hbicc.psdb_0_pin_1
        read_errors = []
        for channel_index,voltage_type in enumerate(EXPECTED_VOLTAGE,register_channel_index):
            error_count = []
            for iteration in range(test_iterations):
                actual_voltage = self.read_voltage_sensor_register(voltage_type, board, channel_index)

                self.compare_actual_voltage_read_with_expected(voltage_type,EXPECTED_VOLTAGE[voltage_type],actual_voltage,error_count)
                if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= test_iterations:
                    break
            read_errors.extend(error_count)
        if read_errors:
            self.report_errors(board, read_errors)
        else:
            self.Log('info', 'Voltages as received by FPGA were ' +
                            f'successful for {test_iterations} iterations.')

    def DirectedPSDB1PinMultiplier0VoltageReadFromRegisterTest(self,test_iterations =1000):
        """
        Read the values from PSDB 1 PinMultipier 0 FPGA registers and ensure that they measured within the known range.

        **Test:**

        1. Read VOLTAGE_SENSOR_CHANNEL_2--> VCC,it should read hex code close to 0.95V
            Read VOLTAGE_SENSOR_CHANNEL_3--> VCCP,it should read hex code close to 0.95V
            Read VOLTAGE_SENSOR_CHANNEL_4--> VCCPT,it should read close to 0.9V. To get the
            actual VCCPT voltage multiply by 2 (the VCCPT value read internal to the FPGA is divided by two)
            Read VOLTAGE_SENSOR_CHANNEL_5 FPGA registers-->VCCERAM,it should read close to 0.95V.
        2. Compare the reference value to the read back value
        3. Repeat step 1-2 for 50 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """

        if self.env.hbicc.psdb_1_pin_0 is None:
            raise unittest.SkipTest('MB/PSDB 1 unavailable')
        board = self.env.hbicc.psdb_1_pin_0
        read_errors = []
        for channel_index,voltage_type in enumerate(EXPECTED_VOLTAGE,register_channel_index):
            error_count = []
            for iteration in range(test_iterations):
                actual_voltage = self.read_voltage_sensor_register(voltage_type, board, channel_index)

                self.compare_actual_voltage_read_with_expected(voltage_type,EXPECTED_VOLTAGE[voltage_type],actual_voltage,error_count)
                if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= test_iterations:
                    break
            read_errors.extend(error_count)
        if read_errors:
            self.report_errors(board, read_errors)
        else:
            self.Log('info', 'Voltages as received by FPGA were ' +
                            f'successful for {test_iterations} iterations.')


    def DirectedPSDB1PinMultiplier1VoltageReadFromRegisterTest(self,test_iterations=1000):
        """
        Read the values from PSDB 1 PinMultipier 1 FPGA registers and ensure that they measured within the known range.

        **Test:**

        1. Read VOLTAGE_SENSOR_CHANNEL_2--> VCC,it should read hex code close to 0.95V
            Read VOLTAGE_SENSOR_CHANNEL_3--> VCCP,it should read hex code close to 0.95V
            Read VOLTAGE_SENSOR_CHANNEL_4--> VCCPT,it should read close to 0.9V. To get the
            actual VCCPT voltage multiply by 2 (the VCCPT value read internal to the FPGA is divided by two)
            Read VOLTAGE_SENSOR_CHANNEL_5 FPGA registers-->VCCERAM,it should read close to 0.95V.
        2. Compare the reference value to the read back value
        3. Repeat step 1-2 for 50 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:** ``

        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise unittest.SkipTest('MB/PSDB 1 unavailable')
        board = self.env.hbicc.psdb_1_pin_1
        read_errors = []
        for channel_index,voltage_type in enumerate(EXPECTED_VOLTAGE,register_channel_index):
            error_count = []
            for iteration in range(test_iterations):
                actual_voltage = self.read_voltage_sensor_register(voltage_type, board, channel_index)

                self.compare_actual_voltage_read_with_expected(voltage_type,EXPECTED_VOLTAGE[voltage_type],actual_voltage,error_count)
                if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= test_iterations:
                    break
            read_errors.extend(error_count)
        if read_errors:
            self.report_errors(board, read_errors)
        else:
            self.Log('info', 'Voltages as received by FPGA were ' +
                            f'successful for {test_iterations} iterations.')

    def read_voltage_sensor_register(self, voltage_type, board, channel_index):
        voltage_register = 'VOLTAGE_SENSOR_CHANNEL_{}'.format(channel_index)
        voltage_hex_code = board.read_bar_register(getattr(board.registers,voltage_register)).value
        if voltage_type == 'VCCPT':
            return self.convert_hex_code_to_voltage(voltage_hex_code) * voltage_multiplier_factor
        else:
            return self.convert_hex_code_to_voltage(voltage_hex_code)

    def convert_hex_code_to_voltage(self,hex_code):
        return hex_code*voltage_conversion_factor

    def compare_actual_voltage_read_with_expected(self,voltage_type,expected_voltage,actual_voltage,error_count):
        upper_control_limit = expected_voltage+allowable_voltage_margin
        lower_control_limit = expected_voltage-allowable_voltage_margin
        if not (lower_control_limit<=actual_voltage and actual_voltage<=upper_control_limit):
            error_count.append(
                {'Voltage type': voltage_type,'Expected': expected_voltage, 'Observed': actual_voltage})

    def report_errors(self, device, register_errors):
        column_headers = ['Voltage type','Expected', 'Observed']
        table = device.contruct_text_table(column_headers, register_errors)
        self.Log('error', 'Voltage sensor read incorrect voltage/s for ' +
                         f'following type/s: \n {table}')


