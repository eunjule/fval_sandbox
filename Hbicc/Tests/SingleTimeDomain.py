# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import threading
import random
import time

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.trigger_utility import TriggerUtility


class Functional(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))
        self.slices.sort()
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.slices)

    def tearDown(self):
        super().tearDown()
        self.trigger_utility.restore_initial_trigger_control_register_settings()

    def DirectedDomainTriggerPropagateUsingTriggerNowTest(self):
        self.trigger_utility.trigger_type = 3
        self.trigger_utility.target_resource = 0
        self.trigger_utility.dut_id = random.randint(0, 62)
        self.trigger_utility.sender = self.env.hbicc.rc
        self.trigger_utility.receiver = self.env.hbicc.pat_gen
        # self.env.hbicc.rc.reset_link(self.trigger_utility.receiver) #aurora link reset after initialize is not recommended
        payload = self.trigger_utility.construct_trigger_word()
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'dut_id': self.trigger_utility.dut_id})
        self.pattern_helper.end_status = 0xabcd1234
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                        S stype=DUT_SERIAL, channel_set={self.trigger_utility.dut_id},   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        
                        Call PATTERN1
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        NoCompareDriveZVectors length=1
                        
                        PATTERN1:
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                        NoCompareDriveZVectors length=10000
                        I optype=EXT, extop=TRIGGERNOW, action=TRGEXT_I, imm={payload}
                        NoCompareDriveZVectors length=1000
                        Return
                       '''
        self.trigger_utility.configure_trigger_control_register()
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()
        self.trigger_utility.verify_last_trigger_seen_value(payload)

    def DirectedDomainTriggerPropagateUsingTriggerSyncTest(self):
        self.trigger_utility.trigger_type = 3
        self.trigger_utility.target_resource = 0
        self.trigger_utility.dut_id = random.randint(0, 62)
        self.pattern_helper.end_status = 0xcafef00d
        payload = self.trigger_utility.construct_trigger_word()
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'dut_id': self.trigger_utility.dut_id})
        pattern_string = f'''
                                PATTERN_START:
                                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                                S stype=DUT_SERIAL, channel_set={self.trigger_utility.dut_id},   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                                Call PATTERN1
                                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                                NoCompareDriveZVectors length=1

                                PATTERN1:
                                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                                SUBR:
                                NoCompareDriveZVectors length=10000
                                I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={payload}
                                NoCompareDriveZVectors length=1000
                                Return
                               '''
        self.trigger_utility.configure_trigger_control_register()
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()
        self.trigger_utility.verify_last_trigger_seen_value(payload)

    def DirectedSetSoftwareTriggerFlagTest(self):
        self.trigger_utility.trigger_type = 0
        self.trigger_utility.target_resource = 0
        self.trigger_utility.sender = self.env.hbicc.rc
        self.trigger_utility.receiver = self.env.hbicc.pat_gen
        self.trigger_utility.dut_id = random.randint(0, 62)
        payload = self.trigger_utility.construct_trigger_word()
        wait_before_joining_thread = 3
        wait_for_pattern_looping = 2
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'dut_id': self.trigger_utility.dut_id})
        self.trigger_utility.configure_trigger_control_register()
        self.pattern_helper.end_status = 0x1234abcd
        # self.env.hbicc.rc.reset_link(self.trigger_utility.receiver) #aurora link reset after initialize is not recommended
        send_trigger_thread = threading.Thread(target=self.env.hbicc.rc.send_trigger_from_sender_to_receiver,
                                               args=(self.trigger_utility.sender, 16, payload,
                                                     self.trigger_utility.receiver))

        pattern_execute_thread = threading.Thread(target=self.pattern_helper.execute_pattern_scenario)

        pattern_string = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set={self.trigger_utility.dut_id},   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

            DriveZeroVectors length=100000
            I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
            LOOP:
            DriveZeroVectors length=10000
            I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
            NoCompareDriveZVectors length=1
                               '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern=pattern_string, slices=[0, 1, 2, 3, 4], )
        pattern_execute_thread.start()
        if self.wait_for_enable_burst():
            time.sleep(wait_for_pattern_looping)
            send_trigger_thread.start()
            time.sleep(wait_before_joining_thread)
            send_trigger_thread.join()
            pattern_execute_thread.join()
            self.trigger_utility.is_sw_flag_set()
        else:
            self.Log('error', 'Burst start never happened exiting test')

    def DirectedSoftwareTriggerUpTest(self):
        self.trigger_utility.errors = []
        self.trigger_utility.trigger_type = 3
        self.trigger_utility.target_resource = 0
        self.trigger_utility.sender = self.env.hbicc.pat_gen
        self.trigger_utility.dut_id = random.randint(0, 62)
        self.trigger_utility.software_trigger_up_only = 1
        self.trigger_utility.expected_source = 0x10
        self.pattern_helper.end_status = 0xcafef00d
        payload = self.trigger_utility.construct_trigger_word()
        self.trigger_utility.hbicc.rc.clear_sw_trigger_fifo()
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'dut_id': self.trigger_utility.dut_id})
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                        S stype=DUT_SERIAL, channel_set={self.trigger_utility.dut_id},   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                        Call PATTERN1
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        NoCompareDriveZVectors length=1

                        PATTERN1:
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                        NoCompareDriveZVectors length=10000
                        I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={payload}
                        NoCompareDriveZVectors length=1000
                        Return
                               '''
        self.trigger_utility.configure_trigger_control_register()
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()
        try:
            self.trigger_utility.check_for_trigger_entry_in_rc_software_fifo()
            observed_source = self.trigger_utility.hbicc.rc.read_sw_trigger_fifo_source()
            observed_trigger = self.trigger_utility.hbicc.rc.read_sw_trigger_from_rc()
            self.trigger_utility.log_error(payload, observed_trigger, observed_source)
            self.trigger_utility.report_errors()
        except self.trigger_utility.MissedTriggerError:
            self.Log('error', 'Trigger not received at Patgen')

    def wait_for_enable_burst(self):
        timeout_seconds = 2
        return self.pattern_helper.enable_burst_event.wait(timeout_seconds)
