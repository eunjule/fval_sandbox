# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
import random

from Common.fval import process_snapshot
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.assembler import HbiPatternAssembler
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.rpg import RPG

SLICECOUNT = range(5)


class VectorLevel(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = 0b11111111111111111111111111111111111
        self.pattern_helper.user_mode = 'LOW'
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))
        self.slices.sort()
        self.end_status= random.randint(0, 0xFFFFFFFF)

    def DirectedRandomSlicesOneChannelSetFourChannelsUnmaskedTest(self):
        """Test focuses on only unmasking the first four (0 to 3) channels on a random set of slices in a channel set.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose a single channel set randomly
        * Set mask on All channel sets
        * Unmask the first four channels on the randomly selected channel set
        * Run 32 CompareHigh Vectors

        **Criteria:** Only unmasked channels should show failures

        """
        channel_set = random.randint(0, 15)
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7fffffff0, channel_sets=[channel_set])

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                    
                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1
                    
                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomSlicesAllChannelSetsAllChannelsMaskedTest(self):
        """Test focuses on masking every channel on a random set of slices in all channel sets, using two different
        MTV mask registers.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose two random MTV mask registers.
        * Set mask on All channel sets
        * Run 50 CompareHigh Vectors with first random MTV
        * Run 75 CompareHigh Vectors with second random MTV

        **Criteria:** No channels should show failures

        """
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        id_0 = random.randint(0, 100)
        random_mtv1 = random.randint(0, 14)
        random_mtv2 = random.randint(15, 31)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(8),
                                                        mask_index=[random_mtv1])
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(8, 16),
                                                        mask_index=[random_mtv2])

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=50, mtv={random_mtv1}
                        CompareHighVectors length=75, mtv={random_mtv2}
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomSlicesAllChannelSetsWalkingMaskChannelsTest(self):
        """Test focuses on masking a single channel per vector in a walking fashion, on a random set of slices in
        all channel sets, using all 32 MTV mask registers.

        **Test:**

        * Sets FixedDriveState to HIGH
        * Randomize the set of slices and choose MTV mask registers in a walking fashion.
        * Set mask on All channel sets
        * Run 32 Vectors, each vector with a single CompareLow in a walking fashion, except vector 32 with 4 CompareLow

        **Criteria:** No channels should show failures

        """
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        id_0 = random.randint(0, 100)
        for mask_index in range(32):
            value = 1 << mask_index
            if mask_index == 31:
                value |= 0x700000000

            self.pattern_helper.mtv_mask_object.modify_mask(value=value, mask_index=[mask_index])
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                   PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                    SUBR:
                    DriveZeroVectors length=250
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHL
                    V ctv=0, mtv=1, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLH
                    V ctv=0, mtv=2, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHH
                    V ctv=0, mtv=3, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHH
                    V ctv=0, mtv=4, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHH
                    V ctv=0, mtv=5, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHH
                    V ctv=0, mtv=6, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHH
                    V ctv=0, mtv=7, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHH
                    V ctv=0, mtv=8, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHH
                    V ctv=0, mtv=9, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHH
                    V ctv=0, mtv=10, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHH
                    V ctv=0, mtv=11, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHH
                    V ctv=0, mtv=12, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHH
                    V ctv=0, mtv=13, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHH
                    V ctv=0, mtv=14, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHH
                    V ctv=0, mtv=15, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHH
                    V ctv=0, mtv=16, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=17, lrpt=0, data=0vHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=18, lrpt=0, data=0vHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=19, lrpt=0, data=0vHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=20, lrpt=0, data=0vHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=21, lrpt=0, data=0vHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=22, lrpt=0, data=0vHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=23, lrpt=0, data=0vHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=24, lrpt=0, data=0vHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=25, lrpt=0, data=0vHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=26, lrpt=0, data=0vHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=27, lrpt=0, data=0vHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=28, lrpt=0, data=0vHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=29, lrpt=0, data=0vHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=30, lrpt=0, data=0vHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    V ctv=0, mtv=31, lrpt=0, data=0vLLLLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    DriveZeroVectors length=250
                    Return
                    '''
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def RandomMTVTest(self):
        """Test focuses on maximum randomization - masking random channels for each vector generated itself randomly,
        on a random set of slices in all channel sets, using a MTV mask register selected randomly, the value in the
        mask register itself being set randomly and hence masking random channels.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose the MTV mask register in a random fashion.
        * Set mask on All channel sets
        * Set the mask register values randomly

        **Criteria:** Random channels should show failures

        """
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.randomize_mask()
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                   PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                    SUBR:
                        DriveZeroVectors length=250
                        %repeat 100
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        %end
                        DriveZeroVectors length=250
                        Return
                    '''
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedAllSlicesAllChannelSetsAllChannelsMaskedTest(self):
        """Test focuses on masking ALL the channels on ALL the slices in ALL the channel sets.

        **Test:**

        * Sets FixedDriveState to LOW
        * Choose ALL the slices and ALL the channel sets
        * Set mask on ALL the channels in ALL the channel sets using a vector-level mask
        * Run 32 CompareHigh Vectors

        **Criteria:** NO channel anywhere should show failures!

        """
        self.slices = range(5)
        channel_sets = range(0, 16)
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=channel_sets)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedAllSlicesAllChannelSetsAllChannelsUnMaskedTest(self):
        """Test focuses on masking ALL the channels on ALL the slices in ALL the channel sets.

        **Test:**

        * Sets FixedDriveState to LOW
        * Choose ALL the slices and ALL the channel sets
        * Set mask on ALL the channels in ALL the channel sets using a vector-level mask
        * Run 32 CompareHigh Vectors

        **Criteria:** NO channel anywhere should show failures!

        """
        self.slices = range(5)
        channel_sets = range(0, 16)
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x000000000, channel_sets=channel_sets)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedAllSlicesAllChannelSetsRandomChannelsMaskedTest(self):
        """Test focuses on masking ALL the channels on ALL the slices in ALL the channel sets.

        **Test:**

        * Sets FixedDriveState to LOW
        * Choose ALL the slices and ALL the channel sets
        * Set mask on ALL the channels in ALL the channel sets using a vector-level mask
        * Run 32 CompareHigh Vectors

        **Criteria:** NO channel anywhere should show failures!

        """
        self.slices = range(5)
        channel_sets = range(0, 16)
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x101010101, channel_sets=channel_sets)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedSelectableFailMaskTest(self):
        """Test focuses on only unmasking the first four (0 to 3) channels on a random set of slices in a channel set.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose a single channel set randomly
        * Set mask on All channel sets
        * Unmask the first four channels on the randomly selected channel set
        * Run 32 CompareHigh Vectors

        **Criteria:** Only unmasked channels should show failures

        """
        channel_set = random.randint(0, 15)
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x0f0f0f0f0, channel_sets=[channel_set])

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=100
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()
        # pass

    def DirectedMTVFailStatusTest(self):
        """Test focuses on maximum randomization - masking random channels for each vector generated itself randomly,
        on a random set of slices in all channel sets, using a MTV mask register selected randomly, the value in the
        mask register itself being set randomly and hence masking random channels.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose the MTV mask register in a random fashion.
        * Set mask on All channel sets
        * Set the mask register values randomly

        **Criteria:** Random channels should show failures

        """
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.randomize_mask()
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                   PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                    SUBR:
                        DriveZeroVectors length=500
                        %repeat 200
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        RandomCompareHighAndLow length=1, mtv={random.randint(0, 31)}
                        %end
                        DriveZeroVectors length=500
                        Return
                    '''
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()
        # pass


class PatternLevel(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = 0b11111111111111111111111111111111111
        self.pattern_helper.user_mode = 'LOW'
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))
        self.slices.sort()
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def DirectedRandomSlicesAllChannelSetsEightChannelsPlistMaskedTest(self):
        """Test focuses on Plist masking eight (12 to 19) channels on a random set of slices in all channel sets
        using Pattern-Level Masking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask eight channels on all the channel sets using Pattern-Level Mask (Plist Mask before PCall)
        * Make the PCall to Run 32 CompareHigh Vectors

        **Criteria:** All channels except the eight masked channels should show failures

        """
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    S stype=MASK,                   data=0x0000ff000

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomSlicesAllChannelSetsFourChannelsPlistMaskedUnmaskedTest(self):
        """Test focuses on Plist masking four (16 to 19) channels on a random set of slices in all channel sets
        using Pattern-Level Masking and Unmasking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask eight channels on all the channel sets using Pattern-Level Mask (Plist Mask before PCall)
        * UnMask four of the eight masked channels on all the channel sets
          using Pattern-Level UnMask (Plist UnMask before PCall)
        * Make the PCall to Run 32 CompareHigh Vectors

        **Criteria:** All channels except the final four masked channels should show failures

        """
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    S stype=MASK,                   data=0x0000ff000
                    S stype=UNMASK,                 data=0x0000f00f0

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomSlicesAllChannelSetsElevenChannelsPlistMultiMaskedUnmaskedTest(self):
        """Test focuses on Plist masking eleven dispersed channels on a random set of slices in all channel sets
        using Multiple Pattern-Level Masking and Unmasking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask eleven dispersed channels on all the channel sets
          using Multiple Pattern-Level Mask/Unmask (Plist Mask/Unmask before PCall)
        * Make the PCall to Run 32 CompareHigh Vectors

        **Criteria:** All channels except the final eleven masked channels should show failures

        """
        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    S stype=MASK,                   data=0x00f0ff000
                    S stype=UNMASK,                 data=0x00f0f00f0
                    S stype=UNMASK,                 data=0x70f00f00f
                    S stype=MASK,                   data=0x70000000f

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomSlicesAllChannelSetsMultiPlistMultiMaskedUnmaskedTest(self):
        """Test focuses on two PCalls after masking/unmasking different channels
        on a random set of slices in all channel sets using Multiple Pattern-Level Masking and Unmasking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask eleven dispersed channels on all the channel sets
          using Multiple Pattern-Level Mask/Unmask (Plist Mask/Unmask before first PCall)
        * Make the PCall to Run 32 CompareHigh Vectors
        * Mask another set of channels and make a second PCall

        **Criteria:** Effect of Mask/Unmask should not start until PCall and must disappear after the PCall
        """

        id_0 = random.randint(0, 100)
        id_1 = random.randint(101, 200)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                    
                    CompareHighVectors length=5
                    S stype=MASK,                   data=0x00f0ff000
                    S stype=UNMASK,                 data=0x00f0f00f0
                    S stype=UNMASK,                 data=0x70f00f00f
                    S stype=MASK,                   data=0x70000000f
                    CompareHighVectors length=10

                    PatternId {id_0}
                    PCall PATTERN1
                    
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                    CompareHighVectors length=25
                    S stype=MASK,                   data=0x0f0f0ff00
                    S stype=UNMASK,                 data=0x7ff0f00ff
                    CompareHighVectors length=30
                    
                    
                    PatternId {id_1}
                    PCall PATTERN2

                    CompareHighVectors length=100
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        S stype=MASK,                   data=0x0000f0000
                        CompareHighVectors length=40
                        Return

                    PATTERN2:
                        CompareHighVectors length=50
                        S stype=MASK,                   data=0x0000f0000
                        CompareHighVectors length=64
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomSlicesAllChannelSetsNestedPlistMultiMaskedUnmaskedTest(self):
        """Test focuses on Nested Plist masking/unmasking different channels
        on a random set of slices in all channel sets using Multiple Pattern-Level Masking and Unmasking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask first three channels on all the channel sets
        * Make the first PCall to Run 32 CompareHigh Vectors
        * Mask another four middle channels within the first PCall and then make a nested second PCall

        **Criteria:** Inside the nested PCall, both the first and second PCall masks should take effect!

        """
        id_0 = random.randint(0, 100)
        id_1 = 100 - id_0
        id_2 = random.randint(101, 200)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    CompareHighVectors length=5
                    S stype=MASK,                   data=0x700000000                        
                    CompareHighVectors length=10

                    PatternId {id_0}
                    PCall PATTERN1
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    CompareHighVectors length=25
                    S stype=MASK,                   data=0x00000000f
                    CompareHighVectors length=30

                    PatternId {id_1}
                    PCall PATTERN2

                    CompareHighVectors length=100
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        S stype=MASK,                   data=0x0000f0000
                        CompareHighVectors length=40
                        PatternId {id_2}
                        PCall PATTERN2
                        Return

                    PATTERN2:
                        CompareHighVectors length=50
                        S stype=MASK,                   data=0x0000f0000
                        CompareHighVectors length=64
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedAllSlicesAllChannelSetsNestedPlistMultiMaskedUnmaskedTest(self):
        """Test focuses on Nested Plist masking/unmasking different channels
        on a random set of slices in all channel sets using Multiple Pattern-Level Masking and Unmasking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask first three channels on all the channel sets
        * Make the first PCall to Run 32 CompareHigh Vectors
        * Mask another four middle channels within the first PCall and then make a nested second PCall

        **Criteria:** Inside the nested PCall, both the first and second PCall masks should take effect!

        """
        self.slices = [0, 1, 2, 3, 4]
        id_0 = random.randint(0, 100)
        id_1 = 100 - id_0
        id_2 = random.randint(101, 200)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    CompareHighVectors length=5
                    S stype=MASK,                   data=0x700000000                        
                    CompareHighVectors length=10

                    PatternId {id_0}
                    PCall PATTERN1
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    CompareHighVectors length=25
                    S stype=MASK,                   data=0x00000000f
                    CompareHighVectors length=30

                    PatternId {id_1}
                    PCall PATTERN2

                    CompareHighVectors length=100
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        S stype=MASK,                   data=0x0000f0000
                        CompareHighVectors length=40
                        PatternId {id_2}
                        PCall PATTERN2
                        Return

                    PATTERN2:
                        CompareHighVectors length=50
                        S stype=MASK,                   data=0x0000f0000
                        CompareHighVectors length=64
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedAllSlicesAllChannelSetsNestedPlistComplexMaskedUnmaskedTest(self):
        """Test focuses on Nested Plist masking/unmasking different channels
        on a random set of slices in all channel sets using Multiple Pattern-Level Masking and Unmasking.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask first three channels on all the channel sets
        * Make the first PCall to Run 32 CompareHigh Vectors
        * Mask another four middle channels within the first PCall and then make a nested second PCall

        **Criteria:** Inside the nested PCall, both the first and second PCall masks should take effect!

        """
        self.slices = [0, 1, 2, 3, 4]
        id_0 = random.randint(0, 100)
        id_1 = 100 - id_0
        id_2 = random.randint(101, 200)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    CompareHighVectors length=5
                    S stype=MASK,                   data=0x7000F0000                        
                    CompareHighVectors length=10

                    PatternId {id_0}
                    PCall PATTERN1
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    CompareHighVectors length=25
                    S stype=MASK,                   data=0x00000F00f
                    CompareHighVectors length=30

                    PatternId {id_1}
                    PCall PATTERN2

                    CompareHighVectors length=100
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        S stype=MASK,                   data=0x000ff0000
                        CompareHighVectors length=40
                        PatternId {id_2}
                        PCall PATTERN2
                        Return

                    PATTERN2:
                        CompareHighVectors length=50
                        S stype=MASK,                   data=0x0000ff000
                        CompareHighVectors length=64
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPlistMaskCheckingStackTest(self):
        """Pcalls with Plist Masking and Fail Capture Active.

        **Test:**

        * Construct pattern string
            - Make first Pcalls each with its own unique Plist mask and vectors with failures
            - Make second Pcalls each with its own unique Plist mask and vectors with failures

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: PCall, Return, Pcall, Return
            - Error data + full error header for Pin Multiplier 0-3: 2 failures + End of Burst blocks
        """
        
        id_0 = random.randint(0, 100)
        id_1 = 100 - id_0
        self.pattern_helper.user_mode = 'HIGH'
        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}


        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                
                S stype=CAPTURE_PIN_MASK,       data=0b11111111111111111111111111111111111
                S stype=MASK,                   data=0b10000000000000000000000000000000010
                PatternId {id_0}
                PCall P0

                S stype=CAPTURE_PIN_MASK,       data=0b11111111111111111111111111111111111
                S stype=MASK,                   data=0b01000000000000000000000000000000000
                PatternId {id_1}
                PCall P1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                NoCompareDriveZVectors length=1


                P0:
                    CompareHighVectors length=256
                    V ctv=0, mtv=0, lrpt=0, data=0vLLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLH
                    CompareHighVectors length=256
                    Return

                P1:
                    CompareHighVectors length=256
                    V ctv=0, mtv=0, lrpt=0, data=0vLLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    CompareHighVectors length=256
                    Return
        '''

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPlistMaskNestedTest(self, TEST_ITERATIONS=1):
        """Nested Pcalls with Plist Masking and Ctv vectors

        **Test:**

        * Construct pattern string
            - Make 64 Nested Pcalls each with its own unique Plist mask
            - Each Pcall contains 32 ctv vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 32 vectors * 63 pcalls
            - CTV Data Objects: Dependent on randomization + One End of Burst block per channel set
            - Jump/Call trace: 63 Pcalls blocks,  63 Returns blocks
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        for i in range(TEST_ITERATIONS):
            with self.subTest(Iteration=f' {i}'):
                body = self.construct_nexted_pcall_with_plist_mask()
                mask = random.getrandbits(35)
                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                        S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
                        
                        {body}
                            
                        '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def RandomPerPatternMaskTest(self, TEST_ITERATIONS=5):
        """Different CTV Pattern Groups with Different Masking

        **Test:**

        * Construct pattern string
            - PCall to a group of CTV vectors
            - Apply a different mask to each vector group

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 464 Blocks
            - CTV Data Objects: Dependent on ctp randomization + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Returns block
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        for i in range(TEST_ITERATIONS):
            self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
            with self.subTest(Iteration=f' {i}'):
                pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                
                S stype=CAPTURE_PIN_MASK,       data={bin(random.getrandbits(35))}
                S stype=MASK,                   data={bin(random.getrandbits(35))}
                PatternId 0x100
                PCall L100
                
                S stype=CAPTURE_PIN_MASK,       data={bin(random.getrandbits(35))}
                S stype=MASK,                   data={bin(random.getrandbits(35))}
                PatternId 0x200
                PCall L100
                
                S stype=CAPTURE_PIN_MASK,       data={bin(random.getrandbits(35))}
                S stype=MASK,                   data={bin(random.getrandbits(35))}
                PatternId 0x300
                PCall L100
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                L100:
                    RandomCompareHighAndLow length=128, ctv=1
                    AddressVectors          length=256, ctv=1
                    AddressVectorsPlusAtt   length=32
                    RandomCompareHighAndLow length=48, ctv=1
                    
                    Return

                '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def DirectedPlistMaskUnmaskWithKeepPinsTest(self, TEST_ITERATIONS=5):
        '''CTV vectors, Keep pins and Plist Masking/Unmasking

        **Test:**

        * Set up test attributes:
            - set random ctp mask, plist mask, plist unmask
            - create three pcalls
            - Inject CTV vectors with Keep(K) pins at the beginning of all jumps besides the first PCall
             Having K pins should keep the previous branch CTV vector value.
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice should match
                that of the simulator.
        '''
        for i in range(TEST_ITERATIONS):
            with self.subTest(Iteration=f' {i}'):
                pattern_string = f'''
        
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        
                    S stype=CAPTURE_PIN_MASK,           data={bin(random.getrandbits(35))}
                    S stype=MASK,                       data={bin(random.getrandbits(35))}
                    S stype=UNMASK,                     data={bin(random.getrandbits(35))}
                    PatternId 0x100
                    PCall LOOP1
        
                    S stype=CAPTURE_PIN_MASK,           data={bin(random.getrandbits(35))}
                    S stype=MASK,                       data={bin(random.getrandbits(35))}
                    S stype=UNMASK,                     data={bin(random.getrandbits(35))}
                    PatternId 0x200
                    PCall LOOP2                
        
                    S stype=CAPTURE_PIN_MASK,           data={bin(random.getrandbits(35))}
                    S stype=MASK,                       data={bin(random.getrandbits(35))}
                    S stype=UNMASK,                     data={bin(random.getrandbits(35))}
                    PatternId 0x300
                    PCall LOOP3
                                    
        
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1
        
                    LOOP1:
                        RandomCompareHighAndLow length=1, ctv=1
                        %repeat 31
                        V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                        %end
        
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        
                        Return
        
                    SUBR:
                        %repeat 32
                        V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                        %end
                        Return
        
                    LOOP2:
                        %repeat 63
                        V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                        %end
                        RandomCompareHighAndLow length=1, ctv=1
                        Return
        
                    LOOP3:
                        %repeat 128
                        V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                        %end
                        RandomCompareHighAndLow length=100, ctv=1
                        Return
                    '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def construct_nexted_pcall_with_plist_mask(self):
        body = f''''''
        total_pins = 35
        stack_depth = 63
        last_loop = stack_depth - 1

        for loop in range(stack_depth):
            mask = random.getrandbits(total_pins)
            current_pattern_id = 100 + (loop * 100)
            next_pattern_id = current_pattern_id + 100

            if loop == 0:
                body += f'''
                        S stype=MASK,                   data={bin(mask)}
                        PatternId {current_pattern_id}
                        PCall L{current_pattern_id}
                        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1
                        
                        L{current_pattern_id}:
                            AddressVectorsPlusAtt length=32
                            
                            S stype=MASK,                   data={bin(mask)}
                            PatternId {next_pattern_id}
                            PCall L{next_pattern_id}
                            
                            Return
        
                        '''
            elif loop < last_loop:
                body += f'''
                        L{current_pattern_id}:
                            AddressVectorsPlusAtt length=32

                            S stype=MASK,                   data={bin(mask)}
                            PatternId {next_pattern_id}
                            PCall L{next_pattern_id}

                            Return

                        '''
            elif loop == last_loop:
                body += f'''
                        L{current_pattern_id}:
                            AddressVectorsPlusAtt length=32
                            Return

                        '''

        return body


class Mix(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = 0b11111111111111111111111111111111111
        self.pattern_helper.user_mode = 'LOW'
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))
        self.slices.sort()
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def DirectedMaskUnmaskMTVTest(self):
        """Test focuses on Plist masking twelve (12 to 23) channels on a random set of slices in all channel sets
        using Pattern-Level Masking, and on masking the first sixteen (0 to 15) channels on a the same set of slices
        in all channel sets using Vector-Level Masking.


        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Mask twelve channels on all the channel sets using Pattern-Level Mask (Plist Mask before PCall)
        * Set Vector-Level Mask on All channel sets
        * Make the PCall to Run 32 CompareHigh Vectors

        **Criteria:** Only eleven channels (24 to 34) should show failures

        """

        id_0 = random.randint(0, 100)
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff)
        # self.pattern_helper.mtv_mask_object.modify_mask(value=0x7fffffff0, channel_sets=[channel_set])
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x00000ffff)

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    S stype=MASK,                   data=0x000fff000

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=32
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def DirectedAllChannelsVectorMaskedSomeChannelsPlistMaskedUnmaskedTest(self):
        """Test focuses on masking every channel on a random set of slices in all channel sets, using two different
        MTV mask registers.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose two random MTV mask registers.
        * Set mask on All channel sets
        * Run 50 CompareHigh Vectors with first random MTV
        * Run 75 CompareHigh Vectors with second random MTV

        **Criteria:** No channels should show failures

        """
        self.pattern_helper.create_slice_channel_set_pattern_combo('', slices=self.slices)
        id_0 = random.randint(0, 100)
        random_mtv1 = random.randint(0, 14)
        random_mtv2 = random.randint(15, 31)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(8),
                                                        mask_index=[random_mtv1])
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(8, 16),
                                                        mask_index=[random_mtv2])

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    S stype=MASK,                   data=0x0000ff000
                    # S stype=UNMASK,                 data=0x7000ff00f
                    S stype=UNMASK,                 data=0x0000f00f0
                    # S stype=MASK,                   data=0x70f00f00f

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareHighVectors length=50, mtv={random_mtv1}
                        CompareHighVectors length=75, mtv={random_mtv2}
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        for slice in self.pattern_helper.slice_channel_sets_combo.values():
            slice.pattern = pattern_string
        self.pattern_helper.execute_pattern_scenario()

    def RandomVectorAndPatternMaskTest(self):
        """Random PList patterns with Masking and Unmasking of vector pins.

        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose two random MTV mask registers.
        * Set CTP, mask and unmask values for all channel sets

        **Criteria:** Expect pin error count only on pins still active after OR'ing masks.

        """

        constraints = {'NUM_PAT': [50, 100], 'LEN_PAT': [100, 200], 'CLINK': 0.01, 'LRPT': 0.01, 'CTV': 0.1,
                       'RPT': 0.002, 'MTV': 0.01, 'PMASK': True, 'Meta': 0.01, 'ALU': 0.01}

        pattern = HbiPatternAssembler()
        p = RPG(constraints=constraints)
        p.GeneratePlist('ADDRESS',end_status=self.end_status)
        pattern.LoadString(p.GetPattern())
        self.pattern_helper.create_slice_channel_set_pattern_combo(p.GetPattern(), fixed_drive_state='HIGH',
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 0, 'capture_ctv': 0})
        for slice in self.slices:
            self.pattern_helper.slice_channel_sets_combo[slice].pattern_start_address = pattern.Resolve(
                'eval[PATTERN_START]')
        self.pattern_helper.execute_pattern_scenario()
