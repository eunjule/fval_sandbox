# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

class Functional(HbiccTest):

    def PlayTOSPattern1Test(self):
        pattern_helper = PatternHelper(self.env)
        # pattern_helper.tos_snapshot_path = r'\\orststtde02\hdmt\Users\Taylor\For\Shilpa\Snapshots\test_preceding_missedend_12_04_2019_13_06_57'
        pattern_helper.tos_snapshot_path = r'\\orststtde02\hdmt\Users\Taylor\For\Shilpa\Snapshots\missedend_first_occurrence_12_04_2019_13_01_37'
        fixed_drive_state = 'RELEASE'
        pattern_string =''
        pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=[0, 1, 2, 3])
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x19eb283),slice=0)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x19eb283),slice=1)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x19eb283),slice=2)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x19eb283),slice=3)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0),slice=4)
        pattern_helper.execute_pattern_scenario()

    def TFS93882Test(self):
        pattern_helper = PatternHelper(self.env)
        pattern_helper.tos_snapshot_path = r'\\orststtde02\HDMT\FVAL\Snapshots\HBICC\TOS\SKX_SBFT_H_12_13_2019_12_55_33'
        pattern_helper.create_slice_channel_set_pattern_combo()
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x2bd1799),slice=0)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x2bd1799),slice=1)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x2bd1799),slice=2)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0x2bd1799),slice=3)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.PATTERN_START(value=0),slice=4)

        pattern_helper.set_per_pattern_fail_count(1)
        pattern_helper.set_global_fail_count(1)

        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.CAPTURE_DISTRIBUTION_CONTROL(value=0x911), slice=0)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.CAPTURE_DISTRIBUTION_CONTROL(value=0x922), slice=1)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.CAPTURE_DISTRIBUTION_CONTROL(value=0x944), slice=2)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.CAPTURE_DISTRIBUTION_CONTROL(value=0x988), slice=3)
        pattern_helper.hbicc.pat_gen.write_slice_register(pattern_helper.hbicc.pat_gen.registers.CAPTURE_DISTRIBUTION_CONTROL(value=0),slice=4)

        pattern_helper.execute_pattern_scenario()

