# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Capture This Vector (CTV) is per vector flag to store the current vector

Captured data must be traceable to the
vector and channel set that captured it.

The HBICC fans out vector data to all channel sets, but capture data
is unique for each channel set. Performance in specific cases is
measured.
"""
import random
import time
import unittest

from Common import configs
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.capture_structs import CTV_HEADER_AND_DATA
from Common.fval import Object, expected_failure

PIN_DIRECTION = {'DRIVE_LOW': 0x0000, 'DRIVE_HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE_PATTERN_CNTRL': 0xFFFF}

PADDING = 1024
SLICECOUNT = range(0, 5)


class Functional(HbiccTest):
    """Full speed capture tests

    Full speed operation is most demanding of capture bandwidth. All patterns include four settings: Enable Clock reset,
    all channel sets are active, all unserialized pins hold value in serialized sections and have at least one Call
    for mask set up. Unless specified, ctp mask has channels 1, 12 and 15 active.

    **Criteria: ** All tests within this class should complete with the following:

        - Correct Pattern End Status word
        - No underrun issues
        - No critical alarms
    """

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = random.randint(0, 0xFFFFFFFF)
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT) - 1))
        self.slices.sort()

    def Directed1kCtvXtremeAllSlicesAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 1024 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select all slices, set CTV vector repeat count to 1024
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024 blocks
            - CTV Data Objects: 128 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        custom_slice_list = [0, 1, 2, 3, 4]
        repeat = 1024
        id_0= random.randint(0,100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed5kCtvXtremeAllChannelSetCtp3SubCycle0Test(self):
        """Captures 5120 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 5120
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 5120 blocks
            - CTV Data Objects: 640 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        id_0 = random.randint(0,100)
        repeat = 5120
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed7kCtvXtremeAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 7168 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 7168
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 7168 blocks
            - CTV Data Objects: 896 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * 7
        id_0 = random.randint(0,100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed12kCtvXtremeAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 12,288 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 12,288
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 12,288 blocks
            - CTV Data Objects: 1,536 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        id_0 = random.randint(0,100)
        repeat = 1024 * 12
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed15kCtvXtremeAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 15,360 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 15,360
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 15,360 blocks
            - CTV Data Objects: 1,920 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        id_0 = random.randint(0, 100)
        repeat = 1024 * 15
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed30kCtvXtremeAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 30,720 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 30,720
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 30,720 blocks
            - CTV Data Objects: 3,840 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * 30
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed35kCtvXtremeAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 35,840 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 35,840
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 35,840 blocks
            - CTV Data Objects: 4,480 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * 35
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed7kCtvXtreme2SlicesSlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures 6,720 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 2 random slices, set CTV vector repeat count to 6,720
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 6,720 blocks
            - CTV Data Objects: 840 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = self.get_random_list(SLICECOUNT, 2)
        custom_slice_list.sort()
        id_0 = random.randint(0, 100)
        repeat = 6720
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed10kCtvXtreme3SlicesSlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures 9,600 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select two random slices, set CTV vector repeat count to 9,600
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 9,600 blocks
            - CTV Data Objects: 1,200 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = self.get_random_list(SLICECOUNT, 2)
        repeat = 9600
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed30kCtvXtreme4SlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures 30,016 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 30,016
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 30,016 blocks
            - CTV Data Objects: 3,752 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = self.get_random_list(SLICECOUNT, 4)
        repeat = 30016
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed250kCtvXtremeAllChannelSetsCtp3SubCycle0Test(self):
        """Captures 256,000 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 256,000
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 256,000 blocks
            - CTV Data Objects: 32,000 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        if not configs.SPLITTER_FEATURE:
            self.skipTest('Bandwidth Limitation')
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * 250
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomCtvsCountXtreme4SlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures Random Number of CTV Data Vectors between 10 and 30 thousand

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 4 random slices, set CTV vector repeat count to random value
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: same number of blocks as the number of ctv vectors
            - CTV Data Objects: ctv vectors divided by 8 cycles/block + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        multiplier = random.randint(10, 30)
        custom_slice_list = self.get_random_list(SLICECOUNT, 4)
        repeat = 1024 * multiplier
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed30kCtvXtreme3SlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures 30,720 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-2 slices, set CTV vector repeat count to 30,720
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 30,720 blocks
            - CTV Data Objects: 3,840 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        multiplier = 30
        id_0 = random.randint(0, 100)
        custom_slice_list = [0, 1, 2]
        repeat = 1024 * multiplier
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed60kCtvXtreme3SlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures 61,440 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 61,440
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 61,440 blocks
            - CTV Data Objects: 7,680 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        multiplier = 60
        id_0 = random.randint(0, 100)
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * multiplier
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed100kCtvXtreme3SlicesAllChannelSetCtp3SubCycle0Test(self):
        """Captures 102,400 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 102,400
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 102,400 blocks
            - CTV Data Objects: 12,800 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks 

        """
        multiplier = 100
        id_0 = random.randint(0, 100)
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * multiplier
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed1kCtvWithPcallCtp3AllChannelSetsSubCycle0Test(self):
        """Captures 1024 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 1024
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024 blocks
            - CTV Data Objects: 128 blocks + One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (Call, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * 1
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        padding0 = 1024
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask
            
            
            NoCompareDriveZVectors length=1024
            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1
            
            PATTERN0:
                AddressVectorsPlusAtt length={repeat}
                NoCompareDriveZVectors length={padding0}
                Return                                                           

            PATTERN1:
                PatternId {id_1}                                                 
                PCall PATTERN0                                                   
                Return                                                           



            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed30kCtvWithPcallCtp3AllChannelSetsSubCycle0Test(self):
        """Captures 30,720 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 30,720
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 30,720 blocks
            - CTV Data Objects: 3,840 blocks + One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (Call, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * 30
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        padding0 = 1024
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

            NoCompareDriveZVectors length=1024
            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1
            
            PATTERN0:
                AddressVectorsPlusAtt length={repeat}
                NoCompareDriveZVectors length={padding0}
                Return                                                              

            PATTERN1:
                PatternId {id_1}                                                    
                PCall PATTERN0                                                      
                Return                                                              



            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed100kCtvWithPcallCtp3AllChannelSetsSubCycle0Test(self):
        """Captures 102,400 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 102,400
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 102,400 blocks
            - CTV Data Objects: 12,800 blocks + One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (Call, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * 100
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        padding0 = 1024
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

            NoCompareDriveZVectors length=1024
            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1
            
            PATTERN0:
                AddressVectorsPlusAtt length={repeat}
                NoCompareDriveZVectors length={padding0}
                Return                                                              

            PATTERN1:
                PatternId {id_1}                                                    
                PCall PATTERN0                                                      
                Return                                                              
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomCtvWith2SamePcallCtp3AllChannelSetsSubCycle0Test(self):
        """Captures Random Number of CTV Data Vectors between 10 and 30 thousand

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to random value
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: same number of blocks as the number of ctv vectors
            - CTV Data Objects: ctv vectors divided by 8 cycles/block + One End of Burst block per channel set
            - Jump/Call trace: 6 blocks (Call, PCall, Return, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * random.randint(1, 30)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        id_2 = random.randint(200, 300)
        padding0 = 1024
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

            NoCompareDriveZVectors length=1024
            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1024
            
            PATTERN0:
                AddressVectorsPlusAtt length={repeat}
                Return                                                              

            PATTERN1:
                PatternId {id_1}                                                    
                PCall PATTERN0                                                      
                
                PatternId {id_2}                                                    
                PCall PATTERN0                                                      
                Return                                                              
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomCtvWith2NestedPcallCtp3AllChannelSetsSubCycle0Test(self):
        """Captures Random Number of CTV Data Vectors between 10 and 30 thousand

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to random value
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: same number of blocks as the number of ctv vectors
            - CTV Data Objects: ctv vectors divided by 8 cycles/block + One End of Burst block per channel set
            - Jump/Call trace: 6 blocks (Call, PCall, PCall, Return, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * random.randint(1, 30)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        id_2 = random.randint(200, 300)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

            NoCompareDriveZVectors length={PADDING}
            PatternId {id_0}
            PCall PATTERN2
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1
            
            NoCompareDriveZVectors length=1024
            PATTERN0:
                PatternId {id_1}
                PCall PATTERN1
                Return
            
            PATTERN1:
                AddressVectorsPlusAtt length={repeat}
                Return                                                              

            PATTERN2:
                PatternId {id_2}
                PCall PATTERN0
                Return                                                               
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomCtvWith2DifferentPcallCtp3AllChannelSetsTest(self):
        """Captures Random Number of CTV Data Vectors between one and three thousand

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to random value
        * Construct pattern string based on attributes
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: same number of blocks as the number of ctv vectors
            - CTV Data Objects: ctv vectors divided by 8 cycles/block + One End of Burst block per channel set
            - Jump/Call trace: 6 blocks (Call, PCall, Return, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * random.randint(1, 3)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        id_2 = random.randint(200, 300)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

            NoCompareDriveZVectors length={PADDING}
            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1024
            
            PATTERN0:
                AddressVectorsPlusAtt length={repeat}
                Return
            
            PATTERN1:
                PatternId {id_1} 
                PCall PATTERN0
                PatternId {id_2}
                PCall PATTERN2
                Return
            
            PATTERN2:
                AddressVectorsPlusAtt length={repeat}
                Return 
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed5kCtvXtremeAllCompareLowCtp3SubCycle0Test(self):
        """Captures 5,120 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 5,120,
        set fixed drive state to HIGH
        * Construct pattern string based on attributes. CTV vectors are all CompareLow
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 5,120 blocks
            - CTV Data Objects: 640 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        fixed_drive_state = 'HIGH'
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * 5
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareLowVectors length={repeat}, ctv=1
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=custom_slice_list)

        ctp_list = self.env.hbicc.get_active_pins_from_ctp(ctp)
        ctp_len = len(ctp_list)
        for slice in custom_slice_list:
            ctv_data = {}
            for pm in self.env.hbicc.get_pin_multipliers():
                start = 0
                ctv_data.update({pm.slot_index: []})
                for i in range(start, repeat, 8):
                    one_ctv = CTV_HEADER_AND_DATA()
                    one_ctv.block_count = ctp_len
                    one_ctv.relative_cycle_count = i & 0b111111111
                    one_ctv.valid_cycles = 0x11111111
                    one_ctv.entry_is_wide = 0x000000FF
                    one_ctv.end_of_burst = 0
                    one_ctv.is_cycle_count = 0
                    one_ctv.originating_slice = slice
                    one_ctv.originating_chip = pm.slot_index
                    one_ctv.capture_word_type = 1
                    if pm.slot_index == 0 or pm.slot_index == 2:
                        one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                        one_ctv.packet2 = 0xFFFFFFFFFFFFFFFF
                        one_ctv.packet3 = 0xFFFFFFFFFFFFFFFF
                    else:
                        one_ctv.packet1 = 0x0
                        one_ctv.packet2 = 0x0
                        one_ctv.packet3 = 0x0

                    ctv_data[pm.slot_index].append(one_ctv)
                offset = 10
                one_ctv = CTV_HEADER_AND_DATA()
                one_ctv.block_count = 0
                one_ctv.relative_cycle_count = (i + offset) & 0b11111111111
                one_ctv.valid_cycles = 0x00000000
                one_ctv.entry_is_wide = 0x00000000
                one_ctv.end_of_burst = 1
                one_ctv.is_cycle_count = 0
                one_ctv.originating_slice = slice
                one_ctv.originating_chip = pm.slot_index
                one_ctv.capture_word_type = 1
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                ctv_data[pm.slot_index].append(one_ctv)
            self.pattern_helper.set_expected_ctv_data(ctv_data, [slice])
        self.Log('debug', pattern_string)
        self.set_expected_ctv_header(repeat, custom_slice_list)
        self.pattern_helper.set_ctp(ctp, custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomCtvsXtremeAllCompareLowAllChannelSetsCtp3SubCycle0Test(self):
        """Captures Random Number of CTV Data Vectors between five and 30 thousand

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to
        random value, set fixed drive state to HIGH
        * Construct pattern string based on attributes. CTV vectors are all CompareLow
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: same number of blocks as the number of ctv vectors
            - CTV Data Objects: ctv vectors divided by 8 cycles/block + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, Goto_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        fixed_drive_state = 'HIGH'
        custom_slice_list = [0, 1, 2, 3]
        id_0 = random.randint(0, 100)
        repeat = 1024 * random.randint(5, 30)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareLowVectors length={repeat}, ctv=1
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=custom_slice_list)
        ctp_list = self.env.hbicc.get_active_pins_from_ctp(ctp)
        ctp_len = len(ctp_list)
        for slice in custom_slice_list:
            ctv_data = {}
            for pm in self.env.hbicc.get_pin_multipliers():
                start = 0
                ctv_data.update({pm.slot_index: []})
                for i in range(start, repeat, 8):
                    one_ctv = CTV_HEADER_AND_DATA()
                    one_ctv.block_count = ctp_len
                    one_ctv.relative_cycle_count = i & 0b111111111
                    one_ctv.valid_cycles = 0x11111111
                    one_ctv.entry_is_wide = 0x000000FF
                    one_ctv.end_of_burst = 0
                    one_ctv.is_cycle_count = 0
                    one_ctv.originating_slice = slice
                    one_ctv.originating_chip = pm.slot_index
                    one_ctv.capture_word_type = 1
                    if pm.slot_index == 0 or pm.slot_index == 2:
                        one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                        one_ctv.packet2 = 0xFFFFFFFFFFFFFFFF
                        one_ctv.packet3 = 0xFFFFFFFFFFFFFFFF
                    else:
                        one_ctv.packet1 = 0x0
                        one_ctv.packet2 = 0x0
                        one_ctv.packet3 = 0x0

                    ctv_data[pm.slot_index].append(one_ctv)
                offset = 10
                one_ctv = CTV_HEADER_AND_DATA()
                one_ctv.block_count = 0
                one_ctv.relative_cycle_count = (i + offset) & 0b11111111111
                one_ctv.valid_cycles = 0x00000000
                one_ctv.entry_is_wide = 0x00000000
                one_ctv.end_of_burst = 1
                one_ctv.is_cycle_count = 0
                one_ctv.originating_slice = slice
                one_ctv.originating_chip = pm.slot_index
                one_ctv.capture_word_type = 1
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                ctv_data[pm.slot_index].append(one_ctv)
            self.pattern_helper.set_expected_ctv_data(ctv_data, [slice])
        self.Log('debug', pattern_string)
        self.set_expected_ctv_header(repeat, custom_slice_list)
        self.pattern_helper.set_ctp(ctp, custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomCtvsXtremeAllSlicesAllCompareHighAllChannelSetsCtp3SubCycle0Test(self):
        """Captures Random Number of CTV Data Vectors between 10 and 30 thousand

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to
        random value, set fixed drive state to LOW
        * Construct pattern string based on attributes. CTV vectors are a mixture of CompareLow and CompareHigh
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: same number of blocks as the number of ctv vectors
            - CTV Data Objects: ctv vectors divided by 8 cycles/block + One End of Burst block per channel set
            - Jump/Call trace: 3  blocks (Call, Goto_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        self.pattern_helper.user_mode = 'LOW'
        custom_slice_list = [0, 1, 2, 3]
        id_0 = random.randint(0, 100)
        repeat = 1024 * random.randint(10, 30)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareHighVectors length={repeat/2}, ctv=1
                    CompareLowVectors length={repeat}, ctv=0
                    CompareHighVectors length={repeat/2}, ctv=1
                    Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)

        ctp_list = self.env.hbicc.get_active_pins_from_ctp(ctp)
        ctp_len = len(ctp_list)
        for slice in custom_slice_list:
            ctv_data = {}
            for pm in self.env.hbicc.get_pin_multipliers():
                start = 0
                ctv_data.update({pm.slot_index: []})
                for i in range(start, repeat, 8):
                    one_ctv = CTV_HEADER_AND_DATA()
                    one_ctv.block_count = ctp_len
                    one_ctv.relative_cycle_count = i & 0b111111111
                    one_ctv.valid_cycles = 0x11111111
                    one_ctv.entry_is_wide = 0x000000FF
                    one_ctv.end_of_burst = 0
                    one_ctv.is_cycle_count = 0
                    one_ctv.originating_slice = slice
                    one_ctv.originating_chip = pm.slot_index
                    one_ctv.capture_word_type = 1
                    one_ctv.packet1 = 0xAAAAAAAAAAAAAAAA
                    one_ctv.packet2 = 0xAAAAAAAAAAAAAAAA
                    one_ctv.packet3 = 0xAAAAAAAAAAAAAAAA

                    ctv_data[pm.slot_index].append(one_ctv)
                offset = 10
                one_ctv = CTV_HEADER_AND_DATA()
                one_ctv.block_count = 0
                one_ctv.relative_cycle_count = (i + offset) & 0b11111111111
                one_ctv.valid_cycles = 0x00000000
                one_ctv.entry_is_wide = 0x00000000
                one_ctv.end_of_burst = 1
                one_ctv.is_cycle_count = 0
                one_ctv.originating_slice = slice
                one_ctv.originating_chip = pm.slot_index
                one_ctv.capture_word_type = 1
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                ctv_data[pm.slot_index].append(one_ctv)
            self.pattern_helper.set_expected_ctv_data(ctv_data, [slice])
        self.Log('debug', pattern_string)
        self.set_expected_ctv_header(repeat, custom_slice_list)
        self.pattern_helper.set_ctp(ctp, custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedInterleaveCtvHeader5kTest(self):
        """Captures 4,096 CTV Data Vectors

        **Test:**

        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 4,096
        * Construct pattern string based on attributes. Interleave CTV and non-CTV vectors. 
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 4,096 blocks
            - CTV Data Objects: 512 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, Goto_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        multiplier = 2
        custom_slice_list = [0, 1, 2, 3]
        id_0 = random.randint(0, 100)
        repeat = 1024 * multiplier
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    NoCompareDriveZVectors length={PADDING}
                    AddressVectorsPlusAtt length={repeat}
                    NoCompareDriveZVectors length={PADDING}
                    AddressVectorsPlusAtt length={repeat}
                    NoCompareDriveZVectors length={PADDING}
                    AddressVectorsPlusAtt length={repeat}
                    NoCompareDriveZVectors length={PADDING}
                    AddressVectorsPlusAtt length={repeat}                    
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed1kCtvAllChannelSetsCtp3CaptureAllSubCycle0Test(self):
        """Captures 1,024 CTV Data Vectors and other non-CTV vectors                                                                                        
                                                                                                                                       
        **Test:**                                                                                                                      
                                                                                                                                       
        * Set up test attributes: set ctp mask to three pins, select 0-4 slices, set CTV vector repeat count to 1,024                  
        * Construct pattern string based on attributes. Interleave CTV and non-CTV vectors.                                            
        * Set up tester for pattern execution (see PatternHelper class for more info)                                                  
        * Execute pattern                                                                                                              
        * Process data capture results                                                                                                 
                                                                                                                                       
        **Criteria:** The expected result for each data stream per slice is as follows:                                                
                                                                                                                                       
            - Full CTV Headers: 1,024 blocks                                                                                           
            - CTV Data Objects: 128 blocks non-CTV vectors + 128 blocks CTV vectors + One End of Burst block per channel set                                                    
            - Jump/Call trace: 3 blocks (Call, Goto_I, Return)                                                                         
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks                                          
                                                                                                                                       
        """
        custom_slice_list = [0, 1, 2, 3, 4]
        repeat = 1024
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list,
                                                                   attributes={'capture_all': 1, 'capture_ctv': 0})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCtpOverrideAllPMsWithCtvTest(self):
        """CTP Override feature masks data capture at the Pin Multiplier level.

        **Test:**

        *	Set bit 11 to 1 in CAPTURE_CONTROL register in order to use CTP Override feature.
        *	If bit 11 = 1, setting bit 10 to 1 will suppress all CTPs for this chip.
        *	If bit 11 is 1 and bit 10 is 0, use bits [9:8] to specify which local channel set will be able to see the CTPs.
            -	CTPs for the channel set selected will be controlled by the CTP mask, and CaptureAll or capture window
            settings, just as before. Only difference is 'steering them' to the channel set of interest. In order to
            isolate down to the selected channel set, all 4 PMs should have bit 11 set to 1, 3 of them should have
            bit 10 set to 1, and the remaining 1 of them should have the local channel set selected with bit 10 set to 0.

        **Criteria:**

            * Expect only EOB when whole pin multiplier is masked out.
            * Expect data capture only from non-overriden channel sets, PMs, Slices.

        **NOTE**

            * Error count registers are not impacted and should still report correct error count.

        """
        fixed_drive_state = 'HIGH'
        repeat = 512
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_ctp_override()

        self.pattern_helper.execute_pattern_scenario()

    def DirectedChannelSet0InAllPMsWithCtpOverrideTest(self):
        """CTP Override feature masks data capture at the Pin Multiplier level.

        **Test:**

        *	Set bit 11 to 1 in CAPTURE_CONTROL register in order to use CTP Override feature.
        *	If bit 11 = 1, setting bit 10 to 1 will suppress all CTPs for this chip.
        *	If bit 11 is 1 and bit 10 is 0, use bits [9:8] to specify which local channel set will be able to see the CTPs.
            -	CTPs for the channel set selected will be controlled by the CTP mask, and CaptureAll or capture window
            settings, just as before. Only difference is 'steering them' to the channel set of interest. In order to
            isolate down to the selected channel set, all 4 PMs should have bit 11 set to 1, 3 of them should have
            bit 10 set to 1, and the remaining 1 of them should have the local channel set selected with bit 10 set to 0.

        **Criteria:**

            * Expect only EOB when whole pin multiplier is masked out.
            * Expect data capture only from non-overriden channel sets, PMs, Slices.

        **NOTE**

            * Error count registers are not impacted and should still report correct error count.

        """
        fixed_drive_state = 'HIGH'
        repeat = 1024
        self.slices = [0]
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_ctp_override(ctp_override_chip_wide=False)

        self.pattern_helper.execute_pattern_scenario()

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.reset_ctp_override_channel_sets()

    def RandomPMsSlicesChannelSetInAllPMWithCtpOverrideTest(self):
        """Random PMs, Slices and Channel Sets CTP Override feature masks data capture at the Pin Multiplier level.

        **Test:**

        *	Set bit 11 to 1 in CAPTURE_CONTROL register in order to use CTP Override feature.
        *	If bit 11 = 1, setting bit 10 to 1 will suppress all CTPs for this chip.
        *	If bit 11 is 1 and bit 10 is 0, use bits [9:8] to specify which local channel set will be able to see the CTPs.
            -	CTPs for the channel set selected will be controlled by the CTP mask, and CaptureAll or capture window
            settings, just as before. Only difference is 'steering them' to the channel set of interest. In order to
            isolate down to the selected channel set, all 4 PMs should have bit 11 set to 1, 3 of them should have
            bit 10 set to 1, and the remaining 1 of them should have the local channel set selected with bit 10 set to 0.

        **Criteria:**

            * Expect 32 cycles from single random channel set packed into a 64-bit block per pin per PM.
            * Expect data capture only from non-overriden channel sets, PMs, Slices.

        **NOTE**

            * Error count registers are not impacted and should still report correct error count.

        """
        repeat = 1024
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)

        for slice in self.slices:
            ctp_override_chip_wide_list = [random.choice([True, False]) for x in range(4)]
            for pm in self.env.hbicc.get_pin_multipliers():
                active_channel_set = random.randint(0,3)
                pm.set_ctp_override(ctp_override_chip_wide=ctp_override_chip_wide_list[pm.slot_index], active_channel_set=active_channel_set, slices=[slice])

        self.pattern_helper.execute_pattern_scenario()

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.reset_ctp_override_channel_sets()

    def CharacterizeSplitter(self):
        """Captures 10,240 CTV Data Vectors 
                                                                                                                                                                        
        **Test:**                                                                                                                                                       
                                                                                                                                                                        
        * Set up test attributes: set ctp mask to three pins, select 0-3 slices, set CTV vector repeat count to 10,240                                                   
        * Construct pattern string based on attributes. Interleave CTV and non-CTV vectors.                                                                             
        * Set up tester for pattern execution (see PatternHelper class for more info)                                                                                   
        * Execute pattern                                                                                                                                               
        * Process data capture results                                                                                                                                  
                                                                                                                                                                        
        **Criteria:** The expected result for each data stream per slice is as follows:                                                                                 
                                                                                                                                                                        
            - Full CTV Headers: 10,240 blocks                                                                                                                            
            - CTV Data Objects: 1,280 blocks CTV vectors + One End of Burst block per channel set                                            
            - Jump/Call trace: 3 blocks (Call, Goto_I, Return)                                                                                                          
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks                                                                           
                                                                                                                                                                        
        """
        multiplier = 10
        id_0 = random.randint(0, 100)
        custom_slice_list = [0, 1, 2, 3]
        repeat = 1024 * multiplier
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def set_expected_ctv_header(self, repeat, slices):
        return
        self.Log('debug', f'Constructing Expected CTV Headers..')
        start_timer = time.clock()
        ctv_headers = bytearray(repeat * 32)
        div = repeat // 4
        for i in range(repeat):
            start, end = i * 32, i * 32 + 32
            one = (0).to_bytes(4, byteorder='little')
            two = (1029 + i).to_bytes(4, byteorder='little')
            three = (1024 + i).to_bytes(4, byteorder='little')
            repeat = (0).to_bytes(8, byteorder='little')
            cycles = three
            last = (0).to_bytes(8, byteorder='little')
            ctv_headers[start: end] = one + two + three + repeat + cycles + last
            if i % div == 0:
                self.Log('debug', f'Constructed {i/1000}k Headers')

        self.pattern_helper.set_expected_ctv_header(bytes(ctv_headers), slices)
        end_timer = time.clock()
        self.Log('debug',
                 f'All Slices CTV Header Construction took: {round(end_timer - start_timer, 0)} seconds')

    def get_random_list(self, list, number, sort=True):
        population = set(list)
        samples = random.sample(population, number)
        if sort:
            samples.sort()
        return samples


class SubCycles(HbiccTest):
    """Low speed capture tests

    At lower speeds the internal frequency is higher than the channel
    frequency. This exercises different logic in the design than
    full speed tests. All patterns include four settings: Enable Clock reset,
    all channel sets are active, all unserialized pins hold value in serialized sections and have at least one Call
    for mask set up.
    
    **Criteria: ** All tests within this class should complete with the following:    
    
        - Correct Pattern End Status word    
        - No underrun issues    
        - No critical alarms    
    """

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = hex(random.randint(0, 0xFFFFFFFF))

    def Directed15kCtvRandomSlicesAllChannelSets3CtpRandomSubCycleTest(self):
        """Captures 15,360 CTV Data Vectors                                                                                                                                              
                                                                                                                                                                                       
        **Test:**                                                                                                                                                                      
                                                                                                                                                                                       
        * Set up test attributes: set ctp mask to three pins, select between 2, 3, or 3 random slices, set CTV 
        vector repeat count to 15,360                                                                   
        * Construct pattern string based on attributes                                 
        * Set subcycle to a random exponent between 1 and 7. Speed will be 200 MHz divided by 2**random_exponent
        * Set up tester for pattern execution (see PatternHelper class for more info)                                                                                                  
        * Execute pattern                                                                                                                                                              
        * Process data capture results                                                                                                                                                 
                                                                                                                                                                                       
        **Criteria:** The expected result for each data stream per slice is as follows:                                                                                                
                                                                                                                                                                                       
            - Full CTV Headers: 15,360 blocks                                                                                                                                            
            - CTV Data Objects: 1,920 blocks + One End of Burst block per channel set                                                                                                    
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)                                                                                                                         
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks                                                                                          
                                                                                                                                                                                       
        """
        custom_slice_list = self.get_random_list(SLICECOUNT, random.randint(2, 4))
        self.pattern_helper.set_pm_subcycle(random.randint(1, 7), slices=custom_slice_list)
        repeat = 1024 * 15
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def Directed30kCtvRandomSlicesAllChannelSets3CtpRandomSubCycleTest(self):
        """Captures 30,720 CTV Data Vectors                                                                                                                                              
                                                                                                                                                                                       
        **Test:**                                                                                                                                                                      
                                                                                                                                                                                       
        * Set up test attributes: set ctp mask to three pins, select between 2, 3, or 3 random slices, set CTV 
        vector repeat count to 30,720                                                                   
        * Construct pattern string based on attributes                                 
        * Set subcycle to a random exponent between 1 and 7. Speed will be 200 MHz divided by 2**random_exponent
        * Set up tester for pattern execution (see PatternHelper class for more info)                                                                                                  
        * Execute pattern                                                                                                                                                              
        * Process data capture results                                                                                                                                                 
                                                                                                                                                                                       
        **Criteria:** The expected result for each data stream per slice is as follows:                                                                                                
                                                                                                                                                                                       
            - Full CTV Headers: 30,720 blocks                                                                                                                                            
            - CTV Data Objects: 3,840 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)                                                                                                                         
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks                                                                                          
                                                                                                                                                                                       
        """
        custom_slice_list = self.get_random_list(SLICECOUNT, random.randint(3, 5))
        self.pattern_helper.set_pm_subcycle(random.randint(1, 7), slices=custom_slice_list)
        repeat = 1024 * 30
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}

                PatternId {id_0} 
                PCall PATTERN1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
                
                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    AddressVectorsPlusAtt length={repeat}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def contruct_expected_ctv_data(self, custom_slice_list, pattern_helper, repeat, ctp, special=False):
        ctp_list = self.env.hbicc.get_active_pins_from_ctp(ctp)
        ctp_len = len(ctp_list)
        for slice in custom_slice_list:
            ctv_data = {}
            for pm in self.env.hbicc.get_pin_multipliers():
                start = 0
                ctv_data.update({pm.slot_index: []})
                for i in range(start, repeat, 8):
                    one_ctv = CTV_HEADER_AND_DATA()
                    one_ctv.block_count = ctp_len
                    one_ctv.relative_cycle_count = i & 0b111111111
                    one_ctv.valid_cycles = 0x11111111
                    one_ctv.entry_is_wide = 0x000000FF
                    one_ctv.end_of_burst = 0
                    one_ctv.is_cycle_count = 0
                    one_ctv.originating_slice = slice
                    one_ctv.originating_chip = pm.slot_index
                    one_ctv.capture_word_type = 1
                    self.get_packets(i, ctp_len, one_ctv, pm.slot_index)
                    ctv_data[pm.slot_index].append(one_ctv)
                offset = 10
                one_ctv = CTV_HEADER_AND_DATA()
                one_ctv.block_count = 0
                one_ctv.relative_cycle_count = (i + offset) & 0b11111111111
                one_ctv.valid_cycles = 0x00000000
                one_ctv.entry_is_wide = 0x00000000
                one_ctv.end_of_burst = 1
                one_ctv.is_cycle_count = 0
                one_ctv.originating_slice = slice
                one_ctv.originating_chip = pm.slot_index
                one_ctv.capture_word_type = 1
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                ctv_data[pm.slot_index].append(one_ctv)
            self.pattern_helper.set_expected_ctv_data(ctv_data, [slice])

    def get_packets(self, repeat, ctp_len, one_ctv, pm):
        if repeat % 32 == 0:
            if ctp_len == 1:
                one_ctv.packet1 = 0x55FF55FF55FF55FF
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0x55FF55FF55FF55FF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0x55FF55FF55FF55FF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0x55555555FFFFFFFF
        if repeat % 32 == 8:
            if ctp_len == 1:
                one_ctv.packet1 = 0xFF55FF55FF55FF55
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0xFF55FF55FF55FF55
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0xFF55FF55FF55FF55
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0xFFFFFFFF55555555
        if repeat % 32 == 16:
            if ctp_len == 1:
                one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0xFFFFFFFF55555555
        if repeat % 32 == 24:
            if ctp_len == 1:
                one_ctv.packet1 = 0xFFFF55555555FFFF
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0xFFFF55555555FFFF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0xFFFF55555555FFFF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0xFFFFFFFFFFFFFFFF
        if pm == 0 or pm == 2:
            pass
        elif pm == 1 or pm == 3:
            if ctp_len == 1:
                one_ctv.packet1 = one_ctv.packet1 ^ 0xFFFFFFFFFFFFFFFF
            elif ctp_len == 2:
                one_ctv.packet1 = one_ctv.packet1 ^ 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = one_ctv.packet2 ^ 0xFFFFFFFFFFFFFFFF
            elif ctp_len == 3:
                one_ctv.packet1 = one_ctv.packet1 ^ 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = one_ctv.packet2 ^ 0xFFFFFFFFFFFFFFFF
                one_ctv.packet3 = one_ctv.packet3 ^ 0xFFFFFFFFFFFFFFFF

    def _read_patgen_last_tx_seen(self, custom_slice_list=range(0, 5)):
        data = []
        for slice in custom_slice_list:
            packet_0 = self.env.hbicc.pat_gen.read_slice_register(
                self.env.hbicc.pat_gen.registers.RING_BUS_CAPTURE_DATA_HOLD,
                slice, index=0).value
            packet_1 = self.env.hbicc.pat_gen.read_slice_register(
                self.env.hbicc.pat_gen.registers.RING_BUS_CAPTURE_DATA_HOLD,
                slice, index=1).value
            packet_2 = self.env.hbicc.pat_gen.read_slice_register(
                self.env.hbicc.pat_gen.registers.RING_BUS_CAPTURE_DATA_HOLD,
                slice, index=2).value
            packet_3 = self.env.hbicc.pat_gen.read_slice_register(
                self.env.hbicc.pat_gen.registers.RING_BUS_CAPTURE_DATA_HOLD,
                slice, index=3).value

            data.append({'Slice': slice, 'packet_3': f'0x{packet_3:08X}', 'packet_2': f'0x{packet_2:08X}',
                         'packet_1': f'0x{packet_1:08X}', 'packet_0': f'0x{packet_0:08X}'})
        headers = ['Slice', 'packet_3', 'packet_2', 'packet_1', 'packet_0']
        table = self.env.hbicc.contruct_text_table(headers, data, title_in=f'PatGen')
        self.Log('debug', f'{table}')

    def get_random_list(self, list, number, sort=True):
        population = set(list)
        samples = random.sample(population, number)
        if sort:
            samples.sort()
        return samples