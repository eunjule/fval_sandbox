# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The abort operation allows burst execution to be stopped

Whatever the reason, the HBICC must be able to stop and then be
ready to restart normally.
"""

import random
import threading
import time

from Common.fval import skip_snapshot
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.trigger_utility import TriggerUtility


class Functional(HbiccTest):
    """Various execution abort scenarios."""
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.hbicc = self.env.hbicc
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.custom_slice_list = [0, 1, 2, 3, 4]
        self.send_abort_thread = threading.Thread(name='abort pattern thread begun', target=self.hbicc.abort_pattern,
                                                  args=(self.custom_slice_list, 'abort being called by test'))

        self.pattern_execute_thread = threading.Thread(name='executing pattern thread begun',
                                                       target=self.pattern_helper.execute_pattern_scenario)
        self.pattern_helper.end_status = 0x1800dead
        self.wait_before_abort=5
        self.pattern_helper.e32_interval =128

    def DirectedAbortIRPTTest(self):
        """Abort during repeated vectors.

        This test creates an IRPT Pattern and executes it on the pattern execute thread. It issues an abort from the
        abort thread to interject the pattern execution. End status us read to check if the pattern was aborted via
         the Abort Instruction or if the pattern hit its end instruction.
        """

        pattern_string = f'''
            StartPattern
            NoCompareDriveZVectors length=1000
            LABEL1:
            NoCompareDriveZVectors length=1000
            Repeat 200000000
            NoCompareDriveZVectors length=1
            Goto LABEL1
            NoCompareDriveZVectors length=1000
            StopPattern 0xACED 
            '''
        self.execute_test_scenario(pattern_string)
        self.check_abort_results()


    def DirectedAbortFlatTest(self):  # same as IRPT test
        """Abort during Flat Pattern Test

        This test creates a flat pattern (with an infinite loop.)
        """

        pattern_string = f'''
            StartPattern
            NoCompareDriveZVectors length=1000
            LABEL1:
            NoCompareDriveZVectors length=50000
            Goto LABEL1
            NoCompareDriveZVectors length=1000
            StopPattern 0xACED
            '''
        self.execute_test_scenario(pattern_string)
        self.check_abort_results()


    def DirectedAbortInsidePatternTest(self): #same as IRPT test
        """Abort during Inside Pattern Test

        This test creates a pattern that consists of semi randomly constructed pattern of jumps and loops
        """

        pattern_string = f'''
            StartPattern
            NoCompareDriveZVectors length=1000
            LABEL0:
            NoCompareDriveZVectors length=1000
            '''
        list_of_labels=['LABEL1', 'LABEL2', 'LABEL3', 'LABEL4', 'LABEL5', 'LABEL6', 'LABEL7',
                        'LABEL8', 'LABEL9', 'LABEL10']
        label_selection=random.sample(list_of_labels, 6)
        for label in label_selection:
            pattern_string += f'''Call {label}
                             NoCompareDriveZVectors length=1
                          '''
        pattern_string+=f'''Goto LABEL0
                         NoCompareDriveZVectors length=1000
                         StopPattern 0xACED\n'''
        for label in list_of_labels:
            rpt = random.randint(10, 10000)
            lrpt = random.randint(1, 8)
            pattern_substr = f'''{label}:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt}
                    V ctv=0, mtv=0, lrpt={lrpt}, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                    Return\n'''
            pattern_string += pattern_substr
        self.execute_test_scenario(pattern_string)
        self.check_abort_results()

    def DirectedAbortNestedPCallTest(self):
        """Abort during a nested PCall.

        This test creates an Nested PCall Pattern and executes it on the pattern execute thread. It issues an abort from
        an abort thread to interject the pattern execution. End status us read to check if the pattern was aborted via
        the Abort Instruction or if the pattern hit its end instruction.
        """
        fixed_drive_state = 'HIGH'
        custom_slice_list = [0, 1, 2, 3]
        ctp = 0b00000000000000000001001000000000010
        repeat = 1024 * random.randint(1, 30)

        id_1 = random.randint(100, 200)
        id_2 = random.randint(200, 300)
        pattern_string = f'''
                   PATTERN_START:
                   S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                   S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                   S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                   S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                   NoCompareDriveZVectors length=1024
                   
                   Call PATTERN0
                   StopPattern {hex(random.randint(0, 0xFFFFFFFF))}
                   DriveZeroVectors length=1

                   NoCompareDriveZVectors length=1024
                   PATTERN1:
                   I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=1809795

                   LOOP1:
                    Repeat 2000
                    NoCompareDriveZVectors length=1

                   I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                   I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP1`, cond=COND.ZERO, invcond=1
                   PatternId {id_2}
                   PCall PATTERN2
                   Return

                   PATTERN2:
                   I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=1809795

                   LOOP2:
                    Repeat 2000
                    NoCompareDriveZVectors length=1

                   I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                   I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP2`, cond=COND.ZERO, invcond=1
                   Return                                                              

                   PATTERN0:
                   I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=1809795

                   LOOP3:
                    Repeat 2000
                    NoCompareDriveZVectors length=1

                   I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                   I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP3`, cond=COND.ZERO, invcond=1

                   PatternId {id_1}
                   PCall PATTERN1
                   Return                                                               
                   '''

        self.execute_test_scenario(pattern_string)
        self.check_abort_results()


    @skip_snapshot
    def DirectedFlushThenAbortTest(self):
        """Abort the pattern after the flush is completed by the FPGA.

       This test creates a pattern with an Valid PID and an Invalid PID. The flush bit (CaptureControl[10]) is
       set on seeing the invalid PID. When the Flush bit is set, the test toggles CaptureControl[14]. Upon which
       CaptureControl[10] is cleared by the FPGA. The abort is then issued by the test.

       """
        fixed_drive_state = 'HIGH'
        wait_before_flush_set=5
        wait_before_trigger_thread = 5
        ctp = 0b00000000000000000000000000000000111
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.custom_slice_list)

        payload = self.setup_triggerutility()
        send_trigger_thread = threading.Thread(target=self.env.hbicc.rc.send_trigger_from_sender_to_receiver,
                                               args=(self.trigger_utility.sender, 16, payload,
                                                     self.trigger_utility.receiver))

        pattern_string = f'''
            PATTERN_START:   
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask
            
            PatternId 0x01
            PCall PATTERN2
            
            PatternId 0xFFFFFFFF
            PCall PATTERN3
            
            PatternId 0x02
            PCall PATTERN4
            
            
            StopPattern {hex(random.randint(0, 0xFFFFFFFF))}
            DriveZeroVectors length=1
            
            PATTERN2:
            DriveZeroVectors length=100
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=1809795
            LOOP1:
                V ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000011
                 DriveZeroVectors length=10000
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
            I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP1`, cond=COND.ZERO, invcond=1
            Return 
            
            PATTERN3:
            DriveZeroVectors length=100000
            I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
            LOOP:
                DriveZeroVectors length=10000
            I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
            Return
            
            PATTERN4:  
            DriveZeroVectors length=100
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=180979500
            LOOP3:
                V ctv=1, mtv=0, lrpt=0, data=0v00000000000000000000000000000000011
                DriveZeroVectors length=10000
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
            I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP3`, cond=COND.ZERO, invcond=1
            Return               
            '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                   slices=self.custom_slice_list)
        self.hbicc.hbicc_alarms.set_alarms_as_non_critical(['underrun'])
        self.pattern_execute_thread.start()
        time.sleep(wait_before_trigger_thread)
        send_trigger_thread.start()
        time.sleep(wait_before_flush_set)
        for slice in self.custom_slice_list:
            for i in range(500):
                capture_ctrl = self.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.CAPTURE_CONTROL, slice)
                if capture_ctrl.flush_buffer :
                    capture_ctrl = self.hbicc.pat_gen.read_slice_register(
                        self.env.hbicc.pat_gen.registers.CAPTURE_CONTROL, slice)
                    capture_ctrl.reset_capture_addr_counts = 1
                    self.hbicc.pat_gen.write_slice_register(capture_ctrl, slice)
                    capture_ctrl = self.hbicc.pat_gen.read_slice_register(
                        self.env.hbicc.pat_gen.registers.CAPTURE_CONTROL, slice)
                    if capture_ctrl.flush_buffer != 1:
                        capture_ctrl.reset_capture_addr_counts = 0
                        self.hbicc.pat_gen.write_slice_register(capture_ctrl, slice)
                        self.Log('error', 'Flush bit cleared on rising edge')
                        break
                    capture_ctrl.reset_capture_addr_counts = 0
                    self.hbicc.pat_gen.write_slice_register(capture_ctrl, slice)
                    capture_ctrl = self.hbicc.pat_gen.read_slice_register(
                        self.env.hbicc.pat_gen.registers.CAPTURE_CONTROL, slice)
                    if(capture_ctrl.flush_buffer==0):
                        break
                    else:
                        self.Log('error','Flush bit is not cleared.')

        self.send_abort_thread.start()
        self.send_abort_thread.join()
        send_trigger_thread.join()
        self.pattern_execute_thread.join()
        self.check_abort_results()

    def check_abort_results(self):
        for slice in self.custom_slice_list:
           pattern_ctrl = self.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.PATTERN_CONTROL, slice)

           while(pattern_ctrl.PatternRunning):
                pattern_ctrl = self.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.PATTERN_CONTROL,
                                                                      slice)
                pass
        self.pattern_helper.log_status_report()
        for slice in self.custom_slice_list:
            self.pattern_helper.verify_pattern_end_status(slice)

    def execute_test_scenario(self, pattern_string):
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state='HIGH',
                                                                   slices=self.custom_slice_list)
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical(['underrun'])
        self.pattern_execute_thread.start()
        time.sleep(self.wait_before_abort)
        self.send_abort_thread.start()
        self.send_abort_thread.join()
        self.pattern_execute_thread.join()

    def setup_triggerutility(self):
        self.trigger_utility.trigger_type = 0
        self.trigger_utility.target_resource = 0
        self.trigger_utility.sender = self.env.hbicc.rc
        self.trigger_utility.receiver = self.env.hbicc.pat_gen
        self.trigger_utility.dut_id = random.randint(0, 62)
        payload = self.trigger_utility.construct_trigger_word()
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'dut_id': self.trigger_utility.dut_id})
        self.trigger_utility.configure_trigger_control_register()
        return payload
