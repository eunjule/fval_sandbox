# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""This module focuses on DUT serialization control(DSC) pin state word functionality testing.

Tests range from simple baby step test serializing one DUT at a time and parking the remainder
of the duts to interleaving ctv capture,fail capture,capture all modes,ctp over ride with DSC feature.

"""

import random

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Common.fval.testcasedecorators import process_snapshot


PADDING = 1024
SLICECOUNT = range(0, 5)


class Functional(HbiccTest):

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = random.randint(0, 0xFFFFFFFF)
        population = set(SLICECOUNT)
        self.ctp = random.randint(1, 0b11111111111111111111111111111111111)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT) - 1))
        self.slices.sort()
        self.pattern_helper.user_mode = 'LOW'

    def DirectedEnterExitDutSerialWithCtvTest(self):
        """Enter/Exit Single DUT with Before/After CTVs

        **Test:**

        * Construct pattern string containing 3 PCall:
            1. All Channel Sets, 64 CTVs
            2. DUT Serial, Random Ch Set, No CTVs
            3. All Channel Sets, 64 CTVs

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 128 blocks
            - CTV Data Objects: 8 blocks + One End of Burst block per channel set
            - Jump/Call trace: 3 Pcall block,  3 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        ch_set = random.randint(0, 15)
        ctv_header_filter = 1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=ctv_header_filter)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x200 
                PCall PATTERN2


                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010 
                PatternId 0x300 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length=64
                    Return

                PATTERN2:
                    DriveHighVectors length=64
                    Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDutSerialWalkingAllChSetsWithCtvTest(self):
        """DUT Serial - Iterate All Channel Sets with Inactive Set to Float

        **Test:**

        * Run random number of CTVs (32-256) on all 16 channel sets
        * Construct pattern string containing 16 PCall having:
            - Set DUT Serial Control
            - Active Channel Set to 0 (will increment by one with subsequent PCall)
            - Inactive Channel Sets set to each possible state
            - Random number of CTVs (32-256)

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        self.slices = [0, 1, 2, 3]
        for inactive_dut_state in ['K', 'X', '0', '1', 'F']:
            with self.subTest(InactiveSate=f'{inactive_dut_state}'):
                self.env.hbicc.pat_gen.set_un_parked_duts(duts=0b1111111111111111)
                inactive_state = '0v' + inactive_dut_state * 35
                pcalls, pcall_bodies = self.construct_pcall_string(ch_sets=range(16))
                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                        PatternId 0x100
                        PCall PATTERN1
        
                        {pcalls}
        
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data={inactive_state} # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                        PatternId 0x100
                        PCall PATTERN1
        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1
        
                        PATTERN1:
                            AddressVectorsPlusAtt length=64
                            Return
        
                        {pcall_bodies}
        
                        '''
                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def DirectedAllChSetsFollowedByOneRandomChSetTest(self):
        """DUT Serial - Activate All Channels Then Select One Active Channel Set

        **Test:**

        * Construct pattern string containing 2 PCall:
            PCall One:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active Random Channel Set
                - Inactive Channel Sets set to each possible state.
                - Random number of CTVs (32-256)
            PCall One:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        self.slices = [0, 1, 2, 3]
        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        ctp = self.contruct_ctp(num_pin_active=3)
        vector_length =  random.randint(32, 1024)
        for inactive_dut_state in ['K', 'X', '0', '1', 'F']:
            with self.subTest(InactiveSate=f'{inactive_dut_state}'):
                inactive_state = '0v' + inactive_dut_state * 35
                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                        PatternId 0x100
                        PCall PATTERN1
        
                        S stype=DUT_SERIAL, channel_set={ch_set},   data={inactive_state} # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data={bin(ctp)}
                        PatternId 0x200 
                        PCall PATTERN2
        
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                        PatternId 0x300
                        PCall PATTERN1
        
        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1
        
                        PATTERN1:
                            AddressVectorsPlusAtt length=64
                            Return
        
                        PATTERN2:
                            AddressVectorsPlusAtt length={vector_length}
                            Return
        
                        '''
                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def DirectedAllChSetsFollowedByOneRandomChSetInactiveRandomTest(self):
        """DUT Serial - Activate All Channels Then Select One Active Channel Set

        **Test:**

        * Construct pattern string containing 2 PCall:
            PCall One:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active Random Channel Set
                - Inactive Channel Sets set to random state from pin to pin.
                - Random number of CTVs (32-256)
            PCall One:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        self.slices = [0, 1, 2, 3]
        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        ctp = self.contruct_ctp(num_pin_active=random.randint(1, 34))
        vector_length = random.randint(32, 1024)
        inactive_states = ['K', 'X', '0', '1', 'F']
        inactive_state = '0v' + ''.join(random.choices(inactive_states, k=35))
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data={inactive_state} # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={bin(ctp)}
                PatternId 0x200 
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x300
                PCall PATTERN1


                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length=64
                    Return

                PATTERN2:
                    AddressVectorsPlusAtt length={vector_length}
                    Return

                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedOneRandomChSetFollowedByAllChSetsRandomInactiveTest(self):
        """DUT Serial - Activate One Channel Set Then Active All Channel Sets

        **Test:**

        * Run random number of CTVs (32-256) on all 16 channel sets
        * Construct pattern string containing 2 PCall:
            PCall One:
                - Set DUT Serial Control
                - Activate Random Channel Set
                - Inactive Channel Sets set to random state encoding.
                - Random number of CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        ch_set = random.randint(0, 15)
        ctv_header_filter = 1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=ctv_header_filter)
        ctp = self.contruct_ctp(num_pin_active=3)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}
                PatternId 0x200 
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x300
                PCall PATTERN2

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length=64
                    Return

                PATTERN2:
                    AddressVectorsPlusAtt length=64
                    Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDutSerialCaptureAllTest(self):
        """DUT Serial - Random Channel Set Active with Capture All Active

        **Test:**

        * Construct pattern string containing 2 PCall:
            PCall One:
                - Set DUT Serial Control
                - Activate All Channel Set
                - Random number of CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active Random Channel Set
                - Inactive Channel Sets set to random state encoding.
                - Random number of CTVs (32-256)

        * Activate Capture All Feature
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        attributes = {'capture_fails': 0,
                      'capture_ctv': 0,
                      'capture_all': 1}

        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        ctp = self.contruct_ctp(num_pin_active=3)

        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={bin(ctp)}
                PatternId 0x200 
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x300
                PCall PATTERN1


                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    DriveZeroVectors length=64
                    Return

                PATTERN2:
                    DriveHighVectors length=32, ctv=1
                    CompareHighVectors length=32, ctv=1
                    Return

                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,
                                                                   slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDutSerialFailCaptureNoCtvsTest(self):
        """DUT Serial - Random Channel Set Active with Capture All Active

        **Test:**

        * Construct pattern string containing 2 PCall:
            PCall One:
                - Set DUT Serial Control
                - Activate All Channel Set
                - Random number of Non-CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active Random Channel Set
                - Inactive Channel Sets set to random state encoding.
                - Random number of Non-CTVs (32-256)

        * Activate Fail Capture Feature
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}

        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        ctp = self.contruct_ctp(num_pin_active=3)

        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={bin(ctp)}
                PatternId 0x200 
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x300
                PCall PATTERN1


                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    DriveZeroVectors length=64
                    Return

                PATTERN2:
                    DriveHighVectors length=32
                    CompareHighVectors length=32
                    Return

                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,
                                                                   slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedOneChannelSetDutSerialNoCtvTest(self):
        """DUT Serial - baby step to test entry and exit from Dsc region.

        **Test:**

        * Run a flat pattern no ctv on all 16 channel set
        * Randomly pick one channel set  and set to run a unique content where as other duts would be set to drive 0.
        * Change DUT serial back to all channel set and enable CTV.

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization for all duts
            - CTV Data Objects:  depending on randomization for all duts + EOB per pm
            - Jump/Call trace: 16 Pcall block,  16 Return block

        """
        self.pattern_helper.user_mode = 'LOW'
        ch_set = random.randint(0, 15)
        ctv_header_filter = 1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=ctv_header_filter)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0v00000000000000000000000000000000000
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask
                PatternId 0x200
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask
                PatternId 0x300
                PCall PATTERN3
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1

                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    DriveZeroVectors length=1000
                    CompareLowVectors length=1032
                    Return

                PATTERN2:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1]
                SUBR1:
                    %repeat 30
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLH
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLL
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLL
                    %end
                    Return

                PATTERN3:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR2]
                SUBR2:
                    AddressVectorsPlusAtt length={random.randint(32,1024)}
                    Return

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedOneChannelSetDutSerialWithCtpOverrideTest(self):
        """DUT Serial - with CTP over ride feature.

        **Test:**

        * Construct pattern string containing 3 PCall:
            PCall One:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active Random Channel Set
                - Inactive Channel Sets set to float.
                - Random number of CTVs (32-256)
            PCall Three:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)
        * Add CPT override to all PM besides that one doing DUT serialization
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        ctp = self.contruct_ctp(num_pin_active=3)
        vector_length =  random.randint(32, 1024)

        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}
                PatternId 0x200 
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}
                PatternId 0x300
                PCall PATTERN1


                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length=64
                    Return

                PATTERN2:
                    AddressVectorsPlusAtt length={vector_length}
                    Return

                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)

        for pm in self.env.hbicc.get_pin_multipliers():
            active_pm = ch_set//4
            active_ch_set = ch_set%4
            if pm.slot_index == active_pm:
                pm.set_ctp_override(ctp_override_chip_wide=False, active_channel_set=active_ch_set)
            else:
                pm.set_ctp_override(ctp_override_chip_wide=True, active_channel_set=active_ch_set)

        self.pattern_helper.execute_pattern_scenario()

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.reset_ctp_override_channel_sets()

    def DirectedOneChannelSetDutSerialWithActiveChannelSetPinStateWordMaskTest(self):
        """All Channel Sets followed by One Active Channel Set with PinState Word Mask

        **Test:**

        * Construct pattern string containing 2 PCall:
            PCall One:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)
            PCall Two:
                - Set DUT Serial Control
                - Active Random Channel Set
                - Inactive Channel Sets set to float.
                - Random number of CTVs (32-256)
                - Add Pin State Word mask
            PCall Three:
                - Set DUT Serial Control
                - Active All Channel Set
                - Random number of CTVs (32-256)

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: depending on randomization
            - CTV Data Objects: depending on randomization +
                    One End of Burst block per channel set
            - Jump/Call trace: 16 Pcall block,  16 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        """
        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        vector_length =  random.randint(32, 1024)
        pin_state_mask = random.randint(1, 0b11111111111111111111111111111111111)
        pin_state_unmask = random.randint(1, 0b11111111111111111111111111111111111)

        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={bin(self.ctp)}
                S stype=MASK,                   data={pin_state_mask}
                S stype=UNMASK,                 data={pin_state_unmask}
                PatternId 0x200 
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x300
                PCall PATTERN1


                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length=64
                    Return

                PATTERN2:
                    AddressVectorsPlusAtt length={vector_length}
                    Return

                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedOneChannelSetDutSerialWithUniqueInstructionTest(self):
        """DUT Serial -to verify that serialization section can execute any unique instruction.

        **Test:**

        * Run a flat pattern no ctv on all 16 channel set
        * Randomly pick one channel set  and set to run a unique content with (alu,vector ops etc)multiple instructions
        where as other duts would be set to drive 0

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** No flow control issues and that it is able to process all instructions:
        """
        ch_set = random.randint(0, 15)
        un_park_duts = 0b1 << ch_set
        self.env.hbicc.pat_gen.set_un_parked_duts(duts=un_park_duts)
        vector_length = random.randint(32, 1024)

        register_a_count = random.randint(0xFFF, 0xFFFFFF)
        random_value = random.randint(0x0, 0xFF)
        register_a = random.randint(0, 31)
        register_b = random.randint(0, 31)

        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x100
                PCall PATTERN1

                S stype=DUT_SERIAL, channel_set={ch_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={bin(self.ctp)}
                PatternId 0x200 
                PCall PATTERN2

                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
                PatternId 0x300
                PCall PATTERN1


                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    AddressVectorsPlusAtt length=64
                    Return

                PATTERN2:
                    PatternId 0xBEE
                    PCall PATTERN1
                    AddressVectorsPlusAtt length={vector_length}

                   I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={register_a}, imm={register_a_count}
                   I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA={register_a}, opdest=ALUDEST.REG, dest={register_b}, imm={random_value}

                    Return

                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

        for slice in self.slices:
            expected_register_b = register_a_count - random_value
            observed_register_b = self.env.hbicc.pat_gen.read_central_domain_register_value(slice=slice, register_index=register_b)
            if expected_register_b != observed_register_b:
                self.Log('error', f'ALU Operation during DUT Serialization failed. '
                                  f'Expected {expected_register_b} vs. Observed {observed_register_b}')

    def contruct_ctp(self, num_pin_active):
        ctp = 0
        pins = random.sample(range(34), k=num_pin_active)
        for pin_index in pins:
            ctp |= 1 << pin_index
        return ctp

    def construct_pcall_string(self, ch_sets=[0], pins=3, inactive_dut_state='X'):
        pcalls = f''''''
        pcall_bodies = f''''''

        inactive_dut_state = f'0v' + (inactive_dut_state *35)
        for ch_set in ch_sets:
            ctp = self.contruct_ctp(pins)
            pattern_id = ch_set + 100
            vector_length = ch_set + 100
            pcalls += f'''
                S stype=DUT_SERIAL, channel_set={ch_set},   data={inactive_dut_state} # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={bin(ctp)}
                PatternId {pattern_id} 
                PCall PATTERN{pattern_id}

                '''
            pcall_bodies += f'''
                PATTERN{pattern_id}:
                    AddressVectorsPlusAtt length={32 + (32 * ch_set)}
                    Return

                '''

        return pcalls, pcall_bodies