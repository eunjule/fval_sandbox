# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Enabling both ctv and error capture.

They may be set on the same vector or different sets of vectors.
Integrity of received capture packet is checked when both modes are turned on.
"""

import random

from Common.fval import skip
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

PADDING = 1024
SLICECOUNT = range(0, 5)


class Functional(HbiccTest):

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def Directed1kCtvRandomFailsAllSlicesAllChannelSetsCtp3Test(self):
        """Captures 1024 CTV Data Vectors, random fails (<10) at full speed test """
        ctv_repeat = 1024
        fails_repeat = random.randint(0,9)
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                        PatternId {id_0} 
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:
                        NoCompareDriveZVectors length={PADDING}
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            CompareLowVectors length={ctv_repeat}, ctv=1
                            CompareHighVectors length=1,lrpt= {fails_repeat}
                        Return
                        '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,attributes={'capture_fails': 1, 'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def Directed5kCtvWithMtvRandomFailsAllSlicesAllChannelSetsCtp3Test(self):
        """Captures 4096 CTV Data Vectors, random fails (<10) at full speed test """
        custom_slice_list = [0, 1, 2, 3, 4]
        ctv_repeat = 4096
        id_0 = random.randint(0, 100)
        self.pattern_helper.user_mode = 'LOW'
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                                PATTERN_START:
                                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                                PatternId {id_0} 
                                PCall PATTERN1

                                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                                NoCompareDriveZVectors length=1

                                PATTERN1:
                                NoCompareDriveZVectors length={PADDING}
                                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                                SUBR:
                                    CompareHighVectors length={32}, ctv=0,mtv=31
                                    AddressVectorsPlusAtt length={ctv_repeat}
                                    CompareHighVectors length=16
                                Return
                                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 1})
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(16),
                                                        mask_index=[31])
        self.pattern_helper.execute_pattern_scenario()

    def Directed2kCtvWithFailAllSlicesAllChannelSetsCtp3RandomSubcycleTest(self):
        custom_slice_list = [0, 1,2, 3, 4]
        ctv_repeat = 2048
        id_0 = random.randint(0, 100)
        self.pattern_helper.set_pm_subcycle(random.randint(1, 7), slices=custom_slice_list)
        self.pattern_helper.user_mode = 'HIGH'
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
               PATTERN_START:
               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
               S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

               PatternId {id_0} 
               PCall PATTERN1

               I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
               NoCompareDriveZVectors length=1

               PATTERN1:
               NoCompareDriveZVectors length={PADDING}
               I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
               SUBR:
                   CompareLowVectors length={ctv_repeat},ctv=1
                   CompareHighVectors length=16
               Return
                                       '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 1})
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(16),
                                                        mask_index=[31])
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCtvWithFailsMtvLrptOnSameVectorAllSlicesAllChannelSetsCtp3Test(self):
        custom_slice_list = [0, 1, 3, 4]
        id_0 = random.randint(0, 100)
        self.pattern_helper.user_mode='LOW'
        self.pattern_helper.set_pm_subcycle(random.randint(1, 7), slices=custom_slice_list)
        ctp = 0b00000000000000000000000000000000111
        pattern_string = f'''
                       PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                       S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                       PatternId {id_0} 
                       PCall PATTERN1

                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                       NoCompareDriveZVectors length=1

                       PATTERN1:
                       NoCompareDriveZVectors length={PADDING}
                       I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                       SUBR:
                           V ctv=1, mtv=31, lrpt=31, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHHH
                       Return
                                               '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=custom_slice_list,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 1})
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(16),
                                                        mask_index=[31])
        self.pattern_helper.execute_pattern_scenario()



