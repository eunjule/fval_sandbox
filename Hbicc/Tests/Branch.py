# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The HBICC provides branch instructions for use during execution.

Branches include Goto, Call, Pcall, and Return.

Branches can use several base offsets:
+ Global : Address used directly
+ Commmon File Base (CFB) : Software defined offset
+ Relative : +/- from current address
+ Pattern File Base (PFB) : relative to last PCall address

Branches can be conditional on many flags:
+ None
+ Local Sticky Error
+ Domain Sticky Error
+ Global Sticky Error
+ WiredOr
+ LoadActive
+ Software Trigger Received
+ Domain Trigger Received
+ RC Trigger Received
+ ALU Zero
+ ALU Carry
+ ALU Overflow
+ ALU Sign
+ ALU Below or Equal (unsigned operation)
+ ALU Less Than or Equal (signed operation)
+ ALU Greater Than or Equal (signed operation)
"""
import random

from Common.fval import skip
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.rpg import RPG

KILO = 0x00000400


class Functional(HbiccTest):
    """Tests for all branch types and conditions"""

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        population = set(range(5))
        self.slices = random.sample(population,
                                    random.randint(1, len(range(5)) - 1))
        self.slices.sort()
        self.fixed_drive_state = 'LOW'
        self.end_status = random.randint(0, 0xFFFFFFFF)
        self.ctp = 0b00000000000000000001001000000000010

    def DirectedSubrHeadCallIGlobalTest(self):
        """Absolute CALL to a subroutine at the beginning of a pattern.

        The vector at the PCALL target address is a CALL instruction.
        The CALL uses the immediate field its target address.
        The CALL target is a global address.
        """
        pattern_string = self.simple_subr_head("GLOBAL", "CALL_I")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrTailCallIGlobalTest(self):
        """Absolute CALL to a subroutine at the end of a pattern.

        The CALL instruction is immediately before the RET instruction for the PCALL.
        The CALL uses the immediate field as its target address.
        The CALL target is a global address.
        """
        pattern_string = self.simple_subr_tail("GLOBAL", "CALL_I")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrHeadCallRRelTest(self):
        """Relative CALL a subroutine at the beginning of a pattern.

        The vector at the PCALL target address is a CALL instruction.
        The CALL uses a register for its target address.
        The CALL target is relative to the current address.
        """
        pattern_string = self.simple_subr_head("RELATIVE", "CALL_R")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrTailCallRRelTest(self):
        """Relative Call to a subroutine at the end of a pattern.

        The CALL instruction is immediately before the RET instruction for the PCALL.
        The CALL uses a register for its target address.
        The CALL target is relative to the current address.
        """
        pattern_string = self.simple_subr_tail("RELATIVE", "CALL_R")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrTailGapCallICfbTest(self):
        '''Drive L/H Vectors using Call_I - CFB Base

        The CALL uses a register for its target address.
        The CALL target is relative to the Common File Base (CFB).

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached
        by referencing the Common File Base register value set by the user.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, Call_I using CFB base to reach pattern_string_2
            - Pattern_string_2 has Drive pin instruction vectors and return
            - Complete pattern_string_1 with more drive pin instructions
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, Call_I, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks
        '''
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}

                PRE_RAND:
                    CompareHighVectors length=32
                    I optype=BRANCH, base=BRBASE.CFB, br=CALL_I, imm=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    CompareHighVectors length=64

                Return
            '''

        pattern_string_2 = '''
            CompareLowVectors length=128
            Return
        '''
        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrTailGapCallRCfbTest(self):
        '''Drive L/H Vectors using Call_R - CFB Base

        The CALL uses a register for its target address.
        The CALL target is relative to the Common File Base (CFB).

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached
        by referencing the Common File Base register value set by the user.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, Call_I using CFB base to reach pattern_string_2
            - Pattern_string_2 has Drive pin instruction vectors and return
            - Complete pattern_string_1 with more drive pin instructions
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, Call_I, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks
        '''
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}

                PRE_RAND:
                    AddressVectors length=32
                    I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest={0}, imm={0}
                    I optype=BRANCH, base=BRBASE.CFB, br=CALL_R, dest=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    AddressVectors length=32

                Return
            '''

        pattern_string_2 = '''
            AddressVectors length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrTailGapPCallICfbTest(self):
        '''Drive L/H using PCAll_R - CFB Base

        The PCALL uses the immediate field as its target address.
        The PCALL target is relative to the Common File Base (CFB).

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached
        by referencing the Common File Base register value set by the user.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, PCALL_R using CFB base to reach pattern_string_2
            - Pattern_string_2 has Drive pin instruction vectors and return
            - Complete pattern_string_1 with more drive pin instructions
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, Call_I, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks
        '''
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}

                PRE_RAND:
                    AddressVectors length=32
                    PatternId 0x200
                    I optype=BRANCH, base=BRBASE.CFB, br=PCALL_I, imm=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    AddressVectors length=32

                Return
            '''

        pattern_string_2 = '''
            AddressVectors length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrTailGapPCallRCfbTest(self):
        '''Drive L/H Vectors using PCAll_R - CFB Base

        The PCALL uses the immediate field as its target address.
        The PCALL target is relative to the Common File Base (CFB).

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached
        by referencing the Common File Base register value set by the user.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, PCALL_R using CFB base to reach pattern_string_2
            - Pattern_string_2 has Drive pin instruction vectors and return
            - Complete pattern_string_1 with more drive pin instructions
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, Call_I, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks
        '''
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}

                PRE_RAND:
                    AddressVectors length=32
                    I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest={0}, imm={0}
                    PatternId 0x200
                    I optype=BRANCH, base=BRBASE.CFB, br=PCALL_R, imm=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    AddressVectors length=32

                Return
            '''

        pattern_string_2 = '''
            AddressVectors length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrRecursivePCallIPfb10Test(self):
        """Call a pattern recursively.

        Recursion depth is 10.
        The PCALL uses the immediate field as its target address.
        The PCALL target is relative to the Pattern File Base (PFB).
        """
        pattern_string = self.recursive_pcall_i_pfb(10)
        self.execute_and_check_results(pattern_string)

    def DirectedSubrRecursivePCallIPfb63Test(self):
        """Call a pattern recursively.

        Recursion depth is 63.
        The PCALL uses the immediate field as its target address.
        The PCALL target is relative to the Pattern File Base (PFB).
        """
        pattern_string = self.recursive_pcall_i_pfb(63)
        self.execute_and_check_results(pattern_string)

    def DirectedSubrNestedPCallIGlobalTest(self):
        """Two patterns recursively call each other.

        Recursion depth is 3.
        The PCALL uses the immediate field as its target address.
        The PCALL target is a global address.
        """
        loops = 3
        pattern_string = self.simple_nest_pcall_global(loops)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoAllAddrInOrderTest(self):
        """Exercise all bits in the jump target address

        Branch to locations at each power of 2 in the address space, so
        that all address bits are exercised.
        """
        address_and_pattern_list = self.generate_goto_all_address_patter_list()
        self.pattern_helper.create_slice_channel_set_pattern_combo(fixed_drive_state=self.fixed_drive_state,
                                                                   slices=self.slices,
                                                                   address_and_pattern_list=address_and_pattern_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedGotoImmediateGlobalTest(self):
        """Jump to an immediate target address."""
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)

        pattern_string = self.go_to_imm_global_pattern(address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoImmediateRelativeTest(self):
        """Jump to relative address in the immediate field."""
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)

        pattern_string = self.get_togo_imm_relative(address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoRegisterGlobalTest(self):
        """Jump to a global address held in a register"""
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)

        register = random.randint(0, 32)
        pattern_string = self.get_togo_register_global(register, address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoRegisterRelativeTest(self):
        """Jump to a relative address held in a register"""
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)

        register = random.randint(0, 32)
        pattern_string = self.get_togo_register_relative(register, address, index)
        self.execute_and_check_results(pattern_string)

    # use LTE for signed operation
    def DirectedPCallLessThanOrEqualCarry1Test(self):
        """Branch not taken test using LTE and negative operands

        LTE uses signed comparison of src1 and src2
        src1 = 0XFFFFFFFF (-1)
        src2 = 0XFFFFFFFE (-2)
        LTE = src1 <= src2
        LTE = 0
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1, register2 = random.sample(registers, 3)

        pattern_string = self.get_jump_based_on_alu_condition_pattern(register0, register1, register2, 0xffffffff,
                                                                      0xfffffffe, 'LTE', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedPCallLessThanOrEqualCarry0Test(self):
        """Branch on LTE flag

        LTE uses signed comparison of src1 and src2
        src1 = 0xA0000000 (-2,684,354,560)
        src2 = 0x00000001
        LTE = src1 <= src2
        LTE = 1
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1, register2 = random.sample(registers, 3)

        pattern_string = self.get_jump_based_on_alu_condition_pattern(register0, register1, register2, 0xA0000000, 0x1,
                                                                      'LTE', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedPCallGreaterThanOrEqualTest(self):
        """Branch not taken on GTE flag

        GTE uses signed comparison of src1 and src2
        src1 = 0xA0000000 (-2,684,354,560)
        src2 = 0x00000001
        GTE = src1 <= src2
        GTE = 0
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1, register2 = random.sample(registers, 3)

        pattern_string = self.get_jump_based_on_alu_condition_pattern(register0, register1, register2, 0xA0000000, 0x1,
                                                                      'GTE', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedPCallBelowOrEqualTest(self):
        """Branch not taken on BE flag

        The BE flag uses unsigned comparison, even if the ALU op is a signed operation.
        src1 = 0xA0000000
        src2 = 0x00000001
        BE = 0
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1, register2 = random.sample(registers, 3)

        pattern_string = self.get_jump_based_on_alu_condition_pattern(register0, register1, register2, 0xA0000000, 0x1,
                                                                      'BE', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoIfCarryImmediateGlobalTest(self):
        """Branch taken on Carry flag

        src1 = 0x80000001 (-2,147,483,649)
        src2 = 0x80000001 (-2,147,483,649)
        operation = ADD
        Carry = 1
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1 = random.sample(registers, 2)

        pattern_string = self.get_goto_if_carry_imm_global_pattern(register0, register1, 0x80000001, 0x80000001,
                                                                   'GotoIfCarry', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoIfOverflowImmediateGlobalTest(self):
        """Branch taken on Overflow flag

        src1 = 0x80000001 (-2,147,483,649)
        src2 = 0x80000000 (-2,147,483,648)
        operation = ADD
        Overflow = 1
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1 = random.sample(registers, 2)

        pattern_string = self.get_goto_if_carry_imm_global_pattern(register0, register1, 0x80000001, 0x80000000,
                                                                   'GotoIfOverflow', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoIfSignImmediateGlobalTest(self):
        """Branch taken on Sign flag

        src1 = 0x80000001 (-2,147,483,649)
        src2 = 0x00000000 (0)
        operation = ADD
        Sign = 1
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1 = random.sample(registers, 2)

        pattern_string = self.get_goto_if_carry_imm_global_pattern(register0, register1, 0x80000001, 0x00000000,
                                                                   'GotoIfSign', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoIfZeroImmediateGlobalTest(self):
        """Branch taken on Zero flag

        src1 = 0x00000000 (0)
        src2 = 0x00000000 (0)
        operation = ADD
        Zero = 1
        """
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)
        registers = [x for x in range(32)]
        register0, register1 = random.sample(registers, 2)

        pattern_string = self.get_goto_if_carry_imm_global_pattern(register0, register1, 0x00000000, 0x00000000,
                                                                   'GotoIfZero', address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedBranchKeepPinTest(self):
        '''Vectors with Keep pins across different branches

        **Test:**

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
        * Construct pattern string based on attributes
            - Using both PCall and Goto_I branch types, construct vectors with ctv active
            - Inject vectors with Keep(K) pins at the beginning of all jumps besides the first PCall.
             Having K pins should keep the previous branch vector value.
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice should match
                that of the simulator.
        '''
        pattern_string = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x100
            PCall LOOP1
            
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x200
            PCall LOOP2                
            
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x300
            PCall LOOP3
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            LOOP1:
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                %repeat 31
                V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                Return

            LOOP2:
                %repeat 100
                V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                %repeat 63
                V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                Return
            
            LOOP3:
                %repeat 100
                V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                %repeat 127
                V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                Return
            '''

        self.execute_and_check_results(pattern_string=pattern_string)

    def generate_goto_all_address_patter_list(self):
        pattern_word_byte_address_list = []
        pattern_word_command_list = []
        pattern_end = f'''
                   NoCompareDriveZVectors length={5}
                   I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xFFFFFFFF}
                   NoCompareDriveZVectors length=1
                   '''
        pattern_count = 3
        for i in range(pattern_count):
            if i == 0:
                word_address = random.randrange((2 ** 5) - 15)
                pattern_word_byte_address_list.append(word_address * 16)
            elif i == 1:
                word_address = random.randrange(2 ** (i + 4), (2 ** (i + 5)) - 15)
                pattern_word_byte_address_list.append(word_address * 16)
                pattern_start = f'''
                           StartPattern
                           Call PATTERN1
                           PATTERN1:
                           NoCompareDriveZVectors length={5}
                           Goto {word_address}
                           '''
                pattern_word_command_list.append(pattern_start)
            elif i < 2:
                word_address = random.randrange(2 ** (i + 4), (2 ** (i + 5)) - 15)
                pattern_word_byte_address_list.append(word_address * 16)
                pattern_middle = f'''
                           NoCompareDriveZVectors length={5}
                           Goto {word_address}
                           '''
                pattern_word_command_list.append(pattern_middle)
            else:
                word_address = (2 ** 32) - 20
                pattern_word_byte_address_list.append(word_address * 16)
                pattern_middle = f'''
                           NoCompareDriveZVectors length={5}
                           Goto {word_address}
                           '''
                pattern_word_command_list.append(pattern_middle)
                pattern_word_command_list.append(pattern_end)
        address_and_pattern_list = dict(zip(pattern_word_byte_address_list, pattern_word_command_list))
        return address_and_pattern_list

    def simple_nest_pcall_global(self, callnum):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        p = RPG()
        p.StartPattern()
        p.ClearStack()
        p.AddBlock("PRE_RAND", 150, 0)
        p.SetRegFromImm(1, callnum)  # r1 = 10
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_1")
        p.AddBlock("POST_RAND", 100, vector_data)
        p.EndPattern()
        # subroutine
        p.AddBlock("SUBR_1", 50, vector_data)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_2", "ZERO", "1")
        p.AddBlock("SUBR_1E", 50, vector_data)
        p.Return()
        p.AddBlock("SUBR_2", 60, vector_data)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR_1", "ZERO", "1")
        p.AddBlock("SUBR_2E", 60, vector_data)
        p.Return()
        # print(p.GetPattern())
        return p.GetPattern()

    def recursive_pcall_i_pfb(self, callnum):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        p = RPG()
        p.StartPattern()
        p.ClearStack()
        p.AddBlock("PRE_RAND", 111, 0)
        p.SetRegFromImm(1, callnum)
        p.insert_line('''PatternId 100''')
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR")
        p.AddBlock("POST_RAND", 100, 0x7ffffffff)
        p.EndPattern()
        # subroutine
        p.AddBlock("SUBR", 122, vector_data)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("PFB", "PCALL_I", 0, "ZERO", "1", pfb='eval[SUBR]')  # should call subr itself
        p.AddBlock("SUBRE", 133, vector_data)
        p.Return()
        # print(p.GetPattern())
        return p.GetPattern()
        # pattern.LoadString(p.GetPattern())

        # expectedCount = 452 + 200 * (callnum - 1)
        # self.ExecuteTest(pattern)

    def simple_subr_head(self, branch_base, branch_type):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1 
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xdeadbeef}
                DriveZeroVectors length=1
                
                PATTERN1:
        '''
        p.insert_line(lines)
        p.Jump("GOTO_I", "eval[PRE_RAND]")
        p.AddSubroutine("SUBR", 50, vector_data)
        p.AddBlock("PRE_RAND", 50, 0)
        p.CallSubr(branch_base, branch_type, "SUBR")
        p.AddBlock("POST_RAND", 50, 0x7ffffffff)
        p.insert_line('''Return''')
        return p.GetPattern()

    def simple_subr_tail(self, branch_base, branch_type):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1 

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                DriveZeroVectors length=1

                PATTERN1:
        '''
        p.insert_line(lines)
        p.AddBlock("PRE_RAND", 100, 0)
        p.CallSubr(branch_base, branch_type, "POST_RAND")
        p.AddBlock("POST_RAND", 100, 0)
        p.insert_line('''Return''')
        p.AddSubroutine("SUBR", 50, 0)
        return p.GetPattern()

    def get_all_addresses(self):
        addressList = [0]
        # 0x8000,0000 is not valid
        for i in range(22):
            addressList.append(0x200 << i)
        addressList.append(0x70000000)
        patterns = {}
        id_0 = random.randint(0, 100)
        for i, addr in enumerate(addressList):
            addr = addressList[i]
            if i + 1 == len(addressList):
                p = RPG()
                p.StartPattern()
                lines = f'''
                        PatternId {id_0}                                                 
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                        DriveZeroVectors length=1

                        PATTERN1:
                '''
                p.insert_line(lines)
                p.AddBlock("PRE_RAND", 200, 0)
                p.SetRegFromImm(i, f'0xABCDEF{i}')
                p.insert_line('''Return''')
                pattern = p.GetPattern()
            else:
                p = RPG()
                id_0 = random.randint(0, 100)
                p.StartPattern()
                lines = f'''
                        PatternId {id_0}                                                 
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                        DriveZeroVectors length=1

                        PATTERN1:
                '''
                p.insert_line(lines)
                p.AddBlock("PRE_RAND", 100, 0)
                p.SetRegFromImm(i, f'0xABCDEF{i}')
                p.insert_line(f'Goto {addr}')
                # p.insert_line(f'Goto {addressList[i + 1]}')
                p.insert_line('''Return''')
                pattern = p.GetPattern()
            patterns[addr] = pattern
        return patterns

    def go_to_imm_global_pattern(self, address, rand_jump):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1
                PATTERN1:
        '''
        p.insert_line(lines)
        lines = f"""\
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            AddressVectors length=1024
            Goto L{address[rand_jump]}
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
            """
        p.insert_line(lines)
        return p.GetPattern()

    def get_togo_imm_relative(self, address, index):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1
                PATTERN1:
        '''
        p.insert_line(lines)
        lines = f"""\
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            AddressVectors length=1024
            Goto eval[int32(L{address[index]}-vecaddr)], base=RELATIVE
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
        """
        p.insert_line(lines)
        return p.GetPattern()

    def get_jump_based_on_alu_condition_pattern(self, register0, register1, register2, value0, value1, condition,
                                                address, index):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        # p.AddBlock('LOOP', 1024, 0)
        lines = f"""\
            PatternId {id_0}                                                 
            PCall PATTERN1
            
            PATTERN1:
            # ClearFlags
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            SetRegister {register0}, 0x{value0:X}  
            SetRegister {register1}, 0x{value1:X}
            I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_RB, regA={register0}, regB={register1}, opdest=ALUDEST.REG, dest={register2}
            PatternId 100
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=`L{address[index]}`, cond={condition}
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
        """
        p.insert_line(lines)
        return p.GetPattern()

    def get_goto_if_carry_imm_global_pattern(self, register0, register1, value0, value1, condition, address, index):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}
               PCall PATTERN1
                PATTERN1:
        '''
        p.insert_line(lines)
        p.AddBlock('LOOP', 1024, 0)
        lines = f"""\
            # ClearFlags
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest={register0}, imm={value0}
            # r1 = r0 + 0x80000000 (this should set the carry flag)
            I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA={register0}, opdest=ALUDEST.REG, dest={register1}, imm={value1}
            {condition} L{address[index]}
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
        """
        p.insert_line(lines)
        return p.GetPattern()

    def get_togo_register_global(self, register, address, index):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}
               PCall PATTERN1
                PATTERN1:
        '''
        p.insert_line(lines)
        lines = f"""\
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            AddressVectors length=1024
            SetRegister {register}, L{address[index]}
            GotoR {register}
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
        """
        p.insert_line(lines)
        return p.GetPattern()

    def get_togo_register_relative(self, register, address, index):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}
               PCall PATTERN1
                PATTERN1:
        '''
        p.insert_line(lines)
        lines = f"""\
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            AddressVectors length=1024
            SetRegister {register}, eval[int32(L{address[index]}-vecaddr-1)]
            GotoR {register}, base=RELATIVE
            AddressVectors length=1024
            SetRegister {register}, L{address[index]}
            GotoR {register}
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
        """
        p.insert_line(lines)
        return p.GetPattern()

    def execute_and_check_results(self, pattern_string, address_and_pattern_list={}):
        self.log_pattern(pattern_string, address_and_pattern_list)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, self.fixed_drive_state,
                                                                   slices=self.slices, attributes={'capture_fails': 0},
                                                                   address_and_pattern_list=address_and_pattern_list)
        self.pattern_helper.execute_pattern_scenario()

    def log_pattern(self, pattern, address_and_pattern_list):
        if address_and_pattern_list:
            self.Log('debug', f'\n{"*"*40}Multiple patterns {"*"*40}')
            for address, pattern in address_and_pattern_list.items():
                self.Log('debug', f'\n{"*"*100} \nAddress {hex(address)} \n{pattern} \n{"*"*100}')
        else:
            self.Log('debug', f'\n{"*"*100} \n{pattern} {"*"*100}')
