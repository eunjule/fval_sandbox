# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The HBICC uses many clocks, some are measurable in the FPGA"""

import random
import unittest
import time

from Common import fval
from Hbicc.Tests.HbiccTest import HbiccTest, HbiccTestDecorators
from Common.fval import expected_failure
ITERATIONS = 1

PATGEN = {'PCIE_FABRIC_CLOCK': 125.0,
          'DDR4_CH_FABRIC_CLOCK': 33.33,
          'PIN_CLOCK_PSDB': 200.0,
          'AURORA_TRIGGER_FABRIC_CLOCK': 62.5,
          'REF_CLOCK_FREQUENCY': 125.0}

RINGMULTIPLIER = {'GPIO_REFERENCE_CLOCK': 125.0,
                  'TRIGGER_FABRIC_CLOCK_FREQUQNECY': 62.5,
                  'PCIE_FABRIC_CLOCK_FREQUENCY': 125.0,
                  'DDR4_FABRIC_CLOCK_FREQUENCY': 166.63,
                  'SPI_CLOCK_FREQUNECY': 10.0,
                  'REF_CLOCK_FREQUENCY': 100}

PSDB = {'PIN_CLOCK_FREQUENCY': 200.0,
        'CLOCK_FREQUENCY_30MHZ': 30.0,
        'PCIE_FABRIC_CLOCK_FREQUENCY': 125.0,
        'TRANSCEIVER_FABRIC_CLOCK_FREQUENCY': 250,
        'PHYLITE_CLOCK': 200,
        'REF_CLOCK_FREQUENCY': 125}

CLOCKS = {'PATGEN': PATGEN,
          'RINGMULTIPLIER': RINGMULTIPLIER,
          'PSDB_0_PIN_0': PSDB,
          'PSDB_0_PIN_1': PSDB,
          'PSDB_1_PIN_0': PSDB,
          'PSDB_1_PIN_1': PSDB}

LCL_MULTIPLIER = 0.9
UCL_MULTIPLIER = 1.1

CLOCK_TEST_HEADERS = ['Device', 'Register (ADDR)', 'Reference Clock', 'Reg Value', 'Measure Counter', 'Div Ratio',
                      'Window Size', 'Expected (LCL/UCL)', 'Observed']


class Diagnostics(HbiccTest):
    """Verify that clock frequencies are correct

    The frequency measurement capability of the FPGA is not highly
    precise, but is sufficient to tell that the clocks have been
    set up correctly.
    """

    def DirectedPatGenClocksTest(self):
        """Checks clock accuracy for all clocks available in the FPGA. All clocks should be within 10% of the specified value.

        * Read the register for the clock in question:
            - PCIE_FABRIC_CLOCK
            - DDR4_CH_FABRIC_CLOCK
            - PIN_CLOCK_PSDB0
            - PIN_CLOCK_PSDB1
            - AURORA_TRIGGER_FABRIC_CLOCK
        * Calculate the clock frequency using the reference clock.
        * Compare observed and expected clock frequency.

        **Criteria:** All Clock should be +/- 10% of the spec clock
        """
        if self.env.hbicc.pat_gen is None:
            raise unittest.SkipTest('HBIMB unavailable')

        device = self.env.hbicc.pat_gen
        clocks = [device.registers.PCIE_FABRIC_CLOCK,
                  device.registers.DDR4_CH_FABRIC_CLOCK,
                  device.registers.PIN_CLOCK_PSDB,
                  device.registers.AURORA_TRIGGER_FABRIC_CLOCK]

        clk = ClockInspector(device, clocks)
        clk.read_clock_and_check_accuracy()
        clk.report_results()

    def DirectedRingMUltiplierClocksTest(self):
        """Checks clock accuracy for all clocks available in the FPGA. All clocks should be within 10% of the specified value.

        * Read the register for the clock in question:
            - PIN_CLOCK_FREQUENCY
            - TRIGGER_FABRIC_CLOCK_FREQUQNECY
            - PCIE_FABRIC_CLOCK_FREQUENCY
            - DDR4_FABRIC_CLOCK_FREQUENCY
            - SPI_CLOCK_FREQUNECY
        * Calculate the clock frequency using the reference clock.
        * Compare observed and expected clock frequency.

        **Criteria:** All Clock should be +/- 10% of the spec clock
        """
        if self.env.hbicc.ring_multiplier is None:
            raise unittest.SkipTest('HBIMB unavailable')

        device = self.env.hbicc.ring_multiplier

        clocks = [device.registers.GPIO_REFERENCE_CLOCK,
                  device.registers.TRIGGER_FABRIC_CLOCK_FREQUQNECY,
                  device.registers.PCIE_FABRIC_CLOCK_FREQUENCY,
                  device.registers.DDR4_FABRIC_CLOCK_FREQUENCY,
                  device.registers.SPI_CLOCK_FREQUNECY]

        clk = ClockInspector(device, clocks)
        clk.read_clock_and_check_accuracy()
        clk.report_results()

    @HbiccTestDecorators.execute_on_all_pms
    def DirectedPinMultiplierClocksTest(self):
        """Checks clock accuracy for all clocks available in the FPGA. All clocks should be within 10% of the specified value.

        * Read the register for the clock in question:
            - PIN_CLOCK_FREQUENCY
            - CLOCK_FREQUENCY_30MHZ
            - PCIE_FABRIC_CLOCK_FREQUENCY
            - TRANSCEIVER_FABRIC_CLOCK_FREQUENCY
        * Calculate the clock frequency using the reference clock.
        * Compare observed and expected clock frequency.

        **Criteria:** All Clock should be +/- 10% of the spec clock
        """

        self.execute_clock_reading_for_psdb()

    def DirectedSi5344ScratchRegisterTest(self, ITERATIONS=100_000):
        """Interface robustness test"""
        scratch_registers = [reg for reg in range(0x026B, 0x0273)]
        errors = []
        for i in range(ITERATIONS):
            for register in scratch_registers:
                expected = random.randint(0x00, 0xFF)
                self.env.hbicc.pat_gen.si_5344_write(register, expected)
                observed = self.env.hbicc.pat_gen.si_5344_read(register)
                self.log_error(register, expected, observed, errors)
        self.report_errors(errors, ITERATIONS)

    def DirectedSi5344SoftwareResetTest(self):
        """ Resets the Si5344 clock chip using the HARD_RST register.

        This test reads the tool version register, modifies the tool version register. The reset is issued using the
        HARD_RST register via SPI interface and the tool version register is read at the end of the test and should be updated to the
        default value i.e 7.
        """
        self.reset_via_register()
        expected= self.modify_register_value()
        self.reset_via_register()
        self.check_register_value_was_reset(expected)

    def check_register_value_was_reset(self, expected):
        tool_version_minor_register = 0x07
        observed = self.env.hbicc.pat_gen.si_5344_read(tool_version_minor_register)
        if expected != observed:
            self.Log('error', f'Mismatch between expected: 0x{expected:08X} and observed: 0x{observed:08X} in Tool Version Register')
            self.env.hbicc.pat_gen.si_5344_write(tool_version_minor_register, expected)

    def reset_via_register(self):
        reset_register = 0x001E
        device_ready_register=0xFE
        reset_bit=0x02
        self.env.hbicc.pat_gen.si_5344_write(reset_register, reset_bit)
        time.sleep(0.1)
        for tries in range(10000):
            device_status = self.env.hbicc.pat_gen.si_5344_read(device_ready_register)
            if(device_status==0xF):
                break

    @expected_failure('TFS Ticket 112339:')
    def DirectedSi5344ResetThroughFPGATest(self):
        """ Resets the Si5344 clock chip using the FPGA register.

        This test reads the tool version register, modifies the tool version register. The reset is issued using the
        FPGA Si5344 control register and the tool version register is read at the end of the test and should be updated
        to the default value.
        """
        expected=self.modify_register_value()
        self.env.hbicc.pat_gen.reset_si5344_via_pin()
        self.check_register_value_was_reset(expected)

    def modify_register_value(self):
        tool_version_minor_register = 0x07
        expected = self.env.hbicc.pat_gen.si_5344_read(tool_version_minor_register)
        self.Log('info',f'Expected modify_register_value {expected}')
        tool_version_modified_value = expected + 0x5
        self.env.hbicc.pat_gen.si_5344_write(tool_version_minor_register, tool_version_modified_value)
        time.sleep(1)
        readback_modified= self.env.hbicc.pat_gen.si_5344_read(tool_version_minor_register)
        if tool_version_modified_value == readback_modified:
            return expected
        else:
            self.Log('error', 'Error modifying the version register')
            return None


    def log_error(self, register, expected, observed, errors):
        if expected != observed:
            errors.append({'Register': register, 'Expected': f'0x{expected:08X}',
                           'Observed': f'0x{observed:08X}'})

    def report_errors(self, errors, ITERATIONS):
        if errors:
            table = self.env.hbicc.contruct_text_table(data=errors, title_in='Si5344 Device Scratch Registers')
            self.Log('error', f'{table}')
        else:
            self.Log('info', f'Si5344 Device Scratch Register. Executed W/R {ITERATIONS:,} times for each register.')

    def execute_clock_reading_for_psdb(self):
        clocks = [self.pm.registers.PIN_CLOCK_FREQUENCY,
                  self.pm.registers.CLOCK_FREQUENCY_30MHZ,
                  self.pm.registers.PCIE_FABRIC_CLOCK_FREQUENCY,
                  self.pm.registers.TRANSCEIVER_FABRIC_CLOCK_FREQUENCY,
                  self.pm.registers.PHYLITE_CLOCK]

        clk = ClockInspector(self.pm, clocks)
        clk.read_clock_and_check_accuracy()
        clk.report_results()


class ClockInspector(fval.Object):
    def __init__(self, device, clocks):
        self.device = device
        self.clocks = clocks
        self.results = []
        self.failures = []
        self.ref_clock_frequency = CLOCKS[device.name()]['REF_CLOCK_FREQUENCY']

    def read_clock_and_check_accuracy(self):
        for register in self.clocks:
            if register.REGCOUNT > 1:
                for index in range(register.REGCOUNT):
                    self.check_accuracy(register, index)
            else:
                self.check_accuracy(register)

    def check_accuracy(self, register, index=0):
        clock_frequency, observed = self.read_clock_frequency(register, index)
        clock_att, high_end, low_end = self.get_clock_attributes_into_dict(clock_frequency, index, observed)

        if not (observed > low_end and observed < high_end):
            self.failures.append(clock_att)
        self.results.append(clock_att)

    def read_clock_frequency(self, register, index):
        clock_frequency = self.device.read_slice_register(register, index=index)
        observed = self.decode_clock_frequency(clock_frequency)
        return clock_frequency, observed

    def get_clock_attributes_into_dict(self, clock_frequency, index, observed):
        register_name = clock_frequency.name()
        high_end, low_end, mean = self.get_clock_mean_lcl_ucl(register_name, index)
        addr = index * 4 + clock_frequency.ADDR

        clock_att = {'Device': self.device.name(),
                     'Register (ADDR)': '{} (0x{:X})'.format(register_name, addr),
                     'Reference Clock': '{:.2f} MHz'.format(self.ref_clock_frequency),
                     'Reg Value': '0x{:X}'.format(clock_frequency.value),
                     'Measure Counter': clock_frequency.measure_counter,
                     'Div Ratio': clock_frequency.div_ratio,
                     'Window Size': clock_frequency.reference_window_size,
                     'Expected (LCL/UCL)': '{:.2f} ({:.2f}/{:.2f})'.format(mean, low_end, high_end),
                     'Observed': '{:.2f}'.format(observed)}

        return clock_att, high_end, low_end

    def get_clock_mean_lcl_ucl(self, register_name, index):
        try:
            mean = CLOCKS[self.device.name()][register_name][index]
        except:
            mean = CLOCKS[self.device.name()][register_name]
        low_end = mean * LCL_MULTIPLIER
        high_end = mean * UCL_MULTIPLIER
        return high_end, low_end, mean

    def decode_clock_frequency(self, encoded_clock_frequency):
        try:
            x = encoded_clock_frequency.measure_counter
            y = encoded_clock_frequency.div_ratio
            z = encoded_clock_frequency.reference_window_size
            return (((x * y) / z) * self.ref_clock_frequency)
        except ZeroDivisionError:
            return 0.0

    def report_results(self):
        if self.failures:

            table = self.device.contruct_text_table(CLOCK_TEST_HEADERS, self.failures)
            self.Log('error', '{} Clock(s) failed. {}'.format(self.device.name(), table))
        else:
            self.Log('info', 'All {} clocks are operating within specs.'.format(self.device.name()))

    def __str__(self):
        if not self.results:
            return 'Results list is empty'
        return self.device.contruct_text_table(CLOCK_TEST_HEADERS, self.results)


class ClockModifier:
    def __init__(self, device):
        self.device = None
        self.bridge_reg = None
        self.write_reg = None
        self.read_reg = None
        self.tag_reg = None
        self.target_reg = None
        self.target_reg_value = None

    def modify_counter(self, frequency):
        pass


class MCounter:
    def __init__(self):
        self.high_count_register = 0b00000100
        self.low_count_register = 0b00000111
        self.bypass_enable_register = 0b00000101
        self.odd_division_register = 0b00000110
        self.high_count_data = None
        self.low_count_data = None
        self.bypass_enable_data = None
        self.odd_division_data = None


class NCounter:
    def __init__(self):
        self.high_count_register = 0b00000000
        self.low_count_register = 0b00000010
        self.bypass_enable_register = 0b00000001
        self.odd_division_register = 0b0000001
        self.high_count_data = None
        self.low_count_data = None
        self.bypass_enable_data = None
        self.odd_division_data = None