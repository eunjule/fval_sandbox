# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""BLT interface tests

The BLT ROM is not accessed through the FPGA, but it is used to choose the
correct FPGA image to load.
"""

import queue
import threading
from Hbicc.Tests.HbiccTest import HbiccTest

MAX_FAIL_COUNT = 10


class Diagnostics(HbiccTest):
    """Check for robustness of BLT interface"""

    def DirectedExhaustiveAllDevicesTest(self, TEST_ITERATIONS=100):
        """Read the BLT ROM many times and compare.

        All HBICC BLTs are tested in parallel threads due to slow access to
        these devices.
        """
        devices = [self.env.hbicc.mainboard, self.env.hbicc.psdb_0, self.env.hbicc.psdb_1]

        threads = []
        q = queue.Queue()
        for device in devices:
            if device:
                threads.append(
                    threading.Thread(target=self.blt_, args=(device, TEST_ITERATIONS, q), name=device.name()))

        self.env.hbicc.start_threads(threads)
        self.env.hbicc.join_threads(threads)

        for device in devices:
            board_name, errors = q.get()

            if errors:
                self.report_errors(device, errors)
            else:
                self.Log('info', f'{board_name} BLT board read was successful for {TEST_ITERATIONS} iterations.')

    def blt_(self, board, TEST_ITERATIONS, q):
        error_count = []
        reference_blt = board.read_blt()

        for iteration in range(TEST_ITERATIONS):
            try:
                sample_blt = board.read_blt()

                if sample_blt != reference_blt:
                    self.compare_sample_to_reference(board, sample_blt, reference_blt, error_count, iteration)
            except RuntimeError as e:
                self.Log('error', f'{board.name()} Failed Blt. HIL Error: {e}')
                return
            if len(error_count) > MAX_FAIL_COUNT:
                break
        q.put([board.name(), error_count])

    def report_errors(self, board, error_count):
        if error_count:
            column_headers = list(error_count[0].keys())
        table = board.contruct_text_table(column_headers, error_count)
        board.log_device_message('BLT Board read was unsuccessful'.format(table), 'error')

    def compare_sample_to_reference(self, board, sample_blt, reference_blt, error_count, sample_number):
        Errors = 0
        result = {'Board': board.name(), 'VendorName': 'PASS', 'DeviceName': 'PASS', 'PartNumberAsBuilt': 'PASS',
                  'PartNumberCurrent': 'PASS',
                  'SerialNumber': 'PASS', 'ManufactureDate': 'PASS', 'Iteration': None}

        if sample_blt.DeviceName != reference_blt.DeviceName:
            Errors += 1
            result['DeviceName'] = 'FAIL'
        if sample_blt.VendorName != reference_blt.VendorName:
            Errors += 1
            result['VendorName'] = 'FAIL'
        if sample_blt.PartNumberAsBuilt != reference_blt.PartNumberAsBuilt:
            Errors += 1
            result['PartNumberAsBuilt'] = 'FAIL'
        if sample_blt.PartNumberCurrent != reference_blt.PartNumberCurrent:
            Errors += 1
            result['PartNumberCurrent'] = 'FAIL'
        if sample_blt.SerialNumber != reference_blt.SerialNumber:
            Errors += 1
            result['SerialNumber'] = 'FAIL'
        if sample_blt.ManufactureDate != reference_blt.ManufactureDate:
            Errors += 1
            result['ManufactureDate'] = 'FAIL'

        result['Iteration'] = sample_number
        if Errors:
            error_count.append(result)

        return False
