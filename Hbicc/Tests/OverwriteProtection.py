# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBICC capture streams not overwriting data after stream end address

When HBICC capture stream length is larger than the preset memory space
which is determined by the stream start and end address, the stream should
be chipped at the end address and the data after the end address shouldn't
be changed by the capture stream.
"""

import random
import threading

from Common import fval
from Common.fval import skip
from Common.fval.core import HighlightColor
from Hbicc.testbench.capturefailures import CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM, CTV_DATA_STREAM_BLOCK_SIZE, \
    PIN_ERRROR_BLOCK, STREAM_BLOCK, Capture
from Hbicc.testbench.PatternUtility import PatternHelper, Slice
from Hbicc.testbench.trigger_utility import TriggerUtility
from Hbicc.Tests.Alarms import OverflowAlarmTestConstants
from Hbicc.Tests.HbiccTest import HbiccTest


CONDITIONAL_BRANCH_NOT_TAKEN = 1
PCALL_NUM = 1
RETURN_NUM = 1
UNCONDITIONAL_BRANCH_NUM = 1
KEEPALIVE_TRACE_BLOCK_NUM = 3

ID_MAX_VALUE = 100
SLICECOUNT = range(5)


class OverwriteProtectionTestConstants(OverflowAlarmTestConstants):
    MAX_OVERFLOW_BLOCK_NUM = 20
    PROTECTED_DATA_LEN = 256
    BLOCK_NUM_FOR_PROTECTED_DATA_LEN = PROTECTED_DATA_LEN // CTV_DATA_STREAM_BLOCK_SIZE


class OverwriteProtection(HbiccTest, OverwriteProtectionTestConstants):
    """Tests for overwrite protection for data after end address of capture streams"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.reset_pattern_helper_slices_and_constants()
        self.ctp = 0b00000000000000000001001000000000010
        self.randomize_protected_data()
        self.randomize_ctv_data_num_and_limit()
        self.randomize_ctv_header_num_and_limit()
        self.randomize_trace_num_and_limit()
        self.randomize_error_num_and_limit()
        self.initialize_keepalive_checker()
        self.data_before_run = None
        self.data_after_run = None
        self.unit_test = False

    def initialize_keepalive_checker(self):
        self.package_num_1 = self.package_num_limit_1 = self.package_num_2 = self.package_num_limit_2 = 0
        self.stream_keepalive_checker = None

    def randomize_protected_data(self):
        self.data_out_of_bound = bytes([random.getrandbits(8) for _ in range(self.PROTECTED_DATA_LEN)])

    def randomize_ctv_data_num_and_limit(self):
        self.ctv_data_num_limit = random.randint(1, self.MAX_BLOCK_NUM_LIMIT)
        self.ctv_data_num = random.randint(self.ctv_data_num_limit + 1,
                                           self.ctv_data_num_limit + self.MAX_OVERFLOW_BLOCK_NUM)

    def randomize_ctv_header_num_and_limit(self):
        self.ctv_header_num_limit = \
            random.randrange(self.MIN_HEADER_BLOCK_NUM_LIMIT, self.MAX_BLOCK_NUM_LIMIT, self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE)
        self.ctv_header_num = random.randrange(self.ctv_header_num_limit + self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE,
                                               self.ctv_header_num_limit + self.MAX_OVERFLOW_BLOCK_NUM,
                                               self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE) * len(self.slices)

    def randomize_error_num_and_limit(self):
        self.error_num_limit = random.randint(self.PIN_ERROR_EOB_BLOCK_NUM, self.MAX_BLOCK_NUM_LIMIT)
        self.error_num = self.get_even_error_num(self.error_num_limit)

    def DirectedCTVRowCaptureOverwriteProtectionTest(self, TEST_ITERATION=10):
        """Test to check data after ctv data end address not overwritten

        The test:
        1. set up the data after the end address as some specific pattern
        2. make sure that the pattern is written in the desired region
        3. run the pattern that will overflow the CTV data memory
        4. test the alarm and the data after end address not changed
        """
        for iteration in range(TEST_ITERATION):
            if not self.unit_test:
                self.reset_ctv_data_test()
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = (self.ctv_data_num - CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM) * self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
    
                        NoCompareDriveZVectors length=1024
                        PatternId {id_0}
                        PCall PATTERN0
    
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
    
                        PATTERN0:            
                            NoCompareDriveZVectors length=1024
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
    
                            SUBR:
                                ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
    
                            Return
                '''
                address_map = {slice_index: {'ctv_data_stream_end_address':
                               self.ctv_data_stream_start_address + self.ctv_data_num_limit * CTV_DATA_STREAM_BLOCK_SIZE}
                               for slice_index in self.slices}
                self.assert_capture_stream_will_overflow(num=self.ctv_data_num, num_limit=self.ctv_data_num_limit,
                                                         stream_name='CTV DATA')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_data_after_address('ctv_data_stream_end_address')
                self.data_before_run = self.read_data_after_address('ctv_data_stream_end_address')
                self.check_is_data_equal(self.data_before_run, self.data_out_of_bound, 'before run', 'ctv_data')
                self.pattern_helper.execute_pattern_scenario()
                self.check_ctv_data_capture_count(self.ctv_data_num * len(self.env.hbicc.get_pin_multipliers()))
                self.data_after_run = self.read_data_after_address('ctv_data_stream_end_address')
                self.check_is_data_equal(self.data_after_run, self.data_out_of_bound, 'after run', 'ctv_data')
                expected_alarm_set_map = {slice_index: True for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='ctv_row_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)

    def DirectedCTVRowCaptureOverwriteProtectionAlarmResetTest(self, TEST_ITERATION=2):
        """Test to check data after ctv data end address not overwritten

        The test:
        1. set up the data after the end address as some specific pattern
        2. make sure that the pattern is written in the desired region
        3. run the pattern that will overflow the CTV data memory
        4. test the alarm and the data after end address not changed
        """
        for iteration in range(TEST_ITERATION):
            if not self.unit_test:
                self.reset_ctv_data_test()
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = (self.ctv_data_num - CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM) * self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        NoCompareDriveZVectors length=1024
                        PatternId {id_0}
                        PCall PATTERN0

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1

                        PATTERN0:            
                            NoCompareDriveZVectors length=1024
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]

                            SUBR:
                                ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                            Return
                '''
                address_map = {slice_index: {'ctv_data_stream_end_address':
                                                 self.ctv_data_stream_start_address + self.ctv_data_num_limit * CTV_DATA_STREAM_BLOCK_SIZE}
                               for slice_index in self.slices}
                self.assert_capture_stream_will_overflow(num=self.ctv_data_num, num_limit=self.ctv_data_num_limit,
                                                         stream_name='CTV DATA')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_data_after_address('ctv_data_stream_end_address')
                self.data_before_run = self.read_data_after_address('ctv_data_stream_end_address')
                self.check_is_data_equal(self.data_before_run, self.data_out_of_bound, 'before run', 'ctv_data')
                self.pattern_helper.execute_pattern_scenario()
                self.check_ctv_data_capture_count(self.ctv_data_num * len(self.env.hbicc.get_pin_multipliers()))
                self.data_after_run = self.read_data_after_address('ctv_data_stream_end_address')
                self.check_is_data_equal(self.data_after_run, self.data_out_of_bound, 'after run', 'ctv_data')
                expected_alarm_set_map = {slice_index: True for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='ctv_row_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)
                for slice in self.slices:
                    self.env.hbicc.pat_gen.reset_capture_addr_counts_of_capture_control_register(slice=slice)
                expected_alarm_set_map = {slice_index: False for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='ctv_row_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)

    def check_ctv_data_capture_count(self, expected_count, location_mark=''):
        if not isinstance(location_mark, str):
            self.Log('error', f'location_mark: {location_mark} is not a string')
        if len(location_mark) != 0:
            location_mark = location_mark + ':'
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            observed_count = capture.get_ctv_data_capture_register_count()
            if observed_count != expected_count:
                self.Log('error', f'{location_mark} ctv data count mismatch: observed: {observed_count} != expected: {expected_count}')

    def reset_ctv_data_test(self):
        self.reset_pattern_helper_slices_and_constants()
        self.randomize_protected_data()
        self.randomize_ctv_data_num_and_limit()

    def check_is_data_equal(self, observed_data, expected_data, check_place, stream_name, pm_index=0):
        block_num = getattr(self, stream_name + '_num')
        block_num_limit = getattr(self, stream_name + '_num_limit')
        if stream_name == 'error':
            stream_name = stream_name + f' pm_{pm_index}'
        error_message = {'before run': f'{stream_name} stream: The data is not correctly written into protection region',
                         'after run': f'{stream_name} stream: The data after end address is overwritten, block_num = {block_num}, '
                         f'block_num_limit = {block_num_limit}'}
        for iteration, data in enumerate(observed_data):
            if data != expected_data:
                self.Log('error', f'slice {self.slices[iteration]}: ' + error_message[check_place])

    def assert_capture_stream_will_overflow(self, num, num_limit, stream_name):
        if num < num_limit:
            self.Log('error', f'The {stream_name} stream didn\'t overflow the memory')

    def read_data_after_address(self, address, pm_index=0):
        data = []
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            if address == 'pm_stream_end_addresses':
                data.append(
                    capture.patgen.dma_read(getattr(capture, address)[pm_index], self.PROTECTED_DATA_LEN, slice_object.index)
                )
            else:
                data.append(
                    capture.patgen.dma_read(getattr(capture, address), self.PROTECTED_DATA_LEN, slice_object.index)
                )
        return data

    def write_data_after_address(self, address, pm_index=0):
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            if address == 'pm_stream_end_addresses':
                capture.patgen.dma_write(getattr(capture, address)[pm_index], self.data_out_of_bound, slice_object.index)
            else:
                capture.patgen.dma_write(getattr(capture, address), self.data_out_of_bound, slice_object.index)

    def check_log_alarm(self, alarm_bit, by_slice=False, expected_alarm_set_map={}, location_mark=''):
        if False in self.env.hbicc.hbicc_alarms.is_alarm_set({alarm_bit}, by_slice, expected_alarm_set_map):
            if location_mark:
                location_mark = location_mark + ':'
            self.Log('error', f'{location_mark} Alarm was not set for {alarm_bit}')

    def check_log_alarm_not_set(self, alarm_bit, by_slice=False, expected_alarm_set_map={}):
        if self.env.hbicc.hbicc_alarms.is_alarm_set({alarm_bit}, by_slice, expected_alarm_set_map):
            self.Log('error', f'Alarm was unexpectedly set for {alarm_bit}')

    def DirectedCTVHeaderCaptureOverwriteProtectionTest(self, TEST_ITERATION=2):
        """Test to check data after ctv header end address not overwritten

        The test:
        1. set the end address and ctv header number so that on average, every
            slice will overflow(ctv header will be distributed across slices)
        2. set up the data after the end address as some specific pattern
        3. make sure that the pattern is written in the desired region
        4. run the pattern that will overflow the CTV header memory
        5. test the data after end address not changed
        6. test the alarm both overall and by slice
        """
        for iteration in range(TEST_ITERATION):
            if not self.unit_test:
                self.reset_ctv_header_test()
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = self.ctv_header_num
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                pattern_string = f'''
                   PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                       S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
    
                       NoCompareDriveZVectors length=1024
                       PatternId {id_0}
                       PCall PATTERN0
    
                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                       DriveZeroVectors length=1
    
                       PATTERN0:            
                           NoCompareDriveZVectors length=1024
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
    
                           SUBR:
                               ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
    
                           Return
                '''
                self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
                address_map = {slice_index: {'ctv_header_stream_end_address': self.ctv_header_stream_start_address
                                + self.ctv_header_num_limit * STREAM_BLOCK} for slice_index in self.slices}
                self.assert_capture_stream_will_overflow(num=self.ctv_header_num,
                                                         num_limit=self.ctv_header_num_limit,
                                                         stream_name='CTV HEADER')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_data_after_address('ctv_header_stream_end_address')
                self.data_before_run = self.read_data_after_address('ctv_header_stream_end_address')
                self.check_is_data_equal(self.data_before_run, self.data_out_of_bound, 'before run', 'ctv_header')
                self.pattern_helper.execute_pattern_scenario()
                self.check_ctv_header_capture_count(self.ctv_header_num)
                self.data_after_run = self.read_data_after_address('ctv_header_stream_end_address')
                self.check_is_data_equal(self.data_after_run, self.data_out_of_bound, 'after run', 'ctv_header')
                self.pattern_helper.check_ctv_header_capture_size(self.ctv_header_num)
                expected_ctv_header_overflow_map = \
                    self.pattern_helper.get_expected_ctv_header_overflow_map(self.ctv_header_num, self.ctv_header_num_limit)
                self.check_log_alarm(alarm_bit='ctv_header_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_ctv_header_overflow_map)

    def DirectedCTVHeaderCaptureOverwriteProtectionAlarmResetTest(self, TEST_ITERATION=2):
        """Test to check data after ctv header end address not overwritten

        The test:
        1. set the end address and ctv header number so that on average, every
            slice will overflow(ctv header will be distributed across slices)
        2. set up the data after the end address as some specific pattern
        3. make sure that the pattern is written in the desired region
        4. run the pattern that will overflow the CTV header memory
        5. test the data after end address not changed
        6. test the alarm both overall and by slice
        """
        for iteration in range(TEST_ITERATION):
            if not self.unit_test:
                self.reset_ctv_header_test()
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = self.ctv_header_num
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                pattern_string = f'''
                   PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                       S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                       NoCompareDriveZVectors length=1024
                       PatternId {id_0}
                       PCall PATTERN0

                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                       DriveZeroVectors length=1

                       PATTERN0:            
                           NoCompareDriveZVectors length=1024
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]

                           SUBR:
                               ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                           Return
                '''
                self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
                address_map = {slice_index: {'ctv_header_stream_end_address': self.ctv_header_stream_start_address
                                                                              + self.ctv_header_num_limit * STREAM_BLOCK}
                               for slice_index in self.slices}
                self.assert_capture_stream_will_overflow(num=self.ctv_header_num,
                                                         num_limit=self.ctv_header_num_limit,
                                                         stream_name='CTV HEADER')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_data_after_address('ctv_header_stream_end_address')
                self.data_before_run = self.read_data_after_address('ctv_header_stream_end_address')
                self.check_is_data_equal(self.data_before_run, self.data_out_of_bound, 'before run', 'ctv_header')
                self.pattern_helper.execute_pattern_scenario()
                self.check_ctv_header_capture_count(self.ctv_header_num)
                self.data_after_run = self.read_data_after_address('ctv_header_stream_end_address')
                self.check_is_data_equal(self.data_after_run, self.data_out_of_bound, 'after run', 'ctv_header')
                self.pattern_helper.check_ctv_header_capture_size(self.ctv_header_num)
                expected_ctv_header_overflow_map = \
                    self.pattern_helper.get_expected_ctv_header_overflow_map(self.ctv_header_num,
                                                                             self.ctv_header_num_limit)
                self.check_log_alarm(alarm_bit='ctv_header_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_ctv_header_overflow_map)
                for slice in self.slices:
                    self.env.hbicc.pat_gen.reset_capture_addr_counts_of_capture_control_register(slice=slice)

                expected_alarm_set_map = {slice_index: False for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='ctv_header_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)

    def check_ctv_header_capture_count(self, expected_total_count, location_mark='After Pattern Run'):
        ctv_header_count_slice_map = self.pattern_helper.get_slice_ctv_header_distribution(expected_total_count)
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            observed_count = capture.get_ctv_header_capture_register_count()
            if observed_count != ctv_header_count_slice_map[slice_object.index]:
                self.Log('warning', f'{location_mark}: ctv header register count mismatch: '
                                  f'observed: {observed_count} '
                                  f'!= expected: {ctv_header_count_slice_map[slice_object.index]}')

    def reset_ctv_header_test(self):
        self.reset_pattern_helper_slices_and_constants()
        self.randomize_protected_data()
        self.randomize_ctv_header_num_and_limit()

    def DirectedTraceCaptureOverwriteProtectionTest(self, TEST_ITERATION=2):
        """Test to Check Trace Capture Overwrite Protection

        In this test:
        1. Pattern helper, protected data, intended trace_num_limit and trace_num
            will be reset. This method is correlative to the pattern and the way
            the number of iteration of the loop in the pattern is determined.
        2. Determine the repeat times of the iteration in the patter in order
            to create intended length of trace capture stream
        3. Set the trace memory end address according to trace_num_limit reset
            in reset_trace_test()
        4. Write random generated data into the memory after trace stream end
            address and read the data back to make sure it is successfully written
        5. Run the pattern
        6. Read data after trace stream end address and compare it with the protected
            data to make sure that data after end address is not overwritten
        7. Check the trace stream overflow alarm for all the slices
        """
        for iteration in range(0, TEST_ITERATION):
            if not self.unit_test:
                self.reset_trace_test()
            self.pattern_helper.user_mode = 'HIGH'
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = self.trace_num - PCALL_NUM - UNCONDITIONAL_BRANCH_NUM - RETURN_NUM + CONDITIONAL_BRANCH_NOT_TAKEN
            capture_attributes = {'capture_fails': 1, 'capture_ctv': 1}
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
    
                    NoCompareDriveZVectors length=1024
                    PatternId {id_0}
                    PCall PATTERN1
    
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    DriveZeroVectors length=1
    
                    PATTERN1:
                        I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm={repeat}
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            DriveZeroVectors length=100
                            I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR], cond=ZERO, invcond=1
                            DriveZeroVectors length=100
                        Return 
                '''
                self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
                self.assert_capture_stream_will_overflow(num=self.trace_num,
                                                         num_limit=self.trace_num_limit,
                                                         stream_name='TRACE')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                           attributes=capture_attributes)
                address_map = {slice_index: {'trace_stream_end_address': self.trace_stream_start_address
                                             + self.trace_num_limit * STREAM_BLOCK} for slice_index in self.slices}
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_data_after_address('trace_stream_end_address')
                self.data_before_run = self.read_data_after_address('trace_stream_end_address')
                self.check_is_data_equal(self.data_before_run, self.data_out_of_bound, 'before run', 'trace')
                self.pattern_helper.execute_pattern_scenario()
                self.check_trace_capture_count(self.trace_num)
                self.data_after_run = self.read_data_after_address('trace_stream_end_address')
                self.check_is_data_equal(self.data_after_run, self.data_out_of_bound, 'after run', 'trace')
                expected_alarm_set_map = {slice_index: True for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='trace_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)

    def DirectedTraceCaptureOverwriteProtectionCheckAlarmTest(self, TEST_ITERATION=2):
        """Test to Check Trace Capture Overwrite Protection

        In this test:
        1. Pattern helper, protected data, intended trace_num_limit and trace_num
            will be reset. This method is correlative to the pattern and the way
            the number of iteration of the loop in the pattern is determined.
        2. Determine the repeat times of the iteration in the patter in order
            to create intended length of trace capture stream
        3. Set the trace memory end address according to trace_num_limit reset
            in reset_trace_test()
        4. Write random generated data into the memory after trace stream end
            address and read the data back to make sure it is successfully written
        5. Run the pattern
        6. Read data after trace stream end address and compare it with the protected
            data to make sure that data after end address is not overwritten
        7. Check the trace stream overflow alarm for all the slices
        """
        for iteration in range(0, TEST_ITERATION):
            if not self.unit_test:
                self.reset_trace_test()
            self.pattern_helper.user_mode = 'HIGH'
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = self.trace_num - PCALL_NUM - UNCONDITIONAL_BRANCH_NUM - RETURN_NUM + CONDITIONAL_BRANCH_NOT_TAKEN
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                capture_attributes = {'capture_fails': 1, 'capture_ctv': 1}

                pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
    
                    NoCompareDriveZVectors length=1024
                    PatternId {id_0}
                    PCall PATTERN1
    
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    DriveZeroVectors length=1
    
                    PATTERN1:
                        I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm={repeat}
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            DriveZeroVectors length=100
                            I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR], cond=ZERO, invcond=1
                            DriveZeroVectors length=100
                        Return 
                '''
                self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
                self.assert_capture_stream_will_overflow(num=self.trace_num,
                                                         num_limit=self.trace_num_limit,
                                                         stream_name='TRACE')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                           attributes=capture_attributes)
                address_map = {slice_index: {'trace_stream_end_address': self.trace_stream_start_address
                                                                         + self.trace_num_limit * STREAM_BLOCK} for
                               slice_index in self.slices}
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_data_after_address('trace_stream_end_address')
                self.data_before_run = self.read_data_after_address('trace_stream_end_address')
                self.check_is_data_equal(self.data_before_run, self.data_out_of_bound, 'before run', 'trace')
                self.pattern_helper.execute_pattern_scenario()
                self.check_trace_capture_count(self.trace_num)
                self.data_after_run = self.read_data_after_address('trace_stream_end_address')
                self.check_is_data_equal(self.data_after_run, self.data_out_of_bound, 'after run', 'trace')
                expected_alarm_set_map = {slice_index: True for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='trace_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)
                for slice in self.slices:
                    self.env.hbicc.pat_gen.reset_capture_addr_counts_of_capture_control_register(slice=slice)
                expected_alarm_set_map = {slice_index: False for slice_index in self.slices}
                self.check_log_alarm(alarm_bit='trace_capture_overflow',
                                     by_slice=True,
                                     expected_alarm_set_map=expected_alarm_set_map)

    def check_trace_capture_count(self, expected_count, location_mark='After PatternRun'):
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            observed_count = capture.get_trace_capture_register_count()
            if observed_count != expected_count:
                self.Log('error',
                         f'{location_mark}: trace register count mismatch: observed: {observed_count} '
                         f'!= expected: {expected_count}')

    def reset_trace_test(self):
        self.reset_pattern_helper_slices_and_constants()
        self.randomize_protected_data()
        self.randomize_trace_num_and_limit()

    def randomize_trace_num_and_limit(self):
        self.trace_num_limit = random.randrange(max(4,
                                                    PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                                self.MAX_BLOCK_NUM_LIMIT, 2)
        self.trace_num = self.trace_num_limit + random.randint(1, self.MAX_OVERFLOW_BLOCK_NUM)

    def reset_pattern_helper_slices_and_constants(self):
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.user_mode = 'HIGH'
        self.pattern_helper.end_status = random.getrandbits(32)
        self.pattern_helper.is_simulator_active = True
        self.slices = [0, 1, 2, 3]
        dummy_slice_object = Slice(self.pattern_helper, self.env.hbicc, None, None, None, None)
        dummy_capture = Capture(dummy_slice_object)
        self.ctv_header_stream_start_address = dummy_capture.ctv_header_stream_start_address
        self.ctv_data_stream_start_address = dummy_capture.ctv_data_stream_start_address
        self.trace_stream_start_address = dummy_capture.trace_stream_start_address
        self.pm_stream_start_addresses = dummy_capture.pm_stream_start_addresses.copy()
        self.ctv_header_stream_end_address = dummy_capture.ctv_header_stream_end_address
        self.ctv_data_stream_end_address = dummy_capture.ctv_data_stream_end_address
        self.trace_stream_end_address = dummy_capture.trace_stream_end_address
        self.pm_stream_end_addresses = dummy_capture.pm_stream_end_addresses.copy()
        del dummy_capture
        del dummy_slice_object

    def DirectedErrorPMCaptureOverwriteProtectionTest(self, TEST_ITERATION=2):
        """Test to Check Trace Capture Overwrite Protection

        In this test:
        1. Pattern helper, protected data, intended trace_num_limit and trace_num
            will be reset. This method is correlative to the pattern and the way
            the number of iteration of the loop in the pattern is determined.
        2. Determine the repeat times of the iteration in the pattern in order
            to create intended length of pin error capture stream for each pm
        3. Set the pin error memory end address according to error_num_limit reset
            in reset_pin_error_test()
        4. Write random generated data into the memory after pin error stream end
            addresses and read the data back to make sure it is successfully written
        5. Run the pattern
        6. Read data after pin error stream end address and compare it with the protected
            data to make sure that data after end address is not overwritten
        7. Check the pin error stream overflow alarms for all the slices
        """
        for iteration in range(TEST_ITERATION):
            if not self.unit_test:
                self.reset_pin_error_test()
            self.pattern_helper.user_mode = 'HIGH'
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = (self.error_num - self.PIN_ERROR_EOB_BLOCK_NUM) // self.PIN_ERROR_BLOCK_PER_VECTOR
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                capture_attributes = {'capture_fails': 1,
                                      'capture_ctv': 0,
                                      'per_pattern_max_fail': self.error_num + 1,
                                      'global_max_fail': self.error_num + 1}
                pattern_string = f'''
                   PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                       S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
    
                       NoCompareDriveZVectors length=1024
                       PatternId {id_0}
                       PCall PATTERN0
    
                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                       DriveZeroVectors length=1
    
                       PATTERN0:            
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
    
                           SUBR:
                               ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
    
                           Return
                '''
                self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
                self.assert_capture_stream_will_overflow(num=self.error_num,
                                                         num_limit=self.error_num_limit,
                                                         stream_name='PIN ERROR')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                           attributes=capture_attributes)
                address_map = {slice_index: {'pm_stream_end_addresses':
                               {pm_index: self.pm_stream_start_addresses[pm_index] + self.error_num_limit * PIN_ERRROR_BLOCK
                                for pm_index in range(4)}}
                               for slice_index in self.slices}
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_protected_data_behind_pin_error_stream_and_verify_before_run()
                self.pattern_helper.execute_pattern_scenario()
                self.check_pin_error_capture_count()
                self.verify_protected_data_behind_pin_error_stream_not_changed_after_run()
                self.check_pin_error_stream_overflow_alarm()

    def DirectedErrorPMCaptureOverwriteProtectionAlarmResetTest(self, TEST_ITERATION=2):
        """Test to Check Trace Capture Overwrite Protection

        In this test:
        1. Pattern helper, protected data, intended trace_num_limit and trace_num
            will be reset. This method is correlative to the pattern and the way
            the number of iteration of the loop in the pattern is determined.
        2. Determine the repeat times of the iteration in the pattern in order
            to create intended length of pin error capture stream for each pm
        3. Set the pin error memory end address according to error_num_limit reset
            in reset_pin_error_test()
        4. Write random generated data into the memory after pin error stream end
            addresses and read the data back to make sure it is successfully written
        5. Run the pattern
        6. Read data after pin error stream end address and compare it with the protected
            data to make sure that data after end address is not overwritten
        7. Check the pin error stream overflow alarms for all the slices
        """
        for iteration in range(TEST_ITERATION):
            if not self.unit_test:
                self.reset_pin_error_test()
            self.pattern_helper.user_mode = 'HIGH'
            id_0 = random.randint(0, ID_MAX_VALUE)
            repeat = (self.error_num - self.PIN_ERROR_EOB_BLOCK_NUM) // self.PIN_ERROR_BLOCK_PER_VECTOR
            with self.subTest(SubTest=f'Iteration: {iteration} Pattern Loop Repeat Count: {repeat}'):
                capture_attributes = {'capture_fails': 1,
                                      'capture_ctv': 0,
                                      'per_pattern_max_fail': self.error_num + 1,
                                      'global_max_fail': self.error_num + 1}
                pattern_string = f'''
                   PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                       S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                       NoCompareDriveZVectors length=1024
                       PatternId {id_0}
                       PCall PATTERN0

                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                       DriveZeroVectors length=1

                       PATTERN0:            
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]

                           SUBR:
                               ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                           Return
                '''
                self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
                self.assert_capture_stream_will_overflow(num=self.error_num,
                                                         num_limit=self.error_num_limit,
                                                         stream_name='PIN ERROR')
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                           attributes=capture_attributes)
                address_map = {slice_index: {'pm_stream_end_addresses':
                                                 {pm_index: self.pm_stream_start_addresses[
                                                                pm_index] + self.error_num_limit * PIN_ERRROR_BLOCK
                                                  for pm_index in range(4)}}
                               for slice_index in self.slices}
                self.pattern_helper.update_capture_start_and_end_address(address_map)
                self.write_protected_data_behind_pin_error_stream_and_verify_before_run()
                self.pattern_helper.execute_pattern_scenario()
                self.check_pin_error_capture_count()
                self.verify_protected_data_behind_pin_error_stream_not_changed_after_run()
                self.check_pin_error_stream_overflow_alarm()
                for slice in self.slices:
                    self.env.hbicc.pat_gen.reset_capture_addr_counts_of_capture_control_register(slice=slice)
                self.check_pin_error_stream_overflow_alarm(expected_alarm=False)

    def check_pin_error_stream_overflow_alarm(self, location_mark='', expected_alarm=True):
        expected_alarm_set_map = {slice_index: expected_alarm for slice_index in self.slices}
        for pm in self.env.hbicc.get_pin_multipliers():
            error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(pm.slot_index)
            self.check_log_alarm(alarm_bit=error_pm_x_capture_overflow,
                                 by_slice=True,
                                 expected_alarm_set_map=expected_alarm_set_map,
                                 location_mark=location_mark)

    def check_pin_error_stream_overflow_alarm_not_set(self):
        expected_alarm_set_map = {slice_index: False for slice_index in self.slices}
        for pm in self.env.hbicc.get_pin_multipliers():
            error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(pm.slot_index)
            self.check_log_alarm_not_set(alarm_bit=error_pm_x_capture_overflow,
                                         by_slice=True,
                                         expected_alarm_set_map=expected_alarm_set_map)

    def verify_protected_data_behind_pin_error_stream_not_changed_after_run(self):
        self.data_after_run = []
        for pm in self.env.hbicc.get_pin_multipliers():
            observed_data = self.read_data_after_address('pm_stream_end_addresses', pm_index=pm.slot_index)
            for data in observed_data:
                self.data_after_run.append(data)
            self.check_is_data_equal(observed_data, self.data_out_of_bound, 'after run', 'error', pm.slot_index)

    def write_protected_data_behind_pin_error_stream_and_verify_before_run(self):
        self.data_before_run = []
        for pm in self.env.hbicc.get_pin_multipliers():
            self.write_data_after_address('pm_stream_end_addresses', pm_index=pm.slot_index)
            observed_data = self.read_data_after_address('pm_stream_end_addresses', pm_index=pm.slot_index)
            for data in observed_data:
                self.data_before_run.append(data)
            self.check_is_data_equal(observed_data, self.data_out_of_bound, 'before run', f'error', pm.slot_index)

    def reset_pin_error_test(self):
        self.reset_pattern_helper_slices_and_constants()
        self.randomize_protected_data()
        self.randomize_error_num_and_limit()

    def DirectedErrorPMCaptureKeepaliveOverflowTest(self):
        """Overflow the pin error capture twice with alarm reset while keep alive between the two overflows
            The test:
                1. Set up end address and launch the pattern which will overflow the pin error memory
                2. Software wait until pattern finish first overflow and send up trigger to software
                3. Pattern goes to keep alive after sending the up trigger
                4. Software check alarm, capture count and overwrite protection and then reset capture,
                    start and end address as well as the alarm while pattern keeping alive
                5. Software send down trigger to resume pattern and wait for pattern to finish
                6. Pattern  resumes and overflows the updated pin error capture memory again
                7. After pattern finishes, software check the alarm, capture count and overwrite
                    protection again.
        """
        self.reset_pin_error_keepalive_test()
        self.pattern_helper.user_mode = 'HIGH'
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False

        repeat_1 = self.error_num_1 // self.PIN_ERROR_BLOCK_PER_VECTOR
        repeat_2 = (self.error_num_2 - self.PIN_ERROR_EOB_BLOCK_NUM) // self.PIN_ERROR_BLOCK_PER_VECTOR

        max_fail = max(self.error_num_1, self.error_num_2) + 1
        
        capture_attributes = {'capture_fails': 1,
                              'capture_ctv': 0,
                              'per_pattern_max_fail': max_fail,
                              'global_max_fail': max_fail}

        self.set_up_keepalive_triggers()
        up_trigger = self.trigger_utility_up.construct_trigger_word()
        pattern_execute_thread = threading.Thread(target=self.pattern_helper.execute_pattern_scenario)
        trigger_sending_thread = threading.Thread(target=self.software_pin_error_stream_check_during_keep_alive)
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        NoCompareDriveZVectors length=1024
                        PatternId 0x1
                        PCall PATTERN1

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x2
                        PCall PATTERN2

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0xFFFFFFFF 
                        PCall PATTERN3

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x4
                        PCall PATTERN4

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:            
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1]

                            SUBR1:
                                ConstantVectors length={repeat_1}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                            Return

                        PATTERN2:
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR2]

                            SUBR2:
                            NoCompareDriveZVectors length=1024
                            I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={up_trigger}
                            NoCompareDriveZVectors length=1024
                            Return


                        PATTERN3:                
                            NoCompareDriveZVectors length=1024
                            I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
                            LOOP:
                            NoCompareDriveZVectors length={10 * 1024}
                            I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
                            Return


                        PATTERN4:            
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR4]

                            SUBR4:
                                ConstantVectors length={repeat_2}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                            Return
                    '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.assert_capture_stream_will_overflow(num=self.error_num_1,
                                                 num_limit=self.error_num_limit_1,
                                                 stream_name='PIN ERROR')
        self.overwrite_protection_test_set_up_before_launch(capture_attributes, pattern_string)
        trigger_sending_thread.start()
        pattern_execute_thread.start()
        pattern_execute_thread.join()
        trigger_sending_thread.join()

    def overwrite_protection_test_set_up_before_launch(self, capture_attributes, pattern_string):
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        address_map = {slice_index: {'pm_stream_end_addresses':
                                     {pm_index: self.pm_stream_start_addresses[pm_index] +
                                      self.error_num_limit_1 * PIN_ERRROR_BLOCK for
                                      pm_index in range(4)}}
                       for slice_index in self.slices}
        self.pattern_helper.update_capture_start_and_end_address(address_map)
        self.write_protected_data_behind_pin_error_stream_and_verify_before_run()

    def set_up_keepalive_triggers(self):
        self.trigger_utility_up = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.trigger_utility_up.setup_trigger_utility_up(trigger_type=3, dut_id=32)
        self.trigger_utility_down = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.trigger_utility_down.setup_trigger_utility_down(trigger_type=0, dut_id=32)

    def software_pin_error_stream_check_during_keep_alive(self):
        self.wait_for_up_trigger()
        self.check_pin_error_stream_in_one_pattern(self.error_num_1 * self.STREAM_BLOCK_PER_PIN_ERROR_PACKAGE, 'Pattern 1')
        self.pattern_helper.process_keepalive_capture_reset()
        self.update_addresses_and_protected_data()
        self.reset_alarm()
        self.trigger_utility_down.software_keep_alive_resume()
        if self.pattern_helper.is_pattern_execution_complete_and_end_status_updated():
            self.Log('info', f'{"*"*30}Pattern Run Finish Observed in software thread{"*"*30}')
            self.check_pin_error_stream_in_one_pattern(self.error_num_2 * self.STREAM_BLOCK_PER_PIN_ERROR_PACKAGE,
                                                       'Pattern 2')

    def wait_for_up_trigger(self):
        if not self.trigger_utility_up.rc_trigger_check():
            self.Log('error', 'RCTC not Received Patgen Trigger')
        self.Log('info', f'{"*" * 30}Up Trigger Waiting Finished{"*" * 30}')

    def update_addresses_and_protected_data(self):
        address_map = {slice_index: {'pm_stream_end_addresses': {
            pm_index: self.pm_stream_start_addresses[pm_index] + self.error_num_limit_2 * PIN_ERRROR_BLOCK
            for pm_index in range(4)}
                                     } for slice_index in self.slices}
        self.pattern_helper.update_capture_start_and_end_address(streams_address=address_map)
        self.write_protected_data_behind_pin_error_stream_and_verify_before_run()

    def check_pin_error_stream_in_one_pattern(self, expected_capture_count, location_mark):
        self.verify_protected_data_behind_pin_error_stream_not_changed_after_run()
        self.check_pin_error_stream_overflow_alarm(location_mark)
        self.check_pin_error_capture_count(expected_capture_count, location_mark)

    def reset_alarm(self):
        for slice_index in self.slices:
            self.env.hbicc.pat_gen.reset_alarm_on_slice(slice_index)
        self.check_pin_error_stream_overflow_alarm_not_set()

    def check_pin_error_capture_count(self, expected_count=None, location_maker=''):
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            for pm in self.env.hbicc.get_pin_multipliers():
                if expected_count == None:
                    expected_count = self.env.hbicc.model.patgen.get_pin_error_count(pm, slice_object.index)
                observed_count = capture.get_pin_error_capture_register_count(pm.slot_index)
                if observed_count != expected_count:
                    self.Log('error',
                             f'{location_maker}: '
                             f'capture count mismatch: '
                             f'slice {slice_object.index}, pm {pm.slot_index}: error count read from register '
                             f'{observed_count } != expected {expected_count}, '
                             f'difference = {observed_count - expected_count}')

    def reset_pin_error_keepalive_test(self):
        self.reset_pattern_helper_slices_and_constants()
        self.randomize_error_num_and_limit_for_double_overflow()
        self.randomize_protected_data()

    def randomize_error_num_and_limit_for_double_overflow(self):
        self.error_num_limit_1 = random.randint(self.PIN_ERROR_EOB_BLOCK_NUM, self.MAX_BLOCK_NUM_LIMIT)
        self.error_num_1 = self.get_even_error_num(self.error_num_limit_1)
        self.error_num_limit_2 = random.randint(self.PIN_ERROR_EOB_BLOCK_NUM, self.MAX_BLOCK_NUM_LIMIT)
        self.error_num_2 = self.get_even_error_num(self.error_num_limit_2)

    def get_even_error_num(self, error_num_limit):
        if error_num_limit % 2:
            error_num = error_num_limit + 1 + random.randrange(self.PIN_ERROR_BLOCK_PER_VECTOR,
                                                                 self.MAX_OVERFLOW_BLOCK_NUM,
                                                                 self.PIN_ERROR_BLOCK_PER_VECTOR)
        else:
            error_num = error_num_limit + random.randrange(self.PIN_ERROR_BLOCK_PER_VECTOR,
                                                             self.MAX_OVERFLOW_BLOCK_NUM,
                                                             self.PIN_ERROR_BLOCK_PER_VECTOR)
        self.Log('debug', f'error package count before keepalive = {error_num}; '
                          f'error package count limit = {error_num_limit}')
        return error_num

    def DirectedCTVDataCaptureKeepaliveOverflowTest(self):
        """Overflow the ctv data capture twice with alarm reset while keep alive between the two overflows

            The test:
                1. Set up end address and launch the pattern which will overflow the ctv data stream memory
                2. Software wait until the pattern finish first overflow and send up trigger to software
                3. Pattern goes to keep alive after sending the up trigger
                4. Software check alarm, capture count and overwrite protection and then reset capture,
                    end address as well as the alarm while pattern keeping alive
                5. Software send down trigger to resume pattern and wait for pattern to finish
                6. Pattern resumes and overflows the updated ctv data stream memory again
                7. After pattern finishes, software check the alarm, capture count and overwrite
                    protection again.
        """
        self.stream_keepalive_checker = KeepAliveCTVDataStreamChecker(self)
        self.stream_keepalive_checker.reset_keep_alive_test()
        self.pattern_helper.user_mode = 'High'
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        repeat1, repeat2 = self.stream_keepalive_checker.get_vector_repeat_num()

        capture_attributes = {'capture_fails': 1,
                              'capture_ctv': 1}

        self.set_up_keepalive_triggers()
        up_trigger = self.trigger_utility_up.construct_trigger_word()
        pattern_execute_thread = threading.Thread(target=self.pattern_helper.execute_pattern_scenario)
        trigger_sending_thread = threading.Thread(target=self.software_stream_check_during_keep_alive)
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        
                        
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x1
                        PCall PATTERN1

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x2
                        PCall PATTERN2
                        
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b} 
                        PatternId 0xFFFFFFFF 
                        PCall PATTERN3

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x4
                        PCall PATTERN4

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:            
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1]

                            SUBR1:
                                ConstantVectors length={repeat1}, ctv=1, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH

                            Return

                        PATTERN2:
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR2]

                            SUBR2:
                            NoCompareDriveZVectors length=1024
                            I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={up_trigger}
                            NoCompareDriveZVectors length=1024
                            Return


                        PATTERN3:                
                            NoCompareDriveZVectors length=1024
                            I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
                            LOOP:
                                DriveZeroVectors length={1024 * 10}
                                I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
                            Return


                        PATTERN4:            
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR4]

                            SUBR4:
                                ConstantVectors length={repeat2}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                            Return
                    '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.stream_keepalive_checker.test_set_up(capture_attributes, pattern_string)
        trigger_sending_thread.start()
        pattern_execute_thread.start()
        pattern_execute_thread.join()
        trigger_sending_thread.join()

    def software_stream_check_during_keep_alive(self):
        self.wait_for_up_trigger()
        self.stream_keepalive_checker.check_stream_in_one_pattern(expected_package_num=self.package_num_1, pattern=1)
        self.pattern_helper.process_keepalive_capture_reset()
        self.stream_keepalive_checker.update_addresses_and_protected_data()
        self.trigger_utility_down.software_keep_alive_resume()
        if self.pattern_helper.is_pattern_execution_complete_and_end_status_updated():
            self.Log('info', f'{"*"*30}Pattern Run Finish Observed in software thread{"*"*30}')
            self.stream_keepalive_checker.\
                check_stream_in_one_pattern(expected_package_num=self.package_num_2, pattern=2)
        self.pattern_helper.capture_control_reset()
        self.stream_keepalive_checker.check_alarm(location_mark=2, expected_alarm=False)

    def DirectedTraceCaptureKeepaliveOverflowTest(self):
        """Overflow the trace capture twice with alarm reset while keep alive between the two overflows

            The test:
                1. Set up end address and launch the pattern which will overflow the trace stream memory
                2. Software wait until pattern finish first overflow and send up trigger to software
                3. Pattern goes to keep alive after sending the up trigger
                4. Software check alarm, capture count and overwrite protection and then reset capture,
                    end address as well as the alarm while pattern keeping alive
                5. Software send down trigger to resume pattern and wait for pattern to finish
                6. Pattern  resumes and overflows the updated trace stream memory again
                7. After pattern finishes, software check the alarm, capture count and overwrite
                    protection again.
        """
        self.stream_keepalive_checker = KeepAliveTraceStreamChecker(self)
        self.stream_keepalive_checker.reset_keep_alive_test()
        self.package_num_2 = self.package_num_1
        self.package_num_limit_2 = self.package_num_limit_1
        self.pattern_helper.user_mode = 'Low'
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False

        repeat_1, repeat_2 = self.stream_keepalive_checker.get_vector_repeat_num()

        capture_attributes = {'capture_fails': 0,
                              'capture_ctv': 0}

        self.set_up_keepalive_triggers()
        up_trigger = self.trigger_utility_up.construct_trigger_word()
        pattern_execute_thread = threading.Thread(target=self.pattern_helper.execute_pattern_scenario)
        trigger_sending_thread = threading.Thread(target=self.software_stream_check_during_keep_alive)
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        NoCompareDriveZVectors length=1024
                        PatternId 0x1
                        PCall PATTERN1

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x2
                        PCall PATTERN2

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0xFFFFFFFF 
                        PCall PATTERN3

                        PatternId 0x4
                        PCall PATTERN4

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:            
                            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm={repeat_1}
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1]
                            SUBR1:
                                DriveZeroVectors length=1024
                                I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1], cond=ZERO, invcond=1
                                DriveZeroVectors length=1024
                            Return 


                        PATTERN2:
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR2]

                            SUBR2:
                            NoCompareDriveZVectors length=1024
                            I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={up_trigger}
                            NoCompareDriveZVectors length=1024
                            Return


                        PATTERN3:                
                            NoCompareDriveZVectors length=1024
                            I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
                            LOOP:
                            NoCompareDriveZVectors length={1024 * 10}
                            I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
                            Return


                        PATTERN4:            
                            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm={repeat_2}
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR4]
                            SUBR4:
                                DriveZeroVectors length=1024
                                I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR4], cond=ZERO, invcond=1
                                DriveZeroVectors length=1024
                            Return 
                    '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.stream_keepalive_checker.test_set_up(capture_attributes, pattern_string)
        trigger_sending_thread.start()
        pattern_execute_thread.start()
        pattern_execute_thread.join()
        trigger_sending_thread.join()

    def DirectedCTVHeaderCaptureKeepaliveOverflowTest(self):
        """Overflow the ctv header capture twice with alarm reset while keep alive between the two overflows

            The test:
                1. Set up end address and launch the pattern which will overflow the ctv header stream memory
                2. Software wait until pattern finish first overflow and send up trigger to software
                3. pattern goes to keep alive after sending the up trigger
                4. Software check alarm, capture count and overwrite protection and then reset capture,
                    end address as well as the alarm while pattern keeping alive
                5. Software send down trigger to resume pattern and wait for pattern to finish
                6. Pattern  resumes and overflows the updated ctv header stream memory again
                7. After pattern finishes, software check the alarm, capture count and overwrite
                    protection again.
        """
        self.stream_keepalive_checker = KeepAliveCTVHeaderStreamChecker(self)
        self.stream_keepalive_checker.reset_keep_alive_test()
        self.pattern_helper.user_mode = 'Low'
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False

        repeat_1, repeat_2 = self.stream_keepalive_checker.get_vector_repeat_num()

        capture_attributes = {'capture_fails': 0,
                              'capture_ctv': 1}

        self.set_up_keepalive_triggers()
        up_trigger = self.trigger_utility_up.construct_trigger_word()
        pattern_execute_thread = threading.Thread(target=self.pattern_helper.execute_pattern_scenario)
        trigger_sending_thread = threading.Thread(target=self.software_stream_check_during_keep_alive)
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        NoCompareDriveZVectors length=1024
                        PatternId 0x1
                        PCall PATTERN1

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x2
                        PCall PATTERN2

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0xFFFFFFFF 
                        PCall PATTERN3

                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        PatternId 0x4
                        PCall PATTERN4

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:            
                           NoCompareDriveZVectors length=1024
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1]

                           SUBR1:
                               ConstantVectors length={repeat_1}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                           Return



                        PATTERN2:
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR2]

                            SUBR2:
                            NoCompareDriveZVectors length=1024
                            I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={up_trigger}
                            DriveZeroVectors length=1024
                            Return


                        PATTERN3:                
                            NoCompareDriveZVectors length=1024
                            I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
                            LOOP:
                            NoCompareDriveZVectors length={1024 * 10}
                            I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
                            Return


                        PATTERN4:            
                           NoCompareDriveZVectors length=1024
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR4]

                           SUBR4:
                               ConstantVectors length={repeat_2}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                           Return

                    '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.stream_keepalive_checker.test_set_up(capture_attributes, pattern_string)
        trigger_sending_thread.start()
        pattern_execute_thread.start()
        pattern_execute_thread.join()
        trigger_sending_thread.join()


class KeepAliveStreamChecker(fval.Object):
    def __init__(self, directed_test):
        super().__init__()
        self.test = directed_test
        self.alarm_name = None
        self.protected_end_address_name = None

    def reset_keep_alive_test(self):
        pass

    def check_stream_in_one_pattern(self, expected_package_num, pattern, expected_alarm=True):
        location_mark = f'Pattern {pattern}'
        self.check_protected_data(location_mark)
        self.check_alarm(location_mark)
        self.check_stream_count(expected_package_num, location_mark)

    def check_protected_data(self, location_mark):
        pass

    def check_stream_count(self, expected_count, location_mark):
        pass

    def check_alarm(self, location_mark, expected_alarm=True):
        expected_alarm_set_map = {slice_index: expected_alarm for slice_index in self.test.slices}
        self.test.check_log_alarm(alarm_bit=self.alarm_name,
                                  by_slice=True,
                                  expected_alarm_set_map=expected_alarm_set_map,
                                  location_mark=location_mark)

    def reset_alarm(self):
        for slice_index in self.test.slices:
            self.test.env.hbicc.pat_gen.reset_alarm_on_slice(slice_index)

        expected_alarm_set_map = {slice_index: False for slice_index in self.test.slices}
        self.test.check_log_alarm_not_set(alarm_bit=self.alarm_name,
                                          by_slice=True,
                                          expected_alarm_set_map=expected_alarm_set_map)

    def update_addresses_and_protected_data(self):
        pass

    def check_is_data_equal(self, observed_data, expected_data, location_mark):
        for iteration, data in enumerate(observed_data):
            if data != expected_data:
                self.Log('error', f'{location_mark}: slice {self.test.slices[iteration]}: '
                                  f'protected data not the same as expected')

    def get_vector_repeat_num(self):
        pass

    def test_set_up(self, capture_attributes, pattern_string):
        self.test.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.test.slices,
                                                                        attributes=capture_attributes)
        self.setup_address(self.test.package_num_limit_1)
        self.test.write_data_after_address(self.protected_end_address_name)
        data_before_run = self.test.read_data_after_address(self.protected_end_address_name)
        self.check_is_data_equal(data_before_run, self.test.data_out_of_bound, 'Before Run')

    def setup_address(self, package_limit):
        pass


class KeepAliveCTVDataStreamChecker(KeepAliveStreamChecker):
    def __init__(self, directed_test):
        super().__init__(directed_test)
        self.alarm_name = 'ctv_row_capture_overflow'
        self.protected_end_address_name = 'ctv_data_stream_end_address'

    def randomize_ctv_data_num_and_limit_for_double_overflow(self):

        self.test.package_num_limit_1 = random.randint(1, self.test.MAX_BLOCK_NUM_LIMIT)
        self.test.package_num_1 = random.randint(self.test.package_num_limit_1 + 1,
                                         self.test.package_num_limit_1 + self.test.MAX_OVERFLOW_BLOCK_NUM)
        if self.test.package_num_limit_1 >= self.test.package_num_1:
            self.Log('error', f'keepalive test capture package num 1 didn\'t overflow limit 1')

        self.test.package_num_limit_2 = random.randint(1, self.test.MAX_BLOCK_NUM_LIMIT)
        self.test.package_num_2 = random.randint(self.test.package_num_limit_2 + 1,
                                         self.test.package_num_limit_2 +self.test.MAX_OVERFLOW_BLOCK_NUM)

        if self.test.package_num_limit_2 >= self.test.package_num_2:
            self.Log('error', f'keepalive test capture package num 2 didn\'t overflow limit 2')

    def reset_keep_alive_test(self):
        self.randomize_ctv_data_num_and_limit_for_double_overflow()
        self.test.randomize_protected_data()
        self.test.reset_pattern_helper_slices_and_constants()

    def check_stream_in_one_pattern(self, expected_package_num, pattern, expected_alarm=True):
        location_mark = f'Pattern {pattern}'
        self.check_protected_data(location_mark)
        self.check_alarm(location_mark)
        self.check_stream_count(expected_package_num * len(self.test.env.hbicc.get_pin_multipliers()), location_mark)

    def check_stream_count(self, expected_count, location_mark):
        self.test.check_ctv_data_capture_count(expected_count, location_mark)

    def check_protected_data(self, location_mark):
        observed_data = self.test.read_data_after_address('ctv_data_stream_end_address')
        self.check_is_data_equal(observed_data, self.test.data_out_of_bound, location_mark)

    def update_addresses_and_protected_data(self):
        self.setup_address(self.test.package_num_limit_2)
        self.test.write_data_after_address('ctv_data_stream_end_address')

    def get_vector_repeat_num(self):
        repeat_1 = self.test.package_num_1 * self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE
        repeat_2 = (self.test.package_num_2 - CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM) * self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE
        return repeat_1, repeat_2

    def setup_address(self, package_limit):
        address_map = {slice_index: {
            'ctv_data_stream_end_address': self.test.ctv_data_stream_start_address + package_limit * CTV_DATA_STREAM_BLOCK_SIZE}
            for slice_index in self.test.slices}
        self.test.pattern_helper.update_capture_start_and_end_address(streams_address=address_map)


class KeepAliveTraceStreamChecker(KeepAliveStreamChecker):
    def __init__(self, directed_test):
        super().__init__(directed_test)
        self.alarm_name = 'trace_capture_overflow'
        self.protected_end_address_name = 'trace_stream_end_address'

    def randomize_trace_num_and_limit_for_double_overflow(self):

        self.test.package_num_limit_1 = random.randrange(max(4, PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM +
                                                             KEEPALIVE_TRACE_BLOCK_NUM),
                                                         self.test.MAX_BLOCK_NUM_LIMIT, 2)
        self.test.package_num_1 = self.test.package_num_limit_1 + random.randint(1, self.test.MAX_OVERFLOW_BLOCK_NUM)

        if self.test.package_num_limit_1 >= self.test.package_num_1:
            self.Log('error', f'keepalive test capture package num 1 didn\'t overflow limit 1')

        self.test.package_num_limit_2 = random.randrange(max(4, PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                                         self.test.MAX_BLOCK_NUM_LIMIT, 2)
        self.test.package_num_2 = self.test.package_num_limit_2 + random.randint(1, self.test.MAX_OVERFLOW_BLOCK_NUM)

        if self.test.package_num_limit_2 >= self.test.package_num_2:
            self.Log('error', f'keepalive test capture package num 2 didn\'t overflow limit 2')

    def reset_keep_alive_test(self):
        self.randomize_trace_num_and_limit_for_double_overflow()
        self.test.randomize_protected_data()
        self.test.reset_pattern_helper_slices_and_constants()

    def check_stream_count(self, expected_count, location_mark):
        self.test.check_trace_capture_count(expected_count, location_mark)

    def check_protected_data(self, location_mark):
        observed_data = self.test.read_data_after_address('trace_stream_end_address')
        self.check_is_data_equal(observed_data, self.test.data_out_of_bound, location_mark)

    def get_vector_repeat_num(self):
        repeat_1 = self.test.package_num_1 - PCALL_NUM - UNCONDITIONAL_BRANCH_NUM - RETURN_NUM \
                   + CONDITIONAL_BRANCH_NOT_TAKEN - KEEPALIVE_TRACE_BLOCK_NUM
        repeat_2 = self.test.package_num_2 - PCALL_NUM - UNCONDITIONAL_BRANCH_NUM - RETURN_NUM \
                   + CONDITIONAL_BRANCH_NOT_TAKEN
        return repeat_1, repeat_2

    def setup_address(self, package_limit):
        address_map = {slice_index: {
            'trace_stream_end_address': self.test.trace_stream_start_address + package_limit * STREAM_BLOCK}
            for slice_index in self.test.slices}
        self.test.pattern_helper.update_capture_start_and_end_address(streams_address=address_map)


class KeepAliveCTVHeaderStreamChecker(KeepAliveStreamChecker):
    def __init__(self, directed_test):
        super().__init__(directed_test)
        self.alarm_name = 'ctv_header_capture_overflow'
        self.protected_end_address_name = 'ctv_header_stream_end_address'

    def randomize_ctv_header_num_and_limit_for_double_overflow(self):
        self.test.package_num_limit_1 = \
            random.randrange(self.test.MIN_HEADER_BLOCK_NUM_LIMIT, self.test.MAX_BLOCK_NUM_LIMIT,
                             self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE)
        self.test.package_num_1 = \
            random.randrange(self.test.package_num_limit_1 + self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE,
                             self.test.package_num_limit_1 + self.test.MAX_OVERFLOW_BLOCK_NUM,
                             self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE) // len(self.test.slices)

        self.test.package_num_limit_2 = \
            random.randrange(self.test.MIN_HEADER_BLOCK_NUM_LIMIT, self.test.MAX_BLOCK_NUM_LIMIT,
                             self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE)
        self.test.package_num_2 = random.randrange(
            self.test.package_num_limit_2 + self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE,
            self.test.package_num_limit_2 + self.test.MAX_OVERFLOW_BLOCK_NUM,
            self.test.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE) // len(self.test.slices)

    def reset_keep_alive_test(self):
        self.test.reset_pattern_helper_slices_and_constants()
        self.randomize_ctv_header_num_and_limit_for_double_overflow()
        self.test.randomize_protected_data()

    def check_stream_count(self, expected_count, location_mark):
        self.test.check_ctv_header_capture_count(expected_count, location_mark)

    def check_alarm(self, location_mark, expected_alarm=True):
        if location_mark == 'Pattern 1':
            total_header_num = self.test.package_num_1
            header_num_limit = self.test.package_num_limit_1
        else:
            total_header_num = self.test.package_num_2
            header_num_limit = self.test.package_num_limit_2
        expected_alarm_set_map = \
            self.test.pattern_helper.get_expected_ctv_header_overflow_map(total_header_num, header_num_limit, expected_alarm=expected_alarm)
        self.test.check_log_alarm(alarm_bit=self.alarm_name,
                                  by_slice=True,
                                  expected_alarm_set_map=expected_alarm_set_map,
                                  location_mark=location_mark)

    def check_protected_data(self, location_mark):
        observed_data = self.test.read_data_after_address('ctv_header_stream_end_address')
        self.check_is_data_equal(observed_data, self.test.data_out_of_bound, location_mark)

    def get_vector_repeat_num(self):
        repeat_1 = self.test.package_num_1 * 1000
        repeat_2 = self.test.package_num_2 * 1000
        return repeat_1, repeat_2

    def setup_address(self, package_limit):
        address_map = \
            {slice_index:
                 {'ctv_header_stream_end_address':
                      self.test.ctv_header_stream_start_address + package_limit * STREAM_BLOCK}
             for slice_index in self.test.slices}
        self.test.pattern_helper.update_capture_start_and_end_address(streams_address=address_map)

    def update_addresses_and_protected_data(self):
        self.setup_address(self.test.package_num_limit_2)
        self.test.write_data_after_address('ctv_header_stream_end_address')
