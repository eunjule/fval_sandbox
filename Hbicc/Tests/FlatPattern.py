# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random

from Common.fval import skip
from Hbicc.Tests.HbiccTest import HbiccTest
from Common.fval import expected_failure
from Hbicc.testbench.PatternUtility import PatternHelper

PADDING = 1024


class Functional(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))
        self.slices.sort()

    def DirectedDriveLowFailOnOnePinTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare Low on all but one pin per vector.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1
                
                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                NoCompareDriveZVectors length={PADDING}

                %repeat 30
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLH
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                V ctv=0, mtv=0, lrpt=0, data=0vHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                %end
                NoCompareDriveZVectors length={PADDING}
                Return
                '''
        pattern_string3 = f'''
                       PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                       S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                       PatternId {id_0}
                       PCall PATTERN1
                           I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xabcddef}
                           NoCompareDriveZVectors length=1

                       PATTERN1:
                       I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                       SUBR:
                       DriveZeroVectors length={PADDING}
                       DriveZeroVectors length=3500
                       DriveZeroVectors length={PADDING}

                       Return
                       '''
        pattern_string2 = f'''
                               PATTERN_START:
                               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                               S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                               PatternId {id_0}
                               PCall PATTERN1
                                   I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xabcddef}
                                   NoCompareDriveZVectors length=1

                               PATTERN1:
                               I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                               SUBR:
                               DriveZeroVectors length={PADDING}
                               CompareHighVectors length=2000
                               DriveZeroVectors length={PADDING}

                               Return
                               '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=[0,1],address_and_pattern_list={0:pattern_string2})
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1,slices=[2,3,4],address_and_pattern_list={0x19eb28300:pattern_string3})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighFailOnOnePinTest(self):
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
           PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            
            PatternId {id_0}
            PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
            PATTERN1:
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            DriveZeroVectors length={PADDING}
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHL
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vHLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            V ctv=0, mtv=0, lrpt=0, data=0vLHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
            DriveZeroVectors length={PADDING}
            Return
            '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowOnAllPinsCompareLowTest(self):
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1
                
                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=1000
                CompareLowVectors length=1024
                DriveZeroVectors length=250

               Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighOnAllPinsCompareHighTest(self):
        """
        1. Create DC trigger queue
        2. Drive High on all pins. Compare High pattern on all pins and all vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
       
        PatternId {id_0}
        PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
        PATTERN1:
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length={PADDING}
        CompareHighVectors length=1000
        DriveZeroVectors length={PADDING}
        
        Return
        '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowOnAllPinsCompareHighTest(self):
        """
        1. Create DC trigger queue
        2. Drive High on all pins. Compare Low pattern on all pins and all vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
          PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            
            PatternId {id_0}
            PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
            PATTERN1:
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            DriveZeroVectors length={PADDING}
            CompareHighVectors length=1000
            DriveZeroVectors length={PADDING}
            
            Return
           '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighOnAllPinsCompareLowTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare High pattern on all pins and all vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
        
        PatternId {id_0}
        PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
        PATTERN1:
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length={PADDING}
        CompareLowVectors length=1024
        DriveZeroVectors length={PADDING}

        Return
        '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowFailAlternatePinCompareLowTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare Low then High on same pin for all pins on all vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
        
        PatternId {id_0}
        PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
        PATTERN1:
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length={PADDING}
        %repeat 760
        V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
        V ctv=0, mtv=0, lrpt=0, data=0vHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLH
        %end
        DriveZeroVectors length={PADDING}

        Return
        '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighFailAlternatePinCompareHighTest(self):
        """
        1. Create DC trigger queue
        2. Drive High on all pins. Compare Low then High on same pin for all pins on all vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            
            PatternId {id_0}
            PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
            PATTERN1:
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            DriveZeroVectors length={PADDING}
            %repeat 890
            V ctv=0, mtv=0, lrpt=0, data=0vHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLH
            V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
            %end
            DriveZeroVectors length={PADDING}
            
            Return
           '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighFailAlternatePairVectorCompareHighTest(self):
        """
        1. Create DC trigger queue
        2. Drive High on all pins. Compare Low on all pins on two vectors, then Compare High on two vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            
            PatternId {id_0}
            PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
            PATTERN1:
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            DriveHighVectors length={PADDING}
            %repeat 100
               CompareHighVectors length=2
               CompareLowVectors length=2
            %end
            DriveHighVectors length={PADDING}
            
            Return
           '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowFailAlternatePairVectorCompareLowTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare Low on all pins on two vectors, then Compare High on two vectors.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
       PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

        PatternId {id_0}
        PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
        PATTERN1:
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length={PADDING}
        %repeat 100
            CompareLowVectors length=2
            CompareHighVectors length=2
        %end
        DriveZeroVectors length={PADDING}

        Return
        '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedOverflowFailDriveLowCompareLowTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low and Compare Low on all pins and all vectors
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        repeats = random.randint(10000, 30000)
        pattern_string = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
        
        PatternId {id_0}
        PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
        PATTERN1:
            
        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
        SUBR:
        DriveZeroVectors length=1000
        CompareLowVectors length={repeats}
        DriveZeroVectors length=250

        Return
        '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedSaturateErrorCountDriveHighCompareLowTest(self):
        """
        1. Create DC trigger queue
        2. Create Drive High fail on one pin pattern.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.slices = [0]
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
          PATTERN_START:
           S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
           S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
           S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
           S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
           
           PatternId {id_0}
           PCall PATTERN1
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            NoCompareDriveZVectors length=1
                
            PATTERN1:
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            DriveZeroVectors length=1000
            CompareLowVectors length=66000
            DriveZeroVectors length=250
            
            Return
           '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighOnAllPinsCompareLowWithLocalRepeatTest(self):
        self.pattern_helper.user_mode = 'HIGH'
        local_repeat = random.randint(0, 31)
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1
                
                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length={PADDING}
                CompareLowVectors length=1, lrpt={local_repeat}
                DriveZeroVectors length=250

                Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighOnAllPinsCompareLowWithMtvSetTest(self):
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        mtv_mask_index = random.randint(0, 31)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1
                
            PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length={PADDING}
                CompareLowVectors length=10, mtv={mtv_mask_index}
                DriveZeroVectors length=250

                Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.mtv_mask_object.modify_mask(value=0x7ffffffff, channel_sets=range(16),
                                                        mask_index=[mtv_mask_index])

        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighCompareLowUsingKeepMode(self):
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1
                
            PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                NoCompareDriveZVectors length={PADDING}
                CompareLowVectors length=2
                CompareHighVectors length=1
                KeepVectors length=500
                Return
                '''
        pattern_helper = PatternHelper(self.env)
        self.pattern_helper.user_mode = 'HIGH'

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[2])
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowCompareLowUsingKeepMode(self):
        id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=1
                
            PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                NoCompareDriveZVectors length={PADDING}
                CompareLowVectors length=1
                KeepVectors length=1000

                Return
                '''
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighUsingPatternandRetainingStateUsingKeepMode(self):
        id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                
                PatternId {id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                KeepVectors length=1
                
            PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                NoCompareDriveZVectors length={PADDING}
                DriveHighVectors length=1

                Return
                '''
        self.pattern_helper.user_mode = 'RELEASE'
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveOneCompareHighEvenToOddExternalLoopbackTest(self):
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        repeat_iteration = random.randint(1, 1000)
        pattern_string = f'''
                        
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                        S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                        
                        PatternId {id_0}
                        PCall PATTERN1
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                        DriveZeroVectors length=1
                
                        PATTERN1:
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                        NoCompareDriveZVectors length=100
                        ConstantVectors length={repeat_iteration}, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                        NoCompareDriveZVectors length=1000
                        
                        Return
                      '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0])
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveOneCompareLowOddToEvenExternalLoopbackTest(self):
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        repeat_iteration = random.randint(1, 1000)
        pattern_string = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

            PatternId {id_0}
            PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1
                
            PATTERN1:
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
                NoCompareDriveZVectors length=100
                ConstantVectors length={repeat_iteration}, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
                NoCompareDriveZVectors length=100
                
            Return
            '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0])
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveZeroCompareHighwOddToEvenExternalLoopbackTest(self):
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        repeat_iteration = random.randint(1, 1000)
        pattern_string = f'''

                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                    
                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                    DriveZeroVectors length=1
                
                    PATTERN1:
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                    SUBR:
                    NoCompareDriveZVectors length=100
                    ConstantVectors length={repeat_iteration}, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    NoCompareDriveZVectors length=100
                    
                    Return
                      '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0])
        self.pattern_helper.execute_pattern_scenario()


    def DirectedFlatPatternKeepPinTest(self):
        """Tests to see if K (keep pin) symbol correctly maintains status of of the previous symbol.
        The pattern includes transtion from L, H, X, Z, 0, 1 to K symbol and back"""
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        pattern_string = f'''

                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId {id_0}
                    PCall PATTERN1
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                    DriveZeroVectors length=1

                    PATTERN1:
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                    SUBR:
                    NoCompareDriveZVectors length=500
                    V ctv=0, mtv=0, lrpt=9, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    V ctv=0, mtv=0, lrpt=9, data=0vKHKHKHKHKHKHKHKHKHKHKHKHKHKHKHKHKHK                    
                    V ctv=0, mtv=0, lrpt=9, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    V ctv=0, mtv=0, lrpt=9, data=0vKHKHKHKHKHKHKHKHKHKHKHKHKHKHKHKHKHK 
                    V ctv=0, mtv=0, lrpt=9, data=0v0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0X0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    V ctv=0, mtv=0, lrpt=9, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                    V ctv=0, mtv=0, lrpt=9, data=0v0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0K0
                    NoCompareDriveZVectors length=500

                    Return
                      '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0])
        self.pattern_helper.execute_pattern_scenario()

    def DirectedMultiSliceGroupDriveLowFailOnOnePinTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare Low on all but one pin per vector.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)

        pattern_string1 = f'''
                       PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                       S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                       PatternId {id_0}
                       PCall PATTERN1
                           I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xabcddef}
                           NoCompareDriveZVectors length=1

                       PATTERN1:
                       I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                       SUBR:
                       DriveZeroVectors length={PADDING}
                       DriveZeroVectors length=3500
                       DriveZeroVectors length={PADDING}

                       Return
                       '''
        pattern_string2 = f'''
                               PATTERN_START:
                               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                               S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                               PatternId {id_0}
                               PCall PATTERN1
                                   I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xabcddef}
                                   NoCompareDriveZVectors length=1

                               PATTERN1:
                               I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                               SUBR:
                               DriveZeroVectors length={PADDING}
                               CompareHighVectors length=2000
                               DriveZeroVectors length={PADDING}

                               Return
                               '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=[0, 1],
                                                                   address_and_pattern_list={0: pattern_string2})
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=[2, 3, 4], address_and_pattern_list={
            0x19eb28300: pattern_string1})
        self.pattern_helper.execute_pattern_scenario()