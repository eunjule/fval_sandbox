# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import contextlib
from Common.fval import TestCase
from Common.instruments import tester as t
from Hbicc.testbench.env import HbiccEnv


class HbiccTest(TestCase):
    dutName = 'hbicc'

    def setUp(self, tester=None):
        self.LogStart()
        if tester == None:
            tester = t.get_tester()
        self.env = HbiccEnv(self, tester=tester)
        self.env.hbicc.pat_gen.set_all_slices_inactive()
        self.env.hbicc.pat_gen.set_un_park_duts_to_default()
        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False

    def tearDown(self):
        self.env.read_all_available_fpga_temperature()
        self.env.print_vccio_vref()
        self.LogEnd()
        self.env.hbicc.model.patgen.skip_multi_burst_simulation = False

    def take_snapshot_if_enabled(self):
        self.env.take_snapshot_if_enabled()

    def process_snapshots(self):
        self.env.process_snapshots()

    def disable_simulator(self):
        self.env.disable_simulator()

    def enable_simulator(self):
        self.env.enable_simulator()


class HbiccTestDecorators(object):
    '''Decorating methods

    Example:
        from Common.fval.testcase import HbiccTestDecorators

        @HbiccTestDecorators.some_decorator
        def test():
            execute_test()
    '''

    @classmethod
    def execute_on_all_pms(self, decorated):
        """Run decorated function on all available PMs using subtests.

        In the method to be executed on all PMs, use self.pm to refer to each available PM.
        """

        def wrapper(test, *args, **kwargs):
            pms = [test.env.hbicc.psdb_0_pin_0,
                   test.env.hbicc.psdb_0_pin_1,
                   test.env.hbicc.psdb_1_pin_0,
                   test.env.hbicc.psdb_1_pin_1]
            for index, pm in enumerate(pms):
                psdb, pin = index // 2, index % 2
                pin_multiplier = f'PSDB_{psdb}_PIN_{pin}'
                with test.subTest(pin_multiplier=pin_multiplier):
                    if pm is None:
                        test.Log('error', f'{pin_multiplier} not found')
                        continue
                    test.pm = pm
                    decorated(test, *args, **kwargs)

        return wrapper


class PinStackDelay():
    def __init__(self, hbicc, rx_delay, tx_delay=0, pins=range(140), slices=range(5)):
        self.hbicc = hbicc
        self.rx_delay = rx_delay
        self.tx_delay = tx_delay
        self.pins = pins
        self.slices = slices

    def __enter__(self):
        for pm in self.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=self.rx_delay, TXdelay=self.tx_delay,
                                           slices=self.slices, pins=self.pins)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        for pm in self.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins()


class Si5344Clock():
    def __init__(self, pg, frequency_mhz):
        self.pg = pg
        self.frequency_mhz = frequency_mhz

    def __enter__(self):
        self.pg.set_clock(si5344=True)
        self.pg.set_si5344_frequency_via_configs_file(frequency_mhz=self.frequency_mhz)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.pg.set_clock(pll=True)
        self.pg.reset_si5344_via_pin()
