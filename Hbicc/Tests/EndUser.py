# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""End user scenarios that are too specific for other categories

Examples:
Altering the return stack in a subroutine.
"""

from Common.fval import skip

from Hbicc.Tests.HbiccTest import HbiccTest
from Common.fval import skip
from Hbicc.testbench.assembler import HbiPatternAssembler
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.rpg import RPG
from Common.fval import skip
import random
class Special(HbiccTest):
    """Unique scenarios"""
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status= random.randint(0, 0xFFFFFFFF)
        self.slices = [0, 1, 2, 3, 4]
        self.hbicc = self.env.hbicc

    def DirectedAlterReturnStackTest(self):
        """Modify the return stack in a subroutine

        Return to a different offset than the offset after the calling
        instruction.
        """
        pattern = HbiPatternAssembler()
        pattern_string = f'''
            PATTERN1:                          
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b11111111111111111111111111111111111 # ctp mask
    
                AddressVectors 500
                I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=2
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN2]
                 
                AddressVectors 500
                I optype=BRANCH, br=RET                                                                         
                  
            PATTERN2:                          
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b11111111111111111111111111111111111 # ctp mask
                
                AddressVectors 500
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=eval[ALT_END]
                PushRegister 0
                I optype=BRANCH, br=RET 
            
            PATTERN_START:                                                                                 
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b11111111111111111111111111111111111 # ctp mask
                
                I optype=VECTOR, vptype=VPLOG, vpop=PATID_I, imm=1
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[PATTERN1] 
            
            PATTERN_END:                                                                                   
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                DriveZeroVectors length=1
                
            ALT_END:                                                                                   
                StopPattern {self.pattern_helper.end_status}
                DriveZeroVectors length=1
      
                '''
        pattern.LoadString(pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state='HIGH',
                                                                   slices=self.slices)
        address=pattern.Resolve('eval[PATTERN_START]')
        slice_address_dict={0:address,1:address,2:address,3:address,4:address}
        self.pattern_helper.set_pattern_start_address(slice_address_dict)
        self.pattern_helper.execute_pattern_scenario()