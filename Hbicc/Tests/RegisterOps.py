# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

PIN_DIRECTION = {'DRIVE_LOW': 0x0000, 'DRIVE_HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE_PATTERN_CNTRL': 0xFFFF}


class Functional(HbiccTest):
    def DirectedImmediateToRegisterTest(self):
        
        expected_values = [random.randint(0, 0xFFFFFFFF) for x in range(32)]
        
        pattern = f'''
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={expected_values[0]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=1, imm={expected_values[1]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=2, imm={expected_values[2]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=3, imm={expected_values[3]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=4, imm={expected_values[4]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=5, imm={expected_values[5]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=6, imm={expected_values[6]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=7, imm={expected_values[7]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=8, imm={expected_values[8]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=9, imm={expected_values[9]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=10, imm={expected_values[10]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=11, imm={expected_values[11]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=12, imm={expected_values[12]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=13, imm={expected_values[13]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=14, imm={expected_values[14]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=15, imm={expected_values[15]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=16, imm={expected_values[16]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=17, imm={expected_values[17]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=18, imm={expected_values[18]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=19, imm={expected_values[19]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=20, imm={expected_values[20]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=21, imm={expected_values[21]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=22, imm={expected_values[22]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=23, imm={expected_values[23]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=24, imm={expected_values[24]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=25, imm={expected_values[25]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=26, imm={expected_values[26]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=27, imm={expected_values[27]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=28, imm={expected_values[28]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=29, imm={expected_values[29]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=30, imm={expected_values[30]}
            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=31, imm={expected_values[31]}
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            DriveZeroVectors length=1
        '''
        pattern_helper = PatternHelper(self.env)
        pattern_helper.create_slice_channel_set_pattern_combo(pattern, fixed_drive_state='LOW')
        pattern_helper.execute_pattern_scenario()
        self.check_register_operation(expected_values, range(32))

    def DirectedSetAndDecrementRegistersTest(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare Low on all but one pin per vector.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        pattern_helper = PatternHelper(self.env)
        custom_slice_list = [0, 1, 2, 3, 4]
        fixed_drive_state = 'LOW'
        pattern_helper.track_error_count = False

        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=100
                LOOP:
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=0
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1
                '''

        pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=custom_slice_list)
        pattern_helper.execute_pattern_scenario()

        self.check_register_operation([100], [0])

    def check_register_operation(self, expected, register_index=range(32)):
        register_type = self.env.hbicc.pat_gen.registers.CENTRAL_DOMAIN_REGISTER
        for index in register_index:
            register = self.env.hbicc.pat_gen.read_slice_register(register_type, index=index)
            if register.value != expected[index]:
                self.Log('error', f'Domain Register 0x{register.ADDR + index * 4}. Expected {expected[index]} does not match Observed {register.value}')

    def DirectedSetAndDecrementRegisters2Test(self):
        """
        1. Create DC trigger queue
        2. Drive Low on all pins. Compare Low on all but one pin per vector.
        3. Execute pattern
        4. Read the error count register for failure.

        """
        pattern_helper = PatternHelper(self.env)
        custom_slice_list = [0, 1, 2, 3, 4]
        fixed_drive_state = 'LOW'
        pattern_helper.track_error_count = False
        ctp = 0b10
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp}
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=1, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=2, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=3, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=4, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=5, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=6, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=7, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=8, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=9, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=10, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=11, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=12, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=13, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=14, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=15, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=16, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=17, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=18, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=19, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=20, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=21, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=22, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=23, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=24, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=25, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=26, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=27, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=28, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=29, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=30, imm=100
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=31, imm=100

                LOOP:
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=0
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=1, dest=1, imm=1
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=2, dest=2, imm=2
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=3, dest=3, imm=3
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=4, dest=4, imm=4
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=5, dest=5, imm=5
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=6, dest=6, imm=6
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=7, dest=7, imm=7
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=8, dest=8, imm=8
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=9, dest=9, imm=9
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=10, dest=10, imm=10
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=11, dest=11, imm=11
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=12, dest=12, imm=12
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=13, dest=13, imm=13
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=14, dest=14, imm=14
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=15, dest=15, imm=15
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=16, dest=16, imm=16
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=17, dest=17, imm=17
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=18, dest=18, imm=18
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=19, dest=19, imm=19
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=20, dest=20, imm=20
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=21, dest=21, imm=21
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=22, dest=22, imm=22
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=23, dest=23, imm=23
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=24, dest=24, imm=24
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=25, dest=25, imm=25
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=26, dest=26, imm=26
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=27, dest=27, imm=27
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=28, dest=28, imm=28
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=29, dest=29, imm=29
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=30, dest=30, imm=30
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=31, dest=31, imm=31
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`END`, cond=COND.ZERO, invcond=0
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.NONE, invcond=0
                END:
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1
                '''

        pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=custom_slice_list)
        pattern_helper.execute_pattern_scenario()

        self.check_register_operation([100, 96, 92, 88, 84, 80, 76, 72, 68, 64, 60, 56, 52, 48, 44, 40, 36, 32, 28, 24, 20, 16, 12, 8, 4, 0, 22, 19, 16, 13, 10, 7])
        