# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Loops are an important feature for pattern execution

Check loop performance and exit behavior.
"""

import random
import threading
from time import perf_counter

from Common import fval
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.trigger_utility import TriggerUtility
from Hbicc.testbench.PatternUtility import PatternHelper

PADDING = 1024
SLICECOUNT = range(5)


class Performance(HbiccTest):
    """Characterizations for loop size and behavior across slices"""

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.custom_slice_list = [0, 1, 2, 3, 4]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.custom_slice_list)

    def tearDown(self):
        super().tearDown()

    def DirectedGotoLoopSizeTest(self):
        """Check how small a count based loop can be without under run.

        Run 100,000 loops in a pattern at minimum tester period.The loop
        contains a subtraction operation and goto conditional on the zero
        flag.

        Start with a loop size of 70 vectors and decrease until failure.
        """
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False

        performancelimit = 70
        loopcount = 1000000

        for vector_count in range(performancelimit, 30, -1):
            self.Log('info', 'Beginning test for Vector Length = {}'.format(vector_count))
            pattern_string = f'''
                               PATTERN_START:
                               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
    
                               DriveZeroVectors length=10000
                               I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loopcount}
    
                               LOOP:
                                NoCompareDriveZVectors length={vector_count}
    
                               I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                               I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1
                               
                               END:
                               I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                               DriveZeroVectors length=1
                           '''
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.custom_slice_list,
                                                                       fixed_drive_state='HIGH')
            try:
                self.pattern_helper.execute_pattern_scenario()
                self.Log('info', 'Passed at Vector Length = {}'.format(vector_count))
                passedVectorCount = vector_count
            except self.pattern_helper.CriticalAlarmError:
                self.Log('info', 'Vector Length failed at = {}'.format(vector_count))
                self.Log('info', 'Vector Length last passed at = {}'.format(passedVectorCount))
                break

    def DirectedParallelLoopExitTest (self):
        """Show that Keep Alive Loops work down to a specified loop size

        Keep alive loops exit based on a software trigger.

        Parallel loops on multiple slices must exit the loop on the same
        iteration. Demonstrate that this is robust at the required loop
        size and for larger loops.
        """

        vector_count = 70
        loop_count = 3000

        self.Log('info', 'Beginning test for Vector Length = {}'.format(vector_count))
        self.pattern_helper.end_status = 0xcafef00d
        self.trigger_utility.hbicc.rc.clear_sw_trigger_fifo()
        dut_id = random.randint(0, 62)
        payload = self.sendTrigger(3, 0, 1, self.env.hbicc.pat_gen, None, dut_id)
        pattern_string = f'''
                                PATTERN_START:
                                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                                S stype=DUT_SERIAL, channel_set={dut_id},   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                                Call PATTERN0

                                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                                NoCompareDriveZVectors length=1

                                PATTERN0:
                                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                                SUBR:
                                NoCompareDriveZVectors length=10000

                                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loop_count}
                                LOOP1:
                                    PatternId 0xFFFFFFFF
                                    PCall PATTERN1
                                     I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                                     I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP1`, cond=COND.ZERO, invcond=1
                                Return

                                PATTERN1:
                                    I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=1, imm=0
                                    I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF #Sw trigger received bit being cleared
                                    I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={payload}
                                    LOOP:
                                        DriveZeroVectors length={vector_count} #set the ctv vector for 1
                                        I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=1, opdest=ALUDEST.REG, dest=1, imm=0x00000001 #increments the number of loops executed
                                        I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
                                Return
                               '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern=pattern_string,
                                                                   slices=self.custom_slice_list,
                                                                   fixed_drive_state='HIGH')
        self.pattern_helper.start_execution()
        trigger_count = 0
        if self.wait_for_enable_burst():
            while (trigger_count < loop_count):
                try:
                    self.trigger_utility.check_for_trigger_entry_in_rc_software_fifo()
                    trigger_count += 1
                    observed_source = self.trigger_utility.hbicc.rc.read_sw_trigger_fifo_source()
                    observed_trigger = self.trigger_utility.hbicc.rc.read_sw_trigger_from_rc()
                    self.trigger_utility.log_error(payload, observed_trigger, observed_source)

                    # payload1 = self.sendSoftwareTrigger()
                    dut_id = random.randint(0, 62)
                    payload1 = self.sendTrigger(0, 0, 0, self.env.hbicc.rc, self.env.hbicc.pat_gen, dut_id)
                    self.env.hbicc.rc.send_trigger_from_sender_to_receiver(self.trigger_utility.sender, 16, payload1,
                                                                           self.trigger_utility.receiver)
                    self.trigger_utility.is_sw_flag_set()
                except self.trigger_utility.MissedTriggerError:
                    self.Log('error', f'Trigger not received from Patgen in loop number: {trigger_count} ')
                    self.check_count_across_slices()
                    break
        else:
            self.Log('error', 'Burst start never happened exiting test')
        self.trigger_utility.report_errors()
        self.pattern_helper.post_burst_processing()
        self.check_count_across_slices()

    def sendTrigger(self, triggerType, targetResource, upTrigger, sender, receiver, dut_id):
        self.trigger_utility.trigger_type = triggerType
        self.trigger_utility.target_resource = targetResource
        self.trigger_utility.software_trigger_up_only = upTrigger
        self.trigger_utility.sender = sender
        self.trigger_utility.receiver = receiver
        self.trigger_utility.dut_id = dut_id
        payload1 = self.trigger_utility.construct_trigger_word()
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'dut_id': self.trigger_utility.dut_id})
        self.trigger_utility.configure_trigger_control_register()
        return payload1

    def check_count_across_slices(self):
        reg_count = []
        for slice in self.custom_slice_list:
            reg_count.append(self.env.hbicc.pat_gen.read_slice_register(
                self.env.hbicc.pat_gen.registers.CENTRAL_DOMAIN_REGISTER, slice, index=1).Data)
        if (reg_count[0] == reg_count[1] == reg_count[2] == reg_count[3] == reg_count[4]):
            self.Log('info', 'Equal number of loops ran across all slices')
        else:
            self.Log('error',
                     f'Mismatch in number of loops run {reg_count[0]},{reg_count[1]},{reg_count[2]},{reg_count[3]},{reg_count[4]}')

    def wait_for_enable_burst(self):
        timeout_seconds = 10
        return self.pattern_helper.enable_burst_event.wait(timeout_seconds)


class KeepAlive(HbiccTest):

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))

    def DirectedResetCaptureMemoryAndCountTest(self):
        """Demonstrate capture reset during keep alive

        1) Cause a capture to occur
        2) Enter a keep alive loop
        3) Reset captures via software
        4) Resume pattern execution
        5) Cause another capture to occur
        6) Check that only the 2nd capture is recorded
        """
        self.dut_id = 32
        self.trigger_utility_up = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.trigger_utility_flag = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.pattern_helper.end_status = 0x1234abcd
        self.setup_trigger_utility_up()
        expect_error_stream = self.create_expect_error_stream()
        trigger_up_payload = self.softwaretriggeruppayload()

        send_trigger_thread = threading.Thread(target=self.software_execution)
        pattern_execute_thread = threading.Thread(target=self.pattern_helper.execute_pattern_scenario)
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

            PatternId 0x1
            PCall PATTERN1

            PatternId 0x2
            PCall PATTERN2

            PatternId 0xFFFFFFFF 
            PCall PATTERN3

            PatternId 0x4
            PCall PATTERN4

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
            NoCompareDriveZVectors length=1

            PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR1]
                SUBR1:
                CompareLowVectors length=1024
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                Return

            PATTERN2:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR2]
                SUBR2:
                NoCompareDriveZVectors length=1000
                I optype=EXT, extop=TRIGGER, action=TRGEXT_I, imm={trigger_up_payload}
                NoCompareDriveZVectors length=1000
                Return

            PATTERN3:                
                DriveZeroVectors length=1000
                I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0xFFBFFFFF
                LOOP:
                DriveZeroVectors length=10000
                I optype=BRANCH, br=GOTO_I, cond=COND.SWTRIGRCVD, imm=`LOOP`, invcond=1 
                Return

            PATTERN4:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR4]
                SUBR4:
                CompareLowVectors length=1024
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_capture_expect_steam(expect_error_stream)
        send_trigger_thread.start()
        pattern_execute_thread.start()
        pattern_execute_thread.join()
        send_trigger_thread.join()
        self.trigger_utility_flag.is_sw_flag_set()
        self.trigger_utility_flag.restore_initial_trigger_control_register_settings()

    @fval.skip_snapshot
    @fval.deactivate_simulator
    def DirectedStayAliveForOneMinuteTest(self):
        """" Demonstrates KeepAlive Loop running patterns for 60 seconds without unexpected behaviour.

         This test runs a long keep alive pattern for 60 seconds and shows that there is no unexpected behavior.
         """
        self.pattern_helper.user_mode = 'LOW'
        self.slices = [0, 1, 2, 3, 4]
        vectors_per_second = 200000000
        vectors_per_loop = 10000
        loop_per_sec=vectors_per_second//vectors_per_loop
        loop_number=loop_per_sec*60
        ctp = 0b00000000000000000001001000000000010
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loop_number}

                PatternId 0xFFFFFFFF 
                PCall KEEP_ALIVE

                KEEP_ALIVE:
                    DriveZeroVectors length={vectors_per_loop}
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`KEEP_ALIVE`, cond=COND.ZERO, invcond=1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'check_capture_results': False})
        self.pattern_helper.e32_interval = 128
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def software_execution(self):
        trigger_match = self.rc_trigger_check()
        if trigger_match:
            self.Log('debug', 'RCTC Received Patgen Trigger')
        else:
            self.Log('error', 'RCTC not Received Patgen Trigger')
        self.software_capture_check()
        self.software_capture_reset()
        self.setup_trigger_utility_flag()
        self.software_keep_alive_resume()
        self.software_capture_check()

    def software_execution_backup(self):
        trigger_match = self.rc_trigger_check()
        if trigger_match:
            self.software_capture_check()
            self.software_capture_reset()
            self.setup_trigger_utility_flag()
            self.software_keep_alive_resume()
            self.software_capture_check()
        else:
            self.Log('error', 'Trigger not received at Patgen')

    def rc_trigger_check(self):
        payload = self.trigger_utility_up.construct_trigger_word()
        trigger_match = False
        start_time = perf_counter()
        rctc_trigger_wait_time = 10
        while 1:
            wait_time = perf_counter() - start_time
            if wait_time > rctc_trigger_wait_time:
                break
            try:
                self.trigger_utility_up.check_for_trigger_entry_in_rc_software_fifo()
                observed_source = self.trigger_utility_up.hbicc.rc.read_sw_trigger_fifo_source()
                observed_trigger = self.trigger_utility_up.hbicc.rc.read_sw_trigger_from_rc()
                self.trigger_utility_up.log_error(payload, observed_trigger, observed_source)
                self.trigger_utility_up.report_errors()
            except:
                self.Log('info', f'Trigger not received from Patgen for {wait_time}s')
            else:
                trigger_match = True
                self.Log('info', 'RCTC_Trigger_Find')
                break
        return trigger_match

    def setup_trigger_utility_up(self):
        self.trigger_utility_up.trigger_type = 3
        self.trigger_utility_up.target_resource = 0
        self.trigger_utility_up.software_trigger_up_only = 1
        self.trigger_utility_up.dut_id = self.dut_id
        self.trigger_utility_up.errors = []
        self.trigger_utility_up.sender = self.env.hbicc.pat_gen
        self.trigger_utility_up.expected_source = 0x10
        self.trigger_utility_up.hbicc.rc.clear_sw_trigger_fifo()
        self.trigger_utility_up.attribute_holder.update(
            {'target_resource': self.trigger_utility_up.target_resource, 'dut_id': self.dut_id})
        self.trigger_utility_up.configure_trigger_control_register()

    def setup_trigger_utility_flag(self):
        self.trigger_utility_flag.trigger_type = 0
        self.trigger_utility_flag.target_resource = 0
        self.trigger_utility_flag.dut_id = self.dut_id
        self.trigger_utility_flag.sender = self.env.hbicc.rc
        self.trigger_utility_flag.receiver = self.env.hbicc.pat_gen
        self.trigger_utility_flag.attribute_holder.update({'target_resource': self.trigger_utility_flag.target_resource,
                                                           'dut_id': self.trigger_utility_flag.dut_id})
        self.trigger_utility_flag.configure_trigger_control_register()

    def software_capture_check(self):
        self.pattern_helper.process_keepalive_capture()

    def software_capture_reset(self):
        self.pattern_helper.process_keepalive_capture_reset()

    def software_keep_alive_resume(self):
        payload = self.trigger_utility_flag.construct_trigger_word()
        self.env.hbicc.rc.send_trigger_from_sender_to_receiver(self.trigger_utility_flag.sender, 16, payload, self.trigger_utility_flag.receiver)

    def softwaretriggeruppayload(self):
        return self.trigger_utility_up.construct_trigger_word()

    def create_expect_error_stream(self):
        s0_pm0_first_capture = 0x1000001002000000403000000000000000000000403000004120000000500000000000201c700000000ffffffff00000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000500000000000201c700000000ffffffff00000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005000000000002014700000000ffffffff000000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005000000000002014700000000ffffffff000000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000500000000000200c700000000ffffffff00000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000500000000000200c700000000ffffffff00000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005000000000002004700000000ffffffff000000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005000000000002004700000000ffffffff000000000002004700000000ffffffff
        s0_pm1_first_capture = 0x1000001002000000403000000000000000000000403000004120000000510000000000201c700000000ffffffff10000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000510000000000201c700000000ffffffff10000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005100000000002014700000000ffffffff100000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005100000000002014700000000ffffffff100000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000510000000000200c700000000ffffffff10000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000510000000000200c700000000ffffffff10000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005100000000002004700000000ffffffff100000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005100000000002004700000000ffffffff100000000002004700000000ffffffff
        s0_pm2_first_capture = 0x1000001002000000403000000000000000000000403000004120000000520000000000201c700000000ffffffff20000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000520000000000201c700000000ffffffff20000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005200000000002014700000000ffffffff200000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005200000000002014700000000ffffffff200000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000520000000000200c700000000ffffffff20000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000520000000000200c700000000ffffffff20000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005200000000002004700000000ffffffff200000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005200000000002004700000000ffffffff200000000002004700000000ffffffff
        s0_pm3_first_capture = 0x1000001002000000403000000000000000000000403000004120000000530000000000201c700000000ffffffff30000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000530000000000201c700000000ffffffff30000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005300000000002014700000000ffffffff300000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005300000000002014700000000ffffffff300000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000530000000000200c700000000ffffffff30000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000530000000000200c700000000ffffffff30000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005300000000002004700000000ffffffff300000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005300000000002004700000000ffffffff300000000002004700000000ffffffff
        s0_pm0_second_capture = 0x4000002002040fd7f6e0000000000000000000004020000000d0000000b010000207ebfb7400000000000000000010000207ebfb7400000000000000000000004000002002040fd7f6e0000000000000000000004020000000d0000000b010000207ebfb7400000000000000000010000207ebfb7400000000000000000000004000001002040fd7f6c00000000000000000000040000003ae30000000b000000207ebfb64700000000ffffffff000000207ebfb64700000000ffffffff000004000001002040fd7f6c00000000000000000000040000003ae30000000b000000207ebfb64700000000ffffffff000000207ebfb64700000000ffffffff
        s0_pm1_second_capture = 0x4000002002040fd7f6e0000000000000000000004020000000d0000000b110000207ebfb7400000000000000000110000207ebfb7400000000000000000000004000002002040fd7f6e0000000000000000000004020000000d0000000b110000207ebfb7400000000000000000110000207ebfb7400000000000000000000004000001002040fd7f6c00000000000000000000040000003ae30000000b100000207ebfb64700000000ffffffff100000207ebfb64700000000ffffffff000004000001002040fd7f6c00000000000000000000040000003ae30000000b100000207ebfb64700000000ffffffff100000207ebfb64700000000ffffffff
        s0_pm2_second_capture = 0x4000002002040fd7f6e0000000000000000000004020000000d0000000b210000207ebfb7400000000000000000210000207ebfb7400000000000000000000004000002002040fd7f6e0000000000000000000004020000000d0000000b210000207ebfb7400000000000000000210000207ebfb7400000000000000000000004000001002040fd7f6c00000000000000000000040000003ae30000000b200000207ebfb64700000000ffffffff200000207ebfb64700000000ffffffff000004000001002040fd7f6c00000000000000000000040000003ae30000000b200000207ebfb64700000000ffffffff200000207ebfb64700000000ffffffff
        s0_pm3_second_capture = 0x4000002002040fd7f6e0000000000000000000004020000000d0000000b310000207ebfb7400000000000000000310000207ebfb7400000000000000000000004000002002040fd7f6e0000000000000000000004020000000d0000000b310000207ebfb7400000000000000000310000207ebfb7400000000000000000000004000001002040fd7f6c00000000000000000000040000003ae30000000b300000207ebfb64700000000ffffffff300000207ebfb64700000000ffffffff000004000001002040fd7f6c00000000000000000000040000003ae30000000b300000207ebfb64700000000ffffffff300000207ebfb64700000000ffffffff
        s0_first_capture = {0: s0_pm0_first_capture, 1: s0_pm1_first_capture,
                         2: s0_pm2_first_capture, 3: s0_pm3_first_capture}
        s0_second_capture = {0: s0_pm0_second_capture, 1: s0_pm1_second_capture,
                          2: s0_pm2_second_capture, 3: s0_pm3_second_capture}
        s0_expect_error_stream = {0: s0_first_capture, 1: s0_second_capture}
        s1_pm0_first_capture = 0x1000001002000000403000000000000000000000403000004120000000502000000000201c700000000ffffffff02000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000502000000000201c700000000ffffffff02000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005020000000002014700000000ffffffff020000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005020000000002014700000000ffffffff020000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000502000000000200c700000000ffffffff02000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000502000000000200c700000000ffffffff02000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005020000000002004700000000ffffffff020000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005020000000002004700000000ffffffff020000000002004700000000ffffffff
        s1_pm1_first_capture = 0x1000001002000000403000000000000000000000403000004120000000512000000000201c700000000ffffffff12000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000512000000000201c700000000ffffffff12000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005120000000002014700000000ffffffff120000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005120000000002014700000000ffffffff120000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000512000000000200c700000000ffffffff12000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000512000000000200c700000000ffffffff12000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005120000000002004700000000ffffffff120000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005120000000002004700000000ffffffff120000000002004700000000ffffffff
        s1_pm2_first_capture = 0x1000001002000000403000000000000000000000403000004120000000522000000000201c700000000ffffffff22000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000522000000000201c700000000ffffffff22000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005220000000002014700000000ffffffff220000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005220000000002014700000000ffffffff220000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000522000000000200c700000000ffffffff22000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000522000000000200c700000000ffffffff22000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005220000000002004700000000ffffffff220000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005220000000002004700000000ffffffff220000000002004700000000ffffffff
        s1_pm3_first_capture = 0x1000001002000000403000000000000000000000403000004120000000532000000000201c700000000ffffffff32000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000532000000000201c700000000ffffffff32000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005320000000002014700000000ffffffff320000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005320000000002014700000000ffffffff320000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000532000000000200c700000000ffffffff32000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000532000000000200c700000000ffffffff32000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005320000000002004700000000ffffffff320000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005320000000002004700000000ffffffff320000000002004700000000ffffffff
        s1_pm0_second_capture = 0x400000200203f02249e0000000000000000000004020000000d0000000b0300001f81124f4000000000000000000300001f81124f40000000000000000000000400000200203f02249e0000000000000000000004020000000d0000000b0300001f81124f4000000000000000000300001f81124f40000000000000000000000400000100203f02249c00000000000000000000040000003ae30000000b0200001f81124e4700000000ffffffff0200001f81124e4700000000ffffffff00000400000100203f02249c00000000000000000000040000003ae30000000b0200001f81124e4700000000ffffffff0200001f81124e4700000000ffffffff
        s1_pm1_second_capture = 0x400000200203f02249e0000000000000000000004020000000d0000000b1300001f81124f4000000000000000001300001f81124f40000000000000000000000400000200203f02249e0000000000000000000004020000000d0000000b1300001f81124f4000000000000000001300001f81124f40000000000000000000000400000100203f02249c00000000000000000000040000003ae30000000b1200001f81124e4700000000ffffffff1200001f81124e4700000000ffffffff00000400000100203f02249c00000000000000000000040000003ae30000000b1200001f81124e4700000000ffffffff1200001f81124e4700000000ffffffff
        s1_pm2_second_capture = 0x400000200203f02249e0000000000000000000004020000000d0000000b2300001f81124f4000000000000000002300001f81124f40000000000000000000000400000200203f02249e0000000000000000000004020000000d0000000b2300001f81124f4000000000000000002300001f81124f40000000000000000000000400000100203f02249c00000000000000000000040000003ae30000000b2200001f81124e4700000000ffffffff2200001f81124e4700000000ffffffff00000400000100203f02249c00000000000000000000040000003ae30000000b2200001f81124e4700000000ffffffff2200001f81124e4700000000ffffffff
        s1_pm3_second_capture = 0x400000200203f02249e0000000000000000000004020000000d0000000b3300001f81124f4000000000000000003300001f81124f40000000000000000000000400000200203f02249e0000000000000000000004020000000d0000000b3300001f81124f4000000000000000003300001f81124f40000000000000000000000400000100203f02249c00000000000000000000040000003ae30000000b3200001f81124e4700000000ffffffff3200001f81124e4700000000ffffffff00000400000100203f02249c00000000000000000000040000003ae30000000b3200001f81124e4700000000ffffffff3200001f81124e4700000000ffffffff
        s1_first_capture = {0: s1_pm0_first_capture, 1: s1_pm1_first_capture,
                         2: s1_pm2_first_capture, 3: s1_pm3_first_capture}
        s1_second_capture = {0: s1_pm0_second_capture, 1: s1_pm1_second_capture,
                          2: s1_pm2_second_capture, 3: s1_pm3_second_capture}
        s1_expect_error_stream = {0: s1_first_capture, 1: s1_second_capture}
        s2_pm0_first_capture = 0x1000001002000000403000000000000000000000403000004120000000504000000000201c700000000ffffffff04000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000504000000000201c700000000ffffffff04000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005040000000002014700000000ffffffff040000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005040000000002014700000000ffffffff040000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000504000000000200c700000000ffffffff04000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000504000000000200c700000000ffffffff04000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005040000000002004700000000ffffffff040000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005040000000002004700000000ffffffff040000000002004700000000ffffffff
        s2_pm1_first_capture = 0x1000001002000000403000000000000000000000403000004120000000514000000000201c700000000ffffffff14000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000514000000000201c700000000ffffffff14000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005140000000002014700000000ffffffff140000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005140000000002014700000000ffffffff140000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000514000000000200c700000000ffffffff14000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000514000000000200c700000000ffffffff14000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005140000000002004700000000ffffffff140000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005140000000002004700000000ffffffff140000000002004700000000ffffffff
        s2_pm2_first_capture = 0x1000001002000000403000000000000000000000403000004120000000524000000000201c700000000ffffffff24000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000524000000000201c700000000ffffffff24000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005240000000002014700000000ffffffff240000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005240000000002014700000000ffffffff240000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000524000000000200c700000000ffffffff24000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000524000000000200c700000000ffffffff24000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005240000000002004700000000ffffffff240000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005240000000002004700000000ffffffff240000000002004700000000ffffffff
        s2_pm3_first_capture = 0x1000001002000000403000000000000000000000403000004120000000534000000000201c700000000ffffffff34000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000534000000000201c700000000ffffffff34000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005340000000002014700000000ffffffff340000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005340000000002014700000000ffffffff340000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000534000000000200c700000000ffffffff34000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000534000000000200c700000000ffffffff34000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005340000000002004700000000ffffffff340000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005340000000002004700000000ffffffff340000000002004700000000ffffffff
        s2_pm0_second_capture = 0x400000200203eeaf31e0000000000000000000004020000000d0000000b0500001f75798f4000000000000000000500001f75798f40000000000000000000000400000200203eeaf31e0000000000000000000004020000000d0000000b0500001f75798f4000000000000000000500001f75798f40000000000000000000000400000100203eeaf31c00000000000000000000040000003ae30000000b0400001f75798e4700000000ffffffff0400001f75798e4700000000ffffffff00000400000100203eeaf31c00000000000000000000040000003ae30000000b0400001f75798e4700000000ffffffff0400001f75798e4700000000ffffffff
        s2_pm1_second_capture = 0x400000200203eeaf31e0000000000000000000004020000000d0000000b1500001f75798f4000000000000000001500001f75798f40000000000000000000000400000200203eeaf31e0000000000000000000004020000000d0000000b1500001f75798f4000000000000000001500001f75798f40000000000000000000000400000100203eeaf31c00000000000000000000040000003ae30000000b1400001f75798e4700000000ffffffff1400001f75798e4700000000ffffffff00000400000100203eeaf31c00000000000000000000040000003ae30000000b1400001f75798e4700000000ffffffff1400001f75798e4700000000ffffffff
        s2_pm2_second_capture = 0x400000200203eeaf31e0000000000000000000004020000000d0000000b2500001f75798f4000000000000000002500001f75798f40000000000000000000000400000200203eeaf31e0000000000000000000004020000000d0000000b2500001f75798f4000000000000000002500001f75798f40000000000000000000000400000100203eeaf31c00000000000000000000040000003ae30000000b2400001f75798e4700000000ffffffff2400001f75798e4700000000ffffffff00000400000100203eeaf31c00000000000000000000040000003ae30000000b2400001f75798e4700000000ffffffff2400001f75798e4700000000ffffffff
        s2_pm3_second_capture = 0x400000200203eeaf31e0000000000000000000004020000000d0000000b3500001f75798f4000000000000000003500001f75798f40000000000000000000000400000200203eeaf31e0000000000000000000004020000000d0000000b3500001f75798f4000000000000000003500001f75798f40000000000000000000000400000100203eeaf31c00000000000000000000040000003ae30000000b3400001f75798e4700000000ffffffff3400001f75798e4700000000ffffffff00000400000100203eeaf31c00000000000000000000040000003ae30000000b3400001f75798e4700000000ffffffff3400001f75798e4700000000ffffffff
        s2_first_capture = {0: s2_pm0_first_capture, 1: s2_pm1_first_capture,
                         2: s2_pm2_first_capture, 3: s2_pm3_first_capture}
        s2_second_capture = {0: s2_pm0_second_capture, 1: s2_pm1_second_capture,
                          2: s2_pm2_second_capture, 3: s2_pm3_second_capture}
        s2_expect_error_stream = {0: s2_first_capture, 1: s2_second_capture}
        s3_pm0_first_capture = 0x1000001002000000403000000000000000000000403000004120000000506000000000201c700000000ffffffff06000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000506000000000201c700000000ffffffff06000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005060000000002014700000000ffffffff060000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005060000000002014700000000ffffffff060000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000506000000000200c700000000ffffffff06000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000506000000000200c700000000ffffffff06000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005060000000002004700000000ffffffff060000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005060000000002004700000000ffffffff060000000002004700000000ffffffff
        s3_pm1_first_capture = 0x1000001002000000403000000000000000000000403000004120000000516000000000201c700000000ffffffff16000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000516000000000201c700000000ffffffff16000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005160000000002014700000000ffffffff160000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005160000000002014700000000ffffffff160000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000516000000000200c700000000ffffffff16000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000516000000000200c700000000ffffffff16000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005160000000002004700000000ffffffff160000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005160000000002004700000000ffffffff160000000002004700000000ffffffff
        s3_pm2_first_capture = 0x1000001002000000403000000000000000000000403000004120000000526000000000201c700000000ffffffff26000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000526000000000201c700000000ffffffff26000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005260000000002014700000000ffffffff260000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005260000000002014700000000ffffffff260000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000526000000000200c700000000ffffffff26000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000526000000000200c700000000ffffffff26000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005260000000002004700000000ffffffff260000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005260000000002004700000000ffffffff260000000002004700000000ffffffff
        s3_pm3_first_capture = 0x1000001002000000403000000000000000000000403000004120000000536000000000201c700000000ffffffff36000000000201c700000000ffffffff000001000001002000000403000000000000000000000403000004120000000536000000000201c700000000ffffffff36000000000201c700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005360000000002014700000000ffffffff360000000002014700000000ffffffff0000010000010020000004020000000000000000000004020000041100000005360000000002014700000000ffffffff360000000002014700000000ffffffff000001000001002000000401000000000000000000000401000004100000000536000000000200c700000000ffffffff36000000000200c700000000ffffffff000001000001002000000401000000000000000000000401000004100000000536000000000200c700000000ffffffff36000000000200c700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005360000000002004700000000ffffffff360000000002004700000000ffffffff0000010000010020000004000000000000000000000004000000040f00000005360000000002004700000000ffffffff360000000002004700000000ffffffff
        s3_pm0_second_capture = 0x400000200203ec2d1ae0000000000000000000004020000000d0000000b0700001f6168d74000000000000000000700001f6168d740000000000000000000000400000200203ec2d1ae0000000000000000000004020000000d0000000b0700001f6168d74000000000000000000700001f6168d740000000000000000000000400000100203ec2d1ac00000000000000000000040000003ae30000000b0600001f6168d64700000000ffffffff0600001f6168d64700000000ffffffff00000400000100203ec2d1ac00000000000000000000040000003ae30000000b0600001f6168d64700000000ffffffff0600001f6168d64700000000ffffffff
        s3_pm1_second_capture = 0x400000200203ec2d1ae0000000000000000000004020000000d0000000b1700001f6168d74000000000000000001700001f6168d740000000000000000000000400000200203ec2d1ae0000000000000000000004020000000d0000000b1700001f6168d74000000000000000001700001f6168d740000000000000000000000400000100203ec2d1ac00000000000000000000040000003ae30000000b1600001f6168d64700000000ffffffff1600001f6168d64700000000ffffffff00000400000100203ec2d1ac00000000000000000000040000003ae30000000b1600001f6168d64700000000ffffffff1600001f6168d64700000000ffffffff
        s3_pm2_second_capture = 0x400000200203ec2d1ae0000000000000000000004020000000d0000000b2700001f6168d74000000000000000002700001f6168d740000000000000000000000400000200203ec2d1ae0000000000000000000004020000000d0000000b2700001f6168d74000000000000000002700001f6168d740000000000000000000000400000100203ec2d1ac00000000000000000000040000003ae30000000b2600001f6168d64700000000ffffffff2600001f6168d64700000000ffffffff00000400000100203ec2d1ac00000000000000000000040000003ae30000000b2600001f6168d64700000000ffffffff2600001f6168d64700000000ffffffff
        s3_pm3_second_capture = 0x400000200203ec2d1ae0000000000000000000004020000000d0000000b3700001f6168d74000000000000000003700001f6168d740000000000000000000000400000200203ec2d1ae0000000000000000000004020000000d0000000b3700001f6168d74000000000000000003700001f6168d740000000000000000000000400000100203ec2d1ac00000000000000000000040000003ae30000000b3600001f6168d64700000000ffffffff3600001f6168d64700000000ffffffff00000400000100203ec2d1ac00000000000000000000040000003ae30000000b3600001f6168d64700000000ffffffff3600001f6168d64700000000ffffffff
        s3_first_capture = {0: s3_pm0_first_capture, 1: s3_pm1_first_capture,
                         2: s3_pm2_first_capture, 3: s3_pm3_first_capture}
        s3_second_capture = {0: s3_pm0_second_capture, 1: s3_pm1_second_capture,
                          2: s3_pm2_second_capture, 3: s3_pm3_second_capture}
        s3_expect_error_stream = {0: s3_first_capture, 1: s3_second_capture}
        s4_pm0_first_capture = 0x1000001002000000403000000000000000000000403000004120000000508000000000201c000000000003fffff08000000000201c000000000003fffff000001000001002000000403000000000000000000000403000004120000000508000000000201c000000000003fffff08000000000201c000000000003fffff0000010000010020000004020000000000000000000004020000041100000005080000000002014000000000003fffff080000000002014000000000003fffff0000010000010020000004020000000000000000000004020000041100000005080000000002014000000000003fffff080000000002014000000000003fffff000001000001002000000401000000000000000000000401000004100000000508000000000200c000000000003fffff08000000000200c000000000003fffff000001000001002000000401000000000000000000000401000004100000000508000000000200c000000000003fffff08000000000200c000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005080000000002004000000000003fffff080000000002004000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005080000000002004000000000003fffff080000000002004000000000003fffff
        s4_pm1_first_capture = 0x1000001002000000403000000000000000000000403000004120000000518000000000201c000000000003fffff18000000000201c000000000003fffff000001000001002000000403000000000000000000000403000004120000000518000000000201c000000000003fffff18000000000201c000000000003fffff0000010000010020000004020000000000000000000004020000041100000005180000000002014000000000003fffff180000000002014000000000003fffff0000010000010020000004020000000000000000000004020000041100000005180000000002014000000000003fffff180000000002014000000000003fffff000001000001002000000401000000000000000000000401000004100000000518000000000200c000000000003fffff18000000000200c000000000003fffff000001000001002000000401000000000000000000000401000004100000000518000000000200c000000000003fffff18000000000200c000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005180000000002004000000000003fffff180000000002004000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005180000000002004000000000003fffff180000000002004000000000003fffff
        s4_pm2_first_capture = 0x1000001002000000403000000000000000000000403000004120000000528000000000201c000000000003fffff28000000000201c000000000003fffff000001000001002000000403000000000000000000000403000004120000000528000000000201c000000000003fffff28000000000201c000000000003fffff0000010000010020000004020000000000000000000004020000041100000005280000000002014000000000003fffff280000000002014000000000003fffff0000010000010020000004020000000000000000000004020000041100000005280000000002014000000000003fffff280000000002014000000000003fffff000001000001002000000401000000000000000000000401000004100000000528000000000200c000000000003fffff28000000000200c000000000003fffff000001000001002000000401000000000000000000000401000004100000000528000000000200c000000000003fffff28000000000200c000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005280000000002004000000000003fffff280000000002004000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005280000000002004000000000003fffff280000000002004000000000003fffff
        s4_pm3_first_capture = 0x1000001002000000403000000000000000000000403000004120000000538000000000201c000000000003fffff38000000000201c000000000003fffff000001000001002000000403000000000000000000000403000004120000000538000000000201c000000000003fffff38000000000201c000000000003fffff0000010000010020000004020000000000000000000004020000041100000005380000000002014000000000003fffff380000000002014000000000003fffff0000010000010020000004020000000000000000000004020000041100000005380000000002014000000000003fffff380000000002014000000000003fffff000001000001002000000401000000000000000000000401000004100000000538000000000200c000000000003fffff38000000000200c000000000003fffff000001000001002000000401000000000000000000000401000004100000000538000000000200c000000000003fffff38000000000200c000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005380000000002004000000000003fffff380000000002004000000000003fffff0000010000010020000004000000000000000000000004000000040f00000005380000000002004000000000003fffff380000000002004000000000003fffff
        s4_pm0_second_capture = 0x400000200203f1621ce0000000000000000000004020000000d0000000b0900001f8b10e74000000000000000000900001f8b10e740000000000000000000000400000200203f1621ce0000000000000000000004020000000d0000000b0900001f8b10e74000000000000000000900001f8b10e740000000000000000000000400000100203f1621cc00000000000000000000040000003ae30000000b0800001f8b10e64000000000003fffff0800001f8b10e64000000000003fffff00000400000100203f1621cc00000000000000000000040000003ae30000000b0800001f8b10e64000000000003fffff0800001f8b10e64000000000003fffff
        s4_pm1_second_capture = 0x400000200203f1621ce0000000000000000000004020000000d0000000b1900001f8b10e74000000000000000001900001f8b10e740000000000000000000000400000200203f1621ce0000000000000000000004020000000d0000000b1900001f8b10e74000000000000000001900001f8b10e740000000000000000000000400000100203f1621cc00000000000000000000040000003ae30000000b1800001f8b10e64000000000003fffff1800001f8b10e64000000000003fffff00000400000100203f1621cc00000000000000000000040000003ae30000000b1800001f8b10e64000000000003fffff1800001f8b10e64000000000003fffff
        s4_pm2_second_capture = 0x400000200203f1621ce0000000000000000000004020000000d0000000b2900001f8b10e74000000000000000002900001f8b10e740000000000000000000000400000200203f1621ce0000000000000000000004020000000d0000000b2900001f8b10e74000000000000000002900001f8b10e740000000000000000000000400000100203f1621cc00000000000000000000040000003ae30000000b2800001f8b10e64000000000003fffff2800001f8b10e64000000000003fffff00000400000100203f1621cc00000000000000000000040000003ae30000000b2800001f8b10e64000000000003fffff2800001f8b10e64000000000003fffff
        s4_pm3_second_capture = 0x400000200203f1621ce0000000000000000000004020000000d0000000b3900001f8b10e74000000000000000003900001f8b10e740000000000000000000000400000200203f1621ce0000000000000000000004020000000d0000000b3900001f8b10e74000000000000000003900001f8b10e740000000000000000000000400000100203f1621cc00000000000000000000040000003ae30000000b3800001f8b10e64000000000003fffff3800001f8b10e64000000000003fffff00000400000100203f1621cc00000000000000000000040000003ae30000000b3800001f8b10e64000000000003fffff3800001f8b10e64000000000003fffff
        s4_first_capture = {0: s4_pm0_first_capture, 1: s4_pm1_first_capture,
                         2: s4_pm2_first_capture, 3: s4_pm3_first_capture}
        s4_second_capture = {0: s4_pm0_second_capture, 1: s4_pm1_second_capture,
                          2: s4_pm2_second_capture, 3: s4_pm3_second_capture}
        s4_expect_error_stream = {0: s4_first_capture, 1: s4_second_capture}
        expect_error_stream = [s0_expect_error_stream, s1_expect_error_stream, s2_expect_error_stream,
                               s3_expect_error_stream, s4_expect_error_stream]
        return expect_error_stream

class DiagnosticScript(HbiccTest):
    """Manually run tests used in lab scenarios

    These tests are named so that they won't be discovered for regression,
    but can be run when specified by name.
    """
    def LongRunningTogglePatternTest(self):
        ''' the purpose of this test to enable some scope measurements used for different activity.
        To be revisited upon the development of other test in this category'''
        pattern_helper = PatternHelper(self.env)
        pattern_helper.is_simulator_active = False
        pattern_helper.is_capture_active = False
        pattern_helper.user_mode = 'LOW'
        pattern_helper.create_slice_channel_set_pattern_combo(slices=[0])
        pattern_helper.set_pin_fixed_drive_state(psdb_target=0)
        pattern_helper.set_pin_fixed_drive_state(psdb_target=1)
        hbicc = self.env.hbicc
        pattern_string = f'''

        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,							data=0b11111111111111111111111111111111111

        I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001 
        %repeat 10240
        V ctv=0, mtv=0, lrpt=0, data=0v11111111111111111111111111111111111
        V ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000
        %end

        I optype=ALU, aluop=ADD, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=2, imm=0x00000001 
        I optype=BRANCH, br=GOTO_I, base=GLOBAL, imm=0x00000004
        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
        '''
        pattern_helper.user_mode = 'RELEASE'
        self.env.hbicc.CRITAL_FIELDS=[]
        # pattern_helper.is_simulator_active= False
        pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0])
        pattern_helper.execute_pattern_scenario()