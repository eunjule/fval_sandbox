# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Fail Capture is per PM flag to store the current vector pin state and pin error value

Data is captured on all pins, all channel sets within
each PM per slice when any active pin fails.

Each PM sends pin error data to the PagGen and is subsequently store in memory
in one of the four different Pin Multiplier Error data + full error header streams.
"""

import random
import unittest

from Common.fval import skip, expected_failure
from Common.fval.testcasedecorators import process_snapshot
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

PIN_DIRECTION = {'DRIVE_LOW': 0x0000, 'DRIVE_HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE_PATTERN_CNTRL': 0xFFFF}

PADDING = 1024
SLICECOUNT = range(5)


class Functional(HbiccTest):
    """Basic pin error capture testing.

    Focuses on basic functionality and obvious test cases. All patterns include four settings: Enable Clock reset,
    all channel sets are active, all unserialized pins hold value in serialized sections and have at least one Call
    for mask set up.

    **Assumptions** All tests have the following unless otherwise specified:

        - Random CTP mask between 0x1 and 0x7FFFFFFFF
        - Random number of slices and at least one
        - Random End status value

    **Criteria: ** All tests within this class should complete with the following:

        - Correct Pattern End Status word
        - No underrun issues
        - No critical alarms
        - Correct fail captures
    """

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.randint(1, 0b11111111111111111111111111111111111)
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT) - 1))
        self.slices.sort()
        self.end_status = hex(random.randint(0, 0xFFFFFFFF))

    def DirectedDriveHighCompareHighNoFailsTest(self):
        """Capture zero fail.

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct pattern string with 1024 CompareHighVectors, which generate no fails
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareHighVectors length=1024
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowCompareLowNoFailsTest(self):
        """Capture zero fails.

        **Test:**

        * Set up test attributes: fix drive state set to LOW
        * Construct pattern string with 1024 CompareLowVectors, which generate no fails
        * Set capture_fails to 1 and ctv_capture to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareLowVectors length=1024
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveLowCompareHighOneFailTest(self):
        """Capture exactly one fail.

        **Test:**

        * Set up test attributes: fix drive state set to LOW
        * Construct pattern string with 1024 CompareLowVectors followed by one vector with CompareHighVectors
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Two 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareLowVectors length=1024
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveHighCompareLowOneFailTest(self):
        """Capture exactly one fail per PM.

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct pattern string with 1024 CompareHighVectors followed by one vector with CompareLowVectors
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Two 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareHighVectors length=1024
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def Directed5kNoFailsPlusTwoFailsAtEndDriveHighCompareLowTest(self):
        """Capture exactly two fail per PM.

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct pattern string of: one CompareLowVector + 1024 CompareHighVectors + one CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Four 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        repeats = 5000
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={repeats}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def Directed5kNoFailsPlusTwoFailsAtEndDriveLowCompareHighTest(self):
        """Capture exactly two fail per PM.

        **Test:**

        * Set up test attributes: fix drive state set to LOW
        * Construct pattern string of: one CompareHighVector + 5,000 CompareLowVectors + one CompareHighVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Four 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        repeats = 5000
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    CompareLowVectors length={repeats}
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomPatternPlusTwoFailsAtStartAndEndDriveHighCompareLowTest(self):
        """Capture exactly two fail per PM.

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct pattern string of: one CompareLowVector + Random number between 1k and 30k CompareHighVectors +
          one CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Four 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        repeats = random.randint(1000, 30000)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                  
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={repeats}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomPatternPlusTwoFailsAtStartAndEndDriveLowCompareHighTest(self):
        """Capture exactly two fail per PM.

        **Test:**

        * Set up test attributes: fix drive state set to LOW
        * Construct pattern string of: one CompareHighVector + Random number between 1k and 30k CompareLowVectors +
          one CompareHighVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Four 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        repeats = random.randint(1000, 30000)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    CompareLowVectors length={repeats} 
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedTenFailsInterleaveDriveLowCompareHighTest(self):
        """Capture exactly two fail per PM.

        **Test:**

        * Set up test attributes: fix drive state set to LOW
        * Construct pattern string with three pairs of the following: one CompareHighVector +
            Random number between 1k and 5k CompareLowVectors
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Six 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'LOW'
        id_0 = random.randint(0, 100)
        random_repeats = [random.randint(1000, 5000) for x in range(11)]
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    CompareLowVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    CompareLowVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                    CompareLowVectors length={random_repeats[random.randint(0, 10)]}                                        
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedUpperFailThresholdInterleaveDriveHighCompareLowTest(self):
        """Capture exactly two fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct pattern string with ten pairs of the following: one CompareLowVector +
            Random number between 1k and 5k CompareHighVectors
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: 20 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        id_0 = random.randint(0, 100)
        random_repeats = [random.randint(1000, 5000) for x in range(11)]
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    CompareHighVectors length={random_repeats[random.randint(0, 10)]}
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomMtvInterleaveDriveHighCompareLowTest(self):
        """Capture exactly two fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct the following pattern string: one CompareLowVector with ctv=0 + one CompareLowVector with ctv=1
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 3 blocks (Call, GoTo_I, Return)
            - Error data + full error header for Pin Multiplier 0-3: Four 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = range(1)
        id_0 = random.randint(0, 100)
        random_repeats = [random.randint(1000, 5000) for x in range(11)]
        self.ctp = 0x7ffffffff
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                 PatternId {id_0}                                                 
                 PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                    V ctv=0, mtv=1, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                Return
                '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.mtv_mask_object.randomize_mask()
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPCallOneFailTest(self):
        """Capture exactly one fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct the following pattern string: Call to a PCall with random value between 1k and 30k
            CompareHighVector and one CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 4 blocks (Call, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Two 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        repeats = random.randint(1000, 30000)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            NoCompareDriveZVectors length=1024
            
            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1
            
            PATTERN0:
                CompareHighVectors length={repeats}
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                Return                                                              

            PATTERN1:
                PatternId {id_1}                                                     
                PCall PATTERN0                                                      
                Return                                                              
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def Directed30kCtvWithPcallTwoFailuresTest(self):
        """Capture exactly two fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct the following pattern string: Call to a PCall with 300 CompareHighVector and two CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 4 blocks (Call, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Four 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        repeats = 300
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1
            
            PATTERN0:
                CompareHighVectors length={repeats}
                CompareLowVectors length=2
                Return                                                              

            PATTERN1:
                PatternId {id_1}                                                     
                PCall PATTERN0                                                      
                Return                                                               
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRandomFailsWith2SamePcallTest(self):
        """Capture 1-6 fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct the following pattern string: Call to a PCall with 1k-3k CompareHighVector and 1-6 CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 6 blocks (Call, PCall, Return, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: CompareLowVectors*2 512-blocks +
                End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'HIGH'
        repeats = 1024 * random.randint(1, 3)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        id_2 = random.randint(200, 300)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} 

            PatternId {id_0}
            PCall PATTERN1
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1024
            
            PATTERN0:
                CompareHighVectors length={repeats}
                CompareLowVectors length={random.randint(1, 6)}
                Return                                                            

            PATTERN1:
                PatternId {id_1}                                                    
                PCall PATTERN0
                PatternId {id_2}                                                    
                PCall PATTERN0                                                      
                Return                                                              
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCaptureLimitOfOnePerPCallTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                    PatternId 0x100
                    PCall P1
                    PatternId 0x200
                    PCall P1
                    PatternId 0x500
                    PCall P1
                    PatternId 0x400
                    PCall P1
                    Return

                P1:
                    CompareLowVectors length=1
                    CompareHighVectors length=1000
                    CompareLowVectors length=1
                    CompareHighVectors length=1000
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 1,
                                                                               'per_pattern_max_fail': 1})
        self.pattern_helper.execute_pattern_scenario()

    def RandomCaptureLimitPerPCallTest(self):
        """Tests fail capture limit up to 10 fails per PCall

        **Test:**

            - Set Capture limit to random value between 1-10 (maximum fail count per pattern is 10)
            - Construct each PCall as follows: one fail, 50 non failing pairs times 12
            - Adjust simulator to only capture up the maximum number.

        **Criteria:**  Captured data shows correct random of failures per PCall, potentially 10 in this case.

        """
        fixed_drive_state = 'HIGH'
        random_max_fail_count = random.randint(1, 10)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                    PatternId 0x100
                    PCall P1
                    PatternId 0x200
                    PCall P1
                    PatternId 0x500
                    PCall P1
                    PatternId 0x400
                    PCall P1
                    Return

                P1:
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50
                    CompareLowVectors length=1
                    CompareHighVectors length=50                                                            
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 1,
                                                                               'per_pattern_max_fail': random_max_fail_count})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPerPatternMaxFailTest(self):
        '''Tests Maximum Per Pattern Fail Count

        Each PCall has a 50 fails so only 10 of them will be captured for the first PCall and none for the second PCall.
        The global max fail count is continuously counting since burst inception. Therefore the the tally is beyond 20
        by the time the second PCall gets executed, hence not capturing any fails.

        **Criteria: ** Capture 10 fails per PM
        '''
        self.pattern_helper.user_mode = 'HIGH'
        per_pattern_max_fail = 10
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId 0x100
                PCall P1
                
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId 0x200
                PCall P1
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                P1:
                    CompareLowVectors length=50
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 1,
                                                                               'per_pattern_max_fail': per_pattern_max_fail,
                                                                               'global_max_fail': 20})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedGlobalMaxFailCountAcross2PCallsTest(self):
        '''Tests Maximum Global Pattern Fail Count

        Each PCall has a 30 failing vectors but a maximum per pattern fail count of 10. This means that only 10 of
        them will be captured for the first PCall and 10 for the second PCall.
        The global max fail count is continuously counting since burst inception. Therefore the the tally is only 30
        by the time the second PCall gets executed, hence capturing 10 fails on the second PCall.

        **Criteria: ** Capture 20 fails per PM
        '''
        self.slices = [0]
        self.pattern_helper.user_mode = 'HIGH'
        per_pattern_max_fail = 10
        vectors_in_pcall = 30
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId 0x100
                PCall P1

                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId 0x200
                PCall P2
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                P1:
                    CompareLowVectors length={vectors_in_pcall}
                    Return
                    
                P2:
                    CompareLowVectors length={vectors_in_pcall}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 1,
                                                                               'per_pattern_max_fail': per_pattern_max_fail,
                                                                               'global_max_fail': per_pattern_max_fail + vectors_in_pcall})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedGlobalMaxFailCountLessThanPerPatternMaxFailOnePCallsTest(self):
        '''Tests Maximum Global Pattern Fail Count when Less then Max Per Pattern Count

        Maximum global fail count is set lower than the per pattern maximum fail count, therefore only 10 fails are
        expected.

        **Criteria: ** Capture 10 fails per PM
        '''
        self.pattern_helper.user_mode = 'HIGH'
        per_pattern_max_fail = 15
        failed_vectors_in_pcall = 10
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId 0x100
                PCall P1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                P1:
                    CompareLowVectors length={failed_vectors_in_pcall}
                    CompareHighVectors length=30
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string,
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 1,
                                                                               'per_pattern_max_fail': per_pattern_max_fail,
                                                                               'global_max_fail': failed_vectors_in_pcall - 2})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPerPatternMaxFailDoesNotAbortTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=1024, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        # CompareLowVectors length=8, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_fails': 1,
                                                                                   'per_pattern_max_fail': 2,
                                                                                   'capture_ctv_after_max_fail': 1})
        self.pattern_helper.execute_pattern_scenario()


    # @skip('Not implemented')
    def RandomMaxCaptureCountTest(self):
        """a version of DirectedRandomFailsWith2SamePcallTest"""
        """Capture 1-6 fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct the following pattern string: Call to a PCall with 1k-3k CompareHighVector and 1-6 CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 6 blocks (Call, PCall, Return, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: CompareLowVectors*2 512-blocks +
                End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'LOW'
        repeats = 1024 * random.randint(1, 3)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        id_2 = random.randint(200, 300)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} 

            PatternId {id_0}
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1024

            PATTERN0:
                CompareLowVectors length={repeats}
                CompareHighVectors length={random.randint(1, 6)}
                Return                                                            

            PATTERN1:
                PatternId {id_1}                                                    
                PCall PATTERN0
                PatternId {id_2}                                                    
                PCall PATTERN0                                                      
                Return                                                              
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()
        # pass

    # @skip('Not implemented')
    def RandomScoreboardTest(self):
        """might be related to DirectedPCallOneFailTest"""
        """Capture exactly one fail per PM and No CTV data

        **Test:**

        * Set up test attributes: fix drive state set to HIGH
        * Construct the following pattern string: Call to a PCall with random value between 1k and 30k
            CompareHighVector and one CompareLowVector
        * Set capture_fails to 1 and capture_ctv to 0
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 blocks
            - CTV Data Objects: 0 blocks
            - Jump/Call trace: 4 blocks (Call, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Two 512-blocks + End of Burst block per PM

        """
        self.pattern_helper.user_mode = 'LOW'
        repeats = random.randint(1000, 30000)
        id_0 = random.randint(0, 100)
        id_1 = random.randint(100, 200)
        pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            NoCompareDriveZVectors length=1024

            PatternId {id_0}
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            NoCompareDriveZVectors length=1

            PATTERN0:
                CompareLowVectors length={repeats}
                V ctv=0, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
                Return                                                              

            PATTERN1:
                PatternId {id_1}                                                     
                PCall PATTERN0                                                      
                Return                                                              
            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()
        # pass

    def DirectedFirstFailAddressTest(self):
        """Single failure on one pin with Capture All Enable

        **Test:**

        * Construct pattern string containing one PCall having:
            - Random number of Compare Low vectors, ctv = 0
            - Single Compare Low Vector, ctv = 1
            - Single fail on pin 35 Compare Low Vector, ctv = 1

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Depends on random number of vectors
            - CTV Data Objects: Depends on CTP and random number of vectors. One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: Per pattern max fail count of 10 + End of Burst blocks

        """
        self.slices = [0, 1, 2, 3]
        self.pattern_helper.user_mode = 'LOW'
        self.ctp |= 0x400000000

        attributes = {'capture_fails': 0,
                      'capture_ctv': 0,
                      'capture_all': 1}
        repeat = random.randint(200, 1000)

        pattern_string =  f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                    S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

                    S stype=CAPTURE_PIN_MASK,       data={self.ctp}
                    PatternId 0x100
                    PCall L100

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    L100:
                        CompareLowVectors length={repeat}
                        V ctv=1, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                        V ctv=1, mtv=0, lrpt=0, data=0vHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                        Return

                    '''

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    # @process_snapshot
    def DirectedCTVBitCaptureAllTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_all': 1,
                                                                                   'capture_fails': 0,
                                                                                   'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()


    # @process_snapshot
    def DirectedCTVBitCaptureFailTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_all': 1,
                                                                                   'capture_fails': 1,
                                                                                   'capture_ctv': 0})
        self.pattern_helper.execute_pattern_scenario()

    # @process_snapshot
    def DirectedCTVBitCaptureAllPlusFailTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_all': 1,
                                                                                   'capture_fails': 1,
                                                                                   'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCTVBitCaptureAllOnlyTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_all': 1,
                                                                                   'capture_fails': 0,
                                                                                   'capture_ctv': 0})
        self.pattern_helper.execute_pattern_scenario()


    def DirectedCTVAfterPerPatternMaxFailTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_fails': 1,
                                                                                   'per_pattern_max_fail': 2})
        self.pattern_helper.execute_pattern_scenario()


    # @process_snapshot
    def DirectedCTVAfterMaxCaptureFailTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_fails': 1,
                                                                                   'global_max_fail': 5})
        self.pattern_helper.execute_pattern_scenario()

        # self.wait_for_complete = True
        # self.max_fail_count_per_pattern = ONE_MB // SINGLE_BLOCK_SIZE
        # self.max_fail_capture_count = ONE_MB // SINGLE_BLOCK_SIZE
        # self.max_capture_count = ONE_MB // SINGLE_BLOCK_SIZE
        # self.capture_length = ONE_MB
        # self.global_max_fail = 50
        # self.per_pattern_max_fail = 10
        #
        # self.capture_type = 1
        # self.capture_fails = 0
        # self.capture_ctv = 1
        # self.capture_all = 0
        # self.stop_on_max_fail = 0
        # self.capture_ctv_after_max_fail = 0

    # @process_snapshot
    def DirectedCTVAfterMaxCaptureFailNoAbortTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1

                    # PATTERN0:
                    #     PatternId 0x100
                    #     PCall P1
                        # PatternId 0x200
                        # PCall P1
                        # PatternId 0x500
                        # PCall P1
                        # PatternId 0x400
                        # PCall P1
                        # Return

                    P1:
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        CompareLowVectors length=8, ctv=1
                        CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_fails': 1,
                                                                                   'global_max_fail': 5,
                                                                                   'capture_ctv_after_max_fail': 1})
        self.pattern_helper.execute_pattern_scenario()


    def DirectedCTVCaptureMaskedAllFailsTest(self):
        """Mask all fails for random CTV Pattern

        **Test:**

        * Construct pattern string containing two PCalls, each having:
            - Randomize ctp mask
            - Mask out all pins by setting data to 0x7FFFFFFFF

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 256 Blocks
            - CTV Data Objects: Dependent on ctp randomization + One End of Burst block per channel set
                                All blocks will have data equaling 0 due to the masking
                                and the user mode set to LOW
            - Jump/Call trace: 2 Pcall block,  2 Returns block
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        self.slices = [0,1,2,3]
        ctp_0 = random.getrandbits(35)
        ctp_1 = random.getrandbits(35)
        self.pattern_helper.user_mode = 'LOW'

        pattern_string = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
        S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

        S stype=CAPTURE_PIN_MASK,       data={ctp_0}
        S stype=MASK,                   data=0x7FFFFFFFF
        PatternId 0x100
        PCall L100

        S stype=CAPTURE_PIN_MASK,       data={ctp_1}
        S stype=MASK,                   data=0x7FFFFFFFF
        PatternId 0x300
        PCall L100

        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
        DriveZeroVectors length=1

        L100:
            RandomCompareHighAndLow length=256, ctv=1
            Return

        '''

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    # @skip('Not implemented')
    def RandomPlistCTVTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                    PatternId 0x110
                    PCall P2

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1


                    P1:
                        CompareLowVectors length=1024, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        # CompareLowVectors length=8, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        Return

                    P2:
                        CompareLowVectors length=1024, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        # CompareLowVectors length=8, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_fails': 1,
                                                                                   'per_pattern_max_fail': 2,
                                                                                   'capture_ctv_after_max_fail': 1})
        self.pattern_helper.execute_pattern_scenario()
        # pass

    # @skip('Not implemented')
    def RandomPlistCTVPlusTest(self):
        """Tests fail capture limit of 1

        **Test:**

            - Set Capture limit to 1
            - Construct each PCall as follows: one fail, 1000 non failing, one fail, 1000 non failing vectors.
            - Adjust simulator to only capture one fail

        **Criteria:**  Captured data shows one failure per PCall, 10 in this case.

        """

        self.slices = [0, 1, 2, 3]
        fixed_drive_state = 'HIGH'
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    # Call PATTERN0
                    PatternId 0x100
                    PCall P1

                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                    PatternId 0x110
                    PCall P2

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                    DriveZeroVectors length=1


                    P1:
                        CompareLowVectors length=1024, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        # CompareLowVectors length=8, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        PatternId 0x111
                        PCall P2
                        Return

                    P2:
                        CompareLowVectors length=1024, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        # CompareLowVectors length=8, ctv=1
                        # CompareHighVectors length=1024, ctv=1
                        Return
                    '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state,
                                                                       slices=self.slices,
                                                                       attributes={'capture_fails': 1,
                                                                                   'per_pattern_max_fail': 2,
                                                                                   'capture_ctv_after_max_fail': 1})
        self.pattern_helper.execute_pattern_scenario()
        # pass

    def DirectedCaptureWindowFailCaptureTest(self):
        """Enable/Disable Capture during vector processing with Capture Fails Active

          **Test:**

          * Construct pattern string containing one PCall having:
              - Disable capture followed by 500 CTV vectors
              - Enable capture followed by one passing ctv vector and one failing non-ctv vector
              - Disable capture followed by 500 CTV vectors

          * Set up tester for pattern execution (see PatternHelper class for more info)
          * Execute pattern
          * Process data capture results

          **Criteria:** The expected result for each data stream per slice is as follows:

              - Full CTV Headers: None
              - CTV Data Objects: End of Burst block per channel set
              - Jump/Call trace: 1 Pcall block,  1 Return block
              - Error data + full error header for Pin Multiplier 0-3: one + End of Burst blocks

          """
        self.pattern_helper.user_mode = 'HIGH'

        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}

        pattern_string = self.single_capture_capture_window_pattern_string()

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    @expected_failure('USER STORY 149162: if it runs on slice 4')
    def DirectedCaptureWindowCTVCaptureTest(self):
        """Enable/Disable Capture during vector processing with Capture CTV Active

          **Test:**

          * Construct pattern string containing one PCall having:
              - Disable capture followed by 500 CTV vectors
              - Enable capture followed by one passing ctv vector and one failing non-ctv vector
              - Disable capture followed by 500 CTV vectors

          * Set up tester for pattern execution (see PatternHelper class for more info)
          * Execute pattern
          * Process data capture results

          **Criteria:** The expected result for each data stream per slice is as follows:

              - Full CTV Headers: 1 Blocks
              - CTV Data Objects: End of Burst block per channel set
              - Jump/Call trace: 1 Pcall block,  1 Return block
              - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

          """
        self.pattern_helper.user_mode = 'HIGH'
        attributes = {'capture_fails': 0,
                      'capture_ctv': 1,
                      'capture_all': 0}

        pattern_string = self.single_capture_capture_window_pattern_string()

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCaptureWindowAllCaptureTest(self):
        """Enable/Disable Capture during vector processing with Capture All Active

          **Test:**

          * Construct pattern string containing one PCall having:
              - Disable capture followed by 500 CTV vectors
              - Enable capture followed by one passing ctv vector and one failing non-ctv vector
              - Disable capture followed by 500 CTV vectors

          * Set up tester for pattern execution (see PatternHelper class for more info)
          * Execute pattern
          * Process data capture results

          **Criteria:** The expected result for each data stream per slice is as follows:

              - Full CTV Headers: 1 Blocks
              - CTV Data Objects: End of Burst block per channel set
              - Jump/Call trace: 1 Pcall block,  1 Return block
              - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

          """
        self.pattern_helper.user_mode = 'HIGH'
        attributes = {'capture_fails': 0,
                      'capture_ctv': 0,
                      'capture_all': 1}
        self.slices = [0, 1, 2, 3]
        pattern_string = self.single_capture_capture_window_pattern_string()

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCaptureWindowMultipatternAllCaptureTest(self):
        """Enable/Disable Capture during vector processing with Capture Fails Active

        **Test:**

        * Construct pattern string containing one PCall having:
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of CTV vectors
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of vectors
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of CTV vectors
            - Disable capture followed by a fix number of CTV vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 Blocks
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: Per pattern max fail count of 10 + End of Burst blocks

        """
        self.slices = [0,1,2,3]
        self.pattern_helper.user_mode = 'HIGH'

        attributes = {'capture_fails': 0,
                      'capture_ctv': 0,
                      'capture_all': 1}

        pattern_string = self.capture_window_pattern_string()

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCaptureWindowMultipatternFailCaptureTest(self):
        """Enable/Disable Capture during vector processing with Capture Fails Active

        **Test:**

        * Construct pattern string containing one PCall having:
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of CTV vectors
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of vectors
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of CTV vectors
            - Disable capture followed by a fix number of CTV vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 0 Blocks
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: Per pattern max fail count of 10 + End of Burst blocks

        """
        self.slices = [0,1,2,3]
        self.pattern_helper.user_mode = 'HIGH'

        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}

        pattern_string = self.capture_window_pattern_string()

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedCaptureWindowMultipatternCTVCaptureTest(self):
        """Enable/Disable Capture during vector processing with CTV Capture Active

        **Test:**

        * Construct pattern string containing one PCall having:
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of CTV vectors
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of vectors
            - Disable capture followed by a fix number of CTV vectors
            - Enable capture followed by a fix number of CTV vectors
            - Disable capture followed by a fix number of CTV vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 768 Blocks
            - CTV Data Objects: Depends on CTP. One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: Per pattern max fail count of 10 + End of Burst blocks

        """
        self.slices = [0,1,2,3]
        self.pattern_helper.user_mode = 'HIGH'

        attributes = {'capture_fails': 0,
                      'capture_ctv': 1,
                      'capture_all': 0}

        pattern_string = self.capture_window_pattern_string()

        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, slices=self.slices, attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveOneCompareHighEvenToOddExternalLoopbackTest(self):
        """Capture Fail with External Loopback - Drive High Compare High

            **Test:**

        * Construct pattern string containing one PCall having:
            - Drive Z vectors
            - Constant vectors where one pin drives and its pair pin compares
                - Drive 1, Compare High. No failures expected.
            - Drive Z vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks
        """
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        repeat_iteration = random.randint(1, 1000)

        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}

        pattern_string = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

            S stype=CAPTURE_PIN_MASK, data={self.ctp}    
            PatternId {id_0}
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            DriveZeroVectors length=1
    
            PATTERN1:
                NoCompareDriveZVectors length=100
                ConstantVectors length={repeat_iteration}, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                NoCompareDriveZVectors length=1000

                Return
          '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0], attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveOneCompareLowOddToEvenExternalLoopbackTest(self):
        """Capture Fail with External Loopback - Drive High Compare Low

        **Test:**

        * Construct pattern string containing one PCall having:
            - Drive Z vectors
            - Constant vectors where one pin drives and its pair pin compares
                - Drive 1, Compare Low. Failures expected.
            - Drive Z vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: Per pattern max fail count of 10 + End of Burst blocks
        """
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        repeat_iteration = random.randint(1, 1000)
        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}

        pattern_string = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            
            S stype=CAPTURE_PIN_MASK, data={self.ctp} #ctp mask
            PatternId {id_0}
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
            DriveZeroVectors length=1

            PATTERN1:
                NoCompareDriveZVectors length=100
                ConstantVectors length={repeat_iteration}, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
                NoCompareDriveZVectors length=100

                Return
            '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0], attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveZeroCompareHighwOddToEvenExternalLoopbackTest(self):
        """Capture Fail with External Loopback - Drive Low Compare High

        **Test:**

        * Construct pattern string containing one PCall having:
            - Drive Z vectors
            - Constant vectors where one pin drives and its pair pin compares
                - Drive 0, Compare High. Failures expected.
            - Drive Z vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: Per pattern max fail count of 10 + End of Burst blocks
        """
        self.pattern_helper.user_mode = 'RELEASE'
        id_0 = random.randint(0, 100)
        repeat_iteration = random.randint(1, 1000)
        attributes = {'capture_fails': 1,
                      'capture_ctv': 0,
                      'capture_all': 0}
        pattern_string = f'''

                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

                    S stype=CAPTURE_PIN_MASK, data={self.ctp} #ctp mask
                    PatternId {id_0}
                    PCall PATTERN1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                    DriveZeroVectors length=1

                    PATTERN1:
                        NoCompareDriveZVectors length=100
                        ConstantVectors length={repeat_iteration}, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                        NoCompareDriveZVectors length=100
    
                        Return
                      '''

        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0], attributes=attributes)
        self.pattern_helper.execute_pattern_scenario()

    @skip(f'Not Developed. Waiting on FPGA Feature Implementation')
    def DirectedCaptureLengthStopTest(self):
        pass

    @skip(f'Not Developed. Waiting on FPGA Feature Implementation')
    def DirectedCaptureLengthWrapTest(self):
        pass

    @skip(f'Not Developed. Waiting on FPGA Feature Implementation')
    def DirectedCapturePinEnableTest(self):
        pass
    
    def capture_window_pattern_string(self):
        repeat = 32
        return f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

                S stype=CAPTURE_PIN_MASK,       data={self.ctp}
                PatternId 0x100
                PCall L100

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                L100:
                    CaptureWindowDisable
                    CompareLowVectors length={repeat}, ctv=1

                    CaptureWindowEnable
                    CompareLowVectors length={repeat}, ctv=1

                    CaptureWindowDisable
                    CompareLowVectors length={repeat}, ctv=1

                    CaptureWindowEnable
                    CompareLowVectors length={repeat}, ctv=0

                    CaptureWindowDisable
                    CompareLowVectors length={repeat}, ctv=1

                    CaptureWindowEnable
                    CompareLowVectors length={repeat}, ctv=1

                    CaptureWindowDisable
                    CompareLowVectors length={repeat}, ctv=1

                    Return

                '''

    def single_capture_capture_window_pattern_string(self):
        return f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

                S stype=CAPTURE_PIN_MASK,       data={self.ctp}
                PatternId 0x100
                PCall L100

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                L100:
                    CaptureWindowDisable
                    CompareLowVectors length=500, ctv=1

                    CaptureWindowEnable
                    CompareLowVectors length=1, ctv=1
                    CompareHighVectors length=1, ctv=0

                    CaptureWindowDisable
                    CompareLowVectors length=500, ctv=1

                    Return

                '''
