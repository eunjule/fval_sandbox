# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Multi-Slice Group validation aims at running groups of slices with each group 
with different periods.

Each group must run at a speed that is a power of 2 of the base speed.
"""
import random

from Common.fval import skip, skip_snapshot, deactivate_simulator, process_snapshot
from Hbicc.Tests.HbiccTest import HbiccTest, Si5344Clock
from Hbicc.testbench.PatternUtility import PatternHelper


PADDING = 1024
SLICECOUNT = range(0, 5)


class Functional(HbiccTest):
    """Validation of Multi-slice Group Feature with Capture Active
    
    For small patterns tests, execute all combination of capture setting (fail, CTV and pinstate).
    For medium and long patterns tests, randomize the capture setting (fail, CTV and pinstate).
    
    **Criteria**:
        - Slice to slice sync
        - No patgen errors
        - No underruns
        - Vector execution of faster slices ending earlier than others
        - For long patterns:
            - Correct H, L, K counts
            - Correct capture stream counts
            - Correct E32 Counts
    """
    
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = random.randint(0, 0xFFFFFFFF)
        population = set(SLICECOUNT)
        self.ctp = random.randint(1, 0b11111111111111111111111111111111111)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT) - 1))
        self.slices.sort()
        self.pattern_helper.user_mode = 'LOW'

    def DirectedSmallPatternTwoSlicesOneAt200MhzOneModulateSubcycleTest(self):
        """Slice at 200MHz base speed vs slice at slower speeds
        
        1. Write simple pattern between 1024-2048 vectors
        2. Set up tester for pattern execution (see PatternHelper class for more info)
            - Set up one slice group with exactly one slice and set execution speed to base speed of 200 MHz.
            - Set up second slice group with exactly one slice and set execution 
                speed to based speed times 1/2^N (define in step 5)
        3. Execute pattern
        4. Process data capture results
        5. Repeat steps 1-4 for the following N values: 1, 2, 3
        
        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024-2048 Blocks, depending on randomization 
            - CTV Data Objects: Dependent on ctp randomization + random vectors 
                                + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall,  1 Returns
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks
        """
        repeat_1 = random.randint(1024, 2048)
        repeat_2 = random.randint(1024, 2048)
        for subcycle_exponent in [1, 2, 3]:
            with self.subTest(Subcycle_Exponent=f'{subcycle_exponent}'):
                pattern_string1 = f'''
                       PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                       S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
        
                       PatternId 0x100
                       PCall PATTERN1
        
                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                       NoCompareDriveZVectors length=1
        
                       PATTERN1:
                            AddressVectorsPlusAtt length={repeat_1}
                            Return
                       '''
                pattern_string2 = f'''
                       PATTERN_START:
                       S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                       S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
        
                       PatternId 0x200
                       PCall PATTERN1
                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                       NoCompareDriveZVectors length=1
        
                       PATTERN1:
                           AddressVectorsPlusAtt length={repeat_2}
                           Return
                       '''

                group_0_slices = [0]
                self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                           address_and_pattern_list={0: pattern_string1})

                group_1_slices = [1]
                self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                           address_and_pattern_list={
                                                                               0x19eb28300: pattern_string2})
                self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
                self.pattern_helper.set_pm_subcycle(subcycle=subcycle_exponent, slices=group_1_slices)
                self.pattern_helper.execute_pattern_scenario()
                self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

    def DirectedSmallPatternTwoSlicesOneAt101MhzOneModulatesSubcycleTest(self):
        """Slice at 101MHz base speed vs slice at slower speeds

        1. Write simple pattern between 1024-2048 vectors
        2. Set up tester for pattern execution (see PatternHelper class for more info)
            - Set up one slice group with exactly one slice and set execution speed to base speed of 101 MHz.
            - Set up second slice group with exactly one slice and set execution 
                speed to based speed times 1/2^N (define in step 5)
        3. Execute pattern
        4. Process data capture results
        5. Repeat steps 1-4 for the following N values: 1, 2, 3

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024-2048 Blocks, depending on randomization 
            - CTV Data Objects: Dependent on ctp randomization + random vectors 
                                + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall,  1 Returns
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks
        """
        repeat_1 = random.randint(1024, 2048)
        repeat_2 = random.randint(1024, 2048)
        with Si5344Clock(self.env.hbicc.pat_gen, frequency_mhz=101.0):
            for subcycle_exponent in [1, 2, 3]:
                with self.subTest(Subcycle_Exponent=f'{subcycle_exponent}'):
                    pattern_string1 = f'''
                           PATTERN_START:
                           S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                           S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                           S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                           S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
    
                           PatternId 0x100
                           PCall PATTERN1
    
                           I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                           NoCompareDriveZVectors length=1
    
                           PATTERN1:
                                AddressVectorsPlusAtt length={repeat_1}
                                Return
                           '''
                    pattern_string2 = f'''
                           PATTERN_START:
                           S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                           S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                           S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                           S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
    
                           PatternId 0x200
                           PCall PATTERN1
                           I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                           NoCompareDriveZVectors length=1
    
                           PATTERN1:
                               AddressVectorsPlusAtt length={repeat_2}
                               Return
                           '''

                    group_0_slices = [0]
                    self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                               address_and_pattern_list={
                                                                                   0: pattern_string1})

                    group_1_slices = [1]
                    self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                               address_and_pattern_list={
                                                                                   0x19eb28300: pattern_string2})
                    self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
                    self.pattern_helper.set_pm_subcycle(subcycle=subcycle_exponent, slices=group_1_slices)
                    self.pattern_helper.execute_pattern_scenario()
                    self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

    def DirectedSmallPatternFourSlicesOneAt200MhzOthersModulatesSubcycleTest(self):
        """Slice at 200MHz base speed vs other slices at slower speeds

        1. Write simple pattern between 1024-2048 vectors
        2. Set up tester for pattern execution (see PatternHelper class for more info)
            - Set up one slice group with exactly one slice and set execution speed to base speed of 200 MHz.
            - Set up second slice group with exactly one slice and set execution 
                speed to based speed times 1/2^1
            - Set up third slice group with exactly one slice and set execution 
                speed to based speed times 1/2^2
            - Set up fourth slice group with exactly one slice and set execution 
                speed to based speed times 1/2^3
            - Set up fourth slice group with exactly one slice and set execution 
                speed to based speed times 1/2^4
        3. Execute pattern
        4. Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024-2048 Blocks, depending on randomization 
            - CTV Data Objects: Dependent on ctp randomization + random vectors 
                                + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall,  1 Returns
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks
        """
        pattern_string1 = f'''
               PATTERN_START:
               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
               S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

               PatternId 0x100
               PCall PATTERN1

               I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
               NoCompareDriveZVectors length=1

               PATTERN1:
                    AddressVectorsPlusAtt length={random.randint(1024, 2048)}
                    Return
               '''

        group_0_slices = [0]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})

        group_1_slices = [1]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})
        group_2_slices = [2]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=2, slices=group_2_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})
        group_3_slices = [3]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=3, slices=group_3_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})
        group_4_slices = [4]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=4, slices=group_4_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})

        self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=1, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=2, slices=group_2_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=3, slices=group_3_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=4, slices=group_4_slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedSmallPatternFourSlicesOneAt101MhzOthersModulatesSubcycleTest(self):
        """Slice at 101MHz base speed vs other slices at slower speeds

        1. Write simple pattern between 1024-2048 vectors
        2. Set up tester for pattern execution (see PatternHelper class for more info)
            - Set up one slice group with exactly one slice and set execution speed to base speed of 101 MHz.
            - Set up second slice group with exactly one slice and set execution 
                speed to based speed times 1/2^1
            - Set up third slice group with exactly one slice and set execution 
                speed to based speed times 1/2^2
            - Set up fourth slice group with exactly one slice and set execution 
                speed to based speed times 1/2^3
            - Set up fourth slice group with exactly one slice and set execution 
                speed to based speed times 1/2^4
        3. Execute pattern
        4. Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024-2048 Blocks, depending on randomization 
            - CTV Data Objects: Dependent on ctp randomization + random vectors 
                                + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall,  1 Returns
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks
        """
        with Si5344Clock(self.env.hbicc.pat_gen, frequency_mhz=101.0):
            pattern_string1 = f'''
                   PATTERN_START:
                   S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                   S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                   S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                   S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
    
                   PatternId 0x100
                   PCall PATTERN1
    
                   I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                   NoCompareDriveZVectors length=1
    
                   PATTERN1:
                        AddressVectorsPlusAtt length={random.randint(1024, 2048)}
                        Return
                   '''

            group_0_slices = [0]
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                       address_and_pattern_list={
                                                                           0: pattern_string1})

            group_1_slices = [1]
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                       address_and_pattern_list={
                                                                           0: pattern_string1})
            group_2_slices = [2]
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=2, slices=group_2_slices,
                                                                       address_and_pattern_list={
                                                                           0: pattern_string1})
            group_3_slices = [3]
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=3, slices=group_3_slices,
                                                                       address_and_pattern_list={
                                                                           0: pattern_string1})
            group_4_slices = [4]
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=4, slices=group_4_slices,
                                                                       address_and_pattern_list={
                                                                           0: pattern_string1})

            self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
            self.pattern_helper.set_pm_subcycle(subcycle=1, slices=group_1_slices)
            self.pattern_helper.set_pm_subcycle(subcycle=2, slices=group_2_slices)
            self.pattern_helper.set_pm_subcycle(subcycle=3, slices=group_3_slices)
            self.pattern_helper.set_pm_subcycle(subcycle=4, slices=group_4_slices)
            self.pattern_helper.execute_pattern_scenario()

    def DirectedSmallPattern200MHzVs100MhzGroupsTest(self):
        """Slice 0, 1, and 4 at 200MHz base speed vs Slice 2 and 3 at 100 MHz

        1. Write simple pattern between 1024-2048 vectors
        2. Set up tester for pattern execution (see PatternHelper class for more info)
            - Set up first slice group with slice 0, 1 and 4. Set execution speed to base speed of 200 MHz.
            - Set up second slice group with slice 2 and 3. Set execution 
                speed to 100 MHz
        3. Execute pattern
        4. Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1024-2048 Blocks, depending on randomization 
            - CTV Data Objects: Dependent on ctp randomization + random vectors 
                                + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall,  1 Returns
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks
        """
        pattern_string1 = f'''
               PATTERN_START:
               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
               S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

               PatternId 0x100
               PCall PATTERN1

               I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
               NoCompareDriveZVectors length=1

               PATTERN1:
                    AddressVectorsPlusAtt length={random.randint(1024, 2048)}
                    Return
               '''
        pattern_string2 = f'''
               PATTERN_START:
               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
               S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

               PatternId 0x200
               PCall PATTERN1
               I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
               NoCompareDriveZVectors length=1

               PATTERN1:
                   AddressVectorsPlusAtt length={random.randint(1024, 2048)}
                   Return
               '''

        group_0_slices = [0, 1, 4]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})

        group_1_slices = [2, 3]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                   address_and_pattern_list={
                                                                       0x19eb28300: pattern_string2})
        self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=1, slices=group_1_slices)
        self.pattern_helper.execute_pattern_scenario()

    @skip(f'Undeveloped')
    def DirectedTwoGroupsKeepAliveCaptureUponExitTest(self):
        """Two Groups in Keep Alive Loop with Inter-group Loopback Capture

        1. Make two groups, one with slice 0 and second one with slice 1
        2. Set group 0 to base speed 200 MHz with subcycle 0
        3. Set group 1 to base speed 200 MHz with subcycle 1
        4. Construct patterns for different groups
            - Pad faster group with don't care vector to make both groups
                enter Keep Alive at the same time
            - Enter Keep alive loop
            - Using SW trigger, end KA
            - Make first vector following KA a CTV vector
            - Pad with non-capture vectors
        5. Set up tester for pattern execution (see PatternHelper class for more info)
        6. Execute pattern
        7. Process data capture results

        **Criteria:** 
            - Both groups will exit KA at the same time and this is
            validated by having group/slice 1 pin 0 capturing group0/slice 0 pin 34.
            Capture memory should only have exactly one CTV event.
            
            - Check that the faster group cycle count is 2^N more than the slower
                i.e. Faster executed 1,000,00 and slower executed 1,000,000 / 2^1 = 500,000
             
        """
        pass

    def DirectedEachSlicePerGroupCtvTest(self):
        """Each Slice in Its Own Group with unique number of CTV Capture Vectors
        
        1. Make five groups, each having one unique slice
            - Set group 0 to 200 MHz with subcycle 0
            - Set group 1 to 200 MHz with subcycle 1
            - Set group 2 to 200 MHz with subcycle 2
            - Set group 3 to 200 MHz with subcycle 3
            - Set group 4 to 200 MHz with subcycle 4
        2. Construct a pattern with CTV vectors
            - Limit CTV vectors up to 2048 due to bandwith limitation
                since no stripping is available.
        3. Set up tester for pattern execution (see PatternHelper class for more info)
        4. Execute pattern
        5. Process data capture results

        **Criteria:**                  
            - No underrun alarm
            - CTV data capture based randomization. Correctness will 
                be checked against the simulator
        """
        ctv_vector_values = [random.randint(32, 5000) for x in range(5)]
        pattern_string1 = f'''
               PATTERN_START:
               S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
               S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
               S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
               S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

               PatternId 0x100
               PCall PATTERN1

               I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
               NoCompareDriveZVectors length=1

               PATTERN1:
                    AddressVectorsPlusAtt length={{}}
                    Return
               '''

        group_0_slices = [0]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1.format(ctv_vector_values[0])})

        group_1_slices = [1]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1.format(ctv_vector_values[1])})
        group_2_slices = [2]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=2, slices=group_2_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1.format(ctv_vector_values[2])})
        group_3_slices = [3]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=3, slices=group_3_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1.format(ctv_vector_values[3])})
        group_4_slices = [4]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=4, slices=group_4_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1.format(ctv_vector_values[4])})

        self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=1, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=2, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=3, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=4, slices=group_1_slices)
        self.pattern_helper.execute_pattern_scenario()

    @skip_snapshot
    @deactivate_simulator
    def Directed100MillionOneSlicePerGroupSanityCheckNoDataCheckTest(self):
        """Each Slice in Its Own Group with unique 10,000 CTV vectors among 100M Vectors

        1. Make five groups, each having one unique slice
            - Set group 0 to 200 MHz with subcycle 0
            - Set group 1 to 200 MHz with subcycle 1
            - Set group 2 to 200 MHz with subcycle 2
            - Set group 3 to 200 MHz with subcycle 3
            - Set group 4 to 200 MHz with subcycle 4
        2. Construct a pattern with CTV vectors
            - Limit CTV vectors to 5,000 due to bandwith limitation.
                Using a for loop, execute 2 CTV capture per 1,000 vectors

        3. Set up tester for pattern execution (see PatternHelper class for more info)
        4. Execute pattern
        5. Process data capture results

        **Criteria:**
            - No underrun alarm
            - CTV data capture based randomization. Correctness will
                be checked against the simulator
        """
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.e32_interval = 128
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)
        repeat = 10000
        loops = 290
        self.ctp = 0b00000000000000000001001000000000010
        pattern_string1 = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={loops}

                PatternId 0x100
                PCall LOOP

                LOOP:
                    CompareHighVectors length=2, ctv=1
                    CompareHighVectors length=1000, ctv=0
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1
                '''
        self.Log('debug', pattern_string1)

        group_0_slices = [0]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})

        group_1_slices = [1]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})
        group_2_slices = [2]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=2, slices=group_2_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})
        group_3_slices = [3]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=3, slices=group_3_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})
        group_4_slices = [4]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=4, slices=group_4_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1})

        self.pattern_helper.set_pm_subcycle(subcycle=0, slices=group_0_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=1, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=2, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=3, slices=group_1_slices)
        self.pattern_helper.set_pm_subcycle(subcycle=4, slices=group_1_slices)

        self.pattern_helper.execute_pattern_scenario()

        total_ctv_vectors = repeat * loops
        expected_e32_count = total_ctv_vectors//self.pattern_helper.e32_interval

        expected_ctv_header_count = loops * 2
        self.env.hbicc.pat_gen.check_ctv_header_count(expected=expected_ctv_header_count, slices=self.slices)

        cycles_per_ctv_block = 8
        default_pg_packing = 4
        expected_ctv_blocks = (repeat * loops // cycles_per_ctv_block) * \
                              len(self.env.hbicc.get_pin_multipliers()) + default_pg_packing
        self.env.hbicc.pat_gen.check_ctv_data_block_count(expected=1312, slices=self.slices)
        self.env.hbicc.pat_gen.check_e32_count(expected=2270)

        for pm in self.env.hbicc.get_pin_multipliers():
            pm.check_h_counter(expected=290580, slices=self.slices)

    def DirectedEachSlicePerGroupVarySubcycleCtvLengthPerGroupTest(self):
        """Each Slice in Its Own Group with Different CTV Vectors per Group
        
        1. Make five groups, each having one unique slice
            - Set group 0 to 200 MHz with subcycle 0
            - Set group 1 to 200 MHz with subcycle 1
            - Set group 2 to 200 MHz with subcycle 2
            - Set group 3 to 200 MHz with subcycle 3
            - Set group 4 to 200 MHz with subcycle 4
        2. Construct a pattern with CTV vectors
            - Limit CTV vectors up to 2048 due to bandwith limitation
                since no striping is available.
            - Each group should have a unique number of CTV vectors
        3. Set up tester for pattern execution (see PatternHelper class for more info)
        4. Execute pattern
        5. Process data capture results

        **Criteria:**                  
            - No underrun alarm
            - CTV data capture based randomization. Correctness will 
                be checked against the simulator
        """
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b00000000000000000001001000000000010
    
                PatternId 0x100
                PCall LOOP
    
                LOOP:
                    AddressVectorsPlusAtt length={{}}
    
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={{}}
                DriveZeroVectors length=1
                '''
        patterns = [pattern_string.format(random.randint(64, 2048), random.randint(0, 0xFFFFFFFF)) for x in range(5)]

        for slice in range(5):
            group = slice
            subcycle = slice
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=group, slices=[slice],
                                                                       address_and_pattern_list={
                                                                           0: patterns[slice]})
            self.pattern_helper.set_pm_subcycle(subcycle=subcycle, slices=[slice])

        self.pattern_helper.execute_pattern_scenario()

    def DirectedEachSlicePerGroupVaryCtvLengthPerGroupTest(self):
        """Each Slice in Its Own Group with Different CTV Vectors per Group, CTP, and End_Status

        1. Make five groups, each having one unique slice
            - All groups are running at the same speed, vary base and subcycle 0
        2. Construct a pattern with CTV vectors
            - Limit CTV vectors up to 2048 due to bandwith limitation
                since no stripping is available.
            - Each group should have a unique number of CTV vectors
        3. Set up tester for pattern execution (see PatternHelper class for more info)
        4. Execute pattern
        5. Process data capture results

        **Criteria:**                  
            - No underrun alarm
            - CTV data capture based randomization. Correctness will 
                be checked against the simulator
        """
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={{}}
    
                PatternId 0x100
                PCall LOOP
    
                LOOP:
                    AddressVectorsPlusAtt length={{}}
    
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={{}}
                DriveZeroVectors length=1
                '''
        patterns = [pattern_string.format(random.randint(1, 0b11111111111111111111111111111111111),
                                          random.randint(1000, 10000),
                                          random.randint(0, 0xFFFFFFFF))
                    for x in range(4)]

        for slice in range(4):
            group = slice
            self.pattern_helper.create_slice_channel_set_pattern_combo(group=group, slices=[slice],
                                                                       address_and_pattern_list={
                                                                           0: patterns[slice]})
            self.pattern_helper.set_pm_subcycle(subcycle=0, slices=[slice])

        self.pattern_helper.execute_pattern_scenario()

    @deactivate_simulator
    def DirectedHDMT140508Test(self):
        """To have a long burst with 60mega vector to catch if OCT cal happens mid burst affecting capture data
            Pattern size is mimicking one of TOS KA stress test.This test was run in loops to catch the bug.
        1. Make 2 groups, one having even slices and one having odd.
        2. Construct a pattern with strobes on certain pins to use fail capture
            - Limit fails vectors to 1000 due to bandwith limitation.
        3. Set up tester for pattern execution (see PatternHelper class for more info)
        4. Execute pattern
        5. Process data capture results

        **Criteria:**
            - No fails to be observed
            - Error capture stream should have only EOB packets.
        """
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.e32_interval = 128
        self.pattern_helper.user_mode='RELEASE'
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)

        self.ctp = 0b00000000000000000001001000000000010
        pattern_string1 = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={205}
                    NoCompareDriveZVectors length={1024}
                    PatternId 0x100
                    PCall LOOP

                    LOOP:
                        %repeat 163210
                                V ctv=0, mtv=0, lrpt=0, data=0v1X0X0L0H1X0L0L0H1X0L0L0H1X0L0L0H1XX
                                V ctv=0, mtv=0, lrpt=0, data=0v0X0X0L0L0X0L0L0L0X0L0L0L0X0L0L0L0XX
                                %end
                        I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                        I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    DriveZeroVectors length=1
                    '''
        pattern_string2 = f'''
                            PATTERN_START:
                            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                            I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm={205}
                            NoCompareDriveZVectors length={1024}
                            PatternId 0x100
                            PCall LOOP

                            LOOP:
                             %repeat 163210
                                 V ctv=0, mtv=0, lrpt=0, data=0vXX0X0L1H1X0L0L1H1X0L0L1H1X0L0L1H1XX
                                 V ctv=0, mtv=0, lrpt=0, data=0vXX0X0L0L0X0L0L0L0X0L0L0L0X0L0L0L0XX
                                %end
                                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=0, dest=0, imm=1
                                I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1

                            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                            DriveZeroVectors length=1
                            '''
        self.Log('debug', pattern_string2)

        group_0_slices = [0,2,4]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=0, slices=group_0_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string1},attributes={'capture_fails': 1, 'capture_ctv': 0})

        group_1_slices = [1,3]
        self.pattern_helper.create_slice_channel_set_pattern_combo(group=1, slices=group_1_slices,
                                                                   address_and_pattern_list={
                                                                       0: pattern_string2},attributes={'capture_fails': 1, 'capture_ctv': 0})


        self.pattern_helper.execute_pattern_scenario()
        self.pattern_helper.hbicc.pat_gen.check_error_block_count(expected=4)



class EnabledClock(HbiccTest):
    """Validation of Multi-slice Group Feature with Enable Clock

    Enabled clock at Ratio 1 and non-ratio 1 in fast and slow domains
    
    """
    
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.end_status = random.randint(0, 0xFFFFFFFF)
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT) - 1))
        self.slices.sort()

    def DirectedEnablClockVarySubcycle(self):
        pass
