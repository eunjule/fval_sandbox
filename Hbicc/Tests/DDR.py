# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Instrument attached memory is DDR4 DIMMs

The instrument DDR is accessible from the system using DMA (Direct Memory
Access) transfers.

DMA uses the PCIe bus, which has lower bandwidth than
the DDR memory bus.
"""

import os
import queue
import random
import sys
import threading
import time
import unittest

from Hbicc.Tests.HbiccTest import HbiccTest
from Common.fval import skip

MAX_FAIL_COUNT = 5


class Diagnostics(HbiccTest):
    """Tests that span memory and use various patterns to detect issues."""

    PATGEN_MEMORY_SIZE = 32<<30
    last_address_for_64gb_dimm = 64*1024*1024*1024

    def setUp(self, tester=None):
        super().setUp()
        self.board = self.env.hbicc.pat_gen

    def DirectedDMAToPatgenFullMemoryStepTest(self):
        """
        Randomly write and read back correct data to DDR4 memory associated with Patgen.
        There are 5 dimms each with 32gb capacity.
        Repeat this for stability.

        """
        board = self.env.hbicc.pat_gen
        if board is None:
            raise unittest.SkipTest('Patgen unavailable')
        offset_error_list = []
        slice_list = [0, 1, 2, 3, 4]
        memory_chunk_size = 1<<30 # 1gb
        random_1gb_data = os.urandom(memory_chunk_size)
        for slice in board.check_ddr_calibration_status(retry_limit=50, slice_list=slice_list):
            for offset in range(0, self.PATGEN_MEMORY_SIZE, memory_chunk_size):
                start_address = offset
                board.dma_write(start_address, random_1gb_data, slice)
                read_back_data = board.dma_read(start_address, memory_chunk_size, slice)
                if read_back_data != random_1gb_data:
                    offset_error_list.append(
                        {'Offset error seen on': hex(offset),'Slice': slice})
                if len(offset_error_list) >= MAX_FAIL_COUNT:
                    break
        if offset_error_list:
            self.report_errors(board, offset_error_list)
        else:
            self.Log('info', 'DMA operation successful on all slices.')

    def DirectedPatgenDetectMemorySizeTest(self):
        """
        Randomly write and read back correct data to DDR4 memory associated with Patgen.
        There are 5 dimms each with 32gb capacity.
        Repeat this for stability.

        """
        if self.board is None:
            raise unittest.SkipTest('Patgen unavailable')
        slice_list = [0, 1, 2, 3, 4]
        chunk_size = 1 << 10
        init_memory_data, repeated_data = self.get_random_data(chunk_size)
        self.init_all_slices_first_memory_block(init_memory_data)
        for slice in self.board.check_ddr_calibration_status(retry_limit=50, slice_list=slice_list):
            next_address = chunk_size
            for loop in range(1, 28):
                next_address,previous_address = self.get_next_and_previous_address(next_address, slice)
                self.write_slice(next_address, repeated_data, slice)
                if self.ddr_rolled_over(slice, init_memory_data, previous_address):
                    self.init_all_slices_first_memory_block(init_memory_data)
                    break
                if self.is_slice_boundry_reached(slice, init_memory_data, next_address):
                    self.init_all_slices_first_memory_block(init_memory_data)
                    break
                if self.is_there_hole_in_memory(next_address, init_memory_data, slice, repeated_data):
                    self.init_all_slices_first_memory_block(init_memory_data)
                    break

    def write_slice(self, next_address, repeated_data, slices):
        if next_address == 0:
            slices = 0
        self.board.dma_write(next_address, repeated_data, slices)

    def get_next_and_previous_address(self, next_address, slices):
        next_address = next_address << 1
        previous_address = next_address
        if next_address == self.last_address_for_64gb_dimm:
            if slices == 4:
                next_address = 0
        return next_address,previous_address

    def get_random_data(self, chunk_size):
        repeated_data = os.urandom(chunk_size)
        init_memory_data = os.urandom(chunk_size)
        return init_memory_data, repeated_data

    def is_there_hole_in_memory(self, next_address, init_memory_data, slices, repeated_data):
        repeted_data_read_back = self.board.dma_read(next_address, len(init_memory_data), slices)
        if repeated_data != repeted_data_read_back:
            self.Log('error', f'Memory hole found in slice {slices} after {(next_address/(1<<30))} GB')
            return True

    def ddr_rolled_over(self, slices, init_memory_data, previous_address):
        read_original_data = self.board.dma_read(0,len(init_memory_data), 0)
        if read_original_data != init_memory_data:
            if self.last_address_for_64gb_dimm == previous_address:
                if slices == 4:
                    self.Log('info', f'DDR Rolled Over for Slice {slices}. Max DDR available for this slice is {(previous_address/(1<<30))} GB')
            else:
                fab_letter = self.board.mainboard.blt.fab_letter()
                if fab_letter == 'B':
                    self.Log('info', f'DDR Rolled Over for Slice {slices}. Max DDR available for this slice is {(previous_address/(1<<30))} GB')
                else:
                    self.Log('error',
                             f'DDR Rolled Over for Slice {slices} Max DDR available for ths slice is {(previous_address/(1<<30))} GB')
            return True

    def is_slice_boundry_reached(self, slices, init_memory_data, previous_address):
        next_slice = self.get_next_slice_number(slices)
        read_original_data = self.board.dma_read(0, len(init_memory_data), next_slice)
        if read_original_data != init_memory_data:
            self.Log('info', f'Size of DIMM for slice {slices} is {(previous_address/(1<<30))} GB')
            return True

    def init_all_slices_first_memory_block(self, init_memory_data):
        for slices in range(0, 5):
            self.board.dma_write(0, init_memory_data, slices)

    def get_next_slice_number(self, slices):
        next_slice = slices + 1
        if next_slice == 5:
            next_slice = 0
        return next_slice

    def report_errors(self, device, register_errors):
        if register_errors:
            column_headers = list(register_errors[0].keys())
        table = device.contruct_text_table(column_headers, register_errors)
        self.Log('error', 'DMA operation failed at following address ' +
                         f'offset/s: \n {table}')

    def DMAToPatgenHalfDimmMemoryStep(self):
        """ NOTE: Use for EBT only

        Randomly write and read back correct data to DDR4 memory associated with Patgen.
        There are 5 dimms each with 32gb capacity.
        Repeat this for stability.

        ** NOTE ** Use for EBT only
        """
        board = self.env.hbicc.pat_gen
        if self.env.hbicc.pat_gen is None:
            raise unittest.SkipTest('Patgen unavailable')
        offset_error_list = []
        slice_list = [0, 1, 2, 3, 4]
        for slice in board.check_ddr_calibration_status(retry_limit=50, slice_list=slice_list):
            memory_chunk_size = 512<<20  # 512MB
            random_512mb_data = os.urandom(memory_chunk_size)
            for offset in range(0, self.PATGEN_MEMORY_SIZE, memory_chunk_size):
                start_address = offset
                board.dma_write(start_address, random_512mb_data, slice)
                read_back_data = board.dma_read(start_address, memory_chunk_size, slice)
                if read_back_data != random_512mb_data:
                    offset_error_list.append(
                        {'Offset error seen on': hex(offset), 'Slice': slice})
                if len(offset_error_list) >= MAX_FAIL_COUNT:
                    break
        if offset_error_list:
            self.report_errors(board, offset_error_list)
        else:
            self.Log('info', 'DMA operation successful on all slices.')

    def DirectedDMAToRingMultiplierFullMemoryTest(self):
        """
        Randomly write and read back correct data to component memory associated with RingMultiplier.
        The component memory capacity is 2gb. This test dmas to entire 2gb at once and repeats this process 10 times.
        There are no ECC alarms associated with the component memory.
        Repeat this for stability.

        """
        board = self.env.hbicc.ring_multiplier
        if board is None:
            raise unittest.SkipTest('RingMultiplier unavailable')
        offset_error_list = []
        ddr_calibration_status = board.get_ddr_calibration_status()
        if ddr_calibration_status != 5:
            self.Log('error', 'DDR calibration failed.Cannot perform DMA')
        else:
            total_memory_capacity = 1<<30
            memory_chunk_size = 1<<30  # 1gb
            random_1gb_data = os.urandom(memory_chunk_size)
            for offset in range(0, total_memory_capacity, memory_chunk_size):
                board.dma_write(offset, random_1gb_data)
                read_back_data = board.dma_read(offset, memory_chunk_size)
                if read_back_data != random_1gb_data:
                    offset_error_list.append(
                        {'Offset error seen on': hex(offset)})
                if len(offset_error_list) >= MAX_FAIL_COUNT:
                    break
            if offset_error_list:
                self.report_errors(board, offset_error_list)
            else:
                self.Log('info', 'DMA operation successful on all slices.')

    def DirectedDMAToRingMultiplierHalfMemoryStepTest(self):
        """
        Randomly write and read back correct data to component memory associated with RingMultiplier.
        This test writes random data to one gb at a time and verifies and repeats this process 5 times.
        Repeat this for stability.
        """
        board = self.env.hbicc.ring_multiplier
        if board is None:
            raise unittest.SkipTest('RingMultiplier unavailable')
        offset_error_list = []
        ddr_calibration_status = board.get_ddr_calibration_status()
        if ddr_calibration_status != 5:
            self.Log('error', 'DDR calibration failed.Cannot perform DMA')
        else:
            total_memory_capacity = 1 << 30
            memory_chunk_size = 512 << 20  # 128kb
            random_1gb_data = os.urandom(memory_chunk_size)
            for offset in range(0, total_memory_capacity, memory_chunk_size):
                board.dma_write(offset, random_1gb_data)
                read_back_data = board.dma_read(offset, memory_chunk_size)
                if read_back_data != random_1gb_data:
                    offset_error_list.append(
                        {'Offset error seen on': hex(offset)})
                if len(offset_error_list) >= MAX_FAIL_COUNT:
                    break
            if offset_error_list:
                self.report_errors(board, offset_error_list)
            else:
                self.Log('info', 'DMA operation successful on all slices.')

    def DMAToRingMultiplierFixedMemorySizeRegressTest(self, test_iteration=2000):
        """
        Create a fixed size of data to be written (64kb) and DMA it multiple times to check for stabitlity.
        The size chosen here is for a reason because certain sizes for S-10 have been giving DMA timeouts.
        Also the number of times the same data is repeatedly written was added to check since there were mismatch in data that
        was written and readback.
        Repeat this for stability.
        """
        board = self.env.hbicc.ring_multiplier
        if board is None:
            raise unittest.SkipTest('RingMultiplier unavailable')
        start_address = 0
        max_fail_count = 20
        fail_count =0
        cal_fail_count = 0
        max_cal_fail_count = 20
        memory_chunk_size= 64<<10
        random_data = os.urandom(64 << 10)
        for loop in range(test_iteration):
            ddr_calibration_status = board.get_ddr_calibration_status()
            if ddr_calibration_status != 5:
                cal_fail_count+=1
                if cal_fail_count >= max_cal_fail_count:
                    self.Log('error', ' Max attempts for DDR calibration retry failed.Cannot perform DMA')
                    break
            else:
                board.dma_write(start_address, random_data)
                read_back_data = board.dma_read(start_address,memory_chunk_size)
                if random_data != read_back_data:
                    fail_count+=1
                    if fail_count>=max_fail_count:
                        self.Log('error','{} DMA write/read failure exceeded max number of attempts.'.format(board.name()))
                        break
        else:
            self.Log('info','{} {} bytes block size DMA operation successful for all test iterations'.format(board.name(),len(random_data)))

    def DMAToPatgenFixedMemorySizeRegressTest(self, test_iteration=2000):
        """
        Create a fixed size of data to be written (64kb) and DMA it multiple times to check for stabitlity.
        The size chosen here is for a reason because certain sizes for S-10 have been giving DMA timeouts.
        Also the number of times the same data is repeatedly written was added to check since there were mismatch in data that
        was written and readback.
        Repeat this for stability.
        """
        board = self.env.hbicc.pat_gen
        if board is None:
            raise unittest.SkipTest('Patgen unavailable')
        max_fail_count = 20
        fail_count =0
        random_data = os.urandom(64 << 10)
        memory_chunk_size = 64<<10
        for loop in range(test_iteration):
            slice_list = [0, 1, 2, 3, 4]
            for slice in board.check_ddr_calibration_status(retry_limit=50, slice_list=slice_list):
                for offset in range(64 << 30):
                    start_address = offset
                    board.dma_write(start_address, random_data, slice)
                    read_back_data = board.dma_read(start_address, memory_chunk_size, slice)
                    if random_data != read_back_data:
                        fail_count+=1
                        if fail_count>=max_fail_count:
                            self.Log('error','{} DMA write/read max no of attempts exceeded.'.format(board.name()))
                            break
        else:
            self.Log('info','{} {} bytes block size DMA operation successful for all  {} test iterations'.format(board.name(),len(random_data),test_iteration))

    def DirectedDMAToRingMultiplierWalkingPatternTest(self):
        """
        Create a walking pattern with the folowing format all the way upto MSB toggling the bits high-low.
        b'\0x01\0x00\0x00\0x00\0x00\0x00\0x00\0x00' + \
        b'\0xFD\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF'
        Create this pattern for 1gb size and DMA to memory.
        This test is to check for board level stability.
        """
        board = self.env.hbicc.ring_multiplier
        if board is None:
            raise unittest.SkipTest('RingMultiplier unavailable')
        offset_error_list =[]
        pattern_size = 1 << 30
        random_data = toggling_walk_pattern(pattern_size)
        ddr_calibration_status = board.get_ddr_calibration_status()
        if ddr_calibration_status != 5:
            self.Log('error', 'DDR calibration failed.Cannot perform DMA')
        else:
            total_memory_capacity = 1 << 30
            for offset in range(0, total_memory_capacity, pattern_size):
                board.dma_write(offset, random_data)
                read_back_data = board.dma_read(offset, pattern_size)
                if read_back_data != random_data:
                    offset_error_list.append(
                        {'Offset error seen on': hex(offset)})
                if len(offset_error_list) >= MAX_FAIL_COUNT:
                    break
            if offset_error_list:
                self.report_errors(board, offset_error_list)
            else:
                self.Log('info', 'DMA operation successful on all slices.')

    def DMAToPatgenWalkingPattern(self):
        """NOTE: Use for EBT only

        Create a walking pattern with the folowing format all the way upto MSB toggling the bits high-low.
        b'\0x01\0x00\0x00\0x00\0x00\0x00\0x00\0x00' + \
        b'\0xFD\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF'
        Create this pattern for 1gb size and DMA to memory.
        This test is to check for board level stability.

        ** NOTE ** Use for EBT only
        """
        board = self.env.hbicc.pat_gen
        if board is None:
            raise unittest.SkipTest('Patgen unavailable')
        pattern_size = 1 << 30
        offset_error_list=[]
        random_data = toggling_walk_pattern(pattern_size)
        slice_list = [0, 1, 2, 3, 4]
        for slice in board.check_ddr_calibration_status(retry_limit=50, slice_list=slice_list):
            for offset in range(0, self.PATGEN_MEMORY_SIZE, pattern_size):
                start_address = offset
                board.dma_write(start_address, random_data, slice)
                read_back_data = board.dma_read(start_address, pattern_size, slice)
                if read_back_data != random_data:
                    offset_error_list.append(
                        {'Offset error seen on': hex(offset), 'Slice': slice})
                if len(offset_error_list) >= MAX_FAIL_COUNT:
                    break
        if offset_error_list:
            self.report_errors(board, offset_error_list)
        else:
            self.Log('info', 'Walking Pattern DMA operation successful on ' +
                             'all slices.')

    def DirectedTFS94460PatgenNon32BAlignedTest(self,test_iteration=100000):
        """ This test is directed with specific size such that the we see the 32B mis-alignment more often.The loop iteration is
        large since the probablity of occurence of the data mismatch is low. Would fail on all pre-19.4 Quartus built images.
        HDMT Ticket 94460:HBI MB: PG FPGA - DMA data mismatch when the the data is non-32B aligned"""
        board = self.env.hbicc.pat_gen
        if board is None:
            raise unittest.SkipTest('Patgen unavailable')
        slice_list = [0]
        for slice in slice_list:
            self.run_misaligned_buffer_test(board, slice, test_iteration)

    def run_misaligned_buffer_test(self, board, slice, iterations):
        self.Log('info', f'Testing slice {slice}')
        frequent_mismatch_data_size = 180336
        misaligned_buffers = [buffer for buffer in(self.misaligned_buffer(length=frequent_mismatch_data_size) for i in range(20)) if buffer is not None]
        self.print_buffer_page_offsets(misaligned_buffers)
        error_count = 0
        if misaligned_buffers:
            for i in range(iterations):
                data = random.choice(misaligned_buffers)
                try:
                    self.dma_write_read(board, i, data, slice)
                except Diagnostics.DmaCompareError:
                    error_count += 1
                if error_count >= MAX_FAIL_COUNT:
                    self.Log('info', f'Reached max fail count of {MAX_FAIL_COUNT}, stopping test on slice {slice}')
                    break
            else:
                self.Log('info', f'Non-32B Aligned dma operation successful on slice {slice} with {len(misaligned_buffers)} '
                                 f'misaligned buffers.')
        else:
            self.Log('error', f'Slice {slice}: Unable to generate a Misaligned Buffer')

    class DmaCompareError(Exception):
        pass

    def dma_write_read(self, board, i, data, slice):
        start_address = 0
        board.dma_write(start_address, data, slice)
        read_back_data = board.dma_read(start_address, len(data), slice)
        if read_back_data != data:
            self.Log('error', f'Offset error seen on {start_address:09X}, Slice  {slice} at iteration {i}')
            self.write_buffers_to_file(i, data, read_back_data)
            raise Diagnostics.DmaCompareError()

    def write_buffers_to_file(self, i, data, read_back_data):
        with open(f'loop{i}_expected.bin', 'wb') as f:
            f.write(data)
        with open(f'loop{i}_actual.bin', 'wb') as f:
            f.write(read_back_data)

    def print_buffer_page_offsets(self, misaligned_buffers):
        offsets = [self.logical_address_of_data(b) & 0xFFF for b in misaligned_buffers]
        self.Log('info', 'Using buffers with the following offsets within the first page:')
        self.Log('info', ' '.join([f'0x{offset:03X}' for offset in sorted(offsets)]))

    def misaligned_buffer(self, length, retry=500):
        if length == 0:
            return b''
        for i in range(retry):
            integer = random.getrandbits(length * 8)
            result = integer.to_bytes(length, sys.byteorder)
            x = self.logical_address_of_data(result)
            y = os.urandom((15 << 10) * i)
            if x % 32 != 0:
                return result
            length += 16
        else:
            return None

    def logical_address_of_data(self, bytes_object):
        return (id(bytes_object) + sys.getsizeof(bytes_object) - len(bytes_object) - 1)


def toggling_walk_pattern(pattern_size, start=0):
    buffer = b'\0x01\0x00\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFD\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x04\0x00\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xF7\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x10\0x00\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xDF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x40\0x00\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0x7F\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x01\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFD\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x04\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xF7\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x10\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xDF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x40\0x00\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0x7F\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x01\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFD\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x04\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xF7\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x10\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xDF\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x40\0x00\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0x7F\0xFF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x01\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFD\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x04\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xF7\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x10\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xDF\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x40\0x00\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0x7F\0xFF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x01\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFD\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x04\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xF7\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x10\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xDF\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x40\0x00\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0x7F\0xFF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x01\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFD\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x04\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xF7\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x10\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xDF\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x40\0x00\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0x7F\0xFF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x01\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFD\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x04\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xF7\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x10\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xDF\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x40\0x00' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0x7F\0xFF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x00\0x01' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFD' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x00\0x04' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xF7' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x00\0x10' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xDF' + \
             b'\0x00\0x00\0x00\0x00\0x00\0x00\0x00\0x40' + \
             b'\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0xFF\0x7F'
    reps = pattern_size // len(buffer)
    pattern = buffer * reps
    if len(pattern) == pattern_size:
        return pattern
    else:
        return pattern[:pattern_size]
