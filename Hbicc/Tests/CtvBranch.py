# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The HBICC provides branch instructions for use during execution.

Branches include Goto, Call, Pcall, and Return.

Each tests in this suite will coupled a branch instruction with CTV data collection.
CTV Headers and CTV data must contains correct information of the current jump instruction.

Branches can use several base offsets:
+ Global : Address used directly
+ Commmon File Base (CFB) : Software defined offset
+ Relative : +/- from current address
+ Pattern File Base (PFB) : relative to last PCall address

Even though branches can be conditional on many flags, those tests will not be covered here.
"""
import random

from Common.fval import skip
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.rpg import RPG

KILO = 0x00000400
PADDING =1024


class Functional(HbiccTest):
    """Tests for all branch types and conditions with CTV data capture

    **Criteria: ** All tests within this class should complete with the following:

    - Correct Pattern End Status word
    - Correct capture data, mainly CTV and Trace
    - No underrun issues
    - No critical alarms
    """

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        population = set(range(5))
        self.slices = random.sample(population,
                                    random.randint(1, len(range(5)) - 1))
        self.slices.sort()
        self.fixed_drive_state = 'LOW'
        self.end_status = random.randint(0, 0xFFFFFFFF)
        self.ctp = 0b00000000000000000001001000000000010

    def DirectedSubrHeadCallIGlobalTest(self):
        """Absolute CALL to a subroutine at the beginning of a pattern.

        The vector at the PCALL target address is a CALL instruction.
        The CALL uses the immediate field its target address.
        The CALL target is a global address.
        Adding CTV vectors after the Call
        """
        pattern_string = self.simple_subr_head("GLOBAL", "CALL_I")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrTailCallIGlobalTest(self):
        """Absolute CALL to a subroutine at the end of a pattern.

        The CALL instruction is immediately before the RET instruction for the PCALL.
        The CALL uses the immediate field as its target address.
        The CALL target is a global address.
        Adding CTV vectors after the Call

        """
        pattern_string = self.simple_subr_tail("GLOBAL", "CALL_I")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrHeadCallRRelTest(self):
        """Relative CALL a subroutine at the beginning of a pattern.

        The vector at the PCALL target address is a CALL instruction.
        The CALL uses a register for its target address.
        The CALL target is relative to the current address.
        Adding CTV vectors after the Call
        """
        pattern_string = self.simple_subr_head("RELATIVE", "CALL_R")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrTailCallRRelTest(self):
        """Relative Call to a subroutine at the end of a pattern.

        The CALL instruction is immediately before the RET instruction for the PCALL.
        The CALL uses a register for its target address.
        The CALL target is relative to the current address.
        Adding CTV vectors after the Call
        """
        pattern_string = self.simple_subr_tail("RELATIVE", "CALL_R")
        self.execute_and_check_results(pattern_string)

    def DirectedSubrTailGapCallICfbTest(self):
        """Captures CTV Data Vectors using Call_I - CFB Base

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached
        by referencing the Common File Base register value set by the user.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, Call_I using CFB base to reach pattern_string_2
            - Pattern_string_2 has only CTVs and return instruction
            - Complete pattern_string_1 with more CTVs
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 256
            - CTV Data Objects: 32 blocks + One End of
                Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, Call_I, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks

        """
        bytes_per_vector = 16
        random_addresses = list(range(KILO*10, KILO*KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''
        
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
    
            PatternId 0x100
            PCall PATTERN1
    
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1
    
            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}
                
                PRE_RAND:
                    CompareHighVectors length=32
                    I optype=BRANCH, base=BRBASE.CFB, br=CALL_I, imm=0, cond=COND.NONE, invcond=0
                
                POST_RAND:
                    AddressVectorsPlusAtt length=128
                
                Return
            '''

        pattern_string_2 = '''
            AddressVectorsPlusAtt length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrTailGapCallRCfbTest(self):
        """Captures CTV Data Vectors using Call_R - CFB Base

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached using Call_R
        and using Common File Base register value as base.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, Call_R using CFB base to reach pattern_string_2
            - Pattern_string_2 has only CTVs and return instruction
            - Complete pattern_string_1 with more CTVs
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 256
            - CTV Data Objects: 32 blocks + One End of
                Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, Call_I, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks

        """
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest={0}, imm={0} 
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}

                PRE_RAND:
                    CompareHighVectors length=32
                    I optype=BRANCH, base=BRBASE.CFB, br=CALL_R, dest=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    AddressVectorsPlusAtt length=128

                Return
            '''

        pattern_string_2 = '''
            AddressVectorsPlusAtt length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrTailGapPCallICfbTest(self):
        """Captures CTV Data Vectors using PCALL_I - CFB Base

        The PCALL uses the immediate field as its target address.
        The PCALL target is relative to the Common File Base (CFB).

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached using PCALL_I
        and using Common File Base register value as base.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, PCALL_I using CFB base to reach pattern_string_2
            - Pattern_string_2 has only CTVs and return instruction
            - Complete pattern_string_1 with more CTVs
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 256
            - CTV Data Objects: 32 blocks + One End of
                Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks

        """
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}
                
                PRE_RAND:
                    CompareHighVectors length=32
                    PatternId 0x200        
                    I optype=BRANCH, base=BRBASE.CFB, br=PCALL_I, imm=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    AddressVectorsPlusAtt length=128

                Return
            '''

        pattern_string_2 = '''
            AddressVectorsPlusAtt length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrTailGapPCallRCfbTest(self):
        """Captures CTV Data Vectors using PCALL_R - CFB Base

        The PCALL uses the immediate field as its target address.
        The PCALL target is relative to the Common File Base (CFB).

        **Test:**

        Pattern broken up into two memory spaces. The second portion is reached using PCALL_R
        and using Common File Base register value as base.

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - random CFB address located beyond memory space used by pattern_string_1
        * Construct pattern_string_1 and _2 based on attributes
            - In pattern_string_1, store CFB address in designated CFB register
            - In pattern_string_1, PCALL_R using CFB base to reach pattern_string_2
            - Pattern_string_2 has only CTVs and return instruction
            - Complete pattern_string_1 with more CTVs
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 256
            - CTV Data Objects: 32 blocks + One End of
                Burst block per channel set
            - Jump/Call trace: 4 blocks (PCall, PCall, Return, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks

        """
        bytes_per_vector = 16
        random_addresses = list(range(KILO * 10, KILO * KILO, bytes_per_vector))
        cfb_address = random.choice(random_addresses)
        pattern_string_1 = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}

            PatternId 0x100
            PCall PATTERN1

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            PATTERN1:
                I optype=REGISTER, opdest=REGDEST.CFB, opsrc=REGSRC.IMM, imm={cfb_address//bytes_per_vector}
                

                PRE_RAND:
                    CompareHighVectors length=32
                    I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest={0}, imm={0}
                    PatternId 0x200        
                    I optype=BRANCH, base=BRBASE.CFB, br=PCALL_R, dest=0, cond=COND.NONE, invcond=0

                POST_RAND:
                    AddressVectorsPlusAtt length=128

                Return
            '''

        pattern_string_2 = '''
            AddressVectorsPlusAtt length=128
            Return
        '''

        self.execute_and_check_results(pattern_string='', address_and_pattern_list={0: pattern_string_1, cfb_address: pattern_string_2})

    def DirectedSubrRecursivePCallIPfb10Test(self):
        pattern_string = self.recursive_pcall_i_pfb(10)
        self.execute_and_check_results(pattern_string)

    def DirectedSubrRecursivePCallIPfb63Test(self):
        pattern_string = self.recursive_pcall_i_pfb(63)
        self.execute_and_check_results(pattern_string)
        
    def DirectedSubrNestedPCallIGlobalTest(self):
        loops = 3
        pattern_string = self.simple_nest_pcall_global(loops)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoAllAddrInOrderTest(self):
        address_and_pattern_list = self.generate_goto_all_address_patter_list()
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            fixed_drive_state=self.fixed_drive_state, slices=self.slices,
            address_and_pattern_list=address_and_pattern_list)
        self.pattern_helper.execute_pattern_scenario()

    def generate_goto_all_address_patter_list(self):
        address_and_pattern_list = {}
        start_address_pattern_1 = 0x0
        go_to_address_in_pattern_1 = 0x1900
        pattern_start = f'''
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} # ctp mask

            PatternId 0x100
            PCall PATTERN1
            PATTERN1:
            AddressVectorsPlusAtt length=64, ctv=1
                Goto 0x{go_to_address_in_pattern_1:X}
        '''
        address_and_pattern_list.update(
            {start_address_pattern_1 * 16: pattern_start})

        go_to_address_in_pattern_2 = 0x10000
        pattern_middle = f'''
            AddressVectorsPlusAtt length=256, ctv=1
            Goto 0x{go_to_address_in_pattern_2:X}
        '''
        address_and_pattern_list.update(
            {go_to_address_in_pattern_1 * 16: pattern_middle})

        pattern_end = f'''
                   AddressVectorsPlusAtt length=1024, ctv=1
                   I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                   NoCompareDriveZVectors length=1
                   '''
        address_and_pattern_list.update(
            {go_to_address_in_pattern_2 * 16: pattern_end})

        return address_and_pattern_list

    def DirectedGotoImmediateGlobalTest(self):
        address = [x for x in range(0, 48, 8)]
        index = random.randint(0, len(address) - 1)

        pattern_string = self.go_to_imm_global_pattern(address, index)
        self.execute_and_check_results(pattern_string)

    def DirectedGotoImmediateRelativeTest(self):
        """Captures CTV Data Vectors using GoTo Immediate - Relative Address

        **Test:**

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - six addresses equally spaced by 256
            - random index between 0-5 to chose from addresses above
        * Construct pattern string based on attributes
            - Every LOOP{N} call wil have as many CTVs as shown on its name (N)
            - Every LOOP{N} will exit burst with the end status equalling that on the name (N)
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Depends on random_index
            - CTV Data Objects: Depends on random_index + One End of Burst block per channel set
            - Jump/Call trace: 2 blocks (PCall, GoTo_I)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        """
        identifier = list(range(1024, 2500, 256))
        random_index = random.randint(0, len(identifier) - 1)
        ctp = 0b00000000000000000001001000000000010

        pattern_string = f'''
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

        PatternId 0x100
        PCall PATTERN1

        PATTERN1:
            AddressVectors length=1024

            Goto eval[int32(LOOP{identifier[random_index]}-vecaddr)], base=RELATIVE
            StopPattern 0x69dead69

            LOOP{identifier[0]}:
                AddressVectorsPlusAtt length={identifier[0]}
                StopPattern {identifier[0]}
            LOOP{identifier[1]}:
                AddressVectorsPlusAtt length={identifier[1]}
                StopPattern {identifier[1]}
            LOOP{identifier[2]}:
                AddressVectorsPlusAtt length={identifier[2]}
                StopPattern {identifier[2]}
            LOOP{identifier[3]}:
                AddressVectorsPlusAtt length={identifier[3]}
                StopPattern {identifier[3]}
            LOOP{identifier[4]}:
                AddressVectorsPlusAtt length={identifier[4]}
                StopPattern {identifier[4]}
            LOOP{identifier[5]}:
                AddressVectorsPlusAtt length={identifier[5]}
                StopPattern {identifier[5]}
        '''
        self.execute_and_check_results(pattern_string)

    def DirectedGotoRegisterGlobalTest(self):
        """Captures CTV Data Vectors using GoTo Register - Global

        **Test:**

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - six identifiers equally spaced by 256
            - random index between 0-5 to chose from addresses above
            - randnom register value between 0-31
        * Construct pattern string based on attributes
            - Store LOOP{N} in random register
            - GotoR
            - Every LOOP{N} call wil have as many CTVs as shown on its name (N)
            - Every LOOP{N} will exit burst with the end status
                equalling that on the name (N)
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Depends on random_index and corresponding
                identifier value
            - CTV Data Objects: Depends on random_index + One End of
                Burst block per channel set
            - Jump/Call trace: 2 blocks (PCall, GoTo_I)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks

        """
        identifier = list(range(1024, 2500, 256))
        random_index = random.randint(0, len(identifier) - 1)
        register = random.randint(0, 32)
        ctp = 0b00000000000000000001001000000000010

        pattern_string = f'''
        
        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

        PatternId 0x100
        PCall PATTERN1
        
        PATTERN1:
            AddressVectors length=1024
            SetRegister {register}, LOOP{identifier[random_index]}
            GotoR {register}

            StopPattern 0x69dead69

            LOOP{identifier[0]}:
                AddressVectorsPlusAtt length={identifier[0]}
                StopPattern {identifier[0]}
            LOOP{identifier[1]}:
                AddressVectorsPlusAtt length={identifier[1]}
                StopPattern {identifier[1]}
            LOOP{identifier[2]}:
                AddressVectorsPlusAtt length={identifier[2]}
                StopPattern {identifier[2]}
            LOOP{identifier[3]}:
                AddressVectorsPlusAtt length={identifier[3]}
                StopPattern {identifier[3]}
            LOOP{identifier[4]}:
                AddressVectorsPlusAtt length={identifier[4]}
                StopPattern {identifier[4]}
            LOOP{identifier[5]}:
                AddressVectorsPlusAtt length={identifier[5]}
                StopPattern {identifier[5]}
        '''
        self.execute_and_check_results(pattern_string)

    def DirectedGotoRegisterRelativeTest(self):
        """Captures CTV Data Vectors using GoTo Register - Relative

        **Test:**

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
            - six identifiers equally spaced by 256
            - random index between 0-5 to chose from addresses above
            - randnom register value between 0-31
        * Construct pattern string based on attributes
            - Store LOOP{N} in random register
            - GotoR Relative
            - Every LOOP{N} call wil have as many CTVs as shown on its name (N)
            - Every LOOP{N} will exit burst with the end status
                equalling that on the name (N)
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Depends on random_index and corresponding
                identifier value
            - CTV Data Objects: Depends on random_index + One End of
                Burst block per channel set
            - Jump/Call trace: 2 blocks (PCall, GoTo_R)
            - Error data + full error header for Pin Multiplier 0-3: Only
                End of Burst blocks

        """
        identifier = list(range(1024, 2500, 256))
        random_index = random.randint(0, len(identifier) - 1)
        register = random.randint(0, 32)
        ctp = 0b00000000000000000001001000000000010

        pattern_string = f'''

        PATTERN_START:
        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
        S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

        PatternId 0x100
        PCall PATTERN1

        PATTERN1:
            AddressVectors length=1024
            SetRegister {register}, eval[int32(LOOP{identifier[random_index]}-vecaddr-1)]
            GotoR {register}, base=RELATIVE
            
            StopPattern 0x69dead69

            LOOP{identifier[0]}:
                AddressVectorsPlusAtt length={identifier[0]}
                StopPattern {identifier[0]}
            LOOP{identifier[1]}:
                AddressVectorsPlusAtt length={identifier[1]}
                StopPattern {identifier[1]}
            LOOP{identifier[2]}:
                AddressVectorsPlusAtt length={identifier[2]}
                StopPattern {identifier[2]}
            LOOP{identifier[3]}:
                AddressVectorsPlusAtt length={identifier[3]}
                StopPattern {identifier[3]}
            LOOP{identifier[4]}:
                AddressVectorsPlusAtt length={identifier[4]}
                StopPattern {identifier[4]}
            LOOP{identifier[5]}:
                AddressVectorsPlusAtt length={identifier[5]}
                StopPattern {identifier[5]}
        '''
        self.execute_and_check_results(pattern_string)

    def DirectedBranchKeepPinTest(self):
        '''CTV vectors and Keep pins across different branches

        **Test:**

        * Set up test attributes:
            - set ctp mask to three pins
            - select random number of slices
        * Construct pattern string based on attributes
            - Using both PCall and Goto_I branch types, construct vectors with ctv active
            - Inject CTV vectors with Keep(K) pins at the beginning of all jumps besides the first PCall
             Having K pins should keep the previous branch CTV vector value.
        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice should match
                that of the simulator.
        '''
        pattern_string = f'''

            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections

            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x100
            PCall LOOP1

            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x200
            PCall LOOP2                

            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x300
            PCall LOOP3

            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x100
            PCall LOOP1

            S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
            PatternId 0x200
            PCall LOOP2                
            
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            LOOP1:
                V ctv=1, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLH
                %repeat 31
                V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                
                Return

            SUBR:
                %repeat 32
                V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                Return
                
            LOOP2:
                %repeat 63
                V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                V ctv=1, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLHL
                
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp}
                PatternId 0x300
                PCall LOOP3
            
                Return

            LOOP3:
                %repeat 128
                V ctv=1, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK
                %end
                Return
            '''

        self.execute_and_check_results(pattern_string=pattern_string)

    def simple_nest_pcall_global(self, callnum):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        pattern_string = f''' 
        PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} # ctp mask
            I optype=BRANCH, br=CLRSTACK

            PRE_RAND:
                ConstantVectors length=64, ctv=0, mtv=0, lrpt=0, data=0v00000000000000000000000000000000000

            I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm={callnum}
            PatternId 0x100
            I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[SUBR_1], cond=COND.NONE, invcond=0
            

            POST_RAND:
                ConstantVectors length=128, ctv=0, mtv=0, lrpt=0, data={vector_data}

            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1

            SUBR_1:
                AddressVectorsPlusAtt length=256, ctv=1
                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=1, dest=1, imm=1
                PatternId 0x200
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[SUBR_2], cond=ZERO, invcond=1
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[SUBR_1E], cond=ZERO, invcond=0
                I optype=BRANCH, br=RET

            SUBR_1E:
                DriveZeroVectors length=1024
                I optype=BRANCH, br=RET

            SUBR_2:
                AddressVectorsPlusAtt length=256, ctv=1
                I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, opdest=ALUDEST.REG, regA=1, dest=1, imm=1
                PatternId 0x300
                I optype=BRANCH, base=GLOBAL, br=PCALL_I, imm=eval[SUBR_1], cond=ZERO, invcond=1

            SUBR_2E:
                DriveZeroVectors length=128
                I optype=BRANCH, br=RET
        '''
        return pattern_string

    def recursive_pcall_i_pfb(self, callnum):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        p = RPG()
        p.StartPattern(ctp=self.ctp)
        p.ClearStack()
        p.AddBlock("PRE_RAND", 128, 0)
        p.SetRegFromImm(1, callnum)
        p.insert_line('''PatternId 100''')
        p.CallSubr("GLOBAL", "PCALL_I", "SUBR")
        p.AddBlock("POST_RAND", 100, 0x7ffffffff)
        p.EndPattern()
        lines = f'''
            SUBR:
                NoCompareDriveZVectors length={PADDING}
                AddressVectorsPlusAtt length=128, ctv=1
                I optype=BRANCH, br=RET
            '''
        p.insert_line(lines)
        p.AluRegImm("SUB", 1, 1)
        p.CallSubr("PFB", "PCALL_I", 0, "ZERO", "1",
                   pfb='eval[SUBR]')  # should call subr itself
        p.AddBlock("SUBRE", 256, vector_data)
        p.Return()
        return p.GetPattern()

    def simple_subr_head(self, branch_base, branch_type):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern(ctp=self.ctp)
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1 

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={0xdeadbeef}
                DriveZeroVectors length=1

                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
        '''
        p.insert_line(lines)
        p.Jump("GOTO_I", "eval[PRE_RAND]")
        lines = '''
        SUBR:
            AddressVectorsPlusAtt length=128, ctv=1
            I optype=BRANCH, br=RET
        '''
        p.insert_line(lines)
        p.AddBlock("PRE_RAND", 64, 0)
        p.CallSubr(branch_base, branch_type, "SUBR")
        p.insert_line('''Return''')
        return p.GetPattern()

    def simple_subr_tail(self, branch_base, branch_type):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern(ctp=self.ctp)
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1 

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                DriveZeroVectors length=1

                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
        '''
        p.insert_line(lines)
        p.AddBlock("PRE_RAND", 128, 0)
        p.CallSubr(branch_base, branch_type, "POST_RAND")
        lines = '''
                POST_RAND:
                    AddressVectorsPlusAtt length=128, ctv=1
                    I optype=BRANCH, br=RET
                '''
        p.insert_line(lines)
        p.insert_line('''Return''')
        p.AddSubroutine("SUBR", 64, 0)
        return p.GetPattern()

    def simple_subr_cfb(self, branch_base, branch_type, cfb_address):
        vector_data = random.randint(0x400000000,
                                     0x7ffffffff)
        id_0 = random.randint(0, 100)
        p_cfb = RPG()
        p_cfb.AddSubroutine("SUBR", 50, vector_data)
        pattern1 = p_cfb.GetPattern()

        p = RPG()
        p.StartPattern()
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1 

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef
                DriveZeroVectors length=1

                PATTERN1:
                NoCompareDriveZVectors length={PADDING}
        '''
        p.insert_line(lines)
        p.SetCFB(cfb_address)
        p.AddBlock("PRE_RAND", 150, 0)
        p.CallSubr(branch_base, branch_type, 0)
        p.AddBlock("POST_RAND", 100, 0x7ffffffff)
        p.insert_line('''Return''')
        pattern2 = p.GetPattern()

        print(pattern1, '\n', pattern2)
        return pattern1, pattern2

    def get_all_addresses(self):
        addressList = [0]
        # 0x8000,0000 is not valid
        for i in range(22):
            addressList.append(0x200 << i)
        addressList.append(0x70000000)
        patterns = {}
        id_0 = random.randint(0, 100)
        for i, addr in enumerate(addressList):
            addr = addressList[i]
            if i + 1 == len(addressList):
                p = RPG()
                p.StartPattern()
                lines = f'''
                        PatternId {id_0}                                                 
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                        DriveZeroVectors length=1

                        PATTERN1:
                        NoCompareDriveZVectors length={PADDING}
                '''
                p.insert_line(lines)
                p.AddBlock("PRE_RAND", 200, 0)
                p.SetRegFromImm(i, f'0xABCDEF{i}')
                p.insert_line('''Return''')
                pattern = p.GetPattern()
            else:
                p = RPG()
                id_0 = random.randint(0, 100)
                p.StartPattern()
                lines = f'''
                        PatternId {id_0}                                                 
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0x12345678
                        DriveZeroVectors length=1

                        PATTERN1:
                        NoCompareDriveZVectors length={PADDING}
                '''
                p.insert_line(lines)
                p.AddBlock("PRE_RAND", 100, 0)
                p.SetRegFromImm(i, f'0xABCDEF{i}')
                p.insert_line(f'Goto {addr}')
                p.insert_line('''Return''')
                pattern = p.GetPattern()
            patterns[addr] = pattern
        return patterns

    def go_to_imm_global_pattern(self, address, rand_jump):
        p = RPG()
        id_0 = random.randint(0, 100)
        p.StartPattern()
        lines = f'''
                PatternId {id_0}                                                 
                PCall PATTERN1
                PATTERN1:
        '''
        p.insert_line(lines)
        lines = f"""\
            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
            SUBR:
            AddressVectors length=1024
            Goto L{address[rand_jump]}
            StopPattern 0x69dead69
            L{address[0]}:
            AddressVectors length={address[0]}
            StopPattern {address[0]}
            L{address[1]}:
            AddressVectors length={address[1]}
            StopPattern {address[1]}
            L{address[2]}:
            AddressVectors length={address[2]}
            StopPattern {address[2]}
            L{address[3]}:
            AddressVectors length={address[3]}
            StopPattern {address[3]}
            L{address[4]}:
            AddressVectors length={address[4]}
            StopPattern {address[4]}
            L{address[5]}:
            AddressVectors length={address[5]}
            StopPattern {address[5]}
            """
        p.insert_line(lines)
        return p.GetPattern()

    def log_pattern(self, pattern):
        self.Log('debug', f'\n{"*"*100} \n{pattern} {"*"*100}')

    def execute_and_check_results(self, pattern_string, address_and_pattern_list={}):
        self.log_pattern(pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(
            pattern_string, self.fixed_drive_state,
            slices=self.slices,
            attributes={'capture_fails': 0},
            address_and_pattern_list=address_and_pattern_list)
        self.pattern_helper.execute_pattern_scenario()
