# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""This module focuses on tests with more variability, both in vector
composition/structure and pattern set up.

Tests range from nested pcalls to execution on a single channel set with varying
CTP mask

"""

import random
import time
from itertools import combinations

from Common.fval import skip, expected_failure, process_snapshot
from Hbicc.Tests.DCTriggerQueue import TQTestScenarioAD5684r, TQTestScenarioLTM4678_VCCIO
from Hbicc.Tests.HbiccTest import HbiccTest, PinStackDelay
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.capture_structs import CTV_HEADER_AND_DATA
from time import sleep
PADDING = 1024


class Functional(HbiccTest):
    """Various tests to validate unique corner cases

    All tests check for capture data integrity and alarm raised

    **Criteria: ** All tests within this class should complete with the following:

    - Correct Pattern End Status word
    - Correct capture data
    - No underrun issues
    - No critical alarms

    """

    def setUp(self, tester=None):
        super().setUp()
        self.rm_device = self.env.hbicc.ring_multiplier
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
        self.slices = range(5)
        # self.pattern_helper.is_simulator_active = True
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def tearDown(self):
        super().tearDown()
        self.pattern_helper.hbicc.calibrate_loopback_timing()

    def DirectedHighTraceCountAllSlicesAllChannleSetsCtp3Test(self):
        """Executes a high number of jumps.

        The capture data is less important then getting all Trace capture correctly.

        """
        fixed_drive_state = 'HIGH'
        id_0 = random.randint(0, 100)
        custom_slice_list = [0, 1, 2, 3, 4]
        repeat = 32
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                 PatternId {id_0}                                                 
                PCall PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                NoCompareDriveZVectors length={PADDING}

                Call P1
                Call P1
                Call P1
                Call P1
                Call P1
                Call P1

                P1:
                    PatternId 200 
                    PCall P2
                    Return
                    
                P2:
                    AddressVectorsPlusAtt length={repeat}
                    PatternId 300 
                    PCall P3
                    Return
                    
                P3:
                    AddressVectorsPlusAtt length={repeat}
                    PatternId 400 
                    PCall P4
                    Return
                    
                P4:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=custom_slice_list)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedValidPIDWithCtvInvalidPIDNoCtvTest(self):
        """Invalid PID validation.

        Test that TRACE capture stops after setting invalid PID.

        When PATTERN1 is called, two basic patterns run back to back. First PCall to PATTERN_WITH_VALID_PID has a
        valid pattern ID with CTV vectors and the second PCall to PATTERN_WITH_INVALID_PID one does not a
        valid PID (0xFFFFFFFF) or CTV vectors.

        """
        fixed_drive_state = 'HIGH'
        repeat = 32
        id_0 = random.randint(0, 100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0}   
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    NoCompareDriveZVectors length={PADDING}

                    PatternId 0x200
                    PCall PATTERN_WITH_VALID_PID

                    PatternId 0xFFFFFFFF
                    PCall PATTERN_WITH_INVALID_PID

                    Return


                PATTERN_WITH_VALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return

                PATTERN_WITH_INVALID_PID:
                    NoCompareDriveZVectors length={PADDING}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    @expected_failure('Lacks proper simulation')
    def DirectedValidPIDWithCtvInvalidPIDReturnThenCtvTest(self):
        """Invalid PID validation.

        Test that TRACE capture stops after setting invalid PID.

        When PATTERN1 is called, two basic patterns run back to back. First PCall to PATTERN_WITH_VALID_PID has a
        valid pattern ID with CTV vectors and the second PCall to PATTERN_WITH_INVALID_PID does not have a
        valid PID (0xFFFFFFFF) or CTV vector. Upon return to PATTERN1, CTV vectors get executed.

        """
        fixed_drive_state = 'HIGH'
        repeat = 32
        id_0 = random.randint(0,100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0}   
                PCall PATTERN1
                AddressVectorsPlusAtt length={repeat}

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    NoCompareDriveZVectors length={PADDING}

                    PatternId 0x200
                    PCall PATTERN_WITH_VALID_PID

                    PatternId 0xFFFFFFFF
                    PCall PATTERN_WITH_INVALID_PID

                    Return


                PATTERN_WITH_VALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return

                PATTERN_WITH_INVALID_PID:
                    NoCompareDriveZVectors length={PADDING}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedInvalidPIDValidPIDWithCtvTest(self):
        """Invalid PID validation.

        Test that TRACE capture stops after setting invalid PID and is reflected in CTV Headers.

        When PATTERN1 is called, two basic patterns run back to back. First PCall to PATTERN_WITH_INVALID_PID pattern
        does not have a valid PID (0xFFFFFFFF) but does include CTV vectors. Second PCall to PATTERN_WITH_VALID_PID
        has valid PID but no CTV vectors.

        """
        fixed_drive_state = 'HIGH'
        repeat = 32
        id_0 = random.randint(0,100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0}   
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    NoCompareDriveZVectors length={PADDING}
    
                    PatternId 0xFFFFFFFF
                    PCall PATTERN_WITH_INVALID_PID
    
                    PatternId 0x200
                    PCall PATTERN_WITH_VALID_PID
    
                    Return

                PATTERN_WITH_VALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return

                PATTERN_WITH_INVALID_PID:
                    NoCompareDriveZVectors length={PADDING}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    @expected_failure('Lacks proper simulation')
    def DirectedValidPIDWithCtvInvalidPIDWithCtvTest(self):
        """Invalid PID validation.

        Test that TRACE capture stops after invalid PID and is reflected in Pin Error Capture EOBs

        When PATTERN1 is called, two basic patterns run back to back. First PCall to PATTERN_WITH_VALID_PID pattern
        and CTV vectors. Second PCall to PATTERN_WITH_INVALID_PID does not have a valid PID (0xFFFFFFFF) but does have
        CTV vectors.

        """
        fixed_drive_state = 'HIGH'
        repeat = 32
        id_0 = random.randint(0,100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0}   
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    NoCompareDriveZVectors length={PADDING}
                    
                    PatternId 200
                    PCall PATTERN_WITH_VALID_PID
                    
                    PatternId 0xFFFFFFFF
                    PCall PATTERN_WITH_INVALID_PID
                    
                    Return

                PATTERN_WITH_VALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return

                PATTERN_WITH_INVALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    @expected_failure('Lacks proper simulation')
    def DirectedOneValidOneInvalidOneValidPIDAllWithCtvTest(self):
        """Invalid PID validation.

        Test that TRACE capture resumes after setting valid PID.

        When PATTERN1 is called, three basic patterns run back to back. First PCall to PATTERN_WITH_VALID_PID pattern
        and CTV vectors. Second PCall to PATTERN_WITH_INVALID_PID does not have a valid PID (0xFFFFFFFF) but does have
        CTV vectors. Last PCall to PATTERN_WITH_VALID_PID pattern does include CTV vectors.

        """
        fixed_drive_state = 'HIGH'
        repeat = 32
        id_0 = random.randint(0,100)
        ctp = 0b00000000000000000001001000000000010
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={ctp} #ctp mask

                PatternId {id_0}   
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                    NoCompareDriveZVectors length={PADDING}
    
                    PatternId 0x200
                    PCall PATTERN_WITH_VALID_PID
                    
                    PatternId 0xFFFFFFFF
                    PCall PATTERN_WITH_INVALID_PID
                    
                    PatternId 0x400
                    PCall PATTERN_WITH_VALID_PID2
                    Return

                PATTERN_WITH_VALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return

                PATTERN_WITH_VALID_PID2:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                    
                PATTERN_WITH_INVALID_PID:
                    AddressVectorsPlusAtt length={repeat}
                    Return
                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, fixed_drive_state, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    #TODO: fix flush bit checks. 
    def _check_flush_bits(self):
        for slice in self.slices:
            register = self.env.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.CAPTURE_CONTROL,
                                                                  slice=slice)
            if register.flush_captures_complete:
                self.Log('warning', f'Capture Control: flush_captures_complete was not set. ')

            if register.flush_buffer:
                self.Log('warning', f'Capture Control: flush_buffer was not set. ')

    def contruct_expected_ctv_data(self, custom_slice_list, pattern_helper, repeat, ctp, special=False):
        ctp_list = self.env.hbicc.get_active_pins_from_ctp(ctp)
        ctp_len = len(ctp_list)
        for slice in custom_slice_list:
            ctv_data = {}
            for pm in self.env.hbicc.get_pin_multipliers():
                start = 0
                ctv_data.update({pm.slot_index: []})
                for i in range(start, repeat, 8):
                    one_ctv = CTV_HEADER_AND_DATA()
                    one_ctv.block_count = ctp_len
                    one_ctv.relative_cycle_count = i & 0b111111111
                    one_ctv.valid_cycles = 0x11111111
                    one_ctv.entry_is_wide = 0x000000FF
                    one_ctv.end_of_burst = 0
                    one_ctv.is_cycle_count = 0
                    one_ctv.originating_slice = slice
                    one_ctv.originating_chip = pm.slot_index
                    one_ctv.capture_word_type = 1
                    self.get_packets(i, ctp_len, one_ctv, pm.slot_index)
                    ctv_data[pm.slot_index].append(one_ctv)
                offset = 10
                one_ctv = CTV_HEADER_AND_DATA()
                one_ctv.block_count = 0
                one_ctv.relative_cycle_count = (i + offset) & 0b11111111111
                one_ctv.valid_cycles = 0x00000000
                one_ctv.entry_is_wide = 0x00000000
                one_ctv.end_of_burst = 1
                one_ctv.is_cycle_count = 0
                one_ctv.originating_slice = slice
                one_ctv.originating_chip = pm.slot_index
                one_ctv.capture_word_type = 1
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                one_ctv.packet = 0x0000000000000000
                ctv_data[pm.slot_index].append(one_ctv)
            self.pattern_helper.set_expected_ctv_data(ctv_data, [slice])

    def get_packets(self, repeat, ctp_len, one_ctv, pm):
        if repeat % 32 == 0:
            if ctp_len == 1:
                one_ctv.packet1 = 0x55FF55FF55FF55FF
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0x55FF55FF55FF55FF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0x55FF55FF55FF55FF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0x55555555FFFFFFFF
        if repeat % 32 == 8:
            if ctp_len == 1:
                one_ctv.packet1 = 0xFF55FF55FF55FF55
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0xFF55FF55FF55FF55
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0xFF55FF55FF55FF55
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0xFFFFFFFF55555555
        if repeat % 32 == 16:
            if ctp_len == 1:
                one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = 0xFFFF5555FFFF5555
                one_ctv.packet3 = 0xFFFFFFFF55555555
        if repeat % 32 == 24:
            if ctp_len == 1:
                one_ctv.packet1 = 0xFFFF55555555FFFF
                one_ctv.packet2 = 0x0
                one_ctv.packet3 = 0x0
            elif ctp_len == 2:
                one_ctv.packet1 = 0xFFFF55555555FFFF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0x0
            elif ctp_len == 3:
                one_ctv.packet1 = 0xFFFF55555555FFFF
                one_ctv.packet2 = 0x5555FFFF5555FFFF
                one_ctv.packet3 = 0xFFFFFFFFFFFFFFFF
        if pm == 0 or pm == 2:
            pass
        elif pm == 1 or pm == 3:
            if ctp_len == 1:
                one_ctv.packet1 = one_ctv.packet1 ^ 0xFFFFFFFFFFFFFFFF
            elif ctp_len == 2:
                one_ctv.packet1 = one_ctv.packet1 ^ 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = one_ctv.packet2 ^ 0xFFFFFFFFFFFFFFFF
            elif ctp_len == 3:
                one_ctv.packet1 = one_ctv.packet1 ^ 0xFFFFFFFFFFFFFFFF
                one_ctv.packet2 = one_ctv.packet2 ^ 0xFFFFFFFFFFFFFFFF
                one_ctv.packet3 = one_ctv.packet3 ^ 0xFFFFFFFFFFFFFFFF

    def RandomCtpOneChannelSetTest(self, TEST_ITERATIONS=10):
        """Generate CTV Data with Random Number of Ctp and One channel set

        **Test:**

        1. Construct pattern string
            - Randomize ctp mask
            - Randomize channel set
            - Construct pattern with randomized attributes

        2. Set up tester for pattern execution (see PatternHelper class for more info)
        3. Execute pattern
        4. Process data capture results
        5. Repead 1-4 a total of 10 times.

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 32 blocks
            - CTV Data Objects: Dependent on randomization + One End of Burst block per channel set
            - Jump/Call trace: 2 blocks (PCall, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        **NOTE** Excluding slice 4 due to phylite pins. Enable  it once simulator implements it.
        """
        self.slices = list(range(4))
        for i in range(TEST_ITERATIONS):
            channel_set = random.randint(0, 15)
            with self.subTest(Iteration=f' {i}'):
                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                        S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                        S stype=DUT_SERIAL, channel_set={channel_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                        S stype=CAPTURE_PIN_MASK, data={self.ctp}
                        PatternId 0x100 
                        PCall LOOP

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        LOOP:
                            AddressVectorsPlusAtt length=32
                            Return
                        '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def DirectedWalkingCtpOneChannelSetTest(self, TEST_ITERATIONS=10):
        """Walking CTV vectors across all 35 pins using Pcall and CTP Mask

        **Test:**

        * Construct pattern string
            - One pin set includes ctp mask, pattern id, and pcall to LOOP
            - Write 35 sets, one for each pin
            - LOOP contains a random number of consecutive CTV vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 1120 blocks
            - CTV Data Objects: 140 blocks + One End of Burst block per channel set
            - Jump/Call trace: 35 blocks, one for each pin
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        **NOTE** Excluding slice 4 due to phylite pins. Enable  it once simulator implements it.
        """
        self.slices = list(range(4))

        for i in range(TEST_ITERATIONS):
            channel_set = random.randint(0, 15)
            with self.subTest(Iteration=f' {i}'):
                body = self.construct_pcall_to_every_pin()

                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                        S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                        S stype=DUT_SERIAL, channel_set={channel_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                        {body}

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        LOOP:
                            AddressVectorsPlusAtt length=32
                            Return
                        '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def RandomCtpAllChannelSetTest(self, TEST_ITERATIONS=10):
        """Generate CTV Data with Random Number of Ctp and All channel set

        **Test:**

        1. Construct pattern string
            - Randomize ctp mask
            - All channel sets
            - Construct pattern with randomized attributes

        2. Set up tester for pattern execution (see PatternHelper class for more info)
        3. Execute pattern
        4. Process data capture results
        5. Repead 1-4 a total of 10 times.

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: 32 blocks
            - CTV Data Objects: Dependent on randomization + One End of Burst block per channel set
            - Jump/Call trace: 2 blocks (PCall, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        **NOTE** Excluding slice 4 due to phylite pins. Enable  it once simulator implements it.
        """
        self.slices = list(range(4))
        for i in range(TEST_ITERATIONS):
            self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
            with self.subTest(Iteration=f' {i}'):
                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                        S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                        S stype=CAPTURE_PIN_MASK, data={self.ctp}
                        PatternId 0x100 
                        PCall LOOP

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        LOOP:
                            AddressVectorsPlusAtt length=32
                            Return
                        '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def DirectedWalkingCtpAllChannelSetsTest(self, TEST_ITERATIONS=5):
        """Walking CTV vectors across all 35 pins using Pcall and CTP Mask

        **Test:**

        * Construct pattern string
            - One pin set includes ctp mask, pattern id, and pcall to LOOP
            - Write 35 sets, one for each pin
            - LOOP contains a random number of consecutive CTV vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Dependent on randomization
            - CTV Data Objects: Dependent on randomization + One End of Burst block per channel set
            - Jump/Call trace: 35 blocks, one for each pin
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        **NOTE** Excluding slice 4 due to phylite pins. Enable  it once simulator implements it.
        """
        self.slices = [0, 1, 2, 3]

        for i in range(TEST_ITERATIONS):
            with self.subTest(Iteration=f' {i}'):
                body = self.construct_pcall_to_every_pin()

                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF 

                        {body}

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        LOOP:
                            AddressVectorsPlusAtt length={random.randint(32, 64)}
                            Return
                        '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

    def RandomSuperRPGZLowTest(self):
        """Random patterns with CTV and Walking Vref from 0.7 to 1.7

        **Test:**

        * Construct pattern string per iteration
            - Random channel set
            - Random ctp mask
            - Set Vref to equal 1.7 - (0.1 * iteration)
            - LOOP contains a random number of consecutive CTV
                vectors between 40-200

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Dependent on randomization
            - CTV Data Objects: Dependent on randomization + One End of Burst block per channel set
            - Jump/Call trace: 2 blocks (Pcall, Return)
            - Error data + full error header for Pin Multiplier 0-3: Only End of Burst blocks

        **NOTE** Excluding slice 4 due to phylite pins. Enable  it once simulator implements it.

        """
        self.enable_aurora_bus_and_reset_power_state()
        self.switch_to_dctq_sync_delay_mode()

        self.slices = list(range(4))

        for vref in [x / 10 for x in range(7, 18)]:
            self.ctp = random.randint(0, 0b11111111111111111111111111111111111)
            channel_set = random.choice(list(range(16)) + [32])
            repeat = random.randint(40, 200)

            with self.subTest(vref=vref):
                self._set_vref_vccio(vref=vref)
                pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,             data=0o77777777777777777777777777777777777 
                        S stype=EC_RESET,                    data=0b11111111111111111111111111111111111 
                        S stype=DUT_SERIAL, channel_set={channel_set},   data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                        S stype=CAPTURE_PIN_MASK, data={self.ctp}
                        PatternId 0x100 
                        PCall LOOP

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                        DriveZeroVectors length=1

                        LOOP:
                            AddressVectorsPlusAtt length={repeat}
                            Return
                        '''

                self.Log('debug', pattern_string)
                self.pattern_helper.create_slice_channel_set_pattern_combo(
                    pattern_string, slices=self.slices)
                self.pattern_helper.execute_pattern_scenario()

        self._set_vref_vccio()

    def DirectedDriveOneCompareLowOddToEvenExternalLoopbackCtvTest(self):
        """CTV Capture with External Loopback - Drive High Compare Low

        **Test:**

        * Construct pattern string containing one PCall having:
            - Drive Z vectors
            - Constant vectors where one pin drives and its pair pin compares
                - Drive 1, Compare Low. Failures expected.
            - Drive Z vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Depends on repeat_iteration value
            - CTV Data Objects: Depends on repeat_iteration value + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks
        """
        with PinStackDelay(self.env.hbicc, rx_delay=64):
            self.pattern_helper.user_mode = 'RELEASE'
            id_0 = random.randint(0, 100)
            repeat_iteration = random.randint(1, 1000)
            attributes = {'capture_fails': 0,
                          'capture_ctv': 1,
                          'capture_all': 0}

            pattern_string = f'''
    
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
    
                S stype=CAPTURE_PIN_MASK, data={self.ctp}
                PatternId {id_0}
                PCall PATTERN1
    
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1
    
                PATTERN1:
                    NoCompareDriveZVectors length=100
                    ConstantVectors length={repeat_iteration}, ctv=1, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
                    NoCompareDriveZVectors length=100
    
                    Return
                '''

            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0], attributes=attributes)
            self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveZeroCompareHighwOddToEvenExternalLoopbackCtvTest(self):
        """CTV Capture with External Loopback - Drive Low Compare High

        **Test:**

        * Construct pattern string containing one PCall having:
            - Drive Z vectors
            - Constant vectors where one pin drives and its pair pin compares
                - Drive 0, Compare High. Failures expected.
            - Drive Z vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: Depends on repeat_iteration value
            - CTV Data Objects: Depends on repeat_iteration value + One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks
        """
        with PinStackDelay(self.env.hbicc, rx_delay=64):
            self.pattern_helper.user_mode = 'RELEASE'
            id_0 = random.randint(0, 100)
            repeat_iteration = random.randint(1, 1000)
            attributes = {'capture_fails': 0,
                          'capture_ctv': 1,
                          'capture_all': 0}
            pattern_string = f'''
    
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    
                    S stype=CAPTURE_PIN_MASK, data={self.ctp}
                    PatternId {id_0}
                    PCall PATTERN1
    
                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                    DriveZeroVectors length=1
    
                    PATTERN1:
                        NoCompareDriveZVectors length=100
                        ConstantVectors length={repeat_iteration}, ctv=1, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                        NoCompareDriveZVectors length=100
    
                        Return
                      '''

            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=[0], attributes=attributes)
            self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveOneCompareHighEvenToOddExternalLoopbackCtvTest(self):
        """CTV Capture with External Loopback - Drive High Compare High

        **Test:**

        * Construct pattern string containing one PCall having:
            - Drive Z vectors
            - Constant vectors where one pin drives and its pair pin compares
                - Drive 1, Compare High. No failures expected.
            - Drive Z vectors

        * Set up tester for pattern execution (see PatternHelper class for more info)
        * Execute pattern
        * Process data capture results

        **Criteria:** The expected result for each data stream per slice is as follows:

            - Full CTV Headers: None
            - CTV Data Objects: One End of Burst block per channel set
            - Jump/Call trace: 1 Pcall block,  1 Return block
            - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks
        """
        with PinStackDelay(self.env.hbicc, rx_delay=64):
            self.pattern_helper.user_mode = 'RELEASE'
            id_0 = random.randint(0, 100)
            repeat_iteration = random.randint(1, 1000)
            self.slices = range(0, 4)
            attributes = {'capture_fails': 0,
                          'capture_ctv': 1,
                          'capture_all': 0}

            pattern_string = f'''
    
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {id_0}
                PCall PATTERN1
    
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
    
                PATTERN1:
                    NoCompareDriveZVectors length=100
                    ConstantVectors length={repeat_iteration}, ctv=1, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    NoCompareDriveZVectors length=100
        
                    Return
              '''

            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                       attributes=attributes)
            self.pattern_helper.execute_pattern_scenario()

    def DirectedxDriveOneCompareHighEvenToOddExternalLoopbackCaptureAllTest(self):
        """Capture All with External Loopback - Drive High Compare High

            **Test:**

            * Construct pattern string containing one PCall having:
                - Constant vectors where one pin drives and its pair pin compares
                    - Drive 1, Compare High. No failures expected.
                    - CTV bit is set
                - Constant vectors where one pin drives and its pair pin compares
                    - Drive 1, Compare High. No failures expected.
                    - CTV bit is not set

            * Set up Pin Rx stack delay to 64
            * Set up tester for pattern execution (see PatternHelper class for more info)
            * Execute pattern
            * Process data capture results

            **Criteria:** The expected result for each data stream per slice is as follows:

                - Full CTV Headers: 64 blocks
                - CTV Data Objects: 9 blocks per 8 cycles totaling 72 blocks +
                        One End of Burst block per channel set
                - Jump/Call trace: 1 Pcall block,  1 Return block
                - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        # equation = (subcycle=5/8) * 58 + 2.66 ns
        """
        with PinStackDelay(self.env.hbicc, rx_delay=64):
            self.ctp = 0b01111111111111111111111111111111111
            self.slices = [0]
            self.pattern_helper.user_mode = 'RELEASE'
            id_0 = random.randint(0, 100)

            attributes = {'capture_fails': 0,
                          'capture_ctv': 0,
                          'capture_all': 1}

            pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
    
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {id_0}
                PCall PATTERN1
    
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=1
    
                PATTERN1:
                    ConstantVectors length=32, ctv=1, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    ConstantVectors length=32, ctv=0, data=0v1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1H1
                    Return
              '''
            self.Log('debug', pattern_string)
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                       attributes=attributes)
            self.pattern_helper.execute_pattern_scenario()


    def DirectedDriveOneCompareLowOddToEvenExternalLoopbackCaptureAllTest(self):
        """Capture All with External Loopback - Drive High Compare High

            **Test:**

            * Construct pattern string containing one PCall having:
                - Constant vectors where one pin drives and its pair pin compares
                    - Compare Low, Drive High. Failures expected.
                    - CTV bit is set
                - Constant vectors where one pin drives and its pair pin compares
                    - Compare Low, Drive High. Failures expected.
                    - CTV bit is not set

            * Set up Pin Rx stack delay to 64
            * Set up tester for pattern execution (see PatternHelper class for more info)
            * Execute pattern
            * Process data capture results

            **Criteria:** The expected result for each data stream per slice is as follows:

                - Full CTV Headers: depending on randomization
                - CTV Data Objects: depending on randomization +
                        One End of Burst block per channel set
                - Jump/Call trace: 1 Pcall block,  1 Return block
                - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        # equation = (subcycle=5/8) * 58 + 2.66 ns
        """
        with PinStackDelay(self.env.hbicc, rx_delay=64):
            self.ctp = 0b01111111111111111111111111111111111
            self.slices = [0]
            self.pattern_helper.user_mode = 'RELEASE'
            id_0 = random.randint(0, 100)

            attributes = {'capture_fails': 0,
                          'capture_ctv': 0,
                          'capture_all': 1}

            pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
    
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            PatternId {id_0}
            PCall PATTERN1
    
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1
    
            PATTERN1:
                ConstantVectors length={random.randint(32, 128)}, ctv=0, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
                ConstantVectors length={random.randint(32, 128)}, ctv=1, data=0vL1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L1L
    
                Return
            '''
            self.Log('debug', pattern_string)
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                       attributes=attributes)
            self.pattern_helper.execute_pattern_scenario()

    def DirectedDriveZeroCompareHighwOddToEvenExternalLoopbackCaptureAllTest(self):
        """Capture All with External Loopback - Drive Low Compare High

            **Test:**

            * Construct pattern string containing one PCall having:
                - Constant vectors where one pin drives and its pair pin compares
                    - Drive Low, Compare High. Failures expected.
                    - CTV bit is set
                - Constant vectors where one pin drives and its pair pin compares
                    - Drive Low, Compare High. Failures expected.
                    - CTV bit is not set

            * Set up Pin Rx stack delay to 64
            * Set up tester for pattern execution (see PatternHelper class for more info)
            * Execute pattern
            * Process data capture results

            **Criteria:** The expected result for each data stream per slice is as follows:

                - Full CTV Headers: depending on randomization
                - CTV Data Objects: depending on randomization +
                        One End of Burst block per channel set
                - Jump/Call trace: 1 Pcall block,  1 Return block
                - Error data + full error header for Pin Multiplier 0-3: End of Burst blocks

        # equation = (subcycle=5/8) * 58 + 2.66 ns
        """
        with PinStackDelay(self.env.hbicc, rx_delay=64):
            self.ctp = 0b01111111111111111111111111111111111
            self.slices = [0]
            self.pattern_helper.user_mode = 'RELEASE'
            id_0 = random.randint(0, 100)

            attributes = {'capture_fails': 0,
                          'capture_ctv': 0,
                          'capture_all': 1}

            pattern_string = f'''
            PATTERN_START:
            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
    
            S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
            PatternId {id_0}
            PCall PATTERN1
    
            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
            DriveZeroVectors length=1
    
            PATTERN1:
                ConstantVectors length={random.randint(32, 128)}, ctv=0, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                ConstantVectors length={random.randint(32, 128)}, ctv=1, data=0v0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0H0
                
                Return
            '''
            self.Log('debug', pattern_string)
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                       attributes=attributes)
            self.pattern_helper.execute_pattern_scenario()


    def _set_vref_vccio(self, vccio=1.8, vref=0.9):
        self.vccio_vref_devices_init(psdb=0)
        self.vccio_vref_value_set(vout=vccio, vref=vref, psdb=0)
        vccio_setting = 0.3
        time.sleep(vccio_setting)
        self.vccio_vref_value_check(vout=vccio, vref=vref)

    def construct_pcall_to_every_pin(self):
        body = f''''''
        for pin in range(35):
            ctp = 1 << pin
            body += f'''
                        S stype=CAPTURE_PIN_MASK, data={ctp}
                        PatternId {pin} 
                        PCall LOOP

                    '''
        return body

    def vccio_vref_devices_init(self, psdb):
        self.tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        self.tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        self.tq_test_ad5684r.device_init()
        self.tq_test_ltm4678.device_init()
        self.tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
        self.tq_test_ltm4678.set_psdb_target(psdb_target=psdb)

    def vccio_vref_value_set(self, vout, vref, psdb):
        vccio_tq = self.tq_test_ltm4678.vccio_set_tq_content(uv=0, ov=2, vout=vout, psdb=psdb)
        delay_tq = [self.tq_test_ltm4678.delay_trigger_queue_record(11)]
        vref_tq_initial = self.tq_test_ad5684r.vref_set_tq_content(vref=0.5, psdb=psdb)
        vref_tq_final = self.tq_test_ad5684r.vref_set_tq_content(vref=vref, psdb=psdb)
        tq_list = vref_tq_initial + vccio_tq + delay_tq + vref_tq_final
        self.tq_test_ltm4678.combine_tq_content_list(tq_list)
        self.tq_test_ltm4678.send_vccio_vref_broadcast_dma()

    def enable_aurora_bus_and_reset_power_state(self):
        self.enable_aurora_bus(1)
        self.reset_power_state()

    def reset_power_state(self):
        self.rm_device.write_dc_trigger_dut_power_state(0, 0)
        time.sleep(0.1)

    def enable_aurora_bus(self, enable=1):
        self.rm_device.write_dc_trigger_enable_aurora_data(enable)
        time.sleep(0.1)

    def switch_to_dctq_sync_delay_mode(self):
        self.rm_device.switch_dctq_sync_delay_mode(syncdelay_mode=1)

    def vccio_vref_value_check(self, vout, vref):
        self.tq_test_ltm4678.check_vccio_voltage_val(vout)
        self.tq_test_ad5684r.check_vref_voltage_val(vref)