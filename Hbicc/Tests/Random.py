# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Randomize all pattern content to generate arbitrary interactions

Try to create interactions between features that won't be covered
by more targeted tests.
"""
import random
from Common.fval import skip

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.rpg import RPG
from Hbicc.testbench.assembler import HbiPatternAssembler
from Hbicc.testbench.PatternUtility import PatternHelper
from Common.fval import skip


class Random(HbiccTest):
    """Use the Random Pattern Generator (RPG) to create PList scenarios"""

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.slices = [0, 1, 2, 3, 4]
        self.hbicc = self.env.hbicc
        self.pattern_helper.end_status= random.randint(0, 0xFFFFFFFF)

    def RandomSuperRPGTest(self):
        """Random PList patterns with randomized vectors, compression, and instructions"""
        constraints = {'NUM_PAT': [5, 300], 'LEN_PAT': [1000, 2000], 'CLINK': 0.01, 'LRPT': 0.01, 'CTV': 0.1,
                       'RPT': 0.002, 'MTV': 0.01, 'PMASK': True, 'Meta': 0.01, 'ALU': 0.01}
        self._run_pattern(constraints)

    def RandomPlistManyPatternTest(self):
        """30-50 patterns ranging from 1000-2000 vectors each

        Copied from HPCC EndUser.RandomPlist
        """
        constraints = {'NUM_PAT': [30, 50], 'LEN_PAT': [1000, 2000]}

        self._run_pattern(constraints)

    def _run_pattern(self, constraints):
        pattern = HbiPatternAssembler()
        p = RPG(constraints=constraints)
        p.GeneratePlist('ADDRESS',end_status=self.pattern_helper.end_status)
        pattern.LoadString(p.GetPattern())
        self.pattern_helper.create_slice_channel_set_pattern_combo(p.GetPattern(), fixed_drive_state='HIGH',
                                                                   slices=self.slices,
                                                                   attributes={'capture_fails': 0, 'capture_ctv': 0, })
        for slice in self.slices:
            self.pattern_helper.slice_channel_sets_combo[slice].pattern_start_address = pattern.Resolve(
                'eval[PATTERN_START]')
        self.pattern_helper.execute_pattern_scenario()

    def RandomPlistLongPatternTest(self):
        """30-50 patterns ranging from 100,000-200,000 vectors each

        Copied from HPCC EndUser.RandomPlist
        """
        constraints = {'NUM_PAT': [30, 50], 'LEN_PAT': [10000, 20000]}
        self._run_pattern(constraints)