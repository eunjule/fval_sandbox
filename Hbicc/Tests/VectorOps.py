# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Vector Operations are a specific type of instruction operation meant for events that block pattern execution flow.

Operations Supported in HBICC and covered with tests in this module:
log1_i - Update log1 special register using immediate field. The data from this is included in capture header.
log1 - Update log1 special register using register. The data from this is included in capture header.
pattern_inst_i - Update pattern instance special register using immediate field. This updates the pattern id.
pattern_inst_r - Update pattern instance special register using register. This updates the pattern id.
repeat_i - Instruction repeat operation to repeat the next vector the amount of times specified in immediate field.
repeat_r - Instruction repeat operation to repeat the next vector the amount of times specified in immediate field.
end_i - End pattern with status code in immediate field.
end_r - End pattern with status code from register.
capture_i - Global Capture Enable / Disable based on immediate field.
reset_cycle_count - Reset the user counter special register.
reset_fail_counts - Reset total fail count and pin fail counters via internal PVC.

HBICC FPGA HLD also mentions other operations which are either not relevant in HBICC or not supported -
Trigger SIB - Not used
Blkfail_i - Not supported
DrainPinFIFO_i / DrainPinFIFO_r - Supported but not used
Edgecntr_i - Not used
ClearStickyError / ClearDomainStickyError / ClearGlobalStickyError - Not used
"""

import random

from Common.fval import skip, expected_failure
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

SLICES = range(0, 5)
LOCAL_REPEAT_MIN = 0
LOCAL_REPEAT_MAX = 31
PATT_LENGTH_MIN = 1
PATT_LENGTH_MAX = 5
REG_ID_MIN = 0
REG_ID_MAX = 31
PADDING= 1024

class Log(HbiccTest):
    """Feature tests using Vector Operations Instructions of VP type LOG.

    Notes:
    1. Channel Linking is NOT supported in HBICC. So channel compression based user log tests are removed.
    """
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.slices = SLICES
        self.id0= random.randint(1,100)
        self.pattern_helper.is_simulator_active = True
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def DirectedUserLog1ImmediateTest(self):
        """Verify write to User Log1 via Immediate Field in Capture Header"""
        random_value_for_user_log1 = random.randint(0x00000000, 0xffffffff)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                UserLog1_I {hex(random_value_for_user_log1)}
                                DriveZeroVectors length=32, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()





    def DirectedUserLog1ViaRegisterATest(self):
        """Verify write to User Log1 via Register A in Capture Header"""
        random_value_for_user_log1_via_regA = random.randint(0x00000000, 0xffffffff)
        random_reg_id = random.randint(0, 31)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                SetRegister {random_reg_id}, {hex(random_value_for_user_log1_via_regA)}
                                UserLog1_R {random_reg_id}
                                DriveZeroVectors length=32, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedUserLog1ImmediateAndViaRegisterATest(self):
        """Verify write to User Log1 via Immediate Field and Register A in Capture Header"""
        random_value_for_user_log1 = random.randint(0x00000000, 0xffffffff)
        random_value_for_user_log1_via_regA = random.randint(0x00000000, 0xffffffff)
        random_reg_id = random.randint(0, 31)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                UserLog1_I {hex(random_value_for_user_log1)}
                                DriveZeroVectors length=16, ctv=1
                                SetRegister {random_reg_id}, {hex(random_value_for_user_log1_via_regA)}
                                UserLog1_R {random_reg_id}
                                DriveZeroVectors length=16, ctv=1
                                UserLog1_I {hex(random_value_for_user_log1)}
                                DriveZeroVectors length=16, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPatternInstanceRegisterImmediateTest(self):
        """Verify PatternId setting via Immediate Field in Trace Header"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
    
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                DriveZeroVectors length = 1
                                DriveHighVectors length = 32
                                PatternId {hex(random_patt_id)}
                                PCall `PATTERN1`
                                Return
    
                            PATTERN1:
                                CompareHighVectors length=32
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPatternInstanceRegisterViaRegisterATest(self):
        """Verify PatternId setting via Register A in Trace Header"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_reg_id = random.randint(0, 31)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                DriveZeroVectors length = 1
                                DriveHighVectors length = 32
                                SetRegister {random_reg_id}, {hex(random_patt_id)}
                                PatternId_R {random_reg_id}
                                PCall `PATTERN1`
                                Return

                            PATTERN1:
                                CompareHighVectors length=32
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPatternInstanceRegisterImmediateAndViaRegisterATest(self):
        """Verify PatternId setting via Immediate Field and Register A in Trace Header"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_patt_id_via_regA = random.randint(0x00000000, 0xfffffffe)
        random_reg_id = random.randint(0, 31)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                DriveZeroVectors length = 1
                                DriveHighVectors length = 32
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN1
                                Return

                            PATTERN1:
                                CompareHighVectors length=32
                                SetRegister {random_reg_id}, {hex(random_patt_id_via_regA)}
                                PatternId_R {random_reg_id}
                                PCall PATTERN2
                                Return
                            
                            PATTERN2:
                                DriveZeroVectors length=16
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPatternIdAndUserLogTest(self):
        """Verify UserLog1 and PatternId setting via Immediate Field and Register A in Capture and Trace Headers"""
        random_value_for_user_log1 = random.randint(0x00000000, 0xffffffff)
        random_value_for_user_log1_via_regA = random.randint(0x00000000, 0xffffffff)
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_patt_id_via_regA = random.randint(0x00000000, 0xfffffffe)
        random_reg_id = random.randint(0, 31)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                DriveHighVectors length=32
                                SetRegister {random_reg_id}, {hex(random_patt_id_via_regA)}
                                PatternId_R {random_reg_id}
                                PCall PATTERN1
                                Return

                            PATTERN1:
                                SetRegister {random_reg_id}, {hex(random_value_for_user_log1_via_regA)}
                                UserLog1_R {random_reg_id}
                                CompareHighVectors length=32, ctv=1
                                DriveHighVectors length=32
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN2
                                Return

                            PATTERN2:
                                UserLog1_I {hex(random_value_for_user_log1)}
                                DriveZeroVectors length=16, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedPatternIDUsrLogLrptTest(self):
        """Verify UserLog1 and PatternId setting via Immediate Field in the presence of vector local repeats

        Note:
        1. Checking local repeats by getting 8 random valid values from 0 to 31 for a single instruction.
        2. Local repeat is combined with pattern length (which also creates the same pattern instruction multiple times)
        Certain combinations of the above lead to Repeat Count and Pattern Cycle issues as reported in the TFS ticket.
        """
        random_value_for_user_log1 = random.randint(0x00000000, 0xffffffff)
        random_value_for_user_log1_via_regA = random.randint(0x00000000, 0xffffffff)
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_patt_id_via_regA = random.randint(0x00000000, 0xfffffffe)
        local_repeats = random.choices(range(0, 31), k=8)
        for local_repeat in local_repeats:
            for patt_length in range(PATT_LENGTH_MIN, PATT_LENGTH_MAX + 1):
                with self.subTest(Params=f'LRPT: {local_repeat} Pattern Len: {patt_length}'):
                    random_reg_id = random.randint(REG_ID_MIN, REG_ID_MAX)
                    self.pattern_helper.user_mode = 'HIGH'
                    pattern_string = f'''
                                        PATTERN_START:
                                            StartPattern
                                            S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                            PatternId {hex(random_patt_id + 1)}
                                            PCall PATTERN0
                                            StopPattern {hex(self.end_status)}
                                        
                                        PATTERN0:
                                            NoCompareDriveZVectors length={PADDING}
                                            DriveHighVectors length=32
                                            SetRegister {random_reg_id}, {hex(random_patt_id_via_regA)}
                                            PatternId_R {random_reg_id}
                                            PCall PATTERN1
                                            Return
                                        
                                        PATTERN1:
                                            SetRegister {random_reg_id}, {hex(random_value_for_user_log1_via_regA)}
                                            UserLog1_R {random_reg_id}
                                            CompareLowVectors lrpt={local_repeat}, length={patt_length}, ctv=1
                                            DriveHighVectors length=32
                                            PatternId {hex(random_patt_id)}
                                            PCall PATTERN2
                                            Return
                                        
                                        PATTERN2:
                                            UserLog1_I {hex(random_value_for_user_log1)}
                                            DriveZeroVectors length=16, ctv=1
                                            Return
                                        '''
                    self.Log('debug', pattern_string)
                    self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                               attributes={'capture_fails': 1,
                                                                                           'capture_all': 0,
                                                                                           'capture_ctv': 1})
                    self.pattern_helper.execute_pattern_scenario()

    def DirectedPatternIDUsrLogIrptTest(self):
        """Verify UserLog1 and PatternId setting via Immediate Field in the presence of vector instruction repeats"""
        random_value_for_user_log1 = random.randint(0x00000000, 0xffffffff)
        random_value_for_user_log1_via_regA = random.randint(0x00000000, 0xffffffff)
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_patt_id_via_regA = random.randint(0x00000000, 0xfffffffe)
        random_reg_id = random.randint(0, 31)
        random_instruction_repeat_count = random.randint(32, 512)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask
                                PatternId {self.id0}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                DriveHighVectors length=32
                                SetRegister {random_reg_id}, {hex(random_patt_id_via_regA)}
                                PatternId_R {random_reg_id}
                                PCall PATTERN1
                                Return
                            
                            PATTERN1:
                                SetRegister {random_reg_id}, {hex(random_value_for_user_log1_via_regA)}
                                UserLog1_R {random_reg_id}
                                Repeat {random_instruction_repeat_count}
                                CompareLowVectors length=2, ctv=1
                                DriveHighVectors length=32
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN2
                                Return
                            
                            PATTERN2:
                                UserLog1_I {hex(random_value_for_user_log1)}
                                DriveZeroVectors length=16, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()


class Local(HbiccTest):
    """Feature tests using Vector Operations Instructions of VP type LOCAL."""
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.slices = SLICES
        self.pattern_helper.is_simulator_active = True
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def DirectedVectorRepeatAndEndStatusImmediateTest(self):
        """Verify Vector Repeat and End Status setting via Immediate"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_instruction_repeat_count = random.randint(32, 512)
        random_local_repeat_count = random.randint(1, 31)
        zero_instruction_repeat_count = 0
        zero_local_repeat_count = 0
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000}
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1, ctv=1
                                Repeat {random_instruction_repeat_count}
                                CompareHighVectors lrpt={random_local_repeat_count}, length=1, ctv=1
                                Repeat {random_instruction_repeat_count}
                                CompareLowVectors lrpt={zero_local_repeat_count}, length=1, ctv=1
                                Repeat {zero_instruction_repeat_count}
                                CompareHighVectors lrpt={random_local_repeat_count}, length=1, ctv=1
                                Repeat {zero_instruction_repeat_count}
                                CompareHighVectors lrpt={zero_local_repeat_count}, length=1, ctv=1
                                ResetUserCycleCount
                                CompareLowVectors length=1, ctv=1
                                Return                                
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedVectorRepeatAndEndStatusViaRegisterATest(self):
        """Verify Vector Repeat and End Status setting via Register A"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        random_reg_id_1 = random.randint(0, 31)
        random_reg_id_2 = random.randint(0, 31)
        random_instruction_repeat_count = random.randint(32, 512)
        random_local_repeat_count = random.randint(1, 31)
        zero_instruction_repeat_count = 0
        zero_local_repeat_count = 0
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000}
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                SetRegister {random_reg_id_1}, {hex(self.end_status)}
                                StopPatternReturnRegister {random_reg_id_1}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1, ctv=1
                                SetRegister {random_reg_id_2}, {random_instruction_repeat_count}
                                RepeatR {random_reg_id_2}
                                CompareHighVectors lrpt={random_local_repeat_count}, length=1, ctv=1
                                SetRegister {random_reg_id_2}, {random_instruction_repeat_count}
                                RepeatR {random_reg_id_2}
                                CompareLowVectors lrpt={zero_local_repeat_count}, length=1, ctv=1
                                SetRegister {random_reg_id_2}, {zero_instruction_repeat_count}
                                RepeatR {random_reg_id_2}
                                CompareHighVectors lrpt={random_local_repeat_count}, length=1, ctv=1
                                SetRegister {random_reg_id_2}, {zero_instruction_repeat_count}
                                RepeatR {random_reg_id_2}
                                CompareHighVectors lrpt={zero_local_repeat_count}, length=1, ctv=1
                                ResetUserCycleCount
                                CompareLowVectors length=1, ctv=1
                                Return                            
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()


class Other(HbiccTest):
    """Feature tests using Vector Operations Instructions of VP type OTHER.

    Notes:
    1. BlockFails feature is NOT present in HBICC. So, tests for this have been removed.
    2. Channel Linking is NOT supported in HBICC. So channel compression based count reset tests are removed.
    3. DrainPinFIFO feature is present, but NOT relevant in Hbicc use model. So, tests for this have been removed.
    4. Sticky Errors are not relevant in Hbicc use model. So, tests for all ClearStickyErrors have been removed.
    """
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        # self.slices = SLICES
        self.slices = [0, 1, 3, 4]
        self.pattern_helper.is_simulator_active = True
        self.ctp = 0b00000000000000000001001000000000010
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def DirectedResetCycleCountTest(self):
        """Verify User Cycle Count Reset Vector Operation - Multiple resets in single pattern"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                               S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask #ctp mask
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1, ctv=1
                                CompareHighVectors length=16, ctv=1
                                ResetUserCycleCount
                                CompareHighVectors length=20, ctv=1
                                ResetUserCycleCount
                                CompareLowVectors length=16, ctv=1
                                Return                                
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetCycleCountPlistTest(self):
        """Verify User Cycle Count Reset Vector Operation - Multiple resets across different patterns in sequence"""
        patt_id_0 = random.randint(0x00000000, 0x0000ffff)
        patt_id_1 = random.randint(0x00010000, 0x00ffffff)
        patt_id_2 = random.randint(0x01000000, 0xffffffff)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                CapturePins {self.ctp}
                                PatternId {hex(patt_id_0)}
                                PCall PATTERN0
                                CapturePins {self.ctp}
                                PatternId {hex(patt_id_1)}
                                PCall PATTERN1
                                ResetUserCycleCount
                                CapturePins {self.ctp}
                                PatternId {hex(patt_id_2)}
                                PCall PATTERN2
                                CompareLowVectors length=6, ctv=1
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=3, ctv=1
                                ResetUserCycleCount
                                Return
                            
                            PATTERN1:
                                CompareHighVectors length=2, ctv=1
                                ResetUserCycleCount
                                CompareLowVectors length=4, ctv=1
                                Return
                            
                            PATTERN2:
                                CompareLowVectors length=5, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetCycleCountNestedPCallsTest(self):
        """Verify User Cycle Count Reset Vector Operation - Multiple resets across different nested patterns"""
        patt_id_0 = random.randint(0x00000000, 0x0000ffff)
        patt_id_1 = random.randint(0x00010000, 0x00ffffff)
        patt_id_2 = random.randint(0x01000000, 0xffffffff)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask #ctp mask
                                PatternId {hex(patt_id_0)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=2, ctv=1
                                ResetUserCycleCount
                                PatternId {hex(patt_id_1)}
                                PCall PATTERN1
                                Return

                            PATTERN1:
                                PatternId {hex(patt_id_2)}
                                PCall PATTERN2
                                CompareHighVectors length=3, ctv=1
                                ResetUserCycleCount
                                CompareLowVectors length=5, ctv=1
                                Return

                            PATTERN2:
                                CompareLowVectors length=4, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetCycleCountIRPTTest(self):
        """Verify User Cycle Count Reset Vector Operation - Reset followed by Repeat"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        self.pattern_helper.user_mode = 'HIGH'
        random_repeat_count = random.randint(32, 512)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                               S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask#ctp mask
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1, ctv=1
                                CompareHighVectors length=16, ctv=1
                                ResetUserCycleCount
                                Repeat {random_repeat_count}
                                CompareLowVectors length=1, ctv=1
                                CompareLowVectors length=16, ctv=1
                                Return                                
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetFailCountsTest(self):
        """Verify Fail Counts (both total fail count and pin fail counts) Reset Vector Operation"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        self.pattern_helper.user_mode = 'HIGH'
        random_repeat = random.randint(16, 512)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                               S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask #ctp mask
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1           # 1 fail
                                CompareHighVectors length={random_repeat}
                                ResetFailCounts
                                CompareLowVectors length=2           # 1-2 fail
                                CompareHighVectors length={random_repeat}
                                ResetFailCounts
                                CompareLowVectors length=3           # 1-3 fail
                                Return                                
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetFailCountsPListTest(self):
        """Verify Fail Counts Reset Vector Operation - Multiple resets across different patterns in sequence"""
        patt_id_0 = random.randint(0x00000000, 0x0000ffff)
        patt_id_1 = random.randint(0x00010000, 0x00ffffff)
        patt_id_2 = random.randint(0x01000000, 0xffffffff)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                CapturePins {self.ctp}
                                PatternId {hex(patt_id_0)}
                                PCall PATTERN0
                                CapturePins {self.ctp}
                                PatternId {hex(patt_id_1)}
                                PCall PATTERN1
                                ResetFailCounts
                                CapturePins {self.ctp}
                                PatternId {hex(patt_id_2)}
                                PCall PATTERN2
                                CompareLowVectors length=6, ctv=1
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=3, ctv=1
                                ResetFailCounts
                                Return
                            
                            PATTERN1:
                                CompareLowVectors length=2, ctv=1
                                ResetFailCounts
                                CompareHighVectors length=4, ctv=1
                                Return
                            
                            PATTERN2:
                                CompareLowVectors length=5, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetFailCountsNestedPCallsTest(self):
        """Verify Fail Counts Reset Vector Operation - Multiple resets across different nested patterns"""
        patt_id_0 = random.randint(0x00000000, 0x0000ffff)
        patt_id_1 = random.randint(0x00010000, 0x00ffffff)
        patt_id_2 = random.randint(0x01000000, 0xffffffff)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                 S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask #ctp mask
                                PatternId {hex(patt_id_0)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}

                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=2, ctv=1
                                ResetFailCounts
                                PatternId {hex(patt_id_1)}
                                PCall PATTERN1
                                Return

                            PATTERN1:
                                PatternId {hex(patt_id_2)}
                                PCall PATTERN2
                                CompareHighVectors length=3, ctv=1
                                ResetFailCounts
                                CompareLowVectors length=5, ctv=1
                                Return

                            PATTERN2:
                                CompareLowVectors length=4, ctv=1
                                Return
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedResetFailsCountIRPTTest(self):
        """Verify Fail Counts Reset Vector Operation - Reset followed by Repeat"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        self.pattern_helper.user_mode = 'HIGH'
        random_repeat_count = random.randint(32, 512)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                 S stype=CAPTURE_PIN_MASK,                           data={0b00000000000000000000000000000000000} #ctp mask #ctp mask
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1, ctv=1
                                CompareHighVectors length=16, ctv=1
                                ResetFailCounts
                                Repeat {random_repeat_count}
                                CompareLowVectors length=1, ctv=1
                                CompareLowVectors length=16, ctv=1
                                Return                                
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()

    def DirectedGlobalCaptureWindowEnableDisableTest(self):
        """Verify that global capture disable can be turned on and off"""
        random_patt_id = random.randint(0x00000000, 0xfffffffe)
        self.pattern_helper.user_mode = 'HIGH'
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                CapturePins {self.ctp}
                                PatternId {hex(random_patt_id)}
                                PCall PATTERN0
                                StopPattern {hex(self.end_status)}
                            
                            PATTERN0:
                                NoCompareDriveZVectors length={PADDING}
                                CompareLowVectors length=1, ctv=1                   # fail 0 - recorded
                                CompareHighVectors length=5, ctv=1
                                CaptureWindowDisable                                      # Global Capture Disabled
                                CompareLowVectors length=2, ctv=1                   # fail 1-2 - not recorded
                                CompareLowVectors length=1, lrpt=3, ctv=1           # fail 3-5 - not recorded
                                CaptureWindowEnable                                       # Global Capture Enabled
                                CompareHighVectors length=5, ctv=1
                                ResetFailCounts
                                CompareLowVectors length=3, ctv=1                   # fail 6-8 - recorded
                                Return                           
                            '''
        self.Log('debug', pattern_string)
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_all': 0,
                                                                               'capture_ctv': 1})
        self.pattern_helper.execute_pattern_scenario()
