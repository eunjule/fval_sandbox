# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""
Enabled clock features of the pattern generator present on
pins 0, 8, 16, etc.

Requirements:
pattern - desired enable clocks can only be on every eighth pin and
    must be designated with pattern character E
pin multiplier register config2 must be set to desired frequency
dtb - all Enabled Clock tests are loopback tests and so will only
    pass if the dtb is installed

Config 2 register definition:
cycles high: duration of a high output or '1'
cycles low: duration of a low or '0'
EC first edge:  set to either '0' or '1'
EC add half cycle: used to generate 50% duty cycle for odd periods
EC ratio 1: Setting for clock at pattern period (fastest clock option)

Types of waveforms:
ratio - total period of clock in tester cycles
duty cycle - percent of period at which clock is held at high
inverted - waveform will start at 0 after EC reset
compare - compare high or compare low or both
example - DirectedConstrainedRatio4Duty25ComparehighlowExhaustiveTest
    Clock period is 4 cycles long with 1 high cycle.  Pattern uses
    both compare high and compare low
"""

import random

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

SLICECOUNT = range(0, 5)

class BaseClock(HbiccTest):
    """Base class that includes debug tests and helper functions that are common to both EC and FRC tests"""

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        population = range(5)
        self.channel_sets = set(range(16))
        self.ctp = 0b0
        self.slices = population
        self.env.hbicc.clear_config2()
        self.env.hbicc.clear_config3()
        self.end_status = random.randint(0, 0xFFFFFFFF)

    def assign_channel_sets_based_on_dtb(self):
        self.channel_sets = []
        for pm in self.env.hbicc.get_pin_multipliers():
            if (pm.hbidtb):
                first_channel_set = 4 * pm.slot_index
                self.channel_sets.extend(list(range(first_channel_set, first_channel_set + 4)))

    def execute_scenario(self, pattern_string):
        self.pattern_helper.user_mode = 'RELEASE'
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, channel_sets=self.channel_sets,
                                                                   slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def create_single_compare_vector(self, slice, compare_high=False):
        if compare_high:
            cc = 'H'
        else:
            cc = 'L'
        if (slice % 2):
            compare_vector = f"0vXXE{cc}XXXXXXE{cc}XXXXXXE{cc}XXXXXXE{cc}XXXXXXE"
        else:
            compare_vector = f"0vX{cc}EXXXXXX{cc}EXXXXXX{cc}EXXXXXX{cc}EXXXXXX{cc}E"
        return compare_vector

    def create_compare_vectors(self, slice):
        if (slice % 2):
            compare_vector_low = "0vXXELXXXXXXELXXXXXXELXXXXXXELXXXXXXE"
            compare_vector_high = "0vXXEHXXXXXXEHXXXXXXEHXXXXXXEHXXXXXXE"
        else:
            compare_vector_low = "0vXLEXXXXXXLEXXXXXXLEXXXXXXLEXXXXXXLE"
            compare_vector_high = "0vXHEXXXXXXHEXXXXXXHEXXXXXXHEXXXXXXHE"
        return compare_vector_low, compare_vector_high

    def construct_keep_pin_pattern_body(self, num_high_cycles, num_low_cycles):
        compare_vector_low, compare_vector_high = self.create_compare_vectors(self.slices[0])
        vector_1 = f"V ctv=0, mtv=0, lrpt=0, data={compare_vector_high}\n" \
                   f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_high_cycles - 1}\n" \
                   f"V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK\n"
        vector_2 = f"V ctv=0, mtv=0, lrpt=0, data={compare_vector_low}\n" \
                   f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_low_cycles - 1}\n" \
                   f"V ctv=0, mtv=0, lrpt=0, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
        return vector_1 + "                " + vector_2

    def construct_capture_pin_pattern_body(self):
        num_of_repeats = 1024
        pattern_body = f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_of_repeats}\n" \
                       f"V ctv=1, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE"
        return pattern_body

    def construct_pattern_body(self, num_high_cycles, num_low_cycles, reset_high=1):
        compare_vector_low, compare_vector_high = self.create_compare_vectors(self.slices[0])
        if (reset_high):
            vector_1 = f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_high_cycles}\n" \
                       f"V ctv=0, mtv=0, lrpt=0, data={compare_vector_high}\n"
            vector_2 = f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_low_cycles}\n" \
                       f"V ctv=0, mtv=0, lrpt=0, data={compare_vector_low}"
        else:
            vector_1 = f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_low_cycles}\n" \
                       f"V ctv=0, mtv=0, lrpt=0, data={compare_vector_low}\n"
            vector_2 = f"I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={num_high_cycles}\n" \
                       f"V ctv=0, mtv=0, lrpt=0, data={compare_vector_high}"
        return vector_1 + "                " + vector_2

    def create_serialize_control_pin_state_word_pattern(self, num_cycles_hi, num_cycles_lo):
        compare_vector_low, compare_vector_high = self.create_compare_vectors(self.slices[0])
        pattern_id_0 = random.randint(0, 100)
        lrpt_hi = num_cycles_hi - 1
        lrpt_lo = num_cycles_lo - 1
        pattern_string = f'''

                PATTERN_START:
                StartPattern
                PatternId {pattern_id_0}
                PCall PATTERN1
                StopPattern {hex(random.randint(0, 0xFFFFFFFF))}

                PATTERN1:
                    V ctv=0, mtv=0, lrpt={lrpt_hi}, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                    V ctv=0, mtv=0, lrpt={lrpt_lo}, data={compare_vector_low}
                    V ctv=0, mtv=0, lrpt={lrpt_hi}, data={compare_vector_high}
                    V ctv=0, mtv=0, lrpt={lrpt_lo}, data={compare_vector_low}
                    V ctv=0, mtv=0, lrpt={lrpt_hi}, data={compare_vector_high}
                 '''
        for i in range(20):
            channel_set_encode = random.getrandbits(4)
            character_set = ['0', '1', 'E', 'F']
            rc = random.choices(character_set, k=5)
            data_vector = f'0vFF{rc[0]}FFFFFFF{rc[1]}FFFFFFF{rc[2]}FFFFFFF{rc[3]}FFFFFFF{rc[4]}'
            mid_string = f'''S stype=DUT_SERIAL, channel_set={channel_set_encode},   data={data_vector}
                    V ctv=0, mtv=0, lrpt={lrpt_lo}, data={compare_vector_low}
                    V ctv=0, mtv=0, lrpt={lrpt_hi}, data={compare_vector_high}
                    '''
            pattern_string = pattern_string + mid_string
        pattern_string_end = f'''
                    V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                    DriveZeroVectors length=10

                Return
                '''
        pattern_string = pattern_string + pattern_string_end
        return pattern_string

    def create_clock_pattern_with_keep_pin(self, num_cycles_hi, num_cycles_lo):
        pattern_body = self.construct_keep_pin_pattern_body(num_high_cycles=num_cycles_hi, num_low_cycles=num_cycles_lo)
        period = num_cycles_lo + num_cycles_hi
        rpt_cycles = 10 * period - 1
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_cycles}
                V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                %repeat 100
                {pattern_body}
                %end
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=400

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=400
                 '''
        return pattern_string

    def construct_clock_pattern_with_mid_pattern_reset(self, reset_high=1, num_high_cycles=1, num_low_cycles=1):
        compare_vector_low, compare_vector_high = self.create_compare_vectors(self.slices[0])
        if reset_high:
            clock_pattern = self.generate_clock_pattern_with_mid_pattern_reset_and_compare_for_reset_high(
                compare_vector_high, compare_vector_low, num_high_cycles, num_low_cycles)
        else:
            clock_pattern = self.generate_clock_pattern_with_mid_pattern_reset_and_compare_for_reset_low(
                compare_vector_high, compare_vector_low, num_high_cycles, num_low_cycles)
        return clock_pattern

    def generate_clock_pattern_with_mid_pattern_reset_and_compare_for_reset_high(self, compare_vector_high,
                                                                                 compare_vector_low,
                                                                                 num_high_cycles, num_low_cycles):
        random_location_for_clock_reset_within_high_cycle = random.randint(num_high_cycles // 2, num_high_cycles)
        vector_high_cycle_before_reset = f"Repeat {random_location_for_clock_reset_within_high_cycle}\n" \
                                         f"V ctv=0, mtv=0, data={compare_vector_high}\n"
        clock_reset = f'S stype = EC_RESET, data=0b11111111111111111111111111111111111\n'
        vector_high_cycle = f"Repeat {num_high_cycles}\n" \
                            f"V ctv=0, mtv=0, data={compare_vector_high}\n"
        vector_low_cycle = f"Repeat {num_low_cycles}\n" \
                           f"V ctv=0, mtv=0, data={compare_vector_low}"
        return vector_high_cycle_before_reset + clock_reset + vector_high_cycle + vector_low_cycle

    def generate_clock_pattern_with_mid_pattern_reset_and_compare_for_reset_low(self, compare_vector_high,
                                                                                compare_vector_low,
                                                                                num_high_cycles, num_low_cycles):
        random_location_for_clock_reset_within_low_cycle = random.randint(num_low_cycles // 2, num_low_cycles)
        vector_low_cycle_before_reset = f"Repeat {random_location_for_clock_reset_within_low_cycle}\n" \
                                        f"V ctv=0, mtv=0, data={compare_vector_low}\n"
        clock_reset = f'S stype = EC_RESET, data=0b11111111111111111111111111111111111\n'
        vector_low_cycle = f"Repeat {num_low_cycles}\n" \
                           f"V ctv=0, mtv=0, data={compare_vector_low}\n"
        vector_high_cycle = f"Repeat {num_high_cycles}\n" \
                            f"V ctv=0, mtv=0, data={compare_vector_high}"
        return vector_low_cycle_before_reset + clock_reset + vector_low_cycle + vector_high_cycle

    def create_clock_pattern_for_capture_pin(self):
        pattern_body = self.construct_capture_pin_pattern_body()
        id_0 = random.randint(0, 100)
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1

                PATTERN1:
                NoCompareDriveZVectors length=1016
                V ctv=0, mtv=0, lrpt=7, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    {pattern_body}
                Return
                '''
        return pattern_string


class EnabledClock(BaseClock):
    """Test permutations of duty cycle and period to verify that
       config2 register settings correctly set waveform"""

    def MiniEnabledClockRatioLowCyclesPM0Test(self):
        """Sets EC pins to different periods by increasing number of low cycles.
            number of high cycles is 1 period.  Only tests PM0."""
        if self.env.hbicc.psdb_0_pin_0.hbidtb:
            channel_sets = range(4)
            self.enabled_clock_ratio_cycles(channel_sets, high=False)
        else:
            pm_name = self.env.hbicc.psdb_0_pin_0.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioLowCyclesPM1Test(self):
        """Sets EC pins to different periods by increasing number of low cycles.
            number of high cycles is 1 period.  Only tests PM1."""
        if self.env.hbicc.psdb_0_pin_1.hbidtb:
            channel_sets = range(4, 8)
            self.enabled_clock_ratio_cycles(channel_sets, high=False)
        else:
            pm_name = self.env.hbicc.psdb_0_pin_1.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioLowCyclesPM2Test(self):
        """Sets EC pins to different periods by increasing number of low cycles.
            number of high cycles is 1 period.  Only tests PM2."""
        if self.env.hbicc.psdb_1_pin_0.hbidtb:
            channel_sets = range(8, 12)
            self.enabled_clock_ratio_cycles(channel_sets, high=False)
        else:
            pm_name = self.env.hbicc.psdb_1_pin_0.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioLowCyclesPM3Test(self):
        """Sets EC pins to different periods by increasing number of low cycles.
            number of high cycles is 1 period.  Only tests PM3."""
        if self.env.hbicc.psdb_1_pin_1.hbidtb:
            channel_sets = range(12, 16)
            self.enabled_clock_ratio_cycles(channel_sets, high=False)
        else:
            pm_name = self.env.hbicc.psdb_1_pin_1.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioHighCyclesPM0Test(self):
        """Sets EC pins to different periods by increasing number of high cycles.
            number of low cycles is 1 period.  Only tests PM0."""
        if self.env.hbicc.psdb_0_pin_0.hbidtb:
            channel_sets = range(4)
            self.enabled_clock_ratio_cycles(channel_sets)
        else:
            pm_name = self.env.hbicc.psdb_0_pin_0.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioHighCyclesPM1Test(self):
        """Sets EC pins to different periods by increasing number of high cycles.
            number of low cycles is 1 period.  Only tests PM1."""
        if self.env.hbicc.psdb_0_pin_1.hbidtb:
            channel_sets = range(4, 8)
            self.enabled_clock_ratio_cycles(channel_sets)
        else:
            pm_name = self.env.hbicc.psdb_0_pin_1.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioHighCyclesPM2Test(self):
        """Sets EC pins to different periods by increasing number of high cycles.
            number of low cycles is 1 period.  Only tests PM2."""
        if self.env.hbicc.psdb_1_pin_0.hbidtb:
            channel_sets = range(8, 12)
            self.enabled_clock_ratio_cycles(channel_sets)
        else:
            pm_name = self.env.hbicc.psdb_1_pin_0.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockRatioHighCyclesPM3Test(self):
        """Sets EC pins to different periods by increasing number of high cycles.
            number of low cycles is 1 period.  Only tests PM3."""
        if self.env.hbicc.psdb_1_pin_1.hbidtb:
            channel_sets = range(12, 16)
            self.enabled_clock_ratio_cycles(channel_sets)
        else:
            pm_name = self.env.hbicc.psdb_1_pin_1.name()
            self.Log('error', f'No DTB is present on {pm_name}')

    def MiniEnabledClockBigRatio50DutyCycleTest(self):
        """Sets EC pins to max possible values

           Test looks at what happens when num of high cycles and low cycles is 255.
           This works out to a frequency of around 400 kHz at 50 % duty cycle."""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.enabled_clock_directed_body(reset_high=1, num_high_cycles=255, num_low_cycles=255)
        else:
            self.Log('error', f'No DTB is present on tester')

    ##Regular EnCLK Tests

    def DirectedRatio1Duty50CompareHighExhaustiveTest(self):
        """Sets EC pins to ratio 1, adjusts delay so all pins pass compare high
            Waveform starts high"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.enabled_clock_ratio1_body(reset_high=True, compare_high=True)
        else:
            self.Log('error', f'No DTB is present on tester')

    ##InvertedTestCases:Start
    def DirectedInvertedRatio1Duty50CompareHighExhaustiveTest(self):
        """Sets EC pins to ratio 1, adjusts delay so all pins pass compare high.
            Waveform starts low"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.enabled_clock_ratio1_body(reset_high=False, compare_high=True)
        else:
            self.Log('error', f'No DTB is present on tester')

    ##Constrained EnCLK Regular Tests

    def DirectedConstrainedRatio2Duty50ComparehighlowExhaustiveTest(self):
        """Sets EC pins to a period of 2, duty cycle of 50%
           compares alternate high and low"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.enabled_clock_directed_body(reset_high=True, num_high_cycles=1, num_low_cycles=1)
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedConstrainedRatio3Duty50ComparehighlowExhaustiveTest(self):
        """Sets EC pins to a period of 3, duty cycle of 50%
           compares alternate high and low, checks that half cycle delay
           is set correctly"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.enabled_clock_ratio3_body(reset_high=True, compare_high=True)
        else:
            self.Log('error', f'No DTB is present on tester')

    ##Constained Inverted EnCLK Tests

    def DirectedConstrainedInvertedRatio2Duty50ComparehighlowExhaustiveTest(self):
        """Sets EC pins to a period of 2, duty cycle of 50% (2 high)
           compares alternate low and high"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.enabled_clock_directed_body(reset_high=False, num_high_cycles=1, num_low_cycles=1)
        else:
            self.Log('error', f'No DTB is present on tester')

    def RandomRatioDutyCycleComparehighlowExhaustiveTest(self):
        """Sets EC pins to a random setting for start state, num of high cycles and number of low cycles
           compares alternate low and high.
           The test stresses a number or clock settings to provide broader coverage"""

        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            for slice in SLICECOUNT:
                num_high_cycles = random.randint(1, 31)
                num_low_cycles = random.randint(1, 31)
                enable_clock_first_edge = random.getrandbits(1)
                for i in range(4):
                    self.enabled_clock_directed_body(slice=slice, reset_high=enable_clock_first_edge, num_high_cycles=num_high_cycles,
                                                     num_low_cycles=num_low_cycles)
                    self.env.hbicc.model.patgen.skip_multi_burst_simulation = True

                self.pattern_helper = PatternHelper(self.env)
                self.env.hbicc.model.patgen.skip_multi_burst_simulation = False
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedECPinResetMidPatternTest(self):
        """Verify Mid pattern EC pin reset functionality
        Inserts additional EC pin resets mid way through execution
         """
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.slices = [random.randint(0, 4)]
            reset_high = True
            num_high_cycles = random.randint(1, 9)
            num_low_cycles = random.randint(1, 9)
            clock_pattern_body = self.construct_clock_pattern_with_mid_pattern_reset(reset_high=reset_high,
                                                                                     num_high_cycles=num_high_cycles,
                                                                                     num_low_cycles=num_low_cycles)
            self.Log('debug', f'{clock_pattern_body}')
            period = num_high_cycles + num_low_cycles
            start_clock_cycles = 10 * period
            end_clock_cycles = 10 * period
            random_patt_id = random.randint(0x00000000, 0xffffffff)
            random_end_status = random.randint(0, 0xFFFFFFFF)
            random_clock_pattern_body_repeat = random.randint(10, 100)
            pattern_string = f'''
                                PATTERN_START:
                                    StartPattern
                                    CapturePins {self.ctp}
                                    PatternId {hex(random_patt_id)}
                                    PCall PATTERN0
                                    StopPattern {hex(random_end_status)}
                                
                                PATTERN0:
                                    Repeat {start_clock_cycles}
                                    V ctv=0, mtv=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                                    %repeat {random_clock_pattern_body_repeat}
                                    {clock_pattern_body}
                                    %end
                                    Repeat {end_clock_cycles}
                                    V ctv=0, mtv=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                                    DriveZeroVectors length=10
                                    Return
                             '''
            self.Log('debug', f'{pattern_string}')
            self.env.hbicc.set_all_EC_pins(first_edge=reset_high, HighCycles=num_high_cycles, LowCycles=num_low_cycles)
            self.execute_scenario(pattern_string)
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedEnableClockSerializeControlPinStateWordTest(self):
        """Tests EC response to updates to the serialize control pin state word which defines
           how a pin should respond when the channelset is not considered an active channelset"""
        self.slices = [random.randint(0, 4)]
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            pattern = self.create_serialize_control_pin_state_word_pattern(num_cycles_hi=4, num_cycles_lo=4)
            self.env.hbicc.set_all_EC_pins(HighCycles=4, LowCycles=4)
            self.execute_scenario(pattern)
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedEnableClockKeepPinTest(self):
        """Tests to see if enabled clock pins maintain the transition from drive simbol (0,1, E) to K (keep pin) symbol.
        Keep pin is a feature that allows the channel to maintain the state of the previous symbol"""
        self.slices = [random.randint(0, 4)]
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            pattern = self.create_clock_pattern_with_keep_pin(num_cycles_hi=4, num_cycles_lo=4)
            self.env.hbicc.set_all_EC_pins(HighCycles=4, LowCycles=4)
            self.execute_scenario(pattern)
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedCTPforPin0Test(self):
        """Provides coverage for pin 0 which would not be present due to cross slice loopback in DTB"""
        self.ctp = 0b1
        pattern = self.create_clock_pattern_for_capture_pin()
        high_cycles = random.randint(1, 7)
        low_cycles = 8 - high_cycles
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=0)
        self.env.hbicc.set_all_EC_pins(HighCycles=high_cycles, LowCycles=low_cycles)
        self.execute_scenario(pattern)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=67)

    def enabled_clock_ratio3_body(self, reset_high=True, compare_high=False, cycle_delay=0):
        self.slices = [random.randint(0, 4)]
        pat_body = self.construct_pattern_body(reset_high=reset_high, num_high_cycles=2,
                                               num_low_cycles=1)
        period = 3
        lrpt_cycles = 10 * period - 1
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                V ctv=0, mtv=0, lrpt={lrpt_cycles}, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                %repeat 100
                {pat_body}
                %end
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=400

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=400
                 '''
        self.env.hbicc.set_all_EC_pins(ECIsRatio1=0, first_edge=reset_high, HighCycles=1, LowCycles=2, Halfcycledelay=1)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=69)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=65)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=67)

    def enabled_clock_ratio1_body(self, reset_high=True, compare_high=False):
        self.slices = [random.randint(0, 4)]
        compare_vector = self.create_single_compare_vector(self.slices[0], compare_high=compare_high)
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                
                ConstantVectors length=10000, data={compare_vector}
                
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=10

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=10
                '''
        self.env.hbicc.set_all_EC_pins(ECIsRatio1=1, first_edge=reset_high)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=69)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=65)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=67)


    def enabled_clock_directed_body(self, slice=None, reset_high=1, num_high_cycles=1, num_low_cycles=1):
        self.slices = [random.randint(0, 4)] if slice is None else [slice]
        pat_body = self.construct_pattern_body(reset_high=reset_high, num_high_cycles=num_high_cycles,
                                               num_low_cycles=num_low_cycles)
        period = num_high_cycles + num_low_cycles
        consecutive_cycles = 10 * period - 1
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={consecutive_cycles}
                V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                %repeat 100
                {pat_body}
                %end
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=10
     
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.end_status}
                DriveZeroVectors length=10
                 '''
        self.env.hbicc.set_all_EC_pins(first_edge=reset_high, HighCycles=num_high_cycles, LowCycles=num_low_cycles)
        self.execute_scenario(pattern_string)

    def enabled_clock_ratio_cycles(self, channel_sets, high=True):
        self.channel_sets = channel_sets
        self.slices = [random.randint(0, 4)]
        compare_vector = self.create_single_compare_vector(self.slices[0])
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                
                ConstantVectors length=10000, data={compare_vector}
                
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=10

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=10
                '''
        cycle_count_array = [1, 2, 3, 4, 6, 8, 12, 16, 18, 24, 32, 40, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192,
                             218, 234, 255, 255, 255, 255]
        cycle_ind = 0
        for slice_ind in self.slices:
            for pin_ind in range(1, 35, 8):
                num_cycles = cycle_count_array[cycle_ind]
                for channel_set_ind in range(4):
                    for pm in self.env.hbicc.get_pin_multipliers():
                        if high:
                            pm.write_single_pin_config(slice_ind, pin_ind - 1, first_edge=1,
                                                       channel_set=channel_set_ind,
                                                       HighCycles=num_cycles, LowCycles=1)
                        else:
                            pm.write_single_pin_config(slice_ind, pin_ind - 1, first_edge=1,
                                                       channel_set=channel_set_ind,
                                                       HighCycles=1, LowCycles=num_cycles)

                cycle_ind = cycle_ind + 1
        self.execute_scenario(pattern_string)


class FreeRunningClock(BaseClock):
    """Test permutations of period to verify that Free Running clock set by
       config3 register settings functions as defined in the HLD"""

    def MiniFreeRunningClockRatioTest(self):
        """Sets FRC pins to ratio different ratios.  Test does not hit any corner cases and will be primarily used
           to verify simulation framework is working correctly"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_ratio_cycles(high=True)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def MiniInvertedFreeRunningClockRatioTest(self):
        """Sets FRC pins to ratio different ratios.  Test does not hit any corner cases and will be primarily used
           to verify simulation framework is working correctly.  FRC starts at 0 after reset"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_ratio_cycles(high=False)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockRatio1Test(self):
        """Sets FRC pins to ratio 1, adjusts delay so all pins pass compare high
            Waveform starts high.  Second pass adjusts delay so all pins fail"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_ratio1_body(reset_high=True, compare_high=True)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedInvertedFreeRunningClockRatio1Test(self):
        """Sets FRC pins to ratio 1, adjusts delay so all pins pass compare low
            Waveform starts low.  Second pass adjusts delay so all pins fail"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_ratio1_body(reset_high=True, compare_high=True)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockRatio2Test(self):
        """Sets FRC pins to ratio 2, compares high and low are both used to match the free
           running clock.  Expected number of failures is zero"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_evenratio_body(reset_high=True, ratio=2)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockRatio3Test(self):
        """Sets FRC pins to ratio 3. This test verifies if the FRC is able to maintain a 50 %
           Duty Cycle when the ratio is odd."""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_oddratio_body(reset_high=True, period=3)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockRatio5Test(self):
        """Sets FRC pins to ratio 5.  This test verifies if the FRC is able to maintain a 50 %
           Duty Cycle when the ratio is odd."""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_oddratio_body(reset_high=True, period=5)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockRatioLongRatioTest(self):
        """Sets FRC pins to ratio that is very large.  """
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            self.free_running_clock_long_ratio()
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedCTPforPin0Test(self):
        """Provides coverage for pin 0 which would not be present due to cross slice loopback in DTB"""
        self.ctp = 0b1
        pattern = self.create_clock_pattern_for_capture_pin()
        period = 8
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=0)
        self.env.hbicc.set_all_FRC_pins(period=period)
        self.execute_scenario(pattern)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=67)


    def DirectedFreeRunningClockModifiedDutyCycleTest(self):
        """ Uses a combination of 0 and 1 drives to augment the FRC and create a non 50% duty cycle"""
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            period = 2*random.randint(3,12)
            max_mod_cycles=period//2-1
            num_mod_cycles = random.randint(1,max_mod_cycles)
            self.free_running_clock_modified_duty_cyle_body(period, num_mod_cycles)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockKeepPinTest(self):
        """Tests to see if enabled clock pins maintain the transition from drive simbol (0,1, E) to K (keep pin) symbol.
        Keep pin is a feature that allows the channel to maintain the state of the previous symbol"""
        self.slices = [random.randint(0, 4)]
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            pattern = self.create_clock_pattern_with_keep_pin(num_cycles_hi=4, num_cycles_lo=4)
            self.env.hbicc.set_all_FRC_pins(period=8)
            self.execute_scenario(pattern)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def DirectedFreeRunningClockSerializeControlPinStateWordTest(self):
        """Tests FRC response to updates to the serialize control pin state word which defines
           how a pin should respond when the channelset is not considered an active channelset"""
        self.slices = [random.randint(0, 4)]
        self.assign_channel_sets_based_on_dtb()
        if (self.channel_sets):
            pattern = self.create_serialize_control_pin_state_word_pattern(num_cycles_hi=4, num_cycles_lo=4)
            self.env.hbicc.set_all_FRC_pins(period=8)
            self.execute_scenario(pattern)
            self.env.hbicc.clear_config3()
        else:
            self.Log('error', f'No DTB is present on tester')

    def free_running_clock_modified_duty_cyle_body(self, period, num_mod_cycles):
        self.slices = [random.randint(0, 4)]
        pattern_string = self.create_modified_duty_cycle_pattern(self.slices[0], period, num_mod_cycles)
        self.env.hbicc.set_all_FRC_pins(first_edge=1, period=period)
        self.execute_scenario(pattern_string)

    def create_modified_duty_cycle_pattern(self, slice, period, num_mod_cycles):
        half_period = period//2
        unmod_cycles = half_period - num_mod_cycles
        if (slice % 2):
            compare_vector_low = "0vXXELXXXXXXELXXXXXXELXXXXXXELXXXXXXE"
            mod_vector_low = "0vXX0LXXXXXX0LXXXXXX0LXXXXXX0LXXXXXX0"
            compare_vector_high = "0vXXEHXXXXXXEHXXXXXXEHXXXXXXEHXXXXXXE"
            mod_vector_high = "0vXX1HXXXXXX1HXXXXXX1HXXXXXX1HXXXXXX1"
        else:
            compare_vector_low = "0vXLEXXXXXXLEXXXXXXLEXXXXXXLEXXXXXXLE"
            compare_vector_high = "0vXHEXXXXXXHEXXXXXXHEXXXXXXHEXXXXXXHE"
            mod_vector_low = "0vXL0XXXXXXL0XXXXXXL0XXXXXXL0XXXXXXL0"
            mod_vector_high = "0vXH1XXXXXXH1XXXXXXH1XXXXXXH1XXXXXXH1"
        id_0 = random.randint(0, 100)
        pattern_body = f'''
                %repeat 100
                V ctv=0, mtv=0, lrpt={unmod_cycles-1}, data = {compare_vector_high}
                V ctv=0, mtv=0, lrpt={num_mod_cycles-1}, data = {mod_vector_low}
                V ctv=0, mtv=0, lrpt={half_period-1}, data = {compare_vector_low}
                %end
                %repeat 100
                V ctv=0, mtv=0, lrpt={half_period-1}, data = {compare_vector_high}
                V ctv=0, mtv=0, lrpt={num_mod_cycles-1}, data = {mod_vector_high}
                V ctv=0, mtv=0, lrpt={unmod_cycles-1}, data = {compare_vector_low}
                %end
                '''
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                PatternId {id_0} 
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=1

                PATTERN1:
                    V ctv=0, mtv=0, lrpt={period-1}, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                    SUBR:
                        {pattern_body}
                Return
                '''
        return pattern_string

    def free_running_clock_long_ratio(self):
        self.slices = [random.randint(0, 4)]
        compare_vector_low, compare_vector_high = self.create_compare_vectors(self.slices[0])
        period_in_number_of_tester_cycles = 100000  # frequency is 2000 Hz
        consecutive_cycles = period_in_number_of_tester_cycles // 2
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''
            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={consecutive_cycles}
                    V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={consecutive_cycles}
                    V ctv=0, mtv=0, lrpt=0, data={compare_vector_low}
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={consecutive_cycles}
                    V ctv=0, mtv=0, lrpt=0, data={compare_vector_high}
                    V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                    DriveZeroVectors length=10
                
                Return
                '''
        self.env.hbicc.set_all_FRC_pins(first_edge=1, period=period_in_number_of_tester_cycles)
        self.execute_scenario(pattern_string)

    def free_running_clock_ratio_cycles(self, high=True):
        self.slices = [random.randint(0, 4)]
        compare_vector = self.create_single_compare_vector(self.slices[0], compare_high=False)
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                
                ConstantVectors length=10000, data={compare_vector}
                
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=10

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=10
                '''

        cycle_ind = 0
        for slice_ind in self.slices:
            for pin_ind in range(1, 35, 8):
                num_cycles = 20000 + 4000 * cycle_ind  # 20000, 24000, 28000, etc
                for channel_set_ind in range(4):
                    for pm in self.env.hbicc.get_pin_multipliers():
                        if high:
                            pm.write_single_pin_config_FRC(slice_ind, pin_ind - 1, period=num_cycles,
                                                           channel_set=channel_set_ind, first_edge=1)
                        else:
                            pm.write_single_pin_config_FRC(slice_ind, pin_ind - 1, period=num_cycles,
                                                           channel_set=channel_set_ind, first_edge=0)
                cycle_ind = cycle_ind + 1
        self.execute_scenario(pattern_string)

    def free_running_clock_ratio1_body(self, reset_high=True, compare_high=False):
        self.slices = [random.randint(0, 4)]
        compare_vector = self.create_single_compare_vector(self.slices[0], compare_high=compare_high)
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                
                ConstantVectors length=10000, data={compare_vector}

                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=10

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=10
                '''
        self.env.hbicc.set_all_FRC_pins(FRCIsRatio1=1, first_edge=reset_high)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=69)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=65)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=67)


    def free_running_clock_oddratio_body(self, reset_high=True, period=3):
        self.slices = [random.randint(0, 4)]
        num_low_cycles = period // 2
        num_high_cycles = num_low_cycles + 1
        pat_body = self.construct_pattern_body(reset_high=reset_high, num_high_cycles=num_high_cycles,
                                               num_low_cycles=num_low_cycles)
        lrpt_cycles = 4 * period - 1
        pattern_string = f'''
            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                V ctv=0, mtv=0, lrpt={lrpt_cycles}, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                %repeat 100
                {pat_body}
                %end
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=400

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=400
                 '''
        self.env.hbicc.set_all_FRC_pins(first_edge=reset_high, period=period)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=69)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=65)
        self.execute_scenario(pattern_string)
        for pm in self.env.hbicc.get_pin_multipliers():
            pm.set_delay_stack_on_all_pins(RXdelay=67)


    def free_running_clock_evenratio_body(self, reset_high=1, ratio=2):
        self.slices = [random.randint(0, 4)]
        num_high_cycles = num_low_cycles = ratio // 2
        pat_body = self.construct_pattern_body(reset_high=reset_high, num_high_cycles=num_high_cycles,
                                               num_low_cycles=num_low_cycles)
        consecutive_cycles = 10 * ratio - 1
        pattern_string = f'''

            PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={consecutive_cycles}
                V ctv=0, mtv=0, lrpt=0, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                %repeat 100
                {pat_body}
                %end
                V ctv=0, mtv=0, lrpt=10, data=0vXXEXXXXXXXEXXXXXXXEXXXXXXXEXXXXXXXE
                DriveZeroVectors length=10

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                DriveZeroVectors length=400
                 '''
        self.env.hbicc.set_all_FRC_pins(first_edge=reset_high, period=ratio)
        self.execute_scenario(pattern_string)
