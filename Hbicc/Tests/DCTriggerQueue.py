# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBICC Trigger Queues allow setting DC levels for channels

The Trigger Queues provide access to external PMBUS and SPI
devices, as well as per channel controls internal to the HBICC.

Trigger Queues can be executed by single word through a PCIe
register, or DDR may be used for any length TQ.

Broadcast mode executes 64 TQs from a single trigger.
"""

import random
import time
import copy
from time import perf_counter
from Common.fval import expected_failure, SkipTest

from ThirdParty.I2L.Ltc2975 import Ltc2975Commands, Ltc2975OnOffConfigBits

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper

MAX_ERROR_COUNT = 10
SETTLING_TIME = 0.01
TQ_COMMAND_BITS = 64
NUMBER_OF_BITS_IN_A_BYTE = 8
NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK = 100000
WAIT_BETWEEN_RETRIES_IN_SECONDS = 0.00002
WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER = 0.005
NUMBER_OF_MICROSECONDS_IN_A_SECOND = 1000000
TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS = 800
TQ_DELAY_ABSOLUTE_PERCENTAGE_ERROR_THRESHOLD = 5
TRIGGER_SEND_DELAY_MICROSECONDS = 50
MAX_NON_BROADCAST_DUT_ID = 62
NUMBER_OF_NON_BROADCAST_DUT_IDS = 63
TQ_BIRM_START_DUT_ID = 16
TQ_BIRM_END_DUT_ID = 63
NUMBER_OF_BROADCAST_DUT_IDS = 1
TOTAL_NUMBER_OF_DUT_IDS = NUMBER_OF_NON_BROADCAST_DUT_IDS + NUMBER_OF_BROADCAST_DUT_IDS
ALL_DUT_IDS = range(TOTAL_NUMBER_OF_DUT_IDS)
BIRM_DUT_IDS = range(16,64)
BROADCAST_DUT_ID = 63
NUMBER_OF_BITS_FOR_TQ_INDEX = 12
NUMBER_OF_BITS_IN_L11_N = 5
NUMBER_OF_BITS_IN_L11_Y = 11
NUMBER_OF_PAGES_IN_LTC2975 = 4
LTC2975_VOUT_SET_ERROR_THRESHOLD = 0.1
TQ_CONTENT_START_ADDRESS = 0x20000000
VOLTAGE_MARGIN = 0.1
TQ_ENGINE_COUNT = 17
NUMBER_OF_DUTS = 64


class BaseClass(HbiccTest):

    def i2c_data_readback(self, board):
        return board.read_slice_register(
            board.registers.PSDB_DC_TRIGGER_PAGE_I2C_DATA).value

    def spi_data_readback(self, board, target_psdb):
        if target_psdb == 0:
            return board.read_slice_register(board.registers.PSDB0_DC_TRIGGER_PAGE_SPI_DATA).data
        else:
            return board.read_slice_register(board.registers.PSDB1_DC_TRIGGER_PAGE_SPI_DATA).data

    def vmon_data_readback(self, target_psdb, dac_channel):
        if target_psdb == 0:
            return self.env.hbicc.psdb_0_pin_0.read_vref(channel_index=(dac_channel + 1))
        else:
            return self.env.hbicc.psdb_1_pin_0.read_vref(channel_index=(dac_channel + 1))

    def ad5684r_command_encoding(self, command_code, dac_select, voltage_to_be_applied):
        vref = 2.5
        voltage_in_dac_code = int(0xFFF * (voltage_to_be_applied / vref))
        dac_command_encoding_24bit = command_code << 20 | dac_select << 16 | voltage_in_dac_code << 4
        return dac_command_encoding_24bit

    def get_random_pmbusdevice_list(self):
        device_list = list(range(1, 5))
        random.shuffle(device_list)
        return device_list

    def report_errors(self, device, error_results):
        table = device.contruct_text_table(data=error_results)
        self.Log('error', 'Read/Write operation failed for following: ' + f'\n {table}')

    def report_pass_results(self, device, pass_results):
        table = device.contruct_text_table(data=pass_results)
        self.Log('info', 'Read/Write operation passed for following: ' + f'\n {table}')


class Diagnostics(BaseClass):
    """Interface robustness checks using PCIe register direct TQ execution"""

    def DirectedPSDB0Ltm4678BasicReadTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the pmbus revision number for each of the PMBus device (four of LTM 4678)
        and verify the read back value.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read for all
        the 4 devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register .
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each of the 4 devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        pmbus_revision_command = 0x98
        payload_byte_count = 1
        psdb_target = 0
        read_write_direction = 0
        command_data = 0
        expected_ltm4678_pmbus_revision_no = 0x22
        error_list = []
        device_list = self.get_random_pmbusdevice_list()
        for loop in range(test_iteration):
            for device in device_list:
                board.start_trigger_queue_record(psdb_target)
                board.create_trigger_queue_command_record(pmbus_revision_command, device, payload_byte_count,
                                                          psdb_target, read_write_direction, command_data)
                board.end_trigger_queue_record(psdb_target)
                readback_value = self.i2c_data_readback(board)
                if readback_value != expected_ltm4678_pmbus_revision_no:
                    error_list.append(
                        {'Device': device, 'Expected': hex(expected_ltm4678_pmbus_revision_no),
                         'Observed': hex(readback_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message(
                ' PMBUS read transaction for PSDB {} was successful for all {} iterations.'
                    .format(psdb_target, test_iteration), 'info')

    def DirectedPSDB1Ltm4678BasicReadTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the pmbus revision number for each of the PMBus device (four of LTM 4678)
         and verify the read back value.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read for all
         the 4 devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register .
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each of the 4 devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_1_pin_0 is None:
            raise SkipTest('MB/PSDB 1 unavailable')
        pmbus_revision_command = 0x98
        payload_byte_count = 1
        psdb_target = 1
        read_write_direction = 0
        command_data = 0
        expected_ltm4678_pmbus_revision_no = 0x22
        error_list = []
        device_list = self.get_random_pmbusdevice_list()
        for loop in range(test_iteration):
            for device in device_list:
                board.start_trigger_queue_record(psdb_target)
                board.create_trigger_queue_command_record(pmbus_revision_command, device, payload_byte_count,
                                                          psdb_target, read_write_direction, command_data)
                board.end_trigger_queue_record(psdb_target)
                readback_value = self.i2c_data_readback(board)
                if readback_value != expected_ltm4678_pmbus_revision_no:
                    error_list.append(
                        {'Device': device, 'Expected': hex(expected_ltm4678_pmbus_revision_no),
                         'Observed': hex(readback_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            self.Log('info', f'PMBUS read for PSDB {psdb_target} was ' +
                     f'successful for all {test_iteration} ' + 'iterations.')

    def DirectedPSDB0Ltm4678BasicWriteTest(self, test_iteration=1000):
        """Form a DC trigger queue record to write to user_scratch_pad04 register for each of the PMBus device (four LTM
        4678) and verify the read back value.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read for all
         the 4 devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register .
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each of the 4 devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_0_pin_0 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        user_04_scratch_pad_addr = 0xb4
        write_payload_byte_count = 3
        read_payload_byte_count = 2
        psdb_target = 0
        write = 1
        command_data = 0
        read = 0
        error_list = []
        device_list = self.get_random_pmbusdevice_list()
        for loop in range(test_iteration):
            for device in device_list:
                random_data_to_write = random.randrange(0, 0xFFFF)
                command_encoding_lower = random_data_to_write << user_04_scratch_pad_addr.bit_length() | user_04_scratch_pad_addr
                board.start_trigger_queue_record(psdb_target)
                board.create_trigger_queue_command_record(command_encoding_lower, device, write_payload_byte_count,
                                                          psdb_target, write, command_data)
                board.create_trigger_queue_command_record(user_04_scratch_pad_addr, device, read_payload_byte_count,
                                                          psdb_target, read, command_data)
                read_back_value = self.i2c_data_readback(board)
                board.end_trigger_queue_record(psdb_target)
                if read_back_value != random_data_to_write:
                    error_list.append(
                        {'Device': device, 'Expected': hex(random_data_to_write), 'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message(
                'PMBUS write for PSDB {} was successful for all {} iterations.'
                    .format(psdb_target, test_iteration), 'info')

    def DirectedPSDB1Ltm4678BasicWriteTest(self, test_iteration=1000):
        """Form a DC trigger queue record to write to user_scratch_pad04 register for each of the PMBus device (four LTM
        4678) and verify the read back value.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read for all
         the 4 devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register .
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each of the 4 devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_1_pin_0 is None:
            raise SkipTest('MB/PSDB 1 unavailable')
        user_04_scratch_pad_addr = 0xb4
        write_payload_byte_count = 3
        read_payload_byte_count = 2
        psdb_target = 1
        write = 1
        command_data = 0
        read = 0
        error_list = []
        device_list = self.get_random_pmbusdevice_list()
        for loop in range(test_iteration):
            for device in device_list:
                random_data_to_write = random.randrange(0, 0xFFFF)
                command_encoding_lower = random_data_to_write << \
                                         user_04_scratch_pad_addr.bit_length() | user_04_scratch_pad_addr
                board.start_trigger_queue_record(psdb_target)
                board.create_trigger_queue_command_record(command_encoding_lower, device, write_payload_byte_count,
                                                          psdb_target, write, command_data)
                board.create_trigger_queue_command_record(user_04_scratch_pad_addr, device, read_payload_byte_count,
                                                          psdb_target, read, command_data)
                read_back_value = self.i2c_data_readback(board)
                board.end_trigger_queue_record(psdb_target)
                if read_back_value != random_data_to_write:
                    error_list.append(
                        {'Device': device, 'Expected': hex(random_data_to_write), 'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            self.Log('info', f'PMBUS write for PSDB {psdb_target} was ' +
                     f'successful for all {test_iteration} ' + 'iterations.')

    def DirectedPSDB0Ltc2975BasicReadTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the pmbus revision number for PMBus device LTC2975 device and verify the
        readback value.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register .
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each of the 4 devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        pmbus_revision_command = 0x98
        payload_byte_count = 1
        psdb_target = 0
        read_write_direction = 0
        command_data = 0
        error_list = []
        expected_ltc2975_pmbus_revision_no = 0x11
        target_interface = 5
        for loop in range(test_iteration):
            board.start_trigger_queue_record(psdb_target)
            board.create_trigger_queue_command_record(pmbus_revision_command, target_interface, payload_byte_count,
                                                      psdb_target, read_write_direction, command_data)
            board.end_trigger_queue_record(psdb_target)
            read_back_value = self.i2c_data_readback(board)
            if read_back_value != expected_ltc2975_pmbus_revision_no:
                error_list.append(
                    {'Device': target_interface, 'Expected': hex(expected_ltc2975_pmbus_revision_no),
                     'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message(
                'LTC2975 read for PSDB {} was successful for all {} iterations.'.format(psdb_target, test_iteration),
                'info')

    def DirectedPSDB1Ltc2975BasicReadTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the pmbus revision number for PMBus device LTC2975 device and verify the
        readback value.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register .
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each of the 4 devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """

        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_1_pin_0 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        pmbus_revision_command = 0x98
        payload_byte_count = 1
        psdb_target = 1
        read_write_direction = 0
        command_data = 0
        error_list = []
        expected_ltc2975_pmbus_revision_no = 0x11
        target_interface = 5
        for loop in range(test_iteration):
            board.start_trigger_queue_record(psdb_target)
            board.create_trigger_queue_command_record(pmbus_revision_command, target_interface, payload_byte_count,
                                                      psdb_target, read_write_direction, command_data)
            board.end_trigger_queue_record(psdb_target)
            read_back_value = self.i2c_data_readback(board)
            if read_back_value != expected_ltc2975_pmbus_revision_no:
                error_list.append(
                    {'Device': target_interface, 'Expected': hex(expected_ltc2975_pmbus_revision_no),
                     'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            self.Log('info', f'LTC2975 read for PSDB {psdb_target} was ' +
                     f'successful for all {test_iteration} ' + 'iterations.')

    def DirectedPSDB0Ltc2975BasicWriteTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the capability command for the PMBus device (LTM  4678) and verify the
        read back value.

        **Test:**
        1. Using HIL API (`hil.hbiMbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiMbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        user_04_scratch_pad_addr = 0xb4
        write_payload_byte_count = 3
        read_payload_byte_count = 2
        psdb_target = 0
        write = 1
        command_data = 0
        read = 0
        error_list = []
        target_interface = 5
        for loop in range(test_iteration):
            random_data_to_write = random.randrange(0, 0xFFFF)
            command_encoding_lower = random_data_to_write << \
                                     user_04_scratch_pad_addr.bit_length() | user_04_scratch_pad_addr
            board.start_trigger_queue_record(psdb_target)
            board.create_trigger_queue_command_record(command_encoding_lower, target_interface,
                                                      write_payload_byte_count,
                                                      psdb_target, write, command_data)
            board.create_trigger_queue_command_record(user_04_scratch_pad_addr, target_interface,
                                                      read_payload_byte_count,
                                                      psdb_target, read, command_data)
            board.end_trigger_queue_record(psdb_target)
            read_back_value = self.i2c_data_readback(board)

            if read_back_value != random_data_to_write:
                error_list.append(
                    {'Device': target_interface, 'Expected': hex(random_data_to_write),
                     'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message(
                'LTC2975 write for PSDB {} was successful for all {} iterations.'.format(psdb_target, test_iteration),
                'info')

    def DirectedPSDB1Ltc2975BasicWriteTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the capability command for the PMBus device (LTM  4678) and verify the
        read back value.

        **Test:**
        1. Using HIL API (`hil.hbiMbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiMbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if self.env.hbicc.psdb_1_pin_0 is None:
            raise SkipTest('MB/PSDB 1 unavailable')
        user_04_scratch_pad_addr = 0xb4
        write_payload_byte_count = 3
        read_payload_byte_count = 2
        psdb_target = 1
        write = 1
        command_data = 0
        read = 0
        error_list = []
        target_interface = 5
        for loop in range(test_iteration):
            random_data_to_write = random.randrange(0, 0xFFFF)
            command_encoding_lower = random_data_to_write << \
                                     user_04_scratch_pad_addr.bit_length() | user_04_scratch_pad_addr
            board.start_trigger_queue_record(psdb_target)
            board.create_trigger_queue_command_record(command_encoding_lower, target_interface,
                                                      write_payload_byte_count, psdb_target, write, command_data)
            board.create_trigger_queue_command_record(user_04_scratch_pad_addr, target_interface,
                                                      read_payload_byte_count, psdb_target, read, command_data)
            read_back_value = self.i2c_data_readback(board)
            board.end_trigger_queue_record(psdb_target)
            if read_back_value != random_data_to_write:
                error_list.append(
                    {'Device': target_interface, 'Expected': hex(random_data_to_write),
                     'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message(
                'LTC2975 write for PSDB {} was successful for all {} iterations.'.format(psdb_target, test_iteration),
                'info')

    def DirectedPSDB0VCCREFBasicWriteReadTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the capability command for the PMBus device (LTM  4678) and verify the
        read back value.

        **Test:**
        1. Using HIL API (`hil.hbiMbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiMbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """

        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        error_list = []
        dac_list = [1, 2, 4, 8]
        psdb_target = 0
        spi_target = 0
        payload_byte_count = 3
        for loop in range(test_iteration):
            read_write = 1
            command_data = 0

            # voltage_to_set =0.76
            voltage_to_set = random.uniform(0.5, 1.7)
            # print(voltage_to_set)
            dac_select = random.choice(dac_list)
            write_and_update_cmdad5684r = 3
            payload_lower = self.ad5684r_command_encoding(write_and_update_cmdad5684r, dac_select, voltage_to_set)
            expected_read_back_value = (payload_lower >> 4) & 0xFFF
            board.start_trigger_queue_record(psdb_target)
            board.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count, psdb_target,
                                                      read_write, command_data)

            voltage_to_set = 0
            readback_enable_cmdad5684r = 9
            payload_lower = self.ad5684r_command_encoding(readback_enable_cmdad5684r, dac_select, voltage_to_set)
            board.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count, psdb_target,
                                                      read_write, command_data)

            nop_cmdad5684r = 0x0
            voltage_to_set = 0
            payload_lower = self.ad5684r_command_encoding(nop_cmdad5684r, dac_select, voltage_to_set)
            board.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count, psdb_target,
                                                      read_write, command_data)

            board.end_trigger_queue_record(psdb_target)
            read_back_value = self.spi_data_readback(board, psdb_target)
            # # print(hex(read_back_value))
            #
            # readback_vref = self.env.hbicc.psdb_0_pin_0.read_vref(8)
            # self.Log('info', '{} VREF FAb A{} value {}'.format(self.env.hbicc.psdb_0_pin_0.name(), 8, readback_vref))

            spi_data = read_back_value >> 4
            if spi_data != expected_read_back_value:
                error_list.append(
                    {'Device': spi_target, 'Expected': hex(expected_read_back_value), 'Observed': hex(spi_data)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message('SPI transaction for PSDB {} was successful for all {} iterations.'.format(
                psdb_target, test_iteration), 'info')

    def DirectedPSDB1VCCREFBasicWriteReadTest(self, test_iteration=1000):
        """Form a DC trigger queue record to read the capability command for the PMBus device (LTM  4678) and verify the
        read back value.

        **Test:**
        1. Using HIL API (`hil.hbiMbBarWrite()`), write a reference random value to *x* scratchpad register.
        2. Using HIL API (`hil.hbiMbBarRead()`), read the same *x* scratchpad register.
        3. Compare the reference value to the read back value
        3. Repeat step 1-3 for 1000 times for each register

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        board = self.env.hbicc.ring_multiplier
        if board is None or self.env.hbicc.psdb_1_pin_0 is None:
            raise SkipTest('MB/PSDB 1 unavailable')
        error_list = []
        dac_list = [1, 2, 4, 8]
        psdb_target = 1
        for loop in range(test_iteration):
            spi_target = 0
            payload_byte_count = 3
            read_write = 1
            command_data = 0

            voltage_to_set = random.uniform(0.5, 1.7)
            dac_select = random.choice(dac_list)
            write_and_update_cmdad5684r = 3
            payload_lower = self.ad5684r_command_encoding(write_and_update_cmdad5684r, dac_select, voltage_to_set)
            expected_read_back_value = (payload_lower >> 4) & 0xFFF
            board.start_trigger_queue_record(psdb_target)
            board.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count,
                                                      psdb_target, read_write, command_data)

            voltage_to_set = 0
            readback_enable_cmdad5684r = 9
            payload_lower = self.ad5684r_command_encoding(readback_enable_cmdad5684r, dac_select, voltage_to_set)
            board.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count,
                                                      psdb_target,
                                                      read_write, command_data)

            nop_cmdad5684r = 0x0
            voltage_to_set = 0
            payload_lower = self.ad5684r_command_encoding(nop_cmdad5684r, dac_select, voltage_to_set)
            board.create_trigger_queue_command_record(payload_lower, spi_target, payload_byte_count,
                                                      psdb_target,
                                                      read_write, command_data)

            read_back_value = self.spi_data_readback(board, psdb_target)
            # print(read_back_value)
            spi_data = read_back_value >> 4
            board.end_trigger_queue_record(psdb_target)
            if spi_data != expected_read_back_value:
                error_list.append(
                    {'Device': spi_target, 'Expected': hex(expected_read_back_value), 'Observed': hex(read_back_value)})
            if len(error_list) >= MAX_ERROR_COUNT:
                break
        if error_list:
            self.report_errors(board, error_list)
        else:
            board.log_device_message('SPI transaction for PSDB {} was successful for all {}.'.format(
                psdb_target, test_iteration), 'info')


class TQTestScenario:

    def __init__(self, test_case):
        self.scenario_functions = {'Read_Version_Via_DDR': self.generate_version_read_tq_via_DDR,
                                   'Read_Capability_Via_DDR': self.generate_capability_read_tq_via_DDR,
                                   'Write_Read_Scratch_Via_DDR': self.generate_scratch_write_read_tq_via_DDR,
                                   'Random_Delay_Via_TQ_DDR': self.generate_random_delay_tq_via_DDR,
                                   'Write_Read_VOut_Via_DDR': self.generate_vout_write_read_tq_via_DDR}
        self.tq_post_process_actions = {'Random_Delay_Via_TQ_DDR': self.delay_tq_post_process_action,
                                        'Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Processed_Count_Check':
                                            self.broadcast_delay_tq_processed_count_check_post_process_action,
                                        'Broadcast_Trigger_Via_TQ_DDR_Scratch_Write_Check':
                                            self.broadcast_scratch_write_check_post_process_action,
                                        'Write_Read_VOut_Via_DDR': self.vout_write_read_tq_post_process_action}
        self.execute_scenario_actions = {'Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Processed_Count_Check':
                                             self.broadcast_execute_scenario_for_device_delay_tq_processed_count_check,
                                         'Broadcast_Trigger_Via_TQ_DDR_Scratch_Write_Check':
                                             self.broadcast_execute_scenario_for_device_scratch_write_check}
        self.psdbTarget = 0
        self.noOfTestIterations = 0
        self.allScenarios = []
        self.scenarioName = ''
        self.deviceId = 0
        self.env = test_case.env
        self.testClass = test_case
        self.rm_device = test_case.rm_device
        self.rctc = test_case.env.hbicc.rc
        self.Log = test_case.Log
        self.tqProcessStartTimeMicroseconds = 0
        self.tqProcessEndTimeMicroseconds = 0
        self.tqProcessedCount = 0
        self.currentIteration = 0
        self.numberOfTQsExecutionExpected = 0
        self.expectedDataList = list()
        self.observedDataList = list()
        self.errorList = list()

    def set_psdb_target(self, psdb_target):
        self.psdbTarget = psdb_target

    def get_psdb_target(self):
        return self.psdbTarget

    def set_no_of_test_iterations(self, no_of_test_iterations):
        self.noOfTestIterations = no_of_test_iterations

    def get_no_of_test_iterations(self):
        return self.noOfTestIterations

    def set_all_scenarios_list(self, all_scenarios):
        self.allScenarios = all_scenarios

    def get_all_scenarios_list(self):
        return self.allScenarios

    def set_scenario_name(self, scenario_name):
        self.scenarioName = scenario_name

    def get_scenario_name(self):
        return self.scenarioName

    def set_device_id(self, device_id):
        self.deviceId = device_id

    def get_device_id(self):
        return self.deviceId

    def add_to_expected_data(self, expected_data):
        self.expectedDataList.append(expected_data)

    def get_all_expected_data(self):
        return self.expectedDataList

    def add_to_observed_data(self, observed_data):
        self.observedDataList.append(observed_data)

    def get_all_observed_data(self):
        return self.observedDataList

    def set_tq_process_start_time_microseconds(self, tq_process_start_time):
        self.tqProcessStartTimeMicroseconds = tq_process_start_time

    def get_tq_process_start_time_microseconds(self):
        return self.tqProcessStartTimeMicroseconds

    def set_tq_process_end_time_microseconds(self, tq_process_start_time):
        self.tqProcessEndTimeMicroseconds = tq_process_start_time

    def get_tq_process_end_time_microseconds(self):
        return self.tqProcessEndTimeMicroseconds

    def set_tq_processed_count(self, tq_processed_count):
        self.tqProcessedCount = tq_processed_count

    def get_tq_processed_count(self):
        return self.tqProcessedCount

    def execute(self):
        self.errorList = list()
        for test_iteration in range(self.get_no_of_test_iterations()):
            self.currentIteration = test_iteration
            device_list = self.get_pmbus_device_list_randomized()
            for device in device_list:
                self.set_device_id(device)
                for scenario in self.get_all_scenarios_list():
                    self.expectedDataList = list()
                    self.observedDataList = list()
                    self.set_scenario_name(scenario)
                    self.execute_scenario_actions.get(scenario, self.default_execute_scenario_for_device)()
                    if len(self.errorList) >= MAX_ERROR_COUNT:
                        return

    def broadcast_execute_scenario_for_device_delay_tq_processed_count_check(self):
        random_index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        self.prepare_blank_delay_tqs_all_duts_for_broadcast_trigger_exec(random_index_of_tq_to_fire)
        trigger_register_data = self.generate_aurora_trigger_for_dut(BROADCAST_DUT_ID, random_index_of_tq_to_fire)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def broadcast_execute_scenario_for_device_scratch_write_check(self):
        random_index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        self.prepare_scratch_write_and_blank_delay_tqs_for_broadcast_trigger_exec(random_index_of_tq_to_fire)
        trigger_register_data = self.generate_aurora_trigger_for_dut(BROADCAST_DUT_ID, random_index_of_tq_to_fire)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def prepare_blank_delay_tqs_all_duts_for_broadcast_trigger_exec(self, random_index_of_tq_to_fire):
        self.set_up_blank_delay_tqs_for_active_dut_ids_at_byte_offset(ALL_DUT_IDS, random_index_of_tq_to_fire)
        self.numberOfTQsExecutionExpected = TOTAL_NUMBER_OF_DUT_IDS

    def set_up_blank_delay_tqs_for_active_dut_ids_at_byte_offset(self, active_duts, random_index_of_tq_to_fire):
        for dut_id in active_duts:
            self.set_up_tq_for_blank_delay(dut_id, random_index_of_tq_to_fire)
        no_of_trigger_queues_to_be_processed_on_broadcast_trigger_receipt = len(active_duts)
        self.add_to_expected_data(no_of_trigger_queues_to_be_processed_on_broadcast_trigger_receipt)

    def prepare_scratch_write_and_blank_delay_tqs_for_broadcast_trigger_exec(self, random_index_of_tq_to_fire):
        random_dut_ids_for_scratch_write_tq, other_dut_ids_for_blank_delay_tq = \
            self.get_random_dut_ids_for_scratch_write_and_blank_delay_tqs()
        dut_id_for_user_data_03_write = random_dut_ids_for_scratch_write_tq[0]
        self.set_up_tq_for_scratch_write('USER_DATA_03', dut_id_for_user_data_03_write, random_index_of_tq_to_fire)
        dut_id_for_user_data_04_write = random_dut_ids_for_scratch_write_tq[1]
        self.set_up_tq_for_scratch_write('USER_DATA_04', dut_id_for_user_data_04_write, random_index_of_tq_to_fire)
        for dut_id in other_dut_ids_for_blank_delay_tq:
            self.set_up_tq_for_blank_delay(dut_id, random_index_of_tq_to_fire)
        self.numberOfTQsExecutionExpected = TOTAL_NUMBER_OF_DUT_IDS

    def set_up_tq_for_scratch_write(self, scratch_register_name, dut_id, index_of_tq_to_fire):
        trigger_queue_bits = self.generate_scratch_write_tq_via_DDR(scratch_register_name)
        trigger_register_data = self.generate_aurora_trigger_for_dut(dut_id, index_of_tq_to_fire)
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        return trigger_queue_bits

    def set_up_tq_for_blank_delay(self, dut_id, index_of_tq_to_fire):
        trigger_queue_bits = self.generate_blank_delay_tq_via_DDR()
        trigger_register_data = self.generate_aurora_trigger_for_dut(dut_id, index_of_tq_to_fire)
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        return trigger_queue_bits

    def get_random_dut_ids_for_scratch_write_and_blank_delay_tqs(self):
        all_duts = list(ALL_DUT_IDS)
        all_birm_duts = list(BIRM_DUT_IDS)
        random.shuffle(all_birm_duts)
        random_dut_ids_for_scratch_write_tq = all_birm_duts[:2]
        other_dut_ids_for_blank_delay_tq = list(set(all_duts)-set(random_dut_ids_for_scratch_write_tq))
        return random_dut_ids_for_scratch_write_tq, other_dut_ids_for_blank_delay_tq

    def default_execute_scenario_for_device(self):
        trigger_queue_bits = self.generate_trigger_queue_for_scenario()
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        self.send_trigger_from_rctc(trigger_register_data)
        self.evaluate_results_on_tq_process_completion()

    def evaluate_results_on_tq_process_completion(self):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.is_tq_processing_complete():
                self.tq_post_process_actions.get(self.get_scenario_name(), self.default_tq_post_process_action)()
                self.verify_tq_response()
                break
        else:
            self.errorList.append(
                {'Iteration': self.currentIteration, 'Device': self.get_device_id(),
                 'Expected': f'TQs Processed: {self.get_all_expected_data()[0]}',
                 'Observed': f'TQs Processed: {self.check_tq_processed_counter() - self.get_tq_processed_count()}'})

    def get_results_on_tq_process_completion(self):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.is_unicast_tq_processing_complete():
                self.get_observed_data_from_tq_execution()
                break
        else:
            self.errorList.append(
                {'Iteration': self.currentIteration, 'Device': self.get_device_id(),
                 'Expected': 'Read TQ Processed', 'Observed': 'Read TQ NOT processed'})

    def broadcast_scratch_write_check_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.read_data_from_scratch_using_trigger_queue_via_DDR('USER_DATA_03')
        self.read_data_from_scratch_using_trigger_queue_via_DDR('USER_DATA_04')

    def read_data_from_scratch_using_trigger_queue_via_DDR(self, scratch_register_name):
        trigger_queue_bits = self.generate_scratch_read_tq_via_DDR(scratch_register_name)
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        self.save_trigger_queue_to_memory(trigger_queue_bits, trigger_register_data)
        self.send_trigger_from_rctc(trigger_register_data)
        self.get_results_on_tq_process_completion()

    def get_observed_data_from_tq_execution(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
        self.add_to_observed_data(self.testClass.i2c_data_readback(self.rm_device))

    def broadcast_delay_tq_processed_count_check_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.add_to_observed_data(self.check_tq_processed_counter() - self.get_tq_processed_count())

    def delay_tq_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        if (self.calculate_absolute_error_in_observed_delay() < TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS) \
                or (self.calculate_abs_perc_error_in_observed_delay() < TQ_DELAY_ABSOLUTE_PERCENTAGE_ERROR_THRESHOLD):
            self.add_to_observed_data(self.get_all_expected_data()[0])
        else:
            self.add_to_observed_data(self.get_trigger_queue_process_time())

    def vout_write_read_tq_post_process_action(self):
        pass

    def default_tq_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
        self.add_to_observed_data(self.testClass.i2c_data_readback(self.rm_device))

    def calculate_absolute_error_in_observed_delay(self):
        return abs(self.get_trigger_queue_process_time() - self.get_all_expected_data()[0])

    def calculate_abs_perc_error_in_observed_delay(self):
        return self.calculate_absolute_error_in_observed_delay() / self.get_all_expected_data()[0] * 100

    def get_trigger_queue_process_time(self):
        return int(self.get_tq_process_end_time_microseconds() - TRIGGER_SEND_DELAY_MICROSECONDS
                   - self.get_tq_process_start_time_microseconds())

    def wait_for_seconds(self, wait_time_secs):
        start_time = perf_counter()
        while True:
            curr_time = perf_counter()
            if (curr_time - start_time) > wait_time_secs:
                return

    def report_results(self):
        if self.errorList:
            self.Log('error', f'PMBUS transaction using trigger queue for PSDB {self.get_psdb_target()} failed for '
                              f'{len(self.errorList)} tests.')
            self.testClass.report_errors(self.rm_device, self.errorList)
        else:
            self.Log('info', f'PMBUS transaction using trigger queue for PSDB {self.get_psdb_target()} was successful '
                             f'for all {self.get_no_of_test_iterations()} iterations.')

    def get_pmbus_device_list_randomized(self):
        pass

    def generate_trigger_queue_for_scenario(self, scenario_name=None):
        if scenario_name is None:
            scenario_name = self.get_scenario_name()
        scenario_function_to_call = self.scenario_functions.get(scenario_name)
        return scenario_function_to_call()

    def generate_version_read_tq_via_DDR(self):
        trigger_queue = self.version_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(self.get_version_read_expected_data())
        return trigger_queue_bits

    def generate_capability_read_tq_via_DDR(self):
        trigger_queue = self.capability_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(self.get_capability_read_expected_data())
        return trigger_queue_bits

    def capability_read_tq_commands(self):
        trigger_queue = list()
        tq_command_capability_read = self.capability_read_trigger_queue_record()
        trigger_queue.append(tq_command_capability_read)
        return trigger_queue

    def version_read_tq_commands(self):
        trigger_queue = list()
        tq_command_version_read = self.version_read_trigger_queue_record()
        trigger_queue.append(tq_command_version_read)
        return trigger_queue

    def generate_scratch_write_read_tq_via_DDR(self):
        trigger_queue, random_data_to_be_written = self.scratch_write_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(random_data_to_be_written)
        return trigger_queue_bits

    def generate_blank_delay_tq_via_DDR(self):
        trigger_queue = self.delay_tq_commands(delay_us=0)
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        return trigger_queue_bits

    def generate_scratch_write_tq_via_DDR(self, scratch_register_name):
        trigger_queue, random_data_to_be_written = self.scratch_write_tq_commands(scratch_register_name)
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(random_data_to_be_written)
        return trigger_queue_bits

    def generate_scratch_read_tq_via_DDR(self, scratch_register_name):
        trigger_queue = self.scratch_read_tq_commands(scratch_register_name)
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        return trigger_queue_bits

    def generate_random_delay_tq_via_DDR(self):
        trigger_queue, delay_us = self.random_delay_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(delay_us)
        return trigger_queue_bits

    def generate_tq_via_DDR(self, trigger_queue):
        tq_start = self.start_trigger_queue_record()
        tq_end = self.end_trigger_queue_record()
        trigger_queue_bits = self.create_ddr_trigger_queue_bits(tq_end, tq_start, trigger_queue)
        return trigger_queue_bits

    def create_ddr_trigger_queue_bits(self, tq_end, tq_start, trigger_queue):
        trigger_queue_bits = tq_end
        for trigger_record in trigger_queue:
            trigger_queue_bits = trigger_queue_bits << TQ_COMMAND_BITS | trigger_record
        trigger_queue_bits = trigger_queue_bits << TQ_COMMAND_BITS | tq_start
        return trigger_queue_bits

    def scratch_write_tq_commands(self, scratch_name):
        trigger_queue = list()
        if self.currentIteration == 0:
            tq_command_page_write = self.page_write_trigger_queue_record(0x00)
            trigger_queue.append(tq_command_page_write)
            tq_command_delay = self.delay_trigger_queue_record(delay=50000)
            trigger_queue.append(tq_command_delay)
        tq_command_scratch_write, random_data_to_be_written = self.scratch_write_trigger_queue_record(scratch_name)
        trigger_queue.append(tq_command_scratch_write)
        return reversed(trigger_queue), random_data_to_be_written

    def scratch_read_tq_commands(self, scratch_name):
        trigger_queue = list()
        tq_command_scratch_read = self.scratch_read_trigger_queue_record(scratch_name)
        trigger_queue.append(tq_command_scratch_read)
        return trigger_queue

    def scratch_write_read_tq_commands(self):
        trigger_queue = list()
        scratch_names = ['USER_DATA_03', 'USER_DATA_04']
        random.shuffle(scratch_names)
        selected_scratch = scratch_names[0]
        if self.currentIteration == 0:
            tq_command_page_write = self.page_write_trigger_queue_record(0x00)
            trigger_queue.append(tq_command_page_write)
            tq_command_delay = self.delay_trigger_queue_record(delay=50000)
            trigger_queue.append(tq_command_delay)
        tq_command_scratch_write, random_data_to_be_written = self.scratch_write_trigger_queue_record(selected_scratch)
        trigger_queue.append(tq_command_scratch_write)
        tq_command_scratch_read = self.scratch_read_trigger_queue_record(selected_scratch)
        trigger_queue.append(tq_command_scratch_read)
        return reversed(trigger_queue), random_data_to_be_written

    def random_delay_tq_commands(self):
        delay_us = random.randrange(0x0000, 0xFFFF)
        return self.delay_tq_commands(delay_us), delay_us

    def delay_tq_commands(self, delay_us):
        trigger_queue = list()
        tq_command_delay = self.delay_trigger_queue_record(delay=delay_us)
        trigger_queue.append(tq_command_delay)
        return trigger_queue

    def start_trigger_queue_record(self):
        return self.rm_device.start_trigger_queue_record(psdb_target=self.get_psdb_target(), dma=True)

    def end_trigger_queue_record(self):
        return self.rm_device.end_trigger_queue_record(psdb_target=self.get_psdb_target(), dma=True)

    def delay_trigger_queue_record(self, delay):
        return self.rm_device.delay_within_trigger_queue_record(psdb_target=self.get_psdb_target(), dma=True,
                                                                delay=delay, plb_count=2)

    def version_read_trigger_queue_record(self):
        pmbus_command_code = self.get_version_read_command_code()
        return self.rm_device.create_trigger_queue_command_record(payload_lower=pmbus_command_code,
                                                                  target_interface=self.get_device_id(),
                                                                  payload_byte_count=1,
                                                                  psdb_target=self.get_psdb_target(),
                                                                  read_write=0, command_data=0, dma=True)

    def capability_read_trigger_queue_record(self):
        pmbus_command_code = self.get_capability_read_command_code()
        return self.rm_device.create_trigger_queue_command_record(payload_lower=pmbus_command_code,
                                                                  target_interface=self.get_device_id(),
                                                                  payload_byte_count=1,
                                                                  psdb_target=self.get_psdb_target(), read_write=0,
                                                                  command_data=0, dma=True)

    def scratch_write_trigger_queue_record(self, scratch_name):
        pmbus_write_to_scratch_address = self.get_scratch_write_command_code(scratch_name)
        random_data_to_write = random.randrange(0, 0xFFFF)
        pmbus_write_to_scratch_command_encoding = \
            (random_data_to_write << pmbus_write_to_scratch_address.bit_length()) | pmbus_write_to_scratch_address
        tq_command_scratch_write = self.rm_device.create_trigger_queue_command_record(
            payload_lower=pmbus_write_to_scratch_command_encoding, target_interface=self.get_device_id(),
            payload_byte_count=3, psdb_target=self.get_psdb_target(),
            read_write=1, command_data=0, dma=True)
        return tq_command_scratch_write, random_data_to_write

    def scratch_read_trigger_queue_record(self, scratch_name):
        pmbus_read_from_scratch_address = self.get_scratch_read_command_code(scratch_name)
        return self.rm_device.create_trigger_queue_command_record(
            payload_lower=pmbus_read_from_scratch_address, target_interface=self.get_device_id(),
            payload_byte_count=2, psdb_target=self.get_psdb_target(), read_write=0,
            command_data=0, dma=True)

    def page_write_trigger_queue_record(self, page_id=0x00):
        pmbus_command_write_to_page = 0x00
        pmbus_write_to_page_command_encoding = (page_id << NUMBER_OF_BITS_IN_A_BYTE) | pmbus_command_write_to_page
        tq_command_page_write = self.rm_device.create_trigger_queue_command_record(
            payload_lower=pmbus_write_to_page_command_encoding, target_interface=self.get_device_id(),
            payload_byte_count=2, psdb_target=self.get_psdb_target(),
            read_write=1, command_data=0, dma=True)
        return tq_command_page_write

    def get_version_read_command_code(self):
        pass

    def get_capability_read_command_code(self):
        pass

    def get_version_read_expected_data(self):
        pass

    def get_capability_read_expected_data(self):
        pass

    def get_scratch_write_command_code(self, scratch_name):
        pass

    def get_scratch_read_command_code(self, scratch_name):
        pass

    def generate_vout_write_read_tq_via_DDR(self):
        pass

    def save_trigger_queue_to_memory(self, trigger_queue_record, trigger_register_data):
        lut_address = self.calculate_lut_address(trigger_register_data.index_of_tq_to_fire,
                                                 trigger_register_data.dut_id)
        tq_record_start_address = self.dma_tq_record_address_to_lut(trigger_queue_record, lut_address)
        self.send_trigger_queue_to_memory(tq_record_start_address, trigger_queue_record)

    def save_trigger_queue_to_memory_with_queue_record_start_address(self, trigger_queue_record, trigger_register_data,
                                                                     queue_record_start_address):
        lut_address = self.calculate_lut_address(trigger_register_data.index_of_tq_to_fire,
                                                 trigger_register_data.dut_id)
        tq_record_start_address = self.dma_tq_record_address_to_lut_with_start_adress(trigger_queue_record, lut_address,
                                                                                      queue_record_start_address)
        self.send_trigger_queue_to_memory(tq_record_start_address, trigger_queue_record)

    def save_broadcast_trigger_queue_to_memory(self, trigger_queue_record, trigger_register_data):
        self.dma_broadcast_dummy_tq_store(trigger_register_data.index_of_tq_to_fire)
        queue_record_start_address = 0x4000002
        self.save_trigger_queue_to_memory_with_queue_record_start_address(trigger_queue_record, trigger_register_data,
                                                                          queue_record_start_address)

    def dma_broadcast_dummy_tq_store(self, index):
        for dut_id in range(NUMBER_OF_NON_BROADCAST_DUT_IDS):
            lut_address = self.calculate_lut_address(index, dut_id)
            self.dma_lut_update(lut_address, TQ_CONTENT_START_ADDRESS)
        dummy_tq = (self.end_trigger_queue_record() << TQ_COMMAND_BITS) | self.start_trigger_queue_record()
        self.send_trigger_queue_to_memory(TQ_CONTENT_START_ADDRESS, dummy_tq)

    def dma_lut_update(self, lut_address, tq_address):
        self.rm_device.dma_write(lut_address, tq_address.to_bytes(8, byteorder='little'))

    def calculate_lut_address(self, index_of_tq_to_fire, dut_id):
        return (index_of_tq_to_fire + (dut_id << 20)) << 3

    def send_trigger_from_rctc(self, trigger_register_data):
        rctc_device = self.rctc
        trigger_link_index = 17
        processed_tqs_count_before_trigger_send = self.check_tq_processed_counter()
        self.set_tq_processed_count(processed_tqs_count_before_trigger_send)
        trigger_pre = self.rm_device.read_trigger()
        rctc_device.send_trigger(trigger_register_data.value, trigger_link_index)
        self.set_tq_process_start_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.post_trigger_send_action(trigger_register_data, trigger_pre)

    def post_trigger_send_action(self, trigger_register_data, trigger_pre):
        received_trigger_on_rm = self.rm_device.read_trigger()
        if received_trigger_on_rm != trigger_register_data.value:
            self.Log('error', f'Correct Trigger NOT received at RM. Received 0x{hex(received_trigger_on_rm)} '
                              f'instead of 0x{hex(trigger_register_data.value)} '
                              f'trigger_pre 0x{hex(trigger_pre)}')

    def generate_aurora_trigger_for_random_non_broadcast_dut(self, index_of_tq_to_fire=None):
        random_dut_id = random.randrange(TQ_BIRM_START_DUT_ID, TQ_BIRM_END_DUT_ID)
        return self.generate_aurora_trigger_for_dut(random_dut_id, index_of_tq_to_fire)

    def generate_aurora_trigger_for_dut(self, dut_id, index_of_tq_to_fire=None):
        if index_of_tq_to_fire is None:
            index_of_tq_to_fire = random.randint(0, 2 ** NUMBER_OF_BITS_FOR_TQ_INDEX - 1)
        return self.generate_aurora_trigger(dut_id, index_of_tq_to_fire)

    def generate_aurora_trigger(self, dut_id, index_of_tq_to_fire):
        trigger_register_data = self.rm_device.registers.LAST_TRIGGER_SEEN()
        trigger_register_data.target_source = 0x3f
        trigger_register_data.dut_id = dut_id
        trigger_register_data.sw_up_trigger = 0
        trigger_register_data.reserved = 0x1E8  # set field "reserved" to 0x1E8 to facilitate SignalTap capture
        trigger_register_data.index_of_tq_to_fire = index_of_tq_to_fire
        return trigger_register_data

    def dma_tq_record_address_to_lut(self, trigger_queue_record, lut_address):
        no_of_command = (trigger_queue_record.bit_length() + 63) // TQ_COMMAND_BITS
        queue_record_start_address = 0x4000000
        queue_record_end_address = (0x7ffffff - no_of_command)
        tq_record_start_address = random.randint(queue_record_start_address, queue_record_end_address)
        start_address_multiple_of_four = tq_record_start_address * 8
        tq_record_start_address_in_byte = start_address_multiple_of_four.to_bytes(8, byteorder='little')
        self.rm_device.dma_write(lut_address, tq_record_start_address_in_byte)
        return start_address_multiple_of_four

    def dma_tq_record_address_to_lut_with_start_adress(self, trigger_queue_record, lut_address,
                                                       queue_record_start_address):
        no_of_command = (trigger_queue_record.bit_length() + 63) // TQ_COMMAND_BITS
        queue_record_end_address = (0x7ffffff - no_of_command)
        tq_record_start_address = random.randint(queue_record_start_address, queue_record_end_address)
        start_address_multiple_of_four = tq_record_start_address * 8
        tq_record_start_address_in_byte = start_address_multiple_of_four.to_bytes(8, byteorder='little')
        self.rm_device.dma_write(lut_address, tq_record_start_address_in_byte)
        return start_address_multiple_of_four

    def send_trigger_queue_to_memory(self, tq_record_start_address, trigger_queue_record):
        no_of_bytes = (trigger_queue_record.bit_length() + 7) // NUMBER_OF_BITS_IN_A_BYTE
        tq_data_in_byte = trigger_queue_record.to_bytes(no_of_bytes, byteorder='little')
        self.rm_device.dma_write(tq_record_start_address, tq_data_in_byte)
        return tq_data_in_byte

    def is_tq_processing_complete(self):
        if self.is_broadcast_scenario():
            return self.is_broadcast_tq_processing_complete(self.numberOfTQsExecutionExpected)
        else:
            return self.is_unicast_tq_processing_complete()

    def is_broadcast_scenario(self):
        return self.scenarioName[:9] == 'Broadcast'

    def is_broadcast_tq_processing_complete(self, number_of_duts_with_stored_tqs):
        return self.check_tq_processed_counter() >= (self.get_tq_processed_count() + number_of_duts_with_stored_tqs)

    def is_unicast_tq_processing_complete(self):
        return self.check_tq_processed_counter() > self.get_tq_processed_count()

    def check_tq_processed_counter(self):
        return self.rm_device.read_dc_trigger_page_processed_queues()

    def verify_tq_response(self):
        expected_data_list = self.get_all_expected_data()
        observed_data_list = self.get_all_observed_data()
        for i in range(len(expected_data_list)):
            expected_data = expected_data_list[i]
            observed_data = observed_data_list[i]
            if observed_data != expected_data:
                self.log_tq_failure_status(expected_data, observed_data)
                self.errorList.append(
                    {'Iteration': self.currentIteration, 'Device': self.get_device_id(),
                     'Expected': hex(expected_data), 'Observed': hex(observed_data)})

    def log_tq_failure_status(self, expected_data, observed_data):
        self.env.hbicc.ring_multiplier.log_i2c_alarm()
        self.env.hbicc.ring_multiplier.reset_dctq_i2c_bit()
        self.env.hbicc.ring_multiplier.log_i2c_alarm()
        self.Log('info', f'Iteration: {self.currentIteration} '
                         f'Device: {self.get_device_id()} '
                         f'Expected: {hex(expected_data)} '
                         f'Observed: {hex(observed_data)}')


class TQTestScenarioLTM4678(TQTestScenario):
    def __init__(self, test_case):
        super().__init__(test_case)
        self.scratch_addresses = {'USER_DATA_03': 0xB3, 'USER_DATA_04': 0xB4}

    def get_pmbus_device_list_randomized(self):
        device_list = list(range(1, 5))
        random.shuffle(device_list)
        return device_list[0:1]

    def get_version_read_command_code(self):
        return 0x98

    def get_capability_read_command_code(self):
        return 0x19

    def get_version_read_expected_data(self):
        return 0x22

    def get_capability_read_expected_data(self):
        return 0xB0

    def get_scratch_write_command_code(self, scratch_name):
        return self.scratch_addresses.get(scratch_name, 0xB3)

    def get_scratch_read_command_code(self, scratch_name):
        return self.scratch_addresses.get(scratch_name, 0xB3)


class TQTestScenarioLTC2975(TQTestScenario):

    def __init__(self, test_case):
        super().__init__(test_case)
        self.scratch_addresses = {'USER_DATA_03': 0xB3, 'USER_DATA_04': 0xB4}

    def vout_write_read_tq_post_process_action(self):
        self.set_tq_process_end_time_microseconds(time.perf_counter() * NUMBER_OF_MICROSECONDS_IN_A_SECOND)
        self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
        voltage_read = self.from_L16(self.testClass.i2c_data_readback(self.rm_device))
        voltage_set = self.from_L16(self.get_all_expected_data()[0])
        if abs(voltage_read - voltage_set) < LTC2975_VOUT_SET_ERROR_THRESHOLD:
            self.add_to_observed_data(self.to_L16(voltage_set))
        else:
            self.add_to_observed_data(self.to_L16(voltage_read))

    def get_pmbus_device_list_randomized(self):
        device_list = [5]
        return device_list

    def get_version_read_command_code(self):
        return 0x98

    def get_capability_read_command_code(self):
        return 0x19

    def get_version_read_expected_data(self):
        return 0x11

    def get_capability_read_expected_data(self):
        return 0xB0

    def get_scratch_write_command_code(self, scratch_name):
        return self.scratch_addresses.get(scratch_name, 0xB3)

    def get_scratch_read_command_code(self, scratch_name):
        return self.scratch_addresses.get(scratch_name, 0xB3)

    """NON-PAGED COMMANDS"""

    def _set_write_protect(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.WRITE_PROTECT.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_vin_on(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_ON.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_vin_off(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_OFF.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_vin_ov_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_OV_FAULT_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_vin_ov_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_OV_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_vin_ov_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_OV_WARN_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_vin_uv_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_UV_WARN_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_vin_uv_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_UV_FAULT_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_watchdog_t_first(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_WATCHDOG_T_FIRST.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_watchdog_t(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_WATCHDOG_T.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_vin_uv_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VIN_UV_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_user_data_00(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.USER_DATA_00.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_user_data_02(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.USER_DATA_02.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_mfr_iin_cal_gain_tc(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_IIN_CAL_GAIN_TC.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_user_data_04(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.USER_DATA_04.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_mfr_ein_config(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_EIN_CONFIG.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_config_all_ltc2975(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_CONFIG_ALL_LTC2975.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_mfr_pwrgd_en(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_PWRGD_EN.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_mfr_faultb0_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_FAULTB0_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_faultb1_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_FAULTB1_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_config2_ltc2975(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_CONFIG2_LTC2975.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_config3_ltc2975(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_CONFIG3_LTC2975.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_retry_delay(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_RETRY_DELAY.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_restart_delay(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_RESTART_DELAY.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_pwrgood_assertion_delay(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_POWERGOOD_ASSERTION_DELAY.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_page_ff_mask(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_PAGE_FF_MASK.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_iin_cal_gain(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_IIN_CAL_GAIN.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_retry_count(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_RETRY_COUNT.value,
            command_value=val, read_write=1, no_of_bytes=2)

    """PAGED COMMANDS"""

    def _set_page(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.PAGE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_on_off_config(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.ON_OFF_CONFIG.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_operation(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.OPERATION.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_vout_max(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_MAX.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_pwr_good_on(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.POWER_GOOD_ON.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_pwr_good_off(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.POWER_GOOD_OFF.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_vout_ov_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_OV_FAULT_LIMIT.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_vout_ov_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_OV_WARN_LIMIT.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_vout_uv_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_UV_WARN_LIMIT.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_vout_uv_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_UV_FAULT_LIMIT.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_vout_ov_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_OV_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_vout_uv_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_UV_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_iout_oc_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.IOUT_OC_WARN_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_iout_oc_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.IOUT_OC_FAULT_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_iout_oc_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.IOUT_OC_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_iout_uc_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.IOUT_UC_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_iot_cal_gain(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.IOUT_CAL_GAIN.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_ot_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.OT_WARN_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_ot_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.OT_FAULT_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_ut_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.UT_FAULT_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_ot_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.OT_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_ut_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.UT_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_ton_max_fault_response(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.TON_MAX_FAULT_RESPONSE.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _set_ut_warn_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.UT_WARN_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_ton_delay(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.TON_DELAY.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_ton_rise(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.TON_RISE.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_toff_delay(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.TOFF_DELAY.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_iout_cal_gain_tau_inv(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_IOUT_CAL_GAIN_TAU_INV.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_iout_cal_gain_theta(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_IOUT_CAL_GAIN_THETA.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_ton_max_fault_limit(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.TON_MAX_FAULT_LIMIT.value,
            command_value=self.to_L11(val), read_write=1, no_of_bytes=3)

    def _set_mfr_faultbo_propagate(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_FAULTB0_PROPAGATE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_faultb1_propagate(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_FAULTB1_PROPAGATE.value,
            command_value=val, read_write=1, no_of_bytes=2)

    def _set_mfr_config_ltc2975(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.MFR_CONFIG_LTC2975.value,
            command_value=val, read_write=1, no_of_bytes=3)

    def _clear_faults(self):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.CLEAR_FAULTS.value,
            command_value=0, read_write=1, no_of_bytes=1)

    def _set_vout(self, val):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.VOUT_COMMAND.value,
            command_value=self.to_L16(val), read_write=1, no_of_bytes=3)

    def _set_power_state(self, turn_on):
        if turn_on:
            return self._set_operation(0x80)
        else:
            return self._set_operation(0x00)

    def _read_vout(self):
        return self.create_ltc2975_command_tq_record(
            command_code=Ltc2975Commands.READ_VOUT.value,
            command_value=0, read_write=0, no_of_bytes=2)

    def create_ltc2975_command_tq_record(self, command_code, command_value, read_write, no_of_bytes, dma=True):
        if read_write:
            tq_payload = (command_value << NUMBER_OF_BITS_IN_A_BYTE) | command_code
        else:
            tq_payload = command_code
        tq_record = self.rm_device.create_trigger_queue_command_record(
            payload_lower=tq_payload, target_interface=self.get_device_id(),
            payload_byte_count=no_of_bytes, psdb_target=self.get_psdb_target(),
            read_write=read_write, command_data=0, dma=dma)
        return tq_record

    def generate_vout_write_read_tq_via_DDR(self):
        trigger_queue, vout_written = self.vout_write_read_tq_commands()
        trigger_queue_bits = self.generate_tq_via_DDR(trigger_queue)
        self.add_to_expected_data(vout_written)
        return trigger_queue_bits

    def vout_write_read_tq_commands(self):
        trigger_queue = self.non_page_specific_initialization_commands()
        for page in range(NUMBER_OF_PAGES_IN_LTC2975):
            trigger_queue = self.page_specific_initialization_commands(page, trigger_queue)
        tq_command_delay = self.delay_trigger_queue_record(delay=50000)
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(self._set_page(0x01))
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(self._set_power_state(True))
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(tq_command_delay)
        vout_to_write = random.uniform(0.75, 1.25)
        trigger_queue.append(self._set_vout(vout_to_write))
        vout_written_l16 = self.to_L16(vout_to_write)
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(tq_command_delay)
        tq_command_vout_read = self._read_vout()
        trigger_queue.append(tq_command_vout_read)
        return reversed(trigger_queue), vout_written_l16

    def non_page_specific_initialization_commands(self):
        tq_command_delay = self.delay_trigger_queue_record(delay=500)
        trigger_queue = list()
        trigger_queue.append(self._set_write_protect(0x00))
        trigger_queue.append(self._set_vin_on(9))
        trigger_queue.append(self._set_vin_off(8))
        trigger_queue.append(self._set_vin_ov_fault_limit(15))
        trigger_queue.append(self._set_vin_ov_fault_response(0x80))
        trigger_queue.append(self._set_vin_ov_warn_limit(14))
        trigger_queue.append(self._set_vin_uv_warn_limit(0.6))
        trigger_queue.append(self._set_vin_uv_fault_limit(0.6))
        trigger_queue.append(self._set_mfr_watchdog_t_first(0))
        trigger_queue.append(self._set_mfr_watchdog_t(0))
        trigger_queue.append(self._set_vin_uv_fault_response(0x80))
        trigger_queue.append(self._set_user_data_00(0x0000))
        trigger_queue.append(self._set_user_data_02(0x0000))
        trigger_queue.append(self._set_mfr_iin_cal_gain_tc(0x0000))
        trigger_queue.append(self._set_user_data_04(0xC3F5))
        trigger_queue.append(self._set_mfr_ein_config(0x00))
        trigger_queue.append(self._set_mfr_config_all_ltc2975(0x0F7B))
        trigger_queue.append(self._set_mfr_pwrgd_en(0x0000))
        trigger_queue.append(self._set_mfr_faultb0_response(0x00))
        trigger_queue.append(self._set_mfr_faultb0_response(0x00))
        trigger_queue.append(self._set_mfr_config2_ltc2975(0x00))
        trigger_queue.append(self._set_mfr_config3_ltc2975(0x00))
        trigger_queue.append(self._set_mfr_retry_delay(200))
        trigger_queue.append(self._set_mfr_restart_delay(400))
        trigger_queue.append(self._set_mfr_pwrgood_assertion_delay(100))
        trigger_queue.append(self._set_mfr_page_ff_mask(0x0F))
        trigger_queue.append(self._set_mfr_iin_cal_gain(10))
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(self._set_mfr_retry_count(0x07))
        return trigger_queue

    def page_specific_initialization_commands(self, page, trigger_queue):
        tq_command_delay = self.delay_trigger_queue_record(delay=500)
        trigger_queue.append(self._set_page(page))
        on_off_config = (1 << Ltc2975OnOffConfigBits.ON_OFF_CONFIG_CONTROLLED_ON.value) | (
                1 << Ltc2975OnOffConfigBits.ON_OFF_CONFIG_USE_CONTROL.value) | (
                                1 << Ltc2975OnOffConfigBits.ON_OFF_CONFIG_USE_PMBUS.value)
        trigger_queue.append(self._set_on_off_config(on_off_config))
        trigger_queue.append(self._set_power_state(False))
        trigger_queue.append(self._set_vout_max(5))
        trigger_queue.append(self._set_pwr_good_on(0.6))
        trigger_queue.append(self._set_pwr_good_off(0.55))
        trigger_queue.append(self._set_vout_ov_fault_limit(5.5))
        trigger_queue.append(self._set_vout_ov_warn_limit(5.5))
        trigger_queue.append(self._set_vout_uv_warn_limit(0.3))
        trigger_queue.append(self._set_vout_uv_fault_limit(0.3))
        trigger_queue.append(self._set_vout_ov_fault_response(0x80))
        trigger_queue.append(self._set_vout_uv_fault_response(0x80))
        trigger_queue.append(self._set_iout_oc_warn_limit(4.3))
        trigger_queue.append(self._set_iout_oc_fault_limit(4.3))
        trigger_queue.append(self._set_iout_oc_fault_response(0xC0))
        trigger_queue.append(self._set_iout_uc_fault_response(0xC0))
        trigger_queue.append(self._set_iot_cal_gain(0xD28F))
        trigger_queue.append(tq_command_delay)
        trigger_queue.append(self._set_ot_warn_limit(110))
        trigger_queue.append(self._set_ot_fault_limit(105))
        trigger_queue.append(self._set_ut_fault_limit(-5))
        trigger_queue.append(self._set_ot_fault_response(0x80))
        trigger_queue.append(self._set_ut_fault_response(0x80))
        trigger_queue.append(self._set_ton_max_fault_response(0x80))
        trigger_queue.append(self._set_ut_warn_limit(-10))
        trigger_queue.append(self._set_ton_delay(0))
        trigger_queue.append(self._set_ton_rise(0))
        trigger_queue.append(self._set_toff_delay(0))
        trigger_queue.append(self._set_mfr_iout_cal_gain_tau_inv(0))
        trigger_queue.append(self._set_mfr_iout_cal_gain_theta(1))
        trigger_queue.append(self._set_ton_max_fault_limit(100))
        trigger_queue.append(self._set_mfr_faultbo_propagate(0x01))
        trigger_queue.append(self._set_mfr_faultb1_propagate(0x01))
        trigger_queue.append(self._set_mfr_config_ltc2975(0x0402))
        trigger_queue.append(self._clear_faults())
        return trigger_queue

    def to_L11(self, val):
        Y = val
        N = 0
        N, Y = self.shift_mantissa_for_best_precision(N, Y)
        N, Y = self.fix_mantissa_overflow_if_any(N, Y)
        Y = self.twos_complement_from_decimal(Y, NUMBER_OF_BITS_IN_L11_Y)
        N = self.twos_complement_from_decimal(N, NUMBER_OF_BITS_IN_L11_N)
        return (N << NUMBER_OF_BITS_IN_L11_Y) + Y

    def fix_mantissa_overflow_if_any(self, N, Y):
        while (Y < -(2 ** (NUMBER_OF_BITS_IN_L11_Y - 1))) or (Y > (2 ** (NUMBER_OF_BITS_IN_L11_Y - 1) - 1)):
            Y = Y // 2
            N += 1
        return N, Y

    def shift_mantissa_for_best_precision(self, N, Y):
        while (-(2 ** (NUMBER_OF_BITS_IN_L11_Y - 2)) <= Y < (2 ** (NUMBER_OF_BITS_IN_L11_Y - 2))) and (
                -2 ** (NUMBER_OF_BITS_IN_L11_N - 1) < N):
            Y *= 2
            N -= 1
        Y = int(Y)
        return N, Y

    def from_L11(self, l11_val):
        if 0 <= l11_val <= 0xFFFF:
            bits_15_to_11 = (l11_val >> NUMBER_OF_BITS_IN_L11_Y)
            N = self.decimal_from_twos_complement(bits_15_to_11, NUMBER_OF_BITS_IN_L11_N)
            bits_10_0 = l11_val & self.all_ones(NUMBER_OF_BITS_IN_L11_Y)
            Y = self.decimal_from_twos_complement(bits_10_0, NUMBER_OF_BITS_IN_L11_Y)
            return Y * (2 ** N)
        else:
            self.Log('error', 'L11 value NOT in range 0x0000 - 0xFFFF')

    @staticmethod
    def to_L16(val):
        N = -13
        return int(val // (2 ** N))

    @staticmethod
    def from_L16(l16_val):
        N = -13
        Y = l16_val
        return Y * (2 ** N)

    @staticmethod
    def decimal_from_twos_complement(twos_comp_val, number_of_bits):
        sign_bit = twos_comp_val >> (number_of_bits - 1)
        if sign_bit:
            return twos_comp_val - (1 << number_of_bits)
        else:
            return twos_comp_val

    @staticmethod
    def twos_complement_from_decimal(dec_val, number_of_bits):
        if dec_val < 0:
            return dec_val + (1 << number_of_bits)
        else:
            return dec_val

    @staticmethod
    def all_ones(no_of_bits):
        return (1 << no_of_bits) - 1


class TQTestScenarioLTM4678_VCCIO(TQTestScenario):

    def device_init(self):
        self.tq_command_list = []
        self.voltage_settings = {}
        self.ltm4678_cmd_page = 0x00  # R/W Byte Reg
        self.ltm4678_cmd_vout = 0x21  # R/W Word L16
        self.ltm4678_cmd_operation = 0x01  # R/W Byte Reg
        self.ltm4678_cmd_vout_max = 0x24  # R/W Word L16
        self.ltm4678_cmd_vout_margin_low = 0x26  # R/W Word L16
        self.ltm4678_cmd_vout_margin_high = 0x25  # R/W Word L16
        self.ltm4678_cmd_ov_fault = 0x40  # R/W Word L16
        self.ltm4678_cmd_ov_warn = 0x42  # R/W Word L16
        self.ltm4678_cmd_uv_warn = 0x43  # R/W Word L16
        self.ltm4678_cmd_uv_fault = 0x44  # R/W Word L16
        self.ltm4678_cmd_read_vout = 0x8B  # R   Word L16
        self.ltm4678_cmd_iout_oc_fault = 0x46  # R/W Word L11
        self.ltm4678_cmd_iout_oc_warn = 0x4A  # R/W Word L11
        self.ltm4678_cmd_ot_fault = 0x4F  # R/W Word L11
        self.ltm4678_cmd_ot_warn = 0x51  # R/W Word L11
        self.ltm4678_cmd_ut_fault = 0x53  # R/W Word L11
        self.ltm4678_cmd_ton_delay = 0x60  # R/W Word L11
        self.ltm4678_cmd_ton_rise = 0x61  # R/W Word L11
        self.ltm4678_cmd_toff_delay = 0x64  # R/W Word L11
        self.ltm4678_cmd_toff_fall = 0x65  # R/W Word L11
        self.trigger_queue_content = 0
        self.ltm4678_vout_val = 0x21
        self.ltm4678_ov_fault_val = 0
        self.ltm4678_ov_warn_val = 0
        self.ltm4678_uv_warn_val = 0
        self.ltm4678_uv_fault_val = 0
        self.ltm4678_voltage_coefficient = 2 ** 12

        PAGE = {'cmd': 0x00, 'byte': 1, 'value': 0x00, 'format': 'REG', 'type': 'R/W'}
        VOUT_COMMAND = {'cmd': 0x21, 'byte': 2, 'value': 0x1000, 'format': 'L16', 'type': 'R/W'}
        VOUT_MAX = {'cmd': 0x24, 'byte': 2, 'value': 0x399A, 'format': 'L16', 'type': 'R/W'}
        VOUT_MARGIN_HIGH = {'cmd': 0x25, 'byte': 2, 'value': 0x10CD, 'format': 'L16', 'type': 'R/W'}
        VOUT_MARGIN_LOW = {'cmd': 0x26, 'byte': 2, 'value': 0x0F33, 'format': 'L16', 'type': 'R/W'}
        VOUT_OV_FAULT_LIMIT = {'cmd': 0x40, 'byte': 2, 'value': 0x119A, 'format': 'L16', 'type': 'R/W'}
        VOUT_OV_WARN_LIMIT = {'cmd': 0x42, 'byte': 2, 'value': 0x1133, 'format': 'L16', 'type': 'R/W'}
        VOUT_UV_WARN_LIMIT = {'cmd': 0x43, 'byte': 2, 'value': 0x0ECD, 'format': 'L16', 'type': 'R/W'}
        VOUT_UV_FAULT_LIMIT = {'cmd': 0x44, 'byte': 2, 'value': 0x0E66, 'format': 'L16', 'type': 'R/W'}
        IOUT_OC_FAULT_LIMIT = {'cmd': 0x46, 'byte': 2, 'value': 0xE280, 'format': 'L11', 'type': 'R/W'}
        IOUT_OC_WARN_LIMIT = {'cmd': 0x4A, 'byte': 2, 'value': 0xDBC0, 'format': 'L11', 'type': 'R/W'}
        OT_FAULT_LIMIT = {'cmd': 0x4F, 'byte': 2, 'value': 0xF200, 'format': 'L11', 'type': 'R/W'}
        OT_WARN_LIMIT = {'cmd': 0x51, 'byte': 2, 'value': 0xEBE8, 'format': 'L11', 'type': 'R/W'}
        UT_FAULT_LIMIT = {'cmd': 0x53, 'byte': 2, 'value': 0xE530, 'format': 'L11', 'type': 'R/W'}
        TON_DELAY = {'cmd': 0x60, 'byte': 2, 'value': 0x8000, 'format': 'L11', 'type': 'R/W'}
        TON_RISE = {'cmd': 0x61, 'byte': 2, 'value': 0xC300, 'format': 'L11', 'type': 'R/W'}
        TOFF_DELAY = {'cmd': 0x64, 'byte': 2, 'value': 0x8000, 'format': 'L11', 'type': 'R/W'}
        TOFF_FALL = {'cmd': 0x65, 'byte': 2, 'value': 0xC300, 'format': 'L11', 'type': 'R/W'}
        READ_VOUT = {'cmd': 0x8B, 'byte': 2, 'value': 0x0000, 'format': 'L16', 'type': 'R'}

        register_page = {'VOUT_COMMAND': VOUT_COMMAND, 'VOUT_MAX': VOUT_MAX,
                         'VOUT_MARGIN_HIGH': VOUT_MARGIN_HIGH, 'VOUT_MARGIN_LOW': VOUT_MARGIN_LOW,
                         'VOUT_OV_FAULT_LIMIT': VOUT_OV_FAULT_LIMIT, 'VOUT_OV_WARN_LIMIT': VOUT_OV_WARN_LIMIT,
                         'VOUT_UV_WARN_LIMIT': VOUT_UV_WARN_LIMIT, 'VOUT_UV_FAULT_LIMIT': VOUT_UV_FAULT_LIMIT,
                         'IOUT_OC_FAULT_LIMIT': IOUT_OC_FAULT_LIMIT, 'IOUT_OC_WARN_LIMIT': IOUT_OC_WARN_LIMIT,
                         'OT_FAULT_LIMIT': OT_FAULT_LIMIT, 'OT_WARN_LIMIT': OT_WARN_LIMIT,
                         'UT_FAULT_LIMIT': UT_FAULT_LIMIT, 'TON_DELAY': TON_DELAY, 'TON_RISE': TON_RISE,
                         'TOFF_DELAY': TOFF_DELAY, 'TOFF_FALL': TOFF_FALL, 'READ_VOUT': READ_VOUT}

        register_unpage = {'PAGE': PAGE}
        self.register_file = {'UNPAGED': register_unpage, 'PAGE0': register_page, 'PAGE1': copy.deepcopy(register_page)}

    def increase_register_file_value_by1(self):
        for key_register in self.register_file['PAGE0']:
            if self.register_file['PAGE0'][key_register]['type'] == 'R/W':
                new_value, _ = self.register_file_read('PAGE0', key_register)
                new_value += 1
                self.register_file_write('PAGE0', key_register, new_value)
        for key_register in self.register_file['PAGE1']:
            if self.register_file['PAGE1'][key_register]['type'] == 'R/W':
                new_value, _ = self.register_file_read('PAGE1', key_register)
                new_value += 1
                self.register_file_write('PAGE1', key_register, new_value)

    def register_file_update_from_device(self, psdb, target):
        self.register_file_write('UNPAGED', 'PAGE', 0x00)
        self.write_register_to_device('UNPAGED', 'PAGE', psdb, target)
        self.update_register_from_device('UNPAGED', 'PAGE', psdb, target)
        for key in self.register_file['PAGE0']:
            self.update_register_from_device('PAGE0', key, psdb, target)
        self.register_file_write('UNPAGED', 'PAGE', 0x01)
        self.write_register_to_device('UNPAGED', 'PAGE', psdb, target)
        self.update_register_from_device('UNPAGED', 'PAGE', psdb, target)
        for key in self.register_file['PAGE1']:
            self.update_register_from_device('PAGE1', key, psdb, target)

    def backup_register_file(self):
        self.register_file_copy = copy.deepcopy(self.register_file)

    def restore_register_file(self):
        self.register_file = copy.deepcopy(self.register_file_copy)

    def register_file_check_from_device(self, psdb, target):
        time.sleep(0.1)  # delay in I2C communication, without delay will read wrong value; need to check I2C busy bit
        self.register_file_write('UNPAGED', 'PAGE', 0x00)
        self.write_register_to_device('UNPAGED', 'PAGE', psdb, target)
        for key in self.register_file['PAGE0']:
            if self.register_file['PAGE0'][key]['type'] == 'R/W':
                self.check_register_from_device('PAGE0', key, psdb, target)
        self.register_file_write('UNPAGED', 'PAGE', 0x01)
        self.write_register_to_device('UNPAGED', 'PAGE', psdb, target)
        for key in self.register_file['PAGE1']:
            if self.register_file['PAGE1'][key]['type'] == 'R/W':
                self.check_register_from_device('PAGE1', key, psdb, target)

    def print_register_file(self):
        for key_page in self.register_file:
            self.Log('debug', f'{key_page}')
            for key_register in self.register_file[key_page]:
                register_value = self.register_file[key_page][key_register]['value']
                value_format = self.register_file[key_page][key_register]['format']
                value_decoded = self._value_format_decode(register_value, value_format)
                self.Log('debug', f'{key_register}: 0x{register_value:04x} | {value_decoded}')

    def check_register_from_device(self, page, register, psdb, target):
        value = self.read_register_from_device(page, register, psdb, target)
        if value != self.register_file[page][register]['value']:
            expect = self.register_file[page][register]['value']
            self.Log('error', f'{page}, {register}, read 0x{value:04x}, expect 0x{expect:04x}')

    def update_register_from_device(self, page, register, psdb, target):
        value = self.read_register_from_device(page, register, psdb, target)
        self.register_file_write(page, register, value)

    def read_register_from_device(self, page, register, psdb, target):
        tq_list = self._one_command_content_list(self.register_file[page][register], 0, psdb, target)
        self.combine_tq_content_list(tq_list)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback()

    def write_register_to_device(self, page, register, psdb, target):
        tq_list = self._one_command_content_list(self.register_file[page][register], 1, psdb, target)
        self.combine_tq_content_list(tq_list)
        self._send_command_to_ltm4678_dma()

    def increase_and_write_register_file_to_device_multi_times(self, psdb, target, times):
        tq_list = []
        self.Log('debug', f'increase register by {times}')
        for loop in range(times):
            self.increase_register_file_value_by1()
            for key_register in self.register_file['PAGE0']:
                if self.register_file['PAGE0'][key_register]['type'] == 'R/W':
                    self.register_file_write('UNPAGED', 'PAGE', 0x00)
                    tq_list += self._one_command_content_list(self.register_file['UNPAGED']['PAGE'], 1, psdb, target)
                    tq_list += self._one_command_content_list(self.register_file['PAGE0'][key_register], 1, psdb,
                                                              target)
                    self.register_file_write('UNPAGED', 'PAGE', 0x01)
                    tq_list += self._one_command_content_list(self.register_file['UNPAGED']['PAGE'], 1, psdb, target)
                    tq_list += self._one_command_content_list(self.register_file['PAGE1'][key_register], 1, psdb,
                                                              target)
        self.combine_tq_content_list(tq_list)
        self._send_command_to_ltm4678_dma()

    def restore_register_file_to_device(self, psdb, target):
        self.Log('debug', 'retore_device_setting')
        tq_list = []
        for key_register in self.register_file['PAGE0']:
            if self.register_file['PAGE0'][key_register]['type'] == 'R/W':
                self.register_file_write('UNPAGED', 'PAGE', 0x00)
                tq_list += self._one_command_content_list(self.register_file['UNPAGED']['PAGE'], 1, psdb, target)
                tq_list += self._one_command_content_list(self.register_file['PAGE0'][key_register], 1, psdb, target)
                self.register_file_write('UNPAGED', 'PAGE', 0x01)
                tq_list += self._one_command_content_list(self.register_file['UNPAGED']['PAGE'], 1, psdb, target)
                tq_list += self._one_command_content_list(self.register_file['PAGE1'][key_register], 1, psdb, target)
        self.combine_tq_content_list(tq_list)
        self._send_command_to_ltm4678_dma()

    def register_file_write(self, page, register, value):
        self.register_file[page][register]['value'] = value

    def register_file_read(self, page, register):
        register_value = self.register_file[page][register]['value']
        value_format = self.register_file[page][register]['format']
        value_decoded = self._value_format_decode(register_value, value_format)
        return register_value, value_decoded

    def _one_command_content_list(self, register, read_write, psdb, target):
        one_command_content_list = [
            self._create_ltm4678_command(register['cmd'], read_write, register['byte'], register['value'], psdb,
                                         target)]
        return one_command_content_list

    def _value_format_decode(self, value, format):
        if format == 'REG':
            return value
        elif format == 'L16':
            return self._format_l16_decode(value)
        elif format == 'L11':
            return self._format_l11_decode(value)
        else:
            raise Exception('Wrong Data Format')

    def _format_l16_decode(self, value):
        return value / self.ltm4678_voltage_coefficient

    def _format_l11_decode(self, value):
        y = value & (0x7FF)
        n = (value >> 11) & (0x1F)
        if bool(y & 0x400):
            y_signed = 0 - ((y ^ (0x7FF)) + 1)
        else:
            y_signed = y
        if bool(n & 0x10):
            n_signed = 0 - ((n ^ (0x1F)) + 1)
        else:
            n_signed = n
        return y_signed * (2 ** n_signed)

    def set_psdb_target(self, psdb_target):
        self.psdbTarget = psdb_target

    def select_vccio_channel(self, vout_name):
        self._interface_page_decode(vout_name)
        self._write_ltm4678_page_command_list()
        self._send_command_to_ltm4678_dma()

    def check_vccio_voltage_val(self, vout):
        vccio_channel_list = ['vccio_0_0', 'vccio_1_0', 'vccio_2_0', 'vccio_3_0',
                              'vccio_0_1', 'vccio_1_1', 'vccio_2_1', 'vccio_3_1']
        for vccio_channel in vccio_channel_list:
            self.select_vccio_channel(vccio_channel)
            vccio_voltage = self._read_ltm4678_vout()
            self.Log('debug', f'{vccio_channel} read value: {vccio_voltage:.2f} set value: {vout:.2f}')
            if (vccio_voltage > vout + VOLTAGE_MARGIN) | (vccio_voltage < vout - VOLTAGE_MARGIN):
                self.Log('error', f'{vccio_channel} read value: {vccio_voltage:.2f} set value: {vout:.2f}')

    def read_voltage_settings(self):
        self.voltage_settings = []
        self.voltage_settings.append({'page': self._read_ltm4678_page(),
                                      'vout': self._read_ltm4678_vout(),
                                      'vout_reg': self._read_ltm4678_vout_reg(),
                                      'vout_max': self._read_ltm4678_vout_max(),
                                      'operation': self._read_ltm4678_operation(),
                                      'vout_m_low': self._read_ltm4678_vout_margin_low(),
                                      'vout_m_high': self._read_ltm4678_vout_margin_high(),
                                      'uv_warn': self._read_ltm4678_uv_warn_limit(),
                                      'uv_fault': self._read_ltm4678_uv_fault_limit(),
                                      'ov_warn': self._read_ltm4678_ov_warn_limit(),
                                      'ov_fault': self._read_ltm4678_ov_fault_limit()})
        column_headers = ['page', 'vout', 'vout_reg', 'vout_max', 'operation', 'vout_m_low', 'vout_m_high',
                          'uv_warn', 'uv_fault', 'ov_warn', 'ov_fault']
        table = self.rm_device.contruct_text_table(column_headers, self.voltage_settings)
        self.Log('info', 'voltage setting: ' + f'\n {table}')

    def write_voltage_setting(self):
        self._write_ltm4678_uv_ov_vout_limit_command_list()
        self._send_command_to_ltm4678_dma()

    def _interface_page_decode(self, vout_name):
        if vout_name == 'vccio_0_0':
            self.interface_target = 1
            self.page_set_value = 0
        elif vout_name == 'vccio_1_0':
            self.interface_target = 1
            self.page_set_value = 1
        elif vout_name == 'vccio_2_0':
            self.interface_target = 2
            self.page_set_value = 0
        elif vout_name == 'vccio_3_0':
            self.interface_target = 2
            self.page_set_value = 1
        elif vout_name == 'vccio_0_1':
            self.interface_target = 3
            self.page_set_value = 0
        elif vout_name == 'vccio_1_1':
            self.interface_target = 3
            self.page_set_value = 1
        elif vout_name == 'vccio_2_1':
            self.interface_target = 4
            self.page_set_value = 0
        elif vout_name == 'vccio_3_1':
            self.interface_target = 4
            self.page_set_value = 1

    def _read_ltm4678_page(self, dma=True):
        self._read_ltm4678_page_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return hex(self._i2c_data_readback())

    def _read_ltm4678_vout_reg(self, dma=True):
        self._read_ltm4678_vout_reg_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_operation(self, dma=True):
        self._read_ltm4678_operation_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return hex(self._i2c_data_readback())

    def _read_ltm4678_vout_max(self, dma=True):
        self._read_ltm4678_vout_max_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_vout_margin_low(self, dma=True):
        self._read_ltm4678_vout_margin_low_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_vout_margin_high(self, dma=True):
        self._read_ltm4678_vout_margin_high_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_vout(self, dma=True):
        self._read_ltm4678_vout_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_uv_fault_limit(self, dma=True):
        self._read_ltm4678_uv_fault_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_uv_warn_limit(self, dma=True):
        self._read_ltm4678_uv_warn_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_ov_fault_limit(self, dma=True):
        self._read_ltm4678_ov_fault_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _read_ltm4678_ov_warn_limit(self, dma=True):
        self._read_ltm4678_ov_warn_limit_command_list(dma)
        self._send_command_to_ltm4678_dma()
        return self._i2c_data_readback() / self.ltm4678_voltage_coefficient

    def _i2c_data_readback(self):
        return self.env.hbicc.ring_multiplier.read_slice_register(
            self.env.hbicc.ring_multiplier.registers.PSDB_DC_TRIGGER_PAGE_I2C_DATA).value

    def _send_command_to_ltm4678_dma(self):
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        self.save_trigger_queue_to_memory(self.trigger_queue_content, trigger_register_data)
        tq_processed_prev_count = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(trigger_register_data)
        self._evaluate_trigger_processing(tq_processed_prev_count)

    def _evaluate_trigger_processing(self, tq_processed_prev_count):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.check_tq_processed_counter() > tq_processed_prev_count:
                self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
                break
        else:
            self.Log('error', 'Trigger Queue NOT processed')

    def set_uv_ov_vout_value(self, uv, ov, vout):
        self.uv_set_value = int(uv * self.ltm4678_voltage_coefficient)
        self.ov_set_value = int(ov * self.ltm4678_voltage_coefficient)
        self.vout_set_value = int(vout * self.ltm4678_voltage_coefficient)
        self.uv_set_value_byte_low = self.uv_set_value & 0xFFFF
        self.uv_set_value_byte_high = (self.uv_set_value >> 8) & 0xFFFF
        self.ov_set_value_byte_low = self.ov_set_value & 0xFFFF
        self.ov_set_value_byte_high = (self.ov_set_value >> 8) & 0xFFFF
        self.vout_set_value_byte_low = self.vout_set_value & 0xFFFF
        self.vout_set_value_byte_high = (self.vout_set_value >> 8) & 0xFFFF

    def send_vccio_vref_broadcast_dma(self):
        trigger_register_data = self.generate_aurora_trigger_for_dut(BROADCAST_DUT_ID)
        self.save_broadcast_trigger_queue_to_memory(self.trigger_queue_content, trigger_register_data)
        tq_processed_prev_count = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(trigger_register_data)
        self._evaluate_trigger_processing(tq_processed_prev_count)

    def combine_tq_content_list(self, tq_multi_list):
        tq_multi_list.reverse()
        self.trigger_queue_content = self.end_trigger_queue_record()
        for tq_content in tq_multi_list:
            self.trigger_queue_content = (self.trigger_queue_content << TQ_COMMAND_BITS) | tq_content
        self.trigger_queue_content = (self.trigger_queue_content << TQ_COMMAND_BITS) | \
                                     self.start_trigger_queue_record()

    def vccio_set_tq_content(self, uv, ov, vout, psdb):
        tq_content = []
        self.set_uv_ov_vout_value(uv, ov, vout)
        tq_content.append(self._write_ltm4678_page_command(psdb, 1, 0))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 1))
        tq_content.append(self._write_ltm4678_page_command(psdb, 1, 1))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 1))
        tq_content.append(self._write_ltm4678_page_command(psdb, 2, 0))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 2))
        tq_content.append(self._write_ltm4678_page_command(psdb, 2, 1))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 2))
        tq_content.append(self._write_ltm4678_page_command(psdb, 3, 0))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 3))
        tq_content.append(self._write_ltm4678_page_command(psdb, 3, 1))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 3))
        tq_content.append(self._write_ltm4678_page_command(psdb, 4, 0))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 4))
        tq_content.append(self._write_ltm4678_page_command(psdb, 4, 1))
        tq_content.append(self._write_ltm4678_vout_command(psdb, 4))
        return tq_content

    def _write_ltm4678_page_command(self, psdb, target, page):
        return self._create_ltm4678_command(self.ltm4678_cmd_page, 1, 1, page, psdb, target)

    def _write_ltm4678_vout_command(self, psdb, target, value=None):
        if value == None:
            value = self.vout_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_vout, 1, 2, value, psdb, target)

    def _write_ltm4678_vout_max_command(self, psdb, target, value=None):
        if value == None:
            value = self.ov_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_vout_max, 1, 2, value, psdb, target)

    def _write_ltm4678_uv_warn_command(self, psdb, target, value=None):
        if value == None:
            value = self.uv_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_uv_warn, 1, 2, value, psdb, target)

    def _write_ltm4678_uv_fault_command(self, psdb, target, value=None):
        if value == None:
            value = self.uv_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_uv_fault, 1, 2, value, psdb, target)

    def _write_ltm4678_ov_warn_command(self, psdb, target, value=None):
        if value == None:
            value = self.ov_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_ov_warn, 1, 2, value, psdb, target)

    def _write_ltm4678_ov_fault_command(self, psdb, target, value=None):
        if value == None:
            value = self.ov_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_ov_fault, 1, 2, value, psdb, target)

    def _write_ltm4678_vout_m_high_command(self, psdb, target, value=None):
        if value == None:
            value = self.ov_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_vout_margin_high, 1, 2, value, psdb, target)

    def _write_ltm4678_vout_m_low_command(self, psdb, target, value=None):
        if value == None:
            value = self.uv_set_value
        return self._create_ltm4678_command(self.ltm4678_cmd_vout_margin_low, 1, 2, value, psdb, target)

    def _write_ltm4678_iout_oc_fault_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_iout_oc_fault, 1, 2, value, psdb, target)

    def _write_ltm4678_iout_oc_warn_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_iout_oc_warn, 1, 2, value, psdb, target)

    def _write_ltm4678_ot_fault_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_ot_fault, 1, 2, value, psdb, target)

    def _write_ltm4678_ot_warn_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_ot_warn, 1, 2, value, psdb, target)

    def _write_ltm4678_ut_fault_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_ut_fault, 1, 2, value, psdb, target)

    def _write_ltm4678_ton_delay_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_ton_delay, 1, 2, value, psdb, target)

    def _write_ltm4678_ton_rise_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_ton_rise, 1, 2, value, psdb, target)

    def _write_ltm4678_toff_delay_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_toff_delay, 1, 2, value, psdb, target)

    def _write_ltm4678_toff_fall_command(self, psdb, target, value):
        return self._create_ltm4678_command(self.ltm4678_cmd_toff_fall, 1, 2, value, psdb, target)

    def _read_ltm4678_page_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_page, 0, 1, 0, psdb, target)

    def _read_ltm4678_operation_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_operation, 0, 1, 0, psdb, target)

    def _read_ltm4678_vout_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_vout, 0, 2, 0, psdb, target)

    def _read_ltm4678_vout_max_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_vout_max, 0, 2, 0, psdb, target)

    def _read_ltm4678_uv_warn_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_uv_warn, 0, 2, 0, psdb, target)

    def _read_ltm4678_uv_fault_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_uv_fault, 0, 2, 0, psdb, target)

    def _read_ltm4678_ov_warn_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_ov_warn, 0, 2, 0, psdb, target)

    def _read_ltm4678_ov_fault_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_ov_fault, 0, 2, 0, psdb, target)

    def _read_ltm4678_vout_m_high_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_vout_margin_high, 0, 2, 0, psdb, target)

    def _read_ltm4678_vout_m_low_command(self, psdb, target):
        return self._create_ltm4678_command(self.ltm4678_cmd_vout_margin_low, 0, 2, 0, psdb, target)

    def _create_ltm4678_command(self, cmd_code, direction_read_write, data_bytes, value, psdb, target):
        if direction_read_write:
            data_byte_count_write = data_bytes
        else:
            data_byte_count_write = 0
        value_byte_low = value & (0xFF)
        value_byte_high = (value >> 8) & (0xFF)
        payload = self._create_payload_ltm4678(cmd_code, data_byte_count_write, value_byte_low, value_byte_high)
        constants = self._create_constants_ltm4678(payload, data_bytes, direction_read_write)
        return self._create_tq_command_ltm4678(constants, psdb, target)

    def _create_tq_command_ltm4678(self, constants, psdb, target):
        tq_command = self.env.hbicc.ring_multiplier. \
            create_trigger_queue_command_record(payload_lower=constants['payload'],
                                                target_interface=target,
                                                payload_byte_count=constants['payload_byte_count'],
                                                psdb_target=psdb, read_write=constants['read_write_direction'],
                                                command_data=constants['command_data'], channel_set=0,
                                                command_encoding_upper=0, dma=constants['dma'],
                                                spi_target=constants['ldac_bar'])
        return tq_command

    def _write_ltm4678_vout_command_list(self, dma=True):
        data_byte_count_write = 2
        direction_write = 1
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout, data_byte_count_write,
                                               self.vout_set_value_byte_low, self.vout_set_value_byte_high)
        write_command = self._create_constants_ltm4678(payload, data_byte_count_write, direction_write)
        self._generate_single_command_list(write_command)
        self._generate_trigger_queue_contant(dma)

    def _write_ltm4678_uv_ov_vout_limit_command_list(self, dma=True):
        data_byte_count_write = 2
        direction_write = 1
        payload_uv_warn = self._create_payload_ltm4678(self.ltm4678_cmd_uv_warn, data_byte_count_write,
                                                       self.uv_set_value_byte_low, self.uv_set_value_byte_high)
        payload_uv_fault = self._create_payload_ltm4678(self.ltm4678_cmd_uv_fault, data_byte_count_write,
                                                        self.uv_set_value_byte_low, self.uv_set_value_byte_high)
        payload_ov_warn = self._create_payload_ltm4678(self.ltm4678_cmd_ov_warn, data_byte_count_write,
                                                       self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_ov_fault = self._create_payload_ltm4678(self.ltm4678_cmd_ov_fault, data_byte_count_write,
                                                        self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_vout_max = self._create_payload_ltm4678(self.ltm4678_cmd_vout_max, data_byte_count_write,
                                                        self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_vout_m_high = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_high, data_byte_count_write,
                                                           self.ov_set_value_byte_low, self.ov_set_value_byte_high)
        payload_vout_m_low = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_low, data_byte_count_write,
                                                          self.uv_set_value_byte_low, self.uv_set_value_byte_high)
        payload_vout = self._create_payload_ltm4678(self.ltm4678_cmd_vout, data_byte_count_write,
                                                    self.vout_set_value_byte_low, self.vout_set_value_byte_high)
        self.uv_warn_write_command = \
            self._create_constants_ltm4678(payload_uv_warn, data_byte_count_write, direction_write)
        self.uv_fault_write_command = \
            self._create_constants_ltm4678(payload_uv_fault, data_byte_count_write, direction_write)
        self.ov_warn_write_command = \
            self._create_constants_ltm4678(payload_ov_warn, data_byte_count_write, direction_write)
        self.ov_fault_write_command = \
            self._create_constants_ltm4678(payload_ov_fault, data_byte_count_write, direction_write)
        self.vout_max_write_command = \
            self._create_constants_ltm4678(payload_vout_max, data_byte_count_write, direction_write)
        self.vout_m_high_write_command = \
            self._create_constants_ltm4678(payload_vout_m_high, data_byte_count_write, direction_write)
        self.vout_m_low_write_command = \
            self._create_constants_ltm4678(payload_vout_m_low, data_byte_count_write, direction_write)
        self.vout_write_command = \
            self._create_constants_ltm4678(payload_vout, data_byte_count_write, direction_write)
        self._generate_multiple_write_command_list()
        self._generate_trigger_queue_contant(dma)

    def _write_ltm4678_page_command_list(self, dma=True):
        data_byte_count_write = 1
        direction_write = 1
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_page, data_byte_count_write, self.page_set_value)
        write_command = self._create_constants_ltm4678(payload, data_byte_count_write, direction_write)
        self._generate_single_command_list(write_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_page_command_list(self, dma=True):
        data_byte_count_read = 1
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_page, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_operation_command_list(self, dma=True):
        data_byte_count_read = 1
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_operation, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_max_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout_max, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_margin_low_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_low, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_margin_high_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout_margin_high, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_reg_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_vout, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_vout_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_read_vout, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_uv_fault_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_uv_fault, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_uv_warn_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_uv_warn, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_ov_fault_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_ov_fault, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _read_ltm4678_ov_warn_limit_command_list(self, dma=True):
        data_byte_count_read = 2
        data_byte_count_write = 0
        direction_read = 0
        payload = self._create_payload_ltm4678(self.ltm4678_cmd_ov_warn, data_byte_count_write)
        read_command = self._create_constants_ltm4678(payload, data_byte_count_read, direction_read)
        self._generate_single_command_list(read_command)
        self._generate_trigger_queue_contant(dma)

    def _generate_trigger_queue_contant(self, dma):
        tq_start = self.env.hbicc.ring_multiplier.start_trigger_queue_record(self.psdbTarget, dma)
        tq_end = self.env.hbicc.ring_multiplier.end_trigger_queue_record(self.psdbTarget, dma)
        self.trigger_queue_content = tq_end
        for command in reversed(self.tq_command_list):
            tq_command = self.env.hbicc.ring_multiplier. \
                create_trigger_queue_command_record(payload_lower=command['payload'],
                                                    target_interface=self.interface_target,
                                                    payload_byte_count=command['payload_byte_count'],
                                                    psdb_target=self.psdbTarget,
                                                    read_write=command['read_write_direction'],
                                                    command_data=command['command_data'], channel_set=0,
                                                    command_encoding_upper=0, dma=command['dma'],
                                                    spi_target=command['ldac_bar'])
            self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_command
        self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_start

    def _create_payload_ltm4678(self, ltm4678_cmd, data_byte_count, data_byte_low=0, data_byte_high=0):
        if data_byte_count == 0:
            return ltm4678_cmd
        elif data_byte_count == 1:
            return ltm4678_cmd | (data_byte_low << 8)
        elif data_byte_count == 2:
            return ltm4678_cmd | (data_byte_low << 8) | (data_byte_high << 16)
        else:
            self.Log('error', 'this method only support upto 2 byte data')

    def _create_constants_ltm4678(self, ltm4678_payload, data_byte_count, read_write_direction):
        if read_write_direction:
            return {'payload': ltm4678_payload, 'payload_byte_count': (data_byte_count + 1),
                    'read_write_direction': read_write_direction, 'ldac_bar': 0, 'command_data': 0, 'dma': True}
        else:
            return {'payload': ltm4678_payload, 'payload_byte_count': data_byte_count,
                    'read_write_direction': read_write_direction, 'ldac_bar': 0, 'command_data': 0, 'dma': True}

    def _generate_multiple_write_command_list(self):
        self.tq_command_list = []
        self.tq_command_list.append(self.uv_warn_write_command)
        self.tq_command_list.append(self.uv_fault_write_command)
        self.tq_command_list.append(self.ov_warn_write_command)
        self.tq_command_list.append(self.ov_fault_write_command)
        self.tq_command_list.append(self.vout_max_write_command)
        self.tq_command_list.append(self.vout_m_high_write_command)
        self.tq_command_list.append(self.vout_m_low_write_command)
        self.tq_command_list.append(self.vout_write_command)

    def _generate_single_command_list(self, command):
        self.tq_command_list = []
        self.tq_command_list.append(command)


class TQTestScenarioAD5684r(TQTestScenario):

    def device_init(self):
        self.dma = True
        self.tq_command_list = []
        self.ldacn_list = [0x0, 0x5, 0x7]
        self.expect_dac_val_set = {}
        self.expect_vol_val_set = {}
        self.expect_dac_val_next_set = {}
        self.expect_vol_val_next_set = {}
        self.observ_dac_val_set = {}
        self.observ_vol_val_set = {}
        self.previo_dac_val_set = {}
        self.previo_vol_val_set = {}
        self.error_dac_num = 0
        self.error_vol_num = 0
        self.active_dac_list = []
        self.readback_dac = 0
        self.vref_command = []
        self.ad5684r_dac_adr = 0
        self.ad5684r_dac_val = 0
        self.ad5684r_rdb_adr = 0
        self.ad5684r_dac_count = 4
        self.ad5684r_adr_bit_length = 4
        self.ad5684r_dac_bit_length = 16
        self.ad5684r_res_bit_length = 4
        self.ad5684r_cmd_nop = 0
        self.ad5684r_cmd_wri = 1
        self.ad5684r_cmd_upd = 2
        self.ad5684r_cmd_wup = 3
        self.ad5684r_cmd_rdb = 9
        self.wri_trigger_dut = 0
        self.wri_trigger_ind = 0
        self.error_count = 0

    def set_trigger_mode(self, dma_or_pcie='dma'):
        if dma_or_pcie == 'dma':
            self.dma = True
        elif dma_or_pcie == 'pcie':
            self.dma = False

    def set_vref_as_clock(self, vref_voltage, dac_list, period_in_us, times):
        self._ad5684r_set_vref_as_clock_command_list(vref_voltage, dac_list, period_in_us, times, self.dma)
        if self.dma:
            self._send_big_command_list_to_ad5684r_dma()

    def send_wup_command_set_vref(self, vref_voltage, dac_list):
        self._ad5684r_set_vref_command_list(vref_voltage, dac_list, self.dma)
        if self.dma:
            self._send_command_to_ad5684r_dma()

    def send_wri_upd_command_set_random_vref(self):
        self._ad5684r_random_wri_upd_command_list(self.dma)
        if self.dma:
            self._send_command_to_ad5684r_dma(wri_upd=True)

    def send_wri_command_set_random_vref(self):
        self._ad5684r_wri_command_list(self.dma)
        if self.dma:
            self._send_command_to_ad5684r_dma(wri_upd=True)

    def send_upd_command_set_random_vref(self):
        self._ad5684r_upd_command_list(self.dma)
        if self.dma:
            self._send_command_to_ad5684r_dma(wri_upd=True)

    def readback_register_val(self):
        self.previo_dac_val_set = self.observ_dac_val_set.copy()
        self.observ_dac_val_set = {}
        for i in range(self.ad5684r_dac_count):
            self._ad5684r_readback_command_list(i, self.dma)
            if self.dma:
                self._send_command_to_ad5684r_dma()
            self.observ_dac_val_set.update({i: self._ad5684r_dac_value()})

    def readback_voltage_val(self):
        self.previo_vol_val_set = self.observ_vol_val_set.copy()
        self.observ_vol_val_set = {}
        for i in range(self.ad5684r_dac_count):
            self.observ_vol_val_set.update({i: self._vmon_data_readback(i)})

    def update_expect_val_set(self):
        self.expect_dac_val_set = self.observ_dac_val_set.copy()
        self.expect_vol_val_set = self.observ_vol_val_set.copy()

    def check_register_val_result(self, loop):
        for i in range(self.ad5684r_dac_count):
            expect_value = self.expect_dac_val_set[i]
            observ_value = self.observ_dac_val_set[i]
            # self._ad5684r_error_print(0, loop)
            if expect_value != observ_value:
                self.error_dac_num += 1
                if self.error_dac_num <= 4:
                    self._ad5684r_error_print(0, loop)
                    self.error_count += 1
                break

    def check_voltage_val_result(self, loop):
        for i in range(self.ad5684r_dac_count):
            expect_value = self.expect_vol_val_set[i]
            observ_value = self.observ_vol_val_set[i]
            # self._ad5684r_error_print(1, loop)
            if (observ_value > (expect_value + 0.05)) | (observ_value < (expect_value - 0.05)):
                self.error_vol_num += 1
                if self.error_vol_num <= 4:
                    self._ad5684r_error_print(1, loop)
                    self.error_count += 1
                break

    def check_vref_voltage_val(self, vref):
        for i in range(self.ad5684r_dac_count):
            vref_vmon = self._vmon_data_readback(i)
            self.Log('debug', f'vref channel[{i}] read value: {vref_vmon:.2f} set value: {vref:.2f}')
            if (vref_vmon > vref + VOLTAGE_MARGIN) | (vref_vmon < vref - VOLTAGE_MARGIN):
                self.Log('error', f'vref channel[{i}] read value: {vref_vmon:.2f} set value: {vref:.2f}')

    def vref_result_print(self):
        column_headers = ['DAC Channel', 'Observed Register', 'Observed Voltage']
        print_list = []
        for i in range(self.ad5684r_dac_count):
            print_list.append({'DAC Channel': i, 'Observed Register': hex(self.observ_dac_val_set[i]),
                               'Observed Voltage': float('%.4f' % (self.observ_vol_val_set[i]))})
        table = self.rm_device.contruct_text_table(column_headers, print_list)
        self.Log('info', 'dac_readback_value: ' + f'\n {table}')

    def _ad5684r_error_print(self, dac_or_vol, loop):
        column_headers = ['DAC Channel', 'Previous', 'Expected', 'Observed']
        print_list = []
        if dac_or_vol:
            for i in range(self.ad5684r_dac_count):
                print_list.append({'DAC Channel': i, 'Previous': float('%.2f' % (self.previo_vol_val_set[i])),
                                   'Expected': float('%.2f' % (self.expect_vol_val_set[i])),
                                   'Observed': float('%.2f' % (self.observ_vol_val_set[i]))})
        else:
            for i in range(self.ad5684r_dac_count):
                print_list.append({'DAC Channel': i, 'Previous': hex(self.previo_dac_val_set[i]),
                                   'Expected': hex(self.expect_dac_val_set[i]),
                                   'Observed': hex(self.observ_dac_val_set[i])})
        table = self.rm_device.contruct_text_table(column_headers, print_list)
        self.Log('error', 'Read/Write operation failed for following: ' + f'\n {table}')
        self.Log('error',
                 f'command failed in loop {loop} with dut_id {self.wri_trigger_dut} and index {self.wri_trigger_ind}' + f'\n{self.vref_command}')

    def _ad5684r_dac_value(self):
        return (self._spi_data_readback()) >> 4

    def _spi_data_readback(self):
        if self.psdbTarget == 0:
            return self.env.hbicc.ring_multiplier. \
                read_slice_register(self.rm_device.registers.PSDB0_DC_TRIGGER_PAGE_SPI_DATA).data
        else:
            return self.env.hbicc.ring_multiplier. \
                read_slice_register(self.rm_device.registers.PSDB1_DC_TRIGGER_PAGE_SPI_DATA).data

    def _vmon_data_readback(self, dac_channel):
        if self.psdbTarget == 0:
            return self.env.hbicc.psdb_0_pin_0.read_vref(channel_index=(dac_channel + 1))
        else:
            return self.env.hbicc.psdb_1_pin_0.read_vref(channel_index=(dac_channel + 1))

    def _send_big_command_list_to_ad5684r_dma(self, wri_upd=False):
        trigger_register_data = self.generate_aurora_trigger_for_dut16_index0()
        if wri_upd:
            self.wri_trigger_dut = trigger_register_data.dut_id
            self.wri_trigger_ind = trigger_register_data.index_of_tq_to_fire
        self.save_trigger_queue_to_memory_for_lut0_index0(self.trigger_queue_content, trigger_register_data)
        tq_processed_prev_count = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(trigger_register_data)
        self._evaluate_trigger_processing(tq_processed_prev_count)

    def save_trigger_queue_to_memory_for_lut0_index0(self, trigger_queue_record, trigger_register_data):
        lut_address = self.calculate_lut_address(trigger_register_data)
        tq_record_start_address = self.dma_tq_record_address_to_lut_with_fix_address(lut_address)
        self.send_trigger_queue_to_memory(tq_record_start_address, trigger_queue_record)

    def dma_tq_record_address_to_lut_with_fix_address(self, lut_address):
        tq_record_start_address = 0x4000000
        start_address_multiple_of_four = tq_record_start_address * 8
        tq_record_start_address_in_byte = start_address_multiple_of_four.to_bytes(8, byteorder='little')
        self.rm_device.dma_write(lut_address, tq_record_start_address_in_byte)
        return start_address_multiple_of_four

    def generate_aurora_trigger_for_dut16_index0(self):
        random_dut_id = 16
        random_index_of_tq_to_fire = 0
        trigger_register_data = self.rm_device.registers.LAST_TRIGGER_SEEN()
        trigger_register_data.target_source = 0x3f
        trigger_register_data.dut_id = random_dut_id
        trigger_register_data.sw_up_trigger = 0
        trigger_register_data.reserved = 0x1E8  # set field "reserved" to 0x1E8 to facilitate SignalTap capture
        trigger_register_data.index_of_tq_to_fire = random_index_of_tq_to_fire
        return trigger_register_data

    def _send_command_to_ad5684r_dma(self, wri_upd=False):
        trigger_register_data = self.generate_aurora_trigger_for_random_non_broadcast_dut()
        if wri_upd:
            self.wri_trigger_dut = trigger_register_data.dut_id
            self.wri_trigger_ind = trigger_register_data.index_of_tq_to_fire
        self.save_trigger_queue_to_memory(self.trigger_queue_content, trigger_register_data)
        tq_processed_prev_count = self.check_tq_processed_counter()
        self.send_trigger_from_rctc(trigger_register_data)
        self._evaluate_trigger_processing(tq_processed_prev_count)

    def _evaluate_trigger_processing(self, tq_processed_prev_count):
        for retries in range(NUMBER_OF_RETRIES_FOR_TQ_PROCESS_CHECK):
            self.wait_for_seconds(WAIT_BETWEEN_RETRIES_IN_SECONDS)
            if self.check_tq_processed_counter() > tq_processed_prev_count:
                self.wait_for_seconds(WAIT_SECONDS_BEFORE_READ_FROM_I2C_READBACK_REGISTER)
                break
        else:
            self.error_count += 1
            self.Log('error', 'Trigger Queue NOT processed')

    def _ad5684r_wri_command_list(self, dma=True):
        self._choose_random_dac_channels_for_write()
        self.ad5684r_ldacn = 0x7
        self.ad5684r_payload_wri = self._create_payload_ad5684r(self.ad5684r_cmd_wri, self.ad5684r_dac_adr,
                                                                self.ad5684r_dac_val)
        self.vrefwri_command = self._create_constants_ad5684r(self.ad5684r_payload_wri, self.ad5684r_ldacn)
        self._generate_wri_command_list()
        self._generate_trigger_queue_contant(0, dma)

    def _generate_wri_command_list(self):
        self.tq_command_list = []
        self.tq_command_list.append(self.vrefwri_command)
        self.vref_command = self.tq_command_list

    def _ad5684r_upd_command_list(self, dma=True):
        self.ad5684r_ldacn = 0x7
        self.ad5684r_payload_upd = self._create_payload_ad5684r(self.ad5684r_cmd_upd, self.ad5684r_dac_adr, 0)
        self.vrefupd_command = self._create_constants_ad5684r(self.ad5684r_payload_upd)
        self._generate_upd_command_list()
        self._generate_trigger_queue_contant(0, dma)

    def _generate_upd_command_list(self):
        self.tq_command_list = []
        self.expect_dac_val_set = self.expect_dac_val_next_set.copy()
        self.expect_vol_val_set = self.expect_vol_val_next_set.copy()
        self.tq_command_list.append(self.vrefupd_command)
        self.vref_command = self.tq_command_list

    def _ad5684r_readback_command_list(self, dac_channel, dma=True):
        self._choose_dac_channels_for_read(dac_channel)
        self.ad5684r_payload_rdb = self._create_payload_ad5684r(self.ad5684r_cmd_rdb, self.ad5684r_rdb_adr, 0)
        self.ad5684r_payload_nop = self._create_payload_ad5684r(self.ad5684r_cmd_nop, 0, 0)
        self.vrefrdb_command = self._create_constants_ad5684r(self.ad5684r_payload_rdb)
        self.vrefnop_command = self._create_constants_ad5684r(self.ad5684r_payload_nop)
        self._generate_read_command_list()
        self._generate_trigger_queue_contant(0, dma)

    def _generate_read_command_list(self):
        self.tq_command_list = []
        self.tq_command_list.append(self.vrefrdb_command)
        self.tq_command_list.append(self.vrefnop_command)

    def _choose_dac_channels_for_read(self, dac_channel):
        self.ad5684r_rdb_adr = 1 << dac_channel

    def vref_set_tq_content(self, vref, psdb):
        tq_content = []
        tq_content.append(self._ad5684r_set_vref_command(vref, [1, 0, 0, 0], psdb))
        tq_content.append(self._ad5684r_set_vref_command(vref, [0, 1, 0, 0], psdb))
        tq_content.append(self._ad5684r_set_vref_command(vref, [0, 0, 1, 0], psdb))
        tq_content.append(self._ad5684r_set_vref_command(vref, [0, 0, 0, 1], psdb))
        return tq_content

    def _ad5684r_set_vref_command(self, vref_voltage, dac_list, psdb):
        self._decode_dac_channels_for_write(dac_list)
        self.ad5684r_dac_val = int((vref_voltage / 2.5) * 0xFFF)
        self.ad5684r_payload_wup = self._create_payload_ad5684r(self.ad5684r_cmd_wup, self.ad5684r_dac_adr,
                                                                self.ad5684r_dac_val)
        self.vrefwup_command = self._create_constants_ad5684r(self.ad5684r_payload_wup)
        return self._create_tq_command_ad5684r(self.vrefwup_command, psdb)

    def _create_tq_command_ad5684r(self, constants, psdb, target=0):
        tq_command = self.env.hbicc.ring_multiplier. \
            create_trigger_queue_command_record(payload_lower=constants['payload'],
                                                target_interface=target,
                                                payload_byte_count=constants['payload_byte_count'],
                                                psdb_target=psdb, read_write=constants['read_write_direction'],
                                                command_data=constants['command_data'], channel_set=0,
                                                command_encoding_upper=0, dma=constants['dma'],
                                                spi_target=constants['ldac_bar'])
        return tq_command

    def _ad5684r_set_vref_as_clock_command_list(self, vref_voltage, dac_list, period_in_us, times, dma=True):
        delay = int(period_in_us / 2)
        self._decode_dac_channels_for_write(dac_list)
        self.ad5684r_dac_val = int((vref_voltage / 2.5) * 0xFFF)
        self.ad5684r_payload_wup_high = self._create_payload_ad5684r(self.ad5684r_cmd_wup, self.ad5684r_dac_adr,
                                                                     self.ad5684r_dac_val)
        self.vrefwup_command_high = self._create_constants_ad5684r(self.ad5684r_payload_wup_high)
        self.ad5684r_payload_wup_low = self._create_payload_ad5684r(self.ad5684r_cmd_wup, self.ad5684r_dac_adr, 0)
        self.vrefwup_command_low = self._create_constants_ad5684r(self.ad5684r_payload_wup_low)
        self._generate_vref_clock_command_list(times)
        self._generate_trigger_queue_contant_with_delay(0, dma, delay)

    def _ad5684r_set_vref_command_list(self, vref_voltage, dac_list, dma=True):
        self._decode_dac_channels_for_write(dac_list)
        self.ad5684r_dac_val = int((vref_voltage / 2.5) * 0xFFF)
        self.ad5684r_payload_wup = self._create_payload_ad5684r(self.ad5684r_cmd_wup, self.ad5684r_dac_adr,
                                                                self.ad5684r_dac_val)
        self.vrefwup_command = self._create_constants_ad5684r(self.ad5684r_payload_wup)
        self._generate_wup_command_list()
        self._generate_trigger_queue_contant(0, dma)

    def _generate_vref_clock_command_list(self, times):
        self.tq_command_list = []
        for i in range(times):
            self.tq_command_list.append(self.vrefwup_command_low)
            self.tq_command_list.append(self.vrefwup_command_high)

    def _generate_wup_command_list(self):
        self.tq_command_list = []
        self.tq_command_list.append(self.vrefwup_command)

    def _ad5684r_random_wri_upd_command_list(self, dma=True):
        self._choose_random_dac_channels_for_write()
        self._choose_random_ldacn()
        self.ad5684r_payload_wri = self._create_payload_ad5684r(self.ad5684r_cmd_wri, self.ad5684r_dac_adr,
                                                                self.ad5684r_dac_val)
        self.ad5684r_payload_wup = self._create_payload_ad5684r(self.ad5684r_cmd_wup, self.ad5684r_dac_adr,
                                                                self.ad5684r_dac_val)
        self.ad5684r_payload_upd = self._create_payload_ad5684r(self.ad5684r_cmd_upd, self.ad5684r_dac_adr, 0)
        self.vrefwri_command = self._create_constants_ad5684r(self.ad5684r_payload_wri, self.ad5684r_ldacn)
        self.vrefwup_command = self._create_constants_ad5684r(self.ad5684r_payload_wup, self.ad5684r_ldacn)
        self.vrefupd_command = self._create_constants_ad5684r(self.ad5684r_payload_upd)
        self._generate_random_wri_upd_command_list()
        self._generate_trigger_queue_contant(0, dma)

    def _generate_trigger_queue_contant(self, device, dma):
        tq_start = self.env.hbicc.ring_multiplier.start_trigger_queue_record(self.psdbTarget, dma)
        tq_end = self.env.hbicc.ring_multiplier.end_trigger_queue_record(self.psdbTarget, dma)
        self.trigger_queue_content = tq_end
        for command in reversed(self.tq_command_list):
            tq_command = self.env.hbicc.ring_multiplier. \
                create_trigger_queue_command_record(payload_lower=command['payload'],
                                                    target_interface=device,
                                                    payload_byte_count=command['payload_byte_count'],
                                                    psdb_target=self.psdbTarget,
                                                    read_write=command['read_write_direction'],
                                                    command_data=command['command_data'], channel_set=0,
                                                    command_encoding_upper=0, dma=command['dma'],
                                                    spi_target=command['ldac_bar'])
            self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_command
        self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_start

    def _generate_trigger_queue_contant_with_delay(self, device, dma, delay):
        tq_start = self.env.hbicc.ring_multiplier.start_trigger_queue_record(0, dma)
        tq_end = self.env.hbicc.ring_multiplier.end_trigger_queue_record(0, dma)
        tq_delay = self.env.hbicc.ring_multiplier.delay_within_trigger_queue_record(self.psdbTarget, dma, delay, 1)
        self.trigger_queue_content = tq_end
        for command in reversed(self.tq_command_list):
            tq_command = self.env.hbicc.ring_multiplier. \
                create_trigger_queue_command_record(payload_lower=command['payload'],
                                                    target_interface=command['target_interface'],
                                                    payload_byte_count=command['payload_byte_count'],
                                                    psdb_target=self.psdbTarget,
                                                    read_write=command['read_write_direction'],
                                                    command_data=command['command_data'], channel_set=0,
                                                    command_encoding_upper=0, dma=command['dma'],
                                                    spi_target=command['ldac_bar'])
            self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_command
            self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_delay
        self.trigger_queue_content = (self.trigger_queue_content << 64) | tq_start

    def _generate_random_wri_upd_command_list(self):
        self.tq_command_list = []
        self.expect_dac_val_set = self.expect_dac_val_next_set.copy()
        self.expect_vol_val_set = self.expect_vol_val_next_set.copy()
        wri_or_wup = random.randrange(2)
        if wri_or_wup:
            self.tq_command_list.append(self.vrefwup_command)
        else:
            self.tq_command_list.append(self.vrefwri_command)
            if self.ad5684r_ldacn == 0x7:
                self.tq_command_list.append(self.vrefupd_command)
        self.vref_command = self.tq_command_list

    def _create_payload_ad5684r(self, ad5684r_cmd, ad5684r_dac_adr, ad5684r_dac_val):
        return (ad5684r_cmd << (self.ad5684r_adr_bit_length + self.ad5684r_dac_bit_length)) | \
               (ad5684r_dac_adr << self.ad5684r_dac_bit_length) | \
               (ad5684r_dac_val << self.ad5684r_res_bit_length)

    def _create_constants_ad5684r(self, ad5684r_payload, ad5684r_ldacn=7):
        return {'payload': ad5684r_payload, 'target_interface': 0, 'payload_byte_count': 3,
                'read_write_direction': 1, 'ldac_bar': ad5684r_ldacn, 'command_data': 0, 'dma': True}

    def _choose_random_dac_channels_for_read(self):
        if len(self.active_dac_list):
            dac_channel = random.choice(self.active_dac_list)
            self.ad5684r_rdb_adr = 1 << dac_channel
        else:
            self.ad5684r_rdb_adr = 0

    def _choose_random_ldacn(self):
        self.ad5684r_ldacn = random.choice(self.ldacn_list)

    def _decode_dac_channels_for_write(self, dac_list):
        self.ad5684r_dac_adr = 0
        for i in range(4):
            if dac_list[i]:
                self.ad5684r_dac_adr = self.ad5684r_dac_adr + (1 << i)

    def _choose_random_dac_channels_for_write(self):
        self.active_dac_list = []
        self.ad5684r_dac_adr = 0
        self.expect_dac_val_next_set = self.observ_dac_val_set.copy()
        self.expect_vol_val_next_set = self.observ_vol_val_set.copy()
        dac_val = random.randrange(0, 0xB85)
        if dac_val >= 0xAE1:
            dac_val = 0xAE1
        if dac_val <= 334:
            dac_val = 334
        self.ad5684r_dac_val = dac_val
        vol_val = 2.5 * (dac_val / 0xFFF)
        for i in range(self.ad5684r_dac_count):
            if random.randrange(2):
                self.active_dac_list.append(i)
                self.expect_dac_val_next_set.update({i: dac_val})
                self.expect_vol_val_next_set.update({i: vol_val})
                self.ad5684r_dac_adr = self.ad5684r_dac_adr + (1 << i)


class Functional(BaseClass):
    """Feature tests using DDR for TQ operation

    Notes:
    1. START command is OPTIONAL for DC Trigger Queues (TQs).
    2. All TQs in memory MUST BE terminated with an END command. TQ execution will NOT STOP until END is reached.
    3. Blank (all zero bytes) or garbage data in memory CAN BE picked up for execution by TQ engine if there are LUT
    addresses pointing to these locations in memory.
    4. Broadcast operations will pick up TQs for all DUTs at the byte offset specified based on memory addresses pointed
     to in corresponding LUT addresses.
    5. When using broadcasts operation, memory locations corresponding to all DUTs need to be populated with valid TQs
    (i.e. TQs terminated with END command) for predictable operation. Can result in improper TQ processed count or
    tester shutdowns otherwise.
    """

    def setUp(self, tester=None):
        super().setUp()
        self.rm_device = self.env.hbicc.ring_multiplier
        self.pattern_helper = PatternHelper(self.env)
        self.env.hbicc.set_up_fval_alarm_logging(slices=[0,1,2,3,4])
        self.check_for_rm_device_availability()
        self.enable_aurora_bus_and_reset_power_state()
        self.switch_to_dctq_sync_delay_mode()

    def switch_to_dctq_sync_delay_mode(self):
        self.env.hbicc.ring_multiplier.switch_dctq_sync_delay_mode(syncdelay_mode=1)

    def DirectedLtc2975VOutWriteReadViaTqDDRTest(self, test_iteration=100):
        """Verify V_out setting on LTC2975 device (Aux Power Supply) using TQ records via DDR.

        **Test:**
        1. Create TQ records for initialization (general and page-specific) of different settings on the LTC2975 device,
         write V_out value (using VOUT_COMMAND command) and read back the V_out value from READ_VOUT register (Write and
          read values are both in L16 format).
        2. Store all generated TQs at same random byte offset from respective start address in memory for each DUT.
        3. Generate a Trigger for each DUT to execute the TQs stored in memory and send this trigger from RCTC
         to Ring Multiplier.
        4. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register (this is the value returned from the TQ
        that reads READ_VOUT register on LTC2975).
        5. Repeat step 1-4 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 TQ V_out values read back must match the values set (values are in L16 data format).

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Write_Read_VOut_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedLtm4678BroadcastTriggerScratchWriteCheckViaTqDDRTest(self, test_iteration=1000):
        """Form a Broadcast Trigger that triggers execution of scratch write and delay TQs via DDR and verify execution.

        **Test:**
        1. Create scratch write TQ records for two random DUTs and blank delay TQ records for all else.
        2. Store all generated TQs at same random byte offset from respective start address in memory for each DUT.
        3. Generate a Broadcast Trigger (DUT 63) with the same byte offset as used for the TQs stored in memory and
        send this trigger from RCTC to Ring Multiplier.
        4. At the Ring Multiplier, the Broadcast Trigger receipt should trigger execution of the trigger queue
        record at the corresponding byte offset (as in the Broadcast Trigger) for each DUT.
        5. TQ processing is completed if the TQ processed count is increased by 64 (i.e. total number of DUTs).
        6. Check if scratch writes were successful using separate TQ read calls.
        7. Repeat step 1-6 for 1000 times for each of the PMBus devices.

        **Criteria:** All scratch reads should match the corresponding scratch writes.

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Broadcast_Trigger_Via_TQ_DDR_Scratch_Write_Check'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedLtc2975BroadcastTriggerScratchWriteCheckViaTqDDRTest(self, test_iteration=1000):
        """Form a Broadcast Trigger that triggers execution of scratch write and delay TQs via DDR and verify execution.

        **Test:**
        1. Create scratch write TQ records for two random DUTs and blank delay TQ records for all else.
        2. Store all generated TQs at same random byte offset from respective start address in memory for each DUT.
        3. Generate a Broadcast Trigger (DUT 63) with the same byte offset as used for the TQs stored in memory and
        send this trigger from RCTC to Ring Multiplier.
        4. At the Ring Multiplier, the Broadcast Trigger receipt should trigger execution of the trigger queue
        record at the corresponding byte offset (as in the Broadcast Trigger) for each DUT.
        5. TQ processing is completed if the TQ processed count is increased by 64 (i.e. total number of DUTs).
        6. Check if scratch writes were successful using separate TQ read calls.
        7. Repeat step 1-6 for 1000 times for each of the PMBus devices.

        **Criteria:** All scratch reads should match the corresponding scratch writes.

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Broadcast_Trigger_Via_TQ_DDR_Scratch_Write_Check'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedLtm4678BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest(self, test_iteration=1000):
        """Form a Broadcast Trigger that triggers execution of delay TQs via DDR and verify TQ processed count increase.

        **Test:**
        1. Create blank delay TQ records for all DUTs.
        2. Store all generated TQs at same random byte offset from respective start address in memory for each DUT.
        3. Generate a Broadcast Trigger (DUT 63) with the same byte offset as used for the TQs stored in memory and
        send this trigger from RCTC to Ring Multiplier.
        4. At the Ring Multiplier, the Broadcast Trigger receipt should trigger execution of the trigger queue
        record at the corresponding byte offset (as in the Broadcast Trigger) for each DUT.
        5. Verify that TQ processing is completed by checking if TQ processed count is increased by 64 (i.e. total
        number of DUTs).
        6. Repeat step 1-4 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 TQ processing count increases should match the total number of DUTs.

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Processed_Count_Check'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedLtc2975BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest(self, test_iteration=1000):
        """Form a Broadcast Trigger that triggers execution of delay TQs via DDR and verify TQ processed count increase.

        **Test:**
        1. Create blank delay TQ records for all DUTs.
        2. Store all generated TQs at same random byte offset from respective start address in memory for each DUT.
        3. Generate a Broadcast Trigger (DUT 63) with the same byte offset as used for the TQs stored in memory and
        send this trigger from RCTC to Ring Multiplier.
        4. At the Ring Multiplier, the Broadcast Trigger receipt should trigger execution of the trigger queue
        record at the corresponding byte offset (as in the Broadcast Trigger) for each DUT.
        5. Verify that TQ processing is completed by checking if TQ processed count is increased by 64 (i.e. total
        number of DUTs).
        6. Repeat step 1-4 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 TQ processing count increases should match the total number of DUTs.

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Broadcast_Trigger_Via_TQ_DDR_Blank_Delay_TQ_Processed_Count_Check'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedLtm4678DelayViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR creating random delay in the range 0x0000 - 0xFFFF microseconds.

        **Test:**
        1. Create a delay trigger queue record for creating random delay during TQ execution.
        2. Start recording trigger processing time right after trigger has been sent from RCTC to RM.
        (Done here to remove any effect of the simulator execution flow on time calculations)
        3. Stop recording trigger processing time right after trigger processing is complete based on
        trigger execution counter.
        4. Check if trigger processing time (i.e. end_time - trigger_send_time - start_time) is
        within an absolute error threshold (400 microseconds) or absolute percentage error threshold (2%).
        trigger_send_time (50 microseconds) is the processing time for a blank delay TQ command.
        Absolute error / percentage error threshold is used to account for delay variance at different delay
        values in the range used (0 - 65535).
        5. Repeat step 1-4 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 processing times should match the expected delays.

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Random_Delay_Via_TQ_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedLtc2975DelayViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR creating random delay in the range 0x0000 - 0xFFFF microseconds.

        **Test:**
        1. Create a delay trigger queue record for creating random delay during TQ execution.
        2. Start recording trigger processing time right after trigger has been sent from RCTC to RM.
        (Done here to remove any effect of the simulator execution flow on time calculations)
        3. Stop recording trigger processing time right after trigger processing is complete based on
        trigger execution counter.
        4. Check if trigger processing time (i.e. end_time - trigger_send_time - start_time) is
        within an absolute error threshold (400 microseconds) or absolute percentage error threshold (2%).
        trigger_send_time (50 microseconds) is the processing time for a blank delay TQ command.
        Absolute error / percentage error threshold is used to account for delay variance at different delay
        values in the range used (0 - 65535).
        5. Repeat step 1-4 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 processing times should match the expected delays.

        **NOTE:**
        """
        psdb_id = self.select_psdb_target_if_available()
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Random_Delay_Via_TQ_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(psdb_id)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB0Ltm4678VersionAndCapabilityReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to read revision number and capability.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read
        for all the PMBus devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Read_Version_Via_DDR', 'Read_Capability_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(0)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def I2CNakFailureTest(self):
        """Negtive Test for I2C NAK Alarm Status & Reset

        **NOTE:**
        Might need to use several million i2c read loops to repoduce error.
        """
        self.DirectedPSDB0Ltm4678VersionAndCapabilityReadViaTqDDRTest(300000)

    def DirectedPSDB1Ltm4678VersionAndCapabilityReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to read revision number and capability.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read
        for all the PMBus devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('PSDB 1 unavailable')
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Read_Version_Via_DDR', 'Read_Capability_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(1)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB0Ltc2975VersionAndCapabilityReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to read revision number and capability.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read
        for all the PMBus devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Read_Version_Via_DDR', 'Read_Capability_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(0)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB1Ltc2975VersionAndCapabilityReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to read revision number and capability.

        **Test:**
        1. Create a trigger queue record for read with the pmbus command encoding and number of bytes to be read
        for all the PMBus devices.
        2. Read back the value from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 for 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('PSDB 1 unavailable')
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Read_Version_Via_DDR', 'Read_Capability_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(1)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB0Ltm4678ScratchWriteReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to write random value to scratch register.

        **Test:**
        1. Create a trigger queue record to write a random reference value to scratch register (0xB3), effect a nominal
        delay (using TQ command) and read the value from the same register.
        2. Read back the value read from scratch from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Write_Read_Scratch_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(0)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB1Ltm4678ScratchWriteReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to write random value to scratch register.

        **Test:**
        1. Create a trigger queue record to write a random reference value to scratch register (0xB3), effect a nominal
        delay (using TQ command) and read the value from the same register.
        2. Read back the value read from scratch from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('PSDB 1 unavailable')
        tq_test_scenario = TQTestScenarioLTM4678(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Write_Read_Scratch_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(1)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB0Ltc2975ScratchWriteReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to write random value to scratch register.

        **Test:**
        1. Create a trigger queue record to write a random reference value to scratch register (0xB3), effect a nominal
        delay (using TQ command) and read the value from the same register.
        2. Read back the value read from scratch from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 1000 times for each of the PMBus devices.

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Write_Read_Scratch_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(0)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB1Ltc2975ScratchWriteReadViaTqDDRTest(self, test_iteration=1000):
        """Verify execution of TQ record via DDR to write random value to scratch register.

        **Test:**
        1. Create a trigger queue record to write a random reference value to scratch register (0xB3), effect a nominal
        delay (using TQ command) and read the value from the same register.
        2. Read back the value read from scratch from the PSDB_DC_TRIGGER_PAGE_I2C_DATA register.
        3. Compare the reference value to the read back value.
        4. Repeat step 1-3 1000 times for each of the PMBus devices.\

        **Criteria:** All 1000 read back values should match the reference values for all registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('PSDB 1 unavailable')
        tq_test_scenario = TQTestScenarioLTC2975(test_case=self)
        tq_test_scenario.set_all_scenarios_list(['Write_Read_Scratch_Via_DDR'])
        tq_test_scenario.set_no_of_test_iterations(test_iteration)
        tq_test_scenario.set_psdb_target(1)
        tq_test_scenario.execute()
        self.pattern_helper.check_all_alarms()
        tq_test_scenario.report_results()

    def DirectedPSDB0Ad5684rVrefSetReadViaTqDMATest(self, test_iteration=100):
        """Use DC TQ to write DAC value and vref to ad5684r device through DMA and read back through SPI.

        This test will set random DACs with random DAC value through random write, update, ldacn command,
        then read back DAC channel registers and voltage monitor

        **Test:**
        1. Create a trigger queue record for write, update, readback, noop (noop is requreied for readback)
        for ad5684r through spi.
        2. Read back the value from the PSDB0_DC_TRIGGER_PAGE_SPI_DATA register and voltage monitor.
        3. Compare the reference value to the read back value
        4. Repeat step 1-3 for test_iteration times for ad5684r.

        **Criteria:** All read back values should match the reference values for all DAC registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target=0)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.update_expect_val_set()
        for i in range(test_iteration):
            tq_test_ad5684r.send_wri_upd_command_set_random_vref()
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.check_register_val_result(i)
            tq_test_ad5684r.check_voltage_val_result(i)
            if tq_test_ad5684r.error_count >= 10:
                break
        self.pattern_helper.check_all_alarms()

    def DirectedPSDB1Ad5684rVrefSetReadViaTqDMATest(self, test_iteration=100):
        """Use DC TQ to write DAC value and vref to ad5684r device through DMA and read back through SPI.

        This test will set random DACs with random DAC value through random write, update, ldacn command,
        then read back DAC channel registers and voltage monitor

        **Test:**
        1. Create a trigger queue record for write, update, readback, noop (noop is requreied for readback)
        for ad5684r through spi.
        2. Read back the value from the PSDB1_DC_TRIGGER_PAGE_SPI_DATA register and voltage monitor.
        3. Compare the reference value to the read back value
        4. Repeat step 1-3 for test_iteration times for ad5684r.

        **Criteria:** All read back values should match the reference values for all DAC registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target=1)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.update_expect_val_set()
        for i in range(test_iteration):
            tq_test_ad5684r.send_wri_upd_command_set_random_vref()
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.check_register_val_result(i)
            tq_test_ad5684r.check_voltage_val_result(i)
            if tq_test_ad5684r.error_count >= 10:
                break
        self.pattern_helper.check_all_alarms()

    def DirectedPSDB0Ad5684rVrefSeperateSetReadViaTqDMATest(self, test_iteration=100):
        """Use DC TQ to write DAC value and vref to ad5684r device through DMA and read back through SPI.

        This test will set random DACs with random DAC value through random write, update, ldacn command,
        then read back DAC channel registers and voltage monitor

        **Test:**
        1. Create a trigger queue record for write with ldacn=7, readback, noop (noop is requreied for readback)
        for ad5684r through spi.
        2. Read back the value from the PSDB0_DC_TRIGGER_PAGE_SPI_DATA register and voltage monitor.
        3. Compare the reference value to the read back value
        4. Create a trigger queue record for update, readback, noop (noop is requreied for readback)
        for ad5684r through spi.
        5. Read back the value from the PSDB0_DC_TRIGGER_PAGE_SPI_DATA register and voltage monitor.
        6. Repeat step 1-5 for test_iteration times for ad5684r.

        **Criteria:** All read back values should match the reference values for all DAC registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target=0)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.update_expect_val_set()
        for i in range(test_iteration):
            tq_test_ad5684r.send_wri_command_set_random_vref()
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            # tq_test_ad5684r.check_register_val_result(i)
            tq_test_ad5684r.check_voltage_val_result(i)
            tq_test_ad5684r.send_upd_command_set_random_vref()
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.check_register_val_result(i)
            tq_test_ad5684r.check_voltage_val_result(i)
            if tq_test_ad5684r.error_count >= 10:
                break
        self.pattern_helper.check_all_alarms()

    def DirectedPSDB1Ad5684rVrefSeperateSetReadViaTqDMATest(self, test_iteration=100):
        """Use DC TQ to write DAC value and vref to ad5684r device through DMA and read back through SPI.

        This test will set random DACs with random DAC value through random write, update, ldacn command,
        then read back DAC channel registers and voltage monitor

        **Test:**
        1. Create a trigger queue record for write with ldacn=7, readback, noop (noop is requreied for readback)
        for ad5684r through spi.
        2. Read back the value from the PSDB1_DC_TRIGGER_PAGE_SPI_DATA register and voltage monitor.
        3. Compare the reference value to the read back value
        4. Create a trigger queue record for update, readback, noop (noop is requreied for readback)
        for ad5684r through spi.
        5. Read back the value from the PSDB1_DC_TRIGGER_PAGE_SPI_DATA register and voltage monitor.
        6. Repeat step 1-5 for test_iteration times for ad5684r.

        **Criteria:** All read back values should match the reference values for all DAC registers

        **NOTE:**
        """
        if self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target=1)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.update_expect_val_set()
        for i in range(test_iteration):
            tq_test_ad5684r.send_wri_command_set_random_vref()
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            # tq_test_ad5684r.check_register_val_result(i)
            tq_test_ad5684r.check_voltage_val_result(i)
            tq_test_ad5684r.send_upd_command_set_random_vref()
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.check_register_val_result(i)
            tq_test_ad5684r.check_voltage_val_result(i)
            if tq_test_ad5684r.error_count >= 10:
                break
        self.pattern_helper.check_all_alarms()

    def DirectedPSDB0VccioVrefBroadcastDMATest(self):
        """Use DC broadcast TQ to write vccio and vref to ltm4678 and ad5684r devices through DMA and read back.

        This test will set vccio & vref through dut_id 63 with broadcast. This is to mimic TOS vccio/vref setting.
        **Test:**
        1. Create a trigger queue record for vccio & vref setting
        2. Set all 64 dut_id address and trigger queue content
        3. Send broadcast trigger
        4. Readback vccio & vref value
        5. Change voltage vccio voltage back to 1.8V, vref voltage to 0.9V

        **Criteria:** All read back values should match the reference values

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        self.vccio_vref_devices_init(psdb=0)
        self.vccio_vref_value_set(vout=1.6, vref=0.8, psdb=0)
        vccio_setting = 0.3  # VCCIO takes aboout 0.2s to update. Use 0.3s here just to be safe.
        time.sleep(vccio_setting)
        self.vccio_vref_value_check(vout=1.6, vref=0.8)
        self.vccio_vref_value_set(vout=1.8, vref=0.9, psdb=0)
        time.sleep(vccio_setting)
        self.vccio_vref_value_check(vout=1.8, vref=0.9)
        self.pattern_helper.check_all_alarms()

    def DirectedPSDB0VccioI2CCommandDropDMATest(self):
        """Use DMA DC TQ to set multiple registers in ltm4678 device and read back to check any I2C command drop.

        This test will send 32,64,96,128,160,196 commands through several single TQs. This is to check a fixed RM bug.
        TFS HDMT Ticket Number: 107472
        **Test:**
        1. Store VCCIO device register value
        2. Run 6 seperate single trigger queue test with 32,64,96,128,160,196 commands
        3. Restore VCCIO device register value

        **Criteria:** All read back values should match the reference values

        **NOTE:**
        """
        if self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('PSDB 0 unavailable')
        self.vccio_device_init(psdb=0)
        self.backup_vccio_device_register_value(psdb=0, target=1)
        try:
            self.various_length_i2c_command_check(psdb=0, target=1, loops=6)
        finally:
            self.restore_vccio_device_register_value(psdb=0, target=1)
        self.pattern_helper.check_all_alarms()

    def vccio_device_init(self, psdb):
        self.tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        self.tq_test_ltm4678.device_init()
        self.tq_test_ltm4678.set_psdb_target(psdb_target=psdb)

    def vref_device_init(self, psdb):
        self.tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        self.tq_test_ad5684r.device_init()
        self.tq_test_ad5684r.set_psdb_target(psdb_target=psdb)

    def restore_vccio_device_register_value(self, psdb, target):
        self.tq_test_ltm4678.restore_register_file()
        self.tq_test_ltm4678.restore_register_file_to_device(psdb, target)
        self.tq_test_ltm4678.print_register_file()
        self.tq_test_ltm4678.register_file_check_from_device(psdb, target)

    def backup_vccio_device_register_value(self, psdb, target):
        self.tq_test_ltm4678.register_file_update_from_device(psdb, target)
        self.tq_test_ltm4678.backup_register_file()
        self.tq_test_ltm4678.print_register_file()
        self.tq_test_ltm4678.register_file_check_from_device(psdb, target)

    def various_length_i2c_command_check(self, psdb, target, loops):
        for loop in range(1, loops + 1):
            self.tq_test_ltm4678.increase_and_write_register_file_to_device_multi_times(psdb, target, loop)
            self.tq_test_ltm4678.register_file_check_from_device(psdb, target)

    def vccio_vref_devices_init(self, psdb):
        self.tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        self.tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        self.tq_test_ad5684r.device_init()
        self.tq_test_ltm4678.device_init()
        self.tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
        self.tq_test_ltm4678.set_psdb_target(psdb_target=psdb)

    def vccio_vref_value_set(self, vout, vref, psdb):
        vccio_tq = self.tq_test_ltm4678.vccio_set_tq_content(uv=0, ov=2, vout=vout, psdb=psdb)
        delay_tq = [self.tq_test_ltm4678.delay_trigger_queue_record(11)]
        vref_tq_initial = self.tq_test_ad5684r.vref_set_tq_content(vref=0.5, psdb=psdb)
        vref_tq_final = self.tq_test_ad5684r.vref_set_tq_content(vref=vref, psdb=psdb)
        tq_list = vref_tq_initial + vccio_tq + delay_tq + vref_tq_final
        self.tq_test_ltm4678.combine_tq_content_list(tq_list)
        self.tq_test_ltm4678.send_vccio_vref_broadcast_dma()

    def vccio_vref_value_check(self, vout, vref):
        self.tq_test_ltm4678.check_vccio_voltage_val(vout)
        self.tq_test_ad5684r.check_vref_voltage_val(vref)

    def Vccio_Vref_Limit_Test(self, vout=1.9):
        if self.rm_device is None or self.env.hbicc.psdb_0_pin_1 is None or self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('MB/PSDB 0/1 unavailable')
        self.enable_aurora_bus_and_reset_power_state()

        psdb = 0
        self.Log('info', f'setting vccio of psdb{psdb}')
        self.setvccio(psdb, vout)
        psdb = 1
        self.Log('info', f'setting vccio of psdb{psdb}')
        self.setvccio(psdb, vout)

        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')

        for i in range(7):
            x = 0.05 * i
            psdb = 0
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'setting vref of psdb{psdb} to 0')
            tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(0), dac_list=[1, 1, 1, 1])
            psdb = 1
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'setting vref of psdb{psdb} to 0')
            tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(0), dac_list=[1, 1, 1, 1])
            time.sleep(1)
            self.print_pm_version()
            psdb = 0
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'read vref voltage of psdb{psdb}, should be {0}')
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.vref_result_print()
            psdb = 1
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'read vref voltage of psdb{psdb}, should be {0}')
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.vref_result_print()
            self.print_pm_version()
            psdb = 0
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'setting vref of psdb{psdb} to {vout - 0.2 + x}')
            tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(vout - 0.2 + x), dac_list=[1, 1, 1, 1])
            psdb = 1
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'setting vref of psdb{psdb} to {vout - 0.2 + x}')
            tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(vout - 0.2 + x), dac_list=[1, 1, 1, 1])
            time.sleep(1)
            self.print_pm_version()
            psdb = 0
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'read vref voltage of psdb{psdb}, should be {vout - 0.2 + x}')
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.vref_result_print()
            psdb = 1
            tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
            self.Log('info', f'read vref voltage of psdb{psdb}, should be {vout - 0.2 + x}')
            tq_test_ad5684r.readback_register_val()
            tq_test_ad5684r.readback_voltage_val()
            tq_test_ad5684r.vref_result_print()
            self.print_pm_version()

    def Vccio_Vref_Margin_Test_one_psdb(self, vout=1.0, psdb=0):
        if self.rm_device is None or self.env.hbicc.psdb_0_pin_1 is None or self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('MB/PSDB 0/1 unavailable')
        self.enable_aurora_bus_and_reset_power_state()
        self.Log('info', f'setting vccio of psdb{psdb}')
        self.setvccio(psdb, 1.8)
        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target=psdb)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        self.Log('info', f'setting vref of psdb{psdb}')
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(vout / 2), dac_list=[1, 1, 1, 1])
        time.sleep(2)
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(vout / 2), dac_list=[1, 1, 1, 1])
        time.sleep(2)
        self.Log('info', f'read vref voltage, should be {vout / 2}')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.print_pm_version()
        self.Log('info', f'setting vref of psdb{psdb} to vccio - 0.2V')
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=(vout - 0.2), dac_list=[1, 1, 1, 1])
        time.sleep(1)
        self.Log('info', f'read vref voltage, should be {vout - 0.2}')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.print_pm_version()
        self.Log('info', f'start 100KHz Vref toggle between 0V and {vout - 0.2}V for 30M times for psdb{psdb}')
        for i in range(5 * 60 * 60):
            tq_test_ad5684r.set_vref_as_clock(vref_voltage=(vout - 0.2), dac_list=[1, 1, 1, 1], period_in_us=20000,
                                              times=10)
            if ((i % (5)) and (i < 50)):
                self.Log('info', f'running for {i // 5} seconds')
                self.print_pm_version()
            if (i % (5 * 10) == 0) and ((i >= 50) and (i < (5 * 10 * 6))):
                self.Log('info', f'running for {i // 5} seconds')
                self.print_pm_version()
            if (i % (5 * 10 * 6) == 0) and ((i >= (5 * 10 * 6))):
                self.Log('info', f'running for {i // (5 * 10 * 6)} minutes')
                self.print_pm_version()
        time.sleep(1)
        self.print_pm_version()

        self.Log('info', f'reading vref of psdb{psdb}')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()

        self.Log('info', f'setting vref of psdb{psdb}')
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=vout / 2, dac_list=[1, 1, 1, 1])
        time.sleep(1)
        self.Log('info', f'read vref voltage, should be {vout / 2}')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.print_pm_version()

    def set_vccio_voltage(self, vccio_voltage=1.8):
        self.enable_aurora_bus_and_reset_power_state()
        if self.env.hbicc.psdb_0_pin_1:
            self.read_vccio_setting(psdb=0)
            self.update_vccio_setting(psdb=0, vccio_voltage=vccio_voltage)
            self.read_vccio_setting(psdb=0)
        if self.env.hbicc.psdb_1_pin_1:
            self.read_vccio_setting(psdb=1)
            self.update_vccio_setting(psdb=1, vccio_voltage=vccio_voltage)
            self.read_vccio_setting(psdb=1)

    def read_vccio_setting(self, psdb):
        tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        tq_test_ltm4678.device_init()
        tq_test_ltm4678.set_psdb_target(psdb_target=psdb)
        vccio_channel_list = ['vccio_0_0', 'vccio_1_0', 'vccio_2_0', 'vccio_3_0',
                              'vccio_0_1', 'vccio_1_1', 'vccio_2_1', 'vccio_3_1']
        for vccio_channel in vccio_channel_list:
            tq_test_ltm4678.select_vccio_channel(vccio_channel)
            self.Log('info', f'reading {vccio_channel} setting of psdb{psdb}')
            tq_test_ltm4678.read_voltage_settings()

    def update_vccio_setting(self, psdb, vccio_voltage):
        tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        tq_test_ltm4678.device_init()
        tq_test_ltm4678.set_psdb_target(psdb_target=psdb)
        vccio_channel_list = ['vccio_0_0', 'vccio_1_0', 'vccio_2_0', 'vccio_3_0',
                              'vccio_0_1', 'vccio_1_1', 'vccio_2_1', 'vccio_3_1']
        for vccio_channel in vccio_channel_list:
            tq_test_ltm4678.select_vccio_channel(vccio_channel)
            tq_test_ltm4678.set_uv_ov_vout_value(uv=vccio_voltage - 0.2, ov=vccio_voltage + 0.2, vout=vccio_voltage)
            tq_test_ltm4678.write_voltage_setting()

    def setvccio(self, psdb, vout):
        tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        tq_test_ltm4678.device_init()
        tq_test_ltm4678.set_psdb_target(psdb_target=psdb)
        tq_test_ltm4678.select_vccio_channel('vccio_0_0')
        self.Log('info', 'vccio_0_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_0')
        self.Log('info', 'vccio_1_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_0')
        self.Log('info', 'vccio_2_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        self.Log('info', 'vccio_3_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_0_1')
        self.Log('info', 'vccio_0_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_1')
        self.Log('info', 'vccio_1_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_1')
        self.Log('info', 'vccio_2_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_1')
        self.Log('info', 'vccio_3_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_0_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_1_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_2_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_0_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_1_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_2_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_3_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=vout)
        tq_test_ltm4678.write_voltage_setting()
        time.sleep(1)
        tq_test_ltm4678.select_vccio_channel('vccio_0_0')
        self.Log('info', 'vccio_0_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_0')
        self.Log('info', 'vccio_1_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_0')
        self.Log('info', 'vccio_2_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        self.Log('info', 'vccio_3_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_0_1')
        self.Log('info', 'vccio_0_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_1')
        self.Log('info', 'vccio_1_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_1')
        self.Log('info', 'vccio_2_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_1')
        self.Log('info', 'vccio_3_1')
        tq_test_ltm4678.read_voltage_settings()

    def Vccio_Vref_Margin_Test(self):
        if self.rm_device is None or self.env.hbicc.psdb_0_pin_1 is None or self.env.hbicc.psdb_1_pin_1 is None:
            raise SkipTest('MB/PSDB 0/1 unavailable')
        self.enable_aurora_bus_and_reset_power_state()
        tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        tq_test_ltm4678.device_init()
        tq_test_ltm4678.set_psdb_target(psdb_target=0)
        tq_test_ltm4678.select_vccio_channel('vccio_0_0')
        self.Log('info', 'vccio_0_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_0')
        self.Log('info', 'vccio_1_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_0')
        self.Log('info', 'vccio_2_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        self.Log('info', 'vccio_3_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_0_1')
        self.Log('info', 'vccio_0_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_1')
        self.Log('info', 'vccio_1_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_1')
        self.Log('info', 'vccio_2_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_1')
        self.Log('info', 'vccio_3_1')
        tq_test_ltm4678.read_voltage_settings()

        tq_test_ltm4678.select_vccio_channel('vccio_0_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_1_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_2_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_0_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_1_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_2_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        tq_test_ltm4678.select_vccio_channel('vccio_3_1')
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()

        time.sleep(1)

        tq_test_ltm4678.select_vccio_channel('vccio_0_0')
        self.Log('info', 'vccio_0_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_0')
        self.Log('info', 'vccio_1_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_0')
        self.Log('info', 'vccio_2_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        self.Log('info', 'vccio_3_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_0_1')
        self.Log('info', 'vccio_0_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_1_1')
        self.Log('info', 'vccio_1_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_2_1')
        self.Log('info', 'vccio_2_1')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.select_vccio_channel('vccio_3_1')
        self.Log('info', 'vccio_3_1')
        tq_test_ltm4678.read_voltage_settings()

        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target=0)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 1.8')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=1.8, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 1.8')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 1.85')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=1.85, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 1.85')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 1.9')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=1.9, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 1.9')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 1.95')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=1.95, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 1.95')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.05')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.05, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.05')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.1')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.1, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.1')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.15')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.15, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.15')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.2')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.2, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.2')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.25')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.25, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.25')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.3')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.3, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.3')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.35')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.35, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.35')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 0')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 0')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        self.Log('info', 'start set vref to 2.4')
        self.print_pm_version()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=2.4, dac_list=[1, 1, 1, 1])
        self.Log('info', 'end set vref to 2.4')
        self.print_pm_version()
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()

    def print_pm_version(self):
        list = self.env.hbicc.check_pin_version()
        self.Log('info', f'pm0 {list[0]} pm1 {list[1]} pm2 {list[2]} pm3 {list[3]}')

    def LTM4678SetVccioViaTqDMA(self):
        if self.rm_device is None or (self.env.hbicc.psdb_0_pin_1 is None and self.env.hbicc.psdb_1_pin_1 is None):
            raise SkipTest('MB/PSDB 0 and 1 unavailable')
        self.enable_aurora_bus_and_reset_power_state()
        tq_test_ltm4678 = TQTestScenarioLTM4678_VCCIO(test_case=self)
        tq_test_ltm4678.device_init()
        tq_test_ltm4678.select_vccio_channel('vccio_3_0')
        tq_test_ltm4678.read_voltage_settings()
        tq_test_ltm4678.set_uv_ov_vout_value(uv=0, ov=2.3, vout=1.9)
        tq_test_ltm4678.write_voltage_setting()
        time.sleep(1)
        tq_test_ltm4678.read_voltage_settings()

    def Ad5684rSetVrefViaTqDMA(self):
        if self.rm_device is None or self.env.hbicc.psdb_0_pin_1 is None:
            raise SkipTest('MB/PSDB 0 unavailable')
        if self.env.hbicc.psdb_0_pin_1 is None:
            psdb_target = 1
        else:
            psdb_target = 0
        self.enable_aurora_bus_and_reset_power_state()
        tq_test_ad5684r = TQTestScenarioAD5684r(test_case=self)
        tq_test_ad5684r.device_init()
        tq_test_ad5684r.set_psdb_target(psdb_target)
        tq_test_ad5684r.set_trigger_mode(dma_or_pcie='dma')
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=0, dac_list=[0, 0, 0, 1])
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()
        tq_test_ad5684r.send_wup_command_set_vref(vref_voltage=1.8, dac_list=[0, 0, 0, 1])
        time.sleep(1)
        tq_test_ad5684r.readback_register_val()
        tq_test_ad5684r.readback_voltage_val()
        tq_test_ad5684r.vref_result_print()

    def enable_aurora_bus_and_reset_power_state(self):
        self.enable_aurora_bus(1)
        self.reset_power_state()

    def reset_power_state(self):
        self.rm_device.write_dc_trigger_dut_power_state(0, 0)
        time.sleep(0.1)

    def enable_aurora_bus(self, enable=1):
        self.rm_device.write_dc_trigger_enable_aurora_data(enable)
        time.sleep(0.1)

    def check_for_rm_device_availability(self):
        if self.rm_device is None:
            raise SkipTest('Ring Multiplier is NOT available')

    def select_psdb_target_if_available(self):
        if self.env.hbicc.psdb_0_pin_1 and self.env.hbicc.psdb_1_pin_1:
            psdb_id = random.randint(0, 1)
            self.Log('info', f'Both PSDBs available. Testing with PSDB: {psdb_id}.')
        elif self.env.hbicc.psdb_0_pin_1:
            psdb_id = 0
            self.Log('info', f'Testing with PSDB: {psdb_id}.')
        elif self.env.hbicc.psdb_1_pin_1:
            psdb_id = 1
            self.Log('info', f'Testing with PSDB: {psdb_id}.')
        else:
            raise SkipTest('NO PSDBs available')
        return psdb_id

    def vccf_vccio_tq_without_sync_delay(self, vref_initial, vref_final, vccio, vccio_delay):
        vref_initial_command_list = self.vref_commands(vref_initial)
        vccio_with_delay_command_list = self.vccio_commands_with_delay(vccio, vccio_delay)
        vref_final_command_list = self.vref_commands(vref_final)
        vref_vccio_command_list = vref_initial_command_list + vccio_with_delay_command_list + vref_final_command_list
        return self.vccf_vccio_tq_with_start_end_commands(vref_vccio_command_list)

    def vccf_vccio_tq_with_one_sync_delay(self, vref_initial, vref_final, vccio, vccio_delay, time_sync):
        sync_delay_command = self.env.hbicc.ring_multiplier.sync_delay_within_trigger_queue_record(delay=time_sync)
        vref_initial_command_list = self.vref_commands(vref_initial)
        vccio_with_delay_command_list = self.vccio_commands_with_delay(vccio, vccio_delay)
        vref_final_command_list = self.vref_commands(vref_final)
        vref_vccio_command_list = vref_initial_command_list + vccio_with_delay_command_list + [sync_delay_command] + \
                                  vref_final_command_list
        return self.vccf_vccio_tq_with_start_end_commands(vref_vccio_command_list)

    def vccf_vccio_tq_with_two_sync_delay(self, vref_initial, vref_final, vccio, vccio_delay, time_sync):
        sync_delay_command = self.env.hbicc.ring_multiplier.sync_delay_within_trigger_queue_record(delay=time_sync)
        vref_vccio_command_list = self.vccf_vccio_tq_without_sync_delay(vref_initial, vref_final, vccio, vccio_delay)
        vref_vccio_command_list = [sync_delay_command] + vref_vccio_command_list + [sync_delay_command]
        return self.vccf_vccio_tq_with_start_end_commands(vref_vccio_command_list)

    def vccf_vccio_tq_with_start_end_commands(self, vref_vccio_command_list):
        start_command = self.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0, dma=True)
        end_command = self.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0, dma=True)
        return [start_command] + vref_vccio_command_list + [end_command]

    def vref_commands(self, vref):
        vref_tq_list = []
        number_of_psdb = 2
        for psdb in range(number_of_psdb):
            self.vref_device_init(psdb)
            vref_tq_list = vref_tq_list + self.tq_test_ad5684r.vref_set_tq_content(vref=vref, psdb=psdb)
        return vref_tq_list

    def vccio_commands_with_delay(self, vccio, delay_us):
        vccio_tq_list = []
        for psdb in range(2):
            self.vccio_device_init(psdb)
            vccio_tq_list = vccio_tq_list + self.tq_test_ltm4678.vccio_set_tq_content(uv=0, ov=2, vout=vccio, psdb=psdb)
        if delay_us:
            delay_tq = [self.tq_test_ltm4678.delay_trigger_queue_record(delay_us)]
            return vccio_tq_list + delay_tq
        else:
            return vccio_tq_list


class PinFixedDriveState(HbiccTest):
    """Force pin control tests"""

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))
        self.slices.sort()

    def DirectedPSDB0DrivePinHighTest(self):
        psdb_target = 0
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=[4, 7, 2, 1])
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB0DrivePinLowTest(self):
        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(channel_sets=[3, 5, 6, 0])
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB1DrivePinHighTest(self):
        psdb_target = 1
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(channel_sets=[8, 11, 14, 10])
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB1DrivePinLowTest(self):
        psdb_target = 1
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=[9, 12, 15, 13])
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB0RandomPinStateTest(self):
        psdb_target = 0
        fixed_drive_state = 'RANDOM'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=[0, 4, 1, 6],
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB1RandomPinStateTest(self):
        psdb_target = 1
        fixed_drive_state = 'RANDOM'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=[10, 11, 13, 15],
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB0RandomPinStateDMATest(self):
        self.aurora_link_setup()
        psdb_target = 0
        fixed_drive_state = 'RANDOM'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=[0, 4, 1, 6],
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state_dma(psdb_target, dma_random=True)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB1RandomPinStateDMATest(self):
        self.aurora_link_setup()
        psdb_target = 1
        fixed_drive_state = 'RANDOM'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=[10, 11, 13, 15],
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.pattern_helper.set_pin_fixed_drive_state_dma(psdb_target, dma_random=True)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)

    def DirectedPSDB0RandomMaskPinStateDMATest(self):
        psdb_target = 0
        self.aurora_link_setup()
        self.pattern_helper.initialize_fds()
        self.fixed_drive_state_set_zero_value(psdb_target)
        for i in range(100):
            self.fixed_drive_state_set_random_mask(psdb_target)
            self.fixed_drive_state_set_random_value(psdb_target)
            time.sleep(SETTLING_TIME)
            self.env.hbicc.check_pin_states(self.complete_set, 'RANDOM')
        self.fixed_drive_state_clear_mask(psdb_target)

    def fixed_drive_state_set_zero_value(self, psdb_target):
        self.slices_population = [0, 1, 2, 3, 4]
        self.channel_sets_population = [0, 1, 2, 3, 4, 5, 6, 7]
        self.pattern_helper.user_mode = fixed_drive_state = 'LOW'
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices_population,
                                                                   channel_sets=self.channel_sets_population,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)
        self.pattern_helper.set_drive_low_on_all_active_slice_channel_sets(psdb_target)
        self.complete_set = list(self.pattern_helper.slice_channel_sets_combo.values())

    def fixed_drive_state_clear_mask(self, psdb_target):
        slices = self.slices_population
        channel_sets = self.channel_sets_population
        self.pattern_helper.update_slice_channel_set_pattern_combo(slices=slices,
                                                                   channel_sets=channel_sets,
                                                                   fixed_drive_state='MASK')
        self.pattern_helper.set_pin_fixed_drive_mask_dma(psdb_target, slices, channel_sets, 'MASKNON')

    def fixed_drive_state_set_random_value(self, psdb_target):
        slices = random.sample(self.slices_population, random.randint(1, 5))
        channel_sets = random.sample(self.channel_sets_population, random.randint(1, 8))
        self.pattern_helper.update_slice_channel_set_pattern_combo(slices=slices,
                                                                   channel_sets=channel_sets,
                                                                   fixed_drive_state='RANDOM')
        self.pattern_helper.set_pin_fixed_drive_state_dma(psdb_target, dma_random=True)

    def fixed_drive_state_set_random_mask(self, psdb_target):
        slices = random.sample(self.slices_population, random.randint(1, 5))
        channel_sets = random.sample(self.channel_sets_population, random.randint(1, 8))
        self.pattern_helper.update_slice_channel_set_pattern_combo(slices=slices,
                                                                   channel_sets=channel_sets,
                                                                   fixed_drive_state='MASK')
        self.pattern_helper.set_pin_fixed_drive_mask_dma(psdb_target, slices, channel_sets, 'MASKRANDOM')

    def TristatePinStateTest(self):
        psdb_target = 0
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=[0], channel_sets=[0, 1])
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.6)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=0.6)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)
        time.sleep(SETTLING_TIME)
        self.env.hbicc.check_pin_states(list(self.pattern_helper.slice_channel_sets_combo.values()), fixed_drive_state)
        fixed_drive_state = 'TRISTATE'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target)

    def DirectedPSDB0AllSetRandomPinStateDMATest(self, psdb_target=0, test_iteration=10):
        self.slices = [0, 1, 2, 3, 4]
        self.channel_sets = [0, 1, 2, 3, 4, 5, 6, 7]
        self.body_all_set_random_pin_state(psdb_target, test_iteration)

    def DirectedPSDB1AllSetRandomPinStateDMATest(self, psdb_target=1, test_iteration=10):
        self.slices = [0, 1, 2, 3, 4]
        self.channel_sets = [8, 9, 10, 11, 12, 13, 14, 15]
        self.body_all_set_random_pin_state(psdb_target, test_iteration)

    def body_all_set_random_pin_state(self, psdb_target, test_iteration):
        original_channel_sets = self.channel_sets
        self.aurora_link_setup()
        self.pattern_helper.initialize_fds()
        self.vref_set_pcie(psdb_target, 0.9)
        self.init_pin_states()
        fixed_drive_state = 'HIGH'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=self.channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.set_pin_fixed_drive_state_dma(psdb_target, dma_random=True)
        complete_set = list(self.pattern_helper.slice_channel_sets_combo.values())
        self.env.hbicc.check_pin_states(complete_set, fixed_drive_state)
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=self.slices, channel_sets=self.channel_sets,
                                                                   fixed_drive_state=fixed_drive_state)
        self.pattern_helper.set_pin_fixed_drive_state_dma(psdb_target, dma_random=True)
        self.env.hbicc.check_pin_states(complete_set, fixed_drive_state)
        for i in range(test_iteration):
            slices_population = set(range(0, 5))
            self.slices = random.sample(slices_population, random.randint(1, 5))
            channel_sets_population = set(original_channel_sets)
            self.channel_sets = random.sample(channel_sets_population, random.randint(1, 8))
            self.pattern_helper.user_mode = fixed_drive_state = 'RANDOM'
            self.pattern_helper.update_slice_channel_set_pattern_combo(slices=self.slices,
                                                                       channel_sets=self.channel_sets,
                                                                       fixed_drive_state=fixed_drive_state)
            self.pattern_helper.set_pin_fixed_drive_state_dma(psdb_target, dma_random=True)
            self.env.hbicc.check_pin_states(complete_set, fixed_drive_state)

    def init_pin_states(self):
        fixed_drive_state = 'LOW'
        self.pattern_helper.user_mode = fixed_drive_state
        self.pattern_helper.create_slice_channel_set_pattern_combo(fixed_drive_state=fixed_drive_state)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target=0)
        self.pattern_helper.set_pin_fixed_drive_state(psdb_target=1)

    def aurora_link_setup(self):
        aurora_link = Functional()
        aurora_link.rm_device = self.env.hbicc.ring_multiplier
        aurora_link.enable_aurora_bus_and_reset_power_state()

    def vref_set_pcie(self, psdb_target, value):
        self.pattern_helper.hbicc.ring_multiplier.set_vref(psdb_target, 0.9)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=0.9)

    def print_rm_all_channel_set_status(self, psdb):
        all_channel_set_dic = self.env.hbicc.check_rm_all_channel_set_pin_control()
        if psdb:
            channel_set_list = range(8, 16)
        else:
            channel_set_list = range(8)
        slice_list = range(5)
        for slice in slice_list:
            for channel_set in channel_set_list:
                self.Log('info', f's{slice}d{channel_set}ControlR: {all_channel_set_dic[channel_set][slice]}')

    def fds_tq_blank_without_sync_delay(self):
        start_command = self.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0, dma=True)
        end_command = self.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0, dma=True)
        return [start_command, end_command]

    def fds_tq_blank_with_one_sync_delay(self, time_sync):
        start_command = self.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0, dma=True)
        sync_delay_command = self.env.hbicc.ring_multiplier.sync_delay_within_trigger_queue_record(delay=time_sync)
        end_command = self.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0, dma=True)
        return [start_command, sync_delay_command, end_command]

    def fds_tq_blank_with_two_sync_delay(self, time_sync):
        start_command = self.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0, dma=True)
        sync_delay_command = self.env.hbicc.ring_multiplier.sync_delay_within_trigger_queue_record(delay=time_sync)
        end_command = self.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0, dma=True)
        return [start_command, sync_delay_command, sync_delay_command, end_command]

    def fds_tq_blank_with_two_sync_delay_with_num_delay(self, delay_count, delay, time_sync):
        start_command = self.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0, dma=True)
        delay_command = self.env.hbicc.ring_multiplier.delay_within_trigger_queue_record(psdb_target=0, dma=True,
                                                                                         delay=delay, plb_count=0)
        sync_delay_command = self.env.hbicc.ring_multiplier.sync_delay_within_trigger_queue_record(delay=time_sync)
        end_command = self.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0, dma=True)
        return [start_command, sync_delay_command] + [delay_command]*delay_count + [sync_delay_command, end_command]

    def fds_tq_without_sync_delay(self, channel_set_list, slice_list, pin_state):
        fds_commands_list = self.fds_commands_list(channel_set_list, slice_list, pin_state, 0)
        return self.fds_tq_add_start_end(channel_set_list, fds_commands_list)

    def fds_tq_with_one_sync_delay(self, channel_set_list, slice_list, pin_state_list, delay, time_sync):
        return self.fds_tq_with_sync_delay(channel_set_list, slice_list, pin_state_list, 1, delay, time_sync)

    def fds_tq_with_two_sync_delay(self, channel_set_list, slice_list, pin_state_list, delay, time_sync):
        return self.fds_tq_with_sync_delay(channel_set_list, slice_list, pin_state_list, 2, delay, time_sync)

    def fds_tq_with_sync_delay(self, channel_set_list, slice_list, pin_state_list, num_sync, delay, time_sync):
        fds_commands_list_first = self.fds_commands_list(channel_set_list, slice_list, pin_state_list[0], delay)
        fds_commands_list_second = self.fds_commands_list(channel_set_list, slice_list, pin_state_list[1], delay)
        sync_delay_command = self.env.hbicc.ring_multiplier.sync_delay_within_trigger_queue_record(delay=time_sync)
        fds_commands_list = self.sync_delay_command_insertion(num_sync, channel_set_list, fds_commands_list_first,
                                                              fds_commands_list_second, sync_delay_command)
        return self.fds_tq_add_start_end(channel_set_list, fds_commands_list)

    def fds_tq_add_start_end(self, channel_set_list, fds_commands_list):
        start_command = self.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0, dma=True)
        end_command = self.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0, dma=True)
        fds_commands_list_with_start_end = {}
        for channel_set in channel_set_list:
            fds_commands_with_start_end = [start_command] + fds_commands_list.get(channel_set) + [end_command]
            fds_commands_list_with_start_end.update({channel_set: fds_commands_with_start_end})
        return fds_commands_list_with_start_end

    def sync_delay_command_insertion(self, num_sync, channel_set_list, fds_first, fds_second, sync_delay):
        fds_commands_list_with_sync_delay = {}
        for channel_set in channel_set_list:
            fds_commands_with_sync_delay = fds_first.get(channel_set)
            fds_commands_second = fds_second.get(channel_set)
            for i in range(num_sync):
                fds_commands_with_sync_delay = fds_commands_with_sync_delay + [sync_delay]
            fds_commands_with_sync_delay = fds_commands_with_sync_delay + fds_commands_second
            fds_commands_list_with_sync_delay.update({channel_set: fds_commands_with_sync_delay})
        return fds_commands_list_with_sync_delay

    def fds_commands_list(self, channel_set_list, slice_list, pin_state, delay):
        self.pattern_helper.update_slice_channel_set_pattern_combo(slices=slice_list, channel_sets=channel_set_list,
                                                                   fixed_drive_state=pin_state)
        fds_commands_list = self.pattern_helper.get_fds_commands_channel_set_list()
        if delay:
            fds_commands_list = self.delay_command_insertion_at_end(channel_set_list, fds_commands_list, delay)
        return fds_commands_list

    def delay_command_insertion_at_end(self, channel_set_list, fds_commands_list, delay):
        delay_command = self.env.hbicc.ring_multiplier.delay_within_trigger_queue_record(psdb_target=0, dma=True,
                                                                                         delay=delay, plb_count=0)
        fds_commands_list_with_delay = {}
        for channel_set in channel_set_list:
            fds_commands_with_delay = fds_commands_list.get(channel_set) + [delay_command]
            fds_commands_list_with_delay.update({channel_set: fds_commands_with_delay})
        return fds_commands_list_with_delay


class SyncDelayDCTQ(HbiccTest):
    """DCTQ Tests of SyncDelay Feature to Execute Trigger Queue in Parallel

       Notes:
       1.SyncDelay command is to create a synchronize point for different dut_id tq content
       2.It includes 2 backward compatible tests; 5 basic function tests; 2 timing tests; 2 lockup related tests
       3.There are 17 TQ engines and each has a process counter.
       4.IRM is FDS TQ; BIRM is the Vccf & Vccio TQ
    """

    def setUp(self, tester=None):
        super().setUp()
        self.irm_tq = PinFixedDriveState()
        self.birm_tq = Functional()
        self.pattern_helper = PatternHelper(self.env)
        self.irm_birm_setting()

        self.aurora_link_setup()
        self.pattern_helper.initialize_fds()
        self.vref_set(0.9)
        self.switch_to_dctq_sync_delay_mode()

        self.tq_proccessed_count_list = [0]*17
        self.broadcast_dut_id = 63
        self.vref_pre = 0.5

        self.set_random_attributes()

    def irm_birm_setting(self):
        self.irm_tq.env = self.env
        self.irm_tq.pattern_helper = self.pattern_helper
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=list(range(5)), channel_sets=list(range(16)),
                                                                   fixed_drive_state='LOW')
        self.complete_set = list(self.pattern_helper.slice_channel_sets_combo.values())
        self.birm_tq.env = self.env
        self.birm_tq.rm_device = self.env.hbicc.ring_multiplier

    def aurora_link_setup(self):
        aurora_link = Functional()
        aurora_link.rm_device = self.env.hbicc.ring_multiplier
        aurora_link.enable_aurora_bus_and_reset_power_state()

    def vref_set(self, vref):
        self.pattern_helper.hbicc.ring_multiplier.set_vref(0, vref)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_0_pin_0, expected=vref)
        self.pattern_helper.hbicc.ring_multiplier.set_vref(1, vref)
        self.pattern_helper.check_vref_voltage(self.env.hbicc.psdb_1_pin_0, expected=vref)

    def switch_to_dctq_sync_delay_mode(self):
        self.env.hbicc.ring_multiplier.switch_dctq_sync_delay_mode(syncdelay_mode=1)

    def set_random_attributes(self):
        self.random_dut_id = random.randrange(0, 63)
        self.random_index = random.randrange(0, 2 ** 12)
        self.single_channel_set = [random.randrange(0, 16)]
        self.active_duts_prepare = list(range(16)) + [63]
        self.active_duts_final = list(range(0, 64))
        self.vref_post = random.choice([0.7, 0.8, 0.9])
        self.vccio = random.choice([1.7, 1.8, 1.9])
        
        population = set(range(0, 16))
        self.multi_channel_sets = random.sample(population, random.randint(1, 16))
        self.multi_channel_sets.sort()
        
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))
        self.slices.sort()

    def DirectedDPSFoldTriggerWithoutSyncDelayTest(self):
        """Verify Execution of Single DPS Fold TQ without SyncDelay

        **Test:**
        1. Create and store a trigger queue to set random pin state value
           in random index memory location
        2. Create DPS fold trigger and send that from RCTC
        3. Check trigger queue process counter should increase by 1
        4. Check the correspond dut_id is powered off
        5. Check the FDS pin state value from PM register

        **Criteria:** Trigger queue process count should match;
        FDS pin state value should match

        **NOTE:**
        """
        irm_commands = self.single_tq_without_sync_delay()
        dps_fold_index = random.randrange(0, 2 ** 12)
        self.random_dut_id = random.randrange(0, 16)
        self.set_fold_trigger_index(dps_fold_index)
        self.store_tq_content_non_broadcast(dps_fold_index, self.random_dut_id, irm_commands)
        self.read_tq_engines_process_count_sum()
        self.send_dps_fold_trigger_from_rctc_to_rm(self.random_dut_id)
        self.trigger_completion_handshake(1)
        self.check_fds_pin_state('RANDOM')
        self.check_dut_power_off_state(self.random_dut_id)

    def set_fold_trigger_index(self, index):
        self.env.hbicc.ring_multiplier.write_dps_fold_trigger_index(index)

    def DirectedSingleIRMwithoutSyncDelayTest(self):
        """Verify Execution of Single FDS TQ without SyncDelay

        **Test:**
        1. Create and execute a trigger queue to set random pin state value in one channel set
        2. Check trigger queue process counter should increase by 1
        3. Check the FDS pin state value from PM register

        **Criteria:** Trigger queue process count should match; FDS pin state value should match

        **NOTE:**
        """
        irm_commands = self.single_tq_without_sync_delay()
        self.store_tq_content_non_broadcast(self.random_index, self.random_dut_id, irm_commands)
        self.read_tq_engines_process_count_sum()
        self.send_rm_trigger_from_rctc(self.random_dut_id, self.random_index)
        self.trigger_completion_handshake(1)
        self.check_fds_pin_state('RANDOM')

    def DirectedBroadcastwithoutSyncDelayTest(self):
        """Verify Execution of Broadcast FDS and Vccio Vref TQ without SyncDelay

        **Test:**
        1. Create and execute a broacast trigger queue to set random pin state value in multiple channel sets \
           and to set Vccio and Vref
        2. Check trigger queue process counter should increase by 17
        3. Check the FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        delay = 10
        self.create_broadcast_tq_list_without_sync_delay(delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.active_duts_final = list(range(64))
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def DirectedSingleIRMwithSyncDelayTest(self):
        """Verify Execution of Single FDS TQ with SyncDelay

        **Test:**
        1. Create and execute a trigger queue to set random pin state value in one channel set
        2. Check trigger queue process counter should increase by 1
        3. Check the FDS pin state value from PM register

        **Criteria:** Trigger queue process count should match; FDS pin state value should match

        **NOTE:**
        """
        delay = 0
        sync_delay = 10
        irm_commands = self.single_tq_with_one_sync_delay(delay, sync_delay)
        self.store_tq_content_non_broadcast(self.random_index, self.random_dut_id, irm_commands)
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.random_dut_id, self.random_index)
        self.active_duts_final = list({self.random_dut_id} & set(self.active_duts_final))
        self.check_tq_engine_individual_sum_count()
        self.check_fds_pin_state('RANDOM')

    @expected_failure('negative test; need to fix some code to remove error log')
    def DirectedSingleIRMwithSyncDelaywithDutPowerOffTest(self):
        """Verify NO Execution of Single FDS TQ with SyncDelay will All Duts Power State Off

        **Test:**
        1. Create and execute a trigger queue to set random pin state value in one channel set
        2. Check trigger queue process counter should increase by 1
        3. Check the FDS pin state value from PM register

        **Criteria:** This test is designed to fail

        **NOTE:**
        """
        self.active_duts_prepare = []
        self.setup_power_dut_list()
        self.DirectedSingleIRMwithSyncDelayTest()

    def DirectedBroadcastwithSyncDelayTest(self):
        """Verify Execution of Broadcast FDS and Vccio Vref TQ with SyncDelay

        **Test:**
        1. Create and execute a broadcast trigger queue to set random pin state value in multiple channel sets \
           and to set Vccio and Vref
        2. Check trigger queue process counter should increase by 17
        3. Check the FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 10
        sync_delay = 10
        self.create_broadcast_tqlist_with_two_sync_delay(irm_delay, birm_delay, sync_delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def DirectedBroadcastwithDelayUnbalancedTest(self):
        """Verify Execution of Broadcast FDS and Vccio Vref TQ with SyncDelay and Unbalanced Commands Count

        **Test:**
        1. Create and execute a broadcast trigger queue to set random pin state value in multiple channel sets \
           and to set Vccio and Vref
        2. Check trigger queue process counter should increase by 17
        3. Check the FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 10
        sync_delay = 10
        self.create_broadcast_tqlist_with_two_sync_delay_unbalanced(irm_delay, birm_delay, sync_delay, sync_delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def DirectedBroadcastwithSyncDelayUnbalancedTest(self):
        """Verify Execution of Broadcast FDS and Vccio Vref TQ with Unbalanced SyncDelay Timing

        **Test:**
        1. Create and execute a broadcast trigger queue to set random pin state value in multiple channel sets \
           and to set Vccio and Vref
        2. Check trigger queue process counter should increase by 17
        3. Check the FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 10
        sync_delay = 10
        dummy_sync_delay = 3000000
        self.create_broadcast_tqlist_with_two_sync_delay_unbalanced(irm_delay, birm_delay, sync_delay,
                                                                    dummy_sync_delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def DirectedBroadcastwithSyncDelayTimingwithoutBirmTest(self):
        """Verify Execution of Broadcast FDS and Vccio Vref TQ with SyncDelay Timing Check in IRM

        **Test:**
        1. Create and execute a broadcast trigger queue to set random pin state value in multiple channel sets \
           and to set Vccio and Vref. Here are 1.5s SyncDelay Commands in dut_id0 while 0s SyncDelay in dut_id1
        2. Check the channel set 0/1 pin state value at 1s and 3s after the trigger sending
        3. Check trigger queue process counter should increase by 17
        4. Check the final FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match;

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 10
        sync_delay_dut_id0 = 1500000
        sync_delay_dut_id1 = 0
        sync_delay_other_dut_id = 10
        self.slices = [0, 1, 2, 3, 4]
        self.create_broadcast_tqlist_without_birm_timing(irm_delay, birm_delay, sync_delay_other_dut_id
                                                         , sync_delay_dut_id0, sync_delay_dut_id1)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.wait_while_syncdelay_exectuion(seconds=2)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[0], expected_pin_state=0)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[1], expected_pin_state=1)
        self.wait_while_syncdelay_exectuion(seconds=2)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[0], expected_pin_state=1)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[1], expected_pin_state=1)
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def DirectedBroadcastwithSyncDelayTimingwithBirmTest(self):
        """Verify Execution of Broadcast FDS and Vccio Vref TQ with SyncDelay Timing Check in IRM & BIRM

        **Test:**
        1. Create and execute a broadcast trigger queue to set random pin state value in multiple channel sets \
           and to set Vccio and Vref. Here are 0s SyncDelay Commands in dut_id0, dut_id1 and 3s SyncDelay in dut_id63
        2. Check the channel set 0/1 pin state value and Vccio Vref value at 1s and 3s after the trigger sending
        3. Check trigger queue process counter should increase by 17
        4. Check the final FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 3000000
        sync_delay = 0
        self.vref_pre = 0.5
        self.vref_post = 0.7
        self.slices = [0, 1, 2, 3, 4]
        self.create_broadcast_tqlist_with_birm_timing(irm_delay, birm_delay, sync_delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.wait_while_syncdelay_exectuion(seconds=1)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[0], expected_pin_state=0)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[1], expected_pin_state=1)
        self.check_vref_vccio(self.vref_pre, self.vccio)
        self.wait_while_syncdelay_exectuion(seconds=3)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[0], expected_pin_state=1)
        self.check_all_slices_rm_fds_control_register_value(channel_set_list=[1], expected_pin_state=0)
        self.check_vref_vccio(self.vref_post, self.vccio)
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def DirectedBroadcastwithSyncDelayMismatchLockupTest(self):
        """Verify DCTQ Lock Up after Illegal TQ Commands and Use DCTQ Reset to Recover

        **Test:**
        1. Create and execute an illegal broadcast trigger gueue with SyncDelay command count mismatch
        2. Check the DCTQ counter doesn't increase correctly
        3. Send a DCTQ reset
        4. Run a happy path broadcast trigger queue test
        5. Check test after the reset pass with process count and FDS Vccio Vref value match

        **Criteria:** Final trigger queue process count, FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 10
        sync_delay = 10
        self.create_broadcast_tqlist_with_sync_delay_mismatch(irm_delay, birm_delay, sync_delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.check_tq_engine_list_process_count(self.active_duts_final, ignore_error=True)
        self.reset_dctq_engines()
        self.check_happypath_sync_delay_test()

    def DirectedBroadcastwithSyncDelayOverflowNoLockupTest(self):
        """Verify No Lock UP with 1000 Commands between Two SyncDelay Commands

        **Test:**
        1. Create and execute a broadcast trigger queue with 1000 commands between two SyncDelay commands,
           which will cause FIFO overflow in RM but no lock up should happen
        2. Check trigger queue process counter should increase by 17
        3. Check the FDS pin state value from PM register and Vccio Vref from Vmon

        **Criteria:** Trigger queue process count should match; FDS pin state value and Vccio Vref value should match

        **NOTE:**
        """
        self.set_random_attributes()
        self.setup_power_dut_list()
        irm_delay = 0
        birm_delay = 10
        sync_delay = 0
        dummy_sync_delay = 1000000
        self.create_broadcast_tqlist_with_sync_delay_overlfow(irm_delay, birm_delay, sync_delay, dummy_sync_delay)
        self.store_tq_content_broadcast()
        self.read_tq_engine_individual_sum_count()
        self.send_rm_trigger_from_rctc(self.broadcast_dut_id, self.random_index)
        self.check_tq_engine_individual_sum_count()
        self.check_fds_vref_vccio()

    def wait_while_syncdelay_exectuion(self, seconds):
        time.sleep(seconds)

    def check_happypath_sync_delay_test(self):
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=list(range(5)), channel_sets=list(range(16)),
                                                                   fixed_drive_state='LOW')
        self.complete_set = list(self.pattern_helper.slice_channel_sets_combo.values())
        self.pattern_helper.initialize_fds()
        self.DirectedBroadcastwithSyncDelayTest()

    def read_tq_engine_individual_sum_count(self):
        self.read_tq_engine_list_process_count()
        self.read_tq_engines_process_count_sum()

    def check_tq_engine_individual_sum_count(self):
        self.trigger_completion_handshake(len(self.active_duts_final))
        self.check_tq_engine_list_process_count(self.active_duts_final)

    def check_fds_vref_vccio(self):
        self.check_fds_pin_state('RANDOM')
        time.sleep(0.2)
        self.check_vref_vccio(self.vref_post, self.vccio)

    def single_tq_without_sync_delay(self):
        return list(self.irm_tq.fds_tq_without_sync_delay(self.single_channel_set, self.slices, 'RANDOM').values())[0]

    def single_tq_with_one_sync_delay(self, delay, sync_delay):
        return list(self.irm_tq.fds_tq_with_one_sync_delay(self.single_channel_set, self.slices,
                                                           ['LOW', 'RANDOM'], delay, sync_delay).values())[0]

    def create_broadcast_tq_list_without_sync_delay(self, delay):
        irm_commands_list = self.irm_tq.fds_tq_without_sync_delay(self.multi_channel_sets, self.slices, 'RANDOM')
        blank_commands = self.irm_tq.fds_tq_blank_without_sync_delay()
        birm_commands = self.birm_tq.vccf_vccio_tq_without_sync_delay(self.vref_pre, self.vref_post, self.vccio, delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def create_broadcast_tqlist_with_two_sync_delay(self, irm_delay, birm_delay, sync_delay):
        irm_commands_list = self.irm_tq.fds_tq_with_two_sync_delay(self.multi_channel_sets, self.slices,
                                                                   ['LOW', 'RANDOM'], irm_delay, sync_delay)
        blank_commands = self.irm_tq.fds_tq_blank_with_two_sync_delay(sync_delay)
        birm_commands = self.birm_tq.vccf_vccio_tq_with_two_sync_delay(self.vref_pre, self.vref_post, self.vccio,
                                                                       birm_delay, sync_delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def create_broadcast_tqlist_with_two_sync_delay_unbalanced(self, irm_delay, birm_delay, sync_delay, dummy_sync_delay):
        delay_count = 450
        irm_commands_list = self.irm_tq.fds_tq_with_two_sync_delay(self.multi_channel_sets, self.slices,
                                                                   ['LOW', 'RANDOM'], irm_delay, sync_delay)
        blank_commands = self.irm_tq.fds_tq_blank_with_two_sync_delay_with_num_delay(delay_count,
                                                                                     irm_delay, dummy_sync_delay)
        birm_commands = self.birm_tq.vccf_vccio_tq_with_two_sync_delay(self.vref_pre, self.vref_post, self.vccio,
                                                                       birm_delay, sync_delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def create_broadcast_tqlist_with_birm_timing(self, irm_delay, birm_delay, sync_delay):
        irm_commands_dut_id0 = self.irm_tq.fds_tq_with_one_sync_delay([0], self.slices, ['LOW', 'HIGH'], irm_delay,
                                                                      sync_delay)
        irm_commands_dut_id1 = self.irm_tq.fds_tq_with_one_sync_delay([1], self.slices, ['HIGH', 'LOW'], irm_delay,
                                                                      sync_delay)
        irm_commands_list = {**irm_commands_dut_id0, **irm_commands_dut_id1}
        blank_commands = self.irm_tq.fds_tq_blank_with_one_sync_delay(sync_delay)
        birm_commands = self.birm_tq.vccf_vccio_tq_with_one_sync_delay(self.vref_pre, self.vref_post, self.vccio,
                                                                       birm_delay, sync_delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def create_broadcast_tqlist_without_birm_timing(self, irm_delay, birm_delay, sync_delay, sync_delay_dut_id0,
                                                    sync_delay_dut_id1):
        irm_commands_dut_id0 = self.irm_tq.fds_tq_with_two_sync_delay([0], self.slices, ['LOW', 'HIGH'], irm_delay,
                                                                      sync_delay_dut_id0)
        irm_commands_dut_id1 = self.irm_tq.fds_tq_with_two_sync_delay([1], self.slices, ['LOW', 'HIGH'], irm_delay,
                                                                      sync_delay_dut_id1)
        irm_commands_list = {**irm_commands_dut_id0, **irm_commands_dut_id1}
        blank_commands = self.irm_tq.fds_tq_blank_with_two_sync_delay(sync_delay)
        birm_commands = self.birm_tq.vccf_vccio_tq_with_two_sync_delay(self.vref_pre, self.vref_post, self.vccio,
                                                                       birm_delay, sync_delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def create_broadcast_tqlist_with_sync_delay_mismatch(self, irm_delay, birm_delay, sync_delay):
        irm_commands_list = self.irm_tq.fds_tq_with_two_sync_delay(self.multi_channel_sets, self.slices,
                                                                   ['LOW', 'RANDOM'], irm_delay, sync_delay)
        blank_commands = self.irm_tq.fds_tq_blank_without_sync_delay()
        birm_commands = self.birm_tq.vccf_vccio_tq_with_two_sync_delay(self.vref_pre, self.vref_post, self.vccio,
                                                                       birm_delay, sync_delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def create_broadcast_tqlist_with_sync_delay_overlfow(self, irm_delay, birm_delay, sync_delay, dummy_sync_delay):
        delay_count = 1000
        irm_commands_list = self.irm_tq.fds_tq_with_two_sync_delay(self.multi_channel_sets, self.slices,
                                                                   ['LOW', 'RANDOM'], irm_delay, sync_delay)
        blank_commands = self.irm_tq.fds_tq_blank_with_two_sync_delay_with_num_delay(delay_count,
                                                                                     irm_delay, dummy_sync_delay)
        birm_commands = self.birm_tq.vccf_vccio_tq_with_two_sync_delay(self.vref_pre, self.vref_post, self.vccio,
                                                                       birm_delay, sync_delay)
        self.duts_tq_commands_list = self.dut0_63_tq_commands_list(irm_commands_list, birm_commands, blank_commands)

    def store_tq_content_non_broadcast(self, index, dut_id, tq_commands):
        self.env.hbicc.ring_multiplier.store_one_dut_tq_dma_non_broadcast(index, dut_id, tq_commands)

    def store_tq_content_broadcast(self):
        index = self.random_index
        duts_tq_commands_list = self.duts_tq_commands_list
        self.env.hbicc.ring_multiplier.store_duts_tq_dma_broadcast(index, duts_tq_commands_list)

    def read_tq_engines_process_count_sum(self):
        self.tq_processed_counter_pre = self.env.hbicc.ring_multiplier.read_dc_trigger_page_processed_queues()

    def send_rm_trigger_from_rctc(self, dut_id, index):
        self.pattern_helper.send_trigger_from_rctc(dut_id, index)

    def trigger_completion_handshake(self, number_of_duts):
        self.pattern_helper.trigger_completion_handshake_with_time_report(number_of_duts, self.tq_processed_counter_pre)

    def dut0_63_tq_commands_list(self, irm_commands_list, birm_commands, blank_commands):
        duts_tq_commands_list = []
        for dut in range(64):
            if dut in irm_commands_list.keys():
                duts_tq_commands_list.append(irm_commands_list.get(dut))
            elif dut == 63:
                duts_tq_commands_list.append(birm_commands)
            else:
                duts_tq_commands_list.append(blank_commands)
        return duts_tq_commands_list

    def check_tq_engine_list_process_count(self, active_duts, ignore_error=False):
        tq_proccessed_count_list_before_trigger = self.tq_proccessed_count_list
        tq_proccessed_count_list_after_trigger = self.env.hbicc.ring_multiplier.read_dctq_engines_processed_count_list()
        read_proccessed_delta = {}
        expect_proccessed_delta = {}
        count_mismatch = False
        for i in range(TQ_ENGINE_COUNT):
            read_delta = tq_proccessed_count_list_after_trigger[i] - tq_proccessed_count_list_before_trigger[i]
            expect_delta = self.expect_tq_engine_count(i, active_duts)
            read_proccessed_delta.update({i: read_delta})
            expect_proccessed_delta.update({i: expect_delta})
            if read_delta != expect_delta:
                count_mismatch = True
        self.Log('debug', f'active_duts: {active_duts}')
        self.tq_engine_count_log(count_mismatch, expect_proccessed_delta, read_proccessed_delta, ignore_error)

    def expect_tq_engine_count(self, tq_engine_number, active_dut_lsit):
        expect_count = 0
        if tq_engine_number == 16:
            for i in range(16, 64):
                if i in active_dut_lsit:
                    expect_count += 1
        elif tq_engine_number in active_dut_lsit:
            expect_count += 1
        return expect_count

    def tq_engine_count_log(self, count_mismatch, expect_proccessed_delta, read_proccessed_delta, ignore_error):
        if count_mismatch & (not ignore_error):
            self.Log('error', 'tq_engine_counter_mismatch')
            self.Log('error', f'r_count{read_proccessed_delta}')
            self.Log('error', f'e_count{expect_proccessed_delta}')
        elif count_mismatch:
            self.Log('debug', 'tq_engine_counter_mismatch')
            self.Log('debug', f'r_count{read_proccessed_delta}')
            self.Log('debug', f'e_count{expect_proccessed_delta}')
        else:
            self.Log('debug', 'tq_engine_counter_match')
            self.Log('debug', f'r_count{read_proccessed_delta}')
            self.Log('debug', f'e_count{expect_proccessed_delta}')

    def check_fds_pin_state(self, pin_state):
        self.env.hbicc.check_pin_states(self.complete_set, pin_state)

    def check_all_slices_rm_fds_control_register_value(self, channel_set_list, expected_pin_state):
        slice_list = self.slices
        self.check_rm_fds_control_register_value(channel_set_list, slice_list, expected_pin_state)

    def check_rm_fds_control_register_value(self, channel_set_list, slice_list, expected_pin_state):
        all_channel_set_dic = self.env.hbicc.check_rm_all_channel_set_pin_control()
        for slice in slice_list:
            for channel_set in channel_set_list:
                failure_one_slice_channel_set = False
                for read_pin_state in all_channel_set_dic[channel_set][slice].values():
                    if read_pin_state != expected_pin_state:
                        failure_one_slice_channel_set = True
                if failure_one_slice_channel_set:
                    self.Log('error', f'EXPECT_PIN_STATE: {expected_pin_state}')
                    self.Log('error',
                             f'S{slice}CS{channel_set} Control READ:{all_channel_set_dic[channel_set][slice]}')

    def check_vref_vccio(self, expect_vref, expect_vccio):
        read_vref, read_vccio = self.read_vref_vccio_through_vmon()
        voltage_mismatch = False
        voltage_margin = 0.05
        for vref in read_vref:
            if (vref > expect_vref + voltage_margin) | (vref < expect_vref - voltage_margin):
                voltage_mismatch = True
        for vccio in read_vccio:
            if (vccio > expect_vccio + voltage_margin) | (vccio < expect_vccio - voltage_margin):
                voltage_mismatch = True
        self.vref_vccio_log(expect_vccio, expect_vref, read_vccio, read_vref, voltage_mismatch)

    def vref_vccio_log(self, expect_vccio, expect_vref, read_vccio, read_vref, voltage_mismatch):
        if voltage_mismatch:
            self.Log('error', 'vref_vccio_mismatch')
            self.Log('error', f'r_vref: {read_vref}; e_vref {expect_vref}')
            self.Log('error', f'r_vccio: {read_vccio}; e_vccio {expect_vccio}')
        else:
            self.Log('debug', 'vref_vccio_match')
            self.Log('debug', f'r_vref: {read_vref}; e_vref {expect_vref}')
            self.Log('debug', f'r_vccio: {read_vccio}; e_vccio {expect_vccio}')

    def read_vref_vccio_through_vmon(self):
        read_all_vmon = self.env.hbicc.psdb_0_pin_0.read_all_vmon()
        read_vref = read_all_vmon[1:5]
        read_vccio = read_all_vmon[8:9]
        return read_vref, read_vccio

    def setup_power_dut_list(self):
        self.active_duts_final = self.active_duts_prepare
        dut_power_state = self.env.hbicc.ring_multiplier.read_dc_trigger_dut_power_state()
        self.Log('debug', f'{dut_power_state:016x}')
        power_state = self.generate_power_dut_list()
        mask = 0
        self.env.hbicc.ring_multiplier.write_dc_trigger_dut_power_state(power_state, mask)
        time.sleep(0.1)
        dut_power_state = self.env.hbicc.ring_multiplier.read_dc_trigger_dut_power_state()
        self.Log('debug', f'{dut_power_state:016x}')

    def generate_power_dut_list(self):
        power_state = 0
        for i in range(NUMBER_OF_DUTS):
            if i not in self.active_duts_final:
                power_state += (1 << i)
        return power_state

    def read_tq_engine_list_process_count(self):
        self.tq_proccessed_count_list = self.env.hbicc.ring_multiplier.read_dctq_engines_processed_count_list()

    def reset_dctq_engines(self):
        self.env.hbicc.ring_multiplier.reset_dctq_engines_bit()

    def send_dps_fold_trigger_from_rctc_to_rm(self, dut_id):
        rctc = self.env.hbicc.rc
        trigger_rm_link_index = 17
        trigger_data = self.create_dps_fold_trigger(dut_id)
        rctc.send_trigger(trigger_data, trigger_rm_link_index)

    def create_dps_fold_trigger(self, dut_id):
        trigger_operation_3_0 = 0x2
        trigger_reserved_14_4 = 0
        trigger_ctrl_trig_15 = 0x1
        trigger_reserved_19_16 = 0
        trigger_dut_id_25_20 = dut_id
        trigger_type_31_26 = 0x01
        trigger_data = (trigger_type_31_26 << 26) | \
                       (trigger_dut_id_25_20 << 20) | \
                       (trigger_reserved_19_16 << 16) | \
                       (trigger_ctrl_trig_15 << 15) | \
                       (trigger_reserved_14_4 << 4) | \
                       (trigger_operation_3_0)
        return trigger_data

    def check_dut_power_off_state(self, dut_id):
        dut_power_state = self.env.hbicc.ring_multiplier.read_dc_trigger_dut_power_state()
        power_state = (dut_power_state >> dut_id) & 0b1
        if power_state == 0:
            self.Log('error', 'power_state_mismatch')

