# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Features that allow a reduction in memory utilization.

Only local repeat and instruction repeat are implemented.

The HLD describes a compressed block feature, but it not yet available.
"""

import random
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper


pattern_start = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data=0b0 #ctp mask
                PatternId 0xFADE
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xACED
                NoCompareDriveZVectors length=10

                PATTERN1:
                DriveZeroVectors length=10
                '''

pattern_end = f'''
                DriveZeroVectors length=10

                Return                
                '''


class Mix(HbiccTest):
    """Instruction and local repeats combined."""

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.getrandbits(35)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))

    def RandomBranchCompressedVectorTest(self):
        """Randomize branches and repeats"""
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = pattern_start
        list_of_labels=['LABEL1', 'LABEL2', 'LABEL3', 'LABEL4', 'LABEL5', 'LABEL6', 'LABEL7',
                        'LABEL8', 'LABEL9', 'LABEL10']
        label_selection=random.sample(list_of_labels, 5)
        for label in label_selection:
            pattern_string += self.create_branch_segment(label)
        for label in list_of_labels:
            pattern_string += self.create_loop_segment(label)
        pattern_string += pattern_end
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def RandomCompressionCCWRTest(self):
        """Randomly mix instruction repeat and local repeat"""
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = pattern_start
        for i in range(100):
            choose_instruction_repeat = random.random() > 0.95
            if(choose_instruction_repeat):
                rpt_immediate=random.randint(10,1000)
                pattern_string += f'''
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}
                V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL\n'''
            else:
                lrpt = random.randint(1, 31)
                pattern_string += f'''V ctv=0, mtv=0, lrpt={lrpt}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL\n'''
        pattern_string += pattern_end
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedLocalAndInstructionRepeatTest(self):
        """Apply instruction repeat and local repeat to the same vector"""
        self.pattern_helper.user_mode = 'LOW'
        rpt_immediate = random.randint(32, 1000)
        lrpt = random.getrandbits(5)
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}
                V ctv=0, mtv=0, lrpt={lrpt}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                DriveZeroVectors length=10

                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def create_loop_segment(self, label):
        self.create_loop_option_table()
        loop_choice = random.randint(0,2)
        pattern_substr = self.loop_option_table[loop_choice](label)
        return pattern_substr

    def create_loop_option_table(self):
        self.loop_option_table = {0b00: self.create_instruction_loop,
                                    0b01: self.create_lrpt_loop,
                                    0b10: self.create_instruction_and_lrpt_loop}

    def create_instruction_loop(self,label):
        rpt= random.randint(10,1000)
        pattern_substr = f'''{label}:
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt}
                V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                Return\n'''
        return pattern_substr

    def create_lrpt_loop(self,label):
        rpt= random.randint(1,31)
        pattern_substr = f'''{label}:
                V ctv=0, mtv=0, lrpt={rpt}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                Return\n'''
        return pattern_substr

    def create_instruction_and_lrpt_loop(self,label):
        rpt = random.randint(10,100)
        lrpt = random.randint(1,8)
        pattern_substr = f'''{label}:
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt}
                V ctv=0, mtv=0, lrpt={lrpt}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                Return\n'''
        return pattern_substr

    def create_branch_segment(self, label):
        self.create_branch_option_table()
        random_branch = random.randint(0,1)
        pattern_substr = self.branch_option_table[random_branch](label)
        return pattern_substr

    def create_branch_option_table(self):
        self.branch_option_table = {0b00: self.create_call_branch,
                                    0b01: self.create_pcall_branch}

    def create_pcall_branch(self, label):
        pattern_substr = f'''PCall {label}
                             NoCompareDriveZVectors length=1
                          '''
        return pattern_substr

    def create_call_branch(self, label):
        pattern_substr = f'''Call {label}
                             NoCompareDriveZVectors length=1
                          '''
        return pattern_substr

    def create_reg_call_branch(self, label):
        pattern_substr = f'''SetRegister 10, {label}
                             NoCompareDriveZVectors length=1
                             I optype=BRANCH, base=base, br=CALL_R, dest=10
                             NoCompareDriveZVectors length=1
                          '''
        return pattern_substr

    def create_reg_pcall_branch(self, label):
        pattern_substr = f'''SetRegister 10, {label}
                             NoCompareDriveZVectors length=1
                             I optype=BRANCH, base=base, br=PCALL_R, dest=10
                             NoCompareDriveZVectors length=1
                          '''
        return pattern_substr


class Instruction(HbiccTest):
    """Repeat by instruction allows for large repeat values.

    The repeat instruction specifies how many copies of the
    following vector to create.
    """

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.getrandbits(35)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))

    def DirectedImmediateTest(self):
        """Instruction repeat with count from immediate field"""
        self.pattern_helper.user_mode = 'LOW'
        rpt_immediate = random.randint(32, 32000)
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}
                V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                DriveZeroVectors length=10

                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRegisterTest(self):
        """Instruction repeat with count from register"""
        self.pattern_helper.user_mode = 'LOW'
        rpt_list = random.choices(range(10, 320), k=32)
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                    DriveZeroVectors length=10'''
        index = 0
        for rpt_value in rpt_list:
            pattern_string += f'''
                    I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, regA={index}, dest={index}, imm={rpt_value}
                    DriveZeroVectors length=10
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_RA, regA={index}
                    V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                    '''
            index += 1
        pattern_string += f'''
                    DriveZeroVectors length=1000
                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedRepeat64Test(self):
        """Repeat 64"""
        self.pattern_helper.user_mode = 'LOW'
        rpt_immediate = 64
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                    NoCompareDriveZVectors length=500
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}
                    V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                    NoCompareDriveZVectors length=500
                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()


    def DirectedRepeatLoopTest(self):
        """Repeat the first vector in a loop body"""
        self.pattern_helper.user_mode = 'LOW'
        rpt_immediate = random.randint(32, 1000)
        rpt_loop = random.randint(1, 50)
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                    DriveZeroVectors length=10
                    %repeat {rpt_loop}
                        I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}
                        V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                        DriveZeroVectors length=10
                    %end
                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def RandomInstructionRPTTest(self):
        """Randomize placement and count"""
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = pattern_start
        for i in range(random.randint(1000, 2000)):
            insert_instruction_repeat = random.random() > 0.95
            rpt_immediate = random.randint(100,1000)
            if insert_instruction_repeat:
                pattern_string +=f'''I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}\n'''
            pattern_string += f'''V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL\n'''
        pattern_string += pattern_end
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def DirectedLargeRepeatTest(self):
        """Repeat a vector 10,000 times"""
        self.pattern_helper.user_mode = 'LOW'
        rpt_immediate = 10000
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                    DriveZeroVectors length=10
                    I optype=VECTOR, vptype=VPLOCAL, vpop=REPEAT_I, imm={rpt_immediate}
                    V ctv=0, mtv=0, lrpt=0, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                    DriveZeroVectors length=10
                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def compare_cycle_to_expected(self, expected):
        register_type = self.env.hbicc.psdb_0_pin_0.registers.VECTOR_ASSEMBLER_LAST_CYCLE_COUNTER
        for pm in self.env.hbicc.get_pin_multipliers():
            for slice in self.slices:
                reg = pm.read_slice_register(register_type, slice=slice)
                if reg.value != expected:
                    self.Log('error', f'{pm.name()}, slice {slice}: {reg.value} not equal to {expected}')


class Local(HbiccTest):
    """Local repeats are specified in a vector field, limited to 31.

    The local repeat field specifies how many extra copies of the vector
    to create.
    """

    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = random.getrandbits(35)
        population = set(range(0, 5))
        self.slices = random.sample(population, random.randint(1, 5))

    def DirectedLocalRepeatTest(self):
        """Repeat=1 for 1000 vectors"""
        self.pattern_helper.user_mode = 'LOW'
        lrpt = 1
        pattern_id_0 = random.randint(0, 100)
        pattern_string = f'''

                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask
                PatternId {pattern_id_0}
                PCall PATTERN1
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={hex(random.randint(0, 0xFFFFFFFF))}
                NoCompareDriveZVectors length=10

                PATTERN1:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                DriveZeroVectors length=10
                %repeat 1000
                V ctv=0, mtv=0, lrpt={lrpt}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL
                %end
                DriveZeroVectors length=10

                Return                

                '''
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def RandomDenseRepeatsTest(self):
        """Repeat = [0-31] for 1000 vectors"""
        self.pattern_helper.user_mode = 'LOW'
        pattern_id_0 = random.randint(0, 100)
        pattern_string = pattern_start
        for i in range(1000):
            pattern_string += f'''V ctv=0, mtv=0, lrpt={random.randint(0, 31)}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL\n'''
        pattern_string += pattern_end
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()

    def RandomSparseRepeatsTest(self):
        """Repeat = [0-31] for 1000-2000 vectors

        local repeat = 0 occurs at a higher frequency
        """
        self.pattern_helper.user_mode = 'LOW'
        pattern_string = pattern_start
        for i in range(random.randint(1000, 2000)):
            nonzero = random.random() > 0.95
            lrpt = nonzero * random.randint(1, 31)
            pattern_string += f'''V ctv=0, mtv=0, lrpt={lrpt}, data=0vLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHLHL\n'''
        pattern_string += pattern_end
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.execute_pattern_scenario()
