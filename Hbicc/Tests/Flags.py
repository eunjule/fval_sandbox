# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
import time

from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.trigger_utility import TriggerType, TriggerUtility, normal_trigger_list, propagate_trigger_list
from Hbicc.testbench.PatternUtility import PatternHelper
from Common.fval import skip

SLICECOUNT = range(5)
LASTFAILCAPTUREDUT32INDEX = - 4
LASTFAILCAPTUREDUT21INDEX = - 2


trigger_cond_map = {TriggerType.ResumeTrigger.value: 'COND.SWTRIGRCVD',
                    TriggerType.DomainTrigger.value: 'COND.DMTRIGRCVD',
                    TriggerType.RCTrigger.value: 'COND.RCTRIGRCVD'}

trigger_map = {TriggerType.ResumeTrigger.value: 'Software_Trigger',
               TriggerType.DomainTrigger.value: 'Domain_Trigger',
               TriggerType.RCTrigger.value: 'RC_Trigger'}


# class Misc(HbiccTest):
#     @skip('Not implemented')
#     def DirectedSetWiredOrTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedSetLoadActiveTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedSetDomainTriggerTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedSetRCTriggerTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedClearWiredOrTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedClearLoadActiveTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedClearSoftwareTriggerTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedClearDomainTriggerTest(self):
#         pass
#
#     @skip('Not implemented')
#     def DirectedClearRCTriggerTest(self):
#         pass
#

class Triggers(HbiccTest):
    def setUp(self, tester=None):
        super().setUp()
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = 0b11111111111111111111111111111111111
        # self.ctp = 0b00000000000000000000000000000000000
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))
        self.slices.sort()
        self.pattern_helper.end_status = random.randint(0, 0xFFFFFFFF)

    def DirectedDomainTriggerTest(self):
        """Test focuses on running an infinite loop and sending a Domain Trigger to break it.
        It tests if the Domain Trigger flag gets set and the desired branching occurs on that flag then.
        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Create a Domain Trigger
        * Make a PCall to a Pattern potentially running an infinite loop
        * Check if Domain Trigger Flag set to branch out of the infinite loop
        * Send the Domain Trigger from RC to Patgen
        * Confirm that the expected trigger was sent and the Domain Trigger Flag was set on all slices

        **Criteria:** Infinte loop was broken, correct Trigger word was seen and Flag was set
        """

        condition = 'COND.DMTRIGRCVD'
        trigger_type = 3
        self.dut_id = 32
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.setup_trigger_utility_flag(trigger_type)
        trigger = self.trigger_utility.construct_trigger_word()

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId 0xFFFFFFFF
                    PCall PATTERN1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    NoCompareDriveZVectors length=1
        
                    PATTERN1:
                        CompareLowVectors length=100
                        ClearFlags
                        LOOP:
                            CompareLowVectors length=256
                            GotoIfNon `LOOP`, {condition}
                            Return
        '''

        self.Log('debug', f'first test run.\n{pattern_string}')
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        # self.env.hbicc.rc.clear_sw_trigger_fifo()
        self.pattern_helper.start_execution()
        time.sleep(.01)
        self.env.hbicc.rc.send_trigger_from_sender_to_receiver(sender=self.env.hbicc.rc, link=16, value=trigger,
                                                               receiver=self.env.hbicc.pat_gen)
        time.sleep(.01)
        self.pattern_helper.post_burst_processing(retry_limit=10000)
        expected_trigger = trigger
        self.trigger_utility.verify_last_trigger_seen_value(expected_trigger_payload=expected_trigger)
        for slice in self.pattern_helper.slice_channel_sets_combo.keys():
            observed_flag = self.env.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.FLAGS, slice)
            print(f'Observe flags: {observed_flag}')
            if observed_flag.Domain_Trigger != 1:
                self.Log('error', f'{slice} Domain Trigger Receive Flag not set. value {observed_flag.Domain_Trigger}')
        self.clear_trigger_utility_flag()

    def DirectedTriggerSyncTest(self):
        """Test for slices in the same domain to respond to trigger synchronously.

        Test will run an infinite loop and sending a Trigger to break it. The test will verify whether
        the normal trigger that is sent to the master slice will make all slices in the same domain to
        get out of the loop at the same user cycle from a propagate trigger from the master slice. Also
        the last seen trigger should be the propagate trigger instead of the normal trigger.
        **Test:**

        * For each trigger type:
        **** Sets Drive states to LOW and randomize the set of slices
        **** Create a Trigger with specific trigger type
        **** Make a PCall to a Pattern potentially running an infinite loop, make a capture after exiting loop
        **** rc send normal trigger, then rc broadcast propagate then all slices get out of the same cycle
        **** Check if Specific Trigger Flag set, also the infinite loop should thereby break
        **** Check if the last seen trigger is the propagate trigger that is expected to be seen
        **** Confirm that all the slices have went out of the infinite loop at the same cycle(user cycle)

        **Criteria:** Infinte loop was broken, correct Trigger word was seen and Flag was set
        Also all of the slices should be out of the loop in the same cycle, which would be tested in the
        last step
        """

        for trigger_type_encoding, condition in trigger_cond_map.items():
            self.run_sync_test_for_trigger_type(condition, trigger_type_encoding)

    def run_sync_test_for_trigger_type(self, condition, trigger_type):
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        trigger = self._construct_trigger(trigger_type)
        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId 0xFFFFFFFF
                    PCall PATTERN1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    NoCompareDriveZVectors length=1

                    PATTERN1:
                        CompareLowVectors length=100
                        ClearFlags 
                        LOOP:
                            CompareLowVectors length=256
                            GotoIfNon `LOOP`, {condition}
                        V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLH
                        Return
        '''
        self.Log('debug', f'first test run.\n{pattern_string}')
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes={'capture_fails': 1, 'capture_ctv': 0})
        self.pattern_helper.start_execution(3)
        self.wait_and_send_trigger(trigger)
        self.pattern_helper.post_burst_processing(retry_limit=10000)
        self.check_trigger_seen_and_trigger_flags(trigger)
        self.check_capture_cycle_count()
        self.clear_trigger_utility_flag()

    def check_trigger_seen_and_trigger_flags(self, trigger):
        expected_trigger = self._find_expected_trigger(trigger)

        trigger_correct = \
            self.trigger_utility.verify_last_trigger_seen_except_cycle_count(expected_trigger_payload=expected_trigger)
        flag_correct = self._check_trigger_flags(trigger_map)
        return trigger_correct and flag_correct

    def _check_trigger_flags(self, trigger_map):
        for slice in self.pattern_helper.slice_channel_sets_combo.keys():
            observed_flags = self.get_flag_register_from_patgen(slice)
            if getattr(observed_flags, trigger_map[self.trigger_utility.trigger_type]) != 1:
                self.Log('error',
                         f'Slice {slice} Domain Trigger Receive Flag not set. value {observed_flags.Domain_Trigger}')
                return False
        return True

    def get_flag_register_from_patgen(self, slice):
        observed_flags = self.env.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.FLAGS, slice)
        return observed_flags

    def _find_expected_trigger(self, original_trigger):
        if self.trigger_utility.trigger_type in normal_trigger_list:
            expected_trigger = self._get_propagate_trigger()
        elif self.trigger_utility.trigger_type in propagate_trigger_list:
            expected_trigger = original_trigger
        else:
            expected_trigger = original_trigger
            self.Log('error', 'testing for this trigger hasn\' t been implemented yet')
        return expected_trigger

    def _get_propagate_trigger(self):
        self.trigger_utility.trigger_type += 1
        propagate_trigger = self.trigger_utility.construct_trigger_word()
        self.trigger_utility.trigger_type -= 1
        return propagate_trigger

    def _construct_trigger(self, trigger_type, dut_id=32):
        self.dut_id = dut_id
        self.setup_trigger_utility_flag(trigger_type)
        trigger = self.trigger_utility.construct_trigger_word()
        return trigger

    def wait_and_send_trigger(self, trigger):
        time.sleep(.01)
        self.env.hbicc.rc.send_trigger_from_sender_to_receiver(sender=self.env.hbicc.rc, link=16, value=trigger,
                                                               receiver=self.env.hbicc.pat_gen)
        time.sleep(.01)

    def check_capture_cycle_count(self):
        domain_cycle = 0
        test_pass = True
        captured_cycles = []
        for slice_object in self.pattern_helper.slice_channel_sets_combo.values():
            slice_cycle = 0
            capture = slice_object.capture
            captured_cycles_slice = {'Slice': slice_object.index}
            for reg_index, pm in capture.pms.items():
                cycles = capture.get_user_cycles_from_error_stream(pm=pm, reg_index=reg_index,
                                                                   section_start=LASTFAILCAPTUREDUT32INDEX,
                                                                   section_end=LASTFAILCAPTUREDUT21INDEX)
                captured_cycles_slice.update({str(reg_index)+', '+str(0): cycles[0]})
                captured_cycles_slice.update({str(reg_index)+', '+str(1): cycles[1]})
                match, slice_cycle = self.check_mismatch_with_previous_pm(current_cycles=cycles,
                                                                          previous_cycle=slice_cycle)
                test_pass = test_pass and match
            match, slice_cycle = self._check_mismatch_with_previous_slice(previous_cycle=domain_cycle,
                                                                          current_cycle=slice_cycle)
            captured_cycles.append(captured_cycles_slice)
            test_pass = test_pass and match
        table = self.env.hbicc.contruct_text_table(data=captured_cycles, title_in=f'Trigger Sync Cycles Faiures')
        return self._log_sync_test_result(test_pass, table)

    def _check_mismatch_with_previous_slice(self, previous_cycle, current_cycle):
        match = True
        if previous_cycle:
            match = self._compare_domain_cycle(current_cycle, previous_cycle)
        else:
            previous_cycle = current_cycle
        return match, previous_cycle

    def _compare_domain_cycle(self, current_cycle, match, previous_cycle):
        if current_cycle != previous_cycle:
            return False
        else:
            return True

    def check_mismatch_with_previous_pm(self, current_cycles, previous_cycle):
        if previous_cycle:
            match = self._compare_slice_cycles(current_cycles, previous_cycle)
        else:
            match, previous_cycle = self._initialize_slice_cycle(current_cycles, previous_cycle)
        return match, previous_cycle

    def _initialize_slice_cycle(self, cycles, slice_cycle):
        if cycles[0] == cycles[1]:
            match = True
            slice_cycle = cycles[1]
        else:
            match = False
        return match, slice_cycle

    def _compare_slice_cycles(self, cycles, slice_cycle):
        if not cycles[0] == cycles[1] == slice_cycle:
            return False
        else:
            return True

    def _log_sync_test_result(self, test_pass, captured_cycles_table):
        if test_pass:
            self.Log('info', 'The Synchronization test was successful')
            return True
        else:
            self.Log('error', 'Synchronization test of triggers response fail: capture user cycle not the same')
            self.Log('error', f'error: {captured_cycles_table}')
            return False

    def DirectedRCTriggerTest(self):
        """Test focuses on running an infinite loop and sending an RC Trigger to break it.
        It tests if the RC Trigger flag gets set and the desired branching occurs on that flag then.
        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Create an RC Trigger
        * Make a PCall to a Pattern potentially running an infinite loop
        * Check if RC Trigger Flag set to branch out of the infinite loop
        * Send the RC Trigger from RC to Patgen
        * Confirm that the expected trigger was sent and the RC Trigger Flag was set on all slices

        **Criteria:** Infinte loop was broken, correct Trigger word was seen and Flag was set
        """

        condition = 'COND.RCTRIGRCVD'
        trigger_type = 12
        self.dut_id = 32
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.setup_trigger_utility_flag(trigger_type)
        trigger = self.trigger_utility.construct_trigger_word()

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId 0xFFFFFFFF
                    PCall PATTERN1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    NoCompareDriveZVectors length=1
                    
                    PATTERN1:
                        CompareLowVectors length=100
                        ClearFlags
                        LOOP:
                            CompareLowVectors length=256
                            GotoIfNon `LOOP`, {condition}
                            Return
        '''

        self.Log('debug', f'first test run.\n{pattern_string}')
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.start_execution()
        time.sleep(.01)
        self.env.hbicc.rc.send_trigger_from_sender_to_receiver(sender=self.env.hbicc.rc, link=16, value=trigger,
                                                               receiver=self.env.hbicc.pat_gen)
        time.sleep(.01)
        self.pattern_helper.post_burst_processing(retry_limit=10000)
        expected_trigger = trigger
        self.trigger_utility.verify_last_trigger_seen_value(expected_trigger_payload=expected_trigger)
        for slice in self.pattern_helper.slice_channel_sets_combo.keys():
            observed_flag = self.env.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.FLAGS, slice)
            print(f'Observe flags: {observed_flag}')
            if observed_flag.RC_Trigger != 1:
                self.Log('error', f'{slice} RC Trigger Receive Flag not set. value {observed_flag.RC_Trigger}')
        self.clear_trigger_utility_flag()

    def DirectedSWTriggerTest(self):
        """Test focuses on running an infinite loop and sending a Software Trigger to break it.
        It tests if the Software Trigger flag gets set and the desired branching occurs on that flag then.
        **Test:**

        * Sets FixedDriveState to LOW
        * Randomize the set of slices and choose all channel sets
        * Create a Software Trigger
        * Make a PCall to a Pattern potentially running an infinite loop
        * Check if Software Trigger Flag set to branch out of the infinite loop
        * Send the Software Trigger from RC to Patgen
        * Confirm that the expected trigger was sent and the Software Trigger Flag was set on all slices

        **Criteria:** Infinte loop was broken, correct Trigger word was seen and Flag was set
        """

        condition = 'COND.SWTRIGRCVD'
        trigger_type = 1
        self.dut_id = 32
        self.trigger_utility = TriggerUtility(self.env.hbicc, slice_list=self.slices)
        self.setup_trigger_utility_flag(trigger_type)
        trigger = self.trigger_utility.construct_trigger_word()

        pattern_string = f'''
                    PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF # all unserialized pins hold value in serialized sections
                    S stype=CAPTURE_PIN_MASK,                           data={self.ctp} #ctp mask

                    PatternId 0xFFFFFFFF
                    PCall PATTERN1

                    I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                    NoCompareDriveZVectors length=1
                            
                    PATTERN1:
                        CompareLowVectors length=100
                        ClearFlags
                        LOOP:
                            CompareLowVectors length=256
                            GotoIfNon `LOOP`, {condition}
                            Return
        '''

        self.Log('debug', f'first test run.\n{pattern_string}')
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
        self.pattern_helper.start_execution()
        time.sleep(.01)
        self.env.hbicc.rc.send_trigger_from_sender_to_receiver(sender=self.env.hbicc.rc, link=16, value=trigger,
                                                               receiver=self.env.hbicc.pat_gen)
        time.sleep(.01)
        self.pattern_helper.post_burst_processing(retry_limit=10000)
        expected_trigger = trigger
        self.trigger_utility.verify_last_trigger_seen_value(expected_trigger_payload=expected_trigger)
        for slice in self.pattern_helper.slice_channel_sets_combo.keys():
            observed_flag = self.env.hbicc.pat_gen.read_slice_register(self.env.hbicc.pat_gen.registers.FLAGS, slice)
            print(f'Observe flags: {observed_flag}')
            if observed_flag.Software_Trigger != 1:
                self.Log('error',
                         f'{slice} Software Trigger Receive Flag not set. value {observed_flag.Software_Trigger}')
        self.clear_trigger_utility_flag()

    def setup_trigger_utility_flag(self, trigger_type):
        self.trigger_utility.trigger_type = trigger_type
        self.trigger_utility.target_resource = 0
        self.trigger_utility.target_domain = 1
        self.trigger_utility.dut_id = self.dut_id
        self.trigger_utility.sender = self.env.hbicc.rc
        self.trigger_utility.receiver = self.env.hbicc.pat_gen
        self.trigger_utility.attribute_holder.update({'target_resource': self.trigger_utility.target_resource,
                                                      'target_domain': self.trigger_utility.target_domain,
                                                      'dut_id': self.trigger_utility.dut_id})
        self.trigger_utility.configure_trigger_control_register()

    def clear_trigger_utility_flag(self):
        self.trigger_utility.target_domain = 0
        self.trigger_utility.attribute_holder.update({'target_domain': self.trigger_utility.target_domain})
        self.trigger_utility.configure_trigger_control_register()

    # @skip('Not implemented')
    # def DirectedDomainTriggerTest(self):
    #     self.Scenario(0x0ff2, 'COND.DMTRIGRCVD')
    #     pass

    # @skip('Not implemented')
    # def DirectedRCTriggerTest(self):
    #     pass

    @skip('Not implemented')
    def PatternCompleteInTriggerTest(self, slot, slice):
        pass

    # @skip('Not implemented')
    # def PatternCompleteInTriggerTest(slot, slice):
    #     pass
