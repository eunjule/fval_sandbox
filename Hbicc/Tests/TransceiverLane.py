# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time

from Common.fval import expected_failure
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.instrument.hbicc import TransceiverLink

MAX_FAIL_COUNT = 10
PSDB_LINK_SPEED = 10
TOTAL_ITERATIONS = 1


class Diagnostics(HbiccTest):

    def tearDown(self):
        self.env.hbicc.check_transceiver_paths()
        super().tearDown()

    def DirectedPatgenTxToRingMultiplierRxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between Patgen-RinMultiplier.
        Check the status register and report pass/fail.

        **Test:**

        * Start PCIe Communication by enabling PatGen RING_BUS_CONTROL *enable_prbs* register to 1
        * In order, check that each of the following BUS_STATUS registers values are set to 1
            - *pll_locked ->tx*
            - *rx_freqlocked ->tx*
            - *wordaligned ->rx*
            - *lanealigned ->tx*
            - *prbs_locked ->rx*
            - *prbs_success ->rx*
        * Check PatGen *ERROR_COUNT* register is 0 ->rx.

        **Criteria:** Error count of 0

        """
        if self.is_link_present(self.env.hbicc.pat_gen, self.env.hbicc.ring_multiplier):
            helper = TransceiverLink(self.env.hbicc.pat_gen, self.env.hbicc.ring_multiplier)
            self.execute_one_way_link_communication(helper, test_iterations)

    def is_link_present(self, tx_device, rx_device):
        if tx_device is None or rx_device is None:
            self.Log('warning', f'Transmitter and/or Receiver Device Unvailable')
            return False
        return True

    def DirectedRingMultiplierTxToPatgenRxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between RinMultiplier-Patgen.
        Check the status register and report pass/fail.

        **Test:**

        * Start PCIe Communication by enabling Ring Multiplier PATGEN_BUS_CONTROL *enable_prbs* register to 1
        * In order, check that each of the following BUS_STATUS registers values are set to 1
            - *pll_locked*
            - *rx_freqlocked*
            - *wordaligned*
            - *lanealigned*
            - *prbs_locked*
            - *prbs_success*
        * Check Ring Multiplier *ERROR_COUNT* register is 0.

        **Criteria:** Error count of 0

        """
        if self.is_link_present(self.env.hbicc.ring_multiplier, self.env.hbicc.pat_gen):
            helper = TransceiverLink(self.env.hbicc.ring_multiplier, self.env.hbicc.pat_gen)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedRingMultiplierTxToPSDB0PinMultiplier0RxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and reciever configuration between RinMultiplier-PSDB.
        Check the status register and report pass/fail

        **Test:**

        * Start PCIe Communication by enabling Ring Multiplier PSDB0_BUS_CONTROL *enable_prbs* register to 1
        * In order, check that each of the following BUS_STATUS registers values are set to 1
            - *pll_locked*
            - *rx_freqlocked*
            - *wordaligned*
            - *lanealigned*
            - *prbs_locked*
            - *prbs_success*
        * Check Ring Multiplier *ERROR_COUNT* register is 0.

        **Criteria:** Error count of 0

        """
        if self.is_link_present(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_0_pin_0):
            helper = TransceiverLink(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_0_pin_0)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedPSDB0PinMultiplier0TxToRingMultiplierRxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between RinMultiplier-PSDB.
        Check the status register and report pass/fail.

        """
        if self.is_link_present(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.ring_multiplier):
            helper = TransceiverLink(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.ring_multiplier)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedRingMultiplierTxToPSDB1PinMultiplier0RxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and reciever configuration between RinMultiplier-PSDB.
        Check the status register and report pass/fail

        **Test:**

        * Start PCIe Communication by enabling Ring Multiplier PSDB0_BUS_CONTROL *enable_prbs* register to 1
        * In order, check that each of the following BUS_STATUS registers values are set to 1
            - *pll_locked*
            - *rx_freqlocked*
            - *wordaligned*
            - *lanealigned*
            - *prbs_locked*
            - *prbs_success*
        * Check Ring Multiplier *ERROR_COUNT* register is 0.

        **Criteria:** Error count of 0

        """
        if self.is_link_present(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_1_pin_0):
            helper = TransceiverLink(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_1_pin_0)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedPSDB1PinMultiplier0TxToRingMultiplierRxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between RinMultiplier-PSDB.
        Check the status register and report pass/fail.

        """
        if self.is_link_present(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.ring_multiplier):
            helper = TransceiverLink(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.ring_multiplier)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedPSDB0PinMultiplier0TxToPinMultiplier1RxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between PSDB0-PSDB1.
        Check the status register and report pass/fail.

        """
        if self.is_link_present(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.psdb_0_pin_1):
            helper = TransceiverLink(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.psdb_0_pin_1)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedPSDB0PinMultiplier1TxToPinMultiplier0RxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verifying that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between PSDB0-PSDB1.
        Check the status register and report pass/fail.

        """
        if self.is_link_present(self.env.hbicc.psdb_0_pin_1, self.env.hbicc.psdb_0_pin_0):
            helper = TransceiverLink(self.env.hbicc.psdb_0_pin_1, self.env.hbicc.psdb_0_pin_0)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedPSDB1PinMultiplier0TxToPinMultiplier1RxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verify that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between PSDB1-PSDB0.
        Check the status register and report pass/fail.

        """
        if self.is_link_present(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.psdb_1_pin_1):
            helper = TransceiverLink(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.psdb_1_pin_1)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedPSDB1PinMultiplier1TxToPinMultiplier0RxTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Configure the control block through registers and verify that random data generated though the BIST engine
        is correct. This would verify correct pin-mapping for the transmitter and receiver configuration between PSDB1-PSDB0.
        Check the status register and report pass/fail.

        """
        if self.is_link_present(self.env.hbicc.psdb_1_pin_1, self.env.hbicc.psdb_1_pin_0):
            helper = TransceiverLink(self.env.hbicc.psdb_1_pin_1, self.env.hbicc.psdb_1_pin_0)
            self.execute_one_way_link_communication(helper, test_iterations)

    def DirectedBidirectionalPatgenToRingMultiplierDataTransferTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Initiate transceiver data transfer for PatGen and Ring Multiplier, in both upstream and downstream
        direction by enabling PRBS. Report error results for each link by slice and lane. Finally,
        disable PRBS to stop data transfer.

        **Test:**

        1. If present, initiate data transfer by enabling PRBS for Pat Gen <---> Ring Multiplier
        2. Report error count for each link/slice/lane
        3. Repeat step 1 and 2 for 50 iterations

        **Criteria:** Error Count of Zero for all links

        **NOTE:** `Refer to test DirectedRingMultiplierTxToPatgenRxTest for more set up information`

        """
        if self.is_link_present(self.env.hbicc.pat_gen, self.env.hbicc.ring_multiplier):
            link = self.initiate_bidirectional_linkage_patgen_ringmultiplier
            linkage_pairs, total_error_count, total_pattern_count = self.execute_two_way_link_communication(link,
                                                                                                            test_iterations)
            self.report_linkage_errors(linkage_pairs, test_iterations, total_error_count, total_pattern_count)

    def DirectedBidirectionalRingMultiplierToPSDB0PinMultiplier0DataTransferTest(self,
                                                                                 test_iterations=TOTAL_ITERATIONS):
        """
        Initiate transceiver data transfer for Ring Multiplier and PSDB0 Pin0, in both upstream and downstream
        direction by enabling PRBS. Report error results for each link by slice and lane. Finally,
        disable PRBS to stop data transfer.

        **Test:**

        1. If present, initiate data transfer by enabling PRBS for Ring Multiplier <---> PSDB0 Pin0
        2. Report error count for each link/slice/lane
        3. Repeat step 1 and 2 for 50 iterations

        **Criteria:** Error Count of Zero for all links

        **NOTE:** `Refer to test DirectedRingMultiplierTxToPatgenRxTest for more set up information`

        """
        if self.is_link_present(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_0_pin_0):
            link = self.initiate_bidirectional_linkage_ringmultiplier_psdb_0_pin_0
            linkage_pairs, total_error_count, total_pattern_count = self.execute_two_way_link_communication(link,
                                                                                                            test_iterations)
            self.report_linkage_errors(linkage_pairs, test_iterations, total_error_count, total_pattern_count)

    def DirectedBidirectionalRingMultiplierToPSDB1Pin0DataTransferTest(self, test_iterations=TOTAL_ITERATIONS):
        """
        Initiate transceiver data transfer for Ring Multiplier and PSDB1 Pin0, in both upstream and downstream
        direction by enabling PRBS. Report error results for each link by slice and lane. Finally,
        disable PRBS to stop data transfer.

        **Test:**

        1. If present, initiate data transfer by enabling PRBS for Ring Multiplier <---> PSDB1 Pin0
        2. Report error count for each link/slice/lane
        3. Repeat step 1 and 2 for 50 iterations

        **Criteria:** Error Count of Zero for all links

        **NOTE:** `Refer to test DirectedRingMultiplierTxToPatgenRxTest for more set up information`

        """
        if self.is_link_present(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_1_pin_0):
            link = self.initiate_bidirectional_linkage_ringmultiplier_psdb_1_pin_0
            linkage_pairs, total_error_count, total_pattern_count = self.execute_two_way_link_communication(link,
                                                                                                            test_iterations)
            self.report_linkage_errors(linkage_pairs, test_iterations, total_error_count, total_pattern_count)

    def DirectedBidirectionalPSDB0PinMultiplier0ToPSDB0PinMultiplier1Test(self, test_iterations=TOTAL_ITERATIONS):
        """
        Initiate transceiver data transfer for PSDB0 Pin0 and Pin1, in both upstream and downstream
        direction by enabling PRBS. Report error results for each link by slice and lane. Finally,
        disable PRBS to stop data transfer.

        **Test:**

        1. If present, initiate data transfer by enabling PRBS for PSDB0 Pin0 to PSDB0 Pin1
        2. Report error count for each link/slice/lane
        3. Repeat step 1 and 2 for 50 iterations

        **Criteria:** Error Count of Zero for all links

        **NOTE:** `Refer to test DirectedRingMultiplierTxToPatgenRxTest for more set up information`

        """
        if self.is_link_present(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.psdb_0_pin_1):
            link = self.initiate_bidirectional_linkage_psdb_0_pin_0_psdb_0_pin_1
            linkage_pairs, total_error_count, total_pattern_count = self.execute_two_way_link_communication(link,
                                                                                                            test_iterations)
            self.report_linkage_errors(linkage_pairs, test_iterations, total_error_count, total_pattern_count)

    def DirectedBidirectionalPSDB1PinMultiplier0ToPSDB1PinMultiplier1Test(self, test_iterations=TOTAL_ITERATIONS):
        """
        Initiate transceiver data transfer for PSDB1 Pin0 and Pin1, in both upstream and downstream
        direction by enabling PRBS. Report error results for each link by slice and lane. Finally,
        disable PRBS to stop data transfer.

        **Test:**

        1. If present, initiate data transfer by enabling PRBS for PSDB1 Pin0 to PSDB1 Pin1
        2. Report error count for each link/slice/lane
        3. Repeat step 1 and 2 for 50 iterations

        **Criteria:** Error Count of Zero for all links

        **NOTE:** `Refer to test DirectedRingMultiplierTxToPatgenRxTest for more set up information`

        """
        if self.is_link_present(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.psdb_1_pin_1):
            link = self.initiate_bidirectional_linkage_psdb_1_pin_0_psdb_1_pin_1
            linkage_pairs, total_error_count, total_pattern_count = self.execute_two_way_link_communication(link,
                                                                                                            test_iterations)
            self.report_linkage_errors(linkage_pairs, test_iterations, total_error_count, total_pattern_count)

    def DirectedHbiBidirectionalDataTransferReliabilityTest(self, hours=0, minutes=.5):
        """
        Initiate transceiver data transfer for all devices present in the HBI tester, in both upstream and downstream
        direction by enabling PRBS. Allow data to transfer in links for the user's specified time. Report error results
        for each link by slice and lane. Finally, disable PRBS to stop data transfer.

        **Test:**

        * If present, initiate data transfer by enabling PRBS for:
            1. Pat Gen <---> Ring Multiplier
            2. Ring Multiplier <---> PSDB0 Pin0
            3. Ring Multiplier <---> PSDB1 Pin0
            4. PSDB0 Pin0 to PSDB0 Pin1
            5. PSDB1 Pin0 to PSDB1 Pin1
        * Allow data to transfer, reporting error count for each link periodically
        * Report final results and disable PRBS

        **Criteria:** Error Count of Zero for all links

        **NOTE:** `Refer to test DirectedRingMultiplierTxToPatgenRxTest for more set up information`

        """
        linkage_pairs = []

        if self.env.hbicc.pat_gen and self.env.hbicc.ring_multiplier:
            self.initiate_bidirectional_linkage_patgen_ringmultiplier(linkage_pairs)

        if self.env.hbicc.ring_multiplier and self.env.hbicc.psdb_0_pin_0:
            self.initiate_bidirectional_linkage_ringmultiplier_psdb_0_pin_0(linkage_pairs)

        if self.env.hbicc.ring_multiplier and self.env.hbicc.psdb_1_pin_0:
            self.initiate_bidirectional_linkage_ringmultiplier_psdb_1_pin_0(linkage_pairs)

        if self.env.hbicc.psdb_0_pin_0 and self.env.hbicc.psdb_0_pin_1:
            self.initiate_bidirectional_linkage_psdb_0_pin_0_psdb_0_pin_1(linkage_pairs)

        if self.env.hbicc.psdb_1_pin_0 and self.env.hbicc.psdb_1_pin_1:
            self.initiate_bidirectional_linkage_psdb_1_pin_0_psdb_1_pin_1(linkage_pairs)

        for index, linkage in enumerate(linkage_pairs):
            if index % 2 == 0:
                self.Log('info', 'Running {}'.format(linkage.link_name(True)))

        self.status_report_after_time_lapse(hours, minutes, linkage_pairs)

        for linkage in linkage_pairs:
            linkage.tx_device.disable_prbs(linkage.tx_control_register())

    def initiate_bidirectional_linkage_patgen_ringmultiplier(self, linkage_pairs):
        downstream = TransceiverLink(self.env.hbicc.pat_gen, self.env.hbicc.ring_multiplier)
        downstream.reset_tx_rx_link()
        downstream.setup_communication_link()
        downstream.is_prbs_set_up()

        upstream = TransceiverLink(self.env.hbicc.ring_multiplier, self.env.hbicc.pat_gen)
        upstream.setup_communication_link()
        upstream.is_prbs_set_up()

        state = self.are_bidirectional_links_up(downstream, upstream)
        if state:
            linkage_pairs.extend([downstream, upstream])
        return state

    def initiate_bidirectional_linkage_ringmultiplier_psdb_0_pin_0(self, linkage_pairs):
        downstream = TransceiverLink(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_0_pin_0)
        for retry in range(10):
            downstream.reset_tx_rx_link()
            downstream.setup_communication_link()
            if downstream.is_prbs_set_up():
                break
        else:
            self.Log('critical', f'Maximum retry limit of {retry} exhausted for Prbs setup exiting iteration {loop}')

        upstream = TransceiverLink(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.ring_multiplier)
        upstream.setup_communication_link()
        upstream.is_prbs_set_up()

        state = self.are_bidirectional_links_up(downstream, upstream)
        if state:
            linkage_pairs.extend([downstream, upstream])
        return state

    def initiate_bidirectional_linkage_ringmultiplier_psdb_1_pin_0(self, linkage_pairs):
        downstream = TransceiverLink(self.env.hbicc.ring_multiplier, self.env.hbicc.psdb_1_pin_0)

        for retry in range(10):
            downstream.reset_tx_rx_link()
            downstream.setup_communication_link()
            if downstream.is_prbs_set_up():
                break
        else:
            self.Log('critical', f'Maximum retry limit of {retry} exhausted for Prbs setup exiting iteration {loop}')

        upstream = TransceiverLink(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.ring_multiplier)
        upstream.setup_communication_link()
        upstream.is_prbs_set_up()

        state = self.are_bidirectional_links_up(downstream, upstream)
        if state:
            linkage_pairs.extend([downstream, upstream])
        return state

    def initiate_bidirectional_linkage_psdb_0_pin_0_psdb_0_pin_1(self, linkage_pairs):
        downstream = TransceiverLink(self.env.hbicc.psdb_0_pin_0, self.env.hbicc.psdb_0_pin_1)

        downstream.reset_tx_rx_link()
        downstream.setup_communication_link()
        downstream.is_prbs_set_up()

        upstream = TransceiverLink(self.env.hbicc.psdb_0_pin_1, self.env.hbicc.psdb_0_pin_0)
        upstream.setup_communication_link()
        upstream.is_prbs_set_up()

        state = self.are_bidirectional_links_up(downstream, upstream)
        if state:
            linkage_pairs.extend([downstream, upstream])
        return state

    def initiate_bidirectional_linkage_psdb_1_pin_0_psdb_1_pin_1(self, linkage_pairs):

        downstream = TransceiverLink(self.env.hbicc.psdb_1_pin_0, self.env.hbicc.psdb_1_pin_1)

        downstream.reset_tx_rx_link()
        downstream.setup_communication_link()
        downstream.is_prbs_set_up()

        upstream = TransceiverLink(self.env.hbicc.psdb_1_pin_1, self.env.hbicc.psdb_1_pin_0)
        upstream.setup_communication_link()
        upstream.is_prbs_set_up()

        state = self.are_bidirectional_links_up(downstream, upstream)
        if state:
            linkage_pairs.extend([downstream, upstream])
        return state

    def execute_one_way_link_communication(self, link, test_iterations):
        pattern_counts = []
        error_counts = []
        for loop in range(test_iterations):
            for retry in range(10):
                link.reset_tx_rx_link()
                link.setup_communication_link()
                if link.is_prbs_set_up():
                    break
            else:
                self.Log('critical',f'Maximum retry limit of {retry} exhausted for Prbs setup exiting iteration {loop}')
                break

            time.sleep(1)
            pattern_count = link.pattern_count_transferred()
            pattern_counts.append(pattern_count)

            self.check_error_count_all_slices_lanes(link, pattern_count, error_counts, loop)
            link.tx_device.disable_prbs(link.tx_control_register())

        self.report_linkage_errors(link, test_iterations, error_counts, pattern_counts)

    def execute_two_way_link_communication(self, link, test_iterations):
        error_counts = []
        pattern_counts = []
        for test_iteration in range(test_iterations):
            linkage_pairs = []
            if not link(linkage_pairs):
                continue

            for linkage in linkage_pairs:
                pattern_count = linkage.pattern_count_transferred()
                if pattern_count > 0:
                    pattern_counts.append(pattern_count)
                self.check_error_count_all_slices_lanes(linkage, pattern_count, error_counts, test_iteration)
                linkage.tx_device.disable_prbs(linkage.tx_control_register())

            total_errors = len(error_counts) >= MAX_FAIL_COUNT
            total_iterations = len(error_counts) >= test_iterations * 2
            total_pattern_count = len(pattern_counts) >= MAX_FAIL_COUNT
            if any([total_errors, total_iterations, total_pattern_count]):
                break

        return linkage_pairs, error_counts, pattern_counts

    def are_bidirectional_links_up(self, downstream, upstream):
        downstream_status = downstream.tx_device.read_bar_register(downstream.tx_bus_status_register).value
        upstream_status = upstream.tx_device.read_bar_register(upstream.tx_bus_status_register).value
        expected_status_register = 0x3e9
        if downstream_status == upstream_status == expected_status_register:
            pass
        else:
            self.Log('warning',
                     '{}: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x{:x} != 0x{:x} != 0x3e9'.format(
                         downstream.link_name(True),
                         downstream_status,
                         upstream_status))
            return True
        return True

    def report_linkage_errors(self, linkages, test_iterations, total_error_count, pattern_counts):
        if isinstance(linkages, list) and linkages:
            link = linkages[0].link_name(True)
        elif linkages:
            link = linkages.link_name()
        else:
            link = 'None'

        is_pattern_count_errors = [True for x in pattern_counts if x == 0]

        if not total_error_count and not is_pattern_count_errors:

            self.Log('info',
                     '{} Transceiver Communication Successful for all {} iterations for all slices/lanes. Total Pattern Count is {:,}'.format(
                         link, test_iterations,
                         sum(pattern_counts)))
        elif is_pattern_count_errors and total_error_count:
            error = '{}: {} of {} iterations had pattern count transfers equal to 0 and error count of 0. This is incorrect.'.format(
                link,
                len(is_pattern_count_errors), test_iterations)
            self.Log('error', error)
        elif total_error_count and not is_pattern_count_errors:
            tables = '\n\n'.join(total_error_count)
            self.Log('error', '{} Transceiver Communication Failed. {}'.format(link, tables))
        else:
            self.Log('error', 'Link communication failed for unknown reason')

    def status_report_after_time_lapse(self, hours, minutes, linkage_pairs):
        total_hours = hours * minutes
        run_time = total_hours + minutes
        run_time_so_far = 0
        sleep_time = 30

        while run_time_so_far < run_time:
            time.sleep(sleep_time)
            run_time_so_far += sleep_time / 60
            for linkage in linkage_pairs:
                headers = ['Link', 'Test Time', '% Completed', 'Pattern Count Transferred']
                link = linkage.link_name()
                test_time = '{} hrs {} mins'.format(hours, minutes)
                completed = '{} mins or {} %'.format(round(run_time_so_far, 2),
                                                     round((run_time_so_far / run_time) * 100, 2))
                pattern_count = '{:,}'.format(linkage.pattern_count_transferred())
                data = [{'Link': link, 'Test Time': test_time, '% Completed': completed,
                         'Pattern Count Transferred': pattern_count}]
                message = linkage.tx_device.contruct_text_table(headers, data)
                table = self.check_error_count_all_slices_lanes(linkage, pattern_count)
                if table:
                    self.Log('error', '{} {}'.format(message, table))
                else:
                    self.Log('info', message)

    def check_error_count_all_slices_lanes(self, helper, pattern_count, total_error_count=[], iteration=0,
                                           slice_list=None):
        headers = ['Iteration', 'Link', 'Slice', 'Lane', 'Tx Bus Status (REG)', 'Rx Bus Status (REG)',
                   'Error Count (REG)', 'Bar Space Error Count (REG)']

        error_count, link = self.read_error_lanes(helper, iteration, slice_list)
        if error_count:
            table = helper.tx_device.contruct_text_table(headers, error_count)
            total_error_count.append(table)
            return table
        elif pattern_count == 0:
            error = '{}: Pattern count equal 0 and error count is 0. This is incorrect.'.format(helper.link_name())
            total_error_count.append(error)
            return error
        else:
            return None

    def read_error_lanes(self, helper, iteration, slice_list=None):
        error_count = []
        rx_lanes = self.get_total_lanes(helper)

        link = helper.link_name()
        for bar_space in range(1, 6):
            bar_space_error_count = helper.rx_device.read_bar_register(helper.rx_error_count_register, bar_space)
            for lane in rx_lanes:
                tx_bus_status_register_value = helper.tx_device.read_bar_register(helper.tx_bus_status_register,
                                                                              bar_space=bar_space)
                rx_bus_status_register_value = helper.rx_device.read_bar_register(helper.rx_bus_status_register,
                                                                              bar_space=bar_space)
                error_count_value = helper.rx_device.read_bar_register(helper.rx_xcvr_error_count_lane,
                                                                   bar_space=bar_space,
                                                                   index=lane)
                if error_count_value.value != 0:
                    error = {'Iteration': iteration, 'Link': link, 'Slice': bar_space, 'Lane': lane}
                    error.update({'Tx Bus Status (REG)': '{:x} (0x{:x})'.format(tx_bus_status_register_value.value,
                                                                                tx_bus_status_register_value.ADDR)})
                    error.update({'Rx Bus Status (REG)': '{:x} (0x{:x})'.format(rx_bus_status_register_value.value,
                                                                                rx_bus_status_register_value.ADDR)})
                    error.update(
                        {'Error Count (REG)': '{} (0x{:x})'.format(error_count_value.value, error_count_value.ADDR)})
                    error.update({'Bar Space Error Count (REG)': '{} (0x{:x})'.format(bar_space_error_count.value,
                                                                                      bar_space_error_count.ADDR)})
                    error_count.append(error)
        return error_count, link

    def get_total_lanes(self, helper):
        if helper.tx_device.name() == 'PATGEN' or helper.rx_device.name() == 'PATGEN':
            rx_lanes = range(2)
        else:
            rx_lanes = range(4)
        return rx_lanes



