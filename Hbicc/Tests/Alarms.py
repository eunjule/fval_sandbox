# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""HBICC alarm bits indicate problems occurred during execution.

Critical alarms indicate the pattern did not execute correctly, or cannot
be guaranteed to have executed correctly.

Non-critical alarms provide extra information that may be used to identify
why a critical alarm occurred.
"""

import os
import random

from Common.fval import skip
from Hbicc.Tests.HbiccTest import HbiccTest
from Hbicc.testbench.PatternUtility import PatternHelper
from Hbicc.testbench.capturefailures import CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM, CTV_DATA_STREAM_BLOCK_SIZE, STREAM_BLOCK


ID_MAX_VALUE = 100

PIN_DIRECTION = {'DRIVE_LOW': 0x0000, 'DRIVE_HIGH': 0x5555, 'TRISTATE': 0xAAAA, 'RELEASE_PATTERN_CNTRL': 0xFFFF}

CTV_HEADER_STREAM_START_ADDRESS = 0x400000000
CTV_DATA_STREAM_START_ADDRESS = 0x440000000
TRACE_STREAM_START_ADDRESS = 0x480000000
PM_STREAM_ADDRESSES = {0: 0x4c0000000, 1: 0x500000000, 2: 0x540000000, 3: 0x580000000}

ADDRESSES = [CTV_DATA_STREAM_START_ADDRESS, CTV_HEADER_STREAM_START_ADDRESS, TRACE_STREAM_START_ADDRESS]
ADDRESSES += list(PM_STREAM_ADDRESSES.values())
PADDING = 1024
SLICECOUNT = range(5)

MILLION = 1_000_000
ONE_MB = 1 << 20


class OverflowAlarmTestConstants:
    VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE = 8
    MIN_CTV_DATA_NUM = 8
    MAX_CTV_DATA_NUM = 128

    MIN_HEADER_BLOCK_NUM_LIMIT = 128

    PIN_ERROR_BLOCK_PER_VECTOR = 2
    PIN_ERROR_EOB_BLOCK_NUM = 2
    STREAM_BLOCK_PER_PIN_ERROR_PACKAGE = 2

    MAX_BLOCK_NUM_LIMIT = 1024


class Critical(HbiccTest, OverflowAlarmTestConstants):
    """Tests for alarms that invalidate execution results

        Comment for CRC Alarm Test:
            Original Goal: Force a CRC error

            Update Sept 25:
                1. CRC error cannot be created by FVAL. CRC
                errors can only be generated in the hardware
                level.
                2. PrbsInsertError in ring bus control won't
                make CRC error happen although it can only
                cause the prbs process to fail with the ring
                bus status register to be 0x3e1 instead of
                0x3e9 and the ring bus error count increased.
                However, that is not considered as a CRC error.
    """

    def setUp(self, tester=None):
        super().setUp(tester)
        self.pattern_helper = PatternHelper(self.env)
        self.ctp = 0b11111111111111111111111111111111111
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT) - 1))
        self.slices.sort()
        self.pattern_helper.is_simulator_active = True
        self.pattern_helper.end_status = random.getrandbits(32)
        self.pattern_helper.user_mode = random.randint(0, 0b11111111111111111111111111111111111)
        self.test_repeat = 10
        self.unit_test = False

    def DirectedStackOverflowCausesStackAlarmTest(self):
        """Overflow the stack with infinite PCALL recursion"""
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [1]
        id_0 = random.randint(0, ID_MAX_VALUE)
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        self.pattern_helper.is_simulator_active = False
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                PatternId {id_0}
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm=64
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    DriveZeroVectors length=100
                    I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                    I optype=BRANCH, base=PFB, br=PCALL_I, imm=0, cond=ZERO, invcond=1      
                    DriveZeroVectors length=100
                    Return 
        '''

        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical(['underrun'])
        self.env.hbicc.hbicc_alarms.set_auto_abort_alarms_as_non_critical_at_prestage(['stack_error', 'illegal_instruction'])

        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        stack_error = 'stack_error'
        self.check_log_alarm(stack_error)

    def DirectedStackUnderrunCauseStackAlarmTest(self):
        """Execute a RETURN instruction with an empty stack"""
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                DriveZeroVectors length=1024
                Return

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1
                Return
        '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical(['underrun'])
        self.env.hbicc.hbicc_alarms.set_auto_abort_alarms_as_non_critical_at_prestage(['stack_error', 'illegal_instruction'])

        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        stack_error = 'stack_error'
        self.check_log_alarm(stack_error)

    def DirectedCheckECCErrorAlarmTest(self):
        """Use a DFT feature to cause ECC errors"""
        ecc_error = 'ecc_error'
        population = set(SLICECOUNT)
        self.slices = random.sample(population, 1)
        self.env.hbicc.set_up_fval_alarm_logging(self.slices)
        self.env.hbicc.hbicc_alarms.set_auto_abort_alarms_as_non_critical_at_prestage(['stack_error', 'illegal_instruction'])
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([ecc_error])

        self.env.hbicc.pat_gen.enable_induced_ecc_errors(slices=self.slices)

        for i in range(100):
            random_data = os.urandom(1 << 20)
            self.env.hbicc.pat_gen.dma_write(0, random_data, self.slices[0])
            self.env.hbicc.pat_gen.dma_read(0, len(random_data), self.slices[0])
            if self.env.hbicc.hbicc_alarms.is_alarm_set({ecc_error}):
                self.env.hbicc.hbicc_alarms.log_all_alarms(self.slices, check_correctness=False,
                                                           table_title='Found Alarm')
                break
        else:
            self.Log('error', f'Alarm was not set for {ecc_error}. Exhausted for loop {i}')

        self.env.hbicc.pat_gen.disable_induced_ecc_errors(slices=self.slices)
        self.env.hbicc.pat_gen.force_ecc_reset(self.slices)
        self.env.hbicc.pat_gen.initialize_memory_block(slices=self.slices)

    @skip('RM recovery WIP; HDMT Ticket 113299')
    def DirectedCtpFullTest(self):
        """
        Reset CTP mask too frequently (3k times)
        Frequent pcall required; Pre-stage mode required
        """
        capture_this_pin_full_error = 'capture_this_pin_full'
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
    
                NoCompareDriveZVectors length=1024
                Call PATTERN0
    
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1
    
                PATTERN0:
                NoCompareDriveZVectors length=1024
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=3000
                    LOOP:
    
                    S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                    PatternId 0x1
                    PCall PATTERN1
    
                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1
                Return                                       
    
                PATTERN1:                
                DriveZeroVectors length=1
                Return
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.alarm_trigger_and_check_in_prestage(capture_this_pin_full_error, pattern_string)

    def DirectedIllegalInstructionAlarmTest(self):
        """ Try to execute an illegal instruction; Verify that this alarm is raised when the following
        illegal instruction scenario is encountered in Pat Gen:
        A Vector Repeat Instruction is followed by a Vector that turns out to be an Instruction,
        Creating a Nested Instruction Scenario which is Illegal!
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        DriveZeroVectors length=1024
                        Repeat 100
                        I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0x00000000, dest=ALUDEST.FLAGS

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        Return
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        illegal_instruction_error = 'illegal_instruction'
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([illegal_instruction_error])

        self.env.hbicc.hbicc_alarms.set_auto_abort_alarms_as_non_critical_at_prestage(['stack_error', 'illegal_instruction'])
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        self.check_log_alarm(illegal_instruction_error)

        for slice in self.slices:
            self.pattern_helper.verify_pattern_end_status(slice)

    def DirectedType2IllegalInstructionAlarmTest(self):
        """ Try to execute an illegal instruction; Verify that this alarm is raised when the following
        illegal instruction scenario is encountered in Pat Gen:
        An Instruction Word combines two operations in one Instruction -
        one regular operation such as an ALU Operation and a Branch Operation,
        both in the same Instruction, which is Illegal!
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        DriveZeroVectors length=1024
                        I optype=ALU, aluop=ALUOP.AND, opsrc=ALUSRC.FLAGS_I, opdest=ALUDEST.FLAGS, imm=0x00000000, dest=ALUDEST.FLAGS, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1     # ALU Op and Branch Op Combined into one Instruction 

                    LOOP:
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        Return
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        illegal_instruction_error = 'illegal_instruction'
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([illegal_instruction_error])
        self.env.hbicc.hbicc_alarms.set_auto_abort_alarms_as_non_critical_at_prestage(['stack_error', 'illegal_instruction'])

        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        self.check_log_alarm(illegal_instruction_error)

        for slice in self.slices:
            self.pattern_helper.verify_pattern_end_status(slice)

    @skip('RM recovery WIP; HDMT Ticket 113299')
    def DirectedEnableClockFullTest(self):
        """
        Reset enabled clocks too frequently (3k times)
        Frequent pcall optional; Pre-stage mode required
        """
        enable_clock_full = 'enable_clock_full'
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                NoCompareDriveZVectors length=1024
                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                NoCompareDriveZVectors length=1024
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=3000
                    LOOP:

                    S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                    DriveZeroVectors length=1

                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1
                Return                                       
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.alarm_trigger_and_check_in_prestage(enable_clock_full, pattern_string)

    @skip('RM recovery WIP; HDMT Ticket 113299')
    def DirectedActiveChannelFullTest(self):
        """
        Update the active channels too frequently (3k times)
        Avoid frequent pcall; Pre-stage mode required
        """
        active_channel_full = 'active_channel_full'
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                NoCompareDriveZVectors length=1024
                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                NoCompareDriveZVectors length=1024
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=3000
                    LOOP:

                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                    DriveZeroVectors length=1

                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1
                Return                                       
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.alarm_trigger_and_check_in_prestage(active_channel_full, pattern_string)

    @skip('RM recovery WIP; HDMT Ticket 113299')
    def DirectedDutSerialControlFullTest(self):
        """"
        Update active dut_serial too frequently (3k times)
        Avoid frequent pcall; Pre-stage mode required
        """
        dut_serial_control_full = 'dut_serial_control_full'
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                NoCompareDriveZVectors length=1024
                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                NoCompareDriveZVectors length=1024
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=3000
                    LOOP:

                    S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    DriveZeroVectors length=1

                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1
                Return                                       
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.alarm_trigger_and_check_in_prestage(dut_serial_control_full, pattern_string)

    @skip('RM recovery WIP; HDMT Ticket 113299')
    def DirectedApplyMaskFullTest(self):
        """
        Apply mask instruction too frequently (3k times)
        Frequent pcall required; Pre-stage mode required
        """
        apply_mask_full = 'apply_mask_full'
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                NoCompareDriveZVectors length=1024
                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                NoCompareDriveZVectors length=1024
                I optype=REGISTER, opsrc=REGSRC.IMM, opdest=REGDEST.REG, dest=0, imm=3000
                    LOOP:

                    S stype=MASK,                   data=0x0000ff000
                    PatternId 0x1
                    PCall PATTERN1

                    I optype=ALU, aluop=SUB, opsrc=ALUSRC.RA_I, regA=0, opdest=ALUDEST.REG, dest=0, imm=0x00000001
                    I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`LOOP`, cond=COND.ZERO, invcond=1
                Return                                       

                PATTERN1:                
                DriveZeroVectors length=1
                Return
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        self.alarm_trigger_and_check_in_prestage(apply_mask_full, pattern_string)

    def alarm_trigger_and_check_in_prestage(self, alarm_type, pattern_string):
        self.env.hbicc.set_up_fval_alarm_logging(self.slices)
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([alarm_type])
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario_without_burst_start()
        self.check_log_alarm(alarm_type)
        self.transceiver_line_recover_after_lockup()

    def transceiver_line_recover_after_lockup(self):
        self.pattern_helper.hbicc.abort_pattern(self.slices)
        self.env.hbicc.check_transceiver_paths()
        for i in range(12 // len(self.slices)):
            for psdb, pin0, pin1 in self.env.hbicc.paths:
                self.pattern_helper.reset_pm_fifo_to_flush_content(pin0, pin1)

    def DirectedIncorrectFirstFetchedAddressTest(self):
        """" Happy path test to check for the setting of incorrect_first_fetched_address alarm.

         Set start address, issue prestage toggle,change the start address.
         Alarm should fire.
         """

        start_address = random.getrandbits(32)
        slice = random.randint(0, 4)
        change_start_address = 0xdeadcafe
        self.pattern_helper.user_mode = 'LOW'
        self.pattern_helper.create_slice_channel_set_pattern_combo(slices=[slice])
        self.pattern_helper.hbicc.pat_gen.set_pattern_start_address(start_address, slice)
        self.pattern_helper.hbicc.pat_gen.toggle_pattern_prestage(self.pattern_helper.active_slice_list[0])
        self.pattern_helper.hbicc.pat_gen.set_pattern_start_address(change_start_address, slice)

        alarm_field = 'incorrect_first_fetched_address'
        self.check_log_alarm(alarm_field)
        self.pattern_helper.hbicc.abort_pattern([slice])

    def DirectedIllegalVectorFormatTest(self):
        """Verify that this alarm is raised when illegal word types (not V, M, S, I) are encountered in Pat Gen"""
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        invalid_pattern_word_types = [0b0000, 0b0001, 0b0010, 0b0011, 0b0100]
        random_invalid_pattern_word_type = random.choice(invalid_pattern_word_types)
        random_invalid_pattern_word = random_invalid_pattern_word_type << 124 | \
                                      random.randint(0, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
        pattern_string = f'''
                            PATTERN_START:
                                StartPattern
                                DriveZeroVectors length=32
                                RawPatternWord {hex(random_invalid_pattern_word)}
                                StopPattern {hex(self.pattern_helper.end_status)}
                                Return
                            '''
        self.Log('debug', f'{pattern_string} \nSlices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        illegal_vector_format = 'illegal_vector_format'
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([illegal_vector_format])
        self.pattern_helper.execute_pattern_scenario()
        self.check_log_alarm(illegal_vector_format)

    def DirectedWriteOnFullLastPcallAddrTest(self):
        """
        If you have way too many PCalls and do not have PatternId to go with those PCalls,
        you will get this alarm.
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        DriveZeroVectors length=1024
                        %repeat 3075
                            PCall PATTERN1     # PCall without a PatternId
                            DriveZeroVectors length=2000
                        %end

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        Return

                        PATTERN1:
                            DriveZeroVectors length=10000
                            Return
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        write_on_full_last_pcall_addr_error = 'write_on_full_last_pcall_addr'
        underrun_error = 'read_on_empty_last_pcall_addr'
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([underrun_error])
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([write_on_full_last_pcall_addr_error])

        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        self.check_log_alarm(write_on_full_last_pcall_addr_error)

        for slice in self.slices:
            self.pattern_helper.verify_pattern_end_status(slice)

    def DirectedReadOnEmptyLastPcallAddrTest(self):
        """
        If you have a PatternId that is NOT followed by a PCall, then this alarm is raised!
        Currently, it is ONLY reproducible if a 2nd PatternId follows the PatternId
        without the associated PCall.
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        id_0 = random.randint(0, ID_MAX_VALUE)
        pattern_string = f'''
                    PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        DriveZeroVectors length=1024
                        
                        PatternId {id_0}
                        V ctv=1, mtv=0, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHL
                        #V ctv=1, mtv=1, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLH
                        #V ctv=1, mtv=2, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHH
                        #V ctv=1, mtv=3, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHH
                        #DriveZeroVectors length=10000
                        
                        PatternId {id_0}
                        V ctv=1, mtv=4, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHH
                        #V ctv=1, mtv=5, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHH
                        #V ctv=1, mtv=6, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHH
                        #V ctv=1, mtv=7, lrpt=0, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHLHHHHHHH
                        #DriveZeroVectors length=10000
                        
                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1
                        Return
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        read_on_empty_last_pcall_addr_error = 'read_on_empty_last_pcall_addr'
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([read_on_empty_last_pcall_addr_error])

        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        self.check_log_alarm(read_on_empty_last_pcall_addr_error)

        for slice in self.slices:
            self.pattern_helper.verify_pattern_end_status(slice)

    # @skip('Not implemented')
    def DirectedWriteOnFullTraceIndexTest(self):
        """
        Comment from FPGA:
        This might be hard to reproduce. This will happen if you have too many back to back jumps.
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        # self.slices = [1]
        id_0 = random.randint(0, ID_MAX_VALUE)
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        self.pattern_helper.is_simulator_active = False
        pattern_string = f'''
                        PATTERN_START:
                        S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                        S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                        S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                        S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                        PatternId {id_0}
                        PCall PATTERN1

                        I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                        DriveZeroVectors length=1

                        PATTERN1:
                        I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm=10000
                        I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                        SUBR:
                            DriveZeroVectors length=1
                            I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                            # I optype=BRANCH, base=PFB, br=PCALL_I, imm=0, cond=ZERO, invcond=1   
                            # I optype=BRANCH, base=GLOBAL, br=GOTO_I, imm=`SUBR`, cond=COND.ZERO, invcond=1
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR], cond=ZERO, invcond=1
                            DriveZeroVectors length=100
                            Return 
                '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_ctv': 0}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        write_on_full_trace_index_error = 'write_on_full_trace_index'
        underrun_error = 'underrun'
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([underrun_error])
        self.env.hbicc.hbicc_alarms.set_alarms_as_non_critical([write_on_full_trace_index_error])

        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()

        self.check_log_alarm(write_on_full_trace_index_error)

        for slice in self.slices:
            self.pattern_helper.verify_pattern_end_status(slice)

    @skip('Not implemented')
    def DirectedReadOnEmptyTraceIndexTest(self):
        """
        Comment from FPGA:
        you can not reproduce this in fval.
        """

    def DirectedErrorPMCaptureOverflowTest(self):
        """Test to Check Error Stream Overflow Alarm Bits

        error stream data reaches error stream end address
        Need to check all 4 PM error stream alarm bits
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        self.pattern_helper.end_status = 0x1800dead
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}
                        
                NoCompareDriveZVectors length=1024
                Call PATTERN0

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN0:
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    CompareHighVectors length=1024
                    V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                Return
                        '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_fails': 1, 'capture_ctv': 0}
        address_map = {slice_index: {'pm_stream_end_addresses':
                       {0: 0x4c0000100, 1: 0x500000100, 2: 0x540000100, 3: 0x580000100}}
                       for slice_index in self.slices}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        self.pattern_helper.update_capture_start_and_end_address(address_map)
        self.pattern_helper.execute_pattern_scenario()
        self.check_four_pm_error_stream_overflow_alarm_bits()

    def check_four_pm_error_stream_overflow_alarm_bits(self):
        for i in range(4):
            error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(i)
            self.check_log_alarm(error_pm_x_capture_overflow)

    def check_log_alarm(self, alarm_bit, by_slice=False, expected_alarm_set_map={}):
        if not self.env.hbicc.hbicc_alarms.is_alarm_set({alarm_bit}, by_slice, expected_alarm_set_map):
            self.Log('error', f'Alarm was not set for {alarm_bit}')

    def DirectedCTVHeaderCaptureOverflowTest(self):
        """Test to check for ctv header stream overflow alarm

        The test:
        * First, set the pins to HIGH and randomly select one or multiple slices
        * Second, set the ctv mask to only capture 3 channels
        * Third, place ctv_header_num(multiple of 8) vectors in pattern
        * Fourth, set the ctv header end address so that on average every
          slice overflows(total ctv headers will be distributed across slices)
        * Fifth, execute the pattern and check the ctv overflow alarm both overall
            and by slice
        """
        for _ in range(0, self.test_repeat):
            if not self.unit_test:
                self.reset_ctv_header_test()
            self.pattern_helper.user_mode = 'HIGH'
            id_0 = random.randint(0, ID_MAX_VALUE)
            self.ctp = 0b00000000000000000001001000000000010
            repeat = self.ctv_header_num
            pattern_string = f'''
                PATTERN_START:
                    S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                       S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                       S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                       S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                       PatternId {id_0}
                       PCall PATTERN0

                       I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                       DriveZeroVectors length=1

                       PATTERN0:            
                           NoCompareDriveZVectors length=1024
                           I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]

                           SUBR:
                               ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

                           Return
            '''
            self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
            address_map = {slice_index: {'ctv_header_stream_end_address': CTV_HEADER_STREAM_START_ADDRESS
                           + self.ctv_header_num_limit * STREAM_BLOCK} for slice_index in self.slices}
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
            self.pattern_helper.update_capture_start_and_end_address(address_map)
            self.pattern_helper.execute_pattern_scenario()
            self.pattern_helper.check_ctv_header_capture_size(self.ctv_header_num)
            expected_ctv_header_overflow_map = \
                self.pattern_helper.get_expected_ctv_header_overflow_map(self.ctv_header_num, self.ctv_header_num_limit)
            self.check_log_alarm(alarm_bit='ctv_header_capture_overflow',
                                 by_slice=True,
                                 expected_alarm_set_map=expected_ctv_header_overflow_map)


    def reset_ctv_header_test(self):
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.end_status = random.getrandbits(32)
        self.pattern_helper.is_simulator_active = True
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))
        self.slices.sort()
        self.ctv_header_num_limit = \
            random.randrange(self.MIN_HEADER_BLOCK_NUM_LIMIT, self.MAX_BLOCK_NUM_LIMIT, self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE)
        self.ctv_header_num = self.ctv_header_num_limit * len(self.slices)

    def DirectedCTVRowCaptureOverflowTest(self):
        """Test to check for ctv data stream overflow alarm

        The test:
        * First, set the pins to HIGH and select a random slice
        * Second, set the ctv mask to only capture 3 channels
        * Third, place (capture_block_num * 8) vectors in pattern
        * Fourth, set the ctv stream end address so that it can contain
            exactly the same amount of ctv block generated by the pattern
        * Fifth, execute the pattern and check the ctv overflow alarm
        """
        for _ in range(self.test_repeat):
            if not self.unit_test:
                self.reset_ctv_data_test()
            self.pattern_helper.user_mode = 'HIGH'
            id_0 = random.randint(0, ID_MAX_VALUE)
            self.ctp = 0b00000000000000000001001000000000010
            repeat = (self.ctv_data_num - CTV_DATA_END_OF_BURST_BLOCK_NUM_PER_PM) * self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE
            pattern_string = f'''
                            PATTERN_START:
                            S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                            S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                            S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                            S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                            PatternId {id_0}
                            PCall PATTERN0

                            I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                            DriveZeroVectors length=1

                            PATTERN0:            
                            NoCompareDriveZVectors length=1024
                            I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                            SUBR:
                                ConstantVectors length={repeat}, ctv=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
                            Return
                                    '''
            self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
            address_map = {slice_index: {'ctv_data_stream_end_address': CTV_DATA_STREAM_START_ADDRESS
                           + (self.ctv_data_num_limit * CTV_DATA_STREAM_BLOCK_SIZE)} for slice_index in self.slices}
            self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices)
            self.pattern_helper.update_capture_start_and_end_address(address_map)
            self.pattern_helper.execute_pattern_scenario()
            self.check_log_alarm(alarm_bit='ctv_row_capture_overflow',
                                 by_slice=True,
                                 expected_alarm_set_map={slice_index: True for slice_index in self.slices})

    def reset_ctv_data_test(self):
        self.pattern_helper = PatternHelper(self.env)
        self.pattern_helper.end_status = random.getrandbits(32)
        self.pattern_helper.is_simulator_active = True
        population = set(SLICECOUNT)
        self.slices = random.sample(population, random.randint(1, len(SLICECOUNT)))
        self.slices.sort()
        self.ctv_data_num = random.randrange(self.MIN_CTV_DATA_NUM, self.MAX_CTV_DATA_NUM, self.VECTOR_PER_CTV_DATA_BLOCK_ENTRY_IS_WIDE)
        self.ctv_data_num_limit = self.ctv_data_num


    def DirectedTraceCaptureOverflowTest(self):
        """Test to Check Trace Capture Overflow Alarm Bit

        trace stream data reaches trace stream end address
        Need to check both alarm bit and data overwrite protection
        """
        self.pattern_helper.user_mode = 'HIGH'
        self.slices = [random.randint(0, 4)]
        id_0 = random.randint(0, 100)
        self.pattern_helper.is_simulator_active = False
        self.pattern_helper.is_capture_active = False
        self.pattern_helper.end_status = 0x1800dead
        pattern_string = f'''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                S stype=CAPTURE_PIN_MASK,                           data=0b{self.ctp:035b}

                PatternId {id_0}
                PCall PATTERN1

                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm={self.pattern_helper.end_status}
                DriveZeroVectors length=1

                PATTERN1:
                I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=1, imm=100
                I optype=BRANCH, br=GOTO_I, imm=eval[SUBR]
                SUBR:
                    DriveZeroVectors length=1
                    I optype=ALU, opdest=ALUDEST.REG, opsrc=ALUSRC.RA_I, aluop=SUB, dest=1, regA=1, imm=1
                    I optype=BRANCH, br=GOTO_I, imm=eval[SUBR], cond=ZERO, invcond=1
                DriveZeroVectors length=100
                Return 
                        '''
        self.Log('debug', f'{pattern_string} \n Slices: {self.slices}')
        capture_attributes = {'capture_fails': 1, 'capture_ctv': 0}
        address_map = {slice_index: {'trace_stream_end_address': 0x480000100} for slice_index in self.slices}
        self.pattern_helper.create_slice_channel_set_pattern_combo(pattern_string, slices=self.slices,
                                                                   attributes=capture_attributes)
        self.pattern_helper.update_capture_start_and_end_address(address_map)
        self.pattern_helper.set_ctp(self.ctp, self.slices)
        self.pattern_helper.execute_pattern_scenario()
        trace_capture_overflow = {'trace_capture_overflow'}
        if not self.env.hbicc.hbicc_alarms.is_alarm_set(trace_capture_overflow):
            self.Log('error', f'Alarm was not set for {trace_capture_overflow}')

        for slice in self.slices:
            self.pattern_helper.verify_pattern_end_status(slice)
