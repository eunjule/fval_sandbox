# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.


from CaptureFileUtilities import CaptureFileUtilities
from CaptureDecodeUtilities import *
from CaptureExpectUtilities import CTVExpectBuilder
from CaptureExpectUtilities import CaptureDecodeExecuter
from CaptureEncodeUtilities import CaptureEncodeUtilities
from stopwatch import StopWatch

import os

from pathlib import Path
from sys import argv, exit


def run_program(args):
    # #---------------
    # # Preliminaries
    # #---------------
    #
    # fval_usage_example ( )
    #
    # return 0

    if len(args) == 0 > len(args) > 2:
        print("    Usage : CaptureDecoder.py filename [-p]")
        return 1

    outputs = parse_arguments(args)

    if outputs == None:
        show_usage()
        return 1

    filename = args[0]
    filename = os.path.abspath(filename)

    if not Path(filename).is_file():
        print("    File [ {0} ] not found.".format(filename))
        return 1

    print("    Decoding [ {0} ]".format(filename))

    print()

    sw = StopWatch.start_new()

    from_packets = len(args) == 2;

    from_packets = False

    cfu = CaptureFileUtilities(filename)

    if "Binary" in outputs:

        print("        Generating fileset...")

        if not from_packets:
            cfu.generate_fileset_from_dma()

        else:
            cfu.generate_fileset_from_packets()
            # cfu.generate_fileset_from_bin ( )

    dict_bytes = cfu.read_bin_dictionary(not from_packets, True)

    cdu = CaptureDecodeUtilities(cfu, dict_bytes)

    cdu.memos_on()

    if "Raw" in outputs:
        print("        Generating raw header decode csv...")
        cdu.generate_raw_ctv_header_decode(stream=CTV_DATA if not from_packets else UNSPECIFIED)

    cdu.use_ctv_headers()
    cdu.use_traces()

    if "Full" in outputs:
        print("        Generating unpacked ctv data csv...")
        cdu.generate_unpacked_ctv_data(stream=CTV_DATA if not from_packets else UNSPECIFIED, skip_eob=True,
                                       skip_invalid=False, chip_break=True, block_break=True, cycle_break=False)

    if "Compare" in outputs:
        print("        Generating actual compare ctv data csv...")
        actual_file_name = cdu.generate_unpacked_actual_ctv_data(stream=CTV_DATA if not from_packets else UNSPECIFIED)

    if "Wide" in outputs:
        print("        Generating wide header csv...")
        cdu.generate_wide_capture_header_decode()

    if "Trace" in outputs:
        print("        Generating trace stream csv...")
        cdu.generate_trace_stream_decode()

    ##ceb = CTVExpectBuilder ( filename )

    ##ceb.start ( )

    ##pattern = [ 0, 1, 2, 3, 4, 5, 6, 7, 7, 6, 5, 4, 3, 2, 1, 0, 6, 6, 4, 4, 2, 2, 0, 0, 0, 0, 3, 3, 1, 1, 2, 2 ]

    ##for i in range ( 1024, 31040 ):
    ##    v = pattern [ i % 32 ]
    ##    ceb.add_cycle ( i, 0x3800, v, v, v, v )

    ##ceb.done ( )

    ##matches = ceb.compare_slice ( actual_file_name, 0 )

    ##print ( "        Expect vs actual compare {0}...".format ( "PASSES" if matches else "FAILS" ) )

    ##--------------------
    ## Fake file creation
    ##--------------------

    ##print ( "        Creating FakeFile.txt..." )

    ##ceu = CaptureEncodeUtilities ( )

    ##count = 1024 * 1024 - 8

    ##with open ( "fakefile.txt", "w" ) as f:
    ##    ceu.write_ctv_header_section ( f, count )
    ##    ceu.write_ctv_data_section ( f, count )
    ##    ceu.write_other_sections ( f )

    # BinStringRepresentation.memo = { }

    # with open ( "bsr.txt", "w" ) as f:

    #    for i in range ( 20 * 1024 * 1024 ):
    #        bsr = BinStringRepresentation ( 35, i // 512, "0" )
    #        f.write ( bsr.to_string ( ) + "\n" )

    print()
    print("    Done.")

    elapsed = sw.elapsed_seconds()

    print()
    print("{0:,.2f} s".format(elapsed))

    return 0


def parse_arguments(args):
    if len(args) == 0 or len(args) > 2:
        return None;

    valid_chars = "wtrfcba"

    result = set(["Binary"])

    if len(args) < 2:
        return None;

    if not args[1].startswith("-"):
        return None;

    work = args[1][1:]

    for c in work:
        if not c in valid_chars:
            return None

    if 'a' in work:
        work = valid_chars

    for c in work:

        if c == 'w':
            result.add("Wide")

        elif c == 't':
            result.add("Trace")

        elif c == 'r':
            result.add("Raw")

        elif c == 'f':
            result.add("Full")

        elif c == 'c':
            result.add("Compare")

        elif c == 'b':
            result.add("Binary")

    return result


def show_usage():
    print("    Usage : CaptureDecoder.py filename [-[w][t][r][f][c][b][a]]");
    print();
    print("        w     Generate wide header CSV.");
    print("        t     Generate trace stream CSV.");
    print("        r     Generate raw header decode CSV.");
    print("        f     Generate full capture decode CSV.");
    print("        c     Generate compare capture decode CSV.");
    print("        b     Generate binary files (implicit in other options).");
    print("        a     Generate all file types.");
    print();
    print("        Example: CaptureDecoder.py streamfile.txt -wtr");


def fval_usage_example():
    filename = "WorkFile.txt"  # One slice's dma streams

    # --------------------
    # Set up expect data
    # --------------------

    ceb = CTVExpectBuilder(filename)

    ceb.set_slices([0, 1, 2, 3, 4])

    ceb.set_chips([0, 1])

    ceb.start()

    pattern = [0, 1, 2, 3, 4, 5, 6, 7, 7, 6, 5, 4, 3, 2, 1, 0, 6, 6, 4, 4, 2, 2, 0, 0, 0, 0, 3, 3, 1, 1, 2, 2]

    for i in range(1024, 31040):
        v = pattern[i % 32]
        ceb.add_cycle(i, 0x3800, v, v, v, v)

    ceb.done()

    # ---------------------
    # Decode the captures
    # ---------------------

    cde = CaptureDecodeExecuter(filename)

    actual_file_name = cde.generate_ctv_decode()

    # ------------------------------------
    # Compare expect data to actual data
    # ------------------------------------

    passing = ceb.compare_slice(actual_file_name, slice=0)


def fval_usage_example_old():
    filename = "WorkFile.txt"  # One slice's dma streams

    # --------------------
    # Set up expect data
    # --------------------

    ceb = CTVExpectBuilder(filename)

    ceb.set_slices([0, 1, 2, 3, 4])

    ceb.set_chips([0, 1])

    ceb.start()

    pattern = [0, 1, 2, 3, 4, 5, 6, 7, 7, 6, 5, 4, 3, 2, 1, 0, 6, 6, 4, 4, 2, 2, 0, 0, 0, 0, 3, 3, 1, 1, 2, 2]

    for i in range(1024, 31040):
        v = pattern[i % 32]
        ceb.add_cycle(i, 0x3800, v, v, v, v)

    ceb.done()

    # ---------------------
    # Decode the captures
    # ---------------------

    cfu = CaptureFileUtilities(filename)
    cfu.generate_fileset_from_dma()

    cdu = CaptureDecodeUtilities(cfu)

    cdu.memos_on()  # This will either speed things up or blow up memory, depending

    cdu.use_ctv_headers()
    cdu.use_traces()

    # --------------------------
    # Below items are optional
    # --------------------------

    cdu.generate_raw_ctv_header_decode()

    cdu.generate_unpacked_ctv_data(polarity=0, skip_eob=True, skip_invalid=False, chip_break=True, block_break=True,
                                   cycle_break=False)

    cdu.generate_wide_capture_header_decode()

    cdu.generate_trace_stream_decode()

    # ---------------------------------------------
    # Have to do this to have file of actual data
    # ---------------------------------------------

    actual_file_name = cdu.generate_unpacked_actual_ctv_data(polarity=0)

    # ------------------------------------
    # Compare expect data to actual data
    # ------------------------------------

    passing = ceb.compare_slice(actual_file_name, slice=0)


# def read_all_lines ( file_name ) :
#    with open ( file_name ) as f :
#        result = [ line.strip ( '\r\n' ) for line in f ]
#        return result


# def write_all_lines ( file_name, lines ) :
#    with open ( file_name + ".out" , 'w' ) as f :
#        for line in lines :
#            print ( line, file=f )


# def distinct ( sequence ) :
#    seen = set ( )
#    for s in sequence :
#        if not s in seen :
#            seen.add ( s )
#            yield s


# def _distinct ( sequence ) :
#    return list ( distinct ( sequence ) )


# def index_of ( sequence, value = None, predicate = None ) :

#    if value :
#        for i, s in enumerate ( sequence ) :
#            if ( s == value ) :
#                return i
#        return None

#    if predicate :
#        for i, s in enumerate ( sequence ) :
#            if ( predicate ( s ) ) :
#                return i
#        return None


print()

exit_code = run_program(argv[1:])

print()

exit(exit_code)
