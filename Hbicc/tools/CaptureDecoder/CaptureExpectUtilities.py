# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import os

from pathlib import Path


class CTVExpectBuilder:

    def __init__(self, filename):
        self.filename = filename
        self.fullpath = os.path.abspath(filename)
        self.fullstem = os.path.splitext(self.fullpath)[0]
        self.decoded_folder = Path(Path(self.fullpath).parent, Path("DecodedFolder"))
        self.decodes_folder = Path(Path(self.fullpath).parent, Path("DecodedFolder"), Path("Decodes"))
        self.fullstem_decode = Path(self.decodes_folder, Path(self.fullstem).name)

        self.slices = [0]
        self.chips = [0, 1]

        self.slice_files = [[None for i in range(4)] for i in range(5)]

    def set_slices(self, slices):
        self.slices = slices

    def set_chips(self, chips):
        self.chips = chips

    def start(self):

        if not self.decoded_folder.is_dir():
            os.mkdir(self.decoded_folder)

        if not self.decodes_folder.is_dir():
            os.mkdir(self.decodes_folder)

        for slice in self.slices:
            for chip in self.chips:
                filename = "{0}_expected_ctv_data_{1}_{2}.csv".format(str(self.fullstem_decode), slice, chip)
                self.slice_files[slice][chip] = open(filename, "w")

    def add_cycle(self, cycle_number, ctp_mask_value, cs0, cs1, cs2, cs3):

        for slice in self.slices:
            for chip in self.chips:
                f = self.slice_files[slice][chip]

                s = "{0},{1},0x{2:x},{3},{4},{5},{6},{7}\n".format(chip, slice, ctp_mask_value, cycle_number, cs0, cs1,
                                                                   cs2, cs3)

                f.write(s)

    def done(self):

        for slice in self.slices:
            for chip in self.chips:
                self.slice_files[slice][chip].close()

        for slice in self.slices:

            out_filename = "{0}_expected_ctv_data_{1}.csv".format(str(self.fullstem_decode), slice)

            with open(out_filename, "w") as f_out:
                for chip in self.chips:

                    in_filename = "{0}_expected_ctv_data_{1}_{2}.csv".format(str(self.fullstem_decode), slice, chip)

                    with open(in_filename, "r") as f_in:
                        for line in f_in:
                            f_out.write(line)

                    os.remove(in_filename)

    def compare_slice(self, filename, slice):

        in_filename = "{0}_expected_ctv_data_{1}.csv".format(str(self.fullstem_decode), slice)

        with open(in_filename, "r") as f_expect:
            with open(filename, "r") as f_actual:

                while (True):

                    line_expect = f_expect.readline()
                    line_actual = f_actual.readline()

                    if not (line_expect or line_actual):
                        return True

                    if not line_expect and line_actual:
                        return False

                    if line_expect != line_actual:
                        return False


class ErrorExpectBuilder:

    def __init__(self, filename):
        self.filename = filename
        self.fullpath = os.path.abspath(filename)
        self.fullstem = os.path.splitext(self.fullpath)[0]
        self.decoded_folder = Path(Path(self.fullpath).parent, Path("DecodedFolder"))
        self.decodes_folder = Path(Path(self.fullpath).parent, Path("DecodedFolder"), Path("Decodes"))
        self.fullstem_decode = Path(self.decodes_folder, Path(self.fullstem).name)

        self.slices = [0]
        self.chips = [0, 1]

        self.slice_files = [[None for i in range(4)] for i in range(5)]

    def set_slices(self, slices):
        self.slices = slices

    def set_chips(self, chips):
        self.chips = chips

    def start(self):

        if not self.decoded_folder.is_dir():
            os.mkdir(self.decoded_folder)

        if not self.decodes_folder.is_dir():
            os.mkdir(self.decodes_folder)

        for slice in self.slices:
            for chip in self.chips:
                filename = "{0}_expected_error_data_{1}_{2}.csv".format(str(self.fullstem_decode), slice, chip)
                self.slice_files[slice][chip] = open(filename, "w")

    def add_cycle(self, cycle_number, p0, e0, p1, e1, p2, e2, p3, e3):

        for slice in self.slices:
            for chip in self.chips:
                f = self.slice_files[slice][chip]

                s = "{0},{1},1,{2},{3},{4},{5},{6},{7},{8},{9},{10}\n".format(chip, slice, cycle_number, p0, e0, p1, e1,
                                                                              p2, e2, p3, e3)

                f.write(s)

    def done(self):

        for slice in self.slices:
            for chip in self.chips:
                self.slice_files[slice][chip].close()

        for slice in self.slices:

            out_filename = "{0}_expected_error_data_{1}.csv".format(str(self.fullstem_decode), slice)

            with open(out_filename, "w") as f_out:
                for chip in self.chips:

                    in_filename = "{0}_expected_error_data_{1}_{2}.csv".format(str(self.fullstem_decode), slice, chip)

                    with open(in_filename, "r") as f_in:
                        for line in f_in:
                            f_out.write(line)

                    os.remove(in_filename)

    def compare_slice(self, filename, slice):

        in_filename = "{0}_expected_ctv_data_{1}.csv".format(str(self.fullstem_decode), slice)

        with open(in_filename, "r") as f_expect:
            with open(filename, "r") as f_actual:

                while (True):

                    line_expect = f_expect.readline()
                    line_actual = f_actual.readline()

                    if not (line_expect or line_actual):
                        return True

                    if not line_expect and line_actual:
                        return False

                    if line_expect != line_actual:
                        return False


class CaptureDecodeExecuter:

    def __init__(self, filename):
        self.filename = filename
        self.filename_folder = os.path.dirname(filename)

    def generate_ctv_decode(self):
        cwd = os.getcwd()
        file_dir = os.path.dirname(os.path.abspath(__file__))
        cmd = '{0}\CaptureDecoder {1} -c'.format(file_dir, self.filename)
        os.system(cmd)

        output_file = None
        just_created_file = os.path.join(cwd, 'DecodedFileJustWritten.txt')
        with open(just_created_file, 'r') as f:
            output_file = f.readline()
            output_file = output_file.strip('\r\n')

        return output_file
