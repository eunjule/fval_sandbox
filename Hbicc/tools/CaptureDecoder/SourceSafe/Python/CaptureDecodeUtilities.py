# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import array
import os
from io import StringIO
from pathlib import Path

from CaptureUnpackUtilities import CycleUnpacker, BinStringRepresentation

CW_ERROR = 0
CW_CTV = 1

TRACE = "TRACE"
CTV_HEADER = "CTV_HEADER"
CTV_DATA = "CTV_DATA"
PSDB_0_PIN_0 = "PSDB_0_PIN_0"
PSDB_0_PIN_1 = "PSDB_0_PIN_1"
PSDB_1_PIN_0 = "PSDB_1_PIN_0"
PSDB_1_PIN_1 = "PSDB_1_PIN_0"
UNSPECIFIED = ""


class StringIOFixed(StringIO):

    def __int__(self):
        super().__init()

    def writeline(self, s=None):
        if s != None:
            self.write(s)

        self.write('\n')


class CommonHeader:

    def __init__(self):
        self.packet_index = 0
        self.capture_word_type = CW_ERROR
        self.originating_chip = 0
        self.originating_slice = 0


class RawCTVDataHeader:

    def __init__(self):

        self.packet_index = 0
        self.capture_word_type = CW_ERROR
        self.originating_chip = 0
        self.originating_slice = 0
        self.is_cycle_count = 0
        self.full_cycle_count = 0
        self.end_of_burst = 0
        self.entry_is_wide = 0
        self.valid_cycles = 0
        self.relative_cycle_count = 0
        self.block_count = 0

    def get_dump_text(self, indent_level=0, lines_sep=0):

        sw = StringIOFixed()

        fill = " " * (indent_level * 4)

        for i in range(lines_sep):
            sw.writeline()

        sw.writeline("{0}RawCTVHeader".format(fill))
        sw.writeline()
        sw.writeline("{0}    packet_index         : {1:10}".format(fill, self.packet_index))
        sw.writeline("{0}    capture_word_type    : {1:10}".format(fill, self.capture_word_type))
        sw.writeline("{0}    originating_chip     : {1:10}".format(fill, self.originating_chip))
        sw.writeline("{0}    originating_slice    : {1:10}".format(fill, self.originating_slice))
        sw.writeline("{0}    is_cycle_count       : {1:10}".format(fill, self.is_cycle_count))

        if self.is_cycle_count:
            sw.writeline("{0}    full_cycle_count     : {1:10}".format(fill, self.full_cycle_count))

        else:
            sw.writeline("{0}    end_of_burst         : {1:10}".format(fill, self.end_of_burst))
            sw.writeline("{0}    entry_is_wide        :       0x{1:02x}".format(fill, self.entry_is_wide))
            sw.writeline("{0}    valid_cycles         : 0x{1:08x}".format(fill, self.valid_cycles))
            sw.writeline("{0}    relative_cycle_count : {1:10}".format(fill, self.relative_cycle_count))
            sw.writeline("{0}    block_count          : {1:10}".format(fill, self.block_count))

        return sw.getvalue()

    def get_cycle_corrected_dump_text(self, full_cycle_count, indent_level=0, lines_sep=0):

        sw = StringIOFixed()

        fill = " " * (indent_level * 4)

        resultant_cycle_count = full_cycle_count & ~ 0x1FF

        resultant_cycle_count |= self.relative_cycle_count;

        for i in range(lines_sep):
            sw.writeline()

        sw.writeline("{0}CycleCorrectedCTVHeader".format(fill))
        sw.writeline()
        sw.writeline("{0}    packet_index         : {1:10}".format(fill, self.packet_index))
        sw.writeline("{0}    capture_word_type    : {1:10}".format(fill, self.capture_word_type))
        sw.writeline("{0}    originating_chip     : {1:10}".format(fill, self.originating_chip))
        sw.writeline("{0}    originating_slice    : {1:10}".format(fill, self.originating_slice))

        sw.writeline("{0}    end_of_burst         : {1:10}".format(fill, self.end_of_burst))
        sw.writeline("{0}    entry_is_wide        :       0x{1:02x}".format(fill, self.entry_is_wide))
        sw.writeline("{0}    valid_cycles         : 0x{1:08x}".format(fill, self.valid_cycles))
        sw.writeline("{0}    cycle_count          : {1:10}".format(fill, resultant_cycle_count))
        sw.writeline("{0}    block_count          : {1:10}".format(fill, self.block_count))

        return sw.getvalue()

    def get_csv_line(self):

        result = "{0},{1},{2},{3},{4},{5},{6},0x{7:02x},0x{8:08x},{9},{10}\n".format(self.packet_index,
                                                                                     self.capture_word_type,
                                                                                     self.originating_chip,
                                                                                     self.originating_slice,
                                                                                     self.is_cycle_count,
                                                                                     self.full_cycle_count,
                                                                                     self.end_of_burst,
                                                                                     self.entry_is_wide,
                                                                                     self.valid_cycles,
                                                                                     self.relative_cycle_count,
                                                                                     self.block_count)
        return result

    def get_cycle_corrected_csv_line(self, full_cycle_count):

        resultant_cycle_count = full_cycle_count & ~ 0x1FF

        resultant_cycle_count |= self.relative_cycle_count;

        result = "{0},{1},{2},{3},{4},0x{5:02x},0x{6:08x},{7},{8}\n".format(self.packet_index,
                                                                            self.capture_word_type,
                                                                            self.originating_chip,
                                                                            self.originating_slice,
                                                                            self.end_of_burst,
                                                                            self.entry_is_wide,
                                                                            self.valid_cycles,
                                                                            resultant_cycle_count,
                                                                            self.block_count)
        return result


class WideCaptureHeader:

    def __init__(self):
        self.packet_index = 0
        self.pattern_counter = 0
        self.trace_index = 0
        self.reserved = 0
        self.user_cycle_count = 0
        self.user_log1 = 0
        self.repeat_cycle_count = 0
        self.pattern_cycle_count = 0
        self.vector_address = 0
        self.last_pcall_address = 0

    def get_csv_line(self):
        result = "{0},{1},{2},{3},{4},{5},{6},0x{7:08x},0x{8:08x}\n".format(self.packet_index,
                                                                            self.pattern_counter,
                                                                            self.trace_index,
                                                                            self.user_cycle_count,
                                                                            self.user_log1,
                                                                            self.repeat_cycle_count,
                                                                            self.pattern_cycle_count,
                                                                            self.vector_address,
                                                                            self.last_pcall_address)
        return result


class TraceEntry:

    def __init__(self):
        self.packet_index = 0
        self.reserved_2a = 0
        self.reserved_2b = 0
        self.pattern_id = 0
        self.jump_type = 0
        self.reserved_1 = 0
        self.capture_this_pin = 0
        self.to_address = 0
        self.from_address = 0

    def get_csv_line(self):
        jump_types = {0: "none", 1: "goto_i", 2: "call_i", 4: "pcall_i", 8: "ret", 17: "goto_r", 18: "call_r",
                      20: "pcall_r", 24: "clrstk"}

        result = "{0},{1},{2},0x{3:09x},0x{4:08x},0x{5:08x}\n".format(self.packet_index,
                                                                      self.pattern_id,
                                                                      jump_types[self.jump_type],
                                                                      self.capture_this_pin,
                                                                      self.to_address,
                                                                      self.from_address)
        return result


class CaptureDecodeUtilities:

    def __init__(self, cfu_instance, dict_bytes=None):

        self.cfu_instance = cfu_instance
        self.dict_bytes = dict_bytes
        self.decode_folder = Path(Path(cfu_instance.decode_folder), Path("Decodes"))
        self.fullstem_decode = Path(self.decode_folder, Path(cfu_instance.fullstem).name)
        self.ctv_header_cycles = None
        self.ctv_header_traces = None
        self.traces = None

        if dict_bytes == None:
            self.dict_bytes = cfu_instance.read_bin_dictionary(True, True)

    def get_packet_count(self, stream):

        self.last_stream = stream
        return (len(self.dict_bytes[stream]) // 32)

    def extract_packet(self, index, stream=None):

        workstream = self.last_stream if stream == None else stream
        work_array = self.dict_bytes[workstream][index * 32: (index + 1) * 32]
        work_array = list(reversed(work_array))
        self.last_packet = work_array
        self.last_index = index
        return work_array

    def extract_64_from_packet(self, index, packet=None):

        workpacket = self.last_packet if packet == None else packet
        work_array = workpacket[index * 8: (index + 1) * 8]
        return work_array

    def extract_int64_from_packet(self, index, packet=None):

        work_array = self.extract_64_from_packet(index, packet)

        result = 0

        for i in range(8):
            result = result << 8 | work_array[i]

        return result

    def extract_32_from_packet(self, index, packet=None):

        workpacket = self.last_packet if packet == None else packet
        work_array = workpacket[index * 4: (index + 1) * 4]
        return work_array

    def extract_int32_from_packet(self, index, packet=None):

        work_array = self.extract_32_from_packet(index, packet)

        result = 0

        for i in range(4):
            result = result << 8 | work_array[i]

        return result

    def extract_24_from_packet(self, index, packet=None):

        workpacket = self.last_packet if packet == None else packet
        work_array = workpacket[index * 3: (index + 1) * 3]
        return work_array

    def extract_int24_from_packet(self, index, packet=None):

        work_array = self.extract_24_from_packet(index, packet)

        result = 0

        for i in range(3):
            result = result << 8 | work_array[i]

        return result

    def get_common_header(self, packet=None):

        workpacket = self.last_packet if packet == None else packet

        cfe = CaptureFieldExtractor(workpacket)

        result = CommonHeader()

        result.packet_index = self.last_index if packet == None else -1

        result.capture_word_type = cfe.extract(2)
        result.originating_chip = cfe.extract(2)
        result.originating_slice = cfe.extract(3)

        return result

    def get_raw_ctv_data_header(self, packet=None):

        workpacket = self.last_packet if packet == None else packet

        cfe = CaptureFieldExtractor(workpacket)

        result = RawCTVDataHeader()

        result.packet_index = self.last_index if packet == None else -1

        result.capture_word_type = cfe.extract(2)
        result.originating_chip = cfe.extract(2)
        result.originating_slice = cfe.extract(3)
        result.is_cycle_count = cfe.extract(1)

        if result.is_cycle_count == 1:
            result.full_cycle_count = cfe.extract(48)
            cfe = CaptureFieldExtractor(workpacket)
            cfe.extract(8)

        # else:
        result.end_of_burst = cfe.extract(1)
        result.entry_is_wide = cfe.extract(8)
        result.valid_cycles = cfe.extract(32)
        result.relative_cycle_count = cfe.extract(9)
        result.block_count = cfe.extract(6)

        return result

    def get_wide_capture_header(self, packet=None):

        workpacket = self.last_packet if packet == None else packet

        cfe = CaptureFieldExtractor(workpacket)

        result = WideCaptureHeader()

        result.packet_index = self.last_index if packet == None else -1

        result.pattern_counter = cfe.extract(24)
        result.trace_index = cfe.extract(24)
        result.reserved = cfe.extract(16)
        result.user_cycle_count = cfe.extract(32)
        result.user_log1 = cfe.extract(32)
        result.repeat_cycle_count = cfe.extract(32)
        result.pattern_cycle_count = cfe.extract(32)
        result.vector_address = cfe.extract(32)
        result.last_pcall_address = cfe.extract(32)

        return result

    def get_trace_entry(self, packet=None):

        workpacket = self.last_packet if packet == None else packet

        cfe = CaptureFieldExtractor(workpacket)

        result = TraceEntry()

        result.packet_index = self.last_index if packet == None else -1

        result.reserved_2a = cfe.extract(64)
        result.reserved_2b = cfe.extract(32)
        result.pattern_id = cfe.extract(32)
        result.jump_type = cfe.extract(5)
        result.reserved_1 = cfe.extract(21)
        result.capture_this_pin = cfe.extract(38)
        result.to_address = cfe.extract(32)
        result.from_address = cfe.extract(32)

        return result

    def extract_user_cycle_count(self, packet):

        result = self.extract_int32_from_packet(2, packet)

        return result

    def extract_trace_index(self, packet):

        result = self.extract_int24_from_packet(1, packet)

        return result

    def use_ctv_headers(self):

        packet_count = self.get_packet_count(CTV_HEADER)

        self.ctv_header_cycles = array.array('i', (0 for i in range(packet_count)))
        self.ctv_header_traces = array.array('i', (0 for i in range(packet_count)))

        for i in range(packet_count):
            packet = self.extract_packet(i)
            cycle_number = self.extract_user_cycle_count(packet)
            trace_index = self.extract_trace_index(packet)
            self.ctv_header_cycles[i] = cycle_number
            self.ctv_header_traces[i] = trace_index

        return

    def use_traces(self):

        packet_count = self.get_packet_count(TRACE)

        self.traces = []

        for i in range(packet_count):
            packet = self.extract_packet(i)
            trace_entry = self.get_trace_entry()
            self.traces.append(trace_entry)

        return

    def clear_memos(self):

        if CaptureFieldExtractor.memo != None:
            CaptureFieldExtractor.memo.clear()

        if CycleUnpacker.memo != None:
            CycleUnpacker.memo.clear()

        if BinStringRepresentation.memo != None:
            BinStringRepresentation.memo.clear()

    def memos_on(self):

        CaptureFieldExtractor.memo = {}
        CycleUnpacker.memo = {}
        BinStringRepresentation.memo = {}

    def memos_off(self):

        CaptureFieldExtractor.memo = None
        CycleUnpacker.memo = None
        BinStringRepresentation.memo = None

    def generate_raw_ctv_header_decode(self, stream=CTV_DATA):

        for i in range(1, 2):

            filename = str(self.fullstem_decode) + (
                "_raw_ctv_data_headers.txt" if i == 0 else "_raw_ctv_data_headers.csv")

            if not self.decode_folder.is_dir():
                os.mkdir(self.decode_folder)

            with open(filename, "w") as f:

                self.clear_memos()

                if (i == 1):
                    print(
                        "index,type,chip,slice,is_count,full_count,eob,wide_mask,valid_cycles,relative_count,block_count,blank,data0_H,data0_L,data1_H,data1_L,data2_H,data2_L,,delta,delta_correct,,mod_4",
                        file=f)

                packet_count = self.get_packet_count(stream)
                current_index = 0

                relative_cycles = [-1, -1, -1, -1]
                cyle_count = [-1, -1, -1, -1]

                while current_index < packet_count:

                    packet = self.extract_packet(current_index)

                    common_header = self.get_common_header()

                    if common_header.capture_word_type == CW_ERROR:
                        current_index += 2;
                        continue

                    raw_ctv_header = self.get_raw_ctv_data_header()

                    if i == 0:
                        s = raw_ctv_header.get_dump_text(1, 2 if current_index == 0 else 4)

                    else:
                        s = raw_ctv_header.get_csv_line().strip()
                        s = "{0},,0x{1:08x},0x{2:08x},0x{3:08x},0x{4:08x},0x{5:08x},0x{6:08x}".format(s,
                                                                                                      self.extract_int64_from_packet(
                                                                                                          1) >> 32,
                                                                                                      self.extract_int64_from_packet(
                                                                                                          1) & 0xFFFFFFFF,
                                                                                                      self.extract_int64_from_packet(
                                                                                                          2) >> 32,
                                                                                                      self.extract_int64_from_packet(
                                                                                                          2) & 0xFFFFFFFF,
                                                                                                      self.extract_int64_from_packet(
                                                                                                          3) >> 32,
                                                                                                      self.extract_int64_from_packet(
                                                                                                          3) & 0xFFFFFFFF)

                        delta = 0

                        if raw_ctv_header.is_cycle_count:
                            delta = 512 if cyle_count[
                                               raw_ctv_header.originating_chip] < 0 else raw_ctv_header.full_cycle_count - \
                                                                                         cyle_count[
                                                                                             raw_ctv_header.originating_chip]
                            cyle_count[raw_ctv_header.originating_chip] = raw_ctv_header.full_cycle_count

                        else:
                            delta = 8 if relative_cycles[
                                             raw_ctv_header.originating_chip] < 0 else raw_ctv_header.relative_cycle_count - \
                                                                                       relative_cycles[
                                                                                           raw_ctv_header.originating_chip]
                            relative_cycles[raw_ctv_header.originating_chip] = raw_ctv_header.relative_cycle_count
                            if delta == -504:
                                delta = 8

                        delta_correct = delta == 512 if raw_ctv_header.is_cycle_count else delta == 8

                        mod_4 = (raw_ctv_header.relative_cycle_count // 8) % 4

                        s = "{0},,{1},{2},,{3}\n".format(s, delta, "TRUE" if delta_correct else "FALSE", mod_4)

                    f.write(s)

                    current_index += raw_ctv_header.block_count // 4 + 1

    def generate_cycle_corrected_ctv_header_decode(self, stream=CTV_DATA):

        full_cycle_count = [0, 0, 0, 0]

        for i in range(2):

            filename = str(self.fullstem_decode) + (
                "_cycle_corrected_ctv_data_headers.txt" if i == 0 else "_cycle_corrected_ctv_data_headers.csv")

            if not self.decode_folder.is_dir():
                os.mkdir(self.decode_folder)

            with open(filename, "w") as f:

                self.clear_memos()

                if (i == 1):
                    print(
                        "index,type,chip,slice,eob,wide_mask,valid_cycles,cycle_count,block_count,blank,data0,data1,data2",
                        file=f)

                packet_count = self.get_packet_count(stream)
                current_index = 0

                while current_index < packet_count:

                    packet = self.extract_packet(current_index)

                    common_header = self.get_common_header()

                    if common_header.capture_word_type == CW_ERROR:
                        current_index += 2;
                        continue

                    raw_ctv_header = self.get_raw_ctv_data_header()

                    if raw_ctv_header.is_cycle_count:
                        full_cycle_count[raw_ctv_header.originating_chip] = raw_ctv_header.full_cycle_count
                        current_index += 1;
                        continue

                    if i == 0:
                        s = raw_ctv_header.get_cycle_corrected_dump_text(
                            full_cycle_count[raw_ctv_header.originating_chip], 1, 2 if current_index == 0 else 4)

                    else:
                        s = raw_ctv_header.get_cycle_corrected_csv_line(
                            full_cycle_count[raw_ctv_header.originating_chip]).strip()
                        s = "{0},,0x{1:016x},0x{2:016x},0x{3:016x}\n".format(s, self.extract_int64_from_packet(1),
                                                                             self.extract_int64_from_packet(2),
                                                                             self.extract_int64_from_packet(3))

                    f.write(s)

                    current_index += raw_ctv_header.block_count // 4 + 1

    def generate_unpacked_ctv_data(self, polarity=0, stream=CTV_DATA, skip_invalid=False, skip_eob=True,
                                   block_break=False, cycle_break=False, chip_break=False):

        filename = str(self.fullstem_decode) + "_unpacked_ctv_data.csv"

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        with open(filename, "w") as f:

            self.clear_memos()

            if CycleUnpacker.ctv_header_cycles:
                for i in range(4):
                    CycleUnpacker.ctv_header_cycle_index[i] = 0

            packet_count = self.get_packet_count(stream)

            current_index = 0

            full_cycle_count = [0, 0, 0, 0]

            packet_list = [[] for chip in range(4)]

            raw_ctv_header = None

            while current_index < packet_count:

                packet = self.extract_packet(current_index)

                common_header = self.get_common_header()

                if common_header.capture_word_type == CW_ERROR:
                    current_index += 2;
                    continue

                raw_ctv_header = self.get_raw_ctv_data_header()

                packet_list[raw_ctv_header.originating_chip].append(packet)

                current_index += 1

                if raw_ctv_header.block_count > 3:
                    data_packet_count = raw_ctv_header.block_count // 4

                    for i in range(data_packet_count):
                        packet = self.extract_packet(current_index)
                        packet_list[raw_ctv_header.originating_chip].append(packet)
                        current_index += 1

            CycleUnpacker.set_ctv_header_cycles(self.ctv_header_cycles)
            CycleUnpacker.set_ctv_header_traces(self.ctv_header_traces)
            CycleUnpacker.set_traces(self.traces)

            first = True

            for chip in range(4):

                first_block = True

                last_cycle = -1

                since_invalid = 1

                if not packet_list[chip]:
                    continue

                if not first and chip_break:
                    f.write("\n\n\n")

                current_index = 0

                raw_ctv_header = None

                while current_index < len(packet_list[chip]):

                    packet = packet_list[chip][current_index]

                    self.last_packet = packet

                    self.last_index = current_index

                    raw_ctv_header = self.get_raw_ctv_data_header()

                    if raw_ctv_header.is_cycle_count or skip_eob and raw_ctv_header.end_of_burst:
                        full_cycle_count[raw_ctv_header.originating_chip] = raw_ctv_header.full_cycle_count
                        current_index += 1
                        continue

                    if not first_block and block_break:
                        f.write("\n")

                    first_block = False

                    CycleUnpacker.set_polarity(polarity, raw_ctv_header.block_count)

                    cu = CycleUnpacker(raw_ctv_header, full_cycle_count[raw_ctv_header.originating_chip], current_index)

                    if first:
                        csv_header = cu.get_csv_header()

                        f.write(csv_header + '\n')

                        first = False

                    for i in range(raw_ctv_header.block_count):

                        if i > 0 and (i + 1) % 4 == 0:
                            current_index += 1
                            packet = packet_list[chip][current_index]
                            self.last_packet = packet
                            self.last_index = current_index

                        extract_index = (i + 1) % 4

                        block_value = self.extract_64_from_packet(extract_index)

                        cu.add_block(block_value)

                    while True:

                        do_force_invalid = False

                        if since_invalid == 25 or since_invalid == 26:
                            do_force_invalid = True

                            if since_invalid == 26:
                                since_invalid = 0

                        since_invalid += 1

                        cycle_number, valid, current = cu.get_next_cycle(force_invalid=False and do_force_invalid)

                        if not current:
                            break

                        if not valid and skip_invalid:
                            continue;

                        if cycle_number != last_cycle + 1 and last_cycle >= 0 and cycle_break and not block_break:
                            f.write("\n")

                        last_cycle = cycle_number

                        f.write(current + '\n')

                    current_index += 1

    def generate_unpacked_actual_ctv_data(self, polarity=0, stream=CTV_DATA):

        filename = str(self.fullstem_decode) + "_unpacked_actual_ctv_data.csv"

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        with open(filename, "w") as f:

            self.clear_memos()

            if CycleUnpacker.ctv_header_cycles:
                for i in range(4):
                    CycleUnpacker.ctv_header_cycle_index[i] = 0

            packet_count = self.get_packet_count(stream)

            current_index = 0

            full_cycle_count = [0, 0, 0, 0]

            packet_list = [[] for chip in range(4)]

            raw_ctv_header = None

            while current_index < packet_count:

                packet = self.extract_packet(current_index)

                common_header = self.get_common_header()

                if common_header.capture_word_type == CW_ERROR:
                    current_index += 2;
                    continue

                raw_ctv_header = self.get_raw_ctv_data_header()

                packet_list[raw_ctv_header.originating_chip].append(packet)

                current_index += 1

            for chip in range(4):

                if not packet_list[chip]:
                    continue

                since_invalid = 1

                current_index = 0

                raw_ctv_header = None

                while current_index < len(packet_list[chip]):

                    packet = packet_list[chip][current_index]

                    self.last_packet = packet

                    self.last_index = current_index

                    common_header = self.get_common_header()

                    raw_ctv_header = self.get_raw_ctv_data_header()

                    if raw_ctv_header.is_cycle_count or raw_ctv_header.end_of_burst:
                        full_cycle_count[raw_ctv_header.originating_chip] = raw_ctv_header.full_cycle_count
                        current_index += 1
                        continue

                    CycleUnpacker.set_polarity(polarity, raw_ctv_header.block_count)

                    CycleUnpacker.set_ctv_header_cycles(self.ctv_header_cycles)
                    CycleUnpacker.set_ctv_header_traces(self.ctv_header_traces)
                    CycleUnpacker.set_traces(self.traces)

                    cu = CycleUnpacker(raw_ctv_header, full_cycle_count[raw_ctv_header.originating_chip], current_index)

                    for i in range(raw_ctv_header.block_count):

                        if i > 0 and (i + 1) % 4 == 0:
                            current_index += 1
                            packet = packet_list[chip][current_index]
                            self.last_packet = packet
                            self.last_index = current_index

                        extract_index = (i + 1) % 4

                        block_value = self.extract_64_from_packet(extract_index)

                        cu.add_block(block_value)

                    while True:

                        do_force_invalid = False

                        if since_invalid == 25 or since_invalid == 26:
                            do_force_invalid = True

                            if since_invalid == 26:
                                since_invalid = 0

                        since_invalid += 1

                        cycle_number, valid, current = cu.get_next_expect_cycle(
                            force_invalid=False and do_force_invalid)

                        if not current:
                            break

                        if not valid:
                            continue;

                        f.write(current + '\n')

                    current_index += 1

        return filename

    def generate_wide_capture_header_decode(self, stream=CTV_HEADER):

        filename = str(self.fullstem_decode) + " _wide_ctv_headers.csv"

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        with open(filename, "w") as f:

            print(
                "index,pattern_counter,trace_index,user_cycle_count,user_log1,repeat_cycle_count,pattern_cycle_count,vector_address,last_pcall_address",
                file=f)

            packet_count = self.get_packet_count(stream)

            current_index = 0

            wide_ctv_header = None

            while current_index < packet_count:
                packet = self.extract_packet(current_index)

                wide_ctv_header = self.get_wide_capture_header()

                s = wide_ctv_header.get_csv_line()

                f.write(s)

                current_index += 1

    def generate_trace_stream_decode(self, stream=TRACE):

        filename = str(self.fullstem_decode) + "_trace_entries.csv"

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        with open(filename, "w") as f:

            print("index,pattern_id,jump_type,capture_this_pin,to_address,from_address", file=f)

            packet_count = self.get_packet_count(stream)

            current_index = 0

            trace_entry = None

            while current_index < packet_count:
                packet = self.extract_packet(current_index)

                trace_entry = self.get_trace_entry()

                s = trace_entry.get_csv_line()

                f.write(s)

                current_index += 1


class CaptureFieldExtractor():
    memo = None

    def __init__(self, packet):
        self.current_index = 0
        self.packet = packet

    def extract(self, count):

        start_byte_index = self.current_index // 8;
        stop_byte_index = (self.current_index + count) // 8;

        key = (tuple(self.packet[start_byte_index: stop_byte_index + 1]), self.current_index, count)

        if self.memo != None and key in self.memo:
            result = self.memo[key]
            self.current_index += count
            return result

        result = 0

        for index in range(self.current_index, self.current_index + count):
            byte_index = index // 8
            bit_index = index % 8
            byte_value = self.packet[byte_index]
            bit_value = 1 if (1 << (7 - bit_index)) & byte_value > 0 else 0
            result = result << 1 | bit_value

        self.current_index += count

        if self.memo != None:
            self.memo[key] = result

        return result
