# INTEL CONFIDENTIAL
# Copyright 2018 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

from time import sleep

from stopwatch import StopWatch

print()

print("Successive sleep 5 ms demo. Press <enter>...", end="")

input()

sw = StopWatch.start_new()

sleep(0.005)

print()

print("{:>25,.3f} us".format(sw.elapsed_microseconds()))

sleep(0.005)

print("{:>25,.3f} us".format(sw.elapsed_microseconds()))

print()

print("\n\nStart and stop accumulated time demo. Press <enter>...", end="")

input()

print()

print("    Sleeping 2 seconds with new stopwatch running   :", end="", flush=True)

sw = StopWatch.start_new()

sleep(2.0)

sw.stop()

print("{:>25,.3f} ms".format(sw.elapsed_milliseconds()))

print()

print("    Now sleeping 2 seconds with stopwatch stopped   :", end="", flush=True)

sleep(2.0)

print("{:>25,.3f} ms".format(sw.elapsed_milliseconds()))

print()

print("    Now sleeping 2 seconds with stopwatch restarted :", end="", flush=True)

sw.start()

sleep(2.0)

print("{:>25,.3f} ms".format(sw.elapsed_milliseconds()))

print()

print("\n\nSpin for 20 microseconds demo. Press <enter>...", end="")

input()

print()

sw = StopWatch.start_new()

elapsed_us = sw.spinclear(20.0E-6) * 1.0E6

print("{:>25,.3f} us".format(elapsed_us))

print()

elapsed_us = sw.spinclear(20.0E-6) * 1.0E6

print("{:>25,.3f} us".format(elapsed_us))

print()

print("\n\nTime loop demo. Press <enter>...", end="")

input()

print()

i = 0

sw = StopWatch.start_new()

while i < 1000000:
    i += 1

sw.stop()

print()

print("{:>25,.3f} us".format(sw.elapsed_microseconds()))

print()
