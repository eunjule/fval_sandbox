# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.


class CycleInfo:

    def __init__(self):
        self.chip = 0
        self.slice = 0
        self.capture_block_index = 0
        self.cycles_covered = 0
        self.pins_covered = 0
        self.cycle_number = 0
        self.is_eob = 0
        self.is_valid = 0
        self.all_same = 0
        self.ctp_mask_value = 0
        self.pin_mask = None

    def set_pin_mask_from_count(self):
        self.ctp_mask_value = 0

        for i in range(self.pins_covered):
            self.ctp_mask_value <<= 1
            self.ctp_mask_value |= 1

        self.pin_mask = BinStringRepresentation(35, self.ctp_mask_value, zero_char='x').to_string()

    def set_pin_mask_from_trace(self, mask_value):
        self.ctp_mask_value = mask_value
        self.pin_mask = BinStringRepresentation(35, self.ctp_mask_value, zero_char='x').to_string()

    def get_csv_header(self):
        return "chip,slice,block,covered,pins,cycle,eob,valid,all_same,pin_mask"

    def get_csv_values(self):
        result = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}".format(self.chip, self.slice, self.capture_block_index,
                                                                  self.cycles_covered, self.pins_covered,
                                                                  self.cycle_number,
                                                                  self.is_eob, self.is_valid, self.all_same,
                                                                  self.pin_mask)

        return result

    def get_expect_csv_header(self):
        return "chip,slice,pins,cycle"

    def get_expect_csv_values(self):
        result = "{0},{1},0x{2:x},{3}".format(self.chip, self.slice, self.ctp_mask_value, self.cycle_number)

        return result


class ChannelSetCycleData:
    polarity = 0
    polarity_mask = 0
    width = 0

    @classmethod
    def set_polarity(cls, polarity, width):
        cls.polarity = polarity
        cls.width = width

        if not polarity:
            cls.polarity_mask = 0

        else:
            for i in range(width):
                cls.polarity_mask <<= 1
                cls.polarity_mask |= 1

    def __init__(self, channel_set, state, error):

        self.channel_set = channel_set
        self.state = state
        self.error = error
        self.corrected_error = error ^ state ^ self.polarity_mask

    def get_csv_header(self):
        return "state_value_{0},error_value_{0},corrected_ev_{0},state_{0},error_{0},corrected_{0}".format(
            self.channel_set)

    def get_csv_values(self):
        if self.state >= 0:
            result = "{0},{1},{2},{3},{4},{5}".format(self.state, self.error, self.corrected_error,
                                                      BinStringRepresentation(self.width, self.state).to_string(),
                                                      BinStringRepresentation(self.width, self.error).to_string(),
                                                      BinStringRepresentation(self.width,
                                                                              self.corrected_error).to_string())

        else:
            result = "{0},{1},{2},{3},{4},{5}".format(-1, -1, -1,
                                                      BinStringRepresentation(self.width, self.state, zero_char='x',
                                                                              one_char='x').to_string(),
                                                      BinStringRepresentation(self.width, self.error, zero_char='x',
                                                                              one_char='x').to_string(),
                                                      BinStringRepresentation(self.width, self.corrected_error,
                                                                              zero_char='x', one_char='x').to_string())

        return result

    def get_expect_csv_header(self):
        return "corrected_ev_{0}".format(self.channel_set)

    def get_expect_csv_values(self):
        result = "{0}".format(self.corrected_error)

        return result


class UnpackedCycle:

    @classmethod
    def set_polarity(cls, polarity, width):
        ChannelSetCycleData.set_polarity(polarity, width)

    def __init__(self, cycle_info):

        self.cycle_info = cycle_info
        self.channel_sets = [None, None, None, None]

    def add_channel_set(self, channel_set, state, error):
        self.channel_sets[channel_set] = ChannelSetCycleData(channel_set, state, error)

        if channel_set == 3:
            self.cycle_info.all_same = 1

            check_state = self.channel_sets[0].state
            check_error = self.channel_sets[0].error

            for i in range(1, 4):
                if self.channel_sets[i].state != check_state or self.channel_sets[i].error != check_error:
                    self.cycle_info.all_same = 0
                    break

    def get_csv_header(self):
        return "{0},,{1},,{2},,{3},,{4}".format(self.cycle_info.get_csv_header(),
                                                self.channel_sets[0].get_csv_header(),
                                                self.channel_sets[1].get_csv_header(),
                                                self.channel_sets[2].get_csv_header(),
                                                self.channel_sets[3].get_csv_header())

    def get_csv_values(self):
        result = "{0},,{1},,{2},,{3},,{4}".format(self.cycle_info.get_csv_values(),
                                                  self.channel_sets[0].get_csv_values(),
                                                  self.channel_sets[1].get_csv_values(),
                                                  self.channel_sets[2].get_csv_values(),
                                                  self.channel_sets[3].get_csv_values())

        return result;

    def get_expect_csv_header(self):
        return "{0},{1},{2},{3},{4}".format(self.cycle_info.get_expect_csv_header(),
                                            self.channel_sets[0].get_expect_csv_header(),
                                            self.channel_sets[1].get_expect_csv_header(),
                                            self.channel_sets[2].get_expect_csv_header(),
                                            self.channel_sets[3].get_expect_csv_header())

    def get_expect_csv_values(self):
        result = "{0},{1},{2},{3},{4}".format(self.cycle_info.get_expect_csv_values(),
                                              self.channel_sets[0].get_expect_csv_values(),
                                              self.channel_sets[1].get_expect_csv_values(),
                                              self.channel_sets[2].get_expect_csv_values(),
                                              self.channel_sets[3].get_expect_csv_values())

        return result


class CycleUnpacker:
    ctv_header_cycles = None
    ctv_header_traces = None
    traces = None

    memo = None

    ctv_header_cycle_index = [0, 0, 0, 0]

    @classmethod
    def set_polarity(cls, polarity, width):
        ChannelSetCycleData.set_polarity(polarity, width)

    @classmethod
    def set_ctv_header_cycles(cls, ctv_header_cycles):
        cls.ctv_header_cycles = ctv_header_cycles

    @classmethod
    def set_ctv_header_traces(cls, ctv_header_traces):
        cls.ctv_header_traces = ctv_header_traces

    @classmethod
    def set_traces(cls, traces):
        cls.traces = traces

    def __init__(self, header, full_cycle_count, capture_block_index):

        self.header = header
        self.full_cycle_count = full_cycle_count
        self.capture_block_index = capture_block_index
        self.base_cycle = header.relative_cycle_count + (full_cycle_count & ~ 0x1FF)
        self.total_cycles = 0
        self.is_wide = [False for i in range(8)]
        self.valid_cycles = [False for i in range(32)]
        self.current_index = 0
        self.current_block_index = 0

        valid_mask_index = 0
        valid_index = 0

        for i in range(8):

            mask = 1 << i

            if (header.entry_is_wide & mask):

                self.is_wide[i] = True

                self.total_cycles += 1

                is_valid = header.valid_cycles & 1 << valid_mask_index != 0

                self.valid_cycles[valid_index] = is_valid

                valid_index += 1

                valid_mask_index += 4

            else:

                self.total_cycles += 4

                for j in range(4):
                    is_valid = header.valid_cycles & 1 << valid_mask_index != 0

                    self.valid_cycles[valid_index] = is_valid

                    valid_index += 1

                    valid_mask_index += 1

        self.pin_states = [[['x'] * header.block_count for i in range(self.total_cycles)] for j in range(4)]
        self.error_states = [[['x'] * header.block_count for i in range(self.total_cycles)] for j in range(4)]

        self.blocks = [0 for i in range(header.block_count)]

    def add_block(self, block):

        self.blocks[self.current_block_index] = block

        self.current_block_index += 1

        if self.current_block_index == self.header.block_count:
            self.unpack_blocks()

    def unpack_blocks(self):

        current_cycle_index = 0

        blocks_tuple = [tuple(self.blocks[i]) for i in range(self.header.block_count)]

        key = (tuple(self.valid_cycles), tuple(self.is_wide), tuple(blocks_tuple))

        if self.memo != None and key in self.memo:
            self.pin_states, self.error_states = self.memo[key]
            return

        for sub_block_index in range(8):

            if self.is_wide[sub_block_index]:

                if not self.valid_cycles[current_cycle_index]:
                    current_cycle_index += 1
                    continue;

                for block_index in range(self.header.block_count):

                    work_value = self.blocks[block_index][7 - sub_block_index]

                    for channel_set in range(4):
                        pin_state = '1' if work_value & 1 << (channel_set * 2 + 0) else '0'
                        error_state = '1' if work_value & 1 << (channel_set * 2 + 1) else '0'

                        self.pin_states[channel_set][current_cycle_index][block_index] = pin_state
                        self.error_states[channel_set][current_cycle_index][block_index] = error_state

                current_cycle_index += 1



            else:

                for cycle_index in range(4):

                    if not self.valid_cycles[current_cycle_index]:
                        current_cycle_index += 1
                        continue;

                    for block_index in range(self.header.block_count):
                        work_value = self.blocks[block_index][7 - sub_block_index]

                        pin_state = '1' if work_value & 1 << (cycle_index * 2 + 0) else '0'
                        error_state = '1' if work_value & 1 << (cycle_index * 2 + 1) else '0'

                        self.pin_states[0][current_cycle_index][block_index] = pin_state
                        self.error_states[0][current_cycle_index][block_index] = error_state

                    current_cycle_index += 1

        if self.memo != None:
            self.memo[key] = (self.pin_states, self.error_states)

    def get_csv_header(self):

        ci = CycleInfo()
        uc = UnpackedCycle(ci)

        uc.add_channel_set(0, -1, -1)
        uc.add_channel_set(1, -1, -1)
        uc.add_channel_set(2, -1, -1)
        uc.add_channel_set(3, -1, -1)

        result = uc.get_csv_header()

        return result

    def get_next_cycle(self, start=False, force_invalid=False):

        if self.total_cycles == self.current_index:
            return (-1, False, None)

        if start:
            self.current_index = 0

        ci = CycleInfo()

        ci.chip = self.header.originating_chip
        ci.slice = self.header.originating_slice
        ci.capture_block_index = self.capture_block_index
        ci.cycles_covered = self.total_cycles
        ci.pins_covered = self.header.block_count
        ci.cycle_number = self.base_cycle + self.current_index
        ci.is_eob = self.header.end_of_burst
        ci.is_valid = 1 if self.valid_cycles[self.current_index] and not force_invalid else 0

        if self.ctv_header_traces and self.traces:
            trace_index = self.ctv_header_traces[self.ctv_header_cycle_index[ci.chip]]
            pin_mask = self.traces[trace_index].capture_this_pin
            ci.set_pin_mask_from_trace(pin_mask)

        else:
            ci.set_pin_mask_from_count()

        if self.ctv_header_cycles and self.valid_cycles[self.current_index]:
            ci.cycle_number = self.ctv_header_cycles[self.ctv_header_cycle_index[ci.chip]]
            self.ctv_header_cycle_index[ci.chip] += 1

        uc = UnpackedCycle(ci)

        for channel_set in range(4):

            if not self.pin_states[channel_set][self.current_index] or self.pin_states[channel_set][self.current_index][
                0] == 'x':
                uc.add_channel_set(channel_set, -1, -1)
                continue

            pin_state_value = 0
            error_state_value = 0

            for block_index in range(self.header.block_count):
                bit_value = 1 if self.pin_states[channel_set][self.current_index][
                                     self.header.block_count - block_index - 1] == '1' else 0

                pin_state_value <<= 1
                pin_state_value |= bit_value

                bit_value = 1 if self.error_states[channel_set][self.current_index][
                                     self.header.block_count - block_index - 1] == '1' else 0

                error_state_value <<= 1
                error_state_value |= bit_value

            uc.add_channel_set(channel_set, pin_state_value, error_state_value);

        self.current_index += 1

        result = (ci.cycle_number, ci.is_valid, uc.get_csv_values())

        return result

    def get_expect_csv_header(self):

        ci = CycleInfo()
        uc = UnpackedCycle(ci)

        uc.add_channel_set(0, -1, -1)
        uc.add_channel_set(1, -1, -1)
        uc.add_channel_set(2, -1, -1)
        uc.add_channel_set(3, -1, -1)

        result = uc.get_expect_csv_header()

        return result

    def get_next_expect_cycle(self, start=False, force_invalid=False):

        if self.total_cycles == self.current_index:
            return (-1, False, None)

        if start:
            self.current_index = 0

        ci = CycleInfo()

        ci.chip = self.header.originating_chip
        ci.slice = self.header.originating_slice
        ci.capture_block_index = self.capture_block_index
        ci.cycles_covered = self.total_cycles
        ci.pins_covered = self.header.block_count
        ci.cycle_number = self.base_cycle + self.current_index
        ci.is_eob = self.header.end_of_burst
        ci.is_valid = 1 if self.valid_cycles[self.current_index] and not force_invalid else 0

        if self.ctv_header_traces and self.traces:
            trace_index = self.ctv_header_traces[self.ctv_header_cycle_index[ci.chip]]
            pin_mask = self.traces[trace_index].capture_this_pin
            ci.set_pin_mask_from_trace(pin_mask)

        else:
            ci.set_pin_mask_from_count()

        if (self.ctv_header_cycles and self.valid_cycles[self.current_index]):
            ci.cycle_number = self.ctv_header_cycles[self.ctv_header_cycle_index[ci.chip]]
            self.ctv_header_cycle_index[ci.chip] += 1

        uc = UnpackedCycle(ci)

        for channel_set in range(4):

            if not self.pin_states[channel_set][self.current_index] or self.pin_states[channel_set][self.current_index][
                0] == 'x':
                uc.add_channel_set(channel_set, -1, -1)
                continue

            pin_state_value = 0
            error_state_value = 0

            for block_index in range(self.header.block_count):
                bit_value = 1 if self.pin_states[channel_set][self.current_index][
                                     self.header.block_count - block_index - 1] == '1' else 0

                pin_state_value <<= 1
                pin_state_value |= bit_value

                bit_value = 1 if self.error_states[channel_set][self.current_index][
                                     self.header.block_count - block_index - 1] == '1' else 0

                error_state_value <<= 1
                error_state_value |= bit_value

            uc.add_channel_set(channel_set, pin_state_value, error_state_value);

        self.current_index += 1

        result = (ci.cycle_number, ci.is_valid, uc.get_expect_csv_values())

        return result


class BinStringRepresentation():
    memo = None

    def __init__(self, places, value, zero_char='0', one_char='1'):
        self.places = places
        self.value = value
        self.zero_char = zero_char
        self.one_char = one_char

        self.representation = self.expand()

    def to_string(self):
        return self.representation

    def expand(self):

        key = (self.places, self.value, self.zero_char, self.one_char)

        if self.memo != None and key in self.memo:
            result = self.memo[key]
            return result

        result = ""
        work_value = self.value

        for i in range(self.places):

            if i != 0:
                if i % 16 == 0:
                    result = "____" + result
                elif i % 8 == 0:
                    result = "__" + result
                elif i % 4 == 0:
                    result = "_" + result

            iBit = work_value & 1

            work_value >>= 1

            if iBit:
                result = self.one_char + result
            else:
                result = self.zero_char + result

        result = '_' + result

        if self.memo != None:
            self.memo[key] = result

        return result
