# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import os

from pathlib import Path


class CaptureFileUtilities:

    def __init__(self, filename):
        self.filename = filename
        self.fullpath = os.path.abspath(filename)
        self.fullstem = os.path.splitext(self.fullpath)[0]
        self.decode_folder = Path(Path(self.fullpath).parent, Path("DecodedFolder"))
        self.fullstem_decode = Path(self.decode_folder, Path(self.fullstem).name)

        self.dict_string_to_byte = {}
        self.byte_to_string = []

        for i in range(256):
            string_value = "{0:02x}".format(i);
            self.dict_string_to_byte[string_value] = i;
            self.byte_to_string.append(string_value);

    def generate_fileset_from_dma(self):

        path = Path(self.fullpath)

        parent = path.parent
        stem = path.stem
        suffix = path.suffix if path.suffix else ".txt"

        fullpath = Path(parent, stem).with_suffix(suffix)

        lines = self.read_all_lines(fullpath)

        lines = [l.strip() for l in lines if l.strip() != ""]

        dict_blocks = self.generate_block_dictionary(lines)

        if not dict_blocks:
            return None

        dict_line_values = self.generate_line_values(dict_blocks)

        dict_bytes = self.generate_bin_values(dict_line_values)

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        workname = Path(self.decode_folder, Path(fullpath).name)

        self.create_bin_files(workname, dict_bytes)

        # self.create_packet_files ( workname, dict_bytes )

    def generate_fileset_from_packets(self, auto_gather=False):
        input('generate')
        filenames = self.generate_auto_gather_filenames(auto_gather, "packets", "txt")
        filenames = [(stream, name) for stream, name in filenames if Path(name).is_file()]

        dict_packets = {}

        for stream, filename in filenames:
            dict_packets[stream] = self.read_all_lines(filename)

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        workname = Path(self.decode_folder, Path(self.fullpath).stem)

        self.create_bin_files_from_packets(workname, dict_packets)

    def generate_fileset_from_bin(self, auto_gather=False):

        filenames = self.generate_auto_gather_filenames(auto_gather)
        filenames = [(stream, name) for stream, name in filenames if Path(name).is_file()]

        dict_bytes = {}

        for stream, filename in filenames:
            with open(filename, "rb") as f:
                dict_bytes[stream] = bytearray(f.read())

        if not self.decode_folder.is_dir():
            os.mkdir(self.decode_folder)

        workname = Path(self.decode_folder, Path(self.fullpath).stem)

        self.create_packet_files(workname, dict_bytes)

    def read_bin_dictionary(self, auto_gather=False, from_decoded_folder=False):

        filenames = self.generate_auto_gather_filenames(auto_gather, from_decoded_folder=from_decoded_folder)
        filenames = [(stream, name) for stream, name in filenames if Path(name).is_file()]

        dict_bytes = {}

        for stream, filename in filenames:
            with open(filename, "rb") as f:
                dict_bytes[stream] = bytearray(f.read())

        return dict_bytes

    def get_stream_names(self):

        streams = "TRACE CTV_HEADER CTV_DATA PSDB_0_PIN_0 PSDB_0_PIN_1 PSDB_1_PIN_0 PSDB_1_PIN_1".split(" ")
        return streams

    def generate_auto_gather_filenames(self, auto_gather=False, suffix="", extension="bin", from_decoded_folder=False):

        fullstem = str(self.fullstem_decode if from_decoded_folder else self.fullstem)

        if not auto_gather:
            result = [("", fullstem + "." + extension)]

        else:
            result = [(stream, fullstem + "_" + stream + ("_" + suffix if suffix else "") + "." + extension) for stream
                      in self.get_stream_names()]

        return result

    def read_all_lines(self, filename, trim_lines=True, remove_empty=True):
        with open(filename) as f:
            result = [line.strip('\r\n') for line in f]

            if (trim_lines):
                result = [l.strip() for l in result if l.strip() != "" or not remove_empty]

            return result

    def generate_block_dictionary(self, lines):

        dict_blocks = {}

        dict_lengths = {}

        work_list = None

        for line in lines:
            if ":" not in line:
                fields = line.split()
                work_list = []
                dict_blocks[fields[0]] = work_list
                dict_lengths[fields[0]] = (int(fields[1]))

            else:
                work_list.append(line)

        for key in dict_lengths.keys():
            if (len(dict_blocks[key]) != dict_lengths[key] * 2 and dict_lengths[key] >= 4):
                print("    Block size mismatch for {0}.".format(key))
                return None

        return dict_blocks

    def generate_line_values(self, dict_blocks):

        dict_values = {}

        work_list = None

        for key in dict_blocks.keys():
            work_list = []
            dict_values[key] = work_list

            for line in dict_blocks[key]:
                fields = line.split(' : ')
                values = fields[1].split(' ')
                for item in values:
                    work_list.append(self.dict_string_to_byte[item])

        return dict_values

    def generate_bin_values(self, dict_values):

        dict_bytearray = {}

        for key in dict_values.keys():
            work_list = bytearray()
            dict_bytearray[key] = work_list

            for item in dict_values[key]:
                work_list.append(item)

        return dict_bytearray

    def create_bin_files(self, filename, dict_bytes):

        for key in dict_bytes.keys():
            outfile = os.path.splitext(filename)[0]
            outfile = outfile + "_" + key + ".bin"

            with open(outfile, "wb") as f:
                f.write(dict_bytes[key])

    def create_packet_files(self, filename, dict_bytes):

        for key in dict_bytes.keys():
            outfile = os.path.splitext(filename)[0]
            outfile = outfile + ("_" if key else "") + key + "_packets.txt"

            with open(outfile, "w") as f:
                line_count = len(dict_bytes[key]) // 16
                for i in range(line_count):
                    work_array = dict_bytes[key][i * 16: (i + 1) * 16]
                    work_array = list(reversed(work_array))

                    for j in range(4):
                        work_item = work_array[j * 4: (j + 1) * 4]

                        for k in range(4):
                            print(self.byte_to_string[work_item[k]], file=f, end='')

                        if (j < 3):
                            print(" ", file=f, end='')

                    print("", file=f)

    def create_bin_files_from_packets(self, filename, dict_packets):

        for key in dict_packets.keys():
            outfile = os.path.splitext(filename)[0]
            outfile = outfile + ("_" if key else "") + key + ".bin"

            line_size = 32 if len(dict_packets[key][0]) > 60 else 16

            with open(outfile, "wb") as f:
                byte_count = len(dict_packets[key]) * line_size
                bytes = bytearray(byte_count)

                byte_index = 0

                for line in dict_packets[key]:

                    line = line.replace(" ", "")

                    for i in range(line_size):
                        byte_str = line[i * 2: (i + 1) * 2]
                        byte_val = int(byte_str, 16)
                        bytes[byte_index + line_size - 1 - i] = byte_val

                    byte_index += line_size

                f.write(bytes)
