# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

from CaptureDecodeUtilities import *


class CaptureEncodeUtilities:

    def __init__(self):

        self.byte_index = 0

    # def get_raw_ctv_data_header ( self, packet = None ):

    #    workpacket = self.last_packet if packet == None else packet

    #    cfe = CaptureFieldExtractor ( workpacket )

    #    result = RawCTVDataHeader ( )

    #    result.packet_index = self.last_index if packet == None else -1

    #    result.capture_word_type = cfe.extract ( 2 )
    #    result.originating_chip  = cfe.extract ( 2 )
    #    result.originating_slice = cfe.extract ( 3 )
    #    result.is_cycle_count    = cfe.extract ( 1 )

    #    if result.is_cycle_count == 1:
    #        result.full_cycle_count = cfe.extract ( 48 )
    #        cfe = CaptureFieldExtractor ( workpacket )
    #        cfe.extract ( 8 )

    #    #else:
    #    result.end_of_burst         = cfe.extract ( 1 )
    #    result.entry_is_wide        = cfe.extract ( 8 )
    #    result.valid_cycles         = cfe.extract ( 32 )
    #    result.relative_cycle_count = cfe.extract ( 9 )
    #    result.block_count          = cfe.extract ( 6 )

    #    return result

    def pack_wide_capture_header(self, wch):

        cfp = CaptureFieldPacker()

        cfp.skip(24)
        cfp.pack(24, wch.trace_index)
        cfp.skip(16)
        cfp.pack(32, wch.user_cycle_count)
        cfp.skip(64)
        cfp.pack(32, wch.pattern_cycle_count)
        cfp.pack(32, wch.vector_address)

        return cfp.packet

    def get_ctv_header_header(self, total_packets):
        return "CTV_HEADER " + str(total_packets) + "\n"

    def get_ctv_header_data(self, wch):

        packet = self.pack_wide_capture_header(wch)

        packet = list(packet)

        packet = list(reversed(packet))

        result = "{0:8} :".format(self.byte_index)

        for i in range(16):
            result = result + " {0:02x}".format(packet[i])

        result = result + "\n"

        result = result + "{0:8} :".format(self.byte_index + 16)

        for i in range(16):
            result = result + " {0:02x}".format(packet[i + 16])

        result = result + "\n"

        self.byte_index += 32

        return result

    def write_ctv_header_section(self, f, count):

        self.byte_index = 0

        wch = WideCaptureHeader()

        lines = self.get_ctv_header_header(count)

        f.write(lines)

        for i in range(count):
            wch.pattern_cycle_count = i + 1024
            wch.user_cycle_count = i + 1024
            wch.vector_address = i + 5 + 1024

            lines = self.get_ctv_header_data(wch)
            f.write(lines)

    def pack_ctv_data(self, cdh):

        cfp = CaptureFieldPacker()

        cfp.pack(2, cdh.capture_word_type)
        cfp.pack(2, cdh.originating_chip)
        cfp.pack(3, cdh.originating_slice)
        cfp.pack(1, cdh.is_cycle_count)

        if cdh.is_cycle_count:
            cfp.pack(48, cdh.full_cycle_count)

        else:
            cfp.pack(1, cdh.end_of_burst)
            cfp.pack(8, cdh.entry_is_wide)
            cfp.pack(32, cdh.valid_cycles)
            cfp.pack(9, cdh.relative_cycle_count)
            cfp.pack(6, cdh.block_count)

        if not cdh.is_cycle_count and not cdh.end_of_burst:

            mod_4 = (cdh.relative_cycle_count // 8) % 4

            if mod_4 == 0:
                values = [0x55, 0xff, 0x55, 0xff, 0x55, 0xff, 0x55, 0xff, 0x55, 0x55, 0xff, 0xff, 0x55, 0x55, 0xff,
                          0xff, 0x55, 0x55, 0x55, 0x55, 0xff, 0xff, 0xff, 0xff]

            elif mod_4 == 1:
                values = [0xff, 0x55, 0xff, 0x55, 0xff, 0x55, 0xff, 0x55, 0xff, 0xff, 0x55, 0x55, 0xff, 0xff, 0x55,
                          0x55, 0xff, 0xff, 0xff, 0xff, 0x55, 0x55, 0x55, 0x55]

            elif mod_4 == 2:
                values = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x55, 0x55, 0xff, 0xff, 0x55,
                          0x55, 0xff, 0xff, 0xff, 0xff, 0x55, 0x55, 0x55, 0x55]

            else:
                values = [0xff, 0xff, 0x55, 0x55, 0x55, 0x55, 0xff, 0xff, 0x55, 0x55, 0xff, 0xff, 0x55, 0x55, 0xff,
                          0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff]

            for i in range(24):
                cfp.packet[8 + i] = values[i];

        return cfp.packet

    def get_ctv_data_header(self, total_packets):
        return "CTV_DATA " + str(total_packets) + "\n"

    def get_ctv_data_data(self, cdh):

        packet = self.pack_ctv_data(cdh)

        packet = list(packet)

        packet = list(reversed(packet))

        result = "{0:8} :".format(self.byte_index)

        for i in range(16):
            result = result + " {0:02x}".format(packet[i])

        result = result + "\n"

        result = result + "{0:8} :".format(self.byte_index + 16)

        for i in range(16):
            result = result + " {0:02x}".format(packet[i + 16])

        result = result + "\n"

        self.byte_index += 32

        return result

    def write_ctv_data_section(self, f, count):

        self.byte_index = 0

        count_512 = (count + 1024) // 512 + 1

        packet_count = count // 8

        eob_count = 1

        total_count = 2 * (count_512 + packet_count + eob_count)

        cdh = RawCTVDataHeader()

        cdh.capture_word_type = CW_CTV

        lines = self.get_ctv_data_header(total_count)

        f.write(lines)

        packets_left = [total_count // 2, total_count // 2]

        relative_cycle = [0, 0]

        next_512 = [0, 0]

        for i in range(5):
            cdh.originating_chip = i % 2
            cdh.is_cycle_count = 1
            cdh.full_cycle_count = 512 * (i // 2)
            lines = self.get_ctv_data_data(cdh)
            f.write(lines)
            packets_left[cdh.originating_chip] -= 1
            next_512[cdh.originating_chip] += 512

        for i in range(27):
            cdh.originating_chip = 0
            cdh.is_cycle_count = 0
            cdh.entry_is_wide = 0xff
            cdh.valid_cycles = 0x11111111
            cdh.relative_cycle_count = relative_cycle[0]
            cdh.block_count = 3
            lines = self.get_ctv_data_data(cdh)
            f.write(lines)
            relative_cycle[0] = (relative_cycle[0] + 8) % 512
            packets_left[0] -= 1

        while True:
            cdh.originating_chip = 1 if cdh.originating_chip == 0 else 0

            if packets_left[0] == 0 and packets_left[1] == 0:
                break

            if packets_left[cdh.originating_chip] == 0:
                continue

            if packets_left[cdh.originating_chip] == 1:
                cdh.end_of_burst = 1
                cdh.entry_is_wide = 0
                cdh.valid_cycles = 0
                cdh.relative_cycle_count = relative_cycle[cdh.originating_chip] + 2
                cdh.block_count = 0
                lines = self.get_ctv_data_data(cdh)
                f.write(lines)
                packets_left[cdh.originating_chip] -= 1
                cdh.end_of_burst = 0

            else:
                if relative_cycle[cdh.originating_chip] == 0:
                    cdh.is_cycle_count = 1
                    cdh.full_cycle_count = next_512[cdh.originating_chip]
                    next_512[cdh.originating_chip] += 512
                    packets_left[cdh.originating_chip] -= 1
                    lines = self.get_ctv_data_data(cdh)
                    f.write(lines)
                    cdh.is_cycle_count = 0

                cdh.entry_is_wide = 0xff
                cdh.valid_cycles = 0x11111111
                cdh.relative_cycle_count = relative_cycle[cdh.originating_chip]
                cdh.block_count = 3
                lines = self.get_ctv_data_data(cdh)
                f.write(lines)
                relative_cycle[cdh.originating_chip] = (relative_cycle[cdh.originating_chip] + 8) % 512
                packets_left[cdh.originating_chip] -= 1

    def write_other_sections(self, f):

        f.write("""TRACE 1
       0 : 04 04 00 00 05 04 00 00 00 38 00 00 00 00 00 08
      16 : 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ERRORS 4
       0 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 01
      16 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 01
      32 : 00 00 00 00 46 75 00 00 41 75 00 00 00 00 00 00
      48 : 00 00 00 00 41 75 00 00 00 00 00 00 00 00 00 00
      64 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 01
      80 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 01
      96 : 00 00 00 00 46 75 00 00 41 75 00 00 00 00 00 00
     112 : 00 00 00 00 41 75 00 00 00 00 00 00 00 00 00 00
ERRORS 4
       0 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 11
      16 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 11
      32 : 00 00 00 00 46 75 00 00 41 75 00 00 00 00 00 00
      48 : 00 00 00 00 41 75 00 00 00 00 00 00 00 00 00 00
      64 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 11
      80 : 00 00 00 00 00 00 00 00 c0 a0 3c 00 00 00 00 11
      96 : 00 00 00 00 46 75 00 00 41 75 00 00 00 00 00 00
     112 : 00 00 00 00 41 75 00 00 00 00 00 00 00 00 00 00
""")

    # def get_trace_entry ( self, packet = None ):

    #    workpacket = self.last_packet if packet == None else packet

    #    cfe = CaptureFieldExtractor ( workpacket )

    #    result = TraceEntry ( )

    #    result.packet_index = self.last_index if packet == None else -1

    #    result.reserved_2a           = cfe.extract ( 64 )
    #    result.reserved_2b           = cfe.extract ( 32 )
    #    result.pattern_id            = cfe.extract ( 32 )
    #    result.jump_type             = cfe.extract (  5 )
    #    result.reserved_1            = cfe.extract ( 21 )
    #    result.capture_this_pin      = cfe.extract ( 38 )
    #    result.to_address            = cfe.extract ( 32 )
    #    result.from_address          = cfe.extract ( 32 )

    #    return result

    # def extract_user_cycle_count ( self, packet ):

    #    result = self.extract_int32_from_packet ( 2, packet )

    #    return result

    # def extract_trace_index ( self, packet ):

    #    result = self.extract_int24_from_packet ( 1, packet )

    #    return result

    # def use_ctv_headers ( self ):

    #    packet_count = self.get_packet_count ( CTV_HEADER )

    #    self.ctv_header_cycles = array.array ( 'i', ( 0 for i in range ( packet_count ) ) )
    #    self.ctv_header_traces = array.array ( 'i', ( 0 for i in range ( packet_count ) ) )

    #    for i in range ( packet_count ):
    #        packet = self.extract_packet ( i )
    #        cycle_number = self.extract_user_cycle_count ( packet )
    #        trace_index = self.extract_trace_index ( packet )
    #        self.ctv_header_cycles [ i ] = cycle_number
    #        self.ctv_header_traces [ i ] = trace_index

    #    return

    # def use_traces ( self ):

    #    packet_count = self.get_packet_count ( TRACE )

    #    self.traces = [ ]

    #    for i in range ( packet_count ):
    #        packet = self.extract_packet ( i )
    #        trace_entry = self.get_trace_entry ( )
    #        self.traces.append ( trace_entry )

    #    return

    # def clear_memos ( self ):

    #    if CaptureFieldExtractor.memo != None:
    #        CaptureFieldExtractor.memo.clear ( )

    #    if CycleUnpacker.memo != None:
    #        CycleUnpacker.memo.clear ( )

    #    if BinStringRepresentation.memo != None:
    #        BinStringRepresentation.memo.clear ( )

    # def memos_on ( self ):

    #    CaptureFieldExtractor   .memo = { }
    #    CycleUnpacker           .memo = { }
    #    BinStringRepresentation .memo = { }

    # def memos_off ( self ):

    #    CaptureFieldExtractor   .memo = None
    #    CycleUnpacker           .memo = None
    #    BinStringRepresentation .memo = None

    # def generate_raw_ctv_header_decode ( self, stream = CTV_DATA ):

    #    for i in range ( 1, 2 ):

    #        filename = str ( self.fullstem_decode ) + ( "_raw_ctv_data_headers.txt" if i == 0 else  "_raw_ctv_data_headers.csv" )

    #        if not self.decode_folder.is_dir ( ):
    #            os.mkdir ( self.decode_folder )

    #        with open ( filename, "w" ) as f:

    #            self.clear_memos ( )

    #            if ( i == 1 ):
    #                print ( "index,type,chip,slice,is_count,full_count,eob,wide_mask,valid_cycles,relative_count,block_count,blank,data0_H,data0_L,data1_H,data1_L,data2_H,data2_L,,delta,delta_correct,,mod_4", file = f )

    #            packet_count  = self.get_packet_count ( stream )
    #            current_index = 0

    #            relative_cycles = [ -1, -1, -1, -1 ]
    #            cyle_count      = [ -1, -1, -1, -1 ]

    #            while current_index < packet_count:

    #                packet = self.extract_packet ( current_index )

    #                common_header = self.get_common_header ( )

    #                if common_header.capture_word_type == CW_ERROR:
    #                    current_index += 2;
    #                    continue

    #                raw_ctv_header = self.get_raw_ctv_data_header ( )

    #                if i == 0:
    #                    s = raw_ctv_header.get_dump_text ( 1, 2 if current_index == 0 else 4 )

    #                else:
    #                    s = raw_ctv_header.get_csv_line ( ).strip ( )
    #                    s = "{0},,0x{1:08x},0x{2:08x},0x{3:08x},0x{4:08x},0x{5:08x},0x{6:08x}".format ( s, self.extract_int64_from_packet ( 1 ) >> 32, self.extract_int64_from_packet ( 1 ) & 0xFFFFFFFF, self.extract_int64_from_packet ( 2 ) >> 32, self.extract_int64_from_packet ( 2 ) & 0xFFFFFFFF, self.extract_int64_from_packet ( 3 ) >> 32, self.extract_int64_from_packet ( 3 ) & 0xFFFFFFFF )

    #                    delta = 0

    #                    if raw_ctv_header.is_cycle_count:
    #                        delta = 512 if cyle_count [ raw_ctv_header.originating_chip ] < 0 else raw_ctv_header.full_cycle_count - cyle_count [ raw_ctv_header.originating_chip ]
    #                        cyle_count [ raw_ctv_header.originating_chip ] = raw_ctv_header.full_cycle_count

    #                    else:
    #                        delta = 8 if relative_cycles [ raw_ctv_header.originating_chip ] < 0 else raw_ctv_header.relative_cycle_count - relative_cycles [ raw_ctv_header.originating_chip ]
    #                        relative_cycles [ raw_ctv_header.originating_chip ] = raw_ctv_header.relative_cycle_count
    #                        if delta == -504:
    #                            delta = 8

    #                    delta_correct = delta == 512 if raw_ctv_header.is_cycle_count else delta == 8

    #                    mod_4 = ( raw_ctv_header.relative_cycle_count // 8 ) % 4

    #                    s = "{0},,{1},{2},,{3}\n".format ( s, delta, "TRUE" if delta_correct else "FALSE", mod_4 )

    #                f.write ( s )

    #                current_index += raw_ctv_header.block_count // 4 + 1

    # def generate_cycle_corrected_ctv_header_decode ( self, stream = CTV_DATA ):

    #    full_cycle_count = [ 0, 0, 0, 0 ]

    #    for i in range ( 2 ):

    #        filename = str ( self.fullstem_decode ) + ( "_cycle_corrected_ctv_data_headers.txt" if i == 0 else  "_cycle_corrected_ctv_data_headers.csv" )

    #        if not self.decode_folder.is_dir ( ):
    #            os.mkdir ( self.decode_folder )

    #        with open ( filename, "w" ) as f:

    #            self.clear_memos ( )

    #            if ( i == 1 ):
    #                print ( "index,type,chip,slice,eob,wide_mask,valid_cycles,cycle_count,block_count,blank,data0,data1,data2", file = f )

    #            packet_count  = self.get_packet_count ( stream )
    #            current_index = 0

    #            while current_index < packet_count:

    #                packet = self.extract_packet ( current_index )

    #                common_header = self.get_common_header ( )

    #                if common_header.capture_word_type == CW_ERROR:
    #                    current_index += 2;
    #                    continue

    #                raw_ctv_header = self.get_raw_ctv_data_header ( )

    #                if raw_ctv_header.is_cycle_count:
    #                    full_cycle_count [ raw_ctv_header.originating_chip ] = raw_ctv_header.full_cycle_count
    #                    current_index += 1;
    #                    continue

    #                if i == 0:
    #                    s = raw_ctv_header.get_cycle_corrected_dump_text ( full_cycle_count [ raw_ctv_header.originating_chip ], 1, 2 if current_index == 0 else 4 )

    #                else:
    #                    s = raw_ctv_header.get_cycle_corrected_csv_line ( full_cycle_count [ raw_ctv_header.originating_chip ] ).strip ( )
    #                    s = "{0},,0x{1:016x},0x{2:016x},0x{3:016x}\n".format ( s, self.extract_int64_from_packet ( 1 ), self.extract_int64_from_packet ( 2 ), self.extract_int64_from_packet ( 3 ) )

    #                f.write ( s )

    #                current_index += raw_ctv_header.block_count // 4 + 1

    # def generate_unpacked_ctv_data ( self, polarity = 0, stream = CTV_DATA, skip_invalid = False, skip_eob = True, block_break = False, cycle_break = False, chip_break = False ):

    #    filename = str ( self.fullstem_decode ) + "_unpacked_ctv_data.csv"

    #    if not self.decode_folder.is_dir ( ):
    #        os.mkdir ( self.decode_folder )

    #    with open ( filename, "w" ) as f:

    #        self.clear_memos ( )

    #        if CycleUnpacker.ctv_header_cycles:
    #            for i in range ( 4 ):
    #                CycleUnpacker.ctv_header_cycle_index [ i ] = 0

    #        packet_count = self.get_packet_count ( stream )

    #        current_index = 0

    #        full_cycle_count = [ 0, 0, 0, 0 ]

    #        packet_list = [ [ ] for chip in range ( 4 ) ]

    #        raw_ctv_header = None

    #        while current_index < packet_count:

    #            packet = self.extract_packet ( current_index )

    #            common_header = self.get_common_header ( )

    #            if common_header.capture_word_type == CW_ERROR:
    #                current_index += 2;
    #                continue

    #            raw_ctv_header = self.get_raw_ctv_data_header ( )

    #            packet_list [ raw_ctv_header.originating_chip ].append ( packet )

    #            current_index += 1

    #            if raw_ctv_header.block_count > 3:
    #                data_packet_count = raw_ctv_header.block_count // 4

    #                for i in range ( data_packet_count ):
    #                    packet = self.extract_packet ( current_index )
    #                    packet_list [ raw_ctv_header.originating_chip ].append ( packet )
    #                    current_index += 1

    #        CycleUnpacker.set_ctv_header_cycles ( self.ctv_header_cycles )
    #        CycleUnpacker.set_ctv_header_traces ( self.ctv_header_traces )
    #        CycleUnpacker.set_traces            ( self.traces            )

    #        first = True

    #        for chip in range ( 4 ):

    #            first_block = True

    #            last_cycle = -1

    #            since_invalid = 1

    #            if not packet_list [ chip ]:
    #                continue

    #            if not first and chip_break:
    #                f.write ( "\n\n\n" )

    #            current_index = 0

    #            raw_ctv_header = None

    #            while current_index < len ( packet_list [ chip ] ):

    #                packet = packet_list [ chip ] [ current_index ]

    #                self.last_packet = packet

    #                self.last_index = current_index

    #                raw_ctv_header = self.get_raw_ctv_data_header ( )

    #                if raw_ctv_header.is_cycle_count or skip_eob and raw_ctv_header.end_of_burst:
    #                    full_cycle_count [ raw_ctv_header.originating_chip ] = raw_ctv_header.full_cycle_count
    #                    current_index += 1
    #                    continue

    #                if not first_block and block_break:
    #                    f.write ( "\n" )

    #                first_block = False

    #                CycleUnpacker.set_polarity ( polarity, raw_ctv_header.block_count )

    #                cu = CycleUnpacker ( raw_ctv_header, full_cycle_count [ raw_ctv_header.originating_chip ], current_index )

    #                if first:

    #                    csv_header = cu.get_csv_header ( )

    #                    f.write ( csv_header + '\n' )

    #                    first = False

    #                for i in range ( raw_ctv_header.block_count ):

    #                    if i > 0 and ( i + 1 ) % 4 == 0:
    #                        current_index += 1
    #                        packet = packet_list [ chip ] [ current_index ]
    #                        self.last_packet = packet
    #                        self.last_index = current_index

    #                    extract_index = ( i + 1 ) % 4

    #                    block_value = self.extract_64_from_packet ( extract_index )

    #                    cu.add_block ( block_value )

    #                while True:

    #                    do_force_invalid = False

    #                    if since_invalid == 25 or since_invalid == 26:
    #                        do_force_invalid = True

    #                        if since_invalid == 26:
    #                            since_invalid = 0

    #                    since_invalid += 1

    #                    cycle_number, valid, current = cu.get_next_cycle ( force_invalid = False and do_force_invalid )

    #                    if not current:
    #                        break

    #                    if not valid and skip_invalid:
    #                        continue;

    #                    if cycle_number != last_cycle + 1 and last_cycle >= 0 and cycle_break and not block_break:
    #                        f.write ( "\n" )

    #                    last_cycle = cycle_number

    #                    f.write ( current + '\n' )

    #                current_index += 1

    # def generate_unpacked_actual_ctv_data ( self, polarity = 0, stream = CTV_DATA ):

    #    filename = str ( self.fullstem_decode ) + "_unpacked_actual_ctv_data.csv"

    #    if not self.decode_folder.is_dir ( ):
    #        os.mkdir ( self.decode_folder )

    #    with open ( filename, "w" ) as f:

    #        self.clear_memos ( )

    #        if CycleUnpacker.ctv_header_cycles:
    #            for i in range ( 4 ):
    #                CycleUnpacker.ctv_header_cycle_index [ i ] = 0

    #        packet_count = self.get_packet_count ( stream )

    #        current_index = 0

    #        full_cycle_count = [ 0, 0, 0, 0 ]

    #        packet_list = [ [ ] for chip in range ( 4 ) ]

    #        raw_ctv_header = None

    #        while current_index < packet_count:

    #            packet = self.extract_packet ( current_index )

    #            common_header = self.get_common_header ( )

    #            if common_header.capture_word_type == CW_ERROR:
    #                current_index += 2;
    #                continue

    #            raw_ctv_header = self.get_raw_ctv_data_header ( )

    #            packet_list [ raw_ctv_header.originating_chip ].append ( packet )

    #            current_index += 1

    #        for chip in range ( 4 ):

    #            if not packet_list [ chip ]:
    #                continue

    #            since_invalid = 1

    #            current_index = 0

    #            raw_ctv_header = None

    #            while current_index < len ( packet_list [ chip ] ):

    #                packet = packet_list [ chip ] [ current_index ]

    #                self.last_packet = packet

    #                self.last_index = current_index

    #                common_header = self.get_common_header ( )

    #                raw_ctv_header = self.get_raw_ctv_data_header ( )

    #                if raw_ctv_header.is_cycle_count or raw_ctv_header.end_of_burst:
    #                    full_cycle_count [ raw_ctv_header.originating_chip ] = raw_ctv_header.full_cycle_count
    #                    current_index += 1
    #                    continue

    #                CycleUnpacker.set_polarity ( polarity, raw_ctv_header.block_count )

    #                CycleUnpacker.set_ctv_header_cycles ( self.ctv_header_cycles )
    #                CycleUnpacker.set_ctv_header_traces ( self.ctv_header_traces )
    #                CycleUnpacker.set_traces            ( self.traces            )

    #                cu = CycleUnpacker ( raw_ctv_header, full_cycle_count [ raw_ctv_header.originating_chip ], current_index )

    #                for i in range ( raw_ctv_header.block_count ):

    #                    if i > 0 and ( i + 1 ) % 4 == 0:
    #                        current_index += 1
    #                        packet = packet_list [ chip ] [ current_index ]
    #                        self.last_packet = packet
    #                        self.last_index = current_index

    #                    extract_index = ( i + 1 ) % 4

    #                    block_value = self.extract_64_from_packet ( extract_index )

    #                    cu.add_block ( block_value )

    #                while True:

    #                    do_force_invalid = False

    #                    if since_invalid == 25 or since_invalid == 26:
    #                        do_force_invalid = True

    #                        if since_invalid == 26:
    #                            since_invalid = 0

    #                    since_invalid += 1

    #                    cycle_number, valid, current = cu.get_next_expect_cycle ( force_invalid = False and do_force_invalid )

    #                    if not current:
    #                        break

    #                    if not valid:
    #                        continue;

    #                    f.write ( current + '\n' )

    #                current_index += 1

    #    return filename

    # def generate_wide_capture_header_decode ( self, stream = CTV_HEADER ):

    #    filename = str ( self.fullstem_decode ) + " _wide_ctv_headers.csv"

    #    if not self.decode_folder.is_dir ( ):
    #        os.mkdir ( self.decode_folder )

    #    with open ( filename, "w" ) as f:

    #        print ( "index,pattern_counter,trace_index,user_cycle_count,user_log1,repeat_cycle_count,pattern_cycle_count,vector_address,last_pcall_address", file = f )

    #        packet_count = self.get_packet_count ( stream )

    #        current_index = 0

    #        wide_ctv_header = None

    #        while current_index < packet_count:

    #            packet = self.extract_packet ( current_index )

    #            wide_ctv_header = self.get_wide_capture_header ( )

    #            s = wide_ctv_header.get_csv_line ( )

    #            f.write ( s )

    #            current_index += 1

    # def generate_trace_stream_decode ( self, stream = TRACE ):

    #    filename = str ( self.fullstem_decode ) + "_trace_entries.csv"

    #    if not self.decode_folder.is_dir ( ):
    #        os.mkdir ( self.decode_folder )

    #    with open ( filename, "w" ) as f:

    #        print ( "index,pattern_id,jump_type,capture_this_pin,to_address,from_address", file = f )

    #        packet_count = self.get_packet_count ( stream )

    #        current_index = 0

    #        trace_entry = None

    #        while current_index < packet_count:

    #            packet = self.extract_packet ( current_index )

    #            trace_entry = self.get_trace_entry ( )

    #            s = trace_entry.get_csv_line ( )

    #            f.write ( s )

    #            current_index += 1


class CaptureFieldPacker():

    def __init__(self):
        self.current_index = 0
        self.packet = bytearray(32)

    def skip(self, count):
        self.current_index += count

    def pack(self, count, item):
        item_index = count - 1

        for index in range(self.current_index, self.current_index + count):
            byte_index = index // 8
            bit_index = index % 8
            byte_value = self.packet[byte_index]
            bit_value = 1 if (1 << item_index) & item > 0 else 0
            byte_value = byte_value | bit_value << (7 - bit_index)
            self.packet[byte_index] = byte_value
            item_index -= 1

        self.current_index += count
