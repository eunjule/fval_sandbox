﻿// INTEL CONFIDENTIAL

// Copyright 2019-2020 Intel Corporation.

// This software and the related documents are Intel copyrighted materials,
// and your use of them is governed by the express license under which they
// were provided to you ("License"). Unless the License provides otherwise,
// you may not use, modify, copy, publish, distribute, disclose or transmit
// this software or the related documents without Intel's prior written
// permission.

// This software and the related documents are provided as is, with no express
// or implied warranties, other than those that are expressly stated in the
// License.

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Altera.SSD.Utilities;

class CaptureDecoder
{
    static int Main ( string [ ] args )
    {
        return new CaptureDecoder ( ).Run ( args );
    }


    int Run ( string [ ] args )
    {

        //---------------
        // Preliminaries
        //---------------

        //DoIt ( );

        //return 1;

        Console.WriteLine ( );

        var hsOutputs = ParseArguments ( args );

        if ( hsOutputs == null )
        {
            ShowUsage ( );
            return 1;
        }

        var sFileName = args [ 0 ];

        sFileName = Path.GetFullPath ( sFileName );

        if ( ! File.Exists ( sFileName ) )
        {
            Console.WriteLine ( "    File [ {0} ] not found.", sFileName );
            Console.WriteLine ( );

            return 1;
        }

        Console.WriteLine ( "    Decoding [ {0} ]", sFileName );

        Console.WriteLine ( );

        var sw = Stopwatch.StartNew ( );

        if ( File.Exists ( "DecodedFileJustWritten.txt" ) )
            File.Delete (  "DecodedFileJustWritten.txt" );

        var bFromPackets = false;

        var cfu = new CaptureFileUtilities ( sFileName );

        if ( hsOutputs.Contains ( "Binary" ) )
        {
            Console.WriteLine ( "        Generating fileset..." );

            if ( ! bFromPackets )
                cfu.GenerateFilesetFromDMA ( );
        }

        var dictBytes = cfu.ReadBinDictionary ( ! bFromPackets );   



        var cdu = new CaptureDecodeUtilities ( cfu, dictBytes );

        cdu.MemosOn ( );

        cdu.UseCTVHeaders ( );
        cdu.UseTraces ( );

        var listActions = new List<Action> ( );

        if ( hsOutputs.Contains ( "Raw" ) )
            listActions.Add ( ( ) => cdu.GenerateRawCTVHeaderDecode ( bFromPackets ? StreamType.UNSPECIFIED : StreamType.CTV_DATA ) );

        if ( hsOutputs.Contains ( "Full" ) )
            listActions.Add ( ( ) => cdu.GenerateUnpackedCTVData ( stream : ! bFromPackets ? StreamType.CTV_DATA : StreamType.UNSPECIFIED, bSkipEOB : true, bSkipInvalid : false, bChipBreak : true, bBlockBreak : true, bCycleBreak : false ) );

        if ( hsOutputs.Contains ( "Compare" ) )
            listActions.Add ( ( ) => cdu.GenerateUnpackedActualCTVData ( stream : ! bFromPackets ? StreamType.CTV_DATA : StreamType.UNSPECIFIED ) );
            
        if ( hsOutputs.Contains ( "Wide" ) )
            listActions.Add ( ( ) => cdu.GenerateWideCaptureHeaderDecode ( ) );

        if ( hsOutputs.Contains ( "Trace" ) )
            listActions.Add ( ( ) => cdu.GenerateTraceStreamDecode ( ) );

        if ( hsOutputs.Contains ( "Error" ) )
            listActions.Add ( ( ) => cdu.GenerateWideErrorCaptureHeaderDecodes ( ) );

        if ( hsOutputs.Contains ( "Error" ) )
            listActions.Add ( ( ) => cdu.GenerateErrorEntryDecodes ( ) );

        if ( hsOutputs.Contains ( "Match" ) )
            listActions.Add ( ( ) => cdu.GenerateErrorEntryActualDecodes ( ) );



        if ( hsOutputs.Contains ( "Raw" ) )
        {
            Console.WriteLine ( "        Generating raw header decode csv..." );
            //cdu.GenerateRawCTVHeaderDecode ( bFromPackets ? StreamType.UNSPECIFIED : StreamType.CTV_DATA );
        }

        if ( hsOutputs.Contains ( "Full" ) )
        {
            Console.WriteLine ( "        Generating unpacked ctv data csv..." );
            //cdu.GenerateUnpackedCTVData ( stream : ! bFromPackets ? StreamType.CTV_DATA : StreamType.UNSPECIFIED, bSkipEOB : true, bSkipInvalid : false, bChipBreak : true, bBlockBreak : true, bCycleBreak : false );
        }

        if ( hsOutputs.Contains ( "Compare" ) )
        {
            Console.WriteLine ( "        Generating actual compare ctv data csv..." );
            //cdu.GenerateUnpackedActualCTVData ( stream : ! bFromPackets ? StreamType.CTV_DATA : StreamType.UNSPECIFIED );
        }
            
        if ( hsOutputs.Contains ( "Wide" ) )
        {
            Console.WriteLine ( "        Generating wide header csv..." );
            //cdu.GenerateWideCaptureHeaderDecode ( );
        }

        if ( hsOutputs.Contains ( "Trace" ) )
        {
            Console.WriteLine ( "        Generating trace stream csv..." );
            //cdu.GenerateTraceStreamDecode ( );
        }

        if ( hsOutputs.Contains ( "Error" ) )
        {
            Console.WriteLine ( "        Generating error stream csv's..." );
            //cdu.GenerateWideErrorCaptureHeaderDecodes ( );
        }

        if ( hsOutputs.Contains ( "Match" ) )
        {
            Console.WriteLine ( "        Generating actual compare error stream csv's..." );
            //cdu.GenerateErrorEntryActualDecodes ( );
        }

        TaskRunner.RunActionList ( listActions );



        Console.WriteLine ( );

        Console.WriteLine ( "    Done." );

        var dElapsed = sw.Elapsed.TotalSeconds;

        Console.WriteLine ( );

        Console.WriteLine ( "{0:N2} s", dElapsed );

        Console.WriteLine ( );

        return 0;
    }




    HashSet<string> ParseArguments ( string [ ] args )
    {
        if ( args.Length == 0 || args.Length > 2 )
            return null;

        var sValid = "wtrfcemba";
        var hsResult = new HashSet<string> ( ) { "Binary" };

        if ( args.Length < 2 )
        {
            //hsResult.Add ( "Compare" );
            return null;
        }

        if ( ! args [ 1 ].StartsWith ( "-" ) )
            return null;

        var sWork = args [ 1 ].Substring ( 1 );

        foreach ( char c in sWork )
        {
            if ( ! sValid.Contains ( c ) )
                return null;
        }

        if ( sWork.Contains ( 'a' ) )
            sWork = sValid;

        foreach ( var c in sWork )
        {
            switch ( c )
            {
                case 'w' : hsResult.Add ( "Wide" );      break;
                case 't' : hsResult.Add ( "Trace" );     break;
                case 'r' : hsResult.Add ( "Raw" );       break;
                case 'f' : hsResult.Add ( "Full" );      break;
                case 'c' : hsResult.Add ( "Compare" );   break;
                case 'e' : hsResult.Add ( "Error" );     break;
                case 'm' : hsResult.Add ( "Match" );     break;
                case 'b' : hsResult.Add ( "Binary" );    break;
            }
        }

        return hsResult;
    }



    void ShowUsage ( )
    {
        Console.WriteLine ( "    Usage : CaptureDecoder filename [-[w][t][r][f][c][e][m][b][a]]" );
        Console.WriteLine ( );
        Console.WriteLine ( "        w     Generate wide header CSV." );
        Console.WriteLine ( "        t     Generate trace stream CSV." );
        Console.WriteLine ( "        r     Generate raw header decode CSV." );
        Console.WriteLine ( "        f     Generate full CTV capture decode CSV." );
        Console.WriteLine ( "        c     Generate compare CTV capture decode CSV." );
        Console.WriteLine ( "        e     Generate full error capture decode CSVs." );
        Console.WriteLine ( "        m     Generate compare error capture decode CSVs." );
        Console.WriteLine ( "        b     Generate binary files (implicit in other options)." );
        Console.WriteLine ( "        a     Generate all file types." );
        Console.WriteLine ( );
        Console.WriteLine ( "        Example: CaptureDecoder streamfile.txt -wtr" );
        Console.WriteLine ( );
        //Console.WriteLine ( "        Note: Second parameter defaults to -c if not specified." );
        //Console.WriteLine ( );
    }




    void DoIt ( )
    {
        var aby = File.ReadAllBytes ( @"\\orststtde02\hdmt\Users\Sandy\FromAfework\Run2\StreamCapture\failData_PM0_S0.bin" );

        var abyWork = new byte [ aby.Length / 2 ];

        int iTotalPackets = aby.Length / 32;

        int iDestOffset = 0;

        int iSourceOffset = 0;

        for ( int iPacket = 0; iPacket < iTotalPackets; iPacket++ )
        {
            if ( iPacket % 2 == 1 )
            {
                for ( int j = 0; j < 32; j++ )
                    abyWork [ iDestOffset++ ] = aby [ iSourceOffset++ ];
            }

            else
                iSourceOffset += 32;
        }

        File.WriteAllBytes ( "Work.bin", abyWork );

        

        var cfu = new CaptureFileUtilities ( "bogus" );

        var dictBytes = new Dictionary<string,byte[]> ( );

        dictBytes [ "CTV_HEADER" ] = abyWork;

        var cdu = new CaptureDecodeUtilities ( cfu, dictBytes );

        cdu.GenerateWideCaptureHeaderDecode ( );
    }
}
