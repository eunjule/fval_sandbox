﻿// INTEL CONFIDENTIAL

// Copyright 2019-2020 Intel Corporation.

// This software and the related documents are Intel copyrighted materials,
// and your use of them is governed by the express license under which they
// were provided to you ("License"). Unless the License provides otherwise,
// you may not use, modify, copy, publish, distribute, disclose or transmit
// this software or the related documents without Intel's prior written
// permission.

// This software and the related documents are provided as is, with no express
// or implied warranties, other than those that are expressly stated in the
// License.

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Altera.SSD.Utilities;

class CaptureFileUtilities
{
    public CaptureFileUtilities ( string sFileName )
    {
        FileName = sFileName;

        FullPath       = Path.GetFullPath ( sFileName );
        FullStem       = Path.Combine ( Path.GetDirectoryName ( sFileName ), Path.GetFileNameWithoutExtension ( FullPath ) );
        DecodeFolder   = Path.Combine ( Directory.GetParent ( FullPath ).FullName, "DecodedFolder" );
        FullStemDecode = Path.Combine ( DecodeFolder, Path.GetFileNameWithoutExtension ( FullStem ) );

        FillStringToByteDictionary ( );
    }



    public bool GenerateFilesetFromDMA ( )
    {
        var sFileName = FullPath;
        var sExtension = Path.GetExtension ( sFileName );

        if ( sExtension == "" )
            sFileName = FullPath + ".txt";

        var asLines = File.ReadAllLines ( sFileName );

        //asLines = asLines.Select ( s => s.Trim ( ) )._Where ( s => s != "" );

        var dictBlocks = GenerateBlockDictionary ( asLines );

        if ( dictBlocks == null )
            return false;

        var dictBytes = GenerateBinValuesParallel ( dictBlocks );

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        var sWorkName = Path.Combine ( DecodeFolder, Path.GetFileNameWithoutExtension ( FullPath ) );

        CreateBinFiles ( sWorkName, dictBytes );

        //CreatePacketFiles ( sWorkName, dictBytes );

        return true;
    }



    Dictionary<string,List<string>> GenerateBlockDictionary ( string [ ] asLines )
    {
        var dictResult  = new Dictionary<string,List<string>> ( );
        var dictLengths = new Dictionary<string,int> ( );

        List<string> listWork    = null;
        List<string> listWorkAux = null;

        string sKeyOriginal = null;

        int iErrorSectionsSeen = 0;

        int i128s = 0;

        var asErrorSectionNames = new [ ] { "PSDB_0_PIN_0", "PSDB_0_PIN_1", "PSDB_1_PIN_0", "PSDB_1_PIN_1" };

        foreach ( var sLine in asLines )
        {
            if ( ! sLine.Contains ( ':' ) )
            {
                var asFields = sLine.Split ( new [ ] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

                listWork    = new List<string> ( );
                listWorkAux = new List<string> ( );

                var sKey = asFields [ 0 ];

                sKeyOriginal = sKey;

                int iDivisor = sKeyOriginal == "ERROR" ? 2 : 1;

                if ( sKeyOriginal == "ERROR" )
                    sKey = asErrorSectionNames [ iErrorSectionsSeen ];

                dictResult  [ sKey ] = listWork;
                dictLengths [ sKey ] = Int32.Parse ( asFields [ 1 ] ) / iDivisor;

                if ( sKeyOriginal == "ERROR" )
                {
                    sKey = asErrorSectionNames [ iErrorSectionsSeen++ ] + "_H";

                    dictResult  [ sKey ] = listWorkAux;
                    dictLengths [ sKey ] = Int32.Parse ( asFields [ 1 ] ) / iDivisor;
                }

                i128s = 0;
            }
            else
            {
                if ( sKeyOriginal != "ERROR" )
                    listWork.Add ( sLine );

                else
                {
                    if ( ( i128s / 2 ) % 2 == 0 )
                        listWork.Add ( sLine );

                    else
                        listWorkAux.Add ( sLine );
                }

                i128s++;
            }
        }

        foreach ( var sKey in dictLengths.Keys )
        {
            if ( dictResult [ sKey ].Count != dictLengths [ sKey ] * 2 && dictLengths [ sKey ] >= 4 )
            {
                Console.WriteLine ( "    Block size mismatch for {0}.", sKey );
                return null;
            }
        }

        return dictResult;
    }
    
    
    
    
    Dictionary<string,List<byte>> GenerateLineValues ( Dictionary<string,List<string>> dictBlocks )
    {
        var dictResult = new Dictionary<string,List<byte>> ( );

        List<byte> listWork = null;

        foreach ( var sKey in dictBlocks.Keys )
        {
            listWork = new List<byte> ( );

            dictResult [ sKey ] = listWork;

            foreach ( var sLine in dictBlocks [ sKey ] )
            {
                var asFields = sLine.Split ( ':' );

                var asValues = asFields [ 1 ].Split ( new [ ] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

                foreach ( var sValue in asValues )
                {
                    byte byValue = dictStringToByte [ sValue ];
                    listWork.Add ( byValue );
                }    
            }
        }

        return dictResult;
    }
    
    
    
    
    Dictionary<string,byte[]> GenerateBinValues ( Dictionary<string,List<string>> dictBlocks )
    {
        var dictResult = new Dictionary<string,byte []> ( );

        byte [ ]  abyWork = null;

        foreach ( var sKey in dictBlocks.Keys )
        {
            abyWork = new byte [ dictBlocks [ sKey ].Count * 16 ];

            dictResult [ sKey ] = abyWork;

            int iIndex = 0;

            foreach ( var sLine in dictBlocks [ sKey ] )
            {
                int iWorkPos = sLine.IndexOf ( ':' ) + 2;

                var asValues = new string [ 16 ];

                for ( int i = 0; i < 16; i++ )
                {
                    asValues [ i ] = sLine.Substring ( iWorkPos, 2 );

                    iWorkPos += 3;
                }

                //var asFields = sLine.Split ( ':' );

                //var asValues = asFields [ 1 ].Split ( new [ ] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

                foreach ( var sValue in asValues )
                {
                    byte byValue = dictStringToByte [ sValue ];

                    abyWork [ iIndex++ ] = byValue;
                }    
            }
        }

        return dictResult;
    }
    
    
    
    
    Dictionary<string,byte[]> GenerateBinValuesParallel ( Dictionary<string,List<string>> dictBlocks )
    {
        var dictResult = new Dictionary<string,byte []> ( );

        var listActions = new List<Action> ( );

        foreach ( var sKey in dictBlocks.Keys )
        {
            var sLocalKey = sKey;

            listActions.Add ( ( ) => { var thisResult = GenerateBinValuesForStream ( dictBlocks [ sLocalKey ] ); lock ( oLock ) { dictResult [ sLocalKey ] = thisResult; } } );
        }

        TaskRunner.RunActionList ( listActions );

        return dictResult;
    }




    byte [ ] GenerateBinValuesForStream ( List<string> liststr )
    {
        var abyWork = new byte [ liststr.Count * 16 ];

        int iIndex = 0;

        foreach ( var sLine in liststr )
        {
            int iWorkPos = sLine.IndexOf ( ':' ) + 2;

            for ( int i = 0; i < 16; i++ )
            {
                char cValue = sLine [ iWorkPos++ ];

                int iWork = 0;
                int iResult = 0;

                if ( cValue >= 'a' )
                    iWork = cValue - 'a' + 10;

                else
                    iWork = cValue - '0';

                iResult = iWork * 16;

                cValue = sLine [ iWorkPos++ ];

                if ( cValue >= 'a' )
                    iWork = cValue - 'a' + 10;

                else
                    iWork = cValue - '0';

                iResult += iWork;

                abyWork [ iIndex++ ] = ( byte ) iResult;

                iWorkPos++;
            }

            //var asFields = sLine.Split ( ':' );

            //var asValues = asFields [ 1 ].Split ( new [ ] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

            //foreach ( var sValue in asValues )
            //{
            //    byte byValue = dictStringToByte [ sValue ];

            //    abyWork [ iIndex++ ] = byValue;
            //}
        }

        return abyWork;
    }
    
    
    
    
    //Dictionary<string,byte[]> GenerateBinValues ( Dictionary<string,List<byte>> dictValues )
    //{
    //    var dictResult = new Dictionary<string,byte []> ( );

    //    foreach ( var sKey in dictValues.Keys )
    //        dictResult [ sKey ] = dictValues [ sKey ].ToArray ( );

    //    return dictResult;
    //}




    void CreateBinFiles ( string sFileName, Dictionary<string,byte[]> dictBytes )
    {
        foreach ( var sKey in dictBytes.Keys )
        {
            var sOutFile = sFileName + "_" + sKey + ".bin";

            File.WriteAllBytes ( sOutFile, dictBytes [ sKey ] );

        }
    }




    void CreatePacketFiles ( string sFileName, Dictionary <string,byte[]> dictBytes )
    {
        var listActions = new List<Action> ( );

        foreach ( var sKey in dictBytes.Keys )
        {
            var sOutFile = sFileName + "_" + sKey + "_packets.txt";

            var sLocalKey = sKey;

            listActions.Add ( ( ) => CreatePacketFile ( sOutFile, dictBytes [ sLocalKey ] ) );
        }

        TaskRunner.RunActionList ( listActions );
    }



    void CreatePacketFile ( string sFileName, byte [ ] aby )
    {
        var abyWork = new byte [ 16 ];

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            int iLineCount = aby.Length / 16;

            for ( int i = 0; i < iLineCount; i++ )
            {
                int iStartIndex = i * 16 + 15;

                for ( int j = 0; j < 16; j++ )
                    abyWork [ j ] = aby [ iStartIndex-- ];

                for ( int j = 0; j < 4; j++ )
                {
                    for ( int k = 0; k < 4; k++ )
                    {
                        var byItem = abyWork [ j * 4 + k ];
                        sw.Write ( asByteToString [ byItem ] );
                    }

                    if ( j < 3 )
                        sw.Write ( " " );
                }

                sw.WriteLine ( );
            }
        }
    }



    public Dictionary<string,byte[]> ReadBinDictionary ( bool bAutoGather = false )
    {
        var dictResult = new Dictionary<string,byte[]> ( );

        var dictFileNames = GenerateAutoGatherFilenames ( bAutoGather );

        dictFileNames = dictFileNames.Where ( kv => File.Exists ( kv.Value ) ).ToDictionary ( kv => kv.Key, kv => kv.Value );

        foreach ( var kv in dictFileNames )
            dictResult [ kv.Key ] = File.ReadAllBytes ( kv.Value );

        return dictResult;
    }



    Dictionary<string,string> GenerateAutoGatherFilenames ( bool bAutoGather = false, string sSuffix = "", string sExtension = "bin" )
    {
        var dictResult = new Dictionary<string,string> ( );

        if ( ! bAutoGather )
            dictResult [ "" ] = FullStemDecode + "." + sExtension;

        else
        {
            var asStream = GetStreamNames ( );

            foreach ( var sStream in asStream )
                dictResult [ sStream ] = FullStemDecode + "_" + sStream + ( sSuffix == "" ? "" : "_" + sSuffix ) + "." + sExtension;
        }

        return dictResult;
    }



    void FillStringToByteDictionary ( )
    {
        for ( int i = 0; i < 256; i++ )
        {
            var sValue = String.Format ( "{0:x2}", ( byte ) i );

            dictStringToByte [ sValue ] = ( byte ) i;

            asByteToString [ i ] = sValue;
        }
    }




    string [ ] GetStreamNames ( )
    {
        return "TRACE CTV_HEADER CTV_DATA PSDB_0_PIN_0 PSDB_0_PIN_1 PSDB_1_PIN_0 PSDB_1_PIN_1 PSDB_0_PIN_0_H PSDB_0_PIN_1_H PSDB_1_PIN_0_H PSDB_1_PIN_1_H".Split ( ' ' );
    }




    object oLock = new Object ( );

    Dictionary<string,byte> dictStringToByte = new Dictionary<string,byte> ( );
    string [ ] asByteToString = new string [ 256 ];

    public string FileName       { get; set; }
    public string FullPath       { get; set; }
    public string FullStem       { get; set; }
    public string DecodeFolder   { get; set; }
    public string FullStemDecode { get; set; }
}
