﻿// INTEL CONFIDENTIAL

// Copyright 2019-2020 Intel Corporation.

// This software and the related documents are Intel copyrighted materials,
// and your use of them is governed by the express license under which they
// were provided to you ("License"). Unless the License provides otherwise,
// you may not use, modify, copy, publish, distribute, disclose or transmit
// this software or the related documents without Intel's prior written
// permission.

// This software and the related documents are provided as is, with no express
// or implied warranties, other than those that are expressly stated in the
// License.

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Altera.SSD.Utilities;

enum CaptureWordType { ERROR, CTV };

enum StreamType { TRACE, CTV_HEADER, CTV_DATA, PSDB_0_PIN_0, PSDB_0_PIN_1, PSDB_1_PIN_0, PSDB_1_PIN_1, PSDB_0_PIN_0_H, PSDB_0_PIN_1_H, PSDB_1_PIN_0_H, PSDB_1_PIN_1_H, UNSPECIFIED, NONE };

class CommonHeader
{
    public int             PacketIndex      { get; set; }
    public CaptureWordType CaptureWordType  { get; set; }
    public int             OriginatingChip  { get; set; }
    public int             OriginatingSlice { get; set; }
}




class RawCTVDataHeader
{
    public int             PacketIndex         { get; set; }
    public CaptureWordType CaptureWordType     { get; set; }
    public int             OriginatingChip     { get; set; }
    public int             OriginatingSlice    { get; set; }
    public bool            IsCycleCount        { get; set; }
    public int             FullCycleCount      { get; set; }
    public bool            EndOfBurst          { get; set; }
    public byte            EntryIsWide         { get; set; }
    public int             ValidCycles         { get; set; }
    public int             RelativeCycleCount  { get; set; }
    public int             BlockCount          { get; set; }



    public string GetCSVLine ( )
    {
        var sResult = String.Format ( "{0},{1},{2},{3},{4},{5},{6},0x{7:x2},0x{8:x8},{9},{10}", PacketIndex,
                                                                                                ( int ) CaptureWordType,
                                                                                                OriginatingChip,
                                                                                                OriginatingSlice,
                                                                                                IsCycleCount ? 1 : 0,
                                                                                                FullCycleCount,
                                                                                                EndOfBurst ? 1 : 0,
                                                                                                EntryIsWide,
                                                                                                ValidCycles,
                                                                                                RelativeCycleCount,
                                                                                                BlockCount );

        return sResult;
    }

}




class WideCaptureHeader
{
    public int             PacketIndex         { get; set; }
    public int             PatternCounter      { get; set; }
    public int             TraceIndex          { get; set; }
    public int             Reserved            { get; set; }
    public int             UserCycleCount      { get; set; }
    public int             UserLog1            { get; set; }
    public int             RepeatCycleCount    { get; set; }
    public int             PatternCycleCount   { get; set; }
    public int             VectorAddress       { get; set; }
    public int             LastPCallAddress    { get; set; }



    public string GetCSVLine ( )
    {
        var sResult = String.Format ( "{0},{1},{2},{3},{4},{5},{6},0x{7:x8},0x{8:x8}", PacketIndex,
                                                                                       PatternCounter,
                                                                                       TraceIndex,
                                                                                       UserCycleCount,
                                                                                       UserLog1,
                                                                                       RepeatCycleCount,
                                                                                       PatternCycleCount,
                                                                                       VectorAddress,
                                                                                       LastPCallAddress );

        return sResult;
    }
}




class TraceEntry
{
    public int             PacketIndex         { get; set; }
    public long            Reserved2A          { get; set; }
    public int             Reserved2B          { get; set; }
    public int             PatternID           { get; set; }
    public int             JumpType            { get; set; }
    public int             Reserved1           { get; set; }
    public long            CaptureThisPin      { get; set; }
    public int             ToAddress           { get; set; }
    public int             FromAddress         { get; set; }



    public string GetCSVLine ( )
    {
        var dictJumpTypes = new Dictionary<int,string>  (  ) { { 0, "none" }, { 1, "goto_i" }, { 2, "call_i" }, { 4, "pcall_i" }, { 8, "ret" }, { 17, "goto_r" }, { 18, "call_r" }, { 20, "pcall_r" }, { 24, "clrstk" } };

        var sResult = String.Format ( "{0},{1},{2},0x{3:x9},0x{4:x8},0x{5:x8}", PacketIndex,
                                                                                PatternID,
                                                                                dictJumpTypes [ JumpType ],
                                                                                CaptureThisPin,
                                                                                ToAddress,
                                                                                FromAddress );

        return sResult;
    }
}




class ErrorEntry
{
    public int             PacketIndex         { get; set; }
    public CaptureWordType CaptureWordType     { get; set; }
    public int             OriginatingChip     { get; set; }
    public int             OriginatingSlice    { get; set; }
    public bool            HeadersMatch        { get; set; }
    public bool            EndOfBurst          { get; set; }
    public bool            AlarmCondition      { get; set; }
    public long            CycleNumber         { get; set; }
    public bool            IsValid             { get; set; }
    public long [ ]        PinStates           { get; set; }
    public long [ ]        ErrorStates         { get; set; }



    public string GetCSVLine ( )
    {
        var astr = new String [ 8 ];

        for ( int i = 0; i < 4; i++ )
        {
            var bsr = new BinStringRepresentation ( 35, PinStates [ i ] );

            astr [ i ] = bsr.ToString ( );

            bsr = new BinStringRepresentation ( 35, ErrorStates [ i ] );

            astr [ i + 4 ] = bsr.ToString ( );
        }

        var sResult = String.Format ( "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24}", 
                                      PacketIndex,
                                      ( int ) CaptureWordType,
                                      OriginatingChip,
                                      OriginatingSlice,
                                      HeadersMatch ? 1 : 0,
                                      EndOfBurst ? 1 : 0,
                                      AlarmCondition ? 1 : 0,
                                      CycleNumber,
                                      IsValid ? 1 :0,
                                      astr [ 0 ],
                                      PinStates [ 0 ],
                                      astr [ 4 ],
                                      ErrorStates [ 0 ],
                                      astr [ 1 ],
                                      PinStates [ 1 ],
                                      astr [ 5 ],
                                      ErrorStates [ 1 ],
                                      astr [ 2 ],
                                      PinStates [ 2 ],
                                      astr [ 6 ],
                                      ErrorStates [ 2 ],
                                      astr [ 3 ],
                                      PinStates [ 3 ],
                                      astr [ 7 ],
                                      ErrorStates [ 3 ]
                                    );

        return sResult;
    }



    public string GetExpectCSVLine ( )
    {
        var sResult = String.Format ( "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                                      OriginatingChip,
                                      OriginatingSlice,
                                      HeadersMatch ? 1 : 0,
                                      CycleNumber,
                                      PinStates   [ 0 ],
                                      ErrorStates [ 0 ],
                                      PinStates   [ 1 ],
                                      ErrorStates [ 1 ],
                                      PinStates   [ 2 ],
                                      ErrorStates [ 2 ],
                                      PinStates   [ 3 ],
                                      ErrorStates [ 3 ]
                                    );

        return sResult;
    }
}




class CaptureDecodeUtilities
{
    public CaptureDecodeUtilities ( CaptureFileUtilities cfuInstance, Dictionary<string,byte[]> dictBytes = null )
    {
        CFUInstance    = cfuInstance;
        DictBytes      = dictBytes;
        DecodeFolder   = Path.Combine ( CFUInstance.DecodeFolder, "Decodes" );
        FullStemDecode = Path.Combine ( DecodeFolder, Path.GetFileNameWithoutExtension ( CFUInstance.FullStem ) );

        if ( DictBytes == null )
            DictBytes = CFUInstance.ReadBinDictionary ( true );
    }




    public int GetPacketCount ( StreamType stream )
    {
        lastStream = stream;

        var aby = DictBytes [ dictStream [ stream ] ];

        int iResult = aby.Length / 32;

        return iResult;
    }




    public byte [ ] ExtractPacket ( int iIndex, StreamType stream = StreamType.NONE )
    {
        byte [ ] abyResult = new byte [ 32 ];

        StreamType workStream = stream == StreamType.NONE ? lastStream : stream;

        int iWorkIndex = 31;

        int iStart = iIndex * 32;

        byte [ ] abyWork = DictBytes [ dictStream [ workStream ] ];

        for ( int i = 0; i < 32; i++ )
            abyResult [ i ] = abyWork [ iStart + iWorkIndex-- ];

        iLastIndex    = iIndex;
        abyLastPacket = abyResult;

        return abyResult;
    }




    public byte [ ] ExtractQuadErrorRecord ( int iIndex, StreamType stream = StreamType.NONE )
    {
        byte [ ] abyResult = new byte [ 64 ];

        StreamType workStream = stream == StreamType.NONE ? lastStream : stream;

        int iWorkIndex = 63;

        int iStart = iIndex * 64;

        byte [ ] abyWork = DictBytes [ dictStream [ workStream ] ];

        for ( int i = 0; i < 64; i++ )
            abyResult [ i ] = abyWork [ iStart + iWorkIndex-- ];

        iLastIndex    = iIndex;
        abyLastPacket = abyResult;

        return abyResult;
    }




    public byte [ ] Extract64FromPacket ( int iIndex, byte [ ] abyPacket = null )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var abyResult = new byte [ 8 ];

        int iStart = iIndex * 8;

        for ( int i = 0; i < 8; i++ )
            abyResult [ i ] = abyWorkPacket [ iStart + i ];

        return abyResult;
    }




    public long ExtractInt64FromPacket ( int iIndex, byte [ ] abyPacket = null )
    {
        byte [ ] abyWork = Extract64FromPacket ( iIndex, abyPacket );

        long lResult = 0;

        for ( int i = 0; i < 8; i++ )
            lResult = lResult << 8 | abyWork [ i ];

        return lResult;
    }




    public byte [ ] Extract32FromPacket ( int iIndex, byte [ ] abyPacket = null )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var abyResult = new byte [ 4 ];

        int iStart = iIndex * 4;

        for ( int i = 0; i < 4; i++ )
            abyResult [ i ] = abyWorkPacket [ iStart + i ];

        return abyResult;
    }




    public int ExtractInt32FromPacket ( int iIndex, byte [ ] abyPacket = null )
    {
        byte [ ] abyWork = Extract32FromPacket ( iIndex, abyPacket );

        int iResult = 0;

        for ( int i = 0; i < 4; i++ )
            iResult = iResult << 8 | abyWork [ i ];

        return iResult;
    }




    public byte [ ] Extract24FromPacket ( int iIndex, byte [ ] abyPacket = null )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var abyResult = new byte [ 3 ];

        int iStart = iIndex * 3;

        for ( int i = 0; i < 3; i++ )
            abyResult [ i ] = abyWorkPacket [ iStart + i ];

        return abyResult;
    }




    public int ExtractInt24FromPacket ( int iIndex, byte [ ] abyPacket = null )
    {
        byte [ ] abyWork = Extract24FromPacket ( iIndex, abyPacket );

        int iResult = 0;

        for ( int i = 0; i < 3; i++ )
            iResult = iResult << 8 | abyWork [ i ];

        return iResult;
    }




    public bool ErrorHeadersMatch ( byte [ ] abyPacket )
    {   
        for ( int iHeaderIndex = 1; iHeaderIndex < 4; iHeaderIndex++ )
        {
            for ( int iByteIndex = 0; iByteIndex < 8; iByteIndex++ )
            {
                int iBase = iHeaderIndex * 16;

                if ( abyPacket [ 0 + iByteIndex ] != abyPacket [ iBase + iByteIndex ] )
                    return false;
            }
        }

        return true;
    }




    public CommonHeader GetCommonHeader ( byte [ ] abyPacket = null, int iIndex = -1 )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var cfe = new CaptureFieldExtractor ( abyWorkPacket );

        var result = new CommonHeader ( );

        result.PacketIndex = abyPacket == null ? iLastIndex : iIndex;

        result.CaptureWordType  = ( CaptureWordType ) cfe.Extract ( 2 );
        result.OriginatingChip  = cfe.Extract ( 2 );
        result.OriginatingSlice = cfe.Extract ( 3 );

        return result;
    }



    public RawCTVDataHeader GetRawCTVDataHeader ( byte [ ] abyPacket = null, int iIndex = -1 )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var cfe = new CaptureFieldExtractor ( abyWorkPacket );

        var result = new RawCTVDataHeader ( );

        result.PacketIndex = abyPacket == null ? iLastIndex : iIndex;

        result.CaptureWordType  = ( CaptureWordType ) cfe.Extract ( 2 );
        result.OriginatingChip  = cfe.Extract ( 2 );
        result.OriginatingSlice = cfe.Extract ( 3 );
        result.IsCycleCount     = cfe.Extract ( 1 ) == 1;

        if ( result.IsCycleCount )
        {
            int iCurrentIndex = cfe.CurrentIndex;

            result.FullCycleCount = cfe.Extract ( 48 );
            cfe.CurrentIndex = iCurrentIndex;
        }

        result.EndOfBurst         = cfe.Extract ( 1 ) == 1;
        result.EntryIsWide        = ( byte ) cfe.Extract ( 8 );
        result.ValidCycles        = cfe.Extract ( 32 );
        result.RelativeCycleCount = cfe.Extract ( 9 );
        result.BlockCount         = cfe.Extract ( 6 );

        return result;
    }



    public WideCaptureHeader GetWideCaptureHeader ( byte [ ] abyPacket = null, int iIndex = -1 )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var cfe = new CaptureFieldExtractor ( abyWorkPacket );

        var result = new WideCaptureHeader ( );

        result.PacketIndex = abyPacket == null ? iLastIndex : iIndex;

        result.PatternCounter     = cfe.Extract ( 24 );
        result.TraceIndex         = cfe.Extract ( 24 );
        result.Reserved           = cfe.Extract ( 16 );
        result.UserCycleCount     = cfe.Extract ( 32 );
        result.UserLog1           = cfe.Extract ( 32 );
        result.RepeatCycleCount   = cfe.Extract ( 32 );
        result.PatternCycleCount  = cfe.Extract ( 32 );
        result.VectorAddress      = cfe.Extract ( 32 );
        result.LastPCallAddress   = cfe.Extract ( 32 );

        return result;
    }



    public TraceEntry GetTraceEntry ( byte [ ] abyPacket = null, int iIndex = -1 )
    {
        var abyWorkPacket = abyPacket == null ? abyLastPacket : abyPacket;

        var cfe = new CaptureFieldExtractor ( abyWorkPacket );

        var result = new TraceEntry ( );

        result.PacketIndex = abyPacket == null ? iLastIndex : iIndex;

        result.Reserved2A         = cfe.ExtractLong ( 64 );
        result.Reserved2B         = cfe.Extract     ( 32 );
        result.PatternID          = cfe.Extract     ( 32 );
        result.JumpType           = cfe.Extract     (  5 );
        result.Reserved1          = cfe.Extract     ( 21 );
        result.CaptureThisPin     = cfe.ExtractLong ( 38 );
        result.ToAddress          = cfe.Extract     ( 32 );
        result.FromAddress        = cfe.Extract     ( 32 );

        return result;
    }



    public ErrorEntry GetErrorEntry ( byte [ ] abyPacket, int iIndex )
    {
        var cfe = new CaptureFieldExtractor ( abyPacket );

        var result = new ErrorEntry ( );

        result.PacketIndex = iIndex;

        result.CaptureWordType  = ( CaptureWordType ) cfe.Extract ( 2 );
        result.OriginatingChip  = cfe.Extract ( 2 );
        result.OriginatingSlice = cfe.Extract ( 3 );
        result.HeadersMatch     = ErrorHeadersMatch ( abyPacket );
        result.EndOfBurst       = cfe.Extract ( 1 ) == 1;
        result.AlarmCondition   = cfe.Extract ( 1 ) == 1;
        result.CycleNumber      = cfe.ExtractLong ( 48 );
        result.IsValid          = cfe.Extract ( 1 ) == 1;

        result.PinStates   = new long [ 4 ];
        result.ErrorStates = new long [ 4 ];

        for ( int i = 0, j = 3; i < 3; i++, j-- )
        {
            long lUpperPinState    = cfe.Extract ( 3 );
            long lUpperErrorState  = cfe.Extract ( 3 );

            long lPinState        = cfe.Extract ( 32 );
            long lErrorState      = cfe.Extract ( 32 );

            lPinState   |= lUpperPinState << 32;
            lErrorState |= lUpperErrorState << 32;

            result.PinStates   [ j ] = lPinState;
            result.ErrorStates [ j ] = lErrorState;

            if ( i < 3 )
                cfe.ExtractLong ( 58 );
        }

        return result;
    }




    public int ExtractUserCycleCount ( byte [ ] abyPacket )
    {
        int iResult = ExtractInt32FromPacket ( 2, abyPacket );

        return iResult;
    }




    public int ExtractTraceIndex ( byte [ ] abyPacket )
    {
        int iResult = ExtractInt24FromPacket ( 1, abyPacket );

        return iResult;
    }




    public void UseCTVHeaders ( )
    {
        int iPacketCount = GetPacketCount ( StreamType.CTV_HEADER );

        HeaderCycles = new int [ iPacketCount ];
        HeaderTraces = new int [ iPacketCount ];

        for ( int i = 0; i < iPacketCount; i++ )
        {
            var packet = ExtractPacket ( i, StreamType.CTV_HEADER );

            HeaderCycles [ i ] = ExtractUserCycleCount ( packet );
            HeaderTraces [ i ] = ExtractTraceIndex ( packet );
        }
    }




    public void UseTraces ( )
    {
        int iPacketCount = GetPacketCount ( StreamType.TRACE );

        Traces = new TraceEntry [ iPacketCount ];

        for ( int i = 0; i < iPacketCount; i++ )
        {
            var packet = ExtractPacket ( i, StreamType.TRACE );

            var traceEntry = GetTraceEntry ( );

            Traces [ i ] = traceEntry;
        }
    }



    public void ClearMemos ( )
    {
        if ( CaptureFieldExtractor.memo != null )
            CaptureFieldExtractor.memo.Clear ( );

        if ( BinStringRepresentation.dictMemo != null )
            BinStringRepresentation.dictMemo.Clear ( );

        if ( CycleUnpacker.dictMemo != null )
            CycleUnpacker.dictMemo.Clear ( );
    }



    public void MemosOn ( )
    {
        //CaptureFieldExtractor.memo = new Dictionary<CFEKey,long> ( );
        //BinStringRepresentation.dictMemo = new Dictionary<Tuple<int,long,char,char>,string> ( );
        //CycleUnpacker.dictMemo = new Dictionary<CUKey,Tuple<char[][][],char[][][]>> ( );
    }



    public void MemosOff ( )
    {
        CaptureFieldExtractor.memo = null;
        BinStringRepresentation.dictMemo = null;
        CycleUnpacker.dictMemo = null;
    }




    public void GenerateRawCTVHeaderDecode ( StreamType stream = StreamType.CTV_DATA )
    {
        var sFileName = FullStemDecode + "_raw_ctv_data_headers.csv";

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            ClearMemos ( );

            sw.WriteLine ( "index,type,chip,slice,is_count,full_count,eob,wide_mask,valid_cycles,relative_count,block_count,blank,data0_H,data0_L,data1_H,data1_L,data2_H,data2_L,,delta,delta_correct,,mod_4" );

            int iPacketCount = GetPacketCount ( stream );

            int iCurrentIndex = 0;

            var aiRelativeCycles = new [ ] { -1, -1, -1, -1 };
            var aiCycleCount     = new [ ] { -1, -1, -1, -1 };

            while ( iCurrentIndex < iPacketCount )
            {
                var packet = ExtractPacket ( iCurrentIndex, stream );

                var commonHeader = GetCommonHeader ( packet, iCurrentIndex );

                if ( commonHeader.CaptureWordType == CaptureWordType.ERROR )
                {
                    iCurrentIndex += 2;
                    continue;
                }

                var rawCTVHeader = GetRawCTVDataHeader ( packet, iCurrentIndex );

                var sWork = rawCTVHeader.GetCSVLine ( );

                ulong ul1 = ( ulong ) ExtractInt64FromPacket ( 1, packet );
                ulong ul2 = ( ulong ) ExtractInt64FromPacket ( 2, packet );
                ulong ul3 = ( ulong ) ExtractInt64FromPacket ( 3, packet );

                sWork = String.Format ( "{0},,0x{1:x8},0x{2:x8},0x{3:x8},0x{4:x8},0x{5:x8},0x{6:x8}", sWork, ul1 >> 32, ul1 & 0xFFFFFFFF, ul2 >> 32, ul2 & 0xFFFFFFFF, ul3 >> 32, ul3 & 0xFFFFFFFF );

                int iDelta = 0;

                if ( rawCTVHeader.IsCycleCount )
                {
                    iDelta = aiCycleCount [ rawCTVHeader.OriginatingChip ] < 0 ? 512 : rawCTVHeader.FullCycleCount- aiCycleCount [ rawCTVHeader.OriginatingChip ];
                    aiCycleCount [ rawCTVHeader.OriginatingChip ] = rawCTVHeader.FullCycleCount;
                }

                else
                {
                    iDelta = aiRelativeCycles [ rawCTVHeader.OriginatingChip ] < 0 ? 8 : rawCTVHeader.RelativeCycleCount- aiRelativeCycles [ rawCTVHeader.OriginatingChip ];
                    aiRelativeCycles [ rawCTVHeader.OriginatingChip ] = rawCTVHeader.RelativeCycleCount;

                    if ( iDelta == -504 )
                        iDelta = 8;
                }

                bool bDeltaCorrect = rawCTVHeader.IsCycleCount ? iDelta == 512 : iDelta == 8;

                int iMod4 = ( rawCTVHeader.RelativeCycleCount / 8 ) % 4;

                sWork = String.Format ( "{0},,{1},{2},,{3}", sWork, iDelta, bDeltaCorrect ? "TRUE" : "FALSE", iMod4 );

                sw.WriteLine ( sWork );

                iCurrentIndex += rawCTVHeader.BlockCount / 4 + 1;
            }
        }
    }




    public void GenerateUnpackedCTVData ( int iPolarity = 0, StreamType stream = StreamType.CTV_DATA, bool bSkipInvalid = false, bool bSkipEOB = true, bool bBlockBreak = false, bool bCycleBreak = false, bool bChipBreak = false )
    {
        GenerateUnpackedCTVDataGeneral ( iPolarity, stream, bSkipInvalid, bSkipEOB, bBlockBreak, bCycleBreak, bChipBreak, true );
    }




    public void GenerateUnpackedActualCTVData ( int iPolarity = 0, StreamType stream = StreamType.CTV_DATA )
    {
        GenerateUnpackedCTVDataGeneral ( iPolarity, stream, true, true, false, false, false, false );
    }



    public void GenerateUnpackedCTVDataGeneral ( int iPolarity = 0, StreamType stream = StreamType.CTV_DATA, bool bSkipInvalid = false, bool bSkipEOB = true, bool bBlockBreak = false, bool bCycleBreak = false, bool bChipBreak = false, bool bFullFormat = true )
    {
        var sFileName = FullStemDecode + ( bFullFormat ? "_unpacked_ctv_data.csv" : "_unpacked_actual_ctv_data.csv" );

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            int iFormatIndex = bFullFormat ? 1 : 0;

            ClearMemos ( );

            if ( CycleUnpacker.CTVHeaderCycles != null )
            {
                for ( int i = 0; i < 4; i++ )
                    CycleUnpacker.CTVHeaderCycleIndex [ iFormatIndex ] [ i ] = 0;
            }

            int iPacketCount = GetPacketCount ( stream );

            int iCurrentIndex = 0;

            var aiFullCycleCount = new [ ] { 0, 0, 0, 0 };

            var alistPacket = Enumerable.Range ( 0, 4 )._Select ( i => new List<byte[]> ( ) );

            RawCTVDataHeader rawCTVHeader = null;

            while ( iCurrentIndex < iPacketCount )
            {
                var packet = ExtractPacket ( iCurrentIndex, stream );

                var commonHeader = GetCommonHeader ( packet, iCurrentIndex );

                if ( commonHeader.CaptureWordType == CaptureWordType.ERROR )
                {
                    iCurrentIndex += 2;
                    continue;
                }

                rawCTVHeader = GetRawCTVDataHeader ( packet, iCurrentIndex );

                alistPacket [ rawCTVHeader.OriginatingChip ].Add ( packet );

                iCurrentIndex += 1;

                if ( rawCTVHeader.BlockCount > 3 )
                {
                    int iDataPacketCount = rawCTVHeader.BlockCount / 4;

                    for ( int i = 0; i < iDataPacketCount; i++ )
                    {
                        packet = ExtractPacket ( iCurrentIndex, stream );
                        
                        alistPacket [ rawCTVHeader.OriginatingChip ].Add ( packet );

                        iCurrentIndex += 1;
                    }
                }
            }




            CycleUnpacker.SetCTVHeaderCycles ( HeaderCycles );
            CycleUnpacker.SetCTVHeaderTraces ( HeaderTraces );
            CycleUnpacker.SetTraces          ( Traces       );

            bool bFirst = true;

            for ( int iChip = 0; iChip < 4; iChip++ )
            {
                bool bFirstBlock = true;

                int iLastCycle = -1;

                int iSinceInvalid = 1;

                if ( alistPacket [ iChip ].Count == 0 )
                    continue;

                if ( ! bFirst && bChipBreak )
                {
                    sw.WriteLine ( );
                    sw.WriteLine ( );
                    sw.WriteLine ( );
                }

                iCurrentIndex = 0;

                while ( iCurrentIndex < alistPacket [ iChip ].Count )
                {
                    var packet = alistPacket [ iChip ] [ iCurrentIndex ];

                    rawCTVHeader = GetRawCTVDataHeader ( packet, iCurrentIndex );

                    if ( rawCTVHeader.IsCycleCount || bSkipEOB && rawCTVHeader.EndOfBurst )
                    {
                        aiFullCycleCount [ rawCTVHeader.OriginatingChip ] = rawCTVHeader.FullCycleCount;
                        iCurrentIndex++;
                        continue;
                    }

                    if ( ! bFirstBlock && bBlockBreak )
                        sw.WriteLine ( );

                    bFirstBlock = false;

                    CycleUnpacker.SetPolarity ( iPolarity, rawCTVHeader.BlockCount );

                    var cu = new CycleUnpacker ( rawCTVHeader, aiFullCycleCount [ rawCTVHeader.OriginatingChip ], iCurrentIndex );

                    if ( bFirst && bFullFormat )
                    {
                        var sCSVHeader = bFullFormat ? cu.GetCSVHeader ( ) : cu.GetExpectCSVHeader ( );

                        sw.WriteLine ( sCSVHeader );

                        bFirst = false;
                    }

                    for ( int i = 0; i < rawCTVHeader.BlockCount; i++ )
                    {
                        if ( i > 0 && i + 1 % 4 == 0 )
                        {
                            iCurrentIndex += 1;

                            packet = alistPacket [ iChip ] [ iCurrentIndex ];
                        }

                        int iExtractIndex = ( i + 1 ) % 4;

                        var abyBlock = Extract64FromPacket ( iExtractIndex, packet );

                        cu.AddBlock ( abyBlock );
                    }


                    while ( true )
                    {
                        bool bDoForceInvalid = false;

                        if ( iSinceInvalid == 25 || iSinceInvalid == 26 )
                        {
                            bDoForceInvalid = true;

                            if ( iSinceInvalid == 26 )
                                iSinceInvalid = 0;
                        }

                        iSinceInvalid++;

                        var tuResult = bFullFormat ? cu.GetNextCycle ( bForceInvalid : false && bDoForceInvalid ) : cu.GetExpectNextCycle ( bForceInvalid : false && bDoForceInvalid );

                        int    iCycleNumber = tuResult.Item1;
                        bool   bValid       = tuResult.Item2;
                        string sCurrent     = tuResult.Item3;

                        if ( sCurrent == null )
                            break;

                        if ( ! bValid && bSkipInvalid )
                            continue;

                        if ( iCycleNumber != iLastCycle && iLastCycle >= 0 && bCycleBreak && ! bBlockBreak )
                            sw.WriteLine ( );

                        iLastCycle = iCycleNumber;

                        sw.WriteLine ( sCurrent );
                    }

                    iCurrentIndex++;
                }
            }
        }

        File.WriteAllText ( "DecodedFileJustWritten.txt", sFileName + Environment.NewLine );

    }




    public void GenerateWideCaptureHeaderDecode ( StreamType stream = StreamType.CTV_HEADER )
    {
        var sFileName = FullStemDecode + " _wide_ctv_headers.csv";

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            sw.WriteLine ( "index,pattern_counter,trace_index,user_cycle_count,user_log1,repeat_cycle_count,pattern_cycle_count,vector_address,last_pcall_address" );

            int iPacketCount = GetPacketCount ( stream );

            int iCurrentIndex = 0;

            while ( iCurrentIndex < iPacketCount )
            {
                var packet = ExtractPacket ( iCurrentIndex, stream );

                var wideCTVHeader = GetWideCaptureHeader ( packet, iCurrentIndex );

                var sWork = wideCTVHeader.GetCSVLine ( );

                sw.WriteLine ( sWork );

                iCurrentIndex += 1;
            }
        }
    }




    public void GenerateWideErrorCaptureHeaderDecode ( StreamWriter sw, int iPM, StreamType stream )
    {
        int iPacketCount = GetPacketCount ( stream );

        int iCurrentIndex = 0;

        while ( iCurrentIndex < iPacketCount )
        {
            var packet = ExtractPacket ( iCurrentIndex, stream );

            var wideCTVHeader = GetWideCaptureHeader ( packet, iCurrentIndex );

            var sWork = wideCTVHeader.GetCSVLine ( );

            sWork = String.Format ( "{0},{1}", iPM, sWork );

            sw.WriteLine ( sWork );

            iCurrentIndex += 1;
        }
    }




    public void GenerateWideErrorCaptureHeaderDecodes ( )
    {
        var sFileName = FullStemDecode + " _wide_error_headers.csv";

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            sw.WriteLine ( "chip,index,pattern_counter,trace_index,user_cycle_count,user_log1,repeat_cycle_count,pattern_cycle_count,vector_address,last_pcall_address" );

            for ( int iPM = 0; iPM < 4; iPM++ )
            {
                StreamType stream = iPM == 0 ? StreamType.PSDB_0_PIN_0_H : iPM == 1 ? StreamType.PSDB_0_PIN_1_H : iPM == 2 ? StreamType.PSDB_1_PIN_0_H : StreamType.PSDB_1_PIN_1_H;

                if ( ! DictBytes.ContainsKey ( dictStream [ stream  ] ) )
                    continue;

                GenerateWideErrorCaptureHeaderDecode ( sw, iPM, stream );
            }
        }
    }




    public void GenerateErrorEntryDecodeGeneral ( StreamWriter sw, StreamType stream, bool bFullFormat )
    {
        int iPacketCount = GetPacketCount ( stream ) / 2;

        int iCurrentIndex = 0;

        while ( iCurrentIndex < iPacketCount )
        {
            var packet = ExtractQuadErrorRecord ( iCurrentIndex, stream );

            var errorEntry = GetErrorEntry ( packet, iCurrentIndex * 2 );

            if ( errorEntry.EndOfBurst && ! bFullFormat )
            {
                iCurrentIndex += 1;
                continue;
            }

            var sWork = bFullFormat ? errorEntry.GetCSVLine ( ) : errorEntry.GetExpectCSVLine ( );

            sw.WriteLine ( sWork );

            iCurrentIndex += 1;
        }
    }




    public void GenerateErrorEntryDecodes ( )
    {
        var sFileName = FullStemDecode + " _error_entries.csv";

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            sw.WriteLine ( "index,type,chip,slice,headers_match,eob,alarm,cycle_count,is_valid,state_value_0,state_0,error_value_0,error_0,state_value_1,state_1,error_value_1,error_1,state_value_2,state_2,error_value_2,error_2,state_value_3,state_3,error_value_3,error_3" );

            for ( int iPM = 0; iPM < 4; iPM++ )
            {
                StreamType stream = iPM == 0 ? StreamType.PSDB_0_PIN_0 : iPM == 1 ? StreamType.PSDB_0_PIN_1 : iPM == 2 ? StreamType.PSDB_1_PIN_0 : StreamType.PSDB_1_PIN_1;

                if ( ! DictBytes.ContainsKey ( dictStream [ stream  ] ) )
                    continue;

                GenerateErrorEntryDecodeGeneral ( sw, stream, true );
            }
        }

        File.WriteAllText ( "DecodedFileJustWritten.txt", sFileName + Environment.NewLine );
    }




    public void GenerateErrorEntryActualDecodes ( )
    {
        var sFileName = FullStemDecode + "_error_actual_entries.csv";

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            //sw.WriteLine ( "chip,slice,headers_match,cycle_count,pin_0,error_0,pin_1,error_1,pin_2,error_2,pin_3,error_3" );

            for ( int iPM = 0; iPM < 4; iPM++ )
            {
                StreamType stream = iPM == 0 ? StreamType.PSDB_0_PIN_0 : iPM == 1 ? StreamType.PSDB_0_PIN_1 : iPM == 2 ? StreamType.PSDB_1_PIN_0 : StreamType.PSDB_1_PIN_1;

                if ( ! DictBytes.ContainsKey ( dictStream [ stream  ] ) )
                    continue;

                GenerateErrorEntryDecodeGeneral ( sw, stream, false );
            }
        }

        File.WriteAllText ( "DecodedFileJustWritten.txt", sFileName + Environment.NewLine );
    }




    public void GenerateTraceStreamDecode ( StreamType stream = StreamType.TRACE )
    {
        var sFileName = FullStemDecode + "_trace_entries.csv";

        if ( ! Directory.Exists ( DecodeFolder ) )
            Directory.CreateDirectory ( DecodeFolder );

        using ( var sw = new StreamWriter ( sFileName ) )
        {
            sw.WriteLine ( "index,pattern_id,jump_type,capture_this_pin,to_address,from_address" );

            int iPacketCount = GetPacketCount ( stream );

            int iCurrentIndex = 0;

            while ( iCurrentIndex < iPacketCount )
            {
                var packet = ExtractPacket ( iCurrentIndex, stream );

                var traceEntry = GetTraceEntry ( packet, iCurrentIndex );

                var sWork = traceEntry.GetCSVLine ( );

                sw.WriteLine ( sWork );

                iCurrentIndex += 1;
            }
        }
    }




    public CaptureFileUtilities      CFUInstance    { get; set; }
    public Dictionary<string,byte[]> DictBytes      { get; set; }
    public string                    DecodeFolder   { get; set; }
    public string                    FullStemDecode { get; set; }
    public int [ ]                   HeaderCycles   { get; set; }
    public int [ ]                   HeaderTraces   { get; set; }
    public TraceEntry [ ]            Traces         { get; set; }

    StreamType lastStream;
    byte [ ]   abyLastPacket;
    int        iLastIndex;

    Dictionary<StreamType,string> dictStream = new Dictionary<StreamType,string> ( ) { { StreamType.TRACE,          "TRACE" },
                                                                                       { StreamType.CTV_HEADER,     "CTV_HEADER" },
                                                                                       { StreamType.CTV_DATA,       "CTV_DATA" },
                                                                                       { StreamType.PSDB_0_PIN_0,   "PSDB_0_PIN_0" },
                                                                                       { StreamType.PSDB_0_PIN_1,   "PSDB_0_PIN_1" },
                                                                                       { StreamType.PSDB_1_PIN_0,   "PSDB_1_PIN_0" },
                                                                                       { StreamType.PSDB_1_PIN_1,   "PSDB_1_PIN_1" },
                                                                                       { StreamType.PSDB_0_PIN_0_H, "PSDB_0_PIN_0_H" },
                                                                                       { StreamType.PSDB_0_PIN_1_H, "PSDB_0_PIN_1_H" },
                                                                                       { StreamType.PSDB_1_PIN_0_H, "PSDB_1_PIN_0_H" },
                                                                                       { StreamType.PSDB_1_PIN_1_H, "PSDB_1_PIN_1_H" },
                                                                                       { StreamType.UNSPECIFIED,    "" } };
}




class CaptureFieldExtractor
{
    public static Dictionary<CFEKey,long> memo;

    public CaptureFieldExtractor ( byte [ ] abyPacket )
    {
        Packet = abyPacket;
    }



    public long ExtractLong ( int iCount )
    {
        long lResult = 0;

        CFEKey key = null;

        if ( memo != null )
        {
            key = new CFEKey ( Packet, CurrentIndex, iCount );

            if ( memo.ContainsKey ( key ) )
            {
                lResult = memo [ key ];
                CurrentIndex += iCount;

                return lResult;
            }
        }

        for ( int i = CurrentIndex; i < CurrentIndex + iCount; i++ )
        {
            int iByteIndex = i / 8;
            int iBitIndex  = i % 8;

            int iByteValue = Packet [ iByteIndex ];
            int iBitValue  = ( 1 << ( 7 - iBitIndex ) & iByteValue ) == 0 ? 0 : 1;

            lResult = lResult << 1 | ( uint ) iBitValue;
        }

        CurrentIndex += iCount;

        if ( memo != null )
        {
            lock ( oLock )
            {
                memo [ key ] = lResult;
            }
        }

        return lResult;
    }



    public int Extract ( int iCount )
    {
        int iResult = ( int ) ExtractLong ( iCount );

        return iResult;
    }

    public int      CurrentIndex    { get; set; }
    public byte [ ] Packet          { get; set; }

    object oLock = new object ( );

}



class CFEKey : IEquatable<CFEKey>
{
    public CFEKey ( byte [ ] packet, int iCurrentIndex, int iCount )
    {
        for ( int i = 0; i < 8; i++ )
            abyHeader [ i ] = packet [ i ];

        this.iCurrentIndex = iCurrentIndex;
        this.iCount        = iCount;
            
        iHashCode = HashCode.Of  ( iCurrentIndex )
                            .And ( iCount )
                            .AndEach ( abyHeader );
    }



    public override bool Equals ( object obj )
    {
        bool bResult = obj is CFEKey && Equals ( obj );

        return bResult;
    }



    public bool Equals ( CFEKey other )
    {
        if ( iCurrentIndex != other.iCurrentIndex )
            return false;

        if ( iCount != other.iCount )
            return false;

        for ( int i = 0; i < 8; i++ )
        {
            if ( abyHeader [ i ] != other.abyHeader [ i ] )
                return false;
        }

        return true;
    }



    public override int GetHashCode ( )
    {
        return iHashCode;
    }

    int      iHashCode;
    byte [ ] abyHeader = new byte [ 8 ];
    int      iCurrentIndex;
    int      iCount;
}
