﻿// INTEL CONFIDENTIAL

// Copyright 2019-2020 Intel Corporation.

// This software and the related documents are Intel copyrighted materials,
// and your use of them is governed by the express license under which they
// were provided to you ("License"). Unless the License provides otherwise,
// you may not use, modify, copy, publish, distribute, disclose or transmit
// this software or the related documents without Intel's prior written
// permission.

// This software and the related documents are provided as is, with no express
// or implied warranties, other than those that are expressly stated in the
// License.

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Altera.SSD.Utilities;



class CycleInfo
{
    public void SetPinMaskFromCount ( )
    {
        CTPMaskValue = 0;

        for ( int i = 0; i < PinsCovered; i++ )
        {
            CTPMaskValue <<= 1;
            CTPMaskValue |= 1;
        }

        PinMask = new BinStringRepresentation ( 35, CTPMaskValue, 'x' ).ToString ( );
    }




    public void SetPinMaskFromTrace ( long lMaskValue )
    {
        CTPMaskValue = lMaskValue;

        PinMask = new BinStringRepresentation ( 35, CTPMaskValue, 'x' ).ToString ( );
    }




    public string GetCSVHeader ( )
    {
        var sResult = "chip,slice,block,covered,pins,cycle,eob,valid,all_same,pin_mask";

        return sResult;
    }




    public string GetCSVValues ( )
    {
        var sResult = String.Format ( "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", Chip, Slice, CaptureBlockIndex, CyclesCovered, PinsCovered, CycleNumber,
                                                                                 IsEOB ? "1" : "0", IsValid ? "1" : "0", AllSame ? "1" : "0", PinMask );

        return sResult;
    }




    public string GetExpectCSVHeader ( )
    {
        var sResult = "chip,slice,pins,cycle";

        return sResult;
    }




    public string GetExpectCSVValues ( )
    {
        var sResult = String.Format ( "{0},{1},0x{2:x},{3}", Chip, Slice, CTPMaskValue, CycleNumber );

        return sResult;
    }



    public int    Chip              { get; set; }
    public int    Slice             { get; set; }
    public int    CaptureBlockIndex { get; set; }
    public int    CyclesCovered     { get; set; }
    public int    PinsCovered       { get; set; }
    public int    CycleNumber       { get; set; }
    public bool   IsEOB             { get; set; }
    public bool   IsValid           { get; set; }
    public bool   AllSame           { get; set; }
    public long   CTPMaskValue      { get; set; }
    public string PinMask           { get; set; }
}




class ChannelSetCycleData
{
    public static int Polarity     { get; set; }
    public static int PolarityMask { get; set; }
    public static int Width        { get; set; }

    public static void SetPolarity ( int iPolarity, int iWidth )
    {
        Polarity = iPolarity;
        Width    = iWidth;

        if ( Polarity == 0 )
            PolarityMask = 0;

        else
        {
            for ( int i = 0; i < Width; i++ )
            {
                PolarityMask <<= 1;
                PolarityMask |= 1;
            }
        }
    }



    public ChannelSetCycleData ( int iChannelSet, int iState, int iError )
    {
        ChannelSet  = iChannelSet;
        State       = iState;
        Error       = iError;

        CorrectedError = Error ^ State ^ PolarityMask;
    }




    public string GetCSVHeader ( )
    {
        var sResult = String.Format ( "state_value_{0},error_value_{0},corrected_ev_{0},state_{0},error_{0},corrected_{0}", ChannelSet );
        return sResult;
    }




    public string GetCSVValues ( )
    {
        string sResult = null;

        if ( State >= 0 )
            sResult = String.Format ( "{0},{1},{2},{3},{4},{5}", State, Error, CorrectedError,
                                                                 new BinStringRepresentation ( Width, State ),
                                                                 new BinStringRepresentation ( Width, Error ),
                                                                 new BinStringRepresentation ( Width, CorrectedError ) );

        else
            sResult = String.Format ( "{0},{1},{2},{3},{4},{5}", -1, -1, -1,
                                                                 new BinStringRepresentation ( Width, State, 'x', 'x' ),
                                                                 new BinStringRepresentation ( Width, Error, 'x', 'x' ),
                                                                 new BinStringRepresentation ( Width, CorrectedError, 'x', 'x' ) );

        return sResult;
    }




    public string GetExpectCSVHeader ( )
    {
        var sResult = String.Format ( "corrected_ev_{0}", ChannelSet );

        return sResult;
    }




    public string GetExpectCSVValues ( )
    {
        var sResult = CorrectedError.ToString ( );

        return sResult;
    }


    public int ChannelSet      { get; set; }
    public int State           { get; set; }
    public int Error           { get; set; }
    public int CorrectedError  { get; set; }
}




class UnpackedCycle
{
    public static void SetPolarity ( int iPolarity, int iWidth )
    {
        ChannelSetCycleData.SetPolarity ( iPolarity, iWidth );
    }



    public UnpackedCycle ( CycleInfo cycleInfo )
    {
        CycleInfo = cycleInfo;
        ChannelSets = new ChannelSetCycleData [ 4 ];
    }



    public void AddChannelSet ( int iChannelSet, int iState, int iError )
    {
        ChannelSets [ iChannelSet ] = new ChannelSetCycleData ( iChannelSet, iState, iError );

        if ( iChannelSet == 3 )
        {
            CycleInfo.AllSame = true;

            int iCheckState = ChannelSets [ 0 ].State;
            int iCheckError = ChannelSets [ 0 ].Error;

            for ( int i = 1; i < 4; i++ )
            {
                if ( ChannelSets [ i ].State != iCheckState || ChannelSets [ i ].Error != iCheckError )
                {
                    CycleInfo.AllSame = false;
                    break;
                }   
            }
        }
    }




    public string GetCSVHeader ( )
    {
        var sResult = String.Format ( "{0},,{1},,{2},,{3},,{4}", CycleInfo.GetCSVHeader ( ), 
                                                                 ChannelSets [ 0 ].GetCSVHeader ( ), 
                                                                 ChannelSets [ 1 ].GetCSVHeader ( ), 
                                                                 ChannelSets [ 2 ].GetCSVHeader ( ), 
                                                                 ChannelSets [ 3 ].GetCSVHeader ( ) );

        return sResult;

    }




    public string GetCSVValues ( )
    {
        var sResult = String.Format ( "{0},,{1},,{2},,{3},,{4}", CycleInfo.GetCSVValues ( ), 
                                                                 ChannelSets [ 0 ].GetCSVValues ( ), 
                                                                 ChannelSets [ 1 ].GetCSVValues ( ), 
                                                                 ChannelSets [ 2 ].GetCSVValues ( ), 
                                                                 ChannelSets [ 3 ].GetCSVValues ( ) );

        return sResult;

    }




    public string GetExpectCSVHeader ( )
    {
        var sResult = String.Format ( "{0},{1},{2},{3},{4}", CycleInfo.GetExpectCSVHeader ( ), 
                                                             ChannelSets [ 0 ].GetExpectCSVHeader ( ), 
                                                             ChannelSets [ 1 ].GetExpectCSVHeader ( ), 
                                                             ChannelSets [ 2 ].GetExpectCSVHeader ( ), 
                                                             ChannelSets [ 3 ].GetExpectCSVHeader ( ) );

        return sResult;

    }




    public string GetExpectCSVValues ( )
    {
        var sResult = String.Format ( "{0},{1},{2},{3},{4}", CycleInfo.GetExpectCSVValues ( ), 
                                                             ChannelSets [ 0 ].GetExpectCSVValues ( ), 
                                                             ChannelSets [ 1 ].GetExpectCSVValues ( ), 
                                                             ChannelSets [ 2 ].GetExpectCSVValues ( ), 
                                                             ChannelSets [ 3 ].GetExpectCSVValues ( ) );

        return sResult;

    }



    public CycleInfo               CycleInfo    { get; set; }
    public ChannelSetCycleData [ ] ChannelSets  { get; set; }
}




class CycleUnpacker
{
    public static int [ ]        CTVHeaderCycles     { get; set; }
    public static int [ ]        CTVHeaderTraces     { get; set; }
    public static TraceEntry [ ] Traces              { get; set; }
    public static int [ ] [ ]    CTVHeaderCycleIndex { get; set; }

    public static Dictionary<CUKey,Tuple<char[][][],char[][][]>> dictMemo;

    
    
    static CycleUnpacker ( )
    {
        CTVHeaderCycleIndex = new int [ 2 ] [ ];

        CTVHeaderCycleIndex [ 0 ] = new [ ] { 0, 0, 0, 0 };
        CTVHeaderCycleIndex [ 1 ] = new [ ] { 0, 0, 0, 0 };
    }
    
        
    
    
    public static void SetPolarity ( int iPolarity, int iWidth )
    {
        ChannelSetCycleData.SetPolarity ( iPolarity, iWidth );
    }
    
        
    
    
    public static void SetCTVHeaderCycles ( int [ ] ctvHeaderCycles )
    {
        CTVHeaderCycles = ctvHeaderCycles;
    }
    
        
    
    
    public static void SetCTVHeaderTraces ( int [ ] ctvHeaderTraces )
    {
        CTVHeaderTraces = ctvHeaderTraces;
    }
    
        
    
    
    public static void SetTraces ( TraceEntry [ ] traces )
    {
        Traces = traces;
    }



    public CycleUnpacker ( RawCTVDataHeader header, int iFullCycleCount, int iCaptureBlockIndex )
    {
        Header            = header;
        FullCycleCount    = iFullCycleCount;
        CaptureBlockIndex = iCaptureBlockIndex;

        BaseCycle = header.RelativeCycleCount + ( FullCycleCount & ~ 0x1FF );

        IsWide      = new bool [ 8 ];
        ValidCycles = new bool [ 32 ];

        int iValidMaskIndex = 0;
        int iValidIndex = 0;

        for ( int i = 0; i < 8; i++ )
        {
            int iMask = 1 << i;

            if ( ( Header.EntryIsWide & iMask ) != 0 )
            {
                IsWide [ i ] = true;

                TotalCycles += 1;

                bool bIsValid = ( Header.ValidCycles & 1 << iValidMaskIndex ) != 0;

                ValidCycles [ iValidIndex ] = bIsValid;

                iValidIndex += 1;

                iValidMaskIndex += 4;
            }

            else
            {
                TotalCycles += 4;
                
                for ( int j = 0; j < 4; j++ )
                {
                    bool bIsValid = ( Header.ValidCycles & 1 << iValidMaskIndex ) != 0;

                    ValidCycles [ iValidIndex ] = bIsValid;

                    iValidIndex += 1;

                    iValidMaskIndex += 1;
                }
            }
        }

        PinStates   = new char [ 4 ] [ ] [ ];
        ErrorStates = new char [ 4 ] [ ] [ ];

        for ( int i = 0; i < 4; i++ )
        {
            PinStates   [ i ] = new char [ TotalCycles ] [ ];
            ErrorStates [ i ] = new char [ TotalCycles ] [ ];

            for ( int j = 0; j < TotalCycles; j++ )
            {
                PinStates   [ i ] [ j ] = new char [ Header.BlockCount ];
                ErrorStates [ i ] [ j ] = new char [ Header.BlockCount ];

                for ( int k = 0; k < Header.BlockCount; k++ )
                {
                    PinStates   [ i ] [ j ] [ k ] = 'x';
                    ErrorStates [ i ] [ j ] [ k ] = 'x';
                }
            }
        }

        Blocks = new byte [ Header.BlockCount ] [ ];
    }




    public void AddBlock ( byte [ ] abyBLock )
    {
        Blocks [ CurrentBlockIndex ] = abyBLock;

        CurrentBlockIndex++;

        if ( CurrentBlockIndex == Header.BlockCount )
            UnpackBlocks ( );
    }




    public void UnpackBlocks ( )
    {
        int iCurrentCycleIndex = 0;

        CUKey key = null;

        if ( dictMemo != null )
        {
            key = new CUKey ( Blocks, IsWide, ValidCycles );

            if ( dictMemo.ContainsKey ( key ) )
            {
                var tu = dictMemo [ key ];

                PinStates   = tu.Item1;
                ErrorStates = tu.Item2;

                return;
            }

        }

        for ( int iSubBlockIndex = 0; iSubBlockIndex < 8; iSubBlockIndex++ )
        {
            if ( IsWide [ iSubBlockIndex ] )
            {
                if ( ! ValidCycles [ iCurrentCycleIndex ] )
                {
                    iCurrentCycleIndex++;
                    continue;
                }

                for ( int iBlockIndex = 0; iBlockIndex < Header.BlockCount; iBlockIndex++ )
                {
                    var byWork = Blocks [ iBlockIndex ] [ 7 - iSubBlockIndex ];

                    for ( int iChannelSet = 0; iChannelSet < 4; iChannelSet++ )
                    {
                        char cPinState   = ( byWork & 1 << ( iChannelSet * 2 + 0 ) ) > 0 ? '1' : '0';
                        char cErrorState = ( byWork & 1 << ( iChannelSet * 2 + 1 ) ) > 0 ? '1' : '0';

                        PinStates   [ iChannelSet ] [ iCurrentCycleIndex ] [ iBlockIndex ] = cPinState;
                        ErrorStates [ iChannelSet ] [ iCurrentCycleIndex ] [ iBlockIndex ] = cErrorState;
                    }
                }

                iCurrentCycleIndex++;
            }

            else
            {
                for ( int iCycleIndex = 0; iCycleIndex < 4; iCycleIndex++ )
                {
                    if ( ! ValidCycles [ iCurrentCycleIndex ] )
                    {
                        iCurrentCycleIndex++;
                        continue;
                    }

                    for ( int iBlockIndex = 0; iBlockIndex < Header.BlockCount; iBlockIndex++ )
                    {
                        var byWork = Blocks [ iBlockIndex ] [ 7 - iSubBlockIndex ];

                        char cPinState   = ( byWork & 1 << ( iCycleIndex * 2 + 0 ) ) > 0 ? '1' : '0';
                        char cErrorState = ( byWork & 1 << ( iCycleIndex * 2 + 1 ) ) > 0 ? '1' : '0';

                        PinStates   [ 0 ] [ iCurrentCycleIndex ] [ iBlockIndex ] = cPinState;
                        ErrorStates [ 0 ] [ iCurrentCycleIndex ] [ iBlockIndex ] = cErrorState;
                    }

                    iCurrentCycleIndex++;
                }
            }
        }

        if ( key != null )
            dictMemo [ key ] = Tuple.Create ( PinStates, ErrorStates );
    }



    public string GetCSVHeader ( )
    {
        var ci = new CycleInfo ( );
        var uc = new UnpackedCycle ( ci );

        uc.AddChannelSet ( 0, -1, -1 );
        uc.AddChannelSet ( 1, -1, -1 );
        uc.AddChannelSet ( 2, -1, -1 );
        uc.AddChannelSet ( 3, -1, -1 );

        var sResult = uc.GetCSVHeader ( );

        return sResult;
    }




    public Tuple<int,bool,string> GetNextCycle ( bool bStart = false, bool bForceInvalid = false )
    {
        return GetNextCycleGeneral ( bStart, bForceInvalid, true );
    }



    public string GetExpectCSVHeader ( )
    {
        var ci = new CycleInfo ( );
        var uc = new UnpackedCycle ( ci );

        uc.AddChannelSet ( 0, -1, -1 );
        uc.AddChannelSet ( 1, -1, -1 );
        uc.AddChannelSet ( 2, -1, -1 );
        uc.AddChannelSet ( 3, -1, -1 );

        var sResult = uc.GetExpectCSVHeader ( );

        return sResult;
    }




    public Tuple<int,bool,string> GetExpectNextCycle ( bool bStart = false, bool bForceInvalid = false )
    {
        return GetNextCycleGeneral ( bStart, bForceInvalid, false );
    }




    Tuple<int,bool,string> GetNextCycleGeneral ( bool bStart = false, bool bForceInvalid = false, bool bFullFormat = true )
    {
        if ( TotalCycles == CurrentIndex )
            return Tuple.Create ( -1, false, ( string ) null );

        if ( bStart )
            CurrentIndex = 0;

        int iFormatIndex = bFullFormat ? 1 : 0;

        var ci = new CycleInfo ( );

        ci.Chip              = Header.OriginatingChip;
        ci.Slice             = Header.OriginatingSlice;
        ci.CaptureBlockIndex = CaptureBlockIndex;
        ci.CyclesCovered     = TotalCycles;
        ci.PinsCovered       = Header.BlockCount;
        ci.CycleNumber       = BaseCycle + CurrentIndex;
        ci.IsEOB             = Header.EndOfBurst;
        ci.IsValid           = ValidCycles [ CurrentIndex ] && ! bForceInvalid;

        if ( CTVHeaderTraces != null && Traces != null )
        {
            int   iTraceIndex = CTVHeaderTraces [ CTVHeaderCycleIndex [ iFormatIndex ] [ ci.Chip ] ];
            long  lPinMask = Traces [ iTraceIndex ].CaptureThisPin;
            
            if ( bFullFormat )
                ci.SetPinMaskFromTrace ( lPinMask );
            else
                ci.CTPMaskValue = lPinMask;
        }

        else if ( bFullFormat )
            ci.SetPinMaskFromCount ( );

        if ( CTVHeaderCycles != null && ValidCycles [ CurrentIndex ] )
        {
            ci.CycleNumber = CTVHeaderCycles [ CTVHeaderCycleIndex  [ iFormatIndex ] [ ci.Chip ] ];
            CTVHeaderCycleIndex  [ iFormatIndex ] [ ci.Chip ] += 1;
        }


        var uc = new UnpackedCycle ( ci );

        for ( int iChannelSet = 0; iChannelSet < 4; iChannelSet++ )
        {
            if ( PinStates [ iChannelSet ] [ CurrentIndex ] == null || PinStates [ iChannelSet ] [ CurrentIndex ] [ 0 ] == 'x' )
            {
                uc.AddChannelSet ( iChannelSet, -1, -1 );
                continue;
            }

            int iPinStateValue   = 0;
            int iErrorStateValue = 0;

            for ( int iBlockIndex = 0; iBlockIndex < Header.BlockCount; iBlockIndex++ )
            {
                int iBitValue = PinStates [ iChannelSet ] [ CurrentIndex ] [ Header.BlockCount - iBlockIndex - 1 ] == '1' ? 1 : 0;

                iPinStateValue <<= 1;
                iPinStateValue |= iBitValue;


                iBitValue = ErrorStates [ iChannelSet ] [ CurrentIndex ] [ Header.BlockCount - iBlockIndex - 1 ] == '1' ? 1 : 0;

                iErrorStateValue <<= 1;
                iErrorStateValue |= iBitValue;
            }

            uc.AddChannelSet ( iChannelSet, iPinStateValue, iErrorStateValue );
        }

        CurrentIndex++;

        var result = Tuple.Create ( ci.CycleNumber, ci.IsValid, bFullFormat ? uc.GetCSVValues ( ) : uc.GetExpectCSVValues ( ) );

        return result;
    }




    public RawCTVDataHeader  Header             { get; set; }
    public int               FullCycleCount     { get; set; }
    public int               CaptureBlockIndex  { get; set; }
    public int               BaseCycle          { get; set; }
    public int               TotalCycles        { get; set; }
    public bool [ ]          IsWide             { get; set; }
    public bool [ ]          ValidCycles        { get; set; }
    public int               CurrentIndex       { get; set; }
    public int               CurrentBlockIndex  { get; set; }

    public char [ ] [ ] [ ]  PinStates          { get; set; }
    public char [ ] [ ] [ ]  ErrorStates        { get; set; }

    public byte [ ] [ ]      Blocks             { get; set; }
}




class BinStringRepresentation
{
    public static Dictionary<Tuple<int,long,char,char>,string> dictMemo;

    public BinStringRepresentation ( int iPlaces, long lValue, char cZero = '0', char cOne = '1' )
    {
        this.iPlaces = iPlaces;
        this.lValue  = lValue;
        this.cZero   = cZero;
        this.cOne    = cOne;

        sRepresentation = Expand ( );
    }




    public override string ToString ( )
    {
        return sRepresentation;
    }



    string Expand ( )
    {
        string sResult = null;
        Tuple<int,long,char,char> tuKey = null;

        if ( dictMemo != null )
        {
            tuKey = Tuple.Create ( iPlaces, lValue, cZero, cOne );

            if ( dictMemo.ContainsKey ( tuKey ) )
            {
                sResult = dictMemo [ tuKey ];
                return sResult;
            }
        }

        var sb = new StringBuilder ( );

        long lWork = lValue;

        for ( int i = 0; i < iPlaces; i++ )
        {
            if ( i != 0 )
            {
                if ( i % 16 == 0 )
                    sb.Append ( "____" );

                else if ( i % 8 == 0 )
                    sb.Append ( "__" );

                else if ( i % 4 == 0 )
                    sb.Append ( '_' );
            }

            long lBit = lWork & 1;

            lWork >>= 1;

            if ( lBit == 1 )
                sb.Append ( cOne );

            else
                sb.Append ( cZero );
        }

        sb.Append ( '_' );

        var acReverse = new char [ sb.Length ];

        for ( int i = 0, j = sb.Length - 1; i < sb.Length; i++, j-- )
            acReverse [ i ] = sb [ j ];

        sResult = new string ( acReverse );

        if ( dictMemo != null )
            dictMemo [ tuKey ] = sResult;

        return sResult;
    }


    int    iPlaces;
    long   lValue;
    char   cZero;
    char   cOne;
    string sRepresentation;
}



class CUKey : IEquatable<CUKey>
{
    public CUKey ( byte [ ] [ ] aabyBlocks, bool [ ] abWide, bool [ ] abValid )
    {
        this.aabyBlocks = aabyBlocks;

        this.abWide = abWide;

        this.abValid = abValid;

        var aiCode = new int [ aabyBlocks.Length ];

        for ( int i = 0; i < aabyBlocks.Length; i++ )
            aiCode [ i ] = HashCode.OfEach ( aabyBlocks [ i ] );
            
        iHashCode = HashCode.OfEach  ( aiCode )
                            .AndEach ( abWide )
                            .AndEach ( abValid );
    }



    public override bool Equals ( object obj )
    {
        bool bResult = obj is CUKey && Equals ( obj );

        return bResult;
    }



    public bool Equals ( CUKey other )
    {
        if ( aabyBlocks.Length != other.aabyBlocks.Length )
            return false;

        for ( int i = 0; i < 8; i++ )
        {
            if ( abWide [ i ] != other.abWide [ i ] )
                return false;    
        }

        for ( int i = 0; i < 32; i++ )
        {
            if ( abValid [ i ] != other.abValid [ i ] )
                return false;    
        }

        for ( int i = 0; i < aabyBlocks.Length; i++ )
        {
            for ( int j = 0; j < aabyBlocks [ i ].Length; j++ )
            {
                if ( aabyBlocks [ i ] [ j ] != other.aabyBlocks [ i ] [ j ] )
                    return false;
            }
        }

        return true;
    }



    public override int GetHashCode ( )
    {
        return iHashCode;
    }

    int          iHashCode;
    byte [ ] [ ] aabyBlocks;
    bool [ ]     abWide;
    bool [ ]     abValid;
}