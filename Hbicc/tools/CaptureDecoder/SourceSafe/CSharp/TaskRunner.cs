﻿// INTEL CONFIDENTIAL

// Copyright 2019-2020 Intel Corporation.

// This software and the related documents are Intel copyrighted materials,
// and your use of them is governed by the express license under which they
// were provided to you ("License"). Unless the License provides otherwise,
// you may not use, modify, copy, publish, distribute, disclose or transmit
// this software or the related documents without Intel's prior written
// permission.

// This software and the related documents are provided as is, with no express
// or implied warranties, other than those that are expressly stated in the
// License.


using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using Altera.SSD.Utilities;




class TaskRunner
{
    public static bool RunActionListFailable ( List<Func<bool>> listActions, CancellationTokenSource cts, Func<bool> fnCheck, bool bSynchronous = false, int iThrottle = Int32.MaxValue )
    {
        bool bFail = false;

        if ( bSynchronous || listActions.Count <= 1 )
        {
            foreach ( var action in listActions )
            {
                bool bResult = action ( );

                if ( ! bResult )
                {
                    bFail = true;
                    break;
                }

                if ( fnCheck != null && ! fnCheck ( ) )
                {
                    bFail = true;
                    break;
                }
            }

            return ! bFail;
        }

        var listTasks = new List<Task<bool>> ( );

        var savedPriority = Thread.CurrentThread.Priority;

        Thread.CurrentThread.Priority = ThreadPriority.Highest;

        using ( SemaphoreSlim concurrencySemaphore = new SemaphoreSlim ( iThrottle ) )
        {
            if ( iThrottle == Int32.MaxValue )
                listActions.Each ( item => listTasks.Add ( Task.Factory.StartNew ( item, cts.Token ) ) );

            else
            {   
                foreach ( var action in listActions )
                {
                    var localAction = action;

                    listTasks.Add ( Task.Factory.StartNew ( ( ) => { concurrencySemaphore.Wait ( );

                                                                     try
                                                                     {
                                                                         return localAction ( );
                                                                     }
                                                                
                                                                     finally
                                                                     {
                                                                         concurrencySemaphore.Release ( );
                                                                     }
                                                                
                                                                   }, cts.Token ) );
                }
            }

            while ( true )
            {
                int iIndex = Task.WaitAny ( listTasks.ToArray ( ), 10 );

                if ( iIndex < 0 )
                {
                    if ( fnCheck == null )
                        continue;

                    if ( ! fnCheck ( ) )
                        bFail = true;

                    else
                        continue;
                }

                if ( ! bFail )
                {
                    bool bResult = listTasks [ iIndex ].Result;

                    if ( ! bResult )
                        bFail = true;

                    listTasks.RemoveAt ( iIndex );

                    if ( ! bFail && fnCheck != null && ! fnCheck ( ) )
                        bFail = true;

                    if ( ! listTasks.Any ( ) )
                        break;
                }

                if ( bFail )
                {
                    try
                    {
                        //Console.WriteLine ( "Triggering cancel" );

                        cts.Cancel ( );

                        while ( true )
                        {
                            Task.WaitAll ( listTasks.ToArray ( ) );

                            break;
                        }

                    }

                    catch //( AggregateException e )
                    {
            
                    }

                    break;
                }
            }
        }

        Thread.CurrentThread.Priority = savedPriority;

        return ! bFail;
    }




    public static void RunActionList ( List<Action> listActions, bool bSynchronous = false, int iThrottle = Int32.MaxValue )
    {
        if ( bSynchronous || listActions.Count <= 1 )
        {
            listActions.Each ( item => item ( ) );
            return;
        }

        var listTasks = new List<Task> ( );

        if ( iThrottle == Int32.MaxValue )
        {
            listActions.Each ( item => listTasks.Add ( Task.Factory.StartNew ( item ) ) );

            Task.WaitAll ( listTasks.ToArray ( ) );
        }

        else
        {
            using ( SemaphoreSlim concurrencySemaphore = new SemaphoreSlim ( iThrottle ) )
            {
                foreach ( var action in listActions )
                {
                    var localAction = action;

                    listTasks.Add ( Task.Factory.StartNew ( ( ) => { concurrencySemaphore.Wait ( );

                                                                     try
                                                                     {
                                                                         localAction ( );
                                                                     }
                                                                
                                                                     finally
                                                                     {
                                                                         concurrencySemaphore.Release ( );
                                                                     }
                                                                
                                                                   } ) );
                }

                Task.WaitAll ( listTasks.ToArray ( ) );
            }
        }
    }
}