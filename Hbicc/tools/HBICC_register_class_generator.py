################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import argparse
import sys
import os.path


def extractPageData(fileData):
    page_data = {}
    for page_info in fileData:
        # PAGE_00            = 'h0000,
        page_info_regex = re.compile(r'\s*([0-9a-zA-Z_]+)\s*\=\s\'h(\w*)', re.IGNORECASE)
        page_info = page_info_regex.search(page_info)
        try:
            page_number = page_info.group(1)
            page_address = page_info.group(2)
            page_data.update({page_number: {'Base':page_address, 'REGCOUNT': []}})
        except:
            pass
    print('\n{:>10} {:40}'.format('Address', 'Page Name'))
    for key, value in page_data.items():
        print('{:>10} {:40}'.format('0x' + value['Base'], key))

    return page_data

def extractRegisterData(fileData, page_data):
    register_data = []

    for register_info in fileData:
        # BAR1_SCRATCH = PAGE_00 + 'h00,  // (0x000)
        register_info_regex = re.compile(r'\s*([0-9a-zA-Z_]+)\s*\=\s([0-9a-zA-Z_]+)\s\+\s\'h([0-9A-Fa-f]+)',
                                         re.IGNORECASE)
        register_info = register_info_regex.search(register_info)
        if register_info is not None:
            print('reg info: ', register_info)

        try:

            register_name = register_info.group(1)
            register_page_address = register_info.group(2)
            offset = register_info.group(3)
            page_data[register_page_address]['REGCOUNT'].append({'register_name': register_name, 'offset': offset})
            print('')
        except:
            pass
    print(page_data)
    return page_data

def printRegisterClasses(bar_regs, registerClass, registerBar):
    for registerName, registerAddress in bar_regs:
        print('class {}({}):'.format(registerName, registerClass))
        print('    BAR = {}'.format(registerBar))
        print('    ADDR = {}'.format((registerAddress)))
        print('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]')
        print('')

def writeRegisterClasses(page_data, registerClass, registerBar, outputFileName):
    with open(outputFileName, 'w') as registerFile:
        for page, register_set in page_data.items():
            for register in register_set['REGCOUNT']:
                addr = add_hex(register['offset'], register_set['Base'])
                registerFile.write('class {}({}):\n'.format(register['register_name'], registerClass))
                registerFile.write('    BAR = {}\n'.format(registerBar))
                registerFile.write('    ADDR = {}\n'.format(addr))
                registerFile.write('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]\n\n\n')

def read_and_clean_input_file(fileName):
    fileData = []
    with open(fileName) as f:
        for line in f:
            fileData.append(line)
    return fileData

def output_register_classes(OutputFileName, page_data, args):
    if OutputFileName == None:
        printRegisterClasses(page_data, args.regclass, args.bar)
    else:
        writeRegisterClasses(page_data, args.regclass, args.bar, OutputFileName)

def add_hex(base, offset):
    return hex(int(offset, 16) + int(base, 16))

def add_page_base_address(page_data, register_data, register_name, register_page_address, register_offset):
    address = hex(int(register_offset, 16) + int(page_data[register_page_address]['Base'], 16))
    register_data.append((register_name, address))

def parse_args(args):
    # Parse command line options
    parser = argparse.ArgumentParser(description='VH to CTYPE register generator')
    parser.add_argument('inputFile', help='Input .vh file that contains register definitions', type=str, action="store")
    parser.add_argument('-bar', help='Bar for registers', type=int, action="store", required=True)
    parser.add_argument('-regclass', help='Register class that registers will derive from', type=str, action="store",
                        required=True)
    parser.add_argument('-o', '--outputfilename', help='optional output filename, otherwise prints to screen', type=str,
                        action="store", default=None)
    args = parser.parse_args(args)
    return args

def Main(user_args=None):
    args = parse_args(user_args)

    outputFileName = args.outputfilename
    inputFileName = args.inputFile

    try:
        with open(inputFileName):
            pass
    except FileNotFoundError:
        print('Input File \'{}\' does not exist!'.format(inputFileName))
        sys.exit(1)

    fileData = read_and_clean_input_file(inputFileName)
    page_data = extractPageData(fileData)
    register_data = extractRegisterData(fileData,page_data)
    output_register_classes(outputFileName, register_data, args)


if __name__ == '__main__':
    Main()
