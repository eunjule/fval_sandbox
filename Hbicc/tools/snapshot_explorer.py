# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import os
import tarfile
import math

final_dst = None

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('snapshot')
    return parser.parse_args()

class PatternWord():
    def __init__(self, offset, byte_data):
        self.offset = offset
        self.data = int.from_bytes(byte_data, byteorder='little')
        self.vector_type_table = {0: self.non_vector,
                                  1: self.non_vector,
                                  2: self.non_vector,
                                  3: self.non_vector,
                                  4: self.non_vector,
                                  5: self.pin_state_word,
                                  6: self.instruction_word,
                                  7: self.metadata_word,
                                  8: self.pin_data_vector,
                                  9: self.pin_data_vector,
                                  10: self.pin_data_vector,
                                  11: self.pin_data_vector,
                                  12: self.pin_data_vector,
                                  13: self.pin_data_vector,
                                  14: self.pin_data_vector,
                                  15: self.pin_data_vector}
        self.pin_state_word_table = {0b000: self.pin_state_word_reserved,
                                     0b001: self.plist_mask_pins,
                                     0b010: self.plist_unmask_pins,
                                     0b011: self.plist_apply_mask,
                                     0b100: self.enabled_clock_reset_mask,
                                     0b101: self.dut_serial_control,
                                     0b110: self.ctp_mask,
                                     0b111: self.active_channels_mask}
        self.op_type_method = {0b0000: self.branch_op,
                               0b0001: self.external_op,
                               0b0010: self.alu_op,
                               0b0100: self.register_op,
                               0b1000: self.vector_op}
        self.branch_op_type = {0b00000: self.no_branch,
                               0b00001: self.goto_immediate,
                               0b00010: self.call_immediate,
                               0b00011: self.compressed_call_immediate,
                               0b00100: self.pcall_immediate,
                               0b01000: self.ret,
                               0b10001: self.goto_register,
                               0b10010: self.call_register,
                               0b10011: self.compressed_call_register,
                               0b10100: self.pcall_register,
                               0b11000: self.clear_stack}
        self.vector_op_by_type = {0b001: self.vector_op_log,
                                  0b010: self.vector_op_local,
                                  0b100: self.vector_op_other}
        self.log_commands = {0b0001: self.log1_immediate,
                             0b0100: self.pattern_id_immediate,
                             0b1001: self.log1_register,
                             0b1100: self.pattern_id_register}
        self.local_commands = {0b0001: self.repeat_immediate,
                               0b0010: self.end_immediate,
                               0b1001: self.repeat_register,
                               0b1010: self.end_register}
        self.other_commands = {0b0001: self.trigger_to_snap_in_board,
                                0b0010: self.block_fail_enable_immediate,
                                0b0011: self.drain_pin_fifo_immediate,
                                0b0100: self.edge_counter_immediate,
                                0b0101: self.clear_sticky_error,
                                0b1000: self.capture_immediate,
                                0b1001: self.reset_fail_counts,
                                0b1011: self.drain_pin_fifo_register,
                                0b1100: self.reset_cycle_count,
                                0b1101: self.clear_domain_sticky_error,
                                0b1111: self.clear_global_sticky_error}
    
    def to_html_table_row(self):
        return '<tr>'\
              f'<td id={self.offset:08X}>{self.offset:08X}</td>'\
              f'<td>{self.data:032X}</td>'\
              f'<td style="text-align:right">{self.decode()}</td>'\
               '</tr>'
               
    def to_html_header_row(self):
        return '<tr>'\
               '<th>Offset</th>'\
               '<th>Raw Vector</th>'\
               '<th>Info</th>'\
               '</tr>'
    
    def decode(self):
        vector_type = (self.data >> 124) & 0xF
        return self.vector_type_table[vector_type]()
        
    def non_vector(self):
        return f'Non-Pattern Data 0x{self.data:032X}'
    
    def pin_state_word(self):
        pin_state_type = (self.data >> 121) & 0x7
        return self.pin_state_word_table[pin_state_type]()

    def pin_state_word_reserved(self):
        return 'Pin State Word Reserved'
        
    def plist_mask_pins(self):
        mask = self.data & 0x3FFFFFFFFF
        return f'PList Mask 0b{mask:035b}'
        
    def plist_unmask_pins(self):
        mask = self.data & 0x3FFFFFFFFF
        return f'PList Unmask 0b{mask:035b}'
        
    def plist_apply_mask(self):
        mask = self.data & 0x3FFFFFFFFF
        return f'PList Apply Mask 0b{mask:035b}'
        
    def enabled_clock_reset_mask(self):
        resets = self.data & 0x3FFFFFFFFF
        return f'Enable Clock Reset 0b{resets:035b}'
        
    def dut_serial_control(self):
        channel_set = (self.data >> 115) & 0x3F
        return f'DUT Serial Control channel_set=0b{channel_set:06b} 0o{self.encoded_3bit_channels():035o}'
        
    def ctp_mask(self):
        mask = self.data & 0x3FFFFFFFFF
        return f'CTP Mask 0b{mask:035b}'
        
    def active_channels_mask(self):
        return f'Active Channels Mask 0o{self.encoded_3bit_channels():035o}'
    
    def pin_data_vector(self):
        ctv = (self.data >> 125) & 1
        mtv = (self.data >> 120) & 0x1F
        repeat = (self.data >> 115) & 0x1F
        encoded_channel_string = f'{self.encoded_3bit_channels():035o}'
        # return f'Pin Data CTV={ctv} MTV=0b{mtv:05b} LRPT={repeat:2} 0o{self.encoded_3bit_channels():035o}'
        return f'Pin Data CTV={ctv} MTV=0b{mtv:05b} LRPT={repeat:2} {self.symbolic_pin_data()}'
    
    def symbolic_pin_data(self):
        encoded_channel_string = f'{self.encoded_3bit_channels():035o}'
        return ''.join([x for x in map(self.encoded_data_pin_to_symbol, encoded_channel_string)])
    
    def encoded_data_pin_to_symbol(self, value):
        return pin_data_symbol_table[value]
    
    def encoded_3bit_channels(self):
        upper_channels = (self.data >> 64) & 0x000003FFFFFFFFFF
        lower_channels = self.data         & 0x7FFFFFFFFFFFFFFF
        return (upper_channels << 63) | lower_channels
        
    def metadata_word(self):
        return f'Metadata 0x{self.data & 0x0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF:032X}'
        
    def instruction_word(self):
        op_type = (self.data >> 66) & 0xF
        return self.op_type_method[op_type]()
    
    def branch_op(self):
        base = (self.data >> 52) & 0x3
        type = (self.data >> 47) & 0x1F
        return self.branch_op_type[type](base)
    
    def no_branch(self, base):
        return 'No Branch'
    
    def goto_immediate(self, base):
        if base == RELATIVE:
            i = self.signed_immediate()
            target = self.offset + i
            return f'Relative GOTO {i} (<a href="#{target:08X}">0x{target:08X}</a>)'
        elif base == GLOBAL:
            return f'GOTO 0x{self.immediate():08X}'
        elif base == CFB:
            return f'GOTO CFB + 0x{self.immediate():08X}'
        else: #PFB:
            return f'GOTO PFB + 0x{self.immediate():08X}'
    
    def call_immediate(self, base):
        if base == RELATIVE:
            i = self.signed_immediate()
            target = self.offset + i
            return 'Relative CALL {i} (<a href="#{target:08X}">0x{target:08X}</a>)'
        elif base == GLOBAL:
            target = self.immediate()
            return f'CALL <a href="#{target:08X}">0x{target:08X}</a>'
        elif base == CFB:
            return f'CALL CFB + (0x{self.immediate():08X})'
        else: #PFB:
            return f'CALL PFB + (0x{self.immediate():08X})'
        
    def compressed_call_immediate(self, base):
        return 'Compressed Call Immediate'
        
    def pcall_immediate(self, base):
        if base == RELATIVE:
            i = self.signed_immediate()
            target = self.offset + i
            return 'Relative PCALL {i} (<a href="#{target:08X}">0x{target:08X}</a>)'
        elif base == GLOBAL:
            target = self.immediate()
            return f'PCALL <a href="#{target:08X}">0x{target:08X}</a>'
        elif base == CFB:
            return f'PCALL CFB + (0x{self.immediate():08X})'
        else: #PFB:
            return f'PCALL PFB + (0x{self.immediate():08X})'
        
    def ret(self, base):
        return 'Return'
        
    def goto_register(self, base):
        return 'Goto from register'
        
    def call_register(self, base):
        return 'Call from register'
        
    def compressed_call_register(self, base):
        return 'Compressed Call from Register'
        
    def pcall_register(self, base):
        return 'PCall from register'
        
    def clear_stack(self, base):
        return 'Clear Stack'
            
    def external_op(self):
        return 'External'
        
    def alu_op(self):
        op = self.op_code()
        operation = op & 0x7
        source = (op >> 3) & 0x3
        destination = op >> 5

        s = alu_ops[operation]
        if source & 0x2:
            s += ' FLAGS'
        else:
            s += f' REG{self.register_a()}'
        if source & 0x1:
            s += f', REG{self.register_b()}'
        else:
            s += f', 0x{self.immediate():08X}'
        s += ' to '
        if destination == 0:
            s += 'NULL'
        elif destination == 1:
            s += f'REG{self.destination()}'
        elif destination == 2:
            s += 'FLAGS'
        else:
            s += '??'
        return s
        
    def register_op(self):
        op_code = self.op_code()
        op_source = reg_source[op_code & 0x7]
        op_destination = reg_destination.get(op_code >> 3, bin(op_code >> 3))
        s = f'Register {op_source}'
        if op_source == 'REG':
            s += str(self.register_a())
        elif op_source == 'IMM':
            s += f' (0x{self.immediate():08X})'
        s += f' to {op_destination}'
        if op_destination == 'REG':
            s += str(self.destination())
        return s

    def op_code(self):
        return (self.data >> 59) & 0x7F

    def vector_op(self):
        op_code = self.op_code()
        vp_type = (op_code >> 4) & 0x7
        vp_op = op_code & 0xF
        return self.vector_op_by_type[vp_type](vp_op)
    
    def vector_op_log(self, op):
        return self.log_commands[op]()
    
    def log1_immediate(self):
        return f'LOG1 0x{self.immediate():08X}'
    
    def pattern_id_immediate(self):
        return f'Pattern ID 0x{self.immediate():08X}'
    
    def log1_register(self):
        return f'LOG1 from register 0x{self.register_a():02X}'
    
    def pattern_id_register(self):
        return f'Pattern ID from register 0x{self.register_a():02X}'
    
    def vector_op_local(self, op):
        return self.local_commands[op]()
    
    def repeat_immediate(self):
        return f'Repeat {self.immediate()}'
    
    def end_immediate(self):
        return f'End 0x{self.immediate():08X}'
    
    def repeat_register(self):
        return f'Repeat from register 0x{self.register_a():02X}'
    
    def end_register(self):
        return f'End from register 0x{self.register_a():02X}'
    
    def vector_op_other(self, op):
        return self.other_commands[op]()

    def trigger_to_snap_in_board(self):
        return f'Trigger to Snap In Board'

    def block_fail_enable_immediate(self):
        return f'Block Fails Enable Immediate: {hex(self.immediate())}'

    def drain_pin_fifo_immediate(self):
        return f'Drain Pin FIFO Immediate: {hex(self.immediate())}'

    def edge_counter_immediate(self):
        return f'Edge Counter Immediate: {hex(self.immediate())}'

    def clear_sticky_error(self):
        return f'Clear Sticky Error'

    def capture_immediate(self):
        return f'Capture Enable Immediate: {hex(self.immediate())}'

    def reset_fail_counts(self):
        return f'Reset Fail Counts'

    def drain_pin_fifo_register(self):
        return f'Drain Pin FIFO Via Register A: {hex(self.register_a())}'

    def reset_cycle_count(self):
        return f'Reset User Cycle Count'

    def clear_domain_sticky_error(self):
        return f'Clear Domain Sticky Error'

    def clear_global_sticky_error(self):
        return f'Clear Global Sticky Error'
    
    def signed_immediate(self):
        if self.data & 0x80000000:
            return (self.data & 0xFFFFFFFF) - 0x100000000
        else:
            return self.data & 0x7FFFFFFF

    def immediate(self):
        return self.data & 0xFFFFFFFF
    
    def register_a(self):
        return (self.data >> 32) & 0x1F

    def register_b(self):
        return (self.data >> 37) & 0x1F

    def destination(self):
        return (self.data >> 42) & 0x1F


reg_source = {0: 'IMM',
              1: 'REG',
              2: 'TCC',
              3: 'VECADDR',
              4: 'CFB',
              5: 'FLAGS',
              6: 'DIRTY_MASK',
              7: 'STACK'}

reg_destination = {0b0001: 'REG',
                   0b0010: 'CFB',
                   0b0100: 'FLAGS',
                   0b1000: 'STACK'}

alu_ops = {0: 'XOR',
           1: 'AND',
           2: 'OR',
           3: 'LSHL',
           4: 'LSHR',
           5: 'ADD',
           6: 'SUB',
           7: 'MUL'}
                             
GLOBAL   = 0
CFB      = 1
RELATIVE = 2
PFB      = 3

pin_data_symbol_table = {'0': 'L',
                         '1': 'H',
                         '2': 'X',
                         '3': 'V',
                         '4': '0',
                         '5': '1',
                         '6': 'E',
                         '7': 'K'}
        
class CtvHeader():
    def __init__(self, byte_data):
        self.last_pcall_address = int.from_bytes(byte_data[0:4], byteorder='little')
        self.vector_address     = int.from_bytes(byte_data[4:8], byteorder='little')
        self.pattern_cycle      = int.from_bytes(byte_data[8:12], byteorder='little')
        self.repeat_count       = int.from_bytes(byte_data[12:16], byteorder='little')
        self.selectable         = int.from_bytes(byte_data[16:20], byteorder='little')
        self.user_cycle         = int.from_bytes(byte_data[20:24], byteorder='little')
        self.trace_index        = int.from_bytes(byte_data[26:29], byteorder='little')
        self.pattern_counter    = int.from_bytes(byte_data[29:32], byteorder='little')
        self.raw                = int.from_bytes(byte_data, byteorder='little')
        self.dut_serial_control = int.from_bytes(byte_data[24:25], byteorder='little')

    def to_html_table_row(self):
        s = '<tr>'\
           f'<td>{self.pattern_counter:08}</td>'\
           f'<td>{self.trace_index:08}</td>'\
           f'<td>{self.user_cycle:010}</td>'\
           f'<td>{self.dut_serial_control:03}</td>'\
           f'<td>0x{self.selectable:08X}</td>'\
           f'<td>{self.repeat_count:010}</td>'\
           f'<td>{self.pattern_cycle:010}</td>'\
           f'<td>{self.vector_address:08X}</td>'\
           f'<td>{self.last_pcall_address:08X}</td>'\
           f'<td>0x{self.raw:064X}</td>'\
           '</tr>'
        return s
    
    def to_html_header_row(self):
        s = '<tr>'\
            '<th>Pattern Counter</th>'\
            '<th>Trace Index</th>'\
            '<th>User Cycle</th>'\
            '<th>DUT Serial Control</th>'\
            '<th>Selectable</th>'\
            '<th>Repeat Count</th>'\
            '<th>Pattern Cycle</th>'\
            '<th>Vector Address</th>'\
            '<th>Last PCALL Address</th>'\
            '<th>Raw Header Data</th>'\
            '</tr>'
        return s
        
class CtvData():
    def __init__(self, byte_data):
        self.raw                = int.from_bytes(byte_data, byteorder='little')
        self.block_0            = (self.raw >> 192) & 0xFFFFFFFFFFFFFFFF
        self.block_1            = (self.raw >> 128) & 0xFFFFFFFFFFFFFFFF
        self.block_2            = (self.raw >>  64) & 0xFFFFFFFFFFFFFFFF
        self.block_3            = (self.raw >>   0) & 0xFFFFFFFFFFFFFFFF
    
    def to_html_table_row(self):
        s = '<tr>'\
           f'<td>0x{self.block_0:016X}</td>'\
           f'<td>0x{self.block_1:016X}</td>'\
           f'<td>0x{self.block_2:016X}</td>'\
           f'<td>0x{self.block_3:016X}</td>'\
           '</tr>'
        return s
    
    def to_html_header_row(self):
        s = '<tr>'\
            '<th>Block 0</th>'\
            '<th>Block 1</th>'\
            '<th>Block 2</th>'\
            '<th>Block 3</th>'\
            '</tr>'
        return s

class CtvHeaderData():
    def __init__(self, byte_data):
        self.raw = int.from_bytes(byte_data, byteorder='little')
        self.type = self.raw >> 254
        self.pm = (self.raw >> 252) & 0x3
        self.slice = (self.raw >> 249) & 0x7
        self.alarm = (self.raw >> 248) & 0x1
        self.eob = (self.raw >> 247) & 0x1
        self.wide_mode = (self.raw >> 239) & 0xFF
        self.valid = (self.raw >> 207) & 0xFFFFFFFF
        self.cycle_count = (self.raw >> 198) & 0x1FF
        self.block_count = (self.raw >> 192) & 0x3F
        self.block_0 = (self.raw >> 128) & 0xFFFFFFFFFFFFFFFF
        self.block_1 = (self.raw >> 64) & 0xFFFFFFFFFFFFFFFF
        self.block_2 = (self.raw >> 0) & 0xFFFFFFFFFFFFFFFF

    def to_html_table_row(self):
        s = '<tr>' \
            f'<td>0x{self.raw:016X}</td>' \
            f'<td>{self.type}</td>' \
            f'<td>{self.pm}</td>' \
            f'<td>{self.slice}</td>' \
            f'<td>{self.alarm}</td>' \
            f'<td>{self.eob}</td>' \
            f'<td>0b{self.wide_mode:08b}</td>' \
            f'<td>0b{self.valid:032b}</td>' \
            f'<td>{self.cycle_count}</td>' \
            f'<td>{self.block_count}</td>' \
            f'<td>0x{self.block_0:016X}</td>' \
            f'<td>0x{self.block_1:016X}</td>' \
            f'<td>0x{self.block_2:016X}</td>' \
            '</tr>'
        return s

    def to_html_header_row(self):
        s = '<tr>' \
            '<th>Raw Data</th>' \
            '<th>Type</th>' \
            '<th>PM</th>' \
            '<th>Slice</th>' \
            '<th>Alarm</th>' \
            '<th>EOB</th>' \
            '<th>Wide Mode</th>' \
            '<th>Valid Mask</th>' \
            '<th>Relative Cycle Count</th>' \
            '<th>Block Count</th>' \
            '<th>Block 0</th>' \
            '<th>Block 1</th>' \
            '<th>Block 2</th>' \
            '</tr>'
        return s

class FailCapture():
    def __init__(self, byte_data):
        self.raw            = int.from_bytes(byte_data, byteorder='little')
        self.type           =  self.raw >> 126
        self.pm             = (self.raw >> 124) & 0x3
        self.slice          = (self.raw >> 121) & 0x7
        self.burst_is_done  = (self.raw >> 120) & 0x1
        self.alarm          = (self.raw >> 119) & 0x1
        self.cycle_count    = (self.raw >>  71) & 0xFFFFFFFFFFFF
        self.valid          = (self.raw >>  70) & 0x1
        self.state_hi       = (self.raw >>  67) & 0x7
        self.error_hi       = (self.raw >>  64) & 0x7
        self.state_lo       = (self.raw >>  32) & 0xFFFFFFFF
        self.error_lo       = (self.raw >>   0) & 0xFFFFFFFF
    
    def to_html_table_row(self):
        if self.valid:
            c = ''
        else:
            c = 'style="color:#aaaaaa"'
        s = '<tr>'\
           f'<td {c}>0x{self.raw:032X}</td>'\
           f'<td {c}>{self.type}</td>'\
           f'<td {c}>{self.pm}</td>'\
           f'<td {c}>{self.slice}</td>'\
           f'<td {c}>{self.burst_is_done}</td>'\
           f'<td {c}>{self.alarm}</td>'\
           f'<td {c}>{self.cycle_count}</td>'\
           f'<td {c}>{self.valid}</td>'\
           f'<td {c}>0b{self.state_hi:03b}{self.state_lo:032b}</td>'\
           f'<td {c}>0b{self.error_hi:03b}{self.error_lo:032b}</td>'\
           '</tr>'
        return s
    
    def to_html_header_row(self):
        s = '<tr>'\
            '<th>Raw Data</th>'\
            '<th>Type</th>'\
            '<th>PM</th>'\
            '<th>Slice</th>'\
            '<th>Burst Done</th>'\
            '<th>Alarm</th>'\
            '<th>Cycle Count</th>'\
            '<th>Valid</th>'\
            '<th>State</th>'\
            '<th>Error</th>'\
            '</tr>'
        return s

class FailBlock():
    def __init__(self, byte_data):
        self.pm2 = FailCapture(byte_data[0:16])
        self.pm3 = FailCapture(byte_data[16:32])
        self.header1 = CtvHeader(byte_data[32:64])
        self.pm0 = FailCapture(byte_data[64:80])
        self.pm1 = FailCapture(byte_data[80:96])
        self.header0 = CtvHeader(byte_data[96:128])
    
    def to_html_table_row(self):
        s = '<tr>'\
           f'<td>{self.header0.pattern_counter:010}</td>'\
           f'<td>{self.header0.trace_index:04}</td>'\
           f'<td>{self.header0.user_cycle:012}</td>'\
           f'<td>0x{self.header0.selectable:08X}</td>'\
           f'<td>{self.header0.repeat_count:06}</td>'\
           f'<td>{self.header0.pattern_cycle:010}</td>'\
           f'<td>{self.header0.vector_address:08X}</td>'\
           f'<td>{self.header0.last_pcall_address:08X}</td>'\
           f'<td>{self.pm0.type}</td>'\
           f'<td>{self.pm0.pm}</td>'\
           f'<td>{self.pm0.slice}</td>'\
           f'<td>{self.pm0.burst_is_done}</td>'\
           f'<td>{self.pm0.alarm}</td>'\
           f'<td>{self.pm0.cycle_count:012}</td>'\
           f'<td>0<br/>1<br/>2<br/>3<br/></td>'\
           f'<td>{self.pm0.valid}<br/>'\
           f'{self.pm1.valid}<br/>'\
           f'{self.pm2.valid}<br/>'\
           f'{self.pm3.valid}</td>'\
           f'<td>0b{self.pm0.state_hi:03b}{self.pm0.state_lo:032b}<br/>'\
           f'0b{self.pm1.state_hi:03b}{self.pm1.state_lo:032b}<br/>'\
           f'0b{self.pm2.state_hi:03b}{self.pm2.state_lo:032b}<br/>'\
           f'0b{self.pm3.state_hi:03b}{self.pm3.state_lo:032b}</td>'\
           f'<td>0b{self.pm0.error_hi:03b}{self.pm0.error_lo:032b}<br/>'\
           f'0b{self.pm1.error_hi:03b}{self.pm1.error_lo:032b}<br/>'\
           f'0b{self.pm2.error_hi:03b}{self.pm2.error_lo:032b}<br/>'\
           f'0b{self.pm3.error_hi:03b}{self.pm3.error_lo:032b}</td>'\
           f'<td>0x{self.header0.raw:064X}<br/>'\
           f'0x{self.pm0.raw:032X}{self.pm1.raw:032X}<br/>'\
           f'0x{self.header1.raw:064X}<br/>'\
           f'0x{self.pm2.raw:032X}{self.pm3.raw:032X}</td>'\
           '</tr>'
        return s
    
    def to_html_header_row(self):
        s = '<tr>'\
            '<th>Pattern Counter</th>'\
            '<th>Trace Index</th>'\
            '<th>User Cycle</th>'\
            '<th>Selectable</th>'\
            '<th>Repeat Count</th>'\
            '<th>Pattern Cycle</th>'\
            '<th>Vector Address</th>'\
            '<th>Last PCALL Address</th>'\
            '<th>Type</th>'\
            '<th>PM</th>'\
            '<th>Slice</th>'\
            '<th>Burst Done</th>'\
            '<th>Alarm</th>'\
            '<th>Cycle Count</th>'\
            '<th>DUT</th>'\
            '<th>Valid</th>'\
            '<th>Pin State</th>'\
            '<th>Error</th>'\
            '<th>Raw Data</th>'\
            '</tr>'
        return s
        
class Trace():
    def __init__(self, byte_data):
        self.raw            = int.from_bytes(byte_data, byteorder='little')
        self.pattern_id     = (self.raw >> 128) & 0xFFFFFFFF
        self.event_type     = (self.raw >> 123) & 0x1F
        self.cache_hit_or_miss      = (self.raw >> 102) & 0x1
        self.ctp_mask       = (self.raw >>  64) & 0x3FFFFFFFFF
        self.destination    = (self.raw >>  32) & 0xFFFFFFFF
        self.source         =  self.raw         & 0xFFFFFFFF
        self.event_type_string = {0b00000: 'No Branch',
                                  0b00001: 'Goto Immediate',
                                  0b00010: 'Call Immediate',
                                  0b00011: 'Compressed Call Immediate',
                                  0b00100: 'PCall Immediate',
                                  0b01000: 'Return',
                                  0b10001: 'Goto Register',
                                  0b10010: 'Call Register',
                                  0b10011: 'Compressed Call Register',
                                  0b10100: 'PCall Register',
                                  0b11000: 'Clear Stack'}
                                  
        self.hit_or_miss_string = {0b0: 'Cache Miss',
                                  0b1: 'Cache Hit'
                                  }
        self.hit_or_miss_string = {0b0: 'Cache Miss',
                                  0b1: 'Cache Hit'
                                  }
    
    def to_html_table_row(self):
        s = '<tr>'\
           f'<td>0x{self.raw:064X}</td>'\
           f'<td>0x{self.pattern_id:08X}</td>'\
           f'<td>{self.event_type_string.get(self.event_type, "")} (0x{self.event_type:02X})</td>'\
           f'<td>{self.hit_or_miss_string.get(self.cache_hit_or_miss, "")} (0x{self.cache_hit_or_miss:02X})</td>'\
           f'<td>0b{self.ctp_mask:038b}</td>'\
           f'<td>0x{self.destination:08X}</td>'\
           f'<td>0x{self.source:08X}</td>'\
           '</tr>'
        return s
    
    def to_html_header_row(self):
        s = '<tr>'\
            '<th>Raw Data</th>'\
            '<th>Pattern ID</th>'\
            '<th>Event Type</th>'\
            '<th>Cache hit or miss</th>'\
            '<th>CTP Mask</th>'\
            '<th>Destination Address</th>'\
            '<th>Source Address</th>'\
            '</tr>'
        return s
    

def ctv_headers(snapshot, member):
    slice_1_ctv_headers_file = snapshot.extractfile(member)
    path_elements = member.name.split('/')
    filename = path_elements[-1]
    filename_without_extension = filename.split('.')[0]
    html_filename = os.path.join(final_dst, filename_without_extension + '.html')
    with open(html_filename, 'w') as html:
        row_number = 0
        print('<html>', file=html)
        print('<head>', file=html)
        print(f'<title>{filename}</title>', file=html)
        print('<style>', file=html)
        print('table, th, td {', file=html)
        print('border: 1px solid black;', file=html)
        print('border-collapse: collapse;', file=html)
        print('font-family: monospace;', file=html)
        print('text-align: center;', file=html)
        print('padding: 2px;', file=html)
        print('}', file=html)
        print('</style>', file=html)
        print('</head>', file=html)
        print('<body>', file=html)
        print(f'snapshot file: {snapshot.name}<br/>', file=html)
        print(f'CTV headers file: {member.name}<br/>', file=html)
        print(f'<br/>', file=html)
        print('<table>', file=html)
        while True:
            header_data = slice_1_ctv_headers_file.read(32)
            if header_data == b'':
                break
            ctv_header = CtvHeader(header_data)
            if (row_number % 20) == 0:
                print(ctv_header.to_html_header_row(), file=html)
            row_number += 1
            print(ctv_header.to_html_table_row(), file=html)
        print('</table>', file=html)
        print('</body>', file=html)
        print('</html>', file=html)

def ctv_data(snapshot, member):
    ctv_data_file = snapshot.extractfile(member)
    path_elements = member.name.split('/')
    filename = path_elements[-1]
    filename_without_extension = filename.split('.')[0]
    html_filename = os.path.join(final_dst, filename_without_extension)
    new_header_block = True
    with PmCtvData(html_filename + '_pm0', snapshot, member) as pm0,\
         PmCtvData(html_filename + '_pm1', snapshot, member) as pm1,\
         PmCtvData(html_filename + '_pm2', snapshot, member) as pm2,\
         PmCtvData(html_filename + '_pm3', snapshot, member) as pm3:
        pm_list = [pm0, pm1, pm2, pm3]
        while True:
            ctv_data_block = ctv_data_file.read(32)
            if ctv_data_block == b'':
                break
            if new_header_block:
                data = CtvHeaderData(ctv_data_block)
                current_pm = data.pm
                pm_list[current_pm].add(data)
                new_header_block = False
                block_count = math.ceil((data.block_count - 3)/4)
                new_data_block = True

            elif block_count > 0:
                data = CtvData(ctv_data_block)
                pm_list[current_pm].add(data, new_data_block)
                block_count -= 1
                new_data_block = False

            if block_count == 0:
                new_header_block = True



class PmCtvData():
    def __init__(self, filename, snapshot, member):
        self.filename = filename
        
    def __enter__(self):
        self.html = html = open(self.filename + '.html', 'w')
        print('<html>', file=html)
        print('<head>', file=html)
        print(f'<title>{self.filename}</title>', file=html)
        print('<style>', file=html)
        print('table, th, td {', file=html)
        print('border: 1px solid black;', file=html)
        print('border-collapse: collapse;', file=html)
        print('font-family: monospace;', file=html)
        print('text-align: center;', file=html)
        print('padding: 2px;', file=html)
        print('}', file=html)
        print('</style>', file=html)
        print('</head>', file=html)
        print('<body>', file=html)
        print(f'snapshot file: {snapshot.name}<br/>', file=html)
        print(f'CTV headers file: {member.name}<br/>', file=html)
        print(f'<br/>', file=html)
        print('<table>', file=html)
        self.row_number = 0
        return self
        
    def add(self, ctv_data, all_data=False):
        html = self.html
        if (self.row_number % 20) == 0 or all_data:
            print(ctv_data.to_html_header_row(), file=html)
        self.row_number += 1
        print(ctv_data.to_html_table_row(), file=html)
        
    def __exit__(self, exc_type, exc_value, exc_traceback):
        html = self.html
        print('</table>', file=html)
        print('</body>', file=html)
        print('</html>', file=html)
        html.close()


def fail_capture(snapshot, member):
    ctv_data_file = snapshot.extractfile(member)
    path_elements = member.name.split('/')
    filename = path_elements[-1]
    filename_without_extension = filename.split('.')[0]
    # slice = filename.split('_')[1]
    html_filename = os.path.join(final_dst, filename_without_extension + '.html')
    with open(html_filename, 'w') as html:
        row_number = 0
        print('<html>', file=html)
        print('<head>', file=html)
        print(f'<title>{filename}</title>', file=html)
        print('<style>', file=html)
        print('table, th, td {', file=html)
        print('border: 1px solid black;', file=html)
        print('border-collapse: collapse;', file=html)
        print('font-family: monospace;', file=html)
        print('text-align: center;', file=html)
        print('padding: 2px;', file=html)
        print('}', file=html)
        print('</style>', file=html)
        print('</head>', file=html)
        print('<body>', file=html)
        print(f'snapshot file: {snapshot.name}<br/>', file=html)
        print(f'CTV headers file: {member.name}<br/>', file=html)
        print(f'<br/>', file=html)
        print('<table>', file=html)
        while True:
            fail_data = ctv_data_file.read(128)
            if fail_data == b'':
                break
            fail_capture_structure = FailBlock(fail_data)
            if (row_number % 20) == 0:
                print(fail_capture_structure.to_html_header_row(), file=html)
            row_number += 1
            print(fail_capture_structure.to_html_table_row(), file=html)
        print('</table>', file=html)
        print('</body>', file=html)
        print('</html>', file=html)


def registers(snapshot, member):
    path_elements = member.name.split('/')
    _, fpga, filename = path_elements
    filename_without_extension = fpga[:-2].lower() + '_' + filename.split('.')[0]
    data = register_data(snapshot, member)
    register_addresses = sorted(set([x.address for x in data]))
    html_filename = os.path.join(final_dst, filename_without_extension + '.html')
    with open(html_filename, 'w') as html:
        print('<html>', file=html)
        print('<head>', file=html)
        print(f'<title>{fpga}/{filename}</title>', file=html)
        print('<style>', file=html)
        print('table, th, td {', file=html)
        print('border: 1px solid black;', file=html)
        print('border-collapse: collapse;', file=html)
        print('font-family: monospace;', file=html)
        print('text-align: center;', file=html)
        print('padding: 3px;', file=html)
        print('}', file=html)
        print('</style>', file=html)
        print('</head>', file=html)
        print('<body>', file=html)
        print(f'snapshot file: {snapshot.name}<br/>', file=html)
        print(f'CTV headers file: {member.name}<br/>', file=html)
        print(f'<br/>', file=html)
        print('<table>', file=html)
        for address in register_addresses:
            slice_registers = [x for x in data if x.address == address]
            name = slice_registers[0].name
            s = '<tr>'
            s += f'<td>{name}</td>'
            s += f'<td>0x{address:04X}</td>'
            for slice in range(5):
                r = [x for x in slice_registers if x.slice == slice]
                if r:
                    value = r[0].value
                    s += f'<td>0x{value:08X}</td>'
                else:
                    s += f'<td></td>'
            s += '</tr>'
            print(s, file=html)
        print('</table>', file=html)
        print('</body>', file=html)
        print('</html>', file=html)

def register_data(snapshot, member):
    ctv_data_file = snapshot.extractfile(member)
    file_rows = [x.decode('utf-8') for x in ctv_data_file.readlines()]
    data_rows = [x for x in file_rows if x.startswith('0x')]
    return [RegisterData(x) for x in data_rows]


class RegisterData():
    def __init__(self, init_string):
        init_tokens = init_string.strip().split(',')
        self.name = init_tokens[3]
        self.address = int(init_tokens[0], 16)
        self.slice = int(init_tokens[1])
        self.value = int(init_tokens[2], 16)


def trace(snapshot, member):
    ctv_data_file = snapshot.extractfile(member)
    path_elements = member.name.split('/')
    filename = path_elements[-1]
    filename_without_extension = filename.split('.')[0]
    # slice = filename.split('_')[1]
    html_filename = os.path.join(final_dst, filename_without_extension + '.html')
    with open(html_filename, 'w') as html:
        row_number = 0
        print('<html>', file=html)
        print('<head>', file=html)
        print(f'<title>{filename}</title>', file=html)
        print('<style>', file=html)
        print('table, th, td {', file=html)
        print('border: 1px solid black;', file=html)
        print('border-collapse: collapse;', file=html)
        print('font-family: monospace;', file=html)
        print('text-align: center;', file=html)
        print('padding: 2px;', file=html)
        print('}', file=html)
        print('</style>', file=html)
        print('</head>', file=html)
        print('<body>', file=html)
        print(f'snapshot file: {snapshot.name}<br/>', file=html)
        print(f'CTV headers file: {member.name}<br/>', file=html)
        print(f'<br/>', file=html)
        print('<table>', file=html)
        while True:
            trace_data = ctv_data_file.read(32)
            if trace_data == b'':
                break
            trace_structure = Trace(trace_data)
            if (row_number % 20) == 0:
                print(trace_structure.to_html_header_row(), file=html)
            row_number += 1
            print(trace_structure.to_html_table_row(), file=html)
        print('</table>', file=html)
        print('</body>', file=html)
        print('</html>', file=html)


def pattern(snapshot, member, offset=0):
    pattern_file = snapshot.extractfile(member)
    path_elements = member.name.split('/')
    filename = path_elements[-1]
    filename_without_extension = filename.split('.')[0]
    # slice = filename.split('_')[1]
    html_filename = os.path.join(final_dst, filename_without_extension + '.html')
    with open(html_filename, 'w') as html:
        row_number = offset
        print('<html>', file=html)
        print('<head>', file=html)
        print(f'<title>{filename}</title>', file=html)
        print('<style>', file=html)
        print('table, th, td {', file=html)
        print('border: 1px solid black;', file=html)
        print('border-collapse: collapse;', file=html)
        print('font-family: monospace;', file=html)
        print('text-align: center;', file=html)
        print('padding: 2px;', file=html)
        print('}', file=html)
        print('</style>', file=html)
        print('</head>', file=html)
        print('<body>', file=html)
        print(f'snapshot file: {snapshot.name}<br/>', file=html)
        print(f'CTV headers file: {member.name}<br/>', file=html)
        print(f'<br/>', file=html)
        print('<table>', file=html)
        while True:
            vector_data = pattern_file.read(16)
            if vector_data == b'':
                break
            pattern_word = PatternWord(row_number, vector_data)
            if (row_number % 32) == 0:
                print(pattern_word.to_html_header_row(), file=html)
            row_number += 1
            print(pattern_word.to_html_table_row(), file=html)
        print('</table>', file=html)
        print('</body>', file=html)
        print('</html>', file=html)

def unhandled(snapshot, member, offset=0):
    if member.isfile():
        print(f'Unhandled data in {member}')

method_table = {'registers.csv': registers,
                'slice_0_ctv_headers.bin': ctv_headers,
                'slice_1_ctv_headers.bin': ctv_headers,
                'slice_2_ctv_headers.bin': ctv_headers,
                'slice_3_ctv_headers.bin': ctv_headers,
                'slice_4_ctv_headers.bin': ctv_headers,
                'slice_0_ctv_data.bin': ctv_data,
                'slice_1_ctv_data.bin': ctv_data,
                'slice_2_ctv_data.bin': ctv_data,
                'slice_3_ctv_data.bin': ctv_data,
                'slice_4_ctv_data.bin': ctv_data,
                'slice_0_pm_0_error_stream.bin': fail_capture,
                'slice_0_pm_1_error_stream.bin': fail_capture,
                'slice_0_pm_2_error_stream.bin': fail_capture,
                'slice_0_pm_3_error_stream.bin': fail_capture,
                'slice_1_pm_0_error_stream.bin': fail_capture,
                'slice_1_pm_1_error_stream.bin': fail_capture,
                'slice_1_pm_2_error_stream.bin': fail_capture,
                'slice_1_pm_3_error_stream.bin': fail_capture,
                'slice_2_pm_0_error_stream.bin': fail_capture,
                'slice_2_pm_1_error_stream.bin': fail_capture,
                'slice_2_pm_2_error_stream.bin': fail_capture,
                'slice_2_pm_3_error_stream.bin': fail_capture,
                'slice_3_pm_0_error_stream.bin': fail_capture,
                'slice_3_pm_1_error_stream.bin': fail_capture,
                'slice_3_pm_2_error_stream.bin': fail_capture,
                'slice_3_pm_3_error_stream.bin': fail_capture,
                'slice_4_pm_0_error_stream.bin': fail_capture,
                'slice_4_pm_1_error_stream.bin': fail_capture,
                'slice_4_pm_2_error_stream.bin': fail_capture,
                'slice_4_pm_3_error_stream.bin': fail_capture,
                'slice_0_trace.bin': trace,
                'slice_1_trace.bin': trace,
                'slice_2_trace.bin': trace,
                'slice_3_trace.bin': trace,
                'slice_4_trace.bin': trace,
                'slice_0_pattern.bin': pattern,
                'slice_1_pattern.bin': pattern,
                'slice_2_pattern.bin': pattern,
                'slice_3_pattern.bin': pattern,
                'slice_4_pattern.bin': pattern}
        

if __name__ == '__main__':
    args = parse_args()
    snapshot_file = os.path.abspath(args.snapshot)
    dst_folder = os.path.dirname(snapshot_file)
    with tarfile.open(snapshot_file, 'r:gz') as snapshot:
        for member in snapshot.getmembers():
            pattern_file = snapshot.extractfile(member)
            path_elements = member.name.split('/')
            final_dst = os.path.join(dst_folder, path_elements[0])
            os.makedirs(final_dst, exist_ok=True)
            print(f'processing {member.name}')
            name = member.name.split('/')[-1]
            if 'address_0x' in name and '.bin' in name:
                offset_string = (name.split('.bin')[0]).split('address_')[1]
                offset = int(offset_string, 16)
                name = name.replace('_address_' + offset_string, '')
                method_table.get(name, unhandled)(snapshot, member, offset)
            else:
                method_table.get(name, unhandled)(snapshot, member)
                        
    # webbrowser.open('.')