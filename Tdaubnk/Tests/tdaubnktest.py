# Copyright 2017 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

from Common.fval import TestCase
from Tdaubnk.tdaubnktb.env import Env

class TdaubnkTest(TestCase):
    dutName = 'tdaubnk'
    
    def setUp(self):
        self.LogStart()
        self.env = Env(self)

    def tearDown(self):
        #self.env.take_snapshot_if_enabled()
        self.LogEnd()
        