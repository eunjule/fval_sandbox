################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: Diags.py
#-------------------------------------------------------------------------------
#     Purpose: TDAUBANK basic functionality tests, default states
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang 
#        Date: 16/02/17
#       Group: HDMT FPGA Validation
################################################################################

from collections import defaultdict
from decimal import Decimal
import os
import random
import time
import unittest

from Common import fval
from Rc2.instrument.rc2 import Rc2
from Tdaubnk.instrument.adt7411Regs import *
from Tdaubnk.instrument.tdaubnkRegs import *
from Common.instruments.tester import get_tester as cards_in_tester
from Tdaubnk.Tests.tdaubnktest import TdaubnkTest


TDAUBJT_Conf_Load = os.path.join(os.path.dirname(__file__),'TDAUBJT_Conf.bin')

READ_TEMPERATURE_1 = 0x8D
READ_TEMPERATURE_2 = 0x8E
PMBUS_REVISION = 0x98
RANDOM_DATA_TO_WRITE = 0x55
SET_TRANSMIT_BIT = 0x1
RESET_TRANSMIT_BIT_TDAU = 0x0
BMR454_ADDRESS_PLUS_READ = 0x27             #address = 0x13 Read = 1
BMR454_ADDRESS_PLUS_WRITE = 0x26            #address = 0x13 Write = 0
TDAU_ADDRESS_PLUS_READ = 0xDF               #address = 0x6F Read = 1
TDAU_ADDRESS_PLUS_WRITE = 0xDE              #address = 0x6F Write = 0
STOPCNV    = 0x10                           # Stop temperature conversions
EXTCAL     = 0x0D                           # Perform extended calibration
SCOCALIB   = 0x20                           # Perform single current calibration
STARTCNV   = 0x01                           # Start temperature Conversion
STATREQ    = 0x1F                           # Status request to read all 4 channels
READERROR  = 0x02                           # Read extended error codes
READBACK_LENGTH_8 = 8
READBACK_LENGTH_4 = 4
SELECT_ONBOARD_CLOCK = 0
SELECT_MS_CLOCK = 1
SYSTEM_CONVERSION_ERROR = 160               # 160 = 1010 0000 -> Verify if System error or Conversion error occured 
MIN_TEMP = 15
MAX_TEMP = 38
EEPROM_DATA_LENGTH = 0x800
RESET_TRIGGER_DOMAIN  = 0
RESET_TRIGGER_TRANSCEIVER = 0
RESET_TRIGGER_CORE = 0
SET_TRIGGER_DOMAIN  = 1
SET_TRIGGER_TRANSCEIVER = 1
SET_TRIGGER_CORE = 1
#Below values are calculated based on device specification of 5% tolerance.
ADT7144_AIN1_HIGH_LIMIT_DATA =  3.465
ADT7144_AIN1_LOW_LIMIT_DATA =  3.135
ADT7144_AIN2_HIGH_LIMIT_DATA =  1.575
ADT7144_AIN2_LOW_LIMIT_DATA =  1.425
ADT7144_AIN3_HIGH_LIMIT_DATA =  1.26
ADT7144_AIN3_LOW_LIMIT_DATA =  1.14
ADT7144_AIN4_HIGH_LIMIT_DATA =  2.00
ADT7144_AIN4_LOW_LIMIT_DATA = 1.5
ADT7144_AIN6_HIGH_LIMIT_DATA =  12.6
ADT7144_AIN6_LOW_LIMIT_DATA =  11.4
LSBMASK = 0x03
P2P_DEFAULT_TEMP_VALUE = 0x42DE42DE
TDAU_AUTO_ENABLE = 1
TDAU_AUTO_DISABLE = 0
no_valid_packet_data = 0xFFFF
packet_read_time = 120
S2HOffDelay = 2
S2H_packet_after_reset = 0xFFFF
min_S2H_packets_in_120_seconds = 30000                   # Packet count cannot be less then 30000 per 120 seconds(250*120).
max_S2H_packets_in_120_seconds = 30100                   # added buffer of 30 packets as software cannot generate exact delay
min_S2H_packets_per_sec = 250.0                   # Packet count cannot be less then 250 per second.
max_S2H_packets_per_sec = 252.0                   # added buffer of 2 packets as software cannot generate exact delay
seconds_to_reach_max_count = 5
min_bit_rate = 2000
med_bit_rate = 50000
max_bit_rate = 200000
BitRateSteps = 1000
PECI_device_count = 18
address_of_PECI_channel = 0x48
read_100_times = 100
random_bit_rate_20_times = 20
BitRate15K = 15000
bit_rate_50k = 50000
error_rate_50_percent = 50
error_rate_5_percent = 5
NoValidPacketData = 0xFFFF
expected_count_after_rollover = 3350

class General(TdaubnkTest):

    def MiniTdbBltEepromReadTest(self):
        '''Read BLT EEPROM content. Verify if it has correct data (Device name)in it'''
        for (slot,tdaubnk) in self.env.instruments.items():
            data = tdaubnk.EepromRead()
            self.Log('info', 'Data received from BLT EEPROM')
            if len(data) != EEPROM_DATA_LENGTH:
                self.Log('error', f'Expected {EEPROM_DATA_LENGTH} bytes but received {len(data)} bytes')
            blts = tdaubnk.instrumentblt
            self.Log('info', 'BLT: DeviceName = {}'.format(blts.DeviceName))
            if blts.DeviceName.encode() not in data:
                self.Log('error', f'"{blts.DeviceName}" not found in EEPROM data')
    
    def DirectedEepromReadExaustiveTest(self):
        '''Read BLT EEPROM content multiple times. Verify if we receive same content all the times.'''
        for (slot,tdaubnk) in self.env.instruments.items():
            firstdata = tdaubnk.EepromRead()
            if len(firstdata) != EEPROM_DATA_LENGTH:
                self.Log('error', f'Expected {EEPROM_DATA_LENGTH} bytes but received {len(firstdata)} bytes')
            self.Log('info', 'First Data received ')
            for loop in range(5): 
                seconddata = tdaubnk.EepromRead()
                if len(seconddata) != EEPROM_DATA_LENGTH:
                    self.Log('error', 'BLT EEPROM Read:Data Count mismatch')
                self.Log('info','BLT EEPROM Read: Count {} . Expected data received'.format(loop))
                if firstdata != seconddata:
                    self.Log('error', 'BLT EEPROM Read:Data mismatch Read Count {}'.format(loop))
                
class ClockSource(TdaubnkTest):

    @unittest.skip('Broken test, also causes subsequent tests to fail')
    def DirectedClockCompensationTest(self):
        for (slot, tdaubnk) in self.env.instruments.items():
            rc2 = self.read_tester_status_before_test(slot, tdaubnk)
            self.send_random_triggers(tdaubnk, rc2,'Enabled')
            self.disable_clock_compensation(slot, tdaubnk, rc2)
            self.reset_aurora_link()
            self.send_random_triggers(tdaubnk,rc2,'Disabled')
            self.enable_clock_compensation(slot, tdaubnk, rc2)
            self.reset_aurora_link()
            self.send_random_triggers(tdaubnk,rc2,'Enabled')
    def read_tester_status_before_test(self, slot, tdaubnk):
        rc2 = Rc2(slot)
        rc2.read_clock_compensation_status()
        rc2.read_clock_compensation_capability()
        gpiostatus = tdaubnk.Read(GpioReg)
        self.Log('info', 'TDAU Bank GPIO Reg status before disabling Clock Compensation = 0x{:X}'.format(gpiostatus.value))
        capability_register_status = tdaubnk.Read(CapabilityReg)
        self.Log('info','TDAU Bank capability register test = 0x{:X}'.format(capability_register_status.value))
        return rc2

    def send_random_triggers(self, tdaubnk,rc2,cc_status):
            for trigger_sent_count in range(0, 100000):
                value = 0x08000000
                while ((value & 0xFC000000)):
                    value = random.getrandbits(32) & 0xFFF7FFFF
                expected = value
                SendTrigger = SendTriggerReg()
                SendTrigger.value = expected
                if (SendTrigger.Payload == 0x1 and SendTrigger.CardType == 0x5):
                    SendTrigger.CardType = 0
                    SendTrigger.Payload = 0
                    expected = SendTrigger.value
                tdaubnk.Write(SendTrigger)
                self.Log('debug', '{})  Clock Compensation: {} , TDAUBANK sent trigger 0x{:x} '.format(trigger_sent_count,cc_status,expected))
                self.receive_random_triggers_at_tdaubank(tdaubnk,expected,rc2,cc_status,trigger_sent_count)

    def receive_random_triggers_at_tdaubank(self, tdaubnk, expected,rc2,cc_status,trigger_sent_count):
        for read_trigger_count in range(100):
            triggertdaubnkdown = tdaubnk.Read(LastReceivedTriggerReg)
            if triggertdaubnkdown.value == expected:
                self.Log('debug', 'TDAUBANK received trigger 0x{:x} '.format(expected))
                self.read_clock_count(tdaubnk,cc_status,trigger_sent_count)
                break
            else:
                self.Log('warning', 'TDAUBANK waiting for trigger.Received {} at count :{} '.format(triggertdaubnkdown.value,read_trigger_count))
                trigerrreg = tdaubnk.Read(TriggerErrorReg)
                self.Log('info', 'Trigger Error Reg status = 0x{:X}'.format(trigerrreg.value))
                rc2.rc_aurora_status()
                self.read_clock_count(tdaubnk,cc_status,trigger_sent_count)
                continue

    def read_clock_count(self,tdaubnk,cc_status,trigger_sent_count):
        expected_clock_count = 73
        clock_count_deviation = 3
        max_deviation = expected_clock_count + clock_count_deviation
        min_deviation = expected_clock_count - clock_count_deviation
        clock_count = tdaubnk.Read(TriggerLoopbackCountReg)
        if clock_count.value <= max_deviation and clock_count.value >= min_deviation:
            self.Log('debug', 'clock count is within expected limit :{} '.format(clock_count.value))
        else:
            if cc_status == 'Disabled':
                log_level = 'error'
            else:
                log_level = 'warning'
            self.Log(log_level, 'Clock count: {} Exceeded expected limit by :{} error detected at {}th trigger'.format(
                clock_count.value, clock_count.value - expected_clock_count, trigger_sent_count))

    def disable_clock_compensation(self,slot, tdaubnk, rc2):
        self.Log('info','*********Disabling Clock Compensation from Tdau bank***********')
        gpiostatus = GpioReg()
        gpiostatus.value = 0
        tdaubnk.Write(gpiostatus)
        gpiostatus = tdaubnk.Read(GpioReg)
        self.Log('info', 'TDAUBANK GPIO Reg status after disabling Clock Compensation = 0x{:X}'.format(gpiostatus.value))
        self.Log('info', '*********Disabling Clock Compensation from RC ***********')
        rc2.disable_clock_compensation()
        self.Log('info', '*********Disabling Clock Compensation from HPCC ***********')
        self.change_clock_compensation_status_for_hpcc('Disable')

    def change_clock_compensation_status_for_hpcc(self,status):
        tester_status = cards_in_tester()
        tester_status.discover_all_instruments()
        for device in tester_status.devices:
            try:
                if device.name() == 'Hpcc':
                    fval.Log('info', "{}ing Clock compensation for HPCC at slot {}".format(status,device.slot))
                    if status == 'Disable':
                        device.disable_dc_fpga_clock_compensation()
                    else:
                        device.enable_dc_fpga_clock_compensation()
            except:
                fval.Log('info','unable to disable Clock compensation for Instrument {} on slot {}'.format(device.name(), device.slot))


    def enable_clock_compensation(self,slot, tdaubnk, rc2):
        self.Log('info', '*********Enabling Clock Compensation from Tdau bank***********')
        gpiostatus = GpioReg()
        gpiostatus.value = 1
        tdaubnk.Write(gpiostatus)
        gpiostatus = tdaubnk.Read(GpioReg)
        self.Log('info', 'TDAUBANK GPIO Reg status after enabling Clock Compensation = 0x{:X}'.format(gpiostatus.value))
        self.Log('info', '*********Enabling Clock Compensation from RC ***********')
        rc2.enable_clock_compensation()
        self.Log('info', '*********Enabling Clock Compensation from HPCC ***********')
        self.change_clock_compensation_status_for_hpcc('Enable')

    def reset_aurora_link(self):
        for (slot,tdaubnk) in self.env.instruments.items():
            self.ResetTDAUBankFPGA(slot,1,1,1)
            time.sleep(0.05)
            failcount = 0
            for repcount in range(4):
                self.ResetRCAuroraBus(slot)
                time.sleep(0.05)
                self.ResetTDAUBankFPGA(slot,1,0,0)
                trigerrreg = tdaubnk.Read(TriggerErrorReg)
                if trigerrreg.value == 0:
                    break
                else:
                    failcount += 1
            if failcount == 4:
                self.Log('error', 'Trigger Error Reg status = 0x{:X}'.format(trigerrreg.value))
            self.Log('info', 'Trigger Error Reg status = 0x{:X}'.format(trigerrreg.value))

    def MiniReferenceClockSourceSwapTest(self):
        ''' o	Test to validate if both clock sources (On board clock and MS clock) can be used for triggers. 
            o	By default on-board clock is selected. Test verifies if there is any error in trigger error register (0x8100) when on-board clock is in use. 
            o	Changes source of clock to MS clock by setting ‘TriggerReferenceClockSelect´ bit from GPIO register (0x8050).
            o	Resets TDAU Bank FPGA using FPGA reset register (0x8010).
            o	Resets RC aurora bus.
            o	Reads an error in trigger error register (0x8100) when MS clock is in use.
            o	If error register reports any error .Report error status and fail the test.'''
        self.clockselect(SELECT_ONBOARD_CLOCK)                 # Select On board clock
        self.clockselect(SELECT_MS_CLOCK)                    	 # Select MS clock
        self.clockselect(SELECT_ONBOARD_CLOCK)                 # Select On board clock
        
    def DirectedTriggerExhaustiveOnboardClkTest(self):
        ''' o	Select on-board clock.
            o	Using random generator, generate a random value which can be sent through trigger bus.
            o	Write generated value to ‘SendTriggerReg’(0x8120)
            o	Read RC trigger up register for specific slot where TDAU is connected verify if same trigger is received.
            o	Send trigger value from RC trigger down register.
            o	Read tdaubank trigger down register(LastReceivedTriggerReg 0x 8110) verify if same trigger is received.
            o	Continue this loop for 200000 time.
            o	If system fails to receive trigger at any location , then fail the test.'''    
        self.clockselect(SELECT_ONBOARD_CLOCK)                     				# Select On board clock
        self.Sendrandomtrigger(SELECT_MS_CLOCK)									# Select MS clock
    
    def DirectedTriggerExhaustiveMSClkTest(self):
        ''' o	Select MS clock.
            o	Using random generator, generate a random value which can be sent through trigger bus.
            o	Write generated value to ‘SendTriggerReg’(0x8120)
            o	Read RC trigger up register for specific slot where TDAU is connected verify if same trigger is received.
            o	Send trigger value from RC trigger down register.
            o	Read tdaubank trigger down register(LastReceivedTriggerReg 0x 8110) verify if same trigger is received.
            o	Continue this loop for 200000 time.
            o	If system fails to receive trigger at any location , then fail the test.'''
        self.clockselect(SELECT_MS_CLOCK)                     				# Select MS clock
        self.Sendrandomtrigger(SELECT_ONBOARD_CLOCK)							# Select on board clock if test fails

    def Sendrandomtrigger(self,clktoswapiffail):        
        for (slot,tdaubnk) in self.env.instruments.items():
            rc2 = Rc2(slot)
            fail_cnt_at_RC_UP = 0
            fail_cnt_at_RC_Down = 0
            fail_cnt_at_TDAU_Down = 0
            cnt = 0
            tottriggercnt = 0
            for i in range(0,20000):
                value = 0x08000000
                while ((value & 0xFC000000)):
                    value = random.getrandbits(32) & 0xFFF7FFFF
                                  
                expected = value
                tottriggercnt += 1
                SendTrigger = SendTriggerReg()
                SendTrigger.value = expected
                if (SendTrigger.Payload == 0x1 and SendTrigger.CardType == 0x5):                    
                      SendTrigger.CardType = 0
                      SendTrigger.Payload = 0
                      expected = SendTrigger.value
                tdaubnk.Write(SendTrigger)
                self.Log('debug','TDAUBANK sent trigger 0x{:x} to RC'.format(expected))
                #to create delay between sending trigger and reading results
                for j in range(0,1000):
                    cnt = cnt + 1
                
                triggerrcup = rc2.read_trigger_up(slot)
                if triggerrcup != expected:
                     self.Log('error','(TDAU->RC)RC received incorrect trigger: expected=0x{:x}, actual=0x{:} for count {}'.format(expected,triggerrcup,i))
                     trigerrreg = tdaubnk.Read(TriggerErrorReg)
                     self.Log('error', 'Trigger Error Reg status for on-board clock = 0x{:X}'.format(trigerrreg.value))
                     fail_cnt_at_RC_UP = fail_cnt_at_RC_UP + 1
                else:
                     self.Log('debug', '(TDAU->RC)RC received trigger from DC 0x{:x}'.format(triggerrcup))
                
                triggerrcdown = rc2.read_trigger_down(slot)
                if triggerrcdown != expected:
                    self.Log('error', '(RCup->RCDown)RC did not sent correct trigger to TDAU: expected=0x{:x} actual=0x{:x} for count {}'.format(expected,triggerrcdown,i))
                    trigerrreg = tdaubnk.Read(TriggerErrorReg)
                    self.Log('error', 'Trigger Error Reg status for on-board clock = 0x{:X}'.format(trigerrreg.value))
                    fail_cnt_at_RC_Down = fail_cnt_at_RC_Down + 1
                else:
                    self.Log('debug', '(RCup->RCDown)RC sent trigger downstream 0x{:x}'.format(triggerrcdown))
                    
                triggertdaubnkdown = tdaubnk.Read(LastReceivedTriggerReg)
                if triggertdaubnkdown.value != expected:
                    self.Log('error', '(RC->Tdaubank)Tdaubank did not received correct trigger from RC: expected=0x{:x} actual=0x{:x} for count {}'.format(expected,triggertdaubnkdown.value,i))
                    trigerrreg = tdaubnk.Read(TriggerErrorReg)
                    self.Log('error', 'Trigger Error Reg status for on-board clock = 0x{:X}'.format(trigerrreg.value))
                    fail_cnt_at_TDAU_Down = fail_cnt_at_TDAU_Down + 1
                else:
                    self.Log('debug', '(RC->Tdaubank)TDAU received trigger from RC 0x{:x}'.format(triggertdaubnkdown.value))
                
                if fail_cnt_at_RC_UP == 10 or fail_cnt_at_RC_Down == 10 or fail_cnt_at_TDAU_Down == 10:
                    self.Log('error', 'Total failing triggers TDAU->RC: {} RCup->RCDown: {} RC->Tdaubank: {} '.format(fail_cnt_at_RC_UP,fail_cnt_at_RC_Down,fail_cnt_at_TDAU_Down))
                    trigerrreg = tdaubnk.Read(TriggerErrorReg)
                    self.Log('error', 'Trigger Error Reg status = 0x{:X}'.format(trigerrreg.value))
                    if trigerrreg.value != 0:
                        self.Log('info', 'Changing Clock source ')
                        self.clockselect(clktoswapiffail)                     # Change Clock Source
                    break
            
            self.Log('info', 'Total Triggers : {}  Each leg Failing triggers -> TDAU->RC: {} RCup->RCDown: {} RC->Tdaubank: {} '.format(tottriggercnt,fail_cnt_at_RC_UP,fail_cnt_at_RC_Down,fail_cnt_at_TDAU_Down))

    def clockselect(self,clockselectbit):
        for (slot,tdaubnk) in self.env.instruments.items():
            gpiostatus = GpioReg()
            gpiostatus.TriggerReferenceClockSelect = clockselectbit                 # Select clock Source
            tdaubnk.Write(gpiostatus)
            gpiostatus = tdaubnk.Read(GpioReg)
            self.Log('info', 'GPIO Reg status = 0x{:X}'.format(gpiostatus.value))
            self.ResetTDAUBankFPGA(slot,1,1,1)
            time.sleep(0.05)
            failcount = 0
            for repcount in range(4):
                self.ResetRCAuroraBus(slot)
                time.sleep(0.05)
                self.ResetTDAUBankFPGA(slot,1,0,0)
                trigerrreg = tdaubnk.Read(TriggerErrorReg)
                if trigerrreg.value == 0:
                    break
                else:
                    failcount += 1
            if failcount == 4:
                self.Log('error', 'Trigger Error Reg status = 0x{:X}'.format(trigerrreg.value))   
            self.Log('info', 'Trigger Error Reg status = 0x{:X}'.format(trigerrreg.value))   
                
    
    def ResetTDAUBankFPGA(self,slot,ResetTriggerDomain,ResetTriggerTransceiver,ResetTriggerCore):
        for (slot,tdaubnk) in self.env.instruments.items():
            FPGAResets = FPGAResetsReg()
            FPGAResets.ResetTriggerDomain = ResetTriggerDomain            
            FPGAResets.ResetTriggerTransceiver = ResetTriggerTransceiver
            FPGAResets.ResetTriggerCore = ResetTriggerCore
            tdaubnk.Write(FPGAResets)
    
    def ResetRCAuroraBus(self,slot):        
        rc2 = Rc2(slot)
        AuroraUp = False
        for slot in range(12):
            rc2.reset_trigger_interface(slot)
            time.sleep(0.05)
            
        # Reset the Trigger Cores One at a Time, after GTx Reset
        data = rc2.Read('Reset')
        data.AuroraResetCards0_3 = 1
        rc2.Write('Reset', data)
        rc2.wait_for_reset_to_clear()

        data = rc2.Read('Reset')
        data.AuroraResetCards4_7 = 1
        rc2.Write('Reset', data)
        rc2.wait_for_reset_to_clear()

        data = rc2.Read('Reset')
        data.AuroraResetCards8_11 = 1
        rc2.Write('Reset', data)
        rc2.wait_for_reset_to_clear()
        slotUp = rc2.log_aurora_status()

class BMR454(TdaubnkTest):


    def DirectedBMR454TemperatureReadExhaustiveTest(self):
        ''' •	Send command (8Dh & 8Eh) to read temperatures of BMR454 device. 
            •	Use hil API - tdbBmr454Read to access the device.
            •	Repeat above steps multiple times and see if system fails to report temperature.'''
        for (slot,tdaubnk) in self.env.instruments.items():
            cmd1 = READ_TEMPERATURE_1
            AverageTemperatureS1 = 0
            for i in range(0,100):
                Readfromsoruceone = tdaubnk.PmbusRead(cmd1)
                self.Log('debug','Readfromsoruceone:{}'.format(Readfromsoruceone))
                if (Readfromsoruceone > 32 and Readfromsoruceone < 40):
                    self.Log('warning','BMR454:Temperature is above normal range for read count: {} Current Temperatuere: {}'.format(i,Readfromsoruceone))
                if (Readfromsoruceone > 40):
                    self.Log('error','BMR454:Temperature for source 1 is out of range. Current Temperatuere {}'.format(Readfromsoruceone))
                AverageTemperatureS1 = AverageTemperatureS1 + Readfromsoruceone
            self.Log('info','BMR454:Average temperature from source 1 :{}'.format(AverageTemperatureS1 / 100))
           
    def MiniBMR454TXFIFOCountErrorBitCheckingTest(self):
        '''Verify if TX FIFO Count Error Bit is reporting expected error or not'''
        for (slot,tdaubnk) in self.env.instruments.items():
            self.ResetFIFO()
            BMR454TXFIFOData = BMR454TXFIFODataReg()
            BMR454TXFIFOData.BMR454TXFIFOData = RANDOM_DATA_TO_WRITE
            self.Log('debug','BMR454TXFIFODataReg:{}'.format(BMR454TXFIFOData.value))
            tdaubnk.Write(BMR454TXFIFOData)
            BMR454Transmit = BMR454TransmitReg()
            BMR454Transmit.BMR454Transmit = SET_TRANSMIT_BIT
            time.sleep(0.05)
            tdaubnk.Write(BMR454Transmit)
            time.sleep(0.05)
            BMR454Status = tdaubnk.Read(BMR454StatusReg)
            self.Log('info','BMR454StatusReg:{}'.format(BMR454Status.BMR454TXFIFOCountError))
            if (BMR454Status.BMR454TXFIFOCountError == 1):
                self.Log('info','BMR454:TX FIFO Count Error bit is set as expected')
                self.ResetFIFO()
            else:
                self.Log('error','BMR454:TX FIFO Count Error bit is not set')
                BMR454TXFIFOCount = tdaubnk.Read(BMR454TXFIFOCountReg)
                self.Log('info','BMR454: TXFIFOCount:{}'.format(BMR454TXFIFOCount.BMR454TXFIFOCount))
                
    def MiniBMR454TXFIFOCountCheckingTest(self):
        '''Verify BMR454 TX FIFO Count register is incrementing or not'''
        for (slot,tdaubnk) in self.env.instruments.items():
            self.ResetFIFO()
            ExpextedCount = 15
            for i in range(15):
                BMR454TXFIFOData = BMR454TXFIFODataReg()
                BMR454TXFIFOData.BMR454TXFIFOData = RANDOM_DATA_TO_WRITE
                self.Log('debug','BMR454TXFIFODataReg:{}'.format(BMR454TXFIFOData.value))
                for i in range (15):
                    tdaubnk.Write(BMR454TXFIFOData)
                BMR454TXFIFOCount = tdaubnk.Read(BMR454TXFIFOCountReg)
                self.Log('debug','BMR454TXFIFOCount:{}'.format(BMR454TXFIFOCount.BMR454TXFIFOCount))
                if (BMR454TXFIFOCount.BMR454TXFIFOCount == ExpextedCount):
                    self.Log('debug','TX Counter has inremented as expected')
                else:
                    self.Log('error','BMR454:TX Counter failed to increment.Expected Value is :{}. Actual Value is :{}'.format(ExpextedCount,BMR454TXFIFOCount.BMR454TXFIFOCount))    
                ExpextedCount = ExpextedCount + 15
            self.ResetFIFO()
    
    def MiniBMR454AddressNAKCheckingTest(self):
        '''Verify BMR454 address NAK Bit is providing correct results or not'''
        for (slot,tdaubnk) in self.env.instruments.items():
            self.send_address(tdaubnk,BMR454_ADDRESS_PLUS_WRITE)
            BMR454Status = tdaubnk.Read(BMR454StatusReg)
            self.Log('info','BMR454StatusReg status:{}'.format(hex(BMR454Status.value)))
            self.Log('info','BMR454StatusReg BMR454Alert_nState status:{}'.format(BMR454Status.BMR454Alert_nState))
            if (BMR454Status.BMR454I2CAddressNAK == 1):
                self.Log('error','BMR454: Correct address was sent.Address NAK Bit is still set.')
            else:
                self.Log('info','BMR454: Correct address was sent Address NAK bit is cleared as expected.')
            self.send_address(tdaubnk, RANDOM_DATA_TO_WRITE)
            BMR454Status = tdaubnk.Read(BMR454StatusReg)
            self.Log('info','BMR454StatusReg status:{}'.format(hex(BMR454Status.value)))
            self.Log('info','BMR454StatusReg BMR454Alert_nState status:{}'.format(BMR454Status.BMR454Alert_nState))
            if (BMR454Status.BMR454I2CAddressNAK == 1):
                self.Log('info','BMR454: Correct address was not sent.Address NAK Bit is set as expected.')
            else:
                self.Log('error','BMR454: Correct address was not sent but still Address NAK bit is cleared.')

    def send_address(self, tdaubnk,addresstowrite):
        BMR454TXFIFOData = BMR454TXFIFODataReg()
        BMR454TXFIFOData.BMR454TXFIFOData = addresstowrite
        tdaubnk.Write(BMR454TXFIFOData)
        time.sleep(0.05)
        BMR454TXFIFOData = BMR454TXFIFODataReg()
        BMR454TXFIFOData.BMR454TXFIFOData = RANDOM_DATA_TO_WRITE
        tdaubnk.Write(BMR454TXFIFOData)
        BMR454Transmit = BMR454TransmitReg()
        BMR454Transmit.BMR454Transmit = SET_TRANSMIT_BIT
        tdaubnk.Write(BMR454Transmit)
        time.sleep(0.03)

    def ResetFIFO(self):
        for (slot,tdaubnk) in self.env.instruments.items():
            for i in range(2):
                BMR454TXFIFOData = BMR454TXFIFODataReg()
                BMR454TXFIFOData.BMR454TXFIFOData = RANDOM_DATA_TO_WRITE
                tdaubnk.Write(BMR454TXFIFOData)
            BMR454Transmit = BMR454TransmitReg()
            BMR454Transmit.BMR454Transmit = SET_TRANSMIT_BIT
            time.sleep(0.05)
            tdaubnk.Write(BMR454Transmit)
            time.sleep(0.05)

class DDR3(TdaubnkTest):

        
    def DirectedDDR3DMAWriteReadExhaustiveTest(self):
        ''' •	Generate random data which of maximum size of the memory (512MB).
            •	Send the data to the DDR3 with DMA write command.
            •	Read back the data with DMA read command.
            •	Compare the sent and received data content.
            •	Repeat the loop 10 time.
        '''
        for (slot,tdaubnk) in self.env.instruments.items():
            memory_size = 512 << 20
            PassCount = 0
            for i in range(10):
                sentdata = os.urandom(memory_size)
                tdaubnk.DDR3DmaWrite(0,sentdata)
                readback = tdaubnk.DDR3DmaRead(0,memory_size)
                if (sentdata != readback):
                    self.Log('error','DDR3_DMA: Data compare mismatch for run:{}'.format(i))
                else:
                    self.Log('debug','DDR3_DMA: Read Count {} . Expected data received'.format(i))
                    PassCount += 1
            if (PassCount == 10):
                self.Log('info','DDR3_DMA: Sent and received data are same for count {}'.format(PassCount))
    
    def DirectedDDR3SimpleWriteReadExhaustiveTest(self):
        '''•	Generate random data and update it in DDR3SDRAMWriteDataReg.
        •	Initialize DDR3SDRAMAddressReg.(address register)
        •	Initialize control register, by updating RW(write) bit and execute bit.
        •	Keep on poling execute bit.
        •	Once execute bit becomes zero. Write transaction is complete.
        •	Initialize DDR3SDRAMAddressReg.(address register)
        •	Initialize control register, by updating RW(read) bit and execute bit.
        •	Keep on poling execute bit.
        •	Once execute bit becomes zero. Read back the data.   
        '''
        for (slot,tdaubnk) in self.env.instruments.items():
            PassCount = 0
            for address in range (0,200,4):
                data = random.getrandbits(32)
                self.DDRsimplecommunicate(address,data,1,1)
                self.Log('debug','Data sent {}'.format(data))
                DDR3SDRAMControl = tdaubnk.Read(DDR3SDRAMControlReg)                
                for i in range(10):
                    if (DDR3SDRAMControl.DDR3SDRAMExecute != 0):
                        time.sleep(0.05)
                        DDR3SDRAMControl = tdaubnk.Read(DDR3SDRAMControlReg)
                        self.Log('info','Control register Write Pass {}'.format(i))
                    else:
                        break
                self.DDRsimplecommunicate(address,data,0,1)
                DDR3SDRAMControl = tdaubnk.Read(DDR3SDRAMControlReg)
                for i in range(10):
                    if (DDR3SDRAMControl.DDR3SDRAMExecute != 0):
                        time.sleep(0.05)
                        DDR3SDRAMControl = tdaubnk.Read(DDR3SDRAMControlReg)
                        self.Log('info','Control register Read Pass {}'.format(i))
                    else:
                        break                
                DDR3SDRAMReadData = tdaubnk.Read(DDR3SDRAMReadDataReg)
                self.Log('debug','Data Received {}'.format(DDR3SDRAMReadData.DDR3SDRAMReadData))
                if (DDR3SDRAMReadData.DDR3SDRAMReadData != data):
                    self.Log('error','DDR3_Simple Access: Data compare mismatch for run:{}. Expected {} Received {}'.format(address,data,DDR3SDRAMReadData.DDR3SDRAMReadData))
                else:
                    PassCount += 1
            if (PassCount == 50):
                self.Log('info','DDR3_Simple Access: Sent and received data are same for count {}'.format(PassCount))
     
    def DDRsimplecommunicate(self,address,data,RWbit,ExecuteBit):
         for (slot,tdaubnk) in self.env.instruments.items():
            DDR3SDRAMAddress = DDR3SDRAMAddressReg()
            DDR3SDRAMAddress.DDR3SDRAMAddress = address
            tdaubnk.Write(DDR3SDRAMAddress)
            DDR3SDRAMWriteData = DDR3SDRAMWriteDataReg()
            DDR3SDRAMWriteData.DDR3SDRAMWriteData = data
            tdaubnk.Write(DDR3SDRAMWriteData)
            DDR3SDRAMControl = DDR3SDRAMControlReg()
            DDR3SDRAMControl.DDR3SDRAMExecute = ExecuteBit
            DDR3SDRAMControl.DDR3SDRAMRW = RWbit                
            tdaubnk.Write(DDR3SDRAMControl)
            time.sleep(0.05)
                
class TDAUDevices(TdaubnkTest):


    def MiniTDAUDevicesTXFIFOCountErrorBitCheckingTest(self):
        '''Verify if TX FIFO Count Error Bit is reporting expected error or not'''
        for (slot,tdaubnk) in self.env.instruments.items():
            for i in range(10):
                for TDAUDevice in range(7):
                    self.Log('debug','****** Test started for TDAU Device{}********'.format(TDAUDevice))
                    self.ResetTDAUDeviceFIFO(TDAUDevice)
                    TDAUTXFIFOCount = tdaubnk.Read(TDAUTXFIFOCountReg,TDAUDevice)
                    self.Log('debug','TDAU Device{}: TXFIFOCount before test start :{} '.format(TDAUDevice,TDAUTXFIFOCount.TDAUTXFIFOCount))
                    TDAUTXFIFOData = tdaubnk.Read(TDAUTXFIFODataReg,TDAUDevice)
                    TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                    self.Log('debug','TDAUTXFIFODataReg:{}'.format(TDAUTXFIFOData.value))
                    tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                    TDAUTransmit = tdaubnk.Read(TDAUTransmitReg,TDAUDevice)
                    TDAUTransmit.TDAUTransmit = SET_TRANSMIT_BIT
                    tdaubnk.Write(TDAUTransmit,TDAUDevice)
                    time.sleep(0.01)
                    TDAUTXFIFOCount = tdaubnk.Read(TDAUTXFIFOCountReg,TDAUDevice)
                    self.Log('debug','TDAU Device{}: TXFIFOCount after writing data :{} '.format(TDAUDevice,TDAUTXFIFOCount.TDAUTXFIFOCount))
                    TDAUStatus = tdaubnk.Read(TDAUStatusReg,TDAUDevice)
                    self.Log('info','TDAUStatusReg:{}'.format(TDAUStatus.TDAUTXFIFOCountError))
                    if (TDAUStatus.TDAUTXFIFOCountError == 1):
                        self.Log('info','TDAU Device{}:TX FIFO Count Error bit is set as expected'.format(TDAUDevice))
                        self.ResetTDAUDeviceFIFO(TDAUDevice)
                        self.ResetTDAUAddress(TDAUDevice)
                    else:
                        self.Log('error','TDAU Device{}:TX FIFO Count Error bit is not set'.format(TDAUDevice))
                        TDAUTXFIFOCount = tdaubnk.Read(TDAUTXFIFOCountReg,TDAUDevice)
                        self.Log('info','TDAU Device{}: TXFIFOCount:{}'.format(TDAUDevice,TDAUTXFIFOCount.TDAUTXFIFOCount))
                        self.ResetTDAUAddress(TDAUDevice)
                
    def MiniTDAUDevicesTXFIFOCountCheckingTest(self):
        '''Verify TDAUDevice TX FIFO Count register is incrementing or not'''
        for (slot,tdaubnk) in self.env.instruments.items():
            for TDAUDevice in range(7):
                self.ResetTDAUDeviceFIFO(TDAUDevice)
                ExpextedCount = 15
                for i in range(15):
                    TDAUTXFIFOData = tdaubnk.Read(TDAUTXFIFODataReg,TDAUDevice)
                    TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                    self.Log('debug','TDAU Device{}: TXFIFODataReg:{}'.format(TDAUDevice,TDAUTXFIFOData.value))
                    for i in range (15):
                        tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                    TDAUTXFIFOCount = tdaubnk.Read(TDAUTXFIFOCountReg,TDAUDevice)
                    self.Log('info','TDAU Device{}: TXFIFOCount:{}'.format(TDAUDevice,TDAUTXFIFOCount.TDAUTXFIFOCount))
                    if (TDAUTXFIFOCount.TDAUTXFIFOCount == ExpextedCount):
                        self.Log('info','TDAU Device:{} TX Counter has inremented as expected'.format(TDAUDevice))
                    else:
                        self.Log('error','TDAU Device:{} TX Counter failed to increment.Expected Value is :{}. Actual Value is :{}'.format(TDAUDevice,ExpextedCount,TDAUTXFIFOCount.TDAUTXFIFOCount))    
                    ExpextedCount = ExpextedCount + 15
                self.ResetTDAUDeviceFIFO(TDAUDevice)
                self.ResetTDAUAddress(TDAUDevice)
    
    def MiniTDAUDevicesAddressNAKCheckingTest(self):
        '''Verify TAUDevice address NAK Bit is providing correct results or not'''
        for (slot,tdaubnk) in self.env.instruments.items():
            for TDAUDevice in range(7):
                TDAUTXFIFOData = TDAUTXFIFODataReg()
                TDAUTXFIFOData.TDAUTXFIFOData = TDAU_ADDRESS_PLUS_READ
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                time.sleep(0.05)
                TDAUTXFIFOData = TDAUTXFIFODataReg()
                TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                TDAUTransmit = TDAUTransmitReg()
                TDAUTransmit.TDAUTransmit = SET_TRANSMIT_BIT
                tdaubnk.Write(TDAUTransmit,TDAUDevice)
                time.sleep(0.03)
                TDAUStatus = tdaubnk.Read(TDAUStatusReg,TDAUDevice)
                self.Log('debug','TDAUStatusReg status:{}'.format(TDAUStatus.value))
                if (TDAUStatus.TDAUI2CAddressNAK == 1):
                    self.Log('error','TDAU Device{}: Correct address was sent.Address NAK Bit is still set.'.format(TDAUDevice))
                else:
                    self.Log('info','TDAU Device{}: Correct address was sent Address NAK bit is cleared as expected.'.format(TDAUDevice))
                TDAUTXFIFOData = TDAUTXFIFODataReg()
                TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                time.sleep(0.05)
                TDAUTXFIFOData = TDAUTXFIFODataReg()
                TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                TDAUTransmit = TDAUTransmitReg()
                TDAUTransmit.TDAUTransmit = SET_TRANSMIT_BIT
                tdaubnk.Write(TDAUTransmit,TDAUDevice)
                time.sleep(0.03)
                TDAUStatus = tdaubnk.Read(TDAUStatusReg,TDAUDevice)
                self.Log('debug','TDAUStatusReg status:{}'.format(TDAUStatus.value))
                if (TDAUStatus.TDAUI2CAddressNAK == 1):
                    self.Log('info','TDAU Device{}: Correct address was not sent.Address NAK Bit is set as expected.'.format(TDAUDevice))
                else:
                    self.Log('error','TDAU Device{}:Correct address was not sent but still Address NAK bit is cleared.'.format(TDAUDevice))
                TDAUTXFIFOData = TDAUTXFIFODataReg()
                TDAUTXFIFOData.TDAUTXFIFOData = TDAU_ADDRESS_PLUS_READ
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                time.sleep(0.05)
                TDAUTXFIFOData = TDAUTXFIFODataReg()
                TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
                TDAUTransmit = TDAUTransmitReg()
                TDAUTransmit.TDAUTransmit = SET_TRANSMIT_BIT
                tdaubnk.Write(TDAUTransmit,TDAUDevice)
                time.sleep(0.03)

    def DirectedTDAUDeviceTemperatureReadExhaustiveTest(self):
        '''Verify read temperature from TAUDevice in manual mode'''
        for (slot,tdaubnk) in self.env.instruments.items():
            time.sleep(0.05)
            self.TDAUinit()
            for TDAUDevice in range(7):
                #tdau.TDAUWrite(TDAUDevice,STARTCNV,b'')
                time.sleep(0.05)
                TDAU_readback_temperature_test = TDAU_readback_temperature()
                TDAU_readback_temperature_test.bytes = tdaubnk.TDAURead(TDAUDevice,STATREQ,READBACK_LENGTH_8)
                self.Log('info','Complete Readback value {}'.format(TDAU_readback_temperature_test.bytes))
                if ((int(TDAU_readback_temperature_test.STAT1) & SYSTEM_CONVERSION_ERROR) != 0):                                # SYSTEM_CONVERSION_ERROR = 1010 0000 -> Verify if System error or Conversion error occured 
                    readback_Extend_error = tdaubnk.TDAURead(TDAUDevice, READERROR, READBACK_LENGTH_4)
                    self.Log('info','TDAU Channel 1 Readback Status {}'.format(TDAU_readback_temperature_test.STAT1))
                    self.Log('info', 'TDAU Device{}: Readback error {}'.format(TDAUDevice, readback_Extend_error))
                else:
                    mantissa = int(TDAU_readback_temperature_test.STAT1) & 15
                    self.Log('info','TDAU Channel 1 Readback Temperature {}.{}'.format(TDAU_readback_temperature_test.TEMP1,mantissa))
                    if TDAU_readback_temperature_test.TEMP1 not in range(MIN_TEMP,MAX_TEMP):
                        self.Log('info','TDAU Channel 1 Readback Temperature is out of normal operating range(15-38) {}.{}'.format(TDAU_readback_temperature_test.TEMP1,mantissa))
                    
                if ((int(TDAU_readback_temperature_test.STAT2) & SYSTEM_CONVERSION_ERROR) != 0):
                    readback_Extend_error = tdaubnk.TDAURead(TDAUDevice, READERROR, READBACK_LENGTH_4)
                    self.Log('info','TDAU Channel 2 Readback Status {}'.format(TDAU_readback_temperature_test.STAT2))
                    self.Log('info', 'TDAU Device{}: Readback error {}'.format(TDAUDevice, readback_Extend_error))
                else:
                    mantissa = int(TDAU_readback_temperature_test.STAT2) & 15
                    self.Log('info','TDAU Channel 2 Readback Temperature {}.{}'.format(TDAU_readback_temperature_test.TEMP2,mantissa))
                    if TDAU_readback_temperature_test.TEMP2 not in range(MIN_TEMP,MAX_TEMP):
                        self.Log('info','TDAU Channel 1 Readback Temperature is out of normal operating range(15-38) {}.{}'.format(TDAU_readback_temperature_test.TEMP2,mantissa))
                    
                if ((int(TDAU_readback_temperature_test.STAT3) & SYSTEM_CONVERSION_ERROR) != 0):
                    readback_Extend_error = tdaubnk.TDAURead(TDAUDevice, READERROR, READBACK_LENGTH_4)
                    self.Log('info','TDAU Channel 3 Readback Status {}'.format(TDAU_readback_temperature_test.STAT3))
                    self.Log('info', 'TDAU Device{}: Readback error {}'.format(TDAUDevice, readback_Extend_error))
                else:
                    mantissa = int(TDAU_readback_temperature_test.STAT3) & 15
                    self.Log('info','TDAU Channel 3 Readback Temperature {}.{}'.format(TDAU_readback_temperature_test.TEMP3,mantissa))
                    if TDAU_readback_temperature_test.TEMP3 not in range(MIN_TEMP,MAX_TEMP):
                        self.Log('info','TDAU Channel 1 Readback Temperature is out of normal operating range(15-38) {}.{}'.format(TDAU_readback_temperature_test.TEMP3,mantissa))

                if ((int(TDAU_readback_temperature_test.STAT4) & SYSTEM_CONVERSION_ERROR) != 0):
                    readback_Extend_error = tdaubnk.TDAURead(TDAUDevice, READERROR, READBACK_LENGTH_4)
                    self.Log('info','TDAU Channel 4 Readback Status {}'.format(TDAU_readback_temperature_test.STAT4))
                    self.Log('info', 'TDAU Device{}: Readback error {}'.format(TDAUDevice, readback_Extend_error))
                else:
                    mantissa = int(TDAU_readback_temperature_test.STAT4) & 15
                    self.Log('info','TDAU Channel 4 Readback Temperature {}.{}'.format(TDAU_readback_temperature_test.TEMP4,mantissa))
                    if TDAU_readback_temperature_test.TEMP4 not in range(MIN_TEMP,MAX_TEMP):
                        self.Log('info','TDAU Channel 1 Readback Temperature is out of normal operating range(15-38) {}.{}'.format(TDAU_readback_temperature_test.TEMP4,mantissa))

    def DirectedTDAUDeviceP2PTemperatureReadTest(self):
        ''' Test to verify TDAU Bank and RC peer to peer communication interface
            •	Initialize TDAU devices 
            •	Initialize P2P communication by enabling P2P enable bit from control register.
            •	Start reading temperature from RC FPGA 
            •   Repeat above steps multiple times and see if system fails to report temperature.'''
        for (slot,tdaubnk) in self.env.instruments.items():
            rc2 = Rc2(slot)
            self.TDAUinit()
            tdaubnk.tdau_auto(slot,TDAU_AUTO_ENABLE)
            base = tdaubnk.RCPeerToPeerMemoryBase()
            addr = base + 0x300 * slot
            P2PStartAddressLower = P2PStartAddressLowerReg()
            P2PStartAddressLower.P2PStartAddressLower = addr & 0xFFFFFFFF
            tdaubnk.Write(P2PStartAddressLower)
            P2PStartAddressUpper = P2PStartAddressUpperReg()
            P2PStartAddressUpper.P2PStartAddressUpper = (addr >> 32) & 0xFFFFFFFF
            tdaubnk.Write(P2PStartAddressUpper)
            P2PControl = P2PControlReg()
            P2PControl.P2PEnable = 1
            tdaubnk.Write(P2PControl)
            base = 0x4000 + 0x300 * slot
            Channel = 0
            TDAUDeviceCount = 0
            for repeatread in range(25):
                time.sleep(0.02)
                self.Log('info','Temperature reading Count {}'.format(repeatread))
                for addr in range(base,base + 0x300,8):
                        readbacktemp = rc2.BarRead(1,addr)
                        if readbacktemp == P2P_DEFAULT_TEMP_VALUE:
                            self.Log('error', 'TDAU communication is not setup . Devices shows default temperature value :0x{:04X} '.format(readbacktemp))
                        if readbacktemp != 0:
                            FirstTempMantissa, FirstTempRead, SecondTempMantissa, SecondTempRead = self.ExtractPerChannelTemp(
                                readbacktemp)
                            self.Log('info','Address :- {:04X} '.format(addr))
                            self.Log('info','TDAU Device {} Channel {} Temperature reading {:}.{:}'.format(TDAUDeviceCount,Channel,FirstTempRead,FirstTempMantissa))
                            if FirstTempRead not in range(MIN_TEMP, MAX_TEMP):
                                self.Log('error','TDAU Device {} Channel {} temperature is out of range {}'.format(TDAUDeviceCount,Channel,FirstTempRead))
                            Channel = Channel + 1
                            self.Log('info','TDAU Device {} Channel {} Temperature reading {:}.{:}'.format(TDAUDeviceCount,Channel,SecondTempRead,SecondTempMantissa))
                            if SecondTempRead not in range(MIN_TEMP, MAX_TEMP):
                                self.Log('error','TDAU Device {} Channel {} temperature is out of range {}'.format(TDAUDeviceCount,Channel,SecondTempRead))
                            Channel = Channel + 1
                        Channel, TDAUDeviceCount = self.ChannelTDAUCounter(Channel, TDAUDeviceCount)
            tdaubnk.tdau_auto(slot,TDAU_AUTO_DISABLE)

    def ExtractPerChannelTemp(self, readbacktemp):
        FirstTempRead = readbacktemp & 0x000000FF
        FirstTempMantissa = (readbacktemp >> 8) & 0x0000000F
        SecondTempRead = (readbacktemp >> 16) & 0x000000FF
        SecondTempMantissa = (readbacktemp >> 24) & 0x0000000F
        return FirstTempMantissa, FirstTempRead, SecondTempMantissa, SecondTempRead

    def ChannelTDAUCounter(self, Channel, TDAUDeviceCount):
        if Channel == 4:
            Channel = 0
            if TDAUDeviceCount == 6:
                TDAUDeviceCount = 0
            else:
                TDAUDeviceCount = TDAUDeviceCount + 1
        return Channel, TDAUDeviceCount

    def ResetTDAUDeviceFIFO(self,TDAUDevice):
        for (slot,tdaubnk) in self.env.instruments.items():
            for i in range(2):
                TDAUTXFIFOData = tdaubnk.Read(TDAUTXFIFODataReg,TDAUDevice)
                TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
                tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
            TDAUTransmit = tdaubnk.Read(TDAUTransmitReg,TDAUDevice)
            TDAUTransmit.TDAUTransmit = RESET_TRANSMIT_BIT_TDAU
            TDAUTXFIFOCount = tdaubnk.Read(TDAUTXFIFOCountReg,TDAUDevice)
            self.Log('info','TDAU Device{}: TXFIFOCount before reset:{}'.format(TDAUDevice,TDAUTXFIFOCount.TDAUTXFIFOCount))
            tdaubnk.Write(TDAUTransmit,TDAUDevice)
            time.sleep(0.05)
            TDAUTXFIFOCount = tdaubnk.Read(TDAUTXFIFOCountReg,TDAUDevice)
            self.Log('info','TDAU Device{}: TXFIFOCount after reset:{}'.format(TDAUDevice,TDAUTXFIFOCount.TDAUTXFIFOCount))
            #if TDAUTXFIFOCount.TDAUTXFIFOCount != 0:
            #    self.Log('error','TDAU TX FIFO was not cleared')
    
    def ResetTDAUAddress(self,TDAUDevice):    
        for (slot,tdaubnk) in self.env.instruments.items():
            TDAUTXFIFOData = TDAUTXFIFODataReg()
            TDAUTXFIFOData.TDAUTXFIFOData = TDAU_ADDRESS_PLUS_READ
            tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
            time.sleep(0.05)
            TDAUTXFIFOData = TDAUTXFIFODataReg()
            TDAUTXFIFOData.TDAUTXFIFOData = RANDOM_DATA_TO_WRITE
            tdaubnk.Write(TDAUTXFIFOData,TDAUDevice)
            TDAUTransmit = TDAUTransmitReg()
            TDAUTransmit.TDAUTransmit = SET_TRANSMIT_BIT
            tdaubnk.Write(TDAUTransmit,TDAUDevice)
            time.sleep(0.03)
            
    def TDAUinit(self):
        for (slot,tdaubnk) in self.env.instruments.items():
            with open(TDAUBJT_Conf_Load,'rb') as f:
                data = f.read()
            for tdauDevice in range(7):
                TDAUStatus = tdaubnk.Read(TDAUStatusReg,tdauDevice)
                self.Log('debug','TDAUStatusReg status:{} device {}'.format(TDAUStatus.value,tdauDevice))
                tdaubnk.TDAUWrite(tdauDevice,STOPCNV,b'')
            for tdauDevice in range(7):
                for addr in range(0,len(data),128):
                    tdaubnk.TDAUWriteMem(tdauDevice,addr,data[addr:addr+128])
            for tdauDevice in range(7):
                tdaubnk.TDAUWrite(tdauDevice,EXTCAL,b'')
            time.sleep(.9)
            for tdauDevice in range(7):
                tdaubnk.TDAUWrite(tdauDevice,SCOCALIB,b'')
            time.sleep(.9)
				
class ADT7411Devices(TdaubnkTest):

        
    def DirectedADT7411DeviceVoltageReadTest(self):
        ''' Test to verify ADT7411 interface
        
            •	Initialize ADT7411 devices 
            •	Start reading data from ADT7411
            •   Repeat above steps multiple times and see if system fails to report data.'''
        for (slot,tdaubnk) in self.env.instruments.items():
            tdaubnk.ADT7411Init()
            deviceid = tdaubnk.ADT7411Read(ADT7144_Device_ID_ADDR)
            if deviceid != 2:
                self.Log('error', 'ADT7411 Device ID is wrong.Please confirm Initialization is Correct. Expected Value 0x02 Actual Value :{} '.format(deviceid))
            else:
                self.Log('info','ADT7411 Device ID :{} '.format(deviceid))
            manufacturerid = tdaubnk.ADT7411Read(ADT7144_MANUFACTURER_ID_ADDR)
            if manufacturerid != 65:
                self.Log('error', 'ADT7411 manufacturerid ID is wrong,Please confirm Initialization is Correct. Expected Value 0x41 Actual Value :{} '.format(hex(manufacturerid)))
            else:
                self.Log('info','ADT7411 MANUFACTURER ID :{} '.format(manufacturerid))
            for repeatread in range(100):
                time.sleep(0.02)
                self.Log('info','ADT7411 reading count {}'.format(repeatread))
                
                for channleread in range(1,7):
                    if channleread == 5 :
                        continue
                    ReadBackValue = tdaubnk.ADT7411ChannelRead(channleread)
                    if channleread == 1:
                        if self.IsInTheRange(ADT7144_AIN1_LOW_LIMIT_DATA, ADT7144_AIN1_HIGH_LIMIT_DATA, ReadBackValue):
                            self.Log('info','AIN {} Voltage Read {}V .Expected Voltage Range (3.135 to 3.465)'.format(channleread,round(ReadBackValue,3)))
                        else:
                            self.Log('error','AIN {} Voltage Read {}V .Voltage out of range.Expected Voltage Range (3.135 to 3.465)'.format(channleread,ReadBackValue))
                    if channleread == 2:
                        if self.IsInTheRange(ADT7144_AIN2_LOW_LIMIT_DATA, ADT7144_AIN2_HIGH_LIMIT_DATA, ReadBackValue):
                            self.Log('info','AIN {} Voltage Read {}V .Expected Voltage Range (1.425 to 1.575)'.format(channleread,round(ReadBackValue,3)))
                        else:
                            self.Log('error','AIN {} Voltage Read {}V .Voltage out of range.Expected Voltage Range (1.425 to 1.575) '.format(channleread,ReadBackValue))
                    if channleread == 3:
                        if self.IsInTheRange(ADT7144_AIN3_LOW_LIMIT_DATA, ADT7144_AIN3_HIGH_LIMIT_DATA, ReadBackValue):
                            self.Log('info', 'AIN {} Voltage Read {}V .Expected Voltage Range (1.14 to 1.26) '.format(channleread, round(ReadBackValue,3)))
                        else:
                            self.Log('error','AIN {} Voltage Read {}V .Voltage out of range. Expected Voltage Range (1.14 to 1.26)'.format(channleread, ReadBackValue))
                    if channleread == 4:
                        if self.IsInTheRange(ADT7144_AIN4_LOW_LIMIT_DATA, ADT7144_AIN4_HIGH_LIMIT_DATA, ReadBackValue):
                            self.Log('info', 'AIN {} Voltage Read {}V .Expected Voltage 1.8V '.format(channleread, round(ReadBackValue,3)))
                        else:
                            self.Log('error','AIN {} Voltage Read {}V .Voltage out of range.Expected Voltage 1.8V.'.format(channleread, ReadBackValue))
                    if channleread == 6:
                        if self.IsInTheRange(ADT7144_AIN6_LOW_LIMIT_DATA, ADT7144_AIN6_HIGH_LIMIT_DATA, ReadBackValue):
                            self.Log('info', 'AIN {} Voltage Read {}V .Expected Voltage Range (11.4 to 12.6)'.format(channleread,round(ReadBackValue,3)))
                        else:
                            self.Log('error','AIN {} Voltage Read {}V .Voltage out of range.Expected Voltage Range (11.4 to 12.6) '.format(channleread, ReadBackValue))

    def IsInTheRange(self, LowRange, HighRange, ReadBackValue):
        StepIncrease = 0.01
        startstep = LowRange
        while startstep < HighRange:
            ConvertToDecimal1 = Decimal(ReadBackValue)
            ReadBackValuePre3 = round(ConvertToDecimal1,2)
            ConvertToDecimal2 = Decimal(startstep)
            startstepPre3 = round(ConvertToDecimal2,2)
            if (ReadBackValuePre3 == startstepPre3):
                return True
            startstep += StepIncrease


# class S2HCommunication(TdaubnkTest):

    # def DirectedS2HPacketCountControlByRegisterTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         last_packet_idx, max_packets_in_site_controller = tdaubnk.S2HRegisterAndBufferSetup(slot, tdaubnk)
    #         s2h_range_evaluate = ADT7411Devices()
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         tdaubnk.ResetStreamingWithControlReg()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         tdaubnk.TurnONStreamingWithControlReg()
    #         start_timer = time.perf_counter()
    #         total_packets = 0
    #         while time.perf_counter() - start_timer < packet_read_time:
    #             total_packets += self.GetCount(last_packet_idx, max_packets_in_site_controller, tdaubnk)
    #             self.WaitTime(3)
    #         total_packets += self.GetCount(last_packet_idx, max_packets_in_site_controller, tdaubnk)
    #         end_timer = time.perf_counter()
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         if s2h_range_evaluate.IsInTheRange(min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds, total_packets):
    #             self.Log('info',
    #                      'Total packets when S2H control register used for S2H ON-OFF = {} Expected is between {} - {}'.format(total_packets, min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds))
    #         else:
    #             self.Log('error',
    #                      'Total packet count out of range when S2H control register used for S2H ON-OFF.Actual: {} Expected is between {} - {} '.format(total_packets,
    #                                                                                                                                                     min_S2H_packets_in_120_seconds,
    #                                                                                                                                                     max_S2H_packets_in_120_seconds))
    #         self.Log('info', 'Total Time = {} '.format(end_timer - start_timer))
    #         if s2h_range_evaluate.IsInTheRange(min_S2H_packets_per_sec, max_S2H_packets_per_sec, (total_packets/(end_timer-start_timer))):
    #             self.Log('info', 'Packets per Second = {} '.format(total_packets / (end_timer - start_timer)))
    #         else:
    #             self.Log('error', 'Packets per Second count out of range.Actual:{} Expected is :{}'.format(total_packets / (end_timer - start_timer), min_S2H_packets_per_sec))

    # def DirectedS2HPacketCountControlByTriggerTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         last_packet_index, max_packets_in_site_controller = tdaubnk.S2HRegisterAndBufferSetup(slot, tdaubnk)
    #         s2h_range_evaluate = ADT7411Devices()
    #         tdaubnk.TurnOFFStreamingWithTrigger()
    #         tdaubnk.ResetStreamingWithTrigger()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         tdaubnk.TurnONStreamingWithTrigger()
    #         start_timer = time.perf_counter()
    #         total_packets_received = 0
    #         while time.perf_counter() - start_timer < packet_read_time:
    #             total_packets_received += self.GetCount(last_packet_index, max_packets_in_site_controller, tdaubnk)
    #             self.WaitTime(3)
    #         total_packets_received += self.GetCount(last_packet_index, max_packets_in_site_controller, tdaubnk)
    #         end_timer = time.perf_counter()
    #         tdaubnk.TurnOFFStreamingWithTrigger()
    #         if s2h_range_evaluate.IsInTheRange(min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds, total_packets_received):
    #             self.Log('info',
    #                      'Total packets when Trigger register used for S2H ON-OFF = {} Expected is between {} - {}'.format(
    #                          total_packets_received, min_S2H_packets_in_120_seconds, max_S2H_packets_in_120_seconds))
    #         else:
    #             self.Log('error',
    #                      'Total packet count out of range when Trigger register used for S2H ON-OFF.Actual: {} Expected is between {} - {} '.format(total_packets_received,
    #                                                                                                                                                 min_S2H_packets_in_120_seconds,
    #                                                                                                                                                 max_S2H_packets_in_120_seconds))
    #         self.Log('info', 'Total Time = {} '.format(end_timer - start_timer))
    #         if s2h_range_evaluate.IsInTheRange(min_S2H_packets_per_sec, max_S2H_packets_per_sec, (total_packets_received / (end_timer - start_timer))):
    #             self.Log('info', 'Packets per Second = {} '.format(total_packets_received / (end_timer - start_timer)))
    #         else:
    #             self.Log('error', 'Packets per Second count out of range.Actual:{} Expected is :{}'.format(
    #                 total_packets_received / (end_timer - start_timer), min_S2H_packets_per_sec))

    # def DirectedS2HTriggerONControlregOFFTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         tdaubnk.StreamingBufferaddrsize()
    #         tdaubnk.TurnOFFStreamingWithTrigger()
    #         tdaubnk.ResetStreamingWithTrigger()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         tdaubnk.TurnONStreamingWithTrigger()
    #         self.WaitTime(3)
    #         total_packets_when_on = tdaubnk.Read(S2HValidPacketIndexReg)
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         self.WaitTime(1)
    #         total_packets_when_off = tdaubnk.Read(S2HValidPacketIndexReg)
    #         self.Log('info', 'Control Register Turn OFF .Actual {} expected {} '.format(total_packets_when_off.value, total_packets_when_on.value))
    #         if total_packets_when_on.value != total_packets_when_off.value:
    #             self.Log('error', 'Turn OFF not successful.Actual {} expected {} '.format(total_packets_when_off.value, total_packets_when_on.value))
    #         tdaubnk.ResetStreamingWithControlReg()
    #         total_packets_when_off = tdaubnk.Read(S2HValidPacketIndexReg)
    #         self.Log('info', 'Reset .Actual {} Expected {}'.format(hex(total_packets_when_off.value), hex(S2H_packet_after_reset)))
    #         if total_packets_when_off.value != S2H_packet_after_reset:
    #                 self.Log('error', 'Reset was not successful.Actual {} Expected {}'.format(hex(total_packets_when_off.value), hex(S2H_packet_after_reset)))

    # def DirectedS2HControlregONTriggerOFFTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         tdaubnk.StreamingBufferaddrsize()
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         tdaubnk.ResetStreamingWithControlReg()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         tdaubnk.TurnONStreamingWithControlReg()
    #         self.WaitTime(3)
    #         total_packets_when_on = tdaubnk.Read(S2HValidPacketIndexReg)
    #         tdaubnk.TurnOFFStreamingWithTrigger()
    #         self.WaitTime(1)
    #         total_packets_when_off = tdaubnk.Read(S2HValidPacketIndexReg)
    #         self.Log('info', 'Trigger Turn OFF .Actual {} expected {} '.format(total_packets_when_off.value, total_packets_when_on.value))
    #         if total_packets_when_on.value != total_packets_when_off.value:
    #             self.Log('error', 'Turn OFF not successful.Actual {} expected {} '.format(total_packets_when_off.value, total_packets_when_on.value))
    #         tdaubnk.ResetStreamingWithControlReg()
    #         total_packets_when_off = tdaubnk.Read(S2HValidPacketIndexReg)
    #         self.Log('info', 'Reset .Actual {} Expected {}'.format(hex(total_packets_when_off.value), hex(S2H_packet_after_reset)))
    #         if total_packets_when_off.value != S2H_packet_after_reset:
    #                 self.Log('error', 'Reset was not successful.Actual {} Expected {}'.format(hex(total_packets_when_off.value), hex(S2H_packet_after_reset)))

    # def DirectedS2HPacketCountIncrementTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         tdaubnk.StreamingBufferaddrsize()
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         tdaubnk.ResetStreamingWithControlReg()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         previous_total_packets_when_OFF = 0
    #         for seconds_count in range(1, 25, 1):
    #             tdaubnk.TurnONStreamingWithControlReg()
    #             self.WaitTime(1)
    #             tdaubnk.TurnOFFStreamingWithTrigger()
    #             total_packets_when_OFF = tdaubnk.Read(S2HValidPacketIndexReg)
    #             self.Log('info', 'Counter value after {} second: {} '.format(seconds_count, total_packets_when_OFF.value))
    #             if (seconds_count % seconds_to_reach_max_count) == 0:
    #                 if total_packets_when_OFF.value > previous_total_packets_when_OFF:
    #                     self.Log('error', 'Packet Counter failed to Reset.Actual:{} Expected close to Zero'.format(
    #                         total_packets_when_OFF.value))
    #                 tdaubnk.ResetStreamingWithControlReg()
    #                 previous_total_packets_when_OFF = 0
    #             else:
    #                 if total_packets_when_OFF.value < previous_total_packets_when_OFF:
    #                     self.Log('error', 'Packet Counter failed to Increment.Actual:{} Expected more than: {}'.format(
    #                         total_packets_when_OFF.value, previous_total_packets_when_OFF))
    #                 previous_total_packets_when_OFF = total_packets_when_OFF.value

    # def DirectedS2HPacketCountResetInPacketHeaderTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         max_packets_in_site_controller, pkt_byte_count, buffer_size, addr = tdaubnk.ReadStreamingDetails(tdaubnk)
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         tdaubnk.ResetStreamingWithControlReg()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         for repeat_count in range(20):
    #             self.OnOffResetLoop(addr, buffer_size, tdaubnk)
    #             tdaubnk.ResetStreamingWithControlReg()

    # def DirectedS2HPacketCountRolloverInPacketHeaderTest(self):
    #     for (slot, tdaubnk) in self.env.instruments.items():
    #         max_packets_in_site_controller, pkt_byte_count, buffer_size, addr = tdaubnk.ReadStreamingDetails(tdaubnk)
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         tdaubnk.ResetStreamingWithControlReg()
    #         if not tdaubnk.IsS2HConfigurationDone():
    #             self.Log('error', 'S2H Configuration is not done')
    #         tdaubnk.TurnONStreamingWithControlReg()
    #         self.WaitTime(255)
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         packet_index_before_rollover = tdaubnk.Read(S2HValidPacketIndexReg)
    #         packet_number_from_packet_header_before_rollover = tdaubnk.ReadPacketNumber(packet_index_before_rollover.value,
    #                                                                                  addr, buffer_size)
    #         self.Log('info','Packet Count in Packet Header before Rollover: {}.'.format(packet_number_from_packet_header_before_rollover))
    #         tdaubnk.TurnONStreamingWithControlReg()
    #         self.WaitTime(20)
    #         tdaubnk.TurnOFFStreamingWithControlReg()
    #         packet_index_after_rollover = tdaubnk.Read(S2HValidPacketIndexReg)
    #         packet_number_from_packet_header_after_rollover = tdaubnk.ReadPacketNumber(packet_index_after_rollover.value,
    #                                                                                  addr, buffer_size)
    #         self.Log('info','Packet Count in Packet Header after Rollover: {}. '.format(packet_number_from_packet_header_after_rollover))
    #         if packet_number_from_packet_header_after_rollover > expected_count_after_rollover:
    #             self.Log('error', 'Packet Count failed to rollover.Expected close to: {} . Actual {}'.format(expected_count_after_rollover,packet_number_from_packet_header_after_rollover))

    #
    # def OnOffResetLoop(self, addr, buffer_size, tdaubnk):
    #     tdaubnk.TurnONStreamingWithControlReg()
    #     self.WaitTime(2)
    #     tdaubnk.TurnOFFStreamingWithControlReg()
    #     packet_index_before_reset = tdaubnk.Read(S2HValidPacketIndexReg)
    #     packet_number_from_packet_header_before_reset = tdaubnk.ReadPacketNumber(packet_index_before_reset.value, addr, buffer_size)
    #     self.Log('info','Packet Index Register: {} Packet Count Before Reset: {}. '.format(packet_index_before_reset.value,packet_number_from_packet_header_before_reset))
    #     tdaubnk.ResetStreamingWithTrigger()
    #     tdaubnk.TurnONStreamingWithControlReg()
    #     self.WaitTime(1)
    #     tdaubnk.TurnOFFStreamingWithControlReg()
    #     packet_index_after_reset = tdaubnk.Read(S2HValidPacketIndexReg)
    #     packet_number_from_packet_header_after_reset = tdaubnk.ReadPacketNumber(packet_index_after_reset.value, addr, buffer_size)
    #     self.Log('info','Packet Index Register: {} Packet Count after Reset: {}. '.format(packet_index_after_reset.value,packet_number_from_packet_header_after_reset))
    #     if packet_index_after_reset.value <= packet_index_before_reset.value:
    #         self.Log('error', 'Packet index register failed to increment')
    #     if packet_number_from_packet_header_after_reset > packet_number_from_packet_header_before_reset:
    #         self.Log('error','Packet number in packet header failed to reset. Actual: {} Expected less than :{}'.format(packet_number_from_packet_header_after_reset,packet_number_from_packet_header_before_reset))
    #
    # def GetCount(self, last_packet_index, max_packets_in_site_controller, tdaubnk):
    #     if not tdaubnk.IsStreamingOn():
    #         self.Log('warning', 'Streaming is not ON ')
    #     packet_index = tdaubnk.Read(S2HValidPacketIndexReg)
    #     if packet_index.value == no_valid_packet_data:
    #         return 0
    #     elif packet_index.value == last_packet_index.value:
    #         return 0
    #     elif packet_index.value > last_packet_index.value:
    #         number_of_packet = packet_index.value - last_packet_index.value
    #         self.Log('debug', 'Number of Packets: {} '.format(number_of_packet))
    #     else:
    #         number_of_packet = packet_index.value + max_packets_in_site_controller - last_packet_index.value
    #         self.Log('debug', 'Number of Packets: {} '.format(number_of_packet))
    #     last_packet_index.value = packet_index.value
    #     return number_of_packet
    #
    # def WaitTime(self, time_to_wait):
    #     start_time = time.perf_counter()
    #     while time.perf_counter() - start_time < time_to_wait:
    #         pass


class PECICommunication(TdaubnkTest):

    def RandomBitRateFailPercentagePECITest(self):
        for (slot, tdaubnk) in self.env.instruments.items():
            diags_reference_voltage = tdaubnk.GetDiagsReferenceVoltage()
            self.Log('info', 'Diags Reference Voltage: {} '.format(diags_reference_voltage))
            self.PECIErrorRateCalculate(tdaubnk, min_bit_rate, max_bit_rate)
            self.PECIErrorRateCalculate(tdaubnk, med_bit_rate, max_bit_rate)

    def PECIErrorRateCalculate(self, tdaubnk, bit_rate_start, bit_rate_end):
        grouped_dictionary_for_lower_bit_rate = defaultdict(int)
        grouped_dictionary_for_higher_bit_rate = defaultdict(int)
        for test_ran in range(random_bit_rate_20_times):
            bit_rate = random.randint(bit_rate_start,bit_rate_end)
            self.Log('debug', 'bit_rate Selected: {}'.format(bit_rate))
            for PECI_devices in range(PECI_device_count):
                tdaubnk.PECIConfig(PECI_devices, True, address_of_PECI_channel, bit_rate)
            time.sleep(.1)
            for PECI_devices in range(PECI_device_count):
                for each_bit_rate in range(read_100_times):
                    time.sleep(.002)
                    try:
                        tdaubnk.PECITemperatureRead(PECI_devices)
                    except RuntimeError:
                        if bit_rate < bit_rate_50k:
                            grouped_dictionary_for_lower_bit_rate[PECI_devices, bit_rate, 'Fail Count'] += 1
                        if bit_rate > bit_rate_50k:
                            grouped_dictionary_for_higher_bit_rate[PECI_devices, bit_rate, 'Fail Count'] += 1
                    else:
                        pass
        for PECI_device_and_channel, fail_rate in sorted(grouped_dictionary_for_lower_bit_rate.items()):
            self.verify_fail_rate_more_than_fifty_percent(PECI_device_and_channel, fail_rate)
        for PECI_device_and_channel, fail_rate in sorted(grouped_dictionary_for_higher_bit_rate.items()):
            self.verify_fail_rate_more_than_five_percent(PECI_device_and_channel, fail_rate)

    def verify_fail_rate_more_than_five_percent(self, PECI_device_and_channel, fail_rate):
        if fail_rate > error_rate_5_percent:
            self.Log('error',
                     'Fail % {} for Higher bit rate :- {} - {}'.format(fail_rate, PECI_device_and_channel,
                                                                       fail_rate))
        self.Log('info',
                 'Fail % {} at Higher Bit rate{} Fail Count: {}'.format(fail_rate, PECI_device_and_channel,
                                                                        fail_rate))

    def verify_fail_rate_more_than_fifty_percent(self, PECI_device_and_channel, fail_rate):
        if fail_rate > error_rate_50_percent:
            self.Log('warning',
                     'Fail % {} for Lower bit rate :- {}  {}'.format(fail_rate, PECI_device_and_channel,
                                                                     fail_rate))
        self.Log('info',
                 'Fail % {} at Lower bit rate :- {} Fail Count: {}'.format(fail_rate, PECI_device_and_channel,
                                                                           fail_rate))