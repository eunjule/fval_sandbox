################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: env.py
#-------------------------------------------------------------------------------
#     Purpose: TDAU Testbench Environment
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 02/07/17
#       Group: HDMT FPGA Validation
################################################################################


from Common import fval

from Common.instruments.tester import get_tester

class Env(fval.Env):

    # This method is called once per test fixture
    def __init__(self, test):
        self.duts = []
        super().__init__(test)
        self.Log('debug', 'Creating new TDAU Bank test environment instance')
        self.duts = self.get_duts()

    @property
    def instruments(self):
        i = {x.slot: x for x in self.tester().get_undertest_instruments('Tdaubnk')}
        if not any(tdaubnk.name() == 'Tdaubnk' for slot, tdaubnk in i.items()):
            self.Log('error', 'No tdaubnk cards on tester')
        return i

    @staticmethod
    def tester():
        return get_tester()

    def get_duts(self):
        duts = []
        for (slot, instrument) in self.instruments.items():
            duts.append(instrument)
        return duts

class BaseTest(fval.TestCase):
    dutName = 'Tdaubnk'

