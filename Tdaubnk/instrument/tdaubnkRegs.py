################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: tdaubnkRegs.py
#-------------------------------------------------------------------------------
#     Purpose: tdaubnk FPGA Registers declared C_Type
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 05/02/17
#       Group: HDMT FPGA Validation
################################################################################

import ctypes

class Register(ctypes.LittleEndianStructure):
    """Register base class - defines setter/getter for integer value of register"""
    BAR = 0
    BASEADDR = 0
    BASEMUL = 80
    REGCOUNT = 1

    def init(self, value=None):
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')
    
    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i.to_bytes(4, byteorder='little'), 4)
  
    @property
    def bytes(self):
        """getter - returns integer value of register"""
        return bytes(self)
    
    @bytes.setter
    def bytes(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i ,ctypes.sizeof(self))
    
    @classmethod
    def address(cls, index=0):
        return cls.BASEADDR + index * cls.BASEMUL

class FPGARevisionReg(Register):
    BAR = 0
    BASEADDR = 0x00008000
    _fields_ = [('MinorRevisionNumber', ctypes.c_uint, 16),
                ('MajorRevisionNumber', ctypes.c_uint, 16)]
     
class FPGAResetsReg(Register):
    BAR = 0
    BASEADDR = 0x00008010    
    _fields_ = [('ResetTriggerDomain', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 15),
                ('ResetTriggerTransceiver', ctypes.c_uint, 1),
                ('ResetTriggerCore', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 14)]
    
class FPGAIdentificationReg(Register):
    BAR = 0
    BASEADDR = 0x00008040
    _fields_ = [('SlotID', ctypes.c_uint, 4),
                ('Reserved', ctypes.c_uint, 28)]
    
class GpioReg(Register):
    BAR = 0
    BASEADDR = 0x00008050
    _fields_ = [('TriggerReferenceClockSelect', ctypes.c_uint, 1),   # 1=Aurora off-board master sequence clock, 0=On Board Oscillator           
                ('Reserved', ctypes.c_uint, 30),
                ('DDR3SDRAMCalibrationComplete', ctypes.c_uint, 1)]
       
class TriggerErrorReg(Register):
    BAR = 0
    BASEADDR = 0x00008100
    _fields_ = [('TriggerSerialTransceiverNotLocked', ctypes.c_uint, 1),
                ('TriggerSerialPLLNotLocked', ctypes.c_uint, 1),
                ('TriggerLaneDown', ctypes.c_uint, 1),
                ('TriggerChannelDown', ctypes.c_uint, 1),
                ('TriggerSoftError', ctypes.c_uint, 1),
                ('TriggerHardError', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 26)]
        
class LastReceivedTriggerReg(Register):
    BAR = 0
    BASEADDR = 0x00008110
    _fields_ = [('Payload', ctypes.c_uint, 12),
                ('Reserved', ctypes.c_uint, 8),
                ('DUTID', ctypes.c_uint, 6),
                ('CardType', ctypes.c_uint, 6)]
    
class SendTriggerReg(Register):
    BAR = 0
    BASEADDR = 0x00008120
    _fields_ = [('Payload', ctypes.c_uint, 12),
                ('Reserved', ctypes.c_uint, 8),
                ('DUTID', ctypes.c_uint, 6),
                ('CardType', ctypes.c_uint, 6)]

class CapabilityReg(Register):
    BAR = 0
    BASEADDR = 0x00008500
    _fields_ = [('value', ctypes.c_uint, 32)]

class TriggerLoopbackCountReg(Register):
    BAR = 0
    BASEADDR = 0x00008510
    _fields_ = [('Count', ctypes.c_uint, 32)]
    
class BMR454StatusReg(Register):
    BAR = 1
    BASEADDR = 0x00000E00
    _fields_ = [('BMR454I2CAddressNAK', ctypes.c_uint, 1),
                ('BMR454I2CDataNAK', ctypes.c_uint, 1),
                ('BMR454TXFIFOCountError', ctypes.c_uint, 1),
                ('BMR454I2CBusyError', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 20),
                ('BMR454PG_nState', ctypes.c_uint, 1),
                ('BMR454Alert_nState', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 5),
                ('BMR454Busy', ctypes.c_uint, 1)]
    
class BMR454TransmitReg(Register):
    BAR = 1
    BASEADDR = 0x00000E04
    _fields_ = [('BMR454Transmit', ctypes.c_uint, 32)]
    
class BMR454TXFIFOCountReg(Register):
    BAR = 1
    BASEADDR = 0x00000E08
    _fields_ = [('BMR454TXFIFOCount', ctypes.c_uint, 11),
                ('Reserved', ctypes.c_uint, 21)]

class BMR454RXFIFOCountReg(Register):
    BAR = 1
    BASEADDR = 0x00000E0C
    _fields_ = [('BMR454RXFIFOCount', ctypes.c_uint, 11),
                ('Reserved', ctypes.c_uint, 21)]
    
class BMR454TXFIFODataReg(Register):
    BAR = 1
    BASEADDR = 0x00000E10
    _fields_ = [('BMR454TXFIFOData', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)]
    
class BMR454RXFIFODataReg(Register):
    BAR = 1
    BASEADDR = 0x00000E14
    _fields_ = [('BMR454RXFIFOData', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)]

class DDR3SDRAMControlReg(Register):
    BAR = 0
    BASEADDR = 0x00008200
    _fields_ = [('DDR3SDRAMExecute', ctypes.c_uint, 1),
                ('DDR3SDRAMRW', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 30)]

class DDR3SDRAMAddressReg(Register):
    BAR = 0
    BASEADDR = 0x00008210
    _fields_ = [('DDR3SDRAMAddress', ctypes.c_uint, 27),
                ('Reserved', ctypes.c_uint, 5)]

class DDR3SDRAMWriteDataReg(Register):
    BAR = 0
    BASEADDR = 0x00008220
    _fields_ = [('DDR3SDRAMWriteData', ctypes.c_uint, 32)]

class DDR3SDRAMReadDataReg(Register):
    BAR = 0
    BASEADDR = 0x00008230
    _fields_ = [('DDR3SDRAMReadData', ctypes.c_uint, 32)]
    
class TDAUResetsReg(Register):
    BAR = 1
    BASEADDR = 0x00000900
    BASEMUL = 128
    _fields_ = [('TDAUResetErrors', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]   
    
class TDAUControlReg(Register):
    BAR = 1
    BASEADDR = 0x00000904
    BASEMUL = 128
    _fields_ = [('TDAUAddress', ctypes.c_uint, 7),
                ('Reserved', ctypes.c_uint, 9),
                ('TDAUChannel1Enable', ctypes.c_uint, 1),
                ('TDAUChannel2Enable', ctypes.c_uint, 1),
                ('TDAUChannel3Enable', ctypes.c_uint, 1),
                ('TDAUChannel4Enable', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 10),
                ('TDAUControlSource', ctypes.c_uint, 1),
                ('TDAUTimeoutInterruptEnable', ctypes.c_uint, 1)]
    
class TDAUStatusReg(Register):
    BAR = 1
    BASEADDR = 0x00000908
    BASEMUL = 128
    _fields_ = [('TDAUI2CAddressNAK', ctypes.c_uint, 1),
                ('TDAUI2CDataNAK', ctypes.c_uint, 1),
                ('TDAUTXFIFOCountError', ctypes.c_uint, 1),
                ('TDAUI2CBusyError', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 4),
                ('TDAUChannel1TimeoutError', ctypes.c_uint, 1),
                ('TDAUChannel2TimeoutError', ctypes.c_uint, 1),
                ('TDAUChannel3TimeoutError', ctypes.c_uint, 1),
                ('TDAUChannel4TimeoutError', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 19),
                ('TDAUBusy', ctypes.c_uint, 1)]
    
class TDAUTimeoutLimitReg(Register):
    BAR = 1
    BASEADDR = 0x0000090C
    BASEMUL = 128
    _fields_ = [('TDAUTimeoutLimit', ctypes.c_uint, 12),
                ('Reserved', ctypes.c_uint, 20)] 
    
class TDAUTransmitReg(Register):
    BAR = 1
    BASEADDR = 0x00000910
    BASEMUL = 128
    _fields_ = [('TDAUTransmit', ctypes.c_uint, 32)]

class TDAUTXFIFOCountReg(Register):
    BAR = 1
    BASEADDR = 0x00000914
    BASEMUL = 128
    _fields_ = [('TDAUTXFIFOCount', ctypes.c_uint, 11),
                ('Reserved', ctypes.c_uint, 21)] 

class TDAURXFIFOCountReg(Register):
    BAR = 1
    BASEADDR = 0x00000918
    BASEMUL = 128
    _fields_ = [('TDAURXFIFOCount', ctypes.c_uint, 11),
                ('Reserved', ctypes.c_uint, 21)]
    
class TDAUTXFIFODataReg(Register):
    BAR = 1
    BASEADDR = 0x0000091C
    BASEMUL = 128
    _fields_ = [('TDAUTXFIFOData', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)] 
    
class TDAURXFIFODataReg(Register):
    BAR = 1
    BASEADDR = 0x00000920
    BASEMUL = 128
    _fields_ = [('TDAURXFIFOData', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)]
    
class TDAULowerChannelsDataReg(Register):
    BAR = 1
    BASEADDR = 0x00000924
    BASEMUL = 128
    _fields_ = [('TDAUChannel1Temperature', ctypes.c_uint, 8),
                ('TDAUChannel1Status', ctypes.c_uint, 8),
                ('TDAUChannel2Temperature', ctypes.c_uint, 8),
                ('TDAUChannel2Status', ctypes.c_uint, 8)]

class TDAUUpperChannelsDataReg(Register):
    BAR = 1
    BASEADDR = 0x00000928
    BASEMUL = 128
    _fields_ = [('TDAUChannel3Temperature', ctypes.c_uint, 8),
                ('TDAUChannel3Status', ctypes.c_uint, 8),
                ('TDAUChannel4Temperature', ctypes.c_uint, 8),
                ('TDAUChannel4Status', ctypes.c_uint, 8)]
    
class TDAUReadErrorReg(Register):
    BAR = 1
    BASEADDR = 0x0000092C
    BASEMUL = 128
    _fields_ = [('TDAUErrorByte1', ctypes.c_uint, 8),
                ('TDAUErrorByte2', ctypes.c_uint, 8),
                ('TDAUErrorByte3', ctypes.c_uint, 8),
                ('TDAUErrorByte4', ctypes.c_uint, 8)]
    
class TDAUTemperatureOffsetReg(Register):
    BAR = 1
    BASEADDR = 0x00000930
    BASEMUL = 128
    _fields_ = [('TDAUChannel1TemperatureOffset', ctypes.c_uint, 8),
                ('TDAUChannel2TemperatureOffset', ctypes.c_uint, 8),
                ('TDAUChannel3TemperatureOffset', ctypes.c_uint, 8),
                ('TDAUChannel4TemperatureOffset', ctypes.c_uint, 8)]
                
class TDAU_readback_temperature(Register):
    _fields_ = [('STAT1', ctypes.c_uint, 8),
                ('TEMP1', ctypes.c_uint, 8),
                ('STAT2', ctypes.c_uint, 8),
                ('TEMP2', ctypes.c_uint, 8),
                ('STAT3', ctypes.c_uint, 8),
                ('TEMP3', ctypes.c_uint, 8),
                ('STAT4', ctypes.c_uint, 8),
                ('TEMP4', ctypes.c_uint, 8)]
                
class P2PControlReg(Register):
    BAR = 0
    BASEADDR = 0x00008300
    _fields_ = [('Reserved', ctypes.c_uint, 30),
                ('P2PAddressType', ctypes.c_uint, 1),
                ('P2PEnable', ctypes.c_uint, 1)]
                
class P2PStartAddressLowerReg(Register):
    BAR = 0
    BASEADDR = 0x00008310
    _fields_ = [('P2PStartAddressLower', ctypes.c_uint, 32)]
                
class P2PStartAddressUpperReg(Register):
    BAR = 0
    BASEADDR = 0x00008320
    _fields_ = [('P2PStartAddressUpper', ctypes.c_uint, 32)]
    
class ADT7411StatusReg(Register):
    BAR = 1
    BASEADDR = 0x00000D00
    _fields_ = [('ADT7411I2CAddressNAK', ctypes.c_uint, 1),
                ('ADT7411I2CDataNAK', ctypes.c_uint, 1),
                ('ADT7411TXFIFOCountError', ctypes.c_uint, 1),
                ('ADT7411I2CBusyError', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 20),
                ('ADT7411OverlimitInterrupt_n', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 6),
                ('ADT7411Busy', ctypes.c_uint, 1)]
    
class ADT7411TransmitReg(Register):
    BAR = 1
    BASEADDR = 0x00000D04
    _fields_ = [('ADT7411Transmit', ctypes.c_uint, 32)]
    
class ADT7411TXFIFOCountReg(Register):
    BAR = 1
    BASEADDR = 0x00000D08
    _fields_ = [('ADT7411TXFIFOCount', ctypes.c_uint, 11),
                ('Reserved', ctypes.c_uint, 21)]

class ADT7411RXFIFOCountReg(Register):
    BAR = 1
    BASEADDR = 0x00000D0C
    _fields_ = [('ADT7411RXFIFOCount', ctypes.c_uint, 11),
                ('Reserved', ctypes.c_uint, 21)]
    
class ADT7411TXFIFODataReg(Register):
    BAR = 1
    BASEADDR = 0x00000D10
    _fields_ = [('ADT7411TXFIFOData', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)]
    
class ADT7411RXFIFODataReg(Register):
    BAR = 1
    BASEADDR = 0x00000D14
    _fields_ = [('ADT7411RXFIFOData', ctypes.c_uint, 8),
                ('Reserved', ctypes.c_uint, 24)]

class S2HPacketByteCountReg(Register):
    BAR = 0
    BASEADDR = 0x00008400
    _fields_ = [('S2HPacketByteCount', ctypes.c_uint, 16),
                ('Reserved', ctypes.c_uint, 16)]

class S2HValidPacketIndexReg(Register):
    BAR = 0
    BASEADDR = 0x00008410
    _fields_ = [('S2HValidPacketIndex', ctypes.c_uint, 16),
                ('Reserved', ctypes.c_uint, 16)]

class S2HAddressLowReg(Register):
    BAR = 0
    BASEADDR = 0x00008420
    _fields_ = [('S2HAddressLow', ctypes.c_uint, 32)]

class S2HAddressHighReg(Register):
    BAR = 0
    BASEADDR = 0x00008430
    _fields_ = [('S2HAddressHigh', ctypes.c_uint, 32)]

class S2HPacketCountReg(Register):
    BAR = 0
    BASEADDR = 0x00008440
    _fields_ = [('S2HPacketCount', ctypes.c_uint, 16),
                ('Reserved', ctypes.c_uint, 16)]

class S2HControlReg(Register):
    BAR = 0
    BASEADDR = 0x00008450
    _fields_ = [('S2HStreamingStart', ctypes.c_uint, 1),
                ('S2HStreamingStop', ctypes.c_uint, 1),
                ('S2HStreamingReset', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 28),
                ('S2HStreamingStatus', ctypes.c_uint, 1),]

class S2HConfigurationReg(Register):
    BAR = 0
    BASEADDR = 0x00008460
    _fields_ = [('S2HConfigurationDone', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]