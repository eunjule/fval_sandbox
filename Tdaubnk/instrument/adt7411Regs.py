################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: ADT7411.py
#-------------------------------------------------------------------------------
#     Purpose: ADT7411 Registers declared C_Type
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 07/25/17
#       Group: HDMT FPGA Validation
################################################################################

import ctypes

class Register(ctypes.LittleEndianStructure):
    """Register base class - defines setter/getter for integer value of register"""
    BAR = 0
    BASEADDR = 0
    BASEMUL = 80
    REGCOUNT = 1

    def init(self, value=None):
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')
    
    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i.to_bytes(4, byteorder='little'), 4)
  
    @property
    def bytes(self):
        """getter - returns integer value of register"""
        return bytes(self)
    
    @bytes.setter
    def bytes(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i ,ctypes.sizeof(self))
  
    
    @classmethod
    def address(cls, index=0):
        return cls.BASEADDR + index * cls.BASEMUL

class ADT7144_CONTROL_CONFIG1_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000018
    _fields_ = [('CONTROL_CONFIG1_data', ctypes.c_uint, 8)]
    
class ADT7144_CONTROL_CONFIG2_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000019
    _fields_ = [('CONTROL_CONFIG2_data', ctypes.c_uint, 8)]
    
class ADT7144_CONTROL_CONFIG3_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000001A
    _fields_ = [('CONTROL_CONFIG3_data', ctypes.c_uint, 8)]
    
class ADT7144_AIN2_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000009
    _fields_ = [('AIN2_MSB_Data', ctypes.c_uint, 8)]

class ADT7144_AIN3_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000000A
    _fields_ = [('AIN3_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_AIN4_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000000B
    _fields_ = [('AIN4_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_AIN5_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000000C
    _fields_ = [('AIN5_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_AIN6_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000000D
    _fields_ = [('AIN6_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_AIN7_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000000E
    _fields_ = [('AIN7_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_AIN8_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000000F
    _fields_ = [('AIN8_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_VDD_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000006
    _fields_ = [('VDD_MSB_Data', ctypes.c_uint, 8)]

class ADT7144_INTERNALTEMP_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000007
    _fields_ = [('INTERNALTEMP_MSB_Data', ctypes.c_uint, 8)]
    
class ADT7144_AIN1_MSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000008
    _fields_ = [('AIN1_MSB_Data', ctypes.c_uint, 8)]

class ADT7144_Device_ID_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000004D
    _fields_ = [('Device_ID', ctypes.c_uint, 8)]

class ADT7144_MANUFACTURER_ID_ADDR(Register):
    BAR = 1
    BASEADDR = 0x0000004E
    _fields_ = [('MANUFACTURER_ID', ctypes.c_uint, 8)]

class ADT7411_AIN1toAIN4_LSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000004
    _fields_ = [('AIN1toAIN4_LSB_DATA', ctypes.c_uint, 8)]
    
class ADT7411_AIN5toAIN8_LSB_ADDR(Register):
    BAR = 1
    BASEADDR = 0x00000005
    _fields_ = [('AIN5toAIN8_LSB_DATA', ctypes.c_uint, 8)]
    