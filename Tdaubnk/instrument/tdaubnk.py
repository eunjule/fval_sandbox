################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: tdaubnk.py
# -------------------------------------------------------------------------------
#     Purpose: Class for one TDAU Bank Card
# -------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 02/08/17
#       Group: HDMT FPGA Validation
################################################################################

import struct
import time

import Common.hilmon as hil
from Common.instruments.base import Instrument
from Common.cache_blt_info import CachingBltInfo
from Common import configs
from Tdaubnk.instrument.tdaubnkRegs import *
from ThirdParty.SASS.suppress_sensor import suppress_sensor

NoValidPacketData = 0xFFFF

class Tdaubnk(Instrument):
    def __init__(self, slot, rc, name=None):
        super().__init__(name)
        self.rc = rc
        self.slot = slot
        self.TDB_Exp_CPLD_VER = 'B010'
        self.TDB_CPLD_VER = None
        self.under_test = False
        self.instrumentblt = CachingBltInfo(callback_function=self.read_instrument_blt,name= 'TDAUBank Instrument slot {}'.format(slot))
        self.boardblt = CachingBltInfo(callback_function=self.read_board_blt,name='TDAUBank Board slot {}'.format(slot))

    def discover(self):
        tdbFound = False
        slot = self.slot

        try:
            hil.tdbConnect(slot)
            hil.tdbDisconnect(slot)
            tdbFound = True
        except RuntimeError:
            pass
        if tdbFound == True:
            return [self]
        else:
            return []

    def FpgaLoad(self, image):
        self.Log('info', 'Flashing TDAUBNK FPGA with image from \'{}\''.format(configs.TDAUBNK_FPGA_IMAGE))
        with suppress_sensor(f'FVAL TDAU Bank {self.slot}', f'HDMT_TDAUBANK:{self.slot}'):
            hil.tdbDeviceDisable(self.slot)
            result = hil.tdbFpgaLoad(self.slot, image)
            hil.tdbDeviceEnable(self.slot)
        self.Log('info', 'Current TDAUBANK Image slot {}: {}'.format(self.slot, self.GetFpgaVersion()))
        return result

    def S2HSupport(self):
        if self.GetFpgaVersion() in configs.TDAUBNK_NON_S2H_FPGAS:
            return False
        else:
            return True

    def GetFpgaVersion(self):
        return hil.tdbFpgaVersionString(self.slot)

    def BarRead(self, bar, offset):
        return hil.tdbBarRead(self.slot, bar, offset)

    def BarWrite(self, bar, offset, data):
        return hil.tdbBarWrite(self.slot, bar, offset, data)

    def Read(self, RegisterType, index=0):
        '''Reads a register of the given type, the index is for register arrays'''
        value = self.BarRead(RegisterType.BAR, RegisterType.address(index))
        return RegisterType(value=value)

    def Write(self, register, index=0):
        '''Writes a register of the given type, the index is for register arrays'''
        self.BarWrite(register.BAR, register.address(index), register.value)

    def BMR454BarRead(self, command, readLength):
        return hil.tdbBmr454Read(self.slot, command, readLength)

    def BMR454BarWrite(self, command, cmdLength):
        return hil.tdbBmr454Write(self.slot, command, cmdLength)

    def BMR454Read(self, command, readLength):
        '''Reads a data from BMR454 device. '''
        value = self.BMR454BarRead(command, readLength)
        return value

    def BMR454Write(self, command, cmdLength):
        '''Writes command to BMR454 Device '''
        self.BMR454BarWrite(command, cmdLength)

    def fromL11(self, b):
        n = struct.unpack('<H', b)[0]
        m = n & 0x7ff
        if m & 0x400: m -= 0x800
        e = (n >> 11) & 0x1F
        if e & 0x10: e -= 0x20
        return m * 2 ** e

    def PmbusRead(self, command):
        return self.fromL11(self.BMR454BarRead(command, 2))

    def pmr(self, cmd, length):
        return hil.psPmbusRead(self.slot, cmd, length)

    def EepromRead(self):
        return hil.tdbBltEepromRead(self.slot)

    def DDR3DmaWrite(self, address, data):
        return hil.tdbDmaWrite(self.slot, address, data)

    def DDR3DmaRead(self, address, length):
        return hil.tdbDmaRead(self.slot, address, length)

    def TDAURead(self, index, cmd, length):
        return hil.tdbTdauRead(self.slot, index, cmd, length)

    def TDAUWrite(self, index, cmd, data):
        hil.tdbTdauWrite(self.slot, index, cmd, data)

    def TDAUWriteMem(self, index, address, data):
        hil.tdbTdauWriteMem(self.slot, index, address, data)

    def TDAUReadMem(self):
        return hil.tdbTdauReadMem(self.slot, index, address, data)

    def RCPeerToPeerMemoryBase(self):
        return hil.rcPeerToPeerMemoryBase()

    def ADT7411Init(self):
        hil.tdbInit(self.slot)

    def ADT7411Write(self, register, data):
        hil.tdbAdt7411Write(self.slot, register.BASEADDR, data)

    def ADT7411Read(self, register):
        return hil.tdbAdt7411Read(self.slot, register.BASEADDR)

    def ADT7411ChannelRead(self, channel):
        return hil.tdbVmonRead(self.slot, channel)
    
    def tdau_auto(self,slot,enable):
        for reg in range(0x904,0xC05,0x80):
            self.bar(slot,1,reg,0x400F006F if enable else 0)

    def bar(self,slot,barno,addr=None,data=None):
        if data is not None and addr is None:
            raise ValueError('need addr')
        elif addr is None:
            _bardump(slot,barno)
        elif data is None:
            return hil.tdbBarRead(slot,barno,addr)
        else:
            hil.tdbBarWrite(slot,barno,addr,data)  

    def StreamingBufferaddrsize(self):
        return hil.tdbStreamingBuffer(self.slot)

    def GetTotalNoofPackets(self):
        no_of_packets = self.Read(S2HValidPacketIndexReg)
        return no_of_packets.value

    def GetPacketCount(self):
        packet_count = self.Read(S2HPacketCountReg)
        return packet_count.value

    def IsStreamingOn(self):
        S2HControlRegister = self.Read(S2HControlReg)
        return S2HControlRegister.S2HStreamingStatus

    def TurnONStreamingWithControlReg(self):
        S2HControlRegister = S2HControlReg()
        S2HControlRegister.S2HStreamingStart = 1
        self.Write(S2HControlRegister)

    def TurnOFFStreamingWithControlReg(self):
        S2HControlRegister = S2HControlReg()
        S2HControlRegister.S2HStreamingStop = 1
        self.Write(S2HControlRegister)
        streaming_off_status = self.Read(S2HControlReg)
        while streaming_off_status.S2HStreamingStatus != 0:
            time.sleep(.02)
        if streaming_off_status.S2HStreamingStatus == 0:
            self.Log('debug', 'S2H Off Successful.S2HControlRegister Status {} '.format(hex(streaming_off_status.value)))

    def ResetStreamingWithControlReg(self):
        if self.IsStreamingOn():
            self.Log('error', 'Reset unsuccessful as Streaming is ON ')
        S2HControlRegister = S2HControlReg()
        streaming_reset_status_value = 1
        S2HControlRegister.S2HStreamingReset = streaming_reset_status_value
        self.Write(S2HControlRegister)
        while streaming_reset_status_value != 0:
            streaming_reset_status_value = self.Read(S2HControlReg).S2HStreamingReset
            time.sleep(.02)
        if streaming_reset_status_value == 0:
            self.Log('debug',
                     'S2H Reset Successful . S2HControlRegister Status {} '.format(hex(streaming_reset_status_value)))

    def TurnONStreamingWithTrigger(self):
        SendTrigger = SendTriggerReg()
        SendTrigger.CardType = 0x5
        SendTrigger.Payload = 0x1
        self.Log('debug', 'Start Trigger value: {} '.format(hex(SendTrigger.value)))
        self.Write(SendTrigger)

    def TurnOFFStreamingWithTrigger(self):
        SendTrigger = SendTriggerReg()
        SendTrigger.CardType = 0x5
        SendTrigger.Payload = 0x0
        self.Log('debug', 'Stop Trigger value: {} '.format(hex(SendTrigger.value)))
        self.Write(SendTrigger)

    def ResetStreamingWithTrigger(self):
        SendTrigger = SendTriggerReg()
        SendTrigger.CardType = 0x5
        SendTrigger.Payload = 0x2
        self.Log('debug', 'Reset Trigger value: {} '.format(hex(SendTrigger.value)))
        self.Write(SendTrigger)

    def IsS2HConfigurationDone(self):
        S2HConfigurationRegister = self.Read(S2HConfigurationReg)
        if S2HConfigurationRegister.S2HConfigurationDone == 1:
            return True
        else:
            return False

    def ReadPacketNumber(self,idx,buffersize):
        return struct.unpack('<H', bytes(buffersize[idx][:2]))[0]

    def S2HRegisterAndBufferSetup(self):
        addr,size = self.StreamingBufferaddrsize()
        max_packets_in_site_controller, pkt_byte_count, buffersize = self.ReadStreamingDetails(addr)
        last_packet_index = self.Read(S2HValidPacketIndexReg)
        self.Log('debug', 'max packets before loss = {} '.format(max_packets_in_site_controller))
        if last_packet_index.value == NoValidPacketData :
            last_packet_index.value = max_packets_in_site_controller - 1
        return last_packet_index, max_packets_in_site_controller

    def ReadStreamingDetails(self, addr):
        addr, size = self.StreamingBufferaddrsize()
        pkt_byte_count = self.Read(S2HPacketByteCountReg)
        max_packets_in_site_controller = self.Read(S2HPacketCountReg)
        buffer_size = (ctypes.c_ubyte * pkt_byte_count.value * max_packets_in_site_controller.value).from_address(addr)
        self.Log('debug', 'pkt_byte_count: {} NumberOfPackets: {} buffersize: {}'.format(pkt_byte_count.value, max_packets_in_site_controller.value, buffer_size))
        return max_packets_in_site_controller.value, pkt_byte_count, buffer_size

    def ReadPacketIndexandPacketHeader(self, buffer_size):
        packet_index = self.Read(S2HValidPacketIndexReg).value
        packet_number_from_packet_header = self.ReadPacketNumber(packet_index, buffer_size)
        self.Log('info','Packet Index Register: {} Packet Count: {}. '.format(packet_index,packet_number_from_packet_header))
        return packet_index,packet_number_from_packet_header

    def IsDiagsCardConnected(self):
        try:
            hil.tddConnect(self.slot)
            return True
        except:
            return False

    def DiagsDisconnect(self):
        hil.tddDisconnect(self.slot)

    def GetDiagsReferenceVoltage(self):
        return hil.tddPeciVrefGet(self.slot)

    def PECIConfig(self,PECIDevice,Status,AddressOfChannel,BitRate):
        hil.tdbPeciConfig(self.slot,PECIDevice,Status,AddressOfChannel,BitRate)

    def PECITemperatureRead(self,PECIDevice):
        return hil.tdbPeciTemperatureRead(self.slot,PECIDevice)

    def Initialize(self):

        #self.log_blt_info()

        # load image / core-init
        if configs.TDAUBNK_FPGA_LOAD:
            self.Log('info', 'Turning OFF TDAU Devices Auto mode.')
            self.tdau_auto(self.slot,0)
            time.sleep(0.02)
            self.Log('info', 'Previous TDAUBNK Image slot {}: {}'.format(self.slot, self.GetFpgaVersion()))
            self.Log('info', 'Flashing TDAUBNK FPGA with image from \'{}\''.format(configs.TDAUBNK_FPGA_IMAGE))
            self.FpgaLoad(configs.TDAUBNK_FPGA_IMAGE)
        self.Log('info', 'Current TDAUBANK Image slot {}: {}'.format(self.slot, self.GetFpgaVersion()))

        # Diags Card connect verification
        if self.IsDiagsCardConnected():
            self.Log('info', 'Diags Card connected on the tester')
        else:
            self.Log('warning', 'Diags Card not connected on the tester')

        # CPLD Version Verification
        TDB_CPLD_VER = hil.tdbCpldVersionString(self.slot)
        self.Log('info', 'TDAU Bank CPLD Version: {} '.format(TDB_CPLD_VER))
        if TDB_CPLD_VER == '':
            self.Log('error',
                     'CPLD is not programmed for TDAU Bank.Please program CPLD (xsvf File) generally stored at location \\datagrovehf\STTD\TSTD\CPLDs with hil.tdbJtagExecute')

            # self.Log('info', 'Initializing TDAUBANK FPGA slot {}'.format(self.slot))



    def read_instrument_blt(self):
        return hil.tdbBltInstrumentRead (self.slot)


    def read_board_blt(self):
        return hil.tdbBltBoardRead(self.slot)

    def log_blt_info(self):
        self.instrumentblt.write_to_log()
        self.boardblt.write_to_log()

    def WaitTime(self, time_to_wait):
        start_time = time.perf_counter()
        while time.perf_counter() - start_time < time_to_wait:
            pass

    def disable_tdaubank_fpga_clock_compensation(self):
        gpiostatus = GpioReg()
        gpiostatus.value = 0
        self.Write(gpiostatus)
        gpiostatus = self.Read(GpioReg)
        self.Log('info', 'TDAUBANK GPIO Reg status after disabling Clock Compensation = 0x{:X}'.format(gpiostatus.value))

    def enable_tdaubank_fpga_clock_compensation(self):
        gpiostatus = GpioReg()
        gpiostatus.value = 1
        self.Write(gpiostatus)
        gpiostatus = self.Read(GpioReg)
        self.Log('info', 'TDAUBANK GPIO Reg status after enabling Clock Compensation = 0x{:X}'.format(gpiostatus.value))

    def reset_aurora_trigger_bus(self):
        pass
