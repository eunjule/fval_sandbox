################################################################################
# INTEL CONFIDENTIAL - Copyright 2018-2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import sys
import time
import unittest
from unittest.mock import patch
from unittest.mock import Mock

from ThirdParty.HPS.hps_diskless_boot import BootHps

if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    print(repo_root_path)
    sys.path.append(os.path.abspath(repo_root_path))

from Common import fval
from Common import pcie
from Common.blt_simulator import BLT
from Common.cache_blt_info import CachingBltInfo
from Hpcc.instrument.hpcc import Hpcc
from Common.instruments.tester import Tester
from Rc2.instrument.rc2 import Rc2
from Hbicc.instrument.hbicc import Hbicc
from Hbicc.instrument.mainboard import MainBoard
from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.pinmultiplier import PinMultiplier
from Hbicc.instrument.psdb import Psdb
from Hbicc.instrument.ringmultiplier import RingMultiplier
from Hbirctc.instrument.hbirctc import Hbirctc
from Hbidtb.instrument.hbidtb import Hbidtb


class HbiccTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None
        self.create_mocks_and_store_original_states()

    def tearDown(self):
        self.remove_mocks_and_restore_original_states()

    def create_mocks_and_store_original_states(self):
        self.original_HpsInit_run_intialization = BootHps.run_intialization
        self.original_sleep = time.sleep

        BootHps.run_intialization = Mock()
        time.sleep = Mock()

    def remove_mocks_and_restore_original_states(self):
        BootHps.run_intialization = self.original_HpsInit_run_intialization
        time.sleep = self.original_sleep

    def test_first_level_discover_one_or_more_devices_found(self):
        with patch('Common.hilmon.hil') as hil,\
             patch('Common.fval.Object.Log') as log:
            
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            
            def mock_device_open(guid, viddid, slot, extra):
                if guid == pcie.pin_multiplier_guid:
                    raise RuntimeError()
            hil.pciDeviceOpen = Mock(side_effect=mock_device_open)

            return_hbicc = hbicc_instance.discover_all_devices()
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)
            self.assertIsInstance(return_hbicc.pat_gen, PatGen)
            self.assertIsInstance(return_hbicc.ring_multiplier, RingMultiplier)
            self.assertEqual(return_hbicc.psdb_0_pin_0, None)
            self.assertEqual(return_hbicc.psdb_0_pin_1, None)
            self.assertEqual(return_hbicc.psdb_1_pin_0, None)
            self.assertEqual(return_hbicc.psdb_1_pin_1, None)

    def test_discover_all_devices_found(self):
        with patch('Common.hilmon.hil') as hil:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hil.pciDeviceOpen = Mock()
            return_hbicc = hbicc_instance.discover_all_devices()
            self.assertIsInstance(return_hbicc.pat_gen, PatGen)
            self.assertIsInstance(return_hbicc.ring_multiplier, RingMultiplier)
            self.assertIsInstance(return_hbicc.psdb_0_pin_0, PinMultiplier)
            self.assertIsInstance(return_hbicc.psdb_0_pin_1, PinMultiplier)
            self.assertIsInstance(return_hbicc.psdb_1_pin_0, PinMultiplier)
            self.assertIsInstance(return_hbicc.psdb_1_pin_1, PinMultiplier)

    def test_discover_no_devices_found(self):
        with patch('Common.hilmon.hil') as hil,\
             patch('Common.fval.Object.Log') as log:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hil.pciDeviceOpen = Mock(side_effect=RuntimeError)
            return_Hbicc = hbicc_instance.discover_all_devices()
            self.assertEqual(return_Hbicc, None)

    def test_load_all_fpgas(self):
        with patch('Common.hilmon.hil') as mock_hil, \
               patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)
            mock_sass.SassApi= Mock()
            mock_hil.hbiMbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            mock_hil.hbiPsdbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            hbicc_instance.load_instrument_fpga()
            log_list = self.get_all_log_calls(hbicc_instance)
            expected_message_fragment = 'FPGA load succeeded'
            expected_log_level = 'INFO'
            successful_loads = 0
            for args, kwargs in log_list:
                log_level, message = args
                if expected_message_fragment in message and log_level.upper() == expected_log_level:
                    successful_loads += 1
            
            if successful_loads < 5:
                self.fail('Not all FPGAs loaded successfully')
                    

    def test_load_fpga_for_patgen_fail(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass,\
                patch.object(PatGen, 'load_fpga_using_hil') as mock_patgen_load_fpga:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            def load():
                raise RuntimeError
                    
            mock_patgen_load_fpga.side_effect = load
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)
            mock_sass.SassApi = Mock()
            mock_hil.hbiMbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            mock_hil.hbiPsdbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            hbicc_instance.load_instrument_fpga()
            log_list = self.get_log_list(hbicc_instance)
            expected_message_fragment = 'load was not successful with image'
            expected_log_level = 'ERROR'
            for args, kwargs in hbicc_instance.pat_gen.Log.call_args_list:
                log_level, message = args
                if expected_message_fragment in message and log_level.upper() == expected_log_level:
                    break
            else:
                self.fail(f'Did not find log call with level={expected_log_level} and message containing "{expected_message_fragment}"')

    def test_load_fpga_for_patgen_set_to_false_by_user(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            hbicc_instance.pat_gen.fpga_load = False
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)
            mock_sass.SassApi = Mock()
            mock_hil.hbiMbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            mock_hil.hbiPsdbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            hbicc_instance.load_instrument_fpga()
            log_list = self.get_log_list(hbicc_instance)
            expected_message_fragment = 'FPGA was not loaded due to configs setting'
            expected_log_level = 'WARNING'
            for args in log_list:
                log_level, message = args
                if expected_message_fragment in message and log_level.upper() == expected_log_level:
                    break
            else:
                self.fail(f'Did not find log call with level={expected_log_level} and message containing "{expected_message_fragment}"')

    def test_load_fpga_for_pinmultiplier_fail(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass,\
                patch.object(PinMultiplier, 'load_fpga_using_hil') as mock_psdb_load_fpga:
                
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            mock_psdb_load_fpga.side_effect = [True, True, True, RuntimeError]
            mock_hil.hbiMbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            mock_hil.hbiPsdbBltBoardRead = Mock(return_value=Mock(PartNumberCurrent='AAK11111-300'))
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)
            mock_sass.SassApi = Mock()
            hbicc_instance.load_instrument_fpga()
            
            expected_message_fragment = 'load was not successful with image'
            expected_log_level = 'ERROR'
            for args, kwargs in hbicc_instance.psdb_1_pin_1.Log.call_args_list:
                log_level, message = args
                if expected_message_fragment in message and log_level.upper() == expected_log_level:
                    break
            else:
                self.fail(f'Did not find log call with level={expected_log_level} and message containing "{expected_message_fragment}"')
                

    def test_log_all_blt(self):
        with patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
             patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
             patch('Hbidtb.instrument.hbidtb.hil') as mock_dtb_hil:
            hbidtbs = {0: Hbidtb(0), 1: Hbidtb(1), 2: Hbidtb(2), 3: Hbidtb(3)}
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs=hbidtbs)


            mb_hil_blt = mock_mainboard_hil.BLT()
            mb_hil_blt.VendorName = 'Intel'
            mb_hil_blt.DeviceName = 'Ehcdps'
            mb_hil_blt.PartNumberAsBuilt = '12334'
            mb_hil_blt.PartNumberCurrent = '12345'
            mb_hil_blt.SerialNumber = '56789'
            mb_hil_blt.ManufactureDate = '08/23/2017'

            psdb_hil_blt = mock_psdb_hil.BLT()
            psdb_hil_blt.VendorName = 'Intel'
            psdb_hil_blt.DeviceName = 'Ehcdps'
            psdb_hil_blt.PartNumberAsBuilt = '12334'
            psdb_hil_blt.PartNumberCurrent = '12345'
            psdb_hil_blt.SerialNumber = '56789'
            psdb_hil_blt.ManufactureDate = '08/23/2017'
            call_back1 = Mock(return_value=mb_hil_blt)
            call_back2 = Mock(return_value=psdb_hil_blt)
            mock_psdb_hil.hbiPsdbBltBoardRead = Mock()
            hbi_mainboard = MainBoard()
            psdb = Psdb(slot=2)
            hbicc_instance.mainboard = hbi_mainboard
            hbicc_instance.psdb_0 = psdb
            hbicc_instance.psdb_1 = psdb
            self.mock_logging_for_all_cc_board_classes(hbicc_instance)
            hbi_mainboard.read_instrument_blt=Mock(return_value=call_back1)
            psdb.read_instrument_blt=Mock(return_value=call_back2)
            hbicc_instance.psdb_0_pin_0 = PinMultiplier(psdb, 0)
            hbicc_instance.psdb_0_pin_1 = PinMultiplier(psdb, 1)
            hbicc_instance.psdb_1_pin_0 = PinMultiplier(psdb, 0)
            hbicc_instance.psdb_1_pin_1 = PinMultiplier(psdb, 1)
            # mock_mainboard_hil.hbiMainBoardBltBoardRead = Mock()
            hbicc_instance.log_instrument_blt()
            self.assertIsInstance(hbicc_instance.mainboard.blt, CachingBltInfo)

    def test_log_all_blt_fail_with_hil_runtime_error(self):
        with patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
             patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            mock_psdb_hil.hbiPsdbBltBoardRead = Mock(side_effect=RuntimeError)
            mock_mainboard_hil.hbiMainBoardBltBoardRead = Mock(side_effect=RuntimeError)
            hbi_mainboard= MainBoard()
            psdb = Psdb(slot=2)
            hbicc_instance.mainboard = hbi_mainboard
            hbicc_instance.psdb_0 = psdb
            hbicc_instance.psdb_1 = psdb
            self.mock_logging_for_all_cc_board_classes(hbicc_instance)
            hbicc_instance.psdb_0_pin_0 = PinMultiplier(psdb, 0)
            hbicc_instance.psdb_0_pin_1 = PinMultiplier(psdb, 1)
            hbicc_instance.psdb_1_pin_0 = PinMultiplier(psdb, 0)
            hbicc_instance.psdb_1_pin_1 = PinMultiplier(psdb, 1)
            dtb0 = Hbidtb(0)
            dtb0.Log = Mock()
            hbicc_instance.log_instrument_blt()
            args, kwargs = hbicc_instance.psdb_0.Log.call_args_list[0]
            log_level, message = args
            self.assertIn('BLT read was unsuccessful', message)
            self.assertEqual('WARNING', log_level.upper())

    def test_discover_all_instruments_when_Hbi_is_present(self):
        with patch('Common.hilmon.hil') as hil:
            tester = Tester()
            hil.rcConnect = Mock(side_effect=RuntimeError)
            hil.rc3Connect = Mock(side_effect=RuntimeError)
            hil.hbiMbConnect = Mock(return_value='')
            tester.discover_all_instruments()
            self.assertEqual(tester._is_hbi_tester, True)
            self.assertIsInstance(tester.devices[0], Hbirctc)
            hbicc_list = [x for x in tester.devices if isinstance(x, Hbicc)]
            self.assertEqual(len(hbicc_list), 1, msg='Hbicc not found in devices')

    def test_discover_all_instruments_when_Hbi_is_not_present(self):
        with patch('Common.hilmon.hil') as hil:
            tester = Tester()
            hil.bpInit = Mock()
            self.mockDevices(hil)
            tester.discover_all_instruments()
            self.assertEqual(tester.is_hbi_tester(), False)
            self.assertIsInstance(tester.devices[0], Rc2)
            for index in range(1, 13):
                self.assertIsInstance(tester.devices[index], Hpcc, str(index) + tester.devices[index].__class__.__name__)

    def test_initialize_when_Hbi_is_present(self):
        with patch('Common.hilmon.hil') as hil, \
            patch('Common.instruments.hbi_instrument.HbiInstrument.is_non_zero_or_non_f'),\
                patch('Common.instruments.s2h_interface.S2hInterface.wait_on_matching_valid_packet_index'):

            hil.rcConnect = Mock(side_effect=RuntimeError)
            hil.rc3Connect = Mock(side_effect=RuntimeError)
            hil.hbiMbConnect = Mock(return_value='')

            tester = Tester()

            device_list = tester.discover_all_instruments()
            self.mock_device_initialization(device_list)
            tester.rc.initialize_fpga = Mock()

            tester.Initialize()

            self.assertEqual(tester.is_hbi_tester(), True)
            self.assertIsInstance(tester.devices[0], Hbirctc)
            self.assertIsInstance(tester.devices[1], Hbicc)
            self.assertIsInstance(tester.devices[1].pat_gen, PatGen)
            self.assertIsInstance(tester.devices[1].ring_multiplier, RingMultiplier)
            self.assertIsInstance(tester.devices[1].psdb_0_pin_0, PinMultiplier)
            self.assertIsInstance(tester.devices[1].psdb_0_pin_1, PinMultiplier)
            self.assertIsInstance(tester.devices[1].psdb_1_pin_0, PinMultiplier)
            self.assertIsInstance(tester.devices[1].psdb_1_pin_1, PinMultiplier)

    def mock_device_initialization(self, device_list):
        for devices in device_list:
            devices.under_test = True
            devices.load_fpga = Mock()
            devices.log_fpga_version = Mock()
            devices.log_chip_id = Mock()
            devices.log_build = Mock()
            devices.initialize = Mock()
            devices.log_blt_info = Mock()
            if isinstance(devices, Hbicc):
                device_list = [devices.pat_gen,
                               devices.ring_multiplier,
                               devices.psdb_0_pin_0,
                               devices.psdb_0_pin_1,
                               devices.psdb_1_pin_0,
                               devices.psdb_1_pin_1]
                for device in device_list:
                    device.initialize = Mock()

    def mockDevices(self, hil):
        rcConnectList = [''] * 12
        rc3ConnectList = [RuntimeError] * 12
        hbiMbConnectList = [RuntimeError] * 12
        tdbConnectList = [RuntimeError] * 12
        hpccDcConnectList = [''] * 12
        hpccAcConnectList = [''] * 12
        hddpsConnectList = [RuntimeError] * 24
        hvdpsConnectList = [RuntimeError] * 12
        hbidpsConnectList = [RuntimeError] * 12

        deviceConnectList = {'rcConnectList': rcConnectList,
                             'rc3ConnectList': rc3ConnectList,
                             'hbiMbConnectList': hbiMbConnectList,
                             'tdbConnectList': tdbConnectList,
                             'hpccDcConnectList': hpccDcConnectList,
                             'hpccAcConnectList': hpccAcConnectList,
                             'hddpsConnectList': hddpsConnectList,
                             'hvdpsConnectList': hvdpsConnectList,
                             'hbidpsConnectList': hbidpsConnectList}

        hil.rcConnect = Mock(side_effect=deviceConnectList['rcConnectList'])
        hil.rc3Connect = Mock(side_effect=deviceConnectList['rc3ConnectList'])
        hil.hbiMbConnect = Mock(side_effect=deviceConnectList['hbiMbConnectList'])
        hil.tdbConnect = Mock(side_effect=deviceConnectList['tdbConnectList'])
        hil.hpccDcConnect = Mock(side_effect=deviceConnectList['hpccDcConnectList'])
        hil.hddpsConnect = Mock(side_effect=deviceConnectList['hddpsConnectList'])
        hil.hvdpsConnect = Mock(side_effect=deviceConnectList['hvdpsConnectList'])
        hil.hbiDpsConnect = Mock(side_effect=deviceConnectList['hbidpsConnectList'])

    def test_log_all_cpld_version_pass(self):
        with patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
             patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
             patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
             patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
             patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)

            hbicc_instance.log_instrument_cpld()
            for args in self.get_log_list(hbicc_instance):
                log_level, message = args
                self.assertIn('FPGA CPLD Version', message)
                self.assertEqual('INFO', log_level.upper())

    def test_log_patgen_cpld_version_fail(self):
        with patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
             patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
             patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
             patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
             patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            mock_patgen_hil.hbiMbCpldVersionString = Mock(side_effect=RuntimeError)
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)

            hbicc_instance.log_instrument_cpld()
            for index, args in enumerate(self.get_log_list(hbicc_instance)):
                log_level, message = args
                if index == 0:
                    self.assertIn('CPLD read was unsuccessful', message)
                    self.assertEqual('WARNING', log_level.upper())
                else:
                    self.assertIn('FPGA CPLD Version', message)
                    self.assertEqual('INFO', log_level.upper())
    
    def test_log_cpld_for_pinmultiplier_fail(self):
        with patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
             patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
             patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
             patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
             patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil:
            hbicc_instance = Hbicc(rc=Hbirctc(mainboard=MainBoard()), hbidtbs={0:Hbidtb(0), 1:Hbidtb(1), 2:Hbidtb(2), 3:Hbidtb(3)})
            hbicc_instance.discover_all_devices()
            mock_pinmultiplier_hil.hbiPsdbCpldVersionString = Mock(side_effect=[True, True, True, RuntimeError])
            self.mock_logging_for_all_cc_fpga_classes(hbicc_instance)

            hbicc_instance.log_instrument_cpld()
            for index, args in enumerate(self.get_log_list(hbicc_instance)):
                log_level, message = args
                if index == 5:
                    self.assertIn('CPLD read was unsuccessful', message)
                    self.assertEqual('WARNING', log_level.upper())
                else:
                    self.assertIn('FPGA CPLD Version', message)
                    self.assertEqual('INFO', log_level.upper())

    def test_log_instrument_fpga_version(self):
        pass

    def test_log_instrument_build(self):
        pass

    def mock_logging_for_all_cc_fpga_classes(self, cc_instance):
        if cc_instance.pat_gen:
            cc_instance.pat_gen.Log = Mock()
        if cc_instance.ring_multiplier:
            cc_instance.ring_multiplier.Log = Mock()
        if cc_instance.psdb_0_pin_0:
            cc_instance.psdb_0_pin_0.Log = Mock()
        if cc_instance.psdb_0_pin_1:
            cc_instance.psdb_0_pin_1.Log = Mock()
        if cc_instance.psdb_1_pin_0:
            cc_instance.psdb_1_pin_0.Log = Mock()
        if cc_instance.psdb_1_pin_1:
            cc_instance.psdb_1_pin_1.Log = Mock()
        if cc_instance:
            cc_instance.Log = Mock()
        return cc_instance

    def mock_logging_for_all_cc_board_classes(self, cc_instance):
        if cc_instance.mainboard:
            cc_instance.mainboard.Log = Mock()
        if cc_instance.psdb_0:
            cc_instance.psdb_0.Log = Mock()
        if cc_instance.psdb_1:
            cc_instance.psdb_1.Log = Mock()
        if cc_instance:
            cc_instance.Log = Mock()

    def get_log_list(self, hbicc_instance):
        return [hbicc_instance.pat_gen.Log.call_args_list[0][0],
                hbicc_instance.ring_multiplier.Log.call_args_list[0][0],
                hbicc_instance.psdb_0_pin_0.Log.call_args_list[0][0],
                hbicc_instance.psdb_0_pin_1.Log.call_args_list[0][0],
                hbicc_instance.psdb_1_pin_0.Log.call_args_list[0][0],
                hbicc_instance.psdb_1_pin_1.Log.call_args_list[0][0]]

    def get_all_log_calls(self, hbicc_instance):
        return hbicc_instance.pat_gen.Log.call_args_list + \
                hbicc_instance.ring_multiplier.Log.call_args_list + \
                hbicc_instance.psdb_0_pin_0.Log.call_args_list + \
                hbicc_instance.psdb_0_pin_1.Log.call_args_list + \
                hbicc_instance.psdb_1_pin_0.Log.call_args_list + \
                hbicc_instance.psdb_1_pin_1.Log.call_args_list

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
