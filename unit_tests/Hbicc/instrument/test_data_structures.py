# INTEL CONFIDENTIAL
# Copyright 2020 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import unittest

from Hbicc.instrument import data_structures


class DcTriggerQueueCommandTests(unittest.TestCase):
    def setUp(self):
        self.tq_struct = data_structures.DcTriggerQueueCommand()

    def test_size(self):
        self.assertEqual(len(bytes(self.tq_struct)), 8)

    def test_initial_value(self):
        self.assertEqual(self.tq_struct.to_int(), 0x0000000000000000)
        self.assertEqual(self.tq_struct.to_bytes(), bytes(8))

    def test_payload(self):
        self._set_all_bits_in_field_and_check_struct_value('payload', 0x0000FFFFFFFFFFFF)

    def test_target(self):
        self._set_all_bits_in_field_and_check_struct_value('target', 0x000F000000000000)

    def test_channel_set(self):
        self._set_all_bits_in_field_and_check_struct_value('channel_set', expected=0x0070000000000000)

    def test_target(self):
        self._set_all_bits_in_field_and_check_struct_value('target', expected=0x0380000000000000)

    def test_byte_count(self):
        self._set_all_bits_in_field_and_check_struct_value('byte_count', expected=0x1C00000000000000)

    def test_psdb(self):
        self._set_all_bits_in_field_and_check_struct_value('psdb', expected=0x2000000000000000)

    def test_is_write(self):
        self._set_all_bits_in_field_and_check_struct_value('is_write', expected=0x4000000000000000)

    def test_is_command(self):
        self._set_all_bits_in_field_and_check_struct_value('is_command', expected=0x8000000000000000)

    def _set_all_bits_in_field_and_check_struct_value(self, field, expected):
        setattr(self.tq_struct, field, 0xFFFFFFFFFFFFFFFF)
        self.assertEqual(self.tq_struct.to_int(), expected)
        self.assertEqual(self.tq_struct.to_bytes(), expected.to_bytes(8, byteorder='little'))

