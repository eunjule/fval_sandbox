# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
from unittest.mock import patch

from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.mainboard import MainBoard
from Hbirctc.instrument.hbirctc import Hbirctc


class PatGenTests(unittest.TestCase):
    def test_init_does_not_touch_hardware(self):
        '''Object creation should not involve any HIL calls'''
        with patch('Common.hilmon.hil') as mock_hil, \
             patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil:
            mb = MainBoard()
            rc = Hbirctc(mainboard=mb)
            pg = PatGen(rc=rc)
            self.assertEqual(mock_hil.method_calls, [])
            self.assertEqual(mock_patgen_hil.method_calls, [])