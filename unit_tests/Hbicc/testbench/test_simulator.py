# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import random
import unittest
from unittest.mock import Mock

from Hbicc.instrument import patgen_register
from Hbicc.instrument.patgen_register import ALARMS1
from Hbicc.testbench.patgen_simulator import PatGen
from Hbicc.testbench.trigger_utility import TriggerType, TriggerUtility


class PatGenTests(unittest.TestCase):
    def setUp(self):
        self.patgen = PatGen(system=Mock())
        for slice in self.patgen.slices:
            slice.add_invalid_ctv_data = Mock()
            slice._get_pin_vector_handlers = Mock()
            slice._pin_vector_handlers = [Mock(handle_vector=Mock(return_value=[None, None, None]), register_read=Mock(return_value=0)) for x in range(4)]
        
    def test_pattern_flow(self):
        exit_code = random.getrandbits(32)
        self.load_pattern(exit_code)
        self.start_prestage()
        self.assertTrue(self.prestaging())
        self.assertFalse(self.prestaging())
        self.patgen.run_pattern()
        self.assertEqual(self.end_status(), exit_code)

    def test_alarms1_reset(self):
        exit_code = random.getrandbits(32)
        self.patgen.slices[0].registers[ALARMS1.ADDR] = 0x00000002
        self.load_pattern(exit_code)
        self.start_prestage()
        self.assertTrue(self.prestaging())
        self.assertFalse(self.prestaging())
        self.patgen.run_pattern()
        self.assertEqual(self.end_status(), exit_code)
        self.assertEqual(self.patgen.slices[0].registers[ALARMS1.ADDR], 0)
    
    def start_prestage(self):
        Reg = patgen_register.PATTERN_CONTROL
        data = self.patgen.bar_read(Reg.BAR, Reg.ADDR)
        pattern_control = Reg(value=data)
        pattern_control.SliceGroupMask = 1
        pattern_control.PrestagePattern = 1
        pattern_control.SliceActiveInBurst = 1
        self.patgen.bar_write(Reg.BAR, Reg.ADDR, pattern_control.value)
    
    def prestaging(self):
        return (self.patgen.bar_read(1, 0x164) & 0x2) == 0x2
    
    def end_status(self):
        return self.patgen.bar_read(1, 0x160)
    
    def load_pattern(self, exit_code):
        drive_0 = b'\x24\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80'
        end = exit_code.to_bytes(4, 'little') + b'\x00\x00\x00\x10\x21\x00\x00\x00\x00\x00\x00\x60'
        last_vector = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80'
        pattern = drive_0*510 + end + last_vector
        self.patgen.dma_write(0, pattern)

    def test_get_domain_id(self):
        for slice_simulator in self.patgen.slices:
            trigger_control = patgen_register.TRIGGER_CONTROL(random.getrandbits(32))
            slice_simulator.registers.update({patgen_register.TRIGGER_CONTROL.ADDR: trigger_control.value})
            self.assertEqual(trigger_control.domain_id, slice_simulator.get_domain_id())

    def test_is_domain_master_present_in_domain(self):
        domain_id_master_list = [(0, False), (1, False), (0, True), (1, False), (0, False)]
        for i, slice_simulator in enumerate(self.patgen.slices):
            trigger_control = patgen_register.TRIGGER_CONTROL()
            trigger_control.domain_id = domain_id_master_list[i][0]
            trigger_control.domain_master = int(domain_id_master_list[i][1])
            slice_simulator.registers.update({trigger_control.ADDR: trigger_control.value})
            slice_simulator.running_pattern = True
        self.assertTrue(self.patgen.is_master_running_pattern_present_in_domain(0))
        self.assertFalse(self.patgen.is_master_running_pattern_present_in_domain(1))

    def test_is_domain_master_present_in_domain_pattern_not_running(self):
        domain_id_master_list = [(0, False), (1, False), (0, True), (1, False), (0, False)]
        for i, slice_simulator in enumerate(self.patgen.slices):
            trigger_control = patgen_register.TRIGGER_CONTROL()
            trigger_control.domain_id = domain_id_master_list[i][0]
            trigger_control.domain_master = int(domain_id_master_list[i][1])
            slice_simulator.registers.update({trigger_control.ADDR: trigger_control.value})
            slice_simulator.running_pattern = False
        self.assertFalse(self.patgen.is_master_running_pattern_present_in_domain(0))
        self.assertFalse(self.patgen.is_master_running_pattern_present_in_domain(1))

    def test_domain_trigger_down(self):
        trigger_type = TriggerType.DomainTrigger.value
        trigger_utility = TriggerUtility(Mock(), Mock())
        trigger_utility.trigger_type = trigger_type
        trigger_utility.target_domain = 0
        data = trigger_utility.construct_trigger_word()
        domain_id_master_list = [(0, False), (1, False), (0, True), (1, False), (0, False)]
        for i, slice_simulator in enumerate(self.patgen.slices):
            trigger_control = patgen_register.TRIGGER_CONTROL()
            trigger_control.domain_id = domain_id_master_list[i][0]
            trigger_control.domain_master = int(domain_id_master_list[i][1])
            slice_simulator.registers.update({trigger_control.ADDR: trigger_control.value})
            slice_simulator.running_pattern = True
        self.patgen.trigger_down(data)
        last_seen_trigger = self.patgen.slices[0].registers[0x4C]
        self.assertEqual(trigger_utility.get_propagate_trigger(), last_seen_trigger)
        for i in [0, 2, 4]:
            flags = patgen_register.FLAGS(self.patgen.slices[i].registers[0x178])
            self.assertTrue(flags.Domain_Trigger)
        for i in [1, 3]:
            flags = patgen_register.FLAGS(self.patgen.slices[i].registers[0x178])
            self.assertFalse(flags.Domain_Trigger)

    def test_domain_propagate_trigger_down(self):
        trigger_type = TriggerType.DomainTriggerPropagate.value
        trigger_utility = TriggerUtility(Mock(), Mock())
        trigger_utility.trigger_type = trigger_type
        trigger_utility.target_domain = 0
        data = trigger_utility.construct_trigger_word()
        domain_id_master_list = [(0, False), (1, False), (0, True), (1, False), (0, False)]
        for i, slice_simulator in enumerate(self.patgen.slices):
            trigger_control = patgen_register.TRIGGER_CONTROL()
            trigger_control.domain_id = domain_id_master_list[i][0]
            trigger_control.domain_master = int(domain_id_master_list[i][1])
            slice_simulator.registers.update({trigger_control.ADDR: trigger_control.value})
            slice_simulator.running_pattern = True
        self.patgen.trigger_down(data)
        last_seen_trigger = self.patgen.slices[0].registers[0x4C]
        self.assertEqual(data, last_seen_trigger)
        for i in [0, 2, 4]:
            flags = patgen_register.FLAGS(self.patgen.slices[i].registers[0x178])
            self.assertTrue(flags.Domain_Trigger)
        for i in [1, 3]:
            flags = patgen_register.FLAGS(self.patgen.slices[i].registers[0x178])
            self.assertFalse(flags.Domain_Trigger)

    def test_domain_trigger_down_no_master(self):
        trigger_type = TriggerType.DomainTrigger.value
        trigger_utility = TriggerUtility(Mock(), Mock())
        trigger_utility.trigger_type = trigger_type
        trigger_utility.target_domain = 1
        data = trigger_utility.construct_trigger_word()
        print(f'targeted domain = {(data & 0xf00) >> 8}')
        domain_id_master_list = [(0, False), (1, False), (0, True), (1, False), (0, False)]
        for i, slice_simulator in enumerate(self.patgen.slices):
            trigger_control = patgen_register.TRIGGER_CONTROL()
            trigger_control.domain_id = domain_id_master_list[i][0]
            trigger_control.domain_master = int(domain_id_master_list[i][1])
            slice_simulator.registers.update({trigger_control.ADDR: trigger_control.value})
            slice_simulator.running_pattern = True
        self.patgen.trigger_down(data)
        last_seen_trigger = self.patgen.slices[0].registers[0x4C]
        self.assertEqual(data, last_seen_trigger)
        for i in [0, 2, 4]:
            flags = patgen_register.FLAGS(self.patgen.slices[i].registers[0x178])
            self.assertFalse(flags.Domain_Trigger)
        for i in [1, 3]:
            flags = patgen_register.FLAGS(self.patgen.slices[i].registers[0x178])
            self.assertFalse(flags.Domain_Trigger)

