# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import unittest

from Hbicc.testbench.assembler import HbiPatternAssembler


class DataVectorTests(unittest.TestCase):
    def test_data_field(self):
        # integers
        self.check_vector(data=0,                                     expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='`rand(0)`',                           expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data=0x29876543210FEDCBA9876543210,         expected=b'\x10\x32\x54\x76\x98\xBA\xDC\x7E\x21\x64\xA8\xEC\x30\x05\x00\x80')
        # self.check_vector(data=0o01234567012345670123456701234567012, expected=b'\x0A\xEE\x72\x0A\xEE\x72\x0A\x6E\xE5\x14\xDC\xE5\x14\x00\x00\x80')
        
        # '0v' words
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLH', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHL', expected=b'\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLL', expected=b'\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLL', expected=b'\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLX', expected=b'\x02\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLVL', expected=b'\x18\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLL0LL', expected=b'\x00\x01\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLL1LLL', expected=b'\x00\x0A\x20\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLELLLL', expected=b'\x00\x60\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLLLLLLLLLLLLHLLLKLLLLL', expected=b'\x00\x80\x03\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLL', expected=b'\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLL', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(data='0vHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x80')
        self.check_vector(data='0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH', expected=b'\x49\x92\x24\x49\x92\x24\x49\x12\x49\x92\x24\x49\x92\x00\x00\x80')
        self.check_vector(data='0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', expected=b'\x92\x24\x49\x92\x24\x49\x92\x24\x92\x24\x49\x92\x24\x01\x00\x80')
        self.check_vector(data='0vVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV', expected=b'\xDB\xB6\x6D\xDB\xB6\x6D\xDB\x36\xDB\xB6\x6D\xDB\xB6\x01\x00\x80')
        self.check_vector(data='0v00000000000000000000000000000000000', expected=b'\x24\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80')
        self.check_vector(data='0v11111111111111111111111111111111111', expected=b'\x6D\xDB\xB6\x6D\xDB\xB6\x6D\x5B\x6D\xDB\xB6\x6D\xDB\x02\x00\x80')
        self.check_vector(data='0vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', expected=b'\xB6\x6D\xDB\xB6\x6D\xDB\xB6\x6D\xB6\x6D\xDB\xB6\x6D\x03\x00\x80')
        self.check_vector(data='0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK', expected=b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x7F\xFF\xFF\xFF\xFF\xFF\x03\x00\x80')
        
    def test_local_repeat(self):
        self.check_vector(lrpt=0, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(lrpt='`rand(0)`',expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(lrpt=1, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x80')
        self.check_vector(lrpt=31, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xF8\x80')
    
    def test_mtv(self):
        self.check_vector(mtv=0, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(mtv=1, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x81')
        self.check_vector(mtv=2, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x82')
        self.check_vector(mtv=4, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x84')
        self.check_vector(mtv=8, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x88')
        self.check_vector(mtv=16, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x90')
        self.check_vector(mtv=31, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x9F')
    
    def test_ctv(self):
        self.check_vector(ctv=0, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80')
        self.check_vector(ctv=1, expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xa0')
    
    def check_vector(self, expected, ctv=None, mtv=None, lrpt=None, data=None):
        pa = HbiPatternAssembler()
        vector = 'V'
        if ctv is not None:
            vector += f' ctv={ctv}'
        if mtv is not None:
            vector += f' mtv={mtv}'
        if lrpt is not None:
            vector += f' lrpt={lrpt}'
        if data is not None:
            vector += f' data={data}'
        # print(vector)
        pa.LoadString(vector)
        if pa.CachedObj() != expected + b'\x00'*240:
            print('')
            print(f'expected: {int.from_bytes(expected[:16], byteorder="little"):032X}')
            print(f'received: {int.from_bytes(pa.CachedObj()[:16], byteorder="little"):032X}')
        self.assertEqual(pa.CachedObj(), expected + b'\x00'*240)
    

class InstructionVectorOpsTests(unittest.TestCase):
    def test_log_ops(self):
        self.check_op(vptype='VPLOG', vpop='LOG1_I', imm='0x80000001',     expected=b'\x01\x00\x00\x80\x00\x00\x00\x88\x20\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOG', vpop='LOG2_I', imm='0xA',            expected=b'\x0A\x00\x00\x00\x00\x00\x00\x90\x20\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOG', vpop='PATID_I', imm='0xB',           expected=b'\x0B\x00\x00\x00\x00\x00\x00\xA0\x20\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOG', vpop='LOG1_RA', reg='0xC',           expected=b'\x00\x00\x00\x00\x0C\x00\x00\xC8\x20\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOG', vpop='LOG2_RA', reg='0xD',           expected=b'\x00\x00\x00\x00\x0D\x00\x00\xD0\x20\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOG', vpop='PATID_RA', reg='0xE',          expected=b'\x00\x00\x00\x00\x0E\x00\x00\xE0\x20\x00\x00\x00\x00\x00\x00\x60')
        
    def test_local_ops(self):
        self.check_op(vptype='VPLOCAL', vpop='REPEAT_I', imm='0x11111111', expected=b'\x11\x11\x11\x11\x00\x00\x00\x08\x21\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOCAL', vpop='END_I',    imm='0x22222222', expected=b'\x22\x22\x22\x22\x00\x00\x00\x10\x21\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOCAL', vpop='REPEAT_RA', reg='1',         expected=b'\x00\x00\x00\x00\x01\x00\x00\x48\x21\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPLOCAL', vpop='END_RA',    reg='31',        expected=b'\x00\x00\x00\x00\x1F\x00\x00\x50\x21\x00\x00\x00\x00\x00\x00\x60')
        
    def test_other_ops(self):
        self.check_op(vptype='VPOTHER', vpop='TRIGGERSIB',                 expected=b'\x00\x00\x00\x00\x00\x00\x00\x08\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='BLKFAIL_I', imm='0xC',       expected=b'\x0C\x00\x00\x00\x00\x00\x00\x10\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='DRAINFIFO_I', imm='0xD',     expected=b'\x0D\x00\x00\x00\x00\x00\x00\x18\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='EDGECNTR_I', imm='0xE',      expected=b'\x0E\x00\x00\x00\x00\x00\x00\x20\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='CLRSTICKY',                  expected=b'\x00\x00\x00\x00\x00\x00\x00\x28\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='CAPTURE_I', imm='0xF',       expected=b'\x0F\x00\x00\x00\x00\x00\x00\x40\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='RSTCYCLCNT',                 expected=b'\x00\x00\x00\x00\x00\x00\x00\x60\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='RSTCAPMEM',                  expected=b'\x00\x00\x00\x00\x00\x00\x00\x50\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='RSTFAILCNT',                 expected=b'\x00\x00\x00\x00\x00\x00\x00\x48\x22\x00\x00\x00\x00\x00\x00\x60')
        self.check_op(vptype='VPOTHER', vpop='DRAINFIFO_R', reg='0x10',    expected=b'\x00\x00\x00\x00\x10\x00\x00\x58\x22\x00\x00\x00\x00\x00\x00\x60')
    
    def check_op(self, expected, vptype, vpop, reg=None, imm=None):
        pa = HbiPatternAssembler()
        vector = f'I optype=VECTOR, vptype={vptype}, vpop={vpop}'
        if imm is not None:
            vector += f', imm={imm}'
        if reg is not None:
            vector += f', regA={reg}'
        # print(vector)
        pa.LoadString(vector)
        if pa.CachedObj() != expected + b'\x00'*240:
            print('')
            print(f'expected: {int.from_bytes(expected[:16], byteorder="little"):032X}')
            print(f'received: {int.from_bytes(pa.CachedObj()[:16], byteorder="little"):032X}')
        self.assertEqual(pa.CachedObj(), expected + b'\x00'*240)
        
        
class PinStateWordTests(unittest.TestCase):
    def test_mask(self):
        self.check_op(stype='MASK',            data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x52')
        self.check_op(stype='MASK',            data='0b00000000000000000000000000000000001', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x52')
        self.check_op(stype='MASK',            data='0b10000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x52')
        self.check_op(stype='MASK',            data='0b11111111111111111111111111111111111', expected=b'\xFF\xFF\xFF\xFF\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x52')
        
    def test_unmask(self):
        self.check_op(stype='UNMASK',          data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x54')
        self.check_op(stype='UNMASK',          data='0b00000000000000000000000000000000001', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x54')
        self.check_op(stype='UNMASK',          data='0b10000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x54')
        self.check_op(stype='UNMASK',          data='0b11111111111111111111111111111111111', expected=b'\xFF\xFF\xFF\xFF\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x54')
        
    def test_mask_apply(self):
        self.check_op(stype='MASK_APPLY',      data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x56')
        self.check_op(stype='MASK_APPLY',      data='0b00000000000000000000000000000000001', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x56')
        self.check_op(stype='MASK_APPLY',      data='0b10000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x56')
        self.check_op(stype='MASK_APPLY',      data='0b11111111111111111111111111111111111', expected=b'\xFF\xFF\xFF\xFF\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x56')
        
    def test_enabled_clock_reset(self):
        self.check_op(stype='EC_RESET',        data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x58')
        self.check_op(stype='EC_RESET',        data='0b00000000000000000000000000000000001', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x58')
        self.check_op(stype='EC_RESET',        data='0b00000000000000000000000000000000010', expected=b'\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x58')
        self.check_op(stype='EC_RESET',        data='0b11111111111111111111111111111111111', expected=b'\xFF\xFF\xFF\xFF\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x58')
        
    def test_dut_serialization_control(self):
        self.check_op(stype='DUT_SERIAL', channel_set=0, data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=1, data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=2, data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=3, data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=31, data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xF8\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set='ALL_CHANNEL_SETS', data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5B')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0o00000000000000000000000000000000002', expected=b'\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0o00000000000000000000000000000000020', expected=b'\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0o00000000000000200000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0o00000000000002000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0o20000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', expected=b'\x92\x24\x49\x92\x24\x49\x92\x24\x92\x24\x49\x92\x24\x01\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0vFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expected=b'\xDB\xB6\x6D\xDB\xB6\x6D\xDB\x36\xDB\xB6\x6D\xDB\xB6\x01\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0v00000000000000000000000000000000000', expected=b'\x24\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0v11111111111111111111111111111111111', expected=b'\x6D\xDB\xB6\x6D\xDB\xB6\x6D\x5B\x6D\xDB\xB6\x6D\xDB\x02\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0vEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', expected=b'\xB6\x6D\xDB\xB6\x6D\xDB\xB6\x6D\xB6\x6D\xDB\xB6\x6D\x03\x20\x5A')
        self.check_op(stype='DUT_SERIAL', channel_set=4, data='0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK', expected=b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x7F\xFF\xFF\xFF\xFF\xFF\x03\x20\x5A')
        
    def test_capture_this_pin(self):
        self.check_op(stype='CAPTURE_PIN_MASK', data='0b00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5C')
        self.check_op(stype='CAPTURE_PIN_MASK', data='0b00000000000000000000000000000000001', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5C')
        self.check_op(stype='CAPTURE_PIN_MASK', data='0b00000000000000000000000000000000010', expected=b'\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5C')
        self.check_op(stype='CAPTURE_PIN_MASK', data='0b10000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5C')
        
    def test_active_channels(self):
        self.check_op(stype='ACTIVE_CHANNELS', data='0o00000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o00000000000000000000000000000000001', expected=b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o00000000000000000000000000000000010', expected=b'\x08\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o00000000000000100000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o00000000000001000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o10000000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o11111111111111111111111111111111111', expected=b'\x49\x92\x24\x49\x92\x24\x49\x12\x49\x92\x24\x49\x92\x00\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o22222222222222222222222222222222222', expected=b'\x92\x24\x49\x92\x24\x49\x92\x24\x92\x24\x49\x92\x24\x01\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o33333333333333333333333333333333333', expected=b'\xDB\xB6\x6D\xDB\xB6\x6D\xDB\x36\xDB\xB6\x6D\xDB\xB6\x01\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o44444444444444444444444444444444444', expected=b'\x24\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o55555555555555555555555555555555555', expected=b'\x6D\xDB\xB6\x6D\xDB\xB6\x6D\x5B\x6D\xDB\xB6\x6D\xDB\x02\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o66666666666666666666666666666666666', expected=b'\xB6\x6D\xDB\xB6\x6D\xDB\xB6\x6D\xB6\x6D\xDB\xB6\x6D\x03\x00\x5E')
        self.check_op(stype='ACTIVE_CHANNELS', data='0o77777777777777777777777777777777777', expected=b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x7F\xFF\xFF\xFF\xFF\xFF\x03\x00\x5E')
    
    def check_op(self, expected, stype, data, channel_set=None):
        pa = HbiPatternAssembler()
        vector = f'S stype={stype}'
        if channel_set is not None:
            vector += f', channel_set={channel_set}'
        vector += f', data={data}'
        # print(vector)
        pa.LoadString(vector)
        if pa.CachedObj() != expected + b'\x00'*240:
            print('')
            print(f'expected: {int.from_bytes(expected[:16], byteorder="little"):032X}')
            print(f'received: {int.from_bytes(pa.CachedObj()[:16], byteorder="little"):032X}')
        self.assertEqual(pa.CachedObj(), expected + b'\x00'*240)


class MetadataWordTests(unittest.TestCase):
    def test_data(self):
        self.check_op(data='0x0000000000000000000000000000000', expected=b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x70')
        self.check_op(data='0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expected=b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x7F')
        self.check_op(data='0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', expected=b'\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x77')
        
    def check_op(self, expected, data):
        pa = HbiPatternAssembler()
        vector = f'M data={data}'
        # print(vector)
        pa.LoadString(vector)
        if pa.CachedObj() != expected + b'\x00'*240:
            print('')
            print(f'expected: {int.from_bytes(expected[:16], byteorder="little"):032X}')
            print(f'received: {int.from_bytes(pa.CachedObj()[:16], byteorder="little"):032X}')
        self.assertEqual(pa.CachedObj(), expected + b'\x00'*240)

class GeneratorTests(unittest.TestCase):
    def test_ConstantVectors(self):
        self.check_equivalence('ConstantVectors length=1, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL',
                                                       'V data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL')
        self.check_equivalence('ConstantVectors length=1, data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH',
                                                       'V data=0vHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
        self.check_equivalence('ConstantVectors length=1, ctv=1, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
                                                       'V ctv=1, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
        self.check_equivalence('ConstantVectors length=1, mtv=0x1F, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK',
                                                       'V mtv=0x1F, data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK')
        self.check_equivalence('ConstantVectors length=1, lrpt=31, data=0v11111111111111111111111111111111111',
                                                       'V lrpt=31, data=0v11111111111111111111111111111111111')
    
    def check_equivalence(self, pattern1, pattern2):
        p1 = HbiPatternAssembler()
        p2 = HbiPatternAssembler()
        p1.LoadString(pattern1)
        p2.LoadString(pattern2)
        obj1 = p1.CachedObj()
        obj2 = p2.CachedObj()
        self.assertEqual(obj1, obj2)