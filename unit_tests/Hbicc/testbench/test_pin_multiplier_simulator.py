# INTEL CONFIDENTIAL
# Copyright 2020 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import enum
import random
import unittest

from Common.fval import skip
from Hbicc.testbench.pin_multiplier_simulator import PinMultiplier, PinMultiplierSlice, ChannelSetPins, \
                                              EnabledClockPins, LoopBackPin
from Hbicc.instrument.data_structures import DcTriggerQueueCommand, \
                                             ChannelSet, CHANNEL_SET_DUTS
import Hbicc.instrument.pin_multiplier_register as pin_multiplier_register

class TestPinMultiplierSimulatorScratchRegisters(unittest.TestCase):
    REGISTER_SIZE_IN_BYTES = 4
    NUMBER_OF_BARS = 6

    def setUp(self):
        self.pm = PinMultiplier(slot=0, index=0, system=None)

    def test_scratch_registers(self):
        test_data = self._create_scratch_register_test_data()
        self._write_scratch_registers_in_random_order(test_data)
        self._read_and_check_scratch_registers_in_random_order(test_data)

    def _create_scratch_register_test_data(self):
        SCRATCH_REGISTER_BASE = 0x0000
        NUMBER_OF_SCRATCH_REGISTERS_PER_BAR = 8
        test_data = []
        for bar in range(self.NUMBER_OF_BARS):
            start = SCRATCH_REGISTER_BASE
            stop = self.REGISTER_SIZE_IN_BYTES * NUMBER_OF_SCRATCH_REGISTERS_PER_BAR
            step = self.REGISTER_SIZE_IN_BYTES
            for offset in range(start, stop, step):
                t = (bar, offset, random.getrandbits(32))
                test_data.append(t)
        return test_data

    def _write_scratch_registers_in_random_order(self, test_data):
        random.shuffle(test_data)
        for bar, offset, write_data in test_data:
            self.pm.bar_write(bar, offset, write_data)

    def _read_and_check_scratch_registers_in_random_order(self, test_data):
        random.shuffle(test_data)
        for bar, offset, write_data in test_data:
            read_data = self.pm.bar_read(bar, offset)
            self.assertEqual(read_data, write_data)


class TestPinMultiplierSimulatorForcePins(unittest.TestCase):
    def setUp(self):
        self.pm = PinMultiplier(slot=0, index=0, system=None)

    def test_force_pins_all_duts(self):
        pin_states = {}
        for slice in range(5):
            for channel_set in range(4):
                pin_control = random.getrandbits(70) & 0x155555555555555555
                self._set_pin_state(slice, channel_set, pin_control)
                pin_states[(slice, channel_set)] = self._expected_pin_state(pin_control)

        new_pin_states = self._all_pin_states()
        for slice in range(5):
            for channel_set in range(4):
                expected = pin_states[(slice, channel_set)]
                observed = new_pin_states[(slice, channel_set)]
                if expected != observed:
                    self.fail(f'''At slice {slice}, channel set {channel_set} 
                                  expected = 0x{expected:09X}, 
                                  observed = 0x{observed:09X}''')

    def test_tristate_pins_loopback(self):
        pin_states = {}
        channels_per_slice = 35
        number_of_slices = 5
        FORCE_LOW = 0
        FORCE_HIGH = 1
        TRISTATE = 2
        cross_slice_drive = [FORCE_LOW, FORCE_HIGH]
        random.shuffle(cross_slice_drive)
        for channel_set in range(1):
            pin_control = []
            expected_state = 0
            for i in range(0, channels_per_slice * number_of_slices, 2):
                if i in [34, 104]:
                    drive = cross_slice_drive.pop()
                else:
                    drive = random.choice([FORCE_LOW, FORCE_HIGH])
                if i == 174:
                    pin_pair = [drive, drive] # no loopback partner
                else:
                    pin_pair = [drive, TRISTATE]
                random.shuffle(pin_pair)
                pin_control.extend(pin_pair)
                expected_state |= (drive + 2 * drive) << i
            for slice in range(5):
                pin_control_word = self._pin_control_word_from_list(slice, channel_set, pin_control)
                self._set_pin_state(slice, channel_set, pin_control_word)
                slice_expected = (expected_state >> (35*slice)) & 0x7FFFFFFFF
                pin_states[(slice, channel_set)] = slice_expected

        new_pin_states = self._all_pin_states()
        for slice in range(5):
            for channel_set in range(1):
                expected = pin_states[(slice, channel_set)]
                observed = new_pin_states[(slice, channel_set)]
                if expected != observed:
                    self.fail(f'''At slice {slice}, channel set {channel_set} 
                                  expected = 0x{expected:09X}, 
                                  observed = 0x{observed:09X}''')

    def _all_pin_states(self):
        pin_states = {}
        for channel_set in range(4):
            for slice in range(5):
                pin_states[(slice, channel_set)] = self._raw_pin_state(slice, channel_set)
        return pin_states

    def _expected_pin_state(self, pin_control):
        state = 0
        for i in range(35):
            state |= (pin_control & 1) << i
            pin_control = pin_control >> 2
        return state

    def _expected_pin_state_with_loopback(self, pin_control, slice):
        if slice & 1:
            state = pin_control & 1
            pin_control = pin_control >> 4
            for i in range(2, 35, 2):
                state |= (pin_control & 1) << i
                state |= (pin_control & 1) << (i - 1)
                pin_control = pin_control >> 4
        else:
            state = 0
            for i in range(0, 35, 2):
                state |= (pin_control & 1) << i
                state |= (pin_control & 1) << (i + 1)
                pin_control = pin_control >> 4
        return state & 0x7FFFFFFFF

    def _set_and_check_pin_state(self, slice, channel_set, pin_control, expected_pin_state):
        self._set_pin_state(slice, channel_set, pin_control)
        pin_state = self._raw_pin_state(slice=slice, dut=0,)
        self.assertEqual(pin_state, expected_pin_state)

    def _pin_control_word_from_list(self, slice, channel_set, pin_control_list):
        pin_control = 0
        for pin, control in enumerate(pin_control_list[slice*35:slice*35+35]):
            pin_control |= control << (pin * 2)
        return pin_control

    def _set_pin_state(self, slice, channel_set, pin_control):
        tq = DcTriggerQueueCommand()
        tq.channel_set = channel_set
        tq.command_or_interface = self._interface_target(slice, pins='lower')
        tq.payload = pin_control & 0xFFFFFFFFFFFF
        self.pm.dc_trigger(tq.to_int())
        tq.channel_set = channel_set
        tq.command_or_interface = self._interface_target(slice, pins='upper')
        tq.payload = pin_control >> 48
        self.pm.dc_trigger(tq.to_int())


    def _raw_pin_state(self, slice, dut):
        return (self.pm.bar_read(slice + 1, 0xA4 + dut * 8) << 32) | self.pm.bar_read(slice + 1, 0xA0 + dut * 8)

    def _interface_target(self, slice, pins):
        if pins == 'lower':
            base = 6
        else:
            base = 7
        return base + 2 * slice


class TestPinMultiplierPatternWord(unittest.TestCase):
    def test_single_vector_no_fails(self):
        self.set_up()
        vector = b'\x24\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80'
        expected_fails = {x:0 for x in range(35)}
        fail_counts = self.process_vector(vector)
        self.assertEqual(fail_counts, expected_fails)


    def test_single_vector_one_fails(self):
        self.set_up()
        vector = b'\x21\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80'
        expected_fails = {x:0 for x in range(35)}
        expected_fails[0] = 1
        fail_counts = self.process_vector(vector)
        self.assertEqual(fail_counts, expected_fails)

    def test_double_vector_two_fails(self):
        self.set_up()
        vector = b'\x21\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80'
        expected_fails = {x:0 for x in range(35)}
        expected_fails[0] = 2
        self.process_vector(vector)
        fail_counts=self.process_vector(vector)
        self.assertEqual(fail_counts, expected_fails)

    def test_double_vector_with_keep_pin(self):
        self.set_up()
        vector = b'\x21\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80'
        expected_fails = {x:0 for x in range(35)}
        expected_fails[0] = 2
        self.process_vector(vector)
        vector = b'\x3F\x49\x92\x24\x49\x92\x24\x49\x24\x49\x92\x24\x49\x02\x00\x80'
        fail_counts=self.process_vector(vector)
        self.assertEqual(fail_counts, expected_fails)

    def process_vector(self, vector):
        low_half = int.from_bytes(vector[0:8], byteorder='little')
        high_half = int.from_bytes(vector[8:16], byteorder='little')
        self.channel_set_pins.pattern_word(low_half, high_half)
        return self.channel_set_pins._fail_counts


    def set_up(self):
        self.channel_set_pins = ChannelSetPins(0,0,0,True)
        self.channel_set_pins._active_channels=range(35)
        self.channel_set_pins._active_lower_channels = [x for x in range(35) if x < 21]
        self.channel_set_pins._active_upper_channels = [x for x in range(35) if x > 20]
        self.channel_set_pins._broadcast = True

class TestPinMultiplierDelayStack(unittest.TestCase):
    def test_calculate_EC_subcycle(self):
        self.set_up()
        config1 = pin_multiplier_register.PER_CHANNEL_CONFIG1()
        RxDelay = 67
        config1.TotalCycleDelayTxToDut = 0
        config1.SubCycleDelayRxFromDut = RxDelay % 8
        config1.TesterCycleDelayRxFromDut = RxDelay // 8
        subcycle=self.pin_multiplier_slice.calculate_subcycle(config1.value)
        self.assertEqual(subcycle, 4)
        RxDelay = 65
        config1.TotalCycleDelayTxToDut = 0
        config1.SubCycleDelayRxFromDut = RxDelay % 8
        config1.TesterCycleDelayRxFromDut = RxDelay // 8
        subcycle=self.pin_multiplier_slice.calculate_subcycle(config1.value)
        self.assertEqual(subcycle, 2)
        RxDelay = 67+8
        config1.TotalCycleDelayTxToDut = 8
        config1.SubCycleDelayRxFromDut = RxDelay % 8
        config1.TesterCycleDelayRxFromDut = RxDelay // 8
        subcycle=self.pin_multiplier_slice.calculate_subcycle(config1.value)
        self.assertEqual(subcycle, 4)

    def set_up(self):
        self.pin_multiplier_slice = PinMultiplierSlice(0,0,0,0)

class TestPinMultiplierEnabledClockPins(unittest.TestCase):
    def test_resolve_EC_for_ratio_2(self):
        self.set_up()
        self.config2.EnableClkTesterCycleHigh=1
        self.config2.EnableClkTesterCycleLow=1
        self.enabled_clock_pin.reset(self.config2.value, self.config3.value)
        ECvalue=self.enabled_clock_pin.resolve_EC(2)
        self.assertEqual(ECvalue, 0)
        ECvalue=self.enabled_clock_pin.resolve_EC(3)
        self.assertEqual(ECvalue, 1)

    def test_resolve_FRC_for_ratio_2(self):
        self.set_up()
        self.config3.FRCMode=1
        self.config3.FRCDivider=0
        self.enabled_clock_pin.reset(self.config2.value, self.config3.value)
        ECvalue=self.enabled_clock_pin.resolve_EC(2)
        self.assertEqual(ECvalue, 0)
        ECvalue=self.enabled_clock_pin.resolve_EC(3)
        self.assertEqual(ECvalue, 1)

    def test_resolve_EC_for_ratio_3(self):
        self.set_up()
        self.config2.EnableClkTesterCycleHigh=2
        self.config2.EnableClkTesterCycleLow=1
        self.config2.EcAddHalfCycle=1
        self.enabled_clock_pin.reset(self.config2.value, self.config3.value)
        self.enabled_clock_pin.set_EC_subcycle(2)
        ECvalue=self.enabled_clock_pin.resolve_EC(1)
        self.assertEqual(ECvalue, 0)
        ECvalue=self.enabled_clock_pin.resolve_EC(2)
        self.assertEqual(ECvalue, 1)

        self.enabled_clock_pin.set_EC_subcycle(6)
        ECvalue=self.enabled_clock_pin.resolve_EC(1)
        self.assertEqual(ECvalue, 1)
        ECvalue=self.enabled_clock_pin.resolve_EC(2)
        self.assertEqual(ECvalue, 1)
        self.config2.EcFirstEdge=1
        self.config2.EnableClkTesterCycleHigh = 1
        self.config2.EnableClkTesterCycleLow = 2
        self.enabled_clock_pin.reset(self.config2.value, self.config3.value)
        self.enabled_clock_pin.set_EC_subcycle(2)
        ECvalue=self.enabled_clock_pin.resolve_EC(1)
        self.assertEqual(ECvalue, 1)
        ECvalue=self.enabled_clock_pin.resolve_EC(2)
        self.assertEqual(ECvalue, 0)

        self.enabled_clock_pin.set_EC_subcycle(6)
        ECvalue=self.enabled_clock_pin.resolve_EC(1)
        self.assertEqual(ECvalue, 0)
        ECvalue=self.enabled_clock_pin.resolve_EC(2)
        self.assertEqual(ECvalue, 0)

    def set_up(self):
        self.enabled_clock_pin = EnabledClockPins(0)
        self.config2 = pin_multiplier_register.PER_CHANNEL_CONFIG2()
        self.config3 = pin_multiplier_register.PER_CHANNEL_CONFIG3()

class TestLoopBackPair(unittest.TestCase):

    def test_loopbackpair_setup(self):
        self.populate_config1(rx_delay=83, tx_delay=8)
        self.loopbackpair=LoopBackPin()
        self.loopbackpair.config_loopback_buffers(self.config1.value, self.tiu_delay)
        self.assertEqual(self.loopbackpair.delay_mismatch, 8)
        self.assertEqual(len(self.loopbackpair.compare_buffer), 1)
        self.assertEqual(len(self.loopbackpair.drive_buffer), 0)
        self.populate_config1(rx_delay=59, tx_delay=8)
        self.loopbackpair=LoopBackPin()
        self.loopbackpair.config_loopback_buffers(self.config1.value, self.tiu_delay)
        self.assertEqual(self.loopbackpair.delay_mismatch, -16)
        self.assertEqual(len(self.loopbackpair.compare_buffer), 0)
        self.assertEqual(len(self.loopbackpair.drive_buffer), 2)
        self.populate_config1(rx_delay=73, tx_delay=8)
        self.loopbackpair=LoopBackPin()
        self.loopbackpair.config_loopback_buffers(self.config1.value, self.tiu_delay)
        self.assertEqual(self.loopbackpair.delay_mismatch, -2)
        self.assertEqual(len(self.loopbackpair.compare_buffer), 0)
        self.assertEqual(len(self.loopbackpair.drive_buffer), 0)

    @skip(f'Failing due to changes made in pin_multiplier_sim. ')
    def test_simple_loopback(self):
        self.populate_config1(rx_delay=67, tx_delay=8)
        self.loopbackchannel=LoopBackPin()
        self.loopbackchannel.config_loopback_buffers(self.config1.value, self.tiu_delay)
        self.loopbackpair=LoopBackPin()
        self.loopbackpair.config_loopback_buffers(self.config1.value, self.tiu_delay)
        channel0_data=[4,4,4,0,0,1,1,4,4,4]
        channel1_data=[4,4,4,4,5,5,5,5,4,4]
        fail0=0
        fail1=0
        for pin0, pin1 in zip(channel0_data, channel1_data):
            self.loopbackchannel.update_values(pin0)
            self.loopbackpair.update_values(pin1)

            pin0d = self.loopbackchannel.drive_buffer[self.loopbackchannel.drive_index]
            self.loopbackchannel.drive_index += 1
            pin0c = self.loopbackchannel.compare_buffer[self.loopbackchannel.compare_index]
            self.loopbackchannel.compare_index += 1

            pin1d = self.loopbackchannel.drive_buffer[self.loopbackchannel.drive_index]
            self.loopbackchannel.drive_index += 1
            pin1c = self.loopbackpair.compare_buffer[self.loopbackpair.compare_index]
            self.loopbackpair.compare_index += 1
            if pin0c&6==0:
                drivestate=pin1d
                if (pin1d&3)!=(pin0c&3):
                    fail0=fail0+1
            if pin1c&6==0:
                if (pin0d&3)!=(pin1c&3):
                    fail1=fail1+1
        self.assertEqual(fail0,0)
        self.assertEqual(fail1,0)




    def populate_config1(self, rx_delay, tx_delay):
        self.config1.TotalCycleDelayTxToDut=tx_delay
        self.config1.SubCycleDelayRxFromDut=rx_delay % 8
        self.config1.TesterCycleDelayRxFromDut=rx_delay // 8

    def setUp(self):
        self.config1 = pin_multiplier_register.PER_CHANNEL_CONFIG1()
        self.tiu_delay =  67