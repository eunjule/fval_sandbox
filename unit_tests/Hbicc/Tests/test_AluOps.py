################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.AluOps import Functional

time.sleep = lambda x: None


class AluOpsTests(HbiccTestCase):
    def check_for_message(self, level, message, calls):
        for args, kwargs in calls:
            self.args = args
            log_level, log_message = self.args
            if level == log_level and message in log_message:
                break
        else:
            call_strings = '\n'.join([str(x) for x in calls])
            self.fail(f'"{level} ,{message}" not in\n{call_strings}')

    def test_RandomRegisterImmediateToRegisterTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.RandomRegisterImmediateToRegisterTest()
        message = 'ALU Operation SUB completed successfully.'
        self.check_for_message('info', message, functional.Log.call_args_list)

    def test_RandomRegisterImmediateToRegisterTest_fail(self):
        self.set_patgen_register(value=0xFE000000)
        functional = self.get_functional()
        functional.RandomRegisterImmediateToRegisterTest()
        message = 'Operation failed'
        self.check_for_message('error', message, functional.Log.call_args_list)
        self.set_patgen_register(value=None)

    def get_functional(self):
        for slice in self.simulator.patgen.slices:
            slice.Log = Mock()
        functional = Functional()
        functional.Log = Mock()
        functional.setUp(tester=self.tester)
        functional.pattern_helper.is_regression = False
        functional.alu_ops = ['SUB']
        functional.pattern_helper._set_mtv_mask = Mock()
        functional.pattern_helper.reset_pm_fifo_to_flush_content = Mock()
        functional.pattern_helper.set_pin_fixed_drive_state = lambda psdb: None
        functional.pattern_helper.set_drive_low_on_all_active_slice_channel_sets = lambda psdb: None
        functional.env.hbicc.check_pin_states = Mock()
        functional.env.hbicc.ring_multiplier.create_trigger_queue_command_record = Mock()
        functional.pattern_helper.check_vref_voltage = Mock()
        # functional.helper.compare_top_bottom_rm = Mock()
        # functional.helper.is_regression = False
        for slice in functional.env.hbicc.model.patgen.slices:
            slice.Log = Mock()
        return functional


    def set_patgen_register(self, value, slices=range(0, 5), reg=0x178):
        for slice in slices:
            self.simulator.patgen.slices[slice].fail_registers[reg] = value

