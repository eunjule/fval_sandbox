################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
from unittest.mock import patch, Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.instrument.psdb import Psdb
from Hbicc.instrument.hbicc import Hbicc
from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.ringmultiplier import RingMultiplier
from Hbicc.instrument.pinmultiplier import PinMultiplier
from Hbicc.instrument.patgen_register import SCRATCH_REG as PatGen_SCRATCH_REG
from Hbicc.Tests.ScratchRegisters import Diagnostics

ITERATIONS = 20
REGCOUNT = 8
FAILURES = 20

class ScratchRegistersTests(HbiccTestCase):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedBasicPatgenScratchRegisterTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected = random.randrange(0, 0xFFFFFFFF)
            expected_list = [expected] * ITERATIONS * REGCOUNT
            calls = self.get_list_of_registers(ITERATIONS, expected, REGCOUNT)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.pat_gen.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPatgenScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.pat_gen.Log.call_args_list[0][0]
            self.assertIn('PATGEN: Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(ITERATIONS-1), message, message)

    def test_DirectedBasicPatgenScratchRegisterTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected_list = [random.randrange(0, 0xFFFFFFFF) for x in range(ITERATIONS*REGCOUNT)]
            calls = self.failing_calls(expected_list)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.pat_gen.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPatgenScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.pat_gen.Log.call_args_list[0][0]
            self.assertIn('PATGEN: Scratchpad writes failed', message, message)

    def test_DirectedBasicRingMultiplierScratchRegisterTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected = random.randrange(0, 0xFFFFFFFF)
            expected_list = [expected] * ITERATIONS * REGCOUNT
            calls = self.get_list_of_registers(ITERATIONS, expected, REGCOUNT)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.ring_multiplier.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicRingMultiplierScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.ring_multiplier.Log.call_args_list[0][0]
            self.assertIn('RINGMULTIPLIER: Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(ITERATIONS-1), message, message)

    def test_DirectedBasicRingMultiplierScratchRegisterTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected_list = [random.randrange(0, 0xFFFFFFFF) for x in range(ITERATIONS*REGCOUNT)]
            calls = self.failing_calls(expected_list)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.ring_multiplier.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicRingMultiplierScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.ring_multiplier.Log.call_args_list[0][0]
            self.assertIn('RINGMULTIPLIER: Scratchpad writes failed', message, message)

    def test_DirectedBasicPSDB0PinMultiplier0ScratchRegisterTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected = random.randrange(0, 0xFFFFFFFF)
            expected_list = [expected] * ITERATIONS * REGCOUNT
            calls = self.get_list_of_registers(ITERATIONS, expected, REGCOUNT)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_0_pin_0.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB0PinMultiplier0ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_0: Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(ITERATIONS-1), message, message)

    def test_DirectedBasicPSDB0PinMultiplier0ScratchRegisterTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected_list = [random.randrange(0, 0xFFFFFFFF) for x in range(ITERATIONS*REGCOUNT)]
            calls = self.failing_calls(expected_list)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)
  
            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_0_pin_0.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB0PinMultiplier0ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_0: Scratchpad writes failed', message, message)

    def test_DirectedBasicPSDB0PinMultiplier1ScratchRegisterTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected = random.randrange(0, 0xFFFFFFFF)
            expected_list = [expected] * ITERATIONS * REGCOUNT
            calls = self.get_list_of_registers(ITERATIONS, expected, REGCOUNT)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_0_pin_1.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB0PinMultiplier1ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_1: Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(ITERATIONS-1), message, message)

    def test_DirectedBasicPSDB0PinMultiplier1ScratchRegisterTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected_list = [random.randrange(0, 0xFFFFFFFF) for x in range(ITERATIONS*REGCOUNT)]
            calls = self.failing_calls(expected_list)
            
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_0_pin_1.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB0PinMultiplier1ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_1: Scratchpad writes failed', message, message)

    def test_DirectedBasicPSDB1PinMultiplier0ScratchRegisterTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected = random.randrange(0, 0xFFFFFFFF)
            expected_list = [expected] * ITERATIONS * REGCOUNT
            calls = self.get_list_of_registers(ITERATIONS, expected, REGCOUNT)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_1_pin_0.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB1PinMultiplier0ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_0: Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(ITERATIONS-1), message, message)

    def test_DirectedBasicPSDB1PinMultiplier0ScratchRegisterTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected_list = [random.randrange(0, 0xFFFFFFFF) for x in range(ITERATIONS*REGCOUNT)]
            calls = self.failing_calls(expected_list)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_1_pin_0.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB1PinMultiplier0ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_0: Scratchpad writes failed', message, message)

    def test_DirectedBasicPSDB1PinMultiplier1ScratchRegisterTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected = random.randrange(0, 0xFFFFFFFF)
            expected_list = [expected] * ITERATIONS * REGCOUNT
            calls = self.get_list_of_registers(ITERATIONS, expected, REGCOUNT)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_1_pin_1.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB1PinMultiplier1ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_1: Scratchpad Registers(0-7) writes were successful for {} iterations.'.format(ITERATIONS-1), message, message)

    def test_DirectedBasicPSDB1PinMultiplier1ScratchRegisterTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            expected_list = [random.randrange(0, 0xFFFFFFFF) for x in range(ITERATIONS*REGCOUNT)]
            calls = self.failing_calls(expected_list)

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            self.diagnostics.write_random_value_to_scratch_register = Mock(side_effect=expected_list)
            self.diagnostics.env.hbicc.psdb_1_pin_1.registers.SCRATCH_REG = Mock(side_effect=calls)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedBasicPSDB1PinMultiplier1ScratchRegisterTest(ITERATIONS-1)
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_1: Scratchpad writes failed', message, message)

    def get_list_of_registers(self, iterations, expected, reg_count):
        reg = PatGen_SCRATCH_REG(value=expected)
        return [reg] * iterations * reg_count
    
    def failing_calls(self, expected_values):
        fail_list = list(expected_values)
        for i in random.choices(range(ITERATIONS*REGCOUNT), k=FAILURES):
            fail_list[i] = self.value_other_than(fail_list[i])
        return [PatGen_SCRATCH_REG(value=x) for x in fail_list]
    
    def value_other_than(self, value):
        while True:
            other = random.randrange(0, 0xFFFFFFFF)
            if other != value:
                return other

    def get_list_of_registers_with_errors(self, iterations, reg_count):
        reg_with_errors = []
        for i in range(iterations):
            error = random.randrange(0, 0xFFFFFFFF)
            reg_with_errors.append(PatGen_SCRATCH_REG(value=error))
        return reg_with_errors * reg_count

    def get_hbicc_instrument(self):
        hbicc = Hbicc()
        hbicc.pat_gen = PatGen()
        hbicc.ring_multiplier = RingMultiplier()
        hbicc.psdb_0 = Psdb()
        hbicc.psdb_1 = Psdb()
        hbicc.psdb_0_pin_0 = PinMultiplier(slot=0,index=0)
        hbicc.psdb_0_pin_1 = PinMultiplier(slot=0,index=1)
        hbicc.psdb_1_pin_0 = PinMultiplier(slot=1,index=0)
        hbicc.psdb_1_pin_1 = PinMultiplier(slot=1,index=1)
        return hbicc

    def mockDevices(self, mock_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil, mock_patgen_hil,
                    mock_hil_hbidps):
        rcConnectList = [RuntimeError] * 1
        hbirctcConnectList = [''] * 1
        patgenConnectList = [''] * 1
        ringConnectList = [''] * 1
        pingConnectList = [''] * 4
        hbidpsConnectList = [''] * 16

        deviceConnectList = {'hbirctcConnectList': hbirctcConnectList, 'hbidpsConnectList': hbidpsConnectList,
                             'patgenConnectList': patgenConnectList, 'ringConnectList': ringConnectList,
                             'pingConnectList': pingConnectList, 'rcConnectList': rcConnectList}

        mock_hil.rcConnect = Mock(side_effect=deviceConnectList['rcConnectList'])
        mock_hbirctc_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['hbirctcConnectList'])
        mock_pinmultiplier_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['pingConnectList'])
        mock_ringmultiplier_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['ringConnectList'])
        mock_patgen_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['patgenConnectList'])
        mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=deviceConnectList['hbidpsConnectList'])

    def mock_logging_for_all_cc_fpga_classes(self, instance):
        if instance.env.hbicc.pat_gen:
            instance.env.hbicc.pat_gen.Log = Mock()
        if instance.env.hbicc.ring_multiplier:
            instance.env.hbicc.ring_multiplier.Log = Mock()
        if instance.env.hbicc.psdb_0_pin_0:
            instance.env.hbicc.psdb_0_pin_0.Log = Mock()
        if instance.env.hbicc.psdb_0_pin_1:
            instance.env.hbicc.psdb_0_pin_1.Log = Mock()
        if instance.env.hbicc.psdb_1_pin_0:
            instance.env.hbicc.psdb_1_pin_0.Log = Mock()
        if instance.env.hbicc.psdb_1_pin_1:
            instance.env.hbicc.psdb_1_pin_1.Log = Mock()
        if instance.env.hbicc.psdb_0:
            instance.env.hbicc.psdb_0.Log = Mock()
        if instance.env.hbicc.psdb_1:
            instance.env.hbicc.psdb_1.Log = Mock()
        if instance.env.hbicc:
            instance.env.hbicc.Log = Mock()
        if instance:
            instance.Log = Mock()

        return instance