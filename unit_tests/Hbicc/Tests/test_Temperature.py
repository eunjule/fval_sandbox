################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from unittest.mock import patch, Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests import Temperature
from Hbicc.Tests.Temperature import Diagnostics
from Common import stratix10
from Hbicc.instrument.hbicc import Hbicc
from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.ringmultiplier import RingMultiplier
from Hbicc.instrument.pinmultiplier import PinMultiplier

means = Temperature.MEAN_TEMPERATURE
Temperature.MAX_DIFF = 1


class TemperatureTests(HbiccTestCase):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedPatgenTxToRingMultiplierRxTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PATGEN']['MEAN']
            hil_value = stratix10.convert_celsius_to_raw(reference_value)
            mock_patgen_hil.hbiMbTmonRead = Mock(return_value=reference_value)
            mock_patgen_hil.hbiMbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPatgenInternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.pat_gen.Log.call_args_list[0][0]
            self.assertIn('PATGEN: HIL Average temperature is', message, message)

            log_level, message = self.diagnostics.env.hbicc.pat_gen.Log.call_args_list[1][0]
            self.assertIn('PATGEN: BAR Read Average temperature is', message, message)

    def test_DirectedPatgenInternalTemperatureReadTest_one_method_pass_one_method_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PATGEN']['MEAN']
            hil_value = stratix10.convert_celsius_to_raw(reference_value)
            mock_patgen_hil.hbiMbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_patgen_hil.hbiMbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPatgenInternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.pat_gen.Log.call_args_list[0][0]
            self.assertIn('PATGEN: FPGA Temperature Failed', message)

    def test_DirectedPatgenInternalTemperatureReadTest_both_methods_outside_10_percent(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PATGEN']['MEAN']
            hil_value = stratix10.convert_celsius_to_raw(reference_value * .8)
            mock_patgen_hil.hbiMbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_patgen_hil.hbiMbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPatgenInternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.pat_gen.Log.call_args_list[0][0]
            self.assertIn('PATGEN: FPGA Temperature Failed', message)

    def test_DirectedRingMultiplierInternalTemperatureReadTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['RINGMULTIPLIER']['MEAN']
            hil_value = stratix10.convert_celsius_to_raw(reference_value)
            mock_ringmultiplier_hil.hbiMbTmonRead = Mock(return_value=reference_value)
            mock_ringmultiplier_hil.hbiMbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedRingMultiplierInternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.ring_multiplier.Log.call_args_list[0][0]
            self.assertIn('RINGMULTIPLIER: HIL Average temperature is', message, message)

            log_level, message = self.diagnostics.env.hbicc.ring_multiplier.Log.call_args_list[1][0]
            self.assertIn('RINGMULTIPLIER: BAR Read Average temperature is', message, message)

    def test_DirectedRingMultiplierInternalTemperatureReadTest_one_method_pass_one_method_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['RINGMULTIPLIER']['MEAN']
            hil_value = stratix10.convert_celsius_to_raw(reference_value)
            mock_ringmultiplier_hil.hbiMbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_ringmultiplier_hil.hbiMbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedRingMultiplierInternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.ring_multiplier.Log.call_args_list[0][0]
            self.assertIn('RINGMULTIPLIER: FPGA Temperature Failed', message)

    def test_DirectedRingMultiplierInternalTemperatureReadTest_both_methods_outside_10_percent(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['RINGMULTIPLIER']['MEAN']
            hil_value = stratix10.convert_celsius_to_raw(reference_value * .8)
            mock_ringmultiplier_hil.hbiMbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_ringmultiplier_hil.hbiMbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedRingMultiplierInternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.ring_multiplier.Log.call_args_list[0][0]
            self.assertIn('RINGMULTIPLIER: FPGA Temperature Failed', message)

    def test_DirectedPSDB0PinMultiplier0InternalTemperatureReadTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][0]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB0PinMultiplier0InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_0: HIL Average temperature is', message)

            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_0.Log.call_args_list[1][0]
            self.assertIn('PSDB_0_PIN_0: BAR Read Average temperature is', message)

    def test_DirectedPSDB0PinMultiplier0InternalTemperatureReadTest_one_method_pass_one_method_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][0]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value*1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB0PinMultiplier0InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_0: FPGA Temperature Failed', message)

    def test_DirectedPSDB0PinMultiplier0InternalTemperatureReadTest_both_methods_outside_10_percent(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][0]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value * .8)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB0PinMultiplier0InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_0: FPGA Temperature Failed', message)

    def test_DirectedPSDB0PinMultiplier1InternalTemperatureReadTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][1]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB0PinMultiplier1InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_1: HIL Average temperature is', message)

            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_1.Log.call_args_list[1][0]
            self.assertIn('PSDB_0_PIN_1: BAR Read Average temperature is', message)

    def test_DirectedPSDB0PinMultiplier1InternalTemperatureReadTest_one_method_pass_one_method_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][1]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value*1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB0PinMultiplier1InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_1: FPGA Temperature Failed', message)

    def test_DirectedPSDB0PinMultiplier1InternalTemperatureReadTest_both_methods_outside_10_percent(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][1]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value * .8)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value*1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB0PinMultiplier1InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_0_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_0_PIN_1: FPGA Temperature Failed', message)

    def test_DirectedPSDB1PinMultiplier0InternalTemperatureReadTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][2]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB1PinMultiplier0InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_0: HIL Average temperature is', message)

            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_0.Log.call_args_list[1][0]
            self.assertIn('PSDB_1_PIN_0: BAR Read Average temperature is', message)

    def test_DirectedPSDB1PinMultiplier0InternalTemperatureReadTest_one_method_pass_one_method_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][2]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value*1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB1PinMultiplier0InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_0: FPGA Temperature Failed', message)

    def test_DirectedPSDB1PinMultiplier0InternalTemperatureReadTest_both_methods_outside_10_percent(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][2]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value * .8)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB1PinMultiplier0InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_0.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_0: FPGA Temperature Failed', message)

    def test_DirectedPSDB1PinMultiplier1InternalTemperatureReadTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][3]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB1PinMultiplier1InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_1: HIL Average temperature is', message,message)

            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_1.Log.call_args_list[1][0]
            self.assertIn('PSDB_1_PIN_1: BAR Read Average temperature is', message)

    def test_DirectedPSDB1PinMultiplier1InternalTemperatureReadTest_one_method_pass_one_method_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][3]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB1PinMultiplier1InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_1: FPGA Temperature Failed', message)

    def test_DirectedPSDB1PinMultiplier1InternalTemperatureReadTest_both_methods_outside_10_percent(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb.hil') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard.hil') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:
            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)

            reference_value = means['PINMULTIPLIER'][3]['MEAN']
            hil_value = self.diagnostics.env.hbicc.psdb_0_pin_0.convert_degrees_celcius_to_raw(reference_value * .8)
            mock_pinmultiplier_hil.hbiPsdbTmonRead = Mock(return_value=reference_value * 1.3)
            mock_pinmultiplier_hil.hbiPsdbBarRead = Mock(return_value=hil_value)

            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)

            self.diagnostics.DirectedPSDB1PinMultiplier1InternalTemperatureReadTest()
            log_level, message = self.diagnostics.env.hbicc.psdb_1_pin_1.Log.call_args_list[0][0]
            self.assertIn('PSDB_1_PIN_1: FPGA Temperature Failed', message)

    def get_hbicc_instrument(self):
        hbicc = Hbicc()
        hbicc.pat_gen = PatGen()
        hbicc.ring_multiplier = RingMultiplier()
        hbicc.psdb_0_pin_0 = PinMultiplier(slot=0,index=0)
        hbicc.psdb_0_pin_1 = PinMultiplier(slot=0,index=1)
        hbicc.psdb_1_pin_0 = PinMultiplier(slot=1,index=0)
        hbicc.psdb_1_pin_1 = PinMultiplier(slot=1,index=1)
        return hbicc

    def mockDevices(self, mock_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil, mock_patgen_hil,
                    mock_hil_hbidps):
        rcConnectList = [RuntimeError] * 1
        hbirctcConnectList = [''] * 1
        patgenConnectList = [''] * 1
        ringConnectList = [''] * 1
        pingConnectList = [''] * 4
        hbidpsConnectList = [''] * 16

        deviceConnectList = {'hbirctcConnectList': hbirctcConnectList, 'hbidpsConnectList': hbidpsConnectList,
                             'patgenConnectList': patgenConnectList, 'ringConnectList': ringConnectList,
                             'pingConnectList': pingConnectList, 'rcConnectList': rcConnectList}

        mock_hil.rcConnect = Mock(side_effect=deviceConnectList['rcConnectList'])
        mock_hbirctc_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['hbirctcConnectList'])
        mock_pinmultiplier_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['pingConnectList'])
        mock_ringmultiplier_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['ringConnectList'])
        mock_patgen_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['patgenConnectList'])
        mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=deviceConnectList['hbidpsConnectList'])

    def mock_logging_for_all_cc_fpga_classes(self, instance):
        if instance.env.hbicc.pat_gen:
            instance.env.hbicc.pat_gen.Log = Mock()
        if instance.env.hbicc.ring_multiplier:
            instance.env.hbicc.ring_multiplier.Log = Mock()
        if instance.env.hbicc.psdb_0_pin_0:
            instance.env.hbicc.psdb_0_pin_0.Log = Mock()
        if instance.env.hbicc.psdb_0_pin_1:
            instance.env.hbicc.psdb_0_pin_1.Log = Mock()
        if instance.env.hbicc.psdb_1_pin_0:
            instance.env.hbicc.psdb_1_pin_0.Log = Mock()
        if instance.env.hbicc.psdb_1_pin_1:
            instance.env.hbicc.psdb_1_pin_1.Log = Mock()
        if instance.env.hbicc:
            instance.env.hbicc.Log = Mock()
        if instance:
            instance.Log = Mock()

        return instance

