################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.instrument import patgen_register
from Hbicc.instrument.patgen_register import ALARMS1
from Hbicc.testbench.capture_structs import CTV_HEADER_AND_DATA
from Hbicc.testbench.capturefailures import STREAM_BLOCK
from Hbicc.Tests.Alarms import Critical, CTV_DATA_STREAM_BLOCK_SIZE, OverflowAlarmTestConstants

TEST_REPEAT = 3


class OverflowAlarmProtectionUnitTestConstants(OverflowAlarmTestConstants):
    MAX_CTV_DATA_BLOCK_NUM_LIMIT_UNIT_TEST = 64
    MAX_CTV_HEADER_BLOCK_NUM_LIMIT_UNIT_TEST = 256
    MAX_BLOCK_NUM_UNIT_TEST = 64


class AlarmTests(HbiccTestCase, OverflowAlarmProtectionUnitTestConstants):
    def setUp(self):
        super().setUp()
        self.alarm_test = self.set_up_alarm_direct_test()

    # @ Common.fval.skip('Only check for corner condition. No need to always test')
    def test_ctv_row_data_overflow_alarm_no_ctv(self):
        ctv_block_num = 0
        ctv_block_num_limit = 1
        self.set_up_ctv_data_alarm_direct_test(ctv_block_num, ctv_block_num_limit)
        with self.assertRaises(Exception):
            self.alarm_test.DirectedCTVHeaderCaptureOverflowTest()
        self.assert_capture_stream_size(byte_num=CTV_DATA_STREAM_BLOCK_SIZE * max(ctv_block_num, 1),
                                        test=self.alarm_test, count_register=patgen_register.CTV_CAPTURE_DATA_COUNT)

    def test_ctv_row_data_overflow_alarm_reach_end_address(self):
        for _ in range(0, TEST_REPEAT):
            self.reset_alarm_test(self.alarm_test)
            ctv_block_num = 8
            ctv_block_num_limit = 8
            self.set_up_ctv_data_alarm_direct_test(ctv_block_num, ctv_block_num_limit)
            self.alarm_test.DirectedCTVRowCaptureOverflowTest()
            self.assert_capture_stream_size(byte_num=CTV_DATA_STREAM_BLOCK_SIZE * ctv_block_num, test=self.alarm_test,
                                            count_register=patgen_register.CTV_CAPTURE_DATA_COUNT)
            self.assert_ctv_data_buffer_is_too_small(byte_num=CTV_DATA_STREAM_BLOCK_SIZE * ctv_block_num, test=self.alarm_test)
            self.assert_alarm_set('ctv_row_capture_overflow', self.alarm_test)
            self.assert_whether_eob_exist(test=self.alarm_test, expected_eob=True)

    def assert_whether_eob_exist(self, test, expected_eob):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            actual_count, register_count, data = slice_object.capture.get_ctv_block_count_and_data()
            observed_eob = False
            self.assertTrue(data)
            for block_num in range(actual_count):
                start = block_num * STREAM_BLOCK
                end = start + STREAM_BLOCK
                block = CTV_HEADER_AND_DATA(data[start: end])
                if block.end_of_burst:
                    observed_eob = True
            self.assertEqual(expected_eob, observed_eob)

    def assert_capture_stream_size(self, byte_num, test, count_register):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            count = capture.get_stream_total_size(capture.patgen, count_register)
            total_bytes = count * STREAM_BLOCK
            self.assertEqual(total_bytes, byte_num)

    def assert_ctv_data_buffer_is_too_small(self, byte_num, test):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            end_address = capture.patgen.read_slice_register(patgen_register.CTV_DATA_STREAM_END_ADDRESS_REG,
                                                             capture.slice.index).Data << 6
            start_address = capture.patgen.read_slice_register(patgen_register.CTV_DATA_STREAM_START_ADDRESS_REG,
                                                               capture.slice.index).Data << 6
            self.assertTrue(end_address <= start_address + byte_num)

    def assert_alarm_set(self, alarm, test):
        self.assertTrue(test.env.hbicc.hbicc_alarms.is_alarm_set({alarm}))
        for slice, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            model_simulator_alarms1 = test.env.hbicc.model.patgen.slices[slice_object.index].registers[ALARMS1.ADDR]
            tester_simulator_alarms1 = test.env.hbicc.pat_gen.read_slice_register(ALARMS1, slice_object.index).value
            self.assertEqual(model_simulator_alarms1, tester_simulator_alarms1)
            self.assertTrue(getattr(ALARMS1(tester_simulator_alarms1), alarm))

    def set_up_ctv_data_alarm_direct_test(self, ctv_block_num=8, ctv_block_num_limit=8):
        self.alarm_test.ctv_data_num = ctv_block_num
        self.alarm_test.ctv_data_num_limit = ctv_block_num_limit

    def set_up_alarm_direct_test(self):
        alarm_test = Critical()
        self.reset_alarm_test(alarm_test)
        return alarm_test

    def reset_alarm_test(self, alarm_test):
        alarm_test.setUp(self.tester)
        alarm_test.pattern_helper.write_fails_to_csv = Mock
        alarm_test.pattern_helper.log_xcvr_count_status = Mock
        alarm_test.unit_test = True
        alarm_test.pattern_helper.is_regression = False
        alarm_test.test_repeat = 1

    def test_ctv_row_data_overflow_alarm_exceeding_end_address(self):
        for _ in range(0, TEST_REPEAT):
            ctv_block_num = 109
            ctv_block_num_limit = 100
            self.set_up_ctv_data_alarm_direct_test(ctv_block_num, ctv_block_num_limit)
            self.alarm_test.DirectedCTVRowCaptureOverflowTest()
            self.assert_alarm_set('ctv_row_capture_overflow', self.alarm_test)
            self.assert_capture_stream_size(ctv_block_num * CTV_DATA_STREAM_BLOCK_SIZE, self.alarm_test,
                                            patgen_register.CTV_CAPTURE_DATA_COUNT)
            self.assert_whether_eob_exist(test=self.alarm_test, expected_eob=False)

    def test_ctv_row_data_overflow_alarm_no_overflow_no_alarm(self):
        for _ in range(0, TEST_REPEAT):
            self.reset_alarm_test(self.alarm_test)
            ctv_block_num = 99
            ctv_block_num_limit = 100
            self.set_up_ctv_data_alarm_direct_test(ctv_block_num, ctv_block_num_limit)
            with self.assertRaises(Exception):
                self.alarm_test.DirectedCTVRowCaptureOverflowTest()
            self.assert_capture_stream_size(ctv_block_num * CTV_DATA_STREAM_BLOCK_SIZE, self.alarm_test,
                                            count_register=patgen_register.CTV_CAPTURE_DATA_COUNT)
            self.assert_alarm_not_set('ctv_row_capture_overflow', self.alarm_test)
            self.assert_whether_eob_exist(test=self.alarm_test, expected_eob=True)

    def assert_alarm_not_set(self, alarm, test):
        self.assertFalse(test.env.hbicc.hbicc_alarms.is_alarm_set({alarm}))
        for slice, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            model_simulator_alarms1 = test.env.hbicc.model.patgen.slices[slice_object.index].registers[ALARMS1.ADDR]
            tester_simulator_alarms1 = test.env.hbicc.pat_gen.read_slice_register(ALARMS1, slice_object.index).value
            self.assertEqual(model_simulator_alarms1, tester_simulator_alarms1)
            self.assertFalse(ALARMS1(tester_simulator_alarms1).ctv_row_capture_overflow)

    def test_ctv_header_overflow_pattern_execute(self):
        for _ in range(0, TEST_REPEAT):
            self.reset_alarm_test(self.alarm_test)
            ctv_header_num_limit = random.randint(self.MIN_HEADER_BLOCK_NUM_LIMIT,
                                                  self.MAX_CTV_HEADER_BLOCK_NUM_LIMIT_UNIT_TEST) * 2
            ctv_header_num = ctv_header_num_limit * len(self.alarm_test.slices)
            self.set_up_ctv_header_alarm_direct_test(ctv_header_num, ctv_header_num_limit)
            self.alarm_test.DirectedCTVHeaderCaptureOverflowTest()
            self.assert_ctv_header_capture_stream_size(ctv_header_num, self.alarm_test)
            self.assert_ctv_header_buffer_size_and_overflowed(byte_num=STREAM_BLOCK * ctv_header_num,
                                                              test=self.alarm_test,
                                                              expected_buffer_size=STREAM_BLOCK * ctv_header_num_limit,
                                                              expect_overflow=True)
            self.assert_ctv_header_overflow_alarm_set(self.alarm_test)

    def test_ctv_header_not_overflow_pattern_execute(self):
        for _ in range(0, TEST_REPEAT):
            self.reset_alarm_test(self.alarm_test)
            ctv_header_num = random.randint(3, self.MAX_CTV_HEADER_BLOCK_NUM_LIMIT_UNIT_TEST)
            ctv_header_num_limit = (ctv_header_num // 2 + 1) * 2
            self.set_up_ctv_header_alarm_direct_test(ctv_header_num, ctv_header_num_limit)
            with self.assertRaises(Exception):
                self.alarm_test.DirectedCTVHeaderCaptureOverflowTest()
            self.assert_ctv_header_capture_stream_size(ctv_header_num, self.alarm_test)
            self.assert_ctv_header_buffer_size_and_overflowed(byte_num=STREAM_BLOCK * ctv_header_num,
                                                              test=self.alarm_test,
                                                              expected_buffer_size=STREAM_BLOCK * ctv_header_num_limit,
                                                              expect_overflow=False)
            self.assert_alarm_not_set('ctv_header_capture_overflow', self.alarm_test)

    def set_up_ctv_header_alarm_direct_test(self, ctv_header_num, ctv_header_num_limit):
        self.alarm_test.ctv_header_num = ctv_header_num
        self.alarm_test.ctv_header_num_limit = ctv_header_num_limit

    def assert_ctv_header_buffer_size_and_overflowed(self, byte_num, test, expected_buffer_size, expect_overflow):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            end_address = capture.patgen.read_slice_register(patgen_register.CTV_HEADER_STREAM_END_ADDRESS_REG,
                                                             capture.slice.index).Data << 6
            start_address = capture.patgen.read_slice_register(patgen_register.CTV_HEADER_STREAM_START_ADDRESS_REG,
                                                               capture.slice.index).Data << 6
            observed_overflow = end_address <= start_address + byte_num
            self.assertEqual(observed_overflow, expect_overflow)
            self.assertEqual((end_address - start_address), expected_buffer_size)

    def assert_ctv_header_capture_stream_size(self, total_count, test):
        slices_count_num = test.pattern_helper.get_slice_ctv_header_distribution(total_count)
        for slice_index, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            capture = slice_object.capture
            count = capture.get_stream_total_size(capture.patgen, patgen_register.CTV_CAPTURE_HEADER_COUNT)
            total_bytes = count * STREAM_BLOCK
            self.assertEqual(total_bytes, slices_count_num[slice_index] * STREAM_BLOCK)

    def assert_ctv_header_overflow_alarm_set(self, test):
        self.assertTrue(test.env.hbicc.hbicc_alarms.is_alarm_set({'ctv_header_capture_overflow'}))
        expected_alarm_list = test.pattern_helper.get_expected_ctv_header_overflow_map(
            ctv_header_num=test.ctv_header_num, ctv_header_num_limit=test.ctv_header_num_limit)
        for slice_index, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            model_simulator_alarms1 = test.env.hbicc.model.patgen.slices[slice_object.index].registers[ALARMS1.ADDR]
            tester_simulator_alarms1 = test.env.hbicc.pat_gen.read_slice_register(ALARMS1, slice_object.index).value
            self.assertEqual(model_simulator_alarms1, tester_simulator_alarms1)
            observed_alarm = getattr(ALARMS1(tester_simulator_alarms1), 'ctv_header_capture_overflow')
            expected_alarm = expected_alarm_list[slice_index]
            self.assertEqual(expected_alarm, observed_alarm)

