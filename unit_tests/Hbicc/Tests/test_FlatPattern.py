################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch, Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.instrument.hbicc import Hbicc
from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.ringmultiplier import RingMultiplier
from Hbicc.instrument.pinmultiplier import PinMultiplier
from Hbicc.Tests.FlatPattern import Functional


class FlatPatternTests(HbiccTestCase):
    @unittest.skip('Waiting for framework completion. 5/24/2019')
    def test_dc_trigger_queue_functions(self):
        with patch('Common.instruments.hbicc.ringmultiplier.hil') as mock_ringmultiplier_hil :
            flat_pattern = Functional()
            flat_pattern.env = Mock()
            hbicc_instrument = self.get_hbicc_instrument()
            flat_pattern.env.hbicc.ring_multiplier = hbicc_instrument.ring_multiplier
            payload_lower, payload_upper = flat_pattern.generate_payload_bits('DRIVE_HIGH')
            flat_pattern.env.hbicc.ring_multiplier.start_trigger_queue_record(psdb_target=0)
            flat_pattern.env.hbicc.ring_multiplier.end_trigger_queue_record(psdb_target=0)
            flat_pattern.env.hbicc.ring_multiplier.set_pin_direction(psdb_target=1,channel_index=5,payload_lower=payload_lower,payload_upper=payload_upper)

    # def test_pattern_string(self):
    #     hbicc = self.get_hbicc_instrument()
    #     flat_pattern_helper = Helper(hbicc.pat_gen,hbicc.ring_multiplier,hbicc.psdb_0_pin_0,hbicc.psdb_1_pin_0)
    #     pattern_string = self.get_pattern_string()
    #     flat_pattern_helper.write_pattern_to_memory(pattern_string)

    def test_flat_pattern_keep_pin(self):
        flat_pattern = Functional()
        flat_pattern.setUp(tester=self.tester)
        flat_pattern.pattern_helper.is_regression = False  
        for pm in flat_pattern.env.hbicc.get_pin_multipliers():		      
            pm.set_delay_stack_on_all_pins()
        flat_pattern.DirectedFlatPatternKeepPinTest()
        num_fails_table = flat_pattern.env.hbicc.psdb_0_pin_0.channel_error_counts(0,0)
        expected_fails=80
        self.assertEqual(num_fails_table[5], expected_fails)

    def get_hbicc_instrument(self):
        hbicc = Hbicc()
        hbicc.pat_gen = PatGen()
        hbicc.ring_multiplier = RingMultiplier()
        hbicc.psdb_0_pin_0 = PinMultiplier(slot=0, index=0)
        hbicc.psdb_0_pin_1 = PinMultiplier(slot=0, index=1)
        hbicc.psdb_1_pin_0 = PinMultiplier(slot=1, index=0)
        hbicc.psdb_1_pin_1 = PinMultiplier(slot=1, index=1)


        return hbicc

    def get_pattern_string(self):
        pattern_string = '''
                PATTERN_START:
                S stype=ACTIVE_CHANNELS,                            data=0o77777777777777777777777777777777777 # all channels active
                S stype=EC_RESET,                                   data=0b11111111111111111111111111111111111 # reset enable clock counts on all pins
                S stype=DUT_SERIAL, channel_set=ALL_CHANNEL_SETS,   data=0vKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK # all unserialized pins hold value in serialized sections
                DriveZeroVectors length=250
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLH # compare High on pin 0
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHL # compare High on pin 1
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLL # compare High on pin 2
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLL # compare High on pin 3
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLL # compare High on pin 4
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLL # compare High on pin 5
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLL # compare High on pin 6
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLL # compare High on pin 7
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLL # compare High on pin 8
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLL # compare High on pin 9
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLL # compare High on pin 10
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLL # compare High on pin 11
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLL # compare High on pin 12
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLL # compare High on pin 13
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLL # compare High on pin 14
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLL # compare High on pin 15
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLL # compare High on pin 16
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLL # compare High on pin 17
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLL # compare High on pin 18
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLL # compare High on pin 19
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLL # compare High on pin 20
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLL # compare High on pin 21
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 22
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 23
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 24
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 25
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 26
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 27
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 28
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 29
                V ctv=0, mtv=0, lrpt=0, data=0vLLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 30
                V ctv=0, mtv=0, lrpt=0, data=0vLLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 31
                V ctv=0, mtv=0, lrpt=0, data=0vLLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 32
                V ctv=0, mtv=0, lrpt=0, data=0vLHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 33
                V ctv=0, mtv=0, lrpt=0, data=0vHLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL # compare High on pin 34
                DriveZeroVectors length=250
                
                I optype=VECTOR, vptype=VPLOCAL, vpop=END_I, imm=0xdeadbeef                                      
                V ctv=0, mtv=0, lrpt=0, data=0vXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                '''
        return pattern_string