################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
import unittest
from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.Branch import Functional
from Hbicc.testbench.capturefailures import Capture


time.sleep = lambda x: None


class BranchTests(HbiccTestCase):
    def get_functional(self):
        functional = Functional()
        functional.Log = Mock()
        functional.setUp(tester=self.tester)
        functional.pattern_helper.is_regression = False
        functional.pattern_helper.Log = Mock()
        for slice in functional.env.hbicc.model.patgen.slices:
            slice.Log = Mock()
        for slice in self.simulator.patgen.slices:
            slice.Log = Mock()
        return functional

    def check_for_message(self, level, message, calls):
        for args, kwargs in calls:
            self.args = args
            log_level, log_message = self.args
            if level == log_level and message in log_message:
                break
        else:
            call_strings = '\n'.join([str(x) for x in calls])
            self.fail(f'"{level} ,{message}" not in\n{call_strings}')

    def set_patgen_register(self, value, slices=range(0, 5), reg=0x178):
        for slice in slices:
            self.simulator.patgen.slices[slice].fail_registers[reg] = value
            self.simulator.patgen.slices[slice].Log = Mock()

    def test_DirectedSubrHeadCallIGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrHeadCallIGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrHeadCallIGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        c = Capture(Mock())
        c.Log = Mock()
        functional.DirectedSubrHeadCallIGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, c.Log.call_args_list)

    def test_DirectedSubrTailCallIGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrTailCallIGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailCallIGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrTailCallIGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedSubrHeadCallRRelTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrHeadCallRRelTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrHeadCallRRelTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrHeadCallRRelTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedSubrTailCallRRelTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrTailCallRRelTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    def _assert_no_logged_errors(self, calls):
        args = [x[0] for x in calls]
        kwargs = [x[1] for x in calls]
        levels = [x[0].lower() for x in args] + [x.get('level', '').lower() for x in kwargs]
        if 'error' in levels:
            self.fail('Unexpected logged error')

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailCallRRelTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrTailCallRRelTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapCallICfbTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrTailGapCallICfbTest()
        message = f'Branch instruction passed'
        self.check_for_message('info', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapCallICfbTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrTailGapCallICfbTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapCallRCfbTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrTailGapCallRCfbTest()
        message = f'Branch instruction passed'
        self.check_for_message('info', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapCallRCfbTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrTailGapCallRCfbTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapPCallICfbTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrTailGapPCallICfbTest()
        message = f'Branch instruction passed'
        self.check_for_message('info', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapPCallICfbTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrTailGapPCallICfbTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapPCallRCfbTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrTailGapPCallRCfbTest()
        message = f'Branch instruction passed'
        self.check_for_message('info', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrTailGapPCallRCfbTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrTailGapPCallRCfbTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedSubrRecursivePCallIPfb10Test_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrRecursivePCallIPfb10Test()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrRecursivePCallIPfb10Test_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrRecursivePCallIPfb10Test()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedSubrRecursivePCallIPfb63Test_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrRecursivePCallIPfb63Test()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrRecursivePCallIPfb63Test_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrRecursivePCallIPfb63Test()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedSubrNestedPCallIGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedSubrNestedPCallIGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedSubrNestedPCallIGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedSubrNestedPCallIGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoAllAddrInOrderTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoAllAddrInOrderTest()
        message = f'Branch instruction passed'
        self.check_for_message('info', message, functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoAllAddrInOrderTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoAllAddrInOrderTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoImmediateGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoImmediateGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoImmediateGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoImmediateGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoImmediateRelativeTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoImmediateRelativeTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoImmediateRelativeTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoImmediateRelativeTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoRegisterGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoRegisterGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoRegisterGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoRegisterGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoRegisterRelativeTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoRegisterRelativeTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoRegisterRelativeTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoRegisterRelativeTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedPCallLessThanOrEqualCarry1Test_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedPCallLessThanOrEqualCarry1Test()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedPCallLessThanOrEqualCarry1Test_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedPCallLessThanOrEqualCarry1Test()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedPCallLessThanOrEqualCarry0Test_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedPCallLessThanOrEqualCarry0Test()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedPCallLessThanOrEqualCarry0Test_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedPCallLessThanOrEqualCarry0Test()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedPCallGreaterThanOrEqualTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedPCallGreaterThanOrEqualTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedPCallGreaterThanOrEqualTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedPCallGreaterThanOrEqualTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedPCallBelowOrEqualTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedPCallBelowOrEqualTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedPCallBelowOrEqualTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedPCallBelowOrEqualTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoIfCarryImmediateGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoIfCarryImmediateGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoIfCarryImmediateGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoIfCarryImmediateGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoIfOverflowImmediateGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoIfOverflowImmediateGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoIfOverflowImmediateGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoIfOverflowImmediateGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoIfSignImmediateGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoIfSignImmediateGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoIfSignImmediateGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoIfSignImmediateGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)

    def test_DirectedGotoIfZeroImmediateGlobalTest_pass(self):
        self.set_patgen_register(value=None)
        functional = self.get_functional()
        functional.DirectedGotoIfZeroImmediateGlobalTest()
        self._assert_no_logged_errors(functional.Log.call_args_list)

    @unittest.skip('Not Ready')
    def test_DirectedGotoIfZeroImmediateGlobalTest_fail(self):
        self.set_patgen_register(value=0xFF, reg=0x288)
        functional = self.get_functional()
        functional.DirectedGotoIfZeroImmediateGlobalTest()
        message = f'observed_trace_count: 255'
        self.check_for_message('error', message, functional.Log.call_args_list)
