# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
from unittest.mock import Mock

from Hbicc.instrument.patgen_register import FLAGS
from Hbicc.testbench.capturefailures import Capture
from Hbicc.testbench.env import HbiccEnv
from Hbicc.testbench.PatternUtility import Slice, PatternHelper
from Hbicc.testbench.trigger_utility import TriggerWord, TriggerUtility
from Hbicc.Tests.Flags import Triggers
from unit_tests.Hbicc.Tests.hbicc_testcase import HbiccTestCase


class MockCapture(Mock):
    def __init__(self, slice_object):
        super().__init__()
        self.slice = slice_object
        self.error_cycles = []

    def mock_get_user_cycles_from_error_stream(self, *args, **kwargs):
        slice_index = self.slice.index
        return self.error_cycles[slice_index][kwargs['reg_index']]

    def update_error_cycles(self, error_cycles):
        self.error_cycles = error_cycles


class TriggersTest(HbiccTestCase):
    def test_check_trigger_seen_and_trigger_flags(self):
        slices = [0, 1, 2, 3, 4]
        trigger_direct_test = Triggers()
        trigger_direct_test.pattern_helper = PatternHelper(env=Mock())
        trigger_direct_test.Log = self._func_do_nothing

        trigger_type = 0
        dut_id = 32

        trigger_word_sent = 0x02000000
        trigger_word_seen = 0x02000001
        flag_seen = 'Software_Trigger'

        self.assertTrue(self.check_trigger_and_flag_with_given_criteria(dut_id, flag_seen, slices, trigger_direct_test,
                                                                        trigger_type, trigger_word_seen,
                                                                        trigger_word_sent))

        trigger_type = 2
        dut_id = 32

        trigger_word_sent = 0x02000002
        trigger_word_seen = 0x02000005
        flag_seen = 'Domain_Trigger'

        self.assertFalse(self.check_trigger_and_flag_with_given_criteria(dut_id, flag_seen, slices, trigger_direct_test,
                                                                         trigger_type, trigger_word_seen,
                                                                         trigger_word_sent))

    def test_check_capture_cycle_count(self):
        slices = [0, 1, 2, 3, 4]
        reg_indexes = [0, 1, 2, 3]
        mock_captures, trigger_direct_test = self.set_up_capture_for_direct_test(reg_indexes, slices)

        error_cycles = self.create_correct_capture_cycles(slices, reg_indexes)
        for mock_capture in mock_captures:
            mock_capture.update_error_cycles(error_cycles)

        self.assertTrue(trigger_direct_test.check_capture_cycle_count())

        correct_captures = {0: {0: True, 1: True, 2: True, 3: False},
                            1: {0: True, 1: True, 2: True, 3: True},
                            2: {0: True, 1: True, 2: True, 3: True},
                            3: {0: True, 1: True, 2: True, 3: True},
                            4: {0: True, 1: True, 2: True, 3: True}}
        error_cycles = self.create_same_incorrect_capture_cycles(slices, reg_indexes, correct_captures)
        for mock_capture in mock_captures:
            mock_capture.update_error_cycles(error_cycles)

        self.assertFalse(trigger_direct_test.check_capture_cycle_count())

        correct_captures = {0: {0: True, 1: True, 2: True, 3: True},
                            1: {0: True, 1: True, 2: False, 3: True},
                            2: {0: True, 1: True, 2: True, 3: True},
                            3: {0: True, 1: True, 2: True, 3: True},
                            4: {0: True, 1: True, 2: True, 3: True}}
        error_cycles = self.create_same_incorrect_capture_cycles(slices, reg_indexes, correct_captures)
        for mock_capture in mock_captures:
            mock_capture.update_error_cycles(error_cycles)

        self.assertFalse(trigger_direct_test.check_capture_cycle_count())

        correct_captures = {0: {0: True, 1: False, 2: True, 3: True},
                            1: {0: True, 1: True, 2: True, 3: True},
                            2: {0: True, 1: True, 2: True, 3: True},
                            3: {0: True, 1: True, 2: False, 3: True},
                            4: {0: True, 1: True, 2: True, 3: True}}
        error_cycles = self.create_same_incorrect_capture_cycles(slices, reg_indexes, correct_captures)
        for mock_capture in mock_captures:
            mock_capture.update_error_cycles(error_cycles)

        self.assertFalse(trigger_direct_test.check_capture_cycle_count())

    def check_trigger_and_flag_with_given_criteria(self, dut_id, flag_seen, slices, trigger_direct_test, trigger_type,
                                                   trigger_word_seen, trigger_word_sent):
        trigger_direct_test.trigger_utility = self.set_up_virtual_trigger_utility(dut_id, trigger_type)
        self.set_up_flag_dict(flag_seen, slices)
        trigger_direct_test.trigger_utility.get_last_seen_trigger_from_patgen = Mock(return_value=trigger_word_seen)
        return trigger_direct_test.check_trigger_seen_and_trigger_flags(trigger_word_sent)

    def set_up_flag_dict(self, flag_seen, slices):
        self.flag_dict = {}
        for slice in slices:
            observed_flag = FLAGS()
            setattr(observed_flag, flag_seen, 1)
            self.flag_dict.update({slice: observed_flag})

    def set_up_virtual_trigger_utility(self, dut_id, trigger_type):
        trigger_utility = TriggerUtility(hbicc=Mock(), slice_list=Mock())
        trigger_utility.trigger_type = trigger_type
        trigger_utility.dut_id = dut_id
        trigger_utility.target_resource = 0
        return trigger_utility

    def set_up_capture_for_direct_test(self, reg_indexes, slices):
        triggerTest = Triggers()
        triggerTest.Log = self._func_do_nothing
        triggerTest.env = HbiccEnv(test=Mock(), tester=Mock())
        triggerTest.env.hbicc.contruct_text_table = self._func_do_nothing
        triggerTest.pattern_helper = PatternHelper(env=Mock())
        mock_captures = []
        for slice in slices:
            slice_object = Slice(pattern_helper=triggerTest.pattern_helper,
                                 hbicc=Mock(), pattern=Mock(),
                                 fixed_drive_state=Mock(),
                                 slice=slice,
                                 channel_sets=Mock())
            slice_object.capture = Capture(slice=slice_object)
            slice_object.mock_capture = MockCapture(slice_object)
            mock_captures.append(slice_object.mock_capture)
            slice_object.capture.get_user_cycles_from_error_stream = \
                slice_object.mock_capture.mock_get_user_cycles_from_error_stream
            triggerTest.pattern_helper.slice_channel_sets_combo.update({slice: slice_object})
            for reg_index in reg_indexes:
                pm = Mock()
                slice_object.capture.pms.update({reg_index: pm})
        return mock_captures, triggerTest

    def create_correct_capture_cycles(self, slices, reg_indexes):
        correct_cycle = random.getrandbits(32)
        error_cycles = {}
        for slice in slices:
            error_cycles_slice = {}
            for reg_index in reg_indexes:
                error_cycles_slice.update({reg_index: [correct_cycle, correct_cycle]})
            error_cycles.update({slice: error_cycles_slice})
        return error_cycles

    def create_same_incorrect_capture_cycles(self, slices, reg_indexes, correct_captures):
        correct_cycle = random.getrandbits(32)
        error_cycles = {}
        for slice in slices:
            error_cycles_slice = {}
            for reg_index in reg_indexes:
                if correct_captures[slice][reg_index]:
                    error_cycles_slice.update({reg_index: [correct_cycle, correct_cycle]})
                else:
                    incorrect_cycle = correct_cycle
                    while incorrect_cycle == correct_cycle:
                        incorrect_cycle = random.getrandbits(32)
                    error_cycles_slice.update({reg_index: [incorrect_cycle, incorrect_cycle]})
            error_cycles.update({slice: error_cycles_slice})
        return error_cycles

    def test_setup_trigger_utility_up(self):
        trigger_type = random.getrandbits(4)
        dut_id = random.getrandbits(6)
        expected_trigger_word = self.set_up_expected_up_trigger_word(dut_id, trigger_type)
        trigger_utility = TriggerUtility(Mock(), Mock())
        trigger_utility.configure_trigger_control_register = Mock()
        trigger_utility.setup_trigger_utility_up(trigger_type, dut_id)
        self.assertEqual(expected_trigger_word, trigger_utility.construct_trigger_word())

    def set_up_expected_up_trigger_word(self, dut_id, trigger_type):
        expected_trigger_word = TriggerWord()
        expected_trigger_word.trigger_type = trigger_type
        expected_trigger_word.count_value = 0
        expected_trigger_word.target_domain = 0  # could domain of 0 conflict with previous tests?
        expected_trigger_word.reserved = 0
        expected_trigger_word.software_trigger_up_only = 1
        expected_trigger_word.dut_id = dut_id
        expected_trigger_word.target_resource = 0
        return expected_trigger_word.value

    def test_setup_trigger_utility_down(self):
        trigger_type = random.getrandbits(4)
        dut_id = random.getrandbits(6)
        expected_trigger_word = self.set_up_expected_down_trigger_word(dut_id, trigger_type)
        trigger_utility = TriggerUtility(Mock(), Mock())
        trigger_utility.configure_trigger_control_register = Mock()
        trigger_utility.setup_trigger_utility_down(trigger_type, dut_id)
        self.assertEqual(expected_trigger_word, trigger_utility.construct_trigger_word())

    def set_up_expected_down_trigger_word(self, dut_id, trigger_type):
        expected_trigger_word = TriggerWord()
        expected_trigger_word.trigger_type = trigger_type
        expected_trigger_word.count_value = 0
        expected_trigger_word.target_domain = 1  # could domain of 0 conflict with previous tests?
        expected_trigger_word.reserved = 0
        expected_trigger_word.software_trigger_up_only = 0
        expected_trigger_word.dut_id = dut_id
        expected_trigger_word.target_resource = 0  # which number corresponds to which device?
        return expected_trigger_word.value

    def test_rc_trigger_check(self):
        hbicc = self.tester.get_hbicc_undertest('Hbicc')
        up_trigger_utility = TriggerUtility(hbicc, [0, 1, 2, 3, 4])
        trigger_type = random.getrandbits(4)
        dut_id = random.getrandbits(6)
        up_trigger_utility.setup_trigger_utility_up(trigger_type, dut_id)
        expected_trigger = up_trigger_utility.construct_trigger_word()
        self.simulator.rc._software_trigger_fifo.push(expected_trigger, 0x10)
        success = up_trigger_utility.rc_trigger_check()
        self.assertTrue(success)

    def test_rc_trigger_check_negative_test(self):
        hbicc = self.tester.get_hbicc_undertest('Hbicc')
        up_trigger_utility = TriggerUtility(hbicc, [0, 1, 2, 3, 4])
        trigger_type = random.getrandbits(4)
        dut_id = random.getrandbits(6)
        up_trigger_utility.setup_trigger_utility_up(trigger_type, dut_id)
        with self.assertRaises(Exception):
            self.assertTrue(up_trigger_utility.rc_trigger_check())

    def test_software_keep_alive_resume_with_down_trigger(self):
        hbicc = self.tester.get_hbicc_undertest('Hbicc')
        down_trigger_utility = TriggerUtility(hbicc, [0, 1, 2, 3, 4])
        trigger_type = random.getrandbits(4)
        dut_id = random.getrandbits(6)
        down_trigger_utility.setup_trigger_utility_down(trigger_type, dut_id)
        expected_trigger = down_trigger_utility.get_propagate_trigger()
        original_verify_trigger_received = hbicc.rc.verify_trigger_received
        hbicc.rc.verify_trigger_received = Mock(return_value=[0, 0])
        self.trun_on_pattern_run_on_simulator_slices()
        down_trigger_utility.software_keep_alive_resume()
        self.assertEqual(expected_trigger, self.simulator.patgen.slices[0].register_read(0x4c))
        hbicc.rc.verify_trigger_received = original_verify_trigger_received

    def trun_on_pattern_run_on_simulator_slices(self):
        for i in range(5):
            self.simulator.patgen.slices[i].running_pattern = True

    def test_software_keep_alive_resume_with_down_trigger_not_sent(self):
        hbicc = self.tester.get_hbicc_undertest('Hbicc')
        down_trigger_utility = TriggerUtility(hbicc, [0, 1, 2, 3, 4])
        trigger_type = random.getrandbits(4)
        dut_id = random.getrandbits(6)
        down_trigger_utility.setup_trigger_utility_down(trigger_type, dut_id)
        original_send_trigger = hbicc.rc.send_trigger
        hbicc.rc.send_trigger = Mock()
        self.simulator.patgen.slices[0].registers[0x4C] = 0
        self.trun_on_pattern_run_on_simulator_slices()
        with self.assertRaises(Exception):
            down_trigger_utility.software_keep_alive_resume()
        hbicc.rc.send_trigger = original_send_trigger

    def test_trigger_utility_init(self):
        trigger_utility = TriggerUtility(Mock(), Mock())
        self.assertEqual(0, trigger_utility.target_resource)
        self.assertEqual(0x3f, trigger_utility.dut_id)
        self.assertEqual(0, trigger_utility.software_trigger_up_only)
        self.assertEqual(0, trigger_utility.target_domain)
        self.assertEqual(0, trigger_utility.count_value)
        self.assertEqual(0, trigger_utility.trigger_type)

    def test_trigger_utility_random_init(self):
        trigger_word = TriggerWord()
        trigger_word.target_resource = random.getrandbits(6)
        trigger_word.dut_id = random.getrandbits(6)
        trigger_word.software_trigger_up_only = random.getrandbits(1)
        trigger_word.target_resource = random.getrandbits(4)
        trigger_word.count_value = random.getrandbits(4)
        trigger_word.trigger_type = random.getrandbits(4)
        trigger_utility = TriggerUtility(Mock(), Mock(), trigger_word.value)
        self.assertEqual(trigger_word.target_resource, trigger_utility.target_resource)
        self.assertEqual(trigger_word.dut_id, trigger_utility.dut_id)
        self.assertEqual(trigger_word.software_trigger_up_only , trigger_utility.software_trigger_up_only)
        self.assertEqual(trigger_word.target_domain, trigger_utility.target_domain)
        self.assertEqual(trigger_word.count_value, trigger_utility.count_value)
        self.assertEqual(trigger_word.trigger_type, trigger_utility.trigger_type)

    def _func_do_nothing(self, *args, **kwargs):
        pass



