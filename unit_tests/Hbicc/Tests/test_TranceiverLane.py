################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.instrument.patgen import PatGen
from Hbicc.Tests.TransceiverLane import Diagnostics

time.sleep = lambda x: None


class TranceiverLaneTests(HbiccTestCase):

    def test_DirectedPatgenTxToRingMultiplierRxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x94] = 0
        self.simulator.ring_multiplier.slices[0].registers[0x98] = 0
        diagnostics.DirectedPatgenTxToRingMultiplierRxTest(test_iterations)
        expected_message = 'PATGEN ---> RINGMULTIPLIER Transceiver Communication Successful for all {} iterations for all slices/lanes. Total Pattern Count'.format(
            test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPatgenTxToRingMultiplierRxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x94] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x98] = 0
        diagnostics.DirectedPatgenTxToRingMultiplierRxTest(test_iterations)
        expected_message = 'PATGEN ---> RINGMULTIPLIER Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def check_for_message(self, expected_message, calls):
        for args, kwargs in calls:
            log_level, message = args
            if expected_message in message:
                break
        else:
            call_strings = '\n'.join([str(x) for x in calls])
            self.fail(f'"{expected_message}" not in\n{call_strings}')

    def get_diagnostics(self):
        diagnostics = Diagnostics()
        diagnostics.Log = Mock()
        diagnostics.setUp(tester=self.tester)
        return diagnostics

    def test_DirectedRingMultiplierTxToPatgenRxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.patgen.slices[0].registers[0x94] = 0
        self.simulator.patgen.slices[0].registers[0x98] = 0
        diagnostics.DirectedRingMultiplierTxToPatgenRxTest(test_iterations)
        expected_message = 'RINGMULTIPLIER ---> PATGEN Transceiver Communication Successful for all {} iterations for all slices/lanes. Total Pattern Count'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedRingMultiplierTxToPatgenRxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.patgen.slices[0].registers[0x94] = 0
        self.simulator.patgen.slices[0].registers[0x98] = 1
        diagnostics.DirectedRingMultiplierTxToPatgenRxTest(test_iterations)
        expected_message = 'RINGMULTIPLIER ---> PATGEN Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedRingMultiplierTxToPSDB0PinMultiplier0RxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[0].pin_multiplier[0].slices[i].registers[0x108] = 0
            self.simulator.psdb[0].pin_multiplier[0].slices[i].registers[0x108] = 0
        diagnostics.DirectedRingMultiplierTxToPSDB0PinMultiplier0RxTest(test_iterations)
        expected_message = 'RINGMULTIPLIER ---> PSDB_0_PIN_0 Transceiver Communication Successful for all {} iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedRingMultiplierTxToPSDB0PinMultiplier0RxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[0].pin_multiplier[0].slices[3].registers[0x108] = 10
        self.simulator.psdb[0].pin_multiplier[0].slices[3].registers[0x108] = 10
        diagnostics.DirectedRingMultiplierTxToPSDB0PinMultiplier0RxTest(test_iterations)
        expected_message = 'RINGMULTIPLIER ---> PSDB_0_PIN_0 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier0TxToRingMultiplierRxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x110] = 0
        self.simulator.ring_multiplier.slices[0].registers[0x114] = 0
        diagnostics.DirectedPSDB0PinMultiplier0TxToRingMultiplierRxTest(test_iterations)
        expected_message = 'PSDB_0_PIN_0 ---> RINGMULTIPLIER Transceiver Communication Successful for all {} iterations for all slices/lanes'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier0TxToRingMultiplierRxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x110] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x114] = 10
        diagnostics.DirectedPSDB0PinMultiplier0TxToRingMultiplierRxTest(test_iterations)
        expected_message = 'PSDB_0_PIN_0 ---> RINGMULTIPLIER Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedRingMultiplierTxToPSDB1PinMultiplier0RxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[1].pin_multiplier[0].slices[i].registers[0x108] = 0
            self.simulator.psdb[1].pin_multiplier[0].slices[i].registers[0x108] = 0
        diagnostics.DirectedRingMultiplierTxToPSDB1PinMultiplier0RxTest(test_iterations)
        expected_message = 'RINGMULTIPLIER ---> PSDB_1_PIN_0 Transceiver Communication Successful for all {} iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedRingMultiplierTxToPSDB1PinMultiplier0RxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[1].pin_multiplier[0].slices[4].registers[0x108] = 10
        self.simulator.psdb[1].pin_multiplier[0].slices[4].registers[0x108] = 10
        diagnostics.DirectedRingMultiplierTxToPSDB1PinMultiplier0RxTest(test_iterations)
        expected_message = 'RINGMULTIPLIER ---> PSDB_1_PIN_0 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier0TxToRingMultiplierRxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x130] = 0
        self.simulator.ring_multiplier.slices[0].registers[0x134] = 0
        diagnostics.DirectedPSDB1PinMultiplier0TxToRingMultiplierRxTest(test_iterations)
        expected_message = 'PSDB_1_PIN_0 ---> RINGMULTIPLIER Transceiver Communication Successful for all {} iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier0TxToRingMultiplierRxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x130] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x134] = 10
        diagnostics.DirectedPSDB1PinMultiplier0TxToRingMultiplierRxTest(test_iterations)
        expected_message = 'PSDB_1_PIN_0 ---> RINGMULTIPLIER Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier0TxToPinMultiplier1RxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[0].pin_multiplier[1].slices[i].registers[0xF0] = 0
            self.simulator.psdb[0].pin_multiplier[1].slices[i].registers[0xF4] = 0
        diagnostics.DirectedPSDB0PinMultiplier0TxToPinMultiplier1RxTest(test_iterations)
        expected_message = 'PSDB_0_PIN_0 ---> PSDB_0_PIN_1 Transceiver Communication Successful for all {} iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier0TxToPinMultiplier1RxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[0].pin_multiplier[1].slices[3].registers[0xF0] = 10
        self.simulator.psdb[0].pin_multiplier[1].slices[3].registers[0xF4] = 10
        diagnostics.DirectedPSDB0PinMultiplier0TxToPinMultiplier1RxTest(test_iterations)
        expected_message = 'PSDB_0_PIN_0 ---> PSDB_0_PIN_1 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier1TxToPinMultiplier0RxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[0].pin_multiplier[0].slices[i].registers[0xF0] = 0
            self.simulator.psdb[0].pin_multiplier[0].slices[i].registers[0xF4] = 0
        diagnostics.DirectedPSDB0PinMultiplier1TxToPinMultiplier0RxTest(test_iterations)
        expected_message = 'PSDB_0_PIN_1 ---> PSDB_0_PIN_0 Transceiver Communication Successful for all {} iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier1TxToPinMultiplier0RxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[0].pin_multiplier[0].slices[0].registers[0xF0] = 10
        self.simulator.psdb[0].pin_multiplier[0].slices[0].registers[0xF4] = 10
        diagnostics.DirectedPSDB0PinMultiplier1TxToPinMultiplier0RxTest(test_iterations)
        expected_message = 'PSDB_0_PIN_1 ---> PSDB_0_PIN_0 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier0TxToPinMultiplier1RxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[1].pin_multiplier[1].slices[i].registers[0xF0] = 0
            self.simulator.psdb[1].pin_multiplier[1].slices[i].registers[0xF4] = 0
        diagnostics.DirectedPSDB1PinMultiplier0TxToPinMultiplier1RxTest(test_iterations)
        expected_message = 'PSDB_1_PIN_0 ---> PSDB_1_PIN_1 Transceiver Communication Successful for all 1 iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier0TxToPinMultiplier1RxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[1].pin_multiplier[1].slices[1].registers[0xF0] = 10
        self.simulator.psdb[1].pin_multiplier[1].slices[1].registers[0xF4] = 10
        diagnostics.DirectedPSDB1PinMultiplier0TxToPinMultiplier1RxTest(test_iterations)
        expected_message = 'PSDB_1_PIN_0 ---> PSDB_1_PIN_1 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier1TxToPinMultiplier0RxTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[1].pin_multiplier[0].slices[i].registers[0xF0] = 0
            self.simulator.psdb[1].pin_multiplier[0].slices[i].registers[0xF4] = 0
        diagnostics.DirectedPSDB1PinMultiplier1TxToPinMultiplier0RxTest(test_iterations)
        expected_message = 'PSDB_1_PIN_1 ---> PSDB_1_PIN_0 Transceiver Communication Successful for all {} iterations for all slices/lanes.'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier1TxToPinMultiplier0RxTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[1].pin_multiplier[0].slices[2].registers[0xF0] = 10
        self.simulator.psdb[1].pin_multiplier[0].slices[2].registers[0xF4] = 10
        diagnostics.DirectedPSDB1PinMultiplier1TxToPinMultiplier0RxTest(test_iterations)
        expected_message = 'PSDB_1_PIN_1 ---> PSDB_1_PIN_0 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_read_register_call_check_error_count_pass(self):
        rx_device = PatGen(rc=None)
        rx_register = rx_device.registers.RM_RING_BUS_ERROR_COUNT()
        rx_register.value = 0
        rx_device.read_bar_register = Mock(return_value=rx_register)
        rx_device.Log = Mock()
        rx_device.check_error_count(rx_register)
        self.assertEqual(len(rx_device.Log.call_args_list), 0)

    def test_read_register_call_check_error_count_fail(self):
        rx_device = PatGen(rc=None)
        RxRegister = rx_device.registers.RM_RING_BUS_ERROR_COUNT
        rx_device.read_register_raw = Mock(return_value=23)
        rx_device.Log = Mock()
        rx_device.check_error_count(RxRegister)
        args, kwargs = rx_device.Log.call_args_list[0]
        log_level, message = args
        self.assertIn('Transceiver lane error count set to', message)
        self.assertEqual('ERROR', log_level.upper())

    def test_disable_prbs(self):
        tx_device = PatGen(rc=None)
        tx_register = tx_device.registers.RM_RING_BUS_CONTROL
        tx_device.write_bar_register = Mock()
        tx_device.disable_prbs(tx_register)
        tx_write_register_calls = tx_device.write_bar_register.call_args_list
        self.assertEqual(tx_write_register_calls[0][0][0].EnablePrbs, 0)

    def test_enable_prbs(self):
        tx_device = PatGen(rc=None)
        tx_register = tx_device.registers.RM_RING_BUS_CONTROL
        tx_device.write_bar_register = Mock()
        tx_device.enable_prbs(tx_register)

        tx_write_register_calls = tx_device.write_bar_register.call_args_list
        self.assertEqual(tx_write_register_calls[0][0][0].EnablePrbs, 1)

    def test_DirectedBidirectionalPatgenToRingMultiplierDataTransferTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.patgen.slices[0].registers[0x94] = 10
        self.simulator.patgen.slices[0].registers[0x98] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x94] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x98] = 10
        diagnostics.DirectedBidirectionalPatgenToRingMultiplierDataTransferTest(test_iterations)
        expected_message = 'PATGEN <---> RINGMULTIPLIER Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalPatgenToRingMultiplierDataTransferTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.patgen.slices[0].registers[0x94] = 0
        self.simulator.patgen.slices[0].registers[0x98] = 0
        self.simulator.ring_multiplier.slices[0].registers[0x94] = 0
        self.simulator.ring_multiplier.slices[0].registers[0x98] = 0
        diagnostics.DirectedBidirectionalPatgenToRingMultiplierDataTransferTest(test_iterations)
        expected_message = 'PATGEN <---> RINGMULTIPLIER Transceiver Communication Successful for all {} iterations for all slices/lanes'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalRingMultiplierToPSDB1Pin0DataTransferTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x130] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x134] = 10
        self.simulator.psdb[1].pin_multiplier[0].registers[0xF0] = 10
        self.simulator.psdb[1].pin_multiplier[0].registers[0xF4] = 10
        diagnostics.DirectedBidirectionalRingMultiplierToPSDB1Pin0DataTransferTest(test_iterations)
        expected_message = 'RINGMULTIPLIER <---> PSDB_1_PIN_0 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalRingMultiplierToPSDB0PinMultiplier0DataTransferTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x110] = 10
        self.simulator.ring_multiplier.slices[0].registers[0x114] = 10
        self.simulator.psdb[0].pin_multiplier[0].registers[0xF0] = 10
        self.simulator.psdb[0].pin_multiplier[0].registers[0xF4] = 10
        diagnostics.DirectedBidirectionalRingMultiplierToPSDB0PinMultiplier0DataTransferTest(test_iterations)
        expected_message = 'RINGMULTIPLIER <---> PSDB_0_PIN_0 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalRingMultiplierToPSDB0PinMultiplier0DataTransferTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.ring_multiplier.slices[0].registers[0x110] = 0
        self.simulator.ring_multiplier.slices[0].registers[0x114] = 0
        self.simulator.psdb[0].pin_multiplier[0].registers[0xF0] = 0
        self.simulator.psdb[0].pin_multiplier[0].registers[0xF4] = 0
        diagnostics.DirectedBidirectionalRingMultiplierToPSDB0PinMultiplier0DataTransferTest(test_iterations)
        expected_message = 'RINGMULTIPLIER <---> PSDB_0_PIN_0 Transceiver Communication Successful for all {} iterations for all slices/lanes'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalPSDB1PinMultiplier0ToPSDB1PinMultiplier1Test_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[1].pin_multiplier[0].slices[1].registers[0xF0] = 10
        self.simulator.psdb[1].pin_multiplier[0].slices[1].registers[0xF4] = 10
        self.simulator.psdb[1].pin_multiplier[1].slices[1].registers[0xF0] = 10
        self.simulator.psdb[1].pin_multiplier[1].slices[1].registers[0xF4] = 10
        diagnostics.DirectedBidirectionalPSDB1PinMultiplier0ToPSDB1PinMultiplier1Test(test_iterations)
        expected_message = 'PSDB_1_PIN_0 <---> PSDB_1_PIN_1 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalPSDB0PinMultiplier0ToPSDB0PinMultiplier1Test_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[0].pin_multiplier[0].slices[1].registers[0xF0] = 10
        self.simulator.psdb[0].pin_multiplier[0].slices[1].registers[0xF4] = 10
        self.simulator.psdb[0].pin_multiplier[1].slices[1].registers[0xF0] = 10
        self.simulator.psdb[0].pin_multiplier[1].slices[1].registers[0xF4] = 10
        diagnostics.DirectedBidirectionalPSDB0PinMultiplier0ToPSDB0PinMultiplier1Test(test_iterations)
        expected_message = 'PSDB_0_PIN_0 <---> PSDB_0_PIN_1 Transceiver Communication Failed.'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalPSDB0PinMultiplier0ToPSDB0PinMultiplier1Test_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for pm in range(2):
            for slice in range(0, 5):
                self.simulator.psdb[0].pin_multiplier[pm].slices[slice].registers[0xF0] = 0
                self.simulator.psdb[0].pin_multiplier[pm].slices[slice].registers[0xF4] = 0
        diagnostics.DirectedBidirectionalPSDB0PinMultiplier0ToPSDB0PinMultiplier1Test(test_iterations)
        expected_message = 'PSDB_0_PIN_0 <---> PSDB_0_PIN_1 Transceiver Communication Successful for all {} iterations for all slices/lanes'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalPSDB1Pin0ToPSDB1Pin1DataTransferTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        self.simulator.psdb[1].pin_multiplier[0].registers[0xF0] = 0
        self.simulator.psdb[1].pin_multiplier[0].registers[0xF4] = 0
        self.simulator.psdb[1].pin_multiplier[1].registers[0xF0] = 0
        self.simulator.psdb[1].pin_multiplier[1].registers[0xF4] = 0
        diagnostics.DirectedBidirectionalPSDB1PinMultiplier0ToPSDB1PinMultiplier1Test(test_iterations)
        expected_message = 'PSDB_1_PIN_0 <---> PSDB_1_PIN_1 Transceiver Communication Successful for all {} iterations for all slices/lanes'.format(
                test_iterations)
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedBidirectionalPSDB1Pin0ToPSDB1Pin1DataTransferTest_pass(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[1].pin_multiplier[1].slices[i].registers[0x50] = 0x3F9
        diagnostics.DirectedBidirectionalPSDB1PinMultiplier0ToPSDB1PinMultiplier1Test(test_iterations)
        expected_message = 'PSDB_1_PIN_0 <---> PSDB_1_PIN_1: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x3e9 != 0x3f9 != 0x3e9'
        self.check_for_message(expected_message, diagnostics.Log.call_args_list)

        self.simulator.psdb[1].pin_multiplier[1].registers[0x50] = 0x3E9

    def test_DirectedHbiBidirectionalDataTransferReliabilityTest_pass(self):
        diagnostics = self.get_diagnostics()

        diagnostics.DirectedHbiBidirectionalDataTransferReliabilityTest(minutes=.5)
        expected_messages = ['Running PATGEN <---> RINGMULTIPLIER', 'Running PATGEN <---> RINGMULTIPLIER',
                            'Running RINGMULTIPLIER <---> PSDB_0_PIN_0', 'Running RINGMULTIPLIER <---> PSDB_1_PIN_0',
                            'Running PSDB_0_PIN_0 <---> PSDB_0_PIN_1', 'Running PSDB_1_PIN_0 <---> PSDB_1_PIN_1']
        for expected_message in expected_messages:
            self.check_for_message(expected_message, diagnostics.Log.call_args_list)

    def test_DirectedHbiBidirectionalDataTransferReliabilityTest_fail(self):
        test_iterations = 1
        diagnostics = self.get_diagnostics()
        for i in range(0, 5):
            self.simulator.psdb[1].pin_multiplier[0].slices[i].registers[0x50] = 0x3F9
            self.simulator.psdb[1].pin_multiplier[1].slices[i].registers[0x50] = 0x3F9
            self.simulator.psdb[0].pin_multiplier[0].slices[i].registers[0x50] = 0x3F9
            self.simulator.psdb[0].pin_multiplier[1].slices[i].registers[0x50] = 0x3F9
        self.simulator.ring_multiplier.slices[0].registers[0xD0] = 0x3F9
        self.simulator.ring_multiplier.slices[0].registers[0xC4] = 0x3F9
        self.simulator.ring_multiplier.slices[0].registers[0x84] = 0x3F9
        self.simulator.patgen.slices[0].registers[0x84] = 0x3E9

        diagnostics.DirectedHbiBidirectionalDataTransferReliabilityTest(minutes=.5)
        expected_messages = ['PATGEN <---> RINGMULTIPLIER: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x3e9 != 0x3f9 != 0x3e9'
                             ,'RINGMULTIPLIER <---> PSDB_0_PIN_0: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x3f9 != 0x3e9 != 0x3e9'
                             ,'RINGMULTIPLIER <---> PSDB_1_PIN_0: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x3f9 != 0x3e9 != 0x3e9'
                             ,'PSDB_0_PIN_0 <---> PSDB_0_PIN_1: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x3f9 != 0x3f9 != 0x3e9'
                             ,'PSDB_1_PIN_0 <---> PSDB_1_PIN_1: Downstream and Upstream Status Register do not match expected value of 0x3e9. 0x3f9 != 0x3f9 != 0x3e9']
        for expected_message in expected_messages:
            self.check_for_message(expected_message, diagnostics.Log.call_args_list)
