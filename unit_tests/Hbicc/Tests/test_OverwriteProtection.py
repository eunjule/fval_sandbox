################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import threading
from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Common.fval import Object, skip
from Hbicc.instrument import patgen_register
from Hbicc.instrument.patgen_register import ALARMS1
from Hbicc.testbench.capture_structs import CTV_HEADER_AND_DATA
from Hbicc.testbench.capturefailures import Capture, CaptureBase, PIN_ERRROR_BLOCK, STREAM_BLOCK
from Hbicc.Tests.Alarms import CTV_DATA_STREAM_BLOCK_SIZE
from Hbicc.Tests.OverwriteProtection import KeepAliveCTVDataStreamChecker, KeepAliveCTVHeaderStreamChecker, \
    KeepAliveTraceStreamChecker, OverwriteProtection, OverwriteProtectionTestConstants, PCALL_NUM, RETURN_NUM, \
    UNCONDITIONAL_BRANCH_NUM

from unit_tests.Hbicc.Tests.test_Alarms import OverflowAlarmProtectionUnitTestConstants

TEST_REPEAT = 2

class OverwriteProtectionUnitTestConstants(OverwriteProtectionTestConstants, OverflowAlarmProtectionUnitTestConstants):
    CTV_DATA_BLOCK_NUM_FOR_1K_BYTE = 1024 // CTV_DATA_STREAM_BLOCK_SIZE
    CTV_HEADER_BLOCK_NUM_FOR_1K_BYTE = 1024 // STREAM_BLOCK
    TRACE_BLOCK_NUM_FOR_1K_BYTE = 1024 // STREAM_BLOCK
    ERROR_BLOCK_NUM_FOR_1K_BYTE = 1024 // PIN_ERRROR_BLOCK
    MAX_CTV_DATA_BLOCK_NUM_LIMIT_UNIT_TEST = 64
    MAX_CTV_HEADER_BLOCK_NUM_LIMIT_UNIT_TEST = 256
    MAX_BLOCK_NUM_UNIT_TEST = 64

@skip('Debuggin in progress')
class OverwriteProtectionTest(HbiccTestCase, OverwriteProtectionUnitTestConstants):
    def setUp(self):
        super().setUp()
        self.overflow_test = OverwriteProtection()
        self.reset_overflow_test()

    def reset_overflow_test(self):
        self.overflow_test.setUp(self.tester)
        self.overflow_test.unit_test = True
        Capture.set_stream_file = Mock
        Capture.write_stream_data_to_file = Mock
        CaptureBase._write_stream_data_to_file = Mock
        self.overflow_test.pattern_helper.is_regression = False

    def test_CTVRowCaptureOverwriteProtection_PatternGetRun(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            ctv_data_num_limit = random.randint(self.CTV_DATA_BLOCK_NUM_FOR_1K_BYTE,
                                                self.MAX_CTV_DATA_BLOCK_NUM_LIMIT_UNIT_TEST)
            ctv_data_num = random.randint(ctv_data_num_limit + self.BLOCK_NUM_FOR_PROTECTED_DATA_LEN,
                                          ctv_data_num_limit + self.MAX_OVERFLOW_BLOCK_NUM)
            self.assertTrue(ctv_data_num > ctv_data_num_limit)
            self.set_up_ctv_data_overflow_protection_test(ctv_data_num, ctv_data_num_limit)
            protected_data = self.overflow_test.data_out_of_bound
            self.overflow_test.DirectedCTVRowCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_capture_stream_size(expected_byte_count=CTV_DATA_STREAM_BLOCK_SIZE * ctv_data_num,
                                            test=self.overflow_test,
                                            stream_size_register=patgen_register.CTV_CAPTURE_DATA_COUNT)
            self.assert_alarm_set('ctv_row_capture_overflow', self.overflow_test)
            self.assert_ctv_data_buffer_is_too_small(byteNum=CTV_DATA_STREAM_BLOCK_SIZE * ctv_data_num,
                                                     test=self.overflow_test)
            self.assert_whether_eob_exist(test=self.overflow_test, expected_eob=False)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
            self.assert_data_equal(protected_data, self.overflow_test.data_after_run)

    def test_CTVRowCaptureOverwriteProtection_NotOverflow(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            ctv_data_num_limit = random.randint(self.CTV_DATA_BLOCK_NUM_FOR_1K_BYTE + 1,
                                                self.MAX_CTV_DATA_BLOCK_NUM_LIMIT_UNIT_TEST)
            ctv_data_num = random.randint(self.CTV_DATA_BLOCK_NUM_FOR_1K_BYTE, ctv_data_num_limit - 1)
            self.assertTrue(ctv_data_num < ctv_data_num_limit)
            self.set_up_ctv_data_overflow_protection_test(ctv_data_num, ctv_data_num_limit)
            protected_data = self.overflow_test.data_out_of_bound
            self.overflow_test.assert_capture_stream_will_overflow = Mock()
            with self.assertRaises(Exception):
                self.overflow_test.DirectedCTVRowCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_capture_stream_size(expected_byte_count=CTV_DATA_STREAM_BLOCK_SIZE * ctv_data_num,
                                            test=self.overflow_test,
                                            stream_size_register=patgen_register.CTV_CAPTURE_DATA_COUNT)
            self.assert_alarm_not_set('ctv_row_capture_overflow', self.overflow_test)
            self.assert_whether_eob_exist(test=self.overflow_test, expected_eob=True)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
            self.assert_data_equal(protected_data, self.overflow_test.data_after_run)

    def test_CTVRowCaptureOverwriteProtection_DataOverWritten(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            ctv_data_num_limit = random.randint(self.CTV_DATA_BLOCK_NUM_FOR_1K_BYTE,
                                                self.MAX_CTV_DATA_BLOCK_NUM_LIMIT_UNIT_TEST)
            ctv_data_num = random.randint(ctv_data_num_limit + self.BLOCK_NUM_FOR_PROTECTED_DATA_LEN,
                                          ctv_data_num_limit + self.MAX_OVERFLOW_BLOCK_NUM)
            self.assertTrue(ctv_data_num > ctv_data_num_limit)
            self.set_up_ctv_data_overflow_protection_test(ctv_data_num, ctv_data_num_limit)
            protected_data = self.overflow_test.data_out_of_bound
            self.disable_ctv_data_overwrite_protection()
            with self.assertRaises(Exception):
                self.overflow_test.DirectedCTVRowCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_capture_stream_size(expected_byte_count=CTV_DATA_STREAM_BLOCK_SIZE * ctv_data_num,
                                            test=self.overflow_test,
                                            stream_size_register=patgen_register.CTV_CAPTURE_DATA_COUNT)
            self.assert_alarm_set('ctv_row_capture_overflow', self.overflow_test)
            self.assert_ctv_data_buffer_is_too_small(byteNum=CTV_DATA_STREAM_BLOCK_SIZE * ctv_data_num,
                                                     test=self.overflow_test)
            self.assert_whether_eob_exist(test=self.overflow_test, expected_eob=False)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
            self.assert_data_not_equal(protected_data, self.overflow_test.data_after_run)
        self.enable_simulator_ctv_data_overwrite_protection()

    def disable_ctv_data_overwrite_protection(self):
        model_patgen = self.overflow_test.env.hbicc.model.patgen
        simulator_patgen = self.simulator.patgen
        for patgen_slice in model_patgen.slices:
            patgen_slice.ctv_data_overwrite_protection = False
        for patgen_slice in simulator_patgen.slices:
            patgen_slice.ctv_data_overwrite_protection = False

    def enable_simulator_ctv_data_overwrite_protection(self):
        simulator_patgen = self.simulator.patgen
        for patgen_slice in simulator_patgen.slices:
            patgen_slice.ctv_data_overwrite_protection = True

    def test_CTVHeaderStreamCaptureOverwriteProtection_PatternGetRun(self):

        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            ctv_header_num_limit = random.randint(self.CTV_HEADER_BLOCK_NUM_FOR_1K_BYTE,
                                                  self.MAX_CTV_HEADER_BLOCK_NUM_LIMIT_UNIT_TEST) * 2
            ctv_header_num = random.randint(ctv_header_num_limit + 1,
                                            ctv_header_num_limit + self.MAX_OVERFLOW_BLOCK_NUM)\
                             * len(self.overflow_test.slices)
            self.assertTrue(ctv_header_num > ctv_header_num_limit)
            self.set_up_ctv_header_overflow_protection_test(ctv_header_num, ctv_header_num_limit)
            protected_data = self.overflow_test.data_out_of_bound
            self.overflow_test.DirectedCTVHeaderCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_ctv_header_capture_stream_size(total_count=ctv_header_num, test=self.overflow_test)
            self.assert_ctv_header_overflow_alarm_set(self.overflow_test)
            self.assert_ctv_header_buffer_is_too_small(byteNum=STREAM_BLOCK * ctv_header_num, test=self.overflow_test)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
            self.assert_data_equal(protected_data, self.overflow_test.data_after_run)

    def test_CTVHeaderStreamCaptureOverwriteProtection_DataOverWritten(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            ctv_header_num_limit = random.randint(self.CTV_HEADER_BLOCK_NUM_FOR_1K_BYTE,
                                                  self.MAX_CTV_HEADER_BLOCK_NUM_LIMIT_UNIT_TEST) * 2
            ctv_header_num = random.randint(ctv_header_num_limit + 1,
                                            ctv_header_num_limit + self.MAX_OVERFLOW_BLOCK_NUM)\
                             * len(self.overflow_test.slices)
            self.assertTrue(ctv_header_num > ctv_header_num_limit)
            self.set_up_ctv_header_overflow_protection_test(ctv_header_num, ctv_header_num_limit)
            protected_data = self.overflow_test.data_out_of_bound
            self.disable_ctv_header_overwrite_protection()
            with self.assertRaises(Exception):
                self.overflow_test.DirectedCTVHeaderCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_ctv_header_capture_stream_size(total_count=ctv_header_num, test=self.overflow_test)
            self.assert_ctv_header_buffer_is_too_small(byteNum=STREAM_BLOCK * ctv_header_num, test=self.overflow_test)
            self.assert_ctv_header_overflow_alarm_set(self.overflow_test)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
            self.assert_data_not_equal(protected_data, self.overflow_test.data_before_run)
        self.enable_simulator_ctv_header_overwrite_protection()

    def assert_data_not_equal(self, expected_data, observed_data):
        self.assertNotEqual(expected_data, observed_data)

    def disable_ctv_header_overwrite_protection(self):
        model_patgen = self.overflow_test.env.hbicc.model.patgen
        simulator_patgen = self.simulator.patgen
        for patgen_slice in model_patgen.slices:
            patgen_slice.ctv_header_overwrite_protection = False
        for patgen_slice in simulator_patgen.slices:
            patgen_slice.ctv_header_overwrite_protection = False

    def enable_simulator_ctv_header_overwrite_protection(self):
        simulator_patgen = self.simulator.patgen
        for patgen_slice in simulator_patgen.slices:
            patgen_slice.ctv_header_overwrite_protection = True

    def test_get_slice_header_distribution(self):
        self.assert_ctv_header_count_distribution([], 0, {})
        self.assert_ctv_header_count_distribution([], 10, {})
        self.assert_ctv_header_count_distribution([1], 0, {1: 0})
        self.assert_ctv_header_count_distribution([2], 10, {2: 10})
        self.assert_ctv_header_count_distribution([1, 3], 0, {1: 0, 3: 0})
        self.assert_ctv_header_count_distribution([2, 4], 0, {2: 0, 4: 0})
        self.assert_ctv_header_count_distribution([3, 4], 101, {3: 64, 4: 37})
        self.assert_ctv_header_count_distribution([1, 2, 3], 256, {1: 128, 2: 64, 3: 64})
        self.assert_ctv_header_count_distribution([1, 2, 3], 576, {1: 256, 2: 192, 3: 128})
        self.assert_ctv_header_count_distribution([1, 2, 3], 8, {1: 8, 2: 0, 3: 0})
        self.assert_ctv_header_count_distribution([1, 2, 3], 531, {1: 211, 2: 192, 3: 128})
        self.assert_ctv_header_count_distribution([2, 3], 10, {2: 10, 3: 0})
        self.assert_ctv_header_count_distribution([2, 3], 11, {2: 11, 3: 0})
        self.assert_ctv_header_count_distribution([0, 1, 2, 3, 4], 0, {0: 0, 1: 0, 2: 0, 3: 0, 4: 0})
        self.assert_ctv_header_count_distribution([0, 1, 2, 3, 4], 11, {0: 11, 1: 0, 2: 0, 3: 0, 4: 0})
        self.assert_ctv_header_count_distribution([0, 1, 2, 3, 4], 531, {0: 147, 1: 128, 2: 128, 3: 64, 4: 64})
        self.assert_ctv_header_count_distribution([0, 1, 2, 3, 4], 530, {0: 146, 1: 128, 2: 128, 3: 64, 4: 64})
        self.assert_ctv_header_count_distribution([0, 1, 2, 3, 4], 17, {0: 17, 1: 0, 2: 0, 3: 0, 4: 0})
        self.overflow_test.pattern_helper.distribute_bits = \
            self.overflow_test.pattern_helper.distribute_bits_all_header_stream_to_one_slice
        self.assert_ctv_header_count_distribution([0, 1, 2, 3, 4], 530, {0: 0, 1: 530, 2: 0, 3: 0, 4: 0})

    def assert_ctv_header_count_distribution(self, active_slices, total_count, expected_counts):
        self.overflow_test.pattern_helper.slice_channel_sets_combo.clear()
        self.overflow_test.pattern_helper.create_slice_channel_set_pattern_combo(slices=active_slices)
        self.overflow_test.pattern_helper.set_up_ctv_header_distribution()
        self.assertEqual(expected_counts,
                         self.overflow_test.pattern_helper.get_slice_ctv_header_distribution(total_count))

    def set_up_ctv_data_overflow_protection_test(self, ctv_data_num, ctv_data_num_limit):
        self.overflow_test.ctv_data_num_limit = ctv_data_num_limit
        self.overflow_test.ctv_data_num = ctv_data_num
        self.overflow_test.pattern_helper.write_fails_to_csv = Mock
        self.overflow_test.pattern_helper.log_xcvr_count_status = Mock

    def set_up_ctv_header_overflow_protection_test(self, ctv_header_num, ctv_header_num_limit):
        self.overflow_test.ctv_header_num_limit = ctv_header_num_limit
        self.overflow_test.ctv_header_num = ctv_header_num
        self.overflow_test.pattern_helper.write_fails_to_csv = Mock
        self.overflow_test.pattern_helper.log_xcvr_count_status = Mock
        self.overflow_test.pattern_helper.is_regression = False

    def assert_capture_stream_size(self, expected_byte_count, test, stream_size_register):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            count = capture.get_stream_total_size(capture.patgen, stream_size_register)
            actual_byte_count = count * STREAM_BLOCK
            self.assertEqual(expected_byte_count, actual_byte_count)

    def assert_alarm_set(self, alarm, test):
        self.assertTrue(test.env.hbicc.hbicc_alarms.is_alarm_set({alarm}))
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            model_simulator_alarms1 = test.env.hbicc.model.patgen.slices[slice_object.index].registers[ALARMS1.ADDR]
            tester_simulator_alarms1 = test.env.hbicc.pat_gen.read_slice_register(ALARMS1, slice_object.index).value
            self.assertEqual(model_simulator_alarms1, tester_simulator_alarms1)
            self.assertTrue(getattr(ALARMS1(tester_simulator_alarms1), alarm))

    def assert_ctv_header_overflow_alarm_set(self, test):
        self.assertTrue(test.env.hbicc.hbicc_alarms.is_alarm_set({'ctv_header_capture_overflow'}))
        expected_alarm_list = test.pattern_helper.get_expected_ctv_header_overflow_map(
            ctv_header_num=test.ctv_header_num, ctv_header_num_limit=test.ctv_header_num_limit)
        for slice_index, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            model_simulator_alarms1 = test.env.hbicc.model.patgen.slices[slice_object.index].registers[ALARMS1.ADDR]
            tester_simulator_alarms1 = test.env.hbicc.pat_gen.read_slice_register(ALARMS1, slice_object.index).value
            self.assertEqual(model_simulator_alarms1, tester_simulator_alarms1)
            observed_alarm = getattr(ALARMS1(tester_simulator_alarms1), 'ctv_header_capture_overflow')
            expected_alarm = expected_alarm_list[slice_index]
            self.assertEqual(expected_alarm, observed_alarm)

    def assert_ctv_data_buffer_is_too_small(self, byteNum, test):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            end_address = capture.patgen.read_slice_register(patgen_register.CTV_DATA_STREAM_END_ADDRESS_REG,
                                                             capture.slice.index).Data << 6
            start_address = capture.patgen.read_slice_register(patgen_register.CTV_DATA_STREAM_START_ADDRESS_REG,
                                                               capture.slice.index).Data << 6
            self.assertTrue(end_address < start_address + byteNum)

    def assert_ctv_header_buffer_is_too_small(self, byteNum, test):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            end_address = capture.patgen.read_slice_register(patgen_register.CTV_HEADER_STREAM_END_ADDRESS_REG,
                                                             capture.slice.index).Data << 6
            start_address = capture.patgen.read_slice_register(patgen_register.CTV_HEADER_STREAM_START_ADDRESS_REG,
                                                               capture.slice.index).Data << 6
            self.assertTrue(end_address < start_address + byteNum)

    def assert_whether_eob_exist(self, test, expected_eob):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            actual_count, register_count, data = slice_object.capture.get_ctv_block_count_and_data()
            observed_eob = False
            self.assertTrue(data)
            for block_num in range(actual_count):
                start = block_num * STREAM_BLOCK
                end = start + STREAM_BLOCK
                block = CTV_HEADER_AND_DATA(data[start: end])
                if block.end_of_burst:
                    observed_eob = True
            self.assertEqual(expected_eob, observed_eob)

    def assert_data_equal(self, expected_data, actual_data):
        for data in actual_data:
            self.assertEqual(expected_data, data)

    def assert_ctv_header_capture_stream_size(self, total_count, test):
        slices_count_num = test.pattern_helper.get_slice_ctv_header_distribution(total_count)
        for slice_index, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            capture = slice_object.capture
            count = capture.get_stream_total_size(capture.patgen, patgen_register.CTV_CAPTURE_HEADER_COUNT)
            total_bytes = count * STREAM_BLOCK
            self.assertEqual(total_bytes, slices_count_num[slice_index] * STREAM_BLOCK)

    def test_DirectedTraceCaptureOverwriteProtectionTest_PatternGetRun(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            trace_num_limit = random.randrange(max(self.TRACE_BLOCK_NUM_FOR_1K_BYTE,
                                               PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                               self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            trace_num = trace_num_limit + random.randint(1, self.MAX_OVERFLOW_BLOCK_NUM)
            self.assertTrue(trace_num > trace_num_limit)
            self.set_up_trace_overwrite_protection_test(trace_num_limit, trace_num)
            protected_data = self.overflow_test.data_out_of_bound
            self.overflow_test.DirectedTraceCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_capture_stream_size(expected_byte_count=trace_num * STREAM_BLOCK, test=self.overflow_test,
                                            stream_size_register=patgen_register.TRACE_STREAM_COUNT)
            self.assert_trace_buffer_is_too_small(expected_data_length=trace_num * STREAM_BLOCK,
                                                  test=self.overflow_test)
            self.assert_alarm_set('trace_capture_overflow', self.overflow_test)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
            self.assert_data_equal(protected_data, self.overflow_test.data_after_run)
            self.assert_simulator_overwrite_protected_trace_count(trace_num_limit)

    def assert_simulator_overwrite_protected_trace_count(self, expected_count):
        for slice_index in self.overflow_test.slices:
            slice_simulator = self.simulator.patgen.slices[slice_index]
            self.assertEqual(expected_count, slice_simulator._trace_overwrite_protected_count)

    def test_DirectedTraceCaptureOverwriteProtectionTest_OverwriteHappens(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            trace_num_limit = random.randrange(max(self.TRACE_BLOCK_NUM_FOR_1K_BYTE,
                                                   PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                               self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            trace_num = trace_num_limit + random.randint(1, self.MAX_OVERFLOW_BLOCK_NUM)
            self.assertTrue(trace_num > trace_num_limit)
            self.set_up_trace_overwrite_protection_test(trace_num_limit, trace_num)
            self.disable_overwrite_protection('trace_overwrite_protection')
            self.overflow_test.check_log_alarm = Mock
            with self.assertRaises(Exception):
                self.overflow_test.DirectedTraceCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_capture_stream_size(expected_byte_count=trace_num * STREAM_BLOCK, test=self.overflow_test,
                                            stream_size_register=patgen_register.TRACE_STREAM_COUNT)
            self.assert_trace_buffer_is_too_small(expected_data_length=trace_num * STREAM_BLOCK,
                                                  test=self.overflow_test)
            self.assert_alarm_set('trace_capture_overflow', self.overflow_test)
        self.enable_simulator_overwrite_protection('trace_overwrite_protection')

    def disable_overwrite_protection(self, protection_flag):
        model_patgen = self.overflow_test.env.hbicc.model.patgen
        simulator_patgen = self.simulator.patgen
        for patgen_slice in model_patgen.slices:
            setattr(patgen_slice, protection_flag, False)
        for patgen_slice in simulator_patgen.slices:
            setattr(patgen_slice, protection_flag, False)

    def enable_simulator_overwrite_protection(self, protection_flag):
        simulator_patgen = self.simulator.patgen
        for patgen_slice in simulator_patgen.slices:
            setattr(patgen_slice, protection_flag, True)

    def test_DirectedTraceCaptureOverwriteProtectionTest_NoOverflow(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            trace_num_limit = random.randrange(max(self.TRACE_BLOCK_NUM_FOR_1K_BYTE,
                                               PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                               self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            trace_num = trace_num_limit - random.randint(1, trace_num_limit
                                                         - (PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM))
            self.assertTrue(trace_num < trace_num_limit)
            self.assertTrue(trace_num >= PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM)
            self.set_up_trace_overwrite_protection_test(trace_num_limit, trace_num)
            self.overflow_test.assert_capture_stream_will_overflow = Mock
            self.overflow_test.check_is_data_equal = Mock
            with self.assertRaises(Exception):
                self.overflow_test.DirectedTraceCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_capture_stream_size(expected_byte_count=trace_num * STREAM_BLOCK, test=self.overflow_test,
                                            stream_size_register=patgen_register.TRACE_STREAM_COUNT)
            self.assert_alarm_not_set('trace_capture_overflow', self.overflow_test)

    def test_DirectedTraceCaptureOverwriteProtectionTest_randomOverflow(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            trace_num_limit = random.randrange(max(self.TRACE_BLOCK_NUM_FOR_1K_BYTE,
                                                   PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                               self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            trace_num = random.randint(max(self.TRACE_BLOCK_NUM_FOR_1K_BYTE,
                                       PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM),
                                       self.MAX_BLOCK_NUM_UNIT_TEST)
            self.assertTrue(trace_num >= PCALL_NUM + UNCONDITIONAL_BRANCH_NUM + RETURN_NUM)
            self.set_up_trace_overwrite_protection_test(trace_num_limit, trace_num)
            self.overflow_test.assert_capture_stream_will_overflow = Mock
            self.overflow_test.check_is_data_equal = Mock
            if trace_num < trace_num_limit:
                with self.assertRaises(Exception):
                    self.overflow_test.DirectedTraceCaptureOverwriteProtectionTest(1)
                self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
                self.assert_alarm_not_set('trace_capture_overflow', self.overflow_test)
            else:
                self.overflow_test.DirectedTraceCaptureOverwriteProtectionTest(1)
                self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
                self.assert_alarm_set('trace_capture_overflow', self.overflow_test)

            self.assert_capture_stream_size(expected_byte_count=trace_num * STREAM_BLOCK, test=self.overflow_test,
                                            stream_size_register=patgen_register.TRACE_STREAM_COUNT)

    def assert_alarm_not_set(self, alarm, test):
        self.assertFalse(test.env.hbicc.hbicc_alarms.is_alarm_set({alarm}))
        for slice, slice_object in test.pattern_helper.slice_channel_sets_combo.items():
            model_simulator_alarms1 = test.env.hbicc.model.patgen.slices[slice_object.index].registers[ALARMS1.ADDR]
            tester_simulator_alarms1 = test.env.hbicc.pat_gen.read_slice_register(ALARMS1, slice_object.index).value
            self.assertEqual(model_simulator_alarms1, tester_simulator_alarms1)
            self.assertFalse(ALARMS1(tester_simulator_alarms1).ctv_row_capture_overflow)

    def set_up_trace_overwrite_protection_test(self, trace_num_limit, trace_num):
        self.overflow_test.trace_num_limit = trace_num_limit
        self.overflow_test.trace_num = trace_num

    def assert_trace_buffer_is_too_small(self, expected_data_length, test):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            end_address = capture.patgen.read_slice_register(patgen_register.TRACE_STREAM_END_ADDRESS_REG,
                                                             capture.slice.index).Data << 6
            start_address = capture.patgen.read_slice_register(patgen_register.TRACE_STREAM_START_ADDRESS_REG,
                                                               capture.slice.index).Data << 6
            self.assertTrue(end_address < start_address + expected_data_length)

    def test_DirectedErrorPMCaptureOverwriteProtectionTest_PatternGetRun(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            error_num_limit = random.randrange(self.ERROR_BLOCK_NUM_FOR_1K_BYTE, self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            error_num = error_num_limit + random.randrange(2, self.MAX_OVERFLOW_BLOCK_NUM, 2)
            self.assertTrue(error_num > error_num_limit)
            self.set_up_error_overwrite_protection_test(error_num_limit, error_num)
            protected_data = self.overflow_test.data_out_of_bound
            self.overflow_test.DirectedErrorPMCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_error_capture_stream_size(error_num * PIN_ERRROR_BLOCK, self.overflow_test)
            self.assert_pin_error_buffer_is_too_small(error_num * PIN_ERRROR_BLOCK, self.overflow_test)
            for i in range(4):
                error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(i)
                self.assert_alarm_set(error_pm_x_capture_overflow, self.overflow_test)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)

    def test_DirectedErrorPMCaptureOverwriteProtectionTest_OverwriteHappens(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            error_num_limit = random.randrange(self.ERROR_BLOCK_NUM_FOR_1K_BYTE, self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            error_num = error_num_limit + random.randrange(2, self.MAX_OVERFLOW_BLOCK_NUM, 2)
            self.assertTrue(error_num > error_num_limit)
            self.set_up_error_overwrite_protection_test(error_num_limit, error_num)
            protected_data = self.overflow_test.data_out_of_bound
            self.disable_overwrite_protection('pin_error_overwrite_protection')
            with self.assertRaises(Exception):
                self.overflow_test.DirectedErrorPMCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_error_capture_stream_size(error_num * PIN_ERRROR_BLOCK, self.overflow_test)
            self.assert_pin_error_buffer_is_too_small(error_num * PIN_ERRROR_BLOCK, self.overflow_test)
            for i in range(4):
                error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(i)
                self.assert_alarm_set(error_pm_x_capture_overflow, self.overflow_test)
            self.assert_data_equal(protected_data, self.overflow_test.data_before_run)
        self.enable_simulator_overwrite_protection('pin_error_overwrite_protection')

    def test_DirectedErrorPMCaptureOverwriteProtectionTest_NoOverflow(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            error_num_limit = random.randrange(self.ERROR_BLOCK_NUM_FOR_1K_BYTE, self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            error_num = error_num_limit - random.randrange(2, error_num_limit - 2, 2)
            self.assertTrue(error_num < error_num_limit)
            self.set_up_error_overwrite_protection_test(error_num_limit, error_num)
            self.overflow_test.assert_capture_stream_will_overflow = Mock
            with self.assertRaises(Exception):
                self.overflow_test.DirectedErrorPMCaptureOverwriteProtectionTest(1)
            self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
            self.assert_error_capture_stream_size(error_num * PIN_ERRROR_BLOCK, self.overflow_test)
            for i in range(4):
                error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(i)
                self.assert_alarm_not_set(error_pm_x_capture_overflow, self.overflow_test)

    def test_DirectedErrorPMCaptureOverwriteProtectionTest_RandomOverflow(self):
        for _ in range(TEST_REPEAT):
            self.reset_overflow_test()
            error_num_limit = random.randrange(self.ERROR_BLOCK_NUM_FOR_1K_BYTE, self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            error_num = random.randrange(self.TRACE_BLOCK_NUM_FOR_1K_BYTE, self.MAX_BLOCK_NUM_UNIT_TEST, 2)
            self.set_up_error_overwrite_protection_test(error_num_limit, error_num)
            self.overflow_test.assert_capture_stream_will_overflow = Mock
            if error_num_limit > error_num:
                with self.assertRaises(Exception):
                    self.overflow_test.DirectedErrorPMCaptureOverwriteProtectionTest(1)
                self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
                for i in range(4):
                    error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(i)
                    self.assert_alarm_not_set(error_pm_x_capture_overflow, self.overflow_test)
            else:
                self.overflow_test.DirectedErrorPMCaptureOverwriteProtectionTest(1)
                self.assertTrue(self.overflow_test.pattern_helper.slice_channel_sets_combo)
                for i in range(4):
                    error_pm_x_capture_overflow = 'error_pm_{}_capture_overflow'.format(i)
                    self.assert_alarm_set(error_pm_x_capture_overflow, self.overflow_test)
            self.assert_error_capture_stream_size(error_num * PIN_ERRROR_BLOCK, self.overflow_test)

    def set_up_error_overwrite_protection_test(self, error_num_limit, error_num):
        self.overflow_test.error_num_limit = error_num_limit
        self.overflow_test.error_num = error_num

    def assert_error_capture_stream_size(self, expected_byte_count, test):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            for reg_index, pm in capture.pms.items():
                count = capture.get_stream_total_size(capture.patgen, patgen_register.ERROR_STREAM_PM_COUNT, reg_index)
                actual_byte_count = count * STREAM_BLOCK
                self.assertEqual(expected_byte_count, actual_byte_count)

    def assert_pin_error_buffer_is_too_small(self, byteNum, test):
        for slice_object in test.pattern_helper.slice_channel_sets_combo.values():
            capture = slice_object.capture
            for reg_index, pms in capture.pms.items():
                end_address = capture.patgen.read_slice_register(patgen_register.ERROR_STREAM_PM_END_ADDRESS_REG,
                                                                 capture.slice.index,
                                                                 reg_index).Data << 6
                start_address = capture.patgen.read_slice_register(patgen_register.ERROR_STREAM_PM_START_ADDRESS_REG,
                                                                   capture.slice.index,
                                                                   reg_index).Data << 6
                self.assertTrue(end_address < start_address + byteNum)

    def test_trigger_capture_processing(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = \
                self.set_up_pin_error_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            down_trigger_received = self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            self.assertTrue(down_trigger_received)
            if self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception raised in trigger_sending_thread')
            Object.Log = original_object_Log

    def set_up_pin_error_keep_alive_unit_test(self):
        self.overflow_test.reset_pin_error_keepalive_test()
        self.overflow_test.pattern_helper.is_regression = False
        self.overflow_test.set_up_keepalive_triggers()
        expected_up_trigger = self.overflow_test.trigger_utility_up.construct_trigger_word()
        expected_down_trigger = self.overflow_test.trigger_utility_down.get_propagate_trigger()
        trigger_sending_thread = \
            threading.Thread(target=self.overflow_test.software_pin_error_stream_check_during_keep_alive)
        self.overflow_test.overwrite_protection_test_set_up_before_launch({}, '')
        return expected_down_trigger, expected_up_trigger, trigger_sending_thread

    def test_trigger_capture_processing_capture_num_1_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = \
                self.set_up_pin_error_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_1 + 1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: capture count mismatch:', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 1: capture count mismatch:\" not found in Log')
            Object.Log = original_object_Log

    def test_trigger_capture_processing_capture_num_2_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = \
                self.set_up_pin_error_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_2 + 1, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: capture count mismatch:', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 2: capture count mismatch:\" not found in Log')
            Object.Log = original_object_Log

    def test_trigger_capture_processing_first_alarm_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = \
                self.set_up_pin_error_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_1, False)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                print(f'iteration: {_}:\n{self.overflow_test.Log.call_args_list}')
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: Alarm was not set for error_pm_',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 1: Alarm was not set for error_pm_:\" '
                          'not found in Log')
            Object.Log = original_object_Log

    def test_trigger_capture_processing_second_alarm_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = \
                self.set_up_pin_error_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_error_capture_num_and_alarm(self.overflow_test.error_num_2, False)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: Alarm was not set for error_pm_',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: \"Pattern 2: Alarm was not set for error_pm_:\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def wait_for_down_trigger(self, expected_down_trigger, retry_iteration=100000000):
        for i in range(retry_iteration):
            observed_down_trigger = self.simulator.patgen.slices[0].registers.get(0x4C, 0)
            if observed_down_trigger == expected_down_trigger:
                self.simulator.patgen.slices[0].registers.update({0x4C: 0})
                return True
        else:
            return False

    def process_triggers_in_active_slices(self):
        for _ in range(2):
            for slice_simulators in self.simulator.patgen.slices:
                if slice_simulators.slice in self.overflow_test.slices:
                    slice_simulators.process_trigger_received()

    def push_up_trigger_to_rc_fifo_from_patgen(self, expected_up_trigger):
        self.simulator.rc._software_trigger_fifo.push(expected_up_trigger, 0x10)

    def start_virtual_pattern_run(self):
        for slice_simulator in self.simulator.patgen.slices:
            pattern_control_register = patgen_register.PATTERN_CONTROL(slice_simulator.registers[0x164])
            pattern_control_register.PatternRunning = 1
            slice_simulator.registers[0x164] = pattern_control_register.value
            slice_simulator.running_pattern = True
        self.overflow_test.pattern_helper.passing_slices_after_pre_stage = self.overflow_test.slices
        for slice_simulator in self.simulator.patgen.slices:
            slice_simulator.reset_counters_and_overflow_alarms()

    def finish_virtual_pattern_run(self):
        for slice_simulator in self.simulator.patgen.slices:
            slice_simulator.registers[0x160] = self.overflow_test.pattern_helper.end_status
            pattern_control_register = patgen_register.PATTERN_CONTROL(slice_simulator.registers[0x164])
            pattern_control_register.PatternRunning = 0
            slice_simulator.registers[0x164] = pattern_control_register.value

    def set_simulator_error_capture_num_and_alarm(self, package_num, set_alarm):
        for slice_simualtor in self.simulator.patgen.slices:
            for pm_index in range(4):
                slice_simualtor._pin_error_count[pm_index] = package_num
                if set_alarm:
                    slice_simualtor._set_alarms1('error_pm_{}_capture_overflow'.format(pm_index))

    def check_for_log_level(self, expected_log_level, calls):
        for args, kwargs in calls:
            log_level, message = args
            if expected_log_level == log_level:
                print(f'an {expected_log_level} was raised in test with message: {message}')
                return True
        else:
            return False

    def check_for_message(self, expected_message, calls):
        for args, kwargs in calls:
            log_level, message = args
            if expected_message in message:
                return True
        else:
            print(f'messgae: {expected_message} wasn\'t raised in test')
            return False

    def test_software_stream_check_during_keep_alive_ctv_data(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVDataStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception raised in trigger_sending_thread')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_data_num_1_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVDataStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_1 + 1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: ctv data count mismatch:', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 1: ctv data count mismatch::\" not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_data_num_2_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVDataStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_2 + 1, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: ctv data count mismatch:', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 2: ctv data count mismatch::\" not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_data_first_alarm_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVDataStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_1, False)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: Alarm was not set for ctv_row_capture_overflow',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: \"Pattern 1: Alarm was not set for ctv_row_capture_overflow\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_data_second_alarm_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVDataStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_data_capture_num_and_alarm(self.overflow_test.package_num_2, False)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: Alarm was not set for ctv_row_capture_overflow',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: \"Pattern 2: Alarm was not set for ctv_row_capture_overflow\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def set_up_keep_alive_unit_test(self):
        self.overflow_test.stream_keepalive_checker.reset_keep_alive_test()
        self.overflow_test.pattern_helper.is_regression = False
        self.overflow_test.set_up_keepalive_triggers()
        expected_up_trigger = self.overflow_test.trigger_utility_up.construct_trigger_word()
        expected_down_trigger = self.overflow_test.trigger_utility_down.get_propagate_trigger()
        trigger_sending_thread = threading.Thread(target=self.overflow_test.software_stream_check_during_keep_alive)
        self.overflow_test.stream_keepalive_checker.test_set_up({}, '')
        self.overflow_test.pattern_helper.set_up_ctv_header_distribution()
        return expected_down_trigger, expected_up_trigger, trigger_sending_thread

    def set_simulator_ctv_data_capture_num_and_alarm(self, capture_count, alarm_set):
        for slice_simulator in self.simulator.patgen.slices:
            slice_simulator._ctv_data_count = capture_count * 4
            if alarm_set:
                slice_simulator._set_alarms1('ctv_row_capture_overflow')

    def test_software_stream_check_during_keep_alive_trace(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveTraceStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception raised in trigger_sending_thread')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_trace_num_1_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveTraceStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_1 + 1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: trace register count mismatch:', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 1: trace register count mismatch:\" not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_trace_num_2_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveTraceStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_2 + 1, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: trace register count mismatch:', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 2: trace register count mismatch:\" not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_trace_first_alarm_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveTraceStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_1, False)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_2, True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: Alarm was not set for trace_capture_overflow',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: \"Pattern 1: Alarm was not set for trace_capture_overflow\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_trace_second_alarm_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveTraceStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_1, True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_trace_capture_num_and_alarm(self.overflow_test.package_num_2, False)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: Alarm was not set for trace_capture_overflow',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: \"Pattern 2: Alarm was not set for trace_capture_overflow\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def set_simulator_trace_capture_num_and_alarm(self, capture_count, alarm_set):
        for slice_simulator in self.simulator.patgen.slices:
            slice_simulator._trace_count = capture_count
            if alarm_set:
                slice_simulator._set_alarms1('trace_capture_overflow')

    def test_software_stream_check_during_keep_alive_ctv_header(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVHeaderStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_1,
                                                                self.overflow_test.package_num_limit_1,
                                                                True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_2,
                                                                self.overflow_test.package_num_limit_2,
                                                                True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception raised in trigger_sending_thread')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_header_num1_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVHeaderStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_header_capture_num_fail_and_alarm(self.overflow_test.package_num_1,
                                                                self.overflow_test.package_num_limit_1,
                                                                True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_2,
                                                                self.overflow_test.package_num_limit_2,
                                                                True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: ctv header register count mismatch:',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 1: ctv header register count mismatch:\" '
                          'not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_header_num2_check_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVHeaderStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_1,
                                                                     self.overflow_test.package_num_limit_1,
                                                                     True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_header_capture_num_fail_and_alarm(self.overflow_test.package_num_2,
                                                                self.overflow_test.package_num_limit_2,
                                                                True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: ctv header register count mismatch:',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Expected Exception message: \"Pattern 2: ctv header register count mismatch:\" '
                          'not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_header_first_alarm_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVHeaderStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_1,
                                                                self.overflow_test.package_num_limit_1,
                                                                False)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_2,
                                                                self.overflow_test.package_num_limit_2,
                                                                True)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 1: Alarm was not set for ctv_header_capture_overflow',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: \"Pattern 1: Alarm was not set for ctv_header_capture_overflow\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def test_software_stream_check_during_keep_alive_ctv_header_second_alarm_fail(self):
        for _ in range(TEST_REPEAT):
            original_object_Log = Object.Log
            Object.Log = Mock()
            self.overflow_test.stream_keepalive_checker = KeepAliveCTVHeaderStreamChecker(self.overflow_test)
            expected_down_trigger, expected_up_trigger, trigger_sending_thread = self.set_up_keep_alive_unit_test()

            trigger_sending_thread.start()
            self.start_virtual_pattern_run()
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_1,
                                                                self.overflow_test.package_num_limit_1,
                                                                True)
            self.push_up_trigger_to_rc_fifo_from_patgen(expected_up_trigger)
            self.wait_for_down_trigger(expected_down_trigger)
            self.set_simulator_ctv_header_capture_num_and_alarm(self.overflow_test.package_num_2,
                                                                self.overflow_test.package_num_limit_2,
                                                                False)
            self.finish_virtual_pattern_run()
            trigger_sending_thread.join()

            if not self.check_for_log_level('error', self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail('Exception not raised in trigger_sending_thread')
            if not self.check_for_message('Pattern 2: Alarm was not set for ctv_header_capture_overflow',
                                          self.overflow_test.Log.call_args_list):
                Object.Log = original_object_Log
                self.fail(f'Expected Exception message: '
                          f'\"Pattern 2: Alarm was not set for ctv_header_capture_overflow\" '
                          f'not found in Log')
            Object.Log = original_object_Log

    def set_simulator_ctv_header_capture_num_and_alarm(self, capture_count, capture_limit, alarm_set):
        ctv_header_distribution = self.overflow_test.pattern_helper.get_slice_ctv_header_distribution(capture_count)
        alarm_map = self.overflow_test.pattern_helper.get_expected_ctv_header_overflow_map(capture_count, capture_limit)
        for slice_simulator in self.simulator.patgen.slices:
            if slice_simulator.slice in self.overflow_test.slices:
                slice_simulator._ctv_header_count = ctv_header_distribution[slice_simulator.slice]
                if alarm_set and alarm_map[slice_simulator.slice]:
                    slice_simulator._set_alarms1('ctv_header_capture_overflow')

    def set_simulator_ctv_header_capture_num_fail_and_alarm(self, capture_count, capture_limit, alarm_set):
        ctv_header_distribution = self.overflow_test.pattern_helper.get_slice_ctv_header_distribution(capture_count)
        alarm_map = self.overflow_test.pattern_helper.get_expected_ctv_header_overflow_map(capture_count, capture_limit)
        for slice_simulator in self.simulator.patgen.slices:
            if slice_simulator.slice in self.overflow_test.slices:
                slice_simulator._ctv_header_count = ctv_header_distribution[slice_simulator.slice] + 1
                if alarm_set and alarm_map[slice_simulator.slice]:
                    slice_simulator._set_alarms1('ctv_header_capture_overflow')
