# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.VectorCompression import Mix


class VectorCompressionTests(HbiccTestCase):
    def setUp(self):
        super().setUp()
        self.test_mix=Mix()
        self.test_mix.setUp(tester=self.tester)
        self.test_mix.pattern_helper.is_regression = False

    def test_create_branch_segment(self):
        label='LABEL1'
        branch_segment_str = self.test_mix.create_branch_segment(label)
        self.assertTrue(str.find(branch_segment_str, 'LABEL1')>0)

    def test_create_loop_segment(self):
        label='LABEL1'
        branch_segment_str = self.test_mix.create_loop_segment(label)
        self.assertTrue(str.find(branch_segment_str, 'LABEL1:')==0)

    def test_RandomBranchCompressedVectorTest(self):
        self.test_mix.RandomBranchCompressedVectorTest()


if __name__ == '__main__':
    unittest.main()
