# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from .hbicc_testcase import HbiccTestCase
from Common.fval import SkipTest
from Hbicc.Tests.DCTriggerQueue import Diagnostics
from Hbicc.Tests.DCTriggerQueue import Functional
from Hbicc.Tests.DCTriggerQueue import TQTestScenarioAD5684r
from Hbicc.Tests.DCTriggerQueue import SyncDelayDCTQ


TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS = 800
TRIGGER_SEND_DELAY_MICROSECONDS = 50


class DCTriggerQueueTests(HbiccTestCase):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.functional = Functional()
        self.syncdelaydctq = SyncDelayDCTQ()
        self.diagnostics.setUp(tester=self.tester)
        self.functional.setUp(tester=self.tester)
        self.syncdelaydctq.setUp(tester=self.tester)
        self.ad5684r = TQTestScenarioAD5684r(self.functional)
        self.diagnostics.Log = Mock()
        self.functional.Log = Mock()
        self.syncdelaydctq.Log = Mock()

    def test_DirectedPSDB0Ltm4678BasicReadTest_pass(self):
        self.diagnostics.DirectedPSDB0Ltm4678BasicReadTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0Ltm4678BasicWriteTest_pass(self):
        self.diagnostics.DirectedPSDB0Ltm4678BasicWriteTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0Ltm4678BasicWriteTest_fail(self):
        ltm4678 = self.simulator.ring_multiplier.ltm4678
        ltm4678.pmbus_read = Mock(return_value=0xFFFF)
        self.diagnostics.DirectedPSDB0Ltm4678BasicWriteTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)
        
    def test_DirectedPSDB0Ltm4678BasicReadTest_fail(self):
        ltm4678 = self.simulator.ring_multiplier.ltm4678
        ltm4678.pmbus_read = Mock(return_value=0xFFFF)
        self.diagnostics.DirectedPSDB0Ltm4678BasicReadTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678BasicWriteTest_pass(self):
        self.diagnostics.DirectedPSDB1Ltm4678BasicWriteTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678BasicWriteTest_fail(self):
        ltm4678 = self.simulator.ring_multiplier.ltm4678
        ltm4678.pmbus_read = Mock(return_value=0xFFFF)
        self.diagnostics.DirectedPSDB1Ltm4678BasicWriteTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678BasicReadTest_pass(self):
        self.diagnostics.DirectedPSDB1Ltm4678BasicReadTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678BasicReadTest_check_if_skipped(self):
        transceiver_diagnostic = Diagnostics()
        transceiver_diagnostic.env = Mock()
        transceiver_diagnostic.env.hbicc.psdb_1_pin_0 = None
        with self.assertRaises(SkipTest):
            transceiver_diagnostic.DirectedPSDB1Ltm4678BasicReadTest()

    def test_DirectedPSDB0Ltc2975BasicReadTest_pass(self):
        self.diagnostics.DirectedPSDB0Ltc2975BasicReadTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975BasicReadTest_fail(self):
        ltc2975 = self.simulator.ring_multiplier.ltc2975
        ltc2975.pmbus_read = Mock(return_value=0xFFFF)
        self.diagnostics.DirectedPSDB0Ltc2975BasicReadTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975BasicReadTest_pass(self):
        self.diagnostics.DirectedPSDB1Ltc2975BasicReadTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975BasicReadTest_fail(self):
        ltc2975 = self.simulator.ring_multiplier.ltc2975
        ltc2975.pmbus_read = Mock(return_value=0xFFFF)
        self.diagnostics.DirectedPSDB1Ltc2975BasicReadTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975BasicWriteTest_pass(self):
        self.diagnostics.DirectedPSDB0Ltc2975BasicWriteTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975BasicWriteTest_fail(self):
        ltc2975 = self.simulator.ring_multiplier.ltc2975
        ltc2975.pmbus_read = Mock(return_value=0x25)
        self.diagnostics.DirectedPSDB0Ltc2975BasicWriteTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975BasicWriteTest_pass(self):
        self.diagnostics.DirectedPSDB1Ltc2975BasicWriteTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0VCCREFBasicWriteReadTest_pass(self):
        self.diagnostics.DirectedPSDB0VCCREFBasicWriteReadTest(test_iteration=1)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0VCCREFBasicWriteReadTest_fail(self):
        ad5684r = self.simulator.ring_multiplier.ad5684r
        ad5684r.spi_write_read = Mock(return_value=0xFFFF)
        self.diagnostics.DirectedPSDB0VCCREFBasicWriteReadTest(test_iteration=1)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0Ltm4678VersionAndCapabilityReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB0Ltm4678VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltm4678VersionAndCapabilityReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB0Ltm4678VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678VersionAndCapabilityReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB1Ltm4678VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678VersionAndCapabilityReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB1Ltm4678VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975VersionAndCapabilityReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB0Ltc2975VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975VersionAndCapabilityReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB0Ltc2975VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975VersionAndCapabilityReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB1Ltc2975VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975VersionAndCapabilityReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB1Ltc2975VersionAndCapabilityReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltm4678ScratchWriteReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB0Ltm4678ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltm4678ScratchWriteReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB0Ltm4678ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678ScratchWriteReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB1Ltm4678ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltm4678ScratchWriteReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB1Ltm4678ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975ScratchWriteReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB0Ltc2975ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ltc2975ScratchWriteReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB0Ltc2975ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975ScratchWriteReadViaTqDDRTest_pass(self):
        self.functional.DirectedPSDB1Ltc2975ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ltc2975ScratchWriteReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedPSDB1Ltc2975ScratchWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_end_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_start_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_all_expected_data')
    def test_DirectedLtc2975DelayViaTqDDRTest_pass(self, mock_get_all_expected_data,
                                                   mock_get_tq_process_start_time_microseconds,
                                                   mock_get_tq_process_end_time_microseconds):
        mock_get_all_expected_data.return_value = [100]
        mock_get_tq_process_start_time_microseconds.return_value = 500
        mock_get_tq_process_end_time_microseconds.return_value = 500 + 100 + TRIGGER_SEND_DELAY_MICROSECONDS + \
                                                                 (TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS - 1)
        self.functional.DirectedLtc2975DelayViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_end_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_start_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_all_expected_data')
    def test_DirectedLtc2975DelayViaTqDDRTest_fail(self, mock_get_all_expected_data,
                                                   mock_get_tq_process_start_time_microseconds,
                                                   mock_get_tq_process_end_time_microseconds):
        mock_get_all_expected_data.return_value = [100]
        mock_get_tq_process_start_time_microseconds.return_value = 500
        mock_get_tq_process_end_time_microseconds.return_value = 500 + 100 + TRIGGER_SEND_DELAY_MICROSECONDS + \
                                                                 TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS
        self.functional.DirectedLtc2975DelayViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_end_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_start_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_all_expected_data')
    def test_DirectedLtm4678DelayViaTqDDRTest_pass(self, mock_get_all_expected_data,
                                                        mock_get_tq_process_start_time_microseconds,
                                                        mock_get_tq_process_end_time_microseconds):
        mock_get_all_expected_data.return_value = [100]
        mock_get_tq_process_start_time_microseconds.return_value = 500
        mock_get_tq_process_end_time_microseconds.return_value = 500 + 100 + TRIGGER_SEND_DELAY_MICROSECONDS + \
                                                                 (TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS - 1)
        self.functional.DirectedLtm4678DelayViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_end_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_process_start_time_microseconds')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_all_expected_data')
    def test_DirectedLtm4678DelayViaTqDDRTest_fail(self, mock_get_all_expected_data,
                                                        mock_get_tq_process_start_time_microseconds,
                                                        mock_get_tq_process_end_time_microseconds):
        mock_get_all_expected_data.return_value = [100]
        mock_get_tq_process_start_time_microseconds.return_value = 500
        mock_get_tq_process_end_time_microseconds.return_value = 500 + 100 + TRIGGER_SEND_DELAY_MICROSECONDS + \
                                                                 TQ_DELAY_ABSOLUTE_ERROR_THRESHOLD_MICROSECONDS
        self.functional.DirectedLtm4678DelayViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedLtc2975BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest_pass(self):
        self.functional.DirectedLtc2975BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.check_tq_processed_counter')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_processed_count')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.is_tq_processing_complete')
    def test_DirectedLtc2975BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest_fail(
            self, mock_is_tq_processing_complete, mock_get_tq_processed_count, mock_check_tq_processed_counter):
        mock_is_tq_processing_complete.return_value = True
        mock_get_tq_processed_count.return_value = 0
        mock_check_tq_processed_counter.return_value = 62
        self.functional.DirectedLtc2975BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedLtm4678BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest_pass(self):
        self.functional.DirectedLtm4678BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.check_tq_processed_counter')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.get_tq_processed_count')
    @patch('Hbicc.Tests.DCTriggerQueue.TQTestScenario.is_tq_processing_complete')
    def test_DirectedLtm4678BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest_fail(
            self, mock_is_tq_processing_complete, mock_get_tq_processed_count, mock_check_tq_processed_counter):
        mock_is_tq_processing_complete.return_value = True
        mock_get_tq_processed_count.return_value = 0
        mock_check_tq_processed_counter.return_value = 62
        self.functional.DirectedLtm4678BroadcastTriggerBlankDelayTQProcessedCountCheckViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedLtc2975BroadcastTriggerScratchWriteCheckViaTqDDRTest_pass(self):
        self.functional.DirectedLtc2975BroadcastTriggerScratchWriteCheckViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedLtc2975BroadcastTriggerScratchWriteCheckViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedLtc2975BroadcastTriggerScratchWriteCheckViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedLtm4678BroadcastTriggerScratchWriteCheckViaTqDDRTest_pass(self):
        self.functional.DirectedLtm4678BroadcastTriggerScratchWriteCheckViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedLtm4678BroadcastTriggerScratchWriteCheckViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedLtm4678BroadcastTriggerScratchWriteCheckViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedLtc2975VOutWriteReadViaTqDDRTest_pass(self):
        self.functional.DirectedLtc2975VOutWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedLtc2975VOutWriteReadViaTqDDRTest_fail(self):
        self.functional.i2c_data_readback = Mock(return_value=0xFFFF)
        self.functional.DirectedLtc2975VOutWriteReadViaTqDDRTest(test_iteration=1)
        self.check_for_failure(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ad5684rVrefSetReadViaTqDMATest_pass(self):
        self.functional.DirectedPSDB0Ad5684rVrefSetReadViaTqDMATest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ad5684rVrefSetReadViaTqDMATest_pass(self):
        self.functional.DirectedPSDB1Ad5684rVrefSetReadViaTqDMATest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB0Ad5684rVrefSeperateSetReadViaTqDMATest_pass(self):
        self.functional.DirectedPSDB0Ad5684rVrefSeperateSetReadViaTqDMATest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedPSDB1Ad5684rVrefSeperateSetReadViaTqDMATest_pass(self):
        self.functional.DirectedPSDB1Ad5684rVrefSeperateSetReadViaTqDMATest(test_iteration=1)
        self.check_for_success(self.functional.Log.call_args_list)

    def test_DirectedDPSFoldTriggerwithoutSyncDelayTest_pass(self):
        self.syncdelaydctq.DirectedDPSFoldTriggerWithoutSyncDelayTest()
        self.check_for_success(self.syncdelaydctq.Log.call_args_list)

    def test_DirectedDPSFoldTriggerwithoutSyncDelayTest_fail(self):
        self.syncdelaydctq.env.hbicc.ring_multiplier.read_dc_trigger_dut_power_state = Mock(return_value=0)
        self.syncdelaydctq.DirectedDPSFoldTriggerWithoutSyncDelayTest()
        self.check_for_failure(self.syncdelaydctq.Log.call_args_list)

    def check_for_success(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                self.fail(f'Test did not pass as expected: {args} {kwargs}')

    def check_for_failure(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                break
        else:
            self.fail('Test did not fail as expected')