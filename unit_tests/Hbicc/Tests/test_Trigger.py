# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest import skip
from unittest.mock import Mock, patch

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.Triggers import Diagnostics


class TriggerTests(HbiccTestCase):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedRCToRingMultiplierTest_pass(self):
        self.diagnostics.TEST_ITERATIONS = 10
        self.diagnostics.DirectedRCToRingMultiplierTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedRCToRingMultiplierTest_fail(self):
        self.diagnostics.env.hbicc.rc.send_trigger = Mock(side_effect=self.send_trigger)
        self.diagnostics.env.hbicc.ring_multiplier.read_trigger = Mock(side_effect=self.failing_read_trigger)
        self.diagnostics.env.hbicc.rc.Log = Mock()
        self.diagnostics.DirectedRCToRingMultiplierTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_RCToRMLinkFailToComeForRC(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.rc.registers[0x324] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRCToRingMultiplierTest()
            self.check_for_failure(common_log.call_args_list)

    def test_RCToRMLinkFailToStabilize(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.ring_multiplier.slices[0].read_actions[0x4c] = lambda offset: 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRCToRingMultiplierTest()
            self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_RCToRMLinkFailToComeForRM(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.ring_multiplier.slices[0].registers[0x44] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRCToRingMultiplierTest()
            self.check_for_failure(common_log.call_args_list)

    def test_DirectedRingMultiplierToRCTest_pass(self):
        self.diagnostics.TEST_ITERATIONS = 10
        self.diagnostics.DirectedRingMultiplierToRCTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedRingMultiplierToRCTest_fail(self):
        self.diagnostics.env.hbicc.ring_multiplier.send_trigger = Mock(side_effect=self.send_trigger)
        self.diagnostics.env.hbicc.rc.read_trigger = Mock(side_effect=self.failing_read_trigger)
        self.diagnostics.env.hbicc.rc.Log = Mock()
        self.diagnostics.DirectedRingMultiplierToRCTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_RMToRCLinkFailToComeForRC(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.rc.registers[0x324] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRingMultiplierToRCTest()
            self.check_for_failure(common_log.call_args_list)

    def test_RMToRCLinkFailToComeForRM(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.ring_multiplier.slices[0].registers[0x44] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRingMultiplierToRCTest()
            self.check_for_failure(common_log.call_args_list)

    def test_DirectedRCToPatGenTest_pass(self):
        self.diagnostics.TEST_ITERATIONS = 10
        self.diagnostics.DirectedRCToPatgenTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedRCToPatGenTest_fail(self):
        self.diagnostics.env.hbicc.rc.send_trigger = Mock(side_effect=self.send_trigger)
        self.diagnostics.env.hbicc.pat_gen.read_trigger = Mock(side_effect=self.failing_read_trigger)
        self.diagnostics.env.hbicc.rc.Log = Mock()
        self.diagnostics.DirectedRCToPatgenTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_RCToPGLinkFailToComeForRC(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.rc.registers[0x304] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRCToPatgenTest()
            self.check_for_failure(common_log.call_args_list)

    def test_RCToPGLinkFailToComeForPG(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.patgen.slices[0].registers[0x44] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedRCToPatgenTest()
            self.check_for_failure(common_log.call_args_list)

    def test_DirectedPatgenToRCTest_pass(self):
        self.diagnostics.TEST_ITERATIONS = 10
        self.diagnostics.DirectedPatgenToRCTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPatgenToRCTest_fail(self):
        self.diagnostics.env.hbicc.pat_gen.send_trigger = Mock(side_effect=self.send_trigger)
        self.diagnostics.env.hbicc.rc.read_trigger = Mock(side_effect=self.failing_read_trigger)
        self.diagnostics.env.hbicc.rc.Log = Mock()
        self.diagnostics.DirectedPatgenToRCTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_PGToRCLinkFailToComeForRC(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.rc.registers[0x304] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedPatgenToRCTest()
            self.check_for_failure(common_log.call_args_list)

    def test_PGToRCLinkFailToComeForPG(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.patgen.slices[0].registers[0x44] = 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedPatgenToRCTest()
            self.check_for_failure(common_log.call_args_list)

    def test_PGToRCLinkFailToStabilizeForPG(self):
        with patch('Common.fval.Object.Log') as common_log:
            self.simulator.rc.read_actions[0x10c + 32*16] = lambda offset: 0
            self.diagnostics.TEST_ITERATIONS = 10
            self.diagnostics.DirectedPatgenToRCTest()
            self.check_for_failure(self.diagnostics.Log.call_args_list)

    def send_trigger(self, trigger, index=0):
        self.last_trigger = trigger

    def read_trigger(self, index=0):
        return self.last_trigger

    def failing_read_trigger(self, index=0):
        try:
            self.trigger_count += 1
        except AttributeError:
            self.trigger_count = 1
        if self.trigger_count < 103:
            return self.last_trigger
        else:
            return self.last_trigger ^ 0xFFFFFFFF

    def check_for_success(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                print(f'{log_level} is raised: {message}')
                self.fail('Test did not pass as expected')

    def check_for_failure(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            print(f'message: {message}')
            if log_level.lower() == 'error':
                break
        else:
            self.fail('Test did not fail as expected')
