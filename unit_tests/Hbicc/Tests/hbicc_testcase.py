# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time
from unittest import TestCase

from Common.instruments.tester import get_tester_for_unit_test
from Common import hilmon
from Common.hbi_simulator import HbiSimulator


class HbiccTestCase(TestCase):
    def setUp(self):
        self.simulator = HbiSimulator('Simulated HW')
        hilmon.hil = self.simulator

        self.tester = tester = get_tester_for_unit_test()
        tester.discover_all_instruments()
        tester.set_under_test(instrument_name='hbicc', slot=None)


