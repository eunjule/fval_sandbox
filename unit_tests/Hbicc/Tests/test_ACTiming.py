# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
"""
Unit tests for the Dynamic Phase Delay Test module DynamicPhaseDelay.py
"""


from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.ACTiming import Functional

class DynamicPhaseDelayTests(HbiccTestCase):
    """
    Dynamic Phase Delay base module class
    """
    def setUp(self):
        super().setUp()
        self.functional_object = Functional()
        self.functional_object.setUp(tester=self.tester)
        self.functional_object.pattern_helper.is_regression = False
        self.functional_object.slices=[0]

    def test_dynamic_phase_delay_helper_functions(self):
        """
        Test checks all helper functions used for phase delay tests.  Test will fail when simulator results do not match
        or are not able to complete successfully.
        """
        pattern_string = self.functional_object.walking_ones_pattern()
        self.functional_object.set_all_delays()
        self.functional_object.execute_scenario(pattern_string)

    def test_random_pin_helper_functions(self):
        self.functional_object.assign_random_pins()