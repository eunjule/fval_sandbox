################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock

from Hbicc.testbench.capturefailures import Capture, STREAM_BLOCK
from Hbicc.testbench.capture_structs import PIN_ERROR


class CaptureTests(unittest.TestCase):
    def test_get_failure_data_section(self):
        capture = Capture(slice=Mock())
        capture.get_error_stream_data_and_count = self._create_expected_error_stream
        result = capture.get_user_cycles_from_error_stream(pm=0, reg_index=0, section_start=0, section_end=6)
        ref = [1, 1, 2, 2, 3, 3]
        self.assertTrue(result == ref)
        ref = [2, 2]
        result = capture.get_user_cycles_from_error_stream(pm=0, reg_index=0, section_start=2, section_end=4)
        self.assertTrue(result == ref)

    def _create_expected_error_stream(self, *args, **kwargs):
        expected_error_block_list = []
        for cycle in range(1, 4):
            error_block_1 = PIN_ERROR()
            error_block_1.user_cycle = cycle
            expected_error_block_list.append(error_block_1)
            error_block_2 = PIN_ERROR()
            error_block_2.user_cycle = cycle
            expected_error_block_list.append(error_block_2)

        return b''.join(expected_error_block_list), len(b''.join(expected_error_block_list)) // STREAM_BLOCK
