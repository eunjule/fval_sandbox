# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

from random import shuffle
from unittest.mock import patch
from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Common import hilmon as hil
from Hbicc.instrument.hbicc import Hbicc
from Hbicc.instrument.patgen import PatGen
from Hbicc.instrument.pinmultiplier import PinMultiplier
from Hbicc.instrument.psdb import Psdb
from Hbicc.instrument.ringmultiplier import RingMultiplier
from Hbicc.Tests.Blt import Diagnostics


class BltTests(HbiccTestCase):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedExhaustiveAllDevicesTest_pass(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            TEST_ITERATIONS = 30

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)  # ,mock_mainboard_hil,mock_psdb_hil)
            devices = [self.diagnostics.env.hbicc.mainboard, self.diagnostics.env.hbicc.psdb_0,
                       self.diagnostics.env.hbicc.psdb_1]

            for device in devices:
                call_backs = self.hil_call_back(TEST_ITERATIONS, device.name())
                device.read_blt = Mock(side_effect=call_backs)
            self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)
            self.diagnostics.DirectedExhaustiveAllDevicesTest(TEST_ITERATIONS - 1)
            deviceLog = self.diagnostics.Log.call_args_list
            logIndex = 0
            messageLog = ["", "", ""]
            for logIndex in range(len(devices)):
                messageLog[logIndex] = deviceLog[logIndex][0][1]

            for device in devices:
                self.assertIn(
                    '{} BLT board read was successful for {} iterations.'.format(device.name(), TEST_ITERATIONS - 1),
                    messageLog)

    def test_DirectedExhaustiveAllDevicesTest_fail(self):
        with patch('Common.instruments.tester.hil') as mock_tester_hil, \
                patch('Hbirctc.instrument.hbirctc.hil') as mock_hbirctc_hil, \
                patch('Hbicc.instrument.psdb') as mock_psdb_hil, \
                patch('Hbicc.instrument.mainboard') as mock_mainboard_hil, \
                patch('Hbicc.instrument.pinmultiplier.hil') as mock_pinmultiplier_hil, \
                patch('Hbicc.instrument.ringmultiplier.hil') as mock_ringmultiplier_hil, \
                patch('Hbicc.instrument.patgen.hil') as mock_patgen_hil, \
                patch('Rc2.instrument.rc2.hil') as mock_rc_hil, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Hbicc.testbench.env.HbiccEnv') as mock_HbiccEnv, \
                patch('Hbicc.Tests.HbiccTest.HbiccTest') as mock_HbiccTest, \
                patch('Common.instruments.hbi_instrument.HbiInstrument') as mock_hbi_instrument, \
                patch('Hbicc.instrument.patgen_register') as registers, \
                patch('Hbidps.instrument.hbidps') as hbidps_mock:

            TEST_ITERATIONS = 30

            self.mockDevices(mock_tester_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil,
                             mock_patgen_hil, mock_hil_hbidps)  # ,mock_mainboard_hil,mock_psdb_hil)
            devices = [self.diagnostics.env.hbicc.mainboard, self.diagnostics.env.hbicc.psdb_0,
                       self.diagnostics.env.hbicc.psdb_1]

            for device in devices:
                failures = 11
                call_backs = self.hil_call_back(TEST_ITERATIONS, device.name())
                call_backs.extend(self.get_blt_mismatch(device.name(), failures))
                shuffle(call_backs)
                device.read_blt = Mock(side_effect=call_backs)
                self.mock_logging_for_all_cc_fpga_classes(self.diagnostics)
            self.diagnostics.DirectedExhaustiveAllDevicesTest(TEST_ITERATIONS - 1)

            for device in devices:
                error, message = device.Log.call_args_list[0][0]
                self.assertIn('{}: BLT Board read was unsuccessful'.format(device.name()), message)

    def hil_call_back(self, iterations, board='Hbimainboard'):
        hil_blt = hil.BLT()
        hil_blt.VendorName = 'Intel'
        hil_blt.DeviceName = board
        hil_blt.PartNumberAsBuilt = '12334'
        hil_blt.PartNumberCurrent = '12345'
        hil_blt.SerialNumber = '56789'
        hil_blt.ManufactureDate = '11/20/2018'
        return [hil_blt] * iterations

    def get_blt_mismatch(self, board, iterations):
        hil_blt = hil.BLT()
        hil_blt.VendorName = 'NotIntel'
        hil_blt.DeviceName = board
        hil_blt.PartNumberAsBuilt = '1e334'
        hil_blt.PartNumberCurrent = '12345'
        hil_blt.SerialNumber = '56789'
        hil_blt.ManufactureDate = '11/20/2018'
        return [hil_blt] * iterations

    def get_hbicc_instrument(self):
        hbicc = Hbicc()
        hbicc.pat_gen = PatGen()
        hbicc.ring_multiplier = RingMultiplier()
        hbicc.psdb_0 = Psdb()
        hbicc.psdb_1 = Psdb()
        hbicc.psdb_0_pin_0 = PinMultiplier(slot=0, index=0)
        hbicc.psdb_0_pin_1 = PinMultiplier(slot=0, index=1)
        hbicc.psdb_1_pin_0 = PinMultiplier(slot=1, index=0)
        hbicc.psdb_1_pin_1 = PinMultiplier(slot=1, index=1)
        return hbicc

    def mockDevices(self, mock_hil, mock_hbirctc_hil, mock_pinmultiplier_hil, mock_ringmultiplier_hil, mock_patgen_hil,
                    mock_hil_hbidps):  # ,mock_hbimainboard_hil,mock_hbipsdb_hil):
        rcConnectList = [RuntimeError] * 1
        hbirctcConnectList = [''] * 1
        patgenConnectList = [''] * 1
        ringConnectList = [''] * 1
        pingConnectList = [''] * 4
        hbidpsConnectList = [''] * 16
        # hbimainboardConnectList = [''] * 1
        # hbipsdbConnectList = [''] * 2

        deviceConnectList = {'hbirctcConnectList': hbirctcConnectList, 'hbidpsConnectList': hbidpsConnectList,
                             'patgenConnectList': patgenConnectList, 'ringConnectList': ringConnectList,
                             'pingConnectList': pingConnectList, 'rcConnectList': rcConnectList}
        # 'hbimainboardConnectList':hbimainboardConnectList,'hbipsdbConnectList':hbipsdbConnectList}

        mock_hil.rcConnect = Mock(side_effect=deviceConnectList['rcConnectList'])
        mock_hbirctc_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['hbirctcConnectList'])
        mock_pinmultiplier_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['pingConnectList'])
        mock_ringmultiplier_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['ringConnectList'])
        mock_patgen_hil.pciDeviceOpen = Mock(side_effect=deviceConnectList['patgenConnectList'])
        mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=deviceConnectList['hbidpsConnectList'])
        # mock_hbimainboard_hil.hbiMbConnect = Mock(side_effect=deviceConnectList['hbimainboardConnectList'])
        # mock_hbipsdb_hil.hbiPsdbConnect  = Mock(side_effect=deviceConnectList['hbipsdbConnectList'])

    def mock_logging_for_all_cc_fpga_classes(self, instance):
        if instance.env.hbicc.pat_gen:
            instance.env.hbicc.pat_gen.Log = Mock()
        if instance.env.hbicc.ring_multiplier:
            instance.env.hbicc.ring_multiplier.Log = Mock()
        if instance.env.hbicc.psdb_0_pin_0:
            instance.env.hbicc.psdb_0_pin_0.Log = Mock()
        if instance.env.hbicc.psdb_0_pin_1:
            instance.env.hbicc.psdb_0_pin_1.Log = Mock()
        if instance.env.hbicc.psdb_1_pin_0:
            instance.env.hbicc.psdb_1_pin_0.Log = Mock()
        if instance.env.hbicc.psdb_1_pin_1:
            instance.env.hbicc.psdb_1_pin_1.Log = Mock()
        if instance.env.hbicc.psdb_0:
            instance.env.hbicc.psdb_0.Log = Mock()
        if instance.env.hbicc.psdb_1:
            instance.env.hbicc.psdb_1.Log = Mock()
        if instance.env.hbicc:
            instance.env.hbicc.Log = Mock()
        if instance.env.hbicc.mainboard:
            instance.env.hbicc.mainboard.Log = Mock()
        if instance.env.hbicc.psdb_0:
            instance.env.hbicc.psdb_0.Log = Mock()
        if instance.env.hbicc.psdb_1:
            instance.env.hbicc.psdb_1.Log = Mock()
        if instance:
            instance.Log = Mock()

        return instance
