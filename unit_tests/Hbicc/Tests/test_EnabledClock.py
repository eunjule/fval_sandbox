# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.EnabledClock import EnabledClock
from Hbicc.Tests.EnabledClock import FreeRunningClock


class EnabledClockTests(HbiccTestCase):
    def setUp(self):
        super().setUp()
        self.enable_clock = EnabledClock()
        self.free_running_clock = FreeRunningClock()
        self.enable_clock.setUp(tester=self.tester)
        self.enable_clock.pattern_helper.is_regression = False
        self.enable_clock.slices=[0]
        self.free_running_clock.setUp(tester=self.tester)
        self.free_running_clock.pattern_helper.is_regression = False

    def test_CTP_to_check_pin_0(self):
        self.enable_clock.DirectedCTPforPin0Test()

    def test_FRC_period_2(self):
        self.free_running_clock.free_running_clock_evenratio_body(reset_high=1, ratio = 2)
        self.check_free_running_clock_settings(first_edge =1, period = 2)

    def test_free_running_clock_keep_pin(self):
        self.free_running_clock.slices = [0]
        pattern = self.free_running_clock.create_clock_pattern_with_keep_pin(num_cycles_hi=4, num_cycles_lo=4)
        self.free_running_clock.env.hbicc.set_all_FRC_pins(period=8)
        self.free_running_clock.execute_scenario(pattern)

    def test_enabled_clock_directed_body(self):
        reset_high = 1
        num_high_cycles = 2
        num_low_cycles = 2
        self.enable_clock.enabled_clock_directed_body(reset_high = reset_high, num_high_cycles = num_high_cycles, num_low_cycles = num_low_cycles)
        self.check_enabled_clock_settings(num_high_cycles, num_low_cycles, reset_high)


    def check_free_running_clock_settings(self, period, first_edge):
        for pm in self.free_running_clock.env.hbicc.get_pin_multipliers():
            config3expected = self.set_expected_free_running_clock_register(period, first_edge)
            for slice in self.free_running_clock.slices:
                for channel_set in range(4):
                    for pin in range(0, 35, 8):
                        pin_index = pin + channel_set * 35
                        register_type = pm.registers.PER_CHANNEL_CONFIG3
                        config3reg = pm.read_slice_register(register_type, slice, index=pin_index)
                        self.assertEqual(config3reg.FRCMode, config3expected.FRCMode)
                        self.assertEqual(config3reg.FRCDivider, config3expected.FRCDivider)
                        self.assertEqual(config3reg.FRCStartState, config3expected.FRCStartState)

    def set_expected_free_running_clock_register(self, period=100000, first_edge=1):
        config3reg = self.free_running_clock.env.hbicc.psdb_0_pin_0.registers.PER_CHANNEL_CONFIG3()
        config3reg.FRCMode = 1
        config3reg.FRCDivider = period - 2
        config3reg.FRCStartState = first_edge
        config3reg.FRCIsRatio1 = 0
        return config3reg

    def check_enabled_clock_settings(self, num_high_cycles, num_low_cycles, reset_high):
        for pm in self.enable_clock.env.hbicc.get_pin_multipliers():
            config2expected = self.set_expected_enable_clock_register(num_high_cycles, num_low_cycles, reset_high)
            for slice in self.enable_clock.slices:
                for channel_set in range(4):
                    for pin in range(0,35,8):
                        pin_index = pin + channel_set * 35
                        register_type = pm.registers.PER_CHANNEL_CONFIG2
                        config2reg = pm.read_slice_register(register_type, slice, index=pin_index)
                        self.assertEqual(config2reg.EnableClkTesterCycleHigh, config2expected.EnableClkTesterCycleHigh)
                        self.assertEqual(config2reg.EnableClkTesterCycleLow, config2expected.EnableClkTesterCycleLow)
                        self.assertEqual(config2reg.EcFirstEdge, config2expected.EcFirstEdge)


    def set_expected_enable_clock_register(self, HighCycles = 1, LowCycles = 1, first_edge = 1):
        config2expected = self.enable_clock.env.hbicc.psdb_0_pin_0.registers.PER_CHANNEL_CONFIG2()
        config2expected.EnableClkTesterCycleHigh = HighCycles
        config2expected.EnableClkTesterCycleLow = LowCycles
        config2expected.EcFirstEdge = first_edge
        config2expected.EcAddHalfCycle = 0
        config2expected.ECIsRatio1 = 0
        return config2expected