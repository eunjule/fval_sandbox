# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.DDR import Diagnostics


class DDRTests(HbiccTestCase):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.PATGEN_MEMORY_SIZE = 1 << 30
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedDMAToRingMultiplierFullMemoryTest_pass(self):
        self.diagnostics.DirectedDMAToRingMultiplierFullMemoryTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedDMAToRingMultiplierFullMemoryTest_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.ring_multiplier.dma_read = bad_read
        self.diagnostics.DirectedDMAToRingMultiplierFullMemoryTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedDMAToPatgenFullMemoryTest_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.patgen.dma_read = bad_read
        self.diagnostics.DirectedDMAToPatgenFullMemoryStepTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedDMAToPatgenFullMemoryTest_pass(self):
        self.diagnostics.DirectedDMAToPatgenFullMemoryStepTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DMAToPatgenHalfDimmMemoryStep_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.patgen.dma_read = bad_read
        self.diagnostics.DMAToPatgenHalfDimmMemoryStep()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedDMAToRingMultiplierHalfMemoryStepTest_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.ring_multiplier.dma_read = bad_read
        self.diagnostics.DirectedDMAToRingMultiplierHalfMemoryStepTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DMAToRingMultiplierFixedMemorySizeRegressTest_pass(self):
        self.diagnostics.DMAToRingMultiplierFixedMemorySizeRegressTest(test_iteration=25)
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DMAToRingMultiplierFixedMemorySizeRegressTest_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.ring_multiplier.dma_read = bad_read
        self.diagnostics.DMAToRingMultiplierFixedMemorySizeRegressTest(test_iteration=25)
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedDMAToRingMultiplierWalkingPatternTest_pass(self):
        self.diagnostics.DirectedDMAToRingMultiplierWalkingPatternTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedDMAToRingMultiplierWalkingPatternTest_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.ring_multiplier.dma_read = bad_read
        self.diagnostics.DirectedDMAToRingMultiplierWalkingPatternTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DMAToPatgenWalkingPattern_pass(self):
        self.diagnostics.DMAToPatgenWalkingPattern()
        self.check_for_success(self.diagnostics.Log.call_args_list)
        
    def test_DMAToPatgenWalkingPattern_fail(self):
        bad_read = lambda offset, length: b'\x00'
        self.simulator.patgen.dma_read = bad_read
        self.diagnostics.DMAToPatgenWalkingPattern()
        self.check_for_failure(self.diagnostics.Log.call_args_list)
    
    def check_for_success(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                self.fail(f'Test did not pass as expected: {args} {kwargs}')

    def check_for_failure(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                break
        else:
            self.fail('Test did not fail as expected')
