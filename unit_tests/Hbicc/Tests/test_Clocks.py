################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import collections
from unittest.mock import patch, Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.Clocks import Diagnostics


class ClocksTests(HbiccTestCase):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedPatGenClocksTest_pass(self):
        with patch('Common.hilmon.hil') as mock_hil:
            all_clocks = {0xC0: 0x4FFF400,  # PCIe fabric clock
                          0xC4: 0x1FFF444,  # DDR4 clock
                          0xC8: 0x1FFF444,  # DDR4 clock
                          0xCC: 0x1FFF444,  # DDR4 clock
                          0xD0: 0x1FFF444,  # DDR4 clock
                          0xD4: 0x1FFF444,  # DDR4 clock
                          0xDC: 0x6FFF444,  # pin clock PSDB
                          0xF0: 0x2FFF444}  # aurora clock
            def clock_read(index, bar, offset):
                return all_clocks.get(offset, 0)
            mock_hil.hbiMbBarRead = clock_read
            self.diagnostics.DirectedPatGenClocksTest()

    def test_DirectedPatGenClocksTest_fail(self):
        with patch('Common.hilmon.hil') as mock_hil, \
             patch('Common.fval.core.Object.Log') as mock_log:
            all_clocks = collections.OrderedDict(
                {'PCIE_FABRIC_CLOCK': 0x1FFF444, 'DDR4_CH_FABRIC_CLOCK': 0x1FFF444, 'PIN_CLOCK_PSDB0': 0x1FFF444,
                 'PIN_CLOCK_PSDB1': 0x1FFF444, 'AURORA_TRIGGER_FABRIC_CLOCK': 0x1FFF444})
            reads = [value for key, value in all_clocks.items()]
            mock_hil.hbiMbBarRead = Mock(side_effect=(reads * 100))
            self.diagnostics.DirectedPatGenClocksTest()
            call_args_list = mock_log.call_args_list
            self._check_for_logged_error(call_args_list)

    def test_DirectedRingMultiplierClocksTest_pass(self):
        with patch('Common.hilmon.hil') as mock_hil:
            all_clocks = {0xA0: 0x6FFF384,  # GPIO reference clock
                          0xA4: 0x1FFFA00,  # aurora fabric clock
                          0xA8: 0x4FFF500,  # PCIe fabric clock
                          0xAC: 0x4FFF700,  # DDR4 reference clock
                          0xB0: 0x4FFF060}  # SPI clock
            def clock_read(index, bar, offset):
                return all_clocks.get(offset, 0)
            mock_hil.hbiMbBarRead = clock_read
            self.diagnostics.DirectedRingMUltiplierClocksTest()

    def test_DirectedRingMultiplierClocksTest_fail(self):
        with patch('Common.hilmon.hil') as mock_hil, \
             patch('Common.fval.core.Object.Log') as mock_log:
            all_clocks = collections.OrderedDict({'PIN_CLOCK_FREQUENCY': 0x1FFF444,
                                                  'TRIGGER_FABRIC_CLOCK_FREQUQNECY': 0x1FFF444,
                                                  'PCIE_FABRIC_CLOCK_FREQUENCY': 0x1FFF444,
                                                  'DDR4_FABRIC_CLOCK_FREQUENCY': 0x1FFF444,
                                                  'SPI_CLOCK_FREQUNECY': 0x1FFF444})

            reads = [value for key, value in all_clocks.items()]
            mock_hil.hbiMbBarRead = Mock(side_effect=(reads * 100))
            self.diagnostics.DirectedRingMUltiplierClocksTest()
            self._check_for_logged_error(mock_log.call_args_list)

    def test_DirectedPinMultiplierClocksTest_pass(self):
        with patch('Common.hilmon.hil') as mock_hil:
            all_clocks = {0x140: 0x6FFF444, # PIN clock
                          0x144: 0x1FFF400, # 30 Mhz clock
                          0x148: 0x4FFF444, # PCIe fabric clock
                          0x14C: 0x4FFF7F4, # transceiver clock
                          0x150: 0x4FFF704} # phylite clock
            def clock_read(slot, index, bar, offset):
                return all_clocks[offset]
            mock_hil.hbiPsdbBarRead = clock_read
            self.diagnostics.DirectedPinMultiplierClocksTest()

    def test_DirectedPinMultiplierClocksTest_fail(self):
        with patch('Common.hilmon.hil') as mock_hil, \
             patch('Common.fval.core.Object.Log') as mock_log:
            all_clocks = collections.OrderedDict({'PIN_CLOCK_FREQUENCY': 0x1FFF444,
                                                  'CLOCK_FREQUENCY_30MHZ': 0x1FFF444,
                                                  'PCIE_FABRIC_CLOCK_FREQUENCY': 0x1FFF444,
                                                  'TRANSCEIVER_FABRIC_CLOCK_FREQUENCY': 0x1FFF244})
            reads = [value for key, value in all_clocks.items()]
            mock_hil.hbiPsdbBarRead = Mock(side_effect=(reads * 100))
            self.diagnostics.DirectedPinMultiplierClocksTest()
            self._check_for_logged_error(mock_log.call_args_list)

    def _check_for_logged_error(self, call_args_list):
        positional_log_level = [args[0].lower() for args, kwargs in call_args_list]
        if 'error' not in positional_log_level:
            self.fail('No errors logged')