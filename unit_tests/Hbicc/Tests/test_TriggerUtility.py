# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
import random
from unittest.mock import Mock
from Hbicc.testbench.trigger_utility import TriggerUtility

TEST_NUM = 10


class TriggerUtilityTests(unittest.TestCase):
    def test_mask_out_cycle_and_compare_trigger(self):
        observed = 0x200000a8
        expected = 0x20000008
        trigger_utility = TriggerUtility(Mock(), Mock())
        self.assertFalse(trigger_utility.mask_out_cycle_and_check_trigger_mismatch(observed, expected))
        for i in range(TEST_NUM):
            observed = random.getrandbits(32)
            expected = observed & 0xffffff0f
            self.assertFalse(trigger_utility.mask_out_cycle_and_check_trigger_mismatch(observed, expected))

        for i in range(TEST_NUM):
            observed = random.getrandbits(32)
            expected = observed
            while expected == observed:
                expected = random.getrandbits(32)
            self.assertTrue(trigger_utility.mask_out_cycle_and_check_trigger_mismatch(observed, expected))
            expected &= 0xffffff0f
            self.assertTrue(trigger_utility.mask_out_cycle_and_check_trigger_mismatch(observed, expected))

