# INTEL CONFIDENTIAL

# Copyright 2018-2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from .hbicc_testcase import HbiccTestCase
from Hbicc.Tests.VoltageMonitor import Diagnostics


class PSDBVoltageMonitorTests(HbiccTestCase):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.PATGEN_MEMORY_SIZE = 1 << 30
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

    def test_DirectedPSDB0PinMultiplier0VoltageReadFromRegisterTest_pass(self):
        self.diagnostics.DirectedPSDB0PinMultiplier0VoltageReadFromRegisterTest()
        self.check_for_success(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB0PinMultiplier0VoltageReadFromRegisterTest_fail(self):
        self.simulator.psdb[0].pin_multiplier[0].slices[0].registers[0xC8] = 0xFF
        self.diagnostics.DirectedPSDB0PinMultiplier0VoltageReadFromRegisterTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)

    def test_DirectedPSDB1PinMultiplier0VoltageReadFromRegisterTest_fail(self):
        self.simulator.psdb[1].pin_multiplier[0].slices[0].registers[0xC8] = 0xFF
        self.diagnostics.DirectedPSDB1PinMultiplier0VoltageReadFromRegisterTest()
        self.check_for_failure(self.diagnostics.Log.call_args_list)
    
    def check_for_success(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                self.fail(f'Test did not pass as expected: {args} {kwargs}')

    def check_for_failure(self, calls):
        for args, kwargs in calls:
            log_level, message = args
            if log_level.lower() == 'error':
                break
        else:
            self.fail('Test did not fail as expected')


