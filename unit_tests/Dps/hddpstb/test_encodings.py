################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_encodings.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the floating/fixed point encoding/decoding functions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/17/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest

from _build.bin import hddpstbc

class SFPTests(unittest.TestCase):
    def testKnownCases(self):
        self.assertEqual(hddpstbc.EncodeSFP(16.0), 0x4005)
        self.assertEqual(hddpstbc.DecodeSFP(0x4005), 16.0)

#        self.assertEqual(hddpstbc.EncodeSFP(-0.1328125), 0x8889)
        self.assertEqual(hddpstbc.DecodeSFP(0x8889), -0.1328125)

    def testIntegers(self):
        for i in range(-1023, 1024):
            expected = float(i)
            encoded = hddpstbc.EncodeSFP(expected)
            actual = hddpstbc.DecodeSFP(encoded)
            #print('expected={}, encoded=0x{:x}, actual={}'.format(expected, encoded, actual))
            self.assertEqual(actual, expected)

    def testSimpleFractions(self):
        for i in range(-1023, 1024):
            if i != 0:
                expected = 1.0 / i
                encoded = hddpstbc.EncodeSFP(expected)
                actual = hddpstbc.DecodeSFP(encoded)
                #print('expected={}, encoded=0x{:x}, actual={}'.format(expected, encoded, actual))
                self.assertAlmostEqual(actual, expected, places = 3)

class FixedPoint16Tests(unittest.TestCase):
    def testIntegers(self):
        for i in range(-7, 8):
            expected = float(i)
            encoded = hddpstbc.EncodeFixedPoint16(12, expected)
            actual = hddpstbc.DecodeFixedPoint16(12, encoded)
            #print('expected={}, encoded=0x{:x}, actual={}'.format(expected, encoded, actual))
            self.assertEqual(actual, expected)

    def testSimpleFractions(self):
        for i in range(-4095, 4096):
            if i != 0:
                expected = 1.0 / i
                encoded = hddpstbc.EncodeFixedPoint16(12, expected)
                actual = hddpstbc.DecodeFixedPoint16(12, encoded)
                #print('expected={}, encoded=0x{:x}, actual={}'.format(expected, encoded, actual))
                self.assertAlmostEqual(actual, expected, places = 1)

