################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: __init__.py
#-------------------------------------------------------------------------------
#     Purpose: hddpstb module entry. This file just imports other files.
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 10/29/15
#       Group: HDMT FPGA Validation
################################################################################

import importlib.machinery
import os

this_directory = os.path.dirname(os.path.realpath(__file__))
projectpaths_file = os.path.join(this_directory, '..', '..', '..', 'Tools', 'projectpaths.py')
importlib.machinery.SourceFileLoader('projectpaths', projectpaths_file).load_module()
