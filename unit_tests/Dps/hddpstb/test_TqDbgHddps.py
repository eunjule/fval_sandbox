################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_encodings.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the floating/fixed point encoding/decoding functions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/17/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest
from unittest.mock import Mock
from unittest.mock import patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    from Tools import projectpaths
    
from Common import fval
from Dps.hddpstb.TqDbgHddps import TqDebugger

class EnvTests(unittest.TestCase):
    def test_init(self):
        debugger = TqDebugger()
        debugger.Init()

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
