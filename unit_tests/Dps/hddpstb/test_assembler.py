################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_assembler.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the assembler module
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/18/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest

from Dps.hddpstb.assembler import TriggerQueueAssembler


class PatternAssemblerTests(unittest.TestCase):
    def testVectors(self):
        asm = TriggerQueueAssembler()
        asm.LoadString("""\
            SetVoltage rail=0, value=0.95
            SetCurrent rail=0, value=0.0001
            Q cmd=SET_VOLTAGE, arg=0x2, data=`sfp(1.45)`
            Q cmd=SET_CURRENT, arg=0x5, data=0x6
            Q cmd=TIME_DELAY, arg=0x8, data=0x9
        """)
        data = asm.Generate()
        s = []
        for byte in data:
            s.append('{:02x}'.format(byte))
            if len(s) == 4:
                print('0x' + ''.join(reversed(s)))
                s = []
        if len(s) != 0:
            raise Exception('Data length is expected to be a multiple of 4 (i.e. dword aligned)')

