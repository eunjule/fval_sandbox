################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_encodings.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the floating/fixed point encoding/decoding functions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/17/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest
from unittest.mock import Mock
from unittest.mock import patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    from Tools import projectpaths
    
from Common import fval
from Dps.hddpstb.hddpsEnv import HddpsEnv

class EnvTests(unittest.TestCase):
    def test_Initialize(self):
        with patch('Common.instruments.tester.Rc2') as mock_rc2,\
             patch('Common.instruments.tester.Cb2') as mock_cb2, \
             patch('Common.models.hddps.Hddps.ConnectRcModel') as mock_model,\
             patch('Hddps.hddpstb.env.get_tester', return_value=Mock()) as mock_tester,\
             patch('Hddps.hddpstb.env.Hddps') as mock_hddps:
            HddpsEnv.instruments = {}
            HddpsEnv.Initialize()
            
    def test_init(self):
        test_mock = Mock()
        env = HddpsEnv(test_mock)

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
