################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch
from unittest.mock import Mock

from Dps.dpsEnv import DpsEnv

class DpsEnvTests(unittest.TestCase):
    def test_get_dpsenv(self):
        with patch('Dps.dpsEnv.get_tester') as mock_tester:
            myEnv = DpsEnv(test=Mock())
            self.assertIsInstance(myEnv, DpsEnv, 'Unable to get DPS ENV object')

    def test_get_dps_fpgas(self):
        with patch('Dps.dpsEnv.get_tester') as mock_tester:
            myEnv = DpsEnv(test=Mock())
            device0 = Mock(slot=1, subslots=[(Mock(subslot=0)), (Mock(subslot=1))])
            device1 = Mock(slot=2, subslots=[(Mock(subslot=0))])

            myEnv.under_test_dps_devices = [device0, device1]
            myFpgas = myEnv.get_fpgas()
            self.assertEqual([(1, 0), (1, 1), (2, 0)], myFpgas, 'list of fpga tuples is not correct')

    def test_get_dps_duts(self):
        with patch('Dps.dpsEnv.get_tester') as mock_tester:
            myEnv = DpsEnv(test=Mock())
            device0 = Mock(slot=1, subslots=[(Mock(slot=1, subslot=0)), (Mock(slot=1, subslot=1))])
            device1 = Mock(slot=2, subslots=[(Mock(slot=2, subslot=0))])

            myEnv.under_test_dps_devices = [device0, device1]
            myFpgas = myEnv.get_duts()
            result_slots = []
            for fpga in myFpgas:
                result_slots.append(fpga.slot)
            result_subslots = []
            for fpga in myFpgas:
                result_subslots.append(fpga.subslot)

            self.assertEqual(len(myFpgas), 3)
            self.assertEqual([1, 1, 2], result_slots)
            self.assertEqual([0, 1, 0], result_subslots)

    def test_get_instruments_dictionary(self):
        with patch('Dps.dpsEnv.get_tester') as mock_tester:
            myEnv = DpsEnv(test=Mock())
            device0 = Mock(slot=1, subslots=[(Mock(slot=1, subslot=0)), (Mock(slot=1, subslot=1))])
            device1 = Mock(slot=2, subslots=[(Mock(slot=2, subslot=0))])

            myEnv.under_test_dps_devices = [device0, device1]
            myInstruments = myEnv.get_instruments_dictionary()
            self.assertTrue(myInstruments[1] is device0, 'Instrument in slot 1 is not the correct one')
            self.assertTrue(myInstruments[2] is device1, 'Instrument in slot 2 is not the correct one')
