################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock

from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Dps.Tests.DutFoldPolicy import DutFoldPolicy

class DutFoldPolicyTest(unittest.TestCase):

    def test_DirectedNoFaultOrAlarmWithExternalLoadTest_unexpectedrails_folded(self):
        dutfoldpolicy = DutFoldPolicy()
        dutfoldpolicy.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        dutfoldpolicy.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.ConnectRailForce= Mock()
        hvdps_subslot.ConnectRailSense= Mock()
        hvdps_subslot.ConnectExternalLoad= Mock()
        hvdps_subslot.ConnectLvRailSense = Mock()
        dutfoldpolicy.generate_trigger_queue = Mock()
        hvdps_subslot.WriteTriggerQueue = Mock()
        hvdps_subslot.ExecuteTriggerQueue=Mock()
        hvdps_subslot.get_folded_state = Mock(return_value= [0xff,0x3ff])
        hvdps_subslot.get_rail_voltage = Mock(side_effect=[0,0])
        dutfoldpolicy.verify_global_alarm_status = Mock()
        hvdps_subslot.is_close = Mock(return_value= True)
        dutfoldpolicy.get_dut_ov_alarm_status = Mock(return_value=[])
        dutfoldpolicy.Log=Mock()
        dutfoldpolicy.DirectedNoFaultOrAlarmWithExternalLoadTest()
        log_calls = dutfoldpolicy.Log.call_args_list
        for i in range(0, len(log_calls), 3):
            rails_folded_log_level, rails_folded_log_message = log_calls[i][0]
            lv_voltage_log_level, lv_voltage_log_message = log_calls[i + 1][0]
            hv_voltage_log_level, hv_voltage_log_message = log_calls[i + 2][0]
            self.assertEqual(rails_folded_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(lv_voltage_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(hv_voltage_log_level.upper(), 'INFO', 'Log level should be info')

    def test_DirectedNoFaultOrAlarmWithExternalLoadTest_unexpected_dut_ov_alarms_received(self):
        dutfoldpolicy = DutFoldPolicy()
        dutfoldpolicy.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        dutfoldpolicy.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.ConnectRailForce = Mock()
        hvdps_subslot.ConnectRailSense = Mock()
        hvdps_subslot.ConnectExternalLoad = Mock()
        hvdps_subslot.ConnectLvRailSense = Mock()
        dutfoldpolicy.generate_trigger_queue = Mock()
        hvdps_subslot.WriteTriggerQueue = Mock()
        hvdps_subslot.ExecuteTriggerQueue = Mock()
        hvdps_subslot.get_folded_state = Mock(return_value=[0xf7, 0x3f7])
        hvdps_subslot.get_rail_voltage = Mock(side_effect=[0, 0])
        dutfoldpolicy.verify_global_alarm_status = Mock()
        hvdps_subslot.is_close = Mock(return_value=True)
        dutfoldpolicy.get_dut_ov_alarm_status = Mock(return_value=['OvHclcRail2_3'])
        dutfoldpolicy.Log = Mock()
        dutfoldpolicy.DirectedNoFaultOrAlarmWithExternalLoadTest()
        log_calls = dutfoldpolicy.Log.call_args_list
        for i in range(0, len(log_calls), 3):
            rails_folded_log_level, rails_folded_log_message = log_calls[i][0]
            lv_voltage_log_level, lv_voltage_log_message = log_calls[i + 1][0]
            hv_voltage_log_level, hv_voltage_log_message = log_calls[i + 2][0]
            self.assertEqual(rails_folded_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(lv_voltage_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(hv_voltage_log_level.upper(), 'INFO', 'Log level should be info')

    def test_DirectedNoFaultOrAlarmWithExternalLoadTest_incorrect_voltage_received(self):
        dutfoldpolicy = DutFoldPolicy()
        dutfoldpolicy.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        dutfoldpolicy.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.ConnectRailForce = Mock()
        hvdps_subslot.ConnectRailSense = Mock()
        hvdps_subslot.ConnectExternalLoad = Mock()
        hvdps_subslot.ConnectLvRailSense = Mock()
        dutfoldpolicy.generate_trigger_queue = Mock()
        hvdps_subslot.WriteTriggerQueue = Mock()
        hvdps_subslot.ExecuteTriggerQueue = Mock()
        hvdps_subslot.get_folded_state = Mock(return_value=[0xf7, 0x3f7])
        hvdps_subslot.get_rail_voltage = Mock(side_effect=[0, 3])
        dutfoldpolicy.verify_global_alarm_status = Mock()
        hvdps_subslot.is_close = Mock(side_effect=[False,True])
        dutfoldpolicy.get_dut_ov_alarm_status = Mock(return_value=[])
        dutfoldpolicy.Log = Mock()
        dutfoldpolicy.DirectedNoFaultOrAlarmWithExternalLoadTest()
        log_calls = dutfoldpolicy.Log.call_args_list
        for i in range(0, len(log_calls), 3):
            rails_folded_log_level, rails_folded_log_message = log_calls[i][0]
            lv_voltage_log_level, lv_voltage_log_message = log_calls[i + 1][0]
            hv_voltage_log_level, hv_voltage_log_message = log_calls[i + 2][0]
            self.assertEqual(rails_folded_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(lv_voltage_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(hv_voltage_log_level.upper(), 'INFO', 'Log level should be info')

    def test_dut_fold_policy_scenario_unexpected_global_alarms_received(self):
        dutfoldpolicy = DutFoldPolicy()
        rail = 3
        uhc = 5
        alarm_received = 'HclcRailAlarms'
        force_rail_type = 'HV'
        sense_rail_type = 'LVM'
        dutfoldpolicy.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        dutfoldpolicy.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.ConnectRailForce = Mock()
        hvdps_subslot.ConnectLvRailSense = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        dutfoldpolicy.get_ov_uv_alarm_filter_count = Mock(return_value=2)
        dutfoldpolicy.set_ov_uv_alarm_filter_count = Mock()
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.cal_disconnect_force_sense_lines =Mock()
        hvdps_subslot.cal_load_connect = Mock()
        dutfoldpolicy.generate_trigger_queue = Mock()
        hvdps_subslot.WriteTriggerQueue = Mock()
        hvdps_subslot.ExecuteTriggerQueue = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[alarm_received])
        dutfoldpolicy.Log= Mock()
        dutfoldpolicy.dut_fold_policy_scenario_with_cal_load(hvdps_subslot,force_rail_type,sense_rail_type,hvForceVoltage=3,alarm_lv_overvolt_limit=1,alarm_lv_undervolt_limit=-0.2,expected_alarm='No Alarm',fault_alarm = False)

        log_calls = dutfoldpolicy.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')
            self.assertEqual(len(log_calls), 2)

    def test_dut_fold_policy_scenario_no_fault_expected_alarm_received(self):
        dutfoldpolicy = DutFoldPolicy()
        rail = 3
        uhc = 5
        dutfoldpolicy.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        dutfoldpolicy.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.ConnectRailForce = Mock()
        hvdps_subslot.ConnectLvRailSense = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        dutfoldpolicy.get_ov_uv_alarm_filter_count=Mock(return_value=2)
        dutfoldpolicy.set_ov_uv_alarm_filter_count = Mock()
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.cal_disconnect_force_sense_lines =Mock()
        hvdps_subslot.cal_load_connect = Mock()
        dutfoldpolicy.generate_trigger_queue = Mock()
        hvdps_subslot.WriteTriggerQueue = Mock()
        hvdps_subslot.WaitTime=Mock()
        hvdps_subslot.ExecuteTriggerQueue = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        dut_alarm_first_read = hvdps_subslot.registers.DUT_RAIL_ALARM(UserOvAlarm=0)
        hvdps_subslot.ReadRegister = Mock(side_effect=[dut_alarm_first_read])
        hvdps_subslot.check_for_single_dut_rail_alarm = Mock(return_value=True)
        dutfoldpolicy.Log= Mock()
        dutfoldpolicy.DirectedNoFaultUserOverVoltageAlarmTest()
        log_calls = dutfoldpolicy.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(len(log_calls), 1)

    def test_dut_fold_policy_scenario_no_fault_early_ov_alarm_received(self):
        dutfoldpolicy = DutFoldPolicy()
        rail = 3
        uhc = 5
        dutfoldpolicy.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        dutfoldpolicy.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.ConnectRailForce = Mock()
        hvdps_subslot.ConnectLvRailSense = Mock()
        hvdps_subslot.WaitTime = Mock()
        dutfoldpolicy.get_ov_uv_alarm_filter_count=Mock(return_value=2)
        dutfoldpolicy.set_ov_uv_alarm_filter_count = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.cal_disconnect_force_sense_lines =Mock()
        hvdps_subslot.cal_load_connect = Mock()
        dutfoldpolicy.generate_trigger_queue = Mock()
        hvdps_subslot.WriteTriggerQueue = Mock()
        hvdps_subslot.ExecuteTriggerQueue = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        dut_alarm_first_read = hvdps_subslot.registers.DUT_RAIL_ALARM(UserOvAlarm=1)
        hvdps_subslot.ReadRegister = Mock(side_effect=[dut_alarm_first_read])
        hvdps_subslot.check_for_single_dut_rail_alarm = Mock(return_value=True)
        dutfoldpolicy.Log= Mock()
        dutfoldpolicy.DirectedNoFaultUserOverVoltageAlarmTest()
        log_calls = dutfoldpolicy.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')
            self.assertEqual(len(log_calls), 2)

    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()
        inst_subslot.ResetVoltageSoftSpanCode = Mock()
        inst_subslot.ResetCurrentSoftSpanCode = Mock()