################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random
from unittest.mock import patch
from unittest.mock import Mock
from Common import fval
from Common.instruments.dps import hvdps_registers as hvdpsregs
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot

from Dps.Tests.MAX6627 import MAX6627TemperatureInterfacetTestsForHvdps, MAX6627Alarms



LOWEST_IN_RANGE_TEMPERATURE = 10.0625
HIGHEST_IN_RANGE_TEMPERATURE = 60
def encoded_temperature(celsius):
    return int(celsius * 16)
    

class MAX6627Tests(unittest.TestCase):
    def GenerateRandomTemperature(self):
        return random.randrange(encoded_temperature(LOWEST_IN_RANGE_TEMPERATURE),
                                encoded_temperature(HIGHEST_IN_RANGE_TEMPERATURE) + 1)
        
    def GenerateRandomOutOfRangeTemperature(self):
        too_low_temps = list(range(0, encoded_temperature(LOWEST_IN_RANGE_TEMPERATURE)))
        too_high_temps = list(range(encoded_temperature(HIGHEST_IN_RANGE_TEMPERATURE) + 1, 0x1000))
        out_of_range_temps = too_low_temps + too_high_temps
        return random.choice(out_of_range_temps)

    def test_RandomRailForMAX6627ThermalADCTemperatureReadTestInRange(self):
        MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
        my_hvdps = HvdpsSubslot( hvdps= Mock(), slot= 0, subslot= 0)
        random_temperature = self.GenerateRandomTemperature()
        random_temperature_in_celcius = my_hvdps.MAX6627TemperatureDecimalToCelcius(random_temperature)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail =Mock(return_value= [my_hvdps])
        my_hvdps.WriteRegister = Mock()
        MAX6627_object.Log = Mock()
        my_hvdps.ReadRegister = Mock(return_value= Mock(Temperature= random_temperature))
        MAX6627_object.RandomRailForMAX6627ThermalADCTemperatureReadTest()
        self.assertEqual(my_hvdps.WriteRegister.call_count, 4000)
        self.assertEqual(my_hvdps.ReadRegister.call_count, 4000)
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message,'Temperature of Thermal ADC',
                             'Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')


    def test_RandomRailForMAX6627ThermalADCTemperatureReadOutOfRange(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot( hvdps= Mock(), slot= 0, subslot= 0)
            device_id_temperature_list = []
            random_temperature = self.GenerateRandomOutOfRangeTemperature()
            random_temperature_in_celcius = my_hvdps.MAX6627TemperatureDecimalToCelcius(random_temperature)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            for i in range (8):
                device_id_temperature_list.append(Mock(Temperature=random_temperature,MAX6627DeviceSelectField = i))
            my_hvdps.ReadRegister = Mock(side_effect=device_id_temperature_list)
            MAX6627_object.RandomRailForMAX6627ThermalADCTemperatureReadTest(read_attempts = 1)
            self.assertEqual(my_hvdps.WriteRegister.call_count, 8)
            self.assertEqual(my_hvdps.ReadRegister.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0,len(log_calls),1):
                log_level, log_message = log_calls[i][0]
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
                self.assertRegex(log_message,'Temperature of Thermal ADC {} is not in a reasonable range,  Value {} for iteration {}'.format(i,random_temperature_in_celcius,0) ,
                            'Log not displaying correct information')


    def test_RandomRailForMAX6627BJTTemperatureReadTestOutOfRange(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot( hvdps= Mock(), slot= 0, subslot= 0)
            random_temperature = self.GenerateRandomOutOfRangeTemperature()
            random_temperature_in_celcius = my_hvdps.MAX6627TemperatureDecimalToCelcius(random_temperature)
            MAX6627_object.env = Mock()
            device_id_temperature_list = []
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail =Mock(return_value= [my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            for i in range(8):
                device_id_temperature_list.append(Mock(Temperature=random_temperature, MAX6627DeviceSelectField=i))
            my_hvdps.ReadRegister = Mock(side_effect=device_id_temperature_list)
            MAX6627_object.RandomRailForMAX6627BJTSensorTemperatureReadTest(read_attempts=1)
            self.assertEqual(my_hvdps.WriteRegister.call_count, 8)
            self.assertEqual(my_hvdps.ReadRegister.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
                self.assertRegex(log_message,'Temperature of BJT {} is not in a reasonable range,  Value {} for iteration {}'.format(i,random_temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')


    def test_RandomRailForMAX6627BJTTemperatureReadTestInRange(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot( hvdps= Mock(), slot= 0, subslot= 0)
            random_temperature = self.GenerateRandomTemperature()
            random_temperature_in_celcius = my_hvdps.MAX6627TemperatureDecimalToCelcius(random_temperature)
            MAX6627_object.env = Mock()
            device_id_temperature_list = []
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail =Mock(return_value= [my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            for i in range(8):
                device_id_temperature_list.append(Mock(Temperature=random_temperature, MAX6627DeviceSelectField=i))
            my_hvdps.ReadRegister = Mock(side_effect=device_id_temperature_list)
            MAX6627_object.RandomRailForMAX6627BJTSensorTemperatureReadTest(read_attempts=1)
            self.assertEqual(my_hvdps.WriteRegister.call_count, 8)
            self.assertEqual(my_hvdps.ReadRegister.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message,'Temperature of BJT {} is in a reasonable range, Value {} for iteration {}'
                                 .format(i,random_temperature_in_celcius,0),'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627VicorTemperatureReadTestInRange(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            random_temperature = self.GenerateRandomTemperature()
            random_temperature_in_celcius = my_hvdps.MAX6627TemperatureDecimalToCelcius(random_temperature)
            MAX6627_object.env = Mock()
            number_of_devices =4
            device_id_temperature_list = []
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            for i in range(number_of_devices):
                device_id_temperature_list.append(Mock(Temperature=random_temperature, MAX6627DeviceSelectField=i))
            my_hvdps.ReadRegister = Mock(side_effect=device_id_temperature_list)
            MAX6627_object.RandomDeviceForMAX6627VicorTemperatureReadTest(read_attempts=1)
            self.assertEqual(my_hvdps.WriteRegister.call_count, 4)
            self.assertEqual(my_hvdps.ReadRegister.call_count, 4)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message, 'Temperature of Vicor {} is in a reasonable range, Value {} for iteration {}'.format(i,random_temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627VicorTemperatureReadTestNotInRange(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            random_temperature = self.GenerateRandomOutOfRangeTemperature()
            random_temperature_in_celcius = my_hvdps.MAX6627TemperatureDecimalToCelcius(random_temperature)
            MAX6627_object.env = Mock()
            number_of_devices =4
            device_id_temperature_list =[]
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            for i in range(number_of_devices):
                device_id_temperature_list.append(Mock(Temperature=random_temperature, MAX6627DeviceSelectField=i))
                my_hvdps.ReadRegister = Mock(side_effect=device_id_temperature_list)
            MAX6627_object.RandomDeviceForMAX6627VicorTemperatureReadTest(read_attempts=1)
            self.assertEqual(my_hvdps.WriteRegister.call_count, 4)
            self.assertEqual(my_hvdps.ReadRegister.call_count, 4)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message, 'Temperature of Vicor {} is not in a reasonable range,  Value {} for iteration {}'.format(i,random_temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')

    def test_RandomRailForMAX6627ThermalADCTemperatureStabilityInterfaceTest(self):
            celcius35_in_hex = 0x230
            celcius40_in_hex = 0x280
            celcius44_in_hex = 0x2C0
            celcius34_in_hex = 0x220
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            my_hvdps.ReadRegister = Mock(
                side_effect=[Mock(Temperature=celcius35_in_hex), Mock(Temperature=celcius40_in_hex),
                             Mock(Temperature=celcius44_in_hex),
                             Mock(Temperature=celcius34_in_hex)]*800)
            MAX6627_object.RandomRailForMAX6627ThermalADCTemperatureStabilityInterfaceTest()
            self.assertEqual(my_hvdps.WriteRegister.call_count,8)
            self.assertEqual(my_hvdps.ReadRegister.call_count,800)
            log_calls = MAX6627_object.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertRegex(log_message,'Temperature of Thermal ADC device is stable','Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomRailForMAX6627ThermalADCTemperatureInStabilityInterfaceTest(self):
        celcius35_in_hex = 0x230
        celcius40_in_hex = 0x280
        celcius44_in_hex = 0x2C0
        celcius32_in_hex = 0x200
        MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        my_hvdps.WriteRegister = Mock()
        MAX6627_object.Log = Mock()
        my_hvdps.ReadRegister = Mock(
            side_effect=[Mock(Temperature=celcius35_in_hex), Mock(Temperature=celcius40_in_hex),
                         Mock(Temperature=celcius44_in_hex),
                         Mock(Temperature=celcius32_in_hex)]*800)
        MAX6627_object.RandomRailForMAX6627ThermalADCTemperatureStabilityInterfaceTest()
        self.assertEqual(my_hvdps.WriteRegister.call_count, 8)
        self.assertEqual(my_hvdps.ReadRegister.call_count, 800)
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message,'Temperature of Thermal ADC device is unstable','Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')


    def test_RandomRailForMAX6627BJTSensorTemperatureStabilityInterfaceTest(self):
            celcius35_in_hex = 0x230
            celcius40_in_hex = 0x280
            celcius44_in_hex = 0x2C0
            celcius34_in_hex = 0x220
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            my_hvdps.ReadRegister = Mock(
                side_effect=[Mock(Temperature=celcius35_in_hex), Mock(Temperature=celcius40_in_hex),
                             Mock(Temperature=celcius44_in_hex),
                             Mock(Temperature=celcius34_in_hex)]*800)
            MAX6627_object.RandomRailForMAX6627BJTSensorTemperatureStabilityInterfaceTest()
            self.assertEqual(my_hvdps.WriteRegister.call_count,8)
            self.assertEqual(my_hvdps.ReadRegister.call_count,800)
            log_calls = MAX6627_object.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertRegex(log_message,'Temperature of BJT Sensor device is stable','Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomRailForMAX6627BJTSensorTemperatureInStabilityInterfaceTest(self):
        celcius35_in_hex = 0x230
        celcius40_in_hex = 0x280
        celcius44_in_hex = 0x2C0
        celcius32_in_hex = 0x200
        MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        my_hvdps.WriteRegister = Mock()
        MAX6627_object.Log = Mock()
        my_hvdps.ReadRegister = Mock(
            side_effect=[Mock(Temperature=celcius35_in_hex), Mock(Temperature=celcius40_in_hex),
                         Mock(Temperature=celcius44_in_hex),
                         Mock(Temperature=celcius32_in_hex)]*800)
        MAX6627_object.RandomRailForMAX6627BJTSensorTemperatureStabilityInterfaceTest()
        self.assertEqual(my_hvdps.WriteRegister.call_count, 8)
        self.assertEqual(my_hvdps.ReadRegister.call_count, 800)
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message,'Temperature of BJT Sensor device is unstable','Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')

    def test_RandomDeviceForMAX6627VicorTemperatureStabilityInterfaceTest(self):
            celcius35_in_hex = 0x230
            celcius40_in_hex = 0x280
            celcius44_in_hex = 0x2C0
            celcius34_in_hex = 0x220
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.WriteRegister = Mock()
            MAX6627_object.Log = Mock()
            my_hvdps.ReadRegister = Mock(
                side_effect=[Mock(Temperature=celcius35_in_hex), Mock(Temperature=celcius40_in_hex),
                             Mock(Temperature=celcius44_in_hex),
                             Mock(Temperature=celcius34_in_hex)] * 400)
            MAX6627_object.RandomDeviceForMAX6627VicorTemperatureStabilityInterfaceTest()
            self.assertEqual(my_hvdps.WriteRegister.call_count, 4)
            self.assertEqual(my_hvdps.ReadRegister.call_count, 400)
            log_calls = MAX6627_object.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertRegex(log_message, 'Temperature of Vicor device is stable',
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627VicorTemperatureInStabilityInterfaceTest(self):
        celcius35_in_hex = 0x230
        celcius40_in_hex = 0x280
        celcius44_in_hex = 0x2C0
        celcius32_in_hex = 0x200
        MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        my_hvdps.WriteRegister = Mock()
        MAX6627_object.Log = Mock()
        my_hvdps.ReadRegister = Mock(
            side_effect=[Mock(Temperature=celcius35_in_hex), Mock(Temperature=celcius40_in_hex),
                         Mock(Temperature=celcius44_in_hex),
                         Mock(Temperature=celcius32_in_hex)] * 400)
        MAX6627_object.RandomDeviceForMAX6627VicorTemperatureStabilityInterfaceTest()
        self.assertEqual(my_hvdps.WriteRegister.call_count, 4)
        self.assertEqual(my_hvdps.ReadRegister.call_count, 400)
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message, 'Temperature of Vicor Sensor device is unstable',
                             'Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')


    def test_RandomRailForMAX6627ThermalADCTemperatureReadUsingHilInRangeTest(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            temperature_in_celcius = 25
            my_hvdps = HvdpsSubslot(hvdps = Mock(),slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.Log = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.MAX6627ThermalAdcTemperatureRead = Mock(side_effect=[temperature_in_celcius]*8)
            MAX6627_object.RandomRailForMAX6627ThermalADCTemperatureReadUsingHilTest(read_attempts=1)
            self.assertEqual(my_hvdps.MAX6627ThermalAdcTemperatureRead.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message, 'Temperature of Thermal ADC {} is in a reasonable range, Value {} for iteration {}'.format(i,temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomRailForMAX6627ThermalADCTemperatureReadUsingHilOutOfRangeTest(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            temperature_in_celcius = 70
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.Log = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.MAX6627ThermalAdcTemperatureRead = Mock(side_effect=[temperature_in_celcius] * 8)
            MAX6627_object.RandomRailForMAX6627ThermalADCTemperatureReadUsingHilTest(read_attempts=1)
            self.assertEqual(my_hvdps.MAX6627ThermalAdcTemperatureRead.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message, 'Temperature of Thermal ADC {} is not in a reasonable range,  Value {} for iteration {}'.format(i,temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')



    def test_RandomRailForMAX6627BJTSensorTemperatureReadInRangeUsingHilTest(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            temperature_in_celcius = 25
            my_hvdps = HvdpsSubslot(hvdps = Mock(),slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.Log = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.MAX6627BJTTemperatureRead = Mock(side_effect=[temperature_in_celcius] * 8)
            MAX6627_object.RandomRailForMAX6627BJTSensorTemperatureReadUsingHilTest(read_attempts=1)
            self.assertEqual(my_hvdps.MAX6627BJTTemperatureRead.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message, 'Temperature of BJT {} is in a reasonable range, Value {} for iteration {}'.format(i,temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'DEBUG', 'Log level should be info')



    def test_RandomRailForMAX6627BJTSensorTemperatureReadOutOfRangeUsingHilTest(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627TemperatureInterfacetTestsForHvdps()
            temperature_in_celcius =70
            my_hvdps = HvdpsSubslot(hvdps = Mock(),slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.Log = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            my_hvdps.MAX6627BJTTemperatureRead = Mock(side_effect=[temperature_in_celcius] * 8)
            MAX6627_object.RandomRailForMAX6627BJTSensorTemperatureReadUsingHilTest(read_attempts=1)
            self.assertEqual(my_hvdps.MAX6627BJTTemperatureRead.call_count, 8)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                log_level, log_message = log_calls[i][0]
                self.assertRegex(log_message, 'Temperature of BJT {} is not in a reasonable range,  Value {} for iteration {}'.format(i,temperature_in_celcius,0),
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')



    def test_MAX6627BJTSensorHighAlarmTest_error_when_alarms_detected_in_first_level(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.Log = self.mock_log
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627BjtSensorLimitsSet = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
            with self.assertRaises(fval.LoggedError):
                MAX6627_object.RandomDeviceForMAX6627BJTSensorHighAlarmTest(iteration_count=1)


    def test_MAX6627BJTSensorHighAlarmTest_pass(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627BjtSensorLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0,0,0,0,0,0,0,0])
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.Log = Mock()
            MAX6627_object.verify_alarms = Mock(return_value=True)
            MAX6627_object.RandomDeviceForMAX6627BJTSensorHighAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_MAX6627BJTSensorHighAlarmTest_fail(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627BjtSensorLimitsSet = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime=Mock()
            MAX6627_object.verify_no_alarm_received = Mock(return_value=True)
            MAX6627_object.verify_alarms = Mock(return_value=False)
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.RandomDeviceForMAX6627BJTSensorHighAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_MAX6627BJTSensorLowAlarmTest_error_when_alarms_detected_in_first_level(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.Log = self.mock_log
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value=Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627BjtSensorLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
            with self.assertRaises(fval.LoggedError):
                MAX6627_object.RandomDeviceForMAX6627BJTSensorLowAlarmTest(iteration_count=1)

    def test_MAX6627BJTSensorLowAlarmTest_pass(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627BjtSensorLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_alarms = Mock(return_value=True)
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value=Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.RandomDeviceForMAX6627BJTSensorLowAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'DEBUG', 'Log level should be info')


    def test_MAX6627BJTSensorLowAlarmTest_fail(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627BjtSensorLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.verify_no_alarm_received = Mock(return_value=True)
            MAX6627_object.verify_alarms = Mock(return_value=False)
            MAX6627_object.RandomDeviceForMAX6627BJTSensorLowAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_verify_alarms_level1_fail(self):
        rail_type = 'HV'
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['TqNotifyAlarmDut0'])
        my_hvdps.ReadRegister = Mock(return_value=Mock(value=0xff))
        MAX6627_object.Log = Mock()
        result = MAX6627_object.verify_alarms(my_hvdps,'MAX6627BJTHighAlarm',rail_type)
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(result, False)

    def test_verify_alarms_level2_fail(self):
        rail_type = 'HV'
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        my_hvdps.ReadRegister = Mock(return_value=Mock(value=0xff))
        MAX6627_object.Log = Mock()
        result = MAX6627_object.verify_alarms(my_hvdps,'MAX6627BJTHighAlarm',rail_type)
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(result,False)


    def test_verify_alarms_level3_fail(self):
        rail_type = 'HV'
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        my_hvdps.ReadRegister = Mock(
            return_value=Mock(Hclc0Alarms=1, Hclc1Alarms=0, Hclc2Alarms = 0, Hclc3Alarms = 0, Hclc4Alarms = 0, Hclc5Alarms = 0, Hclc6Alarms = 0, Hclc7Alarms = 0,
                              fields=lambda: ['Hclc0Alarms', 'Hclc1Alarms','Hclc2Alarms','Hclc3Alarms', 'Hclc4Alarms','Hclc5Alarms','Hclc6Alarms','Hclc7Alarms']))
        MAX6627_object.get_non_zero_fields = Mock(side_effect=['MAX6627BJTHighAlarm'])
        MAX6627_object.Log = Mock()
        result = MAX6627_object.verify_alarms(my_hvdps, 'MAX6627BJTLowAlarm',rail_type)
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(result,False)


    def test_verify_alarms_pass(self):
        rail_type = 'HV'
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        my_hvdps.ReadRegister = Mock(
            return_value=Mock(Hclc0Alarms=1, Hclc1Alarms=1, Hclc2Alarms = 1, Hclc3Alarms = 1, Hclc4Alarms = 1, Hclc5Alarms = 1, Hclc6Alarms = 1, Hclc7Alarms = 1,MAX6627BJTHighAlarm=1,
                              fields=lambda: ['Hclc0Alarms', 'Hclc1Alarms','Hclc2Alarms','Hclc3Alarms', 'Hclc4Alarms','Hclc5Alarms','Hclc6Alarms','Hclc7Alarms', 'MAX6627BJTHighAlarm']))
        MAX6627_object.get_non_zero_fields = Mock(return_value=['MAX6627BJTHighAlarm'])
        result= MAX6627_object.verify_alarms(my_hvdps,'MAX6627BJTHighAlarm',rail_type)
        self.assertEqual(result,True)


    def test_get_non_zero_fields(self):
        registerName = hvdpsregs.GLOBAL_ALARMS
        MAX6627_object = MAX6627Alarms()
        result = MAX6627_object.get_non_zero_fields(registerName)
        self.assertEqual(len(result),0)

    def test_get_non_zero_fields_when_fields_are_added(self):
        expected_list = ['TqNotifyHV','HclcRailAlarms']
        registerName = hvdpsregs.GLOBAL_ALARMS
        registerName.HclcRailAlarms = 1
        registerName.TqNotifyHV = 1
        MAX6627_object = MAX6627Alarms()
        result = MAX6627_object.get_non_zero_fields(registerName)
        self.assertEqual(result, expected_list)

    def configure_before_test(self,inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearHclcRailAlarms = Mock()
        inst_subslot.ResetThermalAlarmLimitForMAX6627 = Mock()

    def test_list_of_devices_shuffled(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.get_shuffled_device_list(4)

    def test_RandomDeviceForMAX6627ThermalADCLowAlarmTest_pass(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.MAX6627AdcLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_alarms = Mock(return_value=True)
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.verify_no_alarm_received = Mock(return_value=True)
            MAX6627_object.RandomDeviceForMAX6627ThermalADCLowAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627ThermalADCLowAlarmTest_fail(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.MAX6627AdcLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_alarms = Mock(return_value=False)
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.verify_no_alarm_received = Mock(return_value=True)
            MAX6627_object.RandomDeviceForMAX6627ThermalADCLowAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_RandomDeviceForMAX6627ThermalADCLowAlarm_error_when_alarms_detected_in_first_level(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.Log = self.mock_log
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.MAX6627AdcLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
            with self.assertRaises(fval.LoggedError):
                MAX6627_object.RandomDeviceForMAX6627ThermalADCLowAlarmTest(iteration_count=1)

    def test_RandomDeviceForMAX6627ThermalADCHighAlarmTest_pass(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.MAX6627AdcLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_alarms = Mock(return_value=True)
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.RandomDeviceForMAX6627ThermalADCHighAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627ThermalADCHighAlarmTest_fail(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.MAX6627AdcLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_alarms = Mock(return_value=False)
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            MAX6627_object.RandomDeviceForMAX6627ThermalADCHighAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_RandomDeviceForMAX6627ThermalADCHighAlarm_error_when_alarms_detected_in_first_level(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.Log = self.mock_log
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.MAX6627AdcLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.get_third_level_alarm_for_all_hclcrails = Mock(return_value=[0, 0, 0, 0, 0, 0, 0, 0])
            my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
            with self.assertRaises(fval.LoggedError):
                MAX6627_object.RandomDeviceForMAX6627ThermalADCHighAlarmTest(iteration_count=1)

    def test_RandomDeviceForMAX6627VicorHighAlarmTest_pass(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627VicorLimitsSet = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_vicor_alarms = Mock(return_value=True)
            MAX6627_object.Log = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            vicor_alarm = my_hvdps.registers.MAX6627_VICOR_ALARMS(value=0)
            my_hvdps.ReadRegister=Mock(return_value=vicor_alarm)
            my_hvdps.clearMAX6627VicorAlarmRegister = Mock()
            MAX6627_object.RandomDeviceForMAX6627VicorHighAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627VicorHighAlarmTest_fail(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627VicorLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            MAX6627_object.verify_vicor_alarms = Mock(return_value=False)
            MAX6627_object.Log = Mock()
            vicor_alarm = my_hvdps.registers.MAX6627_VICOR_ALARMS(value=0)
            my_hvdps.ReadRegister=Mock(return_value=vicor_alarm)
            my_hvdps.clearMAX6627VicorAlarmRegister = Mock()
            MAX6627_object.RandomDeviceForMAX6627VicorHighAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_RandomDeviceForMAX6627VicorHighAlarm_error_when_alarms_detected_in_first_level(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.Log = self.mock_log
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627VicorLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
            with self.assertRaises(fval.LoggedError):
                MAX6627_object.RandomDeviceForMAX6627VicorHighAlarmTest(iteration_count=1)


    def test_RandomDeviceForMAX6627VicorLowAlarmTest_pass(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627VicorLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            vicor_alarm = my_hvdps.registers.MAX6627_VICOR_ALARMS(value=0)
            my_hvdps.ReadRegister = Mock(return_value=vicor_alarm)
            MAX6627_object.verify_vicor_alarms = Mock(return_value=True)
            MAX6627_object.Log = Mock()
            my_hvdps.clearMAX6627VicorAlarmRegister = Mock()
            MAX6627_object.RandomDeviceForMAX6627VicorLowAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'DEBUG', 'Log level should be info')

    def test_RandomDeviceForMAX6627VicorLowAlarmTest_fail(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627VicorLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            vicor_alarm = my_hvdps.registers.MAX6627_VICOR_ALARMS(value=0)
            my_hvdps.ReadRegister = Mock(return_value=vicor_alarm)
            my_hvdps.get_global_alarms = Mock(return_value=[])
            MAX6627_object.verify_vicor_alarms = Mock(return_value=False)
            MAX6627_object.Log = Mock()
            my_hvdps.clearMAX6627VicorAlarmRegister = Mock()
            MAX6627_object.RandomDeviceForMAX6627VicorLowAlarmTest(iteration_count=1)
            log_calls = MAX6627_object.Log.call_args_list
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_RandomDeviceForMAX6627VicorLowAlarm_when_alarms_detected_in_first_level(self):
        with patch('Dps.Tests.MAX6627.random.shuffle', return_value=[0])as mock_Random:
            MAX6627_object = MAX6627Alarms()
            MAX6627_object.Log = self.mock_log
            my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            MAX6627_object.env = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.Max6627VicorLimitsSet = Mock()
            my_hvdps.WaitTime = Mock()
            MAX6627_object.get_max6627alarm_filter_count_value = Mock(return_value=2)
            MAX6627_object.set_max6627alarm_filter_count_value = Mock()
            vicor_alarm = my_hvdps.registers.MAX6627_VICOR_ALARMS(value=0)
            my_hvdps.ReadRegister = Mock(return_value=vicor_alarm)
            my_hvdps.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
            with self.assertRaises(fval.LoggedError):
                MAX6627_object.RandomDeviceForMAX6627VicorLowAlarmTest(iteration_count=1)

    def mock_log(self, level, message):
        if level.lower() == 'error':
            raise fval.LoggedError

    def test_verify_vicor_alarms_pass(self):
        expected_alarms = ['Vicor0HighTemperatureAlarm', 'Vicor1HighTemperatureAlarm', 'Vicor2HighTemperatureAlarm',
                           'Vicor3HighTemperatureAlarm']
        get_non_zero_alarms = ['Vicor0HighTemperatureAlarm', 'Vicor1HighTemperatureAlarm', 'Vicor2HighTemperatureAlarm',
                               'Vicor3HighTemperatureAlarm']
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['Max6627VicorTempAlarm'])
        my_hvdps.ReadRegister = Mock()
        MAX6627_object.Log = Mock()
        MAX6627_object.get_non_zero_fields = Mock(return_value=get_non_zero_alarms)
        result = MAX6627_object.verify_vicor_alarms(my_hvdps, expected_alarms)
        self.assertEqual(result, True)

    def test_verify_vicor_alarms_fail(self):
        expected_alarms = ['Vicor0HighTemperatureAlarm', 'Vicor1HighTemperatureAlarm', 'Vicor2HighTemperatureAlarm',
                           'Vicor3HighTemperatureAlarm']
        get_non_zero_alarms = ['Vicor0LowTemperatureAlarm', 'Vicor1HighTemperatureAlarm', 'Vicor2HighTemperatureAlarm',
                               'Vicor3HighTemperatureAlarm']
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['Max6627VicorTempAlarm'])
        my_hvdps.ReadRegister = Mock()
        MAX6627_object.Log = Mock()
        MAX6627_object.get_non_zero_fields = Mock(return_value=get_non_zero_alarms)
        result = MAX6627_object.verify_vicor_alarms(my_hvdps, expected_alarms)
        self.assertEqual(result, False)
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

    def test_verify_vicor_alarms_global_alarms_received(self):
        expected_alarms = ['Vicor0HighTemperatureAlarm', 'Vicor1HighTemperatureAlarm', 'Vicor2HighTemperatureAlarm',
                           'Vicor3HighTemperatureAlarm']
        MAX6627_object = MAX6627Alarms()
        my_hvdps = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.get_global_alarms = Mock(return_value=['AdTempAlarm'])
        MAX6627_object.Log = Mock()
        result = MAX6627_object.verify_vicor_alarms(my_hvdps, expected_alarms)
        self.assertEqual(result, False)
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)