################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_DpsAssertionTests.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test to verify if DpsAssertionTests works as expected
#-------------------------------------------------------------------------------
#  Created by:
#        Date: 05/30/18
#       Group: HDMT FPGA Validation
################################################################################

import unittest
from unittest.mock import Mock
from Common import fval
from unittest.mock import patch

from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps.hddps import Hddps
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
import Common.instruments.dps.hddps_registers as hddps_registers
import Common.instruments.dps.hvdps_registers as hvdps_registers

from Dps.Tests.DpsAssertionTests import BaseAssertion,OpMode,IRange


class OpModeAssertionTestClass(unittest.TestCase):

    def test_parameters_for_check_op_mode_scenario_vlc(self):
        rail = 2
        uhc = 3
        assert_test = 'OPMODE'
        op_mode = OpMode()
        op_mode.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        op_mode.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.verify_if_fpga_supports_vmeasure = Mock(return_value=False)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        op_mode.initialize_rail = Mock()
        op_mode.Log = Mock()
        op_mode.check_op_mode_scenario = Mock(return_value=1)
        op_mode.DirectedVlcOpModeAssertionTest()
        validate_check_op_mode_scenario_parameter = op_mode.check_op_mode_scenario.call_args
        self.assertNotEqual(len(validate_check_op_mode_scenario_parameter), 0)
        args, kwargs = validate_check_op_mode_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'VLC')

    def test_parameters_for_check_op_mode_scenario_hc(self):
        rail = 2
        uhc = 3
        assert_test = 'OPMODE'
        op_mode = OpMode()
        op_mode.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        op_mode.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.verify_if_fpga_supports_vmeasure = Mock(return_value=False)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        op_mode.initialize_rail = Mock()
        op_mode.Log = Mock()
        op_mode.check_op_mode_scenario = Mock(return_value=1)
        op_mode.DirectedHcOpModeAssertionTest()
        validate_check_op_mode_scenario_parameter = op_mode.check_op_mode_scenario.call_args
        self.assertNotEqual(len(validate_check_op_mode_scenario_parameter), 0)
        args, kwargs = validate_check_op_mode_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail+16)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'HC')

    def test_parameters_for_check_op_mode_scenario_Lc(self):
        rail = 2
        uhc = 3
        assert_test = 'OPMODE'
        op_mode = OpMode()
        op_mode.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        op_mode.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.verify_if_fpga_supports_vmeasure = Mock(return_value=False)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        op_mode.initialize_rail = Mock()
        op_mode.Log = Mock()
        op_mode.check_op_mode_scenario = Mock(return_value=1)
        op_mode.DirectedLcOpModeAssertionTest()
        validate_check_op_mode_scenario_parameter = op_mode.check_op_mode_scenario.call_args
        self.assertNotEqual(len(validate_check_op_mode_scenario_parameter), 0)
        args, kwargs = validate_check_op_mode_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail+16)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'LC')

    def test_parameters_for_check_op_mode_scenario_Hv(self):
        rail = 2
        uhc = 3
        assert_test = 'OPMODE'
        op_mode = OpMode()
        op_mode.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        op_mode.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.verify_if_fpga_supports_vmeasure = Mock(return_value=False)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        op_mode.initialize_rail = Mock()
        op_mode.Log = Mock()
        op_mode.check_op_mode_scenario = Mock(return_value=1)
        op_mode.DirectedHvOpModeAssertionTest()
        validate_check_op_mode_scenario_parameter = op_mode.check_op_mode_scenario.call_args
        self.assertNotEqual(len(validate_check_op_mode_scenario_parameter), 0)
        args, kwargs = validate_check_op_mode_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail+16)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'HV')

    def test_check_op_mode_senerio_if_global_alarm_is_set(self):
        op_mode = OpMode()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['VlcCFoldAlarms'])
        hddps_subslot.SetRailsToSafeState= Mock()
        op_mode.setup_and_send_rail_commands = Mock()
        hddps_subslot.bring_rails_out_of_safe_state = Mock()
        with self.assertRaises(fval.LoggedError):
            op_mode.check_op_mode_scenario(hddps_subslot, rail=2, dutid=3, assert_test='OPMODE', mode='VFORCE', rail_type='LC',pass_count=0)


    def test_parameters_for_setup_and_send_rail_command(self):
        op_mode = OpMode()
        rail = 2
        asser_test = 'OPMODE'
        expected_command_data = 0
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=[])
        op_mode.Log = Mock()
        hddps_subslot.SetRailsToSafeState= Mock()
        op_mode.check_for_alarm = Mock()
        op_mode.setup_and_send_rail_commands = Mock()
        hddps_subslot.bring_rails_out_of_safe_state = Mock()
        hddps_subslot.send_tq_footer_and_check_tq_notify_alarm = Mock()
        op_mode.check_op_mode_scenario(hddps_subslot, rail=rail, dutid=3, assert_test=asser_test, mode='IFORCE', rail_type='LC',pass_count=0)
        validate_setup_and_send_rail_commands_parameter = op_mode.setup_and_send_rail_commands.call_args
        args, kwargs = validate_setup_and_send_rail_commands_parameter
        call_assert_test, call_assert_test_data, call_board, call_command_data, call_rail = args
        self.assertEqual(call_rail, rail )
        self.assertEqual(call_assert_test, asser_test)
        self.assertEqual(call_command_data, expected_command_data)

    def test_check_if_vlc_rail_method_gets_called(self):
        op_mode = OpMode()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.check_for_vlc_cfold_actual_and_assert_data = Mock()
        op_mode.check_for_alarm(hddps_subslot,'VLC',5,0)
        self.assertEqual(hddps_subslot.check_for_vlc_cfold_actual_and_assert_data.call_count, 1)

    def test_check_if_hclc_rail_method_gets_called(self):
        op_mode = OpMode()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.check_for_hclc_cfold_actual_and_assert_data = Mock()
        op_mode.check_for_alarm(hddps_subslot,'LC',5,0)
        self.assertEqual(hddps_subslot.check_for_hclc_cfold_actual_and_assert_data.call_count, 1)


class IrangeAssertionTestClass(unittest.TestCase):

    def test_parameters_for_check_irange_scenario_hc(self):
        rail = 2
        uhc = 3
        assert_test = 'IRANGE'
        irange_assertion = IRange()
        irange_assertion.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        irange_assertion.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        irange_assertion.initialize_rail = Mock()
        irange_assertion.check_irange_scenario = Mock(return_value=1)
        irange_assertion.Log = Mock()
        irange_assertion.DirectedHcIrangeAssertionTest()
        validate_check_irange_scenario_parameter = irange_assertion.check_irange_scenario.call_args
        self.assertNotEqual(len(validate_check_irange_scenario_parameter), 0)
        args, kwargs = validate_check_irange_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail+16)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'HC')

    def test_parameters_for_check_irange_scenario_lc(self):
        rail = 2
        uhc = 3
        assert_test = 'IRANGE'
        irange_assertion = IRange()
        irange_assertion.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        irange_assertion.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        irange_assertion.initialize_rail = Mock()
        irange_assertion.check_irange_scenario = Mock(return_value=1)
        irange_assertion.Log = Mock()
        irange_assertion.DirectedLcIrangeAssertionTest()
        validate_check_irange_scenario_parameter = irange_assertion.check_irange_scenario.call_args
        self.assertNotEqual(len(validate_check_irange_scenario_parameter), 0)
        args, kwargs = validate_check_irange_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail+16)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'LC')

    def test_parameters_for_check_irange_scenario_vlc(self):
        rail = 2
        uhc = 3
        assert_test = 'IRANGE'
        irange_assertion = IRange()
        irange_assertion.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        irange_assertion.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        irange_assertion.initialize_rail = Mock()
        irange_assertion.check_irange_scenario = Mock(return_value=1)
        irange_assertion.Log = Mock()
        irange_assertion.DirectedVlcIrangeAssertionTest()
        validate_check_irange_scenario_parameter = irange_assertion.check_irange_scenario.call_args
        self.assertNotEqual(len(validate_check_irange_scenario_parameter), 0)
        args, kwargs = validate_check_irange_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'VLC')

    def test_parameters_for_check_irange_scenario_hv(self):
        rail = 2
        uhc = 3
        assert_test = 'IRANGE'
        irange_assertion = IRange()
        irange_assertion.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        irange_assertion.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        irange_assertion.initialize_rail = Mock()
        irange_assertion.check_irange_scenario = Mock(return_value=1)
        irange_assertion.Log = Mock()
        irange_assertion.DirectedHvIrangeAssertionTest()
        validate_check_irange_scenario_parameter = irange_assertion.check_irange_scenario.call_args
        self.assertNotEqual(len(validate_check_irange_scenario_parameter), 0)
        args, kwargs = validate_check_irange_scenario_parameter
        returned_board, returned_rail, returned_dutid, returned_assert_test, returned_mode, returned_rail_type, returned_pass_count = args
        self.assertEqual(returned_rail, rail+16)
        self.assertEqual(returned_assert_test, assert_test)
        self.assertEqual(returned_rail_type, 'HV')

    def test_DirectedHvIrangeAssertionTest_pass(self):
        rail = 2
        uhc = 3
        assert_test = 'IRANGE'
        irange_assertion = IRange()
        irange_assertion.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        irange_assertion.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        irange_assertion.initialize_rail = Mock()
        irange_assertion.check_irange_scenario = Mock(return_value=7)
        irange_assertion.Log = Mock()
        irange_assertion.DirectedHvIrangeAssertionTest()
        log_calls = irange_assertion.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(len(log_calls), 1)


    def test_check_irange_scenario_if_global_alarm_is_set(self):
        irange_assertion = IRange()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['VlcCFoldAlarms'])
        hddps_subslot.SetRailsToSafeState= Mock()
        irange_assertion.setup_and_send_irange_rail_command = Mock()
        hddps_subslot.bring_rails_out_of_safe_state = Mock()
        with self.assertRaises(fval.LoggedError):
            irange_assertion.check_irange_scenario(board=hddps_subslot, rail=2, dutid=5, assert_test='IRANGE', mode='I_2_56_UA', rail_type='VLC',pass_count=0)