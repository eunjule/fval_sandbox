################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_SampleEngine.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test to verify if Correct voltage was forced
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 02/20/18
#       Group: HDMT FPGA Validation
################################################################################

import unittest
from unittest.mock import Mock
from unittest.mock import patch


from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps.hddps import Hddps
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
import Common.instruments.dps.hddps_registers as hddps_registers
import Common.instruments.dps.hvdps_registers as hvdps_registers

from Dps.Tests.SampleEngine import SampleEngineHv,SampleEngineLvm,SampleEngineLc,SampleEngineHc


class SampleEngineTestClass(unittest.TestCase):

    def test_to_verify_trigger_HCLC_rail_queue_string_content(self):
        expected_trigger_queue_data = b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x00\x00\x15\x9a\x18\x1c\x15\xac\xe7\xe3\x15\xab\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\xce\x1c\x15\x9c\xff\xff\x15\x9b\x01\x00\x15\xa2\xc0X\x15\x97P\x00\x15\xbb0uM\x80@\x00H\x80\x00\x00I\x80TvJ\x80\x102K\x80 \x00L\x80\x01\x00N\x80\x00\x00\x15\xb9\x00\x01\x06\x8a\xff\xff\x15\xa6p\x17\x15\xa2\x00\x00\x15\xa7\x00\x00\x06\x8a\x01\x00\x15\x9a\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        sample_engine = SampleEngineHc()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value='HC')
        trigger_queue_content = sample_engine.generate_trigger_queue(hddps_subslot, force_voltage=10, rail=5, uhc = 5, rail_type='HC',dutid=15,turbo=False)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_trigquedata(self):
        input_Trigger_queue_data=  b'\x1f\x00\x00\xb2\x1f\x00\x10\xb2\x05\x00\x15\x99\x00\x00\x15\x9a\xcaL\x15\xab\xca\xcc\x15\xac\n@\x15\x9d\n\xc0\x15\x9e\x00P\x15\x9b\x00\xd0\x15\x9c\x01\x00\x15\xa8\xb8\x0bU\x80@\x00P\x80\x00\x00Q\x80TvR\x80\x102S\x80 \x00T\x80\x01\x00V\x80\x00\x00\x15\xa3\x01\x00\x15\xa2\x01\x00\x15\x9a\x000\x15\xa6\xdc\x05\x15\xa2\x00\x00\x15\xa7\x00\x00\x15\xa5d\x00\x15\xa2\x1e\x00\x00\xb2\x1e\x00\x10\xb2\x00\x00\x00\xb1'
        output_Trigger_queue_data= b'\x1f\x00\x00\xb2\x1f\x00\x10\xb2\x05\x00\x15\x99\x00\x00\x15\x9a\xcaL\x15\xab\xca\xcc\x15\xac\n@\x15\x9d\n\xc0\x15\x9e\x00P\x15\x9b\x00\xd0\x15\x9c\x01\x00\x15\xa8\xb8\x0bU\x80@\x00P\x80\x00\x00Q\x80TvR\x80\x102S\x80 \x00T\x80\x01\x00V\x80\x00\x00\x15\xa3\x01\x00\x15\xa2\x01\x00\x15\x9a\x000\x15\xa6\xdc\x05\x15\xa2\x00\x00\x15\xa7\x00\x00\x15\xa5d\x00\x15\xa2\x1e\x00\x00\xb2\x1e\x00\x10\xb2\x00\x00\x00\xb1'
        self.assertEqual(input_Trigger_queue_data,output_Trigger_queue_data)

    def test_parameters_for_RandomLVMVerifyHeaderVoltage(self):
        iterations = 5
        sample_engine = SampleEngineLvm()
        sample_engine.env = Mock()
        sample_engine.force_voltage_hv_to_lv_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomLvmVerifyHeaderAndVoltageTest()
        scenario_call_args = sample_engine.force_voltage_hv_to_lv_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_iterations = args
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_RandomLvmTurboVerifyHeaderAndVoltage(self):
        iterations = 5
        sample_engine = SampleEngineLvm()
        sample_engine.env = Mock()
        sample_engine.force_voltage_hv_to_lv_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomLvmTurboVerifyHeaderAndVoltageTest()
        scenario_call_args = sample_engine.force_voltage_hv_to_lv_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_iterations = args
        self.assertEqual(call_iterations, iterations)
        self.assertEqual(kwargs['turbo'], True)

    def test_parameters_for_RandomHcVerifyHeaderAndVoltage(self):
        iterations = 5
        rail_type = 'HC'
        sample_engine = SampleEngineHc()
        sample_engine.env = Mock()
        sample_engine.force_voltage_open_socket_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomHcVerifyHeaderAndVoltageTest()
        scenario_call_args = sample_engine.force_voltage_open_socket_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_rail_type, call_iterations = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)


    def test_parameters_for_RandomLcVerifyHeaderAndVoltage(self):
        iterations = 5
        rail_type = 'LC'
        sample_engine = SampleEngineLc()
        sample_engine.env = Mock()
        sample_engine.force_voltage_open_socket_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomLcVerifyHeaderAndVoltageTest()
        scenario_call_args = sample_engine.force_voltage_open_socket_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_rail_type, call_iterations = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_RandomLcVerifyHeaderAndNegativeVoltage(self):
        iterations = 5
        rail_type = 'LC'
        negative = True
        sample_engine = SampleEngineLc()
        sample_engine.env = Mock()
        sample_engine.force_voltage_open_socket_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomLcVerifyHeaderAndNegativeVoltageTest()
        scenario_call_args = sample_engine.force_voltage_open_socket_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_rail_type, call_iterations = args
        call_negative = kwargs['negative']
        self.assertEqual(call_negative, negative)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)


    def test_parameters_for_RandomHvVerifyHeaderAndVoltage(self):
        iterations = 5
        rail_type = 'HV'
        sample_engine = SampleEngineHv()
        sample_engine.env = Mock()
        sample_engine.force_voltage_open_socket_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomHvVerifyHeaderAndVoltageTest()
        scenario_call_args = sample_engine.force_voltage_open_socket_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_rail_type, call_iterations = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_RandomHvTurboVerifyHeaderAndVoltage(self):
        iterations = 5
        rail_type = 'HV'
        sample_engine = SampleEngineHv()
        sample_engine.env = Mock()
        sample_engine.force_voltage_open_socket_scenario_with_sample = Mock()
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.RandomHvTurboVerifyHeaderAndVoltageTest()
        scenario_call_args = sample_engine.force_voltage_open_socket_scenario_with_sample.call_args
        self.assertNotEqual(len(scenario_call_args), 0)
        args, kwargs = scenario_call_args
        call_board, call_rail_type, call_iterations = args
        call_turbo = kwargs['turbo']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)
        self.assertEqual(call_turbo,True)

    def test_randomize_rail_uhc_for_hc(self):
        sample_engine = SampleEngineHc()
        sample_engine.env = Mock()
        my_hddps = Hddps(slot=5,rc=Mock)
        expected_tuple_list_length = 80
        rail_uhc_tuple_list = sample_engine.randomize_rail_uhc(my_hddps.subslots[1], 'HC')
        self.assertEqual(len(rail_uhc_tuple_list),expected_tuple_list_length)

    def test_to_verify_generate_lvm_trigger_queue_content(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\x08\x00\x00\xc3\x08\x00\x00\xc4\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x8b\x0c\x15\xc5\x00\x00\x03\x9a\x00\x00\x15\x9a\x18\x1c\x15\xac\xe7\xe3\x15\xab\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\x07\x03\x15\x9c\xd3\xd4\x15\x9b\x01\x00\x15\xa2\x8b\x0c\x15\xc5\x00!\x15\x97P\x00\x15\xbb\n\xd7\x03\xcb\xe1z\x03\xca@\x00\xc8\x80\x00\x00\xc9\x80Tv\xca\x80\x102\xcb\x80\x08\x00\xcc\x800u\xcd\x80\x01\x00\xce\x80\x01\x00\x03\x9a\x01\x00\x15\x9a\x00\x00\x15\xb9\xd3\x0c\x15\xc5\x00\x01\x06\x8a\xf8>\x15\xa6p\x17\x15\xa2\x00\x00\x15\xa7\x00\x00\x06\x8a\xbc\x02\x15\xa2 \x00\x10\xb2\x08\x00\x00\xb2\x00\x00\x00\xb1'
        sample_engine = SampleEngineLvm()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = sample_engine.generate_lvm_trigger_queue(hvdps_subslot, force_rail = 5, sense_rail = 3, force_voltage=3, dutid = 15, uhc = 5, turbo = False)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_lvm_trigger_queue_content_turbo(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\x08\x00\x00\xc3\x08\x00\x00\xc4\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x8b\x0c\x15\xc5\x00\x00\x03\x9a\x00\x00\x15\x9a\x18\x1c\x15\xac\xe7\xe3\x15\xab\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\x07\x03\x15\x9c\xd3\xd4\x15\x9b\x01\x00\x15\xa2\x8b\x0c\x15\xc5\x00!\x15\x97P\x00\x15\xbb\n\xd7\x03\xcb\xe1z\x03\xca@\x00\xe8\x80\x00\x00\xe9\x80Tv\xea\x80\x102\xeb\x80\x08\x00\xec\x800u\xed\x80\x01\x00\xee\x80\x01\x00\x03\x9a\x01\x00\x15\x9a\x00\x00\x15\xb9\xd3\x0c\x15\xc5\x00\x01\x06\x8a\xf8>\x15\xa6p\x17\x15\xa2\x00\x00\x15\xa7\x00\x00\x06\x8a\xbc\x02\x15\xa2 \x00\x10\xb2\x08\x00\x00\xb2\x00\x00\x00\xb1'
        sample_engine = SampleEngineLvm()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = sample_engine.generate_lvm_trigger_queue(hvdps_subslot, force_rail = 5, sense_rail = 3, force_voltage=3, dutid = 15, uhc = 5, turbo = True)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_trigger_queue_content(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x8b\x0c\x15\xc5\x00\x00\x15\x9a\x18\x1c\x15\xac\xe7\xe3\x15\xab\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\x07\x03\x15\x9c\xd3\xd4\x15\x9b\x01\x00\x15\xa2\x8b\x0c\x15\xc5\x00!\x15\x97P\x00\x15\xbb@\x00H\x80\x00\x00I\x80TvJ\x80\x102K\x80 \x00L\x800uM\x80\x01\x00N\x80\x01\x00\x15\x9a\x00\x00\x15\xb9\xd3\x0c\x15\xc5\x00\x01\x06\x8a\xf8>\x15\xa6p\x17\x15\xa2\x00\x00\x15\xa7\x00\x00\x06\x8a\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        sample_engine = SampleEngineHv()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = sample_engine.generate_trigger_queue(hvdps_subslot, rail_type = 'HV', force_voltage=3, dutid = 15, rail = 5, uhc = 5,turbo=False)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_trigger_queue_content_turbo(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x8b\x0c\x15\xc5\x00\x00\x15\x9a\x18\x1c\x15\xac\xe7\xe3\x15\xab\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\x07\x03\x15\x9c\xd3\xd4\x15\x9b\x01\x00\x15\xa2\x8b\x0c\x15\xc5\x00!\x15\x97P\x00\x15\xbb@\x00\xe0\x80\x00\x00\xe1\x80Tv\xe2\x80\x102\xe3\x80 \x00\xe4\x800u\xe5\x80\x01\x00\xe6\x80\x01\x00\x15\x9a\x00\x00\x15\xb9\xd3\x0c\x15\xc5\x00\x01\x06\x8a\xf8>\x15\xa6p\x17\x15\xa2\x00\x00\x15\xa7\x00\x00\x06\x8a\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        sample_engine = SampleEngineHv()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = sample_engine.generate_trigger_queue(hvdps_subslot, rail_type = 'HV', force_voltage=3, dutid = 15, rail = 5, uhc = 5,turbo=True)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_verify_expected_voltage_pass(self):
        sample_engine = SampleEngineHv()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.get_rail_voltage = Mock(return_value=10.8)
        result = sample_engine.verify_expected_voltage(board=hvdps_subslot, rail=5, uhc=3, rail_type='HV', expected_voltage=10)
        self.assertEqual(result,None)

    def test_verify_expected_voltage_fail(self):
        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.get_rail_voltage = Mock(return_value=5)
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_expected_voltage(board=hvdps_subslot, rail=5, uhc=3, rail_type='HV', expected_voltage=10)
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls),1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level,'error')
            self.assertRegex(log_message, 'is not within the allowable deviation')

    def test_verify_alarm_status_no_alarms(self):
        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.get_global_alarms = Mock(return_value = [])
        alarms = sample_engine.verify_alarm_status(board=hvdps_subslot,rail = 5, rail_type= 'HV')
        self.assertEqual(alarms, None)

    def test_verify_alarm_status_hv_alarm(self):
        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        sample_engine.Log = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        hvdps_subslot.ReadRegister = Mock(return_value=Mock(ClampAlarm=1,KelvinAlarm=0,fields = lambda: ['ClampAlarm','KelvinAlarm'] ))
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_alarm_status(board=hvdps_subslot, rail=5, rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls),2)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level,'error')

    def test_verify_alarm_status_lvm_alarm(self):
        sample_engine = SampleEngineLvm()
        sample_engine.Log = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.get_global_alarms = Mock(return_value=['DutRailAlarms'])
        hvdps_subslot.ReadRegister = Mock(
            return_value=Mock(UnderVoltageComparisonAlarm=1, OverVoltageComparisonAlarm=0, fields=lambda: ['UnderVoltageComparisonAlarm', 'OverVoltageComparisonAlarm']))
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_alarm_status(board=hvdps_subslot, rail=5, rail_type='LVM')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls),2)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level,'error')

    def test_verify_sample_header_no_header(self):
        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=[],rail=5, rail_type='HV')

    def test_verify_sample_header_hv_pass(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0xC00
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        verify_headers = sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5, rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(verify_headers, None)
        self.assertEqual(len(log_calls),0)

    def test_verify_sample_header_lvm_pass(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0x20
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineLvm()
        sample_engine.Log = Mock()
        verify_headers = sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5, rail_type='LVM')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(verify_headers, None)
        self.assertEqual(len(log_calls), 0)

    def test_verify_sample_header_fail_pointer(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010001
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0xC00
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5, rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls),1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level,'error')
            self.assertEqual(log_message, 'Sample pointer returned by fpga 65537, doesnt match expected pointer 65536')

    def test_verify_sample_header_fail_rate(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x1
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0xC00
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                            rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Sample rate returned by fpga 1, doesnt match expected rate 0')

    def test_verify_sample_header_fail_sample_count(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x41
        dummy_sample_header.RailVISelect = 0xC00
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                            rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Sample count returned by fpga 65, doesnt match expected count 64')

    def test_verify_sample_header_fail_rail_select_hv(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0xC01
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                                rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Rail 5 select returned by fpga 3073, doesnt match expected rails 3072')


    def test_verify_sample_header_fail_rail_select_lvm(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0x21
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineLvm()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                            rail_type='LVM', turbo = False)
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Rail 5 select returned by fpga 33, doesnt match expected rails 32')


    def test_verify_sample_header_fail_meta_lo(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0xC00
        dummy_sample_header.MetaLo = 0x3211
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                            rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Meta Data Low returned by fpga 0x3211, doesnt match expected value 0x3210')

    def test_verify_sample_header_fail_meta_high(self):
        dummy_sample_header = hvdps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailVISelect = 0xC00
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7655
        sample_headers = [dummy_sample_header]

        sample_engine = SampleEngineHv()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                            rail_type='HV')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Meta Data High returned by fpga 0x7655, doesnt match expected value 0x7654')

    def test_verify_sample_header_fail_rail_select_hc(self):
        rail_select_value = 0x210
        dummy_sample_header = hddps_registers.SampleHeaderStruct()
        dummy_sample_header.SamplePointer = 0x00010000
        dummy_sample_header.SampleRate = 0x0
        dummy_sample_header.SampleCount = 0x40
        dummy_sample_header.RailSelect = 0x210
        dummy_sample_header.MetaLo = 0x3210
        dummy_sample_header.MetaHi = 0x7654
        sample_headers = [dummy_sample_header]
        rail= 5
        expected_rail_select_value = (0x3 << (rail * 2))
        sample_engine = SampleEngineHc()
        sample_engine.Log = Mock()
        with self.assertRaises(sample_engine.sample_engine_exception):
            sample_engine.verify_sample_header(sample_headers=sample_headers, rail=5,
                                                                rail_type='HC')
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Rail {} select returned by fpga {}, doesnt match expected rails {}'.format(rail,rail_select_value,expected_rail_select_value))


    def test_calls_for_force_voltage_open_socket_scenario_with_sample_positive_hv(self):
        iterations = 1
        rail_type = 'HV'
        negative = False
        sample_engine = SampleEngineHv()
        sample_engine.env = Mock()
        dummy_sample_header = Mock()
        mock_board = Mock(RAIL_COUNT = {'HV':1},
                          UHC_COUNT = 1,
                          get_positive_random_voltage = Mock(return_value = 2.5),
                          get_negative_random_voltage= Mock(return_value = -2.5),
                          getSampleHeaders = Mock(return_value = [dummy_sample_header]))
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.generate_trigger_queue = Mock()
        sample_engine.verify_alarm_status = Mock(return_value=[])
        sample_engine.verify_expected_voltage = Mock(return_value=True)
        sample_engine.verify_sample_header = Mock(return_value=True)
        sample_engine.verify_sample_data = Mock(return_value=True)
        sample_engine.force_voltage_open_socket_scenario_with_sample(board=mock_board, rail_type=rail_type, iterations=iterations, negative=negative)

        generate_trigger_queue_calls = sample_engine.generate_trigger_queue.call_args_list
        self.assertEqual(len(generate_trigger_queue_calls),1)
        args, kwargs = generate_trigger_queue_calls[0]
        call_board,call_rail_type,call_force_voltage,call_dutid,call_rail,call_uhc,call_turbo = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, 2.5)
        self.assertTrue(0 <= call_dutid <= 63)
        self.assertEqual(call_rail, 0)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_turbo, False)

        verify_alarm_status_calls = sample_engine.verify_alarm_status.call_args_list
        self.assertEqual(len(verify_alarm_status_calls),1)
        args, kwargs = verify_alarm_status_calls[0]
        call_board,call_rail,call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_rail, 0)

        verify_expected_voltage_calls = sample_engine.verify_expected_voltage.call_args_list
        self.assertEqual(len(verify_expected_voltage_calls),1)
        args, kwargs = verify_expected_voltage_calls[0]
        call_board,call_rail,call_uhc, call_rail_type, call_voltage = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_rail, 0)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_voltage, 2.5)

        GetSampleHeaders_calls = mock_board.GetSampleHeaders.call_args_list
        self.assertEqual(len(GetSampleHeaders_calls),1)
        args, kwargs = GetSampleHeaders_calls[0]
        call_uhc,call_sample_header_count, call_rail_type= args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_sample_header_count, 1)
        self.assertEqual(call_uhc, 0)

        get_positive_random_voltage_calls = mock_board.get_positive_random_voltage.call_args_list
        self.assertEqual(len(get_positive_random_voltage_calls),1)
        args, kwargs = get_positive_random_voltage_calls[0]
        call_start,call_end,call_step= args
        self.assertEqual(call_start, 1)
        self.assertEqual(call_end, 15)
        self.assertEqual(call_step, 0.25)

        get_negative_random_voltage_calls = mock_board.get_negative_random_voltage.call_args_list
        self.assertEqual(len(get_negative_random_voltage_calls),0)

        verify_sample_data_calls = sample_engine.verify_sample_data.call_args_list
        self.assertEqual(len(verify_sample_data_calls),1)
        args, kwargs = verify_sample_data_calls[0]
        call_board,call_headers,call_voltage,call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_voltage, 2.5)

    def test_calls_for_force_voltage_open_socket_scenario_with_sample_negative_lc(self):
        iterations = 1
        rail_type = 'LC'
        negative = True
        sample_engine = SampleEngineLc()
        sample_engine.env = Mock()
        dummy_sample_header = Mock()
        mock_board = Mock(RAIL_COUNT = {'LC':1},
                          UHC_COUNT = 1,
                          get_positive_random_voltage = Mock(return_value = 2.5),
                          get_negative_random_voltage= Mock(return_value = -2.5),
                          getSampleHeaders = Mock(return_value = [dummy_sample_header]))
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.generate_trigger_queue = Mock()
        sample_engine.verify_alarm_status = Mock(return_value=[])
        sample_engine.verify_expected_voltage = Mock(return_value=True)
        sample_engine.verify_sample_header = Mock(return_value=True)
        sample_engine.verify_sample_data = Mock(return_value=True)
        sample_engine.force_voltage_open_socket_scenario_with_sample(board=mock_board, rail_type=rail_type, iterations=iterations, negative=negative)

        generate_trigger_queue_calls = sample_engine.generate_trigger_queue.call_args_list
        self.assertEqual(len(generate_trigger_queue_calls),1)
        args, kwargs = generate_trigger_queue_calls[0]
        call_board,call_rail_type,call_force_voltage,call_dutid,call_rail,call_uhc,call_turbo = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, -2.5)
        self.assertTrue(0 <= call_dutid <= 63)
        self.assertEqual(call_rail, 0)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_turbo, False)

        verify_alarm_status_calls = sample_engine.verify_alarm_status.call_args_list
        self.assertEqual(len(verify_alarm_status_calls),1)
        args, kwargs = verify_alarm_status_calls[0]
        call_board,call_rail,call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_rail, 0)

        verify_expected_voltage_calls = sample_engine.verify_expected_voltage.call_args_list
        self.assertEqual(len(verify_expected_voltage_calls),1)
        args, kwargs = verify_expected_voltage_calls[0]
        call_board,call_rail,call_uhc, call_rail_type, call_voltage = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_rail, 0)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_voltage, -2.5)

        GetSampleHeaders_calls = mock_board.GetSampleHeaders.call_args_list
        self.assertEqual(len(GetSampleHeaders_calls),1)
        args, kwargs = GetSampleHeaders_calls[0]
        call_uhc,call_sample_header_count, call_rail_type= args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_sample_header_count, 1)
        self.assertEqual(call_uhc, 0)

        get_positive_random_voltage_calls = mock_board.get_positive_random_voltage.call_args_list
        self.assertEqual(len(get_positive_random_voltage_calls),0)

        get_negative_random_voltage_calls = mock_board.get_negative_random_voltage.call_args_list
        self.assertEqual(len(get_negative_random_voltage_calls),1)
        args, kwargs = get_negative_random_voltage_calls[0]
        call_start,call_end,call_step= args
        self.assertEqual(call_start, -2)
        self.assertEqual(call_end, 0)
        self.assertEqual(call_step, 0.25)

        verify_sample_data_calls = sample_engine.verify_sample_data.call_args_list
        self.assertEqual(len(verify_sample_data_calls),1)
        args, kwargs = verify_sample_data_calls[0]
        call_board,call_headers,call_voltage,call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_voltage, -2.5)

    def test_calls_for_force_voltage_open_socket_scenario_with_sample_negative_lc_fail(self):
        iterations = 1
        rail_type = 'LC'
        negative = True
        sample_engine = SampleEngineLc()
        sample_engine.env = Mock()
        dummy_sample_header = Mock()
        mock_board = Mock(RAIL_COUNT = {'LC':1},
                          UHC_COUNT = 1,
                          get_positive_random_voltage = Mock(return_value = 2.5),
                          get_negative_random_voltage= Mock(return_value = -2.5),
                          getSampleHeaders = Mock(return_value = [dummy_sample_header]))
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.generate_trigger_queue = Mock()
        sample_engine.verify_alarm_status = Mock()
        sample_engine.verify_expected_voltage = Mock()
        sample_engine.verify_sample_header = Mock(side_effect=sample_engine.sample_engine_exception)
        sample_engine.verify_sample_data = Mock()
        sample_engine.Log = Mock()
        sample_engine.force_voltage_open_socket_scenario_with_sample(board=mock_board, rail_type=rail_type, iterations=iterations, negative=negative)
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Unexpected sample engine behavior seen on 1 of 1 UHC/Rail combinations during test iteration 1 of 1.')


    def test_calls_for_force_voltage_hv_to_lv_scenario_with_sample(self):
        iterations = 1
        rail_type = 'LVM'
        sample_engine = SampleEngineLvm()
        sample_engine.env = Mock()
        dummy_sample_header = Mock()
        mock_board = Mock(RAIL_COUNT={'LVM': 1},
                          UHC_COUNT=1,
                          get_positive_random_voltage=Mock(return_value=2.5),
                          getSampleHeaders=Mock(return_value=[dummy_sample_header]))
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.generate_lvm_trigger_queue = Mock()
        sample_engine.verify_alarm_status = Mock(return_value=[])
        sample_engine.verify_expected_voltage = Mock(return_value=True)
        sample_engine.verify_sample_header = Mock(return_value=True)
        sample_engine.verify_sample_data = Mock(return_value=True)
        sample_engine.force_voltage_hv_to_lv_scenario_with_sample(board=mock_board,iterations=iterations)

        generate_trigger_queue_calls = sample_engine.generate_lvm_trigger_queue.call_args_list
        self.assertEqual(len(generate_trigger_queue_calls), 1)
        args, kwargs = generate_trigger_queue_calls[0]
        call_board, call_force_rail, call_sense_rail, call_force_voltage, call_dutid, call_uhc = args
        self.assertEqual(call_sense_rail, 0)
        self.assertTrue(0 <= call_dutid <= 63)
        self.assertEqual(call_uhc, 0)

        verify_alarm_status_calls = sample_engine.verify_alarm_status.call_args_list
        self.assertEqual(len(verify_alarm_status_calls), 1)
        args, kwargs = verify_alarm_status_calls[0]
        call_board, call_rail, call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_rail, 0)

        verify_expected_voltage_calls = sample_engine.verify_expected_voltage.call_args_list
        self.assertEqual(len(verify_expected_voltage_calls), 1)
        args, kwargs = verify_expected_voltage_calls[0]
        call_board, call_rail, call_uhc, call_rail_type, call_voltage = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_rail, 0)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_voltage, 2.5)

        GetSampleHeaders_calls = mock_board.GetSampleHeaders.call_args_list
        self.assertEqual(len(GetSampleHeaders_calls), 1)
        args, kwargs = GetSampleHeaders_calls[0]
        call_uhc, call_sample_header_count, call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_sample_header_count, 1)
        self.assertEqual(call_uhc, 0)

        get_positive_random_voltage_calls = mock_board.get_positive_random_voltage.call_args_list
        self.assertEqual(len(get_positive_random_voltage_calls), 1)
        args, kwargs = get_positive_random_voltage_calls[0]
        call_start, call_end, call_step = args
        self.assertEqual(call_start, 1)
        self.assertEqual(call_end, 4)
        self.assertEqual(call_step, 0.125)

        get_negative_random_voltage_calls = mock_board.get_negative_random_voltage.call_args_list
        self.assertEqual(len(get_negative_random_voltage_calls), 0)

        verify_sample_data_calls = sample_engine.verify_sample_data.call_args_list
        self.assertEqual(len(verify_sample_data_calls), 1)
        args, kwargs = verify_sample_data_calls[0]
        call_board, call_headers, call_voltage,call_rail_type = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_voltage, 2.5)


    def test_calls_for_force_voltage_hv_to_lv_scenario_with_sample_fail(self):
        iterations = 1
        sample_engine = SampleEngineLvm()
        sample_engine.env = Mock()
        dummy_sample_header = Mock()
        mock_board = Mock(RAIL_COUNT={'LVM': 1},
                          UHC_COUNT=1,
                          get_positive_random_voltage=Mock(return_value=2.5),
                          getSampleHeaders=Mock(return_value=[dummy_sample_header]))
        sample_engine.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        sample_engine.generate_lvm_trigger_queue = Mock()
        sample_engine.Log = Mock()
        sample_engine.verify_alarm_status = Mock()
        sample_engine.verify_expected_voltage = Mock()
        sample_engine.verify_sample_header = Mock(side_effect = sample_engine.sample_engine_exception)
        sample_engine.verify_sample_data = Mock()
        sample_engine.force_voltage_hv_to_lv_scenario_with_sample(board=mock_board,iterations=iterations)
        log_calls = sample_engine.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level, 'error')
            self.assertEqual(log_message,
                             'Unexpected sample engine behavior seen on 1 of 1 UHC/Rail combinations during test iteration 1 of 1.')


    def test_initialize_sample_engine(self):
        mock_board = Mock()
        rail_type = 'HV'
        turbo = True
        uhc = 5
        sample_engine = SampleEngineHv()
        sample_engine.initialize_sample_engine(mock_board, rail_type, turbo, uhc)
        self.assertEqual(mock_board.InitializeTurboSampleEngine.call_count,1)
        self.assertEqual(mock_board.InitializeSampleEngine.call_count,0)


    def test_initialize_sample_engine_turbi(self):
        mock_board = Mock()
        rail_type = 'HV'
        turbo = False
        uhc = 5
        sample_engine = SampleEngineHv()
        sample_engine.initialize_sample_engine(mock_board, rail_type, turbo, uhc)
        self.assertEqual(mock_board.InitializeTurboSampleEngine.call_count,0)
        self.assertEqual(mock_board.InitializeSampleEngine.call_count,1)