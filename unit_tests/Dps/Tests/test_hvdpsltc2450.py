################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from Common import fval
from unittest.mock import Mock
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Dps.Tests.HvdpsLTC2450 import InterfaceTests

class LTC2450Interface(unittest.TestCase):


    def test_ltc2450_votage_read_pass(self):
        ltc2450Interfacetest = InterfaceTests()
        expected_voltage_from_hil = 0.59
        expected_voltage_from_register = 0.59
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        ltc2450Interfacetest.env = Mock()
        ltc2450Interfacetest.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.enableLtc2450AdcPolling = Mock()
        hvdps_subslot.disableLtc2450AdcPolling = Mock()
        hvdps_subslot.Ltc2450AdcVoltageRead = Mock(return_value=expected_voltage_from_hil)
        ltc2450Interfacetest.Log = Mock()
        hvdps_subslot.ReadRegister = Mock()
        ltc2450Interfacetest.convert_raw_adc_voltage_to_volts = Mock(return_value=expected_voltage_from_register)
        ltc2450Interfacetest.DirectedVoltageReadTest()
        log_calls = ltc2450Interfacetest.Log.call_args_list
        self.assertEqual(len(log_calls),1)
        for args,kwargs in log_calls:
            log_level, log_message = args
            self.assertEqual(log_level.upper(), 'INFO')

    def test_ltc2450_votage_read_fail(self):
        ltc2450Interfacetest = InterfaceTests()
        expected_voltage_from_hil = 0.59
        unexpected_voltage_from_register = 0.23
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        ltc2450Interfacetest.env = Mock()
        ltc2450Interfacetest.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.enableLtc2450AdcPolling = Mock()
        hvdps_subslot.Ltc2450AdcVoltageRead = Mock(return_value=expected_voltage_from_hil)
        hvdps_subslot.ReadRegister = Mock(return_value=Mock(AdcVoltage=unexpected_voltage_from_register))
        with self.assertRaises(fval.LoggedError):
            ltc2450Interfacetest.DirectedVoltageReadTest()

    def test_ltc2450_temperature_read_pass(self):
        ltc2450Interfacetest = InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        ltc2450Interfacetest.env = Mock()
        ltc2450Interfacetest.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        ltc2450Interfacetest.Log = Mock()
        hvdps_subslot.Ltc2450AdcVoltageRead = Mock()
        ltc2450Interfacetest.convert_voltage_to_temperature = Mock(return_value= 60)
        ltc2450Interfacetest.DirectedTemperatureReadTest()
        log_calls = ltc2450Interfacetest.Log.call_args_list
        self.assertEqual(len(log_calls),1)
        for args,kwargs in log_calls:
            log_level, log_message = args
            self.assertEqual(log_level.upper(), 'INFO')

    def test_ltc2450_temperature_read_fail(self):
        negative_30C_voltage_value = 0.71
        ltc2450Interfacetest = InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        ltc2450Interfacetest.env = Mock()
        ltc2450Interfacetest.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.Ltc2450AdcVoltageRead = Mock(return_value=negative_30C_voltage_value)
        with self.assertRaises(fval.LoggedError):
            ltc2450Interfacetest.DirectedTemperatureReadTest()

    def test_convert_raw_adc_voltage_to_volts(self):
        ltc2450Interfacetest = InterfaceTests()
        expected_converted_voltage =0.234649658203125
        converted_voltage =ltc2450Interfacetest.convert_raw_adc_voltage_to_volts(0x1234)
        self.assertEqual(converted_voltage,expected_converted_voltage)

    def test_convert_voltage_to_temperature(self):
        ltc2450Interfacetest = InterfaceTests()
        expected_temperature =30.000000000000025
        converted_temperature =ltc2450Interfacetest.convert_voltage_to_temperature(0.59)
        self.assertEqual(converted_temperature,expected_temperature)

if __name__ == '__main__':
    unittest.main()
