################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock
from Dps.Tests.HvdpsAdcCalibration import CurrentCalibration,VoltageCalibration

from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot

class HvdpsCurrentAdcCalibrationTest(unittest.TestCase):

    def test_parameters_for_current_calibration_scenario_7500MA(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpscurrentcalibration.current_calibration_scenario = Mock()
        hvdpscurrentcalibration.Directed7500MARegularCurrentCalibrationTest()
        current_calibration_parameters = hvdpscurrentcalibration.current_calibration_scenario.call_args
        self.assertNotEqual(len(current_calibration_parameters), 0)
        args, kwargs = current_calibration_parameters
        call_rail_type,call_board,call_forcevoltage,call_irange = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_irange, irange)

    def test_parameters_for_hv_voltage_calibration_scenario_7500MA(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpscurrentcalibration = VoltageCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpscurrentcalibration.hv_voltage_calibration_scenario= Mock()
        hvdpscurrentcalibration.DirectedHvRegularVoltageCalibrationTest()
        voltage_calibration_parameters = hvdpscurrentcalibration.hv_voltage_calibration_scenario.call_args
        self.assertNotEqual(len(voltage_calibration_parameters), 0)
        args, kwargs = voltage_calibration_parameters
        call_rail_type, call_board, call_forcevoltage, call_irange = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_irange, irange)

    def test_parameters_for_lvm_voltage_calibration_scenario(self):
        rail_type = 'LVM'
        irange = 'I_7500_MA'
        hvdpscurrentcalibration = VoltageCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpscurrentcalibration.lvm_voltage_calibration_scenario= Mock()
        hvdpscurrentcalibration.DirectedLvmRegularVoltageCalibrationTest()
        voltage_calibration_parameters = hvdpscurrentcalibration.lvm_voltage_calibration_scenario.call_args
        self.assertNotEqual(len(voltage_calibration_parameters), 0)
        args, kwargs = voltage_calibration_parameters
        call_rail_type, call_board, call_forcevoltage, call_irange = args
        self.assertEqual(call_rail_type, rail_type)

    def test_parameters_for_current_calibration_scenario_7500MA_turbo_mode(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpscurrentcalibration.current_calibration_scenario = Mock()
        hvdpscurrentcalibration.Directed7500MATurboCurrentCalibrationTest()
        current_calibration_parameters = hvdpscurrentcalibration.current_calibration_scenario.call_args
        self.assertNotEqual(len(current_calibration_parameters), 0)
        args, kwargs = current_calibration_parameters
        call_rail_type,call_board,call_forcevoltage,call_irange = args
        call_turbo = kwargs['turbo']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_turbo, True)
        self.assertEqual(call_irange, irange)

    def test_parameters_for_current_calibration_scenario_500MA(self):
        rail_type = 'HV'
        irange = 'I_500_MA'
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpscurrentcalibration.current_calibration_scenario = Mock()
        hvdpscurrentcalibration.Directed500MARegularCurrentCalibrationTest()
        current_calibration_parameters = hvdpscurrentcalibration.current_calibration_scenario.call_args
        self.assertNotEqual(len(current_calibration_parameters), 0)
        args, kwargs = current_calibration_parameters
        call_rail_type,call_board,call_forcevoltage,call_irange = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_irange, irange)

    def test_parameters_for_current_calibration_scenario_25MA(self):
        rail_type = 'HV'
        irange = 'I_25_MA'
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpscurrentcalibration.current_calibration_scenario = Mock()
        hvdpscurrentcalibration.Directed25MARegularCurrentCalibrationTest()
        current_calibration_parameters = hvdpscurrentcalibration.current_calibration_scenario.call_args
        self.assertNotEqual(len(current_calibration_parameters), 0)
        args, kwargs = current_calibration_parameters
        call_rail_type,call_board,call_forcevoltage,call_irange = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_irange, irange)

    def test_Directed7500MACurrentCalibrationTest_pass(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.cal_load_connect = Mock()
        self.configure_before_test(hvdps_subslot)
        hvdpscurrentcalibration.get_uhc_random_rail_tuple_list = Mock(return_value=[(2, 7)])
        hvdpscurrentcalibration.get_random_gain_offset_list = Mock(return_value=[(0x8000,0x8000)])
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdpscurrentcalibration.reset_calibration_gain_and_offset = Mock()
        hvdpscurrentcalibration.force_voltage_with_cal_load = Mock()
        hvdps_subslot.get_rail_current = Mock(return_value=0.3)
        hvdps_subslot.get_rail_voltage = Mock(return_value=3)
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        hvdpscurrentcalibration.set_regular_current_calibration_ram= Mock()
        hvdpscurrentcalibration.set_calibration_gain_offset = Mock()
        hvdpscurrentcalibration.verify_expected_voltage = Mock(return_value=True)
        hvdpscurrentcalibration.verify_expected_current = Mock(side_effect=[True,True])
        hvdps_subslot.cal_disconnect_force_sense_lines = Mock()
        hvdpscurrentcalibration.Log = Mock()
        hvdpscurrentcalibration.Directed7500MARegularCurrentCalibrationTest()
        log_calls = hvdpscurrentcalibration.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(len(log_calls), 1)

    def test_Directed7500MAVoltageCalibrationTest_pass(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpsvoltagecalibration = VoltageCalibration()
        hvdpsvoltagecalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpsvoltagecalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdpsvoltagecalibration.get_uhc_random_rail_tuple_list = Mock(return_value=[(2, 7)])
        hvdpsvoltagecalibration.get_random_gain_offset_list = Mock(return_value=[(0x8000, 0x8000)])
        hvdps_subslot.cal_load_connect = Mock()
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        hvdpsvoltagecalibration.reset_calibration_gain_and_offset = Mock()
        hvdpsvoltagecalibration.force_voltage_with_cal_load = Mock()
        hvdps_subslot.get_rail_voltage = Mock(return_value=3)
        hvdps_subslot.get_rail_current=Mock(return_value=0.3)
        hvdpsvoltagecalibration.configure_voltage_calibration_ram= Mock()
        hvdpsvoltagecalibration.set_calibration_gain_offset = Mock()
        hvdpsvoltagecalibration.verify_expected_current = Mock(return_value=True)
        hvdpsvoltagecalibration.verify_expected_voltage = Mock(side_effect=[True, True])
        hvdps_subslot.cal_disconnect_force_sense_lines = Mock()
        hvdpsvoltagecalibration.Log = Mock()
        hvdpsvoltagecalibration.DirectedHvRegularVoltageCalibrationTest()
        log_calls = hvdpsvoltagecalibration.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(len(log_calls), 1)

    def test_Directed7500MAVoltageCalibrationTest_fail(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpsvoltagecalibration = VoltageCalibration()
        hvdpsvoltagecalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpsvoltagecalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdpsvoltagecalibration.get_uhc_random_rail_tuple_list = Mock(return_value=[(2, 7)])
        hvdpsvoltagecalibration.get_random_gain_offset_list = Mock(return_value=[(0x8000, 0x8000)])
        hvdps_subslot.cal_load_connect = Mock()
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        hvdpsvoltagecalibration.reset_calibration_gain_and_offset = Mock()
        hvdpsvoltagecalibration.force_voltage_with_cal_load = Mock()
        hvdps_subslot.get_rail_voltage = Mock(return_value=3)
        hvdps_subslot.get_rail_current = Mock(return_value=0.3)
        hvdpsvoltagecalibration.configure_voltage_calibration_ram = Mock()
        hvdpsvoltagecalibration.set_calibration_gain_offset = Mock()
        hvdpsvoltagecalibration.verify_expected_current = Mock(return_value=True)
        hvdpsvoltagecalibration.verify_expected_voltage = Mock(side_effect=[True, hvdpsvoltagecalibration.CalibrationException])
        hvdps_subslot.cal_disconnect_force_sense_lines = Mock()
        hvdpsvoltagecalibration.Log = Mock()
        hvdpsvoltagecalibration.DirectedHvRegularVoltageCalibrationTest()
        log_calls = hvdpsvoltagecalibration.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(len(log_calls), 1)

    def test_Directed7500MACurrentCalibrationTest_fail(self):
        rail_type = 'HV'
        irange = 'I_7500_MA'
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdpscurrentcalibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.cal_load_connect = Mock()
        self.configure_before_test(hvdps_subslot)
        hvdpscurrentcalibration.get_uhc_random_rail_tuple_list = Mock(return_value=[(2, 7)])
        hvdpscurrentcalibration.get_random_gain_offset_list = Mock(return_value=[(0x8000, 0x8000)])
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdpscurrentcalibration.reset_calibration_gain_and_offset = Mock()
        hvdpscurrentcalibration.force_voltage_with_cal_load = Mock()
        hvdps_subslot.get_rail_current = Mock(return_value=0.3)
        hvdps_subslot.get_rail_voltage = Mock(return_value=3)
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        hvdpscurrentcalibration.set_regular_current_calibration_ram = Mock()
        hvdpscurrentcalibration.set_calibration_gain_offset = Mock()
        hvdpscurrentcalibration.verify_expected_voltage = Mock(return_value=True)
        hvdpscurrentcalibration.verify_expected_current = Mock(side_effect=[True, hvdpscurrentcalibration.CalibrationException])
        hvdps_subslot.cal_disconnect_force_sense_lines = Mock()
        hvdpscurrentcalibration.Log = Mock()
        hvdpscurrentcalibration.Directed7500MARegularCurrentCalibrationTest()
        log_calls = hvdpscurrentcalibration.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(len(log_calls), 1)

    def test_get_real_calibration_gain_and_offset(self):
        hvdpscurrentcalibration = CurrentCalibration()
        self.assertEqual((hvdpscurrentcalibration.get_real_current_calibration_gain_and_offset(0x0000, 0x8000, 'I_7500_MA')), (0.5, 0))
        self.assertEqual((hvdpscurrentcalibration.get_real_current_calibration_gain_and_offset(0x8000, 0x8000, 'I_7500_MA')), (1.0, 0))
        calibration_gain_real, calibration_offset_real = hvdpscurrentcalibration.get_real_current_calibration_gain_and_offset(0xFFFF, 0x8000, 'I_7500_MA')
        self.assertEqual((round(calibration_gain_real, 2), round(calibration_offset_real, 2)), (1.5, 0))

        self.assertEqual((hvdpscurrentcalibration.get_real_current_calibration_gain_and_offset(0x8000, 0x0000, 'I_500_MA')), (1, -0.25))
        self.assertEqual((hvdpscurrentcalibration.get_real_current_calibration_gain_and_offset(0x8000, 0x8000, 'I_500_MA')), (1, 0))
        calibration_gain_real, calibration_offset_real = hvdpscurrentcalibration.get_real_current_calibration_gain_and_offset(0x8000, 0xFFFF, 'I_500_MA')
        self.assertEqual((round(calibration_gain_real, 2), round(calibration_offset_real, 2)), (1, 0.25))

    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()
        inst_subslot.ResetVoltageSoftSpanCode = Mock()
        inst_subslot.ResetCurrentSoftSpanCode = Mock()

    def test_get_random_gain_offset_list(self):
        hvdpscurrentcalibration = CurrentCalibration()
        hvdpscurrentcalibration.get_random_gain_offset_list()
