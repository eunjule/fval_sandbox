################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_SetMode.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the V Measure mode
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 01/04/18
#       Group: HDMT FPGA Validation
################################################################################


import unittest
from unittest.mock import Mock
import time

time.sleep = lambda x: None

from Common import fval
from Common.instruments.dps import hddps_registers as hddps_regs
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from datetime import datetime
from Dps.Tests.SetMode import OperationModes

AD5560_BIT_15_SET = 0x8000
AD5560_BIT_15_RESET = 0x0000

class OperationModesTestClass(unittest.TestCase):

    def tearDown(self):
        v_measure = OperationModes('RandomRailHCAd5560AndRailModeValRegisterImpactTest')
        v_measure.env = Mock()
        v_measure._outcome = Mock(failures=[], errors=[])
        v_measure._softErrors = []
        v_measure.start_time = datetime.now()
        v_measure.tearDown()

    def test_if_ad5560_bit_15_fails_to_reset_after_vmmod_set(self):
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=AD5560_BIT_15_SET)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_vmmode(
                hddps_subslot, 2,'HC')

    def test_if_HclcRailModeVal_register_fails_to_get_updated_after_vmmod_set(self):
        hclcrailmodeval_incorrect_vmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_vmode_data.value = 0x0040
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_vmode_data)
        hddps_subslot.ReadAd5560Register = Mock(return_value=AD5560_BIT_15_RESET)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_vmmode(
                hddps_subslot, 2,'LC')

    def test_HclcRailModeVal_and_ad5560_register_gets_updated_after_vmmod_set(self):
        hclcrailmodeval_correct_vmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_correct_vmode_data.value = 0x0030
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_correct_vmode_data)
        hddps_subslot.ReadAd5560Register = Mock(return_value=AD5560_BIT_15_RESET)
        v_measure.verify_set_vmmode(
            hddps_subslot, 2,'VLC')

    def test_if_HclcRailModeVal_register_fails_to_get_updated_after_vfmod_set(self):
        dps_two_bit_nine_set = 0x0200
        hclcrailmodeval_incorrect_vfmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_vfmode_data.value = 0x0030
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register= Mock(return_value=dps_two_bit_nine_set)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_vfmode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_vfmode(
                hddps_subslot, 2,'HC')

    def test_if_ad5560_register_fails_to_get_updated_for_ganging_after_vfmod_set(self):
        dps_two_bit_nine_set_incorrect_value = 0x0500
        hclcrailmodeval_incorrect_vfmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_vfmode_data.value = 0x0000
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register= Mock(return_value=dps_two_bit_nine_set_incorrect_value)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_vfmode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_vfmode(
                hddps_subslot, 2,'LC')

    def test_if_ad5560_register_fails_to_get_updated_for_slave_after_vfmod_set(self):
        dps_two_bit_nine_set_incorrect_value = 0x0600
        hclcrailmodeval_incorrect_vfmode_value = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_vfmode_value.value = 0x0000
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register= Mock(return_value=dps_two_bit_nine_set_incorrect_value)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_vfmode_value)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_vfmode(
                hddps_subslot, 2,'LC')

    def test_HclcRailModeVal_and_ad5560_register_gets_updated_after_vfmod_set(self):
        hclcrailmodeval_correct_vfmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_correct_vfmode_data.value = 0x0000
        dps_two_bit_nine_set = 0x0200
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=dps_two_bit_nine_set)
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_correct_vfmode_data)
        v_measure.verify_set_vfmode(
            hddps_subslot, 2, 'HC')


    def test_if_HclcRailModeVal_register_fails_to_get_updated_after_ifmod_set(self):
        dps_two_bit_nine_set = 0x0600
        hclcrailmodeval_incorrect_ifmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_ifmode_data.value = 0x0030
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register= Mock(return_value=dps_two_bit_nine_set)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_ifmode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_ifmode(
                hddps_subslot, 2 ,'LC')

    def test_if_ad5560_register_fails_to_get_updated_for_ganging_after_ifmod_set(self):
        dps_two_bit_nine_set_incorrect_value = 0x0500
        hclcrailmodeval_incorrect_ifmode_data= hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_ifmode_data.value = 0x0000
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register= Mock(return_value=dps_two_bit_nine_set_incorrect_value)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_ifmode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_ifmode(
                hddps_subslot, 2,'VLC')

    def test_if_ad5560_register_fails_to_get_updated_for_slave_after_ifmod_set(self):
        dps_two_bit_nine_set_incorrect_value = 0x0200
        hclcrailmodeval_incorrect_ifmode_data =  hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_ifmode_data.value = 0x0000
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register= Mock(return_value=dps_two_bit_nine_set_incorrect_value)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_ifmode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_ifmode(
                hddps_subslot, 2,'HC')

    def test_HclcRailModeVal_and_ad5560_register_gets_updated_after_ifmod_set(self):
        hclcrailmodeval_correct_ifmode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_correct_ifmode_data.value = 0x0010
        dps_two_bit_nine_set = 0x0600
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=dps_two_bit_nine_set)
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_correct_ifmode_data)
        v_measure.verify_set_ifmode(
            hddps_subslot, 2,'HC')

    def test_if_HclcRailModeVal_register_fails_to_get_updated_after_ismod_set(self):
        dps_two_bit_nine_set = 0x0600
        hclcrailmodeval_incorrect_ismode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_ismode_data.value = 0x0030
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=dps_two_bit_nine_set)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_ismode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_ismode(
                hddps_subslot, 2,'LC')

    def test_if_ad5560_register_fails_to_get_updated_for_ganging_after_ismod_set(self):
        dps_two_bit_nine_set_incorrect_value = 0x0500
        hclcrailmodeval_incorrect_ismode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_ismode_data.value = 0x0000
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=dps_two_bit_nine_set_incorrect_value)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_ismode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_ismode(
                hddps_subslot, 2,'VLC')

    def test_if_ad5560_register_fails_to_get_updated_for_slave_after_ismod_set(self):
        dps_two_bit_nine_set_incorrect_value = 0x0200
        hclcrailmodeval_incorrect_ismode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_incorrect_ismode_data.value = 0x0000
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=dps_two_bit_nine_set_incorrect_value)
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_incorrect_ismode_data)
        with self.assertRaises(fval.LoggedError):
            v_measure.verify_set_ismode(
                hddps_subslot, 2,'HC')

    def test_HclcRailModeVal_and_ad5560_register_gets_updated_after_ismod_set(self):
        hclcrailmodeval_correct_ismode_data = hddps_regs.HclcRailModeVal()
        hclcrailmodeval_correct_ismode_data.value = 0x0020
        dps_two_bit_nine_set = 0x0600
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        v_measure.env = Mock()
        v_measure.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        hddps_subslot.WriteAd5560Register = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.ReadAd5560Register = Mock(return_value=dps_two_bit_nine_set)
        hddps_subslot.ReadRegister = Mock(return_value=hclcrailmodeval_correct_ismode_data)
        v_measure.verify_set_ismode(
            hddps_subslot, 2,'LC')


    def test_method_can_call_each_method_only_once(self):
        dutid, hddps_subslot, rails, resourceid_string, v_measure, write_call_count = self.helper_to_mock_methods()
        v_measure.run_bar_commands(rails, dutid, hddps_subslot, 'HC')
        self.assertEqual(hddps_subslot.WriteTQHeaderViaBar2.call_count, write_call_count)
        self.assertEqual(hddps_subslot.WriteBar2RailCommand.call_count, write_call_count)
        self.assertEqual(v_measure.randomize_set_mode_bar_calls.call_count, write_call_count)
        self.assertEqual(hddps_subslot.WriteTQFooterViaBar2.call_count, write_call_count)
        self.assertEqual(hddps_subslot.CheckTqNotifyAlarm.call_count, write_call_count)

    def test_function_can_call_each_function_with_correct_data(self):
        dutid, hddps_subslot, rails, resourceid_string, v_measure, write_call_count = self.helper_to_mock_methods()
        v_measure.run_bar_commands(rails, dutid, hddps_subslot, 'HC')
        args, kwargs = hddps_subslot.WriteTQHeaderViaBar2.call_args
        call_dutid, call_rail, call_resourceid_string = args
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rails)
        self.assertEqual(call_resourceid_string, resourceid_string)
        args, kwargs = hddps_subslot.WriteTQFooterViaBar2.call_args
        call_dutid, call_rail, call_resourceid_string = args
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rails)
        self.assertEqual(call_resourceid_string, resourceid_string)
        args, kwargs = hddps_subslot.CheckTqNotifyAlarm.call_args
        call_dutid,  call_resourceid_string = args
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_resourceid_string, resourceid_string)

    def helper_to_mock_methods(self):
        v_measure = OperationModes()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        write_call_count = 1
        dutid = 6
        rails = 16
        resourceid_string = 'HC'
        hddps_subslot.initialize_hddps_rail_for_test = Mock()
        hddps_subslot.WriteTQHeaderViaBar2 = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        v_measure.randomize_set_mode_bar_calls = Mock()
        v_measure.call_bartwo_methods = Mock()
        hddps_subslot.WriteTQFooterViaBar2 = Mock()
        hddps_subslot.CheckTqNotifyAlarm = Mock()
        return dutid, hddps_subslot, rails, resourceid_string, v_measure, write_call_count

    def test_randomize_set_mode_bartwo_calls(self):
        v_measure = OperationModes()
        list_of_methods =  v_measure.randomize_set_mode_bar_calls()
        self.assertEqual(len(list_of_methods),4)
        self.assertEqual(len(set(list_of_methods)),4)