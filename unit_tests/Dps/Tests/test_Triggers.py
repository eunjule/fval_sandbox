################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: 
#-------------------------------------------------------------------------------
#     Purpose: 
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 11/15/17
#       Group: HDMT FPGA Validation
################################################################################
from datetime import datetime
import random
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Common import fval
from Dps.Tests.Triggers import TriggerFlow
from Common.instruments.dps import hddps
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps import hddps_registers as hddps_regs


class TriggersTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None


    def test_if_RC_receives_correct_triggers_PassTest(self):
        '''Test to verify if RC receives correct triggers can the test pass'''
        with patch('Common.instruments.dps.hddpsSubslot.hil') as mock_subslot_hil, \
                patch.object(random, 'getrandbits')as mock_Random,\
                patch('Common.instruments.dps.dpsSubslot.hil') as mock_dpssubslot_hil:
            fpga_version_dummy = 0x4000000
            trigger_to_be_sent = 3691495472
            trigger_to_be_received = 3691495472
            slot = 2
            conditions = TriggerFlow()
            conditions.empty_trigger_buffer_and_ddr_for_slot_and_subslot = Mock()
            my_hddps = hddps.Hddps(slot, self.mock_rc)
            conditions.env = Mock()
            conditions.env.instruments = {slot:my_hddps}
            conditions.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([my_hddps.subslots[0],my_hddps.subslots[1]]))
            mock_dpssubslot_hil.dpsBarRead = Mock(return_value = fpga_version_dummy)
            mock_dpssubslot_hil.dpsBarWrite = Mock()
            mock_Random.return_value = trigger_to_be_sent
            conditions.env.read_trigger_up = Mock(side_effect=[trigger_to_be_received,trigger_to_be_received])
            with self.assertRaises(StopIteration):
                conditions.DirectedExhaustiveTriggerFromDpsToRcTest()

    def test_if_RC_receives_incorrect_triggers_FailTest(self):
        '''Test to verify if RC receives incorrect triggers can the test fail'''
        with patch('Common.instruments.dps.hddpsSubslot.hil') as mock_subslot_hil, \
                patch('Common.instruments.dps.dpsSubslot.hil') as mock_dpssubslot_hil,\
            patch.object(random, 'getrandbits')as mock_Random:
            fpga_dummy_version = 0x4000000
            trigger_to_be_sent = 134725680
            trigger_to_be_received_mismatch = 134725681
            slot = 2
            conditions = TriggerFlow()
            my_hddps = hddps.Hddps(slot, self.mock_rc)
            conditions.env = Mock()
            conditions.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([my_hddps.subslots[0],my_hddps.subslots[1]]))
            my_hddps.subslots[0].empty_trigger_buffer = Mock()
            my_hddps.subslots[1].empty_trigger_buffer = Mock()
            mock_dpssubslot_hil.dpsBarRead = Mock(return_value = fpga_dummy_version)
            mock_dpssubslot_hil.dpsBarWrite = Mock()
            mock_Random.return_value = trigger_to_be_sent
            conditions.env.read_trigger_up = Mock(return_value=trigger_to_be_received_mismatch)
            with self.assertRaises(fval.LoggedError):
                conditions.DirectedExhaustiveTriggerFromDpsToRcTest()

    def test_if_DPS_receives_correct_triggers_Test(self):
        '''Test to verify if DPS receives correct triggers can the test pass'''
        slot = 2
        subslot = 0
        expected = 0x4054000
        global_register_value = hddps_regs.GLOBAL_ALARMS()
        global_register_value.value = 0
        trigger_buffer_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_value.value = 0x4054000
        hddps_instrument = hddps.Hddps(slot, self.mock_rc)
        hddps_subslot = HddpsSubslot(hddps_instrument, slot, subslot, self.mock_rc)
        TriggerMethods = TriggerFlow()
        hddps_subslot.ReadRegister = Mock(side_effect = [trigger_buffer_value,global_register_value])
        TriggerMethods.read_trigger_rc_to_dps(expected,hddps_subslot)

    def test_if_DPS_receives_incorrect_triggers_Test(self):
        '''Test to verify if DPS receives incorrect triggers can the test pass'''
        slot = 2
        subslot = 0
        expected = 0x4054000
        global_register_value = hddps_regs.GLOBAL_ALARMS()
        global_register_value.value = 0
        trigger_buffer_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_value.value = 0x4054001
        hddps_instrument = hddps.Hddps(slot, self.mock_rc)
        hddps_subslot = HddpsSubslot(hddps_instrument, slot, subslot, self.mock_rc)
        TriggerMethods = TriggerFlow()
        hddps_subslot.ReadRegister = Mock(side_effect=[trigger_buffer_value, global_register_value])
        with self.assertRaises(fval.LoggedError):
            TriggerMethods.read_trigger_rc_to_dps(expected, hddps_subslot)

    def test_DPS_receives_incorrect_trigger_verify_if_its_from_same_board_print_global_register_and_fail_Test(self):
        '''Test to verify if DPS receives incorrect triggers can the test pass'''
        slot = 2
        subslot = 0
        expected = 0x4058002
        global_register_value = hddps_regs.GLOBAL_ALARMS()
        global_register_value.value = 1
        trigger_buffer_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_value.value = 0x4068002
        hddps_instrument = hddps.Hddps(slot, self.mock_rc)
        hddps_subslot = HddpsSubslot(hddps_instrument, slot, subslot, self.mock_rc)
        TriggerMethods = TriggerFlow()
        hddps_subslot.ReadRegister = Mock(side_effect=[trigger_buffer_value, global_register_value])
        with self.assertRaises(fval.LoggedError):
            TriggerMethods.read_trigger_rc_to_dps(expected, hddps_subslot)

    def test_DPS_receives_incorrect_trigger_verify_if_its_from_different_board_skip_and_read_next_pass_Test(self):
        '''Test to verify if DPS receives incorrect triggers can the test pass'''
        slot = 2
        subslot = 0
        expected = 0x4058002
        global_register_value = hddps_regs.GLOBAL_ALARMS()
        global_register_value.value = 0
        trigger_buffer_wrong_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_wrong_value.value = 0x4068002
        trigger_buffer_correct_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_correct_value.value = 0x4058002
        hddps_instrument = hddps.Hddps(slot, self.mock_rc)
        hddps_subslot = HddpsSubslot(hddps_instrument, slot, subslot, self.mock_rc)
        TriggerMethods = TriggerFlow()
        hddps_subslot.ReadRegister = Mock(side_effect=[trigger_buffer_wrong_value, global_register_value,trigger_buffer_correct_value])
        TriggerMethods.read_trigger_rc_to_dps(expected, hddps_subslot)

    def test_DPS_receives_incorrect_trigger_verify_if_its_from_different_board_skip_and_read_next_fail_Test(self):
        '''Test to verify if DPS receives incorrect triggers can the test pass'''
        slot = 2
        subslot = 0
        expected = 0x4058002
        global_register_value = hddps_regs.GLOBAL_ALARMS()
        global_register_value.value = 0
        trigger_buffer_wrong_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_wrong_value.value = 0x4068002
        trigger_buffer_correct_value = hddps_regs.TRIGGERDOWNTEST()
        trigger_buffer_correct_value.value = 0x4078002
        hddps_instrument = hddps.Hddps(slot, self.mock_rc)
        hddps_subslot = HddpsSubslot(hddps_instrument, slot, subslot, self.mock_rc)
        TriggerMethods = TriggerFlow()
        hddps_subslot.ReadRegister = Mock(side_effect=[trigger_buffer_wrong_value, global_register_value, trigger_buffer_correct_value])
        with self.assertRaises(fval.LoggedError):
            TriggerMethods.read_trigger_rc_to_dps(expected, hddps_subslot)