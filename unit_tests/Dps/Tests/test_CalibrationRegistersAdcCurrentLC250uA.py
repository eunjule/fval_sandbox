################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegistersAdcCurrent
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers for ADC current
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.CalibrationRegistersAdcCurrentLC250uA import Conditions

class CalibrationRegistersAdcCurrentLC250uATests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('LcCurrentAdc250uACalibrationTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()
