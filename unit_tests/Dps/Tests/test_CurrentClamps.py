################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CurrentClamps
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS commands Current Clamps on AD5660
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz/Jeff Nguyen
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.CurrentClamps import Conditions


class CurrentClampsTests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('HCCurrentClampCalibrationHigh24000MaTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()

