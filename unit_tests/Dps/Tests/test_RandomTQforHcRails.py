###################################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# --------------------------------------------------------------------------------------------------
#    Filename: RandomTQforHcRails
# --------------------------------------------------------------------------------------------------
#     Purpose: Randomizing various attributes of Trigger Queue for more comprehensive test coverage
# --------------------------------------------------------------------------------------------------
#  Created by: Shaariq Shaikh
#        Date: 
#       Group: HDMT FPGA DEVELOPMENT
###################################################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.RandomTQforHcRails import Conditions


class RandomTQforHCRailsTests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('RandomSubmodesforVFORCEModeTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()



