################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_DutdomainiD.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test to verify if correct dut domain id sent to RC
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 01/30/18
#       Group: HDMT FPGA Validation
################################################################################

import random
import unittest
from Common import configs
from unittest.mock import Mock
from unittest.mock import patch

from Common import fval
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from datetime import datetime
from Dps.Tests.DutDomainId import DutDomainIDinRC

class DutDomainIDinRCClass(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None

    def test_RandomDutDomainIDSenttoRCValidationTest_call_methods_with_correct_parameters(self):
        rails = 5
        resource_id = 'HC'
        uhc = 7
        dut_domain_id_in_rc = DutDomainIDinRC()
        dut_domain_id_in_rc.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.SetRailsToSafeState = Mock()
        hddps_subslot.EnableOnlyOneUhc = Mock()
        hddps_subslot.ConfigureUhcRail = Mock()
        dut_domain_id_in_rc.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        dut_domain_id_in_rc.CheckForTCLoopingCompatableFpga = Mock()
        dut_domain_id_in_rc.run_tqnotify_bar_commands = Mock()
        dut_domain_id_in_rc.read_rc_verify_dutdomain_id = Mock()
        dut_domain_id_in_rc.generate_random_rail_dutdomainids = Mock(return_value= [(5,58)])
        dut_domain_id_in_rc.RandomHCRailDutDomainIDSenttoRCValidationTest()
        args, kwargs = dut_domain_id_in_rc.run_tqnotify_bar_commands.call_args
        call_uhc, call_rail, call_board ,call_resource_id = args
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_rail, rails)
        self.assertEqual(call_resource_id, resource_id)

    def test_method_can_call_each_method_with_correct_data(self):
        uhc, dut_domain_id_in_rc, dutdomainId, dutid, hddps_subslot, rails, resource_id = self.helper_to_mock_methods()
        dut_domain_id_in_rc.run_tqnotify_bar_commands(uhc, rails, hddps_subslot, resource_id)
        args, kwargs = hddps_subslot.WriteTQHeaderViaBar2.call_args
        call_uhc, call_rail, call_resourceid_string = args
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_rail, rails)
        self.assertEqual(call_resourceid_string, resource_id)
        args, kwargs = hddps_subslot.WriteTQFooterViaBar2.call_args
        call_uhc, call_rail, call_resourceid_string = args
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_rail, rails)
        self.assertEqual(call_resourceid_string, resource_id)
        args, kwargs = hddps_subslot.CheckTqNotifyAlarm.call_args
        call_uhc, call_resourceid_string = args
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_resourceid_string, resource_id)

    def test_if_correct_dutdomain_id_received(self):
        expected_trigger_value = 0xA00000
        dutdomainid_to_check = 10
        dut_domain_id_in_rc = DutDomainIDinRC()
        dut_domain_id_in_rc.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.rc.read_trigger_up = Mock(return_value = expected_trigger_value)
        dut_domain_id_in_rc.read_rc_verify_dutdomain_id(dutdomainid_to_check ,hddps_subslot)

    def test_if_incorrect_dutdomain_id_received(self):
        expected_trigger_value = 0xB00000
        dutdomainid_to_check = 10
        dut_domain_id_in_rc = DutDomainIDinRC()
        dut_domain_id_in_rc.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.rc.read_trigger_up = Mock(return_value = expected_trigger_value)
        with self.assertRaises(fval.LoggedError):
            dut_domain_id_in_rc.read_rc_verify_dutdomain_id(dutdomainid_to_check ,hddps_subslot)


    def helper_to_mock_methods(self):
        dut_domain_id_in_rc = DutDomainIDinRC()
        dut_domain_id_in_rc.env = Mock()
        rails = 5
        dutdomainId = 58
        dutid = 2
        resource_id = 'VLC'
        uhc = 4
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.initialize_hddps_rail_for_test = Mock()
        hddps_subslot.WriteTQHeaderViaBar2 = Mock()
        hddps_subslot.WriteTQFooterViaBar2 = Mock()
        hddps_subslot.WriteBar2RailCommand = Mock()
        hddps_subslot.CheckTqNotifyAlarm = Mock()
        dut_domain_id_in_rc.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
        dut_domain_id_in_rc.CheckForTCLoopingCompatableFpga = Mock()
        return uhc, dut_domain_id_in_rc, dutdomainId, dutid, hddps_subslot, rails, resource_id
