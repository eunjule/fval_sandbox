################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegistersAdcCurrent
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers for ADC current
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.CalibrationRegistersAdcCurrentLC1200mA import Conditions

class CalibrationRegistersAdcCurrentLC1200mATests(unittest.TestCase):


    def test_tearDown(self):
        conditions = Conditions('LcCurrentAdc1200mACalibrationTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()
