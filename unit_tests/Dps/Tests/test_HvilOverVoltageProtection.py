################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock
from unittest.mock import patch
from Common.instruments.dps.hvdpsCombo import HvdpsCombo
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps.hvilSubslot import HvilSubslot
from Dps.Tests.HvilAlarms import OverVoltage,OverCurrent

class OverVoltageTestClass(unittest.TestCase):

    def test_over_voltage_scenario_parameters(self):
        hvil_over_voltage = OverVoltage()
        force_rail_type = 'HV'
        sense_rail_type = 'RLOAD'
        hvil_over_voltage.env = Mock()
        hvil_over_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvil_over_voltage.over_voltage_scenario = Mock()
        hvil_over_voltage.DirectedRloadOverVoltageTest()
        validate_clamp_alarm_scenario = hvil_over_voltage.over_voltage_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        board,dutid,force_voltage,ov_high_limit,ov_low_limit,call_force_rail_type,call_sense_rail_type,iterations = args
        self.assertEqual(call_force_rail_type, force_rail_type)
        self.assertEqual(call_sense_rail_type, sense_rail_type)

    def test_over_current_scenario_parameters(self):
        hvil_over_current = OverCurrent()
        force_rail_type = 'HV'
        sense_rail_type = 'RLOAD'
        expected_alarm = 'ChannelOverCurrent'
        hvil_over_current.env = Mock()
        hvil_over_current.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvil_over_current.over_current_scenario = Mock()
        hvil_over_current.DirectedRloadOverCurrentTest()
        validate_clamp_alarm_scenario = hvil_over_current.over_current_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        board, dutId, force_voltage, call_force_rail_type, call_sense_rail_type, alarm_condition_i_clamp_limit, iterations,call_expected_alarm = args
        self.assertEqual(call_force_rail_type, force_rail_type)
        self.assertEqual(call_sense_rail_type, sense_rail_type)
        self.assertEqual(call_expected_alarm, expected_alarm)

    def test_over_voltage_scenario_global_alarms_found(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil:
            rail = 3
            uhc = 5
            dutid = 15
            force_voltage =2
            hvil_over_voltage = OverVoltage()
            alarm_received = 'ChannelGlobalAlarm'
            hvil_over_voltage.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11,rc=Mock())

            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_over_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
            self.configure_mb_before_test(hvdps_inst)
            hvil_subslot.SetRailsToSafeState = Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits= Mock()
            hvil_subslot.SetDefaultCurrentClampLimit = Mock()
            hvil_subslot.ClearDpsAlarms= Mock()
            hvil_over_voltage.Log = Mock()
            hvil_subslot.setup_load_control = Mock()
            hvil_over_voltage.setup_steady_state = Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(return_value=[])
            hvil_subslot.get_global_alarms =Mock(return_value=[alarm_received])
            hvil_subslot.load_disconnect = Mock()
            hvdps_inst.subslots[0].cal_disconnect_force_sense_lines = Mock()

            hvil_over_voltage.over_voltage_scenario(hvil_subslot, dutid, force_voltage, high_limit=3, low_limit=-0.5, force_rail_type='HV', sense_rail_type='RLOAD', iterations=1)
            log_calls = hvil_over_voltage.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
                self.assertEqual(len(log_calls), 2)

    def test_over_voltage_scenario_unexpected_global_alarms_found_in_mb(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil:
            rail = 3
            uhc = 5
            dutid = 15
            force_voltage =2
            hvil_over_voltage = OverVoltage()
            alarm_received = 'HclcRailAlarms'
            hvil_over_voltage.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11,rc=Mock())

            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_over_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
            self.configure_mb_before_test(hvdps_inst)
            hvil_subslot.SetRailsToSafeState = Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits= Mock()
            hvil_subslot.SetDefaultCurrentClampLimit = Mock()
            hvil_subslot.ClearDpsAlarms= Mock()
            hvil_over_voltage.Log = Mock()
            hvil_subslot.setup_load_control = Mock()
            hvil_over_voltage.setup_steady_state = Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(side_effect=[[],[alarm_received]])
            hvil_subslot.get_global_alarms =Mock(return_value=[])
            hvil_over_voltage.verify_rail_voltage = Mock(return_value=True)
            hvil_over_voltage.set_over_voltage_condition=Mock()
            hvil_subslot.load_disconnect = Mock()
            hvdps_inst.subslots[0].cal_disconnect_force_sense_lines = Mock()

            hvil_over_voltage.over_voltage_scenario(hvil_subslot, dutid, force_voltage, high_limit=3, low_limit=-0.5, force_rail_type='HV', sense_rail_type='RLOAD', iterations=1)
            log_calls = hvil_over_voltage.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
                self.assertEqual(len(log_calls), 2)

    def test_over_voltage_alarms_received(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil:
            rail = 3
            uhc = 5
            dutid = 15
            force_voltage =2
            hvil_over_voltage = OverVoltage()
            hvil_over_voltage.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11,rc=Mock())

            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_over_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
            self.configure_mb_before_test(hvdps_inst)
            hvil_subslot.SetRailsToSafeState = Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits= Mock()
            hvil_subslot.SetDefaultCurrentClampLimit = Mock()
            hvil_subslot.ClearDpsAlarms= Mock()
            hvil_over_voltage.Log = Mock()
            hvil_subslot.setup_load_control = Mock()
            hvil_over_voltage.setup_steady_state = Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(side_effect=[[],[]])
            hvil_subslot.get_global_alarms =Mock(return_value=[])
            hvil_over_voltage.verify_rail_voltage = Mock(return_value=True)
            hvil_over_voltage.set_over_voltage_condition=Mock()
            hvil_subslot.check_per_channel_alarm_details = Mock(return_value=True)
            hvil_subslot.load_disconnect = Mock()
            hvdps_inst.subslots[0].cal_disconnect_force_sense_lines = Mock()

            hvil_over_voltage.over_voltage_scenario(hvil_subslot, dutid, force_voltage, high_limit=3, low_limit=-0.5, force_rail_type='HV', sense_rail_type='RLOAD', iterations=1)
            log_calls = hvil_over_voltage.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
                self.assertEqual(len(log_calls), 1)

    def test_over_current_alarms_received(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil:
            rail = 3
            uhc = 5
            dutid = 15
            force_voltage = 2
            hvil_over_current = OverCurrent()
            hvil_over_current.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11, rc=Mock())

            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_over_current.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
            self.configure_mb_before_test(hvdps_inst)
            hvil_subslot.SetRailsToSafeState = Mock()
            hvil_subslot.SetThermalCurrentClampLimit = Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits = Mock()
            hvil_subslot.SetDefaultCurrentClampLimit = Mock()
            hvil_subslot.ClearDpsAlarms = Mock()
            hvil_subslot.EnableOnlyOneUhc = Mock()
            hvil_subslot.ConfigureUhcRail = Mock()
            hvil_subslot.ClearRailSafeState = Mock()
            hvil_subslot.WriteTQHeaderViaBar2 = Mock()
            hvil_over_current.get_over_current_alarm_filter_count = Mock(return_value=20)
            hvil_over_current.set_over_current_alarm_filter_count = Mock()
            hvil_over_current.set_over_current_condition_using_rail_command = Mock()
            hvil_over_current.Log = Mock()
            hvil_subslot.setup_load_control = Mock()
            hvil_over_current.setup_steady_state = Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(side_effect=[[], []])
            hvil_subslot.get_global_alarms = Mock(return_value=[])
            hvil_over_current.verify_rail_voltage = Mock(return_value=True)
            hvil_over_current.set_over_current_condition = Mock()
            hvil_subslot.check_per_channel_alarm_details = Mock(return_value=True)
            hvil_subslot.load_disconnect = Mock()
            hvdps_inst.subslots[0].cal_disconnect_force_sense_lines = Mock()

            hvil_over_current.over_current_scenario(hvil_subslot, dutid, force_voltage, force_rail_type='HV', sense_rail_type='RLOAD',alarm_condition_i_clamp_limit= 0.5, iterations=1,expected_alarm='ChannelOverCurrent')
            log_calls = hvil_over_current.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
                self.assertEqual(len(log_calls), 1)

    def test_over_current_alarms_not_received(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil:
            rail = 3
            uhc = 5
            dutid = 15
            force_voltage = 2
            hvil_over_current = OverCurrent()
            hvil_over_current.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11, rc=Mock())

            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_over_current.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
            self.configure_mb_before_test(hvdps_inst)
            hvil_subslot.SetRailsToSafeState = Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits = Mock()
            hvil_subslot.SetDefaultCurrentClampLimit = Mock()
            hvil_subslot.SetThermalCurrentClampLimit = Mock()
            hvil_subslot.ClearDpsAlarms = Mock()
            hvil_subslot.ReadRegister = Mock(return_value=20)
            hvil_subslot.EnableOnlyOneUhc = Mock()
            hvil_subslot.ConfigureUhcRail = Mock()
            hvil_subslot.ClearRailSafeState = Mock()
            hvil_subslot.WriteTQHeaderViaBar2 = Mock()
            hvil_over_current.get_over_current_alarm_filter_count=Mock(return_value=20)
            hvil_over_current.set_over_current_alarm_filter_count=Mock()
            hvil_over_current.set_over_current_condition_using_rail_command= Mock()
            hvil_over_current.Log = Mock()
            hvil_subslot.setup_load_control = Mock()
            hvil_over_current.setup_steady_state = Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(side_effect=[[], []])
            hvil_subslot.get_global_alarms = Mock(return_value=[])
            hvil_over_current.verify_rail_voltage = Mock(return_value=True)
            hvil_over_current.set_over_current_condition = Mock()
            hvil_subslot.check_per_channel_alarm_details = Mock(return_value=False)
            hvil_subslot.load_disconnect = Mock()
            hvdps_inst.subslots[0].cal_disconnect_force_sense_lines = Mock()

            hvil_over_current.over_current_scenario(hvil_subslot, dutid, force_voltage, force_rail_type='HV', sense_rail_type='RLOAD',alarm_condition_i_clamp_limit= 0.5, iterations=1,expected_alarm='ChannelOverCurrent')
            log_calls = hvil_over_current.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
                self.assertEqual(len(log_calls), 1)

    def configure_mb_before_test(self, inst_subslot):
        inst_subslot.subslots[0].SetRailsToSafeState = Mock()
        inst_subslot.subslots[0].ClearDpsAlarms = Mock()
        inst_subslot.subslots[0].EnableAlarms = Mock()
        inst_subslot.subslots[0].EnableOnlyOneUhc = Mock()
        inst_subslot.subslots[0].ConfigureUhcRail = Mock()
        inst_subslot.subslots[0].UnGangAllRails = Mock()

