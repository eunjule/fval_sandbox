################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random
from Common import fval
from unittest.mock import Mock, patch
from Common.instruments.dps import hvdps_registers as hvdpsregs
from Common.instruments.dps import hvdpsSubslot
from Common.instruments.dps import hvilSubslot

from unittest.mock import call

from Dps.Tests.LTC2358 import InterfaceTests

class LTC2358Tests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None
        self.reg_types = hvdpsregs.get_register_types()

    def test_VoltageReadTestRegularModeHv_Pass(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for softspancode in [7, 3]:
            for rail in range(8):
                expected_softspan_codes_and_channels.append((softspancode,(2 * rail) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector= Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedHVRegularModeVoltageTest(read_iterations = 1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, 16)
        for log_info,kwargs in log_calls:
            log_level,log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_VoltageReadTestRegularModeHv_Softspan_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for softspancode in [7, 3]:
            for rail in range(8):
                expected_softspan_codes_and_channels.append((softspancode+1,  (2 * rail) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedHVRegularModeVoltageTest(read_iterations = 1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, 32,'Log call count mismatch ')
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i+1][0]
            softspan_log_level,softspan_log_message = log_calls[i][0]
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be Error')
            self.assertRegex(softspan_log_message,'SpanCodeRead')

    def test_VoltageReadTestRegularModeHv_Channel_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for softspancode in [7, 3]:
            for rail in range(8):
                expected_softspan_codes_and_channels.append((softspancode,(2 * rail+1) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedHVRegularModeVoltageTest(read_iterations = 1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, 32)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i][0]
            softspan_log_level,softspan_log_message = log_calls[i+1][0]
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(channel_log_message,'channel id Read')


    def configure_before_test(self,inst_subslot):
        inst_subslot.clear_all_alarms_before_test = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.WriteBar2RailCommand = Mock()
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.WriteTQHeaderViaBar2 = Mock()

    def test_CurrentReadTestRegularModeHv_Pass(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for rail in range(8):
            for softspancode in [1]:
                expected_softspan_codes_and_channels.append((softspancode,(2 * rail) % 8+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
                    side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        my_hvdps.ResetCurrentSoftSpanCode = Mock()
        LTC2358_object.DirectedHVRegularModeCurrentTest(read_iterations = 1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, 8, 'Log call count mismatch ')
        for log_info,kwargs in log_calls:
            log_level,log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_CurrentReadTestRegularModeHv_Softspan_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for rail in range(8):
            for softspancode in [1]:
                expected_softspan_codes_and_channels.append((softspancode+1, (2 * rail) % 8+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        my_hvdps.ResetCurrentSoftSpanCode = Mock()
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedHVRegularModeCurrentTest(read_iterations = 1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, 16)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i+1][0]
            softspan_log_level,softspan_log_message = log_calls[i][0]
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be Error')
            self.assertRegex(softspan_log_message,'SpanCodeRead')

    def test_CurrentReadTestRegularModeHv_Channel_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for rail in range(8):
            for softspancode in [1]:
                expected_softspan_codes_and_channels.append((softspancode,  (2 * rail+1) % 8+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        my_hvdps.ResetCurrentSoftSpanCode = Mock()
        LTC2358_object.DirectedHVRegularModeCurrentTest(read_iterations = 1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, 16)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i][0]
            softspan_log_level,softspan_log_message = log_calls[i+1][0]
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(channel_log_message,'channel id Read')


    def test_DirectedLvmVoltageReadRegularModeTest_pass(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        expected_read_count = 20
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for rail in range(10):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode,(rail) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
                    side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedLVMRegularModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_DirectedLvmVoltageReadRegularModeTest_SpanCode_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        expected_read_count = 40
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        for rail in range(10):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode+1, (rail) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedLVMRegularModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0, len(log_calls), 2):
            channel_log_level,channel_log_message = log_calls[i+1][0]
            softspan_log_level,softspan_log_message = log_calls[i][0]
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be Error')
            self.assertRegex(softspan_log_message,'SpanCodeRead')



    def test_DirectedLvmVoltageReadRegularModeTest_ChannelId_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        expected_read_count =40
        for rail in range(10):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode, (rail) % 8+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedLVMRegularModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i][0]
            softspan_log_level,softspan_log_message = log_calls[i+1][0]
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(channel_log_message,'Channel id Read')


    def test_DirectedHvVoltageReadTurboModeTest_pass(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        expected_read_count = 16
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        LTC2358_object.Log = Mock()
        for rail in range(8):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode,rail))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.DirectedHVTurboModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_DirectedHvVoltageReadTurboModeTest_Spancode_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        expected_read_count =32
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        for rail in range(8):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode+1, rail))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedHVTurboModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count,expected_read_count)
        for i in range(0, len(log_calls), 2):
            channel_log_level,channel_log_message = log_calls[i+1][0]
            softspan_log_level,softspan_log_message = log_calls[i][0]
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be Error')
            self.assertRegex(softspan_log_message,'SpanCodeRead')

    def test_DirectedHvVoltageReadTurboModeTest_ChannelId_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        expected_read_count = 32
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        for rail in range(8):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode, rail+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedHVTurboModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i][0]
            softspan_log_level,softspan_log_message = log_calls[i+1][0]
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(channel_log_message,'Channel id Read')

    def test_DirectedHvCurrentReadTurboModeTest_pass(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        expected_read_count = 8
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        my_hvdps.ResetCurrentSoftSpanCode =Mock()
        LTC2358_object.Log = Mock()
        for rail in range(8):
            for softspancode in [1]:
                expected_softspan_codes_and_channels.append((softspancode, rail))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.DirectedHVTurboModeCurrentTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_DirectedHvCurrentReadTurboModeTest_Spancode_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        expected_read_count =16
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        for rail in range(8):
            for softspancode in [1]:
                expected_softspan_codes_and_channels.append((softspancode+1, rail))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector  = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        my_hvdps.ResetCurrentSoftSpanCode = Mock()
        LTC2358_object.DirectedHVTurboModeCurrentTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0, len(log_calls), 2):
            channel_log_level,channel_log_message = log_calls[i+1][0]
            softspan_log_level,softspan_log_message = log_calls[i][0]
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be Error')
            self.assertRegex(softspan_log_message,'SpanCodeRead')

    def test_DirectedHvCurrentReadTurboModeTest_ChannelId_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        expected_read_count = 16
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        for rail in range(8):
            for softspancode in [1]:
                expected_softspan_codes_and_channels.append((softspancode, rail+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        my_hvdps.ResetCurrentSoftSpanCode = Mock()
        LTC2358_object.DirectedHVTurboModeCurrentTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i][0]
            softspan_log_level,softspan_log_message = log_calls[i+1][0]
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(channel_log_message,'Channel id Read')


    def test_DirectedLvmVoltageReadTurboModeTest_pass(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        expected_read_count = 20
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        for rail in range(10):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode,(rail) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedLVMTurboModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')


    def test_DirectedLvmVoltageReadTurboModeTest_SpanCode_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        expected_read_count = 40
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        for rail in range(10):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode+1, (rail) % 8))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedLVMTurboModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0, len(log_calls), 2):
            channel_log_level,channel_log_message = log_calls[i+1][0]
            softspan_log_level,softspan_log_message = log_calls[i][0]
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be Error')
            self.assertRegex(softspan_log_message,'SpanCodeRead')



    def test_DirectedLvmVoltageReadTurboModeTest_ChannelId_mismatch(self):
        LTC2358_object = InterfaceTests()
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
        expected_softspan_codes_and_channels = []
        LTC2358_object.enable_only_one_turbo_rail = Mock()
        LTC2358_object.clear_turbo_rail_select = Mock()
        expected_read_count =40
        for rail in range(10):
            for softspancode in [7, 3]:
                expected_softspan_codes_and_channels.append((softspancode, (rail) % 8+1))
        my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector = Mock(
            side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvdps)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedLVMTurboModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(LTC2358_object.Log.call_count, expected_read_count)
        for i in range(0,len(log_calls),2):
            channel_log_level,channel_log_message = log_calls[i][0]
            softspan_log_level,softspan_log_message = log_calls[i+1][0]
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(channel_log_message,'Channel id Read')

    def test_read_raw_spancode_and_channel_id_from_sample_collector(self):
        expected_channel_id = 3
        expected_spancode =4
        my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        my_hvdps.WriteToSampleCollectorIndexRegister = Mock()
        my_hvdps.ReadSampleCollectorCount = Mock(return_value=1)
        my_hvdps.ReadSampleCollectorData = Mock(return_value=0x123)
        channel_id,spancode =my_hvdps.read_raw_spancode_and_channel_id_from_sample_collector(4,group=1)
        self.assertEqual(channel_id,expected_channel_id)
        self.assertEqual(spancode,expected_spancode)

    def test_zero_iterations_error(self):
        LTC2358_object = InterfaceTests()
        with self.assertRaises(fval.LoggedError):
            LTC2358_object.DirectedLVMTurboModeVoltageTest(read_iterations=0)

    def test_DirectedVoltageReadRloadRegularModeTest_pass(self):
        LTC2358_object = InterfaceTests()
        my_hvil = hvilSubslot.HvilSubslot(hvdps=Mock(), slot=0, subslot=0)
        expected_softspan_codes_and_channels = []
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        expected_read_count = 56
        for channel in range(8):
            for softspancode in range(1, 8):
                expected_softspan_codes_and_channels.append(Mock(SoftSpanCode=softspancode, ChannelId=(channel) % 8 ) )
        my_hvil.WriteRegister = Mock()
        my_hvil.ReadRegister = Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvil)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedRloadRegularModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(my_hvil.ReadRegister.call_count, expected_read_count)
        self.assertEqual(LTC2358_object.Log.call_count, 56)
        for i in range(0, len(log_calls), 2):
            log_level, log_message = log_calls[i][0]
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')


    def test_DirectedVoltageReadRloadRegularModeSpanCodeMismatchTest(self):
        LTC2358_object = InterfaceTests()
        my_hvil = hvilSubslot.HvilSubslot(hvdps=Mock(), slot=0, subslot=0)
        expected_softspan_codes_and_channels = []
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        expected_read_count = 56
        for channel in range(8):
            for softspancode in range(1, 8):
                expected_softspan_codes_and_channels.append(Mock(SoftSpanCode=softspancode + 1, ChannelId=(channel) % 8 ) )
        my_hvil.WriteRegister = Mock()
        my_hvil.ReadRegister = Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvil)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedRloadRegularModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(my_hvil.ReadRegister.call_count, expected_read_count)
        self.assertEqual(LTC2358_object.Log.call_count, 112)
        for i in range(0, len(log_calls), 2):
            channel_log_level, channel_log_message = log_calls[i+1][0]
            softspan_log_level, softspan_log_message = log_calls[i][0]
            self.assertEqual(softspan_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(channel_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(softspan_log_message, 'SpanCodeRead')

    def test_DirectedVoltageReadRloadRegularModeChannelIdMismatchTest(self):
        LTC2358_object = InterfaceTests()
        my_hvil = hvilSubslot.HvilSubslot(hvdps=Mock(), slot=0, subslot=0)
        expected_softspan_codes_and_channels = []
        my_hvil.ConfigureUhcRail = Mock()
        LTC2358_object.env = Mock()
        LTC2358_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        expected_read_count = 56
        for channel in range(8):
            for softspancode in range(1, 8):
                expected_softspan_codes_and_channels.append(Mock(SoftSpanCode=softspancode, ChannelId=(channel) % 8 +1) )
        my_hvil.WriteRegister = Mock()
        my_hvil.ReadRegister = Mock(side_effect=expected_softspan_codes_and_channels)
        self.configure_before_test(my_hvil)
        LTC2358_object.Log = Mock()
        LTC2358_object.DirectedRloadRegularModeVoltageTest(read_iterations=1)
        log_calls = LTC2358_object.Log.call_args_list
        self.assertEqual(my_hvil.ReadRegister.call_count, expected_read_count)
        self.assertEqual(LTC2358_object.Log.call_count, 112)
        for i in range(0, len(log_calls), 2):
            channel_log_level, channel_log_message = log_calls[i][0]
            softspan_log_level, softspan_log_message = log_calls[i+1][0]
            self.assertEqual(softspan_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(channel_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertRegex(channel_log_message, 'Channel id Read')



if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)