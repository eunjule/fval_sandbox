################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    from Tools import projectpaths

from Common import fval
from Dps.Tests.BabySteps import SimpleBabySteps


class BabyStepTests(unittest.TestCase):

    def test_MiniTest(self):
        conditions = SimpleBabySteps()
        conditions.env = Mock()
        board = Mock(ReadRegister = Mock(return_value = Mock(value = 1234)))
        conditions.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([board]))
        conditions.MiniTest()