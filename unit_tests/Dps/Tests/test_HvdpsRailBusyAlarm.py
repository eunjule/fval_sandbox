################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock,call
from unittest.mock import patch
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Dps.Tests.HvdpsRailBusy import RailBusyAlarm

class HvdpsRailBusyAlarm(unittest.TestCase):

    def test_check_hv_rail_busy_scenario_parameter(self):
        hvdpsrailalarm = RailBusyAlarm()
        test_rail_type = 'HV'
        hvdpsrailalarm.env = Mock()
        hvdpsrailalarm.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpsrailalarm.rail_busy_scenario = Mock()
        hvdpsrailalarm.DirectedHvRailBusyAlarmTest()
        validate_parameters_called = hvdpsrailalarm.rail_busy_scenario.call_args
        args, kwargs = validate_parameters_called
        board, rail_type  = args
        self.assertEqual(rail_type, test_rail_type)

    def test_check_lvm_rail_busy_scenario_parameter(self):
        hvdpsrailalarm = RailBusyAlarm()
        test_rail_type = 'LVM'
        hvdpsrailalarm.env = Mock()
        hvdpsrailalarm.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdpsrailalarm.rail_busy_scenario = Mock()
        hvdpsrailalarm.DirectedLvmRailBusyAlarmTest()
        validate_parameters_called = hvdpsrailalarm.rail_busy_scenario.call_args
        args, kwargs = validate_parameters_called
        board, rail_type  = args
        self.assertEqual(rail_type, test_rail_type)

    def test_rail_busy_scenario_unexpected_global_alarms_received(self):
        hvdpsrailalarm = RailBusyAlarm()
        hvdpsrailalarm.env = Mock()
        rail =4
        uhc= 6
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        self.configure_before_test(hvdps_subslot)
        hvdpsrailalarm.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.get_global_alarms = Mock(return_value = ['HcLcRailAlarm'])
        hvdpsrailalarm.Log = Mock()
        hvdpsrailalarm.rail_busy_scenario(hvdps_subslot,rail_type='HV',test_iterations=1)
        log_calls = hvdpsrailalarm.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(len(log_calls), 2)

    def test_unexpected_rails_busy_scenario_fail(self):
        hvdpsrailalarm = RailBusyAlarm()
        hvdpsrailalarm.env = Mock()
        rail = 4
        uhc = 6
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        self.configure_before_test(hvdps_subslot)
        hvdpsrailalarm.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        hvdpsrailalarm.Log = Mock()
        hvdpsrailalarm.verify_fold_status =Mock()
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdps_subslot.WriteTQFooterViaBar2 = Mock()
        hvdpsrailalarm.verify_rails_busy_folded_register = Mock()
        hvdpsrailalarm.verify_rail_busy_alarm = Mock(side_effect=hvdpsrailalarm.RailBusyException)
        hvdpsrailalarm.rail_busy_scenario(hvdps_subslot, rail_type='HV', test_iterations=1)
        log_calls = hvdpsrailalarm.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(len(log_calls), 1)

    def test_verify_rail_busy_alarm_call_args(self):
        with patch ('Common.instruments.dps.dpsSubslot.DpsSubslot.check_for_single_hclc_rail_alarm') as mock_check_per_rail_alarm:
            hvdpsrailalarm = RailBusyAlarm()
            hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
            hvdpsrailalarm.verify_rail_busy_alarm(hvdps_subslot,rail_type='HV',rail=2)
            self.assertTrue(mock_check_per_rail_alarm.called)

    def test_verify_rail_busy_alarm_unexpected_alarm_received(self):
        hvdpsrailalarm = RailBusyAlarm()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.get_global_alarms=Mock(return_value=['HclcRailAlarms'])
        hvdps_subslot.check_for_single_hclc_rail_alarm= Mock(return_value=False)
        with self.assertRaises(hvdpsrailalarm.RailBusyException):
            hvdpsrailalarm.verify_rail_busy_alarm(hvdps_subslot,rail_type='HV',rail=2)

    def test_verify_fold_status_fail(self):
        hvdpsrailalarm = RailBusyAlarm()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.WriteRegister = Mock()
        hvdps_subslot.ReadRegister=Mock(return_value=Mock(value= 0x3F))
        hvdpsrailalarm.Log=Mock()
        with self.assertRaises(hvdpsrailalarm.RailBusyException):
            hvdpsrailalarm.verify_fold_status(hvdps_subslot,rail_type='HV')
        log_calls = hvdpsrailalarm.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')



    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()
        inst_subslot.ResetVoltageSoftSpanCode = Mock()
        inst_subslot.ResetCurrentSoftSpanCode = Mock()

