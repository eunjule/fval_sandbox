################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from collections import OrderedDict
import unittest
import random
from unittest.mock import patch

from unittest.mock import Mock
from Common import fval
from Common.instruments.dps import hvil_registers as hvil_regs
from Common.instruments.dps.hvilSubslot import HvilSubslot
from Dps.Tests.MAX6627_Hvil import TemperatureInterface,TemperatureAlarms

class MAX6627Tests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None
        self.reg_types = hvil_regs.get_register_types()

    def GenerateRandomTemperature(self):
        celcius10_in_hex = 0xA0
        celcius40_in_hex = 0x280
        random_temperature = random.randrange(celcius10_in_hex+1, celcius40_in_hex)
        return random_temperature

    def GenerateRandomTemperatureExcludingRangeOfTemperatures(self):
        celcius10_in_hex = 0xA0
        celcius40_in_hex = 0x280
        random_temperature_in_range = list(range(celcius10_in_hex+1, celcius40_in_hex+1))
        random_temperature = list(range(0, 0xFFF))
        excluded_temperature_range = [x for x in random_temperature  if x not in random_temperature_in_range]
        temperature = random.choice(excluded_temperature_range)
        return temperature

    def test_RandomMAX6627TemperatureReadUsingHilLevelOneTestInRange(self):
        MAX6627_object = TemperatureInterface()
        my_hvil = HvilSubslot(hvdps = Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.Log = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        random_temperature = self.GenerateRandomTemperature()
        my_hvil.ReadRegister = Mock(return_value=Mock(TemperatureValue=random_temperature))
        MAX6627_object.RandomMAX6627TemperatureReadUsingHilLevelOneTest()
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message,'Temperature of MAX6627 device in a reasonable range',
                             'Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')


    def test_RandomMAX6627TemperatureReadUsingHilLevelOneTestNotInRange(self):
        MAX6627_object = TemperatureInterface()
        my_hvil = HvilSubslot(hvdps = Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.Log = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        random_temperature = self.GenerateRandomTemperatureExcludingRangeOfTemperatures()
        my_hvil.ReadRegister = Mock(return_value=Mock(TemperatureValue=random_temperature))
        MAX6627_object.RandomMAX6627TemperatureReadUsingHilLevelOneTest()
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message,'Temperature of MAX6627 device not in a reasonable range',
                             'Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')


    def test_RandomDeviceForMAX6627TemperatureReadUsingHilLevelTwoTestInRange(self):
            MAX6627_object = TemperatureInterface()
            temperature_in_celcius = 15
            my_hvil = HvilSubslot(hvdps = Mock(), slot=0, subslot=1)
            MAX6627_object.env = Mock()
            MAX6627_object.Log = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
            my_hvil.MAX6627TemperatureRead = Mock(return_value=temperature_in_celcius)
            MAX6627_object.RandomDeviceForMAX6627TemperatureReadUsingHilLevelTwoTest()
            self.assertEqual(my_hvil.MAX6627TemperatureRead.call_count, 60)
            log_calls = MAX6627_object.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertRegex(log_message, 'Temperature of MAX6627 device in a reasonable range',
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_RandomDeviceForMAX6627TemperatureReadUsingHilLevelTwoTestOutOfRange(self):
            MAX6627_object = TemperatureInterface()
            temperature_in_celcius = 45
            my_hvil = HvilSubslot(hvdps = Mock(), slot=0, subslot=1)
            MAX6627_object.env = Mock()
            MAX6627_object.Log = Mock()
            MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
            my_hvil.MAX6627TemperatureRead = Mock(return_value=temperature_in_celcius)
            MAX6627_object.RandomDeviceForMAX6627TemperatureReadUsingHilLevelTwoTest()
            self.assertEqual(my_hvil.MAX6627TemperatureRead.call_count, 60)
            log_calls = MAX6627_object.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertRegex(log_message, 'Temperature of MAX6627 device not in a reasonable range',
                                 'Log not displaying correct information')
                self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')

    def test_DirectedTemperatureStabilityTest(self):
        temperature_15_celcius = 15
        temperature_20_celcius = 20
        temperature_25_celcius = 25
        temperature_18_celcius = 18
        MAX6627_object = TemperatureInterface()
        my_hvil = HvilSubslot(hvdps = Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        MAX6627_object.Log = Mock()
        my_hvil.MAX6627TemperatureRead = Mock(
            side_effect=[temperature_15_celcius, temperature_20_celcius, temperature_25_celcius,
                         temperature_18_celcius] * 6000)
        MAX6627_object.DirectedTemperatureStabilityTest()
        self.assertEqual(my_hvil.MAX6627TemperatureRead.call_count, 6000)
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message, 'Temperature of MAX6627 device is stable',
                             'Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')

    def test_DirectedTemperatureInStabilityTest(self):
        temperature_15_celcius = 15
        temperature_20_celcius = 20
        temperature_25_celcius = 25
        temperature_35_celcius = 35
        MAX6627_object = TemperatureInterface()
        my_hvil = HvilSubslot(hvdps = Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        MAX6627_object.Log = Mock()
        my_hvil.MAX6627TemperatureRead = Mock(
            side_effect=[temperature_15_celcius, temperature_20_celcius, temperature_25_celcius,
                         temperature_35_celcius] * 6000)
        MAX6627_object.DirectedTemperatureStabilityTest()
        self.assertEqual(my_hvil.MAX6627TemperatureRead.call_count, 6000)
        log_calls = MAX6627_object.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertRegex(log_message, 'Temperature of MAX6627 device is unstable',
                             'Log not displaying correct information')
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be error')

# MAX6627 TEMPERATURE ALARMS


    def test_verify_MAX6627Alarms_pass(self):
        expected_alarms = 'MAX6627Device0HighAlarm'
        get_non_zero_alarms = ['MAX6627Device0HighAlarm']
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        my_hvil.get_global_alarms = Mock(return_value=['BrdTempAlarm'])
        my_hvil.get_non_zero_fields = Mock(return_value=get_non_zero_alarms)
        my_hvil.ReadRegister = Mock()
        MAX6627_object.verify_MAX6627Alarms(my_hvil, expected_alarms)
        self.assertEqual(my_hvil.ReadRegister.call_count, 1)


    def test_verify_MAX6627Alarms_fail(self):
        expected_alarms = 'MAX6627Device0HighAlarm'
        get_non_zero_alarms = ['MAX6627Device1HighAlarm']
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        my_hvil.get_global_alarms = Mock(return_value=['BrdTempAlarm'])
        my_hvil.ReadRegister = Mock()
        MAX6627_object.Log = Mock()
        my_hvil.get_non_zero_fields = Mock(return_value=get_non_zero_alarms)
        with self.assertRaises(TemperatureAlarms.UnexpectedAlarm):
            MAX6627_object.verify_MAX6627Alarms(my_hvil, expected_alarms)


    def test_verify_MAX6627Alarms_global_alarms_received(self):
        expected_alarms = ['MAX6627Device0HighAlarm']
        MAX6627_object = TemperatureAlarms()
        MAX6627_object.Log = Mock(side_effect=self.mock_log)
        # fval.core._currentTest = Mock()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        my_hvil.get_global_alarms = Mock(return_value=['TrigExecAlarm'])
        with self.assertRaises(fval.LoggedError):
            MAX6627_object.verify_MAX6627Alarms(my_hvil, expected_alarms)


    def mock_log(self, level, message):
        if level.lower() == 'error':
            raise fval.LoggedError()
        
    def test_expected_alarms_list(self):
        random_numbers_list = [5,0,3]
        expected_list = ['MAX6627Device5HighAlarm', 'MAX6627Device0HighAlarm', 'MAX6627Device3HighAlarm']
        MAX6627_object = TemperatureAlarms()
        result = MAX6627_object.create_expected_alarms_list(random_numbers_list, AlarmType='High')
        self.assertEqual(result,expected_list)


    def test_RandomHighAlarms_global_alarms_received(self):
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        self.configure_before_test(my_hvil)
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        my_hvil.SetTemperatureLimitForSingleMAX6627 = Mock()
        my_hvil.get_global_alarms = Mock(return_value=['MAX6627Device3HighAlarm'])
        MAX6627_object.Log = Mock()
        MAX6627_object.RandomHighAlarmTest()
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(MAX6627_object.Log.call_count, 51)


    def test_RandomHighAlarmTest_pass(self):
        random_list = [3]
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        self.configure_before_test(my_hvil)
        MAX6627_object.create_random_device_list = Mock(return_value=random_list)
        my_hvil.SetTemperatureLimitForSingleMAX6627 = Mock()
        MAX6627_object.Log = Mock()
        my_hvil.get_global_alarms = Mock(return_value=[])
        MAX6627_object.verify_MAX6627Alarms = Mock(return_value=True)
        MAX6627_object.create_expected_alarms_list = Mock(return_value=('MAX6627Device3LowAlarm'))
        MAX6627_object.RandomHighAlarmTest()
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(iteration_log_message, 'High Temperature Alarm generated on all',
                             'Log not displaying correct information')
            self.assertEqual(MAX6627_object.Log.call_count, 1)


    def test_RandomHighAlarmTest_fail(self):
        random_list = [3]
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        self.configure_before_test(my_hvil)
        MAX6627_object.create_random_device_list = Mock(return_value=random_list)
        my_hvil.SetTemperatureLimitForSingleMAX6627 = Mock()
        MAX6627_object.Log = Mock()
        my_hvil.get_global_alarms = Mock(return_value=[])
        MAX6627_object.create_expected_alarms_list = Mock(return_value=(['MAX6627Device3HighAlarm']))
        MAX6627_object.verify_MAX6627Alarms = Mock(side_effect=TemperatureAlarms.UnexpectedAlarm)
        MAX6627_object.RandomHighAlarmTest()
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertRegex(iteration_log_message, 'High Temperature Alarm not generated on MAX6627 devices',
                             'Log not displaying correct information')
            self.assertEqual(MAX6627_object.Log.call_count, 1)

    def test_RandomLowAlarms_global_alarms_received(self):
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        self.configure_before_test(my_hvil)
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        my_hvil.SetTemperatureLimitForSingleMAX6627 = Mock()
        my_hvil.get_global_alarms = Mock(return_value=['MAX6627Device3HighAlarm'])
        MAX6627_object.Log = Mock()
        MAX6627_object.RandomLowAlarmTest()
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(MAX6627_object.Log.call_count, 51)


    def test_RandomLowAlarmTest_pass(self):
        random_list = [3]
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        self.configure_before_test(my_hvil)
        MAX6627_object.create_random_device_list = Mock(return_value=random_list)
        my_hvil.SetTemperatureLimitForSingleMAX6627 = Mock()
        MAX6627_object.Log = Mock()
        my_hvil.get_global_alarms = Mock(return_value=[])
        MAX6627_object.verify_MAX6627Alarms = Mock(return_value=True)
        MAX6627_object.create_expected_alarms_list = Mock(return_value=('MAX6627Device3LowAlarm'))
        MAX6627_object.RandomLowAlarmTest()
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(iteration_log_message, 'Low Temperature Alarm generated on all','Log not displaying correct information')
            self.assertEqual(MAX6627_object.Log.call_count, 1)



    def test_RandomLowAlarmTest_fail(self):
        random_list = [3]
        MAX6627_object = TemperatureAlarms()
        my_hvil = HvilSubslot(hvdps=Mock(), slot=0, subslot=1)
        MAX6627_object.env = Mock()
        MAX6627_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvil])
        self.configure_before_test(my_hvil)
        MAX6627_object.create_random_device_list = Mock(return_value=random_list)
        my_hvil.SetTemperatureLimitForSingleMAX6627 = Mock()
        MAX6627_object.Log = Mock()
        my_hvil.get_global_alarms = Mock(return_value=[])
        MAX6627_object.verify_MAX6627Alarms = Mock(side_effect=TemperatureAlarms.UnexpectedAlarm)
        MAX6627_object.create_expected_alarms_list = Mock(return_value=('MAX6627Device3LowAlarm'))
        MAX6627_object.RandomLowAlarmTest()
        log_calls = MAX6627_object.Log.call_args_list
        for i in range(0, len(log_calls), 1):
            iteration_log_level, iteration_log_message = log_calls[i][0]
            self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertRegex(iteration_log_message,'Low Temperature Alarm not generated on MAX6627 devices','Log not displaying correct information')
            self.assertEqual(MAX6627_object.Log.call_count, 1)


    def configure_before_test(self, inst_subslot):
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.ClearMAX6627TemperatureAlarmDetails = Mock()
        inst_subslot.ResetDefaultTemperatureLimitForMAX6627 = Mock()

    def test_create_random_device_list(self):
        expected_length_list = [0,1,2,3,4,5,6]
        list_length = []
        MAX6627_object = TemperatureAlarms()
        for i in range(100):
            result1 = len(MAX6627_object.create_random_device_list())
            list_length.append(result1)
            list(set(list_length))
            self.assertIn(list_length[i],expected_length_list)
        self.assertEqual(len(set(list_length)),len(expected_length_list))
        self.assertEqual(list(set(list_length)),expected_length_list)

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)