################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegisters
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers
# -------------------------------------------------------------------------------
#  Created by: Sungjin Ho
#        Date: 7/7/16
#       Group: HDMT FPGA Validation
###############################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.SampleCollector import Conditions


class SampleCollectorTests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('VlcSampleCollectorVoltageBroken')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()

