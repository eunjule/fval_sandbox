################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock
from unittest.mock import patch
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Dps.Tests.DpsCurrentClamps import Clamps,RailScenario,FreeDriveClampSetting


class HvdpsCurrentClampsTestClass(unittest.TestCase):


    def test_DirectedHvNoClampAlarmTest_trigger_queue_string_match(self):
        expected_trigger_queue_string = b'@\x00\x10\xc3@\x00\x10\xc4\x01\x00\x16\xd1\xf8*\x16\xa2\xff\xff\x06\xc6\x01\x00\x16\xa8\x07\x00\x16\x99\x01\x00\x16\xd3\x8b\x0c\x16\xc5\x00\x00\x16\x9a\x01{\x16\xac\xfe\x84\x16\xab\x01{\x16\x9e\xf8\x9d\x16\x9d\x00\x00\x16\xa3\x01\x00\x16\xd3\x01\x00\x16\xcf\x01\x00\x16\xd3\x07\x00\x16\xcd\x07\x03\x16\x9c\xd3\xd4\x16\x9b\x01\x00\x16\xa2\x8b\x0c\x16\xc5\x00!\x16\x97P\x00\x16\xbb\x01\x00\x16\x9a\x00\x00\x16\xb9 \x0e\x16\xc5\x00\x01\x06\x8b\xda\xb6\x16\xa6p\x17\x16\xa2\x00\x00\x16\xa7\x00\x00\x06\x8b\xbc\x02\x16\xa2@\x00\x10\xb2\x00\x00\x00\xb1'
        hvdps_current_clamps = Clamps()
        rail_type = 'HV'
        force_voltage = 15.0
        dutid = 7
        rail = 6
        uhc =3
        hvdps_current_clamps.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value=0x0)
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        trigger_queue_content = hvdps_current_clamps.generate_trigger_queue_without_clamp_alarm(hvdps_subslot,rail_type,force_voltage, dutid, rail, uhc)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_string,
                         'expected trigger queue differed from expected')


    def test_DirectedHvClampAlarmTest_trigger_queue_string_match(self):
        expected_trigger_queue_string = b'@\x00\x10\xc3@\x00\x10\xc4\x07\x00\x16\x99\xfd\x89\x16\x9d\x01\x00\x16\xa8\xbc\x02\x16\xa2@\x00\x10\xb2\x00\x00\x00\xb1'
        hvdps_current_clamps = Clamps()
        rail_type = 'HV'
        dutid = 7
        rail = 6
        uhc =3
        hvdps_current_clamps.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value=0x0)
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        trigger_queue_content = hvdps_current_clamps.generate_trigger_queue_with_clamp_alarm(hvdps_subslot,rail_type,dutid, rail, uhc,negative=False)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_string,
                         'expected trigger queue differed from expected')


    def test_hv_clamp_alarm_scenario_parameters(self):
        hvdps_current_clamps = Clamps()
        rail_type = 'HV'
        hvdps_current_clamps.env = Mock()
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdps_current_clamps.clamp_alarm_scenario= Mock()
        hvdps_current_clamps.DirectedHvHighClampTest()
        validate_clamp_alarm_scenario = hvdps_current_clamps.clamp_alarm_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_negative_voltage, False)

    def test_hc_clamp_alarm_scenario_parameters(self):
        hvdps_current_clamps = Clamps()
        rail_type = 'HC'
        hvdps_current_clamps.env = Mock()
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdps_current_clamps.clamp_alarm_scenario= Mock()
        hvdps_current_clamps.DirectedHcHighClampTest()
        validate_clamp_alarm_scenario = hvdps_current_clamps.clamp_alarm_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_negative_voltage, False)

    def test_lc_high_clamp_alarm_scenario_parameters(self):
        hvdps_current_clamps = Clamps()
        rail_type = 'LC'
        hvdps_current_clamps.env = Mock()
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdps_current_clamps.clamp_alarm_scenario= Mock()
        hvdps_current_clamps.DirectedLcHighClampTest()
        validate_clamp_alarm_scenario = hvdps_current_clamps.clamp_alarm_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_negative_voltage, False)

    def test_lc_low_clamp_alarm_scenario_parameters(self):
        hvdps_current_clamps = Clamps()
        rail_type = 'LC'
        hvdps_current_clamps.env = Mock()
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdps_current_clamps.clamp_alarm_scenario= Mock()
        hvdps_current_clamps.DirectedLcLowClampTest()
        validate_clamp_alarm_scenario = hvdps_current_clamps.clamp_alarm_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_negative_voltage, True)

    def test_vlc_high_clamp_alarm_scenario_parameters(self):
        hvdps_current_clamps = Clamps()
        rail_type = 'VLC'
        hvdps_current_clamps.env = Mock()
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdps_current_clamps.clamp_alarm_scenario= Mock()
        hvdps_current_clamps.DirectedVlcHighClampTest()
        validate_clamp_alarm_scenario = hvdps_current_clamps.clamp_alarm_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_negative_voltage, False)

    def test_vlc_low_clamp_alarm_scenario_parameters(self):
        hvdps_current_clamps = Clamps()
        rail_type = 'VLC'
        hvdps_current_clamps.env = Mock()
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvdps_current_clamps.clamp_alarm_scenario= Mock()
        hvdps_current_clamps.DirectedVlcLowClampTest()
        validate_clamp_alarm_scenario = hvdps_current_clamps.clamp_alarm_scenario.call_args
        self.assertNotEqual(len(validate_clamp_alarm_scenario), 0)
        args, kwargs = validate_clamp_alarm_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_negative_voltage, True)

    def test_clamp_alarm_scenario_global_alarms_found(self):
        rail = 3
        uhc = 5
        rail_type = 'HV'
        alarm_received = 'HclcRailAlarms'
        hvdps_current_clamps = Clamps()
        hvdps_current_clamps.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        rail_scenario = RailScenario(hvdps_subslot,rail_type,is_negative=False,dutid=15)
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.cal_load_connect = Mock()
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.cal_disconnect_force_sense_lines =Mock()
        hvdps_current_clamps.setup_steady_state=Mock()
        hvdps_current_clamps.Log = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[alarm_received])
        hvdps_current_clamps.clamp_alarm_scenario(board=hvdps_subslot, rail_type=rail_type,negative=False)
        log_calls = hvdps_current_clamps.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')
            self.assertEqual(len(log_calls), 2)

    def test_clamp_alarm_scenario_clamp_alarm_found(self):
        rail = 3
        uhc = 5
        rail_type = 'HV'
        hvdps_current_clamps = Clamps()
        hvdps_current_clamps.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        rail_scenario = RailScenario(hvdps_subslot,rail_type,is_negative=False,dutid=15)
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.cal_load_connect = Mock()
        hvdps_subslot.cal_connect_force_sense_lines = Mock()
        hvdps_subslot.cal_disconnect_force_sense_lines =Mock()
        hvdps_current_clamps.setup_steady_state=Mock()
        hvdps_current_clamps.Log = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=[])
        hvdps_current_clamps.verify_rail_voltage_and_current = Mock(return_value=True)
        hvdps_current_clamps.setup_current_clamp_condition = Mock()
        hvdps_subslot.check_for_single_hclc_rail_alarm = Mock(return_value=True)
        hvdps_current_clamps.clamp_alarm_scenario(board=hvdps_subslot, rail_type=rail_type,negative=False)
        hvdps_current_clamps.DirectedHvHighClampTest()
        log_calls = hvdps_current_clamps.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(len(log_calls), 2)

    @unittest.skip('This test currently skipped while VLC rails raw conversion is handled')
    def test_generate_vlc_trigger_queue_without_clamp_alarm(self):
        rail_type = 'VLC'
        force_voltage = 1
        dutid = 7
        rail = 6
        uhc = 3
        expected_trigger_queue_string = b'@\x00\x00\xc3@\x00\x00\xc4\x00\x00\x06\xa3\x00\x00\x06\x9ad\x00\x06\xbb\x05\x00\x06\x99\x0b\xc0\x06\x9e\x0b@\x06\x9d\x01\x00\x06\xa8\n\xc0\x06\xac\x07x\x06\xab\xbbl\x06\x97\x01\x00\x06\x9a\x00\xb2\x01\xed\xfa\x00\x06\xbb.\x00\x02\xed\xbc\x02\x06\xa2@\x00\x00\xb2\x00\x00\x00\xb1'
        dps_current_clamps = Clamps()
        dps_current_clamps.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0,rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value=0x0)
        dps_current_clamps.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hddps_subslot])
        trigger_queue_content = dps_current_clamps.generate_vlc_trigger_queue_without_clamp_alarm(hddps_subslot,
                                                                                                rail_type,
                                                                                                force_voltage, dutid,
                                                                                                rail, uhc)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_string,
                         'expected trigger queue differed from expected')


    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()
        inst_subslot.ResetVoltageSoftSpanCode = Mock()
        inst_subslot.ResetCurrentSoftSpanCode = Mock()


    def test_is_close_pass(self):
        expected_value = 0.5
        actual_value = 0.7
        expected_deviation = 0.25
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        result = hddps_subslot.is_in_maximum_allowed_deviation(expected_value, actual_value, expected_deviation)
        self.assertEqual(result, True)

    def test_is_close_fail(self):
        expected_value = 0.5
        actual_value = 0.7
        expected_deviation = 0.15
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        result = hddps_subslot.is_in_maximum_allowed_deviation(expected_value, actual_value, expected_deviation)
        self.assertEqual(result, False)

    def test_DirectedClampEnableDisableTest_pass(self):
        rail = 3
        uhc = 5
        rail_type = 'HV'
        expected_clamp_enable_status_after_fd_start = (0x1 << rail) ^ 0xFF
        free_drive_setting = FreeDriveClampSetting()
        free_drive_setting.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        free_drive_setting.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        free_drive_setting.Log=Mock()
        free_drive_setting.get_clamp_enable_status = Mock(side_effect=[0xFF,expected_clamp_enable_status_after_fd_start,0xFF])
        free_drive_setting.DirectedClampEnableDisableTest()
        log_calls = free_drive_setting.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertEqual(len(log_calls), 1)


    def test_clamp_enable_status_incorrect_before_free_drive(self):
        rail = 3
        uhc = 5
        expected_clamp_enable_status_after_fd_start = (0x1 << rail) ^ 0xFF
        free_drive_setting = FreeDriveClampSetting()
        free_drive_setting.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        free_drive_setting.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        free_drive_setting.Log=Mock()
        free_drive_setting.get_clamp_enable_status = Mock(side_effect=[0x0,expected_clamp_enable_status_after_fd_start,0xFF])
        free_drive_setting.DirectedClampEnableDisableTest()
        log_calls = free_drive_setting.Log.call_args_list
        for i in range(0, len(log_calls),3):
            clamp_enable_log_level, clamp_enable_log_message = log_calls[i][0]
            test_summary_log_level,test_summary_log_message = log_calls[i+1][0]
            self.assertEqual(clamp_enable_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(test_summary_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertIn('AD5560 Clamp Enable status before Free drive start is not as expected', clamp_enable_log_message)
            self.assertIn('Expected Clamp Enable status seen', test_summary_log_message)

    def test_clamp_enable_status_incorrect_after_start_free_drive(self):
        rail = 3
        uhc = 5
        expected_clamp_enable_status_after_fd_start = (0x1 << rail) ^ 0xFF
        free_drive_setting = FreeDriveClampSetting()
        free_drive_setting.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        free_drive_setting.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        free_drive_setting.Log=Mock()
        free_drive_setting.get_clamp_enable_status = Mock(side_effect=[0xFF,expected_clamp_enable_status_after_fd_start+1,0xFF])
        free_drive_setting.DirectedClampEnableDisableTest()
        log_calls = free_drive_setting.Log.call_args_list
        for i in range(0, len(log_calls),3):
            clamp_enable_log_level, clamp_enable_log_message = log_calls[i][0]
            test_summary_log_level,test_summary_log_message = log_calls[i+1][0]
            self.assertEqual(clamp_enable_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(test_summary_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertIn('AD5560 Clamp Enable status after Free drive start is not as expected', clamp_enable_log_message)
            self.assertIn('Expected Clamp Enable status seen', test_summary_log_message)

    def test_clamp_enable_status_incorrect_after_end_free_drive(self):
        rail = 3
        uhc = 5
        expected_clamp_enable_status_after_fd_start = (0x1 << rail) ^ 0xFF
        free_drive_setting = FreeDriveClampSetting()
        free_drive_setting.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        free_drive_setting.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hvdps_subslot)
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        free_drive_setting.Log=Mock()
        free_drive_setting.get_clamp_enable_status = Mock(side_effect=[0xFF,expected_clamp_enable_status_after_fd_start,0x0])
        free_drive_setting.DirectedClampEnableDisableTest()
        log_calls = free_drive_setting.Log.call_args_list
        for i in range(0, len(log_calls),3):
            clamp_enable_log_level, clamp_enable_log_message = log_calls[i][0]
            test_summary_log_level,test_summary_log_message = log_calls[i+1][0]
            self.assertEqual(clamp_enable_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertEqual(test_summary_log_level.upper(), 'ERROR', 'Log level should be error')
            self.assertIn('AD5560 Clamp Enable status after Free drive end is not as expected', clamp_enable_log_message)
            self.assertIn('Expected Clamp Enable status seen', test_summary_log_message)
