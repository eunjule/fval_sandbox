################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_encodings.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the floating/fixed point encoding/decoding functions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/17/15
#       Group: HDMT FPGA Validation
################################################################################

from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    from Tools import projectpaths
    
from Common import fval
from Dps.Tests.Assertions import Conditions

class AssertionsTests(unittest.TestCase):

    def test_DirectedTqnotifyPerUhcTest(self):
        '''just enough code to show imports are working'''
        conditions = Conditions()
        conditions.env = Mock()
        conditions.Log = Mock()
        board = Mock(getTriggerQueueHeaderString = Mock(return_value = ''),
                     getTriggerQueueFooterString = Mock(return_value = ''),
                     Read = lambda x:Mock(Pack = lambda :1234))
        conditions.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([board]))
        conditions.DirectedTqnotifyPerUhcTest()
    
    def test_DirectedVlcPcieVsimTest(self):
        '''just enough code to show imports are working'''
        conditions = Conditions()
        conditions.env = Mock()
        conditions.Log = Mock()
        conditions.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([Mock(Read = lambda x:Mock(Pack = lambda :123))]))
        conditions.DirectedVlcPcieVsimTest()


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
