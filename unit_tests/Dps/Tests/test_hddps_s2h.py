################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
import unittest
from unittest.mock import patch
from unittest.mock import Mock

from Common import fval
from Common import hilmon as hil
from Common.instruments.dps.hddps import Hddps
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Dps.Tests.S2H import DpsS2H


class HddpsS2HTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = Mock()

    def test_to_check_if_inheritance_works(self):
        with patch('Common.instruments.dps.hddpsSubslot.hil') as mock_subslot_hil,\
            patch('Common.instruments.dps.dpsSubslot.hil') as mock_dpssubslot_hil:
            buffer_size = 29999
            addr = 1020
            num_of_iterations = 5
            hddps_s2h = DpsS2H()
            my_hddps = Hddps(slot=1,rc=self.mock_rc)
            hddps_s2h.env = Mock()
            hddps_s2h.WaitTime = Mock()
            hddps_s2h.env.duts = [my_hddps.subslots[0],my_hddps.subslots[1]]
            mock_subslot_hil.hddpsStreamingBuffer = Mock(return_value=[buffer_size,addr])
            mock_dpssubslot_hil.dpsBarRead = Mock(side_effect=[500,1,1,750, 1000, 1500, 249, 499,779,999,1500])
            my_hddps.subslots[0].IsS2HConfigurationDone = Mock(return_value=True)
            my_hddps.subslots[1].IsS2HConfigurationDone = Mock(return_value=True)
            with self.assertRaises(fval.LoggedError):
                hddps_s2h.DirectedS2HPacketCountIncrementTest(num_of_iterations)

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)