################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random
from Common import fval
from unittest.mock import Mock, patch
from Common.instruments.dps import hvdps_registers as hvdpsregs
from Common.instruments.dps import hvdpsSubslot


from Dps.Tests.LTM4653 import InterfaceTests

class LTM4653Tests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None
        self.reg_types = hvdpsregs.get_register_types()


    def test_DirectedHVVoltageTest_pass(self):
        with patch.object(random, 'randint')as mock_Random:
            expected_voltage = 15
            hv_ad5560_tracking_regulator_offset = 4.6
            actual_voltage = 15 + hv_ad5560_tracking_regulator_offset
            successful_test_iterations = 1
            total_test_iterations = 1
            LTM4653_object = InterfaceTests()
            my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            LTM4653_object.env = Mock()
            mock_Random.return_value = expected_voltage
            LTM4653_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.WriteBar2RailCommand = Mock()
            LTM4653_object.GetHvdpsTrackingVoltage = Mock()
            my_hvdps.ReadRegister = Mock(return_value=Mock(SoftSpanCode=7))
            my_hvdps.HvTrackingVoltageAdcValueConversion = Mock(return_value=actual_voltage)
            LTM4653_object.Log = Mock()
            LTM4653_object.DirectedHVVoltageTest(total_test_iterations)
            log_calls = LTM4653_object.Log.call_args_list
            self.assertEqual(my_hvdps.ReadRegister.call_count,8)
            self.assertEqual(LTM4653_object.Log.call_count, 1)
            for i in range(0, len(log_calls), 2):
                voltage_read_log_level, voltage_read_set_log_message = log_calls[i + 0][0]
                self.assertEqual(voltage_read_log_level.upper(), 'DEBUG', 'Log level should be DEBUG')
                self.assertRegex(voltage_read_set_log_message,
                                 'Tracking Voltage read from ADC successfully on {} out of {} iterations on all rails'.format(successful_test_iterations, total_test_iterations))


    def test_DirectedHVVoltageTest_fail(self):
        with patch.object(random, 'randint')as mock_Random:
            expected_voltage = 15
            actual_voltage = 9
            LTM4653_object = InterfaceTests()
            my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            LTM4653_object.env = Mock()
            mock_Random.return_value = expected_voltage
            LTM4653_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.WriteBar2RailCommand = Mock()
            LTM4653_object.GetHvdpsTrackingVoltage = Mock()
            my_hvdps.ReadRegister = Mock(return_value=Mock(SoftSpanCode=7))
            my_hvdps.HvTrackingVoltageAdcValueConversion = Mock(return_value=actual_voltage)
            LTM4653_object.Log = Mock()
            LTM4653_object.DirectedHVVoltageTest(test_iterations=10)
            log_calls = LTM4653_object.Log.call_args_list
            self.assertEqual(my_hvdps.ReadRegister.call_count, 80)
            self.assertEqual(LTM4653_object.Log.call_count, 90)
            log_level_list =['ERROR']*8 +['DEBUG']
            for i,log_level in enumerate(log_level_list):
                voltage_read_log_level, voltage_read_set_log_message = log_calls[i+ 0][0]
                self.assertEqual(voltage_read_log_level.upper(), log_level, 'Log level should be {}'.format(log_level))


    def test_DirectedHVVoltageTest_IncorrectSpanCode(self):
        with patch.object(random, 'randint')as mock_Random:
            softspancode = 5
            expected_voltage = 9
            actual_voltage = 9
            LTM4653_object = InterfaceTests()
            my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            LTM4653_object.env = Mock()
            mock_Random.return_value = expected_voltage
            LTM4653_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.WriteBar2RailCommand = Mock()
            my_hvdps.GetHvdpsTrackingVoltage = Mock()
            my_hvdps.ReadRegister = Mock(return_value=Mock(SoftSpanCode=softspancode))
            my_hvdps.HvTrackingVoltageAdcValueConversion = Mock(return_value=actual_voltage)
            with self.assertRaises(fval.LoggedError):
                LTM4653_object.DirectedHVVoltageTest(test_iterations=1)


    def test_DirectedHVVoltageTest_IncorrectSpanCode_IncorrectVoltage(self):
        with patch.object(random, 'randint')as mock_Random:
            softspancode = 5
            expected_voltage = 15
            actual_voltage = 9
            LTM4653_object = InterfaceTests()
            my_hvdps = hvdpsSubslot.HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            LTM4653_object.env = Mock()
            mock_Random.return_value = expected_voltage
            LTM4653_object.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[my_hvdps])
            self.configure_before_test(my_hvdps)
            my_hvdps.WriteBar2RailCommand = Mock()
            my_hvdps.GetHvdpsTrackingVoltage = Mock()
            my_hvdps.ReadRegister = Mock(return_value=Mock(SoftSpanCode=softspancode))
            my_hvdps.HvTrackingVoltageAdcValueConversion = Mock(return_value=actual_voltage)
            LTM4653_object.Log = Mock()
            LTM4653_object.DirectedHVVoltageTest(test_iterations=1)
            log_calls = LTM4653_object.Log.call_args_list
            self.assertEqual(my_hvdps.ReadRegister.call_count, 8)
            self.assertEqual(LTM4653_object.Log.call_count, 17)
            log_level_list = ['ERROR'] * 16 + ['DEBUG']
            for i, log_level in enumerate(log_level_list):
                voltage_read_log_level, voltage_read_set_log_message = log_calls[i + 0][0]
                self.assertEqual(voltage_read_log_level.upper(), log_level, 'Log level should be {}'.format(log_level))

    def test_DirectedHVVoltageTest_zero_test_iterations(self):
        with patch.object(random, 'randint')as mock_Random:
            LTM4653_object = InterfaceTests()
            LTM4653_object.Log = Mock()
            LTM4653_object.DirectedHVVoltageTest(test_iterations=0)
            log_calls = LTM4653_object.Log.call_args_list
            self.assertEqual(LTM4653_object.Log.call_count, 1)
            for i in range(0, len(log_calls), 1):
                iteration_log_level, iteration_log_message = log_calls[i][0]
                self.assertEqual(iteration_log_level.upper(), 'ERROR', 'Log level should be error')
                self.assertRegex(iteration_log_message,'Test failed to execute due to 0 number of iterations given')


    def configure_before_test(self,inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.LTM4653VoltageConverterEnable = Mock()
        inst_subslot.WriteTQHeaderViaBar2 = Mock()
        inst_subslot.WriteTQFooterViaBar2 = Mock()





