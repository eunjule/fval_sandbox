################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_ForceVoltage.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test to verify if Correct voltage was forced
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 02/20/18
#       Group: HDMT FPGA Validation
################################################################################

import unittest
from unittest.mock import Mock


from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Dps.Tests.ForceVoltage import ForceVoltageOpenSocket


class ForceVoltageTestClass(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None


    def test_verify_expected_voltage_for_incorrect_voltage(self):
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        rail = 5
        uhc =3
        voltage = 1.5
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_rail_voltage = Mock(return_value=0.45)
        force_voltage.Log = Mock()
        force_voltage.verify_expected_voltage(hddps_subslot, rail, uhc ,'LC', voltage)
        log_calls = force_voltage.Log.call_args_list
        self.assertEqual(len(log_calls),1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'is not within the allowable deviation of','Log not displaying correct information')

    def test_verify_alarm_status_if_alarm_found(self):
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        list_of_alarms = ['dummy_alarm']
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=list_of_alarms)
        hddps_subslot.ReadRegister = Mock(return_value=Mock(Alarm = 1, fields = lambda : ['Alarm']))
        force_voltage.Log = Mock()
        force_voltage.verify_alarm_status(hddps_subslot, 5,force_voltage=5)
        log_calls = force_voltage.Log.call_args_list
        self.assertEqual(len(log_calls), 2)
        expected_messages = ['dummy_alarm','Alarm']
        for i in range(2):
            log_level, log_message =  log_calls[i][0]
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, expected_messages[i], 'Log not displaying correct information')

    def test_verify_alarm_status_if_no_alarms_found(self):
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        list_of_alarms = []
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=list_of_alarms)
        hddps_subslot.ReadRegister = Mock(return_value=Mock(Alarm=1, fields=lambda: ['Alarm']))
        force_voltage.Log = Mock()
        force_voltage.verify_alarm_status(hddps_subslot, 5, force_voltage= 10)
        log_calls = force_voltage.Log.call_args_list
        self.assertEqual(len(log_calls), 0)

    def test_parameters_for_generate_trigger_queue_pos_voltage(self):
        rail = 3
        uhc = 5
        dutid = 15
        rail_type = 'HV'
        voltage = 2.2

        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        hvdps_subslot = Mock(RAIL_COUNT = {rail_type:10},UHC_COUNT = 8)
        force_voltage.Log = Mock()
        force_voltage.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        force_voltage.generate_trigger_queue = Mock()
        force_voltage.verify_alarm_status = Mock()
        hvdps_subslot.get_positive_random_voltage = Mock(return_value=voltage)
        force_voltage.force_voltage_open_socket_scenario(board=hvdps_subslot, rail_type=rail_type, iterations = 10, negative=False)
        validate_generate_trigger_queue = force_voltage.generate_trigger_queue.call_args
        self.assertNotEqual(len(validate_generate_trigger_queue), 0)
        args, kwargs = validate_generate_trigger_queue

        call_board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, voltage)
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rail)


    def test_parameters_for_generate_trigger_queue_neg_voltage(self):
        rail = 3
        uhc = 5
        dutid = 15
        rail_type = 'LC'
        voltage = -2.2

        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        hvdps_subslot = Mock(RAIL_COUNT = {rail_type:10},UHC_COUNT = 8)
        force_voltage.Log = Mock()
        force_voltage.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        force_voltage.generate_trigger_queue = Mock()
        force_voltage.verify_alarm_status = Mock()
        hvdps_subslot.get_negative_random_voltage = Mock(return_value=voltage)
        force_voltage.force_voltage_open_socket_scenario(board=hvdps_subslot, rail_type=rail_type, iterations = 10, negative=True)
        validate_generate_trigger_queue = force_voltage.generate_trigger_queue.call_args
        self.assertNotEqual(len(validate_generate_trigger_queue), 0)
        args, kwargs = validate_generate_trigger_queue
        call_board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, voltage)
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rail)

    def test_failing_voltage(self):
        rail = 3
        uhc = 5
        dutid = 15
        rail_type = 'LC'
        voltage = -2.2

        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        hvdps_subslot = Mock(RAIL_COUNT = {rail_type:10},UHC_COUNT = 8)
        force_voltage.Log = Mock()
        force_voltage.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        force_voltage.generate_trigger_queue = Mock()
        force_voltage.verify_alarm_status = Mock()
        force_voltage.verify_expected_voltage = Mock(side_effect=[False] * 10)
        hvdps_subslot.get_negative_random_voltage = Mock(return_value=voltage)
        force_voltage.force_voltage_open_socket_scenario(board=hvdps_subslot, rail_type=rail_type, iterations = 10, negative=True)
        validate_generate_trigger_queue = force_voltage.generate_trigger_queue.call_args
        self.assertNotEqual(len(validate_generate_trigger_queue), 0)
        args, kwargs = validate_generate_trigger_queue

        call_board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, voltage)
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rail)


        log_calls = force_voltage.Log.call_args_list
        self.assertEqual(len(log_calls), 10)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'LC Expected voltage:-2.20 seen on 0 of 1 UHC/Rail combinations.',
                             'Log not displaying correct information')


    def test_randomize_rail_uhc_for_hv(self):
        force_voltage = ForceVoltageOpenSocket()
        expected_tuple_list_length = 64
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        rail_uhc_tuple_list = force_voltage.randomize_rail_uhc(hvdps_subslot, 'HV')
        self.assertEqual(len(rail_uhc_tuple_list),expected_tuple_list_length)

    def test_parameters_for_forcevoltage_hc_rails(self):
        rail_type = 'HC'
        iterations = 10
        negative = False
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        force_voltage.force_voltage_open_socket_scenario = Mock()
        force_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        force_voltage.RandomHCRailForceVoltageOpenSocketTest()
        validate_force_voltage_open_socket_scenario = force_voltage.force_voltage_open_socket_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_open_socket_scenario), 0)
        args, kwargs = validate_force_voltage_open_socket_scenario
        call_board, call_rail_type, call_iterations = args
        call_negative = kwargs['negative']
        self.assertEqual(call_negative, negative)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)


    def test_parameters_for_forcevoltage_lc_rails(self):
        rail_type = 'LC'
        iterations = 10
        negative = False
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        force_voltage.force_voltage_open_socket_scenario = Mock()
        force_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        force_voltage.RandomLCRailForceVoltageOpenSocketTest()
        validate_force_voltage_open_socket_scenario = force_voltage.force_voltage_open_socket_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_open_socket_scenario), 0)
        args, kwargs = validate_force_voltage_open_socket_scenario
        call_board, call_rail_type, call_iterations = args
        call_negative = kwargs['negative']
        self.assertEqual(call_negative, negative)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_forcevoltage_negative_lc_rails(self):
        rail_type = 'LC'
        iterations = 10
        negative = True
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        force_voltage.force_voltage_open_socket_scenario = Mock()
        force_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        force_voltage.RandomLCRailNegativeForceVoltageOpenSocketTest()
        validate_force_voltage_open_socket_scenario = force_voltage.force_voltage_open_socket_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_open_socket_scenario), 0)
        args, kwargs = validate_force_voltage_open_socket_scenario
        call_board, call_rail_type, call_iterations = args
        call_negative = kwargs['negative']
        self.assertEqual(call_negative, negative)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_forcevoltage_hv_rails(self):
        rail_type = 'HV'
        iterations = 10
        negative = False
        force_voltage = ForceVoltageOpenSocket()
        force_voltage.env = Mock()
        force_voltage.force_voltage_open_socket_scenario = Mock()
        force_voltage.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        force_voltage.RandomHVRailForceVoltageOpenSocketTest()
        validate_force_voltage_open_socket_scenario = force_voltage.force_voltage_open_socket_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_open_socket_scenario), 0)
        args, kwargs = validate_force_voltage_open_socket_scenario
        call_board, call_rail_type, call_iterations = args
        call_negative = kwargs['negative']
        self.assertEqual(call_negative, negative)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_to_verify_trigger_HCLC_rail_queue_string_content(self):
        expected_trigger_queue_data = b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x00\x00\x15\x9a\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\xce\x1c\x15\x9c\xff\xff\x15\x9b\x01\x00\x15\xa2\xc0X\x15\x97P\x00\x15\xbb\x00\x00\x15\xb9\xff\xff\x15\x97\x01\x00\x15\x9a\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        force_voltage = ForceVoltageOpenSocket()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value='HC')
        trigger_queue_content = force_voltage.generate_trigger_queue(hddps_subslot, force_voltage=10, rail=5, rail_type='HC', dutid = 15, uhc = 5)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_trigger_HV_rail_queue_string_content(self):
        expected_trigger_queue_data = b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x8b\x0c\x15\xc5\x00\x00\x15\x9a\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\x07\x03\x15\x9c\xd3\xd4\x15\x9b\x01\x00\x15\xa2\x8b\x0c\x15\xc5\x00!\x15\x97P\x00\x15\xbb\x01\x00\x15\x9a\x00\x00\x15\xb9\xcd\r\x15\xc5\xe7\x84\x15\x97\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        force_voltage = ForceVoltageOpenSocket()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value='HV')
        trigger_queue_content = force_voltage.generate_trigger_queue(hvdps_subslot, force_voltage=10, rail=5, rail_type='HV', dutid = 15, uhc = 5)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

