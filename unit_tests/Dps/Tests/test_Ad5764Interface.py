################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random

from unittest.mock import patch
from Common import fval
from unittest.mock import Mock
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Dps.Tests.Ad5764 import Ad5764InterfaceTests


class HvdpsAd5764InterfaceTests(unittest.TestCase):
    POSITIONAL_ARGS_INDEX =0


    def test_ad5764_interface_via_hil_api_pass_with_expected_call_count(self):
        expected_call_count = 36
        hvdpsad5764 = Ad5764InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5764.env = Mock()
        hvdpsad5764.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdpsad5764.write_and_verify_ad5764_data_register = Mock()
        hvdpsad5764.DirectedAd5764InterfaceCheckViaHilCallTest()
        for call in hvdpsad5764.write_and_verify_ad5764_data_register.call_args_list:
            subslot,dac_code,channel,dac_id,rail_type = call[self.POSITIONAL_ARGS_INDEX]
            self.assertIn(dac_code,[0x7FFF,0x8000,0x8001])
            self.assertIn(channel,range(5))
            self.assertIn(dac_id,range(4))
        self.assertEqual(hvdpsad5764.write_and_verify_ad5764_data_register.call_count, expected_call_count)


    def test_verify_write_read_to_ad5764_data_register_pass(self):
        zero_voltage = 0x8000
        hvdpsad5764 = Ad5764InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5764.env = Mock()
        hvdpsad5764.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.WritetoAd5764 = Mock()
        hvdpsad5764.Log = Mock()
        hvdps_subslot.ReadAd5764Register = Mock(return_value=0x8000)
        hvdpsad5764.write_and_verify_ad5764_data_register(hvdps_subslot, binary_data_code=zero_voltage, channel=0, dac=0,rail_type='HV')
        log_calls = hvdpsad5764.Log.call_args_list
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'DEBUG')


    def test_verify_write_read_to_ad5764_data_register_fail(self):
        binary_data_code_for_zero_voltage = 0x8000
        hvdpsad5764 = Ad5764InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5764.env = Mock()
        hvdpsad5764.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.WritetoAd5764 = Mock()
        hvdps_subslot.ReadAd5764Register = Mock(return_value=0x800)
        with self.assertRaises(fval.LoggedError):
            hvdpsad5764.write_and_verify_ad5764_data_register(hvdps_subslot, binary_data_code_for_zero_voltage, channel=0, dac=0,rail_type='HV')



    def configure_before_test(self,inst_subslot):
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.WriteBar2RailCommand = Mock()
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.WriteTQFooterViaBar2 = Mock()
        inst_subslot.WriteTQHeaderViaBar2= Mock()

    def test_validate_set_current_value_fail(self):
        hvdpsad5764 = Ad5764InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.is_close = Mock(return_value=False)
        with self.assertRaises(fval.LoggedError):
            hvdpsad5764.validate_set_ad5764_value(dut=hvdps_subslot, channel=0, raw_dac_value=0x8000, dac_device=1,
                                                  result=0x8001, rail_type='HV', rail=5)

    def test_validate_set_current_value_pass(self):
        hvdpsad5764 = Ad5764InterfaceTests()
        hvdpsad5764.Log = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.is_close = Mock(return_value=True)
        hvdpsad5764.validate_set_ad5764_value(dut=hvdps_subslot, channel=0, raw_dac_value=0x8000, dac_device=1,
                                              result=0x8000, rail_type='HV', rail=5)
        log_calls = hvdpsad5764.Log.call_args_list
        self.assertEqual(len(log_calls),1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'DEBUG')

    def test_Ad5764InterfaceCheckViaRailCommandScenario(self):
        hvdpsad5764 = Ad5764InterfaceTests()
        predefined_list = [(0x8000,0x8000),(0x8001,0x8001)]
        expected_list = [0x8000,0x8001]
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        self.configure_before_test(hvdps_subslot)
        hvdpsad5764.get_predefined_raw_ad5764_currents = Mock(return_value=predefined_list)
        hvdps_subslot.ReadAd5764Register = Mock(side_effect=expected_list)
        hvdpsad5764.validate_set_ad5764_value = Mock()
        hvdpsad5764.Ad5764InterfaceCheckViaRailCommandScenario(dut=hvdps_subslot,rail=random.randint(0,7),uhc=3,rail_type='HV')
        validate_current_call_list = hvdpsad5764.validate_set_ad5764_value.call_args_list
        self.assertEqual(len(validate_current_call_list),2)
        for i in range(2):
            args,kwargs = validate_current_call_list[i]
            dut,channel, current, dac_device, result,rail,rail_type = args
            self.assertEqual(current,expected_list[i])
            self.assertEqual(result,predefined_list[i][0])


    def test_get_predefined_raw_ad5764_currents(self):
        hvdpsad5764 = Ad5764InterfaceTests()
        raw_currents = hvdpsad5764.get_predefined_raw_ad5764_currents()
        self.assertEqual(len(raw_currents),5)
        self.assertEqual(set(raw_currents),set([(0x8000,0x8000),(0x8001,0x8001),(0x8002,0x8002),(0x8003,0x8003),(0x8004,0x8004)]))

    def test_get_predefined_sfp_currents_ad5764(self):
        hvdpsad5764 = Ad5764InterfaceTests()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        sfp_currents = hvdpsad5764.get_predefined_sfp_currents_ad5764(dut=hvdps_subslot)
        self.assertEqual(len(sfp_currents),5)
        self.assertEqual(set(sfp_currents),set([(20977, 9830.4), (25170, 8519.68), (16785, 9175.039999999999), (16787, 7208.960000000001), (16786, 7864.32)]))

    def test_conversion_to_dac(self):
        hvdpsad5764 = Ad5764InterfaceTests()
        self.assertEqual(round(hvdpsad5764.conversion_to_dac(0.001),2), 7208.96)
        self.assertEqual(round(hvdpsad5764.conversion_to_dac(0.002),2),7864.32)
        self.assertEqual(round(hvdpsad5764.conversion_to_dac(0.003),2),8519.68)
        self.assertEqual(round(hvdpsad5764.conversion_to_dac(0.004),2),9175.04)








