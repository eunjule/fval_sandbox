################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: 
# -------------------------------------------------------------------------------
#     Purpose: 
# -------------------------------------------------------------------------------
#  Created by: Renuka Agrawal
#        Date: 1/27/15
#       Group: HDMT FPGA Validation
###############################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.HcRails import Conditions


class HcRailsTests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('DirectedHcForceCurrentTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()
