################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_VerifyRailAlarms.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test to verify if Correct alarms are generated
#-------------------------------------------------------------------------------
#  Created by:
#        Date: 04/18/18
#       Group: HDMT FPGA Validation
################################################################################

import unittest
from unittest.mock import Mock
from unittest.mock import patch


from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps.hddps import Hddps
from Dps.Tests.VerifyRailAlarms import ComparatorAlarms
from Dps.Tests.VerifyRailAlarms import KelvinAlarms
from datetime import datetime


class VerifyRailAlarmsTestClass(unittest.TestCase):


    def tearDown(self):
        comparator_alarms = ComparatorAlarms('DirectedHCRailComparatorHighAlarmTest')
        comparator_alarms.env = Mock()
        comparator_alarms._outcome = Mock(failures=[], errors=[])
        comparator_alarms._softErrors = []
        comparator_alarms.start_time = datetime.now()
        comparator_alarms.tearDown()

    def test_to_verify_generate_non_alarm_condition_trigger_queue_content(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x00\x00\x15\x9a\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\xce\x1c\x15\x9c\xff\xff\x15\x9b\x01\x00\x15\xa2\xc0X\x15\x97P\x00\x15\xbb\x00\x00\x15\xb9\xb1\x94\x15\x97\x01\x00\x15\x9a\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = comparator_alarms.generate_non_alarm_condition_trigger_queue(hddps_subslot, rail_type ='HC', force_voltage=3, dutid = 15, rail = 5, uhc = 5)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_alarm_trigger_queue_content_for_hc_comparator_high(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\xce\x1c\x15\x9c\xc90\x15\x9b\x01\x00\x15\xa8\x00\x00\x15\xa5\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = comparator_alarms.generate_alarm_condition_trigger_queue(hddps_subslot, rail_type ='HC', dutid = 15, rail = 5, uhc = 5, expected_hclc_alarm='ComparatorHighAlarm')
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_alarm_trigger_queue_content_for_hc_comparator_low(self):
        expected_trigger_queue_data = b' \x00\x10\xc3 \x00\x10\xc4\xa7\xbc\x15\x9c\xff\xff\x15\x9b\x01\x00\x15\xa8\x00\x00\x15\xa5\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = comparator_alarms.generate_alarm_condition_trigger_queue(hddps_subslot, rail_type ='HC', dutid = 15, rail = 5, uhc = 5, expected_hclc_alarm='ComparatorLowAlarm')
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_trigger_queue_content_for_lc_comparator_high(self):
        expected_trigger_queue_data = b' \x00\x10\xc3 \x00\x10\xc4\xce\x1c\x15\x9c\xc90\x15\x9b\x01\x00\x15\xa8\x00\x00\x15\xa5\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = comparator_alarms.generate_alarm_condition_trigger_queue(hddps_subslot, rail_type ='LC', dutid = 15, rail = 5, uhc = 5,expected_hclc_alarm='ComparatorHighAlarm')
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_generate_trigger_queue_content_for_lc_comparator_low(self):
        expected_trigger_queue_data =  b' \x00\x10\xc3 \x00\x10\xc4\xa7\xbc\x15\x9c\xff\xff\x15\x9b\x01\x00\x15\xa8\x00\x00\x15\xa5\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value= 0x0)
        trigger_queue_content = comparator_alarms.generate_alarm_condition_trigger_queue(hddps_subslot, rail_type ='LC',dutid = 15, rail = 5, uhc = 5,expected_hclc_alarm='ComparatorLowAlarm')
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_randomize_rail_uhc_for_hc(self):
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        hddps_subslot = Hddps(slot=5,rc=Mock)
        expected_tuple_list_length = 80
        rail_uhc_tuple_list = comparator_alarms.randomize_rail_uhc(hddps_subslot.subslots[1], 'HC')
        self.assertEqual(len(rail_uhc_tuple_list),expected_tuple_list_length)

    def test_calls_for_comparator_alarm_scenario(self):
        iterations = 1
        expected_hclc_alarm = 'ComparatorHighAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        mock_board = Mock(RAIL_COUNT={'HV': 1},
                          UHC_COUNT=1,
                          get_positive_random_voltage=Mock(return_value=2.5))
        comparator_alarms.generate_non_alarm_condition_trigger_queue = Mock()
        comparator_alarms.generate_alarm_condition_trigger_queue = Mock()
        comparator_alarms.check_for_expected_alarm = Mock(return_value= True)
        mock_board.get_global_alarms = Mock(return_value=[])
        comparator_alarms.comparator_alarm_scenario(board=mock_board, rail_type ='HV', iterations=iterations,
                                                    expected_hclc_alarm= expected_hclc_alarm)

        generate_non_alarm_condition_trigger_queue_calls = comparator_alarms.generate_non_alarm_condition_trigger_queue.call_args_list
        self.assertEqual(len(generate_non_alarm_condition_trigger_queue_calls), 1)
        args,kwargs = generate_non_alarm_condition_trigger_queue_calls[0]
        call_board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_dutid, 15)
        self.assertEqual(call_uhc, 0)

        generate_alarm_condition_trigger_queue_calls = comparator_alarms.generate_alarm_condition_trigger_queue.call_args_list
        self.assertEqual(len(generate_alarm_condition_trigger_queue_calls), 1)
        args, kwargs = generate_alarm_condition_trigger_queue_calls[0]
        call_board, call_rail_type, call_dutid, call_rail, call_uhc, call_expected_hclc_alarm = args
        self.assertEqual(call_dutid, 15)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_expected_hclc_alarm, expected_hclc_alarm)

        check_for_single_rail_alarm_calls = mock_board.check_for_single_hclc_rail_alarm.call_args_list
        self.assertEqual(len(check_for_single_rail_alarm_calls), 1)
        args, kwargs = check_for_single_rail_alarm_calls[0]
        call_rail_type, call_expected_alarm = args
        self.assertEqual(call_expected_alarm, expected_hclc_alarm)

    def test_parameters_for_comparator_high_hc_rail(self):
        rail_type = 'HC'
        iterations = 2
        expected_alarm = 'ComparatorHighAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        comparator_alarms.comparator_alarm_scenario = Mock()
        comparator_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        comparator_alarms.DirectedHCRailComparatorHighAlarmTest()
        validate_comparator_alarm_scenario_parameter = comparator_alarms.comparator_alarm_scenario.call_args
        self.assertNotEqual(len(validate_comparator_alarm_scenario_parameter), 0)
        args, kwargs = validate_comparator_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_comparator_low_hc_rail(self):
        rail_type = 'HC'
        iterations = 2
        expected_alarm = 'ComparatorLowAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        comparator_alarms.comparator_alarm_scenario = Mock()
        comparator_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        comparator_alarms.DirectedHCRailComparatorLowAlarmTest()
        validate_comparator_alarm_scenario_parameter = comparator_alarms.comparator_alarm_scenario.call_args
        self.assertNotEqual(len(validate_comparator_alarm_scenario_parameter), 0)
        args, kwargs = validate_comparator_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_comparator_high_lc_rail(self):
        rail_type = 'LC'
        iterations = 2
        expected_alarm = 'ComparatorHighAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        comparator_alarms.comparator_alarm_scenario = Mock()
        comparator_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        comparator_alarms.DirectedLCRailComparatorHighAlarmTest()
        validate_comparator_alarm_scenario_parameter = comparator_alarms.comparator_alarm_scenario.call_args
        self.assertNotEqual(len(validate_comparator_alarm_scenario_parameter), 0)
        args, kwargs = validate_comparator_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_comparator_low_lc_rail(self):
        rail_type = 'LC'
        iterations = 2
        expected_alarm = 'ComparatorLowAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        comparator_alarms.comparator_alarm_scenario = Mock()
        comparator_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        comparator_alarms.DirectedLCRailComparatorLowAlarmTest()
        validate_comparator_alarm_scenario_parameter = comparator_alarms.comparator_alarm_scenario.call_args
        self.assertNotEqual(len(validate_comparator_alarm_scenario_parameter), 0)
        args, kwargs = validate_comparator_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_comparator_high_hv_rail(self):
        rail_type = 'HV'
        iterations = 2
        expected_alarm = 'ComparatorHighAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        comparator_alarms.comparator_alarm_scenario = Mock()
        comparator_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        comparator_alarms.DirectedHVRailComparatorHighAlarmTest()
        validate_comparator_alarm_scenario_parameter = comparator_alarms.comparator_alarm_scenario.call_args
        self.assertNotEqual(len(validate_comparator_alarm_scenario_parameter), 0)
        args, kwargs = validate_comparator_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_comparator_low_hv_rail(self):
        rail_type = 'HV'
        iterations = 2
        expected_alarm = 'ComparatorLowAlarm'
        comparator_alarms = ComparatorAlarms()
        comparator_alarms.env = Mock()
        comparator_alarms.comparator_alarm_scenario = Mock()
        comparator_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        comparator_alarms.DirectedHVRailComparatorLowAlarmTest()
        validate_comparator_alarm_scenario_parameter = comparator_alarms.comparator_alarm_scenario.call_args
        self.assertNotEqual(len(validate_comparator_alarm_scenario_parameter), 0)
        args, kwargs = validate_comparator_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)


    def test_to_verify_trigger_queue_content_for_generate_alarm_condition_lvm_trigger_queue(self):
        expected_trigger_queue_data =  b'\x10\x00\x00\xc3\x10\x00\x00\xc4\xebQ\x04\xcb\x01\x00\x13\xa8\xbc\x02\x04\xa2\x10\x00\x00\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value= 0x4010001)
        trigger_queue_content = comparator_alarms.generate_low_alarm_condition_lvm_trigger_queue(hvdps_subslot, force_rail =3, sense_rail =4, dutid=5, uhc=7)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_trigger_queue_content_for_generate_high_alarm_condition_lvm_trigger_queue(self):
        expected_trigger_queue_data =  b'\x10\x00\x00\xc3\x10\x00\x00\xc4\x85\xeb\x04\xca\x01\x00\x13\xa8\xbc\x02\x04\xa2\x10\x00\x00\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value= 0x4010001)
        trigger_queue_content = comparator_alarms.generate_high_alarm_condition_lvm_trigger_queue(hvdps_subslot, force_rail =3, sense_rail =4, dutid=5, uhc=7)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,'expected trigger queue differed from expected')

    def test_to_verify_trigger_queue_content_for_non_alarm_condition_lvm_trigger_queue(self):
        expected_trigger_queue_data = b'\x08\x00\x10\xc3\x08\x00\x10\xc4\x10\x00\x00\xc3\x10\x00\x00\xc4\x01\x00\x13\xd1\xf8*\x13\xa2\xff\xff\x03\xc6\x01\x00\x13\xa8\x05\x00\x13\x99\x01\x00\x13\xd3\x8b\x0c\x13\xc5\x00\x00\x04\x9a\x00\x00\x13\x9a\x18\x1c\x13\x9e\xe7\xe3\x13\x9d\x00\x00\x13\xa3\x01\x00\x13\xd3\x01\x00\x13\xcf\x01\x00\x13\xd3\x07\x00\x13\xcd\x07\x03\x13\x9c\xd3\xd4\x13\x9b\x01\x00\x13\xa2\x8b\x0c\x13\xc5\x00!\x13\x97P\x00\x13\xbb\n\xd7\x04\xcb\xe1z\x04\xca\x01\x00\x04\x9a\x01\x00\x13\x9a\x00\x00\x13\xb9X\x0e\x13\xc5\xda\xb6\x13\x97\xbc\x02\x13\xa2\x08\x00\x10\xb2\x10\x00\x00\xb2\x00\x00\x00\xb1'
        comparator_alarms = ComparatorAlarms()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value=0x4010001)
        trigger_queue_content = comparator_alarms.generate_non_alarm_condition_lvm_trigger_queue(hvdps_subslot,
                                                                                             force_rail=3, sense_rail=4,
                                                                                             force_voltage=15,
                                                                                             dutid=5, uhc=7)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,
                         'expected trigger queue differed from expected')

    def test_parameters_for_kelvin_hc_rail(self):
        rail_type = 'HC'
        iterations = 2
        expected_alarm = 'KelvinAlarm'
        kelvin_alarms = KelvinAlarms()
        kelvin_alarms.env = Mock()
        kelvin_alarms.kelvin_alarm_scenario = Mock()
        kelvin_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        kelvin_alarms.DirectedHCRailKelvinAlarmTest()
        validate_kelvin_alarm_scenario_parameter = kelvin_alarms.kelvin_alarm_scenario.call_args
        self.assertNotEqual(len(validate_kelvin_alarm_scenario_parameter), 0)
        args, kwargs = validate_kelvin_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_kelvin_lc_rail(self):
        rail_type = 'LC'
        iterations = 2
        expected_alarm = 'KelvinAlarm'
        kelvin_alarms = KelvinAlarms()
        kelvin_alarms.env = Mock()
        kelvin_alarms.kelvin_alarm_scenario = Mock()
        kelvin_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        kelvin_alarms.DirectedLCRailKelvinAlarmTest()
        validate_kelvin_alarm_scenario_parameter = kelvin_alarms.kelvin_alarm_scenario.call_args
        self.assertNotEqual(len(validate_kelvin_alarm_scenario_parameter), 0)
        args, kwargs = validate_kelvin_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_parameters_for_kelvin_hv_rail(self):
        rail_type = 'HV'
        iterations = 2
        expected_alarm = 'KelvinAlarm'
        kelvin_alarms = KelvinAlarms()
        kelvin_alarms.env = Mock()
        kelvin_alarms.kelvin_alarm_scenario = Mock()
        kelvin_alarms.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        kelvin_alarms.DirectedHVRailKelvinAlarmTest()
        validate_kelvin_alarm_scenario_parameter = kelvin_alarms.kelvin_alarm_scenario.call_args
        self.assertNotEqual(len(validate_kelvin_alarm_scenario_parameter), 0)
        args, kwargs = validate_kelvin_alarm_scenario_parameter
        call_board, call_rail_type = args
        call_iterations = kwargs['iterations']
        call_expected_alarm = kwargs['expected_hclc_alarm']
        self.assertEqual(call_expected_alarm, expected_alarm)
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)

    def test_calls_for_kelvin_alarm_scenario(self):
        iterations = 1
        expected_hclc_alarm = 'KelvinAlarm'
        kelvin_alarms = KelvinAlarms()
        kelvin_alarms.env = Mock()
        mock_board = Mock(RAIL_COUNT={'HV': 1},
                          UHC_COUNT=1,
                          get_positive_random_voltage=Mock(return_value=2.5))
        kelvin_alarms.generate_kelvin_trigger_queue = Mock()
        kelvin_alarms.generate_non_alarm_condition_trigger_queue = Mock()
        mock_board.check_for_single_hclc_rail_alarm = Mock(return_value= True)
        mock_board.get_global_alarms = Mock(return_value=[])
        kelvin_alarms.kelvin_alarm_scenario(board=mock_board, rail_type ='HV', iterations=iterations,
                                                    expected_hclc_alarm= expected_hclc_alarm)

        generate_non_alarm_condition_trigger_queue_calls = kelvin_alarms.generate_non_alarm_condition_trigger_queue.call_args_list
        self.assertEqual(len(generate_non_alarm_condition_trigger_queue_calls), 1)
        args, kwargs = generate_non_alarm_condition_trigger_queue_calls[0]
        call_board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_dutid, 15)
        self.assertEqual(call_uhc, 0)

        generate_alarm_condition_trigger_queue_calls = kelvin_alarms.generate_kelvin_trigger_queue.call_args_list
        self.assertEqual(len(generate_alarm_condition_trigger_queue_calls), 1)
        args,kwargs = generate_alarm_condition_trigger_queue_calls[0]
        call_board, call_rail_type, call_dutid, call_rail, call_uhc = args
        call_kelvin_alarm_condition = kwargs['kelvin_alarm_condition']
        self.assertEqual(call_dutid, 15)
        self.assertEqual(call_uhc, 0)
        self.assertEqual(call_kelvin_alarm_condition, 'False')

        check_for_single_rail_alarm_calls = mock_board.check_for_single_hclc_rail_alarm.call_args_list
        self.assertEqual(len(check_for_single_rail_alarm_calls), 1)
        args, kwargs = check_for_single_rail_alarm_calls[0]
        call_rail_type, call_expected_alarm = args
        self.assertEqual(call_expected_alarm, expected_hclc_alarm)

    def test_to_verify_trigger_queue_content_for_non_kelvin_alarm_condition(self):
        expected_trigger_queue_data = b' \x00\x10\xc3 \x00\x10\xc4\x01\x00\x15\xbc\x01\x00\x15\xd1\xf8*\x15\xa2\xff\xff\x05\xc6\x01\x00\x15\xa8\x05\x00\x15\x99\x01\x00\x15\xd3\x00\x00\x15\x9a\x18\x1c\x15\x9e\xe7\xe3\x15\x9d\x00\x00\x15\xa3\x01\x00\x15\xd3\x01\x00\x15\xcf\x01\x00\x15\xd3\x07\x00\x15\xcd\xce\x1c\x15\x9c\xff\xff\x15\x9b\x01\x00\x15\xa2\xc0X\x15\x97P\x00\x15\xbb\x00\x00\x15\xb9\xb1\x94\x15\x97\x01\x00\x15\x9a\xbc\x02\x15\xa2 \x00\x10\xb2\x00\x00\x00\xb1'

        kelvin_alarms = KelvinAlarms()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock(return_value=0x0)
        trigger_queue_content = kelvin_alarms.generate_non_alarm_condition_trigger_queue(hddps_subslot, rail_type ='HC', force_voltage=3, dutid = 15, rail = 5, uhc = 5)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,
                         'expected trigger queue differed from expected')

    def test_to_verify_trigger_queue_content_for_kelvin_alarm_condition(self):
        expected_trigger_queue_data = b'\x08\x00\x10\xc3\x08\x00\x10\xc4\x00\x00\x13\x9a\x00\x00\x13\xbc\x01\x00\x13\x9a\xbc\x02\x13\xa2\x08\x00\x10\xb2\x00\x00\x00\xb1'
        kelvin_alarms = KelvinAlarms()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value=0x4010001)
        trigger_queue_content = kelvin_alarms.generate_kelvin_trigger_queue(hvdps_subslot,rail_type='HC', dutid=5, rail=3, uhc=7,kelvin_alarm_condition='False')
        self.assertEqual(trigger_queue_content, expected_trigger_queue_data,
                         'expected trigger queue differed from expected')

    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()
        inst_subslot.ResetVoltageSoftSpanCode = Mock()
        inst_subslot.ResetCurrentSoftSpanCode = Mock()