################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock
from unittest.mock import patch
from Common.instruments.dps.hvdpsCombo import HvdpsCombo
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps.hvilSubslot import HvilSubslot
from Dps.Tests.HvilAdcCalibration import CurrentCalibration

class HvilAdcCalibrationTestClass(unittest.TestCase):

    def test_rload_regular_current_calibration_scenario_parameters(self):
        hvil_adc_calibration = CurrentCalibration()
        force_rail_type = 'HV'
        sense_rail_type = 'RLOAD'
        hvil_adc_calibration.env = Mock()
        hvil_adc_calibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvil_adc_calibration.rload_current_calibration_scenario = Mock()
        hvil_adc_calibration.DirectedRloadRegularCurrentCalibrationTest()
        validate_rload_regular_current_calibration_scenario = hvil_adc_calibration.rload_current_calibration_scenario.call_args
        self.assertNotEqual(len(validate_rload_regular_current_calibration_scenario), 0)
        args, kwargs = validate_rload_regular_current_calibration_scenario
        dutId, call_force_rail_type, force_voltage, call_sense_rail_type= args
        self.assertEqual(call_force_rail_type, force_rail_type)
        self.assertEqual(call_sense_rail_type, sense_rail_type)

    def test_rload_turbo_current_calibration_scenario_parameters(self):
        hvil_adc_calibration = CurrentCalibration()
        force_rail_type = 'HV'
        sense_rail_type = 'RLOAD'
        hvil_adc_calibration.env = Mock()
        hvil_adc_calibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        hvil_adc_calibration.rload_current_calibration_scenario = Mock()
        hvil_adc_calibration.DirectedRloadTurboCurrentCalibrationTest()
        validate_rload_regular_current_calibration_scenario = hvil_adc_calibration.rload_current_calibration_scenario.call_args
        self.assertNotEqual(len(validate_rload_regular_current_calibration_scenario), 0)
        args, kwargs = validate_rload_regular_current_calibration_scenario
        dutId, call_force_rail_type, force_voltage, call_sense_rail_type= args
        call_turbo = kwargs['turbo']
        self.assertEqual(call_force_rail_type, force_rail_type)
        self.assertEqual(call_sense_rail_type, sense_rail_type)
        self.assertEqual(call_turbo, True)

    def test_DireDirectedRloadRegularCurrentCalibrationTest_pass(self):
            hvil_adc_calibration = CurrentCalibration()
            hvil_adc_calibration.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11, rc=Mock())
            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_adc_calibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            self.configure_before_test(hvdps_inst.subslots[0])
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(2, 7)])
            hvil_adc_calibration.get_random_gain_offset_list = Mock(return_value=[(0x8000, 0x8000)])
            hvil_subslot.SetRailsToSafeState=Mock()
            hvil_subslot.ClearDpsAlarms=Mock()
            hvil_subslot.EnableOnlyOneUhc=Mock()
            hvil_subslot.ConfigureUhcRail=Mock()
            hvil_subslot.WriteTQHeaderViaBar2=Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits = Mock()
            hvil_subslot.SetDefaultCurrentClampLimit = Mock()
            hvil_subslot.SetDefaultThermalCurrentClampLimit = Mock()
            hvil_adc_calibration.reset_calibration_gain_and_offset=Mock()
            hvil_subslot.setup_load_control =Mock()
            hvdps_inst.subslots[0].ConnectRailForce = Mock()
            hvdps_inst.subslots[0].ConnectRailSense = Mock()
            hvdps_inst.subslots[0].ConnectExternalLoad = Mock()
            hvil_subslot.ConnectRailForce=Mock()
            hvil_adc_calibration.setup_steady_state= Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(return_value=[])
            hvil_subslot.get_global_alarms = Mock(return_value=[])
            hvil_subslot.get_rail_current=Mock(return_value=0.3)
            hvil_adc_calibration.verify_rail_voltage = Mock(return_value=True)
            hvil_adc_calibration.set_calibration_gain_offset=Mock()
            hvil_subslot.load_disconnect= Mock()
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_adc_calibration.Log = Mock()
            hvil_adc_calibration.DirectedRloadRegularCurrentCalibrationTest()
            log_calls = hvil_adc_calibration.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
                self.assertEqual(len(log_calls), 1)


    def test_DirectedRloadRegularCurrentCalibrationTest_fail(self):
            hvil_adc_calibration = CurrentCalibration()
            hvil_adc_calibration.env = Mock()
            hvdps_inst = HvdpsCombo(slot=11, rc=Mock())
            hvil_subslot = HvilSubslot(hvdps=hvdps_inst, slot=11, subslot=1)
            hvil_adc_calibration.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvil_subslot])
            self.configure_before_test(hvdps_inst.subslots[0])
            hvil_subslot.randomize_rail_uhc = Mock(return_value=[(2, 7)])
            hvil_adc_calibration.get_random_gain_offset_list = Mock(return_value=[(0x8000, 0x8000)])
            hvil_subslot.SetRailsToSafeState=Mock()
            hvil_subslot.ClearDpsAlarms=Mock()
            hvil_subslot.EnableOnlyOneUhc=Mock()
            hvil_subslot.ConfigureUhcRail=Mock()
            hvil_subslot.WriteTQHeaderViaBar2=Mock()
            hvil_subslot.SetDefaultVoltageComparatorLimits=Mock()
            hvil_subslot.SetDefaultCurrentClampLimit=Mock()
            hvil_subslot.SetDefaultThermalCurrentClampLimit=Mock()
            hvil_adc_calibration.reset_calibration_gain_and_offset=Mock()
            hvil_subslot.setup_load_control =Mock()
            hvdps_inst.subslots[0].ConnectRailForce = Mock()
            hvdps_inst.subslots[0].ConnectRailSense = Mock()
            hvdps_inst.subslots[0].ConnectExternalLoad = Mock()
            hvil_subslot.ConnectRailForce=Mock()
            hvil_adc_calibration.setup_steady_state= Mock()
            hvdps_inst.subslots[0].get_global_alarms = Mock(return_value=[])
            hvil_subslot.get_global_alarms = Mock(return_value=[])
            hvil_subslot.get_rail_current=Mock(side_effect=[0.3,0])
            hvil_adc_calibration.verify_rail_voltage = Mock(return_value=True)
            hvil_adc_calibration.verify_expected_current = Mock(return_value=hvil_adc_calibration.CalibrationException)
            hvil_adc_calibration.set_calibration_gain_offset=Mock()
            hvil_subslot.load_disconnect= Mock()
            hvdps_inst.subslots[0].create_cal_board_instance_and_initialize = Mock()
            hvil_adc_calibration.Log = Mock()
            hvil_adc_calibration.DirectedRloadRegularCurrentCalibrationTest()
            log_calls = hvil_adc_calibration.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
                self.assertEqual(len(log_calls), 1)

    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()
        inst_subslot.ResetVoltageSoftSpanCode = Mock()
        inst_subslot.ResetCurrentSoftSpanCode = Mock()