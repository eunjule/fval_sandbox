################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_DynamicPowerLimit.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the floating/fixed point encoding/decoding functions
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 12/20/17
#       Group: HDMT FPGA Validation
################################################################################

import random
import unittest
from Common import configs
from unittest.mock import Mock
from unittest.mock import patch

from Common import fval
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from datetime import datetime
from Dps.Tests.DynamicPowerLimit import CurrentRangeSelect

class CurrentClampSettingSelectClass(unittest.TestCase):

    def test_when_i_clamp_hi_contains_low_value(self):
        with patch('Dps.Tests.DynamicPowerLimit.random.randint')as mock_random:
            random_value_for_i_clamp_hi = 8
            random_value_for_thermal_i_h_clamp = 12
            return_low_value_in_sfp_format = 0x9ff8
            current_range = CurrentRangeSelect()
            hddps_subslot = HddpsSubslot(hddps = Mock(), slot = 2, subslot = 0, rc = Mock())
            current_range.env = Mock()
            current_range.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
            hddps_subslot.WriteTQHeaderViaBar2= Mock()
            hddps_subslot.WriteTQFooterViaBar2 = Mock()
            hddps_subslot.initialize_hddps_rail_for_test = Mock()
            hddps_subslot.Bar2RailCommandForNewTCLoopingChange = Mock()
            hddps_subslot.WriteBar2RailCommand = Mock()
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            mock_random.side_effect = [random_value_for_i_clamp_hi, random_value_for_thermal_i_h_clamp]
            hddps_subslot.ReadAd5560Register = Mock(return_value = return_low_value_in_sfp_format)
            current_range.CheckForDynamicPowerLimitCompatableFpga = Mock()
            with self.assertRaises(StopIteration):
                current_range.RandomLowCurrentClampSettingSelectTest()

    def test_when_thermal_i_h_clamp_contains_low_value(self):
        with patch('Dps.Tests.DynamicPowerLimit.random.randint')as mock_random:
            random_value_for_thermal_i_h_clamp  = 8
            random_value_for_i_clamp_hi = 12
            return_low_value_in_sfp_format = 0x9ff8
            current_range = CurrentRangeSelect()
            current_range.CheckForDynamicPowerLimitCompatableFpga = Mock()
            hddps_subslot = HddpsSubslot(hddps = Mock(), slot = 2, subslot = 0, rc = Mock())
            current_range.env = Mock()
            current_range.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
            hddps_subslot.WriteTQHeaderViaBar2 = Mock()
            hddps_subslot.WriteTQFooterViaBar2 = Mock()
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            hddps_subslot.initialize_hddps_rail_for_test = Mock()
            hddps_subslot.Bar2RailCommandForNewTCLoopingChange = Mock()
            hddps_subslot.WriteBar2RailCommand = Mock()
            mock_random.side_effect = [random_value_for_i_clamp_hi, random_value_for_thermal_i_h_clamp]
            hddps_subslot.ReadAd5560Register = Mock(return_value = return_low_value_in_sfp_format)
            with self.assertRaises(StopIteration):
                current_range.RandomLowCurrentClampSettingSelectTest()

    def test_when_i_clamp_hi_contains_low_value_but_ad5560_failed_to_detect(self):
        with patch('Dps.Tests.DynamicPowerLimit.random.randint')as mock_random:
            random_value_for_i_clamp_hi = 8
            random_value_for_thermal_i_h_clamp = 12
            return_wrong_value_in_sfp_format = 0x51a5
            current_range = CurrentRangeSelect()
            hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
            current_range.env = Mock()
            current_range.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
            hddps_subslot.WriteTQHeaderViaBar2 = Mock()
            hddps_subslot.WriteTQFooterViaBar2 = Mock()
            hddps_subslot.initialize_hddps_rail_for_test = Mock()
            hddps_subslot.Bar2RailCommandForNewTCLoopingChange = Mock()
            hddps_subslot.WriteBar2RailCommand = Mock()
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            mock_random.side_effect = [random_value_for_i_clamp_hi, random_value_for_thermal_i_h_clamp]
            hddps_subslot.ReadAd5560Register = Mock(return_value=return_wrong_value_in_sfp_format)
            current_range.CheckForDynamicPowerLimitCompatableFpga = Mock()
            with self.assertRaises(fval.LoggedError):
                current_range.RandomLowCurrentClampSettingSelectTest()

    def test_when_thermal_i_h_clamp_contains_low_value_but_ad5560_failed_to_detect(self):
        with patch('Dps.Tests.DynamicPowerLimit.random.randint')as mock_random:
            random_value_for_thermal_i_h_clamp = 8
            random_value_for_i_clamp_hi = 12
            return_wrong_value_in_sfp_format = 0x51a5
            current_range = CurrentRangeSelect()
            hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
            current_range.env = Mock()
            current_range.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            hddps_subslot.WriteTQHeaderViaBar2 = Mock()
            hddps_subslot.WriteTQFooterViaBar2 = Mock()
            hddps_subslot.initialize_hddps_rail_for_test = Mock()
            hddps_subslot.Bar2RailCommandForNewTCLoopingChange = Mock()
            hddps_subslot.WriteBar2RailCommand = Mock()
            mock_random.side_effect = [random_value_for_i_clamp_hi, random_value_for_thermal_i_h_clamp]
            hddps_subslot.ReadAd5560Register = Mock(return_value=return_wrong_value_in_sfp_format)
            current_range.CheckForDynamicPowerLimitCompatableFpga = Mock()
            with self.assertRaises(fval.LoggedError):
                current_range.RandomLowCurrentClampSettingSelectTest()

    def test_when_return_random_high_value_received_which_is_not_sent(self):
        with patch('Dps.Tests.DynamicPowerLimit.random.randint')as mock_random:
            random_value_for_thermal_i_h_clamp = 12
            random_value_for_i_clamp_hi = 8
            return_random_high_value_dected_in_sfp_format = 0x59a5
            current_range = CurrentRangeSelect()
            hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
            current_range.env = Mock()
            current_range.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
            hddps_subslot.WriteTQHeaderViaBar2 = Mock()
            hddps_subslot.WriteTQFooterViaBar2 = Mock()
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            hddps_subslot.initialize_hddps_rail_for_test = Mock()
            hddps_subslot.Bar2RailCommandForNewTCLoopingChange = Mock()
            hddps_subslot.WriteBar2RailCommand = Mock()
            mock_random.side_effect = [random_value_for_i_clamp_hi, random_value_for_thermal_i_h_clamp]
            hddps_subslot.ReadAd5560Register = Mock(return_value=return_random_high_value_dected_in_sfp_format)
            current_range.CheckForDynamicPowerLimitCompatableFpga = Mock()
            with self.assertRaises(fval.LoggedError):
                current_range.RandomLowCurrentClampSettingSelectTest()

    def test_when_return_random_low_value_received_which_is_not_sent(self):
        with patch('Dps.Tests.DynamicPowerLimit.random.randint')as mock_random:
            random_value_for_thermal_i_h_clamp = 8
            random_value_for_i_clamp_hi = 12
            return_random_low_value_dected_in_sfp_format = 0x51a6
            current_range = CurrentRangeSelect()
            hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
            current_range.env = Mock()
            current_range.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=([hddps_subslot]))
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            hddps_subslot.WriteTQHeaderViaBar2 = Mock()
            hddps_subslot.WriteTQFooterViaBar2 = Mock()
            hddps_subslot.initialize_hddps_rail_for_test = Mock()
            hddps_subslot.Bar2RailCommandForNewTCLoopingChange = Mock()
            hddps_subslot.WriteBar2RailCommand = Mock()
            hddps_subslot.SetRailsToSafeState = Mock()
            hddps_subslot.EnableOnlyOneUhc = Mock()
            hddps_subslot.ConfigureUhcRail = Mock()
            mock_random.side_effect = [random_value_for_i_clamp_hi, random_value_for_thermal_i_h_clamp]
            hddps_subslot.ReadAd5560Register = Mock(return_value=return_random_low_value_dected_in_sfp_format)
            current_range.CheckForDynamicPowerLimitCompatableFpga = Mock()
            with self.assertRaises(fval.LoggedError):
                current_range.RandomLowCurrentClampSettingSelectTest()

    def test_skip_test_function(self):
        with patch('Dps.Tests.DynamicPowerLimit.configs') as mock_configs:
            current_range = CurrentRangeSelect()
            mock_configs.HDDPS_LEGACY_TC_LOOPING_FPGAS = [0x012345]
            hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
            hddps_subslot.GetFpgaVersion = Mock(return_value=0x012345)
            hddps_subslot.LegacyTcLooping = True
            with self.assertRaises(unittest.SkipTest):
                current_range.CheckForDynamicPowerLimitCompatableFpga(hddps_subslot)
