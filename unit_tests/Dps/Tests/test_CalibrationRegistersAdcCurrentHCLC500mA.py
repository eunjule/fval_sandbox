################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_encodings.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the floating/fixed point encoding/decoding functions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 12/17/15
#       Group: HDMT FPGA Validation
################################################################################

from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    from Tools import projectpaths
    
from Common import fval
from Dps.Tests.CalibrationRegistersAdcCurrentHCLC500mA import Conditions

class CalibrationRegistersAdcCurrentTests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('LcCurrentAdc500mACalibrationTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()
    
    def test_LcCurrentAdc500mACalibrationTest(self):
        '''just enough code to show imports are working'''
        with patch('Dps.Tests.CalibrationRegistersAdcCurrentHCLC500mA.random') as mock_random:
            conditions = Conditions()
            version_reg = Mock(Pack=Mock(return_value=0))
            conditions.env = Mock(fpgas=[(0,0)],
                                  instruments=[Mock(subslots=[Mock(Read=Mock(return_value=version_reg))])],
                                  ConfigureHclcSamplingEngine=Mock(return_value=''))
            mock_random.choice = Mock(side_effect=[0x8000, 0xC000])
            conditions.LcCurrentAdcCalibrationScenario = Mock(return_value=0.325)
            conditions.LcCurrentAdc500mACalibrationTest()


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
