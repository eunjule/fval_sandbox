################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from Common import fval
from unittest.mock import Mock
from Common.instruments.dps import hvdps_registers as hvdpsregs
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Dps.Tests.ForceVoltageWithLoad import VariableLoad

class VariableLoadTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None
        self.reg_types = hvdpsregs.get_register_types()


    def test_force_voltage_with_load_scenario_pass(self):
        rail = 3
        uhc = 5
        rail_type = 'HC'
        voltage = 2.2
        variable_load = VariableLoad()
        variable_load.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_positive_random_voltage = Mock(return_value=voltage)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hddps_subslot)
        hddps_subslot.cal_load_connect = Mock()
        hddps_subslot.create_cal_board_instance_and_initialize = Mock()
        hddps_subslot.cal_connect_force_sense_lines = Mock()
        hddps_subslot.cal_disconnect_force_sense_lines = Mock()
        variable_load.force_voltage_with_load_trigger_queue = Mock()
        hddps_subslot.get_global_alarms = Mock( return_value= [])
        variable_load.verify_rail_voltage_and_current = Mock(return_value=None)
        variable_load.Log = Mock()
        variable_load.force_voltage_with_load_scenario(board=hddps_subslot, rail_type=rail_type, iteration_count = 1, negative=False)
        log_calls = variable_load.Log.call_args_list
        for log_info,kwargs in log_calls:
            log_level,log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO', 'Log level should be info')
            self.assertRegex(log_message, 'Voltage and current are measured as expected ','Log not displaying correct information')
            self.assertEqual(len(log_calls),1)

    def test_force_voltage_with_load_scenario_scenario_fail(self):
        rail = 3
        uhc = 5
        rail_type = 'HC'
        voltage = 2.2
        variable_load = VariableLoad()
        variable_load.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_positive_random_voltage = Mock(return_value=voltage)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hddps_subslot)
        hddps_subslot.cal_load_connect = Mock()
        hddps_subslot.create_cal_board_instance_and_initialize = Mock()
        hddps_subslot.cal_connect_force_sense_lines = Mock()
        hddps_subslot.cal_disconnect_force_sense_lines = Mock()
        variable_load.force_voltage_with_load_trigger_queue = Mock()
        hddps_subslot.get_global_alarms = Mock( return_value= [])
        variable_load.verify_rail_voltage_and_current = Mock(side_effect=VariableLoad.CurrentOrVoltageMismatch)
        variable_load.Log = Mock()
        variable_load.force_voltage_with_load_scenario(board=hddps_subslot, rail_type=rail_type, iteration_count = 2, negative=False)
        log_calls = variable_load.Log.call_args_list
        for log_info,kwargs in log_calls:
            log_level,log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')
            self.assertRegex(log_message, '{} Voltage and current are measured as expected '.format(rail_type),'Log not displaying correct information')


    def test_force_voltage_with_load_scenario_scenario_global_alarms_found(self):
        rail = 3
        uhc = 5
        rail_type = 'HC'
        alarm_received = 'HclcRailAlarms'
        voltage = 2.2
        variable_load = VariableLoad()
        variable_load.env = Mock()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_positive_random_voltage = Mock(return_value=voltage)
        hddps_subslot.randomize_rail_uhc = Mock(return_value=[(rail, uhc)])
        self.configure_before_test(hddps_subslot)
        hddps_subslot.cal_load_connect = Mock()
        hddps_subslot.create_cal_board_instance_and_initialize = Mock()
        hddps_subslot.cal_connect_force_sense_lines = Mock()
        variable_load.Log = Mock()
        variable_load.force_voltage_with_load_trigger_queue = Mock()
        hddps_subslot.get_global_alarms = Mock( return_value= [alarm_received])
        variable_load.force_voltage_with_load_scenario(board=hddps_subslot, rail_type=rail_type, iteration_count=1,negative=False)
        log_calls = variable_load.Log.call_args_list
        for log_info,kwargs in log_calls:
            log_level,log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR', 'Log level should be info')
            self.assertEqual(len(log_calls), 2)


    def test_force_voltage_with_load_generate_trigger_queue_positive_voltage(self):
        rail = 3
        uhc = 5
        rail_type = 'HC'
        voltage = 2.2
        dutid = 5
        variable_load = VariableLoad()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        variable_load.Log = Mock()
        variable_load.generate_trigger_queue = Mock()
        hddps_subslot.WriteTriggerQueue = Mock()
        hddps_subslot.ExecuteTriggerQueue = Mock()
        variable_load.force_voltage_with_load_trigger_queue(board = hddps_subslot, dutid= dutid, rail= rail, rail_type= rail_type, uhc= uhc, force_voltage = voltage)
        validate_generate_trigger_queue = variable_load.generate_trigger_queue.call_args
        self.assertNotEqual(len(validate_generate_trigger_queue), 0)
        args, kwargs = validate_generate_trigger_queue
        board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, voltage)
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rail)

    def test_force_voltage_with_load_generate_trigger_queue_negative_voltage(self):
        rail = 3
        uhc = 5
        rail_type = 'LC'
        voltage = -2.2
        dutid = 5
        variable_load = VariableLoad()
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        variable_load.Log = Mock()
        variable_load.generate_trigger_queue = Mock()
        hddps_subslot.WriteTriggerQueue = Mock()
        hddps_subslot.ExecuteTriggerQueue = Mock()
        variable_load.force_voltage_with_load_trigger_queue(board = hddps_subslot, dutid= dutid, rail= rail, rail_type= rail_type, uhc= uhc, force_voltage = voltage)
        validate_generate_trigger_queue = variable_load.generate_trigger_queue.call_args
        self.assertNotEqual(len(validate_generate_trigger_queue), 0)
        args, kwargs = validate_generate_trigger_queue
        board, call_rail_type, call_force_voltage, call_dutid, call_rail, call_uhc = args
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_force_voltage, voltage)
        self.assertEqual(call_uhc, uhc)
        self.assertEqual(call_dutid, dutid)
        self.assertEqual(call_rail, rail)


    def test_verify_rail_voltage_and_current_exception_raised_for_voltage_or_current_mismatch(self):
        rail = 3
        uhc = 5
        rail_type = 'HC'
        variable_load = VariableLoad()
        expected_voltage = 1.00
        expected_current = 0.1
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_rail_voltage = Mock(return_value=2.00)
        hddps_subslot.get_rail_current = Mock(return_value=0.1)
        variable_load.Log = Mock()
        variable_load.is_close = Mock(return_value=False)
        with self.assertRaises(VariableLoad.CurrentOrVoltageMismatch):
            variable_load.verify_rail_voltage_and_current(hddps_subslot,rail,rail_type,uhc,expected_voltage,expected_current)

    def test_verify_rail_voltage_and_current_exception_raised_for_voltage_or_current_mismatch_Hv(self):
        rail = 3
        uhc = 5
        rail_type = 'HV'
        variable_load = VariableLoad()
        expected_voltage = 1.00
        expected_current = 0.1
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_rail_voltage = Mock(return_value=2.00)
        hddps_subslot.get_rail_current = Mock(return_value=0.1)
        variable_load.Log = Mock()
        variable_load.is_close = Mock(return_value=False)
        with self.assertRaises(VariableLoad.CurrentOrVoltageMismatch):
            variable_load.verify_rail_voltage_and_current(hddps_subslot,rail,rail_type,uhc,expected_voltage,expected_current)


    def test_verify_rail_voltage_and_current_pass(self):
        rail = 3
        uhc = 5
        rail_type = 'HC'
        variable_load = VariableLoad()
        expected_voltage = 15
        expected_current = 0.1
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_rail_voltage = Mock(return_value=15)
        hddps_subslot.get_rail_current = Mock(return_value=0.1)
        variable_load.Log = Mock()
        result = variable_load.verify_rail_voltage_and_current(hddps_subslot,rail,rail_type,uhc,expected_voltage,expected_current)
        self.assertEqual(result, None)

    def test_verify_rail_voltage_and_current_Hv_voltage_greater_than_ten(self):
        rail = 3
        uhc = 5
        rail_type = 'HV'
        variable_load = VariableLoad()
        expected_voltage = 15
        expected_current = 0.1
        rail_voltage = 15
        allowed_maximum_voltage_deviation = 0.50
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.get_rail_voltage = Mock(return_value=15)
        hvdps_subslot.get_rail_current = Mock(return_value=0.1)
        variable_load.Log = Mock()
        hvdps_subslot.is_in_maximum_allowed_deviation = Mock()
        result = variable_load.verify_rail_voltage_and_current(hvdps_subslot,rail,rail_type,uhc,expected_voltage,expected_current)
        validate_is_in_maximum_allowed_deviation = hvdps_subslot.is_in_maximum_allowed_deviation.call_args
        args,kwargs = validate_is_in_maximum_allowed_deviation
        call_expected_voltage,call_rail_voltage,call_allowed_maximum_deviation = args
        self.assertEqual(call_expected_voltage,expected_voltage)
        self.assertEqual(call_rail_voltage,rail_voltage)
        self.assertEqual(call_allowed_maximum_deviation,allowed_maximum_voltage_deviation)
        self.assertEqual(result, None)

    def test_verify_rail_voltage_and_current_Hv_voltage_less_than_ten(self):
        rail = 3
        uhc = 5
        rail_type = 'HV'
        variable_load = VariableLoad()
        expected_voltage = 7
        expected_current = 0.1
        rail_voltage = 7
        allowed_maximum_voltage_deviation = 0.25
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.get_rail_voltage = Mock(return_value=7)
        hvdps_subslot.get_rail_current = Mock(return_value=0.1)
        variable_load.Log = Mock()
        hvdps_subslot.is_in_maximum_allowed_deviation = Mock()
        result = variable_load.verify_rail_voltage_and_current(hvdps_subslot,rail,rail_type,uhc,expected_voltage,expected_current)
        validate_is_in_maximum_allowed_deviation = hvdps_subslot.is_in_maximum_allowed_deviation.call_args
        args,kwargs = validate_is_in_maximum_allowed_deviation
        call_expected_voltage,call_rail_voltage,call_allowed_maximum_voltage_deviation = args
        self.assertEqual(call_expected_voltage,expected_voltage)
        self.assertEqual(call_rail_voltage,rail_voltage)
        self.assertEqual(call_allowed_maximum_voltage_deviation,allowed_maximum_voltage_deviation)
        self.assertEqual(result, None)

    def test_test_parameters_for_force_voltage_with_load_hc_rail(self):
        rail_type = 'HC'
        iterations = 2
        variable_load = VariableLoad()
        variable_load.env = Mock()
        variable_load.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        variable_load.force_voltage_with_load_scenario = Mock()
        variable_load.RandomForcePositiveVoltageHcTest()
        validate_force_voltage_with_load_scenario = variable_load.force_voltage_with_load_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_with_load_scenario), 0)
        args, kwargs = validate_force_voltage_with_load_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        call_iterations = kwargs['iteration_count']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)
        self.assertEqual(call_negative_voltage, False)

    def test_test_parameters_for_force_positive_voltage_with_load_lc_rail(self):
        rail_type = 'LC'
        iterations = 3
        variable_load = VariableLoad()
        variable_load.env = Mock()
        variable_load.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        variable_load.force_voltage_with_load_scenario = Mock()
        variable_load.RandomForcePositiveVoltageLcTest()
        validate_force_voltage_with_load_scenario = variable_load.force_voltage_with_load_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_with_load_scenario), 0)
        args, kwargs = validate_force_voltage_with_load_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        call_iterations = kwargs['iteration_count']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)
        self.assertEqual(call_negative_voltage, False)

    def test_test_parameters_for_force_negative_voltage_with_load_lc_rail(self):
        rail_type = 'LC'
        iterations = 3
        variable_load = VariableLoad()
        variable_load.env = Mock()
        variable_load.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        variable_load.force_voltage_with_load_scenario = Mock()
        variable_load.RandomForceNegativeVoltageLcTest()
        validate_force_voltage_with_load_scenario = variable_load.force_voltage_with_load_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_with_load_scenario), 0)
        args, kwargs = validate_force_voltage_with_load_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        call_iterations = kwargs['iteration_count']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)
        self.assertEqual(call_negative_voltage, True)

    def test_test_parameters_for_force_positive_voltage_with_load_hv_rail(self):
        rail_type = 'HV'
        iterations = 1
        variable_load = VariableLoad()
        variable_load.env = Mock()
        variable_load.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        variable_load.force_voltage_with_load_scenario = Mock()
        variable_load.RandomForcePositiveVoltageHvTest()
        validate_force_voltage_with_load_scenario = variable_load.force_voltage_with_load_scenario.call_args
        self.assertNotEqual(len(validate_force_voltage_with_load_scenario), 0)
        args, kwargs = validate_force_voltage_with_load_scenario
        call_board, call_rail_type = args
        call_negative_voltage = kwargs['negative']
        call_iterations = kwargs['iteration_count']
        self.assertEqual(call_rail_type, rail_type)
        self.assertEqual(call_iterations, iterations)
        self.assertEqual(call_negative_voltage, False)

    def test_DirectedHvNoClampAlarmTest_trigger_queue_string_match(self):
        expected_trigger_queue_string = b'@\x00\x10\xc3@\x00\x10\xc4\x01\x00\x16\xd1\xf8*\x16\xa2\xff\xff\x06\xc6\x01\x00\x16\xa8\x07\x00\x16\x99\x01\x00\x16\xd3\x8b\x0c\x16\xc5\x00\x00\x16\x9a\x01{\x16\xac\xfe\x84\x16\xab\x01{\x16\x9e\xf8\x9d\x16\x9d\x00\x00\x16\xa3\x01\x00\x16\xd3\x01\x00\x16\xcf\x01\x00\x16\xd3\x07\x00\x16\xcd\x07\x03\x16\x9c\xd3\xd4\x16\x9b\x01\x00\x16\xa2\x8b\x0c\x16\xc5\x00!\x16\x97P\x00\x16\xbb\x01\x00\x16\x9a\x00\x00\x16\xb9 \x0e\x16\xc5\x00\x01\x06\x8b\xda\xb6\x16\xa6@\x1f\x16\xa2\x00\x00\x16\xa7\x00\x00\x06\x8b\xbc\x02\x16\xa2@\x00\x10\xb2\x00\x00\x00\xb1'
        variable_load = VariableLoad()
        rail_type = 'HV'
        force_voltage = 15.0
        dutid = 7
        rail = 6
        uhc =3
        variable_load.env = Mock()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.GetFpgaVersion = Mock(return_value=0x0)
        variable_load.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[Mock()])
        trigger_queue_content = variable_load.generate_trigger_queue(hvdps_subslot,rail_type,force_voltage, dutid, rail, uhc)
        self.assertEqual(trigger_queue_content, expected_trigger_queue_string,'expected trigger queue differed from expected')

    def configure_before_test(self, inst_subslot):
        inst_subslot.SetRailsToSafeState = Mock()
        inst_subslot.ClearDpsAlarms = Mock()
        inst_subslot.EnableAlarms = Mock()
        inst_subslot.EnableOnlyOneUhc = Mock()
        inst_subslot.ConfigureUhcRail = Mock()
        inst_subslot.UnGangAllRails = Mock()