################################################################################
#  INTEL CONFIDENTIAL - Copyright 2016. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: CalibrationRegisters
# -------------------------------------------------------------------------------
#     Purpose: Validating HDDPS Calibration Registers
# -------------------------------------------------------------------------------
#  Created by: Mark Schwartz
#        Date: 8/7/16
#       Group: HDMT FPGA Validation
###############################################################################
from datetime import datetime
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Dps.Tests.CalibrationRegistersOverUnderVolt import Conditions


class CalibrationRegistersOverUnderVoltTests(unittest.TestCase):

    def test_tearDown(self):
        conditions = Conditions('OverVoltageCalibrationLCTest')
        conditions.env = Mock()
        conditions._outcome = Mock(failures=[], errors=[])
        conditions._softErrors = []
        conditions.start_time = datetime.now()
        conditions.tearDown()


