################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random
from unittest.mock import patch
from Common import fval
from unittest.mock import Mock
from Dps.Tests.Ad5560 import InterfaceTests,AutoModeCompensation
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot

class DpsAd5560Tests(unittest.TestCase):

    def test_DirectedModifyRegisterViaHilTest_pass(self):
        with patch('Dps.Tests.Ad5560.random.randint', return_value = 0x0123)as mock_Random:
            hvdpsad5560 = InterfaceTests()
            hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            hvdpsad5560.env = Mock()
            expected_read_register_value = 0x0123
            expected_number_of_iterations =2
            expected_rail_list = [x for x in range(0,8)]
            hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
            hvdps_subslot.SetRailsToSafeState = Mock()
            hvdps_subslot.ClearDpsAlarms = Mock()
            hvdps_subslot.WriteAd5560Register = Mock()
            hvdps_subslot.ReadAd5560Register = Mock(return_value=expected_read_register_value)
            hvdpsad5560.summarize_results = Mock()
            hvdpsad5560.DirectedModifyRegisterViaHilTest(number_of_iterations=2)
            expected_call_list= hvdpsad5560.summarize_results.call_args_list
            self.assertEqual(len(expected_call_list),8)
            for i in range(len(expected_call_list)):
                args, kwargs = expected_call_list[i]
                fail_count, number_of_iterations, pass_count, rail, rail_type = args
                self.assertEqual(number_of_iterations,expected_number_of_iterations)
                self.assertEqual(rail,expected_rail_list[i])

    def test_DirectedModifyRegisterViaHilTest_incorrect_register_value(self):
        with patch('Dps.Tests.Ad5560.random.randint', return_vlaue = 0x0123)as mock_Random:
            hvdpsad5560 = InterfaceTests()
            hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            hvdpsad5560.env = Mock()
            expected_fail_count = 2
            expected_log_calls_length = 16
            hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
            hvdps_subslot.SetRailsToSafeState = Mock()
            hvdps_subslot.ClearDpsAlarms = Mock()
            hvdps_subslot.WriteAd5560Register = Mock()
            hvdps_subslot.ReadAd5560Register = Mock(return_value=0x012)
            hvdpsad5560.summarize_results = Mock()
            hvdpsad5560.Log = Mock()
            hvdpsad5560.DirectedModifyRegisterViaHilTest(number_of_iterations=2)
            log_calls = hvdpsad5560.Log.call_args_list
            expected_call_list = hvdpsad5560.summarize_results.call_args_list
            self.assertEqual(len(expected_call_list), 8)
            for i in range(len(expected_call_list)):
                args, kwargs = expected_call_list[i]
                fail_count, number_of_iterations, pass_count, rail, rail_type = args
                self.assertEqual(fail_count,expected_fail_count)
            self.assertEqual(len(log_calls), expected_log_calls_length)
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')

    def test_Directed6VRangeI7p5MACompensationSettingTest_pass(self):
        hvdpsad5560 = AutoModeCompensation()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5560.env = Mock()
        hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        hvdps_subslot.EnableOnlyOneUhc = Mock()
        hvdps_subslot.UnGangAllRails = Mock()
        hvdps_subslot.ResetVoltageSoftSpanCode = Mock()
        hvdps_subslot.ResetCurrentSoftSpanCode = Mock()
        hvdps_subslot.ConfigureUhcRail = Mock()
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdps_subslot.ReadAd5560Register = Mock(return_value=0x8ae0)
        hvdpsad5560.Log= Mock()
        hvdpsad5560.Directed6VRangeI7p5MACompensationSettingTest()
        log_calls = hvdpsad5560.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertEqual(len(log_calls), 1)

    def test_Directed6VRangeI7p5MACompensationSettingTest_fail(self):
        hvdpsad5560 = AutoModeCompensation()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5560.env = Mock()
        hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.randomize_rail_uhc=Mock(return_value=[(5,4)])
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        hvdps_subslot.EnableOnlyOneUhc = Mock()
        hvdps_subslot.UnGangAllRails = Mock()
        hvdps_subslot.ResetVoltageSoftSpanCode = Mock()
        hvdps_subslot.ResetCurrentSoftSpanCode = Mock()
        hvdps_subslot.ConfigureUhcRail = Mock()
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdps_subslot.ReadAd5560Register = Mock(return_value=0xae0)
        hvdpsad5560.Log= Mock()
        hvdpsad5560.Directed6VRangeI7p5MACompensationSettingTest()
        log_calls = hvdpsad5560.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertEqual(len(log_calls), 1)

    def test_Directed22VrangeI7p5MACompensationSettingTest_scenario_parameters(self):
        hvdpsad5560 = AutoModeCompensation()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5560.env = Mock()
        expected_vrange = 1
        expected_irange = 7
        hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(5, 4)])
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        hvdps_subslot.EnableOnlyOneUhc = Mock()
        hvdps_subslot.UnGangAllRails = Mock()
        hvdps_subslot.ResetVoltageSoftSpanCode = Mock()
        hvdps_subslot.ResetCurrentSoftSpanCode = Mock()
        hvdps_subslot.ConfigureUhcRail = Mock()
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdpsad5560.compensation_mode_setting_scenario = Mock()
        hvdpsad5560.Directed22VrangeI7p5MACompensationSettingTest()
        validate_comp_mode_scenario = hvdpsad5560.compensation_mode_setting_scenario.call_args
        args,kwargs = validate_comp_mode_scenario
        board, dutid,expected_ad5560_compensation_value, rail, rail_type,undertest_vrange, undertest_irange, pass_count = args
        self.assertEqual(undertest_vrange,expected_vrange)
        self.assertEqual(undertest_irange,expected_irange)

    def test_Directed25VrangeI7p5MACompensationSettingTest_scenario_parameters(self):
        hvdpsad5560 = AutoModeCompensation()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5560.env = Mock()
        expected_vrange = 2
        expected_irange = 7
        hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(5, 4)])
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        hvdps_subslot.EnableOnlyOneUhc = Mock()
        hvdps_subslot.UnGangAllRails = Mock()
        hvdps_subslot.ResetVoltageSoftSpanCode = Mock()
        hvdps_subslot.ResetCurrentSoftSpanCode = Mock()
        hvdps_subslot.ConfigureUhcRail = Mock()
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdpsad5560.compensation_mode_setting_scenario = Mock()
        hvdpsad5560.Directed25VrangeI7p5MACompensationSettingTest()
        validate_comp_mode_scenario = hvdpsad5560.compensation_mode_setting_scenario.call_args
        args, kwargs = validate_comp_mode_scenario
        board, dutid, expected_ad5560_compensation_value, rail, rail_type, undertest_vrange, undertest_irange, pass_count = args
        self.assertEqual(undertest_vrange, expected_vrange)
        self.assertEqual(undertest_irange, expected_irange)

    def test_DirectedI500MACompensationSettingTest_scenario_parameters(self):
        hvdpsad5560 = AutoModeCompensation()
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdpsad5560.env = Mock()
        expected_vrange = 1
        expected_irange = 5
        hvdpsad5560.env.duts_or_skip_if_no_vaild_rail = Mock(return_value=[hvdps_subslot])
        hvdps_subslot.SetRailsToSafeState = Mock()
        hvdps_subslot.randomize_rail_uhc = Mock(return_value=[(5, 4)])
        hvdps_subslot.ClearDpsAlarms = Mock()
        hvdps_subslot.EnableAlarms = Mock()
        hvdps_subslot.EnableOnlyOneUhc = Mock()
        hvdps_subslot.UnGangAllRails = Mock()
        hvdps_subslot.ResetVoltageSoftSpanCode = Mock()
        hvdps_subslot.ResetCurrentSoftSpanCode = Mock()
        hvdps_subslot.ConfigureUhcRail = Mock()
        hvdps_subslot.WriteTQHeaderViaBar2 = Mock()
        hvdps_subslot.WriteBar2RailCommand = Mock()
        hvdpsad5560.compensation_mode_setting_scenario = Mock()
        hvdpsad5560.DirectedI500MACompensationSettingTest()
        validate_comp_mode_scenario = hvdpsad5560.compensation_mode_setting_scenario.call_args
        args, kwargs = validate_comp_mode_scenario
        board, dutid, expected_ad5560_compensation_value, rail, rail_type, undertest_vrange, undertest_irange, pass_count = args
        self.assertEqual(undertest_vrange, expected_vrange)
        self.assertEqual(undertest_irange, expected_irange)

    def test_summarize_result_pass(self):
        hvdpsad5560 = InterfaceTests()
        hvdpsad5560.Log = Mock()
        hvdpsad5560.summarize_results(fail_count=0, number_of_iterations=1, pass_count=1, rail=4, rail_type='HV')
        log_calls =hvdpsad5560.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')

    def test_summarize_result_fail(self):
        hvdpsad5560 = InterfaceTests()
        hvdpsad5560.Log = Mock()
        hvdpsad5560.summarize_results(fail_count=1, number_of_iterations=1, pass_count=0, rail=4, rail_type='HV')
        log_calls =hvdpsad5560.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')


    def test_DirectedModifyRegisterViaHilTest_zero_iterations_error_logged(self):
        hvdpsad5560 = InterfaceTests()
        with self.assertRaises(fval.LoggedError):
            hvdpsad5560.DirectedModifyRegisterViaHilTest(number_of_iterations=0)
