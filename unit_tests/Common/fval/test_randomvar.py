################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_randomvar.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the randomvar functions
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 10/14/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
from _build.bin import fvalc


class RandomVarTests(unittest.TestCase):
    def testSingleValueCompile(self):
        r = fvalc.RandomVar.Compile('{10}')
        self.assertTrue(r.Valid())
        self.assertEqual(r(), 10)
    def testRangeCompile(self):
        r = fvalc.RandomVar.Compile('{[10:15]}')
        self.assertTrue(r.Valid())
        self.assertIn(r(), [10, 11, 12, 13, 14, 15])
    def testWeightedDistributionCompile(self):
        r = fvalc.RandomVar.Compile('{10:=1,20:=1}')
        self.assertTrue(r.Valid())
        self.assertIn(r(), [10, 20])
    def testWeightedDistributionDefaultCompile(self):
        r = fvalc.RandomVar.Compile('{10,20}')
        self.assertTrue(r.Valid())
        self.assertIn(r(), [10, 20])
    def testNestedCompile(self):
        r = fvalc.RandomVar.Compile('{1, 5 := 2, [10:15] := 3}')
        self.assertTrue(r.Valid())
        self.assertIn(r(), [1, 5, 10, 11, 12, 13, 14, 15])


if __name__ == '__main__':
    unittest.main(verbosity=2)
