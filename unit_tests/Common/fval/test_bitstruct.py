################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_bitstruct.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the bitstruct module
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/06/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common.fval.bitstruct import Bits, LoadStructsString

class BitsTests(unittest.TestCase):
    def testBasicSlicing(self):
        value = Bits(uint = 0x1234, length = 32)
        self.assertEqual(value[3:0], 0x4)
        self.assertEqual(value[7:4], 0x3)
        self.assertEqual(value[11:8], 0x2)
        self.assertEqual(value[15:12], 0x1)
    def testBasicSlicingReversed(self):
        value = Bits(uint = 0x1234, length = 32)
        self.assertEqual(value[0:3], 0x2)
        self.assertEqual(value[4:7], 0xC)
        self.assertEqual(value[8:11], 0x4)
        self.assertEqual(value[12:15], 0x8)

BasicStructs = LoadStructsString("""\
# Let's put a comment here
@packing.high
struct Byte:
    # and here
    CONSTANT = 69
    bit[7:0] bits

@packing.high
struct ByteOfBits:
    CONSTANT = 101
    bit bit7
    bit bit6
    bit bit5
    bit bit4
    bit bit3
    bit bit2
    bit bit1
    bit bit0

struct NibblePair:  # A NibblePair is a Byte. LOL
    CONSTANT = 3
    bit[3:0] low
    bit[3:0] high
""")

class BitStructBasicTests(unittest.TestCase):
    #
    # Unpacking tests
    #
    def testByteUnpackingHex(self):
        byte = BasicStructs.Byte(uint = 0x8C, length = 8)
        self.assertEqual(byte.bits, 0x8C)
    def testByteUnpackingAuto(self):
        byte = BasicStructs.Byte('0x8C')
        self.assertEqual(byte.bits, 0x8C)
    def testByteOfBitsUnpackingHex(self):
        byte = BasicStructs.ByteOfBits(uint = 0x8C, length = 8)
        self.assertEqual(byte.bit0, 0x0)
        self.assertEqual(byte.bit1, 0x0)
        self.assertEqual(byte.bit2, 0x1)
        self.assertEqual(byte.bit3, 0x1)
        self.assertEqual(byte.bit4, 0x0)
        self.assertEqual(byte.bit5, 0x0)
        self.assertEqual(byte.bit6, 0x0)
        self.assertEqual(byte.bit7, 0x1)
    def testNibblePairUnpackingHex(self):
        pair = BasicStructs.NibblePair(uint = 0x8C, length = 8)
        self.assertEqual(pair.low, 0xC)
        self.assertEqual(pair.high, 0x8)
    def testNibblePairUnpackingAuto(self):
        pair = BasicStructs.NibblePair('0x8C')
        self.assertEqual(pair.low, 0xC)
        self.assertEqual(pair.high, 0x8)
    #
    # Packing tests
    #
    def testBytePacking(self):
        byte = BasicStructs.Byte()
        byte.bits = 0xA1
        self.assertEqual(byte.Pack(), 0xA1)
    def testNibblePairPacking(self):
        pair = BasicStructs.NibblePair()
        pair.low = 0x1
        pair.high = 0xA
        self.assertEqual(pair.Pack(), 0xA1)
    def testDefaultValues(self):
        pair = BasicStructs.NibblePair()
        self.assertEqual(pair.Pack(), 0x00)
    #
    # Other tests
    #
    def testStructConstant(self):
        self.assertEqual(BasicStructs.Byte.CONSTANT, 69)
        self.assertEqual(BasicStructs.ByteOfBits.CONSTANT, 101)
        self.assertEqual(BasicStructs.NibblePair.CONSTANT, 3)
        

TestStruct = LoadStructsString("""\
# https://lost-contact.mit.edu/afs/ict.kth.se/pkg/cadence/incisiv141/14.10.004/doc/sn_eref/simpleexampleunpacking.html#1166790
@packing.high
struct Instruction:
    bit[2:0] opcode
    bit[4:0] operand
    bit[7:0] address

struct Stuff:
    X = 1
    # comment here
    Y = 2
    bit x
    when x == 1:
        # comment here
        bit y
""")

class BitStructTests(unittest.TestCase):
    def testPackingHigh(self):
        instr = TestStruct.Instruction()
        instr.opcode  = 0x4
        instr.operand = 0x19
        instr.address = 0x0F
        # 0x4 0x19 0x0F = 100 11001 00001111 = 1001 1001 0000 1111 = 0x990F
        self.assertEqual(instr.Pack(), 0x990F)
    def testHighUnpackingHex(self):
        instr = TestStruct.Instruction(uint = 0x990F, length = 16)
        self.assertEqual(instr.opcode, 0x4)
        self.assertEqual(instr.operand, 0x19)
        self.assertEqual(instr.address, 0x0F)


Constants = LoadStructsString("""\
X = 1
Y = 0xABCD
Z = 0b011100110000
""")

class BitStructConstantsTests(unittest.TestCase):
    def testConstants(self):
        self.assertEqual(Constants.X, 1)
        self.assertEqual(Constants.Y, 0xABCD)
        self.assertEqual(Constants.Z, 0b011100110000)
        

if __name__ == '__main__':
    unittest.main(verbosity=2)
