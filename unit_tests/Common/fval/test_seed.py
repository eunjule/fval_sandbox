################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: testSeed.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the seed module
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/06/15
#       Group: HDMT FPGA Validation
################################################################################

import random
import unittest

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
from Common.fval.seed import MasterSeed


class SeedTests(unittest.TestCase):
    def testRepeatableMasterSeed(self):
        seed1 = MasterSeed(10,5)
        seed2 = MasterSeed(5,5)
        self.assertEqual(len(seed1.testSeeds), 10)
        self.assertEqual(len(seed2.testSeeds), 5)
        for i in range(5):
            testSeed1 = seed1.GetTestSeed()
            ts1_10 = testSeed1.GetNextInt(0,10)
            ts1_100 = testSeed1.GetNextInt(0,100)
            testSeed2 = seed2.GetTestSeed()
            ts2_10 = testSeed1.GetNextInt(0,10)
            ts2_100 = testSeed1.GetNextInt(0,100)
            self.assertEqual(ts1_10, ts2_10)
            self.assertEqual(ts1_100, ts2_100)
            random.seed()
            
    def testRandomMasterSeed(self):
        seed = MasterSeed(10)
        self.assertEqual(len(seed.testSeeds), 10)
        for i in range(10):
            testSeed1 = seed.GetTestSeed()
        random.seed()

if __name__ == '__main__':
    unittest.main(verbosity=2)
