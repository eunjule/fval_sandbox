################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: testSeed.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the seed module
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/06/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest

import Common.fval.core as core


class CoreTests(unittest.TestCase):
    def test_remove_ansi_graphics_mode_escape_sequences(self):
        core.remove_ansi_graphics_mode_escape_sequences(None)
        
