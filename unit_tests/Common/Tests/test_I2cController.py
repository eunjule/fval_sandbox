# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import random
from random import sample
from unittest.mock import Mock, patch

from Common.fval import Object
from Rc3.instrument.generic_i2c_interface import GENERIC_GROUPS
from Rc3.instrument.rc3 import Rc3

UNUSED_I2C_ADDR = 0x20
from Common.testbench.i2c_tester import expected_vs_observed_msg, I2cTester, tx_fifo_error_msg


class FunctionalTests(Object):

    def validate_i2c_test_pass(self, i2c_test):
        i2c_test()
        self.validate_pass()

    def tx_fifo_count_error_bit_test_valid_count_fail(self, i2c_interfaces, i2c_test):
        with patch.object(I2cTester, 'get_transmit_fifo_count_error') as mock_get_transmit_fifo_count_error, \
                patch.object(I2cTester, 'send_msg') as mock_send_msg:
            mock_send_msg.return_value = [i2c_interfaces[0].i2c_addr, 0]  # Valid message of 2 bytes
            mock_get_transmit_fifo_count_error.return_value = 1  # Force a fail
            i2c_test()
            for device_num in range(len(i2c_interfaces)):
                self.validate_log_message(
                    expected_vs_observed_msg(name=i2c_interfaces[device_num].interface_name, expected=0, observed=1),
                    'error'
                )
            self.validate_fail()

    def tx_fifo_count_error_bit_test_invalid_count_fail(self, i2c_interfaces, i2c_test):
        with patch.object(I2cTester, 'get_transmit_fifo_count_error') as mock_get_transmit_fifo_count_error, \
                patch.object(I2cTester, 'send_msg') as mock_send_msg:
            mock_send_msg.return_value = [UNUSED_I2C_ADDR]
            mock_get_transmit_fifo_count_error.return_value = 0
            i2c_test()
            for device_num in range(len(i2c_interfaces)):
                self.validate_log_message(
                    expected_vs_observed_msg(name=i2c_interfaces[device_num].interface_name, expected=1, observed=0),
                    'error'
                )
            self.validate_fail()

    def address_nak_test_valid_address_fail(self, i2c_interfaces, i2c_test):
        with patch.object(I2cTester, 'get_address_nak') as mock_get_address_nak, \
                patch.object(I2cTester, 'send_msg') as mock_send_msg:
            mock_send_msg.return_value = [i2c_interfaces[0].i2c_addr, 0]
            mock_get_address_nak.return_value = 1
            i2c_test()
            for device_num in range(len(i2c_interfaces)):
                self.validate_log_message(
                    expected_vs_observed_msg(name=i2c_interfaces[device_num].interface_name, expected=0, observed=1),
                    'error'
                )
            self.validate_fail()

    def address_nak_test_invalid_address_fail(self, i2c_interfaces, i2c_test):
        with patch.object(I2cTester, 'get_address_nak') as mock_get_address_nak, \
                patch.object(I2cTester, 'send_msg') as mock_send_msg:
            mock_send_msg.return_value = [UNUSED_I2C_ADDR, 0]
            mock_get_address_nak.return_value = 0
            i2c_test()
            for device_num in range(len(i2c_interfaces)):
                self.validate_log_message(
                    expected_vs_observed_msg(name=i2c_interfaces[device_num].interface_name, expected=1, observed=0),
                    'error'
                )
            self.validate_fail()

    def tx_fifo_count_test_fail(self, i2c_interfaces, i2c_test):
        expected_values = [0, 10, 100]
        actual_value = 10

        with patch('Common.testbench.i2c_tester.generate_push_count_list') as mock_generate_push_count_list, \
                patch.object(I2cTester, 'get_transmit_fifo_count') as mock_get_transmit_fifo_count:
            mock_generate_push_count_list.return_value = expected_values
            mock_get_transmit_fifo_count.return_value = actual_value

            i2c_test()

            self.validate_log_message(
                expected_vs_observed_msg(name=i2c_interfaces[0].interface_name,
                                         expected=expected_values[0],
                                         observed=actual_value),
                'error'
            )
            self.validate_log_message(
                expected_vs_observed_msg(name=i2c_interfaces[0].interface_name,
                                         expected=expected_values[2],
                                         observed=actual_value),
                'error'
            )
            self.validate_fail()

    def validate_i2c_address_test_fail(self, i2c_interfaces, i2c_test):
        for interface in i2c_interfaces:
            with patch('Common.testbench.i2c_tester.get_valid_i2c_addresses') as mock_get_valid_i2c_addresses:
                random_i2c_address_list = sample(range(0x100), 4)
                mock_get_valid_i2c_addresses.return_value = random_i2c_address_list

                if interface.i2c_addr in random_i2c_address_list:
                    random_i2c_address_list.remove(interface.i2c_addr)

                i2c_test()

                observed = [f'0x{i:02X}' for i in random_i2c_address_list]
                self.validate_log_message(
                    expected_vs_observed_msg(
                        name=interface.interface_name,
                        expected=f'0x{interface.i2c_addr:02X}',
                        observed=observed),
                    'error'
                )
                self.validate_fail()


    def reset_tx_fifo_test_fail(self, i2c_interfaces, i2c_test):
        expected, observed = self.get_unequal_values(2, 2047)

        with patch('Common.testbench.i2c_tester.tx_fifo_data_list') as mock_tx_fifo_data_list, \
            patch('Common.testbench.i2c_tester.read_tx_fifo_count') as mock_read_tx_fifo_count:

            mock_tx_fifo_data_list.return_value = [expected]
            mock_read_tx_fifo_count.return_value = observed

            i2c_test()

            for interface in i2c_interfaces:
                for iteration in range(1):
                    self.validate_log_message(
                        tx_fifo_error_msg(iteration=iteration,
                                          group=interface.interface_index,
                                          expected=expected,
                                          observed=observed),
                        'error'
                    )
            self.validate_fail()

    def get_unequal_values(self, number_of_values, max, min=0):
            return random.sample(range(min, max), number_of_values)


    def validate_pass(self):
        for log_info, kwargs in self.Log.call_args_list:
            log_level, log_message = log_info
            assert log_level != 'error', f'ERROR: {log_message}'

    def validate_fail(self):
        for log_info, kwargs in self.Log.call_args_list:
            log_level, log_message = log_info
            if log_level == 'error':
                break
        else:
            raise AssertionError(f'Test did not fail')

    def validate_log_message(self, expected_message, expected_level):
        for log_info, kwargs in self.Log.call_args_list:
            log_level, log_message = log_info
            if (log_message == expected_message) and (log_level == expected_level):
                break
        else:
            raise AssertionError(f'"{expected_message} with log level of {expected_level}" not found.')
