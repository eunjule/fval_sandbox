################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
import unittest
from unittest.mock import Mock
from Common.Tests.s2h_base import S2HBase

class S2HBaseCommunicationTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def get_instrument_and_configure_s2h(self):
        buffer_size = 29999
        addr= 1020
        s2h_base = S2HBase()
        s2h_instrument = Mock(slot=1)
        s2h_base.env = Mock()
        s2h_base.Log = Mock()
        s2h_base.env.duts = [s2h_instrument]
        s2h_instrument.StreamingBufferaddrsize = Mock(return_value=[buffer_size,addr])
        s2h_instrument.S2HRegisterAndBufferSetup = Mock(return_value= [buffer_size,addr])
        s2h_instrument.WaitTime = Mock()
        s2h_instrument.IsS2HConfigurationDone = Mock(return_value=True)
        s2h_instrument.S2HSupport = Mock(return_value=True)
        return s2h_base,s2h_instrument

    def find_logcall_containing_string_and_correct_loglevel(self,log_calls,match_string,log_level):
        for call in log_calls:
            args, kwargs = call
            call_log_level, message = args
            if match_string in message:
                break
        else:
            self.fail('{} not found in any call'.format(match_string))
        self.assertEqual(call_log_level.upper(), log_level)


    def test_directed_s2h_enabled_through_trigger_failed_to_turn_off(self):
        total_packets_when_on = 750
        total_packets_when_off = 720
        total_packets_when_reset = 75
        s2h_base,s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[total_packets_when_on,total_packets_when_off,total_packets_when_reset])
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HTriggerONControlregOFFTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Turn OFF not successful'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls,match_string,'ERROR')

    def test_directed_s2h_enabled_through_trigger_failed_to_reset(self):
        total_packets_when_on = 750
        total_packets_when_off = 750
        total_packets_when_reset = 75
        s2h_base, s2h_instrument=self.get_instrument_and_configure_s2h()
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[total_packets_when_on,total_packets_when_off,total_packets_when_reset])
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HTriggerONControlregOFFTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Reset was not successful'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_control_register_failed_to_turn_off(self):
        total_packets_when_on = 750
        total_packets_when_off = 720
        total_packets_when_reset = 75
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[total_packets_when_on,total_packets_when_off,total_packets_when_reset])
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HControlregONTriggerOFFTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Turn OFF not successful'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_control_register_failed_to_reset(self):
        total_packets_when_on = 750
        total_packets_when_off = 750
        total_packets_when_reset = 75
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[total_packets_when_on,total_packets_when_off,total_packets_when_reset])
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HControlregONTriggerOFFTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Reset was not successful'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_control_register_packet_count_below_range(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.now = Mock(side_effect=[10, 140, 130])
        s2h_base.GetCount = Mock(return_value=29999)
        s2h_base.DirectedS2HPacketCountControlByRegisterTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Total packet count out of range when S2H control register used for S2H ON-OFF'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_control_register_packet_count_above_range(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.now = Mock(side_effect=[10, 140, 130])
        s2h_base.GetCount = Mock(return_value=30101)
        s2h_base.DirectedS2HPacketCountControlByRegisterTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Total packet count out of range when S2H control register used for S2H ON-OFF'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_control_register_packet_count_per_second_out_of_range(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.now = Mock(side_effect=[10, 140, 180])
        s2h_base.GetCount = Mock(return_value=30030)
        s2h_base.DirectedS2HPacketCountControlByRegisterTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packets per Second count out of range'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_trigger_packet_count_below_range(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.now = Mock(side_effect=[10, 140, 130])
        s2h_base.GetCount = Mock(return_value=29999)
        s2h_base.DirectedS2HPacketCountControlByTriggerTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Total packet count out of range when Trigger register used for S2H ON-OFF'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_trigger_packet_count_above_range(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.now = Mock(side_effect=[10, 140, 130])
        s2h_base.GetCount = Mock(return_value=30101)
        s2h_base.DirectedS2HPacketCountControlByTriggerTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Total packet count out of range when Trigger register used for S2H ON-OFF'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_directed_s2h_enabled_through_trigger_packet_count_per_second_out_of_range(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.now = Mock(side_effect=[10, 140, 180])
        s2h_base.GetCount = Mock(return_value=30030)
        s2h_base.DirectedS2HPacketCountControlByTriggerTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packets per Second count out of range'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_packet_counter_failed_to_reset(self):
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_base.no_of_iterations_for_packet_count_increment_test = 5
        s2h_instrument.GetPacketCount = Mock(return_value =500)
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[250, 500,350,1000,5000])
        no_of_iterations = 5
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HPacketCountIncrementTest(no_of_iterations)
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packet Counter failed to Reset'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_packet_counter_failed_to_increment(self):
        packet_count = 1000
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.GetPacketCount = Mock(return_value= packet_count)
        s2h_base.no_of_iterations_for_packet_count_increment_test = 5
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[250,500,350,350])
        no_of_iterations = 5
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HPacketCountIncrementTest(no_of_iterations)
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packet Counter failed to Increment'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_packet_counter_did_roll_over_exceeding_streaming_rate(self):
        max_packets_in_site_controller = 29999
        pkt_byte_count = 1020
        buffer_size = 12
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.ReadStreamingDetails = Mock(return_value=[max_packets_in_site_controller,pkt_byte_count,buffer_size])
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[250, 500, 350, 350])
        s2h_instrument.ReadPacketNumber = Mock(side_effect=[63752,3351])
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HPacketCountRolloverInPacketHeaderTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packet Count failed to rollover'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_packet_counter_did_not_roll_over_at_all(self):
        max_packets_in_site_controller = 29999
        pkt_byte_count = 1020
        buffer_size = 12
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.ReadStreamingDetails = Mock(return_value=[max_packets_in_site_controller,pkt_byte_count,buffer_size])
        s2h_instrument.GetTotalNoofPackets = Mock(side_effect=[250, 500, 350, 350])
        s2h_instrument.ReadPacketNumber = Mock(side_effect=[63752,63800])
        s2h_base.WaitTime = Mock()
        s2h_base.DirectedS2HPacketCountRolloverInPacketHeaderTest()
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packet Count failed to rollover'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_packet_index_register_failed_to_increment(self):
        max_packets_in_site_controller = 29999
        pkt_byte_count = 1020
        buffer_size = 12
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.WaitTime = Mock()
        s2h_instrument.ReadStreamingDetails = Mock(return_value=[max_packets_in_site_controller, pkt_byte_count, buffer_size])
        s2h_instrument.ReadPacketIndexandPacketHeader = Mock(return_value=[499,500])
        no_of_iterations = 1
        s2h_base.DirectedS2HPacketCountResetInPacketHeaderTest(no_of_iterations)
        log_calls = s2h_base.Log.call_args_list
        print(log_calls)
        match_string = 'Packet index register failed to increment'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')

    def test_packet_number_in_packet_header_failed_to_reset(self):
        max_packets_in_site_controller = 29999
        pkt_byte_count = 1020
        buffer_size = 12
        s2h_base, s2h_instrument = self.get_instrument_and_configure_s2h()
        s2h_instrument.WaitTime = Mock()
        s2h_instrument.ReadStreamingDetails = Mock(return_value=[max_packets_in_site_controller, pkt_byte_count, buffer_size])
        s2h_instrument.ReadPacketIndexandPacketHeader = Mock(side_effect=[[499,500], [500,750]])
        no_of_iterations = 1
        s2h_base.DirectedS2HPacketCountResetInPacketHeaderTest(no_of_iterations)
        log_calls = s2h_base.Log.call_args_list
        match_string = 'Packet number in packet header failed to reset'
        self.find_logcall_containing_string_and_correct_loglevel(log_calls, match_string, 'ERROR')