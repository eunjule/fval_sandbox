# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import logging
import os
from random import choice
from unittest.mock import Mock

from Common.fval import ConfigLogger, SetLoggerLevel, skip
from Common.instruments.base import Instrument
from Common.instruments.dps.hddps import Hddps
from Common.instruments.dps.hddps_trigger_interface import \
    create_interface as hddps_trigger_create_interface
from Common.instruments import tester
from Common.instruments.tester import get_tester
from Common.instruments.hdmt_trigger_interface import HdmtTriggerInterface
from Hpcc.instrument.ad9914DomainClock import Ad9914DomainClock
from Hpcc.instrument.hpcc import Hpcc
from Hpcc.instrument.hpcc_trigger_interface import \
    create_interface as hpcc_trigger_create_interface
from Hpcc.instrument.hpccac import HpccAc
from Rc3.instrument.rc3 import Rc3
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest

if os.getenv('DBG_LOGGING'):
    ConfigLogger()
    SetLoggerLevel(logging.INFO)

@skip('')
class HdmtTriggerInterfaceTests(Rc3UnitTest):
    def setUp(self):
        tester.get_tester()
        self.orig_get_tester = tester._tester

        self.rc = Rc3()
        self.hpcc = Hpcc(slot=2, rc=self.rc)
        self.hddps = Hddps(slot=10, rc=self.rc)

        self.rc.ensure_initialized = Mock()
        self.hpcc.ensure_initialized = Mock()
        self.hddps.ensure_initialized = Mock()

        self.tester = tester.Tester()
        self.tester.devices = [self.rc, self.hpcc, self.hddps]
        self.tester.rc = self.rc
        tester._tester =self.tester
        self.rc.update_aurora_link_slots()

    def tearDown(self):
        tester._tester = self.orig_get_tester
        HdmtTriggerInterface.clear_interfaces()

    def test_HpccTriggerInterface_create_interface_pass(self):
        interfaces = hpcc_trigger_create_interface(self.hpcc)
        self.assertIsNotNone(interfaces)

    def test_HpccTriggerInterface_create_interface_InstrumentError_fail(self):
        self.hpcc.ensure_initialized = Mock(
            side_effect=self._mock_raise_instrument_error)

        interfaces = None
        with self.assertRaises(Instrument.InstrumentError):
            interfaces = hpcc_trigger_create_interface(self.hpcc)

        self.assertIsNone(interfaces)

    def test_HpccTriggerInterface_create_interface_initialize_fail(self):
        self.hpcc.ensure_initialized = Mock(
            side_effect=self._mock_raise_hpcc_intialization_error)

        interfaces = None
        with self.assertRaises(Instrument.InstrumentError):
            interfaces = hpcc_trigger_create_interface(self.hpcc)

        self.assertIsNone(interfaces)

    def test_HddpsTriggerInterface_create_interface_pass(self):
        interfaces = hddps_trigger_create_interface(self.hddps)
        self.assertIsNotNone(interfaces)

    def test_HddpsTriggerInterface_create_interface_InstrumentError_fail(self):
        self.hddps.ensure_initialized = Mock(
            side_effect=self._mock_raise_instrument_error)

        interfaces = None
        with self.assertRaises(Instrument.InstrumentError):
            interfaces = hddps_trigger_create_interface(self.hddps)

        self.assertIsNone(interfaces)

    def test_HddpsTriggerInterface_create_interface_initialize_fail(self):
        self.hddps.ensure_initialized = Mock(
            side_effect=self._mock_raise_hddps_intialization_error)

        interfaces = None
        with self.assertRaises(Instrument.InstrumentError):
            interfaces = hddps_trigger_create_interface(self.hddps)

        self.assertIsNone(interfaces)

    def _mock_raise_hpcc_intialization_error(self):
        exception = choice([Hpcc.SpiAdjustReadTriggerError,
                            HpccAc.PatternRunError,
                            Ad9914DomainClock.SyncError,
                            Hpcc.InstrumentError])
        raise exception()

    def _mock_raise_instrument_error(self):
        raise Instrument.InstrumentError()

    def _mock_raise_hddps_intialization_error(self):
        exception = choice([Hddps.AuroraBusException,
                            Hddps.InstrumentError])
        raise exception()

    def test_HdmtTriggerInterface_get_interfaces_pass(self):
        interfaces = HdmtTriggerInterface.get_interfaces()
        devices = get_tester().devices

        self.assertEqual(len(interfaces), len(devices))

    def test_HdmtTriggerInterface_removing_unavailable_instrument(self):
        self.hddps.ensure_initialized = Mock(
            side_effect=self._mock_raise_instrument_error)

        interfaces = HdmtTriggerInterface.get_interfaces()
        devices = get_tester().devices

        self.assertEqual(len(interfaces), len(devices) - 1)
        self.assertTrue(self.hpcc.slot in self.rc.aurora_link_slots)
        self.assertFalse(self.hddps.slot in self.rc.aurora_link_slots)