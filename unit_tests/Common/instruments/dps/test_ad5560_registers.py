################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import ctypes

import Common.instruments.dps.ad5560_registers as ad5560_registers


class AD5560RegistersTests(unittest.TestCase):

    def test_no_ctype_register_bar_and_addresses_are_reused(self):
        seen = {}
        for RegisterType in ad5560_registers.get_register_types():
            if seen.get((RegisterType.ADDR, RegisterType.BAR)) is None:
                seen[(RegisterType.ADDR, RegisterType.BAR)] = RegisterType.__name__
            else:
                print(seen)
                self.fail('Address 0x{:04X} is shared by {} and {}'.format(RegisterType.ADDR, RegisterType.__name__,
                                                                           seen[(RegisterType.ADDR, RegisterType.BAR)]))

    def test_ctype_registers_are_four_bytes_in_size(self):
        for RegisterType in ad5560_registers.get_register_types():
            if ctypes.sizeof(RegisterType) != 4:
                self.fail('{} is not 4 bytes in size'.format(RegisterType.__name__))

    def test_all_16_bits_are_specified_in_ctype_registers(self):
        FIELD_WIDTH_SPECIFIER_INDEX = 2
        for RegisterType in ad5560_registers.get_register_types():
            if 16 != sum([x[FIELD_WIDTH_SPECIFIER_INDEX] for x in RegisterType._fields_]):
                self.fail('{} is not fully specified for all 16 bits'.format(RegisterType.__name__))

    def test_ctype_structs_are_four_bytes_in_size(self):
        for StructType in ad5560_registers.get_struct_types():
            if ctypes.sizeof(StructType) != 4:
                self.fail('{} is not 4 bytes in size'.format(StructType.__name__))

    def test_all_16_bits_are_specified_in_ctype_structs(self):
        FIELD_WIDTH_SPECIFIER_INDEX = 2
        for StructType in ad5560_registers.get_struct_types():
            if 16 != sum([x[FIELD_WIDTH_SPECIFIER_INDEX] for x in StructType._fields_]):
                self.fail('{} is not fully specified for all 16 bits'.format(StructType.__name__))
