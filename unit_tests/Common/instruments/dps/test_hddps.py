################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import sys
import unittest
from unittest.mock import patch
from unittest.mock import Mock

if __name__ == '__main__':
    repo_root_path = os.path.join(
        os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.insert(0, os.path.abspath(repo_root_path))

from Common import fval
from Common.instruments.dps.hddps import Hddps


class HddpsTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def test_Initialize(self):
        with patch('Common.instruments.dps.hddps.hil') as mock_hil, \
             patch('Common.instruments.dps.hddpsSubslot.hil') as\
                     mock_subslot_hil, \
             patch('Common.instruments.dps.dpsSubslot.hil') as \
                     mock_dpssubslot_hil, \
             patch('Common.instruments.dps.hddpsSubslot.time') as \
                     mock_subslot_time, \
             patch('ThirdParty.SASS.suppress_sensor.sass'):

            mock_subslot_hil.dpsFpgaVersion = Mock(return_value=0)
            mock_dpssubslot_hil.dpsBarRead = Mock(return_value=0)
            mock_subslot_hil.dpsDmaWrite = Mock(return_value=0)
            mock_subslot_time.sleep = lambda x: None
            hddps_instrument = Hddps(0, self.mock_rc)
            hddps_instrument.subslots[0].UnGangAllRails = Mock()
            hddps_instrument.subslots[1].UnGangAllRails = Mock()
            hddps_instrument.subslots[0].ClearTriggerQueue = Mock()
            hddps_instrument.subslots[1].ClearTriggerQueue = Mock()
            hddps_instrument.subslots[0].ClearGlobalAlarms = Mock()
            hddps_instrument.subslots[1].ClearGlobalAlarms = Mock()
            hddps_instrument.subslots[0].EnableAlarms = Mock()
            hddps_instrument.subslots[1].EnableAlarms = Mock()
            hddps_instrument.subslots[0].SetRailsToSafeState = Mock()
            hddps_instrument.subslots[1].SetRailsToSafeState = Mock()
            hddps_instrument.subslots[0].DisableAllUhcs = Mock()
            hddps_instrument.subslots[1].DisableAllUhcs= Mock()
            hddps_instrument.train_aurora_link = Mock()
            hddps_instrument.validate_aurora_bus = Mock()
            hddps_instrument.Initialize()

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
