################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random
from Common import fval
from unittest.mock import patch
from unittest.mock import call
from unittest.mock import Mock
from Common.instruments.dps import symbols
from Common.instruments.dps.dpsSubslot import DpsSubslot
from Common.instruments.dps.hddps import Hddps
import Common.instruments.dps.hvil_registers as hvilregs
from Common.instruments.dps import hddps_registers
from Common.instruments.dps.hvilSubslot import HvilSubslot
from Common.register import create_field_dictionary
from _build.bin import hddpstbc
from Common.instruments.dps.hddpsSubslot import HddpsSubslot



if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


class DpsSubslotTests(unittest.TestCase):

    @unittest.skip('Skipping while doing register read switch over')
    def test_read_register(self):
        BAR = 1
        registerName = hvilregs.MAX6627_LOWER_TEMP_LIMIT

        # with patch('Common.instruments.dps.hvil.hil') as mock_subslot_hil:
        hvil_instrument = HvilSubslot(hvdps=Mock(), slot=6, subslot=1)
        hvil_instrument.BarRead = Mock(return_value = 0x344)
        hvil_instrument.ReadRegister(registerName,1)
        hvil_instrument.BarRead.assert_called_once_with(BAR, hvilregs.MAX6627_LOWER_TEMP_LIMIT.address(1))


    def test_write_register(self):
        BAR = 1
        dummyValue = 0x1234
        registerName = hvilregs.MAX6627_LOWER_TEMP_LIMIT
        registerName.value = dummyValue

        # with patch('Common.instruments.dps.hvil.hil') as mock_subslot_hil:
        hvil_instrument = HvilSubslot(hvdps= Mock(), slot=6, subslot=1)
        hvil_instrument.BarWrite = Mock()
        hvil_instrument.WriteRegister(registerName,1)
        hvil_instrument.BarWrite.assert_called_once_with(BAR, hvilregs.MAX6627_LOWER_TEMP_LIMIT.address(1),dummyValue)


    def test_ClearHclcRailAlarm(self):
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        clearPerRailHclcAlarmsRegister_call_count = 1
        dps_instrument.clearPerRailHclcAlarmsRegister = Mock()
        dps_instrument.ClearHclcRailAlarms()
        self.assertEqual(dps_instrument.clearPerRailHclcAlarmsRegister.call_count,clearPerRailHclcAlarmsRegister_call_count)

    def test_ClearGlobalAlarms(self):
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        clearGlobalAlarmRegister_call_count = 1
        dps_instrument.clearGlobalAlarmRegister = Mock()
        dps_instrument.ClearGlobalAlarms()
        self.assertEqual(dps_instrument.clearGlobalAlarmRegister.call_count,clearGlobalAlarmRegister_call_count)

    def test_ClearHclcSampleAlarms(self):
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        clearHclcSampleAlarmRegister_call_count = 1
        dps_instrument.clearHclcSampleAlarmRegister = Mock()
        dps_instrument.ClearHclcSampleAlarms()
        self.assertEqual(dps_instrument.clearHclcSampleAlarmRegister.call_count, clearHclcSampleAlarmRegister_call_count)

    def test_ClearHclcCfoldAlarms(self):
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        clearHclcCfoldAlarmsRegister_call_count = 1
        dps_instrument.clearHclcCfoldAlarmsRegister = Mock()
        dps_instrument.ClearHclcCfoldAlarms()
        self.assertEqual(dps_instrument.clearHclcCfoldAlarmsRegister.call_count,clearHclcCfoldAlarmsRegister_call_count)

    def test_ClearAllDutDomainIds(self):
        dps_subslot = DpsSubslot(slot = 1, subslot = 1)
        dps_subslot.WriteRegister = Mock()
        dps_subslot.getDutDomainIdRegisterType = Mock(side_effect=[Mock() for x in range(8)])
        dps_subslot.DisableAllUhcs()
        writeRegister_calls = dps_subslot.WriteRegister.call_args_list
        dut_id = 0
        for args, kwargs in writeRegister_calls:
            RegisterObject = args[0]
            print(RegisterObject.DutDomainId)
            self.assertEqual(RegisterObject.DutDomainId,dut_id)
            self.assertEqual(RegisterObject.DutValidBit,0)

    def test_SetRailsToSafeState(self):
        dps_subslot = DpsSubslot(slot = 1, subslot= 1)
        dps_subslot.WriteRegister = Mock()
        dps_subslot.getSafeStateRegister = Mock()
        dps_subslot.SetRailsToSafeState()
        writeRegister_calls = dps_subslot.WriteRegister.call_args_list
        for args, kwargs in writeRegister_calls:
            RegisterObject = args[0]
            self.assertEqual(RegisterObject.RailSafeBit,1)

    def test_ResetDdr_Pass(self):
        dps_instrument = DpsSubslot(slot = 6, subslot= 0)
        dps_instrument.getResetsRegister = Mock()
        dps_instrument.getStatusRegister = Mock()
        dps_instrument.ReadRegister = Mock(side_effect=[Mock(DDRControllerPLLLock=1,DDRCalibrationDone=1),
                                                        Mock(DDRControllerPLLLock=0,DDRCalibrationDone=1),
                                                        Mock(DDRControllerPLLLock=1, DDRCalibrationDone=0),
                                                        Mock(DDRControllerPLLLock=0, DDRCalibrationDone=0)])
        dps_instrument.Log = Mock()
        dps_instrument.WriteRegister = Mock()
        dps_instrument.ResetDdr()
        log_calls = dps_instrument.Log.call_args_list
        warning_message_count = 0
        for call in log_calls:
            loglevel,message = call[0]
            if loglevel == 'warning':
                warning_message_count = warning_message_count + 1
        expected_warning_message_count = 4
        self.assertEqual(warning_message_count,expected_warning_message_count)

    def test_ResetDdr_Fail(self):
        dps_instrument = DpsSubslot(slot = 6, subslot= 0)
        dps_instrument.getResetsRegister = Mock()
        dps_instrument.getStatusRegister = Mock()
        dps_instrument.ReadRegister = Mock(side_effect=[Mock(DDRControllerPLLLock=1,DDRCalibrationDone=1),
                                                        Mock(DDRControllerPLLLock=0,DDRCalibrationDone=1),
                                                        Mock(DDRControllerPLLLock=1, DDRCalibrationDone=0),
                                                        Mock(DDRControllerPLLLock=1, DDRCalibrationDone=0)])
        dps_instrument.WriteRegister = Mock()
        with self.assertRaises(fval.LoggedError):
            dps_instrument.ResetDdr(numberOfTries=4)


    @unittest.skip('Skipping Until we update register handling')
    def test_check_global_alarm_register(self):
        global_alarm = hvdps_regs.GLOBAL_ALARMS()
        global_alarm.value = 0x0000
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        dps_instrument.ReadObject = Mock(return_value=global_alarm)
        dps_instrument.CheckAlarmRegisters()

    @unittest.skip('Skipping Until we update register handling')
    def test_check_global_alarm_register_notclear(self):
        global_alarm = hvdps_regs.GLOBAL_ALARMS()
        global_alarm.value = 0x0001
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        dps_instrument.ReadObject = Mock(return_value=global_alarm)
        with self.assertRaises(fval.LoggedError):
            dps_instrument.CheckAlarmRegisters()


    def test_clear_trigger_queue(self):
        offset_less_than_max_memory = 0
        with patch('Common.instruments.dps.hvdpsSubslot.hil') as mock_subslot_hil:
            dps_instrument = DpsSubslot(slot=6, subslot=0)
            dps_instrument.offset = offset_less_than_max_memory
            dps_instrument.TOTAL_MEMORY_SIZE = 512 * 1024 * 1024
            dps_instrument.DmaWrite = Mock()
            dps_instrument.ClearTriggerQueue()

    def test_not_clear_trigger_queue(self):
        offset_less_than_max_memory = 12344
        with patch('Common.instruments.dps.hvdpsSubslot.hil') as mock_subslot_hil:
            dps_instrument = DpsSubslot(slot=6, subslot=0)
            dps_instrument.offset = offset_less_than_max_memory
            dps_instrument.TOTAL_MEMORY_SIZE = 512 * 1024 * 1024
            dps_instrument.DmaWrite = Mock()
            with self.assertRaises(fval.LoggedError):
                dps_instrument.ClearTriggerQueue()

    def test_CheckGlobalAlarms_no_alarms(self):
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        dps_instrument.ReadRegister = Mock(return_value=Mock(value =0x0))
        dps_instrument.getGlobalAlarmRegisterType = Mock()
        dps_instrument.GetMoreAlarmDetails = Mock()
        result = dps_instrument.CheckGlobalAlarms()
        self.assertEqual(dps_instrument.GetMoreAlarmDetails.call_count, 0)
        self.assertEqual(result,False)

    def test_CheckGlobalAlarms_alarms(self):
        dps_instrument = DpsSubslot(slot=6, subslot=0)
        dps_instrument.ReadRegister = Mock(return_value=Mock(value =0x680))
        dps_instrument.GetMoreAlarmDetails = Mock()
        dps_instrument.getGlobalAlarmRegisterType = Mock()
        result = dps_instrument.CheckGlobalAlarms()
        self.assertEqual(dps_instrument.GetMoreAlarmDetails.call_count,1)
        self.assertEqual(result,True)

    def test_GetMoreAlarmDetails_alarms(self):
        global_alarm_register = hddps_registers.GLOBAL_ALARMS()
        global_alarm_register.value = random.randint(0, 0xffffffff)
        dict = create_field_dictionary(global_alarm_register)
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        dps_subslot.Log = Mock()
        dps_subslot.GetMoreAlarmDetails(global_alarm_register)
        log_calls = dps_subslot.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            for name, status in dict.items():
                if status != 0:
                    self.assertRegex(log_message, name)
            self.assertEqual(log_level.upper(), 'WARNING', 'Log level should be warning')

    def test_GetMoreAlarmDetails_noalarms(self):
        global_alarm_register = hddps_registers.GLOBAL_ALARMS()
        global_alarm_register.value = 0
        dict = create_field_dictionary(global_alarm_register)
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        dps_subslot.Log = Mock()
        dps_subslot.GetMoreAlarmDetails(global_alarm_register)
        log_calls = dps_subslot.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            for name, status in dict.items():
                if status != 0:
                    self.assertRegex(log_message, name)
            self.assertEqual(log_level.upper(), 'WARNING', 'Log level should be warning')

    def test_MAX6627TemperatureConversionDecimalToCelcius(self):
        temperature_40celcius_in_decimal = 640
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        result = dps_subslot.MAX6627TemperatureDecimalToCelcius(temperature_40celcius_in_decimal)
        self.assertEqual(result,40.0)

    def test_decode_sfp_to_current(self):
        for i in range(12):
            current_in_sfp_format = [65504,32736,56836, 20739, 10, 49158, 17155, 57350, 23556, 19203, 23556, 20487]
            current_in_amps_format = [-1023.0,1023.0,-47.0,81.0,0.0,-8.0,67.0,-12.0,46.0,75.0,46.0,5.0]
            dps_subslot = DpsSubslot(slot=6, subslot=0)
            current_converted_from_fval = dps_subslot.decode_sfp_to_current_in_amps(current_in_sfp_format[i])
            self.assertEqual(current_converted_from_fval, current_in_amps_format[i])

    def test_decode_x1_to_current(self):
        for i in range(10):
            current_in_x1_format = [0x500b, 0xe3e7, 0xdbe9, 0xd3eb, 0xbbf1, 0xe3e7, 0xe3e7, 0xdfe8, 0xa7f6, 0xbbf1,
                                    0xe3e7]
            current_in_amps_format = [-3.0002, 0.0000, 0.0000, 0.0002, 0.0015, 0.025, 0.5, 1.2, 10, 15, 25]
            dps_subslot = DpsSubslot(slot=6, subslot=0)
            current_converted_from_fval = dps_subslot.decode_x1_to_current_in_amps(current_in_x1_format[i],i)
            converted_to_precision_15 = float('{:.4f}'.format(current_converted_from_fval))
            self.assertEqual(converted_to_precision_15, current_in_amps_format[i])

    def test_encode_current_in_amps_to_sfp(self):
        for i in range(12):
            current_in_sfp_format = [65504, 32736, 56836, 20739, 10, 49158, 17155, 57350, 23556, 19203, 23556, 20487]
            current_in_amps_format = [-1023.0, 1023.0, -47.0, 81.0, 0.0, -8.0, 67.0, -12.0, 46.0, 75.0, 46.0, 5.0]
            dps_subslot = DpsSubslot(slot=6, subslot=0)
            current_converted_from_fval = dps_subslot.encode_current_in_amps_to_sfp(current_in_amps_format[i])
            self.assertEqual(current_converted_from_fval, current_in_sfp_format[i])

    def test_encode_current_in_amps_to_x1(self):
        for i in range(10):
            current_in_amps_format = [-3, 0.0000049999999999999996, 0.000022999999999999998, 0.00021, 0.0015, 0.025,
                                      0.5, 1.2, 10, 15, 25]
            current_in_x1_format = [0x500b, 0xe3e7, 0xdbe9, 0xd3eb, 0xbbf1, 0xe3e7, 0xe3e7, 0xdfe8, 0xa7f6, 0xbbf1,
                                    0xe3e7]
            dps_subslot = DpsSubslot(slot=6, subslot=0)
            current_converted_from_fval = dps_subslot.encode_current_in_amps_to_x1(current_in_amps_format[i],i)
            self.assertEqual(current_converted_from_fval, current_in_x1_format[i])

    def test_is_close(self):
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        expected = 5
        fudge_factor = 20
        values = [4,6,5.5,4.5,5]
        for v in values:
            self.assertTrue(dps_subslot.is_close(expected, v, fudge_factor))
        values = [3.9,6.1]
        for v in values:
            self.assertFalse(dps_subslot.is_close(expected, v, fudge_factor))

        value = 5
        expected = 5
        fudge_factor = 0
        self.assertTrue(dps_subslot.is_close(expected, value, fudge_factor))

        value = -1.55
        expected = -1.5
        fudge_factor = 20
        self.assertTrue(dps_subslot.is_close(expected, value, fudge_factor))

    def test_current_to_ad5560_raw_dac_code(self):
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        i_range = 'I_500_MA'
        dps_subslot.AD5560_MI_AMP_GAIN = 20.0
        current_i_range_500_ma = [0.5, -0.5, 0.25, -0.25, 0]
        expected_converted_raw = [0xe3e7, 0x1c18, 0xb1f3, 0x4e0c, 0x8000]
        for i in range(5):
            raw_value = dps_subslot.i_clamp_current_to_ad5560_raw_dac_code(current_i_range_500_ma[i], i_range)
            self.assertEqual(raw_value,expected_converted_raw[i])
        i_range = 'I_25000_MA'
        dps_subslot.AD5560_MI_AMP_GAIN = 20.0
        current_i_range_25000_ma = [5, -5, 15, 25]
        expected_converted_raw = [0x93fb, 0x6c04, 0xbbf1, 0xe3e7]
        for i in range(4):
            raw_value = dps_subslot.i_clamp_current_to_ad5560_raw_dac_code(current_i_range_25000_ma[i], i_range)
            self.assertEqual(raw_value, expected_converted_raw[i])

    def test_is_raw_conversion_in_expected_range(self):
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        out_of_range = 0xFFFFF
        invalid_negative = 0x8000
        invalid_positive = 0x7FFF
        random_positive_current = 2
        random_negative_current = -2
        with self.assertRaises(fval.LoggedError):
            dps_subslot.is_raw_conversion_in_expected_range(out_of_range, random_positive_current)
        with self.assertRaises(fval.LoggedError):
            dps_subslot.is_raw_conversion_in_expected_range(invalid_negative, random_negative_current)
        with self.assertRaises(fval.LoggedError):
            dps_subslot.is_raw_conversion_in_expected_range(invalid_positive, random_positive_current)

    def test_get_positive_random_voltage(self):
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        low_voltage = 0
        high_voltage = 5
        step_size = 0.25
        for i in range(50):
            positive_voltage = dps_subslot.get_positive_random_voltage(low_voltage, high_voltage, step_size)
            self.assertLessEqual(positive_voltage, high_voltage)
            self.assertGreaterEqual(positive_voltage, low_voltage)

    def test_get_negative_random_voltage(self):
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        low_voltage = -2
        high_voltage = 0
        step_size = 0.25
        for i in range(50):
            negative_voltage = dps_subslot.get_negative_random_voltage(low_voltage, high_voltage, step_size)
            self.assertLessEqual(negative_voltage, high_voltage)
            self.assertGreaterEqual(negative_voltage, low_voltage)

    def test_actual_tracking_voltage_is_close_to_volt_variation_pass(self):
        expected_voltage = 12.00
        actual_voltage = 12.40
        voltage_variation = 0.5
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        result = dps_subslot.actual_tracking_voltage_is_close_to_volt_variation(expected_voltage, actual_voltage, voltage_variation)
        self.assertEqual(result, True)

    def test_actual_tracking_voltage_is_close_to_volt_variation_fail(self):
        expected_voltage = 12.00
        actual_voltage = 12.60
        voltage_variation = 0.5
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        result = dps_subslot.actual_tracking_voltage_is_close_to_volt_variation(expected_voltage, actual_voltage, voltage_variation)
        self.assertEqual(result, False)

    def test_actual_tracking_voltage_is_close_to_volt_variation_when_expected_voltage_is_zero(self):
        expected_voltage = 0
        actual_voltage = 0.1
        voltage_variation = 1
        dps_subslot = DpsSubslot(slot=6, subslot=0)
        result = dps_subslot.actual_tracking_voltage_is_close_to_volt_variation(expected_voltage, actual_voltage, voltage_variation)
        self.assertEqual(result, True)

    def test_when_expected_alarm_received(self):
        rail = 5
        myhddps = Hddps(slot=2,rc=Mock())
        hddps_subslot = HddpsSubslot(hddps=myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        HCLC_ALARMS = hddps_subslot.registers.HCLC_ALARMS(value=0x1<<rail)
        HCLC_PER_RAIL_ALARMS = hddps_subslot.registers.HCLC_PER_RAIL_ALARMS(ComparatorHighAlarm=1)
        hddps_subslot.ReadRegister = Mock(side_effect=[HCLC_ALARMS, HCLC_PER_RAIL_ALARMS])
        return_value = hddps_subslot.check_for_single_hclc_rail_alarm(rail,'ComparatorHighAlarm')
        self.assertEqual(return_value,True)

    def test_when_unexpected_global_alarm_received(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(hddps=myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['TrigExecAlarm'])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_single_hclc_rail_alarm(rail,'ComparatorHighAlarm')
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'Expected only \'HclcRailAlarms\' in GLOBAL_ALARMS to be set, actual fields set',
                             'Log not displaying correct information')

    def test_when_unexpected_alarm_set_in_hclc_rail_alarm_register(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        HCLC_ALARMS = hddps_subslot.registers.HCLC_ALARMS(value=0x1 << rail)
        HCLC_PER_RAIL_ALARMS = hddps_subslot.registers.HCLC_PER_RAIL_ALARMS(ComparatorLowAlarm=1)
        hddps_subslot.ReadRegister = Mock(side_effect=[HCLC_ALARMS,HCLC_PER_RAIL_ALARMS])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_single_hclc_rail_alarm(rail,'ComparatorHighAlarm')
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'specific alarm register, actual alarms set were',
                             'Log not displaying correct information')

    def test_when_unexpected_rail_bit_set_in_hclc_alarm_register(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(hddps=myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        HCLC_ALARMS = hddps_subslot.registers.HCLC_ALARMS(value=0x1 << rail+1)
        hddps_subslot.ReadRegister = Mock(side_effect=[HCLC_ALARMS])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_single_hclc_rail_alarm(rail,'ComparatorHighAlarm')
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'bit to be set in HCLC_alarms register, but got the following:',
                             'Log not displaying correct information')

    def test_when_unexpected_global_alarm_received_insted_of_cfold(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(hddps=myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['TrigExecAlarm'])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_hclc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'Expected only \'HclcCFoldAlarms\' in GLOBAL_ALARMS to be set, actual fields set:',
                             'Log not displaying correct information')

    def test_when_cfold_and_assert_data_same(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['HclcCFoldAlarms'])
        HCLC_CFOLD_ALARMS = hddps_subslot.registers.HclcCfoldAlarms(value=0x1 << rail)
        HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG = hddps_subslot.registers.HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(AssertTestData=1,CFoldActualData=1)
        hddps_subslot.ReadRegister = Mock(side_effect=[HCLC_CFOLD_ALARMS,HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_hclc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'HCLC CFoldActualData shouldn\'t be same as AssertTestData. Actual -> ',
                             'Log not displaying correct information')

    def test_when_cfold_and_assert_data_different(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['HclcCFoldAlarms'])
        HCLC_CFOLD_ALARMS = hddps_subslot.registers.HclcCfoldAlarms(value=0x1 << rail)
        HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG = hddps_subslot.registers.HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(AssertTestData=1,CFoldActualData=2)
        hddps_subslot.ReadRegister = Mock(side_effect=[HCLC_CFOLD_ALARMS,HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_hclc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, True)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'DEBUG')
            self.assertRegex(log_message,
                             'HclcCfoldData register is set to value -> ',
                             'Log not displaying correct information')

    def test_when_hclc_cfold_has_unexpected_bit_set(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['HclcCFoldAlarms'])
        HCLC_CFOLD_ALARMS = hddps_subslot.registers.HclcCfoldAlarms(value=0x1 << rail+1)
        HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG = hddps_subslot.registers.HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(AssertTestData=1,CFoldActualData=2)
        hddps_subslot.ReadRegister = Mock(side_effect=[HCLC_CFOLD_ALARMS,HCLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_hclc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'Expected only Rail 5 bit to be set in HCLC_control_fold_alarms register, but got the',
                             'Log not displaying correct information')

    def test_when_unexpected_global_alarm_received_insted_of_vlc_cfold(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(hddps=myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['TrigExecAlarm'])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_vlc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'Expected only \'VlcCFoldAlarms\' in GLOBAL_ALARMS to be set, actual fields set:',
                             'Log not displaying correct information')

    def test_when_cfold_and_assert_data_same_for_vlc(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['VlcCFoldAlarms'])
        VLC_CFOLD_ALARMS = hddps_subslot.registers.VlcCfoldAlarms(value=0x1 << rail)
        VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG = hddps_subslot.registers.VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(
            AssertTestData=1, CFoldActualData=1)
        hddps_subslot.ReadRegister = Mock(side_effect=[VLC_CFOLD_ALARMS, VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_vlc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'VLC CFoldActualData shouldn\'t be same as AssertTestData. Actual -> ',
                             'Log not displaying correct information')

    def test_when_cfold_and_assert_data_different_for_vlc(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['VlcCFoldAlarms'])
        VLC_CFOLD_ALARMS = hddps_subslot.registers.VlcCfoldAlarms(value=0x1 << rail)
        VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG = hddps_subslot.registers.VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(
            AssertTestData=1, CFoldActualData=2)
        hddps_subslot.ReadRegister = Mock(side_effect=[VLC_CFOLD_ALARMS, VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_vlc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, True)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'DEBUG')
            self.assertRegex(log_message,
                             'VlcCfoldData register is set to value -> 0x',
                             'Log not displaying correct information')

    def test_when_vlc_cfold_has_unexpected_bit_set(self):
        rail = 5
        myhddps = Hddps(slot=2, rc=Mock())
        hddps_subslot = HddpsSubslot(myhddps, slot=2, subslot=0, rc=Mock())
        hddps_subslot.get_global_alarms = Mock(return_value=['VlcCFoldAlarms'])
        VLC_CFOLD_ALARMS = hddps_subslot.registers.VlcCfoldAlarms(value=0x1 << rail+1)
        VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG = hddps_subslot.registers.VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS(
            AssertTestData=1, CFoldActualData=2)
        hddps_subslot.ReadRegister = Mock(side_effect=[VLC_CFOLD_ALARMS, VLC_PER_RAIL_CONTROLLED_FOLD_DETAILS_REG])
        hddps_subslot.Log = Mock()
        return_status = hddps_subslot.check_for_vlc_cfold_actual_and_assert_data(rail)
        log_calls = hddps_subslot.Log.call_args_list
        self.assertEqual(return_status, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'Expected only Rail 5 bit to be set in HCLC_control_fold_alarms register, but got the',
                             'Log not displaying correct information')

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)