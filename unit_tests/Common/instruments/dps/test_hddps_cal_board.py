################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import os
import random
import sys
import unittest
from unittest.mock import patch
from unittest.mock import Mock

if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.insert(0, os.path.abspath(repo_root_path))

from Common import fval
from Common import hilmon as hil
from Common.instruments.dps import hddps_cal_board


class HddpsTests(unittest.TestCase):
    def test_Initialize(self):
        with patch('Common.instruments.dps.hddps_cal_board.hil') as mock_hil:
            cal_board = hddps_cal_board.hddps_cal_board(0)
            del cal_board
    

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
