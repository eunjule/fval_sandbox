################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch, call, Mock
from Common import fval
#from ThirdParty import HAL
import Common.instruments.dps.hvilSubslot as hvil
from Common.instruments.dps import hvil_registers as hvilregs
#from ThirdParty.HAL import dps as dps
import random


class HvilTests(unittest.TestCase):

    def test_linearize(self):
        self.assertEqual(hvil.encode_clamp_current(0), 0)
        self.assertEqual(hvil.encode_clamp_current(29.88), 32767)
        self.assertEqual(hvil.encode_clamp_current(-29.88), 32768)
        self.assertEqual(hvil.encode_clamp_current(10), 10966)
        self.assertEqual(hvil.encode_clamp_current(-10), 54569)
        self.assertEqual(hvil.encode_clamp_current(0.1), 109)
        self.assertEqual(hvil.encode_clamp_current(-0.1), 65426)
        self.assertEqual(hvil.encode_clamp_current(-0.010), 65525)
        self.assertEqual(hvil.encode_clamp_current(0.001), 1)
        self.assertEqual(hvil.encode_clamp_current(-0.001), 65534)
        with self.assertRaises(hvil.ClampValueError):
            hvil.encode_clamp_current(29.89)
        with self.assertRaises(hvil.ClampValueError):
            hvil.encode_clamp_current(-29.89)

    def test_SetCurrentClampLimit_ValuesWrittenToCorrectRegisterAddress(self):
        with patch('Common.instruments.dps.dpsSubslot.hil') as mock_hil:
            my_hvil = hvil.HvilSubslot(hvdps = Mock(), slot=1, subslot=1)
            my_hvil.SetCurrentClampLimit(amps = 0.001)
            actual_register_addr = []
            write_register_offset = []
            mock_hil.dpsBarRead = Mock(side_effect=[0, 0, 0, 0, 0, 0, 0, 0])
            channel_user_current_clamp = hvilregs.CHANNEL_USER_CURRENT_CLAMP_LIMIT()
            for i in range(channel_user_current_clamp.REGCOUNT):
                actual_register_addr.append(1<<17| channel_user_current_clamp.ADDR)
                channel_user_current_clamp.ADDR = channel_user_current_clamp.ADDR + channel_user_current_clamp.BASEMUL
            for args, kwargs in mock_hil.dpsBarWrite.call_args_list:
                slot,subslot,bar,offset,data = args
                write_register_offset.append(offset)
            self.assertEqual(actual_register_addr, write_register_offset)


    def test_SetCurrentClampLimit_CorrectValueofCurrentIsWrittenInAllRegisters(self):
        my_hvil = hvil.HvilSubslot(hvdps = Mock(), slot=1, subslot=1)
        my_hvil.WriteRegister = Mock()
        my_hvil.SetCurrentClampLimit(amps=0.001)
        actual_register_values = []
        list_of_index_values = []
        expected_list_of_index_values = [0,1,2,3,4,5,6,7]
        expected_register_values = [1] * 8
        print(my_hvil.WriteRegister.call_args_list)
        for args, kwargs in my_hvil.WriteRegister.call_args_list:
            register_object = args
            index = kwargs
            actual_register_values.append(register_object[0].value)
            for keys,values in index.items():
                list_of_index_values.append(values)
                self.assertIs(type((values)), int)
        self.assertEqual(list_of_index_values, expected_list_of_index_values)
        self.assertEqual(expected_register_values, actual_register_values)


    def test_SetVoltageComparatorLimits(self):
        with patch('Common.instruments.dps.hvilSubslot.hil') as mock_hil:
            write_call_count = 1
            expected_slot = 0
            my_hvil = hvil.HvilSubslot(hvdps = Mock(), slot=expected_slot, subslot=1)
            mock_hil.hvilVoltageComparatorLimitsSet = Mock()
            expected_low_voltage_limit = 0.5
            expected_upper_voltage_limit = 2
            my_hvil.Log = Mock()
            my_hvil.SetVoltageComparatorLimits(voltage_low_limit=expected_low_voltage_limit,
                                               voltage_high_limit=expected_upper_voltage_limit)
            self.assertEqual(mock_hil.hvilVoltageComparatorLimitsSet.call_count, write_call_count)
            for args, kwargs in mock_hil.hvilVoltageComparatorLimitsSet.call_args_list:
                slot,voltage_low_limit,voltage_high_limit = args
                self.assertEqual(slot,expected_slot)
                self.assertEqual(voltage_low_limit,expected_low_voltage_limit)
                self.assertEqual(voltage_high_limit,expected_upper_voltage_limit)


    def test_ResetTemperatureLimitForMAX6627(self):
        with patch('Common.instruments.dps.hvilSubslot.hil') as mock_hil:
            write_call_count = 6
            expected_slot = 6
            my_hvil = hvil.HvilSubslot(hvdps = Mock(), slot=expected_slot, subslot=0)
            mock_hil.hvilMax6627LimitsSet = Mock()
            my_hvil.Log= Mock()
            expected_lower_temperature = 20
            expected_upper_temperature = 40
            my_hvil.SetTemperatureLimitForAllMAX6627(low_temperature=expected_lower_temperature,
                                                     high_temperature=expected_upper_temperature)
            self.assertEqual(mock_hil.hvilMax6627LimitsSet.call_count, write_call_count)
            expected_index  = 0
            for args, kwargs in  mock_hil.hvilMax6627LimitsSet.call_args_list:
                slot, index, voltage_low_limit, voltage_high_limit = args
                self.assertEqual(expected_index, index )
                self.assertIs(type(slot),int)
                self.assertEqual(slot, expected_slot)
                self.assertIs(type(index),int)
                self.assertEqual(voltage_low_limit, expected_lower_temperature)
                self.assertEqual(voltage_high_limit, expected_upper_temperature)
                expected_index += 1

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
