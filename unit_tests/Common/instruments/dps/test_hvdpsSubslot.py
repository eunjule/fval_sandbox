################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random
from unittest.mock import patch, call, Mock
from Common import fval
from Common.instruments.dps import hvdpsCombo
from Common.instruments.dps.hvdpsSubslot import HvdpsSubslot
from Common.instruments.dps import hvdps_registers as hvdpsregs
from Common.instruments.dps import ad5560_registers
import Common.register as register

class HvdpsSubslotTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None


    def test_ad5560_register_setup_functions(self):
        #(register setup function,registername,expected value)
        ad5560_register_init_functions = [('ad5560DpsReg1Initialize','DPS_REGISTER1',0x2F20),
                                          ('ad5560DpsReg2Initialize','DPS_REGISTER2', 0xE380),
                                          ('ad5560SysControlInitInitialize','SYSTEM_CONTROL',0xE200),
                                          ('ad5560Compensation1Initialize','COMPENSATION1',0x0000),
                                          ('ad5560Compensation2Initialize','COMPENSATION2',0x0110),
                                          ('ad5560AlarmSetupInitialize','ALARM_SETUP',0x01540)]

        for functionName,registerName,expectedValue in ad5560_register_init_functions:
            hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            ad5560RegInitFunc = getattr(hvdps_subslot,functionName)
            ad5560RegInit = ad5560RegInitFunc(getattr(ad5560_registers,registerName))
            self.assertEqual(ad5560RegInit.value, expectedValue,
                         'Expecting register {} to be: 0x{:02X}, getting: 0x{:02X}'.
                         format(registerName, expectedValue,ad5560RegInit.value))

    @unittest.skip('Need to rewrite for new hil based functionality')
    def test_ResetThermalAlarmLimitForMAX6627Pass(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.ReadRegister = Mock()
        hvdps_subslot.WriteRegister = Mock()
        hvdps_subslot.ResetThermalAlarmLimitForMAX6627()
        write_calls = hvdps_subslot.WriteRegister.call_args_list
        upper_temperature_limit = 0x370
        lower_temperature_limit = 0xF0
        write_expected_call_cnt = 6
        write_actual_call_cnt = hvdps_subslot.WriteRegister.call_count
        self.assertEqual(write_expected_call_cnt,write_actual_call_cnt)
        expected_register_values = [upper_temperature_limit,lower_temperature_limit]*3
        actual_register_values = []
        for register, index in write_calls:
           actual_register_values.append(register[0].value)
        self.assertEqual(expected_register_values,actual_register_values)


    def test_ungang_all_hvdps_rails(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.ReadRegister = Mock()
        hvdps_subslot.WriteRegister = Mock()
        hvdps_subslot.UnGangAllHvdpsRails()
        write_expected_call_count = 4
        expected_register_values = [0xFF,0x0,0x0,0xFF]
        self.assertEqual(hvdps_subslot.WriteRegister.call_count,write_expected_call_count)
        write_calls = hvdps_subslot.WriteRegister.call_args_list
        actual_register_values = []
        for register, index in write_calls:
           actual_register_values.append(register[0].value)
        self.assertEqual(expected_register_values,actual_register_values)

    def test_clearPerRailHclcAlarmsRegister(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.WriteRegister = Mock()
        rail_count = 8
        write_expected_call_count = rail_count
        expected_register_values = [0xffffffff]*rail_count
        hvdps_subslot.clearPerRailHclcAlarmsRegister()
        self.assertEqual(hvdps_subslot.WriteRegister.call_count,write_expected_call_count)
        write_calls = hvdps_subslot.WriteRegister.call_args_list
        actual_register_values = []
        for register, index in write_calls:
           actual_register_values.append(register[0].value)
        self.assertEqual(expected_register_values,actual_register_values)

    def test_clearGlobalAlarmRegister(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffffffff]
        actual_register_value = []
        hvdps_subslot.clearGlobalAlarmRegister()
        self.assertEqual(hvdps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hvdps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value,actual_register_value)

    def test_clearHclcSampleAlarmRegister(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffffffff]
        actual_register_value = []
        hvdps_subslot.clearHclcSampleAlarmRegister()
        self.assertEqual(hvdps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hvdps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value,actual_register_value)

    def test_EnableAlarms(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0x1]
        actual_register_value = []
        hvdps_subslot.EnableAlarms()
        self.assertEqual(hvdps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hvdps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value, actual_register_value)

    def test_clearHclcCfoldAlarmsRegister(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffffffff]
        actual_register_value = []
        hvdps_subslot.clearHclcCfoldAlarmsRegister()
        self.assertEqual(hvdps_subslot .WriteRegister.call_count, write_call_count)
        write_calls = hvdps_subslot .WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value, actual_register_value)
        hvdps_subslot.clearHclcCfoldAlarmsRegister()

    def test_getDutDomainIdRegisterType(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        register = hvdps_subslot.getDutDomainIdRegisterType()
        expected_register_type = getattr(hvdps_subslot.hvdps_registers,'DUT_DOMAIN_ID')
        self.assertIs(expected_register_type,register)

    def test_getSafeStateRegister(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        register = hvdps_subslot.getSafeStateRegister()
        expected_register_type = getattr(hvdps_subslot.hvdps_registers,'SET_SAFE_STATE')
        self.assertIs(expected_register_type,register)

    def test_getGlobalAlarmRegisterType(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        register = hvdps_subslot.getGlobalAlarmRegisterType()
        expected_register_type = getattr(hvdps_subslot.hvdps_registers, 'GLOBAL_ALARMS')
        self.assertIs(expected_register_type, register)

    def test_VoltageToSS7Ad5764Dac(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        self.assertEqual(hvdps_subslot.VoltageToSS7Ad5764Dac(0),0)
        self.assertEqual(hvdps_subslot.VoltageToSS7Ad5764Dac(6.25),0x7fff)
        self.assertEqual(hvdps_subslot.VoltageToSS7Ad5764Dac(-6.25),0x8000)
        self.assertEqual(hvdps_subslot.VoltageToSS7Ad5764Dac(2),0x28f5)
        self.assertEqual(hvdps_subslot.VoltageToSS7Ad5764Dac(-2),0xd70a)


    def test_InitializeAd5560Rails(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.WriteAd5560Register=Mock()
        hvdps_subslot.InitializeAd5560Rails()

    def test_clear_all_alarms_before_test(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
        hvdps_subslot.ReadRegister = Mock()

        myMockManager = Mock()

        hvdps_subslot.SetRailsToSafeState = myMockManager.SetRailsToSafeState
        hvdps_subslot.ClearHclcRailAlarms = myMockManager.ClearHclcRailAlarms
        hvdps_subslot.ClearHclcSampleAlarms = myMockManager.ClearHclcSampleAlarms
        hvdps_subslot.clear_dut_rail_alarm = myMockManager.clear_dut_rail_alarm
        hvdps_subslot.ClearGlobalAlarms = myMockManager.ClearGlobalAlarms

        expectedOrder = [str(call.SetRailsToSafeState),str(call.ClearHclcRailAlarms),str(call.ClearHclcSampleAlarms),
                         str(call.clear_dut_rail_alarm),str(call.ClearGlobalAlarms)]

        hvdps_subslot.clear_all_alarms_before_test()
        calledfunctions = []
        for callitem in myMockManager.mock_calls:
            calledfunctions.append(callitem[0])

        self.assertEqual(calledfunctions,expectedOrder,'Order of execution of is not correct, {}, expected order: {}'.format(calledfunctions,expectedOrder))

    def test_voltage_to_raw_dac_conversion(self):
        for i in range (19):
            value_to_test = [-2.5, -3.5, -2.8, -3.3, 0, 1, 1.5, 2, 2.5, 3, 3.8, 4.25, 8.64, 1.35, 1.22, 2.54, 3.78,
                            1.19999, 0.9998035430908203]
            expected_converted_value = [0x806, 0x0, 0x506, 0x8, 0x2100, 0x2afd, 0x2ffc, 0x34fb, 0x39f9, 0x3ef8, 0x46f6,
                                       0x4b75, 0x7750, 0x2e7c, 0x2d30, 0x3a60, 0x46c3, 0x2cfc, 0x2afd]
            dps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            return_from_fval = dps_subslot.voltage_to_ad5560_raw_dac_code(value_to_test[i])
            self.assertEqual(return_from_fval, expected_converted_value[i])

    def test_raw_dac_to_voltage_conversion(self):
        fudge_factor = 2
        for i in range(18):
            value_to_test = [-2.5, -3.3, -2.8, 0, 1, 1.5, 2, 2.5, 3, 3.8, 4.25, 8.64, 1.35, 1.22, 2.54, 3.78, 1.19999,
                             0.9998035430908203]
            dps_subslot = HvdpsSubslot(hvdps=Mock(), slot=0, subslot=0)
            dac_from_fval = dps_subslot.voltage_to_ad5560_raw_dac_code(value_to_test[i])
            return_from_fval = dps_subslot.ad5560_raw_dac_code_to_voltge(dac_from_fval)
            self.assertTrue(dps_subslot.is_close(value_to_test[i], return_from_fval, fudge_factor))


    def test_ConfigureUhcRail(self):
        with patch('Common.instruments.dps.hvdpsSubslot.random.choice', return_value=3)as mock_Random:
            uhc = 5
            slot = 6
            subslot = 0
            rail =7
            rail_mask =0x1<<rail
            hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=slot, subslot=subslot)
            hvdps_subslot.WriteRegister = Mock(return_value=Mock(value=0))
            expected_write_values = []
            for i in range(8):
                index = i
                if i == uhc:
                    data = rail_mask
                else:
                    data = 0x0000
                expected_write_values.append((data,index))
            unused_uhc_ofset = mock_Random.return_value
            inverted_rails = (~rail_mask & 0xFFFF)
            expected_write_values.append((inverted_rails,unused_uhc_ofset))
            hvdps_subslot.ConfigureUhcRail(uhc, 0x1 <<rail,'LVM')
            write_calls = hvdps_subslot.WriteRegister.call_args_list
            write_values = []
            for call in write_calls:
                register,index =  call
                write_values.append((register[0].value,index['index']))
            self.assertEqual(expected_write_values, write_values)

    def test_Ltc2630TrackingDacVoltageConversion(self):
        slot = 6
        subslot = 0
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=slot, subslot=subslot)
        expected_voltage = [0x17a, 0x220, 0x2ab, 0x258, 0x2ff, 0x30a]
        voltage = [9, 15, 20, 17, 23, 25]
        for items in range(6):
            dac_voltage = round(hvdps_subslot.Ltc2630TrackingDacVoltageConversion(voltage[items]))
            self.assertEqual(dac_voltage, expected_voltage[items])

    def test_GetCmdDataForSetTrackingVoltage(self):
        slot = 6
        subslot = 0
        expected_command_data = [0xd7a, 0xeab, 0xe20, 0xe58, 0xeff, 0xf0a]
        voltage = [9, 20, 15, 17, 23, 25]
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=slot, subslot=subslot)
        for items in range(6):
            command_data = hvdps_subslot.GetCmdDataForSetTrackingVoltage(voltage[items])
            self.assertEqual(command_data, expected_command_data[items])

    def test_HvTrackingVoltageAdcValueConversion(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=6, subslot=0)
        expected_voltage = [13, 12, 18, 20, 7, 22]
        voltage = [0xDE44, 0xCCA1, 0x132F1, 0x15555, 0x775E, 0X17777]
        for items in range(6):
            actual_voltage = round(hvdps_subslot.HvTrackingVoltageAdcValueConversion(voltage[items]))
            self.assertEqual(actual_voltage, expected_voltage[items])

    def test_image_load_attribute_missing_from_configs(self):
        with patch('Common.instruments.dps.hvdpsSubslot.configs') as mock_configs:
            del mock_configs.HVDPS_FPGA_LOAD
            hvdps_instrument = hvdpsCombo.HvdpsCombo(slot = 7, rc = self.mock_rc)
            with self.assertRaises(fval.LoggedError):
                hvdps_instrument.subslots[0].FpgaImageLoadAttributeExistsInConfigs()

    def test_when_unexpected_dut_global_alarm_received(self):
        hvdps_subslot = HvdpsSubslot(hvdps=Mock(), slot=2, subslot=0)
        hvdps_subslot.Log = Mock()
        hvdps_subslot.get_global_alarms = Mock(return_value=['HclcRailAlarms'])
        return_value = hvdps_subslot.check_for_single_lvm_rail_comparator_alarm('Rail02UnderVolt')
        log_calls = hvdps_subslot.Log.call_args_list
        self.assertEqual(return_value, False)
        self.assertEqual(len(log_calls), 1)
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message,
                             'Expected only \'DutOvUvCompAlarms\' in GLOBAL_ALARMS to be set, actual fields set: ',
                             'Log not displaying correct information')


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
