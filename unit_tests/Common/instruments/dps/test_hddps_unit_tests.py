################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch
from unittest.mock import Mock

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.insert(0, os.path.abspath(repo_root_path))
    
from Common.instruments.dps import hddps
from Common import fval
from Common import hilmon as hil


class HddpsTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def test_create_hddps_instrument(self):
        # check that no hil calls are made in instrument generation
        with patch('Common.instruments.dps.dps.hil') as mock_hddps_hil:
            hddps.Hddps(slot=2, rc=self.mock_rc)
            if mock_hddps_hil.mock_calls != []:
                self.fail('Hil function called in __init__')

    def test_log_blt_info(self):
        hddps_inst = hddps.Hddps(slot=0,rc=None)
        hddps_inst.instrumentblt = Mock()
        hddps_inst.main_boardblt = Mock()
        hddps_inst.daughter_boardblt = Mock()
        hddps_inst.log_blt_info()
        if not hddps_inst.instrumentblt.write_to_log.called :
            self.fail('Instrument BLT not logged')
        if not hddps_inst.main_boardblt.write_to_log.called :
            self.fail('Main board BLT not logged')
        if not hddps_inst.daughter_boardblt.write_to_log.called :
            self.fail('Daughterboard BLT not logged')


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
