################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch, call, Mock
from Common import fval
from Common.instruments.dps import hvdpsCombo
from Common.instruments.dps import hvdpsSubslot
from Common.instruments.dps import hvdps_registers as hvdpsregs

class HvdpsTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None
        self.reg_types = hvdpsregs.get_register_types()

    def test_hvdps_FpgaLoad_with_no_initialize(self):
        with patch ('Common.instruments.dps.hvdpsSubslot.hil') as mock_hil \
                ,patch ('Common.instruments.dps.hvdpsCombo.configs') as mock_configs \
                ,patch ('Common.instruments.dps.hvdpsSubslot.configs') as mock_hvdps_subslot_configs \
                ,patch ('Common.instruments.dps.hvdpsCombo.hil') as mock_combo_hil:
            hvdps_instrument = hvdpsCombo.HvdpsCombo (slot = 6, rc =self.mock_rc)
            hvdps_instrument.subslots[0].FpgaLoad = Mock()
            hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value = 0x0)
            mock_configs.HVDPS_FPGA_LOAD = True
            mock_configs.HVDPS_FPGA_INITIALIZE = False
            mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE = 'dummy_hvdps_image'
            hvdps_instrument.subslots[0].ResetDdr = Mock()
            hvdps_instrument.subslots[0].ClearHclcRailAlarms = Mock()
            hvdps_instrument.subslots[0].ClearGlobalAlarms = Mock()
            hvdps_instrument.subslots[0].InitializeAd5560Rails = Mock()
            hvdps_instrument.subslots[0].ResetThermalAlarmLimitForMAX6627 = Mock()
            hvdps_instrument.subslots[0].ClearTriggerQueue = Mock()
            hvdps_instrument.subslots[0].EnableAlarms = Mock()
            hvdps_instrument.subslots[0].SetRailsToSafeState = Mock()
            hvdps_instrument.subslots[0].UnGangAllHvdpsRails = Mock()
            hvdps_instrument.subslots[0].DisableAllUhcs = Mock()
            hvdps_instrument.subslots[0].CheckForAlarms = Mock(return_value=False)
            hvdps_instrument.Initialize()
            self.assertEqual(hvdps_instrument.subslots[0].CheckForAlarms.call_count, 0)
            self.assertEqual(hvdps_instrument.subslots[0].FpgaLoad.call_count,1)
            hvdps_instrument.subslots[0].FpgaLoad.assert_has_calls([call('dummy_hvdps_image')])

    def test_hvdps_Initialize_with_FpgaLoad(self):
        with patch ('Common.instruments.dps.hvdpsSubslot.hil') as mock_hil \
                ,patch ('Common.instruments.dps.hvdpsCombo.configs') as mock_configs \
                ,patch ('Common.instruments.dps.hvdpsSubslot.configs') as mock_hvdps_subslot_configs \
                ,patch ('Common.instruments.dps.hvdpsCombo.hil') as mock_combo_hil:
            hvdps_instrument = hvdpsCombo.HvdpsCombo (slot = 6, rc =self.mock_rc)
            hvdps_instrument.subslots[0].FpgaLoad = Mock()
            hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value = 0x0)
            mock_configs.HVDPS_FPGA_LOAD = True
            mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE = 'dummy_hvdps_image'
            hvdps_instrument.subslots[0].ResetDdr = Mock()
            hvdps_instrument.subslots[0].ClearHclcRailAlarms = Mock()
            hvdps_instrument.subslots[0].ClearGlobalAlarms = Mock()
            hvdps_instrument.subslots[0].InitializeAd5560Rails = Mock()
            hvdps_instrument.subslots[0].ResetThermalAlarmLimitForMAX6627 = Mock()
            hvdps_instrument.subslots[0].ClearTriggerQueue = Mock()
            hvdps_instrument.subslots[0].EnableAlarms = Mock()
            hvdps_instrument.subslots[0].SetRailsToSafeState = Mock()
            hvdps_instrument.subslots[0].UnGangAllHvdpsRails = Mock()
            hvdps_instrument.subslots[0].DisableAllUhcs = Mock()
            hvdps_instrument.subslots[0].DpsBarWrite = Mock()
            hvdps_instrument.subslots[0].CheckForAlarms = Mock(return_value=False)
            hvdps_instrument.subslots[0].get_folded_state = Mock()
            hvdps_instrument.Initialize()
            self.assertEqual(hvdps_instrument.subslots[0].CheckForAlarms.call_count, 2)
            self.assertEqual(hvdps_instrument.subslots[0].FpgaLoad.call_count,1)
            hvdps_instrument.subslots[0].FpgaLoad.assert_has_calls([call(mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE)])

    def test_hvdps_Initialize_with_FpgaLoad_with_post_fpga_alarm(self):
        with patch ('Common.instruments.dps.hvdpsSubslot.hil') as mock_hil \
                ,patch ('Common.instruments.dps.hvdpsCombo.configs') as mock_configs \
                ,patch ('Common.instruments.dps.hvdpsSubslot.configs') as mock_hvdps_subslot_configs \
                ,patch ('Common.instruments.dps.hvdpsCombo.hil') as mock_combo_hil:
            hvdps_instrument = hvdpsCombo.HvdpsCombo (slot = 6, rc =self.mock_rc)
            hvdps_instrument.subslots[0].FpgaLoad = Mock()
            hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value = 0x0)
            mock_configs.HVDPS_FPGA_LOAD = True
            mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE = 'dummy_hvdps_image'
            hvdps_instrument.subslots[0].ResetDdr = Mock()
            hvdps_instrument.subslots[0].ClearHclcRailAlarms = Mock()
            hvdps_instrument.subslots[0].ClearGlobalAlarms = Mock()
            hvdps_instrument.subslots[0].InitializeAd5560Rails = Mock()
            hvdps_instrument.subslots[0].ResetThermalAlarmLimitForMAX6627 = Mock()
            hvdps_instrument.subslots[0].ClearTriggerQueue = Mock()
            hvdps_instrument.subslots[0].EnableAlarms = Mock()
            hvdps_instrument.subslots[0].SetRailsToSafeState = Mock()
            hvdps_instrument.subslots[0].UnGangAllHvdpsRails = Mock()
            hvdps_instrument.subslots[0].DisableAllUhcs = Mock()
            hvdps_instrument.subslots[0].CheckForAlarms = Mock(side_effect =[True,False])
            with self.assertRaises(fval.LoggedError):
                hvdps_instrument.Initialize()
            self.assertEqual(hvdps_instrument.subslots[0].CheckForAlarms.call_count,1)
            self.assertEqual(hvdps_instrument.subslots[0].FpgaLoad.call_count,1)
            hvdps_instrument.subslots[0].FpgaLoad.assert_has_calls([call(mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE)])

    def test_hvdps_Initialize_with_FpgaLoad_with_post_initialization_alarm(self):
        with patch ('Common.instruments.dps.hvdpsSubslot.hil') as mock_hil \
                ,patch ('Common.instruments.dps.hvdpsCombo.configs') as mock_configs \
                ,patch ('Common.instruments.dps.hvdpsSubslot.configs') as mock_hvdps_subslot_configs \
                ,patch ('Common.instruments.dps.hvdpsCombo.hil') as mock_combo_hil:
            hvdps_instrument = hvdpsCombo.HvdpsCombo (slot = 6, rc =self.mock_rc)
            hvdps_instrument.subslots[0].FpgaLoad = Mock()
            hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value = 0x0)
            mock_configs.HVDPS_FPGA_LOAD = True
            mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE = 'dummy_hvdps_image'
            hvdps_instrument.subslots[0].ResetDdr = Mock()
            hvdps_instrument.subslots[0].ClearHclcRailAlarms = Mock()
            hvdps_instrument.subslots[0].ClearGlobalAlarms = Mock()
            hvdps_instrument.subslots[0].InitializeAd5560Rails = Mock()
            hvdps_instrument.subslots[0].ResetThermalAlarmLimitForMAX6627 = Mock()
            hvdps_instrument.subslots[0].ClearTriggerQueue = Mock()
            hvdps_instrument.subslots[0].EnableAlarms = Mock()
            hvdps_instrument.subslots[0].SetRailsToSafeState = Mock()
            hvdps_instrument.subslots[0].UnGangAllHvdpsRails = Mock()
            hvdps_instrument.subslots[0].DisableAllUhcs = Mock()
            hvdps_instrument.subslots[0].DpsBarWrite = Mock()
            hvdps_instrument.subslots[0].CheckForAlarms = Mock(side_effect =[False,True])
            hvdps_instrument.subslots[0].get_folded_state = Mock()
            with self.assertRaises(fval.LoggedError):
                hvdps_instrument.Initialize()
            self.assertEqual(hvdps_instrument.subslots[0].CheckForAlarms.call_count,2)
            self.assertEqual(hvdps_instrument.subslots[0].FpgaLoad.call_count,1)
            hvdps_instrument.subslots[0].FpgaLoad.assert_has_calls([call(mock_hvdps_subslot_configs.HVDPS_FPGA_IMAGE)])

    def test_hvdps_Initialize_noFpgaLoad(self):
        with patch ('Common.instruments.dps.hvdpsSubslot.hil') as mock_hil \
                ,patch ('Common.instruments.dps.hvdpsCombo.configs') as mock_configs \
                ,patch ('Common.instruments.dps.hvdpsSubslot.configs') as mock_hvdps_subslot_configs \
                ,patch ('Common.instruments.dps.hvdpsCombo.hil') as mock_combo_hil:
            hvdps_instrument = hvdpsCombo.HvdpsCombo (slot = 6, rc =self.mock_rc)
            hvdps_instrument.subslots[0].FpgaLoad = Mock()
            hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value=0x0)
            mock_configs.HVDPS_FPGA_LOAD = False
            hvdps_instrument.LogBltInfo = Mock()
            hvdps_instrument.subslots[0].ResetDdr = Mock()
            hvdps_instrument.subslots[0].ClearHclcRailAlarms = Mock()
            hvdps_instrument.subslots[0].ClearGlobalAlarms = Mock()
            hvdps_instrument.subslots[0].InitializeAd5560Rails = Mock()
            hvdps_instrument.subslots[0].ResetThermalAlarmLimitForMAX6627 = Mock()
            hvdps_instrument.subslots[0].ClearTriggerQueue = Mock()
            hvdps_instrument.subslots[0].EnableAlarms = Mock()
            hvdps_instrument.subslots[0].SetRailsToSafeState = Mock()
            hvdps_instrument.subslots[0].UnGangAllHvdpsRails = Mock()
            hvdps_instrument.subslots[0].DisableAllUhcs = Mock()
            hvdps_instrument.subslots[0].CheckForAlarms = Mock(return_value=False)
            hvdps_instrument.subslots[0].DpsBarWrite = Mock()
            hvdps_instrument.subslots[0].get_folded_state = Mock()
            hvdps_instrument.Initialize()
            self.assertEqual(hvdps_instrument.subslots[0].CheckForAlarms.call_count, 2)
            self.assertEqual(hvdps_instrument.subslots[0].FpgaLoad.call_count,0)

    def test_hvdps_initialize_bad_image_name(self):
       with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil:
            for HVDPS_FPGA_IMAGE_NAME in ['invalid','','hddps.bin']:
                hvdps_instrument = hvdpsCombo.HvdpsCombo(slot = 7, rc = self.mock_rc)
                with self.assertRaises(fval.LoggedError):
                    hvdps_instrument.subslots[0].FpgaLoad(HVDPS_FPGA_IMAGE_NAME)

    def test_hvdps_initialize_correct_image_name(self):
        with patch('Common.instruments.dps.hvdpsSubslot.hil') as mock_hil,\
             patch ('Common.instruments.dps.hvdpsCombo.configs') as mock_configs,\
             patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            for HVDPS_FPGA_IMAGE_NAME in ['HVDPS.bin', 'dummy_HVDPS_image', 'hvdps.bin']:
                mock_hil.dpsFpgaLoad = Mock(return_value=True)
                hvdps_instrument = hvdpsCombo.HvdpsCombo(slot = 7, rc = self.mock_rc)
                load_result = hvdps_instrument.subslots[0].FpgaLoad(HVDPS_FPGA_IMAGE_NAME)
                self.assertEqual(load_result, True)

    def test_image_name_attribute_missing_from_configs(self):
        with patch('Common.instruments.dps.hvdpsSubslot.configs') as mock_configs:
            del mock_configs.HVDPS_FPGA_IMAGE
            hvdps_instrument = hvdpsCombo.HvdpsCombo(slot = 7, rc = self.mock_rc)
            with self.assertRaises(fval.LoggedError):
                hvdps_instrument.subslots[0].FpgaImageAttributeExistsInConfigs()


    def test_if_calbase_ignore_is_selected(self):
        with patch('Common.instruments.dps.hvdpsCombo.configs') as mock_configs:
            hvdps_instrument = hvdpsCombo.HvdpsCombo(slot=6, rc=self.mock_rc)
            mock_configs.CALBASE_IGNORE = False
            mock_configs.HVDPS_CAL_CARD_INITIALIZE = True
            hvdps_instrument.Log = Mock()

            hvdps_instrument.cal_card_initialize()
            log_calls = hvdps_instrument.Log.call_args_list
            print(log_calls)
            log_calls = hvdps_instrument.Log.call_args_list
            for args, kwargs in log_calls:
                log_level = args[0]
                log_message = args[1]
                print(log_level)
                print(log_message)
                self.assertRegex(log_message,'CalBase not initialized, HVDPS cal card cant be initialized.Update config to initialize Cal Base',
                                 'Log not displaying correct information')
                self.assertEqual(log_level, 'warning', 'Log level should be warning')


    def test_if_calcard_do_not_configure_is_selected(self):
        with patch('Common.instruments.dps.hvdpsCombo.configs') as mock_configs:
            hvdps_instrument = hvdpsCombo.HvdpsCombo(slot=6, rc=self.mock_rc)
            hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value=0x0)
            mock_configs.CALBASE_IGNORE = True
            mock_configs.HVDPS_CAL_CARD_INITIALIZE = False
            hvdps_instrument.Log = Mock()
            # self.mock_methods_in_initialize(hvdps_instrument)
            hvdps_instrument.cal_card_initialize()
            log_calls = hvdps_instrument.Log.call_args_list
            for args, kwargs in log_calls:
                log_level = args[0]
                log_message = args[1]
                self.assertRegex(log_message,
                                 'HVDPS Cal Card not initialized.Update config to initialize Cal Card',
                                 'Log not displaying correct information')
                self.assertEqual(log_level, 'warning', 'Log level should be warning')


    def test_if_calls_were_made_to_expected_methods(self):
        with patch('Common.instruments.dps.hvdpsCombo.configs') as mock_configs:
            hvdps_instrument = hvdpsCombo.HvdpsCombo(slot=6, rc=self.mock_rc)
            write_expected_call_count = 1
            mock_configs.CALBASE_IGNORE = True
            mock_configs.HVDPS_CAL_CARD_INITIALIZE = True
            hvdps_instrument.CalCardConnect = Mock()
            hvdps_instrument.CalCardInitialize = Mock()
            hvdps_instrument.calboardblt.write_to_log = Mock()
            hvdps_instrument.cal_card_initialize()
            self.assertEqual(hvdps_instrument.CalCardConnect.call_count, write_expected_call_count)
            self.assertEqual(hvdps_instrument.CalCardInitialize.call_count, write_expected_call_count)
            self.assertEqual(hvdps_instrument.calboardblt.write_to_log.call_count, write_expected_call_count)

    def mock_methods_in_initialize(self, hvdps_instrument):
        hvdps_instrument.LogBltInfo = Mock()
        hvdps_instrument.subslots[0].FpgaLoad = Mock()
        hvdps_instrument.subslots[0].GetFpgaVersion = Mock(return_value=0x0)
        hvdps_instrument.subslots[0].CheckForAlarms = Mock(return_value=False)
        hvdps_instrument.subslots[0].ResetDdr = Mock()
        hvdps_instrument.subslots[0].ClearHclcRailAlarms = Mock()
        hvdps_instrument.subslots[0].ClearGlobalAlarms = Mock()
        hvdps_instrument.subslots[0].UnGangAllHvdpsRails = Mock()
        hvdps_instrument.subslots[0].InitializeAd5560Rails = Mock()
        hvdps_instrument.subslots[0].ResetThermalAlarmLimitForMAX6627 = Mock()
        hvdps_instrument.subslots[0].ClearTriggerQueue = Mock()
        hvdps_instrument.subslots[0].SetRailsToSafeState = Mock()
        hvdps_instrument.subslots[0].DisableAllUhcs = Mock()
        hvdps_instrument.subslots[0].EnableAlarms = Mock()
        hvdps_instrument.subslots[0].WriteRegister = Mock()
        hvdps_instrument.subslots[0].WriteBar2RailCommand = Mock()
        hvdps_instrument.subslots[0].BarRead = Mock()

        hvdps_instrument.Log = Mock()


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
