################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch
from unittest.mock import Mock


if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common.instruments.dps.hvdpsCombo import HvdpsCombo
from Common.instruments.dps.hvilSubslot import HvilSubslot
from Common import fval
import random

class HvdpsInstrumentDiscoveryTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None

    def test_discover_Hvdps_found_Hvil_found(self):
        with patch ('Common.instruments.dps.hvdpsCombo.hil') as mock_hvdps_hil:
            slot = random.randint(0,11)
            mock_hvdps_hil.hvdpsConnect = Mock(side_effect=[''] )
            mock_hvdps_hil.hvilConnect = Mock(side_effect=[''])
            hvdps_instrument =  HvdpsCombo(slot = slot, rc= self.mock_rc)
            hvdps_return = hvdps_instrument.discover()
            print(hvdps_return)
            self.assertIsInstance(hvdps_return[0], HvdpsCombo)
            self.assertIsInstance(hvdps_return[0].subslots[1], HvilSubslot)
            self.assertEquals(len(hvdps_return),1)


    def test_discover_Hvdps_not_found(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hvdps_hil:
            slot = random.randint(0, 11)
            mock_hvdps_hil.hvdpsConnect = Mock(side_effect=RuntimeError)
            hvdps_instrument = HvdpsCombo(slot=slot, rc=self.mock_rc)
            hvdps_return = hvdps_instrument.discover()
            self.assertEqual(hvdps_return, [])


    def test_discover_Hvdps_found_Hvil_not_found(self):
        with patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hvdps_hil:
            slot = random.randint(0, 11)
            mock_hvdps_hil.hvdpsConnect = Mock([''])
            mock_hvdps_hil.hvilConnect = Mock(side_effect=RuntimeError)
            hvdps_instrument = HvdpsCombo(slot=slot, rc=self.mock_rc)
            hvdps_return = hvdps_instrument.discover()
            print(hvdps_return)
            self.assertIsInstance(hvdps_return[0], HvdpsCombo)



if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)