################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
import os
import random
import sys
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from unittest.mock import call
from Common.instruments.dps import hddps_registers as hddps_regs
from Common.instruments.dps import ad5560_registers

from Common.register import Register


if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common.instruments.dps.hddpsSubslot import HddpsSubslot
from Common.instruments.dps.hddps import Hddps
from Common import fval


class HddpsSubSlotTests(unittest.TestCase):
    def test_ad5560_register_setup_functions(self):
        #(register setup function,registername,expected value)
        ad5560_register_init_functions = [('ad5560DpsReg1Initialize','DPS_REGISTER1',0x3730),
                                          ('ad5560DpsReg2Initialize','DPS_REGISTER2', 0x8280),
                                          ('ad5560SysControlInitInitialize','SYSTEM_CONTROL',0xD200),
                                          ('ad5560Compensation1Initialize','COMPENSATION1',0x0000),
                                          ('ad5560Compensation2Initialize','COMPENSATION2',0xA150),
                                          ('ad5560AlarmSetupInitialize','ALARM_SETUP',0x1540)]

        for functionName,registerName,expectedValue in ad5560_register_init_functions:
            hvdps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc = Mock())
            ad5560RegInitFunc = getattr(hvdps_subslot,functionName)
            ad5560RegInit = ad5560RegInitFunc(getattr(ad5560_registers,registerName))
            self.assertEqual(ad5560RegInit.value, expectedValue,
                         'Expecting register {} to be: 0x{:02X}, getting: 0x{:02X}'.
                         format(registerName, expectedValue,ad5560RegInit.value))

    def test_ClearTriggerQueue(self):
        with patch('Common.instruments.dps.hddpsSubslot.hil') as mock_subslot_hil,\
                patch('Common.instruments.dps.dpsSubslot.hil') as mock_dpssubslot_hil:
            mock_dpssubslot_hil.dpsDmaWrite = Mock()
            hddps_subslot = HddpsSubslot(hddps=Mock(),slot=0,subslot=0, rc=Mock())
            hddps_subslot.ClearTriggerQueue()
            total_bytes = 0
            for name,args,kwargs in mock_dpssubslot_hil.dpsDmaWrite.mock_calls:
                slot,subslot,offset,data = args
                self.assertEqual(offset,total_bytes)
                total_bytes += len(data)
            self.assertEqual(total_bytes,hddps_subslot.TOTAL_MEMORY_SIZE)

    def test_AuroraIsReadyHardErrorPass(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        status = hddps_regs.STATUS()
        status.AuroraHardError = 0
        hddps_subslot.ReadObject = Mock(return_value=status)
        self.assertEqual(hddps_subslot.AuroraIsReady(),True)

    def test_AuroraIsReadyHardErrorFail(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        status = hddps_regs.STATUS()
        status.AuroraHardError = 1
        hddps_subslot.ReadObject = Mock(return_value=status)
        self.assertEqual(hddps_subslot.AuroraIsReady(),False)

    def test_CheckAlarms(self):
        hddps_sublot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_sublot.ReadRegister = Mock()
        hddps_sublot.RetrieveMultipleAlarms = Mock()
        alarm_result = hddps_sublot.CheckAlarms()
        self.assertEqual(hddps_sublot.RetrieveMultipleAlarms.call_count, 4)
        self.assertEqual(hddps_sublot.ReadRegister.call_count, 7)
        alarm_type_list = ["GLOBAL_ALARMS","HclcSampleAlarms","VlcSampleAlarms","HclcCfoldAlarms","VlcCfoldAlarms","VlcCfoldData","HclcCfoldData","HCLC_ALARMS","VlcRailAlarms","VlcRailAlarmsData","HclcRailAlarmsData"]
        self.assertSetEqual(set(alarm_type_list), set(list(alarm_result.keys())))

    def test_RetrieveMultipleAlarms(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.ReadRegister = Mock()
        alarmNamePrefix = 'Vlc'
        alarmNameSubfix = 'RailAlarms'
        numberRails = 16
        hddps_subslot.RetrieveMultipleAlarms(alarmNamePrefix,alarmNameSubfix,numberRails)
        self.assertEqual(hddps_subslot.ReadRegister.call_count, 16)
        rail_index = 0
        for args,kwargs in hddps_subslot.ReadRegister.call_args_list:
            registerName = '{}{:02d}{}'.format(alarmNamePrefix,rail_index,alarmNameSubfix)
            self.assertIs(args[0],getattr(hddps_regs,registerName))
            rail_index = rail_index + 1

    @unittest.skip('No Longer Supported method of adjusting calibration')
    def test_CheckCurrentAdcCalRegister_fail_1p2A(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        myreg = hddps_regs.Rail01IAdc1p2AGainOffset()
        myreg.value = 0x80008001
        hddps_subslot.ReadRegister = Mock(return_value= myreg)
        irange = '1p2A'
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.CheckCurrentAdcCalRegister(irange)

    @unittest.skip('No Longer Supported method of adjusting calibration')
    def test_CheckCurrentAdcCalRegister_fail_25A(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        myreg = hddps_regs.Rail01IAdc1p2AGainOffset()
        myreg.value = 0x80008001
        hddps_subslot.ReadRegister = Mock(return_value= myreg)
        irange = '25A'
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.CheckCurrentAdcCalRegister(irange)

    @unittest.skip('No Longer Supported method of adjusting calibration')
    def test_CheckCurrentAdcCalRegister_pass_1p2A(self):
        debug_message = 'Current Adc Calibration register'
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        myreg = hddps_regs.Rail01IAdc1p2AGainOffset()
        myreg.value = 0x80008000
        hddps_subslot.ReadRegister = Mock(return_value= myreg)
        irange = '1p2A'
        hddps_subslot.Log = Mock()
        hddps_subslot.CheckCurrentAdcCalRegister(irange)
        log_calls = hddps_subslot.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'DEBUG')
            self.assertRegex(log_message,debug_message)


    def test_CheckForHclcCfoldAlarm(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock()

        myMockManager = Mock()
        hddps_subslot.CheckForHclcGlobalAlarm = myMockManager.CheckForHclcGlobalAlarm
        hddps_subslot.CheckForHclcRailAlarm = myMockManager.CheckForHclcRailAlarm
        hddps_subslot.CheckValidHclcFoldData = myMockManager.CheckValidHclcFoldData

        expectedOrder = [str(call.CheckForHclcGlobalAlarm),str(call.CheckForHclcRailAlarm),str(call.CheckValidHclcFoldData)]

        hddps_subslot.CheckForHclcCfoldAlarm(rail = 5)
        calledfunctions = []
        for callitem in myMockManager.mock_calls:
            calledfunctions.append(callitem[0])

        self.assertEqual(calledfunctions,expectedOrder,'Order of execution of is not correct, {}, expected order: {}'.format(calledfunctions,expectedOrder))


    def test_CheckForHclcGlobalAlarmPassing(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value = Mock(testalarm=1,value = 0x1))
        hddps_subslot.Log = Mock()
        hddps_subslot.CheckForHclcGlobalAlarm('testalarm')
        log_calls = hddps_subslot.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO','Log level should be INFO')
            self.assertRegex(log_message,'GLOBAL_ALARMS','Log not displaying correct information')

    def test_CheckForHclcGlobalAlarmFailing(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value = Mock(testalarm=0,value = 0x0))
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.CheckForHclcGlobalAlarm('testalarm')

    def test_CheckForHclcRailAlarmPassing(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value = Mock(Rail2Alarm=1,value = 0x1))
        hddps_subslot.Log = Mock()
        hddps_subslot.CheckForHclcRailAlarm(rail=2)
        log_calls = hddps_subslot.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO','Log level should be INFO')
            self.assertRegex(log_message,'CFOLD','Log not displaying correct information')

    def test_CheckForHclcRailAlarmFailing(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value = Mock(Rail2Alarm=0,value = 0x0))
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.CheckForHclcRailAlarm(rail=2)

    def test_CheckValidHclcFoldDataFailing(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value = Mock(CFoldActualData=1,AssertTestData=1,value = 0x1))
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.CheckValidHclcFoldData(rail=2)

    def test_CheckValidHclcFoldDataPass(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value=Mock(CFoldActualData=1, AssertTestData=0, value=0x1))
        hddps_subslot.Log = Mock()
        hddps_subslot.CheckValidHclcFoldData(rail=2)
        log_calls = hddps_subslot.Log.call_args_list

        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO','Log level should be INFO')
            self.assertRegex(log_message,'HclcCfoldData','Log not displaying correct information')

    def test_CheckHclcRailAlarms_NoAlarmsSet(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(side_effect=[Mock(HclcRailAlarms=0),Mock(value= 0)])
        result = hddps_subslot.CheckHclcRailAlarm()
        self.assertEqual(result,False)

    def test_CheckHclcRailAlarms_NoRailAlarmsSet(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(side_effect=[Mock(HclcRailAlarms=1),Mock(value= 0)])
        with self.assertRaises(fval.LoggedError):
            result = hddps_subslot.CheckHclcRailAlarm()

    def test_CheckHclcRailAlarms_NeedRegisterClear(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(side_effect=[Mock(HclcRailAlarms=1), Mock(value=1)])

        hddps_subslot.Log = Mock()
        result = hddps_subslot.CheckHclcRailAlarm()
        log_calls = hddps_subslot.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'WARNING','Log level should be WARNING')

        self.assertEqual(result, True)




    def test_CheckHclcSampleAlarms_NoAlarm(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value=Mock(HclcSampleAlarms=0, value = 0))
        hddps_subslot.CheckHclcSampleAlarms()

    def test_CheckHclcSampleAlarms_Alarm(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value=Mock(HclcSampleAlarms=1, value = 0))
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.CheckHclcSampleAlarms()

    def test_ClearTrigExecAlarms_Cleared(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value=Mock(vlcsamplealarm=0, value=0))
        hddps_subslot.Write = Mock()
        hddps_subslot.ClearTrigExecAlarms()

    def test_EnableOnlyOneDutDomainId(self):
        with patch('Common.instruments.dps.dpsSubslot.hil') as mock_subslot_hil:
            dutid = 5
            slot =6
            subslot = 0
            hddps_subslot = HddpsSubslot(hddps=Hddps(slot=slot, rc=Mock()), slot=slot, subslot=0, rc=Mock())
            DutDomainRegister = hddps_subslot.getDutDomainIdRegisterType()
            bar =DutDomainRegister.BAR
            expected_write_calls = []
            expected_read_calls=[]
            for i in range(DutDomainRegister.REGCOUNT):
                offset = DutDomainRegister.address(i)
                if i == dutid:
                    data = DutDomainRegister(DutValidBit=1,DutDomainId = 15).value
                else:
                    data = DutDomainRegister(DutValidBit=0,DutDomainId = 0).value
                expected_write_calls.append(call(slot,subslot,bar,offset,data))
                expected_read_calls.append(call(slot,subslot,bar,offset))
            mock_subslot_hil.dpsBarRead = Mock(return_value=0)
            hddps_subslot.EnableOnlyOneUhc(dutid)
            write_calls = mock_subslot_hil.dpsBarWrite.call_args_list
            read_calls = mock_subslot_hil.dpsBarRead.call_args_list
            self.assertEqual(write_calls,expected_write_calls)
            self.assertEqual(read_calls, expected_read_calls)


    def test_ungang_all_hddps_rails(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.ReadRegister = Mock()
        hddps_subslot.WriteRegister = Mock()
        hddps_subslot.UnGangAllRails()
        write_expected_call_count = 14
        expected_register_values = [0x3FF,0x0,0x0,0x7FF] + [0x8000]*10
        self.assertEqual(hddps_subslot.WriteRegister.call_count,write_expected_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        actual_register_values = []
        for register, index in write_calls:
           actual_register_values.append(register[0].value)
        self.assertEqual(expected_register_values,actual_register_values)


    def test_ExecuteTriggerQueue(self):
        offset = 0x1
        dutid = 5
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        hddps_subslot.ReadRegister = Mock(return_value=Mock(value = 1))
        hddps_subslot.CheckTqNotifyAlarm = Mock()
        hddps_subslot.Log = Mock()
        hddps_subslot.GetFpgaVersion = Mock()
        hddps_subslot.ExecuteTriggerQueue(offset,dutid, 'HC')
        write_calls = hddps_subslot.WriteRegister.call_args_list
        execute_bit = (1<<31)
        expected_trigger_value = execute_bit | offset
        expected_register_offset = 20
        self.assertNotEqual(len(write_calls),0)
        for args, kwargs in write_calls:
            self.assertEqual(args[0].value, expected_trigger_value)
            self.assertEqual(args[0].ADDR, expected_register_offset)

    def test_CheckTqNotifyAlarm(self):
        dutid = 5
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.ReadRegister = Mock(return_value=Mock(value=1))
        hddps_subslot.WaitForGlobalAlarmBit = Mock()
        hddps_subslot.GetFpgaVersion = Mock()
        hddps_subslot.CheckTqNotifyAlarm(dutid, 'HC')
        calls_to_WaitForGlobalAlarmBit = hddps_subslot.WaitForGlobalAlarmBit.call_args_list
        for call in calls_to_WaitForGlobalAlarmBit:
            alarm_expected = call[0][0]
            self.assertEqual(alarm_expected, 'TqNotifyLCHC')

    def test_WaitForGlobalAlarmBit_Fail(self):
        alarm_bit = 'TqNotifyAlarmDut5'
        waitLoop = 2
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock()
        hddps_subslot.ReadRegister = Mock(side_effect=[Mock(TqNotifyAlarmDut5 = 0, value = 0),Mock(TqNotifyAlarmDut5 = 0, value = 0),
                                                       Mock(TqNotifyAlarmDut5 = 0, value = 0),Mock(TqNotifyAlarmDut5 = 0, value = 0),
                                                       Mock(TqNotifyAlarmDut5 = 0, value = 0)])
        hddps_subslot.Write = Mock()
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.WaitForGlobalAlarmBit(alarm_bit,waitLoop)

    def test_WaitForGlobalAlarmBit_UnexpectedAlarm_Fail(self):
        alarm_bit = 'TqNotifyAlarmDut5'
        waitLoop = 2
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock()
        hddps_subslot.ReadRegister = Mock(side_effect=[Mock(TqNotifyAlarmDut5 = 0, value = 0),Mock(TqNotifyAlarmDut5 = 0, value = 1),
                                                       Mock(TqNotifyAlarmDut5 = 0, value = 1),Mock(TqNotifyAlarmDut5 = 0, value = 1),
                                                       Mock(TqNotifyAlarmDut5 = 0, value = 1)])
        hddps_subslot.Write = Mock()
        with self.assertRaises(fval.LoggedError):
            hddps_subslot.WaitForGlobalAlarmBit(alarm_bit,waitLoop)

    def test_WaitForGlobalAlarmBitPass(self):
        alarm_bit = 'TqNotifyAlarmDut5'
        waitLoop = 4
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.GetFpgaVersion = Mock()
        hddps_subslot.ReadRegister = Mock(side_effect=[Mock(TqNotifyAlarmDut5=0, value=0), Mock(TqNotifyAlarmDut5=0, value=0),
                                                       Mock(TqNotifyAlarmDut5=1, value=1), Mock(TqNotifyAlarmDut5=0, value=0),
                                                       Mock(TqNotifyAlarmDut5=0, value=0)])
        hddps_subslot.Write = Mock()
        hddps_subslot.WaitForGlobalAlarmBit(alarm_bit, waitLoop)

    def test_clearPerRailHclcAlarmsRegister(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        rail_count = 10
        write_expected_call_count = rail_count
        expected_register_values = [0xFFFFFFFF]*rail_count
        hddps_subslot.clearPerRailHclcAlarmsRegister()
        self.assertEqual(hddps_subslot.WriteRegister.call_count,write_expected_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        actual_register_values = []
        for register, index in write_calls:
           actual_register_values.append(register[0].value)
        self.assertEqual(expected_register_values,actual_register_values)

    def test_ClearVlcRailAlarm(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        rail_count = 16
        write_expected_call_count = rail_count
        expected_register_values = [0xffffffff]*rail_count
        hddps_subslot.ClearVlcRailAlarm()
        self.assertEqual(hddps_subslot.WriteRegister.call_count,write_expected_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        actual_register_values = []
        for register, index in write_calls:
           actual_register_values.append(register[0].value)
        self.assertEqual(expected_register_values,actual_register_values)

    def test_clearGlobalAlarmRegister(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffffffff]
        actual_register_value = []
        hddps_subslot.clearGlobalAlarmRegister()
        self.assertEqual(hddps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value,actual_register_value)

    def test_ClearVlcCfoldAlarms(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffff]
        actual_register_value = []
        hddps_subslot.ClearVlcCfoldAlarms()
        self.assertEqual(hddps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value, actual_register_value)

    def test_clearHclcSampleAlarmRegister(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffffffff]
        actual_register_value = []
        hddps_subslot.clearHclcSampleAlarmRegister()
        self.assertEqual(hddps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value,actual_register_value)

    def test_ClearVlcSampleAlarms(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0xffffffff]
        actual_register_value = []
        hddps_subslot.ClearVlcSampleAlarms()
        self.assertEqual(hddps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value,actual_register_value)

    def test_EnableAlarms(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0x1]
        actual_register_value = []
        hddps_subslot.EnableAlarms()
        self.assertEqual(hddps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value, actual_register_value)



    def test_clearHclcCfoldAlarmsRegister(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        hddps_subslot.WriteRegister = Mock()
        write_call_count = 1
        expected_register_value = [0x3ff]
        actual_register_value = []
        hddps_subslot.clearHclcCfoldAlarmsRegister()
        self.assertEqual(hddps_subslot.WriteRegister.call_count, write_call_count)
        write_calls = hddps_subslot.WriteRegister.call_args_list
        for register, index in write_calls:
            actual_register_value.append(register[0].value)
        self.assertEqual(expected_register_value, actual_register_value)
        hddps_subslot.clearHclcCfoldAlarmsRegister()

    def test_getDutDomainIdRegisterType(self):
        dut_id = 5
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        register = hddps_subslot.getDutDomainIdRegisterType()
        expected_register_type = getattr(hddps_subslot.registers,'DUT_DOMAIN_ID')
        self.assertIs(expected_register_type,register)

    def test_getSafeStateRegister(self):
        hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        register = hddps_subslot.getSafeStateRegister()
        expected_register_type = getattr(hddps_subslot.registers,'SET_SAFE_STATE')
        self.assertIs(expected_register_type,register)


    def test_InitializeAd5560Rails(self):
        hddps_subslot = HddpsSubslot(hddps=Mock(), slot=0, subslot=0, rc=Mock())
        hddps_subslot.WriteAd5560Register=Mock()
        hddps_subslot.InitializeAd5560Rails()


    def test_GetSampleHeaders(self):
        my_hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        # expected from valid 0x9887766554433221
        input_value = b'\x00\x00\x01\x00\x00\x00\x00\x10\x00\x02\x00\x00\x10\x32\x54\x76\xff\xff\xff\xff\x33\x66\x11\x11\x11\x99\x00\x33\x11\x99\x00\x33\x23'
        memory_top = b'\x00\x00\x00\x00'
        wait_value = b'\xff\xff\xff\xff'
        my_hddps_subslot.DmaRead = Mock(side_effect = [wait_value,memory_top,input_value])
        my_hddps_subslot.ReadRegister = Mock(side_effect=[Mock(value = 0),Mock(value = len(input_value))])
        my_dutid = 1
        expected_sample_count = 1

        sample_headers = my_hddps_subslot.GetSampleHeaders(my_dutid, expected_sample_count,rail_type = 'LC', wait_loops=1)

        self.assertEqual(len(sample_headers),1)
        self.assertEqual(sample_headers[0].SamplePointer, 0x10000)
        self.assertEqual(sample_headers[0].SampleRate, 0x0)
        self.assertEqual(sample_headers[0].SampleCount, 0x1000)
        self.assertEqual(sample_headers[0].RailSelect, 0x200)
        self.assertEqual(sample_headers[0].MetaLo, 0x3210)
        self.assertEqual(sample_headers[0].MetaHi, 0x7654)

    def test_GetSampleHeaders_requested_too_many_headers(self):
        my_hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        input_value = b'\x00\x00\x01\x00\x00\x00\x00\x10\x00\x02\x00\x00\x10\x32\x54\x76\xff\xff\xff\xff'
        my_hddps_subslot.DmaRead = Mock(side_effect = input_value)
        my_hddps_subslot.ReadRegister = Mock(side_effect=[Mock(value = 0),Mock(value = len(input_value))])
        my_dutid = 1
        expected_sample_count = 2
        with self.assertRaises(Exception):
            my_hddps_subslot.GetSampleHeaders(my_dutid,expected_sample_count,'LC')

    def test_GetSampleHeaders_sample_engine_not_run(self):
        my_hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        input_value = b'\xff\xff\xff\xff\x00\x00\x00\x10\x00\x02\x00\x00\x10\x32\x54\x76\xff\xff\xff\xff'
        memory_top = b'\xff\xff\xff\xff'
        wait_value = b'\xff\xff\xff\xff'
        my_hddps_subslot.DmaRead = Mock(side_effect = [wait_value,memory_top])
        my_hddps_subslot.ReadRegister = Mock(side_effect=[Mock(value = 0),Mock(value = len(input_value))])
        my_dutid = 1
        expected_sample_count = 1
        with self.assertRaises(fval.core.LoggedError):
            my_hddps_subslot.GetSampleHeaders(my_dutid, expected_sample_count,'LC')


    def test_GetSampleHeaders_sample_engine_terminator_not_raised(self):
        my_hddps_subslot = HddpsSubslot(hddps=Hddps(slot = 0, rc = Mock()), slot=0, subslot=0, rc = Mock())
        input_value = b'\xff\xff\xff\xff\x00\x00\x00\x10\x00\x02\x00\x00\x10\x32\x54\x76\xff\xff\xff\xff'
        memory_top = b'\xff\xff\xff\xff'
        wait_value = b'\x00\x00\x00\x00'
        my_hddps_subslot.DmaRead = Mock(side_effect = [wait_value,memory_top])
        my_hddps_subslot.ReadRegister = Mock(side_effect=[Mock(value = 0),Mock(value = len(input_value))])
        my_dutid = 1
        expected_sample_count = 1
        with self.assertRaises(fval.core.LoggedError):
            my_hddps_subslot.GetSampleHeaders(my_dutid, expected_sample_count, 'LC',wait_loops = 1)

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
