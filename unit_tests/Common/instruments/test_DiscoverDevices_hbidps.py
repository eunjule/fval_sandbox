################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import random

from unittest.mock import patch
from unittest.mock import Mock
from Common import fval
from Common import pcie
from Common.instruments import tester
from Hbidps.instrument import hbidps
from Hbirctc.instrument import hbirctc
from Common.hbi_simulator import HbiSimulator
from Common.instruments.tester import Tester

class DiscoverDevicesHBIDPSTests(unittest.TestCase):
    def test_create_tester_object(self):
        my_tester = tester.Tester()

    def test_rc_not_on_tester(self):
        with patch('Common.hilmon.hil') as hil:
            hil.rcConnect = Mock(side_effect=RuntimeError)
            hil.rc3Connect = Mock(side_effect=RuntimeError)
            hil.hbiMbConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.Log = Mock(side_effect=self.fake_log)
            with self.assertRaises(Tester.NoRcException):
                my_tester.discover_all_instruments()

    def fake_log(self, level, message):
        if level.lower() == 'error':
            raise fval.core.LoggedError(message)
    
    def test_only_rc_on_tester(self):
        with patch('Common.hilmon.hil') as hil, patch('Common.fval.core.Object.Log') as log:
            hil.hbiDpsConnect = Mock(side_effect=RuntimeError)
            hil.pciDeviceOpen = Mock(side_effect=self.fake_pcie_device_open)
    
            hil.hbiMbBltBoardRead = Mock(side_effect=HbiSimulator().hbiMbBltBoardRead)
            my_tester = tester.Tester()
            my_tester.is_hbi_tester = Mock(return_value=True)
            my_tester.discover_all_instruments()
    
            self.assertEqual(len(my_tester.devices), 1)
            self.assertIsInstance(my_tester.devices[0], hbirctc.Hbirctc)
    
    def test_found_hbidps_on_random_slot(self):
        rc_count = 1
        device_count = 1
        with patch('Common.hilmon.hil') as hil, patch('Common.fval.core.Object.Log') as log:
            self.slots = [random.randint(0, 15)]
    
            hil.hbiMbBltBoardRead = Mock(side_effect=HbiSimulator().hbiMbBltBoardRead)
            hil.pciDeviceOpen = Mock(side_effect=self.fake_pcie_device_open)
            hil.hbiDpsConnect = Mock(side_effect=self.connect)
    
            my_tester = tester.Tester()
            my_tester.is_hbi_tester = Mock(return_value=True)
            my_tester.discover_all_instruments()
            final_list = [x for x in my_tester.devices if x is not None]
            self.assertTrue((any(device.name() == 'HBIRCTC' for device in my_tester.devices)),
                            'RC missing from device list')
            self.assertEqual(device_count + rc_count, len(final_list))
            for device in final_list:
                if device.name() != 'HBIRCTC':
                    self.assertIsInstance(device, hbidps.Hbidps)
                    for slot in self.slots:
                        self.assertEqual(slot, device.slot)
    
    def test_found_hbidps_on_two_random_slots(self):
        rc_count = 1
        device_count = 2
        with patch('Common.hilmon.hil') as hil, patch('Common.fval.core.Object.Log') as log:
            self.slots = random.sample(range(0, 11), 2)
    
            hil.hbiMbBltBoardRead = Mock(side_effect=HbiSimulator().hbiMbBltBoardRead)
            hil.hbiDpsConnect = Mock(side_effect=self.connect)
            hil.pciDeviceOpen = Mock(side_effect=self.fake_pcie_device_open)
    
            my_tester = tester.Tester()
            my_tester.is_hbi_tester = Mock(return_value=True)
            my_tester.discover_all_instruments()
            self.assertTrue((any(device.name() == 'HBIRCTC' for device in my_tester.devices)),
                            'RC missing from device list')
            self.assertEqual(device_count + rc_count, len(my_tester.devices))
    
    
            for device in my_tester.devices:
                if device.name() != 'HBIRCTC':
                    self.assertIsInstance(device, hbidps.Hbidps)
                    self.assertTrue(device.slot in self.slots)
    
    def test_found_hbidps_on_all_slots(self):
        with patch('Common.hilmon.hil') as hil, patch('Common.fval.core.Object.Log') as log:
            self.slots = range(16)
    
            hil.hbiMbBltBoardRead = Mock(side_effect=HbiSimulator().hbiMbBltBoardRead)
            hil.hbiDpsConnect = Mock(side_effect=self.connect)
    
            my_tester = tester.Tester()
            my_tester.is_hbi_tester = Mock(return_value=True)
            my_tester.discover_all_instruments()
    
            device_slots = []
            for device in my_tester.devices:
                if device.name() != 'HBIRCTC' and device.name() != 'HBICC':
                    self.assertIsInstance(device, hbidps.Hbidps)
                    device_slots.append(device.slot)
            self.assertEqual(sorted(list(range(16))), sorted(device_slots))
            
    def fake_pcie_device_open(self, guid, viddid, slot, additional):
        if guid != pcie.hbirctc_guid:
            raise RuntimeError()

    def connect(self, slot):
        if slot not in self.slots:
            raise RuntimeError


