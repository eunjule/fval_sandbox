################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import __init__
import collections
import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock

from Common import fval
from Common.instruments import tester

class DeviceTesterTests(unittest.TestCase):

    def test_deviceTuple_random_tester(self):
        with patch('Common.fval.core.Object.Log'), \
            patch('Common.instruments.tester.hil') as mock_hil, \
            patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
            patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
            patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
            patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
            patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk:

            # create random list of tester cards
            deviceList = ['', 'tdaubnk', 'hddps', 'hpcc', 'hvdps']
            debugTester = self.createRandomCards(deviceList)
            deviceConnectList = self.getConnectLists(debugTester)

            mock_hil.rcConnect = Mock(side_effect=deviceConnectList['rcConnectList'])
            mock_hil.rc3Connect = Mock(side_effect=deviceConnectList['rc3ConnectList'])
            mock_hil.hbiMbConnect = Mock(side_effect=deviceConnectList['hbiMbConnectList'])
            mock_hil_tdaubnk.tdbConnect = Mock(side_effect=deviceConnectList['tdbConnectList'])
            mock_hil_hpcc.hpccDcConnect = Mock(side_effect=deviceConnectList['hpccDcConnectList'])
            mock_hil_hpcc.hpccAcDeviceEnable = Mock(return_value='')
            mock_hil_hddps.hddpsConnect = Mock(side_effect=deviceConnectList['hddpsConnectList'])
            mock_hil_hvdps.hvdpsConnect = Mock(side_effect=deviceConnectList['hvdpsConnectList'])
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=deviceConnectList['hbidpsConnectList'])

            # discover devices on simulated tester
            t = tester.Tester()
            t.is_hbi_tester = Mock(return_value=False)

            t.discover_all_instruments()
            cards_obj = self._create_cards_obj(t)
            card_device_count = (collections.Counter(cards_obj.values()))
            devices_device_list = []
            for device in t.devices:
                if device.name() != 'Rc2':
                    devices_device_list.append(device.name().lower())
            device_device_count = collections.Counter(devices_device_list)

            # check device counts
            self.assertEqual(set(card_device_count.most_common()), set(device_device_count.most_common()))
            self.assertTrue((any(device.name() == 'Rc2' for device in t.devices)), 'RC missing from device list')
    
    def _create_cards_obj(self, tester):
        r = {}
        for device in tester.devices:
            if device.slot is not None:
                r[device.slot] = device.name().lower()
        return r
    
    def createRandomCards(self, deviceList):
        debugTester = []
        for device in range(12):
            debugTester.append(random.choice(deviceList))
        return debugTester

    def getConnectLists(self, debugTester):
        rcConnectList = [''] * 12
        rc3ConnectList = [RuntimeError] * 12
        hbiMbConnectList = [RuntimeError] * 12
        tdbConnectList = [RuntimeError] * 12
        hpccDcConnectList = [RuntimeError] * 12
        hpccAcConnectList = [RuntimeError] * 12
        hddpsConnectList = [RuntimeError] * 24
        hvdpsConnectList = [RuntimeError] * 12
        hvdpsConnectList = [RuntimeError] * 12
        hbidpsConnectList = [RuntimeError] * 12

        slotCount = 0
        for dut in debugTester:
            if dut == 'tdaubnk':
                tdbConnectList[slotCount] = ''
            elif dut == 'hpcc':
                hpccDcConnectList[slotCount] = ''
                hpccAcConnectList[slotCount] = ''
            elif dut == 'hddps':
                hddpsConnectList[slotCount * 2] = ''
                hddpsConnectList[(slotCount * 2) + 1] = ''
            elif dut == 'hvdps':
                hvdpsConnectList[slotCount] = ''
            elif dut == 'hbidps':
                hbidpsConnectList[slotCount] = ''
            slotCount = slotCount + 1

        deviceConnectList = {'rcConnectList': rcConnectList,
                             'rc3ConnectList': rc3ConnectList,
                             'hbiMbConnectList': hbiMbConnectList,
                             'tdbConnectList': tdbConnectList,
                             'hpccDcConnectList': hpccDcConnectList,
                             'hpccAcConnectList': hpccAcConnectList,
                             'hddpsConnectList': hddpsConnectList,
                             'hbidpsConnectList': hbidpsConnectList,
                             'hvdpsConnectList': hvdpsConnectList}
        return deviceConnectList

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
