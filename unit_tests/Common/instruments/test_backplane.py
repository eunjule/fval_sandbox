################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import collections
import itertools
import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    from Tools import projectpaths

from Common import fval
from Common.instruments.backplane import Backplane

    
class BackplaneTests(unittest.TestCase):
    def test_Connect(self):
        with patch('Common.instruments.backplane.hil') as mock_hil:
            mock_hil.bpTiuAuxPowerEnable = Mock()
            backplane = Backplane()
            backplane.Connect()

    def test_PowerEnable(self):
        with patch('Common.instruments.backplane.hil') as mock_hil:
            mock_hil.bpTiuAuxPowerEnable = Mock()
            backplane = Backplane()
            backplane.PowerEnable()

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
