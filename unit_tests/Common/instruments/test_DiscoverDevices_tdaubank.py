################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Common import fval
from Common.instruments import tester
from Tdaubnk.instrument import tdaubnk
from Rc2.instrument import rc2
from Common.instruments.tester import Tester

class DiscoverDevicesTDAUTests(unittest.TestCase):

    def test_create_tester_object(self):
        my_tester = tester.Tester()
        self.mock_rc = lambda x: None

    def test_nothing_on_tester(self):
        with patch('Common.fval.core.Object.Log'), \
            patch('Common.instruments.tester.hil') as mock_hil, \
            patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
            patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
            patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
            patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
            patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk:

            self.remove_rc_instruments(mock_hil, ['rc2', 'rc3', 'hbirctc'])
            mock_hil_hpcc.hpccAcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hpcc.hpccDcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hddps.hddpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_tdaubnk.tdbConnect = Mock(side_effect=RuntimeError)
            mock_hil_hvdps.hvdpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.Log = Mock(side_effect=self.fake_log)

            with self.assertRaises(Tester.NoRcException):
                my_tester.discover_all_instruments()
    
    def fake_log(self, level, message):
        if level.lower() == 'error':
            raise fval.core.LoggedError(message)

    def test_only_rc_on_tester(self):
        with patch('Common.fval.core.Object.Log'), \
            patch('Common.instruments.tester.hil') as mock_hil, \
            patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
            patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
            patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
            patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
            patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk:

            mock_hil.rcConnect = Mock(return_value='')
            self.remove_rc_instruments(mock_hil, ['rc3', 'hbirctc'])
            mock_hil_hpcc.hpccAcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hpcc.hpccDcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hddps.hddpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_tdaubnk.tdbConnect = Mock(side_effect=RuntimeError)
            mock_hil_hvdps.hvdpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.discover_all_instruments()

            self.assertTrue((any(device.name() == 'Rc2' for device in my_tester.devices)),
                            'RC missing from device list')
            self.assertEqual(len(my_tester.devices), 1)
            self.assertIsInstance(my_tester.devices[0], rc2.Rc2)

    def test_found_tdaubnk_on_random_slot(self):
        rc_count = 1
        device_count = 1
        slots = [random.randint(0, 11)]

        def connect(slot):
            if slot not in slots:
                raise RuntimeError

        with patch('Common.fval.core.Object.Log'), \
            patch('Common.instruments.tester.hil') as mock_hil, \
            patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
            patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
            patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
            patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
            patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk:

            mock_hil.rcConnect = Mock(return_value='')
            self.remove_rc_instruments(mock_hil, ['rc3', 'hbirctc'])
            mock_hil_hpcc.hpccAcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hpcc.hpccDcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hddps.hddpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_tdaubnk.tdbConnect = Mock(side_effect=connect)
            mock_hil_hvdps.hvdpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.discover_all_instruments()
            self.assertTrue((any(device.name() == 'Rc2' for device in my_tester.devices)),
                            'RC missing from device list')
            self.assertEqual(device_count + rc_count, len(my_tester.devices))
            for device in my_tester.devices:
                if device.name() != 'Rc2':
                    self.assertIsInstance(device, tdaubnk.Tdaubnk)
                    for slot in slots:
                        self.assertEqual(slot, device.slot)

    def test_found_tdaubnk_on_two_random_slots(self):
        rc_count = 1
        device_count = 2
        slots = random.sample(range(0, 11), 2)

        def connect(slot):
            if slot not in slots:
                raise RuntimeError

        with patch('Common.fval.core.Object.Log'), \
             patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
                patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
                patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk:

            mock_hil.rcConnect = Mock(return_value='')
            self.remove_rc_instruments(mock_hil, ['rc3', 'hbirctc'])
            mock_hil_hpcc.hpccAcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hpcc.hpccDcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hddps.hddpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_tdaubnk.tdbConnect = Mock(side_effect=connect)
            mock_hil_hvdps.hvdpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.discover_all_instruments()
            self.assertTrue((any(device.name() == 'Rc2' for device in my_tester.devices)),
                            'RC missing from device list')
            self.assertEqual(device_count + rc_count, len(my_tester.devices))

            for device in my_tester.devices:
                if device.name() != 'Rc2':
                    self.assertIsInstance(device, tdaubnk.Tdaubnk)
                    self.assertTrue(device.slot in slots)

    def test_found_tdaubnk_on_all_slots(self):
        slots = range(12)

        def connect(slot):
            if slot not in slots:
                raise RuntimeError

        with patch('Common.fval.core.Object.Log'), \
             patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
                patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
                patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk:

            mock_hil.rcConnect = Mock(return_value='')
            self.remove_rc_instruments(mock_hil, ['rc3', 'hbirctc'])
            mock_hil_hpcc.hpccAcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hpcc.hpccDcConnect = Mock(side_effect=RuntimeError)
            mock_hil_hddps.hddpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_tdaubnk.tdbConnect = Mock(side_effect=connect)
            mock_hil_hvdps.hvdpsConnect = Mock(side_effect=RuntimeError)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.discover_all_instruments()

            device_slots = []
            for device in my_tester.devices:
                if device.name() != 'Rc2':
                    self.assertIsInstance(device, tdaubnk.Tdaubnk)
                    device_slots.append(device.slot)
            self.assertEqual(sorted(list(range(12))), sorted(device_slots))

    def remove_rc_instruments(self, hil, rc_list):
        if 'rc2' in rc_list:
            hil.rcConnect = Mock(side_effect=RuntimeError)
        if 'rc3' in rc_list:
            hil.rc3Connect = Mock(side_effect=RuntimeError)
        if 'hbirctc' in rc_list:
            hil.hbiMbConnect = Mock(side_effect=RuntimeError)