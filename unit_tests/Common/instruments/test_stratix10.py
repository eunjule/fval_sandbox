################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest

from Common import stratix10


class Stratix10Tests(unittest.TestCase):

    def test_rctc_raw_to_celsius_conversion_logic(self):
        converted_temperature = stratix10.convert_raw_temp_to_degrees_celcius(0xFFFFE1C0)
        self.assertEqual(converted_temperature,-30.25)

