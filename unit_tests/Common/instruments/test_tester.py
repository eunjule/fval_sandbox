################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import Mock
from unittest.mock import patch

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.insert(0, os.path.abspath(repo_root_path))

from Common import fval
import Common.configs
from Common.instruments import tester
from Common.instruments.dps.hddps import Hddps
from Hpcc.instrument.hpcc import Hpcc
from Rc2.instrument.rc2 import Rc2
from Tdaubnk.instrument.tdaubnk import Tdaubnk
from Hbidps.instrument.hbidps import Hbidps


class TesterTests(unittest.TestCase):
    def setUp(self):
        self.my_rc = lambda x: None

    def test_get_rc_exists(self):
        my_tester = tester.Tester()
        deviceMock = [Rc2()]
        my_tester.devices = deviceMock
        my_rc = my_tester.get_tester_rc()
        self.assertIsInstance(my_rc, Rc2)

    def test_initialize_with_common_init(self):
        with patch.object(tester.Tester, 'get_tester_rc') as mock_get_tester_rc, \
                patch.object(tester.Tester, 'CommonInitialize') as mock_common_init, \
                patch('Common.instruments.tester.Tester.backplane_calbase_init') as mock_backplane_calbase_init, \
                patch('Tools.driver_versions.write_to_log') as mock_driver_versions, \
                patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Common.instruments.tester.configs') as mock_configs:
            mock_configs.COMMON_INITIALIZE = True
            tester.Tester.mock_common_init = Mock()
            mock_hil.rcConnect = Mock(return_value='')
            mock_hil.rc3Connect = Mock(side_effect=RuntimeError)
            mock_hil.hbiMbConnect = Mock(side_effect=RuntimeError)

            my_tester = tester.Tester()
            my_tester.Initialize()

            self.assertEqual(mock_common_init.call_count, 1)
            self.assertEqual(mock_get_tester_rc.call_count, 1)

    def test_initialize_without_common_init(self):
        with patch('Common.instruments.tester.Tester.get_tester_rc') as mock_get_tester_rc, \
            patch('Common.instruments.tester.Tester.CommonInitialize') as mock_common_init, \
            patch('Common.instruments.tester.Tester.backplane_calbase_init') as mock_backplane_calbase_init, \
            patch('Tools.driver_versions.write_to_log') as mock_driver_versions, \
            patch('Common.instruments.tester.hil') as mock_hil, \
            patch('Common.instruments.tester.configs') as mock_configs:

            mock_configs.COMMON_INITIALIZE = False
            mock_hil.rcConnect = Mock(return_value='')
            mock_hil.rc3Connect = Mock(side_effect=RuntimeError)
            mock_hil.hbiMbConnect = Mock(side_effect=RuntimeError)
            my_tester = tester.Tester()
            my_tester.Initialize()
            self.assertEqual(mock_get_tester_rc.call_count, 1)
            self.assertEqual(mock_common_init.call_count, 0)

    def test_initialize_hpcc_backplane_calbase_init(self):
        with patch('Common.instruments.tester.Tester.get_tester_rc') as mock_get_tester_rc, \
                patch('Common.instruments.tester.Tester.CommonInitialize') as mock_common_init, \
                patch('Hpcc.instrument.hpcc.Hpcc.InitializeCal') as mock_initcal, \
                patch('Hpcc.instrument.hpcc.Hpcc.Initialize') as mock_hpcc_initialize, \
                patch('Hpcc.instrument.hpcc.Hpcc.calibration') as mock_hpcc_calibration, \
                patch('Common.models.hpcc.Hpcc.ConnectRcModel') as mock_rcconnect, \
                patch('Tools.driver_versions.write_to_log') as mock_driver_versions, \
                patch('Common.instruments.tester.Tester.backplane_calbase_init') as mock_backplane_calbase_init, \
                patch('Common.instruments.tester.configs') as mock_configs,\
                patch('Common.instruments.tester.hpccConfigs') as mock_hpccconfigs:
            mock_configs.COMMON_INITIALIZE = True
            valid_hpcc_init_configs = ['LOADCAL', 'CAL', 'FULL', 'ZEROCAL']
            mock_hpccconfigs.HPCC_ENV_INIT = random.choice(valid_hpcc_init_configs)
            my_tester = tester.Tester()
            my_tester.is_hbi_tester = Mock(return_value=False)
            my_tester.devices = [Hpcc(1, self.my_rc)]
            my_tester.devices[0].under_test = True
            my_tester.Initialize()
            self.assertTrue(mock_backplane_calbase_init.called)
            self.assertTrue(mock_initcal.called)
            self.assertTrue(mock_rcconnect.called)
            self.assertTrue(mock_hpcc_calibration.called)

    def test_initialize_hpcc_initialize_called(self):
        with patch('Common.instruments.tester.Tester.get_tester_rc') as mock_get_tester_rc, \
                patch('Common.instruments.tester.Tester.CommonInitialize') as mock_common_init, \
                patch('Hpcc.instrument.hpcc.Hpcc.InitializeCal') as mock_initcal, \
                patch('Hpcc.instrument.hpcc.Hpcc.Initialize') as mock_hpcc_initialize, \
                patch('Hpcc.instrument.hpcc.Hpcc.calibration') as mock_hpcc_calibration, \
                patch('Common.models.hpcc.Hpcc.ConnectRcModel') as mock_rcconnect, \
                patch('Tools.driver_versions.write_to_log') as mock_driver_versions, \
                patch('Common.instruments.tester.Tester.backplane_calbase_init') as mock_backplane_calbase_init, \
                patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Common.instruments.tester.configs') as mock_configs,\
                patch('Common.instruments.tester.hpccConfigs') as mock_hpccconfigs:
            mock_configs.COMMON_INITIALIZE = True
            valid_hpcc_init_configs = ['LOADCAL', 'FULL', 'ZEROCAL']
            mock_hpccconfigs.HPCC_ENV_INIT = random.choice(valid_hpcc_init_configs)
            mock_hil.rcConnect = Mock(return_value='')
            mock_hil.rc3Connect = Mock(side_effect=RuntimeError)
            mock_hil.hbiMbConnect = Mock(side_effect=RuntimeError)
            my_tester = tester.Tester()
            my_tester.devices = [Hpcc(1, self.my_rc)]
            my_tester.devices[0].under_test = True
            my_tester.Initialize()
            self.assertTrue(mock_hpcc_initialize.called)

    def test_initialize_hbidps_initialize_called(self):
        with patch('Common.instruments.tester.Tester.get_tester_rc') as mock_get_tester_rc, \
                patch('Common.instruments.tester.Tester.CommonInitialize') as mock_common_init, \
                patch('Common.models.hpcc.Hpcc.ConnectRcModel') as mock_rcconnect, \
                patch('Tools.driver_versions.write_to_log') as mock_driver_versions, \
                patch('Hbidps.instrument.hbidps.Hbidps.Initialize') as mock_hbidps_initialize, \
                patch('Common.instruments.tester.Tester.backplane_calbase_init') as mock_backplane_calbase_init, \
                patch('Hbidps.instrument.hbidps.hil'), \
                patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Common.instruments.tester.configs') as mock_configs:
            mock_configs.COMMON_INITIALIZE = True
            mock_hil.rcConnect = Mock(side_effect=RuntimeError)
            mock_hil.rc3Connect = Mock(side_effect=RuntimeError)
            mock_hil.hbiMbConnect = Mock(return_value='')
            my_tester = tester.Tester()
            my_tester.devices = [Hbidps(1, self.my_rc)]
            my_tester.devices[0].under_test = True
            my_tester.Initialize()
            self.assertTrue(mock_hbidps_initialize.called)

    def test_initialize_hddps_backplane_calbase_init(self):
        with patch('Common.instruments.tester.Tester.get_tester_rc') as mock_get_tester_rc, \
                patch('Common.models.hddps.Hddps.ConnectRcModel') as mock_hddps_sim_connect, \
                patch('Common.instruments.tester.Tester.CommonInitialize') as mock_common_init, \
                patch('Common.instruments.tester.Tester.backplane_calbase_init') as mock_backplane_calbase_init, \
                patch('Common.instruments.dps.hddps.Hddps.Initialize') as mock_hddps_initialize, \
                patch('Tools.driver_versions.write_to_log') as mock_driver_versions, \
                patch('Common.instruments.tester.configs') as mock_configs:
            mock_configs.COMMON_INITIALIZE = True
            my_tester = tester.Tester()
            my_tester.is_hbi_tester = Mock(return_value=False)
            my_tester.devices = [Hddps(1, self.my_rc)]
            my_tester.devices[0].under_test = True
            my_tester.Initialize()
            self.assertTrue(mock_backplane_calbase_init.called)

    def test_backplane_calbase_initRCTIUPass(self):
        with patch('Common.instruments.backplane.hil'), \
                patch('Common.instruments.cb2.hil'), \
                patch('Common.instruments.pca9505.hil'), \
                patch('time.sleep'), \
                patch.object(Common.configs, 'CALBASE_IGNORE', False), \
                patch('Rc2.instrument.rc2.hil') as mock_rc2:

            my_tester = tester.Tester()
            my_tester.Log = Mock()
            my_tester.get_tester_rc = Mock(return_value=Rc2)
            mock_rc2.rcBarRead = Mock(return_value=0x00000000)
            mock_rc2.cypDeviceOpen = Mock(return_value='HS_SUCCESS')
            mock_rc2.cypPortRead = Mock(return_value=0x2f)
            my_tester.is_tiu_physically_connected = Mock(return_value=True)
            my_tester.backplane_calbase_init()

            expected_message = 'TIU detected by RCTC status register'
            expected_level = 'info'
            for log_info, kwargs in my_tester.Log.call_args_list:
                log_level, log_message = log_info
                if (log_message == expected_message) and (log_level == expected_level):
                    break
            else:
                self.fail(f'"{expected_message} with log level of {expected_level}" not found.')

    def test_backplane_calbase_initRCTIUFail(self):
        with patch('Common.instruments.backplane.hil'), \
                patch('Common.instruments.cb2.hil'), \
                patch('Common.instruments.pca9505.hil'), \
                patch('time.sleep'), \
                patch.object(Common.configs, 'CALBASE_IGNORE', False), \
                patch('Rc2.instrument.rc2.hil') as mock_rc2:
            my_tester = tester.Tester()
            my_tester.get_tester_rc = Mock(return_value=Rc2)
            mock_rc2.rcBarRead = Mock(return_value=0x0000001)
            mock_rc2.cypDeviceOpen = Mock(return_value='HS_SUCCESS')
            mock_rc2.cypPortRead = Mock(return_value=0x2f)
            my_tester.is_tiu_physically_connected = Mock(return_value=False)
            with self.assertRaises(fval.LoggedError):
                my_tester.backplane_calbase_init()

    def test_backplane_calbase_skip_if_ignore(self):
        with patch('Common.instruments.backplane.hil'), \
                patch('Common.instruments.cb2.hil'), \
                patch('Common.instruments.pca9505.hil'), \
                patch('time.sleep'), \
                patch.object(Common.configs, 'CALBASE_IGNORE', True), \
                patch('Rc2.instrument.rc2.hil') as mock_rc2:
            my_tester = tester.Tester()
            fval.core.Log = Mock()
            my_tester.get_tester_rc = Mock(return_value=Rc2)
            mock_rc2.rcBarRead = Mock(return_value=0x0000001)
            mock_rc2.cypDeviceOpen = Mock(return_value='HS_SUCCESS')
            mock_rc2.cypPortRead = Mock(return_value=0x2f)
            my_tester.is_tiu_physically_connected = Mock(return_value=True)
            my_tester.backplane_calbase_init()

            expected_message = 'Skipping Cal Base init'
            expected_level = 'info'
            for log_info, kwargs in fval.core.Log.call_args_list:
                log_level, log_message = log_info
                if (log_message == expected_message) and (
                        log_level == expected_level):
                    break
            else:
                self.fail(
                    f'"{expected_message} with log level of {expected_level}" '
                    f'not found.')

    def test_get_hpcc_instruments(self):
        my_tester = tester.Tester()
        my_tester.devices = [Hpcc(1, self.my_rc), Hpcc(2, self.my_rc), Hddps(3, self.my_rc), Hpcc(4, self.my_rc)]
        my_tester.devices[0].under_test = True
        my_tester.devices[1].under_test = True
        my_tester.devices[2].under_test = True

        my_hpcc_instruments = my_tester.get_undertest_instruments('Hpcc')

        for hpcc in my_hpcc_instruments:
            self.assertIsInstance(hpcc, Hpcc)

        self.assertEqual(len(my_hpcc_instruments), 2)

    def test_get_hpcc_instruments_empty(self):
        my_tester = tester.Tester()
        my_tester.devices = [Hpcc(1, self.my_rc), Hpcc(2, self.my_rc), Hddps(3, self.my_rc), Hpcc(4, self.my_rc)]
        my_tester.devices[0].under_test = False
        my_tester.devices[1].under_test = False
        my_tester.devices[2].under_test = True

        my_hpcc_instruments = my_tester.get_undertest_instruments('Hpcc')

        self.assertEqual(len(my_hpcc_instruments), 0)

    def test_get_hddps_instruments(self):
        my_tester = tester.Tester()
        my_tester.devices = [Hddps(1, self.my_rc), Hddps(2, self.my_rc), Hpcc(3, self.my_rc), Hddps(4, self.my_rc)]
        my_tester.devices[0].under_test = True
        my_tester.devices[1].under_test = True
        my_tester.devices[2].under_test = True

        my_hddps_instruments = my_tester.get_undertest_instruments('Hddps')

        for hddps in my_hddps_instruments:
            self.assertIsInstance(hddps, Hddps)

        self.assertEqual(len(my_hddps_instruments), 2)

    def test_get_hddps_instruments_empty(self):
        my_tester = tester.Tester()
        my_tester.devices = [Hddps(1, self.my_rc), Hddps(2, self.my_rc), Hpcc(3, self.my_rc), Hddps(4, self.my_rc)]
        my_tester.devices[0].under_test = False
        my_tester.devices[1].under_test = False
        my_tester.devices[2].under_test = True

        my_hddps_instruments = my_tester.get_undertest_instruments('Hddps')

        self.assertEqual(len(my_hddps_instruments), 0)

    def test_get_tdaubnk_undertest_instruments(self):
        my_tester = tester.Tester()
        my_tester.devices = [Tdaubnk(1, self.my_rc), Tdaubnk(2, self.my_rc), Hpcc(3, self.my_rc),
                             Tdaubnk(4, self.my_rc)]
        my_tester.devices[0].under_test = True
        my_tester.devices[1].under_test = True
        my_tester.devices[2].under_test = True

        my_tdaubnk_instruments = my_tester.get_undertest_instruments('Tdaubnk')

        for tdaubnk in my_tdaubnk_instruments:
            self.assertIsInstance(tdaubnk, Tdaubnk)

        self.assertEqual(len(my_tdaubnk_instruments), 2)

    def test_get_tdaubnk_undertest_instruments_empty(self):
        my_tester = tester.Tester()
        my_tester.devices = [Tdaubnk(1, self.my_rc), Tdaubnk(2, self.my_rc), Hpcc(3, self.my_rc),
                             Tdaubnk(4, self.my_rc)]
        my_tester.devices[0].under_test = False
        my_tester.devices[1].under_test = False
        my_tester.devices[2].under_test = True

        my_tdaubnk_instruments = my_tester.get_undertest_instruments('Tdaubnk')

        self.assertEqual(len(my_tdaubnk_instruments), 0)

    def test_get_undertest_dps_instruments_two_dev_one_dps_udndertest(self):
        my_tester = tester.Tester()
        my_tester.devices = [Tdaubnk(2, self.my_rc), Hddps(0, self.my_rc)]
        my_tester.devices[0].under_test = False
        my_tester.devices[1].under_test = True
        my_undertest_dps_instruments = my_tester.get_undertest_dps_instruments()
        self.assertEqual(len(my_undertest_dps_instruments), 1, 'Should only have one device in undertest list')
        for device in my_undertest_dps_instruments:
            self.assertIsInstance(device, Hddps, 'Expected devices in undertest list can only be DPS devices')

    def test_get_undertest_dps_instruments_two_dev_one_dps_udndertest_one_non_dps_ut(self):
        my_tester = tester.Tester()
        my_tester.devices = [Tdaubnk(2, self.my_rc), Hddps(0, self.my_rc)]
        my_tester.devices[0].under_test = True
        my_tester.devices[1].under_test = True
        my_undertest_dps_instruments = my_tester.get_undertest_dps_instruments()
        self.assertEqual(len(my_undertest_dps_instruments), 1, 'Should only have one device in undertest list')
        for device in my_undertest_dps_instruments:
            self.assertIsInstance(device, Hddps, 'Expected devices in undertest list can only be DPS devices')

    def test_get_undertest_dps_instruments_two_dev_one_dps_not_udndertest_one_non_dps_ut(self):
        my_tester = tester.Tester()
        my_tester.devices = [Tdaubnk(2, self.my_rc), Hddps(0, self.my_rc)]
        my_tester.devices[0].under_test = True
        my_tester.devices[1].under_test = False
        my_tester.Log = Mock()
        my_tester.get_undertest_dps_instruments()
        self._assert_error_logged(my_tester.Log.call_args_list)

    def _assert_error_logged(self, args_list):
        log_levels = [args[0].lower() for args, kwargs in args_list]
        log_levels += [kwargs.get('level', '').lower() for args, kwargs in args_list]
        self.assertIn('error', log_levels)

    def test_get_undertest_dps_instruments_two_dev_one_dps_both_not_ut(self):
        my_tester = tester.Tester()
        my_tester.devices = [Tdaubnk(2, self.my_rc), Hddps(0, self.my_rc)]
        my_tester.devices[0].under_test = False
        my_tester.devices[1].under_test = False
        my_tester.Log = Mock()
        my_tester.get_undertest_dps_instruments()
        self._assert_error_logged(my_tester.Log.call_args_list)

    def test_get_undertest_dps_instruments_no_devices(self):
        my_tester = tester.Tester()
        my_tester.devices = []
        my_tester.Log = Mock()
        my_tester.get_undertest_dps_instruments()
        self._assert_error_logged(my_tester.Log.call_args_list)

    def test_get_undertest_two_dps_instruments_both_udndertest(self):
        my_tester = tester.Tester()
        my_tester.devices = [Hddps(0, self.my_rc), Hddps(1, self.my_rc)]
        my_tester.devices[0].under_test = True
        my_tester.devices[1].under_test = True
        my_undertest_dps_instruments = my_tester.get_undertest_dps_instruments()
        self.assertEqual(len(my_undertest_dps_instruments), 2, 'Should have two device in undertest list')
        for device in my_undertest_dps_instruments:
            self.assertIsInstance(device, Hddps, 'Expected devices in undertest list can only be DPS devices')

    def test_get_undertest_two_dps_instruments_one_udndertest(self):
        my_tester = tester.Tester()
        my_tester.devices = [Hddps(0, self.my_rc), Hddps(1, self.my_rc)]
        my_tester.devices[0].under_test = False
        my_tester.devices[1].under_test = True
        my_undertest_dps_instruments = my_tester.get_undertest_dps_instruments()
        self.assertEqual(len(my_undertest_dps_instruments), 1, 'Should only have one device in undertest list')
        for device in my_undertest_dps_instruments:
            self.assertIsInstance(device, Hddps, 'Expected devices in undertest list can only be DPS devices')
            self.assertEqual(device.slot, 1)


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
