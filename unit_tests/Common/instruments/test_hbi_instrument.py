################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock

from Common.instruments.hbi_instrument import HbiInstrument


class TestHbiInstrument(unittest.TestCase):

    def test_is_non_zero_or_non_f(self):
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['']), False)
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['1']), True)
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['0']), False)
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['000000']), False)
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['ff']), False)
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['FF']), False)
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['fFfF']), False)
        with self.assertRaises(HbiInstrument.InvalidDigitsError):
            HbiInstrument.is_non_zero_or_non_f(['hello'])
        self.assertEqual(HbiInstrument.is_non_zero_or_non_f(['0000000001']), True)
        with self.assertRaises(HbiInstrument.InvalidDigitsError):
            HbiInstrument.is_non_zero_or_non_f(['1zzzz111zz'])

    def test_log_fpga_version(self):
        from Hbirctc.instrument.hbirctc import Hbirctc

        dut = Hbirctc()
        original_log = dut.Log
        original_get_fpga_version_string = dut.fpga_version_string
        dut.Log = Mock()

        fail_values = ['0.0.0', '0.0', '0.0-0', '65535.65535']
        for fail_value in fail_values:
            dut.fpga_version_string = Mock(return_value=fail_value)
            dut.log_fpga_version()
            dut.fpga_version_string.reset_mock()
            self.validate_log_message(dut, f'{dut.name()}: FPGA Version read all 0\' or all f\'s {fail_value}', 'warning')

        pass_values = ['3.0.2', '5.1-3', '0.1', '1.0']
        for pass_value in pass_values:
            dut.fpga_version_string = Mock(return_value=pass_value)
            dut.log_fpga_version()
            dut.fpga_version_string.reset_mock()
            self.validate_log_message(dut, f'{dut.name()}: FPGA Version {pass_value}', 'info')

        dut.Log = original_log
        dut.fpga_version_string = original_get_fpga_version_string

    def validate_log_message(self, dut, expected_message, expected_level):
        for log_info, kwargs in dut.Log.call_args_list:
            log_level, log_message = log_info
            if (log_message == expected_message) and (log_level == expected_level):
                break
        else:
            self.fail(f'"{expected_message} with log level of {expected_level}" not found.')
