################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import Mock
from unittest.mock import patch

from Common import fval
from Hbidps.instrument import hbidps


if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


class HbidpsInstrumentTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None

    def test_discover_Hbidps_found(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            slot = random.randint(0,11)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=[''])
            hbidps_instrument = hbidps.Hbidps(slot = slot, rc= self.mock_rc)
            hbidps_return = hbidps_instrument.discover()
            self.assertIsInstance(hbidps_return, hbidps.Hbidps)

    def test_discover_Hbidps_not_found(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            slot = random.randint(0, 11)
            mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=RuntimeError)
            hbidps_instrument = hbidps.Hbidps(slot=slot, rc=self.mock_rc)
            hbidps_return = hbidps_instrument.discover()
            self.assertEqual(hbidps_return, None)

    def test_log_blt_info(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.instrumentblt = Mock()
        hbidps_instrument.main_boardblt = Mock()
        hbidps_instrument.daughter_boardblt = Mock()
        hbidps_instrument.log_blt_info()
        if not hbidps_instrument.instrumentblt.write_to_log.called :
            self.fail('Instrument BLT not logged')

    def test_hbidps_Initialize(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.initialize_hbi_dps= Mock()
        hbidps_instrument.log_blt_info= Mock()
        hbidps_instrument.ddr_reset= Mock()
        hbidps_instrument.dps_available_list= Mock()
        hbidps_instrument.log_cpld_info= Mock()
        hbidps_instrument.are_clpd_versions_same= Mock()
        hbidps_instrument.initialize_hbi_dps_board = Mock()

        hbidps_instrument.Initialize()

        self.assertEqual(hbidps_instrument.initialize_hbi_dps.call_count, 1)
        self.assertEqual(hbidps_instrument.log_blt_info.call_count, 1)
        self.assertEqual(hbidps_instrument.ddr_reset.call_count, 1)
        self.assertEqual(hbidps_instrument.dps_available_list.call_count, 1)
        self.assertEqual(hbidps_instrument.log_cpld_info.call_count,1)
        self.assertEqual(hbidps_instrument.are_clpd_versions_same.call_count, 1)
        self.assertEqual(hbidps_instrument.initialize_hbi_dps_board.call_count, 1)

    def test_hbidps_initialize_bad_image_name(self):
        for HBIDPS_FPGA_IMAGE_NAME in ['invalid','','hbdps.bin']:
            hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
            with self.assertRaises(fval.LoggedError):
                hbidps_instrument.FpgaLoad(HBIDPS_FPGA_IMAGE_NAME)

    def test_hbidps_initialize_correct_image_name(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil,\
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            for HBIDPS_FPGA_IMAGE_NAME in ['HBI_DPS.bin', 'dummy_HBI_DPS_image', 'hbi_dps.bin']:
                mock_hil.hbiDpsFpgaLoad = Mock(return_value=True)
                hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
                load_result = hbidps_instrument.FpgaLoad(HBIDPS_FPGA_IMAGE_NAME)
                self.assertEqual(load_result, True)

    def test_load_fpga_if_config_option_is_true(self):
        with patch('Hbidps.instrument.hbidps.configs') as mock_configs:
            hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
            mock_configs.HBIDPS_FPGA_IMAGE_LOAD = True
            hbidps_instrument.FpgaLoad = Mock()
            hbidps_instrument.fpga_version_string = Mock(return_value = 0x0)
            hbidps_instrument.load_fpga()
            self.assertEqual(hbidps_instrument.FpgaLoad.call_count, 1)

    def test_load_fpga_if_config_option_is_false(self):
        with patch('Hbidps.instrument.hbidps.configs') as mock_configs:
            hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
            mock_configs.HBIDPS_FPGA_IMAGE_LOAD = False
            hbidps_instrument.FpgaLoad = Mock()
            hbidps_instrument.fpga_version_string = Mock(return_value = 0x0)
            hbidps_instrument.load_fpga()
            self.assertEqual(hbidps_instrument.FpgaLoad.call_count, 0)

    def test_initialize_trigger_link_if_status_is_not_correct(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.reset_rc_aurora_bus = Mock()
        hbidps_instrument.dps_slot_list = [3,5]
        hbidps_instrument.clear_dps_trigger_buffer = Mock()
        hbidps_instrument.reset_dps_aurora_link = Mock()
        aurora_status = hbidps_instrument.registers.STATUS
        aurora_status.value = 0x26
        aurora_error_count = hbidps_instrument.registers.AURORA_ERROR_COUNTS
        aurora_error_count.value = 0
        slot = 3
        hbidps_instrument.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
        hbidps_instrument.Log = Mock()
        hbidps_instrument.retry_count = 2
        hbidps_instrument.initialize_trigger_link()
        log_calls = hbidps_instrument.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'Aurora link not up from DPS for slot 3 at iteration 0. Aurora status :0x6 Aurora error count :0x0',
                             'Log not displaying correct information')
            break

    def test_initialize_trigger_link_if_error_count_non_zero(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.reset_rc_aurora_bus = Mock()
        hbidps_instrument.dps_slot_list = [3,5]
        hbidps_instrument.clear_dps_trigger_buffer = Mock()
        hbidps_instrument.reset_dps_aurora_link = Mock()
        hbidps_instrument.reset_dps_error_count = Mock()
        aurora_status = hbidps_instrument.registers.STATUS
        aurora_status.value = 0x0
        aurora_error_count = hbidps_instrument.registers.AURORA_ERROR_COUNTS
        aurora_error_count.value = 10
        slot = 3
        hbidps_instrument.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
        hbidps_instrument.Log = Mock()
        hbidps_instrument.retry_count = 2
        hbidps_instrument.initialize_trigger_link()
        log_calls = hbidps_instrument.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'Aurora link not up from DPS for slot 3 at iteration 0. Aurora status :0x0 Aurora error count :0xa',
                             'Log not displaying correct information')
            break

    def test_initialize_trigger_link_if_link_up(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.reset_rc_aurora_bus = Mock()
        hbidps_instrument.dps_slot_list = [3,5]
        hbidps_instrument.clear_dps_trigger_buffer = Mock()
        hbidps_instrument.reset_dps_aurora_link = Mock()
        aurora_status = hbidps_instrument.registers.STATUS
        aurora_status.value = 0x0
        aurora_error_count = hbidps_instrument.registers.AURORA_ERROR_COUNTS
        aurora_error_count.value = 0
        slot = 3
        hbidps_instrument.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
        hbidps_instrument.Log = Mock()
        hbidps_instrument.retry_count = 2
        hbidps_instrument.initialize_trigger_link()
        log_calls = hbidps_instrument.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'Aurora link up from DPS at slot 3 at iteration 0. Aurora status :0x0 Aurora error count :0x0',
                             'Log not displaying correct information')
            break

    def test_initialize_trigger_link_if_link_fails_to_come_up(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.reset_rc_aurora_bus = Mock()
        hbidps_instrument.dps_slot_list = [3,5]
        hbidps_instrument.clear_dps_trigger_buffer = Mock()
        hbidps_instrument.reset_dps_aurora_link = Mock()
        hbidps_instrument.reset_dps_error_count = Mock()
        aurora_status = hbidps_instrument.registers.STATUS
        aurora_status.value = 0x0
        aurora_error_count = hbidps_instrument.registers.AURORA_ERROR_COUNTS
        aurora_error_count.value = 10
        slot = 3
        hbidps_instrument.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
        hbidps_instrument.Log = Mock()
        hbidps_instrument.retry_count = 2
        hbidps_instrument.initialize_trigger_link()
        log_calls = hbidps_instrument.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
        self.assertEqual(log_level.upper(), 'ERROR')
        self.assertRegex(log_message, 'Aurora link failed to come up.',
                         'Log not displaying correct information')

    def test_are_clpd_versions_same_fail(self):
        hbidps_instrument = hbidps.Hbidps(slot=0, rc=self.mock_rc)
        hbidps_instrument.Log = Mock()
        hbidps_instrument.slot_cpld_pair = {1: 'h006', 4: 'h007', 6: 'h007'}
        hbidps_instrument.are_clpd_versions_same()
        log_calls = hbidps_instrument.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'CPLD versions mismatch.',
                             'Log not displaying correct information')


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)