################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import ctypes
import unittest
import unittest.mock as Mock

import Common.register as register


class RegisterTests(unittest.TestCase) :
    def test_setting_field_in_constructor(self):
        class Foo(register.Register):
            _fields_ = [('bit_0', ctypes.c_uint, 1),
                ('bits_31_1', ctypes.c_uint, 31)]
        
        foo = Foo(bit_0=1, bits_31_1=42)
        self.assertEqual(foo.bit_0, 1)
        self.assertEqual(foo.bits_31_1, 42)
        
        with self.assertRaises(AttributeError):
            foo2 = Foo(nonexistent_field=28)
        
        foo3 = Foo(value=0xFFFFFFFF, bit_0=0)
        self.assertEqual(foo3.value, 0xFFFFFFFE)
        
    def test_using_valid_index_in_address_method(self):
        class Foo(register.Register):
            BAR = 1
            ADDR = 0x0
            REGCOUNT = 8
            BASEMUL = 32
            _fields_ = [('data', ctypes.c_uint, 32)]
        foo = Foo(data=0)
        expected_address = [i for i in range(0, Foo.REGCOUNT*Foo.BASEMUL, Foo.BASEMUL)]

        invalid_min_tested = False
        invalid_max_tested = False
        for i in range(-1, Foo.REGCOUNT+2):
            if i in range(0, len(expected_address)):
                self.assertEqual(expected_address[i], foo.address(i))
            else:
                if i < 0:
                    invalid_min_tested = True
                if i >= Foo.REGCOUNT:
                    invalid_max_tested = True
                with self.assertRaises(register.Register.IndexOutOfRangeError):
                    foo.address(i)
            i += 1
        self.assertEqual(invalid_max_tested, True)
        self.assertEqual(invalid_min_tested, True)

if __name__ == '__main__':
    unittest.main()
