################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch
from unittest.mock import Mock

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.insert(0, os.path.abspath(repo_root_path))

from Common import fval
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo
from Tools import projectpaths

class BltInfotest(unittest.TestCase) :
    def test_call_back_is_called_only_once(self):
        hil_blt = hil.BLT()
        hil_blt.VendorName = 'Intel'
        hil_blt.DeviceName = 'Ehcdps'
        hil_blt.PartNumberAsBuilt = '12334'
        hil_blt.PartNumberCurrent ='12345'
        hil_blt.SerialNumber = '56789'
        hil_blt.ManufactureDate = '08/23/2017'
        call_back = Mock(return_value=hil_blt)

        blt = CachingBltInfo(callback_function=call_back)
        self.assertEquals(call_back.call_count,0)
        blt.write_to_log()
        self.assertEquals(call_back.call_count,1)
        blt.write_to_log()
        self.assertEquals(call_back.call_count, 1)

        vendorname = blt.VendorName
        self.assertEquals(call_back.call_count, 1)


    def test_ensure_serial_number_is_printed_at_info_level_to_the_log(self):
        hil_blt = hil.BLT()
        test_serial_number = 'UNITSERIALNO'
        hil_blt.SerialNumber = test_serial_number
        call_back = Mock(return_value=hil_blt)
        blt = CachingBltInfo(callback_function=call_back)
        blt.Log = Mock()
        blt.write_to_log()
        log_calls = blt.Log.call_args_list
        self.find_logcall_containing_string_with_loglevel(log_calls, test_serial_number, 'INFO')
    
    def test_get_fab_letter(self):
        for digit, letter in [(1, 'A'), (2, 'B'), (3, 'C'), (4, 'D'), (9, 'I')]:
            hil_blt = hil.BLT()
            hil_blt.VendorName = 'Intel'
            hil_blt.DeviceName = 'Ehcdps'
            hil_blt.PartNumberAsBuilt = '12334'
            hil_blt.PartNumberCurrent = f'AAK12345-{digit}02'
            hil_blt.SerialNumber = '56789'
            hil_blt.ManufactureDate = '08/23/2017'
            call_back = Mock(return_value=hil_blt)
            blt = CachingBltInfo(callback_function=call_back)
            fab_letter = blt.fab_letter()
            self.assertEquals(fab_letter, letter)
    
    def test_get_eco(self):
        for eco in [0, 1, 2, 30, 99]:
            hil_blt = hil.BLT()
            hil_blt.VendorName = 'Intel'
            hil_blt.DeviceName = 'Ehcdps'
            hil_blt.PartNumberAsBuilt = '12334'
            hil_blt.PartNumberCurrent = f'AAK12345-1{eco:02d}'
            hil_blt.SerialNumber = '56789'
            hil_blt.ManufactureDate = '08/23/2017'
            call_back = Mock(return_value=hil_blt)
            blt = CachingBltInfo(callback_function=call_back)
            eco_number = blt.eco_number()
            self.assertEquals(eco_number, eco)

    def find_logcall_containing_string_with_loglevel(self, log_calls, match_string, log_level):
        for call in log_calls:
            args, kwargs = call
            call_log_level, message = args
            if match_string in message:
                break
        else:
            self.fail('{} not found in any call'.format(match_string))
        self.assertEqual(call_log_level.upper(), log_level)


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)