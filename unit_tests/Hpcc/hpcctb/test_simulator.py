################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_simulator.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the simulator components
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 05/12/15
#       Group: HDMT FPGA Validation
################################################################################

import unittest

from _build.bin import hpcctbc

class BfmsTests(unittest.TestCase):
    def test_InternalLoopbackBfm(self):
        bfm = hpcctbc.InternalLoopbackBfm()
        values = set()
        for slot in range(12):
            for slice in range(2):
                for ch in range(0, 56):
                    value = bfm.MapChannel(slot, slice, ch)
                    # Rule 1: all values must be unique
                    self.assertNotIn(value, values)
                    values.add(value)

    def test_EvenToOddLoopbackBfm(self):
        bfm = hpcctbc.EvenToOddLoopbackBfm()
        values = set()
        for slot in range(12):
            for slice in range(2):
                for ch in range(0, 56, 2):
                    even = bfm.MapChannel(slot, slice, ch)
                    odd  = bfm.MapChannel(slot, slice, ch + 1)
                    # Rule 1: even and odd channels must map to the same value
                    self.assertEqual(even, odd)
                    # Rule 2: all values must be unique
                    self.assertNotIn(even, values)
                    values.add(even)

    def test_LowToHighLoopbackBfm(self):
        bfm = hpcctbc.LowToHighLoopbackBfm()
        values = set()
        for slot in range(12):
            for ch in range(56):
                low  = bfm.MapChannel(slot, 0, ch)
                high = None
                if ch % 2 == 0:  # even
                    high = bfm.MapChannel(slot, 1, ch + 1)
                else:  # odd
                    high = bfm.MapChannel(slot, 1, ch - 1)
                # Rule 1: low and high channels must map to the same value
                self.assertEqual(low, high)
                # Rule 2: all values must be unique
                self.assertNotIn(low, values)
                values.add(low)


class WaveformTests(unittest.TestCase):
    def test_DriveSampleAt(self):
        wave = hpcctbc.HpccWaveform()
        self.assertEqual(wave.Size(), 0)
        wave.DriveAt(0.0, '0')
        wave.DriveAt(0.5, 'z')
        wave.DriveAt(1.0, '1')
        self.assertEqual(wave.Size(), 3)
        self.assertEqual(wave.SampleAt(-0.1), 'x')
        self.assertEqual(wave.SampleAt(0.1), '0')
        self.assertEqual(wave.SampleAt(0.6), 'z')
        self.assertEqual(wave.SampleAt(0.9), 'z')
        self.assertEqual(wave.SampleAt(1.1), '1')
    def test_OutOfOrderDriveSampleAt(self):
        wave = hpcctbc.HpccWaveform()
        self.assertEqual(wave.Size(), 0)
        wave.DriveAt(1.0, '1')
        wave.DriveAt(0.5, 'z')
        wave.DriveAt(0.0, '0')
        self.assertEqual(wave.Size(), 3)
        self.assertEqual(wave.SampleAt(-0.1), 'x')
        self.assertEqual(wave.SampleAt(0.1), '0')
        self.assertEqual(wave.SampleAt(0.6), 'z')
        self.assertEqual(wave.SampleAt(0.9), 'z')
        self.assertEqual(wave.SampleAt(1.1), '1')
    def test_AdvanceTo(self):
        wave = hpcctbc.HpccWaveform()
        # Put stuff on the wave
        wave.DriveAt(0.0, '0')
        wave.DriveAt(0.5, 'z')
        wave.DriveAt(1.0, '1')
        # Advance to different points in the wave
        wave.AdvanceTo(-0.1)
        self.assertEqual(wave.Size(), 3)
        self.assertEqual(wave.SampleAt(0.1), '0')
        self.assertEqual(wave.SampleAt(0.9), 'z')
        self.assertEqual(wave.SampleAt(1.1), '1')
        wave.AdvanceTo(0.1)
        self.assertEqual(wave.Size(), 3)
        self.assertEqual(wave.SampleAt(0.2), '0')
        self.assertEqual(wave.SampleAt(0.9), 'z')
        self.assertEqual(wave.SampleAt(1.1), '1')
        wave.AdvanceTo(0.6)
        self.assertEqual(wave.Size(), 2)
        self.assertEqual(wave.SampleAt(0.9), 'z')
        self.assertEqual(wave.SampleAt(1.1), '1')
        wave.AdvanceTo(1.1)
        self.assertEqual(wave.Size(), 1)
        self.assertEqual(wave.SampleAt(1.2), '1')
    def test_CornerCases(self):
        wave = hpcctbc.HpccWaveform()
        # Put stuff on the wave, fill it up
        for i in range(1024 * 2):
            if i == 0:
                self.assertEqual(wave.DriveAt(float(i), 'A'), True)
            elif i == 1:
                self.assertEqual(wave.DriveAt(float(i), 'B'), True)
            else:
                self.assertEqual(wave.DriveAt(float(i), 'C'), True)
        # Try to write pass the end of the wave
        self.assertEqual(wave.DriveAt(100.0, '0'), False)
        self.assertEqual(wave.SampleAt(0.0), 'A')
        self.assertEqual(wave.SampleAt(0.01), 'A')
        self.assertEqual(wave.SampleAt(0.99), 'A')
        self.assertEqual(wave.SampleAt(1.0), 'B')
        self.assertEqual(wave.SampleAt(1.01), 'B')

