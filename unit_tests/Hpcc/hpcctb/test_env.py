################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import patch

from Hpcc.hpcctb.env import Env
from Common.instruments.tester import Tester
from Hpcc.instrument.hpcc import Hpcc


class HpccEnvTests(unittest.TestCase):
    def setUp(self):
        self._tester = Tester()
        self._env = Env(test=None)
        self._env.tester = self._get_new_tester
        
    def test_get_hpcc_ac_fpgas(self):
        self.mock_rc = lambda x: None
        with patch('Hpcc.hpcctb.env.Env') as mock_env:
            mock_env.instruments = {1: Hpcc(1, rc=self.mock_rc)}
            fpgas = Env.get_hpcc_ac_fpgas(mock_env)
            self.assertEqual([(1, 0), (1, 1)], fpgas)

    def _get_new_tester(self):
        return self._tester
        
    def _get_env(self):
        env = Env(test=None)
        env.tester = self._get_new_tester
        return env
        
    def test_tester_is_singleton_normally(self):
        env = Env(test=None)
        t = env.tester()
        u = env.tester()
        self.assertIs(t, u)
        self.assertIsInstance(t, Tester)
        
    def test_tester_is_singleton_when_replaced(self):
        t = self._env.tester()
        u = self._env.tester()
        self.assertIs(t, u)
        self.assertIsInstance(t, Tester)
        
    def test_tester_can_be_replaced_inside_a_test(self):
        t = self._env.tester()
        self._tester = Tester()
        u = self._env.tester()
        self.assertIsNot(t, u)
        

        
