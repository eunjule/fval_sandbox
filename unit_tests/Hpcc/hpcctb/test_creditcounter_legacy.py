################################################################################
# INTEL CONFIDENTIAL - Copyright 2018-2019. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import sys
import unittest

sys.path.insert(0, os.path.abspath('../../..'))

from Hpcc.hpcctb import creditcounter_legacy as creditcounter
from ThirdParty.teamcity import is_running_under_teamcity


class CreditCounterTests(unittest.TestCase):
    def test_period_too_low(self):
        with self.assertRaises(RuntimeError):
            creditcounter.CreditCounter(1e-9)

    def test_balance_too_low(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        with self.assertRaises(RuntimeError):
            counter.Instruction()

    def test_balance_goes_to_zero(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        counter.Vector()
        result = counter.Instruction()
        self.assertEqual(result, 0)

    def test_vector_with_local_repeat(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        counter.Vector(3)
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        with self.assertRaises(RuntimeError):
            counter.Instruction()

        self.assertEqual(counter.GetUnderrunAmount(), 1)

    def test_vector_with_local_repeat_and_link(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        counter.Vector(3, 2)
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        counter.Instruction()
        with self.assertRaises(RuntimeError):
            counter.Instruction()

    def test_repeat_instruction(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        with self.assertRaises(RuntimeError):
            counter.InstructionRepeat(200)

    def test_branch_instruction(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        with self.assertRaises(RuntimeError):
            counter.Branch()

if __name__ == '__main__':
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        runner = unittest.TextTestRunner()

    unittest.main(testRunner=runner)
