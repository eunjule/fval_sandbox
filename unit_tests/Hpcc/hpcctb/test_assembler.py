################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_assembler.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for the assembler module
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 04/28/15
#       Group: HDMT FPGA Validation
################################################################################

import base64
import binascii
import random
import unittest

from Hpcc.hpcctb.assembler import PatternAssembler


class PatternAssemblerTests(unittest.TestCase):
    def testDriveCompareLow(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0, data=0vL0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0L0
        """)
        data = asm.Generate()
        # Compare encoded data
        expectedData = b'\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\x00\x00' +\
                       b'\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\xAA\x00\x04' +\
                       b'\x00' * 224
        self.assertEqual(data, expectedData)
        
    def testMetadata(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            M data=0
        """)
        data = asm.Generate()
        # Compare encoded data
        expectedData =   b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40' + b'\x00' * 240
        self.assertEqual(data, expectedData)
        
    def testVectors(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            V sce=0, link=1, ctv=0, mtv=5, lrpt=0, data=0v01010100101010111000111100100111001010100101010111000111
            V sce=0, link=0, ctv=1, mtv=7, lrpt=15, data=0v00010101010000010100111111100100101001001001010100101010
            V sce=1, link=1, ctv=0, mtv=5, lrpt=0, data=0v01010100101010111000111100100111001010100101010111000111
            V sce=1, link=0, ctv=1, mtv=7, lrpt=15, data=0v00010101010000010100111111100100101001001001010100101010
        """)
        data = asm.Generate()
        data1 = 0X02A0BBBAEEEFEAFFAEBFAEEEBBBBFABF
        data2 = 0X01EFABBBBAABBAFFFEBAEEBAEBBBAEEE
        data3 = 0X06A0BBBAEEEFEAFFAEBFAEEEBBBBFABF
        data4 = 0X05EFABBBBAABBAFFFEBAEEBAEBBBAEEE
        expectedData = data1.to_bytes(16, byteorder="little") +\
                       data2.to_bytes(16, byteorder="little") + \
                       data3.to_bytes(16, byteorder="little") + \
                       data4.to_bytes(16, byteorder="little") + \
                       b'\x00' * 192
        self.assertEqual(data, expectedData)

    def testEvalFunctions(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            V sce=0, link=`rand(1)`, ctv=0, mtv=5, lrpt=0, data=0v01010100101010111000111100100111001010100101010111000111
            V sce=1, link=`rand(1)`, ctv=0, mtv=5, lrpt=0, data=0v01010100101010111000111100100111001010100101010111000111
        """)
        data = asm.Generate()

    def testResolveLiteral(self):
        asm = PatternAssembler()
        asm.ResolveLiteral('0j00000000000000000000000000000000000000000000000000000000')
        asm.ResolveLiteral('0v00000000000000000000000000000000000000000000000000000000')
        asm.ResolveLiteral('0j11111111111111111111111111111111111111111111111111111111')
        asm.ResolveLiteral('0v11111111111111111111111111111111111111111111111111111111')

    def testLabelResolution(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,                     data=0j00000000000000000000000000000000000000000000000000000000 # 0
            %repeat 1024
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000000 # 1
            %end
            %repeat 1024
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000000 # 1025
            %end
            PATTERN_END:                                                                                    # 2049
            StopPattern 0xdeadbeef
            %repeat 13
            S stype=IOSTATEJAM,                     data=0j00000000000000000000000000000000000000000000000000000000 # padding
            %end
        """)
        self.assertEqual(asm.Resolve('PATTERN_START'), 0)
        self.assertEqual(asm.Resolve('PATTERN_END'), 2049)
        data = asm.Generate()
        self.assertEqual(len(data), (1024 + 1024 + 3 + 13) * 16)


    def testRepeatCountSymbol(self):
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,                     data=0j00000000000000000000000000000000000000000000000000000000 # 0
            %repeat REPEATS
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000000 # 1
            %end
            %repeat REPEATS
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000000 # 1025
            %end
            PATTERN_END:                                                                                    # 2049
            StopPattern 0xdeadbeef
            %repeat 13
            S stype=IOSTATEJAM,                     data=0j00000000000000000000000000000000000000000000000000000000 # padding
            %end
        """)
        self.assertEqual(asm.Resolve('PATTERN_START'), 0)
        self.assertEqual(asm.Resolve('PATTERN_END'), 2049)
        data = asm.Generate()
        self.assertEqual(len(data), (1024 + 1024 + 3 + 13) * 16)


    def testSpacesInSourceCode(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,              data=0j00000000000000000000000000000000000000000000000000000000
            # Hello world
            S stype=IOSTATEJAM,              data=0j00000000000000000000000000000000000000000000000000000000

       \t\t S stype=IOSTATEJAM,              data=0j00000000000000000000000000000000000000000000000000000000
        """)
        data = asm.Generate()

    @staticmethod
    def RandomDriveVectorData():
        data = random.randint(0b00000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        literal = '0v{:056b}'.format(data)
        return literal


    @staticmethod
    def DriveHigh():
        return [
            ['', 'V', ['data', '0v11111111111111111111111111111111111111111111111111111111']]
        ]

    def testExternalSymbolsAndMacros(self):
        asm = PatternAssembler()

        asm.symbols['DATA']       = '0v00000000000000000000000000000000000000000000000000001111'
        asm.symbols['randomdata'] = self.RandomDriveVectorData

        asm.macros['DriveHigh'] = self.DriveHigh

        asm.LoadString("""
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0, data=DATA
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0, data=DATA
            %repeat 10
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0, data=randomdata
            %end
            %repeat 10
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0, data=randomdata
            %end
            DriveHigh
            PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        data = asm.Generate()
      
    def testCommentFail(self):
        asm = PatternAssembler()
        
        asm.LoadString("""
            # hello
            S stype=0,                                  data=0j00000000000000000000000000000000000000000000000000000000 # 0
            I optype=BRANCH, br=GOTO_I, imm=`PATTERN_START`                                                     # 1
            SUBR:
                %repeat 50
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0b00000000000000000000000000000000000000000000000000000000 # 2 - 51
                %end
                %repeat 50
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0b00000000000000000000000000000000000000000000000000000000 # 52 - 101
                %end
                I optype=BRANCH, br=RET                                                                         # 102
            PATTERN_START:
                %repeat 150
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0j00000000000000000000000000000000000000000000000000000000 # 103 - 252
                %end
                %repeat 150
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0j00000000000000000000000000000000000000000000000000000000 # 253 - 402
                %end
                I optype=BRANCH, base=GLOBAL, br=CALL_I, imm=`SUBR`                                             # 403
                %repeat 100
                V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v11111111111111111111111111111111111111111111111111111111 # 404 - 503
                %end
                %repeat 100
                V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v11111111111111111111111111111111111111111111111111111111 # 504 - 603
                %end
            PATTERN_END:
            StopPattern 0xdeadbeef                                                                                     # 604, 605
        """)
        data = asm.Generate()
	    
    def testRel(self):
        asm = PatternAssembler()
        
        asm.LoadString("""
        S stype=IOSTATEJAM,                         data=0j00000000000000000000000000000000000000000000000000000000
        I optype=BRANCH, br=GOTO_I, imm=`PRE_RAND`

        SUBR:
            %repeat 50
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v10000100110111011101010011111111111110000101110000010110
            %end
            %repeat 50
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v10000100110111011101010011111111111110000101110000010110
            %end
            I optype=BRANCH, br=RET

        PRE_RAND:
            %repeat 150
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000000
            %end
            %repeat 150
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000000
            %end

        I optype=REGISTER, opdest=REGDEST.REG, opsrc=REGSRC.IMM, dest=0, imm=`int32(SUBR-vecaddr)`
        I optype=BRANCH, base=GLOBAL, br=CALL_R, dest=0

        POST_RAND:
            %repeat 100
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v11111111111111111111111111111111111111111111111111111111
            %end
            %repeat 100
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v11111111111111111111111111111111111111111111111111111111
            %end

            PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        data = asm.Generate()
        

    def testGenerators(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,                     data=0j00000000000000000000000000000000000000000000000000000000
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000001
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000010
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000011
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v00000000000000000000000000000000000000000000000000000100
            PATTERN_END:
        """)
        expectedPattern = asm.Generate()
        asm = PatternAssembler()
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM, data=0j00000000000000000000000000000000000000000000000000000000
            AddressVectors 4
            PATTERN_END:
        """)
        actualPattern = asm.Generate()
        self.assertEqual(actualPattern, expectedPattern)

    def test1MVectors(self):
        asm = PatternAssembler()
        asm.symbols['REPEATS'] = 1024 * 1024
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            AddressVectors length=REPEATS
            PATTERN_END:                                                                                    # 1 + REPEATS
            StopPattern 0xdeadbeef
        """)
        data = asm.Generate()

    def testNestedRepeat(self):
        repeats1 = 13
        repeats2 = 11
        repeats3 = 12
        asm = PatternAssembler()
        asm.symbols['REPEATS1'] = repeats1
        asm.symbols['REPEATS2'] = repeats2
        asm.symbols['REPEATS3'] = repeats3
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,                             data=0j00000000000000000000000000000000000000000000000000000000
            %repeat REPEATS1
                %repeat REPEATS2
                    V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v11111111111111111111111111111111111111111111111111111111
                %end
                 %repeat REPEATS3
                    V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v11111111111111111111111111111111111111111111111111111111
                %end
            %end
            PATTERN_END:
            StopPattern 0xdeadbeef
        """)
        self.assertEqual(asm.Resolve('PATTERN_END'), repeats1 * (repeats2 + repeats3) + 1)

    def testNonZeroStartAddress(self):
        start = 256
        repeats = 100
        asm = PatternAssembler(start)
        asm.symbols['REPEATS'] = repeats
        asm.LoadString("""\
            PATTERN_START:
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000 # 0
            AddressVectors REPEATS
            PATTERN_END:                                                                                    # 1 + REPEATS
            StopPattern 0xdeadbeef
        """)
        self.assertEqual(asm.Resolve('`PATTERN_START`'), start)
        self.assertEqual(asm.Resolve('PATTERN_END'), repeats + 1 + start)

    def testVars(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            %var                           enclk=0b10000000000000000010000000000000000000000000000011111110
            PATTERN_START:
            S stype=IOSTATEJAM,                     data=0j0ZZZZZZZZZZZZZZZZZ0ZZZZZZZZZZZZZZZZZZZZZZZZZZZZ0R00000E0
            V sce=0, link=0, ctv=0, mtv=0, lrpt=0,  data=0v0LLLLLLLLLLLLLLLLL0LLLLLLLLLLLLLLLLLLLLLLLLLLLL0R00000E0            
            V sce=1, link=0, ctv=0, mtv=0, lrpt=0,  data=0v0LLLLLLLLLLLLLLLLL0LLLLLLLLLLLLLLLLLLLLLLLLLLLL0R00000E0
            AddressVectors 10
        """)
        self.assertEqual(asm.Resolve('enclk'), 0b10000000000000000010000000000000000000000000000011111110)
        pattern = asm.Generate()

    def testEval(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            LABEL1:
            AddressVectors 10
            LABEL2:
            AddressVectors 13
            LABEL3:
            AddressVectors 10
            AddressVectors `vecaddr -        \tLABEL2`
        """)
        self.assertEqual(asm.Resolve('`LABEL3 - LABEL2`'), 13)
        pattern = asm.Generate()

    def testRepeatGeneratorBug(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            V sce=0, link=1, ctv=0, mtv=5, lrpt=0,      data=0v01010100101010111000111100100111001010100101010111000111
            RandomVectors length=32768
            V sce=1, link=1, ctv=0, mtv=5, lrpt=0,      data=0v01010100101010111000111100100111001010100101010111000111
            RandomVectors length=32768
            %repeat 100
                V sce=0, link=1, ctv=0, mtv=5, lrpt=0,  data=0v01010100101010111000111100100111001010100101010111000111
                RandomVectors length=1
            %end
            %repeat 100
                V sce=1, link=1, ctv=0, mtv=5, lrpt=0,  data=0v01010100101010111000111100100111001010100101010111000111
                RandomVectors length=1
            %end
            StopPattern 0x69696969
        """)
        data = asm.Generate()

    def testRepeatMacroCallBug(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            StartPattern
            V sce=0, link=1, ctv=0, mtv=5, lrpt=0,      data=0v01010100101010111000111100100111001010100101010111000111
            RandomVectors length=32768
            V sce=1, link=1, ctv=0, mtv=5, lrpt=0,      data=0v01010100101010111000111100100111001010100101010111000111
            RandomVectors length=32768
            %repeat 100
                Repeat 64
                V sce=0, link=1, ctv=0, mtv=5, lrpt=0,  data=0v01010100101010111000111100100111001010100101010111000111
                RandomVectors length=1
            %end
            %repeat 100
                Repeat 64
                V sce=1, link=1, ctv=0, mtv=5, lrpt=0,  data=0v01010100101010111000111100100111001010100101010111000111
                RandomVectors length=1
            %end
            StopPattern 0x69696969
        """)
        data = asm.Generate()


    def testRepeatLabelError(self):
        asm = PatternAssembler()
        raised = False
        try:
            asm.LoadString("""\
                StartPattern
                V sce=0, link=1, ctv=0, mtv=5, lrpt=0,      data=0v01010100101010111000111100100111001010100101010111000111
                RandomVectors length=32768
                V sce=1, link=1, ctv=0, mtv=5, lrpt=0,      data=0v01010100101010111000111100100111001010100101010111000111
                RandomVectors length=32768
                %repeat 100
                    LABEL:
                    V sce=0, link=1, ctv=0, mtv=5, lrpt=0,  data=0v01010100101010111000111100100111001010100101010111000111
                %end
                %repeat 100
                    LABEL:
                    V sce=1, link=1, ctv=0, mtv=5, lrpt=0,  data=0v01010100101010111000111100100111001010100101010111000111
                %end
                StopPattern 0x69696969
            """)
        except:
            raised = True
        self.assertEqual(raised, True)
        
            
    def testRandomVar(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=IOSTATEJAM,             data=0j00000000000000000000000000000000000000000000000000000000
            RandomPassingEvenToOddNoSwitchVectors length=10, lrpt={[0:31]}
        """)
        data = asm.Generate()

    def testScsFunc(self):
        asm = PatternAssembler()
        asm.LoadString("""\
            S stype=SCC, lsb_shift=1, virtual_pin_id=1, record_length=1, serial_channel=1 
        """)
        data = asm.Generate()
        # Compare encoded data
        a = 0x80048000000000000000000000000089
        expectedData =  a.to_bytes(256, byteorder='little')
        self.assertEqual(data, expectedData)
