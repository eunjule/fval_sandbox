################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: test_rpg.py
#-------------------------------------------------------------------------------
#     Purpose: Unit test for random patten generator
#-------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/29/15
#       Group: HDMT FPGA Validation
################################################################################


import binascii
import random
import unittest

from Hpcc.hpcctb.assembler import PatternAssembler
from Hpcc.hpcctb.rpg import RPG

class RPGTests(unittest.TestCase):
    def test(self):
        pattern = PatternAssembler()
        vectorData = random.randint(0b10000000000000000000000000000000000000000000000000000000, 0b11111111111111111111111111111111111111111111111111111111)
        
        p = RPG()
        p.StartPattern()
        p.Jump("GOTO_I", "`PRE_RAND`")
        p.AddSubroutine("SUBR", 50, vectorData)
        p.AddBlock("PRE_RAND", 150, 0)
        p.CallSubr("GLOBAL", "CALL_R", "SUBR")
        p.AddBlock("POST_RAND", 100, 0b11111111111111111111111111111111111111111111111111111111)
        p.EndPattern()
        #print(p.GetPattern())
        pattern.LoadString(p.GetPattern())
        #for i in pattern.Vectors():
        #    print(binascii.hexlify(i))

   
