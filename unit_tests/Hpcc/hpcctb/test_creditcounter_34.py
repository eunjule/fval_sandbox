################################################################################
# INTEL CONFIDENTIAL - Copyright 2018-2019. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import sys
import unittest

sys.path.insert(0, os.path.abspath('../../..'))

from Hpcc.hpcctb import creditcounter_34 as creditcounter
from ThirdParty.teamcity import is_running_under_teamcity

MAX_SUBCYCLE = 7
MIN_PERIOD = 666e-12
MAX_PERIOD = MIN_PERIOD * 2**(MAX_SUBCYCLE+1)
ONE_PICOSECOND = 1e-12
MAX_BALANCE = 16384
SINGLE_VECTOR_COST = 1


class CreditCounterTests(unittest.TestCase):

    def test_balance_too_low(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        with self.assertRaises(RuntimeError):
            counter.Instruction()

    def test_balance_goes_to_zero(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        counter.Vector()
        with self.assertRaises(RuntimeError):
            result = counter.Instruction()
        
        self.assertGreater(counter.GetUnderrunAmount(), 0)

    def test_vector_with_local_repeat(self):
        counter = creditcounter.CreditCounter(5e-9)
        counter.Vector(24)
        for i in range(3):
            counter.Instruction()
        with self.assertRaises(RuntimeError):
            counter.Instruction()

    def test_vector_with_local_repeat_and_link(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        counter.Vector(31, 4)
        for i in range(8):
            counter.Instruction()
        with self.assertRaises(RuntimeError):
            counter.Instruction()

    def test_repeat_instruction(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        with self.assertRaises(RuntimeError):
            counter.InstructionRepeat(30)

    def test_branch_instruction(self):
        counter = creditcounter.CreditCounter(2.5e-9)
        with self.assertRaises(RuntimeError):
            counter.Branch()
    
    def test_invalid_period(self):
        with self.assertRaises(RuntimeError):
            cc = creditcounter.CreditCounter(MIN_PERIOD - ONE_PICOSECOND)
    
    def test_initial_values_for_period_only_constructor(self):
        cc = creditcounter.CreditCounter(MIN_PERIOD)
        self.assertEqual(cc.GetUnderrunAmount(), 0)
        with self.assertRaises(RuntimeError) as e:
            cc.Metadata()
        self.assertIn('below threshold of 0', str(e.exception))
    
    def test_threshold_with_default_initial_balance(self):
        for threshold in range(0, 163840, 300):
            cc = creditcounter.CreditCounter(MIN_PERIOD, threshold)
            with self.assertRaises(RuntimeError) as e:
                cc.Metadata()
            self.assertIn('below threshold of {}'.format(threshold), str(e.exception))
    
    def test_threshold_and_initial_balance(self):
        threshold = 0
        initial_balance = 100
        cc = creditcounter.CreditCounter(MIN_PERIOD, threshold, initial_balance)
        balance1 = cc.Metadata()
        balance2 = cc.Metadata()
        metadata_credit = balance2 - balance1
        self.assertLess(metadata_credit, 0)
        self.assertEqual(initial_balance + metadata_credit, balance1)
    
    def test_underrun(self):
        cc = creditcounter.CreditCounter(MIN_PERIOD)
        with self.assertRaises(RuntimeError) as e:
            cc.Metadata()
        self.assertGreater(cc.GetUnderrunAmount(), 0)
        
    def test_max_balance(self):
        cc = creditcounter.CreditCounter(MIN_PERIOD)
        localrepeat = 0
        linkwidth = 1
        for i in range(MAX_BALANCE*2):
            balance = cc.Vector(localrepeat, linkwidth)
            self.assertLessEqual(balance, MAX_BALANCE)
    
    def test_vector(self):
        cc = creditcounter.CreditCounter(MIN_PERIOD)
        last_balance = 0
        for localrepeat in range(32):
            for linkwidth in [1, 2, 4, 8]:
                balance = cc.Vector(localrepeat, linkwidth)
                credit = balance - last_balance
                self.assertEqual(credit, (localrepeat + 1) * linkwidth)
                last_balance = balance
    
    def test_metadata(self):
        threshold = 0
        initial_balance = 100
        cc = creditcounter.CreditCounter(MIN_PERIOD, threshold, initial_balance)
        balance1 = cc.Metadata()
        balance2 = cc.Metadata()
        metadata_credit = balance2 - balance1
        self.assertLess(metadata_credit, 0)
        self.assertLess(SINGLE_VECTOR_COST + metadata_credit, 0)
    
    def test_instruction_repeat(self):
        threshold = 0
        initial_balance = 16000
        for subcycle in range(MAX_SUBCYCLE + 1):
            period = MIN_PERIOD*2**subcycle + ONE_PICOSECOND
            cc = creditcounter.CreditCounter(period, threshold, initial_balance)
            balance1 = cc.Metadata()
            balance2 = cc.InstructionRepeat(0)
            instruction_repeat_credit = balance2 - balance1
            if subcycle == 0:
                self.assertLess(instruction_repeat_credit, 0)
            else:
                self.assertGreaterEqual(instruction_repeat_credit, faster_period_credit)
            faster_period_credit = instruction_repeat_credit
    
    def test_branch(self):
        threshold = 0
        initial_balance = 16000
        for subcycle in range(MAX_SUBCYCLE + 1):
            period = MIN_PERIOD*2**subcycle
            cc = creditcounter.CreditCounter(period, threshold, initial_balance)
            balance1 = cc.Metadata()
            balance2 = cc.Branch()
            branch_credit = balance2 - balance1
            self.assertLess(branch_credit, 0)
            if subcycle > 0:
                self.assertGreaterEqual(branch_credit, faster_period_credit)
            faster_period_credit = branch_credit

if __name__ == '__main__':
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        runner = unittest.TextTestRunner()

    unittest.main(testRunner=runner)
