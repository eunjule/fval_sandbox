################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import unittest
from datetime import datetime
from unittest.mock import Mock, patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


from Common import fval
from Hpcc.Tests.VectorCompression import ChannelLinking, LocalRepeat,\
                                         Mix, InstructionRepeat


class ChannelLinkingTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = ChannelLinking()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = ChannelLinking('DirectedLinkMode2xTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class LocalRepeatTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = LocalRepeat()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = LocalRepeat('Directed1KVTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class MixTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Mix()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Mix('ThrReversedLinkCountTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class InstructionRepeatTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = InstructionRepeat()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = InstructionRepeat('DirectedImmediateTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()
        

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
