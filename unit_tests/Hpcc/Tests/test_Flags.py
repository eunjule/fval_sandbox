################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import unittest
from datetime import datetime
from unittest.mock import Mock, patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


from Common import fval
from Hpcc.Tests.Flags import Alu, StickyErrors, Misc, Triggers


class AluTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Alu()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Alu('DirectedSetCarryTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class StickyErrorsTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = StickyErrors()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = StickyErrors('DirectedSetLocalStickyErrorTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class MiscTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Misc()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Misc('DirectedSetWiredOrTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class TriggersTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Triggers()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Triggers('DirectedDomainTriggerTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()
        

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
