################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import os
import unittest
from datetime import datetime
from unittest.mock import Mock, patch

if __name__ == '__main__':
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common import fval
from Hpcc.instrument.hpcc import Hpcc
from Hpcc.Tests.ddr import Ddr
from Hpcc.instrument.hpccac import HpccAc


class DdrTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None
        
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Ddr()
            test_case.setUp()
    
    def test_tearDown(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Ddr('RandomDdrDimmFailChannelDetectTest')
            test_case.env = Mock(fpgas=[])
            test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
            test_case._softErrors = []
            test_case.start_time = datetime.now()
            test_case.tearDown()
    
    def test_RandomAcDdrTest_passing(self):
        with patch('Hpcc.instrument.hpccac.hil') as mock_hil:
            ac_simulator = AcSimulator()
            mock_hil.hpccAcBarRead = Mock(return_value=0)
            mock_hil.hpccAcDmaWrite = ac_simulator.DmaWrite
            mock_hil.hpccAcDmaRead = ac_simulator.DmaRead
            test_case = Ddr()
            hpcc = Hpcc(slot=0,rc=self.mock_rc)
            test_case.env = Mock(instruments={0:hpcc}, fpgas=[(0,0)])
            test_case.RandomAcDdrTest()
    
    def test_RandomAcDdrTest_with_ecc_errors(self):
        with patch('Hpcc.instrument.hpccac.hil') as mock_hil:
            ac_simulator = AcSimulator()
            mock_hil.hpccAcBarRead = Mock(return_value=3)
            mock_hil.hpccAcDmaWrite = ac_simulator.DmaWrite
            mock_hil.hpccAcDmaRead = ac_simulator.DmaRead
            hpcc = Hpcc(slot=0,rc=self.mock_rc)
            hpcc.ac[0].get_alarms = Mock(return_value={'ECCError':1})
            for i in range(2):
                hpcc.ac[i].Log = Mock(side_effect=assert_on_logged_error)
            # hpcc.ac[1].Log = Mock(side_effect=assert_on_logged_error)
            test_case = Ddr()
            test_case.env = Mock(instruments={0:hpcc}, fpgas=[(0,0)])
            with self.assertRaises(fval.LoggedError):
                test_case.RandomAcDdrTest()
    
    def test_RandomAcDdrTest_with_block_errors(self):
        with patch('Hpcc.instrument.hpccac.hil') as mock_hil:
            ac_simulator = AcSimulator()
            ac_simulator.block_errors = True
            mock_hil.hpccAcBarRead = Mock(return_value=0)
            mock_hil.hpccAcDmaWrite = ac_simulator.DmaWrite
            mock_hil.hpccAcDmaRead = ac_simulator.DmaRead
            hpcc = Hpcc(slot=0,rc=self.mock_rc)
            hpcc.ac[0].get_alarms = Mock(ECCError=0)
            test_case = Ddr()
            test_case.Log = Mock(side_effect=assert_on_logged_error)
            test_case.env = Mock(instruments={0:hpcc}, fpgas=[(0,0)])
            with self.assertRaises(fval.LoggedError):
                test_case.RandomAcDdrTest()

    def test_DirectedAcDdrPassTest(self):
        with patch('Hpcc.instrument.hpccac.hil') as mock_hil:
            ac_simulator = AcSimulator()
            mock_hil.hpccAcBarRead = Mock(return_value=0)
            mock_hil.hpccAcDmaWrite = ac_simulator.DmaWrite
            mock_hil.hpccAcDmaRead = ac_simulator.DmaRead
            hpcc = Hpcc(slot=0,rc=self.mock_rc)
            test_case = Ddr()
            for i in range(2):
                hpcc.ac[i].Log = Mock(side_effect=assert_on_logged_error)
            HpccAc._RunBigDummyPattern = Mock()
            test_case.env = Mock(instruments={0:hpcc}, fpgas=[(0,0)])
            test_case._create_infinite_pattern = Mock(side_effect=bytes(4*1024))
            test_case.DirectedAcDdrTest()

    def test_DirectedAcDdrFailTest(self):
        with patch('Hpcc.instrument.hpccac.hil') as mock_hil,\
                patch.object(HpccAc, 'IsPatternComplete') as mock_IsPatternComplete:
            ac_simulator = AcSimulator()
            mock_hil.hpccAcBarRead = Mock(return_value=0)
            mock_hil.hpccAcDmaWrite = ac_simulator.DmaWrite
            mock_hil.hpccAcDmaRead = Mock(return_value=0)
            hpcc = Hpcc(slot=0,rc=self.mock_rc)
            test_case = Ddr()
            test_case.Log = Mock(side_effect=assert_on_logged_error)
            mock_IsPatternComplete.return_value = False
            test_case.env = Mock(instruments={0:hpcc}, fpgas=[(0,0)])
            test_case._create_infinite_pattern = Mock(side_effect=bytes(4*1024))
            with self.assertRaises(fval.LoggedError):
                test_case.DirectedAcDdrTest()

def assert_on_logged_error(level, message):
    if level.lower() == 'error':
        raise fval.LoggedError()
        
class AcSimulator():
    def __init__(self):
        self.block_errors = False
    
    def DmaWrite(self, junk1, junk2, offset, data):
        self.data = data
    
    def DmaRead(self, junk, junk1, offset, length):
        if self.block_errors:
            return os.urandom(len(self.data))
        else:
            return self.data
        

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
