################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import unittest
from datetime import datetime
from unittest.mock import Mock, patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


from Common import fval
from Hpcc.Tests.InternalLoopbackTests import PVCCacheUnderRun,\
                                             StackError,\
                                             Subroutine,\
                                             Unconditional,\
                                             Conditional,\
                                             CaptureAll,\
                                             BasicFlatPattern,\
                                             FlatPattern,\
                                             Misses,\
                                             Alu,\
                                             Misc,\
                                             Triggers,\
                                             Abort,\
                                             ChannelLinking,\
                                             LocalRepeat,\
                                             InstructionRepeat


class PVCCacheUnderRunTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = PVCCacheUnderRun()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = PVCCacheUnderRun('DirectedFVALShortLoopSizeTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class StackErrorTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = StackError()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = StackError('DirectedFVALStackOverflowTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class SubroutineTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Subroutine()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Subroutine('DirectedFVALSubrHeadCallIGlobalTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class UnconditionalTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Unconditional()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Unconditional('DirectedFVALGotoImmediateGlobalTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class ConditionalTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Conditional()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Conditional('DirectedFVALGotoIfCarryImmediateGlobalTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class CaptureAllTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = CaptureAll()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = CaptureAll('DirectedFVALMinPeriodTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class BasicFlatPatternTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = BasicFlatPattern()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = BasicFlatPattern('MiniFVAL1KVTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()

class FlatPatternTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = FlatPattern()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = FlatPattern('MiniFVALVectorAddrTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class MissesTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Misses()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Misses('ThrFVALTims32KVFlatPatternTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class AluTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Alu()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Alu('DirectedFVALSetCarryTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class MiscTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Misc()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Misc('DirectedFVALSetWiredOrTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class TriggersTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Triggers()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Triggers('DirectedFVALDomainTriggerTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class AbortTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Abort()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Abort('DirectedFVALAbortFlatTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class ChannelLinkingTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = ChannelLinking()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = ChannelLinking('DirectedFVALLinkMode2xTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class LocalRepeatTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = LocalRepeat()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = LocalRepeat('DirectedFVAL1KVTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()


class InstructionRepeatTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = InstructionRepeat()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = InstructionRepeat('DirectedFVALImmediateTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()
        

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
