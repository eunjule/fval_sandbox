################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import unittest
from datetime import datetime
from unittest.mock import Mock, patch

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


from Common import fval
from Hpcc.Tests.Branch import Conditional, StickyError, Subroutine, Unconditional


class ConditionalTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Conditional()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Conditional('DirectedPCallLessThanOrEqualCarry1Test')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()

            
class StickyErrorTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = StickyError()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = StickyError('RandomDomainStickyErrorTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()

            
class SubroutineTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Subroutine()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Subroutine('DirectedSubrHeadCallIGlobalTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()

            
class UnconditionalTests(unittest.TestCase):
    def test_setUp(self):
        with patch('Hpcc.Tests.hpcctest.Env') as mock_env:
            test_case = Unconditional()
            test_case.setUp()
    
    def test_tearDown(self):
        test_case = Unconditional('DirectedGotoImmediateGlobalTest')
        test_case.env = Mock(fpgas=[])
        test_case._outcome = Mock(failures=[], errors=[], _softErrors=[])
        test_case._softErrors = []
        test_case.start_time = datetime.now()
        test_case.tearDown()

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
