# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import logging
import os
import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock

from Common.fval import LoggedError, SetLoggerLevel
from Common import hilmon as hil
from Hpcc.instrument import hpcc

POSITIONAL_ARGS_INDEX = 1
LOG_LEVEL = logging.CRITICAL

class HpccTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None
        SetLoggerLevel(LOG_LEVEL)

    def test_load_dc_with_image_name_missing_from_configs(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs', spec_set=['HPCC_SLICE0_ONLY', 'HPCC_DCL_FPGA_LOAD']) as mock_configs:
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904-404')
            with self.assertRaises(LoggedError) as e:
                hpcc.Hpcc.load_dc_fpgas(hpcc_instrument)

    def test_load_dc_with_bad_image_name(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs:
            mock_configs.HPCC1_DCL_FPGA_MDUT_FabD_IMAGE = 'this name is not valid'
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904-401')
            with self.assertRaises(LoggedError):
                hpcc.Hpcc.load_dc_fpgas(hpcc_instrument)

    def test_load_dc_with_bad_blt_part_number(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs:
            mock_configs.HPCC_DCL_FPGA_IMAGE = r'M:\directory\structure\dcl_image_rev6_x(parens).bin'
            mock_configs.HPCC_DCU_FPGA_IMAGE = r'M:\directory\structure\dcu_image_rev6_x(parens).bin'
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            # leave off the -xxx from the part number
            hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904')
            with self.assertRaises(LoggedError):
                hpcc.Hpcc.load_dc_fpgas(hpcc_instrument)

    def test_log_blt_info(self):
        hpcc_inst = hpcc.Hpcc(slot=0, rc=None)
        hpcc_inst.instrumentblt = Mock()
        hpcc_inst.acboardblt = Mock()
        hpcc_inst.dcboardblt = Mock()
        hpcc_inst.log_blt_info()
        if not hpcc_inst.instrumentblt.write_to_log.called :
            self.fail('Instrument BLT not logged')
        if not hpcc_inst.acboardblt.write_to_log.called:
                self.fail('AC Board BLT not logged')
        if not hpcc_inst.dcboardblt.write_to_log.called :
            self.fail('DC Board BLT not logged')


    def test_load_dc_on_fab_d_mainboard_with_rev6_image(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpccdc.hil') as mock_hil, \
                patch('Hpcc.instrument.hpccac.hil'),\
                patch('ThirdParty.SASS.suppress_sensor.sass'):
            mock_configs.HPCC1_DCL_FPGA_MDUT_FabD_IMAGE = r'M:\directory\structure\dcl_fpga_rev6_x(parens).bin'
            mock_configs.HPCC1_DCU_FPGA_MDUT_FabD_IMAGE = r'M:\directory\structure\dcu_fpga_rev6_x(parens).bin'
            mock_hil.hpccDcFpgaVersion = Mock(return_value=0xDEADBEEF)
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904-400')
            hpcc.Hpcc.load_dc_fpgas(hpcc_instrument)

    def test_load_dc_on_fab_d_mainboard_with_incompatible_rev7_image(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs:
            mock_configs.HPCC_DCL_FPGA_IMAGE = r'M:\directory\structure\dcl_fpga_rev7_x(parens).bin'
            mock_configs.HPCC_DCU_FPGA_IMAGE = r'M:\directory\structure\dcu_fpga_rev7_x(parens).bin'
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904-400')
            with self.assertRaisesRegex(LoggedError, '.*not compatible.*'):
                hpcc.Hpcc.load_dc_fpgas(hpcc_instrument)

    def test_load_dc_with_hil_runtime_error(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpccdc.hil') as mock_hil,\
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            mock_configs.HPCC_DCL_FPGA_IMAGE = 'dcl_fpga_rev6_x.bin'
            mock_configs.HPCC_DCU_FPGA_IMAGE = 'dcu_fpga_rev6_x.bin'
            mock_hil.hpccDcFpgaVersion = Mock(return_value=0xDEADBEEF)
            mock_hil.hpccDcFpgaLoad = Mock(side_effect=RuntimeError('mock hil message'))
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904-499')
            with self.assertRaises(LoggedError):
                hpcc.Hpcc.load_dc_fpgas(hpcc_instrument)

    def test_load_ac_with_nonexistant_directory(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.os.path') as mock_path:
            mock_configs.HPCC1_AC_FPGA_FABD = 'not a valid path'
            mock_path.isdir = Mock(return_value=False)
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-400')
            with self.assertRaises(LoggedError):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_directory_that_does_not_contain_images(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.os.path') as mock_path, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob:
            mock_configs.HPCC1_AC_FPGA_FABD = 'a valid path'
            mock_path.isdir = Mock(return_value=True)
            mock_path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=[])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-400')
            with self.assertRaises(LoggedError):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_directory_that_contains_multiples_images(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob:
            mock_configs.HPCC1_AC_FPGA_FABD = 'a valid path'
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.5.bin',
                                                     'hpcc_ac_variant0_fpga0_dimms_32b_v.2.5.5.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-400')
            with self.assertRaises(LoggedError):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_with_incorrect_fpga0_image_name(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob:
            mock_configs.HPCC1_AC_FPGA_FABD = 'a valid path'
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.bin',
                                                     'hpcc_ac_variant0_fpga1_dimms_32b_v.2.4.5.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-400')
            with self.assertRaisesRegex(LoggedError, '.*FPGA0.*'):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_with_incorrect_fpga1_image_name(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob:
            mock_configs.HPCC1_AC_FPGA_FABD = 'a valid path'
            mock_configs.HPCC_SLICE0_ONLY = False
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.5.bin',
                                                     'hpcc_ac_variant0_fpga1_dimms_32b_v.2.4.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-400')
            with self.assertRaisesRegex(LoggedError, '.*FPGA1.*'):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_with_incorrect_fpga1_image_name_but_slice0_only_set(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob, \
                patch('Hpcc.instrument.hpccac.hil') as mock_hil,\
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            mock_configs.HPCC1_AC_FPGA_FABD = 'a valid path'
            mock_configs.HPCC_SLICE0_ONLY = True
            mock_hil.hpccAcBarRead = Mock(return_value=0xDEADBEEF)
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.2.bin',
                                                     'hpcc_ac_variant0_fpga1_dimms_32b_v.2.4.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-400')
            hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_fab_g_board_with_fab_d_image(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob, \
                patch('Hpcc.instrument.hpccac.hil') as mock_hil:
            mock_configs.HPCC1_AC_FPGA_FABG = 'a valid path'
            mock_configs.HPCC_SLICE0_ONLY = False
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.2.bin',
                                                     'hpcc_ac_variant0_fpga1_dimms_32b_v.2.4.2.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-750')
            with self.assertRaisesRegex(LoggedError, '.*not compatible.*'):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_load_ac_hpcc2_fab_a_board_with_correct_image(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob, \
                patch('Hpcc.instrument.hpccac.hil') as mock_hil,\
                patch('ThirdParty.SASS.suppress_sensor.sass') as mock_sass:
            mock_configs.HPCC2_AC_FPGA_FABA = 'a valid path'
            mock_configs.HPCC_SLICE0_ONLY = False
            mock_hil.hpccAcBarRead = Mock(return_value=0xDEADBEEF)
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.6.bin',
                                                     'hpcc_ac_variant0_fpga1_dimms_32b_v.2.4.6.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAJ49483-111')
            hpcc_instrument.ac[0].lmk04808S0.Initialize = Mock(return_value=None)
            hpcc.load_ac_fpgas(hpcc_instrument)
            hpcc_instrument.ac[0].lmk04808S0.Initialize.assert_called_once_with(hpcc2=True, fineDelayDivider=0)

    def test_load_ac_with_bad_part_number(self):
        with patch('Hpcc.instrument.hpcc.hpccConfigs') as mock_configs, \
                patch('Hpcc.instrument.hpcc.glob') as mock_glob:
            mock_configs.HPCC_AC_FPGA_PATH = 'a valid path'
            os.path.isdir = Mock(return_value=True)
            os.path.isfile = Mock(return_value=True)
            mock_glob.glob = Mock(return_value=['hpcc_ac_variant0_fpga0_dimms_32b_v.2.4.5.bin',
                                                     'hpcc_ac_variant0_fpga1_dimms_32b_v.2.4.5.bin'])
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AA123456')
            with self.assertRaisesRegex(LoggedError, 'Unknown daughter board part number: AA123456'):
                hpcc.load_ac_fpgas(hpcc_instrument)

    def test_read_instrument_blt(self):
        with patch('Hpcc.instrument.hpcc.hil') as mock_hil:
            blt = Mock(spec=hil.BLT, PartNumberCurrent='AA123456')
            mock_hil.hpccBltInstrumentRead = Mock(return_value=blt)
            hpcc_instrument = hpcc.Hpcc(slot=2, rc=self.mock_rc)
            self.assertEqual(hpcc_instrument.instrumentblt.PartNumberCurrent, 'AA123456')

    def xtest_initialize(self):
        slot = random.randrange(12)
        hpcc_instrument = hpcc.Hpcc(slot=slot, rc=self.mock_rc)
        hpcc_instrument.instrumentBlt = Mock(spec=hil.BLT, PartNumberCurrent='IA123456-789')
        hpcc_instrument.acboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAG70252-526')
        hpcc_instrument.dcboardblt = Mock(spec=hil.BLT, PartNumberCurrent='AAH78904-499')
        hpcc_instrument.mainboard_cpld_version = lambda: 'C040'
        hpcc_instrument._PerformClockSync = lambda: None
        hpcc_instrument.Initialize()

    def test_SPIAdjustmentForTrigger(self):
        with patch('Hpcc.instrument.hpccdc.hil') as mock_dc_hil:
            mock_dc_hil.hpccDcBarRead = Mock(return_value=0x80000000)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=self.mock_rc)
            hpcc_instrument.rc.reset_aurora_bus = Mock()
            self._mock_clear_trigger_errors(hpcc_instrument.dc)
            hpcc_instrument.check_link_stability = Mock(return_value=False)
            with self.assertRaises(hpcc_instrument.SpiAdjustReadTriggerError):
                hpcc_instrument.SPIAdjustmentForTrigger([0, 1])

    def test_SPIAdjustmentForTriggerTrueManyCOnditions(self):
        with patch('Hpcc.instrument.hpccdc.hil') as mock_dc_hil:
            mock_dc_hil.hpccDcBarRead = Mock(return_value=0x80000000)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=self.mock_rc)
            hpcc_instrument.rc.reset_aurora_bus = Mock()
            self._mock_clear_trigger_errors(hpcc_instrument.dc)
            hpcc_instrument.check_link_stability = Mock(side_effect=[False, True, False, True])
            hpcc_instrument.SPIAdjustmentForTrigger([0, 1])
            self.assertEquals(hpcc_instrument.check_link_stability.call_count, 4)

    def test_SPIAdjustmentForTriggerTrue(self):
        with patch('Hpcc.instrument.hpccdc.hil') as mock_dc_hil:
            mock_dc_hil.hpccDcBarRead = Mock(return_value=0x80000000)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=self.mock_rc)
            hpcc_instrument.rc.reset_aurora_bus = Mock()
            self._mock_clear_trigger_errors(hpcc_instrument.dc)
            hpcc_instrument.check_link_stability = Mock(return_value=True)
            hpcc_instrument.SPIAdjustmentForTrigger([0, 1])
            self.assertEquals(hpcc_instrument.check_link_stability.call_count, 2)

    def _mock_clear_trigger_errors(self, dc_fpgas):
        for dc in dc_fpgas:
             dc.clear_trigger_errors = Mock()
