################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import os
import unittest
from unittest.mock import Mock

repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..','..')
if __name__ == '__main__':
    import sys
    sys.path.append(os.path.abspath(repo_root_path))

from Common import fval
from Hpcc.instrument.adate320 import AdAte320
from Hpcc.instrument import adate320Reg as actual_pe_registers

DC_REG_DS = os.path.join(repo_root_path, 'Hpcc', 'instrument', 'hpccDcRegs.ds')
actual_dc_registers = fval.LoadStructs(DC_REG_DS)


class Adate320Tests(unittest.TestCase):
    '''Tests for ADATA320 devices attached to a DC FPGA'''
    
    def test_initialize(self):
        peRegs = Mock(Register7D=0x7D, Register7F=0x7F)
        dc = Mock(slice=0, peRegs=peRegs, BarRead=Mock(return_value=0))
        adate = AdAte320(dc, 0)
        adate.Initialize()
        
    def test_set_attribute_drive(self):
        peRegs = Mock()
        dc = Mock(slice=0, peRegs=peRegs, BarRead=Mock(return_value=0))
        adate = AdAte320(dc, 0)
        adate.SetAttribute('DRIVE', 3)
        
    def test_set_initial_value(self):
        peRegs = Mock(DacControl=actual_pe_registers.DacControl,
                       DrvControl=actual_pe_registers.DrvControl,
                       CmpControl=actual_pe_registers.CmpControl,
                       LoadControl=actual_pe_registers.LoadControl,
                       PpmuControl=actual_pe_registers.PpmuControl,
                       VIHLevel=actual_pe_registers.VIHLevel,
                       VILLevel=actual_pe_registers.VILLevel,
                       VCHLevel=actual_pe_registers.VCHLevel,
                       VCLLevel=actual_pe_registers.VCLLevel,
                       VIOHLevel=actual_pe_registers.VIOHLevel,
                       VIOLLevel=actual_pe_registers.VIOLLevel,
                       VOHLevel=actual_pe_registers.VOHLevel,
                       VITVCOMLevel=actual_pe_registers.VITVCOMLevel)
        dc = Mock(slice=0, peRegs=peRegs, BarRead=Mock(return_value=0))
        adate = AdAte320(dc, 0)
        adate._SetControlRegisters = lambda: [actual_pe_registers.DrvControl(),
                                              actual_pe_registers.CmpControl(),
                                              actual_pe_registers.LoadControl(),
                                              actual_pe_registers.PpmuControl()]
        adate.SetInitialValue()
        
    def test_ExecutePMU_VM(self):
        peRegs = Mock()
        dcRegisters = Mock(UserDefinedData=actual_dc_registers.UserDefinedData)
        dc = Mock(slice=0, peRegs=peRegs, dcRegisters=dcRegisters, BarRead=Mock(return_value=0))
        adate = AdAte320(dc, 0)
        adate.SetInitialValue = lambda:None
        adate.ExecutePMU_VM(0)

        

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
