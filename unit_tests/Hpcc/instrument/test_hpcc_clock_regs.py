# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest

from Hpcc.instrument.hpcc import Hpcc


class ControlFunction1RegisterTests(unittest.TestCase):
    def test_newly_created_control_function_1_register(self):
        reg = Hpcc.clockRegs.ControlFunction1Register()
        self.assertEqual(reg.Pack(), 0)

    def test_newly_created_control_function_2_register(self):
        reg = Hpcc.clockRegs.ControlFunction2Register()
        self.assertEqual(reg.Pack(), 0)

    def test_newly_created_control_function_3_register(self):
        reg = Hpcc.clockRegs.ControlFunction3Register()
        self.assertEqual(reg.Pack(), 0)

    def test_newly_created_control_function_4_register(self):
        reg = Hpcc.clockRegs.ControlFunction4Register()
        self.assertEqual(reg.Pack(), 0)

    def test_newly_created_usr_register(self):
        reg = Hpcc.clockRegs.UsrRegister()
        self.assertEqual(reg.Pack(), 0)

    def test_control_function_1_register_fields(self):
        self._check_field(register='ControlFunction1Register', field='LsbFirstMode', position=0, width=1)
        self._check_field(register='ControlFunction1Register', field='SdioInputOnly', position=1, width=1)
        self._check_field(register='ControlFunction1Register', field='Reserved1', position=2, width=1)
        self._check_field(register='ControlFunction1Register', field='ExternalPowerDownControl', position=3, width=1)
        self._check_field(register='ControlFunction1Register', field='Reserved2', position=4, width=1)
        self._check_field(register='ControlFunction1Register', field='RefClkInputPowerDown', position=5, width=1)
        self._check_field(register='ControlFunction1Register', field='DacPowerDown', position=6, width=1)
        self._check_field(register='ControlFunction1Register', field='DigitalPowerDown', position=7, width=1)
        self._check_field(register='ControlFunction1Register', field='OskEnable', position=8, width=1)
        self._check_field(register='ControlFunction1Register', field='ExternalOskEnable', position=9, width=1)
        self._check_field(register='ControlFunction1Register', field='Reserved3', position=10, width=1)
        self._check_field(register='ControlFunction1Register', field='ClearPhaseAccumulator', position=11, width=1)
        self._check_field(register='ControlFunction1Register', field='ClearDigitalRampAccumulator', position=12, width=1)
        self._check_field(register='ControlFunction1Register', field='AutoclearPhaseAccumulator', position=13, width=1)
        self._check_field(register='ControlFunction1Register', field='AutoclearDigitalRampAccumulator', position=14, width=1)
        self._check_field(register='ControlFunction1Register', field='LoadLrrAtIoUpdate', position=15, width=1)
        self._check_field(register='ControlFunction1Register', field='EnableSineOutput', position=16, width=1)
        self._check_field(register='ControlFunction1Register', field='ParallelPortStreamingEnable', position=17, width=1)
        self._check_field(register='ControlFunction1Register', field='Reserved4', position=18, width=6)
        self._check_field(register='ControlFunction1Register', field='VcoCalEnable', position=24, width=1)
        self._check_field(register='ControlFunction1Register', field='Reserved5', position=25, width=7)

    def test_control_function_2_register_fields(self):
        self._check_field(register='ControlFunction2Register', field='Reserved1', position=0, width=10)
        self._check_field(register='ControlFunction2Register', field='SyncClkInvert', position=10, width=1)
        self._check_field(register='ControlFunction2Register', field='SyncClkEnable', position=11, width=1)
        self._check_field(register='ControlFunction2Register', field='Reserved2', position=12, width=1)
        self._check_field(register='ControlFunction2Register', field='DrgOverOutputEnable', position=13, width=1)
        self._check_field(register='ControlFunction2Register', field='FrequencyJumpEnable', position=14, width=1)
        self._check_field(register='ControlFunction2Register', field='MatchedLatencyEnable', position=15, width=1)
        self._check_field(register='ControlFunction2Register', field='ProgramModulusEnable', position=16, width=1)
        self._check_field(register='ControlFunction2Register', field='DigitalRampNoDwellLow', position=17, width=1)
        self._check_field(register='ControlFunction2Register', field='DigitalRampNoDwellHigh', position=18, width=1)
        self._check_field(register='ControlFunction2Register', field='DigitalRampEnable', position=19, width=1)
        self._check_field(register='ControlFunction2Register', field='DigitalRampDestination', position=20, width=2)
        self._check_field(register='ControlFunction2Register', field='ParallelDataPortEnable', position=22, width=1)
        self._check_field(register='ControlFunction2Register', field='ProfileModeEnable', position=23, width=1)
        self._check_field(register='ControlFunction2Register', field='Reserved3', position=24, width=8)

    def test_control_function_3_register_fields(self):
        self._check_field(register='ControlFunction3Register', field='MinimumLdw', position=0, width=2)
        self._check_field(register='ControlFunction3Register', field='LockDetectEnable', position=2, width=1)
        self._check_field(register='ControlFunction3Register', field='Icp', position=3, width=3)
        self._check_field(register='ControlFunction3Register', field='ManualIcpSelection', position=6, width=1)
        self._check_field(register='ControlFunction3Register', field='Reserved1', position=7, width=1)
        self._check_field(register='ControlFunction3Register', field='FeedbackDividerN', position=8, width=8)
        self._check_field(register='ControlFunction3Register', field='DoublerClockEdge', position=16, width=1)
        self._check_field(register='ControlFunction3Register', field='PllRefDisable', position=17, width=1)
        self._check_field(register='ControlFunction3Register', field='PllEnable', position=18, width=1)
        self._check_field(register='ControlFunction3Register', field='DoublerEnable', position=19, width=1)
        self._check_field(register='ControlFunction3Register', field='InputDivider', position=20, width=2)
        self._check_field(register='ControlFunction3Register', field='InputDividerReset', position=22, width=1)
        self._check_field(register='ControlFunction3Register', field='Reserved2', position=23, width=9)

    def test_control_function_4_register_fields(self):
        self._check_field(register='ControlFunction4Register', field='Default', position=0, width=24)
        self._check_field(register='ControlFunction4Register', field='DacCalEnable', position=24, width=1)
        self._check_field(register='ControlFunction4Register', field='DacCalClockPowerDown', position=25, width=1)
        self._check_field(register='ControlFunction4Register', field='AuxiliaryDividerPowerDown', position=26, width=1)
        self._check_field(register='ControlFunction4Register', field='Reserved1', position=27, width=5)

    def test_usr_register_fields(self):
        self._check_field(register='UsrRegister', field='SyncInDelayAdj', position=0, width=3)
        self._check_field(register='UsrRegister', field='SyncOutDelayAdj', position=3, width=3)
        self._check_field(register='UsrRegister', field='CalWithSync', position=6, width=1)
        self._check_field(register='UsrRegister', field='ResetSyncCounter', position=7, width=1)
        self._check_field(register='UsrRegister', field='Default', position=8, width=16)
        self._check_field(register='UsrRegister', field='PllLock', position=24, width=1)
        self._check_field(register='UsrRegister', field='DllAcquired', position=25, width=1)
        self._check_field(register='UsrRegister', field='Reserved1', position=26, width=6)

    def _check_field(self, register, field, position, width):
        reg = getattr(Hpcc.clockRegs, register)()
        max_field_value = (1 << width) - 1
        setattr(reg, field, max_field_value)
        self.assertEqual(reg.Pack(), max_field_value << position)

        reg = getattr(Hpcc.clockRegs, register)(uint=0xFFFFFFFF, length=32)
        self.assertEqual(getattr(reg, field), max_field_value)

    def test_module_constants(self):
        self.assertEqual(Hpcc.clockRegs.CONTROL_FUNCTION1_ADDR, 0x00)
        self.assertEqual(Hpcc.clockRegs.CONTROL_FUNCTION2_ADDR, 0x01)
        self.assertEqual(Hpcc.clockRegs.CONTROL_FUNCTION3_ADDR, 0x02)
        self.assertEqual(Hpcc.clockRegs.CONTROL_FUNCTION4_ADDR, 0x03)
        self.assertEqual(Hpcc.clockRegs.DIGITAL_RAMP_LOWER_LIMIT_ADDR, 0x04)
        self.assertEqual(Hpcc.clockRegs.DIGITAL_RAMP_UPPER_LIMIT_ADDR, 0x05)
        self.assertEqual(Hpcc.clockRegs.RISING_DIGITAL_RAMP_STEP_SIZE_ADDR, 0x06)
        self.assertEqual(Hpcc.clockRegs.FALLING_DIGITAL_RAMP_STEP_SIZE_ADDR, 0x07)
        self.assertEqual(Hpcc.clockRegs.DIGITAL_RAMP_RATE_ADDR, 0x08)
        self.assertEqual(Hpcc.clockRegs.LOWER_FREQUENCY_JUMP_ADDR, 0x09)
        self.assertEqual(Hpcc.clockRegs.UPPER_FREQUENCY_JUMP_ADDR, 0x0A)
        self.assertEqual(Hpcc.clockRegs.PROFILE0_FREQUENCY_TUNING_WORD_ADDR, 0x0B)
        self.assertEqual(Hpcc.clockRegs.PROFILE0_PHASE_AMPLITUDE_ADDR, 0x0C)
        self.assertEqual(Hpcc.clockRegs.PROFILE1_FREQUENCY_TUNING_WORD_ADDR, 0x0D)
        self.assertEqual(Hpcc.clockRegs.PROFILE1_PHASE_AMPLITUDE_ADDR, 0x0E)
        self.assertEqual(Hpcc.clockRegs.PROFILE2_FREQUENCY_TUNING_WORD_ADDR, 0x0F)
        self.assertEqual(Hpcc.clockRegs.PROFILE2_PHASE_AMPLITUDE_ADDR, 0x10)
        self.assertEqual(Hpcc.clockRegs.PROFILE3_FREQUENCY_TUNING_WORD_ADDR, 0x11)
        self.assertEqual(Hpcc.clockRegs.PROFILE3_PHASE_AMPLITUDE_ADDR, 0x12)
        self.assertEqual(Hpcc.clockRegs.PROFILE4_FREQUENCY_TUNING_WORD_ADDR, 0x13)
        self.assertEqual(Hpcc.clockRegs.PROFILE4_PHASE_AMPLITUDE_ADDR, 0x14)
        self.assertEqual(Hpcc.clockRegs.PROFILE5_FREQUENCY_TUNING_WORD_ADDR, 0x15)
        self.assertEqual(Hpcc.clockRegs.PROFILE5_PHASE_AMPLITUDE_ADDR, 0x16)
        self.assertEqual(Hpcc.clockRegs.PROFILE6_FREQUENCY_TUNING_WORD_ADDR, 0x17)
        self.assertEqual(Hpcc.clockRegs.PROFILE6_PHASE_AMPLITUDE_ADDR, 0x18)
        self.assertEqual(Hpcc.clockRegs.PROFILE7_FREQUENCY_TUNING_WORD_ADDR, 0x19)
        self.assertEqual(Hpcc.clockRegs.PROFILE7_PHASE_AMPLITUDE_ADDR, 0x1A)
        self.assertEqual(Hpcc.clockRegs.USR0_ADDR, 0x1B)
