################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import os
import unittest


from Hpcc.instrument import adate320Reg as adate320_regs

repo_root_path = os.path.join('..', '..', '..', '..')



class Adate320Tests(unittest.TestCase):
    '''Tests for ADATA320 devices attached to a DC FPGA'''
    
    def test_initialize(self):
        r = adate320_regs.DrvControl()
        r.enable = 1
        self.assertEqual(r.Pack(), 1)

