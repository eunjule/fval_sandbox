################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import unittest
from unittest.mock import call, Mock, patch

from Common import hilmon
from Common.hdmt_simulator import HdmtSimulator
from Hpcc.instrument.hpcc import Hpcc
from Hpcc.instrument.hpccac import HpccAc
from Hpcc.instrument.ad9914 import Ad9914
import Hpcc.instrument.hpccAcRegs as ac_registers

BAR = 0


class Ad9914Tests(unittest.TestCase):
    def test_Read(self):
        hpcc = Mock(NUMBER_TRIES=10)
        hpccac = HpccAc(hpcc, 0, 0)
        hpccac.BarRead = Mock(return_value=0xAAAAAAAA)
        hpccac.BarWrite = Mock()
        ad = Ad9914(hpcc=hpccac, isHP=False)
        ad._Read(0)
        hpccac.BarRead.assert_has_calls([call(BAR, ac_registers.AD9914Control.ADDR),
                                         call(BAR, ac_registers.AD9914RDData.ADDR)])
                                         
    def test_Write(self):
        hpcc = Mock(NUMBER_TRIES=10)
        hpccac = HpccAc(hpcc, 0, 0)
        hpccac.BarRead = Mock(return_value=0xAAAAAAAA)
        hpccac.BarWrite = Mock()
        ad = Ad9914(hpcc=hpccac, isHP=False)
        ad._Write(4, 0x11111111)
        hpccac.BarWrite.assert_has_calls([call(BAR, ac_registers.AD9914WRData.ADDR, 0x11111111),
                                          call(BAR, ac_registers.AD9914Control.ADDR, 0xAAAAAB04)])

    def test_Initialiaze(self):
        slot = 0
        old_hil = hilmon.hil
        try:
            hilmon.hil = hdmt = HdmtSimulator()
            hdmt.add_hpcc(slot)
            hpcc = Hpcc(rc=Mock(), slot=slot)
            ad = Ad9914(hpcc=hpcc.ac[0], isHP=False)
            ad.Initialize()
            ad9914_registers = hdmt.hpcc[slot].ac[0].ad9914[0].registers
            print(ad9914_registers)
            self.assertEqual(ad9914_registers[0x00], 0x00010808)
            self.assertEqual(ad9914_registers[0x01], 0x00890900)
            self.assertEqual(ad9914_registers[0x02], 0x0000191C)
            self.assertEqual(ad9914_registers[0x03], 0x00052120)
            self.assertEqual(ad9914_registers[0x04], 0x0aaaaaaa)
            self.assertEqual(ad9914_registers[0x05], 0x00000003)
            self.assertEqual(ad9914_registers[0x06], 0x00000002)
            self.assertEqual(ad9914_registers[0x1B], 0x00000800)
            for i in range(0x07, 0x1A):
                value = ad9914_registers.get(i, 0)
                self.assertEqual(value, 0x00000000, f'mismatch at address {hex(i)}')
        finally:
            hilmon.hil = old_hil
        
if __name__ == '__main__':
    unittest.main(verbosity=2)
