################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import random
import unittest
from unittest.mock import Mock, patch

from Rc2.instrument.rc2 import Rc2
import Hpcc.instrument.hpcc as hpcc
from Hpcc.instrument.hpccac import CAPTURE_BLOCK_SIZE
import Hpcc.instrument.hpccAcRegs as ac_registers


class HpccRCTrigger(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def test_triggerverifyPass(self):
        with patch('Common.hilmon.hil'), \
             patch.object(hpcc.Hpcc, 'verify_trigger_at_rctc_up'), \
             patch.object(hpcc.Hpcc, 'wait_on_trigger_at_hpccac') as mock_wait_on_trigger_at_hpccac,\
             patch.object(hpcc.Hpcc, '_generate_trigger') as mock_generate_trigger:
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=Rc2())
            mock_generate_trigger.return_value = 123
            mock_wait_on_trigger_at_hpccac.return_value = 123

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertEqual(success, True)

    def test_triggerverifyFailforDC(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch.object(random, 'getrandbits')as mock_Random:
            rc_instrument = Rc2()
            mock_hil.rcBarRead = Mock(return_value=0x2877c9a)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=rc_instrument)
            mock_hil.hpccDcBarRead = Mock(return_value=0x2877caa)
            mock_hil.hpccAcBarRead = Mock(return_value=0x2877c9b)
            mock_Random.return_value = 0x2877c9a
            hpcc_instrument.Log = Mock()

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertFalse(success)

    def test_triggerverifyFailforRC(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch.object(random, 'getrandbits')as mock_Random:
            rc_instrument = Rc2()
            mock_hil.rcBarRead = Mock(return_value=0x2877caa)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=rc_instrument)
            mock_hil.hpccDcBarRead = Mock(return_value=0x2877c9a)
            mock_hil.hpccAcBarRead = Mock(return_value=0x2877c9b)
            mock_Random.return_value = 0x2877c9a
            hpcc_instrument.Log = Mock()

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertFalse(success)

    def test_triggerverifyFailforAC(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch.object(random, 'getrandbits')as mock_Random:
            rc_instrument = Rc2()
            mock_hil.rcBarRead = Mock(return_value=0x2877c9a)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=rc_instrument)
            mock_hil.hpccDcBarRead = Mock(return_value=0x2877c9a)
            mock_hil.hpccAcBarRead = Mock(return_value=0x2877caa)
            mock_Random.return_value = 0x2877c9a
            hpcc_instrument.Log = Mock()

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertFalse(success)

    def test_triggerverifyFailforACwithloop(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch.object(random, 'getrandbits')as mock_Random:
            rc_instrument = Rc2()
            mock_hil.rcBarRead = Mock(return_value=0x2877c9a)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=rc_instrument)
            mock_hil.hpccDcBarRead = Mock(return_value=0x2877c9a)
            hpcc_instrument.wait_on_trigger_at_hpccac =\
                Mock(side_effect=[0x2877c9a, 0x2877c9a, 0x2877c9a, 0x2877caa])
            mock_Random.return_value = 0x2877c9a
            hpcc_instrument.Log = Mock()

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertFalse(success)

    def test_triggerverifyFailforRCwithloop(self):
         with patch('Common.hilmon.hil') as mock_hil, \
                patch.object(random, 'getrandbits') as mock_Random,\
                patch.object(hpcc.Hpcc, 'verify_trigger_at_rctc_up'):
            rc_instrument = Rc2()
            mock_hil.rcBarRead =\
                Mock(side_effect=[0x2877c9b, 0x2877c9a, 0x2877c9a, 0x2877caa])
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=rc_instrument)
            mock_hil.hpccDcBarRead = Mock(return_value=0x2877c9a)
            mock_hil.hpccAcBarRead = Mock(return_value=0x2877c9b)
            mock_Random.return_value = 0x2877c9a
            hpcc_instrument.Log = Mock()

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertFalse(success)

    def test_triggerverifyFailforDCwithloop(self):
        with patch('Common.hilmon.hil') as mock_hil, \
                patch.object(random, 'getrandbits')as mock_Random:
            rc_instrument = Rc2()
            mock_hil.rcBarRead = Mock(return_value=0x2877c9a)
            hpcc_instrument = hpcc.Hpcc(slot=5, rc=rc_instrument)
            mock_hil.hpccDcBarRead =\
                Mock(side_effect=[0x2877c9b, 0x2877c9a, 0x2877c9a, 0x2877c9a,
                                  0x2877c9a, 0x2877caa])
            mock_hil.hpccAcBarRead = Mock(return_value=0x2877c9b)
            mock_Random.return_value = 0x2877c9a
            hpcc_instrument.Log = Mock()

            success = hpcc.Hpcc.check_link_stability(hpcc_instrument, 0)
            self.assertFalse(success)


class HpccAcTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def test_create_Hpcc(self):
        dut = hpcc.Hpcc(slot=5, rc=self.mock_rc)

    def test_ReadCapture(self):
        blocks_captured = 10
        capture_buffer_size_in_blocks = 100
        captured_vector_count = 42

        dut = hpcc.Hpcc(slot=7, rc=self.mock_rc)
        bar_read_table = {
            ac_registers.TotalCaptureBlockCount.ADDR: blocks_captured,
            ac_registers.CaptureLength.ADDR: capture_buffer_size_in_blocks,
            ac_registers.TotalCaptureCount.ADDR: captured_vector_count
        }

        def mock_bar_read(bar, offset):
            return bar_read_table.get(offset, 0)

        def mock_dma_read(offset, length):
            return bytes(length)

        dut.ac[0].BarRead = Mock(side_effect=mock_bar_read)
        dut.ac[0].DmaRead = Mock(side_effect=mock_dma_read)
        data, blocks, vectors = dut.ReadCapture(slice=0)
        self.assertEqual(len(data), blocks_captured * CAPTURE_BLOCK_SIZE)
        self.assertEqual(blocks, blocks_captured)
        self.assertEqual(vectors, captured_vector_count)

    def test_ReadCapture_with_more_blocks_than_can_fit_in_capture_buffer(self):
        blocks_captured = 100
        capture_buffer_size_in_blocks = 20
        captured_vector_count = 66

        dut = hpcc.Hpcc(slot=7, rc=self.mock_rc)
        bar_read_table = {
            ac_registers.TotalCaptureBlockCount.ADDR: blocks_captured,
            ac_registers.CaptureLength.ADDR: capture_buffer_size_in_blocks,
            ac_registers.TotalCaptureCount.ADDR: captured_vector_count
        }

        def mock_bar_read(bar, offset):
            return bar_read_table.get(offset, 0)

        def mock_dma_read(offset, length):
            return bytes(length)

        dut.ac[0].BarRead = Mock(side_effect=mock_bar_read)
        dut.ac[0].DmaRead = Mock(side_effect=mock_dma_read)
        data, blocks, vectors = dut.ReadCapture(slice=0)
        self.assertEqual(len(data), capture_buffer_size_in_blocks * CAPTURE_BLOCK_SIZE)
        self.assertEqual(blocks, capture_buffer_size_in_blocks)
        self.assertEqual(vectors, captured_vector_count)

    def test_ReadCapture_with_truncated_dma_read(self):
        blocks_captured = 38
        capture_buffer_size_in_blocks = 256
        captured_vector_count = 101
        dma_read_buffer_size = 2432

        dut = hpcc.Hpcc(slot=7, rc=self.mock_rc)
        bar_read_table = {
            ac_registers.TotalCaptureBlockCount.ADDR: blocks_captured,
            ac_registers.CaptureLength.ADDR: capture_buffer_size_in_blocks,
            ac_registers.TotalCaptureCount.ADDR: captured_vector_count
        }

        def mock_bar_read(bar, offset):
            return bar_read_table.get(offset, 0)

        def mock_dma_read(offset, length):
            return bytes(dma_read_buffer_size)

        dut.ac[0].BarRead = Mock(side_effect=mock_bar_read)
        dut.ac[0].DmaRead = Mock(side_effect=mock_dma_read)
        data, blocks, vectors = dut.ReadCapture(slice=0)
        self.assertEqual(len(data), dma_read_buffer_size)
        self.assertEqual(blocks, len(data) // CAPTURE_BLOCK_SIZE)
        self.assertEqual(vectors, captured_vector_count)
