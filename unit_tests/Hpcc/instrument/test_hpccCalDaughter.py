################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import unittest
from unittest.mock import call, patch, Mock

from Hpcc.instrument.hpccCalDaughter import HpccCalDaughter

class HpccTests(unittest.TestCase):
    def test_Connect_with_no_card(self):
        with patch('Hpcc.instrument.hpccCalDaughter.hil') as hil:
            hil.calHpccConnect = Mock(side_effect=RuntimeError)
            cal = HpccCalDaughter(slot=5)
            cal.Connect()
            self.assertFalse(cal.cardFound)
            
    def test_Connect_with_card(self):
        with patch('Hpcc.instrument.hpccCalDaughter.hil') as hil:
            hil.calHpccConnect = Mock(side_effect=None)
            cal = HpccCalDaughter(slot=5)
            cal.Connect()
            self.assertTrue(cal.cardFound)
            
    def test_Initialize(self):
        with patch('Hpcc.instrument.hpccCalDaughter.hil') as hil:
            write = hil.calHpccPca9506Write = Mock()
            slot = 7
            cal = HpccCalDaughter(slot=slot)
            cal.Initialize()
            write.assert_has_calls([call(slot, 0x08, 0), 
                                    call(slot, 0x09, 0), 
                                    call(slot, 0x0A, 0), 
                                    call(slot, 0x0B, 0), 
                                    call(slot, 0x0C, 0), 
                                    call(slot, 0x18, 0), 
                                    call(slot, 0x19, 0), 
                                    call(slot, 0x1A, 0), 
                                    call(slot, 0x1B, 0), 
                                    call(slot, 0x1C, 0), 
                                    call(slot, 0x10, 0), 
                                    call(slot, 0x11, 0), 
                                    call(slot, 0x12, 0), 
                                    call(slot, 0x13, 0), 
                                    call(slot, 0x14, 0)], any_order=True)
            
            