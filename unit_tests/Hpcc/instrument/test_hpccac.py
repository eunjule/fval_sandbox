################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import os
import shutil
import tempfile
import unittest
from unittest.mock import Mock

from Common.fval.core import LoggedError
import Hpcc.instrument.hpccac as hpccac
import Hpcc.instrument.hpccAcRegs as ac_registers

BAR = 0


class HpccAcTests(unittest.TestCase):
    def test_Read(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarRead = Mock(return_value=0xAAAAAAAA)
        r = ac.Read('ResetsAndStatus')
        ac.BarRead.assert_called_once_with(BAR, ac_registers.ResetsAndStatus.ADDR)
        self.assertEqual(type(r), ac_registers.ResetsAndStatus)
        self.assertEqual(r.ResetDDR3IpBlocks, 0)
    
    def test_ReadChannel(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarRead = Mock(return_value=0xBBBBBBBB)
        r = ac.ReadChannel(12, 'PerChannelControlConfig1')
        ac.BarRead.assert_called_once_with(BAR, ac_registers.PerChannelControlConfig1.address(12))
        self.assertEqual(type(r), ac_registers.PerChannelControlConfig1)
        self.assertEqual(r.OutputQdrPhaseSelect, 3)

    def test_Write_with_integer(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarWrite = Mock()
        ac.Write('TotalFailCount', 42)
        ac.BarWrite.assert_called_once_with(BAR, ac_registers.TotalFailCount.ADDR, 42)

    def test_WriteChannel_with_integer(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarWrite = Mock()
        ac.WriteChannel(55, 'ChannelFrequencyCounts', 123456)
        ac.BarWrite.assert_called_once_with(BAR, ac_registers.ChannelFrequencyCounts.address(55), 123456)

    def test_Write_with_structure(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarWrite = Mock()
        ac.Write('TotalFailCount', ac_registers.DPinDriveEdge(0x99999999))
        ac.BarWrite.assert_called_once_with(BAR, ac_registers.TotalFailCount.ADDR, 0x99999999)

    def test_WriteChannel_with_structure(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarWrite = Mock()
        ac.WriteChannel(32, 'ChannelFrequencyCounts', ac_registers.DPinCompareEdge(654321))
        ac.BarWrite.assert_called_once_with(BAR, ac_registers.ChannelFrequencyCounts.address(32), 654321)
        
    def test_TakeRegistersSnapshot(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarRead = Mock(return_value=0xCCCCCCCC)
        tmp_dir = tempfile.mkdtemp()
        tmp_file = os.path.join(tmp_dir, 'test_TakeRegistersSnapshot.csv')
        ac.TakeRegistersSnapshot(tmp_file)
        with open(tmp_file) as f:
            for line in f:
                if 'UncorrectableECCCount' in line:
                    break
            else:
                self.fail('did not print the UncorrectableECCCount0 register')
        shutil.rmtree(tmp_dir)
    
    def test_ReadCapture_with_1024_blocks(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        def mock_bar_read(bar, offset):
            if offset == ac_registers.TotalCaptureBlockCount.ADDR:
                return 1024
            elif offset == ac_registers.CaptureBaseAddress.ADDR:
                return 0x1000
            else:
                raise Exception('Called an unexpected register')
        ac.BarRead = Mock(side_effect=mock_bar_read)
        ac.DmaRead = Mock(side_effect=lambda address, length: bytes(length))
        capture_data = ac.ReadCapture()
        actual_bytes = len(capture_data)
        expected_bytes = hpccac.CAPTURE_BLOCK_SIZE * 1024
        if actual_bytes != expected_bytes:
            self.fail('Capture data returned ({} bytes) is not the correct size ({} bytes)'.format(actual_bytes, expected_bytes))
    
    def test_ReadCapture_with_0_blocks(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        def mock_bar_read(bar, offset):
            if offset == ac_registers.TotalCaptureBlockCount.ADDR:
                return 0
            elif offset == ac_registers.CaptureBaseAddress.ADDR:
                return 0
            else:
                raise Exception('Called an unexpected register')
        ac.BarRead = Mock(side_effect=mock_bar_read)
        ac.DmaRead = Mock(side_effect=lambda address, length: bytes(length))
        capture_data = ac.ReadCapture()
        actual_bytes = len(capture_data)
        expected_bytes = 0
        if actual_bytes != expected_bytes:
            self.fail('Capture data returned ({} bytes) is not the correct size ({} bytes)'.format(actual_bytes, expected_bytes))
    
    def test_ReadCapture_with_over_2GB_of_captures(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        def mock_bar_read(bar, offset):
            if offset == ac_registers.TotalCaptureBlockCount.ADDR:
                return 1024*1024*1024
            elif offset == ac_registers.CaptureBaseAddress.ADDR:
                return 0
            else:
                raise Exception('Called an unexpected register')
        ac.BarRead = Mock(side_effect=mock_bar_read)
        ac.DmaRead = Mock(side_effect=lambda address, length: bytes(length))
        capture_data = ac.ReadCapture()
        actual_bytes = len(capture_data)
        expected_bytes = 1024*1024
        if actual_bytes != expected_bytes:
            self.fail('Capture data returned ({} bytes) is not the correct size ({} bytes)'.format(actual_bytes, expected_bytes))
    
    def test_CheckAlarms_returns_all_active_alarms(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.BarRead = Mock(return_value=0xFFFFFFFF)
        with self.assertRaises(LoggedError):
            alarms = ac.CheckAlarms()
            self.assertEqual(sorted(alarms), sorted(ac_registers.noncritical_alarms + ac_registers.critical_alarms))

    def test_SetDomainPeriod(self):
        ac = hpccac.HpccAc(hpcc=Mock(), slot=0, slice=0)
        ac.read_register = Mock(return_value=ac_registers.Capability0(TimingVersion=5))
        ac.write_register = Mock()
        ac._set_ddr_request_interval = Mock()
        ac.minQdrMode = 300
        ac.maxPeriod = 1000
        ac.SetDomainPeriod(picoseconds(600))
        with self.assertRaises(hpccac.HpccAc.DomainPeriodError):
            ac.SetDomainPeriod(picoseconds(200))
        with self.assertRaises(hpccac.HpccAc.DomainPeriodError):
            ac.SetDomainPeriod(picoseconds(1200))

def picoseconds(seconds):
    return seconds * 1e-12
