################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################

import ctypes
import unittest

import Hpcc.instrument.hpccAcRegs as ac_registers
import Common.register as register

class HpccAcRegsTests(unittest.TestCase):
    def setUp(self):
        self.reg_types = ac_registers.get_register_types()
        
    def test_registers_are_four_bytes_in_size(self):
        for RegisterType in self.reg_types:
            if ctypes.sizeof(RegisterType) != 4:
                self.fail('{} is not 4 bytes in size'.format(RegisterType.__name__))

    def test_all_32_bits_are_specified(self):
        FIELD_WIDTH_SPECIFIER_INDEX = 2
        for RegisterType in self.reg_types:
            if 32 != sum([x[FIELD_WIDTH_SPECIFIER_INDEX] for x in RegisterType._fields_]):
                self.fail('{} is not fully specified for all 32 bits'.format(RegisterType.__name__))

    def test_no_addresses_are_reused(self):
        seen = {}
        for RegisterType in self.reg_types:
            if RegisterType.ADDR is None:
                continue
            if seen.get(RegisterType.ADDR) is None:
                seen[RegisterType.ADDR] = RegisterType.__name__
            else:
                self.fail('Address 0x{:04X} is shared by {} and {}'.format(RegisterType.ADDR, RegisterType.__name__, seen[RegisterType.ADDR]))

    def test_alarms_are_in_critical_or_noncritical_list(self):
        all_alarms = sorted(register.get_field_names(ac_registers.AlarmControl, ac_registers.AlarmControl1))
        critical_and_non_critical_alarms = sorted(ac_registers.critical_alarms + ac_registers.noncritical_alarms)
        self.assertEqual(all_alarms, critical_and_non_critical_alarms)
        