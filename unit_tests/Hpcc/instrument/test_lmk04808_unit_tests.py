################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock
from unittest.mock import PropertyMock

from Common import fval
from Hpcc.instrument import lmk04808

ARG_LIST = 0

class Lmk04808Tests(unittest.TestCase):
    '''Tests for LMK04808 devices attached to an AC FPGA'''
    
    def test_sync_is_cleared(self):
        '''Check that the LMK writes are not gated by the sync pulse'''
        resets_register = Mock()
        resets_register.Lmk04808Sync = PropertyMock()
        
        hpcc = Mock()
        hpcc.Write = Mock()
        hpcc.Read = Mock(return_value=resets_register)
        
        lmk = lmk04808.Lmk04808(hpcc)
        lmk.Initialize(0)
        
        address, data = hpcc.Write.mock_calls[0][1]
        self.assertEqual(address, 'ResetsAndStatus')
        self.assertEqual(data.Lmk04808Sync, 0)
    
    def test_hpcc2_snap_in_clock_is_250_megahertz(self):
        '''HPCC2 Interlaken interface requires 250MHz clock for manufacturing test'''
        hpcc = Mock()
        hpcc.Write = Mock()
        
        lmk = lmk04808.Lmk04808(hpcc)
        lmk.Initialize(fineDelayDivider=0, hpcc2=True)
        
        lmk_address = None
        lmk_data = None
        for call in hpcc.Write.call_args_list:
            address = call[ARG_LIST][0]
            data = call[ARG_LIST][1]
            if address == 'LMK4808WRData':
                lmk_data = data.WRData
            if address == 'LMK4808Control':
                lmk_address = data.Address
                if lmk_address == 3:
                    self.assertEqual(lmk_data & 0x3FF, 24)

RESETS_REGISTER = 0x8110


class MockLmk04808ControlRegister():
    def __init__(self):
        Address = 0
        RdEnable = 0


class MockLmk04808DataRegister():
    def __init__(self):
        WRData = 0
    

class MockRegisters():
    LMK4808Control = MockLmk04808ControlRegister
    LMK4808WRData = MockLmk04808DataRegister


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
