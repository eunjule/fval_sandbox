################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest

from Common import fval
from Rc2.instrument import rc_registers


class TesterTests(unittest.TestCase):
    def setUp(self):
        self.regs = rc_registers

    def test_bars(self):
        self.check_bar('AuroraStatus1', 1)
        self.check_bar('AuroraStatus2', 1)
        self.check_bar('TriggerOverrun', 1)
        self.check_bar('TriggerOverrun32', 1)
        self.check_bar('TriggerStatus', 1)
        self.check_bar('TriggerStatus32', 1)
        self.check_bar('TriggerHistoryStatus', 1)
        self.check_bar('TriggerHistoryStatus32', 1)
        self.check_bar('SoftwareTrigger', 1)
        self.check_bar('SoftwareTrigger32', 1)
        self.check_bar('TriggerSync', 1)
        self.check_bar('DownTriggerValue',  1)
        self.check_bar('UpTriggerValueSlot0',  1)
        self.check_bar('UpTriggerValueSlot1',  1)
        self.check_bar('UpTriggerValueSlot2',  1)
        self.check_bar('UpTriggerValueSlot3',  1)
        self.check_bar('UpTriggerValueSlot4',  1)
        self.check_bar('UpTriggerValueSlot5',  1)
        self.check_bar('UpTriggerValueSlot6',  1)
        self.check_bar('UpTriggerValueSlot7',  1)
        self.check_bar('UpTriggerValueSlot8',  1)
        self.check_bar('UpTriggerValueSlot9',  1)
        self.check_bar('UpTriggerValueSlot10',  1)
        self.check_bar('UpTriggerValueSlot11',  1)
        self.check_bar('FifoStatusReg', 1)
        self.check_bar('Reset', 0)
        self.check_bar('GeneralPurposeStatus', 0)
        self.check_bar('GeneralPurposeOutputReg', 0) 
        self.check_bar('CapabilitiesReg', 0)
    
    def check_bar(self, register_name, bar):
        r = getattr(self.regs, register_name)()
        self.assertEqual(r.BAR, bar)

    def test_addresses(self):
        self.check_address('AuroraStatus1', 0x0110)
        self.check_address('AuroraStatus2', 0x0100)
        self.check_address('TriggerOverrun', 0x0120)
        self.check_address('TriggerOverrun32', 0x0128)
        self.check_address('TriggerStatus', 0x0130)
        self.check_address('TriggerStatus32', 0x0138)
        self.check_address('TriggerHistoryStatus', 0x0140)
        self.check_address('TriggerHistoryStatus32', 0x0148)
        self.check_address('SoftwareTrigger', 0x0150)
        self.check_address('SoftwareTrigger32', 0x0158)
        self.check_address('TriggerSync', 0x0160)
        self.check_address('DownTriggerValue',  0x0168)
        self.check_address('UpTriggerValueSlot0',  0x0170)
        self.check_address('UpTriggerValueSlot1',  0x0178)
        self.check_address('UpTriggerValueSlot2',  0x0180)
        self.check_address('UpTriggerValueSlot3',  0x0188)
        self.check_address('UpTriggerValueSlot4',  0x0190)
        self.check_address('UpTriggerValueSlot5',  0x0198)
        self.check_address('UpTriggerValueSlot6',  0x01A0)
        self.check_address('UpTriggerValueSlot7',  0x01A8)
        self.check_address('UpTriggerValueSlot8',  0x01B0)
        self.check_address('UpTriggerValueSlot9',  0x01B8)
        self.check_address('UpTriggerValueSlot10',  0x01C0)
        self.check_address('UpTriggerValueSlot11',  0x01C8)
        self.check_address('FifoStatusReg', 0x01D0)
        self.check_address('Reset', 0x8010)
        self.check_address('GeneralPurposeStatus', 0x8090)
        self.check_address('GeneralPurposeOutputReg', 0x80A0) 
        self.check_address('CapabilitiesReg', 0x80B0)
    
    def check_address(self, register_name, address):
        r = getattr(self.regs, register_name)()
        self.assertEqual(r.ADDR, address)
    
    def test_aurora_status1(self):
        self.check_field('AuroraStatus1', 'Aurora0_HardError', 1, 0x00000001)
        self.check_field('AuroraStatus1', 'Aurora0_SoftError', 1, 0x00000002)
        self.check_field('AuroraStatus1', 'Aurora0_ChannelUp', 1, 0x00000004)
        self.check_field('AuroraStatus1', 'Aurora0_LaneUp', 1, 0x00000008)
        self.check_field('AuroraStatus1', 'Aurora1_HardError', 1, 0x00000010)
        self.check_field('AuroraStatus1', 'Aurora1_SoftError', 1, 0x00000020)
        self.check_field('AuroraStatus1', 'Aurora1_ChannelUp', 1, 0x00000040)
        self.check_field('AuroraStatus1', 'Aurora1_LaneUp', 1, 0x00000080)
        self.check_field('AuroraStatus1', 'Aurora2_HardError', 1, 0x00000100)
        self.check_field('AuroraStatus1', 'Aurora2_SoftError', 1, 0x00000200)
        self.check_field('AuroraStatus1', 'Aurora2_ChannelUp', 1, 0x00000400)
        self.check_field('AuroraStatus1', 'Aurora2_LaneUp', 1, 0x00000800)
        self.check_field('AuroraStatus1', 'Aurora3_HardError', 1, 0x00001000)
        self.check_field('AuroraStatus1', 'Aurora3_SoftError', 1, 0x00002000)
        self.check_field('AuroraStatus1', 'Aurora3_ChannelUp', 1, 0x00004000)
        self.check_field('AuroraStatus1', 'Aurora3_LaneUp', 1, 0x00008000)
        self.check_field('AuroraStatus1', 'Aurora4_HardError', 1, 0x00010000)
        self.check_field('AuroraStatus1', 'Aurora4_SoftError', 1, 0x00020000)
        self.check_field('AuroraStatus1', 'Aurora4_ChannelUp', 1, 0x00040000)
        self.check_field('AuroraStatus1', 'Aurora4_LaneUp', 1, 0x00080000)
        self.check_field('AuroraStatus1', 'Aurora5_HardError', 1, 0x00100000)
        self.check_field('AuroraStatus1', 'Aurora5_SoftError', 1, 0x00200000)
        self.check_field('AuroraStatus1', 'Aurora5_ChannelUp', 1, 0x00400000)
        self.check_field('AuroraStatus1', 'Aurora5_LaneUp', 1, 0x00800000)
        self.check_field('AuroraStatus1', 'Aurora6_HardError', 1, 0x01000000)
        self.check_field('AuroraStatus1', 'Aurora6_SoftError', 1, 0x02000000)
        self.check_field('AuroraStatus1', 'Aurora6_ChannelUp', 1, 0x04000000)
        self.check_field('AuroraStatus1', 'Aurora6_LaneUp', 1, 0x08000000)
        self.check_field('AuroraStatus1', 'Aurora7_HardError', 1, 0x10000000)
        self.check_field('AuroraStatus1', 'Aurora7_SoftError', 1, 0x20000000)
        self.check_field('AuroraStatus1', 'Aurora7_ChannelUp', 1, 0x40000000)
        self.check_field('AuroraStatus1', 'Aurora7_LaneUp', 1, 0x80000000)
    
    def test_aurora_status2(self):
        self.check_field('AuroraStatus2', 'Aurora8_HardError', 1, 0x00000001)
        self.check_field('AuroraStatus2', 'Aurora8_SoftError', 1, 0x00000002)
        self.check_field('AuroraStatus2', 'Aurora8_ChannelUp', 1, 0x00000004)
        self.check_field('AuroraStatus2', 'Aurora8_LaneUp', 1, 0x00000008)
        self.check_field('AuroraStatus2', 'Aurora9_HardError', 1, 0x00000010)
        self.check_field('AuroraStatus2', 'Aurora9_SoftError', 1, 0x00000020)
        self.check_field('AuroraStatus2', 'Aurora9_ChannelUp', 1, 0x00000040)
        self.check_field('AuroraStatus2', 'Aurora9_LaneUp', 1, 0x00000080)
        self.check_field('AuroraStatus2', 'Aurora10_HardError', 1, 0x00000100)
        self.check_field('AuroraStatus2', 'Aurora10_SoftError', 1, 0x00000200)
        self.check_field('AuroraStatus2', 'Aurora10_ChannelUp', 1, 0x00000400)
        self.check_field('AuroraStatus2', 'Aurora10_LaneUp', 1, 0x00000800)
        self.check_field('AuroraStatus2', 'Aurora11_HardError', 1, 0x00001000)
        self.check_field('AuroraStatus2', 'Aurora11_SoftError', 1, 0x00002000)
        self.check_field('AuroraStatus2', 'Aurora11_ChannelUp', 1, 0x00004000)
        self.check_field('AuroraStatus2', 'Aurora11_LaneUp', 1, 0x00008000)
        self.check_field('AuroraStatus2', 'AuroraBlock0LockBits', 0x1F, 0x001F0000)
        self.check_field('AuroraStatus2', 'AuroraBlock1LockBits', 0x1F, 0x03E00000)
        self.check_field('AuroraStatus2', 'AuroraBlock2LockBits', 0x1F, 0x7C000000)
        self.check_field('AuroraStatus2', 'Reserved', 1, 0x80000000)
        
        
    def test_trigger_overrun(self):
        self.check_field('TriggerOverrun', 'TriggerValue', 0xFFFF, 0x0000FFFF)
        self.check_field('TriggerOverrun', 'TriggerSource', 0xF, 0x000F0000)
        self.check_field('TriggerOverrun', 'TriggerID', 0x3FF, 0x3FF00000)
        self.check_field('TriggerOverrun', 'OverrunIndicator', 1, 0x40000000)
        self.check_field('TriggerOverrun', 'Reserved', 1, 0x80000000)
    
    def test_trigger_overrun32(self):
        self.check_field('TriggerOverrun32', 'TriggerValue', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_trigger_status(self):
        self.check_field('TriggerStatus', 'TriggerValue', 0xFFFF, 0x0000FFFF)
        self.check_field('TriggerStatus', 'TriggerSource', 0xF, 0x000F0000)
        self.check_field('TriggerStatus', 'TriggerID', 0xFF, 0x0FF00000)
        self.check_field('TriggerStatus', 'QueuedTriggers', 1, 0x10000000)
        self.check_field('TriggerStatus', 'StopReceived', 1, 0x20000000)
        self.check_field('TriggerStatus', 'TriggerDataValid', 1, 0x40000000)
        self.check_field('TriggerStatus', 'Reserved', 1, 0x80000000)
    
    def test_trigger_status32(self):
        self.check_field('TriggerStatus32', 'TriggerValue', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_trigger_history_status(self):
        self.check_field('TriggerHistoryStatus', 'TriggerValue', 0xFFFF, 0x0000FFFF)
        self.check_field('TriggerHistoryStatus', 'TriggerSource', 0xF, 0x000F0000)
        self.check_field('TriggerHistoryStatus', 'TriggerID', 0xFF, 0x0FF00000)
        self.check_field('TriggerHistoryStatus', 'QueuedTriggers', 1, 0x10000000)
        self.check_field('TriggerHistoryStatus', 'StopReceived', 1, 0x20000000)
        self.check_field('TriggerHistoryStatus', 'TriggerDataValid', 1, 0x40000000)
        self.check_field('TriggerHistoryStatus', 'Reserved', 1, 0x80000000)
    
    def test_trigger_history_status32(self):
        self.check_field('TriggerHistoryStatus32', 'TriggerValue', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_software_trigger(self):
        self.check_field('SoftwareTrigger', 'TriggerValue', 0xFFFF, 0x0000FFFF)
        self.check_field('SoftwareTrigger', 'TriggerActive', 1, 0x00010000)
        self.check_field('SoftwareTrigger', 'TriggerSystemInterruptEnable', 1, 0x00020000)
        self.check_field('SoftwareTrigger', 'TriggerSystemInterruptEnableWriteEnable', 1, 0x00040000)
        self.check_field('SoftwareTrigger', 'Reserved', 0xFFF, 0x7FF80000)
        self.check_field('SoftwareTrigger', 'SoftwareEnabled', 1, 0x80000000)
    
    def test_software_trigger32(self):
        self.check_field('SoftwareTrigger32', 'TriggerValue', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_trigger_sync(self):
        self.check_field('TriggerSync', 'TriggerSyncValue', 0xFFF, 0x00000FFF)
        self.check_field('TriggerSync', 'Reserved', 0xFFFFF, 0xFFFFF000)
    
    def test_down_trigger_value(self):
        self.check_field('DownTriggerValue', 'Data', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_up_trigger_value(self):
        for slot in range(12):
            self.check_field(f'UpTriggerValueSlot{slot}', 'Data', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_fifo_status_reg(self):
        self.check_field('FifoStatusReg', 'AuroraInputFIFOEmptySignals', 0xFFF, 0x00000FFF)
        self.check_field('FifoStatusReg', 'AuroraInputFIFOFullSignals', 0xFFF, 0x00FFF000)
        self.check_field('FifoStatusReg', 'AuroraOutputFIFOEmptySignals', 0x7, 0x07000000)
        self.check_field('FifoStatusReg', 'AuroraOutputFIFOFullSignals', 0x7, 0x38000000)
        self.check_field('FifoStatusReg', 'Reserved', 0x3, 0xC0000000)
    
    def test_reset(self):
        for slot in range(12):
            self.check_field('Reset', f'Aurora{slot}_Reset', 1, 1 << slot)
        self.check_field('Reset', 'AuroraResetCards0_3', 0x1, 0x00001000)
        self.check_field('Reset', 'AuroraResetCards4_7', 0x1, 0x00002000)
        self.check_field('Reset', 'AuroraResetCards8_11', 0x1, 0x00004000)
        self.check_field('Reset', 'Reserved', 0x1FFFF, 0xFFFF8000)
    
    def test_general_purpose_status(self):
        self.check_field('GeneralPurposeStatus', 'TIUInstallStatus', 1, 0x000000001)
        self.check_field('GeneralPurposeStatus', 'Reserved', 0x7FFFFFFF, 0xFFFFFFFE)
    
    def test_general_purpose_output_reg(self):
        self.check_field('GeneralPurposeOutputReg', 'Data', 0xFFFFFFFF, 0xFFFFFFFF)
    
    def test_capabilities_reg(self):
        self.check_field('CapabilitiesReg', 'Data', 0xFFFFFFFF, 0xFFFFFFFF)

    def check_field(self, register, field, value, packed_value):
        r = getattr(self.regs, register)(uint=0, length=32)
        setattr(r, field, value)
        self.assertEqual(r.Pack(), packed_value)

if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
