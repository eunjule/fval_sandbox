################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from Common import fval
from unittest.mock import patch
from unittest.mock import Mock
from Tdaubnk.Tests.S2H import TdaubnkS2H
from Tdaubnk.instrument.tdaubnk import Tdaubnk


class TdaubnkS2HTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = Mock()

    def test_to_check_if_inheritance_works(self):
        with patch('Tdaubnk.instrument.tdaubnk.hil') as mock_tdaubnk_hil:
            buffer_size = 29999
            addr = 1020
            num_of_iterations = 5
            tdaubnk_s2h = TdaubnkS2H()
            my_tdaubnk = Tdaubnk(slot=1,rc=self.mock_rc)
            tdaubnk_s2h.env = Mock()
            tdaubnk_s2h.Log = Mock(side_effect=self.assert_on_logged_error)
            tdaubnk_s2h.env.duts= [my_tdaubnk]
            tdaubnk_s2h.WaitTime = Mock()
            mock_tdaubnk_hil.tdbStreamingBuffer = Mock(return_value=[buffer_size,addr])
            mock_tdaubnk_hil.tdbBarRead = Mock(side_effect=[500,1, 750, 1000, 1500, 249, 499,779,999,1500])
            my_tdaubnk.IsS2HConfigurationDone = Mock(return_value=True)
            with self.assertRaises(fval.LoggedError):
                tdaubnk_s2h.DirectedS2HPacketCountIncrementTest(num_of_iterations)
    
    def assert_on_logged_error(self, level, message):
        if level.lower() == 'error':
            raise fval.LoggedError()
