# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest import TestCase
from unittest.mock import patch

from Common.fval.core import LoggedError
from Tdaubnk.tdaubnktb.env import Env

class EnvTests(TestCase):
    def test_object_creation(self):
        with patch('Tdaubnk.tdaubnktb.env.Env.tester') as get_tester:
            get_tester.return_value = FakeTester()
            e = Env(test='fake_test')

    def test_env_tester(self):
        # without setting up any devices, expect to log an error due to no Tdaubnk
        with self.assertRaises(LoggedError):
            e = Env(test='fake_test')
            tester = e.tester()


class FakeTester:
    def get_undertest_instruments(self, name):
        return [FakeTdaubnk()]


class FakeTdaubnk:
    def __init__(self):
        self.slot = 3

    def name(self):
        return 'Tdaubnk'
