################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Tdaubnk.instrument import tdaubnk
from Common import fval


class TdaubnkInstrumentDiscoveryTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def test_discover_tdaubnk(self):
        with patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil:
            slot = random.randint(0, 11)
            mock_hil.tdbConnect = Mock(side_effect=[''])
            tdaubnk_instrument = tdaubnk.Tdaubnk(slot=slot, rc=self.mock_rc)
            discover_return = tdaubnk_instrument.discover()
            self.assertIsInstance(discover_return[0], tdaubnk.Tdaubnk)

    def test_discover_no_tdaubnk(self):
        with patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil:
            slot = random.randint(0, 11)
            mock_hil.tdbConnect = Mock(side_effect=[RuntimeError])
            tdaubnk_instrument = tdaubnk.Tdaubnk(slot=slot, rc=self.mock_rc)
            discover_return = tdaubnk_instrument.discover()
            self.assertEqual(discover_return, [])


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    unittest.main(verbosity=2)
