################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest

from unittest.mock import Mock

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

import Tdaubnk.instrument.tdaubnk as tdaubnk


class TdaubnkTests(unittest.TestCase):
    def setUp(self):
        self.mock_rc = lambda x: None

    def test_log_blt_info(self):
        tdaubnk_inst = tdaubnk.Tdaubnk(slot=0, rc=None)
        tdaubnk_inst.instrumentblt = Mock()
        tdaubnk_inst.boardblt = Mock()
        tdaubnk_inst.log_blt_info()
        if not tdaubnk_inst.instrumentblt.write_to_log.called :
            self.fail('BLT not logged')

if __name__ == '__main__':
    unittest.main(verbosity=2)
