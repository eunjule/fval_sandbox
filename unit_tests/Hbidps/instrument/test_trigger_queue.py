# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import unittest
from unittest.mock import Mock

from Hbidps.instrument import hbidps_register
from Hbidps.instrument.hbidps import Hbidps
from Hbidps.instrument.TQMachine import TriggerQueueDDR


class TriggerQueueTests(unittest.TestCase):
    def test_check_vtarget_notify(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(4))
        tq_ddr.check_tq_notify(0, 0, 1)
        if self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'unexpected error occured in tq notify checking')

    def test_check_vtarget_notify_negative(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(2))
        tq_ddr.check_tq_notify(0, 0, 1)
        if not self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'expected error didn\'t occured in tq notify checking')

    def test_wait_lc_notify(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(1))
        tq_ddr.wait_for_notify(1, 0, 0)
        if self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'unexpected warning occured in tq notify checking\n'
                      f'call_args_list: {tq_ddr.Log.call_args_list}')

    def test_wait_lc_notify_negative(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(6))
        tq_ddr.wait_for_notify(1, 0, 0)
        if not self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'expected warning didn\'t occured in tq notify checking')


    def test_wait_hc_notify(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(2))
        tq_ddr.wait_for_notify(0, 1, 0)
        if self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'unexpected warning occured in tq notify checking\n'
                      f'call_args_list: {tq_ddr.Log.call_args_list}')

    def test_wait_hc_notify_negative(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(5))
        tq_ddr.wait_for_notify(0, 1, 0)
        if not self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'expected warning didn\'t occured in tq notify checking')

    def test_wait_vtarget_notify(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(4))
        tq_ddr.wait_for_notify(0, 0, 1)
        if self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'unexpected warning occured in tq notify checking\n'
                      f'call_args_list: {tq_ddr.Log.call_args_list}')

    def test_wait_vtarget_notify_negative(self):
        tq_ddr = TriggerQueueDDR(Hbidps(0))
        tq_ddr.Log = Mock()
        tq_ddr.hbidps.read_bar_register = Mock(return_value=hbidps_register.GLOBAL_ALARMS(3))
        tq_ddr.wait_for_notify(0, 0, 1)
        if not self.check_for_log_level('error', tq_ddr.Log.call_args_list):
            self.fail(f'expected warning didn\'t occured in tq notify checking')

    def check_for_log_level(self, expected_log_level, calls):
        for args, kwargs in calls:
            log_level, message = args
            if expected_log_level == log_level:
                print(f'an {expected_log_level} was raised in test with message: {message}')
                return True
        else:
            return False

