# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
import random
from unittest.mock import Mock

from Hbidps.instrument.LCRailPMBUS import LCRailPMBUS
from Hbidps.instrument.hbidps import Hbidps


class LCRailPMBUSTest(unittest.TestCase):
    def setUp(self):
        super().setUp()
        Hbidps.bar_read = self.mock_bar_read
        Hbidps.bar_write = self.mock_bar_write
        self.hbi_dps_reg_map = {}
        self.hbidps = Hbidps(Mock())
        self.lc_rail_pmbus = LCRailPMBUS(self.hbidps)

    def test_disable_lc_chips(self):
        chips = random.sample([0, 1, 2, 3], random.randint(1, 4))
        self.hbi_dps_reg_map.update({0x1b0: random.getrandbits(32)})
        self.lc_rail_pmbus.disable_lc_chips(chips)
        ltm2975_control_reg = self.hbi_dps_reg_map.get(0x1b0, 0)
        for chip in chips:
            self.assertFalse(ltm2975_control_reg & (1 << chip))

    def test_enable_lc_chips(self):
        chips = random.sample([0, 1, 2, 3], random.randint(1, 4))
        self.hbi_dps_reg_map.update({0x1b0: random.getrandbits(32)})
        self.lc_rail_pmbus.enable_lc_chips(chips)
        ltm2975_control_reg = self.hbi_dps_reg_map.get(0x1b0, 0)
        for chip in chips:
            self.assertTrue(ltm2975_control_reg & (1 << chip))

    def test_set_power_state(self):
        chips = random.sample([0, 1, 2, 3], random.randint(1, 4))
        self.hbi_dps_reg_map.update({0x898: random.getrandbits(32)})
        for chip in chips:
            for page in range(4):
                expected_power_state = random.choice([0, 1, 2])
                original_power_state_reg = self.hbi_dps_reg_map.get(0x898)
                self.lc_rail_pmbus.set_power_state(chip, page, expected_power_state)
                observed_state_reg = self.hbi_dps_reg_map.get(0x898)
                bit_index = (chip * 8 + page * 2)
                observed_power_state = (observed_state_reg & (1 << bit_index | 1 << (bit_index + 1))) >> bit_index
                self.assertEqual(expected_power_state, observed_power_state)
                self.assertEqual(original_power_state_reg | (1 << bit_index) | (1 << (bit_index + 1)),
                                 observed_state_reg | (1 << bit_index) | (1 << (bit_index + 1)))

    def test_check_info_set(self):
        common_info_list = [
            {'Chip': 0, 'Page': -1, 'Name': 'STATUS_INPUT', 'Value': 0x0},
            {'Chip': 0, 'Page': -1, 'Name': 'STATUS_CML', 'Value': 0x0},
            {'Chip': 0, 'Page': -1, 'Name': 'MFR_FAULT_LOG_STATUS', 'Value': 0x0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_ON', 'Value': 9.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OFF', 'Value': 8.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OV_FAULT_LIMIT', 'Value': 14.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OV_WARN_LIMIT', 'Value': 15.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OV_FAULT_RESPONSE', 'Value': 0x80},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_UV_WARN_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_UV_FAULT_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_UV_FAULT_RESPONSE', 'Value': 0x80}
            ]
        paged_info_list = [
            {'Chip': 0, 'Page': 0, 'Name': 'OPERATION', 'Value': 0x0},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_BYTE', 'Value': 0x40},
            {'Chip': 0, 'Page': 0, 'Name': 'StATUS_WORD', 'Value': 0x40},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_VOUT', 'Value': 0x0},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_IOUT', 'Value': 0x0},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_TEMPERATURE', 'Value': 0x0},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_MFR_SPECIFIC', 'Value': 0x0},
            {'Chip': 0, 'Page': 0, 'Name': 'OT_FAULT_LIMIT', 'Value': 1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'OT_WARN_LIMIT', 'Value':  1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'OT_FAULT_RESPONSE', 'Value': 0x80},
            {'Chip': 0, 'Page': 0, 'Name': 'UT_WARN_LIMIT', 'Value': -1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'UT_FAULT_LIMIT', 'Value': -1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'UT_FAULT_RESPONSE', 'Value': 0x80},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MODE', 'Value': 0x13},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_COMMAND', 'Value': 5.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MAX', 'Value': 6.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MARGIN_HIGH', 'Value': 1.05},  # default value
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MARGIN_LOW', 'Value': 0.95}, # default value
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_OV_FAULT_LIMIT', 'Value': 7.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_OV_WARN_LIMIT', 'Value': 7.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_UV_WARN_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_UV_FAULT_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_OV_FAULT_RESPONSE', 'Value': 0x80},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_UV_FAULT_RESPONSE', 'Value': 0x80},
            {'Chip': 0, 'Page': 0, 'Name': 'IOUT_OC_FAULT_LIMIT', 'Value': 10.0},  # default value
            {'Chip': 0, 'Page': 0, 'Name': 'IOUT_OC_WARN_LIMIT', 'Value': 7.0},
            {'Chip': 0, 'Page': 0, 'Name': 'IOUT_UC_FAULT_LIMIT', 'Value': -10.0}
        ]
        observed_info_set = []
        for chip in range(4):
            chip_info_list = list(common_info_list)

            print(hex(id(chip_info_list)))
            for info in chip_info_list:
                info['Chip'] = chip
                observed_info_set.append(dict(info))
            for page in range(4):
                page_info_list = list(paged_info_list)
                for info in page_info_list:
                    info['Chip'] = chip
                    info['Page'] = page
                    observed_info_set.append(dict(info))

        table = self.lc_rail_pmbus.hbidps.contruct_text_table(
            data=observed_info_set,
            title_in=f'The complete info set')
        # print(table)
        self.lc_rail_pmbus.check_info_set(observed_info_set)

    def test_check_info_set_random_fail(self):
        common_info_list = [
            {'Chip': 0, 'Page': -1, 'Name': 'STATUS_INPUT', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': -1, 'Name': 'STATUS_CML', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': -1, 'Name': 'MFR_FAULT_LOG_STATUS', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_ON', 'Value': 9.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OFF', 'Value': 8.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OV_FAULT_LIMIT', 'Value': 14.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OV_WARN_LIMIT', 'Value': 15.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_OV_FAULT_RESPONSE', 'Value': hex(0x80)},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_UV_WARN_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_UV_FAULT_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': -1, 'Name': 'VIN_UV_FAULT_RESPONSE', 'Value': hex(0x80)}
            ]
        paged_info_list = [
            {'Chip': 0, 'Page': 0, 'Name': 'OPERATION', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_BYTE', 'Value': hex(0x40)},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_WORD', 'Value': hex(0x40)},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_VOUT', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_IOUT', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_TEMPERATURE', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': 0, 'Name': 'STATUS_MFR_SPECIFIC', 'Value': hex(0x0)},
            {'Chip': 0, 'Page': 0, 'Name': 'OT_FAULT_LIMIT', 'Value': 1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'OT_WARN_LIMIT', 'Value':  1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'OT_FAULT_RESPONSE', 'Value': hex(0x80)},
            {'Chip': 0, 'Page': 0, 'Name': 'UT_WARN_LIMIT', 'Value': -1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'UT_FAULT_LIMIT', 'Value': -1000.0},
            {'Chip': 0, 'Page': 0, 'Name': 'UT_FAULT_RESPONSE', 'Value': hex(0x80)},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MODE', 'Value': hex(0x13)},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_COMMAND', 'Value': 5.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MAX', 'Value': 6.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MARGIN_HIGH', 'Value': 1.05},  # default value
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_MARGIN_LOW', 'Value': 0.95}, # default value
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_OV_FAULT_LIMIT', 'Value': 7.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_OV_WARN_LIMIT', 'Value': 7.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_UV_WARN_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_UV_FAULT_LIMIT', 'Value': 0.0},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_OV_FAULT_RESPONSE', 'Value': hex(0x80)},
            {'Chip': 0, 'Page': 0, 'Name': 'VOUT_UV_FAULT_RESPONSE', 'Value': hex(0x80)},
            {'Chip': 0, 'Page': 0, 'Name': 'IOUT_OC_FAULT_LIMIT', 'Value': 10.0},  # default value
            {'Chip': 0, 'Page': 0, 'Name': 'IOUT_OC_WARN_LIMIT', 'Value': 7.0},
            {'Chip': 0, 'Page': 0, 'Name': 'IOUT_UC_FAULT_LIMIT', 'Value': -10.0}
        ]
        observed_info_set = []
        for chip in range(4):
            chip_info_list = list(common_info_list)

            print(hex(id(chip_info_list)))
            for info in chip_info_list:
                info['Chip'] = chip
                observed_info_set.append(dict(info))
            for page in range(4):
                page_info_list = list(paged_info_list)
                for info in page_info_list:
                    info['Chip'] = chip
                    info['Page'] = page
                    observed_info_set.append(dict(info))

        table = self.lc_rail_pmbus.hbidps.contruct_text_table(
            data=observed_info_set,
            title_in=f'The complete info set')
        fail_info_index = random.randint(0, len(observed_info_set) - 1)
        name = observed_info_set[fail_info_index]['Name']
        chip = observed_info_set[fail_info_index]['Chip']
        page = observed_info_set[fail_info_index]['Page']
        observed_info_set[fail_info_index]['Value'] = -2000

        original_object_Log = self.lc_rail_pmbus.Log
        self.lc_rail_pmbus.Log = Mock()
        self.lc_rail_pmbus.check_info_set(observed_info_set)
        if not self.check_for_log_level('warning', self.lc_rail_pmbus.Log.call_args_list):
            self.fail('warn not detected')
        if page == -1:
            if not self.check_for_message(f'Chip: {chip}: {name} observed:', self.lc_rail_pmbus.Log.call_args_list):
                self.fail(f'expected mismatch message not received for Chip: {chip}: {name}')
        else:
            if not self.check_for_message(f'Chip: {chip}, Page {page}: {name} observed:',
                                          self.lc_rail_pmbus.Log.call_args_list):

                self.fail(f'expected mismatch message not received for Chip: {chip}, Page {page}: {name}')
        self.lc_rail_pmbus.Log = original_object_Log

    def check_for_log_level(self, expected_log_level, calls):
        for args, kwargs in calls:
            log_level, message = args
            if expected_log_level == log_level:
                print(f'an {expected_log_level} was raised in test with message: {message}')
                return True
        else:
            return False

    def check_for_message(self, expected_message, calls):
        for args, kwargs in calls:
            log_level, message = args
            if expected_message in message:
                return True
        else:
            print(f'messgae: {expected_message} wasn\'t raised in test')
            return False

    def mock_bar_read(self, bar, offset, slot=None):
        return self.hbi_dps_reg_map.get(offset, 0)

    def mock_bar_write(self, bar, offset, data, slot=None):
        self.hbi_dps_reg_map.update({offset: data})