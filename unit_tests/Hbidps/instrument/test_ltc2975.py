# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
from Hbidps.instrument import ltc2975


class Ltc2975Conversions(unittest.TestCase):
    def test_linear_5s_11s_to_float(self):
        # use examples from data sheet DEFAULT VALUE: FLOAT HEX column
        self.assertEqual(ltc2975.L11_to_float(0xD280), 10)
        self.assertEqual(ltc2975.L11_to_float(0x8000), 0)
        self.assertEqual(ltc2975.L11_to_float(0xFB20), 400)
        self.assertEqual(ltc2975.L11_to_float(0xD2A0), 10.5)

        # test corner cases
        self.assertEqual(ltc2975.L11_to_float(0x7BFF), 33521664) # largest positive value
        self.assertEqual(ltc2975.L11_to_float(0x8001), 1.52587890625e-05) # smallest positive value
        self.assertEqual(ltc2975.L11_to_float(0x7C00), -33554432) # largest negative value
        self.assertEqual(ltc2975.L11_to_float(0x87FF), -1.52587890625e-05) # smallest negative value


    def test_float_to_linear_5s_11s(self):
        # using examples from data sheet DEFAULT VALUE: FLOAT HEX column
        self.assertEqual(ltc2975.float_to_L11(0.0), 0x8000)
        self.assertEqual(ltc2975.float_to_L11(1.0), 0xBA00)
        self.assertEqual(ltc2975.float_to_L11(10.0), 0xD280)
        self.assertEqual(ltc2975.float_to_L11(15.0), 0xD3C0)
        self.assertEqual(ltc2975.float_to_L11(100.0), 0xEB20)
        self.assertEqual(ltc2975.float_to_L11(200.0), 0xF320)
        self.assertEqual(ltc2975.float_to_L11(400.0), 0xFB20)

        # corner cases
        self.assertEqual(ltc2975.float_to_L11(33521664), 0x7BFF) # largest positive value
        self.assertEqual(ltc2975.float_to_L11(1.52587890625e-05), 0x8001) # smallest positive value
        self.assertEqual(ltc2975.float_to_L11(-33554432), 0x7C00) # largest negative value
        self.assertEqual(ltc2975.float_to_L11(-1.52587890625e-05), 0x87FF) # smallest negative value
