# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import unittest
import random
from unittest.mock import Mock

from Hbidps.instrument import VtargetDevice
from Hbidps.instrument.hbidps import Hbidps


class VtargetDeviceUnitTest(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.hbidps = Hbidps(Mock())
        self.vtarget_device = VtargetDevice.VTargetDevice(self.hbidps)

    def test_set_rail_power_state(self):
        power_state = random.getrandbits(16)
        operation_mode = random.getrandbits(2)
        rail = random.randint(0, 7)
        power_state = self.vtarget_device.set_rail_state(rail, operation_mode, power_state)
        self.assertEqual(operation_mode, (power_state >> (rail * 2)) & 0b11)


