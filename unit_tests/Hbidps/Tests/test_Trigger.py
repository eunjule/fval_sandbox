################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from Common import fval
from Hbidps.Tests.Trigger import Diagnostics
from Hbidps.instrument.hbidps import Hbidps


class TriggerTests(unittest.TestCase):

    def test_pass_count_increase_when_correct_trigger_received_at_rc(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            trigger_value = 0xabcdabcd
            trigger_at_rc = 0xabcdabcd
            hbidps = Hbidps(slot=0, rc=Mock())
            trigger_interface.env = Mock()
            pass_count = 0
            fail_count = 0
            expectd_pass_cunt = 1
            trigger_count = 100
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            trigger_interface.initialize_trigger_link = Mock()
            hbidps.get_rc_trigger = Mock(return_value=trigger_at_rc)
            fail_count, pass_count = trigger_interface.send_trigger_from_dps_verify_at_rc(trigger_value,hbidps,pass_count,fail_count,trigger_count)
            self.assertEqual(pass_count, expectd_pass_cunt)
            self.assertEqual(fail_count, fail_count)

    def test_fail_count_increase_when_incorrect_trigger_received_at_rc(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            trigger_sent = 0xabcd
            trigger_received = 0xdead
            hbidps = Hbidps(slot=0, rc=Mock())
            trigger_interface.env = Mock()
            pass_count = 0
            fail_count = 0
            expectd_fail_cunt = 1
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            trigger_interface.initialize_trigger_link = Mock()
            hbidps.reset_auroa_link_and_get_status = Mock()
            hbidps.rc_log_status = Mock()
            aurora_status = hbidps.registers.STATUS
            aurora_status.value = 0x0
            trigger_count = 100
            aurora_error_count = hbidps.registers.AURORA_ERROR_COUNTS
            aurora_error_count.value = 0
            slot = 3
            hbidps.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
            hbidps.get_rc_trigger = Mock(return_value=trigger_received)
            trigger_interface.Log = Mock()
            rctc_link_up = True
            hbidps.rc.is_aurora_link_up = Mock(return_value=[rctc_link_up,aurora_error_count, aurora_status])
            fail_count, pass_count = trigger_interface.send_trigger_from_dps_verify_at_rc(trigger_sent, hbidps,
                                                                                          pass_count, fail_count,trigger_count)
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(log_message, 'RC received incorrect trigger: Received',
                             'Log not displaying correct information')
                self.assertEqual(pass_count, pass_count)
                self.assertEqual(fail_count, expectd_fail_cunt)
                break

    def test_pass_count_increase_when_correct_trigger_received_at_dps(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            trig_received_obj = hbidps.registers.LAST_TRIGGER_SEEN
            trig_received_obj.value = 0xabcdabcd
            trigger_sent = 0xabcdabcd
            trigger_interface.env = Mock()
            pass_count = 0
            fail_count = 0
            expectd_pass_cunt = 1
            trigger_count = 100
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            trigger_interface.initialize_trigger_link = Mock()
            hbidps.send_rc_trigger = Mock()
            hbidps.read_bar_register = Mock(return_value=trig_received_obj)
            fail_count, pass_count = trigger_interface.send_trigger_from_rc_verify_at_dps(trigger_sent,hbidps,pass_count,fail_count,trigger_count)
            self.assertEqual(pass_count, expectd_pass_cunt)
            self.assertEqual(fail_count, fail_count)

    def test_fail_count_increase_when_incorrect_trigger_received_at_dps(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            trig_received_obj = hbidps.registers.LAST_TRIGGER_SEEN
            trig_received_obj.value = 0xabcd
            trigger_sent = 0xdead
            trigger_interface.env = Mock()
            pass_count = 0
            fail_count = 0
            expectd_fail_cunt = 1
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            trigger_interface.initialize_trigger_link = Mock()
            hbidps.send_rc_trigger = Mock()
            hbidps.read_dps_trigger = Mock(return_value=trig_received_obj)
            hbidps.reset_dps_aurora_link = Mock()
            aurora_status = hbidps.registers.STATUS
            aurora_status.value = 0x0
            aurora_error_count = hbidps.registers.AURORA_ERROR_COUNTS
            aurora_error_count.value = 0
            trigger_count = 100
            slot = 3
            hbidps.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
            rctc_link_up = True
            hbidps.rc.is_aurora_link_up = Mock(return_value=[rctc_link_up,aurora_error_count, aurora_status])
            trigger_interface.Log = Mock()
            fail_count, pass_count = trigger_interface.send_trigger_from_rc_verify_at_dps(trigger_sent, hbidps,
                                                                                          pass_count, fail_count,trigger_count)
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(log_message, 'DPS received incorrect trigger Received :0xabcd Expected :0xdead',
                             'Log not displaying correct information')
                self.assertEqual(pass_count, pass_count)
                self.assertEqual(fail_count, expectd_fail_cunt)
                break

    def test_DpsToRc_correct_trigger_sent_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            triger_at_rc = 0xabcdabcd
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_trigger_link = Mock()
            hbidps.clear_alarm_register = Mock()
            trigger_interface.env = Mock()
            trigger_interface.test_iteration_count = 10
            trigger_interface.generate_dps_type_trigger = Mock(return_value=triger_at_rc)
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.get_rc_trigger = Mock(return_value=triger_at_rc)
            trigger_interface.Log = Mock()
            trigger_interface.RandomTriggerUpDpsToRcTest()
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'DPS to RC 10 Triggers sent Successfully',
                             'Log not displaying correct information')

    def test_DpsToRc_incorrect_trigger_sent_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            triger_at_rc = 0xabcdabcd
            incorrect_trigger_value = 0x12341234
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_trigger_link = Mock()
            hbidps.clear_alarm_register = Mock()
            trigger_interface.env = Mock()
            trigger_interface.test_iteration_count = 10
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            trigger_interface.generate_dps_type_trigger = Mock(return_value=triger_at_rc)
            hbidps.get_rc_trigger = Mock(return_value=incorrect_trigger_value)
            trigger_interface.Log = Mock()
            hbidps.reset_dps_aurora_link = Mock()
            aurora_status = hbidps.registers.STATUS
            aurora_status.value = 0x0
            aurora_error_count = hbidps.registers.AURORA_ERROR_COUNTS
            aurora_error_count.value = 0
            slot = 3
            hbidps.get_aurora_status = Mock(return_value=[aurora_error_count,aurora_status,slot])
            rctc_link_up = True
            hbidps.rc.is_aurora_link_up = Mock(return_value=[rctc_link_up, aurora_error_count, aurora_status])
            hbidps.rc_log_status = Mock()
            trigger_interface.RandomTriggerUpDpsToRcTest()
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                print(log_message)
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(log_message, 'RC received incorrect trigger: Received :0x12341234 Expected :0xabcdabcd',
                                 'Log not displaying correct information')
                break

    def test_RcToDps_correct_trigger_sent_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_trigger_link = Mock()
            hbidps.clear_alarm_register = Mock()
            trigger_interface.env = Mock()
            trigger_at_dps = hbidps.registers.LAST_TRIGGER_SEEN
            trigger_at_dps.value = 0xabcdabcd
            trigger_interface.test_iteration_count = 10
            trigger_interface.generate_dps_type_trigger = Mock(return_value=trigger_at_dps.value)
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.read_dps_trigger = Mock(return_value=trigger_at_dps)
            trigger_interface.Log = Mock()
            trigger_interface.RandomTriggerDownRcToDpsTest()
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'RC to DPS 10 Triggers sent successfully',
                             'Log not displaying correct information')

    def test_RcToDps_incorrect_trigger_sent_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_trigger_link = Mock()
            hbidps.clear_alarm_register = Mock()
            trigger_interface.env = Mock()
            trigger_at_dps = hbidps.registers.LAST_TRIGGER_SEEN
            trigger_at_dps.value = 0xabcdabcd
            trigger_sent_to_dps = 0xabcdabca
            trigger_interface.test_iteration_count = 10
            trigger_interface.generate_dps_type_trigger = Mock(return_value=trigger_sent_to_dps)
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.read_dps_trigger = Mock(return_value=trigger_at_dps)
            hbidps.reset_dps_aurora_link = Mock()
            aurora_status = hbidps.registers.STATUS
            aurora_status.value = 0x0
            aurora_error_count = hbidps.registers.AURORA_ERROR_COUNTS
            aurora_error_count.value = 0
            slot = 3
            hbidps.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
            rctc_link_up = True
            hbidps.rc.is_aurora_link_up = Mock(return_value=[rctc_link_up, aurora_error_count, aurora_status])
            trigger_interface.Log = Mock()
            trigger_interface.RandomTriggerDownRcToDpsTest()
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'RC to DPS Only 0 Triggers sent successfully out of 10',
                             'Log not displaying correct information')

    def test_DpsToDps_correct_trigger_sent_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_trigger_link = Mock()
            hbidps.clear_alarm_register = Mock()
            trigger_interface.env = Mock()
            trigger_at_dps = hbidps.registers.LAST_TRIGGER_SEEN
            trigger_at_dps.value = 0xabcdabcd
            trigger_interface.broadcast_iteration_count = 10
            trigger_interface.generate_dps_type_trigger = Mock(return_value=trigger_at_dps.value)
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            slot_trigger_value_pair = {1:0xabcdabcd}
            hbidps.get_triggr_at_all_dps_instruments = Mock(return_value=slot_trigger_value_pair)
            trigger_interface.Log = Mock()
            trigger_interface.RandomTriggerDpsToDpsTest()
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'DPS to DPS 10 Triggers sent successfully',
                             'Log not displaying correct information')

    def test_DpsToDps_incorrect_trigger_sent_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            trigger_interface = Diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_trigger_link = Mock()
            hbidps.clear_alarm_register = Mock()
            trigger_interface.env = Mock()
            trigger_at_dps = hbidps.registers.LAST_TRIGGER_SEEN
            trigger_at_dps.value = 0xabcdabcd
            trigger_interface.broadcast_iteration_count = 10
            trigger_interface.generate_dps_type_trigger = Mock(return_value=trigger_at_dps.value)
            trigger_interface.env.get_fpgas = Mock(return_value=[hbidps])
            slot_trigger_value_pair = {1:0xabcdabca}
            hbidps.get_triggr_at_all_dps_instruments = Mock(return_value=slot_trigger_value_pair)
            trigger_interface.Log = Mock()
            hbidps.reset_dps_aurora_link = Mock()
            aurora_status = hbidps.registers.STATUS
            aurora_status.value = 0x0
            aurora_error_count = hbidps.registers.AURORA_ERROR_COUNTS
            aurora_error_count.value = 0
            slot = 3
            hbidps.get_aurora_status = Mock(return_value=[aurora_error_count, aurora_status, slot])
            rctc_link_up = True
            hbidps.rc.is_aurora_link_up = Mock(return_value=[rctc_link_up, aurora_error_count, aurora_status])
            trigger_interface.RandomTriggerDpsToDpsTest()
            log_calls = trigger_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'DPS to DPS Only 0 Triggers sent successfully out of 10',
                                 'Log not displaying correct information')

    def test_trigger_value_is_correct(self):
        trigger_interface = Diagnostics()
        hbidps = Hbidps(slot=0, rc=Mock())
        for i in range(1000):
            trigger_value = trigger_interface.generate_dps_type_trigger(hbidps)
            sw_bit_19_check = trigger_value | 0xFFF7FFFF
            card_type_check = trigger_value >> 26
            self.assertEqual(sw_bit_19_check,0xFFF7FFFF)
            self.assertEqual(card_type_check,0x1)