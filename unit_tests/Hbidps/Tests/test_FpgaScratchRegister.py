################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from Hbidps.Tests.FpgaScratchRegister import Diagnostics as Interface
from Hbidps.instrument.hbidps import Hbidps


class InterfaceTests(unittest.TestCase):

    def test_scratch_register_read_pass_test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch.object(random, 'randrange')as mock_Random:
            dummy_write_data = 0xabcd
            mock_Random.return_value = dummy_write_data
            scratch_reg_interface = Interface()
            hbidps = Hbidps(slot=0, rc=Mock())
            scratch_reg_interface.env = Mock()
            scratch_reg_interface.Log = Mock()
            scratch_reg_interface.env.get_fpgas = Mock(return_value=[hbidps])
            scratch_reg_interface.test_iteration_count = 10
            mock_hil_hbidps.hbiDpsBarWrite = Mock()
            mock_hil_hbidps.hbiDpsBarRead = Mock(return_value=dummy_write_data)
            scratch_reg_interface.RandomFpgaScratchRegisterReadWriteTest()
            log_calls = scratch_reg_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
                self.assertRegex(log_message, 'Scratch Register read/Write was successful for iterations: 9',
                                 'Log not displaying correct information')

    def test_scratch_register_read_fail_test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch.object(random, 'randrange')as mock_Random:
            dummy_write_data = 0xabcd
            dummy_missmatch_read_data = 0xabc1
            mock_Random.return_value = dummy_write_data
            scratch_reg_interface = Interface()
            hbidps = Hbidps(slot=0, rc=Mock())
            scratch_reg_interface.env = Mock()
            scratch_reg_interface.env.get_fpgas = Mock(return_value=[hbidps])
            mock_hil_hbidps.hbiDpsBarWrite = Mock()
            mock_hil_hbidps.hbiDpsBarRead = Mock(return_value=dummy_missmatch_read_data)
            scratch_reg_interface.Log = Mock()
            scratch_reg_interface.RandomFpgaScratchRegisterReadWriteTest()
            log_calls = scratch_reg_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(log_message, 'Received 0xabc1, expected=0xabcd',
                                 'Log not displaying correct information')
                break

    def test_scratch_register_stop_after_ten_failure_test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch.object(random, 'randrange')as mock_Random:
            dummy_write_data = 0xabcd
            dummy_missmatch_read_data = 0xabc1
            mock_Random.return_value = dummy_write_data
            scratch_reg_interface = Interface()
            hbidps = Hbidps(slot=0, rc=Mock())
            scratch_reg_interface.env = Mock()
            scratch_reg_interface.env.get_fpgas = Mock(return_value=[hbidps])
            mock_hil_hbidps.hbiDpsBarWrite = Mock()
            mock_hil_hbidps.hbiDpsBarRead = Mock(return_value=dummy_missmatch_read_data)
            scratch_reg_interface.Log = Mock()
            scratch_reg_interface.RandomFpgaScratchRegisterReadWriteTest()
            log_calls = scratch_reg_interface.Log.call_args_list
            self.assertEqual(len(log_calls), 11)
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'Scratch Register read/Write was unsuccessful for iterations ',
                             'Log not displaying correct information')