################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from Hbidps.Tests.VtargetVmeasure import Functional as Functional
from Hbidps.instrument.hbidps import Hbidps
from Common.fval import skip


class VtargetVmeasure(unittest.TestCase):

    @skip('Not implemented')
    def test_if_measured_voltage_is_out_of_range(self):
       pass

    @skip('Not implemented')
    def test_if_measured_voltage_is_in_expected_range(self):
        pass
