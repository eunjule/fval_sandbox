################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock
from Common import fval
from Hbidps.Tests.Adt7411VoltageRead import Diagnostics as Interface
from Hbidps.instrument.hbidps import Hbidps


class InterfaceTests(unittest.TestCase):

    def test_vmon_read_value_miss_match_scenario_test(self):
        dummy_different_vmon_return = [5,5]
        vmon_interface = Interface()
        hbidps = Hbidps(slot=0, rc=Mock())
        hbidps.initialize_hbi_dps = Mock()
        vmon_interface.env = Mock()
        vmon_interface.env.get_fpgas = Mock(return_value=[hbidps])
        hbidps.read_v_mon = Mock(side_effect = dummy_different_vmon_return)
        with self.assertRaises(fval.LoggedError):
            vmon_interface.DirectedADT7411VoltageReadTest()

    def test_vmon_read_value_match_scenario_test(self):
        dummy_same_vmon_return = [4.9013671875,0.5944921875,2.5071093749999998,3.317578125,12.336046875,3.16171875,1.80796875,1.1956640625,0.9507421875]
        vmon_interface = Interface()
        hbidps = Hbidps(slot=0, rc=Mock())
        hbidps.initialize_hbi_dps = Mock()
        vmon_interface.env = Mock()
        vmon_interface.env.get_fpgas = Mock(return_value=[hbidps])
        hbidps.read_v_mon = Mock(side_effect = dummy_same_vmon_return)
        with self.assertRaises(StopIteration):
            vmon_interface.DirectedADT7411VoltageReadTest()

    def test_vmon_read_print_success_message_test(self):
        vmon_interface = Interface()
        Interface.expected_pass_count = 0
        Interface.vmon_test_iteration_count = 0
        hbidps = Hbidps(slot=0, rc=Mock())
        vmon_interface.env = Mock()
        hbidps.initialize_hbi_dps = Mock()
        vmon_interface.env.get_fpgas = Mock(return_value=[hbidps])
        vmon_interface.Log = Mock()
        vmon_interface.DirectedADT7411VoltageReadTest()
        log_calls = vmon_interface.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'ADT7411 read was successful for iterations:',
                             'Log not displaying correct information')
        Interface.vmon_test_iteration_count = 100
        Interface.expected_pass_count = 900

    def test_vmon_read_print_unsuccessful_message_test(self):
        vmon_interface = Interface()
        Interface.expected_pass_count = 1
        Interface.vmon_test_iteration_count = 0
        hbidps = Hbidps(slot=0, rc=Mock())
        hbidps.initialize_hbi_dps = Mock()
        vmon_interface.env = Mock()
        vmon_interface.env.get_fpgas = Mock(return_value=[hbidps])
        vmon_interface.Log = Mock()
        vmon_interface.DirectedADT7411VoltageReadTest()
        log_calls = vmon_interface.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'ADT7411 read was successful for only iterations 0 out of 1',
                             'Log not displaying correct information')
        Interface.vmon_test_iteration_count = 100
        Interface.expected_pass_count = 900