################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from Common import fval
from Hbidps.Tests.HC_ForceVoltage import TqDiagnostics as tq_diagnostics
from Hbidps.instrument.hbidps import Hbidps


class TqDiagnostics(unittest.TestCase):

    def test_tq_scratch_pad_i2c_link_busyTest(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            tq_scratch_pad = tq_diagnostics()
            tq_scratch_pad.Log = Mock()
            hbidps = Hbidps(slot=0, rc=Mock())
            tq_scratch_pad.env = Mock()
            tq_scratch_pad.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.clear_alarm_register = Mock()
            hbidps.clear_pmbus_rx_fifo = Mock()
            status_register = hbidps.registers.PMBUS_CONTROL_STATUS()
            status_register.busy = 1
            hbidps.read_bar_register = Mock(return_value=status_register)
            tq_scratch_pad.TEST_ITERATIONS = 2
            tq_scratch_pad.compare_expected_with_observed = Mock()
            tq_scratch_pad.display_pass_fail_messages = Mock()
            tq_scratch_pad.RandomHcRailScratchPadWriteViaTqReadViaPmBusTest()
            log_calls = tq_scratch_pad.Log.call_args_list
            self.assertNotEqual(log_calls, [])
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(log_message, 'PM BUS I2C link busy',
                                 'Log not displaying correct information')
                break

    def test_tq_scratch_pad_i2c_link_not_busy_Test(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps:
            tq_scratch_pad = tq_diagnostics()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.clear_pmbus_rx_fifo = Mock()
            tq_scratch_pad.env = Mock()
            tq_scratch_pad.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.clear_alarm_register = Mock()
            hbidps.is_pm_bus_busy = Mock(return_value=True)
            tq_scratch_pad.TEST_ITERATIONS = 2
            rx_fifo = hbidps.registers.PMBUS_RX_FIFO()
            rx_fifo.value = 0xABCD
            hbidps.read_bar_register = Mock(return_value=rx_fifo)
            with self.assertRaises(fval.core.LoggedError):
                tq_scratch_pad.RandomHcRailScratchPadWriteViaTqReadViaPmBusTest()

