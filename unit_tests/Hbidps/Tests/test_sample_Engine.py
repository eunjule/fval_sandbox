################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from Common import fval
from Hbidps.Tests.Sample_Engine import LcSampleRegisterTests as lc_register_tests
from Hbidps.Tests.Sample_Engine import HcSampleRegisterTests as hc_register_tests
from Hbidps.Tests.Sample_Engine import VMeasureSampleRegisterTests as vmes_register_tests
from Hbidps.instrument.hbidps import Hbidps


class InterfaceTests(unittest.TestCase):

    def test_lc_sample_engine_register_pass_test(self):
        with patch.object(random, 'randint')as mock_Random:
            reg_test = lc_register_tests()
            mock_Random.return_value = 0x2877c9a
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_hbi_dps = Mock()
            read_back_value = hbidps.registers.LC_SAMPLE_RATIO()
            read_back_value.value = 0x2877c9a
            reg_test.env = Mock()
            reg_test.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.write_bar_register = Mock()
            hbidps.read_bar_register = Mock(return_value=read_back_value)
            reg_test.Log = Mock()
            reg_test.DirectedLcRegisterDataAccessTest()
            log_calls = reg_test.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
                self.assertRegex(log_message, 'Sample Engine register writes were successful',
                                 'Log not displaying correct information')
                break


    def test_lc_sample_engine_register_fail_test(self):
            reg_test = lc_register_tests()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_hbi_dps = Mock()
            read_back_value = hbidps.registers.LC_SAMPLE_RATIO()
            read_back_value.value = 0x2877c9a
            reg_test.env = Mock()
            reg_test.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.write_bar_register = Mock()
            hbidps.read_bar_register = Mock(return_value=read_back_value)
            with self.assertRaises(fval.LoggedError):
                reg_test.DirectedLcRegisterDataAccessTest()

    def test_hc_sample_engine_register_pass_test(self):
        with patch.object(random, 'randint')as mock_Random:
            reg_test = hc_register_tests()
            mock_Random.return_value = 0x2877c9a
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_hbi_dps = Mock()
            read_back_value = hbidps.registers.LC_SAMPLE_RATIO()
            read_back_value.value = 0x2877c9a
            reg_test.env = Mock()
            reg_test.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.write_bar_register = Mock()
            hbidps.read_bar_register = Mock(return_value=read_back_value)
            reg_test.Log = Mock()
            reg_test.DirectedHcRegisterDataAccessTest()
            log_calls = reg_test.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
                self.assertRegex(log_message, 'Sample Engine register writes were successful',
                                 'Log not displaying correct information')
                break


    def test_hc_sample_engine_register_fail_test(self):
            reg_test = hc_register_tests()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_hbi_dps = Mock()
            read_back_value = hbidps.registers.LC_SAMPLE_RATIO()
            read_back_value.value = 0x2877c9a
            reg_test.env = Mock()
            reg_test.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.write_bar_register = Mock()
            hbidps.read_bar_register = Mock(return_value=read_back_value)
            with self.assertRaises(fval.LoggedError):
                reg_test.DirectedHcRegisterDataAccessTest()

    def test_vmes_sample_engine_register_pass_test(self):
        with patch.object(random, 'randint')as mock_Random:
            reg_test = vmes_register_tests()
            mock_Random.return_value = 0x2877c9a
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_hbi_dps = Mock()
            read_back_value = hbidps.registers.LC_SAMPLE_RATIO()
            read_back_value.value = 0x2877c9a
            reg_test.env = Mock()
            reg_test.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.write_bar_register = Mock()
            hbidps.read_bar_register = Mock(return_value=read_back_value)
            reg_test.Log = Mock()
            reg_test.DirectedVMeasureRegisterDataAccessTest()
            log_calls = reg_test.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
                self.assertRegex(log_message, 'Sample Engine register writes were successful',
                                 'Log not displaying correct information')
                break


    def test_vmes_sample_engine_register_fail_test(self):
            reg_test = vmes_register_tests()
            hbidps = Hbidps(slot=0, rc=Mock())
            hbidps.initialize_hbi_dps = Mock()
            read_back_value = hbidps.registers.LC_SAMPLE_RATIO()
            read_back_value.value = 0x2877c9a
            reg_test.env = Mock()
            reg_test.env.get_fpgas = Mock(return_value=[hbidps])
            hbidps.write_bar_register = Mock()
            hbidps.read_bar_register = Mock(return_value=read_back_value)
            with self.assertRaises(fval.LoggedError):
                reg_test.DirectedVMeasureRegisterDataAccessTest()
