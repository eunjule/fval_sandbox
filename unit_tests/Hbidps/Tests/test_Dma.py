################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import random
import unittest
from unittest.mock import patch
from unittest.mock import Mock
from Hbidps.Tests.Dma import Diagnostics as DMA
from Hbidps.instrument.hbidps import Hbidps


class DmaTests(unittest.TestCase):

    def test_dma_read_write_pass_scenario(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps,  \
                patch.object(os,'urandom')as mock_random:
            dummy_write_data = 0x2877c9a
            dma_read_write_interface = DMA()
            hbidps = Hbidps(slot=0, rc=Mock())
            dma_read_write_interface.env = Mock()
            dma_read_write_interface.env.get_fpgas = Mock(return_value=[hbidps])
            mock_random.return_value = dummy_write_data
            dma_read_write_interface.Log = Mock()
            mock_hil_hbidps.hbiDpsDmaRead = Mock(return_value=dummy_write_data)
            dma_read_write_interface.DirectedDMAWriteReadExhaustiveTest()
            log_calls = dma_read_write_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'DMA read/Write was successful for 5 iterations',
                             'Log not displaying correct information')

    def test_dma_read_write_fail_scenario(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps,  \
                patch.object(os,'urandom')as mock_random:
            dummy_write_data = 0x2877c9a
            dummy_miss_match_data = [0x2877c9a, 0x2877c1, 0x2877c92, 0x2877c93, 0x2877c94, 0x2877c95, 0x2877c1,
                                     0x2877c92, 0x2877c93, 0x2877c94, 0x2877c95]
            dma_read_write_interface = DMA()
            hbidps = Hbidps(slot=0, rc=Mock())
            dma_read_write_interface.env = Mock()
            dma_read_write_interface.env.get_fpgas = Mock(return_value=[hbidps])
            mock_random.return_value = dummy_write_data
            dma_read_write_interface.Log = Mock()
            mock_hil_hbidps.hbiDpsDmaRead = Mock(side_effect=dummy_miss_match_data)
            dma_read_write_interface.DirectedDMAWriteReadExhaustiveTest()
            log_calls = dma_read_write_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'DMA read/Write was unsuccessful for 2 iterations',
                             'Log not displaying correct information')

    def test_dma_read_write_pass_scenario_2g(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps,  \
                patch.object(os,'urandom')as mock_random:
            dummy_write_data = 0x2877c9a
            dma_read_write_interface = DMA()
            hbidps = Hbidps(slot=0, rc=Mock())
            dma_read_write_interface.env = Mock()
            dma_read_write_interface.hbidps_list = [hbidps]
            mock_random.return_value = dummy_write_data
            dma_read_write_interface.Log = Mock()
            mock_hil_hbidps.hbiDpsDmaRead = Mock(return_value=dummy_write_data)
            dma_read_write_interface.DirectedDMAWriteReadExhaustive2GBTest()
            log_calls = dma_read_write_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'DMA read/Write was successful for 2 iterations',
                             'Log not displaying correct information')

    def test_dma_read_write_fail_scenario_2g(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps,  \
                patch.object(os,'urandom')as mock_random:
            dummy_write_data = 0x2877c9a
            dummy_miss_match_data = [0x2877c1, 0x2877c92, 0x2877c93, 0x2877c94, 0x2877c95, 0x2877c1,
                                     0x2877c92, 0x2877c93, 0x2877c94, 0x2877c95]
            dma_read_write_interface = DMA()
            hbidps = Hbidps(slot=0, rc=Mock())
            dma_read_write_interface.env = Mock()
            dma_read_write_interface.hbidps_list = [hbidps]
            mock_random.return_value = dummy_write_data
            dma_read_write_interface.Log = Mock()
            mock_hil_hbidps.hbiDpsDmaRead = Mock(side_effect=dummy_miss_match_data)
            dma_read_write_interface.DirectedDMAWriteReadExhaustive2GBTest()
            log_calls = dma_read_write_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'DMA read/Write was unsuccessful for 2 iterations',
                             'Log not displaying correct information')

    def test_dma_read_write_pass_scenario_random(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps,  \
                patch.object(os,'urandom')as mock_random:
            dummy_write_data = 0x2877c9a
            dma_read_write_interface = DMA()
            hbidps = Hbidps(slot=0, rc=Mock())
            dma_read_write_interface.env = Mock()
            dma_read_write_interface.hbidps_list = [hbidps]
            mock_random.return_value = dummy_write_data
            dma_read_write_interface.Log = Mock()
            mock_hil_hbidps.hbiDpsDmaRead = Mock(return_value=dummy_write_data)
            dma_read_write_interface.DirectedDMAWriteReadRandomAccessSizeTest()
            log_calls = dma_read_write_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
            self.assertRegex(log_message, 'DMA read/Write was successful for 2 iterations',
                             'Log not displaying correct information')

    def test_dma_read_write_fail_scenario_random(self):
        with patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps,  \
                patch.object(os,'urandom')as mock_random:
            dummy_write_data = 0x2877c9a
            dummy_miss_match_data = [0x2877c1, 0x2877c92, 0x2877c93, 0x2877c94, 0x2877c95, 0x2877c1,
                                     0x2877c92, 0x2877c93, 0x2877c94, 0x2877c95]
            dma_read_write_interface = DMA()
            hbidps = Hbidps(slot=0, rc=Mock())
            dma_read_write_interface.env = Mock()
            dma_read_write_interface.hbidps_list = [hbidps]
            mock_random.return_value = dummy_write_data
            dma_read_write_interface.Log = Mock()
            mock_hil_hbidps.hbiDpsDmaRead = Mock(side_effect=dummy_miss_match_data)
            dma_read_write_interface.DirectedDMAWriteReadRandomAccessSizeTest()
            log_calls = dma_read_write_interface.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'DMA read/Write was unsuccessful for 2 iterations',
                             'Log not displaying correct information')