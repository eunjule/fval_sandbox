# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


import unittest

from Hbidps.instrument.hbidps import Hbidps
from Hbidps.Tests.Comms import GeneralSpiInterfaceTests as GSIT
from unittest.mock import Mock


class InterfaceTests(unittest.TestCase):

    def test_is_signed(self):
        max6628_test = GSIT()
        self.assertEqual(max6628_test.is_signed(4096), 1)
        self.assertEqual(max6628_test.is_signed(4097), 1)
        self.assertEqual(max6628_test.is_signed(22), 0)

    def test_get_twos_compliment(self):
        max6628_test = GSIT()
        self.assertEqual(max6628_test.get_twos_compliment(8191, 13), -1) # -0.0625
        self.assertEqual(max6628_test.get_twos_compliment(7792, 13), -400) # -25
        self.assertEqual(max6628_test.get_twos_compliment(7312, 13), -880) #-55

    def test_convert_max6628_temp_to_degrees(self):
        max6628_test = GSIT()
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(0), 0)
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(400), 25)
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(7792), -25)
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(880), 55)
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(7312), -55)
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(8191), -0.0625)
        self.assertEqual(max6628_test.convert_max6628_temp_to_degrees(1), 0.0625)

    def test_check_fpga_die_temp_within_limits(self):
        max6628_test = GSIT()
        max6628_test.Log = Mock()
        max6628_test.check_fpga_die_temp_limits(24)
        log_calls = max6628_test.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'DEBUG')
            self.assertRegex(log_message, 'Fpga Die temp: 24',
                             'Log not displaying correct information')

    def test_check_fpga_die_temp_exceeding_limits(self):
        max6628_test = GSIT()
        max6628_test.Log = Mock()
        max6628_test.check_fpga_die_temp_limits(58)
        log_calls = max6628_test.Log.call_args_list
        for log_info, kwargs in log_calls:
            log_level, log_message = log_info
            self.assertEqual(log_level.upper(), 'ERROR')
            self.assertRegex(log_message, 'Fpga Die temp Exceeded : 58. Min Temp : 15 Max Temp 42.',
                             'Log not displaying correct information')

    def test_decode_float_to_single_precision_floating_format(self):
        max6628_test = GSIT()
        self.assertEqual(max6628_test.encode_float_to_single_precision_floating_format(0.9125000238418579), 1063885210)
        self.assertEqual(max6628_test.encode_float_to_single_precision_floating_format(0.8500000238418579), 1062836634)
        self.assertEqual(max6628_test.encode_float_to_single_precision_floating_format(0.824999988079071), 1062417203)
        self.assertEqual(max6628_test.encode_float_to_single_precision_floating_format(0.875), 1063256064)
        self.assertEqual(max6628_test.encode_float_to_single_precision_floating_format(0.9375), 1064304640)

