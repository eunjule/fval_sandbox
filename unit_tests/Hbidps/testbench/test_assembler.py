# INTEL CONFIDENTIAL
# Copyright 2019 Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

import unittest

from Hbidps.testbench.assembler import TriggerQueueAssembler


class TqAssemblerTests(unittest.TestCase):

    def test_iee754(self):
        self.check_macro(macro='Q cmd = SET_V, arg = 0, data = 0', expected=b'\x00\x00\x00\x00\x00\x00c\x80')
        self.check_macro(macro='Q cmd = SET_V, arg = 0, data = `ieee754(0.0)`', expected=b'\x00\x00\x00\x00\x00\x00c\x80')
        self.check_macro(macro='Q cmd = SET_V, arg = 0, data = `ieee754(1.0)`', expected=b'\x00\x00\x80?\x00\x00c\x80')
        self.check_macro(macro='Q cmd = SET_V, arg = 0, data = `ieee754(-1.0)`', expected=b'\x00\x00\x80\xbf\x00\x00c\x80')
        self.check_macro(macro='Q cmd = SET_V, arg = 0, data = `ieee754(-3.2)`', expected=b'\xcd\xccL\xc0\x00\x00c\x80')

    def test_macro_instructions(self):
       self.check_macro(macro='''Q cmd = WRITE_WORD, arg = 0x21, data = 0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                  TqComplete rail=0x21, value=0''',
                         expected=b'\xcd\xab\xb4\x00\x00!\x03\x80\x00\x00\x00\x00\x00!\xff\x80')
       self.check_macro(macro='''SendByte rail=0x21, value=0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                  TqComplete rail=0x21, value=0''',
                         expected=b'\xcd\xab\xb4\x00\x00!\x00\x80\x00\x00\x00\x00\x00!\xff\x80')
       self.check_macro(macro='''Q cmd = SEND_BYTE, arg = 0x21, data = 0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 Q cmd = WRITE_BYTE, arg = 0x21, data = 0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 Q cmd = WRITE_WORD, arg = 0x21, data = 0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 Q cmd = READ_BYTE, arg = 0x21 ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 Q cmd = READ_WORD, arg = 0x21 ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 TqComplete rail=0x21, value=0''',
                        expected=b'\xcd\xab\xb4\x00\x00!\x00\x80\xcd\xab\xb4\x00\x00!\x01\x80\xcd\xab\xb4\x00\x00!\x03\x80\x00\x00\xb4\x00\x00!\x02\x80\x00\x00\xb4\x00\x00!\x04\x80\x00\x00\x00\x00\x00!\xff\x80')

       self.check_macro(macro='''SyncCommand rail=0x21, value=0xABCD
                                 TqComplete rail=0x21, value=0''',
                        expected=b'\xcd\xab\x00\x00\x00!1\x80\x00\x00\x00\x00\x00!\xff\x80')
       self.check_macro(macro='''SendByte rail=0x21, value=0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 WriteByte rail=0x21, value=0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 WriteWord rail=0x21, value=0xABCD ,pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 ReadByte rail=0x21, pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 ReadWord rail=0x21, pmbuscmd = USER_DATA_04_SCRATCH_PAD
                                 TqComplete rail=0x21, value=0''',
                        expected=b'\xcd\xab\xb4\x00\x00!\x00\x80\xcd\xab\xb4\x00\x00!\x01\x80\xcd\xab\xb4\x00\x00!\x03\x80\x00\x00\xb4\x00\x00!\x02\x80\x00\x00\xb4\x00\x00!\x04\x80\x00\x00\x00\x00\x00!\xff\x80')
                                 

    def check_macro(self, macro ,expected):
        tq_assembler = TriggerQueueAssembler()
        tq_assembler.LoadString(macro)
        data = tq_assembler.CachedObj()
        s = []
        changed_list = []
        for byte in data:
            s.append('{:02x}'.format(byte))
            if len(s) == 8:
                a = '0x' + ''.join(reversed(s))
                changed_list.append(a)
                s = []
        if len(s) != 0:
            raise Exception('Data length is expected to be a multiple of 4 (i.e. dword aligned)')
        if data != expected :
            print(data)
            print(expected)
            self.assertTrue(False, 'Received trigger queue differed from expected')


