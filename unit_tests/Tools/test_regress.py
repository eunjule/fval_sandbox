################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
from unittest.mock import Mock, patch

from Common.instruments.tester import get_tester
import Tools.regress


class RegressTests(unittest.TestCase):
    def test_verify_activate_devices_single_slot_single_dut_undertest(self):
        with patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
                patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
                patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk, \
                patch('Common.fval.core.logging') as mock_logging:

            dut = 'hpcc'
            slots = [5]
            debugTester = ['','','hddps','','','hpcc','hvdps','','','','','']

            deviceConnectList = self.getConnectLists(debugTester)
            self.mockDevices(mock_hil, mock_hil_hpcc, mock_hil_hddps, mock_hil_tdaubnk, mock_hil_hvdps, mock_hil_hbidps, deviceConnectList )

            tester = get_tester()
            tester.is_hbi_tester = Mock(return_value=False)
            tester.discover_all_instruments()
            Tools.regress.verify_and_activate_devices(dut,slots,tester)
            for device in tester.devices:
                if device.name().lower() != 'rc2':
                    if device.slot in slots and device.name().lower() == dut:
                        self.assertTrue(device.under_test)
                    else:
                        self.assertFalse(device.under_test)



    def test_verify_activate_devices_single_slot_single_dut_mismatch(self):
        with patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
                patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
                patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk, \
                patch('Common.fval.core.logging') as mock_logging:
            duts = ['hpcc']
            slots = [6]
            debugTester = ['','','hddps','','','hpcc','tdaubnk','','','','','']

            deviceConnectList = self.getConnectLists(debugTester)
            self.mockDevices(mock_hil, mock_hil_hpcc, mock_hil_hddps, mock_hil_tdaubnk, mock_hil_hvdps, mock_hil_hbidps, deviceConnectList)
            tester = get_tester()
            tester.is_hbi_tester = Mock(return_value=False)
            tester.discover_all_instruments()
            with self.assertRaises(RuntimeError):
                Tools.regress.verify_and_activate_devices(duts,slots,tester)



    def test_verify_activate_devices_three_slot_two_dut_match(self):
        with patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
                patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
                patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk, \
                patch('Common.fval.core.logging') as mock_logging:
            dut = 'hddps'
            slots = [2,6]
            
            debugTester = ['','','hddps','','','hpcc','hddps','','','','','']

            deviceConnectList = self.getConnectLists(debugTester)
            self.mockDevices(mock_hil, mock_hil_hpcc, mock_hil_hddps, mock_hil_tdaubnk, mock_hil_hvdps, mock_hil_hbidps, deviceConnectList )

            tester = get_tester()
            tester.is_hbi_tester = Mock(return_value=False)
            tester.discover_all_instruments()
            Tools.regress.verify_and_activate_devices(dut, slots, tester)


    def test_verify_activate_devices_invalid_empty_slot(self):
        with patch('Common.instruments.tester.hil') as mock_hil, \
                patch('Hpcc.instrument.hpcc.hil') as mock_hil_hpcc, \
                patch('Common.instruments.dps.hddps.hil') as mock_hil_hddps, \
                patch('Common.instruments.dps.hvdpsCombo.hil') as mock_hil_hvdps, \
                patch('Hbidps.instrument.hbidps.hil') as mock_hil_hbidps, \
                patch('Tdaubnk.instrument.tdaubnk.hil') as mock_hil_tdaubnk, \
                patch('Common.fval.core.logging') as mock_logging:
            duts = ['hddps', 'hpcc']
            slots = [5, 2, 9]
            debugTester = ['', '', 'hddps', '', '', 'hpcc', 'hddps', '', '', '', '', '']

            deviceConnectList = self.getConnectLists(debugTester)
            self.mockDevices(mock_hil, mock_hil_hpcc, mock_hil_hddps, mock_hil_tdaubnk, mock_hil_hvdps, mock_hil_hbidps, deviceConnectList )
            tester = get_tester()
            tester.is_hbi_tester = Mock(return_value=False)
            tester.discover_all_instruments()
            with self.assertRaises(RuntimeError):
                Tools.regress.verify_and_activate_devices(duts, slots, tester)


    def getConnectLists(self, debugTester):
        rcConnectList = [''] * 12
        rc3ConnectList = [RuntimeError] * 12
        hbiMbConnectList = [RuntimeError] * 12
        tdbConnectList = [RuntimeError] * 12
        hpccDcConnectList = [RuntimeError] * 12
        hpccAcConnectList = [RuntimeError] * 12
        hddpsConnectList = [RuntimeError] * 24
        hvdpsConnectList = [RuntimeError] * 12
        hbidpsConnectList = [RuntimeError] * 12


        slotCount = 0
        for dut in debugTester:
            if dut == 'tdaubnk':
                tdbConnectList[slotCount] = ''
            elif dut == 'hpcc':
                hpccDcConnectList[slotCount] = ''
                hpccAcConnectList[slotCount] = ''
            elif dut == 'hddps':
                hddpsConnectList[slotCount * 2] = ''
                hddpsConnectList[(slotCount * 2) + 1] = ''
            elif dut == 'hvdps':
                hvdpsConnectList[slotCount] = ''
            elif dut == 'hbidps':
                hbidpsConnectList[slotCount] = ''
            else:
                pass
            slotCount = slotCount + 1

        deviceConnectList = {'rcConnectList': rcConnectList,
                             'rc3ConnectList': rc3ConnectList,
                             'hbiMbConnectList': hbiMbConnectList,
                             'tdbConnectList': tdbConnectList,
                             'hpccDcConnectList': hpccDcConnectList,
                             'hpccAcConnectList': hpccAcConnectList,
                             'hddpsConnectList': hddpsConnectList,
                             'hvdpsConnectList': hvdpsConnectList,
                             'hbidpsConnectList': hbidpsConnectList}
        return deviceConnectList

    def mockDevices(self, mock_hil, mock_hil_hpcc, mock_hil_hddps, mock_hil_tdaubnk, mock_hil_hvdps, mock_hil_hbidps , deviceConnectList ):
        mock_hil.rcConnect = Mock(side_effect=deviceConnectList['rcConnectList'])
        mock_hil.rc3Connect = Mock(side_effect=deviceConnectList['rc3ConnectList'])
        mock_hil.hbiMbConnect = Mock(side_effect=deviceConnectList['hbiMbConnectList'])
        mock_hil_tdaubnk.tdbConnect = Mock(side_effect=deviceConnectList['tdbConnectList'])
        mock_hil_hpcc.hpccDcConnect = Mock(side_effect=deviceConnectList['hpccDcConnectList'])
        mock_hil_hddps.hddpsConnect = Mock(side_effect=deviceConnectList['hddpsConnectList'])
        mock_hil_hvdps.hvdpsConnect = Mock(side_effect=deviceConnectList['hvdpsConnectList'])
        mock_hil_hbidps.hbiDpsConnect = Mock(side_effect=deviceConnectList['hbidpsConnectList'])
