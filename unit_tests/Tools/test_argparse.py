################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import __init__

import collections
import unittest
from unittest.mock import patch
import argparse

import Tools.regress

class MainRegressTests(unittest.TestCase):
    def test_argparse(self):
        test_args = ['hpcc',  '-s 5']
        args, slots = Tools.regress.parse_args(test_args)
        expected_args = argparse.Namespace(debugcode=False, dut='hpcc', output=None, overrideconfigs='',
                                           regressionseed=None, repeat=1, simulate=False, slots=' 5', localoutput=None,testlist=None, testlistordered=None,
                                           testname=None, testseed=None,uniqueoutput=False, verbosity='15', failfast=False,skipinit=False, skipmemoryinit=False)
        self.assertEqual(expected_args,args)


    def test_argparse2(self):
        test_args = ['dps',  '-s 5']
        args, slots = Tools.regress.parse_args(test_args)
        expected_args = argparse.Namespace(debugcode=False, dut='dps', output=None, overrideconfigs='',
                                           regressionseed=None, repeat=1, simulate=False, slots=' 5', localoutput=None,testlist=None, testlistordered=None,
                                           testname=None, testseed=None,uniqueoutput=False, verbosity='15', failfast=False,skipinit=False,skipmemoryinit=False)

        self.assertEqual(expected_args,args)


    def test_argparse3(self):
        test_args = ['-s 5']
        with patch('sys.stderr') as stdout:
            with self.assertRaises(SystemExit):
                args,duts,slots = Tools.regress.parse_args(test_args)        
        

if __name__ == '__main__':
    unittest.main(verbosity=2)
