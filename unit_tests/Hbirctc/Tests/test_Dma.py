# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Common.fval import SkipTest
from Common.mainboard_simulator import MainBoard
from Hbirctc.instrument.hbirctc import Hbirctc
from Hbirctc.Tests.Dma import Diagnostics
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class DmaDiagnosticsTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.call_args_list = self.diagnostics.Log.call_args_list
        self.diagnostics.test_iterations = 1
        self.diagnostics.max_fail_count = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedDMAWriteReadDDRTest_pass(self):
        with patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                mock_is_fpga_ddr_capable:
            mock_is_fpga_ddr_capable.return_value = True
            self.diagnostics.DirectedDMAWriteReadDDRTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedDMAWriteReadDDRTest_calibration_fail(self):
        with patch.object(Hbirctc, 'ddr_is_ready') as mock_ddr_is_ready,\
            patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_ddr_is_ready.return_value = False
            mock_read_blt.return_value = Blt('D')
            mock_is_fpga_ddr_capable.return_value = True
            self.diagnostics.DirectedDMAWriteReadDDRTest()

        self.validate_fail_message(self.diagnostics)
        self.validate_log_message(self.call_args_list,
                                  self.diagnostics.ddr4_calibration_fail_msg(),
                                  'error')

    def test_DirectedDMAWriteReadDDRTest_data_mismatch_fail(self):
        self._dma_write_data = []
        self._dma_read_data = []
        self._dma_start_address = 0
        self.diagnostics.dma_write_read_transaction = Mock(side_effect=self.mock_dma_write_read_transaction)
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('D')
            mock_is_fpga_ddr_capable.return_value = True
            self.diagnostics.DirectedDMAWriteReadDDRTest()

        self.validate_log_message(self.call_args_list,
                                  self.diagnostics.dma_mismatch_msg(
                                      0, self._dma_start_address, self._dma_start_address + len(self._dma_write_data)),
                                  'error')
        self.validate_fail_message(self.diagnostics)

    def test_DirectedDMAWriteReadDDRTest_invalid_fab(self):
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('C')
            mock_is_fpga_ddr_capable.return_value = True

            with self.assertRaises(SkipTest):
                self.diagnostics.DirectedDMAWriteReadDDRTest()

    def test_DirectedDMAWriteReadDDRTest_invalid_ddr_capability(self):
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('D')
            mock_is_fpga_ddr_capable.return_value = False

            with self.assertRaises(SkipTest):
                self.diagnostics.DirectedDMAWriteReadDDRTest()

    def test_DirectedDMAWriteReadRAMTest_pass(self):
        with patch.object(MainBoard, 'read_blt') as mock_read_blt:
            mock_read_blt.return_value = Blt('C')
            self.diagnostics.DirectedDMAWriteReadRAMTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedDMAWriteReadRAMTest_data_mismatch_fail(self):
        self._dma_write_data = []
        self._dma_read_data = []
        self._dma_start_address = 0
        self.diagnostics.dma_write_read_transaction = Mock(side_effect=self.mock_dma_write_read_transaction)

        with patch.object(MainBoard, 'read_blt') as mock_read_blt:
            mock_read_blt.return_value = Blt('C')
            self.diagnostics.DirectedDMAWriteReadRAMTest()

        self.validate_log_message(self.call_args_list,
                                  self.diagnostics.dma_mismatch_msg(
                                      0, self._dma_start_address, self._dma_start_address + len(self._dma_write_data)),
                                  'error')
        self.validate_fail_message(self.diagnostics)

    def mock_dma_write_read_transaction(self, start_address, write_data):
        self._dma_start_address = start_address
        self._dma_write_data = write_data
        self._dma_read_data = write_data[1:len(write_data)-1]
        return self._dma_read_data

    def test_DirectedMemoryRecalibrationTest_pass(self):
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('D')
            mock_is_fpga_ddr_capable.return_value = True
            self.diagnostics.DirectedMemoryRecalibrationTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedMemoryRecalibrationTest_fail(self):
        self.diagnostics.reset_ddr = Mock()
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('D')
            mock_is_fpga_ddr_capable.return_value = True
            self.diagnostics.DirectedMemoryRecalibrationTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_log_message(self.call_args_list,
                                  self.diagnostics.error_msg_ddr_reset(0,
                                                                       True),
                                  'error')

    def test_DirectedMemoryRecalibrationTest_invalid_fab(self):
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('C')
            mock_is_fpga_ddr_capable.return_value = True

            with self.assertRaises(SkipTest):
                self.diagnostics.DirectedMemoryRecalibrationTest()

    def test_DirectedMemoryRecalibrationTest_invalid_ddr_capability(self):
        with patch.object(MainBoard, 'read_blt') as mock_read_blt, \
                patch.object(Hbirctc, 'is_fpga_ddr_capable') as \
                        mock_is_fpga_ddr_capable:
            mock_read_blt.return_value = Blt('D')
            mock_is_fpga_ddr_capable.return_value = False

            with self.assertRaises(SkipTest):
                self.diagnostics.DirectedMemoryRecalibrationTest()


class Blt():
    def __init__(self, fab):
        part_num = \
            list(f'AAK15216-{str(Hbirctc.DDR4_MAINBOARD_FAB_NUMBER_MINIMUM)}')
        part_num[9] = str(self.convert_fab_letter_to_number(fab))
        part_num = ''.join(part_num)

        self.DeviceName = 'MbDeviceName'
        self.VendorName = 'MbVendorName'
        self.PartNumberAsBuilt = part_num
        self.PartNumberCurrent = part_num
        self.SerialNumber = 'MbSerialNumber'
        self.ManufactureDate = 'MbManufactureDate'

    def convert_fab_letter_to_number(self, fab):
        return (int(fab, 16) - 10) + 1