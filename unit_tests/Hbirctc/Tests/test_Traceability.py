# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from random import choice
from unittest.mock import Mock

from Hbirctc.Tests.Traceability import Chip, Functional, Image
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class ImageTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.image = Image()
        self.image.setUp(tester=self.tester)
        self.image.Log = Mock()
        self.image.test_iterations = 2
        self.image.max_fail_count = 1
        self.call_args_list = self.image.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedHgCleanBitTest_pass(self):
        self.image.DirectedHgCleanBitTest()
        self.validate_pass_message(self.image)

    def test_DirectedHgCleanBitTest_fail(self):
        file_id = self.image.hbirctc.hg_clean_from_file()
        self.image.hbirctc.get_hg_clean_bit = Mock(return_value=0)

        self.image.DirectedHgCleanBitTest()
        self.validate_fail_message(self.image)
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg_hg_clean(file_id, 0),
            'error')

    def test_DirectedFpgaVersionTest_pass(self):
        self.image.DirectedFpgaVersionTest()
        self.validate_pass_message(self.image)

    def test_DirectedFpgaVersionTest_mismatch_fail(self):
        fpga_version = self.image.hbirctc.fpga_version()
        self.image.hbirctc.fpga_version_from_file = \
            Mock(return_value=fpga_version+1)
        self.image.DirectedFpgaVersionTest()
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg_fpga_version(fpga_version+1, fpga_version),
            'error')
        self.validate_pass_message(self.image)

    def test_DirectedFpgaVersionTest_iteration_fail(self):
        self.image.test_iterations = 1
        expected = self.image.hbirctc.fpga_version()
        actual = expected + 1
        self.values = [expected, actual]
        self.image.hbirctc.fpga_version = Mock(side_effect=self.mock_values)
        self.image.DirectedFpgaVersionTest()
        self.validate_fail_message(self.image)
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg(
                iteration=0, expected=expected, actual=actual),
            'error')

    def test_DirectedHgIdTest_pass(self):
        self.image.DirectedHgIdTest()
        self.validate_pass_message(self.image)

    def test_DirectedHgIdTest_mismatch_fail(self):
        from_register = 0xAA
        from_file = 0x55
        self.image.hbirctc.hg_id = Mock(return_value=from_register)
        self.image.hbirctc.hg_id_from_file = Mock(return_value=from_file)
        self.image.DirectedHgIdTest()
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg_hg_id(from_file, from_register),
            'error')
        self.validate_pass_message(self.image)

    def test_DirectedHgIdTest_iteration_fail(self):
        self.image.test_iterations = 1
        expected = self.image.hbirctc.hg_id()
        actual = 0x0123 << 32 | 0x456789AB
        self.values = [expected, actual]
        self.image.hbirctc.hg_id = Mock(side_effect=self.mock_values)
        self.image.DirectedHgIdTest()
        self.validate_fail_message(self.image)
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg(
                iteration=0, expected=expected, actual=actual),
            'error')

    def test_DirectedHwCompatibilityTest_pass(self):
        self.image.DirectedHwCompatibilityTest()
        self.validate_pass_message(self.image)

    def test_DirectedHwCompatibilityTest_mismatch_fail(self):
        from_register = 1
        file_bit = 3
        from_file = fr'\\Fab_C\3.1.1\hbi_rc_tc_top_v3.1.{file_bit}.rbf',
        self.image.hbirctc.hardware_compatibility = \
            Mock(return_value=from_register)
        self.image.hbirctc.get_hw_compatibility_from_config_file_name = \
            Mock(return_value=from_file)
        self.image.DirectedHwCompatibilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg_hw_compatibility(file_bit, from_register),
            'error')
        self.validate_pass_message(self.image)

    def test_DirectedHwCompatibilityTest_iteration_fail(self):
        self.image.test_iterations = 1
        hwc_expected = 0
        hwc_actual = 3
        self.values = [hwc_expected, hwc_actual]
        self.image.hbirctc.hardware_compatibility = \
            Mock(side_effect=self.mock_values)
        self.image.DirectedHwCompatibilityTest()
        self.validate_fail_message(self.image)
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg(
                iteration=0, expected=hwc_expected, actual=hwc_actual),
            'error')

    def test_DirectedDdrCapabilitiesTest_pass(self):
        self.image.DirectedDdrCapabilitiesTest()
        self.validate_pass_message(self.image)

    def test_DirectedDdrCapabilitiesTest_mismatch_fail(self):
        expected_hw_compatibility = 3
        actual_hw_compatibility = 0
        ddr_capabilities = 1
        self.image.hbirctc.hardware_compatibility = \
            Mock(return_value=actual_hw_compatibility)
        self.image.hbirctc.is_fpga_ddr_capable = \
            Mock(return_value=ddr_capabilities)
        self.image.DirectedDdrCapabilitiesTest()
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg_ddr_capabilities(
                expected_hw_compatibility, actual_hw_compatibility),
            'error')
        self.validate_pass_message(self.image)

    def test_DirectedDdrCapabilitiesTest_iteration_fail(self):
        self.image.test_iterations = 1
        hw_compatibility = 3
        expected_ddr_capabilities = 1
        actual_ddr_capabilities = 0
        self.values = [expected_ddr_capabilities, actual_ddr_capabilities]
        self.image.hbirctc.hardware_compatibility = \
            Mock(return_value=hw_compatibility)
        self.image.hbirctc.is_fpga_ddr_capable = \
            Mock(side_effect=self.mock_values)
        self.image.DirectedDdrCapabilitiesTest()
        self.validate_fail_message(self.image)
        self.validate_log_message(
            self.call_args_list,
            self.image.error_msg(iteration=0,
                                 expected=expected_ddr_capabilities,
                                 actual=actual_ddr_capabilities),
            'error')

    def mock_values(self):
        return self.values.pop(0)


class ChipTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.chip = Chip()
        self.chip.setUp(tester=self.tester)
        self.chip.test_iterations = 10
        self.chip.max_fail_count = 1
        self.chip.Log = Mock()

    def tearDown(self):
        self.print_log_messages(self.chip.Log.call_args_list, 'info', 'error')

    def test_DirectedChipIdTest_pass(self):
        self.chip.DirectedChipIdTest()
        self.validate_pass_message(self.chip)

    def test_DirectedChipIdTest_invalid_values_fail(self):
        invalid_combos = [[0x0, 0x0],
                          [0xFFFFFFFF, 0xCA11F00D] ,
                          [0xCA11F00D, 0xFFFFFFFF]]
        upper_lower = choice(invalid_combos)

        self.chip.hbirctc.chip_id = Mock(return_value=upper_lower)
        self.chip.DirectedChipIdTest()

        self.validate_fail_message(self.chip)


class FunctionalTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.test_iterations = 1
        self.functional.max_fail_count = 1
        self.functional.Log = Mock()
        self.call_args_list = self.functional.Log.call_args_list
        self.hbirctc = self.functional.hbirctc

    def test_DirectedHpsCommunicationsRegWriteReadExhaustiveTest_pass(self):
        self.functional.DirectedHpsCommunicationsRegWriteReadExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedHpsCommunicationsRegWriteReadExhaustiveTest_fail(self):
        expected = 0xAA
        actual = 0x55
        self.functional.generate_dword = Mock(return_value=expected)
        self.hbirctc.hps_communications = Mock(return_value=actual)
        self.functional.DirectedHpsCommunicationsRegWriteReadExhaustiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_mismatch(iteration=0, expected=expected,
                                               actual=actual),
            'error')
        self.validate_fail_message(self.functional)

    def test_DirectedHpsCommunicationsRegWriteReadExhaustiveTest_restore_fail(
            self):
        self.functional.test_iterations = 1
        default = 0
        generate_value = 1
        default_error = 2
        self.values = [default, generate_value, default_error]

        self.hbirctc.hps_communications = Mock(side_effect=self.mock_values)
        self.functional.generate_dword = Mock(return_value=generate_value)
        self.functional.DirectedHpsCommunicationsRegWriteReadExhaustiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_default('HPS_COMMUNICATIONS', default,
                                              default_error),
            'error')
        self.validate_pass_message(self.functional)

    def mock_values(self):
        return self.values.pop(0)
