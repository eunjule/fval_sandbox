# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from unittest.mock import Mock

from Hbirctc.instrument.hbirctc_register import SCRATCH_REG
from Hbirctc.Tests.ScratchRegister import Diagnostics
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest

class DiagnosticTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 10
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedScratchRegisterWriteReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedScratchRegisterWriteReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedScratchRegisterWriteReadExhaustiveTest_fail(self):
        self.diagnostics.generate_expected_list = \
            Mock(side_effect=self.mock_generate_expected_list)
        self.diagnostics.read_scratch_register = \
            Mock(side_effect=self.mock_read_scratch_register)
        self.expected_list = \
            [i for i in range(self.diagnostics.test_iterations)]
        self.diagnostics.DirectedScratchRegisterWriteReadExhaustiveTest()
        self.validate_fail_message(self.diagnostics)

        for expected in self.expected_list:
            if expected != self.mock_read_scratch_register():
                for reg_num in range(SCRATCH_REG.REGCOUNT):
                    actual = self.mock_read_scratch_register(reg_num)
                    self.validate_log_message(
                        self.call_args_list,
                        self.diagnostics.error_msg(iteration=expected,
                                                   reg_num=reg_num,
                                                   expected_value=expected,
                                                   actual_value=actual),
                        'error')

    def mock_generate_expected_list(self):
        return self.expected_list

    def mock_read_scratch_register(self, reg_num=0):
        return 0
