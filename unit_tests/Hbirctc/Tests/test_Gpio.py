# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Common.fval import SkipTest
from Hbirctc.Tests.Gpio import Diagnostics, Functional
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class FunctionalTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 10
        self.hbirctc = self.functional.hbirctc
        self.call_args_list = self.functional.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedFpgaDebugOutputToInputTest_pass(self):
        self.functional.DirectedFpgaDebugOutputToInputTest()
        self.validate_pass_message(self.functional)

    def test_DirectedFpgaDebugOutputToInputTest_output_enabled_fail(self):
        expected = 0xAA
        actual = 0x55
        self.functional.generate_random_byte = Mock(return_value=expected)
        self.functional.hbirctc.fpga_debug_input = Mock(return_value=actual)
        self.functional.DirectedFpgaDebugOutputToInputTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_output_enabled(iteration=0,
                                                     expected=expected,
                                                     actual=actual),
            'error')

    def test_DirectedFpgaDebugOutputToInputTest_output_disabled_fail(self):
        expected = 0xAA
        self.actual = 0xAA
        self.count = 0
        self.functional.generate_random_byte = Mock(return_value=expected)
        self.functional.hbirctc.fpga_debug_input = \
            Mock(side_effect=self.mock_fpga_debug_input)
        self.functional.DirectedFpgaDebugOutputToInputTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_output_disabled(iteration=0,
                                                      expected=expected,
                                                      actual=self.actual),
            'error')

    def mock_fpga_debug_input(self):
        if self.count < 2:
            data = 0
        else:
            data = self.actual
        self.count += 1
        self.count %= 3

        return data

    def test_DirectedPowerOnCountRegisterTest_pass(self):
        self.hbirctc.load_fpga_using_hil()
        self.functional.DirectedPowerOnCountRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedPowerOnCountRegisterTest_skiptest(self):
        with self.assertRaises(SkipTest):
            self.functional.DirectedPowerOnCountRegisterTest()

    def test_DirectedPowerOnCountRegisterTest_fail(self):
        actual_count = 0xFFFF_FFFF
        expected_count = 0
        self.hbirctc.load_fpga_using_hil()
        self.hbirctc.power_on_count = Mock(return_value=actual_count)
        self.functional.expected_power_on_count = \
            Mock(return_value=expected_count)

        self.functional.DirectedPowerOnCountRegisterTest()
        self.validate_fail_message(self.functional)
        for iteration in range(self.functional.max_fail_count):
            self.validate_log_message(
                self.call_args_list,
                self.functional.error_msg_power_on(iteration=iteration,
                                                   expected=expected_count,
                                                   actual=actual_count),
                'error')

    def test_DirectedFpgaDebugFlippingBitsTest_pass(self):
        self.functional.DirectedFpgaDebugFlippingBitsTest()
        self.validate_pass_message(self.functional)

    def test_DirectedFpgaDebugFlippingBitsTest_fail(self):
        self.default_debug_input = 0xFF
        self.hbirctc.fpga_debug_input = Mock(
            side_effect=self.mock_fpga_debug_input_toggle)
        self.functional.DirectedFpgaDebugFlippingBitsTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_flipping_bits(iteration=0,
                                                    expected=0b11111110,
                                                    actual=0b00000000,
                                                    control=0b00000001),
            'error')

    def mock_fpga_debug_input_toggle(self):
        input = self.default_debug_input
        self.default_debug_input = 0
        return input

    def test_DirectedFpgaDebugFlippingBitsTest_default_input_fail(self):
        self.hbirctc.fpga_debug_input = Mock(return_value=0x11)
        with self.assertRaises(SkipTest):
            self.functional.DirectedFpgaDebugFlippingBitsTest()

    def test_DirectedFpgaLedWriteReadTest_pass(self):
        self.functional.DirectedFpgaLedWriteReadTest()
        self.validate_pass_message(self.functional)

    def test_DirectedFpgaLedWriteReadTest_fail(self):
        actual = 0xAA
        expected = 0x55
        self.functional.generate_random_led_control = \
            Mock(return_value=expected)
        self.hbirctc.fpga_led = Mock(return_value=actual)

        self.functional.DirectedFpgaLedWriteReadTest()
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_mismtach(iteration=0, expected=expected,
                                               actual=actual),
            'error')
        self.validate_fail_message(self.functional)

    def test_DirectedFpgaLedWriteReadTest_restore_fail(self):
        self.functional.test_iterations = 1
        default = 0
        default_error = 2
        getrandbits_value = 1
        self.values = [default, getrandbits_value, default_error]
        self.hbirctc.fpga_led = Mock(side_effect=self.mock_values)
        self.functional.generate_random_led_control = \
            Mock(return_value=getrandbits_value)

        self.functional.DirectedFpgaLedWriteReadTest()
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_default('FPGA_LED', default,
                                              default_error),
            'error')
        self.validate_pass_message(self.functional)

    def mock_values(self):
        return self.values.pop(0)


class DiagnosticsTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 1
        self.diagnostics.max_fail_count = 1
        self.hbirctc = self.diagnostics.hbirctc
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedGeneralPurposeInputRegisterReadTest_pass(self):
        self.diagnostics.DirectedGeneralPurposeInputRegisterReadTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedGeneralPurposeInputRegisterReadTest_fail(self):
        expected = self.hbirctc.general_purpose_input()
        actual = expected + 1
        self.values = [expected, actual]
        self.hbirctc.general_purpose_input = Mock(side_effect=self.mock_values)
        self.diagnostics.DirectedGeneralPurposeInputRegisterReadTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0, expected=expected,
                                       actual=expected+1),
            'error')
        self.validate_fail_message(self.diagnostics)

    def test_DirectedGeneralPurposeOutputRegisterReadTest_pass(self):
        self.diagnostics.DirectedGeneralPurposeOutputRegisterReadTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedGeneralPurposeOutputRegisterReadTest_default_fail(self):
        expected = self.hbirctc.general_purpose_output()
        actual = expected + 1
        self.hbirctc.general_purpose_output = Mock(return_value=actual)
        self.diagnostics.DirectedGeneralPurposeOutputRegisterReadTest()
        reg_name = self.hbirctc.registers.GENERAL_PURPOSE_OUTPUT().name()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg_default(reg_name, expected, actual),
            'error')
        self.validate_pass_message(self.diagnostics)

    def test_DirectedGeneralPurposeOutputRegisterReadTest_iteration_fail(self):
        expected = self.hbirctc.general_purpose_output()
        actual = expected + 1
        self.values = [expected, actual]
        self.hbirctc.general_purpose_output = \
            Mock(side_effect=self.mock_values)
        self.diagnostics.DirectedGeneralPurposeOutputRegisterReadTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0, expected=expected,
                                       actual=expected+1),
            'error')
        self.validate_fail_message(self.diagnostics)

    def mock_values(self):
        return self.values.pop(0)
