# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Hbirctc.Tests.TriggerSync import TimeStampReset
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class TimeStampResetTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = TimeStampReset(methodName='runTest')
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()

        self.test_class.test_iterations = 4
        self.test_class.max_fail_count = 2
        self.call_args_list = self.test_class.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedTimeStampResetTest_pass(self):
        self.test_class.DirectedTimeStampResetTest()
        self.validate_pass_message(self.test_class)

    def test_DirectedTimeStampResetTest_wait_on_value_fail(self):
        wait_on_timer_value = 10
        self.test_class.generate_initial_timer_value_us = \
            Mock(return_value=wait_on_timer_value)
        live_time_stamp = 1
        self.test_class.hbirctc.live_time_stamp = \
            Mock(return_value=live_time_stamp)

        self.test_class.DirectedTimeStampResetTest()

        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_wait_on_timer_value(
                iteration=0, wait_on_value=wait_on_timer_value),
            'error'
        )
        self.validate_fail_message(self.test_class)

    def test_DirectedTimeStampResetTest_clear_fail(self):
        wait_on_timer_value = 10
        self.test_class.generate_initial_timer_value_us = \
            Mock(return_value=wait_on_timer_value)
        live_time_stamp = 10
        self.test_class.hbirctc.live_time_stamp = \
            Mock(return_value=live_time_stamp)
        self.test_class.DirectedTimeStampResetTest()
        max_value_after_reset = 5
        self.test_class.generate_timer_value_after_reset_max_us = \
            Mock(return_value=max_value_after_reset)

        self.test_class.DirectedTimeStampResetTest()

        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_time_stamp_reset(
                iteration=0, time_stamp=live_time_stamp,
                max_value=max_value_after_reset),
            'error'
        )
        self.validate_fail_message(self.test_class)
