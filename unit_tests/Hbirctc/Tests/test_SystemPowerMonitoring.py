# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from itertools import product
from math import nan
from random import getrandbits
from unittest.mock import Mock, patch

from Common.testbench import s2h_simulator
from Common.utilities import dword_to_float, float_to_dword
from Hbirctc.instrument.spm_scopeshot_interface import SpmScopeshotPacket
from Hbirctc.testbench import spm_scopeshot_simulator, spm_simulator
from Hbirctc.Tests.SystemPowerMonitoring import SpmConfigurationAndStatus,\
    SpmData, SpmInterrupt, SpmScopeshot, SpmStreaming
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import get_fail_msg,\
    HbirctcUnitTest


class SpmDataTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.test_case = SpmData()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 10
        self.test_case.max_fail_count = 5
        self.spm = self.test_case.spm
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_DirectedPoutDataStabilityTest_pass(self):
        self.test_case.DirectedPoutDataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutDataStabilityTest_pout_stability_fail(self):
        num_bps = 3
        pout = dword_to_float(self.simulator.rc.spm.DEFAULT_POUT)
        self.values = [pout, pout] * self.test_case.test_iterations * num_bps
        error_index = 0
        error_pout = 1
        self.values[error_index] = error_pout

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedPoutDataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='pout', iteration=error_index,
                                     data=[error_pout, pout]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedPoutDataStabilityTest_vout_stability_fail(self):
        pout = dword_to_float(self.simulator.rc.spm.DEFAULT_POUT)
        iout = dword_to_float(self.simulator.rc.spm.DEFAULT_IOUT)
        self.error_vout = 1

        self.spm.bps_pout_data = Mock(return_value=pout)
        self.spm.bps_iout_data = Mock(return_value=iout)
        self.spm.bps_vout_data = Mock(side_effect=self.bps_vout_data)

        self.test_case.DirectedPoutDataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='pout=v*i',
                                     iteration=0,
                                     data=[pout, self.error_vout * iout]),
            'error')
        self.validate_fail_message(self.test_case)

    def bps_vout_data(self, bps_num):
        if bps_num == 0:
            return self.error_vout
        else:
            return dword_to_float(self.simulator.rc.spm.DEFAULT_VOUT)

    def test_DirectedVoutDataStabilityTest_pass(self):
        self.test_case.DirectedVoutDataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedVoutDataStabilityTest_vout_stability_fail(self):
        num_bps = 3
        vout = dword_to_float(self.simulator.rc.spm.DEFAULT_VOUT)
        self.values = [vout, vout] * self.test_case.test_iterations * num_bps
        error_index = 0
        error_vout = 1
        self.values[error_index] = error_vout

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedVoutDataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='vout', iteration=error_index,
                                     data=[error_vout, vout]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedIoutDataStabilityTest_pass(self):
        self.test_case.DirectedIoutDataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedIoutDataStabilityTest_iout_stability_fail(self):
        num_bps = 3
        iout = dword_to_float(self.simulator.rc.spm.DEFAULT_IOUT)
        self.values = [iout, iout] * self.test_case.test_iterations * num_bps
        error_index = 0
        error_iout = 1
        self.values[error_index] = error_iout

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedIoutDataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='iout', iteration=error_index,
                                     data=[error_iout, iout]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedTemp1DataStabilityTest_pass(self):
        self.test_case.DirectedTemp1DataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedTemp1DataStabilityTest_temp1_stability_fail(self):
        num_bps = 3
        temp1 = dword_to_float(self.simulator.rc.spm.DEFAULT_TEMP1)
        self.values = [temp1, temp1] * self.test_case.test_iterations * num_bps
        error_index = 0
        error_temp1 = 1
        self.values[error_index] = error_temp1

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedTemp1DataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='temp1', iteration=error_index,
                                     data=[error_temp1, temp1]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedTemp2DataStabilityTest_pass(self):
        self.test_case.DirectedTemp2DataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedTemp2DataStabilityTest_temp2_stability_fail(self):
        num_bps = 3
        temp2 = dword_to_float(self.simulator.rc.spm.DEFAULT_TEMP2)
        self.values = [temp2, temp2] * self.test_case.test_iterations * num_bps
        error_index = 0
        error_temp2 = 1
        self.values[error_index] = error_temp2

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedTemp2DataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='temp2', iteration=error_index,
                                     data=[error_temp2, temp2]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedTemp3DataStabilityTest_pass(self):
        self.test_case.DirectedTemp3DataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedTemp3DataStabilityTest_temp3_stability_fail(self):
        num_bps = 3
        temp3 = dword_to_float(self.simulator.rc.spm.DEFAULT_TEMP3)
        self.values = [temp3, temp3] * self.test_case.test_iterations * num_bps
        error_index = 0
        error_temp3 = 1
        self.values[error_index] = error_temp3

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedTemp3DataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='temp3', iteration=error_index,
                                     data=[error_temp3, temp3]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedFanSpeed1DataStabilityTest_pass(self):
        self.test_case.DirectedFanSpeed1DataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedFanSpeed1DataStabilityTest_fan_speed_1_stability_fail(self):
        num_bps = 3
        fan_speed_1 = dword_to_float(self.simulator.rc.spm.DEFAULT_FAN_SPEED_1)
        self.values = [fan_speed_1, fan_speed_1] * \
                      self.test_case.test_iterations * num_bps
        error_index = 0
        error_fan_speed_1 = 1
        self.values[error_index] = error_fan_speed_1

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedFanSpeed1DataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='fan_speed_1',
                                     iteration=error_index,
                                     data=[error_fan_speed_1, fan_speed_1]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_DirectedPoutSumDataStabilityTest_pass(self):
        self.test_case.DirectedPoutSumDataStabilityTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutSumDataStabilityTest_pout_sum_stability_fail(self):
        pout_sum = dword_to_float(self.simulator.rc.spm.DEFAULT_POUT_SUM)
        self.values = [pout_sum, pout_sum] * self.test_case.test_iterations
        error_index = 0
        error_pout_sum = 1
        self.values[error_index] = error_pout_sum

        self.spm.poll_data_using_refresh_rate = self.poll_data

        self.test_case.DirectedPoutSumDataStabilityTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg(data_type='pout_sum',
                                     iteration=error_index,
                                     data=[error_pout_sum, pout_sum]),
            'error')
        self.validate_log_message(
            self.call_args_list,
            get_fail_msg(self.test_case.test_name(), fail_count=1,
                         test_iterations=self.test_case.test_iterations),
            'error')

    def test_run_data_stability_test_zero_value(self):
        self.spm.poll_data_using_refresh_rate = Mock(return_value=[0, 0])
        self.test_case.run_data_stability_test(iteration=0, data_type='iout')
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_zero_value(data_type='iout', iteration=0,
                                                data=[0, 0]),
            'error')

    def poll_data(self, data_type, bps_num, poll_total):
        return [self.values.pop(0), self.values.pop(0)]

    def test_poll_data_invalid_data_type(self):
        self.spm.Log = Mock()
        self.spm.poll_data_using_refresh_rate(data_type='invalid_type_1',
                                              bps_num=0, poll_total=1)
        self.validate_log_message(
            self.spm.Log.call_args_list,
            self.spm.error_msg_invalid_data_type(data_type='invalid_type_1'),
            'error')

    def test_wait_on_data_availability_zero_data(self):
        self.spm.Log = Mock()
        self.spm.bps_vout_data = Mock(return_value=0)
        self.spm.wait_on_data_availability()
        self.validate_log_message(
            self.spm.Log.call_args_list,
            self.spm.error_msg_non_zero_data(),
            'warning')
        self.print_log_messages(self.spm.Log.call_args_list, 'info', 'warning')


class SpmConfigurationAndStatusTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.test_case = SpmConfigurationAndStatus()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 10
        self.test_case.max_fail_count = 5
        self.spm = self.test_case.spm
        self.num_bps = len(self.spm.bps_present())
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list
        self.perf_count = 0

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_DirectedPresentEnableTests_pass(self):
        self.test_case.DirectedPresentEnableTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPresentEnableTests_always_nan_data_fail(self):
        self.test_case.is_data_all_nan = Mock(return_value=True)
        self.test_case.DirectedPresentEnableTest()
        for bps_num in range(self.num_bps):
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_non_nan_data(
                    iteration=0,
                    bps_num=bps_num,
                    is_present=True,
                    is_data_all_nan=True),
                'error')
        self.validate_fail_message(self.test_case)

    def test_DirectedPresentEnableTests_never_nan_data_fail(self):
        self.test_case.is_data_all_nan = Mock(return_value=False)
        self.test_case.DirectedPresentEnableTest()
        for bps_num in range(self.num_bps):
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_non_nan_data(
                    iteration=0,
                    bps_num=bps_num,
                    is_present=False,
                    is_data_all_nan=False),
                'error')
        self.validate_fail_message(self.test_case)

    def test_DirectedPresentEnableTests_approximate_sum_fail(self):
        pout = dword_to_float(self.simulator.rc.spm.DEFAULT_POUT)
        self.spm.bps_pout_sum_data = Mock(return_value=pout)
        self.spm.bps_pout_data = Mock(return_value=pout)
        self.test_case.DirectedPresentEnableTest()
        for sum_total in [pout*2, pout*3]:
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_sum(iteration=0,
                                             sum_total=sum_total,
                                             pout_sum=pout),
                'error')
        self.validate_fail_message(self.test_case)

    def test_DirectedEnablePollingTest_pass(self):
        self.test_case.DirectedEnablePollingTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedEnablePollingTest_fail(self):
        for result in [[False, False], [False, True], [True, False]]:
            self.test_case.run_polling_enabled_scenario = Mock(return_value=
                                                               result[0])
            self.test_case.run_polling_disabled_scenario = Mock(return_value=
                                                                result[1])
            self.test_case.DirectedEnablePollingTest()

    def test_DirectedEnablePollingTest_is_active_fail(self):
        expected = [[1, 1, 1], [0, 0, 0]]
        actual = [[0, 0, 0], [1, 1, 1]]
        enable = [1, 0]
        for i in range(len(expected)):
            self.spm.bps_polling_active = Mock(return_value=actual[i])
            self.test_case.DirectedEnablePollingTest()
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_polling_active(iteration=0,
                                                        expected=expected[i],
                                                        actual=actual[i]),
                'error')
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_polling_enable(iteration=0,
                                                        enable=enable[i]),
                'error')
            self.validate_fail_message(self.test_case)

    def test_DirectedEnablePollingTest_data_change_fail(self):
        iout_unchanged = 0x55
        self.spm.bps_iout_data = Mock(return_value=iout_unchanged)
        self.test_case.DirectedEnablePollingTest()
        self.validate_log_message(
            self.call_args_list,
            f'IOUT_0: No data change obtained. {iout_unchanged}, '
            f'{iout_unchanged}',
            'warning')
        self.validate_fail_message(self.test_case)

    def test_DirectedPollingStatusTest_pass(self):
        self.test_case.DirectedPollingStatusTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPollingStatusTest_fail(self):
        self.error_present_choice = [0, 1, 0]
        self.error_is_active = [1, 0, 0]
        self.test_case.wait_on_polling_status = self.mock_wait_on_polling_status

        self.test_case.DirectedPollingStatusTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_polling_status(
                iteration=0, expected=self.error_present_choice,
                actual=self.error_is_active),
            'error')
        self.validate_fail_message(self.test_case)

    def mock_wait_on_polling_status(self, present_choice):
        if present_choice == self.error_present_choice:
            return self.error_is_active
        else:
            return present_choice

    def test_DirectedPoutAlarmTriggeringTest_pass(self):
        self.test_case.DirectedPoutAlarmTriggeringTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutAlarmTriggeringTest_fail(self):
        for result in [[True, False], [False, False], [False, True]]:
            self.test_case.run_spm_alarm_trigger_scenario = \
                Mock(return_value=result[0])
            self.test_case.run_spm_alarm_clear_scenario = \
                Mock(return_value=result[1])
            self.test_case.DirectedPoutAlarmTriggeringTest()
            self.validate_fail_message(self.test_case)

    def test_run_spm_alarm_trigger_scenario_fail(self):
        for result in [[1, 0], [0, 1]]:
            actual_spm_alarm = result[0]
            actual_global_alarm = result[1]
            self.spm.spm_alarm = Mock(return_value=actual_spm_alarm)
            self.spm.global_spm_alarm = Mock(return_value=actual_global_alarm)
            self.test_case.run_spm_alarm_trigger_scenario(
                iteration=0, sensor_name='pout', limit=100, expected_alarm=1)
            if actual_spm_alarm == 0:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_spm_alarm(
                        iteration=0, sensor_name='pout', expected=1, actual=0),
                    'error')
            if actual_global_alarm == 0:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_global_spm_alarm(
                        iteration=0, sensor_name='pout', expected=1, actual=0),
                    'error')

    def test_run_spm_alarm_clear_scenario_fail(self):
        for result in [[1, 0], [0, 1]]:
            actual_spm_alarm = result[0]
            actual_global_alarm = result[1]
            self.spm.spm_alarm = Mock(return_value=actual_spm_alarm)
            self.spm.global_spm_alarm = Mock(return_value=actual_global_alarm)
            self.test_case.run_spm_alarm_clear_scenario(
                iteration=0, sensor_name='pout', limit=300)
            if actual_spm_alarm == 1:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_spm_alarm(
                        iteration=0, sensor_name='pout', expected=0, actual=1),
                    'error')
            if actual_global_alarm == 1:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_global_spm_alarm(
                        iteration=0, sensor_name='pout', expected=0, actual=1),
                    'error')

    def test_DirectedPoutSumAlarmTriggeringTest_pass(self):
        self.test_case.DirectedPoutSumAlarmTriggeringTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutSumAlarmTriggeringTest_fail(self):
        for result in [[True, False], [False, False], [False, True]]:
            self.test_case.run_spm_alarm_trigger_scenario = \
                Mock(return_value=result[0])
            self.test_case.run_spm_alarm_clear_scenario = \
                Mock(return_value=result[1])
            self.test_case.DirectedPoutSumAlarmTriggeringTest()
            self.validate_fail_message(self.test_case)

    def test_DirectedPoutFilterCountTest_pass(self):
        self.test_case.DirectedPoutFilterCountTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutFilterCountTest_fail(self):
        self.test_case.run_filter_count_scenario = Mock(return_value=False)
        self.test_case.DirectedPoutFilterCountTest()
        self.validate_fail_message(self.test_case)
        self.assertEqual(self.test_case.fail_count,
                         self.test_case.max_fail_count)

    def test_DirectedPoutSumFilterCountTest_pass(self):
        self.test_case.DirectedPoutSumFilterCountTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutSumFilterCountTest_fail(self):
        self.test_case.run_filter_count_scenario = Mock(return_value=False)
        self.test_case.DirectedPoutSumFilterCountTest()
        self.validate_fail_message(self.test_case)
        self.assertEqual(self.test_case.fail_count,
                         self.test_case.max_fail_count)

    def test_run_filter_count_scenario_SpmAlarmWaitError(self):
        present_choice = (1, 1, 1)
        sensor_name = 'pout'
        expected_alarm = self.test_case.hbirctc.registers.SPM_ALARMS(
            bps_0_spm_pout=present_choice[0],
            bps_1_spm_pout=present_choice[1],
            bps_2_spm_pout=present_choice[2]).value
        actual_alarm = expected_alarm - 1
        filter_count = 1
        self.test_case.spm.spm_alarm = Mock(return_value=actual_alarm)
        iteration = 0
        temp_list = self.test_case.FILTER_COUNT_LIST.copy()
        self.test_case.FILTER_COUNT_LIST = [filter_count]

        success = self.test_case.run_filter_count_scenario(
            iteration, sensor_name, present_choice)

        self.test_case.FILTER_COUNT_LIST = temp_list

        self.assertFalse(success)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_spm_alarm_timeout(
                iteration, expected_alarm, filter_count),
            'error')

    def test_run_filter_count_scenario_time_elapsed_fail(self):
        present_choice = (1, 1, 1)
        sensor_name = 'pout'
        expected_alarm = self.test_case.hbirctc.registers.SPM_ALARMS(
            bps_0_spm_pout=present_choice[0],
            bps_1_spm_pout=present_choice[1],
            bps_2_spm_pout=present_choice[2]).value
        filter_count = 1
        iteration = 0
        temp_list = self.test_case.FILTER_COUNT_LIST.copy()
        self.test_case.FILTER_COUNT_LIST = [filter_count]
        expected_time = self.test_case.expected_filter_count_time_elapsed(
            filter_count, present_choice)
        actual_time = expected_time * filter_count * 10
        self.test_case.calculate_filter_count_time_elapsed_us = Mock(
            return_value=actual_time)

        success = self.test_case.run_filter_count_scenario(
            iteration, sensor_name, present_choice)

        self.test_case.FILTER_COUNT_LIST = temp_list

        self.assertFalse(success)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_filter_count(
                iteration, sensor_name, expected_alarm, filter_count,
                expected_time, actual_time),
            'error')

    def test_run_filter_count_scenario_time_elapsed_zero_fail(self):
        present_choice = (0, 0, 0)
        sensor_name = 'pout'
        expected_alarm = self.test_case.hbirctc.registers.SPM_ALARMS(
            bps_0_spm_pout=present_choice[0],
            bps_1_spm_pout=present_choice[1],
            bps_2_spm_pout=present_choice[2]).value
        filter_count = 0
        iteration = 0
        temp_list = self.test_case.FILTER_COUNT_LIST.copy()
        self.test_case.FILTER_COUNT_LIST = [filter_count]
        expected_time = self.test_case.expected_filter_count_time_elapsed(
            filter_count, present_choice)
        actual_time = self.spm.filter_count_absolute_tolerance * 10
        self.test_case.calculate_filter_count_time_elapsed_us = Mock(
            return_value=actual_time)

        success = self.test_case.run_filter_count_scenario(
            iteration, sensor_name, present_choice)

        self.test_case.FILTER_COUNT_LIST = temp_list

        self.assertFalse(success)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_filter_count(
                iteration, sensor_name, expected_alarm, filter_count,
                expected_time, actual_time),
            'error')

    def test_run_filter_count_scenario_init_filter_fail(self):
        present_choice = (1, 1, 1)
        sensor_name = 'pout'
        iteration = 0
        actual_alarm = self.test_case.hbirctc.registers.SPM_ALARMS(
            bps_0_spm_pout=present_choice[0],
            bps_1_spm_pout=present_choice[1],
            bps_2_spm_pout=present_choice[2]).value
        expected_alarm = 0

        self.spm.spm_alarm = Mock(return_value=actual_alarm)
        success = self.test_case.run_filter_count_scenario(
            iteration, sensor_name, present_choice)

        self.assertFalse(success)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_spm_alarm(
                iteration, sensor_name, expected_alarm, actual_alarm),
            'error')

    def test_generate_expected_spm_alarm_polling_active_warning(self):
        present_choice = (1, 1, 1)
        self.test_case.spm.bps_polling_active = Mock(return_value=(0, 0, 1))

        self.test_case.generate_expected_spm_alarm(
            sensor_name='pout', present_choice=present_choice)

        self.validate_log_level(self.call_args_list, 'warning')


class SpmInterruptTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmInterrupt()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 1
        self.test_case.max_fail_count = 1
        self.spm = self.test_case.spm
        self.num_bps = len(self.spm.bps_present())
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_run_pcie_interrupt_alarm_scenario_with_spm_enabled(self):
        present_choices = list(product([0, 1], repeat=self.num_bps))
        self.spm.set_spm_enable(1)

        for present_choice in present_choices:
            self.test_case.run_pcie_interrupt_alarm_scenario(
                present_choice=present_choice, sensor_name='pout')
            actual_pcie_alarm = self.spm.pcie_alarm
            expected_pcie_alarm = 0
            for i in range(len(present_choice)):
                expected_pcie_alarm |= present_choice[i] << i
            self.assertEqual(expected_pcie_alarm, actual_pcie_alarm)

    def test_DirectedPoutAlarmPCIeInterruptTest_pass(self):
        self.test_case.DirectedPoutAlarmPCIeInterruptTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutAlarmPCIeInterruptTest_interrupt_status_fail(self):
        self.interrupt_status_fail_scenario(
            self.test_case.DirectedPoutAlarmPCIeInterruptTest)

    def test_DirectedPoutAlarmPCIeInterruptTest_pcie_alarm_fail(self):
        self.pcie_alarm_fail_scenario(
            self.test_case.DirectedPoutAlarmPCIeInterruptTest)

    def test_DirectedPoutSumAlarmPCIeInterruptTest_pass(self):
        self.test_case.DirectedPoutSumAlarmPCIeInterruptTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedPoutSumAlarmPCIeInterruptTest_interrupt_status_fail(self):
        self.interrupt_status_fail_scenario(
            self.test_case.DirectedPoutSumAlarmPCIeInterruptTest)

    def test_DirectedPoutSumAlarmPCIeInterruptTest_pcie_alarm_fail(self):
        self.pcie_alarm_fail_scenario(
            self.test_case.DirectedPoutSumAlarmPCIeInterruptTest)

    def interrupt_status_fail_scenario(self, test_method):
        for status in [[True, False], [False, True]]:
            expected_status = status[0]
            actual_status = status[1]

            self.spm.pcie_interrupt = Mock(return_value=int(actual_status))
            test_method()

            present_choices = list(product([0, 1], repeat=self.num_bps))
            present_choices.remove((0, 0, 0))
            for present_choice in present_choices:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_interrupt_status(
                        iteration=0, present_choice=present_choice,
                        expected=expected_status, actual=actual_status),
                    'error')
                if actual_status:
                    self.validate_log_message(
                        self.call_args_list,
                        self.test_case.error_msg_clear_interrupt(
                            iteration=0, present_choice=present_choice),
                        'error')

            self.validate_fail_message(self.test_case)

    def pcie_alarm_fail_scenario(self, test_method):
        present_choices = list(product([0, 1], repeat=self.num_bps))
        present_choices.remove((0, 0, 0))
        actual = 0
        if test_method.__name__ == 'DirectedPoutSumAlarmPCIeInterruptTest':
            temp = self.hbirctc.registers.SPM_ALARMS(spm_pout_sum=1).value
            expected = [temp for i in range(len(present_choices))]
        else:
            expected = [i[2] << 2 | i[1] << 1 | i[0] for i in present_choices]

        self.test_case.spm_pcie_alarm = Mock(return_value=actual)
        test_method()

        for present_choice in present_choices:
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_pcie_driver_alarm(
                    iteration=0, present_choice=present_choice,
                    expected=expected.pop(0), actual=actual),
                'error')
        self.validate_fail_message(self.test_case)


class SpmStreamingDirectedValidatePacketTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmStreaming()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 4
        self.test_case.max_fail_count = 2
        self.test_method = self.test_case.DirectedValidatePacketTest
        self.s2h = self.test_case.s2h
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list
        self.stream = self.test_case.test_class.stream

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_method()
        self.assert_log_error_not_found(self.call_args_list)

    def test_packet_header_count_fail(self):
        actual = 0
        expected = actual + 1

        with patch.object(s2h_simulator.S2h, 'counter_for_packet') \
                as mock_method:
            mock_method.return_value = actual
            self.test_method()

        self.validate_log_message_substring(
            self.call_args_list,
            f'packet_header_count (expected: {expected})',
            'error')

    def test_bps_present_fail(self):
        present_choices = self.test_case.generate_present_choices()
        actual = (1, 1, 1)
        present_choices.remove(actual)

        with patch.object(s2h_simulator.S2h, 'bps_present_for_packet') \
                as mock_method:
            mock_method.return_value = actual
            self.test_method()

        for present_choice in present_choices:
            self.validate_log_message_substring(
                self.call_args_list,
                f'bps_present (expected: {list(present_choice)})',
                'error')

    def test_spm_enable_fail(self):
        actual = 0
        expected = 1
        with patch.object(s2h_simulator.S2h, 'spm_enable_for_packet') \
                as mock_method:
            mock_method.return_value = actual
            self.test_method()

        self.validate_log_message_substring(
            self.call_args_list,
            f'spm_enable (expected: {expected})',
            'error')

    def test_packet_type_fail(self):
        actual = 0
        expected = 1
        with patch.object(s2h_simulator.S2h, 'packet_type_for_packet') \
                as mock_method:
            mock_method.return_value = actual
            self.test_method()

        self.validate_log_message_substring(
            self.call_args_list,
            f'packet_type (expected: {expected})',
            'error')

    def test_time_stamp_fail(self):
        tolerance = self.s2h.TIME_STAMP_TOLERANCE
        expected = int(self.s2h.PACKET_REFRESH_MS * 1e3)
        self.actual_time_stamp_step = expected + (expected * tolerance * 2)

        with patch.object(s2h_simulator.S2h, 'time_stamp_for_packet') \
                as mock_method:
            mock_method.side_effect = self.mock_time_stamp_for_packet
            self.test_method()

        self.validate_log_message_substring(
            self.call_args_list,
            f'time_stamp_us diff is {int(self.actual_time_stamp_step) * 2} '
            f'(expected: range {expected - (tolerance * 100)} to '
            f'{expected + (tolerance * 100)})',
            'error')

    def mock_time_stamp_for_packet(self):
        if not hasattr(self, 'counter'):
            self.counter = 0
        else:
            self.counter += self.actual_time_stamp_step
        return int(self.counter)

    def test_pout_data_fail(self):
        actual = 1.0

        with patch.object(s2h_simulator.S2h, 'pout_data_for_packet') \
                as mock_method:
            mock_method.return_value = actual
            self.test_method()

        errors = []
        present_choices = self.test_case.generate_present_choices()
        for present_choice in present_choices:
            error = []
            if not present_choice[0]:
                error.append('pout_bps_0 (expected: nan)')  # forced to actual
            if not present_choice[1]:
                error.append('pout_bps_1 (expected: nan)')  # forced to actual
            if not present_choice[2]:
                error.append('pout_bps_2 (expected: nan)')  # forced to actual

            if present_choice == (0, 0, 0):
                error.append('pout_sum (expected: nan)')
            else:
                sum = 0
                for bps_present in present_choice:
                    sum += actual if bps_present else 0
                if sum != actual * 3:
                    error.append('pout_sum (expected: ' + str(sum))
            if error:
                errors.append(error)

        for error in errors:
            self.validate_log_message_substring(self.call_args_list, error,
                                                'error')

    def test_pout_sum_fail(self):
        actual = nan
        pout_float = 250.0
        pout_dword = float_to_dword(pout_float)
        with patch.object(s2h_simulator.S2h, 'pout_sum_data_for_packet') \
                as mock_method,\
                patch.object(spm_simulator.SpmInterface, 'generate_pout_data')\
                        as mock_generate_pout_data:
            mock_method.return_value = actual
            mock_generate_pout_data.return_value = pout_dword
            self.test_method()

        num_bps = 3
        errors = [f'pout_sum (expected: {pout_float * i})'
                  for i in range(1, num_bps+1)]
        for error in errors:
            self.validate_log_message_substring(self.call_args_list, error,
                                                'error')

    def test_combo_fail(self):
        actual_enable = 0
        expected_enable = 1
        actual_count = 0
        expected_count = actual_count + 1
        with patch.object(s2h_simulator.S2h, 'spm_enable_for_packet') \
                as mock_enable, \
                patch.object(s2h_simulator.S2h, 'counter_for_packet') \
                        as mock_count:
            mock_enable.return_value = actual_enable
            mock_count.return_value = actual_count
            self.test_method()

        errors = [f'packet_header_count (expected: {expected_count})',
                  f'spm_enable (expected: {actual_enable})']
        self.validate_log_message_substring(self.call_args_list, errors,
                                            'error')

class SpmStreamingTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmStreaming()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 4
        self.test_case.max_fail_count = 2
        self.s2h = self.test_case.s2h
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list
        self.stream = self.test_case.test_class.stream

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_DirectedStreamEnableUsingBarRegisterTest_pass(self):
        self.test_case.DirectedStreamEnableUsingBarRegisterTest()
        self.assert_log_error_not_found(self.call_args_list)

    def test_DirectedStreamEnableUsingBarRegisterTest_fail(self):
        self.stream.wait_on_valid_valid_packet_index = Mock(
            return_value=-1)

        self.stream.wait_on_matching_valid_packet_index = Mock(
            return_value=s2h_simulator.S2hInterface.PACKET_REFRESH_MS)

        num_tests = len(self.test_case.generate_present_choices()) + 1
        error_indexes = [1, 3]
        self.valid_packet_index_list =\
            [s2h_simulator.S2hInterface.INVALID_PACKET_INDEX
             if (i == error_indexes[0] or i == error_indexes[1]) else i
             for i in range(self.test_case.test_iterations * num_tests)]

        self.stream.valid_packet_index = \
            self.valid_packet_index_mock

        self.test_case.DirectedStreamEnableUsingBarRegisterTest()

        for index in error_indexes:
            error_msg = self.test_case.test_class.error_msg_enable_stream(
                iteration=index,
                current=s2h_simulator.S2hInterface.INVALID_PACKET_INDEX,
                previous=index-1)
            self.validate_log_message(
                call_args_list=self.test_case.Log.call_args_list,
                expected_message=error_msg,
                expected_level='error')

    def valid_packet_index_mock(self):
        return self.valid_packet_index_list.pop(0)

    def test_DirectedStreamDisableUsingBarRegisterTest_pass(self):
        self.test_case.DirectedStreamDisableUsingBarRegisterTest()
        self.assert_log_error_not_found(self.call_args_list)

    def test_DirectedStreamDisableUsingBarRegisterTest_fail(self):
        num_tests = len(self.test_case.generate_present_choices()) + 1
        self.valid_packet_index_list = [i for i in range(
            self.test_case.test_iterations * num_tests)]

        self.stream.valid_packet_index =\
            self.valid_packet_index_mock

        self.stream.reset_stream = Mock()
        self.stream.disable_stream = Mock()
        self.stream.enable_stream = Mock()
        self.stream.wait_on_valid_valid_packet_index = Mock()

        self.test_case.DirectedStreamDisableUsingBarRegisterTest()

        for index in range(0, self.test_case.max_fail_count * 2, 2):
            current_index = index
            next_index = index + 1

            error_msg = self.test_case.test_class.error_msg_disable_stream(
                iteration=0,
                current_index=current_index,
                next_index=next_index)

            self.validate_log_message(
                call_args_list=self.test_case.Log.call_args_list,
                expected_message=error_msg,
                expected_level='error')

    def test_DirectedStreamResetUsingBarRegisterTest_pass(self):
        self.test_case.DirectedStreamResetUsingBarRegisterTest()
        self.assert_log_error_not_found(self.call_args_list)

    def test_DirectedStreamResetUsingBarRegisterTest_fail(self):
        self.stream.wait_on_valid_valid_packet_index = Mock()
        self.stream.enable_stream = Mock()
        self.stream.reset_stream = Mock()

        error_indexes = [3, 5]
        error_index_value = getrandbits(16) % \
                            s2h_simulator.S2hInterface.INVALID_PACKET_INDEX

        num_tests = len(self.test_case.generate_present_choices()) + 20
        self.valid_packet_index_list = \
            [error_index_value
             if (i == error_indexes[0] or i == error_indexes[1])
             else s2h_simulator.S2hInterface.INVALID_PACKET_INDEX
             for i in range(self.test_case.test_iterations * num_tests)]

        self.stream.valid_packet_index = self.valid_packet_index_mock

        self.test_case.DirectedStreamResetUsingBarRegisterTest()

        for index in error_indexes:
            error_msg = self.test_case.test_class.error_msg_reset_stream(
                iteration=index, index=error_index_value)

            self.validate_log_message(
                call_args_list=self.test_case.Log.call_args_list,
                expected_message=error_msg,
                expected_level='error')


class SpmScopeshotDirectedValidatePacketTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_case.DirectedValidatePacketTest()
        self.validate_pass_message(self.test_case)

    def test_fail(self):
        self.test_case.num_packets = 2
        actual_bps = [1, 1, 1]
        packet = SpmScopeshotPacket(value=0)
        packet.bps_0_present = actual_bps[0]
        packet.bps_1_present = actual_bps[1]
        packet.bps_2_present = actual_bps[2]
        packet.spm_enable=1
        packet.pout_bps_0 = nan
        packet.pout_bps_1 = nan
        packet.pout_bps_2 = float(12 * 20)
        packet.pout_sum = 200.0
        sa = self.ss.ddr4_starting_address()
        self.test_case.capture_packets = \
            Mock(return_value=[packet
                               for i in range(self.test_case.num_packets)])

        self.test_case.DirectedValidatePacketTest()

        present_choices = \
            list(product([0, 1], repeat=self.test_case.spm.NUM_BPS))
        for present in present_choices:
            present = list(present)
            if present != actual_bps:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_validate_packet(
                        iteration=0, address=sa,
                        expected_bps=present, actual_bps=actual_bps),
                    'error')
            else:
                self.assert_log_message_not_found(
                    self.call_args_list,
                    self.test_case.error_msg_validate_packet(
                        iteration=0, address=sa,
                        expected_bps=present, actual_bps=actual_bps))

            expected_time_stamp = int(self.ss.PACKET_REFRESH_RATE_SEC * 1e6)
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_validate_packet(
                    iteration=0, address=sa + self.ss.DDR4_ENTRY_BYTE_SIZE,
                    expected_time_stamp=expected_time_stamp,
                    actual_time_stamp=packet.time_stamp_us),
                'error')

            if present == [0, 0, 0]:
                expected_pout_012 = [nan, nan, nan]
                actual_pout_012 = [packet.pout_bps_0, packet.pout_bps_1,
                                   packet.pout_bps_2]
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_validate_packet(
                        iteration=0, address=sa,
                        expected_pout_012=expected_pout_012,
                        actual_pout_012=actual_pout_012),
                    'error')

                expected_pout_sum = nan
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_validate_packet(
                        iteration=0, address=sa,
                        expected_pout_sum=expected_pout_sum,
                        actual_pout_sum=packet.pout_sum),
                    'error')

            expected_pout_sum = float(12 * 20)
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_validate_packet(
                    iteration=0, address=sa,
                    expected_pout_sum=expected_pout_sum,
                    actual_pout_sum=packet.pout_sum),
                'error')


class SpmScopeshotDirectedWriteReadBarRegisterTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_DirectedWriteReadBarRegisterTest_pass(self):
        self.test_case.DirectedWriteReadBarRegisterTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedWriteReadBarRegisterTest_fail(self):
        self.ss.is_enabled = lambda : False
        self.test_case.DirectedWriteReadBarRegisterTest()
        self.validate_log_message(
            self.call_args_list, self.test_case.error_msg_wr_fail(
                iteration=0, name='enable', expected=True, actual=False),
            'error')

        self.ss.is_enabled = lambda: True
        self.test_case.DirectedWriteReadBarRegisterTest()
        self.validate_log_message(
            self.call_args_list, self.test_case.error_msg_wr_fail(
                iteration=0, name='enable', expected=False, actual=True),
            'error')

        self.ss.mode = lambda: 0
        self.test_case.DirectedWriteReadBarRegisterTest()
        self.validate_log_message(
            self.call_args_list, self.test_case.error_msg_wr_fail(
                iteration=0, name='mode', expected=1, actual=0),
            'error')

        self.ss.mode = lambda: 1
        self.test_case.DirectedWriteReadBarRegisterTest()
        self.validate_log_message(
            self.call_args_list, self.test_case.error_msg_wr_fail(
                iteration=0, name='mode', expected=0, actual=1),
            'error')

        self.ss.sample_count = lambda: 0
        self.test_case.DirectedWriteReadBarRegisterTest()
        self.validate_log_message(
            self.call_args_list, self.test_case.error_msg_wr_fail(
                iteration=0, name='sample_count', expected=0xFFFF, actual=0),
            'error')


class SpmScopeshotDirectedValidatePacketWithSpmDisabledTestTests(
    HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_DirectedValidatePacketWithSpmDisabledTest_pass(self):
        self.test_case.DirectedValidatePacketWithSpmDisabledTest()
        self.validate_pass_message(self.test_case)

    def test_DirectedValidatePacketWithSpmDisabledTest_fail(self):
        self.test_case.num_packets = 2
        actual_bps = [1, 1, 1]
        packet = SpmScopeshotPacket(value=0)
        packet.bps_0_present = actual_bps[0]
        packet.bps_1_present = actual_bps[1]
        packet.bps_2_present = actual_bps[2]
        packet.spm_enable = 0
        packet.pout_bps_0 = nan
        packet.pout_bps_1 = nan
        packet.pout_bps_2 = float(12 * 20)
        packet.pout_sum = 200.0
        sa = self.ss.ddr4_starting_address()
        self.test_case.capture_packets = \
            Mock(return_value=[packet
                               for i in range(self.test_case.num_packets)])

        self.test_case.DirectedValidatePacketWithSpmDisabledTest()

        present_choices = \
            list(product([0, 1], repeat=self.test_case.spm.NUM_BPS))
        for present in present_choices:
            present = list(present)
            if present != actual_bps:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_validate_packet(
                        iteration=0, address=sa,
                        expected_bps=present, actual_bps=actual_bps),
                    'error')
            else:
                self.assert_log_message_not_found(
                    self.call_args_list,
                    self.test_case.error_msg_validate_packet(
                        iteration=0, address=sa,
                        expected_bps=present, actual_bps=actual_bps))

            self.assert_log_message_not_found(
                self.call_args_list,
                '(expected_time_stamp, actual_time_stamp)')

            expected_pout_012 = [nan, nan, nan]
            actual_pout_012 = [packet.pout_bps_0, packet.pout_bps_1,
                               packet.pout_bps_2]
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_validate_packet(
                    iteration=0, address=sa,
                    expected_pout_012=expected_pout_012,
                    actual_pout_012=actual_pout_012),
                'error')

            expected_pout_sum = nan
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_validate_packet(
                    iteration=0, address=sa,
                    expected_pout_sum=expected_pout_sum,
                    actual_pout_sum=packet.pout_sum),
                'error')


class SpmScopeshotDirectedCircularBufferWriteReadExhaustiveTestTests(
    HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_case.DirectedCircularBufferWriteReadExhaustiveTest()
        self.validate_pass_message(self.test_case)

    def test_retry_error_exception(self):
        sa = self.ss.ddr4_starting_address()

        self.ss.ddr4_latest_address = lambda: sa - 1
        with self.assertRaises(self.ss.RetryError) as e:
            self.test_case.DirectedCircularBufferWriteReadExhaustiveTest()
        self.assertEqual(e.exception.args[0],
                         'Failed to start and sync Scopeshot engine')
        for retry in range(5):  # 5 is num_retries param from test
            self.validate_log_message(
                self.call_args_list,
                self.test_case.warn_msg_retry_latest_address(retry+1, 5),
            'warning')

    def test_memory_wrap_count_fail(self):
        sa = self.ss.ddr4_starting_address()

        self.ss.ddr4_latest_address = lambda: sa
        try:
            self.test_case.DirectedCircularBufferWriteReadExhaustiveTest()
        except self.ss.RetryError:
            pass
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_memory_wrap_count(iteration=0,
                                                       memory_wrap_count=0),
            'error')

    def test_memory_wrap_time_stamp_fail(self):
        # this allows the failed memory reads to continue and wrap around
        self.test_case.max_fail_count = 12

        with patch.object( spm_scopeshot_simulator.SpmScopeshotInterface,
                           'generate_packet') as mock_generate_packet:
            mock_generate_packet.return_value = SpmScopeshotPacket(value=0)
            self.test_case.DirectedCircularBufferWriteReadExhaustiveTest()

        sa = self.ss.ddr4_starting_address()
        ea = self.ss.ddr4_ending_address()
        total_packets = (ea - sa) // self.ss.DDR4_ENTRY_BYTE_SIZE
        address = self.ss.ddr4_next_address(self.ss.ddr4_latest_address())
        for i in range(total_packets-1):
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_memory_wrap_time_stamp(
                    iteration=0, address_1=self.ss.ddr4_next_address(address),
                    address_0=address, stamp_1=0, stamp_0=0),
                'error')
            address = self.ss.ddr4_next_address(address)
            address = self.ss.ddr4_next_address(address)


class DirectedScopeshotStartStopUsingBarRegisterTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.test_method = \
            self.test_case.DirectedScopeshotStartStopUsingBarRegisterTest
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_method()
        self.validate_pass_message(self.test_case)

    def test_clear_memory(self):
        self.test_case.clear_memory(iteration=0, address=0, num_packets=10,
                                    byte_length=24)
        self.assert_log_error_not_found(self.call_args_list)

        with patch.object(self.hbirctc, 'dma_read') as mock_dma_read:
            byte_length = 24
            mock_dma_read.return_value = b'\xff' * byte_length
            self.test_case.clear_memory(iteration=0, address=0, num_packets=10,
                                        byte_length=byte_length)
        self.validate_log_message(
            self.call_args_list,
            f'Iteration 0) Unable to clear memory',
            'error')

    def test_invalid_number_captured_packets(self):
        sa = self.ss.ddr4_starting_address()
        self.ss.ddr4_next_address = lambda x: sa

        self.test_method()

        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_start_stop_packet_count(
                iteration=0, expected=self.test_case.num_packets // 2, actual=0,
                absolute_tolerance=10),
            'error')

    def test_invalid_data_value(self):
        packet_byte_size = self.ss.DDR4_ENTRY_BYTE_SIZE

        zero_bytes = bytes(packet_byte_size)
        self.hbirctc.dma_read = lambda x, y: zero_bytes
        self.test_method()
        address = self.ss.ddr4_starting_address()
        for i in range(self.test_case.num_packets // 2):
            if i >= self.test_case.max_fail_count:
                break
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_start_stop_data_value(
                    iteration=0, address=address, expecting_zero=False),
                'error')
            address = self.ss.ddr4_next_address(address)

        ones_bytes = b'\xFF' * packet_byte_size
        self.hbirctc.dma_read = lambda x, y: ones_bytes
        self.test_method()
        address = self.ss.ddr4_starting_address() + \
                  ((self.test_case.num_packets * packet_byte_size) // 2)
        for i in range(self.test_case.num_packets // 2):
            if i >= self.test_case.max_fail_count:
                break
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_start_stop_data_value(
                    iteration=0, address=address, expecting_zero=True),
                'error')
            address = self.ss.ddr4_next_address(address)


class DirectedScopeshotSampleCountTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.test_method = self.test_case.DirectedScopeshotSampleCountTest
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_method()
        self.assert_log_error_not_found(self.call_args_list)

    def test_still_active_after_sample_count(self):
        self.ss.is_active = lambda : True
        self.test_method()
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_sample_count_status(),
            'error')

    def test_sample_count_mismatch(self):
        sa = self.ss.ddr4_starting_address()

        self.ss.ddr4_latest_address = lambda : sa
        self.test_method()

        for count in self.test_case.sample_counts:
            if count > 1:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_case.error_msg_sample_count(
                        expected=count, actual=0, absolute_tolerance=1),
                    'error')


class DirectedScopeshotEnableAndActiveTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.test_method = self.test_case.DirectedScopeshotEnableAndActiveTest
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_method()
        self.validate_pass_message(self.test_case)

    def test_enabling_scopeshot(self):
        self.ss.enable = Mock(side_effect=self.mock_enable)
        self.assertTrue(self.test_case.run_enable_test(iteration=0))

        self.ss.enable = lambda : None
        self.ss.wait_on_latest_address = lambda x : None
        self.assertFalse(self.test_case.run_enable_test(iteration=0))

    def test_disabling_scopeshot(self):
        self.ss.disable = Mock(side_effect=self.mock_disable)
        self.assertTrue(self.test_case.run_disable_test(iteration=0))

        self.ss.disable = Mock(side_effect=self.mock_enable)
        self.assertFalse(self.test_case.run_disable_test(iteration=0))

    def test_not_active_after_enable_fail(self):
        self.ss.is_active = lambda : False
        self.assertFalse(self.test_case.run_enable_test(iteration=0))
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_enable_active_is_active(
                iteration=0, expected_is_active=True),
            'error')

    def test_active_after_disable_fail(self):
        self.ss.is_active = lambda : True
        with self.assertRaises(self.ss.TimeoutError):
            self.test_case.run_disable_test(iteration=0)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_enable_active_is_active(
                iteration=0, expected_is_active=False),
            'error')

    def test_latest_address_after_enable_fail(self):
        sa = self.ss.ddr4_starting_address()
        self.ss.ddr4_latest_address = lambda : sa
        self.test_case.run_enable_test(iteration=0)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_enable_active_latest_address(
                iteration=0, expected=sa, actual=sa, is_enabled=True),
            'error')

    def test_latest_address_after_disable_fail(self):
        self.test_case.test_iterations = 1
        self.addresses = [0, 1]

        self.ss.ddr4_latest_address = Mock(side_effect=self.mock_latest_address)
        self.test_case.run_disable_test(iteration=0)
        self.validate_log_message(
            self.call_args_list,
            self.test_case.error_msg_enable_active_latest_address(
                iteration=0, expected=0, actual=1, is_enabled=False),
            'error')

    def mock_latest_address(self):
        return self.addresses.pop(0)

    def mock_enable(self):
        reg = self.ss.config_reg()
        reg.enable = 1
        self.hbirctc.write_bar_register(reg)

    def mock_disable(self):
        reg = self.ss.config_reg()
        reg.enable = 0
        self.hbirctc.write_bar_register(reg)


class DirectedScopeshotStopInAlarmModeTestTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_case = SpmScopeshot()
        self.test_case.setUp(tester=self.tester)
        self.test_case.Log = Mock()
        self.test_case.test_iterations = 2
        self.test_case.max_fail_count = 2
        self.test_method = self.test_case.DirectedScopeshotStopInAlarmModeTest
        self.ss = self.test_case.scopeshot
        self.hbirctc = self.test_case.hbirctc
        self.call_args_list = self.test_case.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'error')

    def test_pass(self):
        self.test_method()
        self.assert_log_error_not_found(self.call_args_list)

    def test_active_status_fail(self):
        sensor_names = ['pout', 'pout_sum']

        self.ss.is_active = lambda : True
        enable_alarm_mode = True
        for sensor in sensor_names:
            iteration = 0
            with self.assertRaises(self.ss.TimeoutError):
                self.test_case.run_stop_in_alarm_mode_test(
                    iteration, sensor, enable_alarm_mode)
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_enable_active_is_active(
                    iteration, expected_is_active=False),
                'error')

        self.ss.is_active = lambda: False
        enable_alarm_mode = False
        for sensor in sensor_names:
            iteration = 0
            self.test_case.run_stop_in_alarm_mode_test(
                iteration, sensor, enable_alarm_mode)
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_enable_active_is_active(
                    iteration, expected_is_active=True),
                'error')

    def test_latest_address_after_alarm_fail(self):
        sensor_names = ['pout', 'pout_sum']

        sa = self.ss.ddr4_starting_address()
        self.ss.ddr4_latest_address = lambda : sa
        packet_byte_size = self.ss.DDR4_ENTRY_BYTE_SIZE

        enable_alarm_mode = True
        for sensor in sensor_names:
            iteration = 0
            self.test_case.run_stop_in_alarm_mode_test(
                iteration, sensor, enable_alarm_mode)

            max_num_packets_allowed = self.test_case.num_packets - 10
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_address_in_alarm_mode(
                    iteration,
                    actual_num_packets_captured=0,
                    max_num_packets_allowed=max_num_packets_allowed),
                'error')

        enable_alarm_mode = False
        for sensor in sensor_names:
            iteration = 0
            self.test_case.run_stop_in_alarm_mode_test(
                iteration, sensor, enable_alarm_mode)
            expected_address = sa + (self.test_case.num_packets *
                                     packet_byte_size)
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_address_in_trigger_mode(
                    iteration, expected_address, sa),
                'error')

    def test_set_spm_power_alarm_mode_fail(self):
        self.mode_value = 0
        sensor_names = ['pout', 'pout_sum']

        iteration = 0
        self.ss.set_spm_power_alarm_mode = self.mock_set_mode
        enable_alarm_mode = not bool(self.mode_value)
        for sensor in  sensor_names:
            self.assertFalse(self.test_case.run_stop_in_alarm_mode_test(
                iteration, sensor, enable_alarm_mode))

            max_num_packets_allowed = self.test_case.num_packets - 10
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_address_in_alarm_mode(
                    iteration=0, actual_num_packets_captured=0,
                    max_num_packets_allowed=max_num_packets_allowed),
                'error')

    def test_set_trigger_mode_fail(self):
        self.mode_value = 1
        sensor_names = ['pout', 'pout_sum']

        iteration = 0
        self.ss.set_trigger_mode = self.mock_set_mode
        enable_alarm_mode = not bool(self.mode_value)
        for sensor in  sensor_names:
            self.assertFalse(self.test_case.run_stop_in_alarm_mode_test(
                iteration, sensor, enable_alarm_mode))

            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_enable_active_is_active(
                    iteration=0, expected_is_active=True),
                'error')

            sa = self.ss.ddr4_starting_address()
            expected_address = sa + (self.test_case.num_packets *
                                     self.ss.DDR4_ENTRY_BYTE_SIZE)
            self.validate_log_message(
                self.call_args_list,
                self.test_case.error_msg_address_in_trigger_mode(
                    iteration=0, expected_address=expected_address, address=sa),
                'error')

    def mock_set_mode(self):
        self.ss.set_mode(self.mode_value)
