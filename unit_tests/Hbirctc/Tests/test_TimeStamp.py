# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Hbirctc.Tests.TimeStamp import Functional
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class FunctionalTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.test_class = Functional()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.test_class.test_iterations = 10
        self.test_class.max_fail_count = 5
        self.hbirctc = self.test_class.hbirctc
        self.call_args_list = self.test_class.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedLiveTimeStampReadTest_pass(self):
        self.test_class.DirectedLiveTimeStampReadTest()
        self.validate_pass_message(self.test_class)

    def test_DirectedLiveTimeStampReadTest_no_increment_fail(self):
        time_elapsed_us = 10
        self.test_class.calculate_elapsed_time_us = \
            Mock(return_value=time_elapsed_us)

        time_stamps = [time_elapsed_us, time_elapsed_us]
        self.test_class.read_time_stamps = Mock(return_value=time_stamps)

        self.test_class.DirectedLiveTimeStampReadTest()

        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_time_stamp_increment(
                iteration=0, time_stamp=time_stamps),
            'error')
        self.validate_fail_message(self.test_class)

    def test_DirectedLiveTimeStampReadTest_max_diff_fail(self):
        time_elapsed_us = 10
        self.test_class.calculate_elapsed_time_us = \
            Mock(return_value=time_elapsed_us)

        time_stamps = [time_elapsed_us + time_elapsed_us + 1, time_elapsed_us]
        self.test_class.read_time_stamps = Mock(return_value=time_stamps)

        self.test_class.DirectedLiveTimeStampReadTest()

        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_time_stamp_range(
                iteration=0, time_stamp=time_stamps,
                elapsed_time=time_elapsed_us),
            'error')
        self.validate_fail_message(self.test_class)

    def test_DirectedLiveTimeStampReadTest_zero_diff_fail(self):
        time_elapsed_us = 10
        self.test_class.calculate_elapsed_time_us = \
            Mock(return_value=time_elapsed_us)

        time_stamps = [time_elapsed_us, time_elapsed_us]
        self.test_class.read_time_stamps = Mock(return_value=time_stamps)

        self.test_class.DirectedLiveTimeStampReadTest()

        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_time_stamp_range(
                iteration=0, time_stamp=time_stamps,
                elapsed_time=time_elapsed_us),
            'error')
        self.validate_fail_message(self.test_class)
