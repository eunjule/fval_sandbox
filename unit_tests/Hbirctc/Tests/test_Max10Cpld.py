################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from unittest.mock import Mock

from Hbirctc.Tests.Max10Cpld import Functional
from Hbirctc.Tests.Max10Cpld import Diagnostics
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class Max10CpldFunctionalTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 2
        self.hbirctc = self.functional.hbirctc
        self.call_args_list = self.functional.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info',
                                'error')

    def test_DirectedCpldInterruptSignalTest_pass(self):
        self.functional.DirectedCpldInterruptSignalTest()
        self.validate_pass_message(self.functional)

    def test_DirectedCpldInterruptSignalTest_interrupt_not_enable_fail(self):
        interrupt_values = []
        for iteration in range(self.functional.test_iterations):
            interrupt_values.insert(0, 0)
            interrupt_values.insert(0, 0)
        self.hbirctc.get_cpld_status_interrupt_bit = \
            Mock(side_effect=interrupt_values)

        self.functional.DirectedCpldInterruptSignalTest()

        for iteration in range(self.functional.test_iterations):
            self.validate_log_message(
                self.call_args_list,
                f'Iteration_{iteration} interrupt enable: False, '
                f'interrupt cleared: True',
                'error')
        self.validate_fail_message(self.functional)

    def test_DirectedCpldInterruptSignalTest_interrupt_not_clear_fail(self):
        interrupt_values = []
        for iteration in range(self.functional.test_iterations):
            interrupt_values.insert(0, 1)
            interrupt_values.insert(0, 1)
        self.hbirctc.get_cpld_status_interrupt_bit = \
            Mock(side_effect=interrupt_values)

        self.functional.DirectedCpldInterruptSignalTest()

        for iteration in range(self.functional.test_iterations):
            self.validate_log_message(
                self.call_args_list,
                f'Iteration_{iteration} interrupt enable: True, '
                f'interrupt cleared: False',
                'error')
        self.validate_fail_message(self.functional)

    def test_DirectedCpldRxFifoCountTest_pass(self):
        self.functional.DirectedCpldRxFifoCountTest()
        self.validate_pass_message(self.functional)

    def test_DirectedCpldRxFifoCountTest_fail(self):
        max_fifo_count = 1023

        self.hbirctc.max10.rx_fifo_count = Mock(return_value=max_fifo_count)
        self.functional.DirectedCpldRxFifoCountTest()
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_fifo_count_mismatch(
                iteration=0,
                expected_count=0,
                actual_count=max_fifo_count),
            'error')
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_fifo_reset(
                iteration=0,
                rx_fifo_count=max_fifo_count),
            'error')
        self.validate_fail_message(self.functional)


class Max10CpldDiagnosticsTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.hbirctc = self.diagnostics.hbirctc
        self.call_args_list = self.diagnostics.Log.call_args_list

        self.expected_data = None
        self.actual_data = None

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info',
                                'error')

    def test_DirectedCpldReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedCpldReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedCpldReadExhaustiveTest_fail(self):
        self.diagnostics.test_iterations = 3
        self.read_version_fail_error_index = 1
        self.read_version_fail_iteration_counter = 0
        self.read_version_fail_mod_2= 0
        self.expected_data = [1, 2, 3] # major, minor, pcb
        self.actual_data = [4, 5, 6]
        self.hbirctc.max10.read_fw_revision = \
            Mock(side_effect=self.read_version_fail)

        self.diagnostics.DirectedCpldReadExhaustiveTest()

        for i in range(self.diagnostics.test_iterations):
            referenced_msg = f'0x{self.expected_data[0]:02X}, ' \
                             f'0x{self.expected_data[1]:0X}, ' \
                             f'0x{self.expected_data[2]:0X}'
            observed_msg = f'0x{self.actual_data[0]:02X}, ' \
                           f'0x{self.actual_data[1]:0X}, ' \
                           f'0x{self.actual_data[2]:0X}'
            log_message = f'Iteration_{i} reference(maj, min, pcb), ' \
                          f'observed(maj, min, pcb): ({referenced_msg}), ' \
                          f'({observed_msg})'
            if i == self.read_version_fail_error_index:
                self.validate_log_message(self.call_args_list,
                                          log_message, 'error')

        self.validate_fail_message(self.diagnostics)

    def read_version_fail(self):
        # It takes two version reads for one iteration, so increment on every
        # second read
        if self.read_version_fail_iteration_counter % 2:
            self.read_version_fail_mod_2 +=1
        self.read_version_fail_iteration_counter +=1

        if self.read_version_fail_mod_2 == self.read_version_fail_error_index:
            if self.read_version_fail_iteration_counter % 2:
                return self.actual_data
            else:
                return self.expected_data
        else:
            return self.expected_data

    def test_DirectedCpldWriteReadScratchRegisterTest_pass(self):
        self.diagnostics.DirectedCpldWriteReadScratchRegisterTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedCpldWriteReadScratchRegisterTest_fail(self):
        self.expected_data = []
        self.actual_data = []
        self.hbirctc.max10.write_rctc_scratch_pad = \
            Mock(side_effect=self.capture_scratch_write)
        self.hbirctc.max10.rctc_scratch_pad = \
            Mock(side_effect=self.read_scratch_fail)
        self.diagnostics.DirectedCpldWriteReadScratchRegisterTest()

        for i in range(self.diagnostics.test_iterations):
            self.validate_log_message(
                self.call_args_list,
                f'Iteration_{i} (expected, actual): '
                f'0x{self.expected_data.pop():04X}, '
                f'0x{self.actual_data.pop():04X}',
                'error')
        self.validate_fail_message(self.diagnostics)

    def capture_scratch_write(self, data):
        self.expected_data.insert(0, data)

    def read_scratch_fail(self):
        fail_data = ~self.expected_data[len(self.expected_data)-1]
        self.actual_data.insert(0, fail_data)
        return fail_data
