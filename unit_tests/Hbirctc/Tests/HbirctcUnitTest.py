################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
from unittest import TestCase

from Common import hilmon
from Common.hbi_simulator import HbiSimulator
from Common.instruments.tester import get_tester_for_unit_test
from Hbirctc.testbench.hbirctc_simulator import simulate_from_file_method_calls
from Hbirctc.Tests.HbirctcTest import get_fail_msg, get_pass_msg


class HbirctcUnitTest(TestCase):
    def setUp(self):
        self.simulator = HbiSimulator('Simulated HW')
        hilmon.hil = self.simulator
        self.tester = get_tester_for_unit_test()
        self.tester.discover_all_instruments()
        self.tester.devices[0].under_test = True

        # avoid unit test accessing network share
        simulate_from_file_method_calls(self.tester.rc)

    def name(self):
        return self.__class__.__name__

    def print_log_messages(self, call_args_list, *msg_type):
        if os.getenv('DBG_LOGGING'):
            for log_info, kwargs in call_args_list:
                log_level, log_message = log_info
                if log_level in msg_type:
                    print('{:5s}: {}'.format(log_level.upper(), log_message))

    def validate_pass_message(self, test_class):
        self.validate_log_message(
            test_class.Log.call_args_list,
            get_pass_msg(test_class.test_name(), test_class.test_iterations),
            'info')

    def validate_fail_message(self, test_class):
        self.assert_log_error_found(test_class.Log.call_args_list)

        fail_count = test_class.fail_count
        if test_class.fail_count > test_class.max_fail_count:
            fail_count = test_class.max_fail_count
        self.validate_log_message(
            test_class.Log.call_args_list,
            get_fail_msg(test_class.test_name(), fail_count,
                         test_class.test_iterations),
            'error')

    def validate_log_message(self, call_args_list, expected_message,
                             expected_level):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if (log_message == expected_message) and \
                    (log_level == expected_level):
                break
        else:
            self.fail(f'"{expected_message} with log level of '
                      f'{expected_level}" not found.')

    def validate_log_message_substring(self, call_args_list, expected_messages,
                                       expected_level):
        if not isinstance(expected_messages, list):
            expected_messages = [expected_messages]
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            found_substrings =any(substring in log_message
                                  for substring in expected_messages)
            if found_substrings and(log_level == expected_level):
                break
        else:
            self.fail(f'Substrings "{expected_messages}" with log level of '
                      f'{expected_level} not found.')

    def validate_log_level(self, call_args_list, expected_level):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if log_level == expected_level:
                break
        else:
            self.fail(f'"Log level of \"{expected_level}\" not found.')

    def assert_log_error_found(self, call_args_list):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if log_level == 'error':
                break
        else:
            self.fail(f'No log messages with log_level of error found.')

    def assert_log_error_not_found(self, call_args_list):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if log_level == 'error':
                self.fail(f'Log messages with log_level of error found.')

    def assert_log_message_not_found(self, call_args_list, message):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if log_message == message:
                self.fail(f'Log messages with log_level of error found.')
