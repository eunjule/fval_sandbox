# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Hbirctc.instrument.peci_dac import PECI_DAC_CHANNELS
from Hbirctc.Tests.PeciDac import Diagnostics
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class DiagnosticsTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.hbirctc = self.diagnostics.hbirctc
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedPeciDacWriteReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedPeciDacWriteReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedPeciDacWriteReadExhaustiveTest_dac_mismatch_fail(self):
        expected = 0x123
        actual = 0x132

        self.diagnostics.generate_random_dac_value = Mock(
            return_value=expected)
        self.hbirctc.peci_dac.dac_output = Mock(return_value=actual)

        self.diagnostics.DirectedPeciDacWriteReadExhaustiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg_data_mismatch(iteration=0,
                                                     expected_data=expected,
                                                     actual_data=actual),
            'error')
        self.validate_fail_message(self.diagnostics)

    def test_peci_dac_wait_on_busy_fail(self):
        peci_dac = self.hbirctc.peci_dac
        peci_dac.status_reg = Mock(
            return_value=self.hbirctc.registers.PECI_DAC_STATUS(busy=1))
        peci_dac.Log = Mock()
        self.call_args_list = peci_dac.Log.call_args_list
        peci_dac.wait_on_busy()
        self.validate_log_message(
            self.call_args_list,
            peci_dac.error_msg_busy_timeout(),
            'error')

    def test_peci_dac_is_valid_channel_fail(self):
        peci_dac = self.hbirctc.peci_dac
        peci_dac.Log = Mock()
        self.call_args_list = peci_dac.Log.call_args_list
        max_possible_channel = 0xF
        for channel in range(max_possible_channel+1):
            if channel in PECI_DAC_CHANNELS:
                self.assertEqual(True, peci_dac.is_valid_channel(channel))
            else:
                self.assertEqual(False, peci_dac.is_valid_channel(channel))
                self.validate_log_message(
                    self.call_args_list,
                    peci_dac.warning_msg_invalid_channel(channel),
                    'warning')
