################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

from unittest.mock import Mock

from Hbirctc.Tests.Temperature import DELTA, Diagnostics, TEMPERATURE_RANGE
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class DiagnosticsTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 1
        self.call_args_list = self.diagnostics.Log.call_args_list
        self.hbirctc = self.diagnostics.hbirctc

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedTemperatureTest_pass(self):
        self.diagnostics.DirectedTemperatureTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedTemperatureTest_range_fail(self):
        actual = (TEMPERATURE_RANGE[0] - (DELTA * 2))

        self.hbirctc.temperature_using_bar_read = Mock(return_value=actual)
        self.diagnostics.DirectedTemperatureTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg_out_of_range(actual),
            'error')
        self.validate_pass_message(self.diagnostics)

    def test_DirectedTemperatureTest_iteration_fail(self):
        self.diagnostics.test_iterations = 1
        expected = self.hbirctc.temperature_using_bar_read(0)
        actual = (TEMPERATURE_RANGE[0] - (DELTA * 2))
        num_channels = 2
        self.values = [expected, actual] * num_channels
        self.hbirctc.temperature_using_bar_read = \
            Mock(side_effect=self.mock_values)
        self.diagnostics.DirectedTemperatureTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0, expected=expected,
                                       actual=actual),
            'error')
        self.validate_fail_message(self.diagnostics)

    def mock_values(self, channel=0):
        return self.values.pop(0)
