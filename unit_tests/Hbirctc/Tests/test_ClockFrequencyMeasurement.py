# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Hbirctc.Tests.ClockFrequencyMeasurement import DELTA_PERCENTAGE,\
    FrequencyMeasurement
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class FrequencyMeasurementTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.frequency_measurement = FrequencyMeasurement()
        self.frequency_measurement.setUp(tester=self.tester)
        self.frequency_measurement.Log = Mock()
        self.frequency_measurement.test_iterations = 1
        self.frequency_measurement.max_fail_count = 1
        self.call_args_list = self.frequency_measurement.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedFrequencyReadTest_pass(self):
        self.frequency_measurement.DirectedFrequencyReadTest()
        self.validate_pass_message(self.frequency_measurement)

    def test_DirectedFrequencyReadTest_fail(self):
        self.expected_freqs = FrequencyMeasurement.EXPECTED_FREQS.copy()
        error_index = 4
        error_freq = \
            self.expected_freqs[error_index] * (DELTA_PERCENTAGE + 1.01)
        self.error_clock = {error_index: error_freq}

        self.frequency_measurement.hbirctc.read_clock_frequency_mhz = \
            Mock(side_effect=self._mock_read_clock_frequency_mhz)

        self.frequency_measurement.DirectedFrequencyReadTest()

        self.validate_log_message(
            self.call_args_list,
            self.frequency_measurement.error_msg_frequency_msimatch(
                iteration=0,
                name=FrequencyMeasurement.Clocks(error_index).name,
                expected=self.expected_freqs[error_index],
                measured=error_freq),
            'error')

    def _mock_read_clock_frequency_mhz(self, index):
        return self.error_clock.get(index, self.expected_freqs[index])
