################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import unittest
import ctypes

from Hbirctc.instrument.hbirctc_register import get_register_types,\
    get_struct_types


class HbirctcRegistersTests(unittest.TestCase):

    def test_no_ctype_register_bar_and_addresses_are_reused(self):
        seen = {}
        skipped_address = [0x604]
        for RegisterType in get_register_types():
            if seen.get((RegisterType.ADDR, RegisterType.BAR)) is None:
                seen[(RegisterType.ADDR, RegisterType.BAR)] = \
                    RegisterType.__name__
            else:
                if RegisterType.ADDR not in skipped_address:
                    self.fail(f'Address 0x{RegisterType.ADDR:04X} is shared '
                              f'by {RegisterType.__name__} and '
                              f'{seen[(RegisterType.ADDR, RegisterType.BAR)]}')

    def test_ctype_registers_are_four_bytes_in_size(self):
        for RegisterType in get_register_types():
            if ctypes.sizeof(RegisterType) != 4:
                self.fail(f'{RegisterType.__name__} is not 4 bytes in size')

    def test_all_32_bits_are_specified_in_ctype_registers(self):
        FIELD_WIDTH_SPECIFIER_INDEX = 2
        for RegisterType in get_register_types():
            if 32 != sum([x[FIELD_WIDTH_SPECIFIER_INDEX]
                          for x in RegisterType._fields_]):
                self.fail(f'{RegisterType.__name__} is not fully specified '
                          f'for all 32 bits')

    def test_ctype_structs_are_four_bytes_in_size(self):
        for StructType in get_struct_types():
            if StructType.__name__ != 'SampleHeaderStruct':
                if ctypes.sizeof(StructType) != 4:
                    self.fail(f'{StructType.__name__} is not 4 bytes in size')



