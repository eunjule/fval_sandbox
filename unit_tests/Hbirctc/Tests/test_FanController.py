# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from unittest.mock import Mock

from Hbirctc.instrument.max6650_register import ConfigModes
from Hbirctc.testbench.fan_chip_simulator import MB_FAN_INDEXES, DPS_FAN_INDEXES, create_fan_chips
from Hbirctc.Tests.FanController import Diagnostics
from Hbirctc.Tests.FanController import Functional
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest
from unit_tests.Common.Tests.test_I2cController import FunctionalTests as I2cControllerFunctionalTests


class DiagnosticTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2

        self.fan_chips = create_fan_chips()

    def tearDown(self):
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info', 'error')
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info', 'error')

    def test_DirectedWriteReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedWriteReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedWriteReadExhaustiveTest_invalid_mode_fail(self):
        self.diagnostics.read_fan_mode = self.read_fan_mode_mock
        self.read_fan_mode_chip_num_errors = [MB_FAN_INDEXES[0], DPS_FAN_INDEXES[0]]

        self.diagnostics.DirectedWriteReadExhaustiveTest()

        for iteration in range(self.diagnostics.test_iterations):
            for chip_num in self.read_fan_mode_chip_num_errors:
                self.validate_log_message(self.diagnostics.Log.call_args_list,
                                          f'Fan({chip_num}): Unable to perform read/write test because configuration mode is closed-loop',
                                          'error')

    def read_fan_mode_mock(self, chip_num):
        if chip_num in self.read_fan_mode_chip_num_errors:
            return ConfigModes.CLOSED_LOOP
        else:
            return ConfigModes.FULL_ON

    def test_DirectedWriteReadExhaustiveTest_fail(self):
        self.diagnostics.test_iterations = 1
        self.write_speed_reg_expected = {}
        self.read_speed_reg_actuals = {}
        self.read_speed_reg_error_indexes = [MB_FAN_INDEXES[0], DPS_FAN_INDEXES[0]]
        self.diagnostics.write_speed_reg = self.write_speed_reg_mock
        self.diagnostics.read_speed_reg = self.read_speed_reg_mock

        self.diagnostics.DirectedWriteReadExhaustiveTest()

        for iteration in range(self.diagnostics.test_iterations):
            for chip_num in self.read_speed_reg_error_indexes:
                self.validate_log_message(self.diagnostics.Log.call_args_list,
                                          f'Iteration_{iteration} Fan({chip_num}). RW (Expected, Actual): '
                                          f'{self.write_speed_reg_expected[chip_num]:X}, {self.read_speed_reg_actuals[chip_num]:X}',
                                          'error')
        self.validate_fail_message(self.diagnostics)

    def write_speed_reg_mock(self, chip_num, data):
        self.write_speed_reg_expected[chip_num] = data

    def read_speed_reg_mock(self, chip_num):
        if chip_num in self.read_speed_reg_error_indexes:
            self.read_speed_reg_actuals[chip_num] = self.write_speed_reg_expected[chip_num] - 1
        else:
            self.read_speed_reg_actuals[chip_num] = self.write_speed_reg_expected[chip_num]
        return self.read_speed_reg_actuals[chip_num]


class FunctionalTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.test_iterations = 2
        self.functional.max_fail_count = 2

        self.tests = I2cControllerFunctionalTests()
        self.tests.Log = self.functional.Log = Mock()

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list, 'info', 'error')

    def test_DirectedTxFifoCountErrorBitCheckingTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedTxFifoCountErrorBitCheckingTest_fail(self):
        self.tests.tx_fifo_count_error_bit_test_valid_count_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

        self.functional.fail_count = 0
        self.tests.tx_fifo_count_error_bit_test_invalid_count_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedAddressNakTest_pass(self):
        self.tests.validate_i2c_test_pass(self.functional.DirectedAddressNakTest)

    def test_DirectedAddressNakTest_fail(self):
        self.tests.address_nak_test_valid_address_fail(
            self.functional.interfaces,
            self.functional.DirectedAddressNakTest)

        self.functional.fail_count = 0
        self.tests.address_nak_test_invalid_address_fail(
            self.functional.interfaces,
            self.functional.DirectedAddressNakTest)

    def test_DirectedTxFifoCountTest_pass(self):
        self.tests.validate_i2c_test_pass(self.functional.DirectedTxFifoCountTest)

    def test_DirectedTxFifoCountTest_fail(self):
        self.tests.tx_fifo_count_test_fail(self.functional.interfaces,
                                           self.functional.DirectedTxFifoCountTest)
