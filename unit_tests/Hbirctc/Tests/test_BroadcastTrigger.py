# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Hbirctc.Tests.BroadcastTrigger import Functional
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class FunctionalTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = Functional(methodName='runTest')
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()

        self.test_class.test_iterations = 4
        self.test_class.max_fail_count = 2
        self.call_args_list = self.test_class.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedLastBroadcastedTriggerTest_pass(self):
        self.test_class.DirectedLastBroadcastedTriggerTest()
        self.validate_pass_message(self.test_class)

    def test_DirectedLastBroadcastedTriggerTest_fail(self):
        expected_trigger = 0x123
        self.test_class.generate_trigger = Mock(return_value=expected_trigger)
        actual_trigger = 0x456
        self.test_class.hbirctc.read_last_broadcasted_trigger = \
            Mock(return_value=actual_trigger)

        self.test_class.DirectedLastBroadcastedTriggerTest()

        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_trigger_mismatch(
                iteration=0, expected=expected_trigger, actual=actual_trigger),
            'error'
        )
        self.validate_fail_message(self.test_class)
