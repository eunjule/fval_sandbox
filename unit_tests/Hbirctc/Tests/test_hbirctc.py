################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import logging
import time
import unittest
from unittest.mock import patch
from unittest.mock import Mock

from Common import configs
from Common.fval import ConfigLogger, SetLoggerLevel
from Common.cache_blt_info import BltError
from Hbicc.instrument.mainboard import MainBoard
from Hbirctc.instrument.hbirctc import Hbirctc
from Hbirctc.instrument.hbirctc_register import AURORA_ERROR_COUNTS, \
    AURORA_STATUS, AURORA_CONTROL

time.sleep = lambda x: None

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__),
                                  '..', '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))


class HbirctcInstrumentTests(unittest.TestCase):

    def setUp(self):
        self.mock_rc = lambda x: None

    def test_rctc_aurora_link_if_link_is_up(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            error_count_register = \
                hbirctc_instrument.registers.AURORA_ERROR_COUNTS()
            error_count_register.value = 0x0
            hbirctc_instrument.aurora_link_test_loop = 2
            hbirctc_instrument.read_bar_register = \
                Mock(side_effect=self.normal)
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)
    
    def normal(self, RegisterType, index):
        if RegisterType is AURORA_CONTROL:
            return AURORA_CONTROL(value=0x0)
        if RegisterType is AURORA_STATUS:
            return AURORA_STATUS(value=0x27)
        elif RegisterType is AURORA_ERROR_COUNTS:
            return AURORA_ERROR_COUNTS(value=0x0)

    def test_rctc_aurora_link_tx_fifo_full(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            error_count_register = \
                hbirctc_instrument.registers.AURORA_ERROR_COUNTS(value=0)
            hbirctc_instrument.aurora_link_test_loop = 2
            hbirctc_instrument.aurora_status = \
                Mock(side_effect=self.fail_once())
            hbirctc_instrument.aurora_error_count = \
                Mock(return_value=error_count_register)
            hbirctc_instrument.bar_read = Mock(return_value=0x00000027)
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)
    
    def fail_once(self):
        status_register_fail_status = AURORA_STATUS()
        status_register_fail_status.transceiver_pll_locked = 0x1
        status_register_fail_status.lane_up = 0x1
        status_register_fail_status.channel_up = 0x1
        status_register_fail_status.soft_error = 0x0
        status_register_fail_status.hard_error = 0x0
        status_register_fail_status.tx_pll_lock = 0x1
        status_register_fail_status.reserved = 0x0
        status_register_fail_status.rx_fifo_count = 0x0
        status_register_fail_status.rx_fifo_full = 0x0
        status_register_fail_status.tx_fifo_count = 0x0
        status_register_fail_status.tx_fifo_full = 0x1
        yield status_register_fail_status
        status_register = AURORA_STATUS()
        status_register.transceiver_pll_locked = 0x1
        status_register.lane_up = 0x1
        status_register.channel_up = 0x1
        status_register.soft_error = 0x0
        status_register.hard_error = 0x0
        status_register.tx_pll_lock = 0x1
        status_register.reserved = 0x0
        status_register.rx_fifo_count = 0x0
        status_register.rx_fifo_full = 0x0
        status_register.tx_fifo_count = 0x0
        status_register.tx_fifo_full = 0x0
        while True:
            yield status_register
            
    def test_rctc_aurora_link_rx_fifo_full(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            error_count_register = \
                hbirctc_instrument.registers.AURORA_ERROR_COUNTS(value=0)
            hbirctc_instrument.aurora_status = \
                Mock(side_effect=self.fail_once())
            hbirctc_instrument.aurora_error_count = \
                Mock(return_value=error_count_register)
            hbirctc_instrument.aurora_link_test_loop = 2
            hbirctc_instrument.bar_read = Mock(return_value=0x00000027)
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)

    def test_rctc_aurora_link_rx_fifo_count(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            error_count_register = \
                hbirctc_instrument.registers.AURORA_ERROR_COUNTS(value=0)
            hbirctc_instrument.aurora_status = \
                Mock(side_effect=self.fail_once())
            hbirctc_instrument.aurora_error_count = \
                Mock(return_value=error_count_register)
            hbirctc_instrument.aurora_link_test_loop = 2
            hbirctc_instrument.bar_read = Mock(return_value=0x00000027)
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)

    def test_rctc_aurora_link_tx_fifo_count(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            error_count_register = \
                hbirctc_instrument.registers.AURORA_ERROR_COUNTS(value=0)
            hbirctc_instrument.aurora_status = \
                Mock(side_effect=self.fail_once())
            hbirctc_instrument.aurora_error_count = \
                Mock(return_value=error_count_register)
            hbirctc_instrument.aurora_link_test_loop = 2
            hbirctc_instrument.bar_read = Mock(return_value=0x00000027)
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)

    def test_rctc_aurora_link_error_count(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            status_register = hbirctc_instrument.registers.AURORA_STATUS()
            status_register.transceiver_pll_locked = 0x1
            status_register.lane_up = 0x1
            status_register.channel_up = 0x1
            status_register.soft_error = 0x0
            status_register.hard_error = 0x0
            status_register.tx_pll_lock = 0x1
            status_register.reserved = 0x0
            status_register.rx_fifo_count = 0x0
            status_register.rx_fifo_full = 0x0
            status_register.tx_fifo_count = 0x0
            status_register.tx_fifo_full = 0x0
            hbirctc_instrument.aurora_status = \
                Mock(return_value = status_register)
            hbirctc_instrument.aurora_link_test_loop = 2
            hbirctc_instrument.bar_read = Mock(return_value=0x00000027)
            hbirctc_instrument.aurora_error_count = \
                Mock(side_effect=self.fail_error_n_times(1))
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)
    
    def fail_error_n_times(self, n):
        error_count_fail_register = AURORA_ERROR_COUNTS(soft_error_count=0x50,
                                                        hard_error_count=0x50)
        for i in range(n):
            yield error_count_fail_register
        error_count_pass_register = AURORA_ERROR_COUNTS(value=0)
        while True:
            yield error_count_pass_register

    def test_rctc_aurora_link_reset_all(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            trigger_link_index = 16
            hbirctc_instrument = Hbirctc()
            status_register = hbirctc_instrument.registers.AURORA_STATUS()
            status_register.transceiver_pll_locked = 0x1
            status_register.lane_up = 0x1
            status_register.channel_up = 0x1
            status_register.soft_error = 0x0
            status_register.hard_error = 0x0
            status_register.tx_pll_lock = 0x1
            status_register.reserved = 0x0
            status_register.rx_fifo_count = 0x0
            status_register.rx_fifo_full = 0x0
            status_register.tx_fifo_count = 0x0
            status_register.tx_fifo_full = 0x0
            error_count_pass_register = \
                hbirctc_instrument.registers.AURORA_ERROR_COUNTS(value=0)
            hbirctc_instrument.aurora_status = \
                Mock(return_value = status_register)
            hbirctc_instrument.aurora_link_test_loop = 1
            hbirctc_instrument.bar_read = Mock(return_value=0x00000027)
            hbirctc_instrument.aurora_error_count = \
                Mock(return_value=error_count_pass_register)
            rctc_link_up = hbirctc_instrument.reset_rctc_aurora_link(
                trigger_link_index)
            self.assertEqual(rctc_link_up, True)

    def test_chip_id_correct_message(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            hbirctc_instrument = Hbirctc()
            chip_id_upper = \
                hbirctc_instrument.registers.CHIP_ID_UPPER(Data=0x1800)
            chip_id_lower = \
                hbirctc_instrument.registers.CHIP_ID_LOWER(Data=0xDEAD)
            hbirctc_instrument.read_bar_register = \
                Mock(side_effect=[chip_id_upper,chip_id_lower])
            hbirctc_instrument.Log = Mock()
            hbirctc_instrument.log_chip_id()
            log_calls = hbirctc_instrument.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
                self.assertRegex(
                    log_message,
                    f'HBIRCTC Chip ID : '
                    f'0x{chip_id_upper.value:08X}{chip_id_lower.value:08X}',
                    'Log not displaying correct information')

    def test_chip_id_wrong_message(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            hbirctc_instrument = Hbirctc()
            chip_id_upper = hbirctc_instrument.registers.CHIP_ID_UPPER(Data=0)
            chip_id_lower = hbirctc_instrument.registers.CHIP_ID_LOWER(Data=0)
            hbirctc_instrument.read_bar_register = \
                Mock(side_effect=[chip_id_upper,chip_id_lower])
            hbirctc_instrument.Log = Mock()
            hbirctc_instrument.log_chip_id()
            log_calls = hbirctc_instrument.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(
                    log_message,
                    f'HBIRCTC Chip ID read failed. Received '
                    f'0x{chip_id_upper.value:08X}{chip_id_lower.value:08X}',
                    'Log not displaying correct information')

    def test_chip_id_ffff_message(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            hbirctc_instrument = Hbirctc()
            chip_id_upper = \
                hbirctc_instrument.registers.CHIP_ID_UPPER(Data=0xffffffff)
            chip_id_lower = \
                hbirctc_instrument.registers.CHIP_ID_LOWER(Data=0)
            hbirctc_instrument.read_bar_register = \
                Mock(side_effect=[chip_id_upper,chip_id_lower])
            hbirctc_instrument.Log = Mock()
            hbirctc_instrument.log_chip_id()
            log_calls = hbirctc_instrument.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'ERROR')
                self.assertRegex(
                    log_message,
                    f'HBIRCTC Chip ID read failed. Received '
                    f'0x{chip_id_upper.value:08X}{chip_id_lower.value:08X}',
                    'Log not displaying correct information')

    def test_fpga_version_correct_message(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            hbirctc_instrument = Hbirctc()
            expected_version_string = '5.0-2'
            mock_hil_hbirctc.hbiMbFpgaVersionString = \
                Mock(return_value=expected_version_string)
            hbirctc_instrument.Log = Mock()
            hbirctc_instrument.log_fpga_version()
            log_calls = hbirctc_instrument.Log.call_args_list
            for log_info, kwargs in log_calls:
                log_level, log_message = log_info
                self.assertEqual(log_level.upper(), 'INFO')
                self.assertRegex(
                    log_message,
                    f'HBIRCTC: FPGA Version {expected_version_string}',
                    'Log not displaying correct information')

    def test_fpga_version_wrong_message(self):
        with patch('Hbirctc.instrument.hbirctc.hil') as mock_hil_hbirctc:
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            hbirctc_instrument = Hbirctc()
            invalid_strings = ['0.0', f'{str(0xffff)}.{str(0xffff)}']
            for invalid_string in invalid_strings:
                mock_hil_hbirctc.hbiMbFpgaVersionString = \
                    Mock(return_value=invalid_string)
                hbirctc_instrument.Log = Mock()
                hbirctc_instrument.log_fpga_version()
                log_calls = hbirctc_instrument.Log.call_args_list
                for log_info, kwargs in log_calls:
                    log_level, log_message = log_info
                    self.assertEqual(log_level.upper(), 'WARNING')
                    self.assertRegex(
                        log_message,
                        f'HBIRCTC: FPGA Version read all 0\' or all f\'s {invalid_string}',
                        'Log not displaying correct information')

    def test_blt_part_number(self):
        with patch('Common.hilmon.hil') as mock_hil_hbirctc,\
                patch.object(configs, 'HBICC_IN_HDMT', False):
            invalid_part_numbers = \
                ['K15216-200', 'AAK15216-000', 'AAK15216-20', 'AAK15216-r00',
                 'AAK15216200', 'AAK15216-', '-', '']

            for part_num in invalid_part_numbers:
                mock_blt = MockBlt()
                mock_blt.PartNumberCurrent = part_num
                mock_hil_hbirctc.hbiMbBltBoardRead = \
                    Mock(return_value=mock_blt)
                hbirctc_instrument = Hbirctc(mainboard=MainBoard())
                with self.assertRaises(BltError):
                    hbirctc_instrument.mainboard.blt.fab_letter()
        
            # Valid part number, but FAB_A "100" does not exist
            mock_blt = MockBlt()
            mock_blt.PartNumberCurrent = 'AAK15216-100'
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(return_value=mock_blt)
            hbirctc_instrument = Hbirctc()
            with self.assertRaises(AttributeError):
                hbirctc_instrument.fpga_image_path()

            # Valid part number should not raise any exceptions
            mock_hil_hbirctc.hbiMbBltBoardRead = Mock(side_effect=MockBlt)
            hbirctc_instrument = Hbirctc()


class MockBlt():
    PartNumberCurrent='AAK15216-200'


if __name__ == '__main__':
    SetLoggerLevel(logging.DEBUG)
    ConfigLogger()
    unittest.main(verbosity=2)
