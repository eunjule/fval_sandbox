# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from unittest.mock import Mock

from Hbirctc.Tests.Failsafe import Diagnostics, FAILSAFE_LIMIT_DEFAULT
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest


class DiagnosticsTests(HbirctcUnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 1
        self.diagnostics.max_fail_count = 1
        self.hbirctc = self.diagnostics.hbirctc
        self.hbirctc._failsafe_limit_default = FAILSAFE_LIMIT_DEFAULT
        self.call_args_list = self.diagnostics.Log.call_args_list

    def test_DirectedTriggerSystemFailsafeTest_pass(self):
        self.diagnostics.DirectedTriggerSystemFailsafeTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedTriggerSystemFailsafeTest_fail(self):
        self.diagnostics.wait_on_sys_fail_safe = Mock(return_value=False)
        self.diagnostics.DirectedTriggerSystemFailsafeTest()
        self.validate_fail_message(self.diagnostics)
