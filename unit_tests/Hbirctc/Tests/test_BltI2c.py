# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from unittest.mock import Mock, patch

from Hbirctc.instrument.blt_i2c_interface import MAX_NUM_CHIPS, BltI2cInterface
from Hbirctc.Tests.BltI2c import Diagnostics
from Hbirctc.Tests.BltI2c import Functional
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest
from unit_tests.Common.Tests.test_I2cController import \
    FunctionalTests as I2cFunctionalTests


class DiagnosticTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedWriteReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedWriteReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedWriteReadExhaustiveTest_fail(self):
        self.expected = [0xAA] * MAX_NUM_CHIPS* \
                        (self.diagnostics.test_iterations + 1)
        self.actual = self.expected.copy()
        self.actual[1] = 0x55

        self.diagnostics.generate_random_byte = Mock(
            side_effect=self.mock_generate)
        with patch.object(BltI2cInterface,
                          'read_pca9505_output_0') as mock_read:
            mock_read.side_effect = self.mock_read
            self.diagnostics.DirectedWriteReadExhaustiveTest()

        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0, expected=0xAA, actual=0x55),
            'error')

    def mock_generate(self):
        return self.expected.pop(0)

    def mock_read(self):
        return self.actual.pop(0)


class FunctionalTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.test_iterations = 2
        self.functional.max_fail_count = 2

        self.tests = I2cFunctionalTests()
        self.tests.Log = self.functional.Log = Mock()
        self.call_args_list = self.functional.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedTxFifoCountErrorBitCheckingTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedTxFifoCountErrorBitCheckingTest_fail(self):
        self.tests.tx_fifo_count_error_bit_test_valid_count_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

        self.functional.fail_count = 0
        self.tests.tx_fifo_count_error_bit_test_invalid_count_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    # def test_DirectedAddressNakTest_pass(self):
    #     self.functional.DirectedAddressNakTest()
    #     self.validate_pass_message(self.functional)
    #
    # def test_DirectedAddressNakTest_fail(self):
    #     self.functional.is_address_naked = Mock(return_value=True)
    #     self.functional.DirectedAddressNakTest()
    #     self.validate_fail_message(self.functional)
    #     self.validate_log_message(
    #         self.functional.Log.call_args_list,
    #         self.functional.error_msg_address_nak(iteration=0,
    #                                               blt_num=0,
    #                                               expected=False,
    #                                               actual=True),
    #         'error')

    def test_DirectedTxFifoCountTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountTest)

    def test_DirectedTxFifoCountTest_fail(self):
        self.tests.tx_fifo_count_test_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountTest)
