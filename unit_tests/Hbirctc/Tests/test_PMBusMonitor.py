# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Hbirctc.instrument.pmbus_monitor_i2c_interface import\
    PmBusMonitorInterface
from Hbirctc.Tests.PMBusMonitor import Functional, Diagnostics
from unit_tests.Hbirctc.Tests.HbirctcUnitTest import HbirctcUnitTest
from unit_tests.Common.Tests.test_I2cController import\
    FunctionalTests as I2cFunctionalTests


class DiagnosticTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2

    def tearDown(self):
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info',
                                'error')

    def test_DirectedReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedReadExhaustiveTest_fail(self):
        with patch.object(PmBusMonitorInterface, 'read') as mock_read:
            mock_read.return_value = 0

            self.diagnostics.DirectedReadExhaustiveTest()

            expected_data = PmBusMonitorInterface.KNOWN_VALUE_REGISTERS[0x19]
            self.validate_log_message(
                self.diagnostics.Log.call_args_list,
                self.diagnostics.exhaustive_read_error_msg(
                    iteration=0,
                    name=self.diagnostics.interfaces[0].interface_name,
                    expected_data=expected_data,
                    actual_data=0),
                expected_level='error')

            self.validate_fail_message(self.diagnostics)


class FunctionalTests(HbirctcUnitTest):

    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.test_iterations = 2
        self.functional.max_fail_count = 2

        self.tests = I2cFunctionalTests()
        self.tests.Log = self.functional.Log = Mock()

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list, 'info',
                                'error')

    def test_DirectedTxFifoCountErrorBitCheckingTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedTxFifoCountErrorBitCheckingTest_fail(self):
        self.tests.tx_fifo_count_error_bit_test_valid_count_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

        self.functional.fail_count = 0
        self.tests.tx_fifo_count_error_bit_test_invalid_count_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedAddressNakTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedAddressNakTest)

    def test_DirectedAddressNakTest_fail(self):
        self.tests.address_nak_test_valid_address_fail(
            self.functional.interfaces,
            self.functional.DirectedAddressNakTest)

        self.functional.fail_count = 0
        self.tests.address_nak_test_invalid_address_fail(
            self.functional.interfaces,
            self.functional.DirectedAddressNakTest)

    def test_DirectedTxFifoCountTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountTest)

    def test_DirectedTxFifoCountTest_fail(self):
        self.tests.tx_fifo_count_test_fail(
            self.functional.interfaces,
            self.functional.DirectedTxFifoCountTest)
