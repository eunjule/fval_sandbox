# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
import unittest

from Hbirctc.testbench.hbirctc_simulator import Rc


class SoftwareTriggerTests(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.rc = Rc(system=None)

    def test_software_trigger_fifo(self):
        test_triggers = self._trigger_test_data(count=2)
        self._check_software_fifo_count(expected=0)
        for i, trigger in enumerate(test_triggers):
            self.rc.trigger_up(trigger.data, link=trigger.source)
            self._check_software_fifo_count(expected=i + 1)
        for i, trigger in enumerate(test_triggers):
            self._check_software_trigger_data(trigger.data,
                                              expected_source=trigger.source)
            self._check_software_fifo_count(
                expected=len(test_triggers) - i - 1)

    def test_software_trigger_fifo_empty(self):
        self._check_software_fifo_count(expected=0)
        self._check_software_trigger_data(0, 0)

    def test_software_trigger_reset(self):
        test_triggers = self._trigger_test_data(count=20)
        for trigger in test_triggers:
            self.rc.trigger_up(trigger.data, link=0)
        self._check_software_fifo_count(expected=len(test_triggers))
        self.rc.bar_write(1, 0xA20, 1)
        self._check_software_fifo_count(expected=0)
        self.rc.trigger_up(test_triggers[0].data, link=0)
        self._check_software_fifo_count(expected=0)

    def test_non_software_trigger(self):
        other_trigger = 0x40001234
        self.rc.trigger_up(other_trigger, link=0)
        self._check_software_fifo_count(expected=0)

    def _trigger_test_data(self, count):
        SOFTWARE_TRIGGER_BIT_MASK = 0x00080000
        triggers = []
        for i in range(count):
            data = random.getrandbits(32) | SOFTWARE_TRIGGER_BIT_MASK
            source = random.randrange(0, 18)
            triggers.append(SoftwareTriggerTests.Trigger(data, source))
        return triggers

    def _check_software_trigger_data(self, expected, expected_source):
        source = self.rc.bar_read(bar=1, offset=0xA2C)
        self.assertEqual(source, expected_source)
        value = self.rc.bar_read(bar=1, offset=0xA28)
        self.assertEqual(value, expected)

    def _check_software_fifo_count(self, expected):
        count = self.rc.bar_read(bar=1, offset=0xA24)
        self.assertEqual(count, expected)

    class Trigger():
        def __init__(self, data, source):
            self.data = data
            self.source = source
