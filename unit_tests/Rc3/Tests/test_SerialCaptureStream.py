# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Common.instruments import hdmt_trigger_interface
from Common.triggers import SCSMemoryEncoding
from Rc3.Tests.SerialCaptureStream import ScopeshotEngineDiagnostics,\
    ScopeshotEngineFunctional
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DirectedScopeshotTriggerDataFlowByLinkTestUnitTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = ScopeshotEngineDiagnostics()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.call_args_list = self.test_class.Log.call_args_list
        self.test_class.test_iterations = 1
        self.test_class.max_fail_count = 1
        self.scs = self.test_class.scs

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_pass(self):
        self.test_class.DirectedScopeshotTriggerDataFlowByLinkTest()
        self.assert_log_error_not_found(self.call_args_list)

    def test_ddr4_data_mismatch_fail(self):
        num_triggers = 10
        expected_entries = [
            SCSMemoryEncoding(sensor_data=i)
            for i in range(num_triggers)]
        actual_entries = [
            [SCSMemoryEncoding(sensor_data=i), i * 200]
            for i in range(num_triggers)]

        error_indexes = [0, 2]
        error_data_value = [2, 0]
        actual_entries[error_indexes[0]][0].sensor_data = error_data_value[0]
        actual_entries[error_indexes[1]][0].sensor_data = error_data_value[1]

        self.test_class.number_of_triggers = \
            Mock(return_value=num_triggers)
        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.scs.read_scopeshot_entries_from_memory = \
            Mock(return_value=actual_entries)

        self.test_class.DirectedScopeshotTriggerDataFlowByLinkTest()

        for i in range(len(error_indexes)):
            address = error_indexes[i] * self.scs.DDR4_ENTRY_BYTE_SIZE
            expected_data = expected_entries[error_indexes[i]]
            actual_data = actual_entries[error_indexes[i]][0]

            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0, address=address, sent=expected_data,
                    received=actual_data),
                'error')

    def test_time_stamp_mismatch_fail(self):
        num_triggers = 3
        expected_entries = [SCSMemoryEncoding(sensor_data=i)
                            for i in range(num_triggers)]
        expected_stamps = [[i*200, (i*200) + 100]
                           for i in range(num_triggers)]
        actual_entries = [[SCSMemoryEncoding(sensor_data=i), i * 200]
                          for i in range(num_triggers)]

        error_index = 2
        error_stamp_value = 1000
        actual_entries[error_index][1] = error_stamp_value

        self.test_class.number_of_triggers = \
            Mock(return_value=num_triggers)
        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.scs.read_scopeshot_entries_from_memory = \
            Mock(return_value=actual_entries)

        self.test_class.DirectedScopeshotTriggerDataFlowByLinkTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_time_stamp(
                iteration=None,
                link_num=0,
                address= error_index * self.scs.DDR4_ENTRY_BYTE_SIZE,
                min_stamp=expected_stamps[error_index][0],
                max_stamp=expected_stamps[error_index][1],
                actual_stamp=error_stamp_value),
            'error')

    def test_latest_address_after_transmitting_all_triggers_fail(self):
        num_triggers = 3
        expected_address = (self.scs.DDR4_ENTRY_BYTE_SIZE * num_triggers) - \
            self.scs.DDR4_ENTRY_BYTE_SIZE
        actual_address = 0

        self.scs.ddr4_latest_address = Mock(return_value=actual_address)
        self.test_class.number_of_triggers = Mock(return_value=3)

        self.test_class.DirectedScopeshotTriggerDataFlowByLinkTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_latest_address(
                iteration=0, expected=expected_address, actual=actual_address),
            'error')


class DirectedCircularBufferWriteReadExhaustiveTestUnitTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = ScopeshotEngineDiagnostics()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.call_args_list = self.test_class.Log.call_args_list
        self.test_class.test_iterations = 1
        self.test_class.max_fail_count = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_pass(self):
        self.test_class.DirectedCircularBufferWriteReadExhaustiveTest()
        self.validate_pass_message(self.test_class)

    def test_default_starting_ending_address_fail(self):
        default_addresses = [self.test_class.scs.DDR4_STARTING_ADDRESS_DEFAULT,
                             self.test_class.scs.DDR4_ENDING_ADDRESS_DEFAULT]

        with patch.object(self.test_class.scs, 'ddr4_starting_address') as \
            mock_default:
            actual_default = default_addresses[0] + 10
            mock_default.return_value = actual_default
            self.test_class.DirectedCircularBufferWriteReadExhaustiveTest()
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_at_default_address(
                    iteration=0,
                    expected=default_addresses[0],
                    starting_address=actual_default),
                'error'
            )

        with patch.object(self.test_class.scs, 'ddr4_ending_address') as \
            mock_default:
            actual_default = default_addresses[1] - 10
            mock_default.return_value = actual_default
            self.test_class.DirectedCircularBufferWriteReadExhaustiveTest()
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_at_default_address(
                    iteration=0,
                    expected=default_addresses[1],
                    ending_address=actual_default),
                'error'
            )

    def test_latest_address_against_expected_fail(
            self):
        expected_address = 0x100
        actual_address = 0x200
        self.test_class.scs.ddr4_starting_address = \
            Mock(return_value=expected_address)
        self.test_class.scs.ddr4_latest_address = Mock(
            return_value=actual_address)

        self.test_class.DirectedCircularBufferWriteReadExhaustiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_latest_address(
                iteration=0, expected=expected_address, actual=actual_address),
            'error'
        )

    def test_ddr4_data_mismatch_fail(self):
        expected_data = SCSMemoryEncoding(value=123)
        actual_data = SCSMemoryEncoding(value=321)

        self.test_class.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=[expected_data for i in range(16)])
        self.test_class.scs.read_scopeshot_entries_from_memory = \
            Mock(return_value=(actual_data, 0))

        self.test_class.DirectedCircularBufferWriteReadExhaustiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_sensor_data(
                iteration=0, address=0, sent=expected_data,
                received=actual_data),
            'error')

    def test_time_stamp_mismatch_fail(self):
        expected_stamp = 100
        actual_stamp = 900

        self.test_class.rc.live_time_stamp = Mock(return_value=expected_stamp)
        self.test_class.scs.read_scopeshot_entries_from_memory = \
            Mock(return_value=(SCSMemoryEncoding(), actual_stamp))

        self.test_class.DirectedCircularBufferWriteReadExhaustiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_time_stamp(
                iteration=0, link_num=None, address=0, min_stamp=expected_stamp,
                max_stamp=expected_stamp, actual_stamp=actual_stamp),
            'error')


class DirectedScopeshotStartStopTestUnitTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = ScopeshotEngineFunctional()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.call_args_list = self.test_class.Log.call_args_list
        self.test_class.test_iterations = 1
        self.test_class.max_fail_count = 1
        self.empty_hddps_trigger_buffers_copy = \
            hdmt_trigger_interface.empty_hddps_trigger_buffers
        self.scs = self.test_class.scs

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')
        hdmt_trigger_interface.empty_hddps_trigger_buffers = \
            self.empty_hddps_trigger_buffers_copy

    def test_pass(self):
        self.test_class.DirectedScopeshotStartStopUsingEventTriggerTest()
        self.validate_pass_message(self.test_class)

        self.test_class.DirectedScopeshotStartStopUsingBarRegisterTest()
        self.validate_pass_message(self.test_class)

    def test_default_starting_address_fail(self):
        self.scs.ddr4_starting_address = \
            Mock(return_value=self.scs.DDR4_ENDING_ADDRESS_DEFAULT)

        self.test_class.DirectedScopeshotStartStopUsingEventTriggerTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_STARTING_ADDRESS_DEFAULT,
                starting_address=self.scs.DDR4_ENDING_ADDRESS_DEFAULT),
            'error')
        self.assert_log_message_substring_not_found(self.call_args_list,
                                                    'ending_address')

        self.test_class.DirectedScopeshotStartStopUsingBarRegisterTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_STARTING_ADDRESS_DEFAULT,
                starting_address=self.scs.DDR4_ENDING_ADDRESS_DEFAULT),
            'error')
        self.assert_log_message_substring_not_found(self.call_args_list,
                                                    'ending_address')

    def test_default_ending_address_fail(self):
        self.scs.ddr4_ending_address = \
            Mock(return_value=self.scs.DDR4_STARTING_ADDRESS_DEFAULT)

        self.test_class.DirectedScopeshotStartStopUsingEventTriggerTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_ENDING_ADDRESS_DEFAULT,
                ending_address=self.scs.DDR4_STARTING_ADDRESS_DEFAULT),
            'error')
        self.assert_log_message_substring_not_found(self.call_args_list,
                                                    'startinging_address')

        self.test_class.DirectedScopeshotStartStopUsingBarRegisterTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_ENDING_ADDRESS_DEFAULT,
                ending_address=self.scs.DDR4_STARTING_ADDRESS_DEFAULT),
            'error')
        self.assert_log_message_substring_not_found(self.call_args_list,
                                                    'startinging_address')

    def test_default_starting_ending_address_fail(self):
        self.scs.ddr4_starting_address = \
            Mock(return_value=self.scs.DDR4_ENDING_ADDRESS_DEFAULT)
        self.scs.ddr4_ending_address = \
            Mock(return_value=self.scs.DDR4_STARTING_ADDRESS_DEFAULT)

        self.test_class.DirectedScopeshotStartStopUsingEventTriggerTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_STARTING_ADDRESS_DEFAULT,
                starting_address=self.scs.DDR4_ENDING_ADDRESS_DEFAULT),
            'error')
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_ENDING_ADDRESS_DEFAULT,
                ending_address=self.scs.DDR4_STARTING_ADDRESS_DEFAULT),
            'error')

        self.test_class.DirectedScopeshotStartStopUsingBarRegisterTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_STARTING_ADDRESS_DEFAULT,
                starting_address=self.scs.DDR4_ENDING_ADDRESS_DEFAULT),
            'error')
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_at_default_address(
                iteration=0, expected=self.scs.DDR4_ENDING_ADDRESS_DEFAULT,
                ending_address=self.scs.DDR4_STARTING_ADDRESS_DEFAULT),
            'error')

    def test_ddr4_data_mismatch_after_stop_command_fail(self):
        num_triggers = 10
        expected_entries = [SCSMemoryEncoding(sensor_data=0xBEEF)
                            for i in range(num_triggers // 2)]
        expected_entries.extend([SCSMemoryEncoding(value=0)
                                 for i in range(num_triggers // 2)])
        actual_entries = [[SCSMemoryEncoding(sensor_data=i), i * 200]
                          for i in range(num_triggers)]

        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.test_class.get_start_stop_ddr4_entries = \
            Mock(return_value=actual_entries)

        self.test_class.DirectedScopeshotStartStopUsingEventTriggerTest()
        address = self.scs.ddr4_starting_address()
        for i in range(num_triggers // 2, num_triggers):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0,
                    address=address + (i * self.scs.DDR4_ENTRY_BYTE_SIZE),
                    index=i,
                    sent=expected_entries[i],
                    received=actual_entries[i][0]),
                'error')

        self.test_class.DirectedScopeshotStartStopUsingBarRegisterTest()
        address = self.scs.ddr4_starting_address()
        for i in range(num_triggers // 2, num_triggers):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0,
                    address=address + (i * self.scs.DDR4_ENTRY_BYTE_SIZE),
                    index=i,
                    sent=expected_entries[i],
                    received=actual_entries[i][0]),
                'error')

    def test_ddr4_data_mismatch_without_start_command_fail(self):
        num_triggers = 10
        expected_entries = [SCSMemoryEncoding(sensor_data=0xBEEF)
                            for i in range(num_triggers // 2)]
        expected_entries.extend([SCSMemoryEncoding(value=0)
                                 for i in range(num_triggers // 2)])
        actual_entries = [[SCSMemoryEncoding(value=0), i * 200]
                          for i in range(num_triggers)]

        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.test_class.get_start_stop_ddr4_entries = \
            Mock(return_value=actual_entries)

        self.test_class.DirectedScopeshotStartStopUsingEventTriggerTest()
        address = self.scs.ddr4_starting_address()
        for i in range(0, num_triggers // 2):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0,
                    address=address + (i * self.scs.DDR4_ENTRY_BYTE_SIZE),
                    index=i,
                    sent=expected_entries[i],
                    received=actual_entries[i][0]),
                'error')

        self.test_class.DirectedScopeshotStartStopUsingBarRegisterTest()
        address = self.scs.ddr4_starting_address()
        for i in range(0, num_triggers // 2):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0,
                    address=address + (i * self.scs.DDR4_ENTRY_BYTE_SIZE),
                    index=i,
                    sent=expected_entries[i],
                    received=actual_entries[i][0]),
                'error')


class DirectedScopeshotEnableAndActiveTestUnitTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = ScopeshotEngineFunctional()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.call_args_list = self.test_class.Log.call_args_list
        self.test_class.test_iterations = 1
        self.test_class.max_fail_count = 1
        self.scs = self.test_class.scs
        self.test_class.num_triggers = 16  # Must match what is used in test

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_pass(self):
        self.test_class.DirectedScopeshotEnableAndActiveTest()
        self.validate_pass_message(self.test_class)
        self.assert_log_error_not_found(self.call_args_list)

    def test_active_status_deasserted_while_enabled_fail(self):
        self.scs.wait_on_active = Mock()
        self.scs.is_active = Mock(return_value=False)
        self.test_class.DirectedScopeshotEnableAndActiveTest()
        for trigger_num in range(0, self.test_class.num_triggers // 2):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_is_active(
                    iteration=0, trigger_num=trigger_num,
                    expected=True, actual=False),
                'error')

    def test_ddr4_data_mismatch_while_enabled_fail(self):
        num_triggers = self.test_class.num_triggers
        expected_entries = [SCSMemoryEncoding(sensor_data=i)
                            for i in range(num_triggers)]
        self.actual_entries = [[SCSMemoryEncoding(sensor_data=i), i * 200]
                               for i in range(num_triggers)]

        error_index = 2
        self.actual_entries[error_index][0].sensor_data = 0x1A5

        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.scs.read_scopeshot_entries_from_memory = \
            Mock(side_effect=self.mock_read_scopeshot_entries_from_memory)

        self.test_class.DirectedScopeshotEnableAndActiveTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_sensor_data(
                iteration=0,
                address=error_index * self.scs.DDR4_ENTRY_BYTE_SIZE,
                index=error_index,
                sent=expected_entries[error_index],
                received=self.actual_entries[error_index][0]),
            'error')

    def test_active_status_asserted_while_disabled_fail(self):
        self.scs.wait_on_active = Mock()
        self.scs.is_active = Mock(return_value=True)
        self.test_class.DirectedScopeshotEnableAndActiveTest()
        for trigger_num in range(self.test_class.num_triggers // 2,
                       self.test_class.num_triggers):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_is_active(
                    iteration=0, trigger_num=trigger_num,
                    expected=False, actual=True),
                'error')

    def test_ddr4_data_mismatch_while_disabled_fail(self):
        num_triggers = self.test_class.num_triggers
        expected_entries = [SCSMemoryEncoding(sensor_data=i)
                            for i in range(num_triggers)]
        self.actual_entries = [[SCSMemoryEncoding(sensor_data=i), i * 200]
                               for i in range(num_triggers)]

        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.scs.read_scopeshot_entries_from_memory = \
            Mock(side_effect=self.mock_read_scopeshot_entries_from_memory)

        self.test_class.DirectedScopeshotEnableAndActiveTest()
        for trigger_num in range(self.test_class.num_triggers // 2,
                                 self.test_class.num_triggers):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0,
                    address=trigger_num * self.scs.DDR4_ENTRY_BYTE_SIZE,
                    index=trigger_num,
                    sent=SCSMemoryEncoding(value=0),
                    received=self.actual_entries[trigger_num][0]),
                'error')

    def mock_read_scopeshot_entries_from_memory(self, address, num_entries=1):
        index = address // self.scs.DDR4_ENTRY_BYTE_SIZE
        return self.actual_entries[index]

class DirectedSampleCountTestUnitTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = ScopeshotEngineFunctional()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.call_args_list = self.test_class.Log.call_args_list
        self.test_class.test_iterations = 1
        self.test_class.max_fail_count = 1
        self.scs = self.test_class.scs
        self.test_class.num_triggers = 10  # Must match what is used in test

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'warning', 'error')

    def test_pass(self):
        self.test_class.DirectedSampleCountTest()
        self.validate_pass_message(self.test_class)
        self.assert_log_error_not_found(self.call_args_list)

    def test_set_sample_count_fail(self):
        num_triggers = self.test_class.num_triggers
        num_extra_triggers = 1
        self.test_class.total_triggers_with_sample_count = \
            Mock(return_value=[num_triggers + num_extra_triggers])
        expected_count = num_extra_triggers * 15  # 15 is taken from test
        actual_count = 0

        self.scs.sample_count = Mock(return_value=actual_count)

        self.test_class.DirectedSampleCountTest()
        self.validate_log_message(
            self.call_args_list,
            self.test_class.error_msg_sample_count(
                iteration=0, extra_count=num_extra_triggers,
                expected=expected_count, actual=actual_count),
            'error')

    def test_ddr4_data_mismatch_before_stop_command_fail(self):
        num_triggers = self.test_class.num_triggers
        num_extra_triggers = 1
        delay_times_us = self.scs.SAMPLE_COUNT_DELAY_TIME_US
        self.test_class.total_triggers_with_sample_count = \
            Mock(return_value=[num_triggers + num_extra_triggers])
        num_extra_entries = num_triggers + \
                            (num_extra_triggers * delay_times_us[1]) + 1
        self.test_class.max_fail_count = num_extra_entries
        expected_entries = [SCSMemoryEncoding(sensor_data=123)
                            for i in range(num_extra_entries)]
        actual_entries = [[SCSMemoryEncoding(sensor_data=123), i * 200]
                          for i in range(num_extra_entries)]

        for i in range(num_triggers):
            actual_entries[i][0].sensor_data = 111

        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.scs.read_scopeshot_entries_from_memory = \
            Mock(return_value=actual_entries)

        self.test_class.DirectedSampleCountTest()
        for i in range(num_triggers):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0, address=i * self.scs.DDR4_ENTRY_BYTE_SIZE,
                    index=i,
                    sent=expected_entries[i],
                    received=actual_entries[i][0],
                    extra_count=num_extra_triggers),
                'error')

    def test_ddr4_data_mismatch_between_stop_command_and_sample_count_fail(self):
        num_triggers = self.test_class.num_triggers
        num_extra_triggers = 5
        delay_times_us = self.scs.SAMPLE_COUNT_DELAY_TIME_US
        self.test_class.total_triggers_with_sample_count = \
            Mock(return_value=[num_triggers + num_extra_triggers])
        num_extra_entries = num_triggers + \
                            (num_extra_triggers * delay_times_us[1]) + 1
        self.test_class.max_fail_count = num_extra_entries
        expected_entries = [SCSMemoryEncoding(sensor_data=123)
                            for i in range(num_extra_entries)]
        actual_entries = [[SCSMemoryEncoding(sensor_data=123), i * 200]
                          for i in range(num_extra_entries)]

        for i in range(num_triggers, num_triggers + num_extra_triggers):
            actual_entries[i][0].sensor_data = 111

        self.scs.generate_random_scs_ddr4_data = \
            Mock(return_value=expected_entries)
        self.scs.read_scopeshot_entries_from_memory = \
            Mock(return_value=actual_entries)

        self.test_class.DirectedSampleCountTest()
        for i in range(num_triggers, num_triggers + num_extra_triggers):
            self.validate_log_message(
                self.call_args_list,
                self.test_class.error_msg_sensor_data(
                    iteration=0, address=i * self.scs.DDR4_ENTRY_BYTE_SIZE,
                    index=i,
                    sent=expected_entries[i],
                    received=actual_entries[i][0],
                    extra_count=num_extra_triggers),
                'error')

    def test_sample_count_tolerance_warning(self):
        num_triggers = 10  # taken from test
        expected_ddr4_entries = [SCSMemoryEncoding(sensor_data=0x123)
                                 for i in range(num_triggers)]
        sample_count_num_triggers = 20
        max_num_tolerance_triggers = \
            int(sample_count_num_triggers /
                (1 + self.scs.SAMPLE_COUNT_TOLERANCE))
        tolerance_triggers_count = [max_num_tolerance_triggers + 2,
                              max_num_tolerance_triggers,
                              max_num_tolerance_triggers - 2]

        for count in tolerance_triggers_count:
            sample_count_ddr4_entries = [
                SCSMemoryEncoding(sensor_data=0x321)
                for i in range(count)]
            self.scs.generate_random_scs_ddr4_data = \
                Mock(return_value=expected_ddr4_entries +
                                  sample_count_ddr4_entries +
                                  [SCSMemoryEncoding(value=0)])
            self.test_class.total_triggers_with_sample_count = \
                Mock(return_value=[num_triggers + sample_count_num_triggers])

            self.test_class.DirectedSampleCountTest()

            if count >= max_num_tolerance_triggers:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_class.warn_msg_extra_count(
                        iteration=0,
                        expected=sample_count_num_triggers,
                        actual=count + 1),
                    'warning')
            else:
                self.validate_log_message(
                    self.call_args_list,
                    self.test_class.error_msg_extra_count(
                        iteration=0,
                        expected=sample_count_num_triggers,
                        actual=count + 1),
                    'error')


class Miscellaneous(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.test_class = ScopeshotEngineFunctional()
        self.test_class.setUp(tester=self.tester)
        self.test_class.Log = Mock()
        self.call_args_list = self.test_class.Log.call_args_list
        self.test_class.test_iterations = 1
        self.test_class.max_fail_count = 1
        self.scs = self.test_class.scs
        self.orig_empty_hddps_trigger_buffers =\
            hdmt_trigger_interface.empty_hddps_trigger_buffers
        hdmt_trigger_interface.empty_hddps_trigger_buffers = Mock()

    def tearDown(self):
        hdmt_trigger_interface.empty_hddps_trigger_buffers = \
            self.orig_empty_hddps_trigger_buffers
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_clear_ddr4_entries(self):
        starting_address = [0, self.scs.MAX_NUM_DATA_ENTRIES // 2]
        num_entries = 1000
        for sa in starting_address:
            self.scs.set_ddr4_starting_address(sa)
            self.test_class.clear_ddr4_entries(num_entries)
            end_address = sa + (num_entries * self.scs.DDR4_ENTRY_BYTE_SIZE)
            zero_bytes = bytes(self.scs.DDR4_ENTRY_BYTE_SIZE)
            ones = b'\xff' * self.scs.DDR4_ENTRY_BYTE_SIZE
            self.test_class.rc.dma_write(end_address, ones)
            for i in range(sa, end_address, self.scs.DDR4_ENTRY_BYTE_SIZE):
                self.assertTrue(zero_bytes == self.test_class.rc.dma_read(
                    sa, self.scs.DDR4_ENTRY_BYTE_SIZE))
            self.assertTrue(ones == self.test_class.rc.dma_read(
                end_address, self.scs.DDR4_ENTRY_BYTE_SIZE))

    def test_initiate_scopseshot_timeout_assertion(self):
        from Rc3.instrument.scs_interface import ScsScopeshotInterface
        temp = ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT
        ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT = 10

        for use_event_trigger in [True, False]:
            # Test against enable mocking
            with self.assertRaises(ScsScopeshotInterface.TimeoutError):
                scs = ScsScopeshotInterface()
                scs.enable = Mock()
                scs.initiate_scopeshot(use_event_trigger)

        for use_event_trigger in [True, False]:
            # Test against status mocking
            with self.assertRaises(ScsScopeshotInterface.TimeoutError):
                scs = ScsScopeshotInterface()
                scs.is_active = Mock(return_value=False)
                scs.initiate_scopeshot(use_event_trigger)

        ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT = temp

    def test_terminate_scopeshot_timeout_assertion(self):
        from Rc3.instrument.scs_interface import ScsScopeshotInterface
        temp = ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT
        ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT = 10

        for use_event_trigger in [True, False]:
            with self.assertRaises(ScsScopeshotInterface.TimeoutError):
                scs = ScsScopeshotInterface()
                scs.is_active = Mock()
                scs.terminate_scopeshot(use_event_trigger)

        ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT = temp

    def test_terminate_scopeshot_default_setting(self):
        for use_event_trigger in [True, False]:
            self.scs.set_ddr4_starting_address(
                self.scs.DDR4_STARTING_ADDRESS_DEFAULT * 2)
            self.assertTrue(self.scs.DDR4_STARTING_ADDRESS_DEFAULT * 2 ==
                            self.scs.ddr4_starting_address())

            self.scs.set_ddr4_ending_address(
                self.scs.DDR4_ENDING_ADDRESS_DEFAULT // 2)
            self.assertTrue(self.scs.DDR4_ENDING_ADDRESS_DEFAULT // 2 ==
                            self.scs.ddr4_ending_address())

            self.scs.set_sample_count(self.scs.SAMPLE_COUNT_DEFAULT * 2)
            self.assertTrue(self.scs.SAMPLE_COUNT_DEFAULT * 2 ==
                            self.scs.sample_count())

            self.scs.terminate_scopeshot(use_event_trigger)
            self.assertTrue(self.scs.DDR4_STARTING_ADDRESS_DEFAULT ==
                            self.scs.ddr4_starting_address())
            self.assertTrue(self.scs.DDR4_ENDING_ADDRESS_DEFAULT ==
                            self.scs.ddr4_ending_address())
            self.assertTrue(self.scs.SAMPLE_COUNT_DEFAULT ==
                            self.scs.sample_count())

    def test_convert_ddr4_data_to_trigger(self):
        from random import getrandbits
        from Common.triggers import SCSTriggerEncoding

        values = [getrandbits(32) in range(10)]
        for value in values:
            entry = SCSMemoryEncoding(value=value)
            trigger = SCSTriggerEncoding(
                value=self.scs.convert_ddr4_data_to_trigger(entry))
            self.assertTrue(trigger.sensor_data == entry.sensor_data)
            self.assertTrue(trigger.user_id == entry.user_id)
            self.assertTrue(trigger.pin_id == entry.pin_id)
            self.assertTrue(trigger.ac_slice_id == entry.ac_slice_id)
            self.assertTrue(trigger.card_type ==
                            hdmt_trigger_interface.CardType.SCS.value)
