# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from random import getrandbits
import time
from unittest.mock import Mock

from Common.instruments.s2h_interface import S2hBDHeaderId, S2hInterface
from Rc3.Tests.BulkDataS2hStreaming import Functional
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest

time.sleep = lambda x: None


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.stream = self.functional.test_case.stream

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list,
                                'info',
                                'error')

    def test_DirectedValidatePacketHeadersTest_pass(self):
        self.functional.DirectedValidatePacketHeadersTest()
        self.validate_pass_message(self.functional)

    def test_DirectedValidatePacketHeadersTest_signature_fail(self):
        self.packet_counter = 0
        self.stream.stream_packet = self._create_invalid_signature_mock
        self.functional.DirectedValidatePacketHeadersTest()
        self.validate_fail_message(self.functional)

    def _create_invalid_signature_mock(self, index):
        self.packet_counter += 1
        invalid_signature = 0
        valid_version = S2hBDHeaderId.bulk_packet_version.value
        return S2hInterface.BDStreamPacket(signature=invalid_signature,
                                           bulk_packet_version=valid_version,
                                           packet_counter=self.packet_counter)

    def test_DirectedValidatePacketHeadersTest_bulk_packet_version_fail(self):
        valid_version = S2hBDHeaderId.bulk_packet_version.value
        invalid_version = 5
        expected_fail_count = 2
        self.functional.set_test_iterations = Mock(return_value=10)
        self.bulk_packet_list =\
            [invalid_version if (i == 1 or i == 9) else valid_version for i in
             range(self.stream.packet_count())]
        self.packet_counter = -1
        self.stream.stream_packet = \
            self._create_invalid_bulk_packet_version_mock

        self.functional.DirectedValidatePacketHeadersTest()

        self.assertEqual(self.functional.fail_count, expected_fail_count)
        self.validate_fail_message(self.functional)

    def _create_invalid_bulk_packet_version_mock(self, index):
        self.packet_counter += 1
        valid_signature = S2hBDHeaderId.packet_signature.value
        return S2hInterface.BDStreamPacket(
            packet_signature=valid_signature,
            bulk_packet_version=self.bulk_packet_list.pop(0),
            packet_counter=self.packet_counter)

    def test_DirectedEnableStreamTest_pass(self):
        self.functional.DirectedEnableStreamTest()
        self.validate_pass_message(self.functional)

    def test_DirectedEnableStreamTest_Insert_Errors_fail(self):
        self.stream.wait_on_valid_valid_packet_index = Mock(
            return_value=-1)

        self.stream.wait_on_matching_valid_packet_index = Mock(
            return_value=S2hInterface.PACKET_REFRESH_MS)

        error_indexes = [3, 5]
        self.valid_packet_index_list =\
            [S2hInterface.INVALID_PACKET_INDEX
             if (i == error_indexes[0] or i == error_indexes[1]) else i
             for i in range(0, self.functional.test_iterations)]

        self.stream.valid_packet_index = \
            self.valid_packet_index_mock

        self.functional.DirectedEnableStreamTest()

        for index in error_indexes:
            error_msg = self.functional.test_case.error_msg_enable_stream(
                iteration=index, current=S2hInterface.INVALID_PACKET_INDEX,
                previous=index-1)
            self.validate_log_message(
                call_args_list=self.functional.Log.call_args_list,
                expected_message=error_msg,
                expected_level='error')

        self.assertEqual(self.functional.fail_count, len(error_indexes))
        self.validate_fail_message(self.functional)

    def valid_packet_index_mock(self):
        return self.valid_packet_index_list.pop(0)

    def test_DirectedDisableStreamTest_pass(self):
        self.functional.DirectedDisableStreamTest()
        self.validate_pass_message(self.functional)

    def test_DirectedDisableStreamTest_Check_Max_Fail_Count_fail(self):
        self.valid_packet_index_list = [i for i in range(
            self.functional.test_iterations * 2)]

        self.stream.valid_packet_index =\
            self.valid_packet_index_mock

        self.stream.reset_stream = Mock()
        self.stream.disable_stream = Mock()
        self.stream.enable_stream = Mock()
        self.stream.wait_on_valid_valid_packet_index = Mock()

        self.functional.DirectedDisableStreamTest()

        for index in range(0, self.functional.max_fail_count * 2, 2):
            current_index = index
            next_index = index + 1

            error_msg = self.functional.test_case.error_msg_disable_stream(
                iteration=0,
                current_index=current_index,
                next_index=next_index)

            self.validate_log_message(
                call_args_list=self.functional.Log.call_args_list,
                expected_message=error_msg,
                expected_level='error')

        self.assertEqual(self.functional.fail_count,
                         self.functional.max_fail_count)

        self.validate_fail_message(self.functional)

    def test_DirectedResetStreamTest_pass(self):
        self.functional.DirectedResetStreamTest()
        self.validate_pass_message(self.functional)

    def test_DirectedResetStreamTest_fail(self):
        self.stream.wait_on_valid_valid_packet_index = Mock()
        self.stream.enable_stream = Mock()
        self.stream.reset_stream = Mock()

        error_indexes = [3, 5]
        error_index_value = getrandbits(16) % S2hInterface.INVALID_PACKET_INDEX
        self.valid_packet_index_list = \
            [error_index_value
             if (i == error_indexes[0] or i == error_indexes[1])
             else S2hInterface.INVALID_PACKET_INDEX
             for i in range(0, self.functional.test_iterations)]

        self.stream.valid_packet_index = \
            self.valid_packet_index_mock

        self.functional.DirectedResetStreamTest()

        for index in error_indexes:
            error_msg = self.functional.test_case.error_msg_reset_stream(
                iteration=index, index=error_index_value)

            self.validate_log_message(
                call_args_list=self.functional.Log.call_args_list,
                expected_message=error_msg,
                expected_level='error')

        self.assertEqual(self.functional.fail_count, len(error_indexes))
        self.validate_fail_message(self.functional)
