# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
from unittest.mock import Mock, patch

from Rc3.instrument.rc3_register import SCRATCH_REG
from Rc3.Tests.ScratchRegister import Diagnostics
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticsTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 1

    def tearDown(self):
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info',
                                'error')

    def test_scratch_register_write_read_pass_scenario(self):
        self.diagnostics.DirectedWriteReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_scratch_register_write_read_fail_scenario(self):
        expected_value = 0xabcd
        actual_value = 0xffff

        self._run_scratch_register_write_read_test(expected_value,
                                                   actual_value)

        [self.validate_log_message(
            self.diagnostics.Log.call_args_list,
            self.diagnostics.scratch_write_read_error_msg(
                0,
                i,
                expected_value,
                actual_value),
            'error') for i in range(SCRATCH_REG.REGCOUNT)]
        self.validate_fail_message(self.diagnostics)

    def test_scratch_register_write_read_max_fail_scenario(self):
        self.diagnostics.test_iterations = 4
        self.diagnostics.max_fail_count = 2
        expected_value = 0xabcd
        actual_value = 0xffff

        self._run_scratch_register_write_read_test(expected_value,
                                                   actual_value)

        if self.diagnostics.max_fail_count != self.diagnostics.fail_count:
            self.fail(f'Total fail not equal to max fail count (max, total): '
                      f'{self.diagnostics.max_fail_count}, '
                      f'{self.diagnostics.fail_count}')
        self.validate_fail_message(self.diagnostics)

    def _run_scratch_register_write_read_test(self, expected_value,
                                              actual_value):
        with patch.object(random, 'randrange') as mock_Random:
            scratch_register = SCRATCH_REG(value=actual_value)
            mock_Random.return_value = expected_value
            self.diagnostics.rc.read_bar_register = Mock(
                return_value=scratch_register)

            self.diagnostics.DirectedWriteReadExhaustiveTest()
