# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
from unittest.mock import Mock

from Common.testbench.i2c_tester import tx_fifo_error_msg
from Rc3.instrument.generic_i2c_interface import GENERIC_GROUPS
from Rc3.instrument.rc3_register import RELAYS_GPIO_I2C_CONFIGURATION
from Rc3.Tests.GenericI2cInterface import Functional, tx_fifo_error_msg,\
    error_msg
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest
from unit_tests.Common.Tests.test_I2cController import \
    FunctionalTests as I2cFunctionalTests


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 2
        self.tests = I2cFunctionalTests()
        self.tests.Log = self.functional.Log = Mock()
        self.call_args_list = self.functional.Log.call_args_list

    def test_DirectedRelayGpioI2cConfigurationRegisterTest_pass(self):
        self.functional.DirectedRelayGpioI2cConfigurationRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedRelayGpioI2cConfigurationRegisterTest_fail(self):
        self.functional.test_iterations = 5
        self.functional.max_fail_count = 100
        observed, expected = self.get_unequal_values(2, 2**10)
        self.functional.read_relays_gpio_i2c_configuration_reg = \
            Mock(return_value=RELAYS_GPIO_I2C_CONFIGURATION(observed))
        self.functional.generate_random_value = Mock(return_value=expected)
        self.functional.DirectedRelayGpioI2cConfigurationRegisterTest()
        for iteration in range(self.functional.test_iterations):
            self.validate_log_message(self.call_args_list,
                error_msg(iteration, RELAYS_GPIO_I2C_CONFIGURATION, observed,
                          expected),
                'error')
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedRelayGpioI2cConfigurationRegisterTest_max_fail_count(self):
        self.functional.test_iterations = 5
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.generate_random_value = Mock(return_value=0x3ff)
        unexpected = random.getrandbits(8)
        self.functional.rc.read_relays_gpio_i2c_configuration_reg = \
            Mock(return_value=RELAYS_GPIO_I2C_CONFIGURATION(unexpected))
        self.functional.DirectedRelayGpioI2cConfigurationRegisterTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedExhaustiveWriteReadGenericRegistersTests_pass(self):
        self.functional.DirectedExhaustiveWriteReadGenericRegistersTest()
        self.validate_pass_message(self.functional)

    def test_DirectedExhaustiveWriteReadGenericRegistersTests_fail(self):
        self.functional.compare_data = Mock(return_value=False)
        self.functional.DirectedExhaustiveWriteReadGenericRegistersTest()
        self.validate_fail_message(self.functional)

    def test_DirectedTxFifoCountErrorBitCheckingTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedTxFifoCountErrorBitCheckingTest_fail(self):
        self.tests.tx_fifo_count_error_bit_test_valid_count_fail(
            self.functional.generic_interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

        self.functional.fail_count = 0
        self.tests.tx_fifo_count_error_bit_test_invalid_count_fail(
            self.functional.generic_interfaces,
            self.functional.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedTxFifoCountTest_pass(self):
        self.functional.test_iterations = 1
        self.tests.validate_i2c_test_pass(
            self.functional.DirectedTxFifoCountTest)

    def test_DirectedTxFifoCountTest_fail(self):
        self.tests.tx_fifo_count_test_fail(
            self.functional.generic_interfaces,
            self.functional.DirectedTxFifoCountTest)

    def test_DirectedResetTxFifoTest_pass(self):
        self.tests.validate_i2c_test_pass(self.functional.DirectedResetTxFifoTest)

    def test_DirectedResetTxFifoTest_fail(self):
        self.tests.reset_tx_fifo_test_fail(
        self.functional.generic_interfaces, self.functional.DirectedResetTxFifoTest)

    def get_unequal_values(self, number_of_values, max, min=0):
            return random.sample(range(min, max), number_of_values)
