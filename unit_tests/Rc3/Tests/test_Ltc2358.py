# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Rc3.instrument.ltc2358 import Ltc2358
from Rc3.testbench.ltc2358_simulator import Ltc2358 as Ltc2358Sim
from Rc3.Tests.Ltc2358ADC import Diagnostics, Functional
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.call_args_list = self.diagnostics.Log.call_args_list
        self.diagnostics.test_iterations = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedChannelDisableTest_pass(self):
        self.diagnostics.DirectedChannelDisableTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedChannelDisableTest_disabled_fail(self):
        expected = [0, 0, 0]  # data, channel, code
        actuals = [[0, 0, 0b111],  # Invalid code
                  [0, 8, 0],       # Invalid channel
                  [1, 0, 0]]       # Invalid data
        with patch.object(Ltc2358, 'channel_data') as mock_channel_data:
            for actual in actuals:
                mock_channel_data.return_value = actual
                self.diagnostics.DirectedChannelDisableTest()
                self.validate_log_message(
                    self.call_args_list,
                    self.diagnostics.error_msg_at_disable(
                        iteration=0, adc_num=0,
                        expected=expected,
                        actual=actual),
                    'error')
                self.validate_fail_message(self.diagnostics)

    def test_DirectedChannelDisableTest_enabled_fail(self):
        actual = [Ltc2358Sim.DEFAULT_RAW_DATA, 0, Ltc2358Sim.DEFAULT_CODE]
        with patch.object(Ltc2358, 'channel_data') as mock_channel_data:
            mock_channel_data.return_value = actual
            self.diagnostics.DirectedChannelDisableTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg_at_enable(
                iteration=0, adc_num=0, channel=0, actual=actual),
            'error')
        self.validate_fail_message(self.diagnostics)


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.call_args_list = self.functional.Log.call_args_list
        self.functional.test_iterations = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedSoftspanCodeRegisterWriteReadTest_pass(self):
        self.functional.DirectedSoftspanCodeRegisterWriteReadTest()
        self.validate_pass_message(self.functional)

    def test_DirectedSoftspanCodeRegisterWriteReadTest_softspan_fail(self):
        actual = 0b010
        with patch.object(Ltc2358, 'softspan_code') as mock_softspan_code:
            mock_softspan_code.return_value = actual
            self.functional.DirectedSoftspanCodeRegisterWriteReadTest()
        self.validate_fail_message(self.functional)
        for adc_num in range(Ltc2358.MAX_NUM_LTC2358):
            for channel in range(Ltc2358.MAX_NUM_CHANNELS):
                if channel != actual:
                    self.validate_log_message(
                        self.call_args_list,
                        self.functional.error_msg_softspan_read(
                            iteration=0, adc_num=adc_num, channel=channel,
                            expected=channel, actual=actual),
                        'error')

    def test_DirectedSoftspanCodeRegisterWriteReadTest_data_fail(self):
        # data, channel, code
        actuals = [[123, 1, 9],  # Invalid code
                   [123, 9, 1]]  # Invalid channel
        with patch.object(Ltc2358, 'channel_data') as mock_channel_data:
            for actual in actuals:
                mock_channel_data.return_value = actual
                self.functional.DirectedSoftspanCodeRegisterWriteReadTest()
                self.validate_fail_message(self.functional)
                self.validate_log_message(
                    self.call_args_list,
                    self.functional.error_msg_data_read(
                        iteration=0, adc_num=0, channel=1,
                        expected=[1, 0b001], actual=actual[1:len(actual)]),
                    'error')
