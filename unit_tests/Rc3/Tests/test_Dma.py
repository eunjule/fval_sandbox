# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import os
import random
from unittest.mock import Mock

from Rc3.Tests.Dma import Diagnostics, Functional
from Rc3.instrument.rc3_register import DDR_STATUS
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DmaDiagnosticsTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 1
        self.diagnostics.max_fail_count = 1
        self.diagnostics.chunk_size = 1 << 10

    def tearDown(self):
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info', 'error')

    def test_DirectedDMAWriteReadDDRTest_pass(self):
        self.diagnostics.DirectedDMAWriteReadDDRTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedDMAWriteReadDDRTest_calibration_fail(self):
        self.diagnostics.rc.ddr_is_ready = Mock(return_value=False)

        self.diagnostics.DirectedDMAWriteReadDDRTest()

        self.validate_log_message(self.diagnostics.Log.call_args_list,
                                  self.diagnostics.ddr_calibration_fail_msg(),
                                  'error')
        self.validate_fail_message(self.diagnostics)

    def test_DirectedDMAWriteReadDDRTest_data_mismatch_fail(self):
        self._dma_write_data = []
        self._dma_read_data = []
        self._dma_start_address = 0
        self.diagnostics.dma_write_read_transaction = \
            Mock(side_effect=self.mock_dma_write_read_transaction)

        self.diagnostics.DirectedDMAWriteReadDDRTest()

        self.validate_log_message(
            self.diagnostics.Log.call_args_list,
            self.diagnostics.dma_mismatch_msg(
                0,
                self._dma_start_address,
                self._dma_start_address + len(self._dma_write_data)),
            'error')
        self.validate_fail_message(self.diagnostics)

    def mock_dma_write_read_transaction(self, start_address, write_data):
        self._dma_start_address = start_address
        self._dma_write_data = write_data
        self._dma_read_data = write_data[1:len(write_data)-1]
        return self._dma_read_data

    def test_DirectedInitiateSuccessfulCalibrationTest_pass(self):
        self.diagnostics.DirectedInitiateSuccessfulCalibrationTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedInitiateSuccessfulCalibrationTest_fail(self):
        self.diagnostics.rc.ddr_status_register = Mock(return_value=DDR_STATUS(0))
        self.diagnostics.rc.is_status_calibration_after_wait = Mock(return_value=False)
        self.diagnostics.rc.is_ddr_reset_done_after_wait = Mock(return_value=True)
        self.diagnostics.DirectedInitiateSuccessfulCalibrationTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_max_fail_count(self.diagnostics)

    def test_DirectedInitiateSuccessfulCalibrationTest_max_fail_count(self):
        self.diagnostics.test_iterations = random.randint(5, 10)
        self.diagnostics.max_fail_count = self.diagnostics.test_iterations + 10
        self.diagnostics.rc.ddr_status_register = Mock(return_value=DDR_STATUS(0))
        self.diagnostics.rc.is_status_calibration_after_wait = Mock(return_value=False)
        self.diagnostics.rc.is_ddr_reset_done_after_wait = Mock(return_value=True)
        self.diagnostics.DirectedInitiateSuccessfulCalibrationTest()
        self.validate_max_fail_count(self.diagnostics)

    def test_DirectedInitiateSuccessfulCalibrationTest_reset_fail(self):
        self.diagnostics.rc.is_ddr_reset_done_after_wait = Mock(
            return_value=False)
        self.diagnostics.DirectedInitiateSuccessfulCalibrationTest()
        msg = f'Reset sequence for DDR4 Controller did NOT complete.'
        self.validate_log_message(self.diagnostics.Log.call_args_list, msg, 'error')
        self.validate_max_fail_count(self.diagnostics)

    def test_DirectedInitiateSuccessfulCalibrationTest_calibration_fail(self):
        self.diagnostics.rc.is_status_calibration_after_wait = Mock(return_value=False)
        self.diagnostics.DirectedInitiateSuccessfulCalibrationTest()
        msg = f'DDR Calibration NOT Successful'
        self.validate_log_message(self.diagnostics.Log.call_args_list, msg, 'error')
        self.validate_max_fail_count(self.diagnostics)

    def test_DirectedResetMemoryTest_pass(self):
        self.diagnostics.DirectedResetMemoryTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedResetMemoryTest_fail(self):
        random_data = os.urandom(self.diagnostics.chunk_size)
        self.diagnostics.rc.dma_read = Mock(return_value=random_data)
        self.diagnostics.DirectedResetMemoryTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_max_fail_count(self.diagnostics)

    def test_DirectedResetMemoryTest_reset_fail(self):
        self.diagnostics.check_ddr_reset = Mock(return_value=False)
        self.diagnostics.DirectedResetMemoryTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_max_fail_count(self.diagnostics)

    def test_DirectedResetMemoryTest_max_fail_count(self):
        self.diagnostics.test_iterations = random.randint(5, 10)
        self.diagnostics.max_fail_count = self.diagnostics.test_iterations + 10
        random_data = os.urandom(self.diagnostics.chunk_size)
        self.diagnostics.rc.dma_read = Mock(return_value=random_data)
        self.diagnostics.DirectedResetMemoryTest()
        self.validate_max_fail_count(self.diagnostics)


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 1
        self.functional.rc.is_status_calibration_after_wait = Mock(return_value=True)

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list, 'info',
                                'error')

    def test_DirectedWriteReadControlRegisterExhaustiveTest_pass(self):
        self.functional.DirectedWriteReadControlRegisterExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedWriteReadControlRegisterExhaustiveTest_fail(self):
        self.functional.rc.ddr_controller_reset = Mock(return_value=1)
        self.functional.DirectedWriteReadControlRegisterExhaustiveTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedWriteReadControlRegisterExhaustiveTest_max_fail_count(self):
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.rc.ddr_controller_reset = Mock(return_value=1)
        self.functional.DirectedWriteReadControlRegisterExhaustiveTest()
        self.validate_max_fail_count(self.functional)

    def test_DirectedWriteReadControlRegisterExhaustiveTest_controller_fail(self):
        self.functional.rc.is_status_calibration_after_wait = Mock(return_value=False)
        self.functional.DirectedWriteReadControlRegisterExhaustiveTest()
        msg = f'DDR Calibration NOT Successful'
        self.validate_log_message(self.functional.Log.call_args_list, msg, 'error')
