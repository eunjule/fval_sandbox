# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Rc3.instrument.rc3 import Rc3
from Rc3.Tests.TriggerSync import Functional

from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class FunctionalTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()
        self.functional = Functional(methodName='runTest')
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()

        self.functional.test_iterations = 2
        self.functional.max_fail_count = 2
        self.call_args_list = self.functional.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedTriggerSyncPulseTest_pass(self):
        self.functional.DirectedTriggerSyncPulseTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerSyncPulseTest_fail(self):
        self.functional.wait_on_pattern_complete = Mock(return_value=False)
        self.functional.DirectedTriggerSyncPulseTest()
        self.validate_fail_message(self.functional)
        msg = self.functional.error_msg_wait_on_pattern_complete(
            iteration=0,
            name=self.functional.hpccs[0].ac[0].name()
        )
        self.validate_log_message(
            call_args_list=self.call_args_list,
            expected_message=msg,
            expected_level='error'
        )

    def test_DirectedTriggerSyncModulusTest_pass(self):
        self.functional.DirectedTriggerSyncModulusTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerSyncModulusTest_fail(self):
        initial_modulus = self.functional.rc.trigger_sync_modulus()
        self.functional.wait_on_pattern_complete = Mock(return_value=False)
        self.functional.DirectedTriggerSyncModulusTest()
        self.validate_fail_message(self.functional)
        msg = self.functional.error_msg_wait_on_pattern_complete(
            iteration=0,
            name=self.functional.hpccs[0].ac[0].name()
        )
        self.validate_log_message(
            call_args_list=self.call_args_list,
            expected_message=msg,
            expected_level='error'
        )
        self.assertEqual(initial_modulus,
                         self.functional.rc.trigger_sync_modulus())

    def test_DirectedTimeStampResetTest_pass(self):
        self.functional.DirectedTimeStampResetTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTimeStampResetTest_increment_fail(self):
        self.functional._wait_on_timer_value = Mock(return_value=False)
        self.functional.DirectedTimeStampResetTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_time_stamp_increment(iteration=0),
            'error'
        )

    def test_DirectedTimeStampResetTest_clear_stamp_fail(self):
        invalid_time_stamp = 1000
        with patch.object(Rc3, 'live_time_stamp') as mock_live_time_stamp:
            mock_live_time_stamp.return_value = invalid_time_stamp
            self.functional.DirectedTimeStampResetTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_time_stamp_clear(
                iteration=0, time_stamp=invalid_time_stamp),
            'error'
        )

    def test_DirectedTriggerSyncModuluslWriteReadTest_pass(self):
        self.functional.DirectedTriggerSyncModuluslWriteReadTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerSyncModuluslWriteReadTest_power_on_fail(self):
        invalid_power_on = 1
        default_power_on = 0
        with patch.object(Rc3, 'trigger_sync_modulus') \
                as mock_trigger_sync_modulus:
            mock_trigger_sync_modulus.return_value = invalid_power_on
            self.functional.DirectedTriggerSyncModuluslWriteReadTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_modulus_wr_power_on(
                expected=default_power_on, actual=invalid_power_on),
            'error'
        )

    def test_DirectedTriggerSyncModuluslWriteReadTest_modulus_read_fail(self):
        invalid_modulus = 0
        self.functional.read_updated_trigger_sync_modulus = \
            Mock(return_value=invalid_modulus)
        self.functional.DirectedTriggerSyncModuluslWriteReadTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_modulus_wr(
                expected=invalid_modulus+1, actual=invalid_modulus),
            'error'
        )

    def test_DirectedTiuClockDividerlWriteReadTest_pass(self):
        self.functional.DirectedTiuClockDividerlWriteReadTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTiuClockDividerlWriteReadTest_power_on_fail(self):
        invalid_initial = 1
        default_divivder = 4
        with patch.object(Rc3, 'tiu_clock_divider') \
                as mock_tiu_clock_divider:
            mock_tiu_clock_divider.return_value = invalid_initial
            self.functional.DirectedTiuClockDividerlWriteReadTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_tiu_clock_divider_wr_initial(
                expected=default_divivder, actual=invalid_initial),
            'error'
        )

    def test_DirectedTiuClockDividerlWriteReadTest_divider_read_fail(self):
        invalid_divider = 0
        self.functional.read_updated_tiu_clock_divider = \
            Mock(return_value=invalid_divider)
        self.functional.DirectedTiuClockDividerlWriteReadTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_tiu_clock_divider_wr(
                expected=invalid_divider+1, actual=invalid_divider),
            'error'
        )
