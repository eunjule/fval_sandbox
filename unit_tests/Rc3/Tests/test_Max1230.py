# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Rc3.Tests.Max1230 import Diagnostics
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class TemperatureTests(Rc3UnitTest):
    def test_DirectedReadTemperatureExhaustiveTest_pass(self):
        diagnostics = self.get_diagnostics()
        diagnostics.DirectedReadTemperatureExhaustiveTest()
        for channel in range(2):
            channel_running = f'Starting sub-test with parameters: Max1230=Channel {channel}'
            self.check_for_message('info', channel_running, diagnostics.Log.call_args_list)

            hil_expected = 'Rc3: HIL Average temperature is 33.0.'
            self.check_for_message('info', hil_expected, diagnostics.Log.call_args_list)

            bar_expected = 'Rc3: BAR Average temperature is 33.0.'
            self.check_for_message('info', bar_expected, diagnostics.Log.call_args_list)

    def test_DirectedReadTemperatureExhaustiveTest_fail(self):
        diagnostics = self.get_diagnostics()
        diagnostics.rc.bar_write(1, 0x4E0, 100)
        diagnostics.rc.bar_write(1, 0x4E4, 100)
        diagnostics.DirectedReadTemperatureExhaustiveTest()
        for channel in range(2):
            channel_running = f'Starting sub-test with parameters: Max1230=Channel {channel}'
            self.check_for_message('info', channel_running, diagnostics.Log.call_args_list)

            hil_expected = f'Max1230 Temperature Channel {channel} Failed'
            self.check_for_message('error', hil_expected, diagnostics.Log.call_args_list)
        diagnostics.rc.bar_write(1, 0x4E0, 264)
        diagnostics.rc.bar_write(1, 0x4E4, 264)

    def test_DirectedReadVoltageExhaustiveTest_pass(self):
        diagnostics = self.get_diagnostics()
        diagnostics.DirectedReadVoltageExhaustiveTest()

        expected_log = f'Max1230 Voltages Passed'
        self.check_for_message('info', expected_log, diagnostics.Log.call_args_list)

    def test_DirectedReadVoltageExhaustiveTest_fail(self):
        diagnostics = self.get_diagnostics()
        diagnostics.rc.bar_write(1, 0x460, 3000)
        diagnostics.DirectedReadVoltageExhaustiveTest()

        expected_log = f'Max1230 Voltage Monitor Failures'
        self.check_for_message('error', expected_log, diagnostics.Log.call_args_list)
        diagnostics.rc.bar_write(1, 0x460, 3600)

    def get_diagnostics(self):
        diagnostics = Diagnostics()
        diagnostics.Log = self.tester.rc.Log = Mock()
        diagnostics.setUp(tester=self.tester)
        return diagnostics

    def check_for_message(self, level, message, calls):
        for args, kwargs in calls:
            log_level, log_message = args
            if level == log_level and message in log_message:
                break
        else:
            call_strings = '\n'.join([str(x) for x in calls])
            self.fail(f'"{level},{message}" not in\n{call_strings}')

