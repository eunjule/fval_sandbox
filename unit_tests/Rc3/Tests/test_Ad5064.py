# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Rc3.Tests.Ad5064 import Diagnostics
from Rc3.instrument.rc3 import Rc3
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    # NOTE: See Ad5064 test module for reason this is being skipped/commented
    # out.
    # def test_DirectedWriteToDACChannelTest_pass(self):
    #     self.diagnostics.DirectedWriteToDACChannelTest()
    #     self.validate_pass_message(self.diagnostics)
    #
    # def test_DirectedWriteToDACChannelTest_fail(self):
    #     self.error_voltage = [self.diagnostics.V_CNTL_VOLATGE, 0]
    #     with patch.object(Rc3, 'voltage') as mock_voltage:
    #         mock_voltage.side_effect = self._mock_voltage
    #         self.diagnostics.DirectedWriteToDACChannelTest()
    #
    #     self.validate_fail_message(self.diagnostics)
    #
    #     expected_voltage = (self.diagnostics.V_CNTL_VOLATGE -
    #                         self.diagnostics.V_CNTL_STEP)
    #     self.validate_log_message(self.call_args_list,
    #                               self.diagnostics.error_msg_voltage_step(
    #                                   iteration=0,
    #                                   expected=expected_voltage,
    #                                   actual=0),
    #                               'error')
    #
    # def _mock_voltage(self, channel):
    #     if len(self.error_voltage) == 0:
    #         return 0
    #     else:
    #         return self.error_voltage.pop(0)


    def test_DirectedVoutAReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedVoutAReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedVoutAReadExhaustiveTest_fail(self):
        actual_voltage = \
            self.diagnostics.V_CNTL_VOLATGE - self.diagnostics.COMPARE_DELTA
        expected_voltage = self.diagnostics.V_CNTL_VOLATGE
        with patch.object(Rc3, 'voltage') as mock_voltage:
            mock_voltage.return_value = actual_voltage
            self.diagnostics.DirectedVoutAReadExhaustiveTest()

        self.validate_fail_message(self.diagnostics)
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg_voltage_read(iteration=0,
                                                    expected=expected_voltage,
                                                    actual=actual_voltage),
            'error')
