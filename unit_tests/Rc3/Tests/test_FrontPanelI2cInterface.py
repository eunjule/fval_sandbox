# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Rc3.instrument.front_panel_i2c_interface import DEVICE_ID_VALUE
from Rc3.Tests.FrontPanelI2cInterface import Diagnostics, Functional
from unit_tests.Common.Tests.test_I2cController import \
    FunctionalTests as I2cFunctionalTests
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 10

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list, 'info',
                                'error')

    def test_DirectedReadExhaustiveTest_pass(self):
        self.functional.DirectedReadExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedReadExhaustiveTest_fail(self):
        invalid_value = DEVICE_ID_VALUE ^ 0xFF
        self.functional.fp[0].device_id = \
            Mock(return_value=DEVICE_ID_VALUE ^ 0xFF)
        self.functional.DirectedReadExhaustiveTest()

        for i in range(self.functional.max_fail_count):
            self.validate_log_message(
                self.functional.Log.call_args_list,
                self.functional.error_message(i,
                    expected_value=DEVICE_ID_VALUE,
                    actual_value=invalid_value),
                'error')

        self.validate_fail_message(self.functional)


class DiagnosticTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.test_iterations = 2

        self.tests = I2cFunctionalTests()
        self.tests.Log = self.diagnostics.Log = Mock()

    def tearDown(self):
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info',
                                'error')

    def test_DirectedTxFifoCountErrorBitCheckingTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.diagnostics.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedTxFifoCountErrorBitCheckingTest_fail(self):
        self.tests.tx_fifo_count_error_bit_test_valid_count_fail(
            self.diagnostics.fp,
            self.diagnostics.DirectedTxFifoCountErrorBitCheckingTest)

        self.diagnostics.fail_count = 0
        self.tests.tx_fifo_count_error_bit_test_invalid_count_fail(
            self.diagnostics.fp,
            self.diagnostics.DirectedTxFifoCountErrorBitCheckingTest)

    def test_DirectedAddressNakTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.diagnostics.DirectedAddressNakTest)

    def test_DirectedAddressNakTest_fail(self):
        self.tests.address_nak_test_valid_address_fail(
            self.diagnostics.fp,
            self.diagnostics.DirectedAddressNakTest)

        self.diagnostics.fail_count = 0
        self.tests.address_nak_test_invalid_address_fail(
            self.diagnostics.fp,
            self.diagnostics.DirectedAddressNakTest)

    def test_DirectedTxFifoCountTest_pass(self):
        self.tests.validate_i2c_test_pass(
            self.diagnostics.DirectedTxFifoCountTest)

    def test_DirectedTxFifoCountTest_fail(self):
        self.tests.tx_fifo_count_test_fail(
            self.diagnostics.fp, self.diagnostics.DirectedTxFifoCountTest)
