# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Rc3.Tests.ClockFrequencyMeasurement import FrequencyMeasurement
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class FrequencyMeasurementTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.frequency_measurement = FrequencyMeasurement()
        self.frequency_measurement.setUp(tester=self.tester)
        self.frequency_measurement.Log = Mock()
        self.call_args_list = self.frequency_measurement.Log.call_args_list
        self.frequency_measurement.test_iterations = 1
        self.frequency_measurement.max_fail_count = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedFrequencyReadTest_pass(self):
        self.frequency_measurement.DirectedFrequencyReadTest()
        self.validate_pass_message(self.frequency_measurement)

    def test_DirectedFrequencyReadTest_fail(self):
        self.actual_freqs = FrequencyMeasurement.EXPECTED_FREQS.copy()
        self.error_indexes = [0, 2]
        for i in self.error_indexes:
            self.actual_freqs[i] = 0
        self.frequency_measurement.rc.read_clock_frequency_mhz = \
            self._mock_read_clock_frequency_mhz

        self.frequency_measurement.DirectedFrequencyReadTest()

        for i in self.error_indexes:
            self.validate_log_message(
                self.call_args_list,
                self.frequency_measurement.frequency_mismtach_msg(
                    iteration=0,
                    clock_name=FrequencyMeasurement.Clocks(i).name,
                    expected=FrequencyMeasurement.EXPECTED_FREQS[i],
                    actual=self.actual_freqs[i]),
                'error')

    def _mock_read_clock_frequency_mhz(self, index):
        return self.actual_freqs[index]
