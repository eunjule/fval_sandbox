# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import random
import string
from unittest.mock import Mock, patch

from Common import configs
from Rc3.Tests.Traceability import Image, Chip
from Rc3.instrument.rc3_register import FPGA_VERSION, COMPATIBILITY
from Rc3.testbench.rc3_simulator import Rc3
from Rc3.instrument.rc3 import CompatibilityIndexError
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class TraceabilityImageTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()

        self.image = Image()
        self.image.setUp(tester=self.tester)
        self.image.Log = Mock()

        self.image.test_iterations = 1
        self.image.max_fail_count = 1

    def tearDown(self):
        self.print_log_messages(self.image.Log.call_args_list, 'info', 'error')

    def test_DirectedHgCleanBitTest_pass(self):
        self.image.DirectedHgCleanBitTest()
        self.validate_pass_message(self.image)

    def test_DirectedHgCleanBitTest_fail(self):
        self.image.rc.get_hg_clean_bit = Mock(return_value=0)

        self.image.DirectedHgCleanBitTest()
        self.validate_fail_message(self.image)

    def test_DirectedFpgaVersionTest_pass(self):
        self.image.DirectedFpgaVersionTest()
        self.validate_pass_message(self.image)

    def test_DirectedFpgaVersionTest_invalid_file_name_fail(self):
        self._update_fpga_version_with_config_version()
        config_names = [r'\\Fab_C\3.1.1\rctc3_top_v3_1_1.rbf',
                        r'\\Fab_C\3.1.1\rctc3_top_3.1.1.rbf',
                        r'\\Fab_C\3.1.1\rctc3_top_v3.1.rbf',
                        r'\\Fab_C\3.1.1\rctc3_top_v3.J.1.rbf',
                        r'\\Fab_C\3.1.1\rctc3_top_v3.1.1.sof',
                        fr'\\Fab_C\3.1.1\rctc3_top_v{0xFFFF + 1}.1.1.rbf',
                        fr'\\Fab_C\3.1.1\rctc3_top_v{-1}.1.1.rbf',
                        r'\\Fab_C\3.1.1\rctc3_top.rbf']

        orignal_fpga_image_path = self.image.rc.fpga_image_path
        for config_name in config_names:
            self.image.fail_count = 0
            self.image.rc.fpga_image_path = Mock(return_value=config_name)
            fpga_version_config = \
                self.image.get_version_number_from_config_file_name()

            self.image.DirectedFpgaVersionTest()
            self.validate_fail_message(self.image)
            self.validate_log_message(
                self.image.Log.call_args_list,
                self.image.error_msg(iteration=0,
                                     expected=fpga_version_config,
                                     actual=self.fpga_version),
                'error')
            self.image.rc.fpga_image_path.reset_mock()

        self.image.rc.fpga_image_path = orignal_fpga_image_path

    def test_DirectedFpgaVersionTest_invalid_version_number_fail(self):
        self._update_fpga_version_with_config_version()
        expected_version = Rc3(None).registers[FPGA_VERSION.ADDR]

        # increment major number
        # decrement major number
        # increment minor number
        # decrement minor number
        invalid_version_numbers = [expected_version + (1 << 16),
                                   expected_version - (1 << 16),
                                   expected_version + 1,
                                   expected_version - 1]

        original_get_fpga_version = self.image.rc.fpga_version
        for invalid_version_number in invalid_version_numbers:
            self.image.rc.fpga_version = \
                Mock(return_value=invalid_version_number)
            self.image.DirectedFpgaVersionTest()
            self.validate_fail_message(self.image)
            self.validate_log_message(
                self.image.Log.call_args_list,
                self.image.error_msg(iteration=0,
                                     expected=expected_version,
                                     actual=invalid_version_number),
                'error')

        self.image.rc.fpga_version = original_get_fpga_version

    def _update_fpga_version_with_config_version(self):
        self.fpga_version_config = \
            self.image.get_version_number_from_config_file_name()
        reg = FPGA_VERSION(major=self.fpga_version_config >> 16,
                           minor=self.fpga_version_config & 0xFFFF)
        self.fpga_version = reg.value
        self.image.rc.write_bar_register(reg)

    def test_DirectedHgIdTest_pass(self):
        self.image.DirectedHgIdTest()
        self.validate_pass_message(self.image)

    def test_DirectedHgIdTest_mismatch_fail(self):
        from_register = 0xAA
        from_file = 0x55
        self.image.rc.hg_id = Mock(return_value=from_register)
        self.image.rc.hg_id_from_file = Mock(return_value=from_file)
        self.image.DirectedHgIdTest()
        self.validate_log_message(
            self.image.Log.call_args_list,
            self.image.hg_id_error_msg(from_file, from_register),
            'error')

    def test_DirectedHgIdTest_iteration_fail(self):
        self.image.test_iterations = 1
        expected = self.image.rc.hg_id()
        actual = 0x0123 << 32 | 0x456789AB
        self.hg_id_values = [expected, actual]
        self.image.rc.hg_id = Mock(side_effect=self.mock_hg_id)
        self.image.DirectedHgIdTest()
        self.validate_fail_message(self.image)
        self.validate_log_message(
            self.image.Log.call_args_list,
            self.image.error_msg(
                iteration=0, expected=expected, actual=actual),
            'error')

    def mock_hg_id(self):
        return self.hg_id_values.pop(0)

    def test_DirectedFabCompatibilityTest_pass(self):
        self.image.DirectedFabCompatibilityTest()
        self.validate_pass_message(self.image)

    def test_DirectedFabCompatibilityTest_fail(self):
        letters = \
            string.ascii_letters.replace('abcdef','').replace('ABCDEF','')
        with patch.object(self.image.rc, 'compatibility') as \
                mock_compatibility:
            mock_compatibility.return_value = random.choice(letters)
            self.image.DirectedFabCompatibilityTest()
        self.validate_fail_message(self.image)

    def test_DirectedFabCompatibilityTest_config_fab_fail(self):
        with patch.object(self.image.rc, 'compatibility') as \
                mock_compatibility,\
                patch.object(self.image.rc, 'config_fab') as mock_config_fab:
            mock_compatibility.return_value = 'A'
            mock_config_fab.return_value = 'B'
            self.image.DirectedFabCompatibilityTest()
        self.validate_fail_message(self.image)

    def test_DirectedFabCompatibilityTest_config_fab_number_fail(self):
        with patch.object(self.image.rc, 'config_fab') as mock_config_fab:
            mock_config_fab.return_value = random.randint(0, 9)
            self.image.DirectedFabCompatibilityTest()
        self.validate_fail_message(self.image)

    def test_DirectedFabCompatibilityTest_compatability_fail(self):

        self.image.rc.read_bar_register = \
            Mock(return_value=COMPATIBILITY(0xF))

        with self.assertRaises(CompatibilityIndexError) as context:
            self.image.DirectedFabCompatibilityTest()

        self.assertTrue('Invalid Compatibility. Valid values are 0 '
                        'to 5 (a:f)' in str(context.exception))

    def test_DirectedFabCompatibilityTest_config_fab_path_number_fail(self):
        temp = configs.RC3_FPGA_IMAGE_FAB_A
        configs.RC3_FPGA_IMAGE_FAB_A = \
            f'\RCTC3\Release\Fab_{random.randint(0, 9)}\Dynamic_Load_Image'
        self.image.DirectedFabCompatibilityTest()
        configs.RC3_FPGA_IMAGE_FAB_A = temp
        self.validate_fail_message(self.image)


class TraceabilityChipTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()
        self.chip = Chip()
        self.chip.setUp(tester=self.tester)
        self.chip.Log = Mock()

    def tearDown(self):
        self.print_log_messages(self.chip.Log.call_args_list, 'info', 'error')

    def test_DirectedChipIdTest_pass(self):
        self.chip.DirectedChipIdTest()
        self.validate_pass_message(self.chip)

    def test_DirectedChipIdTest_invalid_values_fail(self):
        invalid_combos = [[0x0, 0x0],
                          [0xFFFFFFFF, 0xCA11F00D] ,
                          [0xCA11F00D, 0xFFFFFFFF]]
        upper_lower = random.choice(invalid_combos)

        self.chip.rc.chip_id = Mock(return_value=upper_lower)
        self.chip.DirectedChipIdTest()

        self.validate_fail_message(self.chip)
