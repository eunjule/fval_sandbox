# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
from unittest.mock import Mock, patch

from Common.fval import SkipTest
from Common.instruments.dps.hddps import Hddps
from Hpcc.instrument.hpcc import Hpcc
from Rc3.instrument.rc3 import Rc3
from Rc3.instrument.rc3_register import GENERAL_PURPOSE_INPUT,\
    GENERAL_PURPOSE_OUTPUT
from Rc3.Tests.Gpio import Functional, Diagnostics
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedGeneralPurposeOutputRegisterTest_pass(self):
        self.diagnostics.DirectedGeneralPurposeOutputRegisterTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedGeneralPurposeOutputRegisterTest_fail(self):
        expected = GENERAL_PURPOSE_OUTPUT(0x20_0000)
        unexpected = [GENERAL_PURPOSE_OUTPUT(0x123)
                      for i in range(self.diagnostics.test_iterations + 1)]
        self.mock_fifo = unexpected
        self.diagnostics.general_purpose_output_reg = \
            Mock(side_effect=self.general_purpose_reg_mock)
        self.diagnostics.DirectedGeneralPurposeOutputRegisterTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_max_fail_count(self.diagnostics)
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0,
                                       register=expected,
                                       observed=unexpected[0].value,
                                       expected=expected.value),
            'error')

    def test_DirectedGeneralPurposeInputRegisterTest_pass(self):
        self.diagnostics.DirectedGeneralPurposeInputRegisterTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedGeneralPurposeInputRegisterTest_fail(self):
        expected = GENERAL_PURPOSE_INPUT(0x123)
        unexpected = [GENERAL_PURPOSE_INPUT(random.getrandbits(32))
                      for i in range(self.diagnostics.test_iterations)]
        self.mock_fifo = [expected]
        self.mock_fifo.extend(unexpected)
        self.diagnostics.general_purpose_input_reg = \
            Mock(side_effect=self.general_purpose_reg_mock)
        self.diagnostics.DirectedGeneralPurposeInputRegisterTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_max_fail_count(self.diagnostics)
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0,
                                       register=expected,
                                       observed=unexpected[0].value,
                                       expected=expected.value),
            'error')

    def general_purpose_reg_mock(self):
        return self.mock_fifo.pop(0)


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        slots = [2, 11]
        self.tester.rc.aurora_link_slots = slots
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 2
        self.functional.max_fail_count = 1
        self.call_args_list = self.functional.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest_pass(
            self):
        self.functional.\
            DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest()
        self.validate_pass_message(self.functional)

    def test_DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest_fail(
            self):
        self.functional.rc.write_rc_fp_gpio_control = Mock()
        self.functional.\
            DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest_output_fail(self):
        self.functional.rc.write_rc_fp_gpio_data = Mock()
        self.functional.\
            DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)

    def test_DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest_input_fail(self):
        self.functional.rc.rc_fp_gpio_input = Mock(return_value=0b00)
        self.functional.\
            DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)

    def test_DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest_pass(
            self):
        self.functional.\
            DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest()
        self.validate_pass_message(self.functional)

    def test_DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest_fail(
            self):
        self.functional.rc.write_rc_fpga_spare_control= Mock()
        self.functional.\
            DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)

    def test_DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest_max_fail(
            self):
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.rc.write_rc_fpga_spare_control= Mock()
        self.functional.\
            DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest_output_fail(self):
        self.functional.rc.write_rc_fpga_spare_data = Mock()
        self.functional.\
            DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)

    def test_DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest_input_fail(self):
        self.functional.rc.rc_fpga_spare_input = Mock(return_value=0b00)
        self.functional.\
            DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest()
        self.validate_fail_message(self.functional)

    def test_DirectedPowerOnCountRegisterTest_pass(self):
        self.functional.rc.load_fpga()
        self.functional.DirectedPowerOnCountRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedPowerOnCountRegisterTest_fail(self):
        actual_count = 0xFFFF_FFFF
        expected_count = 0
        self.functional.rc.load_fpga_using_hil()
        self.functional.rc.power_on_count = Mock(return_value=actual_count)
        self.functional.expected_power_on_count = \
            Mock(return_value=expected_count)

        self.functional.DirectedPowerOnCountRegisterTest()
        self.validate_fail_message(self.functional)
        for iteration in range(self.functional.max_fail_count):
            self.validate_log_message(
                self.call_args_list,
                self.functional.power_on_error_msg(iteration=iteration,
                                                   expected=expected_count,
                                                   actual=actual_count),
                'error')

    def test_DirectedSafetyModeInterruptEnableStatusRegisterTest_pass(self):
        self.functional.DirectedSafetyModeInterruptEnableStatusRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedSafetyModeInterruptEnableStatusRegisterTest_fail(self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 0))
        self.functional.DirectedSafetyModeInterruptEnableStatusRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedSafetyModeInterruptEnableStatusRegisterTest_max_fail_count(
            self):
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 0))
        self.functional.DirectedSafetyModeInterruptEnableStatusRegisterTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedSafetyModeInterruptDisableStatusRegisterTest_pass(self):
        self.functional.DirectedSafetyModeInterruptDisableStatusRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedSafetyModeInterruptDisableStatusRegisterTest_fail(self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(1, 0))
        self.functional.DirectedSafetyModeInterruptDisableStatusRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedSafetyModeInterruptDisableStatusRegisterTest_max_fail_count(
            self):
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.rc.read_interrupt_status = Mock(return_value=(1, 0))
        self.functional.DirectedSafetyModeInterruptDisableStatusRegisterTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedSafetyModeInterruptDisableStatusRegisterTest_skip(self):
        with patch.object(self.functional.rc, 'is_tiu_physically_connected')\
            as mock_is_tiu_physically_connected:
            mock_is_tiu_physically_connected.return_value = False

            with self.assertRaises(SkipTest):
                self.functional.\
                    DirectedSafetyModeInterruptDisableStatusRegisterTest()

    def test_DirectedSafetyModeInterruptEnableStatusRegisterTest_skip(self):
        with patch.object(self.functional.rc, 'is_tiu_physically_connected')\
            as mock_is_tiu_physically_connected:
            mock_is_tiu_physically_connected.return_value = False

            with self.assertRaises(SkipTest):
                self.functional.\
                    DirectedSafetyModeInterruptEnableStatusRegisterTest()

    def test_DirectedTriggerInterruptEnableStatusMaxFifoCountRegisterTest_pass(
            self):
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.\
                DirectedTriggerInterruptEnableStatusMaxFifoCountRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerInterruptEnableStatusMaxFifoCountRegisterTest_fail(
            self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 0))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.\
                DirectedTriggerInterruptEnableStatusMaxFifoCountRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedTriggerInterruptEnableStatusZeroFifoCountRegisterTest_pass(
            self):
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional. \
                DirectedTriggerInterruptEnableStatusZeroFifoCountRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerInterruptEnableStatusZeroFifoCountRegisterTest_fail(
            self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(1, 1))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional. \
                DirectedTriggerInterruptEnableStatusZeroFifoCountRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedTriggerInterruptEnableSingleFifoCountRegisterTest_pass(
            self):
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional. \
                DirectedTriggerInterruptEnableStatusSingleFifoCountRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerInterruptEnableSingleFifoCountRegisterTest_fail(
            self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 0))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional. \
                DirectedTriggerInterruptEnableStatusSingleFifoCountRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedTriggerInterruptDisableStatusRegisterTest_pass(self):
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.DirectedTriggerInterruptDisableStatusRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedTriggerInterruptDisableStatusRegisterTest_fail(self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 1))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.DirectedTriggerInterruptDisableStatusRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedTriggerInterruptDisableStatusRegisterTest_max_fail_count(
            self):
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 1))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.DirectedTriggerInterruptDisableStatusRegisterTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedInterruptStatusRegisterTest_pass(self):
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.DirectedInterruptControlRegisterTest()
        self.validate_pass_message(self.functional)

    def test_DirectedInterruptStatusRegisterTest_fail(self):
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 0))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.DirectedInterruptControlRegisterTest()
        self.validate_fail_message(self.functional)

    def test_DirectedInterruptStatusRegisterTest_max_fail_count(
            self):
        self.functional.max_fail_count = self.functional.test_iterations + 10
        self.functional.rc.read_interrupt_status = Mock(return_value=(0, 0))
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.functional.DirectedInterruptControlRegisterTest()
        self.validate_fail_message(self.functional)
        self.validate_max_fail_count(self.functional)

    def test_DirectedFpgaDebugFlippingBitsTes_pass(self):
        self.functional.DirectedFpgaDebugFlippingBitsTes()
        self.validate_pass_message(self.functional)

    def test_DirectedFpgaDebugFlippingBitsTes_fail(self):
        self.functional.test_iterations = 1
        expected = 0b11111011
        invalid = 0b11111111
        expected_values = [0b11111110, 0b11111101, invalid, 0b11110111,
                           0b11101111, 0b11011111, 0b10111111, 0b01111111]
        self.functional.flipping_bits_expected_input_values = Mock(
            return_value=expected_values)
        self.functional.DirectedFpgaDebugFlippingBitsTes()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_flipping_bits(
                iteration=0, expected=invalid, actual=expected, num_bytes=1),
            'error')

    def test_DirectedFpgaDebugOutputToInputTest_pass(self):
        self.functional.DirectedFpgaDebugOutputToInputTest()
        self.validate_pass_message(self.functional)

    def test_DirectedFpgaDebugOutputToInputTest_fail(self):
        expected = 0xAA
        actual = 0x55
        self.functional.generate_random_byte = Mock(return_value=expected)
        with patch.object(Rc3, 'fpga_debug_input') as mock_fpga_debug_input:
            mock_fpga_debug_input.return_value = actual
            self.functional.DirectedFpgaDebugOutputToInputTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_output_enabled(
                iteration=0, expected=expected, actual=actual),
            'error')
