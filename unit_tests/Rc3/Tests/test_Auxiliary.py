# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import random
from unittest.mock import Mock, patch

from Rc3.instrument.rc3 import Rc3
from Rc3.Tests.Auxiliary import Functional, Diagnostics
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 1

    def test_DirectedAuxRegistersWriteReadExhaustiveTest_pass(self):
        self.functional.DirectedAuxRegistersWriteReadExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedAuxRegistersWriteReadExhaustiveTest_fail(self):
        self.functional.compare_data = Mock(return_value=False)
        self.functional.DirectedAuxRegistersWriteReadExhaustiveTest()
        self.validate_fail_message(self.functional)


class DiagnosticTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.call_args_list = self.diagnostics.Log.call_args_list
        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedAuxFpgaSideBusOutputTest_pass(self):
        self.diagnostics.DirectedAuxFpgaSideBusOutputTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedAuxFpgaSideBusOutputTest_fail(self):
        expected = 0xABC
        actual = 0x123
        with patch.object(random, 'getrandbits') as mock_random,\
                patch.object(Rc3, 'aux_fpga_side_input') as mock_input:
            mock_random.return_value = expected
            mock_input.return_value = actual
            self.diagnostics.DirectedAuxFpgaSideBusOutputTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_log_message(
            self.diagnostics.Log.call_args_list,
            self.diagnostics.error_msg_output_test(0, expected, actual),
            'error')

    def test_DirectedBidirectionalControlAllBitsTest_pass(self):
        self.diagnostics.DirectedBidirectionalControlAllBitsTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedBidirectionalControlAllBitsTest_fail(self):
        self.diagnostics.is_aux_out_reflected_in_input = Mock(
            return_value=False)
        self.diagnostics.DirectedBidirectionalControlAllBitsTest()
        self.validate_fail_message(self.diagnostics)

    def test_DirectedBidirectionalControlWalkingOnesTest_pass(self):
        self.diagnostics.DirectedBidirectionalControlWalkingOnesTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedBidirectionalControlWalkingOnesTest_fail(self):
        self.diagnostics.walk_inputs_to_outputs = Mock(
            return_value=False)
        self.diagnostics.DirectedBidirectionalControlWalkingOnesTest()
        self.validate_fail_message(self.diagnostics)

    def test_DirectedAuxSideBidirectionalControlWalkingOnesTest_pass(self):
        self.diagnostics.DirectedAuxSideBidirectionalControlWalkingOnesTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedAuxSideBidirectionalControlWalkingOnesTest_fail(self):
        self.diagnostics.rc.aux_side_input = Mock(
            return_value=random.getrandbits(12))
        self.diagnostics.DirectedAuxSideBidirectionalControlWalkingOnesTest()
        self.validate_fail_message(self.diagnostics)


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.call_args_list = self.functional.Log.call_args_list
        self.functional.test_iterations = 2
        self.functional.max_fail_count = 1

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedAuxRegistersWriteReadExhaustiveTest_pass(self):
        self.functional.DirectedAuxRegistersWriteReadExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedAuxRegistersWriteReadExhaustiveTest_fail(self):
        self.functional.compare_data = Mock(return_value=False)
        self.functional.DirectedAuxRegistersWriteReadExhaustiveTest()
        self.validate_fail_message(self.functional)
