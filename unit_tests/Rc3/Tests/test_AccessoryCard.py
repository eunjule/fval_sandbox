# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Common.utilities import compliment_bytes
from Rc3.instrument.rc3 import Rc3
from Rc3.Tests.AccessoryCard import Functional
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()
        self.functional.test_iterations = 2
        self.call_args_list = self.functional.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedAccCardIoFlippingBitsTest_pass(self):
        self.functional.DirectedAccCardIoFlippingBitsTest()
        self.validate_pass_message(self.functional)

    def test_DirectedAccCardIoFlippingBitsTest_fail(self):
        self.functional.test_iterations = 1
        num_bytes = 3
        invalid_index = 0
        expected = compliment_bytes(1 << invalid_index, num_bytes)
        invalid = 0
        expected_values = []
        bit_size = num_bytes * 8
        expected_values = []
        for bit_num in range(bit_size):
            expected_values.append(compliment_bytes(1 << bit_num, num_bytes))
        expected_values[invalid_index] = invalid
        self.functional.flipping_bits_expected_input_values = Mock(
            return_value=expected_values)
        self.functional.DirectedAccCardIoFlippingBitsTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_flipping_bits(
                iteration=0, expected=invalid, actual=expected,
                num_bytes=num_bytes),
            'error')

    def test_DirectedAccCardIoOutputToInputTest_pass(self):
        self.functional.DirectedAccCardIoOutputToInputTest()
        self.validate_pass_message(self.functional)

    def test_DirectedAccCardIoOutputToInputTest_fail(self):
        expected = 0xAAAAAA
        actual = 0x555555
        self.functional.generate_random_bytes = Mock(return_value=expected)
        with patch.object(Rc3, 'acc_card_io_input') as mock_acc_card_io_input:
            mock_acc_card_io_input.return_value = actual
            self.functional.DirectedAccCardIoOutputToInputTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_output_enabled(
                iteration=0, expected=expected, actual=actual),
            'error')

    def test_DirectedAccCardIoOutputWriteReadTest_pass(self):
        self.functional.DirectedAccCardIoOutputWriteReadTest()
        self.validate_pass_message(self.functional)

    def test_DirectedAccCardIoOutputWriteReadTest_fail(self):
        self.functional.test_iterations = 3
        expected_outputs = [i for i in range(self.functional.test_iterations)]
        actual_output = 0
        self.functional.generate_writes = \
            Mock(return_value=expected_outputs)
        with patch.object(Rc3, 'acc_card_io_output') as mock_acc_card_io_output:
            mock_acc_card_io_output.return_value = actual_output
            self.functional.DirectedAccCardIoOutputWriteReadTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_write_read(
                iteration=1, write=expected_outputs[1], read=actual_output),
            'error')

    def test_DirectedAccCardIoControlReadWriteAccessTest_pass(self):
        self.functional.DirectedAccCardIoControlReadWriteAccessTest()
        self.validate_pass_message(self.functional)

    def test_DirectedAccCardIoControlReadWriteAccessTest_fail(self):
        self.functional.test_iterations = 3
        expected_outputs = [i for i in range(self.functional.test_iterations)]
        actual_output = 0
        self.functional.generate_writes = \
            Mock(return_value=expected_outputs)
        with patch.object(Rc3,
                          'acc_card_io_control') as mock_acc_card_io_control:
            mock_acc_card_io_control.return_value = actual_output
            self.functional.DirectedAccCardIoControlReadWriteAccessTest()
        self.validate_fail_message(self.functional)
        self.validate_log_message(
            self.call_args_list,
            self.functional.error_msg_write_read(
                iteration=1, write=expected_outputs[1], read=actual_output),
            'error')
