# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Common.fval import skip
from Common.instruments.dps.hddps import Hddps
from Common.instruments.hdmt_trigger_interface import HdmtTriggerInterface
from Hpcc.instrument.hpcc import Hpcc
from Rc3.Tests.BroadcastTrigger import Diagnostics
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


@skip('')
class DiagnosticsTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics(methodName='runTest')
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedSendBroadcastTriggerTest_pass(self):
        self.diagnostics.DirectedSendBroadcastTriggerTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedSendBroadcastTriggerTest_invalid_down_trigger_fail(self):
        self.diagnostics.test_iterations = 10
        self.diagnostics.max_fail_count = self.diagnostics.test_iterations
        self.expected_trigger = 0
        slot = self.diagnostics.rc.aurora_link_slots[0]

        receive_interface = HdmtTriggerInterface.get_interface(slot)
        temp = [receive_interface[0].check_for_down_trigger,
                receive_interface[1].check_for_down_trigger]
        receive_interface[0].check_for_down_trigger = \
            self.check_for_down_trigger_mock
        receive_interface[1].check_for_down_trigger = \
            self.check_for_down_trigger_mock

        self.diagnostics.DirectedSendBroadcastTriggerTest()

        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(
                iteration=self.diagnostics.test_iterations-1,
                name=receive_interface[0].name(),
                expected=self.expected_trigger,
                actual=self.expected_trigger-1),
            'error')
        self.validate_fail_message(self.diagnostics)
        self.assertEqual(
            self.diagnostics.fail_count, self.diagnostics.test_iterations)

        receive_interface[0].check_for_down_trigger = temp[0]
        receive_interface[1].check_for_down_trigger = temp[1]

    def check_for_down_trigger_mock(self, trigger):
        self.expected_trigger = trigger
        return trigger-1

    def test_test_DirectedSendBroadcastTriggerTest_last_broadcast_trigger_fail(
            self):
        with patch.object(self.diagnostics.rc,
                          'read_last_broadcasted_trigger') as mock_trigger:
            mock_trigger.side_effect = self.last_broadcasted_trigger_mock
            with self.assertRaises(
                    self.diagnostics.rc.LastBroadcastedTriggerError):
                self.diagnostics.DirectedSendBroadcastTriggerTest()
        self.assertEqual(self.diagnostics.fail_count, 0)

    def last_broadcasted_trigger_mock(self):
        self.expected_trigger = self.diagnostics.rc.read_bar_register(
            self.diagnostics.rc.registers.LAST_BROADCASTED_TRIGGER).trigger
        return self.expected_trigger - 1
