# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Common.fval import SkipTest
from Common.instruments.dps.hddps import Hddps
from Common.instruments.hdmt_trigger_interface import HdmtTriggerInterface
from Hpcc.instrument.hpcc import Hpcc
from Rc3.Tests.AuroraTrigger import error_broadcast_msg, error_msg
from Rc3.Tests.AuroraTrigger import InstrumentToRcDiagnostics,\
    RcToInstrumentDiagnostics, SimulatedUpTriggerDiagnostics

from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class RcToInstrumentDiagnosticsTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = RcToInstrumentDiagnostics(methodName='runTest')
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedRcToInstrumentSlotTriggerTest_pass(self):
        for slot in self.diagnostics.rc.aurora_link_slots:
            test_name = f'DirectedRcToInstrumentSlot{slot}TriggerTest'
            self.diagnostics.fail_count = 0
            getattr(self.diagnostics, test_name)()
            self.validate_pass_message(self.diagnostics)

    def test_DirectedRcToInstrumentSlotTriggerTest_trigger_fail(self):
        for slot in self.diagnostics.rc.aurora_link_slots:
            self.diagnostics.fail_count = 0
            self.invalid_down_trigger_injection(slot)

    def invalid_down_trigger_injection(self, slot):
        test_name = f'DirectedRcToInstrumentSlot{slot}TriggerTest'
        self.num_mock_calls = 0
        self.fail_iteration = 1

        receive_interface = HdmtTriggerInterface.get_interface(slot)
        temp = [receive_interface[0].check_for_down_trigger,
                receive_interface[1].check_for_down_trigger]
        receive_interface[0].check_for_down_trigger = \
            self.check_for_down_trigger_mock
        receive_interface[1].check_for_down_trigger = \
            self.check_for_down_trigger_mock

        getattr(self.diagnostics, test_name)()

        self.validate_log_message(
            self.call_args_list,
            error_msg(iteration=self.fail_iteration,
                      receive_name=receive_interface[0].name(),
                      expected=self.expected_down_trigger,
                      actual=self.actual_down_trigger),
            'error')
        self.validate_fail_message(self.diagnostics)

        receive_interface[0].check_for_down_trigger = temp[0]
        receive_interface[1].check_for_down_trigger = temp[1]

    def check_for_down_trigger_mock(self, expected_trigger):
        if self.num_mock_calls == self.fail_iteration + 1:
            self.expected_down_trigger = expected_trigger
            self.actual_down_trigger = expected_trigger - 1
            expected_trigger = self.actual_down_trigger

        self.num_mock_calls += 1
        return expected_trigger

    def test_DirectedRcToInstrument_SkipTest(self):
        self.diagnostics.get_interfaces = Mock(return_value=None)
        with self.assertRaises(SkipTest):
            self.diagnostics.DirectedRcToInstrumentSlot0TriggerTest()

    def test_DirectedRcToInstrumentSlotTriggerTest_max_fail_count_fail(self):
        self.diagnostics.test_iterations = 10
        self.diagnostics.max_fail_count = 2

        for slot in self.diagnostics.rc.aurora_link_slots:
            test_name = f'DirectedRcToInstrumentSlot{slot}TriggerTest'
            self.diagnostics.fail_count = 0

            receive_interface = HdmtTriggerInterface.get_interface(slot)
            temp = [receive_interface[0].check_for_down_trigger,
                    receive_interface[1].check_for_down_trigger]
            receive_interface[0].check_for_down_trigger = Mock(return_value=0)
            receive_interface[1].check_for_down_trigger = Mock(return_value=0)

            getattr(self.diagnostics, test_name)()

            receive_interface[0].check_for_down_trigger = temp[0]
            receive_interface[1].check_for_down_trigger = temp[1]

            self.assertEqual(
                self.diagnostics.fail_count, self.diagnostics.max_fail_count)
            self.validate_fail_message(self.diagnostics)


class InstrumentToRcDiagnosticsTests(Rc3UnitTest):

    def setUp(self):
        super().setUp()
        self.diagnostics = InstrumentToRcDiagnostics(methodName='runTest')
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedInstrumentToRcSlotTriggerTest_pass(self):
        for slot in self.diagnostics.rc.aurora_link_slots:
            test_name = f'DirectedInstrumentToRcSlot{slot}TriggerTest'
            self.diagnostics.fail_count = 0
            getattr(self.diagnostics, test_name)()
            self.validate_pass_message(self.diagnostics)

    def test_DirectedInstrumentToRcSlotTriggerTest_trigger_fail(self):
        for slot in self.diagnostics.rc.aurora_link_slots:
            self.diagnostics.fail_count = 0
            self.invalid_up_trigger_injection(slot)

    def invalid_up_trigger_injection(self, slot):
        test_name = f'DirectedInstrumentToRcSlot{slot}TriggerTest'
        self.num_mock_calls = 0
        self.fail_iteration = 1

        receive_interface = HdmtTriggerInterface.get_interface(None)[slot]
        temp = receive_interface.check_for_up_trigger
        receive_interface.check_for_up_trigger = self.check_for_up_trigger_mock

        getattr(self.diagnostics, test_name)()

        self.validate_log_message(
            self.call_args_list,
            error_msg(iteration=self.fail_iteration,
                      receive_name=receive_interface.name(),
                      expected=self.expected_up_trigger,
                      actual=self.actual_up_trigger),
            'error')
        self.validate_fail_message(self.diagnostics)

        receive_interface.check_for_up_trigger = temp

    def check_for_up_trigger_mock(self, trigger):
        if self.num_mock_calls == self.fail_iteration + 1:
            self.expected_up_trigger = trigger
            self.actual_up_trigger = trigger - 1
            trigger = self.actual_up_trigger

        self.num_mock_calls += 1
        return trigger

    def test_DirectedInstrumentToRcSlotTriggerTest_broadcast_fail(self):
        for slot in self.diagnostics.rc.aurora_link_slots:
            name = self.diagnostics.interfaces.get(slot)[0].name()
            if 'dps' not in name:
                self.diagnostics.fail_count = 0
                self.invalid_broadcast_trigger_injection(slot)

    def invalid_broadcast_trigger_injection(self, slot):
        test_name = f'DirectedInstrumentToRcSlot{slot}TriggerTest'
        self.num_mock_calls = 0
        self.fail_iteration = 1

        receive_interface = HdmtTriggerInterface.get_interface(slot)
        temp = [receive_interface[0].check_for_down_trigger,
                receive_interface[1].check_for_down_trigger]
        receive_interface[0].check_for_down_trigger = \
            self.check_for_down_trigger_mock
        receive_interface[1].check_for_down_trigger = \
            self.check_for_down_trigger_mock

        getattr(self.diagnostics, test_name)()

        self.validate_log_message(
            self.call_args_list,
            error_broadcast_msg(iteration=self.fail_iteration,
                                receive_name=receive_interface[0].name(),
                                expected=self.expected_up_trigger,
                                actual=self.actual_up_trigger),
            'error')
        self.validate_fail_message(self.diagnostics)

        receive_interface[0].check_for_down_trigger = temp[0]
        receive_interface[1].check_for_down_trigger = temp[1]

    def check_for_down_trigger_mock(self, trigger):
        if self.num_mock_calls == self.fail_iteration + 1:
            self.expected_up_trigger = trigger
            self.actual_up_trigger = trigger - 1
            trigger = self.actual_up_trigger

        self.num_mock_calls += 1
        return trigger

    def test_DirectedInstrumentToRc_SkipTest(self):
        with self.assertRaises(SkipTest):
            with patch.object(self.diagnostics, 'get_interfaces') as \
                    mock_get_interfaces:
                mock_get_interfaces.return_value = None
                self.diagnostics.DirectedInstrumentToRcSlot0TriggerTest()

    def test_DirectedInstrumentToRcSlotTriggerTest_max_fail_count_fail(self):
        self.diagnostics.test_iterations = 10
        self.diagnostics.max_fail_count = 2

        for slot in self.diagnostics.rc.aurora_link_slots:
            name = self.diagnostics.interfaces.get(slot)[0].name()
            if 'dps' not in name:
                test_name = f'DirectedInstrumentToRcSlot{slot}TriggerTest'
                self.diagnostics.fail_count = 0

                receive_interface = HdmtTriggerInterface.get_interface(slot)
                temp = [receive_interface[0].check_for_down_trigger,
                        receive_interface[1].check_for_down_trigger]
                receive_interface[0].check_for_down_trigger = \
                    Mock(return_value=0)
                receive_interface[1].check_for_down_trigger = \
                    Mock(return_value=0)

                getattr(self.diagnostics, test_name)()

                receive_interface[0].check_for_down_trigger = temp[0]
                receive_interface[1].check_for_down_trigger = temp[1]

                self.assertEqual(
                    self.diagnostics.fail_count,
                    self.diagnostics.max_fail_count)
                self.validate_fail_message(self.diagnostics)


class SimulatedUpTriggerDiagnosticsTest(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = SimulatedUpTriggerDiagnostics(methodName='runTest')
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def test_DirectedSimulatedToBroadcastRegisterTest_pass(self):
        self.diagnostics.DirectedSimulatedToBroadcastRegisterTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedSimulatedToBroadcastRegisterTest_fail(self):
        expected = 0x123
        actual = 0x321

        self.diagnostics.generate_invalid_card_type_trigger = \
            Mock(return_value=expected)
        self.diagnostics.read_last_broadcasted = Mock(return_value=actual)
        self.diagnostics.DirectedSimulatedToBroadcastRegisterTest()
        self.validate_fail_message(self.diagnostics)
        self.validate_log_message(
            self.call_args_list,
            error_msg(iteration=0, receive_name=f'Rc3 link 0',
                      expected=expected, actual=actual),
            'error')
