# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Rc3.instrument.lmk04832 import Lmk04832
from Rc3.instrument.rc3_register import LMK04832_STATUS
from Rc3.Tests.Lmk04832 import Diagnostics, Functional
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 1

    def tearDown(self):
        self.print_log_messages(self.diagnostics.Log.call_args_list, 'info',
                                'error')

    def test_DirectedRegisterReadExhaustiveTest_pass(self):
        self.diagnostics.DirectedRegisterReadExhaustiveTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedRegisterReadExhaustiveTest_fifo_count_fail(self):
        self.diagnostics.lmk04832.rx_fifo_count = self.mock_rx_fifo_count
        with self.assertRaises(Lmk04832.ReadTimeout):
            self.diagnostics.DirectedRegisterReadExhaustiveTest()

    def mock_rx_fifo_count(self):
        return 0

    def test_DirectedRegisterReadExhaustiveTest_read_fail(self):
        self.error_id_registers = Lmk04832.ID_REGISTERS.copy()
        commands = list(Lmk04832.ID_REGISTERS.keys())
        self.error_id_registers[commands[0]] = 0

        self.diagnostics.lmk04832.read = self._mock_lmk04832_read
        self.diagnostics.DirectedRegisterReadExhaustiveTest()

        expected = Lmk04832.ID_REGISTERS.get(commands[0])
        actual = self.error_id_registers.get(commands[0])
        self.validate_log_message(
            self.diagnostics.Log.call_args_list,
            self.diagnostics.exhaustive_read_error_msg(
                0,
                self.diagnostics.lmk04832.__class__.__name__,
                expected,
                actual),
            'error')

        self.validate_fail_message(self.diagnostics)

    def _mock_lmk04832_read(self, command):
        return self.error_id_registers.get(command)


class FunctionalTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.setUp(tester=self.tester)
        self.functional.Log = Mock()

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list, 'info',
                                'error')

    def test_DirectedRxFifoCountTest_pass(self):
        self.functional.DirectedRxFifoCountTest()
        self.validate_pass_message(self.functional)

    def test_DirectedRxFifoCountTest_reset_fail(self):
        self.functional.lmk04832.status = self._mock_status
        max_fifo_count = 1024 - 1

        non_zero_counts = [[1, 0], [0, 1], [max_fifo_count, 1]]
        for counts in non_zero_counts:
            self.status = LMK04832_STATUS(tx_fifo_count=counts[0],
                                          rx_fifo_count=counts[1])
            with self.assertRaises(Lmk04832.ResetError):
                self.functional.DirectedRxFifoCountTest()

        # Should assert if counts are both zero
        self.status = LMK04832_STATUS(tx_fifo_count=0, rx_fifo_count=0)
        self.functional.DirectedRxFifoCountTest()

    def _mock_status(self):
        return self.status

    def test_DirectedRxFifoCountTest_fifo_count_fail(self):
        self.functional.test_iterations = 1
        self.expected_count_list = [1, 10, 3]

        self.functional.rx_fifo_count_list = self._mock_rf_fifo_count_list
        self.functional.lmk04832.rx_fifo_count = self._mock_rx_fifo_count

        self.functional.DirectedRxFifoCountTest()

        self.validate_log_message(
            self.functional.Log.call_args_list,
            self.functional.invalid_rx_count_msg(
                iteration=1,
                expected=self.expected_count_list[1],
                actual=1),
            'error')

        self.validate_log_message(
            self.functional.Log.call_args_list,
            self.functional.invalid_rx_count_msg(
                iteration=2,
                expected=self.expected_count_list[2],
                actual=1),
            'error')

        self.validate_fail_message(self.functional)

    def _mock_rf_fifo_count_list(self):
        return self.expected_count_list

    def _mock_rx_fifo_count(self):
        return self.expected_count_list[0]
