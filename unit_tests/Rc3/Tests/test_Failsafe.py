# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import random
from unittest.mock import Mock

from Rc3.Tests.Failsafe import Functional
from Rc3.instrument.failsafe import FAILSAFE_LIMIT_DEFAULT
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class FailsafeTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.functional = Functional()
        self.functional.Log = self.tester.rc.Log = Mock()
        self.functional.setUp(tester=self.tester)
        self.functional.test_iterations = 10

    def tearDown(self):
        self.print_log_messages(self.functional.Log.call_args_list, 'info',
                                'error', 'warning')

    def test_DirectedWriteReadFailsafeLimitExhaustiveTest_pass(self):
        self.functional.DirectedWriteReadFailsafeLimitExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedWriteReadFailsafeLimitExhaustiveTest_fail(self):
        self.functional.rc.read_failsafe_limit = Mock(return_value=random.getrandbits(32))
        self.functional.DirectedWriteReadFailsafeLimitExhaustiveTest()
        self.validate_fail_message(self.functional)

    def test_DirectedWriteReadFailsafeLimitExhaustiveTest_default_fail(self):
        actual_default = FAILSAFE_LIMIT_DEFAULT + 1
        self.functional.rc.failsafe_limit_default = \
            Mock(return_value=actual_default)
        self.functional.DirectedWriteReadFailsafeLimitExhaustiveTest()
        self.validate_log_message(
            self.functional.Log.call_args_list,
            self.functional.failsafe_default_limit_error_msg(actual_default),
            'error')

    def test_DirectedWriteReadFailsafeControlExhaustiveTest_pass(self):
        self.functional.DirectedWriteReadFailsafeControlExhaustiveTest()
        self.validate_pass_message(self.functional)

    def test_DirectedWriteReadFailsafeControlExhaustiveTest_fail(self):
        self.functional.rc.read_failsafe_control = Mock(return_value=random.getrandbits(32))
        self.functional.DirectedWriteReadFailsafeControlExhaustiveTest()
        self.validate_fail_message(self.functional)
