# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock, patch

from Common.instruments.dps.hddps import Hddps
from Hpcc.instrument.hpcc import Hpcc
from Rc3.Tests.SoftwareTriggerFIFO import Diagnostics,\
    SimulatedUpTriggerDiagnostics
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticsTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics(methodName='runTest')
        with patch.object(Hddps, 'ensure_initialized'), \
             patch.object(Hpcc, 'ensure_initialized'):
            self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list


    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedSendSoftwareTriggerReadTest_pass(self):
        self.diagnostics.DirectedSendSoftwareTriggerReadTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedSendSoftwareTriggerReadTest_invalid_trigger_fail(self):
        self.diagnostics.validate_triggers = Mock(return_value=False)
        self.diagnostics.DirectedSendSoftwareTriggerReadTest()
        self.validate_fail_message(self.diagnostics)


class SimulatedUpTriggerDiagnosticsTest(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = SimulatedUpTriggerDiagnostics(methodName='runTest')
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()

        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 2
        self.call_args_list = self.diagnostics.Log.call_args_list

    def test_DirectedSimulatedToSoftwareFifoTest_pass(self):
        self.diagnostics.DirectedSimulatedToSoftwareFifoTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedSimulatedToSoftwareFifoTest_fail(self):
        self.diagnostics.validate_triggers = Mock(return_value=False)
        self.diagnostics.DirectedSimulatedToSoftwareFifoTest()
        self.validate_fail_message(self.diagnostics)
