# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from unittest.mock import Mock

from Rc3.Tests.Temperature import DELTA, Diagnostics, TEMPERATURE_RANGE
from unit_tests.Rc3.Tests.Rc3UnitTest import Rc3UnitTest


class DiagnosticsTests(Rc3UnitTest):
    def setUp(self):
        super().setUp()
        self.diagnostics = Diagnostics()
        self.diagnostics.setUp(tester=self.tester)
        self.diagnostics.Log = Mock()
        self.diagnostics.test_iterations = 2
        self.diagnostics.max_fail_count = 1
        self.call_args_list = self.diagnostics.Log.call_args_list
        self.rc = self.diagnostics.rc

    def tearDown(self):
        self.print_log_messages(self.call_args_list, 'info', 'error')

    def test_DirectedTemperatureTest_pass(self):
        self.diagnostics.DirectedTemperatureTest()
        self.validate_pass_message(self.diagnostics)

    def test_DirectedTemperatureTest_range_fail(self):
        actual = (TEMPERATURE_RANGE[0] - (DELTA * 2))

        self.rc.temperature_using_bar_read = Mock(return_value=actual)
        self.diagnostics.DirectedTemperatureTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg_out_of_range(actual),
            'error')
        self.validate_pass_message(self.diagnostics)

    def test_DirectedTemperatureTest_iteration_fail(self):
        self.diagnostics.test_iterations = 1
        expected = self.rc.temperature_using_bar_read(0)
        actual = (TEMPERATURE_RANGE[0] - (DELTA * 2))
        num_channels = 2
        self.values = [expected, actual] * num_channels
        self.rc.temperature_using_bar_read = \
            Mock(side_effect=self.mock_values)
        self.diagnostics.DirectedTemperatureTest()
        self.validate_log_message(
            self.call_args_list,
            self.diagnostics.error_msg(iteration=0, expected=expected,
                                       actual=actual),
            'error')
        self.validate_fail_message(self.diagnostics)

    def mock_values(self, channel=0):
        return self.values.pop(0)
