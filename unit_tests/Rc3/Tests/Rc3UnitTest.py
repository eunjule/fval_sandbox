# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import unittest

from Common.hdmt_simulator import HdmtSimulator
from Common.instruments.tester import get_tester_for_unit_test
from Common import hilmon
from Rc3.testbench.rc3_simulator import simulate_from_file_method_calls
from Rc3.Tests.Rc3Test import get_fail_msg, get_pass_msg


class Rc3UnitTest(unittest.TestCase):
    def setUp(self):
        self.simulator = HdmtSimulator('Simulated HW')
        hilmon.hil = self.simulator

        self.add_simulated_instruments()

        self.tester = tester = get_tester_for_unit_test()
        tester.discover_all_instruments()
        tester.devices[0].under_test = True

        # avoid unit test accessing network share
        simulate_from_file_method_calls(self.tester.rc)

    def add_simulated_instruments(self):
        self.simulator.add_hddps(0)
        self.simulator.add_hpcc(1)

    def name(self):
        return self.__class__.__name__

    def print_log_messages(self, call_args_list, *msg_type):
        if os.getenv('DBG_LOGGING'):
            for log_info, kwargs in call_args_list:
                log_level, log_message = log_info
                if log_level in msg_type:
                    print('{:5s}: {}'.format(log_level.upper(), log_message))

    def validate_pass_message(self, test_class):
        self.assertEqual(
            test_class.fail_count,
            0,
            f'Error: Fail count {test_class.fail_count} should be zero')
        self.validate_log_message(test_class.Log.call_args_list,
                                  get_pass_msg(test_class.test_name(),
                                               test_class.test_iterations),
                                  'info')

    def validate_fail_message(self, test_class):
        self.assertNotEqual(
            test_class.fail_count,
            0,
            f'Error: Fail count {test_class.fail_count} should not be zero')
        self.validate_log_message(test_class.Log.call_args_list,
                                  get_fail_msg(test_class.test_name(),
                                               test_class.fail_count,
                                               test_class.test_iterations),
                                  'error')

    def validate_log_message(self, call_args_list, expected_message,
                             expected_level):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if (log_message == expected_message) and \
                    (log_level == expected_level):
                break
        else:
            self.fail(f'"{expected_message} with log level of '
                      f'{expected_level}" not found.')

    def validate_max_fail_count(self, test_class):
        actual_max_fail = min([test_class.max_fail_count,
                               test_class.test_iterations])
        expected_message = f'{test_class.test_name()} was ' \
                           f'unsuccessful for {actual_max_fail} out ' \
                           f'of {test_class.test_iterations} iterations'
        for log_info, kwargs in test_class.Log.call_args_list:
            log_level, log_message = log_info
            if expected_message in log_message and \
                    (log_level == 'error'):
                break
        else:
            self.fail(f'({expected_message}) with log level of '
                      f'{"error"} not found.')

    def has_log_level(self, call_args_list, log_level):
        for log_info, kwargs in call_args_list:
            level, message = log_info
            if log_level == level:
                return True
        else:
            return False

    def assert_log_error_found(self, call_args_list):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if log_level == 'error':
                break
        else:
            self.fail(f'No log messages with log_level of error found.')

    def assert_log_error_not_found(self, call_args_list):
        for log_info, kwargs in call_args_list:
            log_level, log_message = log_info
            if log_level == 'error':
                self.fail(f'Log messages with log_level of error found.')

    def assert_log_message_substring_not_found(self, call_args_list, substring):
        for log_info, kwargs in call_args_list:
            level, message = log_info
            if substring in message:
                self.fail(f'Substring \"{substring}\" found in log message '
                          f'\"{message}\"')
