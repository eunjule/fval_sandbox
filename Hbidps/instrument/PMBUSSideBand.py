# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import time

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class PMBUSSideBand(Object):
    """Helper Class for HBI DPS PMBUS SideBand Setting

        Methods:

    """

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.slot = hbidps.slot
        self.regs = hbidps_register

    def send_pmbus_commands(self, tx_data_list):
        tx_fifo_count = self.read_pmbus_sideband_tx_fifo_count()
        if tx_fifo_count > 0:
            self.Log('error', f'init tx fifo not empty; start the reset')
            self.reset_pmbus_sideband()
            tx_fifo_count = self.read_pmbus_sideband_tx_fifo_count()
            self.Log('info', f'tx fifo count after reset: {tx_fifo_count}')
        self.store_all_tx_data(tx_data_list)
        self.start_tx_fifo_send()
        self.wait_for_pmbus_done()
        tx_count = self.read_pmbus_sideband_tx_fifo_count()
        self.Log('info', f'tx_count after go: {tx_count}')

    def start_tx_fifo_send(self):
        pmbus_control_reg = \
            self.hbidps.read_bar_register(self.regs.PMBUS_CONTROL_STATUS)
        pmbus_control_reg.go = 1
        self.hbidps.write_bar_register(pmbus_control_reg)

    def reset_pmbus_sideband(self):
        pmbus_control_reg = \
            self.hbidps.read_bar_register(self.regs.PMBUS_CONTROL_STATUS)
        pmbus_control_reg.reset = 1
        self.hbidps.write_bar_register(pmbus_control_reg)

    def is_pmbus_sideband_busy(self):
        pmbus_control_reg = \
            self.hbidps.read_bar_register(self.regs.PMBUS_CONTROL_STATUS)
        return pmbus_control_reg.busy

    def wait_for_pmbus_done(self, time_start=None):
        if time_start == None:
            time_start = time.perf_counter()
        busy_detected = False
        for i in range(1000):
            if not self.is_pmbus_sideband_busy():
                time_delta = time.perf_counter() - time_start
                self.Log('info', f'PMBUS Sideband Takes {time_delta:.3f}s')
                break
            else:
                busy_detected=True
            time.sleep(0.001)
        else:
            time_delta = time.perf_counter() - time_start
            self.Log('error', f'PMBUS Sideband Fails with {time_delta:.3f}s')
        self.Log('info', f'PMBUS Busy Detection: {busy_detected}')

    def check_pmbus_sidebank_error_status(self):
        # what error and retry? This read or overall?
        # Need to check with FPGA designer
        pass

    def read_pmbus_sideband_tx_fifo_count(self):
        pmbus_control_reg = \
            self.hbidps.read_bar_register(self.regs.PMBUS_CONTROL_STATUS)
        return pmbus_control_reg.tx_fifo_count

    def read_pmbus_sideband_rx_fifo_count(self):
        pmbus_control_reg = \
            self.hbidps.read_bar_register(self.regs.PMBUS_CONTROL_STATUS)
        print(hex(pmbus_control_reg.value))
        return pmbus_control_reg.rx_fifo_count

    def read_one_rx_data(self):
        rx_fifo_reg = self.hbidps.read_bar_register(self.regs.PMBUS_RX_FIFO)
        return rx_fifo_reg.rx_data

    def read_all_rx_data(self):
        rx_count = self.read_pmbus_sideband_rx_fifo_count()
        rx_data_list = []
        for i in range(rx_count):
            rx_data_list.append(self.read_one_rx_data())
        return rx_data_list

    def store_one_tx_data(self, pmbus_data_set):
        tx_fifo_reg = self.regs.PMBUS_TX_FIFO()
        tx_fifo_reg.target_device_number = pmbus_data_set["device"]
        tx_fifo_reg.command_code = pmbus_data_set["cmd_code"]
        tx_fifo_reg.pm_bus_transaction = pmbus_data_set["transaction"]
        tx_fifo_reg.tx_data = pmbus_data_set["tx_data"]
        self.hbidps.write_bar_register(tx_fifo_reg)

    def store_all_tx_data(self, tx_data_list):
        tx_data_count = len(tx_data_list)
        for pmbus_data_set in tx_data_list:
            self.store_one_tx_data(pmbus_data_set)
        tx_fifo_count = self.read_pmbus_sideband_tx_fifo_count()
        if tx_fifo_count != tx_data_count:
            self.Log('error', f'tx_count mis-match; read: {tx_fifo_count} '
                             f'expect: {tx_data_count}')
        else:
            self.Log('info', f'tx_count match; read: {tx_fifo_count} '
                             f'expect: {tx_data_count}')
