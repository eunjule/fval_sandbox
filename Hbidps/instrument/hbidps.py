################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import struct
import time

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common import configs
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo
from Common.instruments.hbi_instrument import HbiInstrument
from Hbidps.instrument import hbidps_register
from Hbidps.instrument import symbols
from Hbidps.instrument.LCRailPMBUS import LCRailPMBUS
from Hbidps.instrument.VtargetDevice import VTargetDevice
from Hbidps.instrument.AlarmSystem import AlarmHelper
from Hbidps.instrument.HCRailPMBUS import HCRailPMBUS
from Hbidps.instrument.PMBUSSideBand import PMBUSSideBand
from Hbidps.instrument.RailTransitionControl import RailTransitionControl
from Hbidps.instrument.SampleEngine import SampleEngine
from Hbidps.instrument.SelfTestBlock import VtVmBus, CommonForceBus
from Hbidps.instrument.TQMachine import TriggerQueueDDR, TriggerQueuePCIE
from Hbidps.instrument.MdutConfiguration import MdutConfiguration
from Hbidps.testbench.assembler import TriggerQueueAssembler


def get_comp_gpio(chip):
    return chip * 2


def get_track_gpio(chip):
    return chip * 2 + 1


class Hbidps(HbiInstrument):
    dps_slot_list = []
    slot_cpld_pair = {}
    retry_count = 100

    def __init__(self, slot, rc=None, name=None, hbidtb=None):
        super().__init__()
        self.slot = slot
        self.rc = rc
        self.hbidtb = hbidtb
        self.registers = hbidps_register
        self.symbols = symbols
        self.instrumentblt = CachingBltInfo(callback_function=self.read_instrument_blt,
                                            name='HBIDPS Instrument slot {}'.format(slot))
        self.create_helpers()

    def S2HSupport(self):
        return True

    def create_helpers(self):
        self.alarm = AlarmHelper(self)
        self.hc_rail_pmbus = HCRailPMBUS(self)
        self.lc_rail_pmbus = LCRailPMBUS(self)
        self.rail_transition_control = RailTransitionControl(self)
        self.vtaget = VTargetDevice(self)
        self.vtvm_bus = VtVmBus(self)
        self.common_force_bus = CommonForceBus(self)
        self.tq_ddr = TriggerQueueDDR(self)
        self.tq_pcie = TriggerQueuePCIE(self)
        self.mdut = MdutConfiguration(self)
        self.pmbus_sideband = PMBUSSideBand(self)
        self.sample_engine = SampleEngine(self)

    def discover_all_devices(self):
        if self.discover():
            return self
        return None

    def attach_model(self):
        pass

    def open_pcie_device(self):
        hil.hbiDpsConnect(self.slot)
        hil.hbiDpsDisconnect(self.slot)

    def dps_fpga_load(self):
        if configs.SKIP_INIT:
            self.Log('warning', f'Skipping {self.name_with_slot()} FPGA Load.')
            return True
        else:
            self.load_fpga()

    def Initialize(self):
        if configs.SKIP_INIT:
            self.Log('warning', f'Skipping {self.name_with_slot()} instrument initialization')
            self.initialize_hbi_dps_board()
            return True
        else:
            self.initialize_hbi_dps()
            self.log_blt_info()
            self.ddr_reset()
            self.dps_available_list()
            self.log_cpld_info()
            self.are_clpd_versions_same()
            self.initialize_hbi_dps_board()

    def ddr_reset(self):
        ddr_status = self.read_bar_register(self.registers.STATUS, slot=self.slot)
        self.Log('info',f'DDR Calibration status for slot {self.slot} is {hex(ddr_status.value)}')
        ddr_status_shifted = (ddr_status.value >> 4) & 3
        if ddr_status_shifted != 0:
            self.Log('info', f'Resetting DDR controller for DPS in Slot {self.slot} ')
            for reset_loop in range(10):
                reset_reg = self.registers.RESETS(value=0x1)
                self.write_bar_register(reset_reg, slot=self.slot)
                time.sleep(1)
                ddr_status = self.read_bar_register(self.registers.STATUS, slot=self.slot)
                ddr_status_shifted = (ddr_status.value >> 4) & 3
                if ddr_status_shifted == 0:
                    reset_loop_count = reset_loop
                    break
                else:
                    self.Log('info',
                             f'Retry DDR controller reset for DPS {self.name_with_slot()} attempt {reset_loop} DDR status {hex(ddr_status.value)}')
                    reset_loop_count = reset_loop
            if reset_loop_count == 9:
                self.Log('warning',
                         f'DDR calibration failed for DPS {self.name_with_slot()} after {reset_loop_count} attempts')
            else:
                self.Log('info', f'DDR reset Success for DPS {self.name_with_slot()} after {reset_loop_count} attempts')
        else:
            self.Log('info', f'DDR already calibrated for DPS {self.name_with_slot()}')

    def are_clpd_versions_same(self):
        for slots in self.slot_cpld_pair:
            cpld_version = self.slot_cpld_pair[slots]
            for slot_under_test in self.slot_cpld_pair:
                if self.slot_cpld_pair[slot_under_test] != cpld_version:
                    self.Log('error', 'CPLD versions mismatch. {}'.format(self.slot_cpld_pair))

    def dps_available_list(self):
        self.dps_slot_list.append(self.slot)

    def load_fpga(self):
        """
        •	Load FPGA and read back version number

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Passing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        if configs.HBIDPS_FPGA_IMAGE_LOAD:
            self.Log('info', 'Flashing HBIDPS FPGA with image from \'{}\''.format(configs.HBIDPS_FPGA_IMAGE))
            self.FpgaLoad(configs.HBIDPS_FPGA_IMAGE)
        self.Log('info', f'Current HBIDPS MB Image slot {self.slot} : {self.fpga_version_string()}')

    def FpgaLoad(self, image):
        if self.is_fpga_image_valid(image):
            num_retries = 5
            for retry in range(num_retries):
                try:
                    with suppress_sensor(f'FVAL HBIDPS {self.slot}',
                                         f'HBIDPS:{self.slot}'):
                        hil.hbiDpsDeviceDisable(self.slot)
                        hil.hbiDpsFpgaLoad(self.slot, image)
                        time.sleep(0.5)
                        hil.hbiDpsDeviceEnable(self.slot)
                        self.Log('info', f'{self.name()} FPGA Version: '
                                         f'{self.fpga_version_string()}')
                        result = True
                    break
                except RuntimeError as e:
                    self.Log('warning',
                             f'{self.name()} failed to properly load '
                             f'image and re-enable driver {retry + 1} of '
                             f'{num_retries} times')
        else:
            result = False
            self.Log('critical', 'Invalid HBIDPS Image on slot {}'.format(self.slot))
        return result

    def is_fpga_image_valid(self, image):
        if 'hbi_dps' in image.lower():
            return True
        else:
            return False

    def fpga_version_string(self):
        return hil.hbiDpsFpgaVersionString(self.slot)

    def log_blt_info(self):
        """
        •	Read BLT

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Passing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        try:
            self.instrumentblt.write_to_log()
        except RuntimeError:
            self.Log('warning', '{} BLT read was unsuccessful'.format(self.__class__.__name__))

    def read_instrument_blt(self):
        return hil.hbiDpsBltBoardRead(self.slot)

    def get_lc_rail_voltage(self, rail):
        return hil.hbiDpsLcRailVoltageRead(self.slot, rail)

    def get_lc_rail_current(self, rail):
        return hil.hbiDpsLcRailCurrentRead(self.slot, rail)

    def initialize_hbi_dps(self):
        hil.hbiDpsInit(self.slot)

    def initialize_hbi_dps_board(self):
        self.vtvm_bus.turn_on_and_reset_all_max14662_switch()
        time.sleep(0.1) #time for switch to turn on, based on I2L number

        self.alarm.initialize_alarm_settings()
        self.alarm.update_global_alarm_mask(0, 1, 1)

        self.vtvm_bus.initialize_vtvm_bus(log=False)
        self.common_force_bus.initialize_common_force_bus(log=False)
        self.rail_transition_control.init_rail_transition_time(log=False)

        self.hc_rail_pmbus.init_hc_rails_hw(log=False)
        self.lc_rail_pmbus.init_lc_rails_hw(log=False)
        self.vtaget.init_vtarget_device(log=False)
        time.sleep(0.2) #time for hc/lc rails, based on I2L number
        self.preset_hc_lc_vt_uhc(log=False, ignore_error=True)

        self.alarm.reset_one_hbi_dps_global_alarm_reg()
        self.alarm.update_global_alarm_mask(0, 0, 0)
        self.alarm.reset_one_hbi_dps_global_alarm_reg()

    @staticmethod
    def decode_single_precision_floating_value(raw_data):
        value_decimal = \
            struct.unpack('!f', struct.pack('!I', int(bin(raw_data), 2)))[0]
        return value_decimal

    # s2h interface API START
    def TurnOFFStreamingWithControlReg(self):
        S2HControlRegister = self.registers.S2H_CONTROL()
        streaming_off_status_value = 1
        S2HControlRegister.w_stop = streaming_off_status_value
        self.write_bar_register(S2HControlRegister)
        streaming_off_status_value = self.read_bar_register(self.registers.S2H_CONTROL).w_stop
        for turnoff_attempt in range(100):
            if streaming_off_status_value != 0:
                streaming_off_status_value = self.read_bar_register(self.registers.S2H_CONTROL).w_stop
                time.sleep(.02)
        if streaming_off_status_value == 0:
            self.Log('debug',
                     'S2H Off Successful.S2HControlRegister Status {} '.format(hex(streaming_off_status_value)))
        else:
            self.Log('error', 'S2H Off was Unsuccessful in 100 attempts')
    # s2h interface API END

    # alarm interface API START
    def get_global_alarm_sample_engines(self):
        return self.alarm.read_global_alarm_reg_sample_engines()

    def restore_alarm_settings(self):
        self.alarm.initialize_alarm_settings()

    def get_max6628_temp(self, device_index):
        return self.alarm.read_max6628_temp(device_index)

    def change_temp_upper_limit(self, device_index, value):
        self.alarm.write_max6628_upper_limit(device_index, value)

    def reset_global_alarm(self):
        self.alarm.reset_one_hbi_dps_global_alarm_reg()

    def detect_global_alarm(self):
        self.alarm.detect_global_alarm_status_one_dps()

    def reset_hc_sync_vrange_fail_alarm(self):
        self.alarm.reset_hc_sync_vrange_alarm()

    def get_global_alarms(self):
        return self.alarm.read_global_alarm_reg()

    def get_lc_rail_alarms(self):
        return self.alarm.read_lc_rail_alarm()

    def get_lc_cfold_alarms(self):
        return self.alarm.read_lc_cfold_alarm()

    def get_lc_temp_alarms(self):
        return self.alarm.read_lc_rail_temp_alarm()

    def get_lc_sample_alarms_value(self):
        return self.alarm.read_lc_sample_engine_alarm().value

    def reset_lc_sample_engine_busy(self):
        self.alarm.reset_lc_sample_engine_busy_alarm()

    def get_lc_busy_alarms(self):
        return self.alarm.read_lc_busy_alarm()

    def get_lc_channel_power_off_delay_alarm(self):
        return self.alarm.read_lc_pwr_off_delay_alarm()

    def get_lc_irange_check_fail_alarm(self):
        return self.alarm.read_lc_irange_check_fail_alarm()

    def get_hc_channel_power_off_delay_alarm(self):
        return self.alarm.read_hc_pwr_off_delay_alarm()

    def get_hc_rail_alarms(self):
        return self.alarm.read_hc_rail_alarm()

    def get_hc_cfold_alarms(self):
        return self.alarm.read_hc_cfold_alarm()

    def get_hc_irange_check_fail_alarm(self):
        return self.alarm.read_hc_irange_check_fail_alarm()

    def get_hc_vrange_alarm(self):
        return self.alarm.read_hc_vrange_alarm()

    def get_hc_temp_alarms(self):
        return self.alarm.read_hc_rail_temp_alarm()

    def get_hc_sample_alarms_value(self):
        return self.alarm.read_hc_sample_engine_alarm().value

    def reset_hc_sample_engine_busy(self):
        self.alarm.reset_hc_sample_engine_busy_alarm()

    def get_hc_busy_alarms(self):
        return self.alarm.read_hc_rail_busy_alarm()

    def get_v_target_busy_alarms(self):
        return self.alarm.read_vtarget_busy_alarm()

    def get_vm_sample_alarms_value(self):
        return self.alarm.read_vmeas_sample_engine_alarm().value

    def reset_vm_sample_engine_busy(self):
        self.alarm.reset_vmeas_sample_engine_busy_alarm()

    def get_status_alarm_mask(self):
        return self.read_bar_register(self.registers.LC_HC_STATUS_ALARM_MASK)

    def set_status_alarm_mask(self, hc_mask, lc_mask):
        alarm_mask = self.read_bar_register(self.registers.LC_HC_STATUS_ALARM_MASK)
        alarm_mask.rw_lc_mask = lc_mask
        alarm_mask.rw_hc_mask = hc_mask
        self.write_bar_register(alarm_mask)
    # alarm interface API END

    # hc_rails interface API START
    def get_hc_rails_fold_state(self):
        return self.hc_rail_pmbus.check_hc_rails_fold_status()

    def get_hc_power_state(self):
        return self.hc_rail_pmbus.get_hc_rail_power_state()

    def hc_device_pretest_check(self, rail, vout):
        chip = rail//2
        rail_index = rail%2
        self.hc_rail_pmbus.pre_test_clean_setup(chip, rail_index, vout)

    def reinit_hc_rails(self):
        self.hc_rail_pmbus.init_hc_rails_hw()

    def log_hc_rails_status(self, chip, rail):
        self.hc_rail_pmbus.read_ltm4680_status(chip, rail)

    def check_init_hc_rails_start_state(self, log=False):
        self.hc_rail_pmbus.init_hc_rails_hw(log)
        time.sleep(0.2)

    def enable_hc_run_pins(self, log=False):
        self.hc_rail_pmbus.enable_hc_run_reg(log)

    def set_hc_rails_to_safe_state(self, log=False):
        self.set_board_sources_to_safe_state()
        self.hc_rail_pmbus.init_hc_rails_hw(log)
        self.reset_uhc_dut_map()

    def read_hc_rails_device_status(self):
        self.hc_rail_pmbus.read_hc_rails_info_set()

    def read_one_hc_rail_device_status(self, rail):
        self.hc_rail_pmbus.read_one_hc_rail_info_set(rail)

    def read_hc_rails_fpga_status(self):
        self.hc_rail_pmbus.debug_hc_rails_state_status()

    def get_hc_user_data_03(self, chip, page):
        return self.hc_rail_pmbus.get_user_data_03(chip, page)

    def get_hc_user_data_04(self, chip):
        return self.hc_rail_pmbus.get_user_data_04(chip)

    def read_hc_reg_command(self, cmd, length, chip, page=-1):
        return self.hc_rail_pmbus.read_reg_command(cmd, length, chip, page)

    def read_hc_reg_frequency(self, chip):
        return self.hc_rail_pmbus.get_chip_freq_switch(chip)

    def set_hc_reg_frequency(self, chip, val):
        self.hc_rail_pmbus.set_chip_freq_switch(chip, val)

    def get_hc_channel_power_off_delay(self):
        return self.read_bar_register(self.registers.HC_CHANNEL_POWER_OFF_DELAY).value

    def set_hc_channel_power_off_delay(self, value):
        self.write_bar_register(self.registers.HC_CHANNEL_POWER_OFF_DELAY(value=value))

    def get_hc_lower_temp_limit(self):
        return self.read_bar_register(self.registers.HC_LOWER_TEMP_LIMIT)

    def set_hc_lower_temp_limit(self, temp):
        self.write_bar_register(self.registers.HC_LOWER_TEMP_LIMIT(value=temp))

    def get_hc_upper_temp_limit(self):
        return self.read_bar_register(self.registers.HC_UPPER_TEMP_LIMIT)

    def set_hc_upper_temp_limit(self, temp):
        self.write_bar_register(self.registers.HC_UPPER_TEMP_LIMIT(value=temp))
    # hc_rails interface API END

    # lc_rails interface API START
    def reinit_lc_rails(self):
        self.lc_rail_pmbus.init_lc_rails_hw()

    def check_init_lc_rails_start_state(self, log=False):
        self.lc_rail_pmbus.check_lc_rails_clean_start_state(log)

    def enable_lc_rails_ctrl(self, log=False):
        self.lc_rail_pmbus.enable_lc_chips(self.lc_rail_pmbus.chips, log)

    def set_lc_rails_to_safe_state(self, log=False):
        self.set_board_sources_to_safe_state()
        self.lc_rail_pmbus.init_lc_rails_hw(log)
        self.reset_uhc_dut_map()

    def get_lc_power_state(self):
        return self.lc_rail_pmbus.get_lc_power_state()

    def read_lc_rails_device_status(self, log=False):
        return self.lc_rail_pmbus.read_ltc2975_info_set(log)

    def get_lc_ctrl_reg(self):
        return self.read_bar_register(self.registers.LTC2975_CTRL_REGISTER)

    def check_status_word(self):
        self.lc_rail_pmbus.check_status_word()

    def get_lc_user_data_03(self, chip, page):
        return self.lc_rail_pmbus.get_user_data_03(chip, page)

    def get_lc_user_data_04(self, chip):
        return self.lc_rail_pmbus.get_user_data_04(chip)

    def read_lc_reg_command(self, cmd, length, chip, page=-1):
        return self.lc_rail_pmbus.read_reg_command(cmd, length, chip, page)

    def check_ltc2975_busy_status(self):
        self.lc_rail_pmbus.check_busy_status()

    def log_ltc2975_user_data34(self, chip):
        self.lc_rail_pmbus.log_ltc2975_user_data(chip)

    def change_ltc2975_user_data34(self, chip):
        self.lc_rail_pmbus.change_ltc2975_user_data(chip)

    def get_lc_lower_temp_limit(self):
        return self.read_bar_register(self.registers.LC_LOWER_TEMP_LIMIT)

    def set_lc_lower_temp_limit(self, temp):
        self.write_bar_register(self.registers.LC_LOWER_TEMP_LIMIT(value=temp))

    def get_lc_upper_temp_limit(self):
        return self.read_bar_register(self.registers.LC_UPPER_TEMP_LIMIT)

    def set_lc_upper_temp_limit(self, temp):
        self.write_bar_register(self.registers.LC_UPPER_TEMP_LIMIT(value=temp))

    def get_lc_channel_power_off_delay(self):
        return self.read_bar_register(self.registers.LC_CHANNEL_POWER_OFF_DELAY).value

    def set_lc_channel_power_off_delay(self, value):
        self.write_bar_register(self.registers.LC_CHANNEL_POWER_OFF_DELAY(value=value))
    # lc_rails interface API END

    # vtarget interface API START
    def check_init_vtarget_rails_start_state(self, log=False):
        self.vtaget.check_vtarget_rails_clean_start_state(log)

    def check_vt_fold_status(self):
        self.vtaget.check_vt_fold_status()

    def read_vtarg_power_state(self):
        return self.read_bar_register(self.registers.VTARG_POWER_STATE)

    def set_vtvm_rails_to_safe_state(self, log=False):
        self.vtaget.init_vtarget_device(log)
        self.set_board_sources_to_safe_state()
        self.reset_uhc_dut_map(log)
    # vtarget interface API END

    # vtvm interface API START
    def connect_vt_vm_through_vtvmbus(self, vm_rail, vt_rail):
        self.vtvm_bus.turn_on_and_reset_all_max14662_switch()
        self.vtvm_bus.get_state_attribute_from_board()
        self.vtvm_bus.clear_stage_n_attribute()
        self.vtvm_bus.turn_on_a_pair_of_vtvm_channel(vt_rail, vm_rail)
        self.vtvm_bus.check_safe_before_apply_attribute()
        self.vtvm_bus.apply_next_state_attribute()
        self.vtvm_bus.update_state_attribute()
        self.vtvm_bus.check_state_attribute_from_board()

    def turn_off_all_vt_vm_switches(self):
        self.vtvm_bus.initialize_vtvm_bus()
    # vtvm interface API END

    # tq_ddr interface API START
    def store_dps_tq_list_and_lut_in_memory(self, tq_list, dps_uhc, index, tq_address=0x10_0000, log=False):
        tq_data = self.tq_ddr.get_tq_data(tq_list, log)
        lut_address = self.tq_ddr.get_dps_lut_address(dps_uhc, index)
        self.tq_ddr.store_tq_address_to_lut(lut_address, tq_address, log)
        self.tq_ddr.store_tq_content_to_mem(tq_address, tq_data, log)

    def send_trigger_from_rctc_to_dps(self, card_type, dut_id, index):
        time_start = self.tq_ddr.send_dps_trigger_through_rctc(card_type, dut_id, index)
        return time_start

    def clear_tq_notify_bits(self, log=False):
        self.tq_ddr.clear_tq_notify(log)

    def check_tq_notify_bits(self, lc, hc, vt, log=False):
        self.tq_ddr.check_tq_notify(lc, hc, vt, log)

    def wait_for_tq_notify_bits(self, lc, hc, vt, time_start=None, log=True):
        time_delta = self.tq_ddr.wait_for_notify(lc, hc, vt, time_start, log)
        return time_delta
    # tq_ddr interface API END

    # mdut interface API START
    def set_uhc_dut_map(self, uhc_id, dut_id, valid, log=False):
        self.mdut.set_uhc_dut_status(uhc_id, dut_id, valid, log)

    def reset_uhc_dut_map(self, log=False):
        self.mdut.set_uhc_dut_status(0, 0, 0, log)
        self.mdut.set_uhc_dut_status(1, 0, 0, log)
        self.mdut.set_uhc_dut_status(2, 0, 0, log)
        self.mdut.set_uhc_dut_status(3, 0, 0, log)

    def preset_hc_lc_vt_uhc(self, log=False, ignore_error=False):
        self.reset_hc_uhc(log, ignore_error)
        self.set_one_hc_uhc(0, 0x3FF, log, ignore_error)
        self.reset_lc_uhc(log, ignore_error)
        self.set_one_lc_uhc(0, 0xFFFF, log, ignore_error)
        self.reset_vt_uhc(log, ignore_error)
        self.set_one_vt_uhc(0, 0xFFFF, log, ignore_error)

    def reset_hc_uhc(self, log=False, ignore_error=False):
        self.mdut.clear_all_uhc_hc_rail_status(log, ignore_error)

    def set_one_hc_uhc(self, uhc_id, rails, log=False, ignore_error=False):
        self.mdut.set_uhc_hc_rail_status(uhc_id, rails, log, ignore_error)

    def reset_lc_uhc(self, log=False, ignore_error=False):
        self.mdut.clear_all_uhc_lc_rail_status(log, ignore_error)

    def set_one_lc_uhc(self, uhc_id, rails, log=False, ignore_error=False):
        self.mdut.set_uhc_lc_rail_status(uhc_id, rails, log, ignore_error)

    def reset_vt_uhc(self, log=False, ignore_error=False):
        self.mdut.clear_all_uhc_vtarget_rail_status(log, ignore_error)

    def set_one_vt_uhc(self, uhc_id, rails, log=False, ignore_error=False):
        self.mdut.set_uhc_vtarget_rail_status(uhc_id, rails, log, ignore_error)
    # mdut interface API END

    # safe_state related API START
    def set_board_sources_to_safe_state(self):
        reg = self.registers.SET_SAFE_STATE()
        reg.w_set_safe_state = 1
        self.write_bar_register(reg)
    # safe_state related API END

    # sample_engine related API START
    def get_hc_rail_poll_voltage(self, rail):
        return self.sample_engine.read_hc_one_rail_polling_voltage(rail)

    def get_hc_rail_poll_current(self, rail):
        return self.sample_engine.read_hc_one_rail_polling_current(rail)

    def get_lc_rail_poll_voltage(self, rail):
        return self.sample_engine.read_lc_one_rail_polling_voltage(rail)

    def get_lc_rail_poll_current(self, rail):
        return self.sample_engine.read_lc_one_rail_polling_current(rail)

    def get_vm_rail_poll_voltage(self, rail):
        return self.sample_engine.read_vm_one_rail_polling_voltage(rail)

    def get_one_sample_engine_settings(self, rail_type, engine_index, log=False):
        if log:
            self.log_one_sample_engine_settings(rail_type, engine_index)
        return self.sample_engine.get_single_sample_engine_settings(rail_type, engine_index)

    def log_one_sample_engine_settings(self, rail_type, engine_index):
        self.sample_engine.log_single_sample_engine_settings(rail_type, engine_index)

    def get_one_sample_engine_sample(self, rail_type, engine_index, sample, log=False):
        return self.sample_engine.get_single_sample_engine_sample_content(rail_type, engine_index, sample, log)

    def get_one_sample_engine_header(self, rail_type, engine_index, sample, log=False):
        return self.sample_engine.get_single_sample_engine_header_content(rail_type, engine_index, sample, log)

    def reset_13_sample_engines(self):
        self.sample_engine.reset_all_sample_engines()

    def reset_one_sample_engine(self, type, index):
        self.sample_engine.reset_single_sample_engine(type, index)

    def read_13_sample_engines_address_and_size(self):
        return self.sample_engine.read_all_sample_engines_address_size()

    def set_one_sample_engine_header_address(self, type, index, address):
        self.sample_engine.set_single_sample_engine_header_address(type, index, address)

    def set_one_sample_engine_header_size(self, type, index, size):
        self.sample_engine.set_single_sample_engine_header_size(type, index, size)

    def set_one_sample_engine_sample_address(self, type, index, address):
        self.sample_engine.set_single_sample_engine_sample_address(type, index, address)

    def set_one_sample_engine_sample_size(self, type, index, size):
        self.sample_engine.set_single_sample_engine_sample_size(type, index, size)
    # sample_engine related API END

    # dtb interface START
    def turn_off_all_HC_rails_dtb_connection(self):
        self.hbidtb.turnOffPinByName("HC_FB0_LD_EN_N")
        self.hbidtb.turnOffPinByName("HC_FB1_LD_EN_N")
        self.hbidtb.turnOffPinByName("HC_FB2_LD_EN_N")
        self.hbidtb.turnOffPinByName("HC_FB3_LD_EN_N")

        self.hbidtb.turnOffPinByName("HC_FB0_SEN_EN_N")
        self.hbidtb.turnOffPinByName("HC_FB1_SEN_EN_N")
        self.hbidtb.turnOffPinByName("HC_FB2_SEN_EN_N")
        self.hbidtb.turnOffPinByName("HC_FB3_SEN_EN_N")

        self.hbidtb.turnOffPinByName("HC_LD_COM_R1_EN_N")
        self.hbidtb.turnOffPinByName("HC_LD_COM_R2_EN_N")
        self.hbidtb.turnOffPinByName("HC_LD_COM_R3_EN_N")
        self.hbidtb.turnOffPinByName("HC_LD_COM_R4_EN_N")
        self.hbidtb.turnOffPinByName("HC_LD_COM_R5_EN_N")
        self.hbidtb.turnOffPinByName("HC_LD_COM_R6_EN_N")

    def reset_dtb_all_pca9505_devices(self, log=False):
        self.hbidtb.reset_all_pca9505_devices(log)

    def disconnect_dtb_all_max14661_devices(self, log=False):
        self.hbidtb.disconnect_all_max14661_devices(log)

    def initialize_dtb_hc_lc_mux(self, log=False):
        self.reset_dtb_all_pca9505_devices(log)
        self.disconnect_dtb_all_max14661_devices(log)

    def clear_dtb_hc_lc_charge(self, log=False):
        self.reset_dtb_all_pca9505_devices(log)
        self.disconnect_dtb_all_max14661_devices(log)
        self.config_dtb_hc_rail_load_feedback(0, log)
        self.config_dtb_lc_rail_load_feedback(0, log)
        time.sleep(1)
        self.reset_dtb_all_pca9505_devices(log)
        self.disconnect_dtb_all_max14661_devices(log)

    def config_dtb_hc_rail_feedback_loop(self, hc_rail, log=False):
        self.hbidtb.connect_hc_dtb_feedback(self.slot % 4, hc_rail)
        if log:
            self.hbidtb.log_all_pca9505_devices()

    def config_dtb_hc_rail_load_feedback(self, hc_rail, log=False):
        self.hbidtb.connect_hc_dtb_feedback(self.slot%4, hc_rail)
        self.hbidtb.connect_hc_common_load(1)
        if log:
            self.hbidtb.log_all_pca9505_devices()
            self.hbidtb.log_all_hc_max14661_devices()

    def config_dtb_lc_rail_load_feedback(self, lc_rail, log=False):
        self.hbidtb.connect_lc_dtb_feedback(self.slot%4, lc_rail)
        self.hbidtb.connect_lc_common_load()
        if log:
            self.hbidtb.log_all_pca9505_devices()
            self.hbidtb.log_all_lc_max14661_devices()
    # dtb interface END

    # pmbus sideband interface START
    def pmbus_send_tx_commands(self, tx_data_list):
        self.pmbus_sideband.send_pmbus_commands(tx_data_list)

    def read_pmbus_rx_data_list(self):
        return self.pmbus_sideband.read_all_rx_data()
    # pmbus sideband interface END

    # ganging sync vrange interface START
    def get_sync_channel_vrange_value(self, rail):
        reg = self.read_bar_register(self.registers.HC_RAIL_NN_VRANGE, rail)
        vrange_external = reg.vrange_is_external
        valid = reg.vrange_valid
        vrange = reg.vrange
        return vrange_external, valid, vrange

    def log_sync_channel_mask_value(self):
        for chip in range(5):
            freeze, mask = self.get_sync_channel_mask_value(chip)
            self.Log('info', f'chip{chip}, freeze {freeze}, mask 0x{mask:05x}')

    def check_cleared_sync_channel_mask_value(self, log=False):
        for chip in range(5):
            freeze, mask = self.get_sync_channel_mask_value(chip)
            if (freeze != 0) or (mask != 0):
                self.Log('error', f'chip{chip}, freeze {freeze}, mask 0x{mask:05x}')
            elif log:
                self.Log('info', f'chip{chip}, freeze {freeze}, mask 0x{mask:05x}')

    def check_reset_sync_channel_mask_value(self, metadata=''):
        reset_happend = False
        self.Log('info', metadata + f' vrange mask logging')
        for chip in range(5):
            freeze, mask = self.get_sync_channel_mask_value(chip)
            self.Log('info', f'chip{chip}, freeze {freeze}, mask 0x{mask:05x}')
        for chip in range(5):
            freeze, _ = self.get_sync_channel_mask_value(chip)
            if freeze:
                reset_happend = True
                self.Log('warning', metadata +
                         f' hc chip {chip} freeze bit high, start the reset')
                self.unfreeze_sync_channel_mask(chip)
        if reset_happend:
            self.Log('warning', f'after mask freeze reset')
            for chip in range(5):
                freeze, mask = self.get_sync_channel_mask_value(chip)
                self.Log('info', f'chip{chip}, freeze {freeze}, mask 0x{mask:05x}')

    def get_sync_channel_mask_value(self, chip):
        reg = self.read_bar_register(self.registers.HC_SYNC_CHANNEL_MASK, chip)
        freeze = reg.sync_channel_mask_freeze
        mask = reg.sync_channel_mask
        return freeze, mask

    def unfreeze_sync_channel_mask(self, chip):
        reg = self.read_bar_register(self.registers.HC_SYNC_CHANNEL_MASK, chip)
        reg.sync_channel_mask_freeze = 1
        self.write_bar_register(reg, chip)

    def get_last_sync_channel_mask(self, chip):
        reg = self.read_bar_register(self.registers.HC_USER_LAST_SYNC_CHANNEL_MASK, chip)
        last_mask = reg.sync_channel_mask
        return last_mask

    def get_sync_check_fail_alarm_value(self):
        reg = self.read_bar_register(self.registers.HC_VRANGE_SYNC_CHECK_FAIL_ALARM)
        sync_check_alarm = reg.rwc_sync_check_fail_alarm
        return sync_check_alarm
    # ganging sync vrange interface end

    # RC trigger interface START
    def send_time_reset_trigger_from_rctc_to_dps(self):
        operation = 0x5
        type = 0x05
        trigger_code = (type << 26) | operation
        self.rc_send_broadcast_trigger(trigger_code)

    def rc_send_broadcast_trigger(self, trigger):
        self.rc.send_broadcast_trigger(trigger)
    # RC trigger interface END

    # microsecond counter interface START
    def get_microsecond_counter_value(self):
        reg = self.read_bar_register(self.registers.US_COUNTER_VALUE)
        return reg.r_data
    # microsecond counter interface END

    def log_cpld_info(self):
        """
        •	Read CPLD version

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Passing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        cpld_version = hil.hbiDpsCpldVersionString(self.slot)
        self.slot_cpld_pair.update({self.slot: cpld_version})
        self.Log('info', 'HBIDPS CPLD Version for slot {}:- {}'.format(self.slot, cpld_version))

    def read_v_mon(self, channel):
        return hil.hbiDpsVmonRead(self.slot, channel)

    def read_t_mon(self, channel):
        return hil.hbiDpsTmonRead(self.slot, channel)

    def read_bar_register(self, register_type, index=0, slot=None):
        """Reads a register of the given type, the index is for register arrays"""
        reg_value = self.bar_read(register_type.BAR, register_type.address(index), slot)
        return register_type(value=reg_value)

    def get_max14662_shutdown_register(self):
        return self.read_bar_register(self.registers.MAX14662_SHUTDOWN_CONTROL)

    def set_max14662_shutdown_register(self, data):
        return self.write_bar_register(self.registers.MAX14662_SHUTDOWN_CONTROL(value=data.value))

    def bar_read(self, bar, offset, slot=None):
        if not slot:
            if slot != 0:
                slot = self.slot
        return hil.hbiDpsBarRead(slot, bar, offset)

    def write_bar_register(self, register, index=0, slot=None):
        """Writes a register of the given type, the index is for register arrays"""
        self.bar_write(register.BAR, register.address(index), register.value, slot)

    def bar_write(self, bar, offset, data, slot=None):
        if not slot:
            if slot != 0:
                slot = self.slot
        return hil.hbiDpsBarWrite(slot, bar, offset, data)

    def dma_read(self, address, length):
        return hil.hbiDpsDmaRead(self.slot, address, length)

    def write_ltm4680(self, chip, cmd, data,  page=-1, slot=None):
        if slot == None:
            slot = self.slot
        return hil.hbiDpsLtm4680PmbusWrite(slot, chip, page, cmd, data)

    def read_ltm4680(self, chip, cmd, length, page=-1, slot=None):
        if slot == None:
            slot = self.slot
        return hil.hbiDpsLtm4680PmbusRead(slot, chip, page, cmd, length)

    def write_ltc2975(self, chip, cmd, data, page=-1, slot=None):
        if slot == None:
            slot = self.slot
        return hil.hbiDpsLtc2975PmbusWrite(slot, chip, page, cmd,data)

    def read_ltc2975(self, chip, cmd, length, page=-1, slot=None):
        if slot == None:
            slot = self.slot
        return hil.hbiDpsLtc2975PmbusRead(slot, chip, page, cmd, length)

    def froml11(self, data_bytes):
        return hil.hilFromL11(data_bytes)

    def froml16(self, data_bytes, mode):
        return hil.hilFromL16(data_bytes, mode)

    def tol11(self, data_val):
        return hil.hilToL11(data_val)

    def tol16(self, data_val, mode):
        return hil.hilToL16(data_val, mode)

    def execute_trigger_queue(self, offset):
        trigger_register = self.registers.EXECUTE_TRIGGER()
        trigger_register.execute_trigger_address = offset
        self.write_bar_register(self.registers.EXECUTE_TRIGGER(value=trigger_register.value))

    def write_trigger_queue_to_memory(self, offset, data):
        self.dma_write(offset, data)

    def dma_write(self, address, data):
        return hil.hbiDpsDmaWrite(self.slot, address, data)

    def ltc2975_pmbus_write(self, chip, page, command, data):
        return hil.hbiDpsLtc2975PmbusWrite(self.slot, chip, page, command, data)

    def set_ltc2975_ov_limit(self, chip, page, limit):
        self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.VOUT_OV_FAULT_LIMIT, int.to_bytes(limit, 2, byteorder='little'))

    def set_ltc2975_ov_warn(self, chip, page, limit):
        self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.VOUT_OV_WARN_LIMIT, int.to_bytes(limit, 2, byteorder='little'))

    def set_ltc2975_oc_limit(self, chip, page, limit):
        self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.IOUT_OC_FAULT_LIMIT, int.to_bytes(limit, 2, byteorder='little'))

    def ltc2975_pmbus_read(self, chip, page, command, length):
        return hil.hbiDpsLtc2975PmbusRead(self.slot, chip, page, command, length)

    def hc_rail_voltage_read(self, channel):
        return hil.hbiDpsHcRailVoltageRead(self.slot, channel)

    def hc_rail_current_read(self, channel):
        return hil.hbiDpsHcRailCurrentRead(self.slot, channel)

    def lc_rail_voltage_read(self, channel):
        return hil.hbiDpsLcRailVoltageRead(self.slot, channel)

    def lc_rail_current_read(self, channel):
        return hil.hbiDpsLcRailCurrentRead(self.slot, channel)

    def hbi_dps_ad5676_write(self, chip_index, command, address, data):
        hil.hbiDpsAd5676Write(self.slot, chip_index, command, address, data)

    def hbi_dps_ad5676_read(self, chip_index, address):
        return hil.hbiDpsAd5676Read(self.slot, chip_index, address)

    def hbi_dps_vmeasure_voltage_read(self, channel):
        return hil.hbiDpsVMeasureVoltageRead(self.slot, channel)

    def conver_to_vtarget_dac_format(self, voltage):
        return round(voltage / 5.0 * 2 ** 16)

    def print_ltc2975_status_registers(self, chip, page):
        self.Log('info', f'LTC2975 Status Byte = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_BYTE, 1 ):02x}')
        self.Log('info', f'LTC2975 Status Word = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_WORD, 2 ):02x}')
        self.Log('info', f'LTC2975 Status V Out = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_VOUT, 1 ):02x}')
        self.Log('info', f'LTC2975 Status I Out = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_IOUT, 1 ):02x}')
        self.Log('info', f'LTC2975 Status Input = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_INPUT, 1 ):02x}')
        self.Log('info', f'LTC2975 Status Temperature = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_TEMPERATURE, 1 ):02x}')
        self.Log('info', f'LTC2975 Status CML = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_CML, 1 ):02x}')
        self.Log('info', f'LTC2975 Status Mfr Specific = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_MFR_SPECIFIC, 1 ):02x}')

    def print_ltc2975_limits(self, chip, page):
        self.Log('info', f'LTC2975 Status OV Limit = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.VOUT_OV_FAULT_LIMIT, 2 ):02x}')
        self.Log('info', f'LTC2975 Status UV Limit = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.VOUT_UV_FAULT_LIMIT, 2 ):02x}')
        self.Log('info', f'LTC2975 Status OV Warn = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.VOUT_OV_WARN_LIMIT, 2 ):02x}')
        self.Log('info', f'LTC2975 Status UV Warn = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.VOUT_UV_WARN_LIMIT, 2 ):02x}')
        self.Log('info', f'LTC2975 Status OC Limit = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.IOUT_OC_FAULT_LIMIT, 2 ):02x}')
        self.Log('info', f'LTC2975 Status OC Warn = '
                         f'0x{self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.IOUT_OC_WARN_LIMIT, 2 ):02x}')

    def print_all_2975_status_word(self):
        for chip in range(4):
            for page in range(4):
                self.print_ltc2975_status_registers(chip, page)

    def get_ltc2975_register(self, chip, page, register, register_size):
        data = self.ltc2975_pmbus_read(chip, page, register, register_size)
        dataint = int.from_bytes(data, byteorder='little')
        return dataint


    def clear_alarm_register(self):
        for chip in range(4):
            self.ltc2975_pmbus_write(chip, 0xff, symbols.PMBUSCOMMAND.CLEAR_FAULTS,
                                        symbols.LTC2975PMBUSDATA.CLEAR_FAULTS_DATA)
        time.sleep(.2)
        self.reset_global_alarm()

    def turn_off_all_LTC(self):
        operation = self.symbols.PMBUSCOMMAND.OPERATION
        turn_off_command = int.to_bytes(0x00, 1, byteorder='little')
        for chip in range(4):
            for page in range(4):
                hil.hbiDpsLtc2975PmbusWrite(self.slot, chip, page, operation, turn_off_command)

    def ltc2975_l11_format_from_int(self, number_to_convert):
        return hil.hilToL11(number_to_convert)

    def set_ltc2975_ton_fault_delay(self, chip, page, delay_in_ms):
        delay = self.ltc2975_l11_format_from_int(delay_in_ms)
        self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.TON_MAX_FAULT_LIMIT, delay)

    def clear_pmbus_rx_fifo(self):
        control_status = self.read_bar_register(self.registers.PMBUS_CONTROL_STATUS)
        if control_status.value != 0:
            for fifo_depth in range(2047):
                self.Log('debug', f'RX FIFO register status : {self.read_bar_register(self.registers.PMBUS_RX_FIFO).value}')

    def execute_pm_bus_command(self):
        pm_bus_control_status = self.registers.PMBUS_CONTROL_STATUS()
        pm_bus_control_status.go = 1
        self.write_bar_register(self.registers.PMBUS_CONTROL_STATUS(value=pm_bus_control_status.value))

    def initialize_ltc2975(self, chip):
        fault_propagation_delay_2975 = 0.5
        status_temperature_length = 1
        default_fault_status = b'\x00'
        ton_delay_in_ms = 200
        for page in range(4):
            self.set_ltc2975_ton_fault_delay(chip, page, ton_delay_in_ms)
            self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.OT_FAULT_LIMIT,
                                     symbols.LTC2975PMBUSDATA.OT_FAULT_LIMIT_DATA)
            time.sleep(fault_propagation_delay_2975)
            self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.CLEAR_FAULTS,
                                 symbols.LTC2975PMBUSDATA.CLEAR_FAULTS_DATA)
        for page in range(4):
            time.sleep(fault_propagation_delay_2975)
            fault_status = self.ltc2975_pmbus_read(chip, page, symbols.PMBUSCOMMAND.STATUS_TEMPERATURE,
                                                   status_temperature_length)
            if fault_status != default_fault_status:
                self.Log('warning',
                         f'Failed to clear Fault bits on LTC2975.Expected:{default_fault_status} '
                         f'Received :{fault_status} for device :{chip} page {page}')

    def initialization_trigger_que_ltc(self, lc_rail, rail_under_test):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        resourceid = 0x80
        railmask = 1 << rail_under_test
        rail = lc_rail
        write_protect = self.symbols.PMBUSCOMMAND.WRITE_PROTECT
        vin_on = self.symbols.PMBUSCOMMAND.VIN_ON
        vin_off = self.symbols.PMBUSCOMMAND.VIN_OFF
        vin_ov_fault_limit = self.symbols.PMBUSCOMMAND.VIN_OV_FAULT_LIMIT
        vin_ov_fault_response = self.symbols.PMBUSCOMMAND.VIN_OV_FAULT_RESPONSE
        vin_ov_warn_limit = self.symbols.PMBUSCOMMAND.VIN_OV_WARN_LIMIT
        vin_uv_warn_limit = self.symbols.PMBUSCOMMAND.VIN_UV_WARN_LIMIT
        vin_uv_fault_limit = self.symbols.PMBUSCOMMAND.VIN_UV_FAULT_LIMIT
        vin_uv_fault_response = self.symbols.PMBUSCOMMAND.VIN_UV_FAULT_RESPONSE
        user_data_00 = self.symbols.PMBUSCOMMAND.USER_DATA_00
        user_data_02 = self.symbols.PMBUSCOMMAND.USER_DATA_02
        user_data_04 = self.symbols.PMBUSCOMMAND.USER_DATA_04_SCRATCH_PAD
        mfr_ein_config = self.symbols.PMBUSCOMMAND.MFR_EIN_CONFIG
        mfr_iin_cal_gain_tc = self.symbols.PMBUSCOMMAND.MFR_IIN_CAL_GAIN_TC
        mfr_config_all_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG_ALL_LTC2975
        mfr_pwrgd_en = self.symbols.PMBUSCOMMAND.MFR_PWRGD_EN
        mfr_faultB0_response = self.symbols.PMBUSCOMMAND.MFR_FAULTB0_RESPONSE
        mfr_faultB1_response = self.symbols.PMBUSCOMMAND.MFR_FAULTB1_RESPONSE
        mfr_config2_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG2_LTC2975
        mfr_config3_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG3_LTC2975
        mfr_retry_delay = self.symbols.PMBUSCOMMAND.MFR_RETRY_DELAY
        mfr_restart_delay = self.symbols.PMBUSCOMMAND.MFR_RESTART_DELAY
        mfr_powergood_assertion_delay = self.symbols.PMBUSCOMMAND.MFR_POWERGOOD_ASSERTION_DELAY
        mfr_watchdog_t_first = self.symbols.PMBUSCOMMAND.MFR_WATCHDOG_T_FIRST
        mfr_watchdog_t = self.symbols.PMBUSCOMMAND.MFR_WATCHDOG_T
        mfr_page_ff_mask = self.symbols.PMBUSCOMMAND.MFR_PAGE_FF_MASK
        mfr_iin_cal_gain = self.symbols.PMBUSCOMMAND.MFR_IIN_CAL_GAIN
        mfr_retry_count = self.symbols.PMBUSCOMMAND.MFR_RETRY_COUNT
        tq_string = f'''
                       WakeRail resourceid ={resourceid},  railmask ={railmask}
                       SetRailBusy resourceid ={resourceid}, railmask ={railmask}
                       WriteByte rail={rail} ,value= 0x00, pmbuscmd = {write_protect} 
                       WriteWord rail={rail}, value = 0xD240, pmbuscmd = {vin_on}
                       WriteWord rail={rail}, value = 0xD200 , pmbuscmd = {vin_off}
                       WriteWord rail={rail}, value = 0xD3C0 , pmbuscmd = {vin_ov_fault_limit}
                       WriteByte rail={rail}, value = 0x80, pmbuscmd = {vin_ov_fault_response}
                       WriteWord rail={rail}, value = 0xD380 , pmbuscmd = {vin_ov_warn_limit}
                       WriteWord rail={rail}, value = 0x8000 , pmbuscmd = {vin_uv_warn_limit}
                       WriteWord rail={rail}, value = 0x8000 , pmbuscmd = {vin_uv_fault_limit}
                       WriteByte rail={rail}, value = 0x80, pmbuscmd = {vin_uv_fault_response}
                       WriteWord rail={rail}, value = 0x0000, pmbuscmd = {user_data_00}
                       WriteWord rail={rail}, value = 0x0000, pmbuscmd = {user_data_02}
                       WriteWord rail={rail}, value = 0xC3F5, pmbuscmd = {user_data_04}
                       WriteByte rail={rail}, value = 0x00, pmbuscmd = {mfr_ein_config}
                       WriteWord rail={rail}, value = 0x0000, pmbuscmd = {mfr_iin_cal_gain_tc}
                       WriteWord rail={rail}, value = 0x0F7B, pmbuscmd = {mfr_config_all_ltc2975}
                       WriteWord rail={rail}, value = 0x0000, pmbuscmd = {mfr_pwrgd_en}
                       WriteByte rail={rail}, value = 0x00, pmbuscmd = {mfr_faultB0_response}
                       WriteByte rail={rail}, value = 0x00, pmbuscmd = {mfr_faultB1_response}    
                       WriteByte rail={rail}, value = 0x00, pmbuscmd = {mfr_config2_ltc2975}
                       WriteByte rail={rail}, value = 0x00, pmbuscmd = {mfr_config3_ltc2975}
                       WriteWord rail={rail}, value = {int.from_bytes(hil.hilToL11(200), byteorder='big')}, pmbuscmd = {mfr_retry_delay}
                       WriteWord rail={rail}, value = {int.from_bytes(hil.hilToL11(400), byteorder='big')}, pmbuscmd = {mfr_restart_delay}
                       WriteWord rail={rail}, value = {int.from_bytes(hil.hilToL11(100), byteorder='big')}, pmbuscmd = {mfr_powergood_assertion_delay}
                       WriteWord rail={rail}, value = {int.from_bytes(hil.hilToL11(0), byteorder='big')}, pmbuscmd = {mfr_watchdog_t_first}
                       WriteWord rail={rail}, value = {int.from_bytes(hil.hilToL11(0), byteorder='big')}, pmbuscmd = {mfr_watchdog_t}
                       WriteByte rail={rail}, value = 0x0F, pmbuscmd = {mfr_page_ff_mask}
                       WriteWord rail={rail}, value = {int.from_bytes(hil.hilToL11(10), byteorder='big')}, pmbuscmd = {mfr_iin_cal_gain} 
                       WriteByte rail={rail}, value = 0x07, pmbuscmd = {mfr_retry_count}
                       TimeDelay rail={rail}, value = 0x186A0
                       TqNotify resourceid ={resourceid}, railmask ={railmask}
                       TqComplete rail ={rail}, value = 0
                 '''

        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        self.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        self.execute_trigger_queue(trigger_queue_offset)

    def paged_initialization_trigger_que_ltc(self, lc_rail, rail_under_test, page_num):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        rail = lc_rail
        resourceid = 0x80
        railmask = 1 << rail_under_test
        page_cmd = self.symbols.PMBUSCOMMAND.PAGE_CMD
        on_off_config = self.symbols.PMBUSCOMMAND.ON_OFF_CONFIG
        iout_cal_gain = self.symbols.PMBUSCOMMAND.IOUT_CAL_GAIN
        mfr_iout_cal_gain_tau_inv = self.symbols.PMBUSCOMMAND.MFR_IOUT_CAL_GAIN_TAU_INV
        mfr_iout_cal_gain_theta = self.symbols.PMBUSCOMMAND.MFR_IOUT_CAL_GAIN_THETA
        mfr_config_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG_LTC2975
        mfr_faultb0_propagate = self.symbols.PMBUSCOMMAND.MFR_FAULTB0_PROPAGATE
        mfr_faultb1_propagate = self.symbols.PMBUSCOMMAND.MFR_FAULTB1_PROPAGATE
        ut_fault_response = self.symbols.PMBUSCOMMAND.UT_FAULT_RESPONSE
        ot_fault_response = self.symbols.PMBUSCOMMAND.OT_FAULT_RESPONSE
        iout_uc_fault_response = self.symbols.PMBUSCOMMAND.IOUT_UC_FAULT_RESPONSE
        vout_uv_fault_response = self.symbols.PMBUSCOMMAND.VOUT_UV_FAULT_RESPONSE
        iout_oc_fault_response = self.symbols.PMBUSCOMMAND.IOUT_OC_FAULT_RESPONSE
        vout_ov_fault_response = self.symbols.PMBUSCOMMAND.VOUT_OV_FAULT_RESPONSE
        ton_max_fault_response = self.symbols.PMBUSCOMMAND.TON_MAX_FAULT_RESPONSE
        ot_fault_limit = self.symbols.PMBUSCOMMAND.OT_FAULT_LIMIT
        ot_warn_limit = self.symbols.PMBUSCOMMAND.OT_WARN_LIMIT
        ut_warn_limit = self.symbols.PMBUSCOMMAND.UT_WARN_LIMIT
        ut_fault_limit = self.symbols.PMBUSCOMMAND.UT_FAULT_LIMIT
        vout_ov_warn_limit = self.symbols.PMBUSCOMMAND.VOUT_OV_WARN_LIMIT
        vout_uv_warn_limit = self.symbols.PMBUSCOMMAND.VOUT_UV_WARN_LIMIT
        iout_oc_warn_limit = self.symbols.PMBUSCOMMAND.IOUT_OC_WARN_LIMIT
        vout_ov_fault_limit = self.symbols.PMBUSCOMMAND.VOUT_OV_FAULT_LIMIT
        vout_uv_fault_limit = self.symbols.PMBUSCOMMAND.VOUT_UV_FAULT_LIMIT
        toff_delay = self.symbols.PMBUSCOMMAND.TOFF_DELAY
        ton_delay = self.symbols.PMBUSCOMMAND.TON_DELAY
        ton_rise = self.symbols.PMBUSCOMMAND.TON_RISE
        vout_max = self.symbols.PMBUSCOMMAND.VOUT_MAX
        vout_command = self.symbols.PMBUSCOMMAND.VOUT_COMMAND
        clear_faults = self.symbols.PMBUSCOMMAND.CLEAR_FAULTS
        power_good_on = self.symbols.PMBUSCOMMAND.POWER_GOOD_ON
        power_good_off = self.symbols.PMBUSCOMMAND.POWER_GOOD_OFF
        L16_mode = 0x13
        tq_string = f'''
                      WakeRail resourceid ={resourceid},  railmask ={railmask}
                      SetRailBusy resourceid ={resourceid}, railmask ={railmask}
                      WriteByte rail={rail}, value={page_num} ,pmbuscmd = {page_cmd}
                      WriteByte rail={rail}, value = 0x1C, pmbuscmd = {on_off_config} 
                      SetPwrState rail={rail}, value = 0
                      WriteWord rail={rail}, value = 0xD280, pmbuscmd = {iout_cal_gain}
                      
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(0.6, L16_mode), byteorder='big')}, pmbuscmd = {power_good_on}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(0.55, L16_mode), byteorder='big')}, pmbuscmd = {power_good_off}
                      
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(0), byteorder='big')}, pmbuscmd = {mfr_iout_cal_gain_tau_inv}   
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(1), byteorder='big')}, pmbuscmd = {mfr_iout_cal_gain_theta}
                      WriteWord rail={rail}, value =0x0420, pmbuscmd = {mfr_config_ltc2975}
                      WriteByte rail={rail}, value = 0x01, pmbuscmd = {mfr_faultb0_propagate}
                      WriteByte rail={rail}, value = 0x01, pmbuscmd = {mfr_faultb1_propagate}
                      WriteByte rail={rail}, value = 0x80, pmbuscmd = {ut_fault_response}
                      WriteByte rail={rail}, value = 0x80, pmbuscmd = {ot_fault_response}
                      WriteByte rail={rail}, value = 0x84, pmbuscmd = {iout_uc_fault_response}
                      WriteByte rail={rail}, value = 0x80, pmbuscmd = {vout_uv_fault_response}
                      WriteByte rail={rail}, value = 0x84, pmbuscmd = {iout_oc_fault_response}
                      WriteByte rail={rail}, value = 0x80, pmbuscmd = {vout_ov_fault_response}
                      WriteByte rail={rail}, value = 0x80, pmbuscmd = {ton_max_fault_response}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(85), byteorder='big')}, pmbuscmd = {ot_fault_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(90), byteorder='big')}, pmbuscmd = {ot_warn_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(-10), byteorder='big')}, pmbuscmd = {ut_warn_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(-5), byteorder='big')}, pmbuscmd = {ut_fault_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(7, L16_mode), byteorder='big')}, pmbuscmd = {vout_ov_warn_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(0, L16_mode), byteorder='big')}, pmbuscmd = {vout_uv_warn_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(7), byteorder='big')}, pmbuscmd = {iout_oc_warn_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(7, L16_mode), byteorder='big')}, pmbuscmd = {vout_ov_fault_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(0, L16_mode), byteorder='big')}, pmbuscmd = {vout_uv_fault_limit}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(0), byteorder='big')}, pmbuscmd = {toff_delay}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(0), byteorder='big')}, pmbuscmd = {ton_delay}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL11(0), byteorder='big')}, pmbuscmd = {ton_rise}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(6, L16_mode), byteorder='big')}, pmbuscmd = {vout_max}
                      WriteWord rail={rail}, value ={int.from_bytes(hil.hilToL16(5, L16_mode), byteorder='big')}, pmbuscmd = {vout_command}
                      SendByte rail = {rail}, value = 0 ,pmbuscmd = {clear_faults}
                      TimeDelay rail ={rail}, value = 0x186A0 
                      TqNotify resourceid ={resourceid}, railmask ={railmask}
                      TqComplete rail ={rail}, value = 0                                      
                '''

        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        self.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        self.execute_trigger_queue(trigger_queue_offset)

    def send_rc_trigger(self, trigger):
        self.rc.send_trigger(trigger, self.slot)

    def rc_log_status(self, verbosity='debug'):
        self.rc.log_aurora_status(verbosity)

    def get_rc_trigger(self, trigger_from_dps):
        for trigger_read_loop in range(1000):
            trigger_value = self.rc.read_trigger(self.slot)
            if trigger_value == trigger_from_dps:
                break
        return trigger_value

    def reset_rc_trigger(self):
        self.rc.reset_sw_trigger_fifo()

    def get_rc_sw_trigger_count(self):
        return self.rc.read_sw_trigger_fifo_count()

    def check_rc_sw_trigger_source(self):
        self.rc.check_sw_trigger_fifo_source(self.slot)

    def reset_rc_aurora_bus(self):
        self.rc.reset_rctc_aurora_link(self.slot)

    def initialize_trigger_link(self):
        return_status = False
        # self.reset_rc_aurora_bus()
        for slots in self.dps_slot_list:
            for reset_loop in range(self.retry_count):
                aurora_error_count, aurora_status, slots = self.get_aurora_status(slots)
                aurora_status = aurora_status.value & 0x0000000f
                if aurora_status != 0x0 or aurora_error_count.value != 0:
                    self.Log('info', f'Aurora link not up from DPS for slot {slots} at iteration {reset_loop}. '
                                     f'Aurora status :0x{aurora_status:x} Aurora error count '
                                     f':0x{aurora_error_count.value:x}')
                    if aurora_error_count.value != 0:
                        self.reset_dps_error_count(slots)
                    aurora_error_count, aurora_status, slots = self.get_aurora_status(slots)
                    aurora_status = aurora_status.value & 0x0000000f
                    self.Log('info', f'aurora_error_count 0x{aurora_error_count.value:x} '
                                     f'aurora_status {aurora_status} slots {slots}')
                    if aurora_status != 0x0 or aurora_error_count.value != 0:
                        self.reset_dps_aurora_link(slots)
                    # time.sleep(0.5)
                else:
                    self.Log('info', f'Aurora link up from DPS at slot {slots} at iteration {reset_loop}. '
                                     f'Aurora status :0x{aurora_status:x} Aurora error count'
                                     f' :0x{aurora_error_count.value:x} ')
                    return_status = True
                    break
            self.clear_dps_trigger_buffer(slots)
        if not return_status:
            self.Log('error', 'Aurora link failed to come up.')
        return return_status

    def clear_dps_trigger_buffer(self, slots):
        trigger_fifo = self.read_bar_register(self.registers.TRIG_RX_TEST_FIFO_DATA_COUNT, slot=slots)
        trig_value = self.read_bar_register(self.registers.LAST_TRIGGER_SEEN, slot=slots)
        for loop in range(512):
            if trigger_fifo.r_rx_fifo_count != 0 and trig_value.value != 0:
                trig_value = self.read_bar_register(self.registers.LAST_TRIGGER_SEEN, slot=slots)
                trigger_fifo = self.read_bar_register(self.registers.TRIG_RX_TEST_FIFO_DATA_COUNT, slot=slots)
            else:
                break

    def get_aurora_status(self, slots=None):
        if not slots:
            if slots != 0:
                slots = self.slot
        aurora_status_after = self.read_bar_register(self.registers.STATUS, slot=slots)
        aurora_error_count_after = self.read_bar_register(self.registers.AURORA_ERROR_COUNTS, slot=slots)
        return aurora_error_count_after, aurora_status_after, slots

    def get_rctc_aurora_status(self):
        for slots in self.dps_slot_list:
            rctc_link_up, rctc_aurora_status, rctc_aurora_error_count = self.rc.is_aurora_link_up(slots)
            if rctc_link_up == False:
                self.Log('error', f'is RCTC Aurora link up: {rctc_link_up} Aurora Status 0x{rctc_aurora_status.value:x}'
                                  f' Aurora_error_count: 0x{rctc_aurora_error_count.value:x} Slot: {slots}')
            else:
                self.Log('info', f'is RCTC Aurora link up: {rctc_link_up} Aurora Status 0x{rctc_aurora_status.value:x} '
                                 f'Aurora_error_count: 0x{rctc_aurora_error_count.value:x} Slot: {slots}')

    def reset_dps_aurora_link(self, slots=None):
        if not slots:
            if slots != 0:
                slots = self.slot
        reset_reg = self.registers.RESETS(value=0x2)
        error_count = self.registers.AURORA_ERROR_COUNTS(value=0xffffffff)
        self.write_bar_register(reset_reg, slot=slots)
        self.write_bar_register(error_count, slot=slots)
        rctc_trigger_register = self.rc.registers.AURORA_CONTROL()
        self.Log('info', f'Resetting RCTC Aurora link for DPS Slot {slots}')
        self.rc.reset_rctc_link(slots)
        time.sleep(0.001)

    def reset_dps_error_count(self, slots=None):
        if not slots:
            if slots != 0:
                slots = self.slot
        self.Log('info', f'Resetting DPS Eror count for slot {slots}')
        error_count = self.registers.AURORA_ERROR_COUNTS(value=0xffffffff)
        self.write_bar_register(error_count, slot=slots)

    def get_random_voltage(self, low_voltage, high_voltage):
        random_force_voltage = random.uniform(low_voltage, high_voltage)
        force_voltage = round(random_force_voltage, 2)
        return force_voltage

    def write_lcm_gang_max14662(self, chip, data):
        hil.hbiDpsLcmGangMax14662Write(self.slot, chip, data)

    def read_lcm_gang_max14662(self, chip):
        return hil.hbiDpsLcmGangMax14662Read(self.slot, chip)

    def lc_gang_comp_write(self, chip, data):
        self.write_lcm_gang_max14662(get_comp_gpio(chip), data)

    def lc_gang_track_write(self, chip, data):
        self.write_lcm_gang_max14662(get_track_gpio(chip), data)

    def write_vsense_max14662(self, chip, data):
        hil.hbiDpsISourceVsenseMax14662Write(self.slot, chip, data)

    def read_vsense_max14662(self, chip):
        return hil.hbiDpsISourceVsenseMax14662Read(self.slot, chip)

    def write_lb_max14662(self, chip, data):
        hil.hbiDpsVtVmLbMax14662Write(self.slot, chip, data)

    def read_lb_max14662(self, chip):
        return hil.hbiDpsVtVmLbMax14662Read(self.slot, chip)

    def a10_decimal_to_celsius(self, raw_temperature):
        # Conversion formula from Arria 10
        # Temperature = {(A x C )/1024 - B} :Where A = 693/ B = 265/ C = decimal value of tempout [9:0]
        a = 693
        b = 265
        test = (a * raw_temperature) / 1024
        return test - b

    def hil_version(self):
        """
        •	Read HIL Version

        +---------------+-------------------+-----------------------+---------------+
        | Milestone     | TestArea          | TestImplementation    | TestStatus    |
        +===============+===================+=======================+===============+
        | PowerOn       | Infrastructure    | Yes                   | Passing       |
        +---------------+-------------------+-----------------------+---------------+
        """
        self.Log('info', f'HIL version in use : {hil.hilVersionString()}')

    def print_slot_under_test(self):
        self.Log('info', f'{self.name_with_slot()} is under test')

    def set_attribute(self, channel, attribute, value):
        channel.SetAttribute(attribute, value)

    def set_attributes(self, channel, attributeMap):
        channel.SetAttributes(attributeMap)

    def apply_power(self, channel):
        channel.Apply()

    def check_alarm(self, channel):
        return channel.CheckStatus()

    def get_results(self, channel):
        return channel.GetResults()

    def get_attribute_from_hw(self, channel):
        return channel.GetAttributesFromHw()

    def safe_state(self, channel):
        channel.Safestate()

    def get_triggr_at_all_dps_instruments(self, trigger_from_dps):
        slot_trigger_pair = {}
        for slots in self.dps_slot_list:
            trigger_value = self.read_dps_trigger(trigger_from_dps, slots=slots)
            slot_trigger_pair.update({slots: trigger_value.value})
        return slot_trigger_pair

    def read_dps_trigger(self, trigger_from_dps, slots=None):
        if not slots:
            if slots != 0:
                slots = self.slot
        for trigger_read_loop in range(1000):
            trigger_value = self.read_bar_register(self.registers.LAST_TRIGGER_SEEN, slot=slots)
            if trigger_value.value == trigger_from_dps:
                self.Log('debug',
                         'Trigger value match at attempt {} for slot {} received 0x{:X} expected 0x{:x}'.format(
                             trigger_read_loop, slots, trigger_value.value, trigger_from_dps))
                break
            else:
                self.Log('debug',
                         'Trigger value mis match at attempt {} for slot {} received 0x{:x} expected 0x{:x}'.format(
                             trigger_read_loop, slots, trigger_value.value, trigger_from_dps))
        return trigger_value

    def is_pm_bus_busy(self):
        pm_bus_status = False
        for polling in range(10):
            if self.read_bar_register(self.registers.PMBUS_CONTROL_STATUS).busy:
                time.sleep(0.001)
            else:
                pm_bus_status = True
                break
        return pm_bus_status

    def is_trigger_busy(self):
        trigger_status = False
        for polling in range(10):
            if self.read_bar_register(self.registers.EXECUTE_TRIGGER).trigger_busy:
                time.sleep(0.001)
            else:
                pm_bus_status = True
                break
        return trigger_status

    def wait_for_ltc2975(self, chip, page, iterations):
        for polling in range(iterations):
            if self.get_ltc2975_register(chip, page, symbols.PMBUSCOMMAND.STATUS_BYTE, 1) != 0x00:
                time.sleep(.01)
            else:
                return True
        return False

    def wait_for_trigger(self, iterations):
        for polling in range(iterations):
            if not(self.is_trigger_busy()):
                return True
        return False

    def discover(self):
        try:
            self.open_pcie_device()
            self.Log('info', 'Found {}'.format(self.name_with_slot()))
        except RuntimeError:
            self.Log('warning',
                     '{}: Unable to find FPGA using pciDeviceOpen HIL API. Try enabling it in the Device Manager.'.format(
                         self.name_with_slot()))
            return None
        return self

    def name_with_slot(self):
        return f'HBIDPS{self.slot:02}'

    def print_ltc2975_internal_control(self, chip, page):
        self.Log('info', f'LTC internal control: {self.ltc2975_pmbus_read(chip, page, symbols.PMBUSCOMMAND.ON_OFF_CONFIG, 1)}')

    def set_ltc2975_internal_control(self, chip, page, command):
        self.ltc2975_pmbus_write(chip, page, symbols.PMBUSCOMMAND.ON_OFF_CONFIG, command)

    def init_ltc_device(self, target_pmbus_device, page_num):
        write_protect = self.symbols.PMBUSCOMMAND.WRITE_PROTECT
        vin_on = self.symbols.PMBUSCOMMAND.VIN_ON
        vin_off = self.symbols.PMBUSCOMMAND.VIN_OFF
        vin_ov_fault_limit = self.symbols.PMBUSCOMMAND.VIN_OV_FAULT_LIMIT
        vin_ov_fault_response = self.symbols.PMBUSCOMMAND.VIN_OV_FAULT_RESPONSE
        vin_ov_warn_limit = self.symbols.PMBUSCOMMAND.VIN_OV_WARN_LIMIT
        vin_uv_warn_limit = self.symbols.PMBUSCOMMAND.VIN_UV_WARN_LIMIT
        vin_uv_fault_limit = self.symbols.PMBUSCOMMAND.VIN_UV_FAULT_LIMIT
        vin_uv_fault_response = self.symbols.PMBUSCOMMAND.VIN_UV_FAULT_RESPONSE
        user_data_00 = self.symbols.PMBUSCOMMAND.USER_DATA_00
        user_data_02 = self.symbols.PMBUSCOMMAND.USER_DATA_02
        user_data_04 = self.symbols.PMBUSCOMMAND.USER_DATA_04_SCRATCH_PAD
        mfr_ein_config = self.symbols.PMBUSCOMMAND.MFR_EIN_CONFIG
        mfr_iin_cal_gain_tc = self.symbols.PMBUSCOMMAND.MFR_IIN_CAL_GAIN_TC
        mfr_config_all_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG_ALL_LTC2975
        mfr_pwrgd_en = self.symbols.PMBUSCOMMAND.MFR_PWRGD_EN
        mfr_faultB0_response = self.symbols.PMBUSCOMMAND.MFR_FAULTB0_RESPONSE
        mfr_faultB1_response = self.symbols.PMBUSCOMMAND.MFR_FAULTB1_RESPONSE
        mfr_config2_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG2_LTC2975
        mfr_config3_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG3_LTC2975
        mfr_retry_delay = self.symbols.PMBUSCOMMAND.MFR_RETRY_DELAY
        mfr_restart_delay = self.symbols.PMBUSCOMMAND.MFR_RESTART_DELAY
        mfr_powergood_assertion_delay = self.symbols.PMBUSCOMMAND.MFR_POWERGOOD_ASSERTION_DELAY
        mfr_watchdog_t_first = self.symbols.PMBUSCOMMAND.MFR_WATCHDOG_T_FIRST
        mfr_watchdog_t = self.symbols.PMBUSCOMMAND.MFR_WATCHDOG_T
        mfr_page_ff_mask = self.symbols.PMBUSCOMMAND.MFR_PAGE_FF_MASK
        mfr_iin_cal_gain = self.symbols.PMBUSCOMMAND.MFR_IIN_CAL_GAIN
        mfr_retry_count = self.symbols.PMBUSCOMMAND.MFR_RETRY_COUNT
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, write_protect,
                                    int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_on,
                                    int.to_bytes(0xD240, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_off,
                                    int.to_bytes(0xD200, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_ov_fault_limit,
                                    int.to_bytes(0xD3C0, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_ov_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_ov_warn_limit,
                                    int.to_bytes(0xD380, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_uv_warn_limit,
                                    int.to_bytes(0x8000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_uv_fault_limit,
                                    int.to_bytes(0x8000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vin_uv_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, user_data_00,
                                    int.to_bytes(0x0000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, user_data_02,
                                    int.to_bytes(0x0000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, user_data_04,
                                    int.to_bytes(0xC3F5, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_ein_config,
                                    int.to_bytes(0x0000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_iin_cal_gain_tc,
                                    int.to_bytes(0x0000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_config_all_ltc2975,
                                    int.to_bytes(0x0F7B, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_pwrgd_en,
                                    int.to_bytes(0x0000, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_faultB0_response,
                                    int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_faultB1_response,
                                    int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_config2_ltc2975,
                                    int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_config3_ltc2975,
                                    int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_retry_delay, hil.hilToL11(200))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_restart_delay, hil.hilToL11(400))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_powergood_assertion_delay,
                                    hil.hilToL11(100))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_watchdog_t_first, hil.hilToL11(0))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_watchdog_t, hil.hilToL11(0))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_page_ff_mask,
                                    int.to_bytes(0x0F, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_iin_cal_gain, hil.hilToL11(10))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_retry_count,
                                    int.to_bytes(0x07, 1, byteorder='little'))

    def init_paged_ltc_device(self, target_pmbus_device, page_num):
        page_cmd = self.symbols.PMBUSCOMMAND.PAGE_CMD
        on_off_config = self.symbols.PMBUSCOMMAND.ON_OFF_CONFIG
        iout_cal_gain = self.symbols.PMBUSCOMMAND.IOUT_CAL_GAIN
        mfr_iout_cal_gain_tau_inv = self.symbols.PMBUSCOMMAND.MFR_IOUT_CAL_GAIN_TAU_INV
        mfr_iout_cal_gain_theta = self.symbols.PMBUSCOMMAND.MFR_IOUT_CAL_GAIN_THETA
        mfr_config_ltc2975 = self.symbols.PMBUSCOMMAND.MFR_CONFIG_LTC2975
        mfr_faultb0_propagate = self.symbols.PMBUSCOMMAND.MFR_FAULTB0_PROPAGATE
        mfr_faultb1_propagate = self.symbols.PMBUSCOMMAND.MFR_FAULTB1_PROPAGATE
        ut_fault_response = self.symbols.PMBUSCOMMAND.UT_FAULT_RESPONSE
        ot_fault_response = self.symbols.PMBUSCOMMAND.OT_FAULT_RESPONSE
        iout_uc_fault_response = self.symbols.PMBUSCOMMAND.IOUT_UC_FAULT_RESPONSE
        vout_uv_fault_response = self.symbols.PMBUSCOMMAND.VOUT_UV_FAULT_RESPONSE
        iout_oc_fault_response = self.symbols.PMBUSCOMMAND.IOUT_OC_FAULT_RESPONSE
        vout_ov_fault_response = self.symbols.PMBUSCOMMAND.VOUT_OV_FAULT_RESPONSE
        ton_max_fault_response = self.symbols.PMBUSCOMMAND.TON_MAX_FAULT_RESPONSE
        ot_fault_limit = self.symbols.PMBUSCOMMAND.OT_FAULT_LIMIT
        ot_warn_limit = self.symbols.PMBUSCOMMAND.OT_WARN_LIMIT
        ut_warn_limit = self.symbols.PMBUSCOMMAND.UT_WARN_LIMIT
        ut_fault_limit = self.symbols.PMBUSCOMMAND.UT_FAULT_LIMIT
        vout_ov_warn_limit = self.symbols.PMBUSCOMMAND.VOUT_OV_WARN_LIMIT
        vout_uv_warn_limit = self.symbols.PMBUSCOMMAND.VOUT_UV_WARN_LIMIT
        iout_oc_warn_limit = self.symbols.PMBUSCOMMAND.IOUT_OC_WARN_LIMIT
        vout_ov_fault_limit = self.symbols.PMBUSCOMMAND.VOUT_OV_FAULT_LIMIT
        vout_uv_fault_limit = self.symbols.PMBUSCOMMAND.VOUT_UV_FAULT_LIMIT
        toff_delay = self.symbols.PMBUSCOMMAND.TOFF_DELAY
        ton_delay = self.symbols.PMBUSCOMMAND.TON_DELAY
        ton_rise = self.symbols.PMBUSCOMMAND.TON_RISE
        vout_max = self.symbols.PMBUSCOMMAND.VOUT_MAX
        vout_command = self.symbols.PMBUSCOMMAND.VOUT_COMMAND
        clear_faults = self.symbols.PMBUSCOMMAND.CLEAR_FAULTS
        power_good_on = self.symbols.PMBUSCOMMAND.POWER_GOOD_ON
        power_good_off = self.symbols.PMBUSCOMMAND.POWER_GOOD_OFF
        operation = self.symbols.PMBUSCOMMAND.OPERATION
        L16_mode = 0x13
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, page_cmd,
                                    int.to_bytes(page_num, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, on_off_config,
                                    int.to_bytes(0x1C, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, operation,
                                    int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, iout_cal_gain,
                                    int.to_bytes(0xD280, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, power_good_on,
                                    hil.hilToL16(0.6, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, power_good_off,
                                    hil.hilToL16(0.55, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_iout_cal_gain_tau_inv,
                                    hil.hilToL11(0))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_iout_cal_gain_theta,
                                    hil.hilToL11(1))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_config_ltc2975,
                                    int.to_bytes(0x0420, 2, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_faultb0_propagate,
                                    int.to_bytes(0x01, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, mfr_faultb1_propagate,
                                    int.to_bytes(0x01, 1, byteorder='little'))
        time.sleep(0.001)
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ut_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ot_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, iout_uc_fault_response,
                                    int.to_bytes(0x84, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_uv_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, iout_oc_fault_response,
                                    int.to_bytes(0x84, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_ov_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ton_max_fault_response,
                                    int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ot_fault_limit, hil.hilToL11(85))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ot_warn_limit, hil.hilToL11(90))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ut_warn_limit, hil.hilToL11(-10))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ut_fault_limit, hil.hilToL11(-5))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_ov_warn_limit,
                                    hil.hilToL16(7, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_uv_warn_limit,
                                    hil.hilToL16(0, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, iout_oc_warn_limit, hil.hilToL11(7))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_ov_fault_limit,
                                    hil.hilToL16(7, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_uv_fault_limit,
                                    hil.hilToL16(0, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, toff_delay, hil.hilToL11(0))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ton_delay, hil.hilToL11(0))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, ton_rise, hil.hilToL11(0))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_max, hil.hilToL16(6, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, vout_command,
                                    hil.hilToL16(5, L16_mode))
        hil.hbiDpsLtc2975PmbusWrite(self.slot, target_pmbus_device, page_num, clear_faults,
                                  int.to_bytes(0x00, 1, byteorder='little'))
