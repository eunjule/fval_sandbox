# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

def float_to_L11(fp):
    Y = fp
    N = 0

    # shift the mantissa as far as possible, to get best precision
    while (-512 <= Y < 512) and (-16 < N):
        Y *= 2
        N -= 1
    Y = int(Y)

    # fix mantissa overflow
    while (Y < -1024) or (Y > 1023):
        Y = Y // 2
        N += 1

    # convert to two's complement
    if Y < 0:
        Y = Y + 2048
    if N < 0:
        N = N + 32

    # package into 16-bit value
    return (N << 11) + Y


def L11_to_float(data):
    assert 0 <= data <= 0xFFFF

    # extract mantissa and exponent
    Y = data & 0x7FF
    N = data >> 11

    # convert mantissa and exponenet to two's complement
    if Y > 1023:
        Y = Y - 2048
    if N > 15:
        N = N - 32

    # create floating point value
    return Y * 2**N

