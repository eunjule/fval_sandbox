# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class SampleEngine(Object):

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.regs = hbidps_register

    def read_lc_one_rail_polling_voltage(self, rail):
        reg = self.hbidps.read_bar_register(self.regs.LC_CHANNEL_VOLTAGE,
                                            index=rail)
        return self.hbidps.decode_single_precision_floating_value(reg.value)

    def read_lc_one_rail_polling_current(self, rail):
        reg = self.hbidps.read_bar_register(self.regs.LC_CHANNEL_CURRENT,
                                            index=rail)
        return self.hbidps.decode_single_precision_floating_value(reg.value)

    def read_hc_one_rail_polling_voltage(self, rail):
        reg = self.hbidps.read_bar_register(self.regs.HC_CHANNEL_VOLTAGE,
                                            index=rail)
        return self.hbidps.decode_single_precision_floating_value(reg.value)

    def read_hc_one_rail_polling_current(self, rail):
        reg = self.hbidps.read_bar_register(self.regs.HC_CHANNEL_CURRENT,
                                            index=rail)
        return self.hbidps.decode_single_precision_floating_value(reg.value)

    def read_vm_one_rail_polling_voltage(self, rail):
        reg = self.hbidps.read_bar_register(self.regs.AD7616_CHANNEL_VM_ADC,
                                            index=rail)
        return self.hbidps.decode_single_precision_floating_value(reg.value)

    def get_single_sample_engine_settings(self, type, index):
        rail_select = self.read_sample_rail_select_register(type, index)
        sample_count = self.read_sample_count_register(type, index)
        sample_ratio = self.read_sample_ratio_register(type, index)
        sample_metadata = self.read_sample_metadata_register(type, index)
        return rail_select, sample_count, sample_ratio, sample_metadata

    def log_single_sample_engine_settings(self, type, index):
        rail_select, sample_count, sample_ratio, sample_metadata = \
            self.get_single_sample_engine_settings(type, index)
        self.Log('info', f'rail_select: {rail_select}; '
                 f'sample_count: {sample_count}; '
                 f'sample_ratio: {sample_ratio}; '
                 f'sample_metadata: {sample_metadata}')

    def read_sample_rail_select_register(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_RAIL_SEL
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_RAIL_SEL
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_RAIL_SEL
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).rw_sample_rail_sel

    def read_sample_count_register(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_COUNT
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_COUNT
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_COUNT
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).rw_sample_count

    def read_sample_ratio_register(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_RATIO
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_RATIO
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_RATIO
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).rw_sample_ratio

    def read_sample_metadata_register(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_METADATA
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_METADATA
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_METADATA
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).rw_sample_metadata

    def get_single_sample_engine_sample_content(self, rail_type, engine_index, sample, log=False):
        data_pointer, sample_count, sample_select, _, _ = \
            self.get_single_sample_engine_header_content(rail_type, engine_index, sample, log)
        data_size = self.get_sample_data_capture_size(rail_type, sample_select, sample_count)
        rail_list = self.get_rail_list_from_rail_select(rail_type, sample_select)
        content = int.from_bytes(self.hbidps.dma_read(data_pointer, data_size), "little")
        if log:
            self.log_sample_data_content(rail_type, content, data_size, sample_count, rail_list)
        return content

    def log_sample_data_content(self, type, content, data_size, sample_count, rail_list):
        entry_count = data_size//4
        sample_count_size = entry_count // sample_count
        rail_count_size = sample_count_size // len(rail_list)
        data_log = ''
        for i in range(entry_count):
            sample_order = i // sample_count_size
            rail_index = rail_list[(i % sample_count_size)//rail_count_size]
            entry = content & 0xFFFF_FFFF
            content = content >> 32
            if i % 4 == 0:
                entry_decode = self.hbidps.decode_single_precision_floating_value(entry)
                data_log = f'line{i} Sample{sample_order} Rail{rail_index} Data {entry_decode:.3f}V'
            elif (i % 4 == 2) & ((type == 'hc') | (type == 'lc')):
                entry_decode = self.hbidps.decode_single_precision_floating_value(entry)
                data_log = f'line{i} Sample{sample_order} Rail{rail_index} Data {entry_decode:.3f}A'
            elif (i % 4 == 2) & (type == 'vm'):
                entry_decode = self.hbidps.decode_single_precision_floating_value(entry)
                data_log = f'line{i} Sample{sample_order} Rail{rail_index} Data {entry_decode:.3f}V'
            else:
                self.Log('debug', data_log + f' Time 0x{entry:08x}')

    def get_sample_data_capture_size(self, type, sample_select, sample_count):
        rail_data_entries_count = len(self.get_rail_list_from_rail_select(type, sample_select))
        if (type == 'hc') | (type == 'lc'):
            data_time_count = 4
        elif (type == 'vm'):
            data_time_count = 2
        bytes_per_entry = 4
        return rail_data_entries_count*data_time_count*sample_count*bytes_per_entry

    def get_rail_list_from_rail_select(self, type, sample_select):
        rail_list = []
        if (type == 'hc') | (type == 'lc'):
            rail_bits = 2
        elif (type == 'vm'):
            rail_bits = 1
        for i in range(16):
            if sample_select & 0b1:
                rail_list.append(i)
            sample_select = sample_select >> rail_bits
        return rail_list

    def get_single_sample_engine_header_content(self, rail_type, engine_index, sample, log=False):
        base_address = self.read_single_sample_engine_header_address(rail_type, engine_index)
        address = base_address + (sample * 0x18)
        data_pointer = \
            int.from_bytes(self.hbidps.dma_read(address + 0x00, 4), "little")
        zeros = \
            int.from_bytes(self.hbidps.dma_read(address + 0x04, 4), "little")
        sample_ratio = \
            int.from_bytes(self.hbidps.dma_read(address + 0x08, 4), "little")
        sample_count = \
            int.from_bytes(self.hbidps.dma_read(address + 0x0c, 4), "little")
        sample_select = \
            int.from_bytes(self.hbidps.dma_read(address + 0x10, 4), "little")
        sample_metadata = \
            int.from_bytes(self.hbidps.dma_read(address + 0x14, 4), "little")
        if log:
            self.Log('debug', f'data_pointer 0x{data_pointer:08x} '
                              f'sample_count 0d{sample_count} '
                              f'sample_select 0x{sample_select:08x} '
                              f'sample_ratio 0d{sample_ratio} '
                              f'sample_metadata 0x{sample_metadata:08x} '
                              f'zeros 0x{zeros:08x}')
        return data_pointer, sample_count, sample_select, sample_ratio, sample_metadata

    def reset_single_sample_engine(self, type, index):# what does reset do? start address/ size?
        if (type == 'hc') & (0 <= index < 4):
            reset_bit = 1 << index
        elif (type == 'lc') & (0 <= index < 4):
            reset_bit = 1 << (index+4)
        elif (type == 'vm') & (0 <= index < 5):
            reset_bit = 1 << (index+8)
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        reg = self.hbidps.read_bar_register(self.regs.SAMPLE_ENGINE_RESET)
        reg.value = reset_bit
        self.hbidps.write_bar_register(reg)

    def reset_all_sample_engines(self):
        reg = self.hbidps.read_bar_register(self.regs.SAMPLE_ENGINE_RESET)
        select_all_engines = 0b1_1111_1111_1111
        reg.value = select_all_engines
        self.hbidps.write_bar_register(reg)

    def read_all_sample_engines_address_size(self):
        for index in range(4):
            address = self.read_single_sample_engine_header_address('hc', index)
            self.Log('info', f'hc index {index} header address {hex(address)}')
        for index in range(4):
            address = self.read_single_sample_engine_header_address('lc', index)
            self.Log('info', f'lc index {index} header address {hex(address)}')
        for index in range(5):
            address = self.read_single_sample_engine_header_address('vm', index)
            self.Log('info', f'vm index {index} header address {hex(address)}')

        for index in range(4):
            address = self.read_single_sample_engine_sample_size('hc', index)
            self.Log('info', f'hc index {index} header size {hex(address)}')
        for index in range(4):
            address = self.read_single_sample_engine_sample_size('lc', index)
            self.Log('info', f'lc index {index} header size {hex(address)}')
        for index in range(5):
            address = self.read_single_sample_engine_sample_size('vm', index)
            self.Log('info', f'vm index {index} header size {hex(address)}')

        for index in range(4):
            address = self.read_single_sample_engine_sample_address('hc', index)
            self.Log('info', f'hc index {index} sample address {hex(address)}')
        for index in range(4):
            address = self.read_single_sample_engine_sample_address('lc', index)
            self.Log('info', f'lc index {index} sample address {hex(address)}')
        for index in range(5):
            address = self.read_single_sample_engine_sample_address('vm', index)
            self.Log('info', f'vm index {index} sample address {hex(address)}')

        for index in range(4):
            address = self.read_single_sample_engine_sample_size('hc', index)
            self.Log('info', f'hc index {index} sample size {hex(address)}')
        for index in range(4):
            address = self.read_single_sample_engine_sample_size('lc', index)
            self.Log('info', f'lc index {index} sample size {hex(address)}')
        for index in range(5):
            address = self.read_single_sample_engine_sample_size('vm', index)
            self.Log('info', f'vm index {index} sample size {hex(address)}')

    def read_single_sample_engine_header_address(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_HEADER_REGION_BASE
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_HEADER_REGION_BASE
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_HEADER_REGION_BASE
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).value

    def read_single_sample_engine_sample_address(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_REGION_BASE
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_REGION_BASE
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_REGION_BASE
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).value

    def read_single_sample_engine_header_size(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_HEADER_REGION_SIZE
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_HEADER_REGION_SIZE
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_HEADER_REGION_SIZE
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).value

    def read_single_sample_engine_sample_size(self, type, index):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_REGION_SIZE
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_REGION_SIZE
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_REGION_SIZE
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        return self.hbidps.read_bar_register(reg, index).value

    def set_single_sample_engine_header_address(self, type, index, address):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_HEADER_REGION_BASE
            reg.rw_hearder_region_base = address
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_HEADER_REGION_BASE
            reg.rw_hearder_region_base = address
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_HEADER_REGION_BASE
            reg.rw_hearder_region_base = address
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        self.hbidps.write_bar_register(reg, index)

    def set_single_sample_engine_sample_address(self, type, index, address):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_REGION_BASE
            reg.rw_hearder_region_base = address
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_REGION_BASE
            reg.rw_hearder_region_base = address
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_REGION_BASE
            reg.rw_hearder_region_base = address
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        self.hbidps.write_bar_register(reg, index)

    def set_single_sample_engine_header_size(self, type, index, size):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_HEADER_REGION_SIZE
            reg.rw_hearder_region_base = size
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_HEADER_REGION_SIZE
            reg.rw_hearder_region_base = size
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_HEADER_REGION_SIZE
            reg.rw_hearder_region_base = size
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        self.hbidps.write_bar_register(reg, index)

    def set_single_sample_engine_sample_size(self, type, index, size):
        if (type == 'hc') & (0 <= index < 4):
            reg = self.regs.HC_SAMPLE_REGION_SIZE
            reg.rw_hearder_region_base = size
        elif (type == 'lc') & (0 <= index < 4):
            reg = self.regs.LC_SAMPLE_REGION_SIZE
            reg.rw_hearder_region_base = size
        elif (type == 'vm') & (0 <= index < 5):
            reg = self.regs.VMEAS_SAMPLE_REGION_SIZE
            reg.rw_hearder_region_base = size
        else:
            self.Log('error', f'illegal type {type} (hc, lc, vm) '
                              f'or index {index}')
        self.hbidps.write_bar_register(reg, index)

