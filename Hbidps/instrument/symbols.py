################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import sys
from enum import IntEnum

class RESOURCEIDS:
    LC = 0x10
    HC = 0x10

# TODO maybe should be derived from pmbuscommand
class LTM_PMBUSCOMMAND:
    MFR_PWM_COMP                = 0xD3
    FREQUENCY_SWITCH            = 0x33
    VOUT_UV_FAULT_RESPONSE      = 0x45
    VOUT_OV_FAULT_RESPONSE      = 0x41
    OT_FAULT_RESPONSE           = 0x50
    MFR_CONFIG_ALL              = 0xD1
    VOUT_COMMAND                = 0X21
    MFR_FAULT_RESPONSE          = 0xD5
    MFR_PWM_MODE                = 0xD4

class PMBUSCOMMAND(IntEnum):
    OT_FAULT_LIMIT              = 0x4F
    STATUS_TEMPERATURE          = 0x7D
    STATUS_INPUT                = 0x7C
    STATUS_BYTE                 = 0x78
    STATUS_WORD                 = 0x79
    STATUS_VOUT                 = 0x7A
    STATUS_IOUT                 = 0x7B
    STATUS_CML                  = 0x7E
    STATUS_MFR_SPECIFIC         = 0x80
    CLEAR_FAULTS                = 0x03
    OT_FAULT_RESPONSE           = 0x50
    USER_DATA_04_SCRATCH_PAD    = 0xB4
    USER_DATA_03_SCRATCH_PAD    = 0xB3
    PAGE_CMD                    = 0X00
    WRITE_PROTECT               = 0x10
    VIN_ON                      = 0x35
    VIN_OFF                     = 0x36
    VIN_OV_FAULT_LIMIT          = 0x55
    VIN_OV_FAULT_RESPONSE       = 0x56
    VIN_OV_WARN_LIMIT           = 0x57
    VIN_UV_WARN_LIMIT           = 0x58
    VIN_UV_FAULT_LIMIT          = 0x59
    VIN_UV_FAULT_RESPONSE       = 0x5A
    USER_DATA_00                = 0xB0
    USER_DATA_02                = 0xB2
    MFR_EIN_CONFIG              = 0xC1
    MFR_IIN_CAL_GAIN_TC         = 0xC3
    MFR_CONFIG_ALL_LTC2975      = 0xD1
    MFR_PWRGD_EN                = 0xD4
    MFR_FAULTB0_RESPONSE        = 0xD5
    MFR_FAULTB1_RESPONSE        = 0xD6
    MFR_CONFIG2_LTC2975         = 0xD9
    MFR_CONFIG3_LTC2975         = 0xDA
    MFR_RETRY_DELAY             = 0xDB
    MFR_RESTART_DELAY           = 0xDC
    MFR_POWERGOOD_ASSERTION_DELAY = 0xE1
    MFR_WATCHDOG_T_FIRST        = 0xE2
    MFR_WATCHDOG_T              = 0xE3
    MFR_PAGE_FF_MASK            = 0xE4
    MFR_IIN_CAL_GAIN            = 0xE8
    MFR_RETRY_COUNT             = 0xF7

    OPERATION                   = 0X01
    ON_OFF_CONFIG               = 0x02
    IOUT_CAL_GAIN               = 0x38
    POWER_GOOD_ON               = 0x5E
    POWER_GOOD_OFF              = 0x5F
    MFR_IOUT_CAL_GAIN_TAU_INV   = 0xB9
    MFR_IOUT_CAL_GAIN_THETA     = 0xBA
    MFR_CONFIG_LTC2975          = 0xD0
    MFR_FAULTB0_PROPAGATE       = 0xD2
    MFR_FAULTB1_PROPAGATE       = 0xD3
    UT_FAULT_RESPONSE           = 0x54
    IOUT_UC_FAULT_RESPONSE      = 0x4C
    VOUT_UV_FAULT_RESPONSE      = 0x45
    IOUT_OC_FAULT_RESPONSE      = 0x47
    VOUT_OV_FAULT_RESPONSE      = 0x41
    TON_MAX_FAULT_RESPONSE      = 0x63
    OT_WARN_LIMIT               = 0x51
    UT_WARN_LIMIT               = 0x52
    UT_FAULT_LIMIT              = 0x53
    VOUT_OV_WARN_LIMIT          = 0x42
    VOUT_UV_WARN_LIMIT          = 0x43
    IOUT_OC_WARN_LIMIT          = 0x4A
    VOUT_OV_FAULT_LIMIT         = 0x40
    VOUT_UV_FAULT_LIMIT         = 0x44
    TON_MAX_FAULT_LIMIT         = 0x62
    TOFF_DELAY                  = 0x64
    TON_DELAY                   = 0x60
    TON_RISE                    = 0x61
    VOUT_MAX                    = 0x24
    VOUT_COMMAND                = 0x21
    VOUT_TRANSITION_RATE        = 0x27
    IOUT_OC_FAULT_LIMIT         = 0x46

class LTC2975PMBUSDATA:
    OT_FAULT_LIMIT_DATA         = b'\x08\xea'
    CLEAR_FAULTS_DATA           = b''
    OT_FAULT_RESPONSE_DATA      = b'\xB8'

class RAILCOMMANDS(IntEnum):
    TRIGGER_Q_COMPLETE  = 0xFF
    SYNC_COMMAND        = 0x31
    TIME_DELAY          = 0x30
    WAKE_RAIL           = 0xA0
    SET_RAIL_BUSY       = 0xA1
    TQ_NOTIFY           = 0xA2
    SET_HI_I_LIMIT      = 0x60
    SET_OV_LIMIT        = 0x61
    SET_UV_LIMIT        = 0x62
    SET_V               = 0x63
    SET_PWR_STATE       = 0x64
    SET_FDT             = 0x65
    SET_FD_CURRENT_HI   = 0x66
    APPLY_USER_ATTRIBUTES = 0x70
    SMP_COUNT           = 0x40
    SMP_RATIO           = 0x41
    SMP_SELECT          = 0x42
    SMP_METADATA        = 0x43

class PMBUSCOMMANDS:
    SEND_BYTE = 0x0
    WRITE_BYTE = 0x1
    READ_BYTE = 0x2
    WRITE_WORD = 0x3
    READ_WORD = 0x4


class DEVICEID:
    SMP_LC_00 = 0x50
    SMP_LC_01 = 0x51
    SMP_LC_02 = 0x52
    SMP_LC_03 = 0x53
    SMP_HC_00 = 0x60
    SMP_HC_01 = 0x61
    SMP_HC_02 = 0x62
    SMP_HC_03 = 0x63
    SMP_VMEAS_00 = 0x70
    SMP_VMEAS_01 = 0x71
    SMP_VMEAS_02 = 0x72
    SMP_VMEAS_03 = 0x73
    SMP_VMEAS_04 = 0x74


def Load():
    allConstants = {} # dict of all constants
    names = set()   # set of all constant names
    conflicts = set()
    for className, cls in vars(sys.modules[__name__]).items():
        if not className.startswith('_') and className.upper() == className:
            for name, value in _GetClassConstants(cls).items():
                if name in names:
                    conflicts.add(name)
                else:
                    names.add(name)
                allConstants[(className, name)] = value
    constants = {}   # list of output constants
    for key, value in allConstants.items():
        className, name = key
        # Always add "full form" of the constant
        constants['{}.{}'.format(className, name)] = value
        if name not in conflicts:
            # Convenient "short form" (not supported for conficts)
            constants[name] = value
    return constants, {}

def _GetClassConstants(cls):
    s = {}
    for var, value in vars(cls).items():
        if not var.startswith('_'):
            s[var] = value
    return s

if __name__ == "__main__":
    # Parse command line options
    import os
    import argparse
    import datetime
    import getpass
    from string import Template
    parser = argparse.ArgumentParser(description = 'C++ symbols header file generator')
    parser.add_argument('outfile', help = 'Output filename', type = str)
    args = parser.parse_args()

    filename = os.path.basename(args.outfile)
    split = os.path.splitext(filename)

    template = {
        'USER'     : getpass.getuser(),
        'DATE'     : datetime.datetime.now().strftime("%m/%d/%Y"),
        'FILENAME' : filename,
        'BASENAME' : split[0].upper(),
        'FILEEXT'  : split[1].upper().replace('.', ''),
    }

    fout = open(args.outfile, 'w')
    # Print header
    fout.write(Template("""\
////////////////////////////////////////////////////////////////////////////////
// INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
//------------------------------------------------------------------------------
//   Filename: ${FILENAME} (auto-generated file, see symbols.py)
//------------------------------------------------------------------------------
//    Purpose: HDDPS testbench symbols
//------------------------------------------------------------------------------
// Created by: ${USER}
//       Date: ${DATE}
//      Group: HDMT FPGA Validation
////////////////////////////////////////////////////////////////////////////////
#ifndef __${BASENAME}_${FILEEXT}__
#define __${BASENAME}_${FILEEXT}__

#include <cstdint>

namespace hddpstbc {

""").substitute(template))

    constants, functions = Load()
    for name, value in constants.items():
        if isinstance(value,float):
            fout.write('const float {} = {}f;\n'.format(name.replace('.', '_'), value))
        else:
            fout.write('const uint64_t {} = 0x{:x};\n'.format(name.replace('.', '_'), value))

    # Print footer
    fout.write(Template("""\

}  // namespace hddpstbc

#endif  // __${BASENAME}_H__
""").substitute(template))

    fout.close()
