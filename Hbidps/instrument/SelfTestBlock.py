# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time
from enum import Enum

from Common.fval import Object
from Hbidps.instrument import hbidps_register

class max14662_device(Enum):
    VSENSE_AD00 = 0x00
    VSENSE_AD01 = 0x01
    VSENSE_AD10 = 0x02
    VSENSE_AD11 = 0x03
    VTVM_AD00 = 0x00
    VTVM_AD01 = 0x01
    VTVM_AD10 = 0x02
    VTVM_AD11 = 0x03


class VtVmBus_State():
    def __init__(self):
        self.cfb_vm0_switch = False
        self.vtarget_switch_list = [0]*16
        self.vsense_switch_list = [0]*26
        self.vmeasure_switch_list = [0]*16
        self.isource_load =  [0]*5
        self.iforce_switch = False


class CommonForceBus_State():
    def __init__(self):
        self.cfb_hc_connect_list = [0]*10
        self.cfb_lc_connect_list = [0]*16
        self.cfb_load_list = [1, 0] # [2.0Ohm, 0.5Ohm]


class VtVmBus(Object):

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.regs = hbidps_register

        self.state = VtVmBus_State()
        self.state_n = VtVmBus_State()

    def initialize_vtvm_bus(self, log=False):
        self.turn_on_and_reset_all_max14662_switch()
        self.get_state_attribute_from_board(log)
        self.clear_stage_n_attribute()
        self.check_safe_before_apply_attribute()
        self.apply_next_state_attribute()
        self.update_state_attribute()
        self.check_state_attribute_from_board(log)

    def clear_stage_n_attribute(self):
        self.state_n.cfb_vm0_switch = False
        self.state_n.vtarget_switch_list = [0]*16
        self.state_n.vsense_switch_list = [0]*26
        self.state_n.vmeasure_switch_list = [0]*16
        self.state_n.isource_load = [0]*5
        self.state_n.iforce = False

    def turn_on_a_pair_of_vtvm_channel(self, vt_channel, vm_channel):
        self.state_n.vtarget_switch_list[vt_channel] = 1
        self.state_n.vmeasure_switch_list[vm_channel] = 1

    def apply_next_state_attribute(self):
        self.apply_vsense_load_switch_attribute()
        self.apply_vsense_switch_attribute()
        self.apply_cfb_vm0_switch_attribute()
        self.apply_vmeasure_switch_attribute()
        self.apply_vtarget_switch_attribute()
        self.apply_iforce_switch_attribute()

    def check_safe_before_apply_attribute(self):
        self.check_state_n_configure()
        self.check_drive_channel_off()
        self.check_switch_en_before_apply_attribute()

    def check_state_n_configure(self):
        pass

    def check_drive_channel_off(self):
        pass

    def check_switch_en_before_apply_attribute(self):
        self.check_max14662_switch_control('vt')
        self.check_max14662_switch_control('vsense')
        self.check_max14662_switch_control('vm')

    def update_state_attribute(self):
        self.state = self.state_n

    def apply_iforce_switch_attribute(self):
        if self.state_n.iforce_switch == False:
            self.turn_off_iforce_switch()
        elif self.state_n.iforce_switch == True:
            self.turn_on_iforce_switch()
        else:
            self.Log('error', f'illegal iforce mode')

    def apply_vtarget_switch_attribute(self):
        if self.state_n.vtarget_switch_list == [0]*16:
            self.turn_off_all_vtarget_switch()
        else:
            self.turn_on_vtarget_switch()

    def apply_vsense_switch_attribute(self):
        if self.state_n.vsense_switch_list == [0]*26:
            self.turn_off_all_vsense_switch()
        else:
            self.turn_on_vsense_switch()

    def apply_vmeasure_switch_attribute(self):
        if self.state_n.vmeasure_switch_list == [0]*16:
            self.turn_off_all_vmeasure_switch()
        else:
            self.turn_on_vmeasure_switch()

    def apply_vsense_load_switch_attribute(self):
        if self.state_n.isource_load == [0]*5:
            self.turn_off_all_vsense_load_switch()
        else:
            self.turn_on_vsense_load_switch()

    def apply_cfb_vm0_switch_attribute(self):
        if self.state_n.cfb_vm0_switch == False:
            self.turn_off_cfb_vm0_switch()

    def turn_off_iforce_switch(self):
        reg = self.regs.FIVR_DAC_GPIO_CONTROL()
        reg.rw_vtarget_00_sel = 1
        reg.rw_current_source_en = 0
        reg.rw_vtarget_short_test_sel = 0
        self.hbidps.write_bar_register(reg)

    def turn_on_iforce_switch(self):
        reg = self.regs.FIVR_DAC_GPIO_CONTROL()
        reg.rw_vtarget_00_sel = 0
        reg.rw_current_source_en = 1
        reg.rw_vtarget_short_test_sel = 1
        self.hbidps.write_bar_register(reg)

    def turn_off_all_vtarget_switch(self):
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD00.value, 0)
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD01.value, 0)

    def turn_on_vtarget_switch(self):
        vsense_ad00_status = vsense_ad01_status = 0
        for i in range(8):
            vsense_ad00_status |= (self.state_n.vtarget_switch_list[i] << i)
        for i in range(8):
            vsense_ad01_status |= (self.state_n.vtarget_switch_list[i+8] << i)
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD00.value, vsense_ad00_status)
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD01.value, vsense_ad01_status)

    def turn_off_all_vsense_switch(self):
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD00.value, 0)
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD01.value, 0)
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD10.value, 0)
        vsense_ad11_status = \
            self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD11.value)
        vsense_ad11_status = vsense_ad11_status & 0b1111_1100
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD11.value,
                                          vsense_ad11_status)

    def turn_off_all_vmeasure_switch(self):
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD10.value, 0)
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD11.value, 0)

    def turn_on_vmeasure_switch(self):
        vmeasure_ad10_status = vmeasure_ad11_status = 0
        for i in range(8):
            vmeasure_ad10_status |= (self.state_n.vmeasure_switch_list[i] << i)
        for i in range(8):
            vmeasure_ad11_status |= (self.state_n.vmeasure_switch_list[i + 8] << i)
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD10.value, vmeasure_ad10_status)
        self.hbidps.write_lb_max14662(max14662_device.VTVM_AD11.value, vmeasure_ad11_status)

    def turn_on_vsense_load_switch(self):
        vsense_ad11_status = \
            self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD11.value)
        vsense_ad11_status = vsense_ad11_status & 0b0000_0111
        for i in range(5):
            vsense_ad11_status |= (self.state_n.isource_load[i] << (i + 3))
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD11.value,
                                          vsense_ad11_status)

    def turn_off_all_vsense_load_switch(self):
        vsense_ad11_status = \
            self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD11.value)
        vsense_ad11_status = vsense_ad11_status & 0b0000_0111
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD11.value,
                                          vsense_ad11_status)

    def turn_off_cfb_vm0_switch(self):
        vsense_ad11_status = \
            self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD11.value)
        vsense_ad11_status = vsense_ad11_status & 0b1111_1011
        self.hbidps.write_vsense_max14662(max14662_device.VSENSE_AD11.value,
                                          vsense_ad11_status)

    def get_state_attribute_from_board(self, log=False):
        self.state.cfb_vm0_switch, self.state.isource_load = self.get_cfb_vm0_short_and_loads()
        self.state.vtarget_switch_list = self.get_vtarget_switch_status()
        self.state.vsense_switch_list = self.get_vsense_switch_list()
        self.state.vmeasure_switch_list = self.get_vmeasure_switch_status()
        self.state.iforce = self.get_iforce_status()
        if log:
            self.Log('info', f'vtvm_load read: {self.state.isource_load}')
            self.Log('info', f'vtvm_vsense read: {self.state.vsense_switch_list}')
            self.Log('info', f'cfb_vm0 read: {self.state.cfb_vm0_switch}')
            self.Log('info', f'vtvm_vmeasure read: {self.state.vmeasure_switch_list}')
            self.Log('info', f'vtvm_vtarget read: {self.state.vtarget_switch_list}')
            self.Log('info', f'vtvm_iforce read: {self.state.iforce}')

    def check_state_attribute_from_board(self, log=False):
        cfb_vm0_switch, isource_load = self.get_cfb_vm0_short_and_loads()
        vtarget_switch_list = self.get_vtarget_switch_status()
        vsense_switch_list = self.get_vsense_switch_list()
        vmeasure_switch_list = self.get_vmeasure_switch_status()
        iforce_switch = self.get_iforce_status()
        if self.state.isource_load != isource_load:
            self.Log('info', f'vtvm_load read: {isource_load} '
                              f'set {self.state.isource_load}')
        if self.state.vsense_switch_list != vsense_switch_list:
            self.Log('info', f'vtvm_vsense read: {vsense_switch_list} '
                              f'set {self.state.vsense_switch_list}')
        if self.state.cfb_vm0_switch != cfb_vm0_switch:
            self.Log('info', f'cfb_vm0 read: {cfb_vm0_switch} '
                              f'set {self.state.cfb_vm0_switch}')
        if self.state.vmeasure_switch_list != vmeasure_switch_list:
            self.Log('info', f'vtvm_vmeasure read: {vmeasure_switch_list} '
                              f'set {self.state.vmeasure_switch_list}')
        if self.state.vtarget_switch_list != vtarget_switch_list:
            self.Log('info', f'vtvm_vtarget read: {vtarget_switch_list} '
                              f'set {self.state.vtarget_switch_list}')
        if self.state.iforce_switch != iforce_switch:
            self.Log('info', f'vtvm_iforce read: {iforce_switch} '
                              f'set {self.state.iforce_switch}')
        if log:
            self.Log('info', f'vtvm_load read: {isource_load} '
                              f'set {self.state.isource_load}')
            self.Log('info', f'vtvm_vsense read: {vsense_switch_list} '
                              f'set {self.state.vsense_switch_list}')
            self.Log('info', f'cfb_vm0 read: {cfb_vm0_switch} '
                              f'set {self.state.cfb_vm0_switch}')
            self.Log('info', f'vtvm_vmeasure read: {vmeasure_switch_list} '
                              f'set {self.state.vmeasure_switch_list}')
            self.Log('info', f'vtvm_vtarget read: {vtarget_switch_list} '
                              f'set {self.state.vtarget_switch_list}')
            # self.Log('info', f'vtvm_iforce read: {iforce} '
            #                   f'set {self.state.iforce}')

    def get_vsense_switch_list(self):
        vsense_ad00_set = self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD00.value)
        vsense_ad01_set = self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD01.value)
        vsense_ad10_set = self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD10.value)
        vsense_ad11_set = self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD11.value)
        vsense_status_all = (vsense_ad11_set << 24) + (vsense_ad10_set << 16) + (vsense_ad01_set << 8) + vsense_ad00_set
        vsense_switch_list = []
        for i in range(26):
            vsense_switch_list.append(vsense_status_all & 0b1)
            vsense_status_all = vsense_status_all >> 1
        return vsense_switch_list

    def get_cfb_vm0_short_and_loads(self):
        vsense_ad11_set = \
            self.hbidps.read_vsense_max14662(max14662_device.VSENSE_AD11.value)
        cfb_vm0_short = (vsense_ad11_set >> 2) & 0x1
        load_10 = (vsense_ad11_set >> 3) & 0x1
        load_20 = (vsense_ad11_set >> 4) & 0x1
        load_50 = (vsense_ad11_set >> 5) & 0x1
        load_100k = (vsense_ad11_set >> 6) & 0x1
        load_200k = (vsense_ad11_set >> 7) & 0x1
        return cfb_vm0_short, [load_10, load_20, load_50, load_100k, load_200k]

    def get_vtarget_switch_status(self):
        vtarget_switch = \
            self.hbidps.read_lb_max14662(max14662_device.VTVM_AD00.value)
        vtarget_switch = vtarget_switch << 8
        vtarget_switch += \
            self.hbidps.read_lb_max14662(max14662_device.VTVM_AD01.value)
        vtarget_switch_list = []
        for i in range(16):
            vtarget_switch_list.append(vtarget_switch & 0b1)
            vtarget_switch >> 1
        return vtarget_switch_list

    def get_vmeasure_switch_status(self):
        vmeasure_switch = \
            self.hbidps.read_lb_max14662(max14662_device.VTVM_AD10.value)
        vmeasure_switch = vmeasure_switch << 8
        vmeasure_switch += \
            self.hbidps.read_lb_max14662(max14662_device.VTVM_AD11.value)
        vmeasure_switch_list = []
        for i in range(16):
            vmeasure_switch_list.append(vmeasure_switch & 0b1)
            vmeasure_switch >> 1
        return vmeasure_switch_list

    def get_iforce_status(self):
        reg = self.hbidps.read_bar_register(self.regs.FIVR_DAC_GPIO_CONTROL)
        iforce_status = None
        if (reg.value & 0b111) == 0b001:
            iforce_status = False
        elif (reg.value & 0b111) == 0b110:
            iforce_status = True
        return iforce_status

    def check_max14662_switch_control(self, channel=None, log=False):
        switch_control = \
            self.hbidps.read_bar_register(self.regs.MAX14662_SHUTDOWN_CONTROL)
        if (channel == 'vsense') & (switch_control.rw_st_vhdd_lb != 0):
            self.Log('error', f'vhdd: {hex(switch_control.rw_st_vhdd_lb)}')
        if (channel == 'vm') & (switch_control.rw_st_vm_lb != 0):
            self.Log('error', f'vm: {hex(switch_control.rw_st_vm_lb)}')
        if (channel == 'vt') & (switch_control.rw_st_vt_lb != 0):
            self.Log('error', f'vt: {hex(switch_control.rw_st_vt_lb)}')
        if log:
            self.Log('info', f'st_vt_lb: {hex(switch_control.rw_st_vt_lb)} '
                         f'st_vm_lb: {hex(switch_control.rw_st_vm_lb)} '
                         f'st_vhdd_lb: {hex(switch_control.rw_st_vhdd_lb)} '
                         f'st_gang_lb: {hex(switch_control.rw_st_gang_lb)}')

    def turn_on_and_reset_all_max14662_switch(self):
        switch_control = \
            self.hbidps.read_bar_register(self.regs.MAX14662_SHUTDOWN_CONTROL)
        switch_control.value = 0b111_1111
        self.hbidps.write_bar_register(switch_control)
        time.sleep(0.1) # wait for Max14662 to turn on
        switch_control.value = 0b000_0000
        self.hbidps.write_bar_register(switch_control)
        time.sleep(0.1) # wait for Max14662 to turn on

    def turn_on_vsense_max14662_switch(self):
        switch_control = \
            self.hbidps.read_bar_register(self.regs.MAX14662_SHUTDOWN_CONTROL)
        if switch_control.rw_st_vhdd_lb != 0:
            switch_control.rw_st_vhdd_lb = 0
            self.hbidps.write_bar_register(switch_control)
            time.sleep(0.1) # wait for Max14662 to turn on

    def turn_on_st_vm_max14662_switch(self):
        switch_control = \
            self.hbidps.read_bar_register(self.regs.MAX14662_SHUTDOWN_CONTROL)
        if switch_control.rw_st_vm_lb != 0:
            switch_control.rw_st_vm_lb = 0
            self.hbidps.write_bar_register(switch_control)
            time.sleep(0.1) # wait for Max14662 to turn on

    def turn_on_st_vt_max14662_switch(self):
        switch_control = \
            self.hbidps.read_bar_register(self.regs.MAX14662_SHUTDOWN_CONTROL)
        if switch_control.rw_st_vt_lb != 0:
            switch_control.rw_st_vt_lb = 0
            self.hbidps.write_bar_register(switch_control)
            time.sleep(0.1) # wait for Max14662 to turn on


class CommonForceBus(Object):

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.regs = hbidps_register

        self.state = CommonForceBus_State()
        self.state_n = CommonForceBus_State()

    def initialize_common_force_bus(self, log=False):
        self.enable_pcie_contorl()
        self.get_state_attribute_from_board(log)
        self.clear_stage_n_attribute()
        self.check_safe_before_apply_attribute()
        self.apply_next_state_attribute()
        self.update_state_attribute()
        self.check_state_attribute_from_board(log)
        self.enable_tq_contorl()

    def get_state_attribute_from_board(self, log=False):
        self.state.cfb_hc_connect_list = self.get_cfb_hc_connect_list()
        self.state.cfb_lc_connect_list = self.get_cfb_lc_connect_list()
        self.state.cfb_load_list = self.get_cfb_load_list()
        if log:
            self.Log('info', f'cfb_hc read: {self.state.cfb_hc_connect_list}')
            self.Log('info', f'cfb_lc read: {self.state.cfb_lc_connect_list}')
            self.Log('info', f'cfb_load read: {self.state.cfb_load_list}')

    def check_state_attribute_from_board(self, log=False):
        cfb_hc_connect_list = self.get_cfb_hc_connect_list()
        cfb_lc_connect_list = self.get_cfb_lc_connect_list()
        cfb_load_list = self.get_cfb_load_list()
        if self.state.cfb_hc_connect_list != cfb_hc_connect_list:
            self.Log('info',f'cfb_hc read: {cfb_hc_connect_list} '
                             f'set {self.state.cfb_hc_connect_list}')
        if self.state.cfb_lc_connect_list != cfb_lc_connect_list:
            self.Log('info', f'cfb_lc read: {cfb_lc_connect_list} '
                              f'set {self.state.cfb_lc_connect_list}')
        if self.state.cfb_load_list != cfb_load_list:
            self.Log('info', f'cfb_load read: {cfb_load_list} '
                              f'set {self.state.cfb_load_list}')
        if log:
            self.Log('info', f'cfb_hc read: {cfb_hc_connect_list} '
                             f'set {self.state.cfb_hc_connect_list}')
            self.Log('info', f'cfb_lc read: {cfb_lc_connect_list} '
                             f'set {self.state.cfb_lc_connect_list}')
            self.Log('info', f'cfb_load read: {cfb_load_list} '
                             f'set {self.state.cfb_load_list}')

    def get_cfb_hc_connect_list(self):
        reg = self.hbidps.read_bar_register(self.regs.TPS22976N_ST_HC_LD_EN)
        cfb_hc_connect = reg.rw_ld_en
        cfb_hc_connect_list = []
        for i in range(10):
            cfb_hc_connect_list.append(cfb_hc_connect & 0b1)
            cfb_hc_connect = cfb_hc_connect >> 1
        return cfb_hc_connect_list

    def get_cfb_lc_connect_list(self):
        reg = self.hbidps.read_bar_register(self.regs.TPS22976N_ST_LC_LD_EN)
        cfb_lc_connect = reg.rw_ld_en
        cfb_lc_connect_list = []
        for i in range(16):
            cfb_lc_connect_list.append(cfb_lc_connect & 0b1)
            cfb_lc_connect = cfb_lc_connect >> 1
        return cfb_lc_connect_list

    def get_cfb_load_list(self):
        reg = self.hbidps.read_bar_register(self.regs.TPS22976N_ST_LD_EN)
        cfb_load_list = [reg.rw_st_2p0_ohm_ld_en, reg.rw_st_0p5_ohm_ld_en]
        return cfb_load_list

    def check_safe_before_apply_attribute(self):
        pass

    def update_state_attribute(self):
        self.state = self.state_n

    def clear_stage_n_attribute(self):
        self.state_n.cfb_hc_connect_list = [0]*10
        self.state_n.cfb_lc_connect_list = [0]*16
        self.state_n.cfb_load_list = [1, 0]  # [2.0Ohm, 0.5Ohm]

    def enable_pcie_contorl(self):
        reg = self.regs.GROUND_CONTROL_MODE()
        reg.rw_mode = 1
        self.hbidps.write_bar_register(reg)

    def enable_tq_contorl(self):
        reg = self.regs.GROUND_CONTROL_MODE()
        reg.rw_mode = 0
        self.hbidps.write_bar_register(reg)

    def apply_next_state_attribute(self):
        self.disconnect_cfb_hc_rails()
        self.disconnect_cfb_lc_rails()
        self.connect_default_load()

    def apply_cfb_hc_rails_attribute(self):
        if self.state_n.cfb_hc_connect_list == [0]*10:
            self.disconnect_cfb_hc_rails()

    def apply_cfb_lc_rails_attribute(self):
        if self.state_n.cfb_hc_connect_list == [0]*16:
            self.disconnect_cfb_lc_rails()

    def apply_cfb_load_attribute(self):
        if self.state_n.cfb_hc_connect_list == [1, 0]:
            self.connect_default_load()

    def disconnect_cfb_hc_rails(self):
        reg = self.regs.TPS22976N_ST_HC_LD_EN()
        reg.rw_ld_en = 0
        self.hbidps.write_bar_register(reg)

    def disconnect_cfb_lc_rails(self):
        reg = self.regs.TPS22976N_ST_LC_LD_EN()
        reg.rw_ld_en = 0
        self.hbidps.write_bar_register(reg)

    def connect_default_load(self):
        reg = self.regs.TPS22976N_ST_LD_EN()
        reg.rw_st_2p0_ohm_ld_en = self.state_n.cfb_load_list[0]
        reg.rw_st_0p5_ohm_ld_en = self.state_n.cfb_load_list[1]
        self.hbidps.write_bar_register(reg)

