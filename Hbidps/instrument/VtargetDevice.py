# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class VTargetDevice(Object):
    def __init__(self, hbidps):
        super().__init__()
        self.hbidps = hbidps
        self.regs = hbidps_register
        self.chips = [chip for chip in range(2)]
        self.rails_per_chip = [channel for channel in range(8)]

    def init_vtarget_device(self, log=False):
        for chip in self.chips:
            self.chip_common_init(chip)
            for rail in self.rails_per_chip:
                self.rail_init(rail)
        self.read_ad5676_info_set(log)

    def chip_common_init(self, chip):
        self.turn_off_all_rails(chip)

        # command 0x5; Hardware LDAC_bar Mask Register; Address bits ignored
        self.hbidps.hbi_dps_ad5676_write(chip, 0x5, 0x0, 0x0000)

        # command 0x7; gain setup; set gain to be 2x which means DB2 set to 1
        self.hbidps.hbi_dps_ad5676_write(chip, 0x7, 0x0, 0x04)

        # command 0x8; set up daisy-chain enable; using stand along mode
        self.hbidps.hbi_dps_ad5676_write(chip, 0x8, 0x0, 0x0000)

    def set_dac_reg(self, rail, data):
        chip = rail // 8
        rail = rail % 8
        self.hbidps.hbi_dps_ad5676_write(chip, 0x3, rail, data)

    def read_dac_reg(self, rail):
        chip = rail // 8
        rail = rail % 8
        return self.hbidps.hbi_dps_ad5676_read(chip, rail)

    def turn_off_all_rails(self, chip):
        operation_mode = 0b11
        power_state = 0x0
        for rail in self.rails_per_chip:
            power_state = self.set_rail_state(rail, operation_mode, power_state)
        # command 0x4; DAC Power Down/Up Operation; rail index don't care
        self.hbidps.hbi_dps_ad5676_write(chip, 0x4, 0x0, power_state)

    def trun_on_all_rails(self, chip):
        operation_mode = 0b00
        power_state = 0x0
        for rail in self.rails_per_chip:
            power_state = self.set_rail_state(rail, operation_mode, power_state)
        # command 0x4; DAC Power Down/Up Operation; rail index don't care
        self.hbidps.hbi_dps_ad5676_write(chip, 0x4, 0, power_state)

    def turn_on_rail(self, rail):
        """set the rail state such that only rail(0-15) is on

        Currently, test cases only activate one rail;
        12/18/2020 This is only used for debugging purpose and
        this method is acutally setting all the rail to normal state.
        """
        chip = rail // 8
        rail = rail % 8
        power_state = 0x0
        power_state = self.set_rail_state(rail, 0b00, power_state)
        self.hbidps.hbi_dps_ad5676_write(chip, 0x04, 0, power_state)

    @staticmethod
    def set_rail_state(rail, operation_mode, power_state):
        bit_shift = 2 * rail
        power_state &= ~(0b11 << bit_shift)
        power_state |= (operation_mode << bit_shift)
        return power_state

    def rail_init(self, rail):
        pass

    def check_vtarget_rails_clean_start_state(self, log=False):
        for rail in range(16):
            self.set_dac_reg(rail, 0x0000)
        if self.device_need_reinitialize():
            self.hbidps.set_board_sources_to_safe_state()
            self.Log('info', f'before reinit')
            self.log_power_state()
            self.init_vtarget_device(log)
            self.Log('info', f'after reinit')
            self.log_power_state()
        self.read_ad5676_info_set(log)
        if self.device_need_reinitialize():
            self.Log('error', f'Vtarget device reinitialize is not successful')

    def log_power_state(self):
        power_state = self.hbidps.read_vtarg_power_state()
        self.Log('info', f'vtarget power state bar register(0x748): {hex(power_state.value)}')

    def device_need_reinitialize(self):
        re_init = False

        vtarget_folded = self.check_folded_status()
        if vtarget_folded.rwc_vtarget_folded != 0xFFFF:
            self.Log('error', f'vtarget rail not all folded {hex(vtarget_folded.value)}')
            re_init = True

        vtarget_power_state = self.hbidps.read_bar_register(self.regs.VTARG_POWER_STATE)
        if vtarget_power_state.value != 0xffffffff:
            self.Log('error', f'vtarget rail power state not all off {hex(vtarget_power_state.value)} ')
            re_init = True

        return re_init

    def check_folded_status(self):
        vtarget_folded = self.regs.VTARG_FOLDED()
        vtarget_folded.value = 0xFFFF
        self.hbidps.write_bar_register(vtarget_folded)
        time.sleep(0.1)
        vtarget_folded = self.hbidps.read_bar_register(self.regs.VTARG_FOLDED)
        return vtarget_folded

    def read_ad5676_info_set(self, log=False):
        info_set = []
        vtarget_busy = self.hbidps.read_bar_register(self.regs.VTARG_BUSY)
        info_set.append({'Chip': 'Common', 'Rail': 'Common', 'Name': 'VTARG_BUSY', 'Value': hex(vtarget_busy.value)})

        vtarget_folded = self.regs.VTARG_FOLDED(0xFFFF)
        self.hbidps.write_bar_register(vtarget_folded)
        time.sleep(0.1)
        vtarget_folded = self.hbidps.read_bar_register(self.regs.VTARG_FOLDED)
        info_set.append({'Chip': 'Common', 'Rail': 'Common', 'Name': 'VTARG_FOLDED', 'Value': hex(vtarget_folded.value)})

        vtarget_power_state = self.hbidps.read_bar_register(self.regs.VTARG_POWER_STATE)
        info_set.append({'Chip': 'Common', 'Rail': 'Common', 'Name': 'VTARG_POWER_STATE',
                         'Value': hex(vtarget_power_state.value)})
        for chip in self.chips:
            for rail in self.rails_per_chip:
                dac_register = self.hbidps.hbi_dps_ad5676_read(chip, rail)
                info_set.append({'Chip': chip, 'Rail': rail, 'Name': 'DAC_REG', 'Value': hex(dac_register)})

        if log:
            self.print_info_table(info_set)
        return info_set

    def check_vt_fold_status(self):
        vt_fold_status = self.hbidps.read_bar_register(hbidps_register.VTARG_FOLDED)
        vt_fold_status.value = 0xFFFF
        self.hbidps.write_bar_register(vt_fold_status)
        time.sleep(0.1)
        vt_fold_status = self.hbidps.read_bar_register(hbidps_register.VTARG_FOLDED)
        self.Log('info', f'vtarget_fold_status {hex(vt_fold_status.value)}')

    def print_info_table(self, info_set_list, title=None):
        if title is None:
            title = f'HBIDPS slot {self.hbidps.slot} VTarget Device(AD5676) info set'
        table = self.hbidps.contruct_text_table(data=info_set_list,
                                                title_in=title)
        self.Log('info', f'{table}')

