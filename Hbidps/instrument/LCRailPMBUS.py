# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class LCRailPMBUS(Object):
    """Helper Class for HBI DPS Rail Devices(LTM4680) Setting

        Methods:
        init_lc_rails_hw() Initialize 16 rails of LTC2975 in one dps
        read_ltc2975_info_set() Read information set for all rails
        print_info_table() Log information set in table
        print_one_chip_table() Log information set for a specific chip
        read_lc_chip_common_info_set() Read common information for a chip
        read_ltc2975_unpaged_status() Read common status information for a chip
        read_ltc2975_vin() Read vin related information, not paged
        read_ltc2975_iin() Read iin related information, not paged
        read_ltc2975_pin() Read pin related information, not paged
        read_ltc2975_internal_temp() Read internal temperature, not paged
        read_lc_chip_page_info_set() Read paged information for a page
        read_ltc2975_paged_status() Read paged status for a page
        read_ltc2975_paged_temperature() Read external related temperature, paged
        read_ltc2975_vout() Read vout related information, paged
        read_ltc2975_paged_iout_and_pout() Read iout and power out related information, paged
        log_lc_rail_dps_power_state() Log LC rail power state

    """

    def __init__(self, hbidps):
        super().__init__()
        self.hbidps = hbidps
        self.regs = hbidps_register
        self.chips = [chip for chip in range(4)]
        self.pages_in_chip = [page for page in range(4)]

    def init_lc_rails_hw(self, log=False):
        self.disable_lc_chips(self.chips)
        self.initialize_manufacture_confige()
        self.configure_gang_switching()

        for chip in self.chips:
            self.chip_common_init(chip)
            for page in self.pages_in_chip:
                self.chip_page_init(chip, page)
                self.set_power_state(chip, page, 0)

        if log:
            time.sleep(0.5)
            self.read_ltc2975_info_set(log)

    def print_info_table(self, info_set_list, title=None):
        if title is None:
            title = f'HBIDPS slot {self.hbidps.slot} LCPMBUS Device info set'
        table = self.hbidps.contruct_text_table(data=info_set_list,
                                                title_in=title)
        self.Log('info', f'{table}')

    def read_ltc2975_info_set(self, log=False):
        info_set_list = []
        for chip in self.chips:
            info_set_list.extend(self.read_lc_chip_common_info_set(chip))
            for page in self.pages_in_chip:
                info_set_list.extend(self.read_lc_chip_page_info_set(chip, page))
        if log:
            self.print_info_table(info_set_list)

        return info_set_list

    def check_info_set(self, observed_info_list):
        expected_chip_info_map = {
            'STATUS_INPUT': hex(0x0),
            'STATUS_CML': hex(0x0),
            'MFR_FAULT_LOG_STATUS': hex(0x0),
            'VIN_ON': 9.0,
            'VIN_OFF': 8.0,
            'VIN_OV_FAULT_LIMIT': 14.0,
            'VIN_OV_WARN_LIMIT': 15.0,
            'VIN_OV_FAULT_RESPONSE': hex(0x80),
            'VIN_UV_WARN_LIMIT': 0.0,
            'VIN_UV_FAULT_LIMIT': 0.0,
            'VIN_UV_FAULT_RESPONSE': hex(0x80)
        }

        expected_paged_info_map = {
            'OPERATION': hex(0x0),
            'STATUS_BYTE': hex(0x40),
            'STATUS_WORD': hex(0x40),
            'STATUS_VOUT': hex(0x0),
            'STATUS_IOUT': hex(0x0),
            'STATUS_TEMPERATURE': hex(0x0),
            'STATUS_MFR_SPECIFIC': hex(0x0),
            'OT_FAULT_LIMIT': 1000.0,
            'OT_WARN_LIMIT': 1000.0,
            'OT_FAULT_RESPONSE': hex(0x80),
            'UT_WARN_LIMIT': -1000.0,
            'UT_FAULT_LIMIT': -1000.0,
            'UT_FAULT_RESPONSE': hex(0x80),
            'VOUT_MODE': hex(0x13),
            'VOUT_COMMAND': 5.0,
            'VOUT_MAX': 6.0,
            'VOUT_MARGIN_HIGH': 1.05,  # default value
            'VOUT_MARGIN_LOW': 0.95, # default value
            'VOUT_OV_FAULT_LIMIT': 7.0,
            'VOUT_OV_WARN_LIMIT': 7.0,
            'VOUT_UV_WARN_LIMIT': 0.0,
            'VOUT_UV_FAULT_LIMIT': 0.0,
            'VOUT_OV_FAULT_RESPONSE': hex(0x80),
            'VOUT_UV_FAULT_RESPONSE': hex(0x80),
            'IOUT_OC_FAULT_LIMIT': 4,  # default value
            'IOUT_OC_WARN_LIMIT': 7.0,
            'IOUT_UC_FAULT_LIMIT': -10.0

        }
        for chip in self.chips:
            self.check_chip_common_info_set(chip, expected_chip_info_map, observed_info_list)

            for page in self.pages_in_chip:
                self.check_chip_paged_info_set(chip, expected_paged_info_map, observed_info_list, page)

    def check_chip_common_info_set(self, chip, expected_chip_info_map, observed_info_list):
        for name, expected_value in expected_chip_info_map.items():
            for info_map in observed_info_list:
                if info_map['Name'] == name and info_map['Chip'] == chip:
                    if isinstance(expected_value, float):
                        if abs(info_map['Value'] - expected_value) > 0.2:
                            self.Log('warning', f'Chip: {chip}: {name} observed: {info_map["Value"]} '
                                             f'not the same as expected: {expected_value}')
                    else:
                        if info_map['Value'] != expected_value:
                            self.Log('warning', f'Chip: {chip}: {name} observed: {info_map["Value"]} '
                                             f'not the same as expected: {expected_value}')
                    break
            else:
                self.Log('warning', f'{name} not found in observed info set')

    def check_chip_paged_info_set(self, chip, expected_paged_info_map, observed_info_list, page):
        for name, expected_value in expected_paged_info_map.items():
            for info_map in observed_info_list:
                if info_map['Name'] == name and info_map['Chip'] == chip and info_map['Page'] == page:
                    if isinstance(expected_value, float):
                        if abs(info_map['Value'] - expected_value) > 0.2:
                            self.Log('warning', f'Chip: {chip}, Page {page}: {name} observed: {info_map["Value"]} '
                                             f'not the same as expected: {expected_value}; difference: {abs(info_map["Value"] - expected_value)}')
                    else:
                        if info_map['Value'] != expected_value:
                            self.Log('warning', f'Chip: {chip}, Page {page}: {name} observed: {info_map["Value"]} '
                                             f'not the same as expected: {expected_value}')
                    break
            else:
                self.Log('warning', f'{name} not found in observed info set')

    def print_one_chip_table(self, chip, info_set_list):
        chip_info_set_list = []
        for info in info_set_list:
            if info['Chip'] == chip:
                chip_info_set_list.append(info)
        self.print_info_table(chip_info_set_list, f'Chip {chip} info set')

    def configure_gang_switching(self):
        for chip in self.chips:
            self.hbidps.lc_gang_comp_write(chip, 0x00)
            self.hbidps.lc_gang_track_write(chip, 0x55)

    def enable_lc_chips(self, chips, log=False):
        self.Log('info', f'Enable LC Control')
        mask = 0
        for chip in chips:
            mask |= 1 << chip
        ltc2975_control_reg = self.hbidps.read_bar_register(self.regs.LTC2975_CTRL_REGISTER)
        if log:
            self.Log('info', f'LTC2975_CTRL_REGISTER before enable: {hex(ltc2975_control_reg.value)}')
        ltc2975_control_reg.value |= mask
        self.hbidps.write_bar_register(ltc2975_control_reg)
        if log:
            self.Log('info', f'LTC2975_CTRL_REGISTER after enable: {hex(ltc2975_control_reg.value)}')

    def log_lc_rail_dps_power_state(self):
        lc_rail_power_state = self.get_lc_power_state()
        self.Log('info', f'lc power state: {hex(lc_rail_power_state.value)}')

    def check_lc_rail_dps_power_state(self, chip, rail, expected_state):
        lc_rail_power_state = self.get_lc_power_state()
        bit_index = chip * 8 + rail * 2
        observed_state = (lc_rail_power_state.value >> bit_index) & 0b11
        if expected_state != observed_state:
            self.Log('error', f'Chip {chip}, Rail {rail} observed_state: {bin(observed_state)} '
                              f'!= expected_state" {bin(expected_state)}')

    def disable_lc_chips(self, chips):
        mask = 0
        for chip in chips:
            mask |= 1 << chip
        mask ^= 0xffffffff
        ltc2975_control_reg = self.hbidps.read_bar_register(self.regs.LTC2975_CTRL_REGISTER)
        ltc2975_control_reg.value &= mask
        self.hbidps.write_bar_register(ltc2975_control_reg)

    def check_lc_rails_clean_start_state(self, log=False):
        if self.device_need_reinitialize():
            self.init_lc_rails_hw(log)
        info_set_after = self.read_ltc2975_info_set(log)
        self.check_info_set(info_set_after)
        if self.device_need_reinitialize():
            self.Log('error', f'device reinitialize is not successful')

    def device_need_reinitialize(self):
        re_init = False
        reg = self.hbidps.read_bar_register(self.regs.LTC2975_CTRL_REGISTER)
        if reg.rw_ctrl != 0:
            self.Log('error', f'lc rail ctrl not all off {hex(reg.ctrl)}')
            re_init = True
        reg = self.get_lc_power_state()
        if reg.lc_pwr_state != 0:
            self.Log('error', f'lc rail pwr not all off {hex(reg.rw_lc_pwr_state_slice)}')
            re_init = True
        lc_fold_status = self.check_lc_fold_status()
        if lc_fold_status != 0xFFFF:
            self.Log('error', f'lc rail not all folded {hex(lc_fold_status)}')
            re_init = True
        lc_info_set = self.read_ltc2975_info_set()
        self.check_info_set(lc_info_set)
        for info in lc_info_set:
            if info['Name'] == 'STATUS_BYTE' and info['Value'] != hex(0x40):
                self.Log('warning', f'lc rail Chip {info["Chip"]}, Page {info["Page"]}, '
                                    f'{info["Name"]} is {info["Value"]}')
                re_init = True
        return re_init

    def get_lc_power_state(self):
        return self.hbidps.read_bar_register(self.regs.LC_RAIL_STATE)

    def get_user_data_03(self, chip, page):
        data_03 = self.hbidps.read_ltc2975(chip, 0xb3, 2, page)
        data_03 = self.bytes_to_val(data_03)
        return data_03

    def get_user_data_04(self, chip):
        data_04 = self.hbidps.read_ltc2975(chip, 0xb4, 2)
        data_04 = self.bytes_to_val(data_04)
        return data_04

    def read_reg_command(self, cmd, length, chip, page=-1):
        command = self.hbidps.read_ltc2975(chip, cmd, length, page)
        command = self.bytes_to_val(command)
        return command

    def check_lc_fold_status(self):
        lc_fold_status = self.hbidps.read_bar_register(self.regs.LC_FOLDED)
        lc_fold_status.value = 0xFFFF  # are these register bits also reset by writing a one to it?
        self.hbidps.write_bar_register(lc_fold_status)
        time.sleep(0.1)
        lc_fold_status = self.hbidps.read_bar_register(self.regs.LC_FOLDED)
        return lc_fold_status.value

    def initialize_manufacture_confige(self):
        # low resolution; DAC controled by MFR_DAC??? why 0x420 ???
        self.hbidps.write_bar_register(self.regs.MFR_CONFIG_LTC2975(0x420))

    def chip_common_init(self, chip):
        # WRITE_PROTECT; cmd 0x10; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0x10, self.val_to_byte_reg(0x00))

        # VIN_ON; cmd 0x35; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0x35, self.val_to_l11(9))

        # VIN_OFF; cmd 0x36; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0x36, self.val_to_l11(8))

        # VIN_OV_FAULT_LIMIT; cmd 0x55; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0x55, self.val_to_l11(14))

        # VIN_OV_FAULT_RESPONSE; cmd 0x56; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0x56, self.val_to_byte_reg(0x80))

        # VIN_OV_WARN_LIMIT; cmd 0x57; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0x57, self.val_to_l11(15))

        # VIN_UV_WARN_LIMIT; cmd 0x58; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0x58, self.val_to_l11(0))

        # VIN_UV_FAULT_LIMIT; cmd 0x59; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0x59, self.val_to_l11(0))

        # VIN_UV_FAULT_RESPONSE; cmd 0x5a; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0x5a, self.val_to_byte_reg(0x80))

        # MFR_EIN_CONFIG; cmd 0xc1; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xc1, self.val_to_byte_reg(0x00))

        # MFR_IIN_CAL_GAIN_TC; cmd 0xc3; CF; Not Paged
        self.hbidps.write_ltc2975(chip, 0xc3, self.val_to_word_reg(0x0000))

        # MFR_CONFIG_ALL_LTC2975; cmd 0xd1; Reg Word; Not Paged
        self.hbidps.write_ltc2975(chip, 0xd1, self.val_to_word_reg(0x0f7b))

        # MFR_PWRGD_EN; cmd 0xd4; Reg Word; Not Paged
        self.hbidps.write_ltc2975(chip, 0xd4, self.val_to_word_reg(0x0000))

        # MFR_FAULTB0_RESPONSE; cmd 0xd5; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xd5, self.val_to_byte_reg(0x00))

        # MFR_FAULTB1_RESPONSE; cmd 0xd6; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xd6, self.val_to_byte_reg(0x00))

        # MFR_CONFIG2_LTC2975; cmd 0xd9; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xd9, self.val_to_byte_reg(0x00))

        # MFR_CONFIG3_LTC2975; cmd 0xda; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xda, self.val_to_byte_reg(0x00))

        # MFR_RETRY_DELAY; cmd 0xdb; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0xdb, self.val_to_l11(200))

        # MFR_RESTART_DELAY; cmd 0xdc; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0xdc, self.val_to_l11(0))

        # MFR_POWERGOOD_ASSERTION_DELAY; cmd 0xe1; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0xe1, self.val_to_l11(100))

        # MFR_WATCHDOG_T_FIRST; cmd 0xe2; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0xe2, self.val_to_l11(0))

        # MFR_WATCHDOG_T; cmd 0xe3; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0xe3, self.val_to_l11(0))

        # MFR_PAGE_FF_MASK; cmd 0xe4; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xe4, self.val_to_byte_reg(0x0f))

        # MFR_IIN_CAL_GAIN; cmd 0xe8; L11; Not Paged
        self.hbidps.write_ltc2975(chip, 0xe8, self.val_to_l11(10))

        # MFR_RETRY_COUNT; cmd 0xf7; Reg Byte; Not Paged
        self.hbidps.write_ltc2975(chip, 0xf7, self.val_to_byte_reg(0x07))

    def chip_page_init(self, chip, page):
        # ON_OFF_CONFIG; cmd 0x02; Reg Byte; Paged
        on_off_config = 0b0011100
        self.hbidps.write_ltc2975(chip, 0x02, self.val_to_byte_reg(on_off_config), page)

        # OPERATION; cmd 0x01; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x01, self.val_to_byte_reg(0x00), page)

        # IOUT_CAL_GAIN; cmd 0x38; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x38, self.val_to_l11(10.24267), page)

        # POWER_GOOD_ON; cmd 0x5e; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x5e, self.val_to_l16(0.6), page)

        # POWER_GOOD_OFF; cmd 0x5f; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x5f, self.val_to_l16(0.55), page)

        # MFR_IOUT_CAL_GAIN_TAU_INV; cmd 0xb9; L11; Paged
        self.hbidps.write_ltc2975(chip, 0xb9, self.val_to_l11(0), page)

        # MFR_IOUT_CAL_GAIN_THETA; cmd 0xba; L11; Paged
        self.hbidps.write_ltc2975(chip, 0xBA, self.val_to_l11(1), page)

        # MFR_CONFIG_LTC2975; cmd 0xd0; Reg Word; Paged
        self.hbidps.write_ltc2975(chip, 0xd0, self.val_to_word_reg(0x0420), page)

        # MFR_FAULTB0_PROPAGATE; cmd 0xd2; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0xd2, self.val_to_byte_reg(0x01), page)

        # MFR_FAULTB1_PROPAGATE; cmd 0xd3; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0xd3, self.val_to_byte_reg(0x01), page)

        # UT_FAULT_RESPONSE; cmd 0x54; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x54, self.val_to_byte_reg(0x80), page)

        # OT_FAULT_RESPONSE; cmd 0x50; Reg Byte
        self.hbidps.write_ltc2975(chip, 0x50, self.val_to_byte_reg(0x80), page)

        # IOUT_UC_FAULT_RESPONSE; cmd x04c; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x4c, self.val_to_byte_reg(0x84), page)

        # VOUT_UV_FAULT_RESPONSE; cmd 0x45; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x45, self.val_to_byte_reg(0x80), page)

        # IOUT_OC_FAULT_LIMIT; cmd: 0x46; Output overcurrent fault limit; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x46, self.val_to_l11(4), page)

        # IOUT_OC_FAULT_RESPONSE; cmd 0x47; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x47, self.val_to_byte_reg(0x84), page)

        # VOUT_OV_FAULT_RESPONSE; cmd 0x41; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x41, self.val_to_byte_reg(0x80), page)

        # TON_MAX_FAULT_RESPONSE; cmd 0x63; Reg Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x63, self.val_to_byte_reg(0x80), page)

        # TON_MAX_FAULT_LIMIT; cmd 0x62; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x62, self.val_to_l11(100), page)

        # OT_FAULT_LIMIT; cmd 0x4f; L11; Page
        self.hbidps.write_ltc2975(chip, 0x4f, self.val_to_l11(1000), page)

        # OT_WARN_LIMIT; cmd 0x51; L11; Page
        self.hbidps.write_ltc2975(chip, 0x51, self.val_to_l11(1000), page)

        # UT_WARN_LIMIT; cmd 0x52; L11; Page
        self.hbidps.write_ltc2975(chip, 0x52, self.val_to_l11(-1000), page)

        # UT_FAULT_LIMIT; cmd 0x53; L11; Page
        self.hbidps.write_ltc2975(chip, 0x53, self.val_to_l11(-1000), page)

        # VOUT_OV_WARN_LIMIT; cmd 0x42; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x42, self.val_to_l16(7), page)

        # VOUT_UV_WARN_LIMIT; cmd 0x42; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x43, self.val_to_l16(0), page)

        # IOUT_OC_WARN_LIMIT; cmd 0x4a; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x4a, self.val_to_l11(7), page)

        # VOUT_OV_FAULT_LIMIT; cmd 0x40; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x40, self.val_to_l16(7), page)

        # VOUT_UV_FAULT_LIMIT; cmd 0x44; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x44, self.val_to_l16(0), page)

        # IOUT_UC_FAULT_LIMIT; cmd 0x4b; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x4b, self.val_to_l11(-10), page)

        # TOFF_DELAY; cmd 0x64; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x64, self.val_to_l11(0), page)

        # TON_DELAY; cmd 0x60; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x60, self.val_to_l11(0), page)

        # TON_RISE; cmd 0x61; L11; Paged
        self.hbidps.write_ltc2975(chip, 0x61, self.val_to_l11(0), page)

        # VOUT_MAX; cmd 0x24; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x24, self.val_to_l16(6.0), page)

        # VOUT_COMMAND; cmd 0x21; L16; Paged
        self.hbidps.write_ltc2975(chip, 0x21, self.val_to_l16(5), page)

        # CLEAR_FAULTS; cmd 0x03; Send Byte; Paged
        self.hbidps.write_ltc2975(chip, 0x03, b'', page)

    def set_power_state(self, chip, page, power_state):
        bit_index = chip * 8 + page * 2
        lc_channel_state = self.get_lc_power_state()

        if power_state == 0:
            lc_channel_state.value &= ~(1 << (bit_index + 1))
            lc_channel_state.value &= ~(1 << bit_index)
        if power_state == 1:
            lc_channel_state.value &= ~(1 << (bit_index + 1))
            lc_channel_state.value |= (1 << bit_index)
        elif power_state == 2:
            lc_channel_state.value |= (1 << (bit_index + 1))
            lc_channel_state.value &= ~(1 << bit_index)
        self.hbidps.write_bar_register(lc_channel_state)

    def read_lc_chip_common_info_set(self, chip):
        common_info_set_list = []
        common_info_set_list.extend(self.read_ltc2975_unpaged_status(chip))

        common_info_set_list.extend(self.read_ltc2975_vin(chip))
        common_info_set_list.extend(self.read_ltc2975_iin(chip))
        common_info_set_list.extend(self.read_ltc2975_pin(chip))
        common_info_set_list.extend(self.read_ltc2975_internal_temp(chip))
        return common_info_set_list

    def change_ltc2975_user_data(self, chip):
        self.hbidps.write_ltc2975(chip, 0xB4, self.val_to_byte_reg(0x0001))
        for i in range(4):
            self.hbidps.write_ltc2975(chip, 0xB3, self.val_to_byte_reg(i+1), i)

    def log_ltc2975_user_data(self, chip):
        user_data4 = self.hbidps.read_ltc2975(chip, 0xB4, 2)
        user_data4 = self.bytes_to_val(user_data4)
        self.Log('info', f'chip: {chip} user_data4: {user_data4:04x}')
        for i in range(4):
            user_data3 = self.hbidps.read_ltc2975(chip, 0xB3, 2, i)
            user_data3 = self.bytes_to_val(user_data3)
            self.Log('info', f'chip: {chip} rail: {i} '
                             f'user_data3: {user_data3:04x}')

    def read_ltc2975_unpaged_status(self, chip):
        status_list = []
        # STATUS_INPUT; cmd: 0x7c; Input supply fault and warning status; R Byte; Not Paged
        status_input = self.hbidps.read_ltc2975(chip, 0x7c, 1)
        status_input = self.bytes_to_val(status_input)
        status_list.append({'Chip': chip, 'Page': -1, 'Name': 'STATUS_INPUT', 'Value': hex(status_input)})

        # STATUS_CML; cmd: 0x7e; Communication and memory fault and warning status; R Byte; Not Paged
        status_cml = self.hbidps.read_ltc2975(chip, 0x7e, 1)
        status_cml = self.bytes_to_val(status_cml)
        status_list.append({'Chip': chip, 'Page': -1, 'Name': 'STATUS_CML', 'Value': hex(status_cml)})

        # MFR_PADS; cmd: 0xe5; Current state of selected digital I/O pads. R Ward; Not Paged
        mfr_pads = self.hbidps.read_ltc2975(chip, 0xe5, 2)
        mfr_pads = self.bytes_to_val(mfr_pads)
        status_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_PADS', 'Value': hex(mfr_pads)})

        # MFR_COMMON; cmd 0xef; Manufacturer status bits that are common across multiple LTC chips; R Byte; Not Paged
        mfr_common = self.hbidps.read_ltc2975(chip, 0xef, 1)
        mfr_common = self.bytes_to_val(mfr_common)
        status_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_COMMON', 'Value': hex(mfr_common)})

        # MFR_FAULT_LOG_STATUS; cmd: 0xed; Fault logging status; Reg Byte; Not Paged
        mfr_fault_log_status = self.hbidps.read_ltc2975(chip, 0xed, 1)
        mfr_fault_log_status = self.bytes_to_val(mfr_fault_log_status)
        status_list.append({'Chip': chip, 'Page': -1,
                            'Name': 'MFR_FAULT_LOG_STATUS', 'Value': hex(mfr_fault_log_status)})

        return status_list

    def read_ltc2975_vin(self, chip):
        vin_list = []
        # READ_VIN; cmd: 0x88; Input supply voltage; L11; Not Paged
        vin_read = self.hbidps.read_ltc2975(chip, 0x88, 2)
        vin_read = self.l11_to_val(vin_read)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'READ_VIN', 'Value': vin_read})

        # MFR_VIN_PEAK; cmd: 0xde; Maximum measured value of READ_VIN; L11; Not Paged
        mfr_vin_peak = self.hbidps.read_ltc2975(chip, 0xde, 2)
        mfr_vin_peak = self.l11_to_val(mfr_vin_peak)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_VIN_PEAK', 'Value': mfr_vin_peak})

        # MFR_VIN_MIN; cmd: 0xfc; Minimum measured value of READ_VIN; L11; Not Paged
        mfr_vin_min = self.hbidps.read_ltc2975(chip, 0xfc, 2)
        mfr_vin_min = self.l11_to_val(mfr_vin_min)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_VIN_MIN', 'Value': mfr_vin_min})

        # VIN_ON; cmd: 0x35; Input voltage above which power conversion can be enabled; l11; Not Paged
        vin_on = self.hbidps.read_ltc2975(chip, 0x35, 2)
        vin_on = self.l11_to_val(vin_on)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_ON', 'Value': vin_on})

        # VIN_OFF; cmd: 0x36; Input voltage below which power conversion is disabled; l11; Not Paged
        vin_off = self.hbidps.read_ltc2975(chip, 0x36, 2)
        vin_off = self.l11_to_val(vin_off)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_OFF', 'Value': vin_off})

        # VIN_OV_FAULT_LIMIT; cmd: 0x55; Input ov fault limit measued at VIN_SNS pin; l11; Not Paged
        vin_ov_fault_limit = self.hbidps.read_ltc2975(chip, 0x55, 2)
        vin_ov_fault_limit = self.l11_to_val(vin_ov_fault_limit)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_OV_FAULT_LIMIT', 'Value': vin_ov_fault_limit})

        # VIN_OV_WARN_LIMIT; cmd: 0x57; Input ov warning limit measured at VIN_SNS pin; l11; Not Paged
        vin_ov_warn_limit = self.hbidps.read_ltc2975(chip, 0x57, 2)
        vin_ov_warn_limit = self.l11_to_val(vin_ov_warn_limit)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_OV_WARN_LIMIT', 'Value': vin_ov_warn_limit})

        # VIN_OV_FAULT_RESPONSE; cmd: 0x56; Action to take when input ov fault is detected; Reg Byte; Not Paged
        vin_ov_fault_response = self.hbidps.read_ltc2975(chip, 0x56, 1)
        vin_ov_fault_response = self.bytes_to_val(vin_ov_fault_response)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_OV_FAULT_RESPONSE',
                         'Value': hex(vin_ov_fault_response)})

        # VIN_UV_WARN_LIMIT; cmd: 0x58; Input uv warning limit measured at VIN_SNS pin; l11; Not Paged
        vin_uv_warn_limit = self.hbidps.read_ltc2975(chip, 0x58, 2)
        vin_uv_warn_limit = self.l11_to_val(vin_uv_warn_limit)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_UV_WARN_LIMIT', 'Value': vin_uv_warn_limit})

        # VIN_UV_FAULT_LIMIT; cmd: 0x59; Input uv fault limit measured at VIN_SNS_pin; l11; Not Paged
        vin_uv_fault_limit = self.hbidps.read_ltc2975(chip, 0x59, 2)
        vin_uv_fault_limit = self.l11_to_val(vin_uv_fault_limit)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_UV_FAULT_LIMIT', 'Value': vin_uv_fault_limit})

        # VIN_UV_FAULT_RESPONSE; cmd: 0x5a; Action to take when input uv fault detected; Reg Byte; Not Paged
        vin_uv_fault_response = self.hbidps.read_ltc2975(chip, 0x5a, 1)
        vin_uv_fault_response = self.bytes_to_val(vin_uv_fault_response)
        vin_list.append({'Chip': chip, 'Page': -1, 'Name': 'VIN_UV_FAULT_RESPONSE',
                         'Value': hex(vin_uv_fault_response)})

        return vin_list

    def read_ltc2975_iin(self, chip):
        iin_list = []
        # READ_IIN; cmd: 0x89; DC/DC converter input current; L11; Not Paged
        iin_read = self.hbidps.read_ltc2975(chip, 0x89, 2)
        iin_read = self.l11_to_val(iin_read)
        iin_list.append({'Chip': chip, 'Page': -1, 'Name': 'READ_IIN', 'Value': iin_read})

        # MFR_IIN_PEAK; cmd: 0xc4; Maximum measured value of READ_IIN; L11; Not Paged
        mfr_iin_peak = self.hbidps.read_ltc2975(chip, 0xc4, 2)
        mfr_iin_peak = self.l11_to_val(mfr_iin_peak)
        iin_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_IIN_PEAK', 'Value': mfr_iin_peak})

        # MFR_IIN_MIN; cmd: 0xc5; Minimum measured value of READ_IIN; L11; Not Paged
        mfr_iin_min = self.hbidps.read_ltc2975(chip, 0xc5, 2)
        mfr_iin_min = self.l11_to_val(mfr_iin_min)
        iin_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_IIN_MIN', 'Value': mfr_iin_min})

        return iin_list

    def read_ltc2975_pin(self, chip):
        pin_list = []
        # READ_PIN; cmd: 0x97; DC/DC converter input power; L11; Not Paged
        pin_read = self.hbidps.read_ltc2975(chip, 0x97, 2)
        pin_read = self.l11_to_val(pin_read)
        pin_list.append({'Chip': chip, 'Page': -1, 'Name': 'READ_PIN', 'Value': pin_read})

        # MFR_PIN_PEAK; cmd: 0xc6; Maximum measured value of READ_PIN; L11; Not Paged
        mfr_pin_peak = self.hbidps.read_ltc2975(chip, 0xc6, 2)
        mfr_pin_peak = self.l11_to_val(mfr_pin_peak)
        pin_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_PIN_PEAK', 'Value': mfr_pin_peak})

        # MFR_PIN_MIN; cmd: 0xc7; Minimum measured value of READ_PIN; L11; Not Paged
        mfr_pin_min = self.hbidps.read_ltc2975(chip, 0xc7, 2)
        mfr_pin_min = self.l11_to_val(mfr_pin_min)
        pin_list.append({'Chip': chip, 'Page': -1, 'Name': 'MFR_PIN_PEAK', 'Value': mfr_pin_min})

        return pin_list

    def read_ltc2975_internal_temp(self, chip):
        internal_temp_list = []
        # READ_TEMPERATURE_2; cmd: 0x8e; Internal junction temperature; L11; Not Paged
        temperature_2_read = self.hbidps.read_ltc2975(chip, 0x8e, 2)
        temperature_2_read = self.l11_to_val(temperature_2_read)
        internal_temp_list.append({'Chip': chip, 'Page': -1,
                                  'Name': 'READ_TEMPERATURE_2', 'Value': temperature_2_read})

        return internal_temp_list

    def read_lc_chip_page_info_set(self, chip, page):
        paged_info_set_list = []
        paged_info_set_list.extend(self.read_ltc2975_paged_status(chip, page))
        paged_info_set_list.extend(self.read_ltc2975_paged_temperature(chip, page))
        paged_info_set_list.extend(self.read_ltc2975_vout(chip, page))
        paged_info_set_list.extend(self.read_ltc2975_paged_iout_and_pout(chip, page))

        return paged_info_set_list

    def check_status_word(self):
        for chip in self.chips:
            for page in self.pages_in_chip:
                rail = chip * 4 + page
                status_word = self.hbidps.read_ltc2975(chip, 0x79, 2, page)
                status_word = self.bytes_to_val(status_word)
                if status_word & (1 << 7):
                    self.Log('warning', f'Rail {rail}: busy bit raised, status: {hex(status_word)}')
                else:
                    self.Log('info', f'Rail {rail}: busy bit not raised, status: {hex(status_word)}')

    def check_busy_status(self):
        for chip in self.chips:
            for page in self.pages_in_chip:
                rail = chip * 4 + page
                status_word = self.hbidps.read_ltc2975(chip, 0x79, 2, page)
                status_word = self.bytes_to_val(status_word)
                if status_word & (1 << 7):
                    self.Log('error', f'Rail {rail}: busy bit raised, status: {hex(status_word)}')

    def read_ltc2975_paged_status(self, chip, page):
        paged_status_list = []

        # Operation; cmd: 0x01; Operating mode control; R Byte; Paged;
        operation = self.hbidps.read_ltc2975(chip, 0x01, 1, page)
        operation = self.bytes_to_val(operation)
        paged_status_list.append({'Chip': chip, 'Page': page, 'Name': 'OPERATION', 'Value': hex(operation)})

        # STATUS_BYTE; cmd: 0x78; One byte summary of the unit's fault condition; R Byte; Paged
        status_byte = self.hbidps.read_ltc2975(chip, 0x78, 1, page)
        status_byte = self.bytes_to_val(status_byte)
        paged_status_list.append({'Chip': chip, 'Page': page, 'Name': 'STATUS_BYTE', 'Value': hex(status_byte)})

        # STATUS_WORD; cmd: 0x79; Two byte summary of the unit's fault condition; R Word; Paged
        status_word = self.hbidps.read_ltc2975(chip, 0x79, 2, page)
        status_word = self.bytes_to_val(status_word)
        paged_status_list.append({'Chip': chip, 'Page': page, 'Name': 'STATUS_WORD', 'Value': hex(status_word)})

        # STATUS_VOUT; cmd: 0x7a; Output voltage fault and warning sdtatus; R Byte; Paged
        status_vout = self.hbidps.read_ltc2975(chip, 0x7a, 1, page)
        status_vout = self.bytes_to_val(status_vout)
        paged_status_list.append({'Chip': chip, 'Page': page, 'Name': 'STATUS_VOUT', 'Value': hex(status_vout)})

        # STATUS_IOUT; cmd: 0x7b; OUTPUT current fault and warning statusl R Byte; Paged
        status_iout = self.hbidps.read_ltc2975(chip, 0x7b, 1, page)
        status_iout = self.bytes_to_val(status_iout)
        paged_status_list.append({'Chip': chip, 'Page': page, 'Name': 'STATUS_IOUT', 'Value': hex(status_iout)})

        # STATUS_TEMPERATURE; cmd: 0x7d; External temperature fault and warning status; R Byte; Paged
        status_temperature = self.hbidps.read_ltc2975(chip, 0x7d, 1, page)
        status_temperature = self.bytes_to_val(status_temperature)
        paged_status_list.append({'Chip': chip, 'Page': page,
                                  'Name': 'STATUS_TEMPERATURE', 'Value': hex(status_temperature)})

        # STATUS_MFR_SPECIFIC; cmd: 0x80; Manufacturer specific fault and state information. R Byte; Paged
        status_mfr_specific = self.hbidps.read_ltc2975(chip, 0x80, 1, page)
        status_mfr_specific = self.bytes_to_val(status_mfr_specific)
        paged_status_list.append({'Chip': chip, 'Page': page,
                                  'Name': 'STATUS_MFR_SPECIFIC', 'Value': hex(status_mfr_specific)})

        return paged_status_list

    def read_ltc2975_paged_temperature(self, chip, page):
        paged_temp_list = []
        # READ_TEMPERATURE_1; cmd: 0x8d; External diode junction temperature; l11; Paged
        temperature_1 = self.hbidps.read_ltc2975(chip, 0x8d, 2, page)
        temperature_1 = self.l11_to_val(temperature_1)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'READ_TEMPERATURE_1', 'Value': temperature_1})

        # MFR_TEPERATURE_1_MIN; cmd:0xfd; Minimum measured value of READ_TEMPERATURE_1; L11; Paged
        mfr_temperature_1_min = self.hbidps.read_ltc2975(chip, 0xfd, 2, page)
        mfr_temperature_1_min = self.l11_to_val(mfr_temperature_1_min)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_TEMPERATURE_1_MIN',
                                'Value': mfr_temperature_1_min})

        # MFR_TEMPERATURE_1_PEAK; cmd: 0xdf; Maximum measured value of READ_TEMPERATURE_1; L11; Paged
        mfr_temperature_1_peak = self.hbidps.read_ltc2975(chip, 0xdf, 2, page)
        mfr_temperature_1_peak = self.l11_to_val(mfr_temperature_1_peak)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_TEMPERATURE_1_PEAK',
                                'Value': mfr_temperature_1_peak})

        # OT_FAULT_LIMIT; cmd: 0x4f; Overtemperature fault limit for external temperature senser; L11; Paged
        ot_fault_limit = self.hbidps.read_ltc2975(chip, 0x4f, 2, page)
        ot_fault_limit = self.l11_to_val(ot_fault_limit)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'OT_FAULT_LIMIT', 'Value': ot_fault_limit})

        # OT_WARN_LIMIT; cmd: 0x51; Overtemperature warning limit for the external temperature sensor; L11; Paged
        ot_warn_limit = self.hbidps.read_ltc2975(chip, 0x51, 2, page)
        ot_warn_limit = self.l11_to_val(ot_warn_limit)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'OT_WARN_LIMIT', 'Value': ot_warn_limit})

        # OT_FAULT_RESPONSE; cmd: 0x50; Action taken when ot fault is detected; Reg Byte; Paged
        ot_fault_response = self.hbidps.read_ltc2975(chip, 0x50, 1, page)
        ot_fault_response = self.bytes_to_val(ot_fault_response)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'OT_FAULT_RESPONSE',
                                'Value': hex(ot_fault_response)})

        # UT_WARN_LIMIT; cmd: 0x52; Undertemperature warning limit for the external temperature sensor; L11; Paged
        ut_warn_limit = self.hbidps.read_ltc2975(chip, 0x52, 2, page)
        ut_warn_limit = self.l11_to_val(ut_warn_limit)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'UT_WARN_LIMIT', 'Value': ut_warn_limit})

        # UT_FAULT_LIMIT; cmd: 0x53; Undertemperature fault limit for the external temperature sensor; L11; Paged
        ut_fault_limit = self.hbidps.read_ltc2975(chip, 0x53, 2, page)
        ut_fault_limit = self.l11_to_val(ut_fault_limit)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'UT_FAULT_LIMIT', 'Value': ut_fault_limit})

        # UT_FAULT_RESPONSE; cmd: 0x54; Action taken when ut fault is detected; Reg Byte; Paged
        ut_fault_response = self.hbidps.read_ltc2975(chip, 0x54, 1, page)
        ut_fault_response = self.bytes_to_val(ut_fault_response)
        paged_temp_list.append({'Chip': chip, 'Page': page, 'Name': 'UT_FAULT_RESPONSE',
                                'Value': hex(ut_fault_response)})

        return paged_temp_list

    def read_ltc2975_vout(self, chip, page):
        vout_list = []
        # VOUT_MODE; cmd: 0x20; Output voltage data format and mantissa exponent; Reg Byte; Paged
        vout_mode = self.hbidps.read_ltc2975(chip, 0x20, 1, page)
        vout_mode = self.bytes_to_val(vout_mode)
        vout_mode_type = (vout_mode & 0b11100000) >> 5
        vout_mode_parameter = (vout_mode & 0b00011111)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_MODE', 'Value': hex(vout_mode)})
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_MODE_TYPE', 'Value': hex(vout_mode_type)})
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_MODE_PARAMETER', 'Value': hex(vout_mode_parameter)})

        # VOUT_COMMAND; cmd: 0x21; Servo target. Nominal DC/DC converter output voltage setpoint; l16; Paged
        vout_command = self.hbidps.read_ltc2975(chip, 0x21, 2, page)
        vout_command = self.l16_to_val(vout_command)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_COMMAND', 'Value': vout_command})

        # VOUT_MAX; cmd: 0x24; Upper limit vout can command; l16; Paged
        vout_max = self.hbidps.read_ltc2975(chip, 0x24, 2, page)
        vout_max = self.l16_to_val(vout_max)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_MAX', 'Value': vout_max})

        # check operation cmd; margin high higher than vout_max
        # VOUT_MARGIN_HIGH; cmd: 0x25; Margin high DC/DC converter output voltage setting; l16; Paged
        vout_margin_high = self.hbidps.read_ltc2975(chip, 0x25, 2, page)
        vout_margin_high = self.l16_to_val(vout_margin_high)
        # read plain data
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_MARGIN_HIGH', 'Value': vout_margin_high})

        # VOUT_MARGIN_LOW; cmd: 0x26; Margin low DC/DC converter output voltage setting; l16; Paged
        vout_margin_low = self.hbidps.read_ltc2975(chip, 0x26, 2, page)
        vout_margin_low = self.l16_to_val(vout_margin_low)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_MARGIN_LOW', 'Value': vout_margin_low})

        # READ_VOUT; cmd: 0x8b; DC/DC converter output voltage; L16; Paged
        vout_read = self.hbidps.read_ltc2975(chip, 0x8b, 2, page)
        vout_read = self.l16_to_val(vout_read)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'READ_VOUT', 'Value': vout_read})

        # MFR_VOUT_PEAK; cmd: 0xdd; Maximum measured value of READ_VOUT; L16; Paged
        mfr_vout_peak = self.hbidps.read_ltc2975(chip, 0xdd, 2, page)
        mfr_vout_peak = self.l16_to_val(mfr_vout_peak)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_VOUT_PEAK', 'Value': mfr_vout_peak})

        # MFR_VOUT_MIN; cmd: 0xfb; Minimum measured value of READ_VOUT; L16; Paged;
        mfr_vout_min = self.hbidps.read_ltc2975(chip, 0xfb, 2, page)
        mfr_vout_min = self.l16_to_val(mfr_vout_min)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_VOUT_MIN', 'Value': mfr_vout_min})

        # VOUT_OV_FAULT_LIMIT; cmd: 0x40; Output overvoltage fault limit; l16; Paged
        vout_ov_fault_limit = self.hbidps.read_ltc2975(chip, 0x40, 2, page)
        vout_ov_fault_limit = self.l16_to_val(vout_ov_fault_limit)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_OV_FAULT_LIMIT', 'Value': vout_ov_fault_limit})

        # VOUT_OV_WARN_LIMIT; cmd: 0x42; Output overvoltage warning limit; l16; Paged
        vout_ov_warn_limit = self.hbidps.read_ltc2975(chip, 0x42, 2, page)
        vout_ov_warn_limit = self.l16_to_val(vout_ov_warn_limit)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_OV_WARN_LIMIT', 'Value': vout_ov_warn_limit})

        # VOUT_OV_FAULT_RESPONSE; cmd: 0x41; Action to take when vout ov fault detected; reg Byte; Paged
        vout_ov_fault_response = self.hbidps.read_ltc2975(chip, 0x41, 1, page)
        vout_ov_fault_response = self.bytes_to_val(vout_ov_fault_response)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_OV_FAULT_RESPONSE',
                          'Value': hex(vout_ov_fault_response)})

        # VOUT_UV_WARN_LIMIT; cmd: 0x43; Output undervoltage warning limit; l16; Paged
        vout_uv_warn_limit = self.hbidps.read_ltc2975(chip, 0x43, 2, page)
        vout_uv_warn_limit = self.l16_to_val(vout_uv_warn_limit)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_UV_WARN_LIMIT', 'Value': vout_uv_warn_limit})

        # VOUT_UV_FAULT_LIMIT; cmd: 0x44; Output undervoltage fault limit; l16; Paged
        vout_uv_fault_limit = self.hbidps.read_ltc2975(chip, 0x44, 2, page)
        vout_uv_fault_limit = self.l16_to_val(vout_uv_fault_limit)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_UV_FAULT_LIMIT', 'Value': vout_uv_fault_limit})

        # VOUT_UV_FAULT_RESPONSE; cmd: 0x45; Action to take when output uv detected; Reg Byte; Paged
        vout_uv_fault_response = self.hbidps.read_ltc2975(chip, 0x45, 1, page)
        vout_uv_fault_response = self.bytes_to_val(vout_uv_fault_response)
        vout_list.append({'Chip': chip, 'Page': page, 'Name': 'VOUT_UV_FAULT_RESPONSE',
                          'Value': hex(vout_uv_fault_response)})

        return vout_list

    def read_ltc2975_paged_iout_and_pout(self, chip, page):
        iout_list = []
        # READ_IOUT; cmd: 8x8c; DC/DC converter output current; L11; Paged
        iout_read = self.hbidps.read_ltc2975(chip, 0x8c, 2, page)
        iout_read = self.l11_to_val(iout_read)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'READ_IOUT', 'Value': iout_read})

        # MFR_READ_IOUT; cmd: 0xbb; Alternate data format for READ_IOUT. One LSB = 2.5 mA; CF; Paged
        mfr_iout_read = self.hbidps.read_ltc2975(chip, 0xbb, 2, page)
        mfr_iout_read = self.bytes_to_val(mfr_iout_read)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_READ_IOUT', 'Value': hex(mfr_iout_read)})

        # MFR_IOUT_SENSE_VOLTAGE; cmd: 0xfa; CF; Paged
        mfr_iout_sense_voltage = self.hbidps.read_ltc2975(chip, 0xfa, 2, page)
        mfr_iout_sense_voltage = self.bytes_to_val(mfr_iout_sense_voltage)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_IOUT_SENSE_VOLTAGE',
                          'Value': hex(mfr_iout_sense_voltage)})

        # MFR_IOUT_PEAK; cmd: 0xd7; Maximum measured value of READ_IOUT; L11; Paged
        mfr_iout_peak = self.hbidps.read_ltc2975(chip, 0xd7, 2, page)
        mfr_iout_peak = self.l11_to_val(mfr_iout_peak)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_IOUT_PEAK', 'Value': mfr_iout_peak})

        # MFR_IOUT_MIN; cmd:0xd8; Minimum measured value of READ_IOUT; L11; Paged;
        mfr_iout_min = self.hbidps.read_ltc2975(chip, 0xd8, 2, page)
        mfr_iout_min = self.l11_to_val(mfr_iout_min)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'MFR_IOUT_MIN', 'Value': mfr_iout_min})

        # IOUT_OC_FAULT_LIMIT; cmd: 0x46; Output overcurrent fault limit; L11; Paged
        iout_oc_fault_limit = self.hbidps.read_ltc2975(chip, 0x46, 2, page)
        iout_oc_fault_limit = self.l11_to_val(iout_oc_fault_limit)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'IOUT_OC_FAULT_LIMIT', 'Value': iout_oc_fault_limit})

        # IOUT_OC_WARN_LIMIT; cmd: 0x4a; Output overcurrent warning limit; l11; Paged
        iout_oc_warn_limit = self.hbidps.read_ltc2975(chip, 0x4a, 2, page)
        iout_oc_warn_limit = self.l11_to_val(iout_oc_warn_limit)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'IOUT_OC_WARN_LIMIT', 'Value': iout_oc_warn_limit})

        # IOUT_UC_FAULT_LIMIT; cmd: 0x4b; Output undercurrent fault limit, must be negative vaule; l11; Paged
        iout_uc_fault_limit = self.hbidps.read_ltc2975(chip, 0x4b, 2, page)
        iout_uc_fault_limit = self.l11_to_val(iout_uc_fault_limit)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'IOUT_UC_FAULT_LIMIT', 'Value': iout_uc_fault_limit})

        # READ_POUT; cmd: 0x96; DC/DC converter output power; L11; Paged
        pout_read = self.hbidps.read_ltc2975(chip, 0x96, 2, page)
        pout_read = self.l11_to_val(pout_read)
        iout_list.append({'Chip': chip, 'Page': page, 'Name': 'READ_POUT', 'Value': pout_read})

        return iout_list

    def bytes_to_val(self, data_bytes):
        return int.from_bytes(data_bytes, 'little')

    def l16_to_val(self, data_bytes):
        mode = 0b10011
        return self.hbidps.froml16(data_bytes, mode)

    def l11_to_val(self, data_bytes):
        return self.hbidps.froml11(data_bytes)

    def val_to_l16(self, data_val):
        mode = 0b10011
        return self.hbidps.tol16(data_val, mode)

    def val_to_l11(self, data_val):
        return self.hbidps.tol11(data_val)

    def val_to_byte_reg(self, data):
        return data.to_bytes(1, 'little')

    def val_to_word_reg(self, data):
        return data.to_bytes(2, 'little')

