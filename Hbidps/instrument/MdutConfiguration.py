# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class MdutConfiguration(Object):

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.regs = hbidps_register

    def set_uhc_dut_status(self, uhc_id, dut_id, valid, log=False):
        uhc = self.hbidps.read_bar_register(self.regs.DUT_DOMAIN_ID_NN, index=uhc_id)
        if log:
            self.Log('info', f'before the change')
            self.Log('info', f'mdut_uhc{uhc_id} valid: '
                             f'{uhc.dut_domain_id_valid} '
                             f'dut_id: {uhc.dut_domain_id}')
        uhc.dut_domain_id = dut_id
        uhc.dut_domain_id_valid = valid
        self.hbidps.write_bar_register(uhc)
        uhc = self.hbidps.read_bar_register(self.regs.DUT_DOMAIN_ID_NN,index=uhc_id)
        if log:
            self.Log('info', f'after the change')
            self.Log('info', f'mdut_uhc{uhc_id} '
                             f'valid: {uhc.dut_domain_id_valid} '
                             f'dut_id: {uhc.dut_domain_id}')

    def clear_all_uhc_hc_rail_status(self, log=False, ignore_error=False):
        for uhc_id in [0, 1, 2, 3]:
            uhc = self.hbidps.read_bar_register(self.regs.HC_DUT_NN_RAIL_CONFIG, index=uhc_id)
            uhc.rw_rail_config = 0
            self.hbidps.write_bar_register(uhc)
            uhc = self.hbidps.read_bar_register(self.regs.HC_DUT_NN_RAIL_CONFIG, index=uhc_id)
            assigned_rails = uhc.rw_rail_config
            if (assigned_rails != 0) and (not ignore_error):
                self.Log('error', f'hc uhc reset, uhc{uhc_id} not zero 0b{assigned_rails:010b}')
            elif log:
                self.Log('info', f'hc uhc reset, uhc{uhc_id} zero 0b{assigned_rails:010b}')
            else:
                self.Log('debug', f'hc uhc reset, uhc{uhc_id} zero 0b{assigned_rails:010b}')

    def set_uhc_hc_rail_status(self, uhc_id, rails, log=False, ignore_error=False):
        uhc = self.hbidps.read_bar_register(self.regs.HC_DUT_NN_RAIL_CONFIG, index=uhc_id)
        if log:
            self.Log('info', f'before the change')
            self.Log('info', f'mdut_uhc{uhc_id} rails: '
                             f'{uhc.rw_rail_config} ')
        uhc.rw_rail_config = rails
        self.hbidps.write_bar_register(uhc)
        uhc = self.hbidps.read_bar_register(self.regs.HC_DUT_NN_RAIL_CONFIG, index=uhc_id)
        assigned_rails = uhc.rw_rail_config
        if (assigned_rails != rails) and (not ignore_error):
            self.Log('error', f'hc uhc set, uhc{uhc_id} 0b{assigned_rails:010b}; expect 0b{rails:010b}')
        elif log:
            self.Log('info', f'hc uhc set, uhc{uhc_id} 0b{assigned_rails:010b}; expect 0b{rails:010b}')
        else:
            self.Log('debug', f'hc uhc set, uhc{uhc_id} 0b{assigned_rails:010b}; expect 0b{rails:010b}')

    def clear_all_uhc_lc_rail_status(self, log=False, ignore_error=False):
        for uhc_id in [0, 1, 2, 3]:
            uhc = self.hbidps.read_bar_register(self.regs.LC_DUT_NN_RAIL_CONFIG, index=uhc_id)
            uhc.rw_rail_config = 0
            self.hbidps.write_bar_register(uhc)
            uhc = self.hbidps.read_bar_register(self.regs.LC_DUT_NN_RAIL_CONFIG, index=uhc_id)
            assigned_rails = uhc.rw_rail_config
            if (assigned_rails != 0) and (not ignore_error):
                self.Log('error', f'lc uhc reset, uhc{uhc_id} not zero 0b{assigned_rails:016b}')
            elif log:
                self.Log('info', f'lc uhc reset, uhc{uhc_id} zero 0b{assigned_rails:016b}')
            else:
                self.Log('debug', f'lc uhc reset, uhc{uhc_id} zero 0b{assigned_rails:016b}')

    def set_uhc_lc_rail_status(self, uhc_id, rails, log=False, ignore_error=False):
        uhc = self.hbidps.read_bar_register(self.regs.LC_DUT_NN_RAIL_CONFIG, index=uhc_id)
        if log:
            self.Log('info', f'before the change')
            self.Log('info', f'mdut_uhc{uhc_id} rails: '
                             f'{uhc.rw_rail_config} ')
        uhc.rw_rail_config = rails
        self.hbidps.write_bar_register(uhc)
        uhc = self.hbidps.read_bar_register(self.regs.LC_DUT_NN_RAIL_CONFIG, index=uhc_id)
        assigned_rails = uhc.rw_rail_config
        if (assigned_rails != rails) and (not ignore_error):
            self.Log('error', f'lc uhc set, uhc{uhc_id} 0b{assigned_rails:016b}; expect 0b{rails:016b}')
        elif log:
            self.Log('info', f'lc uhc set, uhc{uhc_id} 0b{assigned_rails:016b}; expect 0b{rails:016b}')
        else:
            self.Log('debug', f'lc uhc set, uhc{uhc_id} 0b{assigned_rails:016b}; expect 0b{rails:016b}')

    def clear_all_uhc_vtarget_rail_status(self, log=False, ignore_error=False):
        for uhc_id in [0, 1, 2, 3]:
            uhc = self.hbidps.read_bar_register(self.regs.VTARGER_DUT_NN_RAIL_CONFIG, index=uhc_id)
            uhc.rw_rail_config = 0
            self.hbidps.write_bar_register(uhc)
            uhc = self.hbidps.read_bar_register(self.regs.VTARGER_DUT_NN_RAIL_CONFIG, index=uhc_id)
            assigned_rails = uhc.rw_rail_config
            if (assigned_rails != 0) and (not ignore_error):
                self.Log('error', f'vt uhc reset, uhc{uhc_id} not zero 0b{assigned_rails:016b}')
            elif log:
                self.Log('info', f'vt uhc reset, uhc{uhc_id} zero 0b{assigned_rails:016b}')
            else:
                self.Log('debug', f'vt uhc reset, uhc{uhc_id} zero 0b{assigned_rails:016b}')

    def set_uhc_vtarget_rail_status(self, uhc_id, rails, log=False, ignore_error=False):
        uhc = self.hbidps.read_bar_register(self.regs.VTARGER_DUT_NN_RAIL_CONFIG, index=uhc_id)
        if log:
            self.Log('info', f'before the change')
            self.Log('info', f'mdut_uhc{uhc_id} rails: '
                             f'{uhc.rw_rail_config} ')
        uhc.rw_rail_config = rails
        self.hbidps.write_bar_register(uhc)
        uhc = self.hbidps.read_bar_register(self.regs.VTARGER_DUT_NN_RAIL_CONFIG, index=uhc_id)
        assigned_rails = uhc.rw_rail_config
        if (assigned_rails != rails) and (not ignore_error):
            self.Log('error', f'vt uhc set, uhc{uhc_id} 0b{assigned_rails:016b}; expect 0b{rails:016b}')
        elif log:
            self.Log('info', f'vt uhc set, uhc{uhc_id} 0b{assigned_rails:016b}; expect 0b{rails:016b}')
        else:
            self.Log('debug', f'vt uhc set, uhc{uhc_id} 0b{assigned_rails:016b}; expect 0b{rails:016b}')

    def check_uhc_dut_status(self):
        for uhc in range(4):
            uhc_n = self.hbidps.read_bar_register(self.regs.DUT_DOMAIN_ID_NN, index=uhc)
            self.Log('info', f'mdut_uhc{uhc} valid: {uhc_n.dut_domain_id_valid} '
                             f'dut_id: {uhc_n.dut_domain_id}')

    def reset_uhc_dut_rails(self, uhc_id):
        self.set_uhc_hc_rail_status(uhc_id, 0)
        self.set_uhc_lc_rail_status(uhc_id, 0)
        self.set_uhc_vtarget_rail_status(uhc_id, 0)

