# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
from enum import Enum
from time import perf_counter, sleep

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class device(Enum):
    NONE = 0x00
    FPGA = 0x10
    LCrail0 = 0x20
    LCrail1 = 0x21
    LCrail2 = 0x22
    LCrail3 = 0x23
    LCrail4 = 0x24
    LCrail5 = 0x25
    LCrail6 = 0x26
    LCrail7 = 0x27
    LCrail8 = 0x28
    LCrail9 = 0x29
    LCrail10 = 0x2a
    LCrail11 = 0x2b
    LCrail12 = 0x2c
    LCrail13 = 0x2d
    LCrail14 = 0x2e
    LCrail15 = 0x2f
    HCrail0 = 0x30
    HCrail1 = 0x31
    HCrail2 = 0x32
    HCrail3 = 0x33
    HCrail4 = 0x34
    HCrail5 = 0x35
    HCrail6 = 0x36
    HCrail7 = 0x37
    HCrail8 = 0x38
    HCrail9 = 0x39
    Vtarget0 = 0x40
    Vtarget1 = 0x41
    Vtarget2 = 0x42
    Vtarget3 = 0x43
    Vtarget4 = 0x44
    Vtarget5 = 0x45
    Vtarget6 = 0x46
    Vtarget7 = 0x47
    Vtarget8 = 0x48
    Vtarget9 = 0x49
    Vtarget10 = 0x4a
    Vtarget11 = 0x4b
    Vtarget12 = 0x4c
    Vtarget13 = 0x4d
    Vtarget14 = 0x4e
    Vtarget15 = 0x4f
    LCSampleEngine0 = 0x50
    LCSampleEngine1 = 0x51
    LCSampleEngine2 = 0x52
    LCSampleEngine3 = 0x53
    HCSampleEngine0 = 0x60
    HCSampleEngine1 = 0x61
    HCSampleEngine2 = 0x62
    HCSampleEngine3 = 0x63
    VmeasSampleEngine0 = 0x70
    VmeasSampleEngine1 = 0x71
    VmeasSampleEngine2 = 0x72
    VmeasSampleEngine3 = 0x73
    VmeasSampleEngine4 = 0x74
    LCBroadcast = 0x80
    HCBroadcast = 0x81
    VtargetBroadcast = 0x82


class command(Enum):
    send_byte = 0x00
    write_byte = 0x01
    read_byte = 0x02
    write_word = 0x03
    read_word = 0x04
    delay = 0x30
    smp_count = 0x40
    smp_ratio = 0x41
    smp_select = 0x42
    smp_metadata = 0x43
    smp_sshot_count_hi = 0x44
    vtarg_pwr = 0x50
    vtarg_set = 0x51
    set_hi_i_limit = 0x60
    set_ov_limit = 0x61
    set_uv_limit = 0x62
    set_v = 0x63
    set_pwr_state = 0x64
    set_fdt = 0x65
    set_fd_current_hi = 0x66
    set_vrange = 0x67
    set_irange = 0x68
    apply_user_attributes = 0x70
    flush_user_attributes = 0x71
    wake = 0xa0
    set_busy = 0xa1
    tq_notify = 0xa2
    tq_end = 0xff


class TriggerQueueDDR(Object):
    """ Helper Class for HBI DPS TQ DDR Operation

        Usage Example:
        get_tq_data()
        log_tq_list()
        get_dps_lut_address()
        get_tq_content_address()
        store_tq_address_to_lut()
        store_tq_content_to_mem()
        clear_tq_notify()
        send_dps_trigger_through_rctc()
        check_tq_notify()
    """

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.regs = hbidps_register
        self.dps_base_address = 0x0
        self.ltc_base_address = 0x8_0000

    def get_dps_lut_address(self, uhc, index):
        dps_lut_address = \
            self.dps_base_address + 0x10000 * uhc + 0x4 * index
        return dps_lut_address

    def get_ltc_lut_address(self, uhc, index):
        ltc_lut_address = \
            self.ltc_base_address + 0x10000 * uhc + 0x4 * index
        return ltc_lut_address

    def get_tq_data(self, tq_list, log=False):
        if log:
            self.log_tq_list(tq_list)
        tq_data = 0
        list_len = len(tq_list)
        for i in range(list_len):
            tq_data = (tq_data << 64) + tq_list[list_len-1-i]
        return tq_data

    def log_tq_list(self, tq_list):
        self.Log('info', 'tq log start')
        list_len = len(tq_list)
        for i in range(list_len):
            self.Log('info', f'{hex(tq_list[i])} line {i}')
        self.Log('info', 'tq log end')

    def get_tq_content_address(self, tq_address=0x10_0000):
        tq_content_address = tq_address
        return tq_content_address

    def store_tq_address_to_lut(self, lut_address, tq_content_address, log=False):
        data_bytes = tq_content_address.to_bytes(4, byteorder='little')
        self.hbidps.dma_write(lut_address, data_bytes)
        if log:
            string = self.hbidps.dma_read(lut_address, 4)
            dma_read = int.from_bytes(string, byteorder='little')
            self.Log('info', f'lut_address {hex(lut_address)}; data {hex(dma_read)}')

    def store_tq_content_to_mem(self, tq_content_address, tq_data, log=False):
        num_bytes = ((tq_data.bit_length() + 63) // 64)*8
        data_bytes = tq_data.to_bytes(num_bytes, byteorder='little')
        self.hbidps.dma_write(tq_content_address, data_bytes)
        if log:
            string = self.hbidps.dma_read(tq_content_address, num_bytes)
            dma_read = int.from_bytes(string, byteorder='little')
            self.Log('info', f'tq_address {hex(tq_content_address)}; data {hex(dma_read)}')

    def send_dps_trigger_through_rctc(self, card_type, dut_id, index):
        trigger = self.generate_to_dps_trigger(card_type, dut_id, index)
        self.hbidps.send_rc_trigger(trigger)
        return perf_counter()

    def clear_tq_notify(self, log=False):
        alarm = self.hbidps.read_bar_register(self.regs.GLOBAL_ALARMS)
        if log:
            self.Log('info', f'lc_tq_notify: {alarm.rwc_lc_tq_notify}')
            self.Log('info', f'hc_tq_notify: {alarm.rwc_hc_tq_notify}')
            self.Log('info', f'vtarg_tq_notify: {alarm.rwc_vtarg_tq_notify}')
        alarm.rwc_lc_tq_notify = 1
        alarm.rwc_hc_tq_notify = 1
        alarm.rwc_vtarg_tq_notify = 1
        alarm.rwc_invalid_gnd_ctrl_tq_cmd = 0
        alarm.rwc_invalid_tq_cmd_slice_lo = 0
        self.hbidps.write_bar_register(alarm)
        self.check_tq_notify(0, 0, 0, log)

    def check_tq_notify(self, lc_notify, hc_notify, vtarg_notify, log=False):
        alarm = self.hbidps.read_bar_register(self.regs.GLOBAL_ALARMS)
        if log:
            self.Log('info', f'lc_tq_notify: {alarm.rwc_lc_tq_notify}')
            self.Log('info', f'hc_tq_notify: {alarm.rwc_hc_tq_notify}')
            self.Log('info', f'vtarg_tq_notify: {alarm.rwc_vtarg_tq_notify}')
        if lc_notify != alarm.rwc_lc_tq_notify:
            self.Log('error', f'lc_tq_notify mismatch set: {lc_notify} '
                              f'get: {alarm.rwc_lc_tq_notify}')
        if hc_notify != alarm.rwc_hc_tq_notify:
            self.Log('error', f'hc_tq_notify mismatch set: {hc_notify} '
                              f'get: {alarm.rwc_hc_tq_notify}')
        if vtarg_notify != alarm.rwc_vtarg_tq_notify:
            self.Log('error', f'vtarget_tq_notify mismatch set: {vtarg_notify} '
                              f'get: {alarm.rwc_vtarg_tq_notify}')

    def generate_to_dps_trigger(self, card_type, dut_id, lut_index):
        dps_trigger = self.regs.LAST_TRIGGER_SEEN()
        dps_trigger.lut_dword_index = lut_index
        dps_trigger.dps_ctrl_trig = 0
        dps_trigger.sw_type_trigger = 0
        dps_trigger.dut_domain_id = dut_id
        dps_trigger.card_type = card_type
        return dps_trigger.value

    def wait_for_notify(self, lc_notify, hc_notify, vtarg_notify, time_start=None, log=False):
        if time_start==None:
            time_start = perf_counter()
        try_times = 1000
        for _ in range(try_times):
            global_alarm = self.hbidps.read_bar_register(self.regs.GLOBAL_ALARMS)
            if ((not lc_notify) | (global_alarm.rwc_lc_tq_notify & (lc_notify))) and \
               ((not hc_notify) | (global_alarm.rwc_hc_tq_notify & (hc_notify))) and \
               ((not vtarg_notify) | (global_alarm.rwc_vtarg_tq_notify & (vtarg_notify))):
                time_end = perf_counter()
                if log:
                    self.Log('info', f'tq notify raised in {time_end-time_start}s')
                    self.Log('info', f'******************trigger_done******************')
                break
            sleep(0.002)
        else:
            time_end = perf_counter()
            self.Log('error', f'tq notify not detected in {time_end-time_start}s')
        return (time_end-time_start)

class TriggerQueuePCIE(Object):

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.regs = hbidps_register

    def tq_list_inject(self, tq_list, log=False):
        list_len = len(tq_list)
        for i in range(list_len):
            command_low = tq_list[i] & 0xFFFF_FFFF
            command_high = tq_list[i] >> 32
            cmd_direct_inject_low = self.regs.CMD_DIRECT_INJECT_LO
            cmd_direct_inject_low.value = command_low
            cmd_direct_inject_high = self.regs.CMD_DIRECT_INJECT_HI
            cmd_direct_inject_high.value = command_high
            self.hbidps.write_bar_register(cmd_direct_inject_high)
            self.hbidps.write_bar_register(cmd_direct_inject_low)
            if log:
                self.Log('info', f'command: {i}')
                self.Log('info', f'high: {hex(cmd_direct_inject_high.value)}')
                self.Log('info', f'low: {hex(cmd_direct_inject_low.value)}')
