# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class RailTransitionControl(Object):
    """Helper Class for HBI DPS Rail Delay Time Setting

        Methods:
        init_rail_transition_time() Set all rail operation delay (I2L value)
        log_rail_transition_time() Log all rail operation delay
        individual delay time register settings
    """

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.slot = hbidps.slot
        self.regs = hbidps_register

    def init_rail_transition_time(self, log=False):
        if log:
            self.Log('info', 'before the change')
            self.log_rail_transition_time()
        self.update_lc_channel_pwr_off_delay()
        self.update_hc_channel_pwr_off_delay()
        self.update_cfb_relay_connect_delay_reg()
        self.update_cfb_relay_disconnect_delay_reg()
        self.update_pmbus_commit_delay_reg()
        self.update_pmbus_command_delay_reg()
        self.update_channel_off_delay_reg()
        self.update_flush_command_delay_reg()
        if log:
            self.Log('info', 'after the change')
            self.log_rail_transition_time()

    def log_rail_transition_time(self):
        reg = self.hbidps.read_bar_register(self.regs.LC_CHANNEL_POWER_OFF_DELAY)
        self.Log('info', f'LC_PWR_OFF_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.HC_CHANNEL_POWER_OFF_DELAY)
        self.Log('info', f'HC_PWR_OFF_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.RELAY_CONN_DELAY)
        self.Log('info', f'RELAY_CONN_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.RELAY_DIS_DELAY)
        self.Log('info', f'RELAY_DIS_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.PMBUS_COM_DELAY)
        self.Log('info', f'PMBUS_COM_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.PMBUS_CMD_DELAY)
        self.Log('info', f'PMBUS_CMD_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.CHAN_OFF_DELAY)
        self.Log('info', f'CHAN_OFF_DELAY: {reg.value} ms')
        reg = self.hbidps.read_bar_register(self.regs.FLUSH_COMMAND_DELAY)
        self.Log('info', f'FLUSH_COMMAND_DELAY: {reg.value} ms')

    def update_lc_channel_pwr_off_delay(self, value_us=100000):
        lc_channel_pwr_off_delay_reg = self.regs.LC_CHANNEL_POWER_OFF_DELAY
        lc_channel_pwr_off_delay_reg.value = value_us
        self.hbidps.write_bar_register(lc_channel_pwr_off_delay_reg)

    def update_hc_channel_pwr_off_delay(self, value_us=160000):
        hc_channel_pwr_off_delay_reg = self.regs.HC_CHANNEL_POWER_OFF_DELAY
        hc_channel_pwr_off_delay_reg.value = value_us
        self.hbidps.write_bar_register(hc_channel_pwr_off_delay_reg)

    def update_cfb_relay_connect_delay_reg(self, value_us=1500):
        cfb_relay_connect_delay_reg = self.regs.RELAY_CONN_DELAY
        cfb_relay_connect_delay_reg.value = value_us
        self.hbidps.write_bar_register(cfb_relay_connect_delay_reg)

    def update_cfb_relay_disconnect_delay_reg(self, value_us=3):
        cfb_relay_disconnect_delay_reg = self.regs.RELAY_DIS_DELAY
        cfb_relay_disconnect_delay_reg.value = value_us
        self.hbidps.write_bar_register(cfb_relay_disconnect_delay_reg)

    def update_pmbus_commit_delay_reg(self, value_us=6000):
        pmbus_commit_delay_reg = self.regs.PMBUS_COM_DELAY
        pmbus_commit_delay_reg.value = value_us
        self.hbidps.write_bar_register(pmbus_commit_delay_reg)

    def update_pmbus_command_delay_reg(self, value_us=6000):
        pmbus_command_delay_reg = self.regs.PMBUS_CMD_DELAY
        pmbus_command_delay_reg.value = value_us
        self.hbidps.write_bar_register(pmbus_command_delay_reg)

    def update_channel_off_delay_reg(self, value_us=1000):
        channel_off_delay_reg = self.regs.CHAN_OFF_DELAY
        channel_off_delay_reg.value = value_us
        self.hbidps.write_bar_register(channel_off_delay_reg)

    def update_flush_command_delay_reg(self, value_us=0):
        flush_command_delay_reg = self.regs.FLUSH_COMMAND_DELAY
        flush_command_delay_reg.value = value_us
        self.hbidps.write_bar_register(flush_command_delay_reg)
