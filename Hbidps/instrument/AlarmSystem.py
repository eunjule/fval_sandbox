################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing alarm status and reset"""

import time
import struct

from Hbidps.instrument import hbidps_register
from Common.fval import Object

class AlarmHelper(Object):

    def __init__(self, hbidps):
        self.hbidps = hbidps

    def update_global_alarm_mask(self, temp_mask, lc_mask, hc_mask):
        reg = self.read_global_alarm_reg()
        reg.rwc_lc_tq_notify = 0
        reg.rwc_hc_tq_notify = 0
        reg.rwc_vtarg_tq_notify = 0
        reg.rwc_invalid_tq_cmd = 0
        reg.rwc_invalid_gnd_ctrl_tq_cmd = 0
        reg.rw_temp_alarms_mask = temp_mask
        reg.rw_lc_alarms_mask = lc_mask
        reg.rw_hc_alarms_mask = hc_mask
        self.hbidps.write_bar_register(reg)

    def initialize_alarm_settings(self, filter_time=10):
        """ 1. Initialize the TLI4970_SAMPLE_FILTER_TIME register with some non zero value.
               This will ensure that the SYS_CURRENT_ALARM goes high when the current exceeds the threshold values.
            2. Set the TEMP and CURRENT threshold values to default.
        """
        self.set_sample_filter_time(filter_time)
        self.set_default_tli4970_current_threshold()
        self.set_default_max6628_temp_threshold()

    def set_default_max6628_temp_threshold(self):
        for max6628_device in range(4):
            self.write_max6628_lower_limit(max6628_device, 0x00)
            self.write_max6628_upper_limit(max6628_device, 0x0fff)

    def set_default_tli4970_current_threshold(self):
        under_current_default = self.encode_float_to_single_precision_floating_format(-10)
        self.write_tli4970_lower_limit(under_current_default)
        over_current_default = self.encode_float_to_single_precision_floating_format(40)
        self.write_tli4970_upper_limit(over_current_default)

    def reset_initialize_alarm_settings(self):
        """ Reset all the values initialized by the InitializeAlarmSettings helper function.
        """
        filter_time_default = 0
        self.set_sample_filter_time(filter_time_default)

    def alarm_register_status_one_dps(self, hbidps):
        hbidps.print_slot_under_test()
        self.Log('info', f'TLI4970 SAMPLE FILTER TIME : {self.read_tli4970_sample_filter_time()}')
        self.Log('info', f'TLI4970 MAX LIMIT : {self.read_convert_tli4970_max_current()}')
        self.Log('info', f'TLI4970 MIN LIMIT : {self.read_convert_tli4970_min_current()}')
        for max6628_device in range(4):
            max_limit = self.read_max6628_upper_limit(max6628_device)
            max_limit = self.convert_max6628_temp_to_degrees(max_limit)
            self.Log('info', f'MAX6628_D{max6628_device} MAX LIMIT : {max_limit}')
            min_limit = self.read_max6628_lower_limit(max6628_device)
            min_limit = self.convert_max6628_temp_to_degrees(min_limit)
            self.Log('info', f'MAX6628_D{max6628_device} MIN LIMIT : {min_limit}')

    def convert_max6628_temp_to_degrees(self, raw_temp):
        if self.is_signed(raw_temp):
            raw_temp = self.get_twos_compliment(raw_temp, 13)
        raw_temp *= 0.0625
        return raw_temp

    def detect_global_alarm_status_one_dps(self):
        """ 1. Read the Global Alarm Register for a specific HBIDPS.
            2. Check for the alarms registers.
            3. If there is an alarm, check for more alarm details by reading the lower level registers.
        """
        self.global_alarm = self.read_global_alarm_reg()
        self.Log('info', f'GLOBAL ALARM REG : {hex(self.global_alarm.value)}')
        self.check_rail_alarm()
        self.check_rail_busy_alarm()
        self.check_sample_engine_alarm()
        self.check_current_alarm()
        self.check_temp_alarm()

    def check_rail_busy_alarm(self):
        self.check_lc_rail_busy_alarm()
        self.check_hc_rail_busy_alarm()
        self.check_vtarget_rail_busy_alarm()

    def check_lc_rail_busy_alarm(self):
        if self.global_alarm.r_lc_busy == 1:
            self.Log('info', f'There is an LC Rail Busy Alarm Set.')
            lc_busy_alarm = self.read_lc_busy_alarm()
            if lc_busy_alarm.rwc_alarm != 0:
                self.Log('info', f'LC BUSY ALARM REG : {hex(lc_busy_alarm.value)} ADDR : {hex(lc_busy_alarm.ADDR)}')
                self.Log('info', f'One of the LC rail was not available.')

    def check_hc_rail_busy_alarm(self):
        if self.global_alarm.r_hc_busy == 1:
            self.Log('info', f'There is an HC Rail Busy Alarm Set.')
            hc_busy_alarm = self.read_hc_rail_busy_alarm()
            if hc_busy_alarm.rwc_alarm != 0:
                self.Log('info', f'HC BUSY ALARM REG : {hex(hc_busy_alarm.value)} ADDR : {hex(hc_busy_alarm.ADDR)}')
                self.Log('info', f'One of the HC rail was not available.')

    def check_vtarget_rail_busy_alarm(self):
        if self.global_alarm.r_vtarg_busy == 1:
            self.Log('info', f'There is a Vtarget Rail Busy Alarm Set.')
            vtarget_busy_alarm = self.read_vtarget_busy_alarm()
            if vtarget_busy_alarm.rwc_alarm != 0:
                self.Log('info', f'VTARG BUSY ALARM REG : {hex(vtarget_busy_alarm.value)} ADDR : {hex(vtarget_busy_alarm.ADDR)}')
                self.Log('info', f'One of the VTarget rail was not available.')

    def check_sample_engine_alarm(self):
        self.check_lc_sample_engine_alarm()
        self.check_hc_sample_engine_alarm()
        self.check_vmeasure_sample_engine_alarm()

    def check_lc_sample_engine_alarm(self):
        if self.global_alarm.r_lc_sample == 1:
            self.Log('info', f'There is an LC Sample Engine Alarm Set.')
            lc_sample_engine_alarm = self.read_lc_sample_engine_alarm()
            self.Log('info', f'LC SAMPLE ALARMS REG : {hex(lc_sample_engine_alarm.value)} ADDR : {hex(lc_sample_engine_alarm.ADDR)}')
            if lc_sample_engine_alarm.rwc_start_error != 0:
                self.Log('info', f'A LC sample engine start occurred while the sampling engine was already running.')
            if lc_sample_engine_alarm.rwc_oob_error != 0:
                self.Log('info', f'A LC header or sample was to be written outside of the alloted memory area.')
            if lc_sample_engine_alarm.rwc_samples_lost != 0:
                self.Log('info', f'LC Samples arrived faster than they could be written to DDR memory.')

    def check_hc_sample_engine_alarm(self):
        if self.global_alarm.r_hc_sample == 1:
            self.Log('info', f'There is an HC Sample Engine Alarm Set.')
            hc_sample_engine_alarm = self.read_hc_sample_engine_alarm()
            self.Log('info', f'HC SAMPLE ALARMS REG : {hex(hc_sample_engine_alarm.value)} ADDR : {hex(hc_sample_engine_alarm.ADDR)}')
            if hc_sample_engine_alarm.rwc_start_error != 0:
                self.Log('info', f'A HC sample engine start occurred while the sampling engine was already running.')
            if hc_sample_engine_alarm.rwc_oob_error != 0:
                self.Log('info', f'A HC header or sample was to be written outside of the alloted memory area.')
            if hc_sample_engine_alarm.rwc_samples_lost != 0:
                self.Log('info', f'HC Samples arrived faster than they could be written to DDR memory.')

    def check_vmeasure_sample_engine_alarm(self):
        if self.global_alarm.r_vmeas_sample == 1:
            self.Log('info', f'There is a VMeasure Sample Engine Alarm Set.')
            vmeas_sample_engine_alarm = self.read_vmeas_sample_engine_alarm()
            self.Log('info', f'VMEAS SAMPLE ALARMS REG : {hex(vmeas_sample_engine_alarm.value)} ADDR : {hex(vmeas_sample_engine_alarm.ADDR)}')
            if vmeas_sample_engine_alarm.rwc_start_error != 0:
                self.Log('info', f'A VMeasure sample engine start occurred while the sampling engine was already running.')
            if vmeas_sample_engine_alarm.rwc_oob_error != 0:
                self.Log('info', f'A VMeasure header or sample was to be written outside of the alloted memory area.')
            if vmeas_sample_engine_alarm.rwc_samples_lost != 0:
                self.Log('info', f'VMeasure Samples arrived faster than they could be written to DDR memory.')

    def check_temp_alarm(self):
        if self.global_alarm.r_temp == 1:
            self.Log('info', f'There is a Temperature Alarm Set.')
            temp_alarm = self.read_temp_alarm()
            self.Log('info', f'TEMP ALARM REG : {hex(temp_alarm.value)} ADDR : {hex(temp_alarm.ADDR)}')
            if temp_alarm.low_temp != 0:
                self.Log('info', f'MAX6628 temperature below threshold.')
            if temp_alarm.high_temp != 0:
                self.Log('info', f'MAX6628 temperature above threshold.')

    def check_current_alarm(self):
        if self.global_alarm.r_sys_current == 1:
            self.Log('info', f'There is a Current Alarm Set.')
            sys_current = self.read_sys_current_alarm()
            self.Log('info', f'SYS CURRENT ALARM REG : {hex(sys_current.value)} ADDR : {hex(sys_current.ADDR)}')
            if sys_current.hardware_error == 1:
                self.Log('info', f'TLI4970 Current hardware error was signaled.')
            if sys_current.temp_error == 1:
                self.Log('info', f'TLI4970 Current temperature error was signaled.')
            if sys_current.common_error == 1:
                self.Log('info', f'TLI4970 Current comm error was signaled.')
            if sys_current.parity_error == 1:
                self.Log('info', f'TLI4970 Current parity error occurred.')
            if sys_current.pos_limit == 1:
                self.Log('info', f'TLI4970 Current Exceeded the upper threshold.')
            if sys_current.neg_limit == 1:
                self.Log('info', f'TLI4970 Current Exceeded the lower threshold.')

    def check_rail_alarm(self):
        self.check_hc_rail_alarm()
        self.check_lc_rail_alarm()

    def check_hc_rail_alarm(self):
        if self.global_alarm.r_hc == 1:
            self.Log('info', f'There is an HC Alarm Set.')
            self.check_hc_alarm()

    def is_hc_status_or_alert(self):
        hc_alarm = self.read_hc_rail_alarm()
        if hc_alarm.rwc_alert != 0 or hc_alarm.rwc_status != 0:
            return True
        else:
            return False

    def check_hc_alarm(self):
        hc_alarm = self.read_hc_rail_alarm()
        self.Log('info', f'HC ALARM REG : {hex(hc_alarm.value)} ADDR : {hex(hc_alarm.ADDR)}')
        if hc_alarm.r_hc_cfold == 1:
            self.Log('info', f'There is a HC CFold Alarm Set.')
        if hc_alarm.r_hc_temp == 1:
            self.Log('info', f'There is a HC Temperature Alarm Set.')
            self.check_hc_rail_temp_alarm()
        if hc_alarm.r_irange_check_fail == 1:
            self.Log('info', f'There is one or more irange check fail alarms are set.')
        if hc_alarm.r_pwr_off_delay == 1:
            self.Log('info', f'There is one or more power off delay alarms are set.')
        if hc_alarm.r_vrange == 1:
            self.Log('info', f'There is One or more VRange alarms are set.')
        if hc_alarm.rwc_alert != 0:
            self.get_status_for_alert_modules(hc_alarm.rwc_alert)
            self.Log('info', f'PMBus hardware alert line was asserted.')
        if hc_alarm.rwc_status != 0:
            self.Log('info', f'One or more unmasked PMBus status register bits are set.')
        if hc_alarm.rwc_comm != 0:
            self.Log('info', f'PMBus retry threshold exceeded.')

    def get_status_for_alert_modules(self, alert):
        alert_module_list = self.get_alert_module_list(alert)
        for module in alert_module_list:
            self.Log('warning', f'hc module{module} status')
            self.hbidps.log_hc_rails_status(module, 0)
            self.hbidps.log_hc_rails_status(module, 1)

    def get_alert_module_list(self, alert):
        alert_list = []
        for i in range(5):
            if (alert >> i) & 1:
                alert_list.append(i)
        return alert_list

    def check_hc_rail_temp_alarm(self):
        hc_temp_alarm = self.read_hc_rail_temp_alarm()
        self.Log('info', f'HC TEMP ALARM REG : {hex(hc_temp_alarm.value)} ADDR : {hex(hc_temp_alarm.ADDR)}')
        if hc_temp_alarm.rwc_hc_temp_lo != 0:
            self.Log('info', f'There is a HC Temperature Alarm Exceeded the Lower Limit.')
        if hc_temp_alarm.rwc_hc_temp_hi != 0:
            self.Log('info', f'There is a HC Temperature Alarm Exceeded the Upper Limit.')

    def check_lc_rail_alarm(self):
        if self.global_alarm.r_lc == 1:
            self.Log('info', f'There is a LC Alarm Set.')
            self.check_lc_alarm()

    def is_lc_status_or_alert(self):
        lc_alarm = self.read_lc_rail_alarm()
        if lc_alarm.rwc_alert != 1 or lc_alarm.rwc_status != 0:
            return True
        else:
            return False

    def check_lc_alarm(self):
        lc_alarm = self.read_lc_rail_alarm()
        self.Log('info', f'LC ALARM REG : {hex(lc_alarm.value)} ADDR : {hex(lc_alarm.ADDR)}')
        if lc_alarm.r_lc_cfold == 1:
            self.Log('info', f'There is a LC CFold Alarm Set.')
        if lc_alarm.r_lc_temp == 1:
            self.Log('info', f'There is a LC Temperature Alarm Set.')
            self.check_lc_rail_temp_alarm()
        if lc_alarm.r_irange_check_fail == 1:
            self.Log('info', f'There is one or more irange check fail alarms are set.')
        if lc_alarm.r_pwr_off_delay == 1:
            self.Log('info', f'There is one or more power off delay alarms are set.')
        if lc_alarm.rwc_alert != 0:
            self.Log('info', f'PMBus hardware alert line was asserted.')
        if lc_alarm.rwc_status != 0:
            self.Log('info', f'One or more unmasked PMBus status register bits are set.')
        if lc_alarm.rwc_comm != 0:
            self.Log('info', f'PMBus retry threshold exceeded.')

    def check_lc_rail_temp_alarm(self):
        lc_temp_alarm = self.read_lc_rail_temp_alarm()
        self.Log('info', f'LC TEMP ALARM REG : {hex(lc_temp_alarm.value)} ADDR : {hex(lc_temp_alarm.ADDR)}')
        if lc_temp_alarm.rwc_lc_temp_lo != 0:
            self.Log('info', f'There is a LC Temperature Alarm Exceeded the Lower Limit.')
        if lc_temp_alarm.rwc_lc_temp_hi != 0:
            self.Log('info', f'There is a LC Temperature Alarm Exceeded the Upper Limit.')

    def reset_one_hbi_dps_global_alarm_reg(self):
        """ 1. Read Global Alarm Register for a specific HBIDPS.
            2. Clear all the lower lever registers to reset the bits of the global alarm register.
        """
        self.global_alarm = self.read_global_alarm_reg()
        self.Log('info', f'GLOBAL ALARM REG BEFORE RESET: {hex(self.global_alarm.value)}')
        self.reset_rail_busy_alarm()
        self.reset_sample_engine_alarm()
        self.reset_temp_alarm()
        self.reset_current_alarm()
        self.reset_rail_alarm()
        self.reset_invalid_tq_cmd()
        self.reset_invalid_gnd_ctrl_tq_cmd()
        self.Log('info', f'GLOBAL ALARM REG AFTER RESET: {hex(self.read_global_alarm_reg().value)}')

    def reset_rail_busy_alarm(self):
        self.reset_lc_rail_busy_alarm()
        self.reset_hc_rail_busy_alarm()
        self.reset_vtarget_rail_busy_alarm()

    def reset_lc_rail_busy_alarm(self):
        lc_busy_alarm = self.read_lc_busy_alarm()
        lc_busy_alarm.rwc_alarm = 0b1111111111111111
        self.hbidps.write_bar_register(lc_busy_alarm)

    def reset_hc_rail_busy_alarm(self):
        hc_busy_alarm = self.read_hc_rail_busy_alarm()
        hc_busy_alarm.rwc_alarm = 0b1111111111
        self.hbidps.write_bar_register(hc_busy_alarm)

    def reset_vtarget_rail_busy_alarm(self):
        vtarget_busy_alarm = self.read_vtarget_busy_alarm()
        vtarget_busy_alarm.rwc_alarm = 0b1111111111111111
        self.hbidps.write_bar_register(vtarget_busy_alarm)

    def reset_sample_engine_alarm(self):
        self.reset_lc_sample_engine_alarm()
        self.reset_hc_sample_engine_alarm()
        self.reset_vmeasure_sample_engine_alarm()

    def reset_lc_sample_engine_alarm(self):
        lc_sample_engine_alarm = self.read_lc_sample_engine_alarm()
        lc_sample_engine_alarm.rwc_start_error = 0b1111
        lc_sample_engine_alarm.rwc_oob_error = 0b1111
        lc_sample_engine_alarm.rwc_samples_lost = 0b1111
        self.hbidps.write_bar_register(lc_sample_engine_alarm)

    def reset_hc_sample_engine_alarm(self):
        hc_sample_engine_alarm = self.read_hc_sample_engine_alarm()
        hc_sample_engine_alarm.rwc_start_error = 0b1111
        hc_sample_engine_alarm.rwc_oob_error = 0b1111
        hc_sample_engine_alarm.rwc_samples_lost = 0b1111
        self.hbidps.write_bar_register(hc_sample_engine_alarm)

    def reset_vmeasure_sample_engine_alarm(self):
        vmeas_sample_engine_alarm = self.read_vmeas_sample_engine_alarm()
        vmeas_sample_engine_alarm.rwc_start_error = 0b11111
        vmeas_sample_engine_alarm.rwc_oob_error = 0b11111
        vmeas_sample_engine_alarm.rwc_samples_lost = 0b11111
        self.hbidps.write_bar_register(vmeas_sample_engine_alarm)

    def reset_temp_alarm(self):
        temp_alarm = self.read_temp_alarm()
        temp_alarm.low_temp = 0b1111
        temp_alarm.high_temp = 0b1111
        self.hbidps.write_bar_register(temp_alarm)

    def reset_current_alarm(self):
        sys_current = self.read_sys_current_alarm()
        sys_current.hardware_error = 1
        sys_current.temp_error = 1
        sys_current.common_error = 1
        sys_current.parity_error = 1
        sys_current.neg_limit = 1
        sys_current.pos_limit = 1
        self.hbidps.write_bar_register(sys_current)

    def reset_rail_alarm(self):
        self.reset_hc_rail_alarm()
        self.reset_lc_rail_alarm()

    def reset_hc_rail_alarm(self):
        if self.is_hc_status_or_alert():
            self.hbidps.reinit_hc_rails()
        self.reset_hc_cfold_alarm()
        self.reset_hc_temp_alarm()
        self.reset_hc_pwr_off_delay_alarm()
        self.reset_hc_irange_check_fail_alarm()
        self.reset_hc_vrange_alarm()
        self.reset_hc_alert()
        self.reset_hc_status()
        self.reset_hc_comm()

    def reset_hc_temp_alarm(self):
        hc_temp_alarm = self.read_hc_rail_temp_alarm()
        hc_temp_alarm.rwc_hc_temp_lo = 0b1111111111
        hc_temp_alarm.rwc_hc_temp_hi = 0b1111111111
        self.hbidps.write_bar_register(hc_temp_alarm)

    def reset_hc_cfold_alarm(self):
        hc_cfold_alarm = self.read_hc_cfold_alarm()
        hc_cfold_alarm.rwc_hc_cfold = 0b1111111111
        self.hbidps.write_bar_register(hc_cfold_alarm)

    def reset_hc_pwr_off_delay_alarm(self):
        hc_pwr_off_delay_alarm = self.read_hc_pwr_off_delay_alarm()
        hc_pwr_off_delay_alarm.rwc_hc_pwr_off_delay_check_fail = 0b1111111111
        self.hbidps.write_bar_register(hc_pwr_off_delay_alarm)

    def reset_hc_irange_check_fail_alarm(self):
        hc_irange_check_fail_alarm = self.read_hc_irange_check_fail_alarm()
        hc_irange_check_fail_alarm.rwc_hc_irange_check_fail = 0b1111111111
        self.hbidps.write_bar_register(hc_irange_check_fail_alarm)

    def reset_hc_vrange_alarm(self):
        hc_vrange_alarm = self.read_hc_vrange_alarm()
        hc_vrange_alarm.rwc_vrange_self = 0b1111111111
        hc_vrange_alarm.rwc_vrange_neighbor = 0b1111111111
        hc_vrange_alarm.rwc_vrange_check_fail = 0b1111111111
        self.hbidps.write_bar_register(hc_vrange_alarm)
        sync_vrange_alarm = self.read_sync_vrange_alarm()
        sync_vrange_alarm.rwc_sync_check_fail_alarm = 0b11_1111_1111
        self.hbidps.write_bar_register(sync_vrange_alarm)

    def reset_hc_sync_vrange_alarm(self):
        sync_vrange_alarm = self.read_sync_vrange_alarm()
        sync_vrange_alarm.rwc_sync_check_fail_alarm = 0b11_1111_1111
        self.hbidps.write_bar_register(sync_vrange_alarm)

    def reset_hc_alert(self):
        hc_alarm = self.read_hc_rail_alarm()
        hc_alarm.rwc_alert = 0b11111
        self.hbidps.write_bar_register(hc_alarm)

    def reset_hc_status(self):
        hc_alarm = self.read_hc_rail_alarm()
        hc_alarm.rwc_status = 0b1111111111
        self.hbidps.write_bar_register(hc_alarm)

    def reset_hc_comm(self):
        hc_alarm = self.read_hc_rail_alarm()
        hc_alarm.rwc_comm = 0b11111
        self.hbidps.write_bar_register(hc_alarm)

    def reset_lc_rail_alarm(self):
        if self.is_lc_status_or_alert():
            self.hbidps.reinit_lc_rails()
        self.reset_lc_cfold_alarm()
        self.reset_lc_temp_alarm()
        self.reset_lc_pwr_off_delay_alarm()
        self.reset_lc_irange_check_fail_alarm()
        self.reset_lc_alert()
        self.reset_lc_status()
        self.reset_lc_comm()

    def reset_lc_temp_alarm(self):
        lc_temp_alarm = self.read_lc_rail_temp_alarm()
        lc_temp_alarm.rwc_lc_temp_lo = 0b1111
        lc_temp_alarm.rwc_lc_temp_hi = 0b1111
        self.hbidps.write_bar_register(lc_temp_alarm)

    def reset_lc_cfold_alarm(self):
        lc_cfold_alarm = self.read_lc_cfold_alarm()
        lc_cfold_alarm.rwc_lc_cfold = 0b1111111111111111
        self.hbidps.write_bar_register(lc_cfold_alarm)

    def reset_lc_pwr_off_delay_alarm(self):
        pwr_off_delay_alarm = self.read_lc_pwr_off_delay_alarm()
        pwr_off_delay_alarm.rwc_lc_pwr_off_delay_check_fail = 0b1111111111111111
        self.hbidps.write_bar_register(pwr_off_delay_alarm)

    def reset_lc_irange_check_fail_alarm(self):
        irange_check_fail_alarm = self.read_lc_irange_check_fail_alarm()
        irange_check_fail_alarm.rwc_lc_irange_check_check_fail = 0b1111111111111111
        self.hbidps.write_bar_register(irange_check_fail_alarm)

    def reset_lc_alert(self):
        lc_alarm = self.read_lc_rail_alarm()
        lc_alarm.rwc_alert = 0b1111
        self.hbidps.write_bar_register(lc_alarm)

    def reset_lc_status(self):
        lc_alarm = self.read_lc_rail_alarm()
        lc_alarm.rwc_status = 0b1111111111111111
        self.hbidps.write_bar_register(lc_alarm)

    def reset_lc_comm(self):
        lc_alarm = self.read_lc_rail_alarm()
        lc_alarm.rwc_comm = 0b1111
        self.hbidps.write_bar_register(lc_alarm)

    def reset_invalid_tq_cmd(self):
        self.global_alarm.rwc_invalid_tq_cmd = 0b1111
        self.hbidps.write_bar_register(self.global_alarm)

    def reset_invalid_gnd_ctrl_tq_cmd(self):
        self.global_alarm.rwc_invalid_gnd_ctrl_tq_cmd = 0b1
        self.hbidps.write_bar_register(self.global_alarm)

    def read_convert_tli4970_min_current(self):
        min = self.hbidps.read_bar_register(hbidps_register.TLI4970_UNDERCURRENT_LIMIT).value
        min_current = self.decode_single_precision_floating_point_current(min)
        return min_current

    def read_convert_tli4970_max_current(self):
        max = self.hbidps.read_bar_register(hbidps_register.TLI4970_OVERCURRENT_LIMIT).value
        max_current = self.decode_single_precision_floating_point_current(max)
        return max_current

    @staticmethod
    def decode_single_precision_floating_point_current(raw_current):
        current_decimal = struct.unpack('!f', struct.pack('!I', int(bin(raw_current), 2)))[0]
        return current_decimal

    @staticmethod
    def encode_float_to_single_precision_floating_format(float_value):
        binary_value = ''.join(f'{c:0>8b}' for c in struct.pack('!f', float_value))
        return int(binary_value, 2)

    def set_sample_filter_time(self, value):
        self.hbidps.write_bar_register(hbidps_register.TLI4970_SAMPLE_FILTER_TIME(value=value))

    def read_TLI4970_current(self):
        return self.hbidps.read_bar_register(hbidps_register.TLI4970_COMMUNICATIONS_REG).value

    def read_lc_sample_engine_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_SAMPLE_ALARMS)

    def reset_lc_sample_engine_busy_alarm(self):
        reg = self.hbidps.read_bar_register(hbidps_register.LC_SAMPLE_ALARMS)
        reg.value = reg.rwc_start_error
        self.hbidps.write_bar_register(reg)

    def read_global_alarm_reg(self):
        return self.hbidps.read_bar_register(hbidps_register.GLOBAL_ALARMS)

    def read_global_alarm_reg_sample_engines(self):
        reg = self.hbidps.read_bar_register(hbidps_register.GLOBAL_ALARMS)
        return reg.r_hc_sample, reg.r_lc_sample, reg.r_vmeas_sample

    def read_hc_sample_engine_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_SAMPLE_ALARMS)

    def reset_hc_sample_engine_busy_alarm(self):
        """start_error means sample engine is still busy when restart"""
        reg = self.hbidps.read_bar_register(hbidps_register.HC_SAMPLE_ALARMS)
        reg.value = reg.rwc_start_error
        self.hbidps.write_bar_register(reg)

    def read_vmeas_sample_engine_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.VMEAS_SAMPLE_ALARMS)

    def reset_vmeas_sample_engine_busy_alarm(self):
        reg = self.hbidps.read_bar_register(hbidps_register.VMEAS_SAMPLE_ALARMS)
        reg.value = reg.rwc_start_error
        self.hbidps.write_bar_register(reg)

    def read_temp_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.TEMP_ALARMS)

    def read_vtarget_busy_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.V_TARGET_BUSY_ALARMS)

    def read_lc_rail_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_ALARMS)

    def read_lc_rail_temp_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_TEMP_ALARMS)

    def read_lc_busy_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_BUSY_ALARMS)

    def read_hc_rail_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_ALARMS)

    def read_hc_rail_temp_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_TEMP_ALARMS)

    def read_hc_cfold_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_CFOLD_ALARMS)

    def read_lc_cfold_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_CFOLD_ALARMS)

    def read_hc_rail_busy_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_BUSY_ALARMS)

    def read_sys_current_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.SYS_CURRENT_ALARMS)

    def read_hc_pwr_off_delay_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_POWER_OFF_DELAY_ALARMS)

    def read_hc_irange_check_fail_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_IRANGE_CHECK_FAIL_ALARMS)

    def read_hc_vrange_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_VRANGE_ALARMS)

    def read_sync_vrange_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.HC_VRANGE_SYNC_CHECK_FAIL_ALARM)

    def read_lc_irange_check_fail_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_IRANGE_CHECK_FAIL_ALARMS)

    def read_lc_pwr_off_delay_alarm(self):
        return self.hbidps.read_bar_register(hbidps_register.LC_POWER_OFF_DELAY_ALARMS)

    def read_tli4970_sample_filter_time(self):
        return self.hbidps.read_bar_register(hbidps_register.TLI4970_SAMPLE_FILTER_TIME).value

    def read_max6628_upper_limit(self, max6628_device):
        return self.hbidps.read_bar_register(hbidps_register.MAX6627_UPPER_TEMP_LIMIT, index=max6628_device).value

    def read_max6628_lower_limit(self, max6628_device):
        return self.hbidps.read_bar_register(hbidps_register.MAX6627_LOWER_TEMP_LIMIT, index=max6628_device).value

    def write_max6628_lower_limit(self, _index, val):
        self.hbidps.write_bar_register(hbidps_register.MAX6627_LOWER_TEMP_LIMIT(value=val), index=_index)

    def write_max6628_upper_limit(self, _index, val):
        self.hbidps.write_bar_register(hbidps_register.MAX6627_UPPER_TEMP_LIMIT(value=val), index=_index)

    def write_tli4970_upper_limit(self, val):
        self.hbidps.write_bar_register(hbidps_register.TLI4970_OVERCURRENT_LIMIT(value=val))

    def write_tli4970_lower_limit(self, val):
        self.hbidps.write_bar_register(hbidps_register.TLI4970_UNDERCURRENT_LIMIT(value=val))

    def read_max6628_temp(self, max6628_device):
        return self.hbidps.read_bar_register(hbidps_register.MAX6627_TEMP, index=max6628_device).value

    def read_die_temp(self):
        return self.hbidps.read_bar_register(hbidps_register.DIE_TEMP).value

