################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import ctypes
from ctypes import c_uint

if __name__ == '__main__':
    import os
    import sys

    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))

import Common.register as register

module_dict = globals()

def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbidpsRegister):
                if attribute is not HbidpsRegister:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbidpsRegister):
                if attribute is not HbidpsRegister:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbidpsStruct):
                if attribute is not HbidpsStruct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, HbidpsStruct):
                if attribute is not HbidpsStruct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class HbidpsRegister(register.Register):
    BASEMUL = 4
    BAR = 1



class HbidpsStruct(register.Register):
    ADDR = None


    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class DUMMY_STRUCT(HbidpsStruct):
    _fields_ = [('Data', ctypes.c_uint, 32)]

class BAR1_SCRATCH(HbidpsRegister):
    BAR = 1
    ADDR = 0x0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class FPGA_VERSION(HbidpsRegister):
    BAR = 1
    ADDR = 0x4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class RESETS(HbidpsRegister):
    BAR = 1
    ADDR = 0x8
    _fields_ = [('w_reset_ddr', ctypes.c_uint, 1),
                ('w_reset_aurora', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 30)]

class STATUS(HbidpsRegister):
    BAR = 1
    ADDR = 0xc
    _fields_ = [('r_aur_tx_pll_locked_n', ctypes.c_uint, 1),
                ('r_aur_rx_locked_to_ref_n', ctypes.c_uint, 1),
                ('r_aur_rx_locked_to_data_n', ctypes.c_uint, 1),
                ('r_aur_channel_up_n', ctypes.c_uint, 1),
                ('r_ddr_cal_fail', ctypes.c_uint, 1),
                ('r_ddr_cal_success_n', ctypes.c_uint, 1),
                ('r_pll_5mhz_locked_n', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 25)]

class SLOT_ID(HbidpsRegister):
    BAR = 1
    ADDR = 0x10
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DIE_TEMP(HbidpsRegister):
    BAR = 1
    ADDR = 0x240
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TEMP_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x710
    _fields_ = [('low_temp', ctypes.c_uint, 4),
                ('high_temp', ctypes.c_uint, 4),
                ('Reserved', ctypes.c_uint, 24)]


class EXECUTE_TRIGGER(HbidpsRegister):
    BAR = 1
    ADDR = 0x14
    _fields_ =  [('trigger_busy', ctypes.c_uint, 1),
                ('execute_trigger_address', ctypes.c_uint, 31)]

class EXECUTE_INTERRUPT(HbidpsRegister):
    BAR = 1
    ADDR = 0x18
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SET_SAFE_STATE(HbidpsRegister):
    BAR = 1
    ADDR = 0x1c
    _fields_ = [('w_set_safe_state', ctypes.c_uint, 1),
                ('rsvd', ctypes.c_uint, 31)]


class US_COUNTER_VALUE(HbidpsRegister):
    BAR = 1
    ADDR = 0x020
    _fields_ = [('r_data', ctypes.c_uint, 32)]


class MEM_WR_DATA_TAG(HbidpsRegister):
    BAR = 1
    ADDR = 0x24
    _fields_ = [('Data', ctypes.c_uint, 32)]

class MEM_RD_DATA_TAG(HbidpsRegister):
    BAR = 1
    ADDR = 0x28
    _fields_ = [('Data', ctypes.c_uint, 32)]

class MEM_ADDRESS_TAG(HbidpsRegister):
    BAR = 1
    ADDR = 0x2c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class CAPABILITIES(HbidpsRegister):
    BAR = 1
    ADDR = 0x3c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_00_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x40
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_00_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x44
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_00_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x48
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_00_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x4c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_01_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x50
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_01_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x54
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_01_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x58
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_01_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x5c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_02_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x60
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_02_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x64
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_02_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x68
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_02_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x6c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_03_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x70
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_03_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x74
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_03_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x78
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_03_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x7c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class ADT7411_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x80
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]

class ADT7411_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x84
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]

class ADT7411_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x88
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class ADT7411_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x8c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class TPS22976N_ST_LC_LD_EN(HbidpsRegister):
    BAR = 1
    ADDR = 0x90
    _fields_ = [('rw_ld_en', ctypes.c_uint, 16),
                ('reserved', ctypes.c_uint, 16)]

class TPS22976N_ST_HC_LD_EN(HbidpsRegister):
    BAR = 1
    ADDR = 0x94
    _fields_ = [('rw_ld_en', ctypes.c_uint, 10),
                ('reserved', ctypes.c_uint, 22)]

class TPS22976N_ST_LD_EN(HbidpsRegister):
    BAR = 1
    ADDR = 0x98
    _fields_ = [('rw_st_2p0_ohm_ld_en', ctypes.c_uint, 1),
                ('rw_st_0p5_ohm_ld_en', ctypes.c_uint, 1),
                ('reserved', ctypes.c_uint, 30)]


class TLI4970_COMMUNICATIONS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x9c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TLI4970_OVERCURRENT_LIMIT(HbidpsRegister):
    BAR = 1
    ADDR = 0x6f0
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HC_CHANNEL_POWER_OFF_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x914
    _fields_ = [('rw_hc_pwr_off_delay', ctypes.c_uint, 32)]


class LC_CHANNEL_POWER_OFF_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x918
    _fields_ = [('rw_lc_pwr_off_delay', ctypes.c_uint, 32)]


class TLI4970_UNDERCURRENT_LIMIT(HbidpsRegister):
    BAR = 1
    ADDR = 0x930
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TLI4970_CURRENT_ALARM_FAILING_VALUE(HbidpsRegister):
    BAR = 1
    ADDR = 0x940
    _fields_ = [('Data', ctypes.c_uint, 32)]


class TLI4970_SAMPLE_FILTER_TIME(HbidpsRegister):
    BAR = 1
    ADDR = 0x93c
    _fields_ = [('Data', ctypes.c_uint, 32)]


class SYS_CURRENT_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x70c
    _fields_ = [('Reserved', ctypes.c_uint, 2),
                ('hardware_error', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 1),
                ('temp_error', ctypes.c_uint, 1),
                ('common_error', ctypes.c_uint, 1),
                ('parity_error', ctypes.c_uint, 1),
                ('neg_limit', ctypes.c_uint, 1),
                ('pos_limit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 9),
                ('mask_hardware', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 1),
                ('mask_temp', ctypes.c_uint, 1),
                ('mask_comm', ctypes.c_uint, 1),
                ('mask_parity', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 9)]


class MAX6627_UPPER_TEMP_LIMIT(HbidpsRegister):
    BAR = 1
    REGCOUNT = 4
    ADDR = 0xa0
    _fields_ = [('Data', ctypes.c_uint, 13),
                ('Reserved', ctypes.c_uint, 19)]


class MAX6627_LOWER_TEMP_LIMIT(HbidpsRegister):
    BAR = 1
    REGCOUNT = 4
    ADDR = 0xb0
    _fields_ = [('Data', ctypes.c_uint, 13),
                ('Reserved', ctypes.c_uint, 19)]


class MAX6627_TEMP(HbidpsRegister):
    BAR = 1
    REGCOUNT = 4
    ADDR = 0xc0
    _fields_ = [('Data', ctypes.c_uint, 13),
                ('Reserved', ctypes.c_uint, 19)]


class ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0xd0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SAMPLE_COLLECTOR_INDEX(HbidpsRegister):
    BAR = 1
    ADDR = 0xd4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SAMPLE_COLLECTOR_COUNT(HbidpsRegister):
    BAR = 1
    ADDR = 0xd8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class SAMPLE_COLLECTOR_DATA(HbidpsRegister):
    BAR = 1
    ADDR = 0xdc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_00_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xe0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_00_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xe4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_00_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xe8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_00_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xec
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_01_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xf0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_01_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xf4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_01_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xf8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_01_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0xfc
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_02_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x100
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_02_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x104
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_02_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x108
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_02_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x10c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_03_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x110
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_03_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x114
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_03_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x118
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_03_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x11c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_04_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x120
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_04_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x124
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_04_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x128
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTM4680_04_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x12c
    _fields_ = [('Data', ctypes.c_uint, 32)]

class MAX14662_VT_VM_LB_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x130
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]

class MAX14662_VT_VM_LB_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x134
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]

class MAX14662_VT_VM_LB_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x138
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_VT_VM_LB_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x13c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_ISOURCE_VSENSE_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x140
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]

class MAX14662_ISOURCE_VSENSE_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x144
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]

class MAX14662_ISOURCE_VSENSE_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x148
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_ISOURCE_VSENSE_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x14c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_01_GANG_LTC2975_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x150
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]

class MAX14662_01_GANG_LTC2975_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x154
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]

class MAX14662_01_GANG_LTC2975_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x158
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_01_GANG_LTC2975_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x15c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_23_GANG_LTC2975_I2C_STATUS_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x180
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]

class MAX14662_23_GANG_LTC2975_I2C_CONTROL_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x184
    _fields_ = [('transmit', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]

class MAX14662_23_GANG_LTC2975_I2C_TX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x188
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class MAX14662_23_GANG_LTC2975_I2C_RX_FIFO_DATA_REG(HbidpsRegister):
    BAR = 1
    ADDR = 0x18c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]

class HC_GANG_COMPA_HCR_EN(HbidpsRegister):
    BAR = 1
    ADDR = 0x190
    _fields_ = [('comp_en', ctypes.c_uint, 9),
                ('slave_in_en', ctypes.c_uint, 1),
                ('master_out_en', ctypes.c_uint, 1),
                ('rsvd', ctypes.c_uint, 21)]

class HC_GANG_COMPB_HCR_EN(HbidpsRegister):
    BAR = 1
    ADDR = 0x194
    _fields_ = [('comp_en', ctypes.c_uint, 9),
                ('slave_in_en', ctypes.c_uint, 1),
                ('master_out_en', ctypes.c_uint, 1),
                ('rsvd', ctypes.c_uint, 21)]

class HC_GANG_SYNC_HCM_EN(HbidpsRegister):
    BAR = 1
    ADDR = 0x198
    _fields_ = [('comp_en', ctypes.c_uint, 9),
                ('slave_in_en', ctypes.c_uint, 1),
                ('master_out_en', ctypes.c_uint, 1),
                ('rsvd', ctypes.c_uint, 21)]

class AD5676_VT_DAC_00_WR_DATA(HbidpsRegister):
    BAR = 1
    ADDR = 0x1a0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5676_VT_DAC_00_RD_DATA(HbidpsRegister):
    BAR = 1
    ADDR = 0x1a4
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5676_VT_DAC_01_WR_DATA(HbidpsRegister):
    BAR = 1
    ADDR = 0x1a8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class AD5676_VT_DAC_01_RD_DATA(HbidpsRegister):
    BAR = 1
    ADDR = 0x1ac
    _fields_ = [('Data', ctypes.c_uint, 32)]

class LTC2975_CTRL_REGISTER(HbidpsRegister):
    BAR = 1
    ADDR = 0x1b0
    _fields_ = [('rw_ctrl', ctypes.c_uint, 4),
                ('rsvd', ctypes.c_uint, 12),
                ('r_pbad_n', ctypes.c_uint, 4),
                ('rsvd', ctypes.c_uint, 12)]


class LTM4680_RUN(HbidpsRegister):
    BAR = 1
    ADDR = 0x1b4
    _fields_ = [('run', ctypes.c_uint, 10),
                ('rsvd', ctypes.c_uint, 22)]

class AD7616_VM_ADC_CHANNEL_SEL(HbidpsRegister):
    BAR = 1
    ADDR = 0x1b8
    _fields_ = [('Data', ctypes.c_uint, 32)]


class AD7616_CHANNEL_VM_ADC(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 16
    ADDR = 0x1c0
    _fields_ = [('data', c_uint, 32)]


class FIVR_DAC_GPIO_CONTROL(HbidpsRegister):
    BAR = 1
    ADDR = 0x200
    _fields_ = [('rw_vtarget_00_sel', ctypes.c_uint, 1),
                ('rw_current_source_en', ctypes.c_uint, 1),
                ('rw_vtarget_short_test_sel', ctypes.c_uint, 1),
                ('rsvd', ctypes.c_uint, 29)]

class GLOBAL_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x700
    _fields_ = [('rwc_lc_tq_notify', ctypes.c_uint, 1),
                ('rwc_hc_tq_notify', ctypes.c_uint, 1),
                ('rwc_vtarg_tq_notify', ctypes.c_uint, 1),
                ('r_lc', ctypes.c_uint, 1),
                ('r_hc', ctypes.c_uint, 1),
                ('r_sys_current', ctypes.c_uint, 1),
                ('r_temp', ctypes.c_uint, 1),
                ('r_lc_sample', ctypes.c_uint, 1),
                ('r_hc_sample', ctypes.c_uint, 1),
                ('r_vmeas_sample', ctypes.c_uint, 1),
                ('rwc_invalid_tq_cmd', ctypes.c_uint, 4),
                ('unused_17_14_slice_lo', ctypes.c_uint, 4),
                ('r_lc_busy', ctypes.c_uint, 1),
                ('r_hc_busy', ctypes.c_uint, 1),
                ('r_vtarg_busy', ctypes.c_uint, 1),
                ('rwc_invalid_gnd_ctrl_tq_cmd', ctypes.c_uint, 1),
                ('unused_28_21_slice_lo', ctypes.c_uint, 7),
                ('rw_temp_alarms_mask', ctypes.c_uint, 1),
                ('rw_lc_alarms_mask', ctypes.c_uint, 1),
                ('rw_hc_alarms_mask', ctypes.c_uint, 1)]


class LC_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x704
    _fields_ = [('rwc_alert', ctypes.c_uint, 4),
                ('rwc_status', ctypes.c_uint, 16),
                ('rwc_comm', ctypes.c_uint, 4),
                ('r_irange_check_fail', ctypes.c_uint, 1),
                ('r_pwr_off_delay', ctypes.c_uint, 1),
                ('r_lc_temp', ctypes.c_uint, 1),
                ('r_lc_cfold', ctypes.c_uint, 1),
                ('unused', ctypes.c_uint, 4)]


class LC_TEMP_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x98c
    _fields_ = [('rwc_lc_temp_lo', ctypes.c_uint, 4),
                ('rwc_lc_temp_hi', ctypes.c_uint, 4),
                ('unused', ctypes.c_uint, 24)]


class LC_LOWER_TEMP_LIMIT(HbidpsRegister):
    BAR = 1
    ADDR = 0x984
    _fields_ = [('rw_data', ctypes.c_uint, 32)]


class LC_UPPER_TEMP_LIMIT(HbidpsRegister):
    BAR = 1
    ADDR = 0x988
    _fields_ = [('rw_data', ctypes.c_uint, 32)]


class LC_POWER_OFF_DELAY_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x920
    _fields_ = [('rwc_lc_pwr_off_delay_check_fail', ctypes.c_uint, 16),
                ('unused', ctypes.c_uint, 16)]


class LC_IRANGE_CHECK_FAIL_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x928
    _fields_ = [('rwc_lc_irange_check_check_fail', ctypes.c_uint, 16),
                ('unused', ctypes.c_uint, 16)]


class LC_SAMPLE_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x714
    _fields_ = [('rwc_start_error', ctypes.c_uint, 4),
                ('unused_7_4', ctypes.c_uint, 4),
                ('rwc_oob_error', ctypes.c_uint, 4),
                ('unused_15_12', ctypes.c_uint, 4),
                ('rwc_samples_lost', ctypes.c_uint, 4),
                ('unused_31_20', ctypes.c_uint, 12)]


class HC_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x708
    _fields_ = [('rwc_alert', ctypes.c_uint, 5),
                ('rwc_status', ctypes.c_uint, 10),
                ('rwc_comm', ctypes.c_uint, 5),
                ('r_vrange', ctypes.c_uint, 1),
                ('r_irange_check_fail', ctypes.c_uint, 1),
                ('r_pwr_off_delay', ctypes.c_uint, 1),
                ('r_hc_temp', ctypes.c_uint, 1),
                ('r_hc_cfold', ctypes.c_uint, 1),
                ('unused', ctypes.c_uint, 7)]


class HC_TEMP_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x998
    _fields_ = [('rwc_hc_temp_lo', ctypes.c_uint, 10),
                ('rwc_hc_temp_hi', ctypes.c_uint, 10),
                ('unused', ctypes.c_uint, 12)]


class HC_LOWER_TEMP_LIMIT(HbidpsRegister):
    BAR = 1
    ADDR = 0x990
    _fields_ = [('rw_data', ctypes.c_uint, 32)]


class HC_UPPER_TEMP_LIMIT(HbidpsRegister):
    BAR = 1
    ADDR = 0x994
    _fields_ = [('rw_data', ctypes.c_uint, 32)]


class HC_POWER_OFF_DELAY_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x91c
    _fields_ = [('rwc_hc_pwr_off_delay_check_fail', ctypes.c_uint, 10),
                ('unused', ctypes.c_uint, 22)]


class HC_IRANGE_CHECK_FAIL_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x924
    _fields_ = [('rwc_hc_irange_check_fail', ctypes.c_uint, 10),
                ('unused', ctypes.c_uint, 22)]

class HC_VRANGE_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x90c
    _fields_ = [('rwc_vrange_self', ctypes.c_uint, 10),
                ('rwc_vrange_neighbor', ctypes.c_uint, 10),
                ('rwc_vrange_check_fail', ctypes.c_uint, 10),
                ('unused', ctypes.c_uint, 2)]


class DEBUG_LED_CTRL_GREEN(HbidpsRegister):
    BAR = 1
    ADDR = 0x204
    _fields_ = [('Data', ctypes.c_uint, 32)]


class DEBUG_LED_CTRL_RED(HbidpsRegister):
    BAR = 1
    ADDR = 0x208
    _fields_ = [('Data', ctypes.c_uint, 32)]


class MAX14662_SHUTDOWN_CONTROL(HbidpsRegister):
    BAR = 1
    ADDR = 0x20c
    _fields_ = [('rw_st_vt_lb', ctypes.c_uint, 1),
                ('rw_st_vm_lb', ctypes.c_uint, 1),
                ('rw_st_vhdd_lb', ctypes.c_uint, 1),
                ('rw_st_gang_lb', ctypes.c_uint, 4),
                ('unused', ctypes.c_uint, 25)]


class AURORA_ERROR_COUNTS(HbidpsRegister):
    BAR = 1
    ADDR = 0x228
    _fields_ = [('Soft_error', ctypes.c_uint, 16),
                ('Hard_error', ctypes.c_uint, 16)]


class LAST_TRIGGER_SEEN(HbidpsRegister):
    BAR = 1
    ADDR = 0x22c
    _fields_ = [('lut_dword_index', c_uint, 14),
                ('reserved', c_uint, 1),
                ('dps_ctrl_trig', c_uint, 1),
                ('reserved', c_uint, 3),
                ('sw_type_trigger', c_uint, 1),
                ('dut_domain_id', c_uint, 6),
                ('card_type', c_uint, 6)]


class SEND_TRIGGER(HbidpsRegister):
    BAR = 1
    ADDR = 0x230
    _fields_ = [('lut_dword_index', c_uint, 14),
                ('reserved', c_uint, 1),
                ('dps_ctrl_trig', c_uint, 1),
                ('reserved', c_uint, 3),
                ('sw_type_trigger', c_uint, 1),
                ('dut_domain_id', c_uint, 6),
                ('card_type', c_uint, 6)]


class TRIG_RX_TEST_FIFO_DATA_COUNT(HbidpsRegister):
    BAR = 1
    ADDR = 0x234
    _fields_ = [('r_tx_fifo_count', c_uint, 10),
                ('reserved', c_uint, 6),
                ('r_rx_fifo_count', c_uint, 10),
                ('reserved', c_uint, 6)]


class DUT_DOMAIN_ID_NN(HbidpsRegister):
    BAR = 1
    BASEMUL = 8
    ADDR = 0x244
    _fields_ = [('Data', ctypes.c_uint, 32)]


class PMBUS_CONTROL_STATUS(HbidpsRegister):
    BAR = 1
    ADDR = 0x270
    _fields_ = [('rx_fifo_count', c_uint, 10),
                ('unused_15_10', c_uint, 6),
                ('tx_fifo_count', c_uint, 10),
                ('unused28_26', c_uint, 3),
                ('reset', c_uint, 1),
                ('go', c_uint, 1),
                ('busy', c_uint, 1)]


class PMBUS_TX_FIFO(HbidpsRegister):
    BAR = 1
    ADDR = 0x274
    _fields_ = [('tx_data', c_uint, 16),
                ('pm_bus_transaction', c_uint, 3),
                ('unused', c_uint, 1),
                ('command_code', c_uint, 8),
                ('target_device_number', c_uint, 4)]


class PMBUS_RX_FIFO(HbidpsRegister):
    BAR = 1
    ADDR = 0x278
    _fields_ = [('rx_data', c_uint, 16),
                ('retries', c_uint, 8),
                ('unused', c_uint, 7),
                ('error', c_uint, 1)]


class CMD_DIRECT_INJECT_LO(HbidpsRegister):
    BAR = 1
    ADDR = 0x280
    _fields_ = [('generic_payload', c_uint, 32)]


class CMD_DIRECT_INJECT_HI(HbidpsRegister):
    BAR = 1
    ADDR = 0x284
    _fields_ = [('generic_payload', c_uint, 8),
                ('device_number', c_uint, 8),
                ('command', c_uint, 8),
                ('reserved', c_uint, 7),
                ('write_or_read', c_uint, 1)]


class VMEAS_SAMPLE_COUNT(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x290
    _fields_ = [('rw_sample_count', ctypes.c_uint, 32)]


class VMEAS_SAMPLE_RATIO(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x294
    _fields_ = [('rw_sample_ratio', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class VMEAS_SAMPLE_METADATA(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x298
    _fields_ = [('rw_sample_metadata', ctypes.c_uint, 32)]


class VMEAS_SAMPLE_RAIL_SEL(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x29c
    _fields_ = [('rw_sample_rail_sel', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class LC_SAMPLE_COUNT(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x2e0
    _fields_ = [('rw_sample_count', ctypes.c_uint, 32)]


class LC_SAMPLE_RATIO(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x2e4
    _fields_ = [('rw_sample_ratio', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class LC_SAMPLE_METADATA(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x2e8
    _fields_ = [('rw_sample_metadata', ctypes.c_uint, 32)]


class LC_SAMPLE_RAIL_SEL(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x2ec
    _fields_ = [('rw_sample_rail_sel', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class HC_SAMPLE_COUNT(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x320
    _fields_ = [('rw_sample_count', ctypes.c_uint, 32)]


class HC_SAMPLE_RATIO(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x324
    _fields_ = [('rw_sample_ratio', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class HC_SAMPLE_METADATA(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x328
    _fields_ = [('rw_sample_metadata', ctypes.c_uint, 32)]


class HC_SAMPLE_RAIL_SEL(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x32c
    _fields_ = [('rw_sample_rail_sel', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class SAMPLE_ENGINE_RESET(HbidpsRegister):
    BAR = 1
    ADDR = 0x360
    _fields_ = [('Data', ctypes.c_uint, 32)]


class VMEAS_HEADER_REGION_BASE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x364
    _fields_ = [('rw_hearder_region_base', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class VMEAS_HEADER_REGION_SIZE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x368
    _fields_ = [('rw_header_region_size', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class VMEAS_SAMPLE_REGION_BASE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x36c
    _fields_ = [('rw_sample_region_base', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class VMEAS_SAMPLE_REGION_SIZE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 5
    ADDR = 0x370
    _fields_ = [('rw_sample_region_size', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class LC_HEADER_REGION_BASE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3b4
    _fields_ = [('rw_hearder_region_base', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class LC_HEADER_REGION_SIZE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3b8
    _fields_ = [('rw_header_region_size', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class LC_SAMPLE_REGION_BASE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3bc
    _fields_ = [('rw_sample_region_base', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class LC_SAMPLE_REGION_SIZE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3c0
    _fields_ = [('rw_sample_region_size', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class HC_HEADER_REGION_BASE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3f4
    _fields_ = [('rw_hearder_region_base', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class HC_HEADER_REGION_SIZE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3f8
    _fields_ = [('rw_header_region_size', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]

class HC_SAMPLE_REGION_BASE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x3fc
    _fields_ = [('rw_sample_region_base', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class HC_SAMPLE_REGION_SIZE(HbidpsRegister):
    BAR = 1
    BASEMUL = 16
    REGCOUNT = 4
    ADDR = 0x400
    _fields_ = [('rw_sample_region_size', ctypes.c_uint, 31),
                ('rsvd', ctypes.c_uint, 1)]


class SAMPLING_ACTIVE(HbidpsRegister):
    BAR = 1
    ADDR = 0x434
    _fields_ = [('Data', ctypes.c_uint, 32)]


class HC_DUT_NN_RAIL_CONFIG(HbidpsRegister):
    BAR = 1
    REGCOUNT = 4
    ADDR = 0x4d0
    _fields_ = [('rw_rail_config', ctypes.c_uint, 10),
                ('rsvd', ctypes.c_uint, 22)]


class LC_DUT_NN_RAIL_CONFIG(HbidpsRegister):
    BAR = 1
    ADDR = 0x4c0
    REGCOUNT = 4
    _fields_ = [('rw_rail_config', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class VTARGER_DUT_NN_RAIL_CONFIG(HbidpsRegister):
    BAR = 1
    ADDR = 0x4e0
    REGCOUNT = 4
    _fields_ = [('rw_rail_config', ctypes.c_uint, 16),
                ('rsvd', ctypes.c_uint, 16)]


class DUT_DOMAIN_ID_NN(HbidpsRegister):
    BAR = 1
    ADDR = 0x4a0
    REGCOUNT = 4
    _fields_ = [('dut_domain_id', ctypes.c_uint, 6),
                ('dut_domain_id_valid', ctypes.c_uint, 1),
                ('rsvd', ctypes.c_uint, 25)]


class PMBUS_CMD_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x894
    _fields_ = [('delay', c_uint, 32)]


class CHAN_OFF_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x890
    _fields_ = [('delay', c_uint, 32)]


class PMBUS_COM_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x88c
    _fields_ = [('delay', c_uint, 32)]


class RELAY_DIS_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x888
    _fields_ = [('delay', c_uint, 32)]


class RELAY_CONN_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x884
    _fields_ = [('delay', c_uint, 32)]

class CLK_FREQ_AUR_USER_CLK(HbidpsRegister):
    BAR = 1
    ADDR = 0x9a0
    _fields_ = [('measure_counter', c_uint, 12),
                ('window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]

class CLK_FREQ_DDR4_REF_CLK(HbidpsRegister):
    BAR = 1
    ADDR = 0x9a4
    _fields_ = [('measure_counter', c_uint, 12),
                ('window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]

class CLK_FREQ_PCIE_USER_CLK(HbidpsRegister):
    BAR = 1
    ADDR = 0x99c
    _fields_ = [('measure_counter', c_uint, 12),
                ('window_size', c_uint, 12),
                ('div_ratio', c_uint, 8)]

class FLUSH_COMMAND_DELAY(HbidpsRegister):
    BAR = 1
    ADDR = 0x9a8
    _fields_ = [('delay', c_uint, 32)]

class LC_CHANNEL_VOLTAGE(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 16
    ADDR = 0x600
    _fields_ = [('data', c_uint, 32)]


class LC_CHANNEL_CURRENT(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 16
    ADDR = 0x640
    _fields_ = [('data', c_uint, 32)]


class LC_RAIL_STATE(HbidpsRegister):
    BAR = 1
    ADDR = 0x898
    _fields_ = [('lc_pwr_state', c_uint, 32)]


class GROUND_CONTROL_MODE(HbidpsRegister):
    BAR = 1
    ADDR = 0x030
    _fields_ = [('rw_mode', c_uint, 1),
                ('unused', c_uint, 31)]


class HC_RAIL_STATE(HbidpsRegister):
    BAR = 1
    ADDR = 0x89c
    _fields_ = [('rw_hc_pwr_state_slice', c_uint, 20),
                ('unused_31_20_slice', c_uint, 12)]


class S2H_CONTROL(HbidpsRegister):
    BAR = 1
    ADDR = 0x8c8
    _fields_ = [('w_start', c_uint, 1),
                ('w_stop', c_uint, 1),
                ('w_reset', c_uint, 1),
                ('rsvd', c_uint, 29)]


class HC_CHANNEL_VOLTAGE(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 10
    ADDR = 0x680
    _fields_ = [('data', c_uint, 32)]


class HC_CHANNEL_CURRENT(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 10
    ADDR = 0x6c0
    _fields_ = [('data', c_uint, 32)]


class LC_FOLDED(HbidpsRegister):
    BAR = 1
    ADDR = 0x734
    _fields_ = [('folded', c_uint, 16),
                ('rsvd', c_uint, 16)]


class HC_FOLDED(HbidpsRegister):
    BAR = 1
    ADDR = 0x73c
    _fields_ = [('rwc_folded', c_uint, 10),
                ('unused', c_uint, 22)]


class HC_BUSY(HbidpsRegister):
    BAR = 1
    ADDR = 0x738
    _fields_ = [('r_busy', c_uint, 10),
                ('unused_15_10', c_uint, 6),
                ('r_valid', c_uint, 10),
                ('unused_31_26', c_uint, 6)]


class VTARG_BUSY(HbidpsRegister):
    BAR = 1
    ADDR = 0x740
    _fields_ = [('r_busy', c_uint, 16),
                ('r_valid', c_uint, 16)]


class VTARG_FOLDED(HbidpsRegister):
    BAR = 1
    ADDR = 0x744
    _fields_ = [('rwc_vtarget_folded', c_uint, 16),  # 10 according to HLD, why? vtarget has 16 rails in total
                ('unused', c_uint, 16)]


class VTARG_POWER_STATE(HbidpsRegister):
    Bar = 1
    ADDR = 0x748
    _fields_ = [('r_dac_0', c_uint, 16),
               ('r_dac_1', c_uint, 16)]


class HC_RAIL_NN_VRANGE(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 10
    ADDR = 0x8e4
    _fields_ = [('vrange', c_uint, 16),
                ('vrange_valid', c_uint, 1),
                ('vrange_is_external', c_uint, 1),
                ('unused_31_18', c_uint, 14)]


class HC_SYNC_CHANNEL_MASK(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 5
    ADDR = 0xb00
    _fields_ = [('sync_channel_mask', c_uint, 20),
                ('sync_channel_mask_freeze', c_uint, 1),
                ('rsvd', c_uint, 11)]


class HC_VRANGE_SYNC_CHECK_FAIL_ALARM(HbidpsRegister):
    BAR = 1
    ADDR = 0xb14
    _fields_ = [('rwc_sync_check_fail_alarm', c_uint, 10),
                ('rsvd', c_uint, 22)]


class HC_USER_LAST_SYNC_CHANNEL_MASK(HbidpsRegister):
    BAR = 1
    BASEMUL = 4
    REGCOUNT = 5
    ADDR = 0xb18
    _fields_ = [('sync_channel_mask', c_uint, 20),
                ('rsvd', c_uint, 12)]


class HC_SAMPLE_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x718
    _fields_ = [('rwc_start_error', c_uint, 4),
                ('unused_7_4', c_uint, 4),
                ('rwc_oob_error', c_uint, 4),
                ('unused_15_12', c_uint, 4),
                ('rwc_samples_lost', c_uint, 4),
                ('unused_31_20', c_uint, 12)]


class HC_CFOLD_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x9d0
    _fields_ = [('rwc_hc_cfold', c_uint, 10),
                ('unused', c_uint, 22)]


class LC_CFOLD_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x9ac
    _fields_ = [('rwc_lc_cfold', c_uint, 16),
                ('unused', c_uint, 16)]


class LC_BUSY_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x724
    _fields_ = [('rwc_alarm', c_uint, 16),
                ('unused', c_uint, 16)]


class HC_BUSY_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x728
    _fields_ = [('rwc_alarm', c_uint, 10),
                ('unused', c_uint, 22)]


class V_TARGET_BUSY_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x72c
    _fields_ = [('rwc_alarm', c_uint, 16),
                ('unused', c_uint, 16)]


class VMEAS_SAMPLE_ALARMS(HbidpsRegister):
    BAR = 1
    ADDR = 0x71c
    _fields_ = [('rwc_start_error', c_uint, 5),
                ('unused_7_5', c_uint, 3),
                ('rwc_oob_error', c_uint, 5),
                ('unused_15_13', c_uint, 3),
                ('rwc_samples_lost', c_uint, 5),
                ('unused_31_21', c_uint, 11)]


class MFR_CONFIG_LTC2975(HbidpsRegister):
    BAR = 1
    ADDR = 0x910
    _fields_ = [('rw_mfr_config_ltc2975', c_uint, 16),
                ('unused', c_uint, 16)]


class LC_HC_STATUS_ALARM_MASK(HbidpsRegister):
    BAR = 1
    ADDR = 0x720
    _fields_ = [('rw_lc_mask', c_uint, 16),
                ('rw_hc_mask', c_uint, 16)]
