# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time
from time import perf_counter

from Common.fval import Object
from Hbidps.instrument import hbidps_register


class HCRailPMBUS(Object):
    """Helper Class for HBI DPS Rail Devices(LTM4680) Setting

        Methods:
        init_hc_rails_hw() Init 10 rails of 5 LTM4680 in one dps (I2L value)
        read_hc_rails_info_set Read information set for all rails
        init_hc_one_rail_hw() Init 1 rail of 1 LTM4680 in one dps (I2L value)
        read_ltm4680_status() Read status from one LTM4680 rail
        read_ltm4680_temp() Read temperature data set from one LTM4680 rail
        read_ltm4680_vout() Read output voltage data set from one LTM4680 rail
        read_ltm4680_iout() Read output current data set from one LTM4680 rail
        read_ltm4680_input() Read input supply data set from one LTM4680 rail
    """

    def __init__(self, hbidps):
        super().__init__()

        self.hbidps = hbidps
        self.slot = hbidps.slot
        self.regs = hbidps_register

    def init_hc_rails_hw(self, log=False):
        self.disable_hc_run_reg()
        self.disable_hc_gang_reg()
        for hc_chip in range(5):
            for rail in range(2):
                self.init_hc_one_rail_hw(hc_chip, rail, log)

    def read_hc_rails_info_set(self):
        for hc_chip in range(5):
            for rail in range(2):
                self.read_ltm4680_info_set(hc_chip, rail)

    def read_one_hc_rail_info_set(self, hc_rail):
        hc_chip = hc_rail//2
        rail = hc_rail%2
        self.read_ltm4680_info_set(hc_chip, rail)

    def set_operation_on(self, chip, rail):
        # Operation; PAGED Y; CMD 0x01; DATA 0x80; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x01, self.val_to_reg(0x80), rail)

    def init_hc_one_rail_hw(self, chip, rail, log=False):
        # ON_OFF_CONFIG; PAGED Y; CMD 0x02; DATA 0x1F; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x02, self.val_to_reg(0x1F), rail)
        # Operation; PAGED Y; CMD 0x01; DATA 0x00; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x01, self.val_to_reg(0x00), rail)
        # Set HBI DPS FPGA Power State
        self.disable_hc_rail_dps_power_state(chip, rail)

        # MFR_IIN_CAL_GAIN; PAGED N; CMD 0xE8; DATA L11(3.0); Word L11
        self.hbidps.write_ltm4680(chip, 0xE8, self.val_to_l11(3.0))

        # MFR_PWM_MODE; PAGED Y; CMD 0xD4; DATA mode; Byte Reg
        mode_bytes = self.hbidps.read_ltm4680(chip, 0xD4, 1, rail)
        mode = self.bytes_to_val(mode_bytes)
        mode &= 0b01111101
        self.hbidps.write_ltm4680(chip, 0xD4, self.val_to_reg(mode), rail)

        # MFR_PWM_COMP; PAGED Y; CMD 0xD3; DATA comp; Byte Reg
        comp = 0b10011000
        # 3.69ms 20kOhm
        self.hbidps.write_ltm4680(chip, 0xD3, self.val_to_reg(comp), rail)

        # MFR_CONFIG_ALL; PAGED N; CMD 0xD1; DATA conf; Byte Reg
        conf_bytes = self.hbidps.read_ltm4680(chip, 0xD1, 1)
        conf = self.bytes_to_val(conf_bytes)
        conf |= 0b00010000
        self.hbidps.write_ltm4680(chip, 0xD1, self.val_to_reg(conf))

        # VIN_OV_FAULT_LIMIT; PAGED N; CMD 0x55; DATA L11(14.0); Word L11
        self.hbidps.write_ltm4680(chip, 0x55, self.val_to_l11(14.0))

        # VOUT_TRANSITION_RATE; PAGED Y; CMD 0x27; DATA L11(1.0); Word L11
        self.hbidps.write_ltm4680(chip, 0x27, self.val_to_l11(1.0), rail)

        # VOUT_MAX; PAGED Y; CMD 0x24; DATA L16(3.6); Word L16
        self.hbidps.write_ltm4680(chip, 0x24, self.val_to_l16(3.6), rail)
        # VOUT_MARGIN_HIGH; PAGED Y; CMD 0x25; DATA L16(3.6); Word L16
        self.hbidps.write_ltm4680(chip, 0x25, self.val_to_l16(3.6), rail)
        # VOUT_MARGIN_LOW; PAGED Y; CMD 0x26; DATA L16(0); Word L16
        self.hbidps.write_ltm4680(chip, 0x26, self.val_to_l16(0), rail)
        # VOUT_OV_WARN_LIMIT; PAGED Y; CMD 0x42; DATA L16(5); Word L16
        self.hbidps.write_ltm4680(chip, 0x42, self.val_to_l16(5), rail)
        # VOUT_UV_WARN_LIMIT; PAGED Y; CMD 0x43; DATA L16(0); Word L16
        self.hbidps.write_ltm4680(chip, 0x43, self.val_to_l16(0), rail)
        # VOUT_OV_FAULT_LIMIT; PAGED Y; CMD 0x40; DATA L16(3.6); Word L16
        self.hbidps.write_ltm4680(chip, 0x40, self.val_to_l16(3.6), rail)
        # VOUT_UV_FAULT_LIMIT; PAGED Y; CMD 0x44; DATA L16(0); Word L16
        self.hbidps.write_ltm4680(chip, 0x44, self.val_to_l16(0), rail)
        # VOUT_OV_FAULT_RESPONSE; PAGED Y; CMD 0x41; DATA 0x80; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x41, self.val_to_reg(0x80), rail)
        # VOUT_UV_FAULT_RESPONSE; PAGED Y; CMD 0x45; DATA 0x80; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x45, self.val_to_reg(0x80), rail)

        # IOUT_OC_WARN_LIMIT; PAGED Y; CMD 0x4A; DATA L11(55); Word L11
        self.hbidps.write_ltm4680(chip, 0x4A, self.val_to_l11(55), rail)
        # IOUT_OC_FAULT_LIMIT; PAGED Y; CMD 0x46; DATA L11(50); Word L11
        self.hbidps.write_ltm4680(chip, 0x46, self.val_to_l11(50), rail)
        # IOUT_OC_FAULT_RESPONSE; PAGED Y; CMD 0x47; DATA 0xC0; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x47, self.val_to_reg(0xC0), rail)

        # OT_WARN_LIMIT; PAGED Y; CMD 0x51; DATA L11(1000); Word L11
        self.hbidps.write_ltm4680(chip, 0x51, self.val_to_l11(1000), rail)
        # OT_FAULT_LIMIT; PAGED Y; CMD 0x4F; DATA L11(1000); Word L11
        self.hbidps.write_ltm4680(chip, 0x4F, self.val_to_l11(1000), rail)
        # UT_FAULT_LIMIT; PAGED Y; CMD 0x53; DATA L11(-1000); Word L11
        self.hbidps.write_ltm4680(chip, 0x53, self.val_to_l11(-1000), rail)
        # OT_FAULT_RESPONSE; PAGED Y; CMD 0x47; DATA 0x80; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x47, self.val_to_reg(0x80), rail)
        # UT_FAULT_RESPONSE; PAGED Y; CMD 0x54; DATA 0x80; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x54, self.val_to_reg(0x80), rail)

        # TON_MAX_FAULT_LIMIT; PAGED Y; CMD 0x62; DATA L11(200); Word L11
        self.hbidps.write_ltm4680(chip, 0x62, self.val_to_l11(200), rail)
        # TON_MAX_FAULT_RESPONSE; PAGED Y; CMD 0x63; DATA 0x80; Byte Reg
        self.hbidps.write_ltm4680(chip, 0x63, self.val_to_reg(0x80), rail)

        # MFR_FAULT_RESPONSE; PAGED Y; CMD 0xD5; DATA C0; Byte Reg
        self.hbidps.write_ltm4680(chip, 0xD5, self.val_to_reg(0xC0), rail)

        # MFR_CLEAR_PEAKS; PAGED N; CMD 0xE3; CMD Only
        self.hbidps.write_ltm4680(chip, 0xE3, b'')

        # CLEAR_FAULTS; PAGED N; CMD 0x03; CMD Only
        self.hbidps.write_ltm4680(chip, 0x03, b'')

        if log:
            time.sleep(0.5) #check the time delay between set and read
            self.read_ltm4680_info_set(chip, rail)

    def pre_test_clean_setup(self, chip, rail, vout):
        self.Log('info', f'hc device pre-test check')
        self.check_chip_status_clean(chip, rail)
        self.preset_vout(chip, rail, vout)
        self.check_rail_off_and_vout(chip, rail)
        self.clear_fault_and_MFR_peak(chip)
        time.sleep(1)

    def preset_vout(self, chip, rail, vout):
        if vout != None:
            if vout > 0:
                # VOUT_COMMAND; PAGED Y; CMD 0x21; DATA L16(vout); Word L16
                self.hbidps.write_ltm4680(chip, 0x21, self.val_to_l16(vout), rail)

    def check_chip_status_clean(self, chip, rail):
        status_word = self.hbidps.read_ltm4680(chip, 0x79, 2, 0)
        status_word = self.bytes_to_val(status_word)
        if status_word != 0x840:
            self.Log('warning', f'hc chip{chip} rail{rail} status 0x{status_word:04x}')
        else:
            self.Log('info', f'hc chip{chip} rail{rail} status 0x{status_word:04x}')

    def check_rail_off_and_vout(self, chip, rail):
        operation_bytes = self.hbidps.read_ltm4680(chip, 0x01, 1, rail)
        operation = self.bytes_to_val(operation_bytes)
        if operation != 0x00:
            self.Log('warning', f'hc chip{chip} rail{rail} not off 0x{operation:02x}')
        else:
            self.Log('info', f'hc chip{chip} rail{rail} not off 0x{operation:02x}')
        # VOUT_COMMAND; PAGED Y; CMD 0x21; DATA READ; Word L16
        vout_command_l16 = self.hbidps.read_ltm4680(chip, 0x21, 2, rail)
        vout_command = self.l16_to_val(vout_command_l16)
        self.Log('info', f'hc chip{chip} rail{rail} VOUT_COMMAND: {vout_command}V')

    def clear_fault_and_MFR_peak(self, chip):
        self.Log('info', f'clear_fault_and_MFR_peak for chip{chip}')
        # CLEAR_FAULTS; PAGED N; CMD 0x03; CMD Only
        self.hbidps.write_ltm4680(chip, 0x03, b'')
        # MFR_CLEAR_PEAKS; PAGED N; CMD 0xE3; CMD Only
        self.hbidps.write_ltm4680(chip, 0xE3, b'')

    def read_ltm4680_info_set(self, chip, rail):
        self.Log('info', '*************************************************')
        self.Log('info', f'chip {chip}; rail {rail}')
        self.read_ltm4680_status(chip, rail)
        self.read_ltm4680_temp(chip, rail)
        self.read_ltm4680_vout(chip, rail)
        self.read_ltm4680_iout(chip, rail)
        self.read_ltm4680_input(chip, rail)

    def read_ltm4680_status(self, chip, rail):
        self.Log('info', '********************status***********************')
        # STATUS_BYTE; PAGED Y; CMD 0x78; DATA READ; Byte Reg
        status_byte = self.hbidps.read_ltm4680(chip, 0x78, 1, rail)
        status_byte = self.bytes_to_val(status_byte)
        self.Log('info', f'STATUS_BYTE: {hex(status_byte)}')

        # STATUS_WORD; PAGED Y; CMD 0x79; DATA READ; Word Reg
        status_word = self.hbidps.read_ltm4680(chip, 0x79, 2, rail)
        status_word = self.bytes_to_val(status_word)
        self.Log('info', f'STATUS_WORD: {hex(status_word)}')

        # STATUS_VOUT; PAGED Y; CMD 0x7A; DATA READ; Byte Reg
        status_vout = self.hbidps.read_ltm4680(chip, 0x7A, 1, rail)
        status_vout = self.bytes_to_val(status_vout)
        self.Log('info', f'STATUS_VOUT: {hex(status_vout)}')

        # STATUS_IOUT; PAGED Y; CMD 0x7B; DATA READ; Byte Reg
        status_iout = self.hbidps.read_ltm4680(chip, 0x7B, 1, rail)
        status_iout = self.bytes_to_val(status_iout)
        self.Log('info', f'STATUS_IOUT: {hex(status_iout)}')

        # STATUS_INPUT; PAGED N; CMD 0x7C; DATA READ; Byte Reg
        status_input = self.hbidps.read_ltm4680(chip, 0x7C, 1)
        status_input = self.bytes_to_val(status_input)
        self.Log('info', f'STATUS_INPUT: {hex(status_input)}')

        # STATUS_TEMP; PAGED Y; CMD 0x7D; DATA READ; Byte Reg
        status_temp = self.hbidps.read_ltm4680(chip, 0x7D, 1, rail)
        status_temp = self.bytes_to_val(status_temp)
        self.Log('info', f'STATUS_TEMP: {hex(status_temp)}')

        # STATUS_CML; PAGED N; CMD 0x7E; DATA READ; Byte Reg
        status_cml_bytes = self.hbidps.read_ltm4680(chip, 0x7E, 1)
        status_cml = self.bytes_to_val(status_cml_bytes)
        self.Log('info', f'STATUS_CML: {hex(status_cml)}')

        # STATUS_MFR; PAGED Y; CMD 0x80; DATA READ; Byte Reg
        status_mfr_bytes = self.hbidps.read_ltm4680(chip, 0x80, 1, rail)
        status_mfr = self.bytes_to_val(status_mfr_bytes)
        self.Log('info', f'STATUS_MFR: {hex(status_mfr)}')

    def read_ltm4680_temp(self, chip, rail):
        self.Log('info', '********************temp*************************')
        # READ_TEMPERATURE_1; PAGED Y; CMD 0x8D; DATA READ; Word L11
        t_read_l11 = self.hbidps.read_ltm4680(chip, 0x8D, 2, rail)
        t_read = self.l11_to_val(t_read_l11)
        self.Log('info', f'READ_TEMPERATURE_1: {t_read} C')

        # OT_WARN_LIMIT; PAGED Y; CMD 0x51; DATA READ; Word L11
        ot_warn_limit_l11 = self.hbidps.read_ltm4680(chip, 0x51, 2, rail)
        ot_warn_limit = self.l11_to_val(ot_warn_limit_l11)
        self.Log('info', f'OT_WARN_LIMIT: {ot_warn_limit} C')

        # OT_FAULT_LIMIT; PAGED Y; CMD 0x4F; DATA READ; Word L11
        ot_fault_limit_l11 = self.hbidps.read_ltm4680(chip, 0x4F, 2, rail)
        ot_fault_limit = self.l11_to_val(ot_fault_limit_l11)
        self.Log('info', f'OT_FAULT_LIMIT: {ot_fault_limit} C')

        # UT_FAULT_LIMIT; PAGED Y; CMD 0x53; DATA READ; Word L11
        ut_fault_limit_l11 = self.hbidps.read_ltm4680(chip, 0x53, 2, rail)
        ut_fault_limit = self.l11_to_val(ut_fault_limit_l11)
        self.Log('info', f'UT_FAULT_LIMIT: {ut_fault_limit} C')

        # OT_FAULT_RESPONSE; PAGED Y; CMD 0x47; DATA READ; Byte Reg
        ot_response_bytes = self.hbidps.read_ltm4680(chip, 0x47, 1, rail)
        ot_response = self.bytes_to_val(ot_response_bytes)
        self.Log('info', f'OT_FAULT_RESPONSE: {hex(ot_response)}')

        # UT_FAULT_RESPONSE; PAGED Y; CMD 0x54; DATA READ; Byte Reg
        ut_response_bytes = self.hbidps.read_ltm4680(chip, 0x54, 1, rail)
        ut_response = self.bytes_to_val(ut_response_bytes)
        self.Log('info', f'UT_FAULT_RESPONSE: {hex(ut_response)}')

    def read_ltm4680_vout(self, chip, rail):
        self.Log('info', '********************vout*************************')
        # OPERATION; PAGED Y; CMD 0x01; DATA READ; Byte Reg
        operation_bytes = self.hbidps.read_ltm4680(chip, 0x01, 1, rail)
        operation = self.bytes_to_val(operation_bytes)
        self.Log('info', f'OPERATION: {hex(operation)}')

        # ON_OFF_CONFIG; PAGED Y; CMD 0x02; DATA READ; Byte Reg
        on_off_config_bytes = self.hbidps.read_ltm4680(chip, 0x02, 1, rail)
        on_off_config = self.bytes_to_val(on_off_config_bytes)
        self.Log('info', f'ON_OFF_CONFIG: {hex(on_off_config)}')

        # FREQ_SWITCH; PAGED N; CMD 0x33; DATA READ; Word L11
        freq_switch_l11 = self.hbidps.read_ltm4680(chip, 0x33, 2)
        freq_switch = self.l11_to_val(freq_switch_l11)
        self.Log('info', f'FREQ_SWITCH: {freq_switch} kHz')

        # VOUT_MODE; PAGED Y; CMD 0x20; DATA READ; Byte Reg
        vout_mode_bytes = self.hbidps.read_ltm4680(chip, 0x20, 1, rail)
        vout_mode = self.bytes_to_val(vout_mode_bytes)
        self.Log('info', f'VOUT_MODE: {hex(vout_mode)}')

        # VOUT_COMMAND; PAGED Y; CMD 0x21; DATA READ; Word L16
        vout_command_l16 = self.hbidps.read_ltm4680(chip, 0x21, 2, rail)
        vout_command = self.l16_to_val(vout_command_l16)
        self.Log('info', f'VOUT_COMMAND: {vout_command} V')

        # READ_VOUT; PAGED Y; CMD 0x8B; DATA READ; Word L16
        read_vout_l16 = self.hbidps.read_ltm4680(chip, 0x8B, 2, rail)
        read_vout = self.l16_to_val(read_vout_l16)
        self.Log('info', f'READ_VOUT: {read_vout} V')

        # MFR_VOUT_PEAK; PAGED Y; CMD 0xDD; DATA READ; Word L16
        read_vout_l16 = self.hbidps.read_ltm4680(chip, 0xDD, 2, rail)
        read_vout = self.l16_to_val(read_vout_l16)
        self.Log('info', f'MFR_VOUT_PEAK: {read_vout} V')

        # VOUT_MAX; PAGED Y; CMD 0x24; DATA READ; Word L16
        vout_max_l16 = self.hbidps.read_ltm4680(chip, 0x24, 2, rail)
        vout_max = self.l16_to_val(vout_max_l16)
        self.Log('info', f'VOUT_MAX: {vout_max} V')

        # VOUT_MAR_HIGH; PAGED Y; CMD 0x25; DATA READ; Word L16
        vout_mar_high_l16 = self.hbidps.read_ltm4680(chip, 0x25, 2, rail)
        vout_mar_high = self.l16_to_val(vout_mar_high_l16)
        self.Log('info', f'VOUT_MAR_HIGH: {vout_mar_high} V')

        # VOUT_MAR_LOW; PAGED Y; CMD 0x26; DATA READ; Word L16
        vout_mar_low_l16 = self.hbidps.read_ltm4680(chip, 0x26, 2, rail)
        vout_mar_low = self.l16_to_val(vout_mar_low_l16)
        self.Log('info', f'VOUT_MAR_LOW: {vout_mar_low} V')

        # VOUT_RATE; PAGED Y; CMD 0x27; DATA READ; Word L11
        vout_rate_l11 = self.hbidps.read_ltm4680(chip, 0x27, 2, rail)
        vout_rate = self.l11_to_val(vout_rate_l11)
        self.Log('info', f'VOUT_RATE: {vout_rate} V/ms')

        # VOUT_OV_FAULT_LIMIT; PAGED Y; CMD 0x40; DATA READ; Word L16
        vout_ov_fault_l16 = self.hbidps.read_ltm4680(chip, 0x40, 2, rail)
        vout_ov_fault = self.l16_to_val(vout_ov_fault_l16)
        self.Log('info', f'VOUT_OV_FAULT_LIMIT: {vout_ov_fault} V')

        # VOUT_OV_WARN_LIMIT; PAGED Y; CMD 0x42; DATA READ; Word L16
        vout_ov_warn_l16 = self.hbidps.read_ltm4680(chip, 0x42, 2, rail)
        vout_ov_warn = self.l16_to_val(vout_ov_warn_l16)
        self.Log('info', f'VOUT_OV_WARN_LIMIT: {vout_ov_warn} V')

        # VOUT_UV_FAULT_LIMIT; PAGED Y; CMD 0x44; DATA READ; Word L16
        vout_uv_fault_l16 = self.hbidps.read_ltm4680(chip, 0x44, 2, rail)
        vout_uv_fault = self.l16_to_val(vout_uv_fault_l16)
        self.Log('info', f'VOUT_UV_FAULT_LIMIT: {vout_uv_fault} V')

        # VOUT_UV_WARN_LIMIT; PAGED Y; CMD 0x43; DATA READ; Word L16
        vout_uv_warn_l16 = self.hbidps.read_ltm4680(chip, 0x43, 2, rail)
        vout_uv_warn = self.l16_to_val(vout_uv_warn_l16)
        self.Log('info', f'VOUT_UV_WARN_LIMIT: {vout_uv_warn} V')

        # VOUT_OV_FAULT_RES; PAGED Y; CMD 0x41; DATA READ; Byte Reg
        vout_ov_fault_res_bytes = self.hbidps.read_ltm4680(chip, 0x41, 1, rail)
        vout_ov_fault_res = self.bytes_to_val(vout_ov_fault_res_bytes)
        self.Log('info', f'VOUT_OV_FAULT_RES: {hex(vout_ov_fault_res)}')

        # VOUT_UV_FAULT_RES; PAGED Y; CMD 0x45; DATA READ; Byte Reg
        vout_uv_fault_res_bytes = self.hbidps.read_ltm4680(chip, 0x45, 1, rail)
        vout_uv_fault_res = self.bytes_to_val(vout_uv_fault_res_bytes)
        self.Log('info', f'VOUT_UV_FAULT_RES: {hex(vout_uv_fault_res)}')

    def read_ltm4680_iout(self, chip, rail):
        self.Log('info', '********************iout*************************')
        # READ_IOUT; PAGED Y; CMD 0x8C; DATA READ; Word L11
        read_iout_l11 = self.hbidps.read_ltm4680(chip, 0x8C, 2, rail)
        read_iout = self.l11_to_val(read_iout_l11)
        self.Log('info', f'READ_IOUT: {read_iout} A')

        # MFR_IOUT_PEAK; PAGED Y; CMD 0xD7; DATA READ; Word L11
        read_iout_l11 = self.hbidps.read_ltm4680(chip, 0xD7, 2, rail)
        read_iout = self.l11_to_val(read_iout_l11)
        self.Log('info', f'MFR_IOUT_PEAK: {read_iout} A')

        # IOUT_OC_FAULT_LIMIT; PAGED Y; CMD 0x46; DATA READ; Word L11
        iout_oc_fault_limit_l11 = self.hbidps.read_ltm4680(chip, 0x46, 2, rail)
        iout_oc_fault_limit = self.l11_to_val(iout_oc_fault_limit_l11)
        self.Log('info', f'IOUT_OC_FAULT_LIMIT: {iout_oc_fault_limit} A')

        # IOUT_OC_WARN_LIMIT; PAGED Y; CMD 0x4A; DATA READ; Word L11
        iout_oc_warn_limit_l11 = self.hbidps.read_ltm4680(chip, 0x4A, 2, rail)
        iout_oc_warn_limit = self.l11_to_val(iout_oc_warn_limit_l11)
        self.Log('info', f'IOUT_OC_WARN_LIMIT: {iout_oc_warn_limit} A')

        # IOUT_OC_FAULT_RES; PAGED Y; CMD 0x50; DATA READ; Byte Reg
        iout_oc_fault_res_bytes = self.hbidps.read_ltm4680(chip, 0x50, 1, rail)
        iout_oc_fault_res = self.bytes_to_val(iout_oc_fault_res_bytes)
        self.Log('info', f'IOUT_OC_FAULT_RES: {hex(iout_oc_fault_res)}')

    def read_ltm4680_input(self, chip, rail):
        self.Log('info', '********************input************************')
        # READ_VIN; PAGED N; CMD 0x88; DATA READ; Word L11
        read_vin_l11 = self.hbidps.read_ltm4680(chip, 0x88, 2)
        read_vin = self.l11_to_val(read_vin_l11)
        self.Log('info', f'READ_VIN: {read_vin} V')

        # READ_IIN; PAGED N; CMD 0x89; DATA READ; Word L11
        read_iin_l11 = self.hbidps.read_ltm4680(chip, 0x89, 2)
        read_iin = self.l11_to_val(read_iin_l11)
        self.Log('info', f'READ_IIN: {read_iin} A')

        # VIN_OV_FAULT_LIMIT; PAGED N; CMD 0x55; DATA READ; Word L11
        vin_ov_fault_limit_l11 = self.hbidps.read_ltm4680(chip, 0x55, 2)
        vin_ov_fault_limit = self.l11_to_val(vin_ov_fault_limit_l11)
        self.Log('info', f'VIN_OV_FAULT_LIMIT: {vin_ov_fault_limit} V')

        # VIN_UV_WARN_LIMIT; PAGED N; CMD 0x58; DATA READ; Word L11
        vin_uv_warn_limit_l11 = self.hbidps.read_ltm4680(chip, 0x58, 2)
        vin_uv_warn_limit = self.l11_to_val(vin_uv_warn_limit_l11)
        self.Log('info', f'VIN_UV_WARN_LIMIT: {vin_uv_warn_limit} V')

        # IIN_OC_WARN_LIMIT; PAGED N; CMD 0x5D; DATA READ; Word L11
        iin_oc_warn_limit_l11 = self.hbidps.read_ltm4680(chip, 0x5D, 2)
        iin_oc_warn_limit = self.l11_to_val(iin_oc_warn_limit_l11)
        self.Log('info', f'IIN_OC_WARN_LIMIT: {iin_oc_warn_limit} A')

        # VIN_OV_FAULT_RES; PAGED Y; CMD 0x56; DATA READ; Byte Reg
        vin_ov_fault_res_bytes = self.hbidps.read_ltm4680(chip, 0x56, 1, rail)
        vin_ov_fault_res = self.bytes_to_val(vin_ov_fault_res_bytes)
        self.Log('info', f'VIN_OV_FAULT_RES: {hex(vin_ov_fault_res)}')

    def get_user_data_03(self, chip, page):
        data_03 = self.hbidps.read_ltm4680(chip, 0xb3, 2, page)
        data_03 = self.bytes_to_val(data_03)
        return data_03

    def get_user_data_04(self, chip):
        data_04 = self.hbidps.read_ltm4680(chip, 0xb4, 2)
        data_04 = self.bytes_to_val(data_04)
        return data_04

    def read_reg_command(self, cmd, length, chip, page=-1):
        command = self.hbidps.read_ltm4680(chip, cmd, length, page)
        command = self.bytes_to_val(command)
        return command

    def bytes_to_val(self, data_bytes):
        return int.from_bytes(data_bytes, 'little')

    def l16_to_val(self, data_bytes):
        mode = 0b10100
        return self.hbidps.froml16(data_bytes, mode)

    def l11_to_val(self, data_bytes):
        return self.hbidps.froml11(data_bytes)

    def val_to_l16(self, data_val):
        mode = 0b10100
        return self.hbidps.tol16(data_val, mode)

    def val_to_l11(self, data_val):
        return self.hbidps.tol11(data_val)

    def val_to_reg(self, data, bytes=1):
        return data.to_bytes(bytes, 'little')

    def set_chip_freq_switch(self, chip, val):
        self.hbidps.write_ltm4680(chip, 0x33, self.val_to_reg(val, 2))

    def get_chip_freq_switch(self, chip):
        freq_switch_l11 = self.hbidps.read_ltm4680(chip, 0x33, 2)
        freq_switch = self.bytes_to_val(freq_switch_l11)
        return freq_switch

    def disable_hc_rail_dps_power_state(self, chip, rail):
        hc_rail_power_state = \
            self.hbidps.read_bar_register(self.regs.HC_RAIL_STATE)
        rail_num = chip * 2 + rail
        disable_mask = 0xFFFF - (0b11 << (rail_num*2))
        hc_rail_power_state.value &= disable_mask
        self.hbidps.write_bar_register(hc_rail_power_state)

    def get_hc_rail_power_state(self):
        hc_rail_power_state = \
            self.hbidps.read_bar_register(self.regs.HC_RAIL_STATE)
        return hc_rail_power_state.value

    def log_hc_rail_dps_power_state(self):
        hc_rail_power_state = \
            self.hbidps.read_bar_register(self.regs.HC_RAIL_STATE)
        self.Log('info', f'hc power state: {hex(hc_rail_power_state.value)}')

    def enable_hc_run_reg(self, log=False):
        hc_run_reg = self.hbidps.read_bar_register(self.regs.LTM4680_RUN)
        hc_run_reg.value = 0x3FF
        self.hbidps.write_bar_register(hc_run_reg)
        if log:
            hc_run_reg = self.hbidps.read_bar_register(self.regs.LTM4680_RUN)
            self.Log('info', f'HC run pins {hex(hc_run_reg.value)}')

    def log_hc_run_reg(self):
        hc_run_reg = self.hbidps.read_bar_register(self.regs.LTM4680_RUN)
        self.Log('info', f'HC run pins {hex(hc_run_reg.value)}')

    def disable_hc_run_reg(self):
        hc_run_reg = self.hbidps.read_bar_register(self.regs.LTM4680_RUN)
        hc_run_reg.value = 0
        self.hbidps.write_bar_register(hc_run_reg)

    def check_hc_rails_clean_start_state(self, log=False):
        re_init = False
        reg = self.hbidps.read_bar_register(self.regs.LTM4680_RUN)
        if (reg.run != 0):
            self.Log('warning', f'hc rail run not all off {hex(reg.run)}')
            re_init = True
        reg = self.hbidps.read_bar_register(self.regs.HC_RAIL_STATE)
        if (reg.rw_hc_pwr_state_slice != 0):
            self.Log('warning', f'hc rail pwr not all off {hex(reg.rw_hc_pwr_state_slice)}')
            re_init = True
        hc_fold_status = self.check_hc_rails_fold_status()
        if (hc_fold_status != 0b11_1111_1111):
            self.Log('warning', f'hc rail not all folded {hex(hc_fold_status)}')
            re_init = True
        # for chip in range(5):
        #     self.hbidps.write_ltm4680(chip, 0x03, b'')
        #     time.sleep(0.5)
        #     for page in range(2):
        #         self.hbidps.write_ltm4680(chip, 0x7e, self.val_to_reg(0x02), page)
        #         status_word = self.hbidps.read_ltm4680(chip, 0x79, 1, page)
        #         status_word = self.bytes_to_val(status_word)
        #         if status_word != 0x40:
        #             self.Log('error', f'Chip {chip}, Page {page}: hc rail status word: {hex(status_word)}')
        #
        #         status_cml = self.hbidps.read_ltm4680(chip, 0x7e, 1, page)
        #         status_cml = self.bytes_to_val(status_cml)
        #         self.Log('info', f'Chip {chip}, Page {page}: hc rail cml status byte: {hex(status_cml)}')
        if re_init:
            self.init_hc_rails_hw(log)

    def debug_hc_rails_state_status(self):
        reg = self.hbidps.read_bar_register(self.regs.LTM4680_RUN)
        self.Log('info', f'hc rail run {hex(reg.run)}')
        reg = self.hbidps.read_bar_register(self.regs.HC_RAIL_STATE)
        self.Log('info', f'hc rail pwr {hex(reg.rw_hc_pwr_state_slice)}')
        hc_fold_status = self.check_hc_rails_fold_status()
        self.Log('info', f'hc rail fold {hex(hc_fold_status)}')

    def check_hc_rails_fold_status(self):
        hc_fold_status = self.hbidps.read_bar_register(self.regs.HC_FOLDED)
        hc_fold_status.value = 0x3FF
        self.hbidps.write_bar_register(hc_fold_status)
        hc_fold_status = self.hbidps.read_bar_register(self.regs.HC_FOLDED)
        return hc_fold_status.value

    def disable_hc_gang_reg(self):
        hc_gang_compa_reg = \
            self.hbidps.read_bar_register(self.regs.HC_GANG_COMPA_HCR_EN)
        hc_gang_compa_reg.value = 0
        self.hbidps.write_bar_register(hc_gang_compa_reg)
        hc_gang_compb_reg = \
            self.hbidps.read_bar_register(self.regs.HC_GANG_COMPB_HCR_EN)
        hc_gang_compb_reg.value = 0
        self.hbidps.write_bar_register(hc_gang_compb_reg)
        hc_gang_sync_reg = \
            self.hbidps.read_bar_register(self.regs.HC_GANG_SYNC_HCM_EN)
        hc_gang_sync_reg.value = 0
        self.hbidps.write_bar_register(hc_gang_sync_reg)
