# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import os
import sys
import time

root = os.path.abspath(os.path.join(os.path.abspath(__file__), '..', '..', '..'))
sys.path.insert(0, root)

from Common import hilmon as hil
from Common.configs import HBIDPS_FPGA_IMAGE
from ThirdParty.SASS.suppress_sensor import suppress_sensor

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('slot', type=int)
    return parser.parse_args()


def load_fpga(slot):
    print(f'Loading: {HBIDPS_FPGA_IMAGE}')
    with suppress_sensor(f'FVAL HBIDPS {slot}', f'HBIDPS:{slot}'):
        hil.hbiDpsDeviceDisable(slot)
        hil.hbiDpsFpgaLoad(slot, HBIDPS_FPGA_IMAGE)
        time.sleep(0.5)
        hil.hbiDpsDeviceEnable(slot)

if __name__ == '__main__':
    args = parse_args()
    load_fpga(args.slot)
