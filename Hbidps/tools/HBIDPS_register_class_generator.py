################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import argparse
import sys
import os.path


def extract_register_data(fileData):
    register_data = []
    extract_first_line, register_addrss_regex, register_info_regex = get_regx_statements()
    for register_info in fileData:
        get_register_name(register_data, register_info, register_info_regex)
        get_register_address(register_addrss_regex, register_data, register_info)
        get_register_with_multiple_address(extract_first_line, register_data, register_info)
    return register_data


def get_regx_statements():
    register_info_regex = re.compile(r'\s*([0-9a-zA-Z_]+)\s*bar1_([0-9a-zA-Z_]+);')
    register_addrss_regex = re.compile(r'\s*localparam\s*addr\s*=\s\'h([0-9a-zA-Z_]+)')
    extract_first_line = re.compile('\\s*([a-z]+)\\s*([a-z]+)\\s*([a-z]+)\\s*')
    return extract_first_line, register_addrss_regex, register_info_regex


def get_register_with_multiple_address(extract_first_line, register_data, register_info):
    split_multiple_register = register_info.split(' = ')
    if split_multiple_register != None:
        first_line = extract_first_line.search(register_info)
        if first_line != None and first_line.group(3) == 'addr':
            for ch in "{};,'h":
                split_multiple_register[1] = split_multiple_register[1].replace(ch, '')
            all_register_address = split_multiple_register[1].split()
            register_name = register_data.pop()
            for reg_count in range(len(all_register_address)):
                each_register_name = register_name.replace('NN', str(reg_count))
                register_data.append(each_register_name)
                register_data.append(hex(int(all_register_address[reg_count], 16)))
        else:
            pass


def get_register_address(register_addrss_regex, register_data, register_info):
    register_address_info = register_addrss_regex.search(register_info)
    if register_address_info != None:
        register_address = register_address_info.group(1)
        register_data.append(hex(int(register_address, 16)))


def get_register_name(register_data, register_info, register_info_regex):
    register_name_info = register_info_regex.search(register_info)
    if register_name_info != None:
        register_name = register_name_info.group(2)
        register_data.append(register_name)


def print_register_classes(bar_regs, registerClass, registerBar):
    for registerName, registerAddress in bar_regs:
        print('class {}({}):'.format(registerName, registerClass))
        print('    BAR = {}'.format(registerBar))
        print('    ADDR = {}'.format((registerAddress)))
        print('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]')
        print('')


def write_register_classes(bar_regs, registerClass, registerBar, outputFileName):
    with open(outputFileName, 'w') as registerFile:
        for registerName, registerAddress in bar_regs:
            registerFile.write('class {}({}):\n'.format(registerName, registerClass))
            registerFile.write('    BAR = {}\n'.format(registerBar))
            registerFile.write('    ADDR = {}\n'.format((registerAddress)))
            registerFile.write('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]\n\n')


def read_and_clean_input_file(fileName):
    fileData = []
    with open(fileName) as f:
        return [line for line in f]


def output_register_classes(OutputFileName, bar_regs, args):
    if OutputFileName == None:
        print_register_classes(bar_regs, args.regclass, args.bar)
    else:
        if os.path.isfile(OutputFileName):
            print('Output file already exists:')
            overwrite = input('\tWould you like to overwrite existing output file {}?\n'.format(OutputFileName))
            if overwrite.lower() == 'y' or overwrite.lower == 'yes':
                print('\n\toverwriting existing register output file')
            else:
                print('\tFile already exists: Exiting')
                sys.exit(1)
        write_register_classes(bar_regs, args.regclass, args.bar, OutputFileName)


def parse_args(args):
    # Parse command line options
    parser = argparse.ArgumentParser(description='VH to CTYPE register generator')
    parser.add_argument('inputFile', help='Input .vh file that contains register definitions', type=str, action="store")
    parser.add_argument('-bar', help='Bar for registers', type=int, action="store", required=True)
    parser.add_argument('-regclass', help='Register class that registers will derive from', type=str, action="store",
                        required=True)
    parser.add_argument('-o', '--outputfilename', help='optional output filename, otherwise prints to screen', type=str,
                        action="store", default=None)
    args = parser.parse_args(args)
    return args


def Main(user_args=None):
    args = parse_args(user_args)

    outputFileName = args.outputfilename
    inputFileName = args.inputFile

    try:
        with open(inputFileName):
            pass
    except FileNotFoundError:
        print('Input File \'{}\' does not exist!'.format(inputFileName))
        sys.exit(1)

    fileData = read_and_clean_input_file(inputFileName)
    register_data = extract_register_data(fileData)
    register_data = list_to_tuple_converter(register_data)
    output_register_classes(outputFileName, tuple(register_data), args)


def list_to_tuple_converter(register_data):
    list_data_address = iter(register_data)
    register_data = zip(list_data_address, list_data_address)
    return register_data


if __name__ == '__main__':
    Main()
