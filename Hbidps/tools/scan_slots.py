# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import sys

root = os.path.abspath(os.path.join(os.path.abspath(__file__), '..', '..', '..'))
sys.path.insert(0, root)

from Common import hilmon as hil


for slot in range(16):
    try:
        blt = hil.hbiDpsBltBoardRead(slot)
    except RuntimeError:
        pass
    else:
        print(f'slot {slot}', blt)

