################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Test the DIrect Digital IO that is attached to the FPGA from the LC Rails"""
import random
import time


from Common.fval import skip
from Hbidps.testbench.Ganging_Max14662_I2C import MAX_GANG_CHIPS, GangMax14662I2cInterface
from Hbidps.testbench.lc import LCRailTestbench
from Hbidps.testbench.Loopback_Max14662_I2C import MAX_LB_CHIPS, LoopbackMax14662I2cInterface
from Hbidps.testbench.VSense_Max14662_I2C import MAX_VS_CHIPS, VSenseMax14662I2cInterface
from Hbidps.Tests.HbidpsTest import HbidpsTest



class BaseTest(HbidpsTest):

    def compare_expected_with_observed(self, expected, observed, error_count, iteration, target_pmbus_device,
                                       rail_under_test):
        if observed != expected:
            error_count.append(
                {'Iteration': iteration, 'Expected': hex(expected), 'Observed': hex(observed), 'Rail': rail_under_test,
                 'Target pmbus device': target_pmbus_device})

    def report_errors(self, device, register_errors, error_message_to_print):
        column_headers = ['Iteration', 'Expected', 'Observed', 'Rail', 'Target pmbus device']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print, table), 'error')


class LcIOTests(BaseTest):
    """
    this test classs tests
    LTC Alert Pin - done
    LTC PBad - not done
    LCM_CTRL - not done
    LCM Gang - not done

    +---------------+-------------------+-----------------------+---------------+
    | Milestone     | TestArea          | TestImplementation    | TestStatus    |
    +===============+===================+=======================+===============+
    | PostPowerOn   | Interface         | Yes                   | Passing       |
    +---------------+-------------------+-----------------------+---------------+
    """
    TEST_ITERATIONS = 100
    ltc_pages = 4
    ltc_devices = 4
    lc_base = 0x20
    limit_tolerance_in_volts = .2

    def Directed_LC_Alert_Test(self):
        """
        Test the alert IO bit from the 2975 and LC_alarm register bits 0-3
        Clear alert bit from LTC2975, verify FPGA sees alert off
        Set alert bit from LTC2975, verify FPGA sees alert on
        Clear alert bit from LTC2975, verify FPGA sees alert off
        """
        hbidps = random.choice(self.env.get_fpgas())
        hbidps.print_slot_under_test()
        # Check that DTB is installed'
        if hbidps.hbidtb:
            self._check_lc_alert(hbidps)
        else:
            self.Log('error', f'{hbidps.name_with_slot()}: Failed due to absent of HBIDTB')

    def _check_lc_alert(self, hbidps):
        lcrail = LCRailTestbench()
        three_volts = 3
        four_volts = 4
        five_volts = 5
        lcrail.init_lc_for_test(hbidps)
        lcrail.set_all_ltc_ov_limits(hbidps, five_volts)
        lcrail.shutdown_lc_rails(hbidps)
        for chip in range(lcrail.ltc_devices):
            page_num = random.choice(range(lcrail.ltc_pages))
            hbidps.clear_alarm_register()
            lcrail.initialize_dps_dtb(hbidps)
            lcrail.init_2975_for_test(hbidps, chip, page_num)
            lcrail.turn_on_and_set_voltage(hbidps, chip, page_num, four_volts)
            if not(self._check_for_no_alert_and_expected_voltage(hbidps, chip * self.ltc_devices + page_num, four_volts)):
                self.Log('error', f'***Test Failed on HBIDPS: {hbidps.name_with_slot()}, Chip:{chip}'
                                 f' Rail:{page_num} due to not turning on output')
            else:
                hbidps.set_ltc2975_ov_limit(chip, page_num, lcrail.ltc2975_float_to_l16(three_volts))
                time.sleep(.1)
                if hbidps.get_lc_rail_alarms().rwc_alert == (2**chip):
                    self.Log('info', f'***Test Passed on HBIDPS: {hbidps.name_with_slot()}, Chip:{chip}'
                                     f' Rail:{page_num} ')
                else:
                    self.Log('error', f'***Test Failed on HBIDPS: {hbidps.name_with_slot()}, Chip:{chip}'
                                     f' Rail:{page_num} due to not setting correct alarm')
                hbidps.set_ltc2975_ov_limit(chip, page_num, lcrail.ltc2975_float_to_l16(five_volts))
            lcrail.shutdown_lc_rails(hbidps)

    def _check_for_no_alert_and_expected_voltage(self, hbidps, rail_under_test, target_voltage):
        hil_voltage = hbidps.get_lc_rail_voltage(rail_under_test)
        for i in range(100):
            if abs(target_voltage - hil_voltage) > self.limit_tolerance_in_volts:
                time.sleep(.001)
                hil_voltage = hbidps.get_lc_rail_voltage(rail_under_test)
            else:
                return True
        self.Log('info', f'Voltage Measure: {hil_voltage} Target: {target_voltage}')
        return False


    def Directed_LTC_CTRL_Test(self):
        """IO Pin to LTC2975 that can turn on and shutoff the LTC if the LTC has it enabled."""
        hbidps = random.choice(self.env.get_fpgas())
        hbidps.print_slot_under_test()
        # Check that DTB is installed'
        if hbidps.hbidtb:
            read_back_errors = self._check_ltc_ctrl(hbidps)
        else:
            self.Log('error', f'{hbidps.name_with_slot()}: Failed due to absent of HBIDTB')
        self._print_control_pin_ltc2975_test_results(hbidps, read_back_errors)

    def _check_ltc_ctrl(self, hbidps):
        error_list = []
        lcrail = LCRailTestbench()
        four_volts = 4
        five_volts = 5
        zero_volts = 0
        lcrail.init_lc_for_test(hbidps)
        lcrail.set_all_ltc_ov_limits(hbidps, five_volts)
        lcrail.shutdown_lc_rails(hbidps)
        for chip in range(lcrail.ltc_devices):
            page_num = random.choice(range(lcrail.ltc_pages))
            rail_under_test = chip * lcrail.ltc_devices + page_num
            hbidps.clear_alarm_register()
            lcrail.initialize_dps_dtb(hbidps)
            lcrail.init_2975_for_test(hbidps, chip, page_num)
            lcrail.enable_ltc_ctrl_pin(hbidps, chip)
            lcrail.turn_on_and_set_voltage(hbidps, chip, page_num, four_volts)
            self._check_vm_result(hbidps, rail_under_test, four_volts, error_list)
            lcrail.set_LTC_control(hbidps, control=15-2**chip)
            self._check_vm_result(hbidps, rail_under_test, zero_volts, error_list)
            lcrail.shutdown_lc_rails(hbidps)
        return error_list

    def _print_control_pin_ltc2975_test_results(self, hbidps, read_back_errors):
        if read_back_errors:
            error_message_to_print = 'LTC 2975 Directed_LTC_CTRL_Test failed '
            self.report_errors(hbidps, read_back_errors, error_message_to_print)
        else:
            hbidps.log_device_message('LTC 2975 Directed_LTC_CTRL_Test successful.', 'info')

    def _check_vm_result(self, hbidps, rail_under_test, target_voltage, error_list):
        hil_voltage = hbidps.get_lc_rail_voltage(rail_under_test)
        for i in range(100):
            if abs(target_voltage - hil_voltage) > self.limit_tolerance_in_volts:
                time.sleep(.01)
                hil_voltage = hbidps.get_lc_rail_voltage(rail_under_test)
            else:
                return
        error_list.append({'Iteration': 1, 'Expected': target_voltage, 'Observed': hil_voltage, 'Rail': rail_under_test, 'Target pmbus device': rail_under_test//4})

    def Directed_LCM_Ganging_SD_Test(self):
        """This pin shuts down the ganging 14662, neeeds to be verified it will do so."""
        hbidps = random.choice(self.env.get_fpgas())
        hbidps.print_slot_under_test()
        lcrail = LCRailTestbench()
        lcrail.setup_delays(hbidps)
        lcrail.init_lc_for_test(hbidps)
        i2c_interfaces = self._create_gang14662_i2C_interfaces(hbidps)
        i2c_interfaces += self._create_loopback_i2C_interfaces(hbidps)
        i2c_interfaces += self._create_vsense_i2C_interfaces(hbidps)
        for i2c_interface in i2c_interfaces:
            self._test_shutdown_pin(hbidps, i2c_interface)

    def _test_shutdown_pin(self, hbidps, i2c_interface):
        test_passed = True
        shutdown_register_orig = hbidps.get_max14662_shutdown_register()
        self._set_shutdown_register(hbidps, i2c_interface, True)
        if not self._read_reg_stress_check(i2c_interface):
            self.Log('info', f'Was not able to read the I2C interface when it should have before')
            test_passed = False
        self._set_shutdown_register(hbidps, i2c_interface, False)
        if self._read_reg_stress_check(i2c_interface):
            self.Log('info', f'Was able to read the I2C interface when it should not have')
            test_passed = False
        self._set_shutdown_register(hbidps, i2c_interface, True)
        if not self._read_reg_stress_check(i2c_interface):
            self.Log('info', f'Was not able to read the I2C interface when it should have after')
            test_passed = False
        hbidps.set_max14662_shutdown_register(shutdown_register_orig)
        if not test_passed:
            self.Log('error', f'***Test Failed on HBIDPS: {hbidps.name_with_slot()}, I2C_Interface:{i2c_interface.interface_name}'
                                 f' for the shutdown Pin not working')

    def _set_shutdown_register(self, hbidps, i2c_interface, turn_on):
        shutdown_register_control = hbidps.get_max14662_shutdown_register()
        if not turn_on:
            set_value = 2 ** (i2c_interface.chip_num//i2c_interface.NUM_CHIPS_PER_SHUTDOWN_CONTROL)
        else:
            set_value = 0
        if i2c_interface.SHUTDOWN_NAME == 'gang':
            shutdown_register_control.rw_st_gang_lb = set_value
        elif i2c_interface.SHUTDOWN_NAME == 'vsense':
            shutdown_register_control.rw_st_vhdd_lb = set_value
        elif i2c_interface.SHUTDOWN_NAME == 'loopback':
            if i2c_interface.chip_num < 2:
                shutdown_register_control.rw_st_vt_lb = set_value
            else:
                shutdown_register_control.rw_st_vm_lb = set_value
        hbidps.set_max14662_shutdown_register(shutdown_register_control)
        time.sleep(.0001)

    def _create_gang14662_i2C_interfaces(self, hbidps):
        i2c_interfaces = []
        for chip_num in range(MAX_GANG_CHIPS):
            i2c_interfaces.append(GangMax14662I2cInterface(hbidps,chip_num))
        return i2c_interfaces

    def _create_loopback_i2C_interfaces(self, hbidps):
        i2c_interfaces = []
        for chip_num in range(MAX_LB_CHIPS):
            i2c_interfaces.append(LoopbackMax14662I2cInterface(hbidps, chip_num))
        return i2c_interfaces

    def _create_vsense_i2C_interfaces(self, hbidps):
        i2c_interfaces = []
        for chip_num in range(MAX_VS_CHIPS):
            i2c_interfaces.append(VSenseMax14662I2cInterface(hbidps, chip_num))
        return i2c_interfaces

    def _read_reg_stress_check(self, i2c_interface):
        num_bytes = 1
        i2c_interface.write(i2c_interface.COMM_STRESS_REGISTER, enable_stop_bit=0)
        i2c_interface.push_data_fifo(i2c_interface.i2c_addr | 1)
        i2c_interface.push_data_fifo(num_bytes)
        i2c_interface.set_transmit_bit(enable_stop_bit=1)
        i2c_interface.wait_i2c_busy()

        num_bytes_read = i2c_interface.read_status_register().receive_fifo_count
        if num_bytes_read != num_bytes:
            read_check = False
        else:
            read_check = True
        return read_check


class HcIOTests(BaseTest):
    """
    this test classs tests
    LTM_Gang_GPIO - not done
    """

    @skip('Not implemented')
    def Directed_LTC_Ganging_GPIO_Test(self):
        """This pin shuts down the ganging via GPIO signals, neeeds to be verified it will do so."""
        pass


class GeneralIO(BaseTest):
    """This class tests all of the IO not part of LC or HC Rails"""

    @skip('Not implemented')
    def Directed_SelfTest_Loads_Test(self):
        """Test the Load 0-15 enables"""

    @skip('Not implemented')
    def Directed_ST_2p0_ld_en_Test(self):
        """Test the Self Test 2.0 Load enable"""

    @skip('Not implemented')
    def Directed_ST_0p5_load_en_Test(self):
        """Test the Self Test 0.5 Load enable"""

    @skip('Not implemented')
    def Directed_ST_HC_Load_enable_Test(self):
        """Test the Self Test High Current Load enable"""

    @skip('Not implemented')
    def Directed_PMIC_reset_IO_Test(self):
        """Test the PMIC Reset"""

    @skip('Not implemented')
    def Directed_VTarg_Short_test_en_Test(self):
        """Test VTarg Short test enable"""

    @skip('Not implemented')
    def Directed_Current_source_en_Test(self):
        """Test the Current source enable"""

