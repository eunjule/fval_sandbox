################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing Aurora Interface (Trigger Bus) between HBIDPS and RCTC"""
import random
import struct
import time
from Hbidps.Tests.HbidpsTest import HbidpsTest

MAX_FAIL_COUNT = 5


class Diagnostics(HbidpsTest):
    """Test DPS to RC and RC to DPS trigger interface"""
    test_iteration_count = 1
    broadcast_iteration_count = 1

    def RandomTriggerUpDpsToRcTest(self):
        """Generate random trigger data and send it from DPS to RC"""
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            is_link_up = hbidps.initialize_trigger_link()
            if is_link_up:
                hbidps.clear_alarm_register()
                for trigger_count in range(self.test_iteration_count):
                    random_trigger_from_dps = self.generate_dps_type_trigger(hbidps)
                    fail_count, pass_count = self.send_trigger_from_dps_verify_at_rc(random_trigger_from_dps, hbidps,fail_count, pass_count,trigger_count)
                    if fail_count == MAX_FAIL_COUNT:
                        break
            if pass_count == self.test_iteration_count:
                self.Log('info', 'DPS to RC {:,} Triggers sent Successfully'.format(pass_count))
            else:
                self.Log('error', 'DPS to RC Only {} Triggers sent successfully out of {:,}'.format(pass_count,self.test_iteration_count))

    def RandomTriggerDownRcToDpsTest(self):
        """Generate random trigger data and send it from RC to DPS"""
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            is_link_up = hbidps.initialize_trigger_link()
            if is_link_up:
                hbidps.clear_alarm_register()
                for trigger_count in range(self.test_iteration_count):
                    random_trigger_from_rc = self.generate_dps_type_trigger(hbidps)
                    fail_count,pass_count = self.send_trigger_from_rc_verify_at_dps(random_trigger_from_rc, hbidps,fail_count, pass_count,trigger_count)
                    if fail_count == MAX_FAIL_COUNT:
                        break
                if pass_count == self.test_iteration_count:
                    self.Log('info', 'RC to DPS {:,} Triggers sent successfully'.format(pass_count))
                else:
                    self.Log('error', 'RC to DPS Only {} Triggers sent successfully out of {:,}'.format(pass_count,
                                                                                                       self.test_iteration_count))

    def RandomTriggerDpsToDpsTest(self):
        """Generate random trigger data and send it from DPS to DPS"""
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            is_link_up = hbidps.initialize_trigger_link()
            if is_link_up:
                hbidps.clear_alarm_register()
                for trigger_count in range(self.broadcast_iteration_count):
                    random_trigger_from_dps = self.generate_dps_type_trigger(hbidps)
                    fail_count, pass_count,total_dps_available = self.send_trigger_from_dps_verify_at_dps(random_trigger_from_dps, hbidps,
                                                                                     fail_count, pass_count,trigger_count)
                    if fail_count == MAX_FAIL_COUNT:
                        break
                if (pass_count/total_dps_available) == self.broadcast_iteration_count:
                    self.Log('info', 'DPS to DPS {:,} Triggers sent successfully'.format(pass_count))
                else:
                    self.Log('error', 'DPS to DPS Only {} Triggers sent successfully out of {:,}'.format(pass_count,
                                                                                                      self.broadcast_iteration_count))

    def send_trigger_from_dps_verify_at_dps(self, trigger_from_dps, hbidps, fail_count, pass_count,trigger_count):
        hbidps.write_bar_register(hbidps.registers.SEND_TRIGGER(value=trigger_from_dps))
        trigger_at_rc = hbidps.get_rc_trigger(trigger_from_dps)
        if trigger_at_rc != trigger_from_dps:
            self.Log('error','RC didn\'t received correct trigger. Expected {} received {} '.format(trigger_from_dps,trigger_at_rc))
        trigger_at_dps = hbidps.get_triggr_at_all_dps_instruments(trigger_from_dps)
        for slots in trigger_at_dps:
            if trigger_at_dps[slots] != trigger_from_dps:
                self.Log('error',
                         'DPS received incorrect trigger: Received :0x{:x} Expected :0x{:x} at slot {} at count {}'.format(trigger_at_dps[slots],
                                                                                                   trigger_from_dps,slots,trigger_count))
                fail_count += 1
                aurora_error_count, aurora_status, slot = hbidps.get_aurora_status(slots)
                self.Log('info', 'dps aurora_status: 0x{:x} aurora_error_count: 0x{:x} slot {}'.format(aurora_status.value,
                                                                                   aurora_error_count.value,slots))
                hbidps.get_rctc_aurora_status()
                break
            else:
                self.Log('debug', 'DPS received trigger: 0x{:x} at slot {}'.format(trigger_at_dps[slots],slots))
                pass_count += 1
        return fail_count, pass_count, len(trigger_at_dps)

    def send_trigger_from_dps_verify_at_rc(self, trigger_from_dps, hbidps,fail_count, pass_count,trigger_count):
        hbidps.write_bar_register(hbidps.registers.SEND_TRIGGER(value=trigger_from_dps))
        trigger_at_rc = hbidps.get_rc_trigger(trigger_from_dps)
        if trigger_at_rc != trigger_from_dps:
            aurora_error_count, aurora_status, slot = hbidps.get_aurora_status()
            self.Log('error',
                     'RC received incorrect trigger: Received :0x{:x} Expected :0x{:x} from slot: {} at count {}'.format(trigger_at_rc,trigger_from_dps,slot,trigger_count))
            fail_count += 1
            self.Log('info', 'dps aurora_status: 0x{:x} aurora_error_count: 0x{:x} slot: {}'.format(aurora_status.value,
                                                                                                   aurora_error_count.value,slot))
            hbidps.get_rctc_aurora_status()
        else:
            self.Log('debug', 'RC received trigger: 0x{:x}'.format(trigger_at_rc))
            pass_count += 1
        return fail_count, pass_count

    def send_trigger_from_rc_verify_at_dps(self, trigger_from_rc, hbidps,fail_count, pass_count,trigger_count):
        hbidps.send_rc_trigger(trigger_from_rc)
        trigger_at_dps = hbidps.read_dps_trigger(trigger_from_rc)
        if trigger_at_dps.value != trigger_from_rc:
            aurora_error_count, aurora_status, slot = hbidps.get_aurora_status()
            self.Log('error',
                     'DPS received incorrect trigger Received :0x{:x} Expected :0x{:x} to slot {} at count {}'.format(trigger_at_dps.value,
                                                                                       trigger_from_rc,slot,trigger_count))
            self.Log('info', 'dps aurora_status: 0x{:x} aurora_error_count: 0x{:x} slot: {}'.format(aurora_status.value,
                                                                                                   aurora_error_count.value,slot))
            hbidps.get_rctc_aurora_status()
            fail_count += 1
        else:
            self.Log('debug', 'DPS received correct trigger: 0x{:x}'.format(trigger_at_dps.value))
            pass_count += 1
        return fail_count,pass_count

    def generate_dps_type_trigger(self,hbidps):
        random_trigger = random.getrandbits(32)
        random_dps_trigger = hbidps.registers.SEND_TRIGGER()
        random_dps_trigger.value = random_trigger
        random_dps_trigger.card_type = 0x01
        random_dps_trigger.sw_type_trigger = 0
        return random_dps_trigger.value