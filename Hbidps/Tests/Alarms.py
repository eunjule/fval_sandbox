################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import time
import random

from Common.fval import skip
from Hbidps.instrument.AlarmSystem import AlarmHelper
from Hbidps.instrument import hbidps_register
from Hbidps.instrument.HCRailPMBUS import HCRailPMBUS
from Hbidps.instrument.RailTransitionControl import RailTransitionControl
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.instrument.TQMachine import TriggerQueueDDR
from Hbidps.instrument.MdutConfiguration import MdutConfiguration
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands
from Hbidps.Tests.HbidpsTest import HbidpsTest

HC_TQ_RAIL_ID_BASE = 0x30
HC_TQ_FDT = 0x1000
HC_TQ_I_HI = 10
LC_TQ_RAIL_ID_BASE = 0x20
LC_TQ_I_HI = 2

class Functional(HbidpsTest):
    """Check alarm status, setting condition and reset

    In all following alarm test, make sure to check/reset all global alarm bits
    before and after the test.
    """

    def setUp(self):
        super().setUp()
        random_slot = random.randint(0, 15)
        self.hbidps = self.hbidps_list[random_slot]
        self.hc_rail = random.randint(0, 9)
        self.lc_rail = random.randint(0, 15)

    def DirectedHCRailCFoldAlarmTest(self):
        """ Validate the hc_cfold bit of the HC Rail Alarms.

            Test Steps:
                1. Set the channel transition (OFF to OFF/ON/ZVF) for the HC Rails.
                2. Set the payload (OFF state) of the apply user attributes command to be same
                   as the channel transition by the software.
                3. Non zero payload of apply user attributes command indicates that the software
                   is trying to block particular channel transition.
                4. Validate the hc_cfold bit of the HC Rail Alarm.
                5. If you are trying to block a state which is not the destination state, no alarm
                   should be raised.
                6. Reset the alarm bits.
        """
        self.check_hc_cfold_for_off_to_off_block_off_transition()
        self.check_hc_cfold_for_off_to_zvf_block_zvf_transition()
        self.check_hc_cfold_for_off_to_on_block_on_transition()
        self.check_hc_cfold_for_on_to_on_block_on_transition()
        self.check_hc_cfold_for_off_to_on_block_zvf_transition()

    def DirectedLCRailCFoldAlarmTest(self):
        """ Validate the lc_cfold bit of the LC Rail Alarms.

            Test Steps:
                1. Set the channel transition (OFF to OFF/ON/ZVF) for the LC Rails.
                2. Set the payload (OFF state) of the apply user attributes command to be same
                   as the channel transition by the software.
                3. Non zero payload of apply user attributes command indicates that the software
                   is trying to block particular channel transition.
                4. Validate the lc_cfold bit of the LC Rail Alarm.
                5. Reset the alarm bits.
        """
        self.check_lc_cfold_for_off_to_off_block_off_transition()
        self.check_lc_cfold_for_off_to_zvf_block_zvf_transition()
        self.check_lc_cfold_for_off_to_on_block_on_transition()
        self.check_lc_cfold_for_on_to_on_block_on_transition()
        self.check_lc_cfold_for_off_to_on_block_zvf_transition()

    def DirectedHCRailAlertAlarmTest(self):
        """ Validate the alert bit of the HC Rail Alarms.

            Test Steps:
                1. In the channel transition (OFF to ON), assert the hardware alert line.
                2. Set the over voltage limit to be less than the voltage.
                3. A HC alert alarm should be raised.
        """
        self.general_hc_1rail_off_to_on_flow_alert_alarm(vout=2, rail=self.hc_rail, hbidps=self.hbidps)

    def DirectedLCRailAlertAlarmTest(self):
        """ Validate the alert bit of the LC Rail Alarms.

            Test Steps:
                1. In the channel transition (OFF to ON), assert the hardware alert line.
                2. Set the over voltage limit to be less than the voltage.
                3. A LC alert alarm should be raised.
        """
        self.general_lc_1rail_off_to_on_flow_alert_alarm(vout=2, rail=self.lc_rail, hbidps=self.hbidps)

    def DirectedHCRailStatusAlarmTest(self):
        """ Validate the status bits of the HC Rail Alarms.

            Test Steps:
                1. Unmask the status_word_off bit of the PMBUS status register by unmasking bit
                   23 of LC_HC_STATUS_ALARM_MASK register.
                2. The FPGA polls the LTC2975 every 190ms. After unmasking the above bit, check for
                   the HC Rail Alarm. The status bit of HC rail alarm should be high.
        """
        self.Log('info', f'STATUS REGISTER BEFORE: {hex(self.hbidps.get_status_alarm_mask().value)}')
        self.unmask_hc_status_bits()
        self.Log('info', f'STATUS REGISTER AFTER: {hex(self.hbidps.get_status_alarm_mask().value)}')
        time.sleep(0.2)
        self.check_hc_status_alarm(self.hbidps)
        self.mask_all_hc_lc_status_bits()

    def DirectedLCRailStatusAlarmTest(self):
        """ Validate the status bits of the LC Rail Alarms.

            Test Steps:
                1. Unmask the status_word_off bit of the PMBUS status register by unmasking bit
                   7 of LC_HC_STATUS_ALARM_MASK register.
                2. The FPGA polls the PMBUS status register every 190ms. After unmasking the above bit,
                    check for the LC Rail Alarm. The status bit of LC rail alarm should be high.
        """
        self.Log('info', f'STATUS REGISTER BEFORE: {hex(self.hbidps.get_status_alarm_mask().value)}')
        self.unmask_lc_status_bits()
        self.Log('info', f'STATUS REGISTER AFTER: {hex(self.hbidps.get_status_alarm_mask().value)}')
        time.sleep(0.2)
        self.check_lc_status_alarm(self.hbidps)
        self.mask_all_hc_lc_status_bits()

    def DirectedHCRailPowerOffDelayAlarmTest(self):
        """ Validate the pwr_off_delay bit of the HC Rail Alarms.

            Test Steps:
                1. In the channel transition from OFF to ON, it is not safe to turn ON the channel
                   before waiting for certain amount of delay.
                2. Write the maximum delay possible in the HC DELAY ALARM REGISTER (0x7FFFFFFF ms).
                3. In the OFF to ON transition, the rails do not wait for this amount of time
                   before they turn ON.
                4. An alarm is expected to be raised in the HC ALARM REGISTER.
                5. Validate the alarm in HC ALARM REGISTER and HC POWER OFF DELAY ALARM register.
                6. Reset all the Alarms.
        """
        self.general_hc_1rail_off_to_on_flow_pwr_off_delay(vout=2, rail=self.hc_rail, hbidps=self.hbidps, state_block=0)

    def DirectedLCRailPowerOffDelayAlarmTest(self):
        """ Validate the pwr_off_delay bit of the LC Rail Alarms.

            Test Steps:
                1. In the channel transition from OFF to ON, it is not safe to turn ON the channel
                   before waiting for certain amount of delay.
                2. Write the maximum delay possible in the LC DELAY ALARM REGISTER (0x7FFFFFFF ms).
                3. In the OFF to ON transition, the rails do not wait for this amount of time
                   before they turn ON.
                4. An alarm is expected to be raised in the LC ALARM REGISTER.
                5. Validate the alarm in LC ALARM REGISTER and LC POWER OFF DELAY ALARM register.
                6. Reset all the Alarms.
        """
        self.general_lc_1rail_off_to_on_flow_power_off_delay(vout=2, rail=self.lc_rail, hbidps=self.hbidps, state_block=0)

    def DirectedHCRailTempAlarmTest(self):
        """ Validate the hc_temp bit of the HC Rail Alarms.

            Test Steps:
                1. Clear if there is any existing alarm already present.
                2. Change the upper threshold limit for the HC rails.
                3. Wait for 190ms x 5, since the fpga polls 190ms/sample to get median sample.
                4. Check for the HC Temp Alarm Register.
                5. Validate the HC ALARMS register also.
                6. Change the upper threshold to default and reset the alarm.
                7. Repeat th same process for the lower threshold limit.
        """
        with RailsAlarmEnv(self.hbidps):
            self.validate_upper_threshold_limit_hc_rails(self.hbidps)
            self.validate_lower_threshold_limit_hc_rails(self.hbidps)

    def DirectedLCRailTempAlarmTest(self):
        """ Validate the lc_temp bit of the LC Rail Alarms.

            Test Steps:
                1. Clear if there is any existing alarm already present.
                2. Change the upper threshold limit for the LC rails.
                3. Wait for 190ms x 5, since the fpga polls 190ms/sample to get median sample.
                4. Check for the LC Temp Alarm Register.
                5. Validate the LC ALARMS register also.
                6. Change the upper threshold to default and reset the alarm.
                7. Repeat th same process for the lower threshold limit.
        """
        with RailsAlarmEnv(self.hbidps):
            self.validate_upper_threshold_limit_lc_rails(self.hbidps)
            self.validate_lower_threshold_limit_lc_rails(self.hbidps)

    def DirectedHCRailiRangeCheckFailAlarmTest(self):
        """ Validate the irange_check_fail bit of the HC Rail Alarms.

            Test Steps:
                1. In the Vbump up/down test, switch the irange (from 0 to 1 or vice versa) when
                   the channel transitions from one rail to another.
                2. If the irange is different when the voltage transition takes place, hc rail
                   alarm should be set.
        """
        self.general_hc_1rail_on_to_on_flow_irange_check_fail(vout=2, rail=self.hc_rail, hbidps=self.hbidps, vout_initial=1)

    def DirectedLCRailiRangeCheckFailAlarmTest(self):
        """ Validate the irange_check_fail bit of the LC Rail Alarms.

            Test Steps:
                1. In the Vbump up/down test, switch the irange (from 0 to 1 or vice versa) when
                   the channel transitions from one rail to another.
                2. If the irange is different when the voltage transition takes place, hc rail
                   alarm should be set.
        """
        self.general_lc_1rail_on_to_on_flow_irange_check_fail(vout_initial=1, vout=2, rail=self.lc_rail, hbidps=self.hbidps)

    def DirectedHCRailVRangeSelfTest(self):
        """ Validate the vrange_self bit of the HC VRange Alarms.

            Test Steps:
                1. In the Vbump up/down test, switch the vrange (from a list of valid vrange values) when
                   the channel transitions voltage for the same rail.
                2. If the vrange is different when the voltage transition takes place, hc rail
                   alarm should be set.
        """
        self.general_hc_1rail_on_to_on_flow_vrange_self_flow(vout=2, rail=self.hc_rail, hbidps=self.hbidps, vout_initial=1)

    def DirectedHCRailVRangeNeighborTest(self):
        """ Validate the vrange_neighbor bit of the HC VRange Alarms.

            Test Steps:
                1. In the OFF to ON test, switch the vrange (from a list of valid vrange values) for a
                   rail when the neighbouring rail in the same LTM4688 was ON.
                2. Select a rail pair in the same LTM4688 like (0,1), (2,3) .. (8,9).
                3. From the above rail pairs, turn one rail from the OFF state to the ON state with one
                   vrange value and turn the other rail with different vrange value.
                4. If the vrange is different as compared to the neighbouring rail, hc rail
                   alarm should be set.
        """
        self.general_hc_1rail_on_to_on_flow_vrange_neighbor_flow(vout=2, rail=self.hc_rail, hbidps=self.hbidps, vout_initial=1)

    def DirectedInvalidGndCtrlTQCmdAlarmAllDpsTest(self):
        """Verify global alarm register for All DPS:

        1.Set bar1_GROUND_CONTROL_MODE register to pcie
        and set voltage to ground using T1/T2;
        check the global alarm bit 'invalid_gnd_ctrl_tq_cmd'
        2.Reset should clear the individual global alarm bit
        """
        for self.hbidps in self.env.get_fpgas():
            self.DirectedInvalidGndCtrlTQCmdAlarmOneDpsTest(self.hbidps)

    def DirectedInvalidGndCtrlTQCmdAlarmOneDpsTest(self, hbidps=None):
        """Verify global alarm register for One DPS:

        1.Set bar1_GROUND_CONTROL_MODE register to pcie
        and set voltage to ground using T1/T2;
        check the global alarm bit 'invalid_gnd_ctrl_tq_cmd'
        2.Reset should clear the individual global alarm bit
        """

        self.create_object_interface()
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        self.check_invalid_gnd_ctrl_tq_cmd_bit(0)
        self.set_ground_control_mode("pcie_mode")

        self.setup_hc_rail_config()
        tq_list = self.create_tq_commands_invalid_gnd_ctrl()
        self.send_trigger_queue(tq_list)
        self.check_invalid_gnd_ctrl_tq_cmd_bit(1)

        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        self.check_invalid_gnd_ctrl_tq_cmd_bit(0)
        self.set_ground_control_mode("trigger_queue_mode")

        # Reset the HC rails back to the safe state - Method pending from jiabo

    def DirectedInvalidTQCmdAlarmAllDpsTest(self):
        """Verify global alarm register for All DPS:

        1.Set TQ command with bits [62:56] set to 0x00;
        check the global alarm bit 'invalid_tq_cmd'
        2.Reset should clear the individual global alarm bit
        """
        for self.hbidps in self.env.get_fpgas():
            self.DirectedInvalidTQCmdAlarmOneDpsTest(self.hbidps)

    def DirectedInvalidTQCmdAlarmOneDpsTest(self, hbidps=None):
        """Verify global alarm register for One DPS:

        1.Set TQ command with bits [62:56] set to 0x00;
        check the global alarm bit 'invalid_tq_cmd'
        2.Reset should clear the individual global alarm bit
        """

        self.create_object_interface()
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        self.setup_hc_rail_config()
        self.check_invalid_tq_cmd_bit(0)

        tq_list = self.create_invalid_tq_command_ddr()
        self.send_trigger_queue(tq_list)
        self.hc_rail.read_ltm4680_info_set(0, 0)
        self.check_invalid_tq_cmd_bit(1)

        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        self.check_invalid_tq_cmd_bit(0)

    def DirectedTempAlarmTest(self):
        """Verify global alarm register and TEMP_ALARM for:

        1.Set temperature limit to make temp out of margin;
        Check the temp alarm register and global alarm
        2.Reset should clear the individual alarm bit and the global alarm
        """
        for self.hbidps in self.env.get_fpgas():
            self.alarm_helper = AlarmHelper(self.hbidps)
            self.hbidps.print_slot_under_test()
            self.alarm_helper.detect_global_alarm_status_one_dps()
            for max6628_device in range(4):
                self.check_alarm_if_temp_within_limit(max6628_device)
                self.check_alarm_if_temp_under_limit(max6628_device)
                self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
                self.check_alarm_if_temp_over_limit(max6628_device)
                self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()

    def DirectedBulkCurrentAlarmTest(self):
        """Verify global alarm register and SYS_CURRENT_ALARM for:

        1.Set current limit to make current out of margin;
        Check the current alarm register and global alarm
        2.Reset should clear the individual alarm bit and the global alarm
        """
        self.filter_time = 10

        for self.hbidps in self.env.get_fpgas():
            self.hbidps.print_slot_under_test()
            self.alarm_helper = AlarmHelper(self.hbidps)
            self.alarm_helper.detect_global_alarm_status_one_dps()
            self.alarm_helper.initialize_alarm_settings()
            self.check_alarm_if_curr_within_limit()
            self.check_alarm_if_curr_under_limit()
            self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
            self.check_alarm_if_curr_over_limit()
            self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
            self.alarm_helper.reset_initialize_alarm_settings()

    def validate_upper_threshold_limit_hc_rails(self, hbidps):
        hbidps.set_hc_upper_temp_limit(0)
        time.sleep(1)
        self.check_hc_temp_alarm(hbidps, 1)
        self.check_hc_rail_temp_alarm(hbidps, 1)
        hbidps.reset_global_alarm()
        reset_val = AlarmHelper.encode_float_to_single_precision_floating_format(50)
        hbidps.set_hc_upper_temp_limit(reset_val)

    def validate_lower_threshold_limit_hc_rails(self, hbidps):
        reset_val = AlarmHelper.encode_float_to_single_precision_floating_format(50)
        hbidps.set_hc_lower_temp_limit(reset_val)
        time.sleep(1)
        self.check_hc_temp_alarm(hbidps, 1)
        self.check_hc_rail_temp_alarm(hbidps, 1)
        hbidps.reset_global_alarm()
        reset_val = AlarmHelper.encode_float_to_single_precision_floating_format(-10)
        hbidps.set_hc_lower_temp_limit(reset_val)

    def validate_upper_threshold_limit_lc_rails(self, hbidps):
        hbidps.set_lc_upper_temp_limit(0)
        time.sleep(1)
        self.check_lc_temp_alarm(hbidps, 1)
        self.check_lc_rail_temp_alarm(hbidps, 1)
        hbidps.reset_global_alarm()
        reset_val = AlarmHelper.encode_float_to_single_precision_floating_format(50)
        hbidps.set_lc_upper_temp_limit(reset_val)

    def validate_lower_threshold_limit_lc_rails(self, hbidps):
        reset_val = AlarmHelper.encode_float_to_single_precision_floating_format(50)
        hbidps.set_lc_lower_temp_limit(reset_val)
        time.sleep(1)
        self.check_lc_temp_alarm(hbidps, 1)
        self.check_lc_rail_temp_alarm(hbidps, 1)
        hbidps.reset_global_alarm()
        reset_val = AlarmHelper.encode_float_to_single_precision_floating_format(-10)
        hbidps.set_lc_lower_temp_limit(reset_val)

    def unmask_lc_status_bits(self):
        self.hbidps.set_status_alarm_mask(hc_mask=0xFFFF, lc_mask=0xFFBF)

    def mask_all_hc_lc_status_bits(self):
        self.hbidps.set_status_alarm_mask(hc_mask=0xFFFF, lc_mask=0xFFFF)

    def unmask_hc_status_bits(self):
        self.hbidps.set_status_alarm_mask(hc_mask=0xFFBF, lc_mask=0xFFFF)

    def check_hc_cfold_for_off_to_off_block_off_transition(self):
        self.general_hc_1rail_off_to_on_off_zvf_flow_cfold(vout=None, rail=self.hc_rail, hbidps=self.hbidps, state_block=0xFFF, alarm_expected=1)

    def check_hc_cfold_for_off_to_zvf_block_zvf_transition(self):
        self.general_hc_1rail_off_to_on_off_zvf_flow_cfold(vout=0, rail=self.hc_rail, hbidps=self.hbidps, state_block=0xFFF000000, alarm_expected=1)

    def check_hc_cfold_for_off_to_on_block_on_transition(self):
        self.general_hc_1rail_off_to_on_off_zvf_flow_cfold(vout=2, rail=self.hc_rail, hbidps=self.hbidps, state_block=0xFFF000, alarm_expected=1)

    def check_hc_cfold_for_on_to_on_block_on_transition(self):
        self.general_hc_1rail_on_to_on_flow_cfold(vout=2, rail=self.hc_rail, hbidps=self.hbidps, vout_initial=1, state_block=0xFFF000, alarm_expected=1)

    def check_hc_cfold_for_off_to_on_block_zvf_transition(self):
        self.general_hc_1rail_off_to_on_off_zvf_flow_cfold(vout=2, rail=self.hc_rail, hbidps=self.hbidps, state_block=0xFFF000000, alarm_expected=0)

    def check_lc_cfold_for_off_to_off_block_off_transition(self):
        self.general_lc_1rail_off_to_on_off_zvf_flow_cfold(vout=None, rail=self.lc_rail, hbidps=self.hbidps, state_block=0xFFF, alarm_expected=1)

    def check_lc_cfold_for_off_to_zvf_block_zvf_transition(self):
        self.general_lc_1rail_off_to_on_off_zvf_flow_cfold(vout=0, rail=self.lc_rail, hbidps=self.hbidps, state_block=0xFFF000000, alarm_expected=1)

    def check_lc_cfold_for_off_to_on_block_on_transition(self):
        self.general_lc_1rail_off_to_on_off_zvf_flow_cfold(vout=2, rail=self.lc_rail, hbidps=self.hbidps, state_block=0xFFF000, alarm_expected=1)

    def check_lc_cfold_for_on_to_on_block_on_transition(self):
        self.general_lc_1rail_on_to_on_flow_cfold(vout_initial=1, vout=2, rail=self.lc_rail, hbidps=self.hbidps, state_block=0xFFF000, alarm_expected=1)

    def check_lc_cfold_for_off_to_on_block_zvf_transition(self):
        self.general_lc_1rail_off_to_on_off_zvf_flow_cfold(vout=2, rail=self.lc_rail, hbidps=self.hbidps, state_block=0xFFF000000, alarm_expected=0)

    def general_hc_1rail_on_to_on_flow_irange_check_fail(self, vout, rail=None, hbidps=None, vout_initial=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            if vout_initial != None :
                self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout_initial, irange=1, vrange=0xFABC)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout, irange=0, vrange=0xFABC)
            self.check_hc_irange_check_fail_alarms(hbidps)
            self.check_hc_rail_irange_check_fail_alarms(hbidps)

    def general_hc_1rail_on_to_on_flow_cfold(self, vout, rail=None, hbidps=None, vout_initial=None, state_block=0, alarm_expected=0):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            if vout_initial != None :
                self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout_initial, irange=0, vrange=0xFABC, state_block=state_block)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout, irange=0, vrange=0xFABC, state_block=state_block)
            self.check_hc_rail_cfold_alarms(hbidps, alarm_expected)
            self.check_hc_cfold_alarms(hbidps, rail, alarm_expected)

    def general_hc_1rail_off_to_on_off_zvf_flow_cfold(self, vout, rail=None, hbidps=None, state_block=0, alarm_expected=0):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            hbidps.enable_hc_run_pins()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_off_to_on_tq_list(rail=rail, vout=vout, irange=0, vrange=0xFABC, state_block=state_block)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.check_hc_rail_cfold_alarms(hbidps, alarm_expected)
            self.check_hc_cfold_alarms(hbidps, rail, alarm_expected)

    def general_hc_1rail_off_to_on_flow_alert_alarm(self, vout, rail=None, hbidps=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            hbidps.enable_hc_run_pins()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_off_to_on_tq_list_alert_alarm(rail=rail, vout=vout)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.check_hc_rail_alert_alarm(hbidps)

    def general_hc_1rail_off_to_on_flow_pwr_off_delay(self, vout, rail=None, hbidps=None, state_block=0):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            hbidps.enable_hc_run_pins()
            hbidps.set_hc_channel_power_off_delay(0x7FFFFFFF)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_off_to_on_tq_list(rail=rail, vout=vout, irange=0, vrange=0xFABC, state_block=state_block)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.check_hc_power_off_delay_alarm(hbidps, rail)
            self.check_hc_rail_power_off_delay_alarm(hbidps)
            hbidps.set_hc_channel_power_off_delay(160000)

    def general_lc_1rail_off_to_on_off_zvf_flow_cfold(self, vout, rail=None, hbidps=None, state_block=0, alarm_expected=0):
        """ Set one LC rail from float mode to >0.5V force mode """
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        irange=0
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            self.execute_one_tq_transition(hbidps, rail, None, vout, irange, state_block)
            self.check_lc_rail_cfold_alarms(hbidps, alarm_expected)
            self.check_lc_cfold_alarms(hbidps, rail, alarm_expected)

    def general_lc_1rail_off_to_on_flow_alert_alarm(self, vout, rail=None, hbidps=None):
        """ Set one LC rail from float mode to >0.5V force mode """
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            self.execute_one_tq_transition_alert_alarm(hbidps, rail, None, vout)

    def general_lc_1rail_off_to_on_flow_power_off_delay(self, vout, rail=None, hbidps=None, state_block=0):
        """ Set one LC rail from float mode to >0.5V force mode """
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        irange=0
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            hbidps.set_lc_channel_power_off_delay(0x7FFFFFFF)
            self.execute_one_tq_transition(hbidps, rail, None, vout, irange, state_block)
            self.check_lc_power_off_delay_alarm(hbidps, rail)
            self.check_lc_rail_power_off_delay_alarm(hbidps)
            hbidps.set_lc_channel_power_off_delay(100000)

    def general_hc_1rail_on_to_on_flow_vrange_self_flow(self, vout, rail=None, hbidps=None, vout_initial=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            if vout_initial != None :
                self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout_initial, vrange=0xFABC)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout, vrange=0xFB52)
            self.check_hc_vrange_self_alarms(hbidps, rail)
            self.check_hc_rail_vrange_alarms(hbidps)

    def general_hc_1rail_on_to_on_flow_vrange_neighbor_flow(self, vout, rail=None, hbidps=None, vout_initial=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail%2 == 0:
            neighbor_rail = rail
            current_rail = rail + 1
        else:
            neighbor_rail = rail - 1
            current_rail = rail
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            if vout_initial != None :
                self.hc_1rail_arbitrary_initial_state_test(hbidps, neighbor_rail, vout_initial, vrange=0xFABC)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, current_rail, vout, vrange=0xFB52)
            self.check_hc_vrange_neighbor_alarms(hbidps, neighbor_rail)
            self.check_hc_rail_vrange_alarms(hbidps)

    def general_lc_1rail_on_to_on_flow_irange_check_fail(self, vout_initial, vout, hbidps=None, rail=None):
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        self.Log('info', f'{"*" * 10}Testing lc rail {rail}; hbidps{hbidps.slot}; vout = {vout}V{"*" * 10}')
        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            self.execute_one_tq_transition(hbidps, rail, None, vout_initial, irange=1, state_block=0)
            self.execute_one_tq_transition(hbidps, rail, vout_initial, vout, irange=0, state_block=0)
            self.check_lc_irange_check_fail_alarms(hbidps)
            self.check_lc_rail_irange_check_fail_alarms(hbidps)

    def general_lc_1rail_on_to_on_flow_cfold(self, vout_initial, vout, hbidps=None, rail=None, state_block=0, alarm_expected=0):
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        self.Log('info', f'{"*" * 10}Testing lc rail {rail}; hbidps{hbidps.slot}; vout = {vout}V{"*" * 10}')
        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            self.execute_one_tq_transition(hbidps, rail, None, vout_initial, irange=0, state_block=0)
            self.execute_one_tq_transition(hbidps, rail, vout_initial, vout, irange=0, state_block=state_block)
            self.check_lc_rail_cfold_alarms(hbidps, alarm_expected)
            self.check_lc_cfold_alarms(hbidps, rail, alarm_expected)

    def hc_1rail_arbitrary_initial_state_test(self, hbidps, rail, vout, irange=0, vrange=0xFABC, state_block=0):
        hbidps.enable_hc_run_pins()
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_hc_1rail_off_to_on_tq_list(rail=rail, vout=vout, irange=irange, vrange=vrange, state_block=state_block)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
        print(time_delta)
        time.sleep(0.5)

    def execute_one_tq_transition(self, hbidps, rail, vout_initial, vout, irange, state_block):
        self.Log('info', f'execute transition: vout_initial = {vout_initial}V; vout = {vout}V')
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_lc_1rail_tq_list(rail=rail, vout=vout, irange=irange, state_block=state_block)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        observed_power_state = (hbidps.get_lc_power_state().value >> (rail * 2)) & 0b11
        _, initial_pwr_state, _, _, _ = self.get_vout_settings_and_power_state(vout_initial)
        if observed_power_state != initial_pwr_state:
            self.Log('error', f'lc rail {rail} initial power state{bin(observed_power_state)}'
                              f'!= expected: {bin(initial_pwr_state)}')
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)

    def execute_one_tq_transition_alert_alarm(self, hbidps, rail, vout_initial, vout):
        self.Log('info', f'execute transition: vout_initial = {vout_initial}V; vout = {vout}V')
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_lc_1rail_tq_list_alert_alarm(rail=rail, vout=vout)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        observed_power_state = (hbidps.get_lc_power_state().value >> (rail * 2)) & 0b11
        _, initial_pwr_state, _, _, _ = self.get_vout_settings_and_power_state(vout_initial)
        if observed_power_state != initial_pwr_state:
            self.Log('error', f'lc rail {rail} initial power state{bin(observed_power_state)}'
                              f'!= expected: {bin(initial_pwr_state)}')
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
        self.check_lc_rail_alert_alarm(hbidps)

    def create_hc_1rail_off_to_on_tq_list(self, rail, vout, irange, vrange, state_block=0):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <=9:
            hc_one_hot = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
        if vout == None:
            vout_on = 0
            pwr_state = 0b00
            ov = uv = 0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            ov = uv = 0
        elif 0.5 <= vout <= 3:
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.5
        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (vrange+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.set_irange, hc_rail, irange)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, state_block)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_hc_1rail_off_to_on_tq_list_alert_alarm(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <=9:
            hc_one_hot = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)

        vout_on = vout
        pwr_state = 0b01
        ov = vout - 1
        uv = 0.5
        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.set_irange, hc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_lc_1rail_tq_list(self, rail, vout, irange, state_block):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <= 15:
            lc_one_hot = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
        else:
            lc_one_hot = 0
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE)
            self.Log('error', f'rail number {lc_rail} given to lc trigger queue is not valid')
        ov, pwr_state, uv, vout_on, oc = self.get_vout_settings_and_power_state(vout)

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)
        self.Log('info', f'ov: {ov}, uv: {uv}, vout: {vout_on}, fdi_hi: {LC_TQ_I_HI}, i_hi_hex: {oc}, pwr_state: {pwr_state}')

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, 0x01000)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.set_irange, lc_rail, irange)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, state_block)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_lc_1rail_tq_list_alert_alarm(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <= 15:
            lc_one_hot = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)

        vout_on = vout
        pwr_state = 0b01
        ov = vout - 1
        uv = 3.0
        oc = 3.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)
        self.Log('info', f'ov: {ov}, uv: {uv}, vout: {vout_on}, fdi_hi: {LC_TQ_I_HI}, i_hi_hex: {oc}, pwr_state: {pwr_state}')

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, 0x01000)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.set_irange, lc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    # alarm interface API START
    def check_hc_temp_alarm(self, hbidps, expected_value):
        hc_temp_alarm = hbidps.get_hc_temp_alarms().value
        if not expected_value:
            if hc_temp_alarm != 0:
                self.Log('error', f'HC Temp Alarm not set correctly.')
        else:
            if hc_temp_alarm == 0:
                self.Log('error', f'HC Temp Alarm not set correctly.')

    def check_hc_rail_temp_alarm(self, hbidps, expected_value):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.r_hc_temp != expected_value:
            self.Log('error', f'HC Rail Temperature Alarm not set correctly.')

    def check_lc_temp_alarm(self, hbidps, expected_value):
        lc_temp_alarm = hbidps.get_lc_temp_alarms().value
        if not expected_value:
            if lc_temp_alarm != 0:
                self.Log('error', f'LC Temperature Alarm not set correctly.')
        else:
            if lc_temp_alarm == 0:
                self.Log('error', f'LC Temperature Alarm not set correctly.')

    def check_lc_rail_temp_alarm(self, hbidps, expected_value):
        lc_rail_alarm = hbidps.get_lc_rail_alarms()
        if lc_rail_alarm.r_lc_temp != expected_value:
            self.Log('error', f'LC Rail Temperature Alarm not set correctly.')

    def check_hc_status_alarm(self, hbidps, log=True):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.rwc_status != 0x3FF:
            self.Log('error', f'HC rail status alarm not set.')
        if log:
            self.Log('info', f'HC RAIL ALARM : {hex(hc_rail_alarm.value)}')
            self.Log('info', f'HC RAIL STATUS ALARM : {hex(hc_rail_alarm.rwc_status)}')

    def check_lc_status_alarm(self, hbidps, log=True):
        lc_rail_alarm = hbidps.get_lc_rail_alarms()
        if lc_rail_alarm.rwc_status != 0xFFFF:
            self.Log('error', f'LC rail status alarm not set.')
        if log:
            self.Log('info', f'LC RAIL ALARM : {hex(lc_rail_alarm.value)}')
            self.Log('info', f'LC RAIL STATUS ALARM : {hex(lc_rail_alarm.rwc_status)}')

    def check_hc_rail_irange_check_fail_alarms(self, hbidps, log=True):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.r_irange_check_fail != 1:
            self.Log('error', f'HC rail iRange alarm not set.')
        if log:
            self.Log('info', f'HC RAIL ALARM : {hex(hc_rail_alarm.value)}')
            self.Log('info', f'HC RAIL IRANGE ALARM : {hex(hc_rail_alarm.r_irange_check_fail)}')

    def check_hc_irange_check_fail_alarms(self, hbidps, log=True):
        hc_irange_check_fail = hbidps.get_hc_irange_check_fail_alarm()
        if hc_irange_check_fail.rwc_hc_irange_check_fail == 0:
            self.Log('error', f'HC iRange check fail alarm not set.')
        if log:
            self.Log('info', f'HC IRANGE CHECK FAIL ALARM : {hex(hc_irange_check_fail.value)}')

    def check_hc_rail_vrange_alarms(self, hbidps, log=True):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.r_vrange != 1:
            self.Log('error', f'HC rail VRange alarm not set.')
        if log:
            self.Log('info', f'HC RAIL ALARM : {hex(hc_rail_alarm.value)}')
            self.Log('info', f'HC RAIL VRANGE ALARM : {hex(hc_rail_alarm.r_vrange)}')

    def check_hc_power_off_delay_alarm(self, hbidps, rail, log=True):
        hc_power_off_delay_alarm = hbidps.get_hc_channel_power_off_delay_alarm()
        hc_power_off_delay_alarm = hc_power_off_delay_alarm.rwc_hc_pwr_off_delay_check_fail
        if hc_power_off_delay_alarm != 1 << rail:
            self.Log('error', f'HC Power Off Delay alarm not set for rail : {rail}.')
        if log:
            self.Log('info', f'HC POWER OFF DELAY ALARM : {hex(hc_power_off_delay_alarm)}')

    def check_hc_rail_power_off_delay_alarm(self, hbidps, log=True):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.r_pwr_off_delay != 1:
            self.Log('error', f'HC rail Power Off Delay alarm not set.')
        if log:
            self.Log('info', f'HC RAIL ALARM : {hex(hc_rail_alarm.value)}')
            self.Log('info', f'HC RAIL POWER OFF DELAY ALARM : {hex(hc_rail_alarm.r_pwr_off_delay)}')

    def check_lc_power_off_delay_alarm(self, hbidps, rail, log=True):
        lc_power_off_delay_alarm = hbidps.get_lc_channel_power_off_delay_alarm()
        lc_power_off_delay_alarm = lc_power_off_delay_alarm.rwc_lc_pwr_off_delay_check_fail
        if lc_power_off_delay_alarm != 1 << rail:
            self.Log('error', f'LC Power Off Delay alarm not set for rail : {rail}.')
        if log:
            self.Log('info', f'LC POWER OFF DELAY ALARM : {hex(lc_power_off_delay_alarm)}')

    def check_lc_rail_power_off_delay_alarm(self, hbidps, log=True):
        lc_rail_alarm = hbidps.get_lc_rail_alarms()
        if lc_rail_alarm.r_pwr_off_delay != 1:
            self.Log('error', f'LC rail Power Off Delay alarm not set.')
        if log:
            self.Log('info', f'LC RAIL ALARM : {hex(lc_rail_alarm.value)}')
            self.Log('info', f'LC RAIL POWER OFF DELAY ALARM : {hex(lc_rail_alarm.r_pwr_off_delay)}')

    def check_hc_rail_cfold_alarms(self, hbidps, expected_bit, log=True):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.r_hc_cfold != expected_bit:
            self.Log('error', f'HC rail CFold alarm not set.')
        if log:
            self.Log('info', f'HC RAIL ALARM : {hex(hc_rail_alarm.value)}')
            self.Log('info', f'HC RAIL CFOLD ALARM : {hex(hc_rail_alarm.r_hc_cfold)}')

    def check_hc_cfold_alarms(self, hbidps, rail, expected_bit, log=True):
        hc_cfold_alarm = hbidps.get_hc_cfold_alarms().rwc_hc_cfold
        if hc_cfold_alarm != expected_bit<<rail:
            self.Log('error', f'HC CFold alarm not set.')
        if log:
            self.Log('info', f'HC CFOLD ALARM : {hex(hc_cfold_alarm)}')

    def check_lc_rail_cfold_alarms(self, hbidps, expected_bit, log=True):
        lc_rail_alarm = hbidps.get_lc_rail_alarms()
        if lc_rail_alarm.r_lc_cfold != expected_bit:
            self.Log('error', f'LC rail CFold alarm not set.')
        if log:
            self.Log('info', f'LC RAIL ALARM : {hex(lc_rail_alarm.value)}')
            self.Log('info', f'LC RAIL CFOLD ALARM : {hex(lc_rail_alarm.r_lc_cfold)}')

    def check_lc_cfold_alarms(self, hbidps, rail, expected_bit, log=True):
        lc_cfold_alarm = hbidps.get_lc_cfold_alarms().rwc_lc_cfold
        if lc_cfold_alarm != expected_bit<<rail:
            self.Log('error', f'LC CFold alarm not set.')
        if log:
            self.Log('info', f'LC CFOLD ALARM : {hex(lc_cfold_alarm)}')

    def check_hc_rail_alert_alarm(self, hbidps, log=True):
        hc_rail_alarm = hbidps.get_hc_rail_alarms()
        if hc_rail_alarm.rwc_alert == 0:
            self.Log('error', f'HC rail Alert alarm not set.')
        if log:
            self.Log('info', f'HC RAIL ALARM : {hex(hc_rail_alarm.value)}')
            self.Log('info', f'HC RAIL ALERT ALARM : {hex(hc_rail_alarm.rwc_alert)}')

    def check_lc_rail_alert_alarm(self, hbidps, log=True):
        lc_rail_alarm = hbidps.get_lc_rail_alarms()
        if lc_rail_alarm.rwc_alert == 0:
            self.Log('error', f'LC rail Alert alarm not set.')
        if log:
            self.Log('info', f'LC RAIL ALARM : {hex(lc_rail_alarm.value)}')
            self.Log('info', f'LC RAIL ALERT ALARM : {hex(lc_rail_alarm.rwc_alert)}')

    def check_hc_vrange_self_alarms(self, hbidps, rail, log=True):
        hc_vrange_self_alarm = hbidps.get_hc_vrange_alarm().rwc_vrange_self
        if hc_vrange_self_alarm != 1<<rail:
            self.Log('error', f'HC VRange self alarm not set for rail : {rail}.')
        if log:
            self.Log('info', f'HC VRANGE SELF ALARM : {hex(hc_vrange_self_alarm)}')

    def check_hc_vrange_neighbor_alarms(self, hbidps, rail, log=True):
        hc_vrange_neighbor_alarm = hbidps.get_hc_vrange_alarm().rwc_vrange_neighbor
        if hc_vrange_neighbor_alarm != 1<<rail:
            self.Log('error', f'HC VRange neighbor alarm not set for rail : {rail}.')
        if log:
            self.Log('info', f'HC VRANGE NEIGHBOR ALARM : {hex(hc_vrange_neighbor_alarm)}')

    def check_lc_irange_check_fail_alarms(self, hbidps, log=True):
        lc_irange_check_fail = hbidps.get_lc_irange_check_fail_alarm()
        if lc_irange_check_fail.rwc_lc_irange_check_check_fail == 0:
            self.Log('error', f'LC Rail iRange Check Fail alarm not set.')
        if log:
            self.Log('info', f'LC IRANGE CHECK FAIL ALARM : {hex(lc_irange_check_fail.value)}')

    def check_lc_rail_irange_check_fail_alarms(self, hbidps, log=True):
        lc_rail_alarm = hbidps.get_lc_rail_alarms()
        if lc_rail_alarm.r_irange_check_fail != 1:
            self.Log('error', f'LC Rail Alarm not set.')
        if log:
            self.Log('info', f'LC RAIL ALARM : {hex(lc_rail_alarm.value)}')
            self.Log('info', f'LC RAIL IRANGE CHECK FAIL ALARM : {hex(lc_rail_alarm.r_irange_check_fail)}')
    # alarm interface API END

    def init_limits(self):
        self.previous_ov = 7
        self.previous_uv = 0
        self.previous_oc = 4

    def get_vout_settings_and_power_state(self, vout):
        if vout is None:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 6.0
            oc = 2.0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            uv = 0
            ov = 0.5
            oc = 1.5
        elif 0.6 <= vout <= 5:
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0
        else:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 7.0
            oc = 0.5
            self.Log('error', f'vout: {vout}V is not a valid value for lc rail')
        return ov, pwr_state, uv, vout_on, oc

    def create_object_interface(self):
        self.alarm_helper = AlarmHelper(self.hbidps)
        self.tq_generator = TriggerQueueCommands()
        self.tq_ddr = TriggerQueueDDR(self.hbidps)
        self.hc_rail = HCRailPMBUS(self.hbidps)
        self.rail_transition = RailTransitionControl(self.hbidps)
        self.mdut_config = MdutConfiguration(self.hbidps)

    def set_ground_control_mode(self, mode):
        if mode == "pcie_mode":
            self.hbidps.write_bar_register(hbidps_register.GROUND_CONTROL_MODE(value=0x1))
        elif mode == "trigger_queue_mode":
            self.hbidps.write_bar_register(hbidps_register.GROUND_CONTROL_MODE(value=0x0))

    def setup_hc_rail_config(self):
        self.hc_rail.init_hc_rails_hw()
        self.rail_transition.init_rail_transition_time()
        self.rail_transition.log_rail_transition_time()
        self.hc_rail.log_hc_run_reg()
        self.hc_rail.enable_hc_run_reg()
        self.mdut_config.set_uhc_dut_status(uhc_id=0, dut_id=1, valid=1, log=False)

    def create_tq_commands_invalid_gnd_ctrl(self):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, 0x3)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, 0x3)
        self.tq_generator.add_command(tq_cmd.set_ov_limit, tq_dev.HCrail0, 0x40400000)
        self.tq_generator.add_command(tq_cmd.set_ov_limit, tq_dev.HCrail1, 0x40400000)
        self.tq_generator.add_command(tq_cmd.set_uv_limit, tq_dev.HCrail0, 0x0)
        self.tq_generator.add_command(tq_cmd.set_uv_limit, tq_dev.HCrail1, 0x0)
        self.tq_generator.add_command(tq_cmd.set_v, tq_dev.HCrail0, 0x0)
        self.tq_generator.add_command(tq_cmd.set_v, tq_dev.HCrail1, 0x0)
        self.tq_generator.add_command(tq_cmd.set_fd_current_hi, tq_dev.HCrail0, 0x40000000)
        self.tq_generator.add_command(tq_cmd.set_fd_current_hi, tq_dev.HCrail1, 0x40000000)
        self.tq_generator.add_command(tq_cmd.set_pwr_state, tq_dev.HCrail0, 0x2)
        self.tq_generator.add_command(tq_cmd.set_pwr_state, tq_dev.HCrail1, 0x2)
        self.tq_generator.add_command(tq_cmd.set_fdt, tq_dev.HCrail0, 0x01000)
        self.tq_generator.add_command(tq_cmd.set_fdt, tq_dev.HCrail1, 0x01000)
        self.tq_generator.add_command(tq_cmd.set_hi_i_limit, tq_dev.HCrail0, 0x40000000)
        self.tq_generator.add_command(tq_cmd.set_hi_i_limit, tq_dev.HCrail1, 0x40000000)
        self.tq_generator.add_command(tq_cmd.apply_user_attributes, tq_dev.HCrail0, 0)
        self.tq_generator.add_command(tq_cmd.apply_user_attributes, tq_dev.HCrail1, 0)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, 0x3)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def send_trigger_queue(self, tq_list):
        tq_data = self.tq_ddr.get_tq_data(tq_list)
        lut_address = self.tq_ddr.get_dps_lut_address(0, 0)
        tq_content_address = self.tq_ddr.get_tq_content_address()
        self.tq_ddr.store_tq_address_to_lut(lut_address, tq_content_address)
        self.tq_ddr.store_tq_content_to_mem(tq_content_address, tq_data)
        self.tq_ddr.clear_tq_notify()
        self.tq_ddr.send_dps_trigger_through_rctc(0x01, 1, 0)
        self.check_hc_fold_status(self.hbidps)
        self.tq_ddr.check_tq_notify(0, 1, 0, False)

    def check_invalid_gnd_ctrl_tq_cmd_bit(self, expected_bit):
        global_alarm = self.alarm_helper.read_global_alarm_reg()
        if global_alarm.rwc_invalid_gnd_ctrl_tq_cmd != expected_bit:
            self.Log('error', f'Invalid Ground Control Trigger Queue Command bit wrongly set. Expected : {expected_bit}')

    def check_hc_fold_status(self, hbidps):
        hc_fold_status = hbidps.read_bar_register(hbidps_register.HC_FOLDED)
        hc_fold_status.value = 0x3FF
        hbidps.write_bar_register(hc_fold_status)
        time.sleep(0.1)
        hc_fold_status = hbidps.read_bar_register(hbidps_register.HC_FOLDED)
        self.Log('info', f'hc_fold_status {hex(hc_fold_status.value)}')

    def create_invalid_tq_command_ddr(self):
        invalid_cmd = 0x03FFFF
        meta_data = random.randrange(0xfff)
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, 0x3)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, 0x3)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, 0x3)
        self.tq_generator.add_raw_command(0xf03, 0x30, invalid_cmd)
        self.tq_generator.add_raw_command(0xf03, 0x31, invalid_cmd)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def check_invalid_tq_cmd_bit(self, expected_bit):
        global_alarm = self.alarm_helper.read_global_alarm_reg()
        if global_alarm.rwc_invalid_tq_cmd != expected_bit:
            self.Log('error', f'Invalid Trigger Queue Command bit wrongly set. Expected : {expected_bit}')

    def check_alarm_if_temp_within_limit(self, max6628_device):
        max_limit = self.alarm_helper.read_max6628_upper_limit(max6628_device)
        min_limit = self.alarm_helper.read_max6628_lower_limit(max6628_device)
        temp = self.alarm_helper.read_max6628_temp(max6628_device)
        if temp >= min_limit and temp <= max_limit:
            max_alarm_bits = self.alarm_helper.read_temp_alarm()
            max_alarm_low_bits = max_alarm_bits.low_temp
            max_alarm_high_bits = max_alarm_bits.high_temp
            if max_alarm_low_bits != 0 or max_alarm_high_bits != 0:
                self.Log('error', f'Temp Alarm bits falsely set. Temp within limits.')
            global_alarm = self.alarm_helper.read_global_alarm_reg()
            if global_alarm.r_temp == 1:
                self.Log('error', f'Global Alarm Bit falsely set when the temp is within the limits')

    def check_alarm_if_temp_under_limit(self, max6628_device):
        temp = self.alarm_helper.read_max6628_temp(max6628_device)
        self.change_lower_temp_limit(temp, max6628_device)
        max_limit = self.alarm_helper.read_max6628_upper_limit(max6628_device)
        min_limit = self.alarm_helper.read_max6628_lower_limit(max6628_device)
        if temp < min_limit:
            max_alarm_bits = self.alarm_helper.read_temp_alarm()
            max_alarm_low_bits = max_alarm_bits.low_temp
            if max_alarm_low_bits == 0:
                self.Log('error', f'Lower Temp Alarm bits falsely set. Temp exceeding lower limits.')
            global_alarm = self.alarm_helper.read_global_alarm_reg()
            if global_alarm.r_temp == 0:
                self.Log('error', f'Global Alarm Bit not set when the temp exceeded the lower threshold')
        self.reset_lower_temp_limit(max6628_device)

    def change_lower_temp_limit(self, temp, max6628_device):
        self.alarm_helper.write_max6628_lower_limit(max6628_device, temp+100)
        time.sleep(0.5)

    def check_alarm_if_temp_over_limit(self, max6628_device):
        temp = self.alarm_helper.read_max6628_temp(max6628_device)
        self.change_upper_temp_limit(temp, max6628_device)
        max_limit = self.alarm_helper.read_max6628_upper_limit(max6628_device)
        min_limit = self.alarm_helper.read_max6628_lower_limit(max6628_device)
        if temp > max_limit:
            max_alarm_bits = self.alarm_helper.read_temp_alarm()
            max_alarm_high_bits = max_alarm_bits.high_temp
            if max_alarm_high_bits == 0:
                self.Log('error', f'Upper Temp Alarm bits falsely set. Temp exceeding upper limits.')
            global_alarm = self.alarm_helper.read_global_alarm_reg()
            if global_alarm.r_temp == 0:
                self.Log('error', f'Global Alarm Bit not set when the temp exceeded the upper threshold')
        self.reset_upper_temp_limit(max6628_device)

    def change_upper_temp_limit(self, temp, max6628_device):
        self.alarm_helper.write_max6628_upper_limit(max6628_device, temp-100)
        time.sleep(0.5)

    def check_alarm_if_curr_within_limit(self):
        max_curr = self.alarm_helper.read_convert_tli4970_max_current()
        min_curr = self.alarm_helper.read_convert_tli4970_min_current()
        curr = self.read_convert_tli4970_current()
        if curr >= min_curr and curr <= max_curr:
            curr_alarm_bits = self.alarm_helper.read_sys_current_alarm()
            curr_alarm_low_bits = curr_alarm_bits.neg_limit
            curr_alarm_high_bits = curr_alarm_bits.pos_limit
            if curr_alarm_low_bits != 0 or curr_alarm_high_bits != 0:
                self.Log('error', f'Current Alarm bits falsely set. Current within limits.')
            global_alarm = self.alarm_helper.read_global_alarm_reg()
            if global_alarm.r_sys_current == 1:
                self.Log('error', f'Global Alarm Bit falsely set when the current is within the limits')

    def read_convert_tli4970_current(self):
        curr = self.alarm_helper.read_TLI4970_current()
        curr = self.alarm_helper.decode_single_precision_floating_point_current(curr)
        return curr

    def check_alarm_if_curr_under_limit(self):
        self.change_current_lower_limit()
        max_curr = self.alarm_helper.read_convert_tli4970_max_current()
        min_curr = self.alarm_helper.read_convert_tli4970_min_current()
        time.sleep(self.filter_time * 0.000001)
        curr = self.read_convert_tli4970_current()
        if curr < min_curr:
            curr_alarm_bits = self.alarm_helper.read_sys_current_alarm()
            curr_alarm_low_bits = curr_alarm_bits.neg_limit
            if curr_alarm_low_bits == 0:
                self.Log('error', f'Lower Current Alarm bits falsely set. Current exceeding lower limits.')
            global_alarm = self.alarm_helper.read_global_alarm_reg()
            if global_alarm.r_sys_current == 0:
                self.Log('error', f'Global Alarm Bit not set when the current exceeded the lower threshold')
        self.reset_lower_current_limit()

    def reset_lower_current_limit(self):
        under_current_default = self.alarm_helper.encode_float_to_single_precision_floating_format(-10)
        self.alarm_helper.write_tli4970_lower_limit(under_current_default)

    def change_current_lower_limit(self):
        curr = self.read_convert_tli4970_current()
        min = self.alarm_helper.encode_float_to_single_precision_floating_format(curr + 2)
        self.alarm_helper.write_tli4970_lower_limit(min)

    def check_alarm_if_curr_over_limit(self):
        self.change_current_upper_limit()
        max_curr = self.alarm_helper.read_convert_tli4970_max_current()
        min_curr = self.alarm_helper.read_convert_tli4970_min_current()
        time.sleep(self.filter_time * 0.000001)
        curr = self.read_convert_tli4970_current()
        if curr > max_curr:
            curr_alarm_bits = self.alarm_helper.read_sys_current_alarm()
            curr_alarm_high_bits = curr_alarm_bits.pos_limit
            if curr_alarm_high_bits == 0:
                self.Log('error', f'Upper Current Alarm bits falsely set. Current exceeding Upper limits.')
            global_alarm = self.alarm_helper.read_global_alarm_reg()
            if global_alarm.r_sys_current == 0:
                self.Log('error', f'Global Alarm Bit not set when the current exceeded the Upper threshold')
        self.reset_upper_current_limit()

    def change_current_upper_limit(self):
        curr = self.read_convert_tli4970_current()
        max = self.alarm_helper.encode_float_to_single_precision_floating_format(curr - 2)
        self.alarm_helper.write_tli4970_upper_limit(max)

    def reset_upper_current_limit(self):
        over_current_default = self.alarm_helper.encode_float_to_single_precision_floating_format(40)
        self.alarm_helper.write_tli4970_upper_limit(over_current_default)

    def reset_lower_temp_limit(self, max6628_device):
        self.alarm_helper.write_max6628_lower_limit(max6628_device, 0x00)

    def reset_upper_temp_limit(self, max6628_device):
        self.alarm_helper.write_max6628_upper_limit(max6628_device, 0x0fff)


class HCRailsEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_hc_rails_start_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_hc_rails_to_safe_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)


class RailsAlarmEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()

class LCRailsEnv(object):
    def __init__(self, hbidps, test):
        self.hbidps = hbidps
        self.test = test

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_lc_rails_start_state()
        self.test.init_limits()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.set_lc_rails_to_safe_state()
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)