################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Sample Engine Testing

Test BAR1 registers, read back.

Check header region ,when reails are OFF.

Check header region and data region ,when reails are ON.

Test alarms
"""

import random
import time

from Common.fval import skip
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.instrument.hbidps_constant_number import *
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands
from Hbidps.Tests.HbidpsTest import HbidpsTest

TEST_ITERATION_COUNT = 1000
COMMAND_REGISTER_DICT = {0x40: 'SAMPLE_COUNT', 0x41: 'SAMPLE_RATIO', 0x43: 'SAMPLE_METADATA'}
MAX_FAIL_COUNT = 10
HC_TQ_RAIL_ID_BASE = 0x30
HC_TQ_FDT = 0x1000
HC_TQ_I_HI = 10
LC_TQ_RAIL_ID_BASE = 0x20
LC_TQ_FDT = 0x1000
LC_TQ_I_HI = 2.0
LC_INITIAL_I_HI = 4
VT_TQ_RAIL_ID_BASE = 0x40
VT_REF_VOLTAGE = 2.5
RAIL_RISE_DELAY_US = 200000
VT_RISE_DELAY_US = 1000

class BaseTest(HbidpsTest):

    def compare_expected_with_observed(self, expected, observed, error_count, iteration,rail_under_test):
        if observed != expected:
            error_count.append(
                {'Iteration': iteration, 'Expected': hex(expected), 'Observed': hex(observed), 'Rail': rail_under_test})

    def report_errors(self, device, register_errors, error_message_to_print):
        column_headers = ['Iteration', 'Expected', 'Observed', 'Rail']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print, table), 'error')

    def get_rail_command(self, hbidps,device_list):
        command_list = [int(hbidps.symbols.RAILCOMMANDS.SMP_COUNT),int(hbidps.symbols.RAILCOMMANDS.SMP_RATIO),int(hbidps.symbols.RAILCOMMANDS.SMP_METADATA)]
        command_encoading_hi = hbidps.registers.CMD_DIRECT_INJECT_HI()
        command_encoading_hi.write_or_read = 1
        random.shuffle(command_list)
        command_encoading_hi.command = command_list[0]
        command_encoading_hi.device_number = device_list[0]
        command_encoading_hi.generic_payload = 0
        command_encoading_lo = hbidps.registers.CMD_DIRECT_INJECT_LO()
        random_data_to_write = random.randint(0,0xffff)
        command_encoading_lo.generic_payload = random_data_to_write
        return command_encoading_hi.value,command_encoading_lo.value,random_data_to_write,command_list[0]


class LcSampleRegisterTests(BaseTest):
    """
    Sample Register Testing belongs here.
    bar1_LC_SAMPLE_COUNT_NN
    bar1_LC_SAMPLE_RATIO_NN
    bar1_LC_SAMPLE_METADATA_NN
    bar1_LC_SAMPLE_RAIL_SEL_NN
    bar1_LC_HEADER_REGION_BASE_NN
    bar1_LC_HEADER_REGION_SIZE_NN
    bar1_LC_SAMPLE_REGION_BASE_NN
    bar1_LC_SAMPLE_REGION_SIZE_NN
    bar1_SAMPLING_ACTIVE
    bar1_LC_SAMPLE_ALARMS
    """

    device_register_dict = {0x50: 0, 0x51: 1, 0x52: 2, 0x53: 3}

    def DirectedLcRegisterDataAccessTest(self):
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            hbidps.initialize_hbi_dps()
            error_count = []
            register_errors = []
            for iteration in range(TEST_ITERATION_COUNT):
                device_list = self.get_device_list(hbidps)
                command_encoading_hi, command_encoading_lo,expected_data,command = self.get_rail_command(hbidps,device_list)
                self.execute_rail_command(command_encoading_hi, command_encoading_lo, hbidps)
                read_register_name, index =  self.get_register_to_access(hbidps,command, device_list)
                received_value = hbidps.read_bar_register(read_register_name, index).value
                self.compare_expected_with_observed(expected_data,received_value,error_count,iteration,'LC')
                register_errors.extend(error_count)
                if len(register_errors) >= MAX_FAIL_COUNT:
                    break
            if register_errors:
                error_message_to_print = 'Failed to write in Sample Engine register '
                self.report_errors(hbidps, register_errors,error_message_to_print)
            else:
                self.Log('info','Sample Engine register writes were successful for {} iterations.'.format(TEST_ITERATION_COUNT))

    def execute_rail_command(self, command_encoading_hi, command_encoading_lo, hbidps):
        hbidps.write_bar_register(hbidps.registers.CMD_DIRECT_INJECT_HI(value=command_encoading_hi))
        hbidps.write_bar_register(hbidps.registers.CMD_DIRECT_INJECT_LO(value=command_encoading_lo))

    def get_register_to_access(self,hbidps, command, device_list):
        register_to_access = COMMAND_REGISTER_DICT[command]
        register_to_access = self.get_lc_register(register_to_access)
        index = self.device_register_dict[device_list[0]]
        register_name = register_to_access
        read_register_name = getattr(hbidps.registers, register_name)
        return read_register_name, index

    def get_lc_register(self,register_to_access):
        return register_to_access.replace('SAMPLE','LC_SAMPLE')

    def get_device_list(self, hbidps):
        device_list = [hbidps.symbols.DEVICEID.SMP_LC_00, hbidps.symbols.DEVICEID.SMP_LC_01,
                       hbidps.symbols.DEVICEID.SMP_LC_02, hbidps.symbols.DEVICEID.SMP_LC_03]
        random.shuffle(device_list)
        return device_list


class HcSampleRegisterTests(BaseTest):
    """
    Sample Register Testing belongs here.
    bar1_HC_SAMPLE_COUNT_NN
    bar1_HC_SAMPLE_RATIO_NN
    bar1_HC_SAMPLE_METADATA_NN
    bar1_HC_SAMPLE_RAIL_SEL_NN
    bar1_HC_HEADER_REGION_BASE_NN
    bar1_HC_HEADER_REGION_SIZE_NN
    bar1_HC_SAMPLE_REGION_BASE_NN
    bar1_HC_SAMPLE_REGION_SIZE_NN
    bar1_SAMPLING_ACTIVE
    bar1_HC_SAMPLE_ALARMS
    """
    device_register_dict = {0x60: 0, 0x61: 1, 0x62: 2, 0x63: 3}

    def DirectedHcRegisterDataAccessTest(self):
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            hbidps.initialize_hbi_dps()
            error_count = []
            register_errors = []
            for iteration in range(TEST_ITERATION_COUNT):
                device_list = self.get_device_list(hbidps)
                command_encoading_hi, command_encoading_lo,expected_data,command = self.get_rail_command(hbidps,device_list)
                self.execute_rail_command(command_encoading_hi, command_encoading_lo, hbidps)
                read_register_name, index = self.get_register_to_access(hbidps,command, device_list)
                received_value = hbidps.read_bar_register(read_register_name, index).value
                self.compare_expected_with_observed(expected_data,received_value,error_count,iteration,'HC')
                register_errors.extend(error_count)
                if len(register_errors) >= MAX_FAIL_COUNT:
                    break
            if register_errors:
                error_message_to_print = 'Failed to write in Sample Engine register '
                self.report_errors(hbidps, register_errors,error_message_to_print)
            else:
                self.Log('info','Sample Engine register writes were successful for {} iterations.'.format(TEST_ITERATION_COUNT))

    def execute_rail_command(self, command_encoading_hi, command_encoading_lo, hbidps):
        hbidps.write_bar_register(hbidps.registers.CMD_DIRECT_INJECT_HI(value=command_encoading_hi))
        hbidps.write_bar_register(hbidps.registers.CMD_DIRECT_INJECT_LO(value=command_encoading_lo))

    def get_register_to_access(self,hbidps, command, device_list):
        register_to_access = COMMAND_REGISTER_DICT[command]
        register_to_access = self.get_lc_register(register_to_access)
        index = self.device_register_dict[device_list[0]]
        register_name = register_to_access
        read_register_name = getattr(hbidps.registers, register_name)
        return read_register_name, index

    def get_lc_register(self,register_to_access):
        return register_to_access.replace('SAMPLE','HC_SAMPLE')

    def get_device_list(self, hbidps):
        device_list = [hbidps.symbols.DEVICEID.SMP_HC_00, hbidps.symbols.DEVICEID.SMP_HC_01,
                       hbidps.symbols.DEVICEID.SMP_HC_02, hbidps.symbols.DEVICEID.SMP_HC_03]
        random.shuffle(device_list)
        return device_list


class VMeasureSampleRegisterTests(BaseTest):
    """
    Sample Register Testing belongs here.
    bar1_VMeasure_SAMPLE_COUNT_NN
    bar1_VMeasure_SAMPLE_RATIO_NN
    bar1_VMeasure_SAMPLE_METADATA_NN
    bar1_VMeasure_SAMPLE_RAIL_SEL_NN
    bar1_VMeasure_HEADER_REGION_BASE_NN
    bar1_VMeasure_HEADER_REGION_SIZE_NN
    bar1_VMeasure_SAMPLE_REGION_BASE_NN
    bar1_VMeasure_SAMPLE_REGION_SIZE_NN
    bar1_SAMPLING_ACTIVE
    bar1_VMeasure_SAMPLE_ALARMS
    """
    device_register_dict = {0x70: 0, 0x71: 1, 0x72: 2, 0x73: 3, 0x74: 4}

    def DirectedVMeasureRegisterDataAccessTest(self):
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            hbidps.initialize_hbi_dps()
            error_count = []
            register_errors = []
            for iteration in range(TEST_ITERATION_COUNT):
                device_list = self.get_device_list(hbidps)
                command_encoading_hi, command_encoading_lo,expected_data,command = self.get_rail_command(hbidps,device_list)
                self.execute_rail_command(command_encoading_hi, command_encoading_lo, hbidps)
                read_register_name, index = self.get_register_to_access(hbidps,command, device_list)
                received_value = hbidps.read_bar_register(read_register_name, index).value
                self.compare_expected_with_observed(expected_data,received_value,error_count,iteration,'VMeasure')
                register_errors.extend(error_count)
                if len(register_errors) >= MAX_FAIL_COUNT:
                    break
            if register_errors:
                error_message_to_print = 'Failed to write in Sample Engine register '
                self.report_errors(hbidps, register_errors,error_message_to_print)
            else:
                self.Log('info','Sample Engine register writes were successful for {} iterations.'.format(TEST_ITERATION_COUNT))

    def execute_rail_command(self, command_encoading_hi, command_encoading_lo, hbidps):
        hbidps.write_bar_register(hbidps.registers.CMD_DIRECT_INJECT_HI(value=command_encoading_hi))
        hbidps.write_bar_register(hbidps.registers.CMD_DIRECT_INJECT_LO(value=command_encoading_lo))

    def get_register_to_access(self,hbidps, command, device_list):
        register_to_access = COMMAND_REGISTER_DICT[command]
        register_to_access = self.get_lc_register(register_to_access)
        index = self.device_register_dict[device_list[0]]
        register_name = register_to_access
        read_register_name = getattr(hbidps.registers, register_name)
        return read_register_name, index

    def get_lc_register(self,register_to_access):
        return register_to_access.replace('SAMPLE','VMEAS_SAMPLE')

    def get_device_list(self, hbidps):
        device_list = [hbidps.symbols.DEVICEID.SMP_VMEAS_00, hbidps.symbols.DEVICEID.SMP_VMEAS_01,
                       hbidps.symbols.DEVICEID.SMP_VMEAS_02, hbidps.symbols.DEVICEID.SMP_VMEAS_03,
                       hbidps.symbols.DEVICEID.SMP_VMEAS_04]
        random.shuffle(device_list)
        return device_list


class Functional(HbidpsTest):
    """ Check the sample engine capability and correctness

    Need to clean up the above class and test"""

    @skip('Not implemented')
    def DirectedHCLCVTPollingRegisterTest(self):
        """ Check the constantly polling register value is accurate or not

        Steps:
        1.Follow step in HC/LC/Vtarget rail setting to set rails
        2.Read bar1_LC_CHANNEL_NN_VOLTAGE, bar1_LC_CHANNEL_NN_CURRENT,
        bar1_HC_CHANNEL_NN_VOLTAGE, bar1_HC_CHANNEL_NN_CURRENT
        and bar1_AD7616_CHANNEL_NN_VM_ADC to check voltage and current.

        FPGA Review Note:
        1.Pay attention to refresh rate
        2.Give different value to different rails in HC/LC/VT
        """
        pass

    def DirectedHCConstantPollingRegisterTest(self):
        """ Check the HC constantly polling register value is accurate or not

        Steps:
        1.Follow step in HC rail setting to set rail and DTB load
        2.Read bar1_HC_CHANNEL_NN_VOLTAGE, bar1_HC_CHANNEL_NN_CURRENT
          to check voltage and current.
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(10):
            self.general_hc_1rail_off_to_on_dtb_feedback_poll_registers_flow(vout=1, rail=rail, hbidps=hbidps)

    def DirectedLCConstantPollingRegisterTest(self):
        """ Check the LC constantly polling register value is accurate or not

        Steps:
        1.Follow step in LC rail setting to set rail and DTB load
        2.Read bar1_LC_CHANNEL_NN_VOLTAGE, bar1_LC_CHANNEL_NN_CURRENT
          to check voltage and current
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.general_lc_1rail_off_to_on_dtb_feedback_poll_registers_flow(vout=1, rail=rail, hbidps=hbidps)

    def DirectedVtargetConstantPollingRegisterTest(self):
        """ Check the VTVM constantly polling register value is accurate or not

        Steps:
        1.Follow step in VTVM rail setting to set rail
        2.Read bar1_AD7616_CHANNEL_NN_VM_ADC
          to check voltage
        """
        for hbidps in self.hbidps_list:
            self.vtvm_single_force_and_readback_poll_registers_flow(vtarget_init=0.25, hbidps=hbidps)

    def DirectedHCSampleEngineCountRatioTest(self):
        """ Check the sample engine count & ratio

        Steps:
        1.Follow step in HC rail setting to set rail from off to on
        2.Check Polling Register
        3.Check HC Sample Setting Bar Register
        4.Check HC Sample DMA Header Content
        5.Check HC Sample DMA Data Content
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(10):
            self.general_hc_1rail_off_to_on_dtb_feedback_sample_engine_flow(
                vout=1, rail=rail, hbidps=hbidps)

    def DirectedLCSampleEngineCountRatioTest(self):
        """ Check the sample engine count & ratio

        Steps:
        1.Follow step in LC rail setting to set rail from off to on
        2.Check Polling Register
        3.Check LC Sample Setting Bar Register
        4.Check LC Sample DMA Header Content
        5.Check LC Sample DMA Data Content
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.general_lc_1rail_off_to_on_dtb_feedback_sample_engine_flow(
                vout=1, rail=rail, hbidps=hbidps)

    def DirectedVtargetSampleEngineCountRatioTest(self):
        """ Check the sample engine count & ratio

        Steps:
        1.Follow step in VTVM rail setting to set rail from off to on
        2.Check Polling Register
        3.Check VM Sample Setting Bar Register
        4.Check VM Sample DMA Header Content
        5.Check VM Sample DMA Data Content
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.vtvm_single_force_and_readback_sample_engine_flow(
                vtarget_init=0.25, rail=rail, hbidps=hbidps)

    def DirectedHCMultiSampleEngineTest(self):
        """ Check the 4 HC sample engine count & ratio

        Steps:
        1.Follow step in HC rail setting to set one rail from off to on.
        2.Check Polling Register
        3.Check HC Sample Setting Bar Register
        4.Check 4 HC Sample DMA Header Content
        5.Check 4 HC Sample DMA Data Content
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 9)
        self.general_hc_1rail_off_to_on_dtb_feedback_all_sample_engine_flow(vout=1, rail=rail, hbidps=hbidps)

    def DirectedLCMultiSampleEngineTest(self):
        """ Check the 4 LC sample engine count & ratio

        Steps:
        1.Follow step in LC rail setting to set one rail from off to on.
        2.Check Polling Register
        3.Check LC Sample Setting Bar Register
        4.Check 4 LC Sample DMA Header Content
        5.Check 4 LC Sample DMA Data Content
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.general_lc_1rail_off_to_on_dtb_feedback_all_sample_engine_flow(vout=1, rail=rail, hbidps=hbidps)

    def DirectedVMMultiSampleEngineTest(self):
        """ Check the 5 VM sample engine count & ratio

        Steps:
        1.Follow step in VTVM to seup 16 VT rails
        2.Check Polling Register
        3.Check VM Sample Setting Bar Register
        4.Check 5 VM Sample DMA Header Content
        5.Check 5 VM Sample DMA Data Content
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.vtvm_single_force_and_readback_all_sample_engine_flow(vtarget_init=0.25, rail=rail, hbidps=hbidps)

    def DirectedHCSampleEngineTimingMetadataTest(self):
        """ Check the 2 HC sample engine timing delta and value

        Steps:
        1.Follow step in HC rail setting to set one rail from off to on
        2.Start 2 HC Sample Engines with Time Delta
        2.Check Polling Register
        3.Check HC Sample Setting Bar Register
        4.Check 2 HC Sample DMA Header Content
        5.Check 2 HC Sample DMA Data Content
        6.Check the Time Stamp Value Delta
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 9)
        self.general_hc_1rail_off_to_on_dtb_feedback_double_sample_engine_flow(vout=1, rail=rail, hbidps=hbidps)

    def DirectedLCSampleEngineTimingMetadataTest(self):
        """ Check the 2 LC sample engine timing delta and value

        Steps:
        1.Follow step in LC rail setting to set one rail from off to o
        2.Start 2 LC Sample Engines with Time Delta
        2.Check Polling Register
        3.Check LC Sample Setting Bar Register
        4.Check 2 LC Sample DMA Header Content
        5.Check 2 LC Sample DMA Data Content
        6.Check the Time Stamp Value Delta
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.general_lc_1rail_off_to_on_dtb_feedback_double_sample_engine_flow(vout=1, rail=rail, hbidps=hbidps)

    def DirectedVtargetSampleEngineTimingMetadataTest(self):
        """ Check the 2 VM sample engine timing delta and value

        Steps:
        1.Follow step in VTVM rail setting to set all rails on
        2.Start 2 VM Sample Engines with Time Delta
        2.Check Polling Register
        3.Check VM Sample Setting Bar Register
        4.Check 2 VM Sample DMA Header Content
        5.Check 2 VM Sample DMA Data Content
        6.Check the Time Stamp Value Delta
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.vtvm_single_force_and_readback_double_sample_engine_flow(vtarget_init=0.25, rail=rail, hbidps=hbidps)

    def DirectedHCSampleEngineShadowRegisterTest(self):
        """ Check the sample engine shadow register

        Steps:
        1.Follow Step in HC Rail Setting to Set Rail from Off to On
        2.Configure Sample Engine in 3 Different Location of TQ
        3.Check Only the Middle One Sample Engine Setting Get Applied
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 9)
        self.general_hc_1rail_off_to_on_dtb_feedback_sample_engine_flow(
            vout=1, rail=rail, hbidps=hbidps, shadow_test=True)

    def DirectedLCSampleEngineShadowRegisterTest(self):
        """ Check the sample engine shadow register

        Steps:
        1.Follow Step in LC Rail Setting to Set Rail from Off to On
        2.Configure Sample Engine in 3 Different Location of TQ
        3.Check Only the Middle One Sample Engine Setting Get Applied
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.general_lc_1rail_off_to_on_dtb_feedback_sample_engine_flow(
            vout=1, rail=rail, hbidps=hbidps, shadow_test=True)

    def DirectedVtargetSampleEngineShadowRegisterTest(self):
        """ Check the sample engine shadow register

        Steps:
        1.Follow Step in VTVM Rail Setting to Set Rail from Off to On
        2.Configure Sample Engine in 3 Different Location of TQ
        3.Check Only the Middle One Sample Engine Setting Get Applied
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.vtvm_single_force_and_readback_sample_engine_flow(
            vtarget_init=0.25, rail=rail, hbidps=hbidps, shadow_test=True)

    def DirectedHCSampleEngineResetTest(self):
        """ Check the sample engine reset function

        Steps:
        1.Follow step in HC rail setting to set rail from off to on
        2.Setup sample engine to polling HC rail vaoltage & current
        3.Reset sample engine
        4.Follow step in HC rail setting to set rail on to a different voltage
        5.Setup sample engine to polling HC rail vaoltage & current
        6.Check sample engine header & data in memory-only one polling result
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(10):
            self.hc_1rail_sample_engine_sequency_repeat_flow(
                vout=0.8, rail=rail, hbidps=hbidps, reset_in_between=True)

    def DirectedHCSampleEngineResetNegtiveTest(self):
        """ Check the sample engine reset function - negative test

        Steps:
        1.Follow step in HC rail setting to set rail from off to on
        2.Setup sample engine to polling HC rail vaoltage & current
        3.Don't reset sample engine
        4.Follow step in HC rail setting to set rail on to a different voltage
        5.Setup sample engine to polling HC rail vaoltage & current
        6.Check sample engine header & data in memory-both polling result
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(10):
            self.hc_1rail_sample_engine_sequency_repeat_flow(
                vout=0.8, rail=rail, hbidps=hbidps, reset_in_between=False)

    def DirectedLCSampleEngineResetTest(self):
        """ Check the sample engine reset function - negative test

        Steps:
        1.Follow step in LC rail setting to set rail from off to on
        2.Setup sample engine to polling LC rail vaoltage & current
        3.Reset sample engine
        4.Follow step in LC rail setting to set rail on to a different voltage
        5.Setup sample engine to polling LC rail vaoltage & current
        6.Check sample engine header & data in memory-only one polling result
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.lc_1rail_sample_engine_sequency_repeat_flow(
                vout=0.8, rail=rail, hbidps=hbidps, reset_in_between=True)

    def DirectedLCSampleEngineResetNegtiveTest(self):
        """ Check the sample engine reset function - negative test

        Steps:
        1.Follow step in LC rail setting to set rail from off to on
        2.Setup sample engine to polling LC rail vaoltage & current
        3.Don't reset sample engine
        4.Follow step in LC rail setting to set rail on to a different voltage
        5.Setup sample engine to polling LC rail vaoltage & current
        6.Check sample engine header & data in memory-both polling result
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.lc_1rail_sample_engine_sequency_repeat_flow(
                vout=0.8, rail=rail, hbidps=hbidps, reset_in_between=False)

    def DirectedVMSampleEngineResetTest(self):
        """ Check the sample engine reset function - negative test

        Steps:
        1.Follow step in VT rail setting to set rail from off to on
        2.Setup sample engine to polling VM rail vaoltage
        3.Reset sample engine
        4.Follow step in VT rail setting to set rail on to a different voltage
        5.Setup sample engine to polling VM rail vaoltage
        6.Check sample engine header & data in memory-only one polling result
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.vtvm_sample_engine_sequency_repeat_flow(
                vtarget_init=0.25, rail=rail, hbidps=hbidps,
                reset_in_between=True)

    def DirectedVMSampleEngineResetNegtiveTest(self):
        """ Check the sample engine reset function - negative test

        Steps:
        1.Follow step in VT rail setting to set rail from off to on
        2.Setup sample engine to polling VM rail vaoltage
        3.Don't reset sample engine
        4.Follow step in VT rail setting to set rail on to a different voltage
        5.Setup sample engine to polling VM rail vaoltage
        6.Check sample engine header & data in memory-both polling result
        """
        hbidps = random.choice(self.hbidps_list)
        for rail in range(16):
            self.vtvm_sample_engine_sequency_repeat_flow(
                vtarget_init=0.25, rail=rail, hbidps=hbidps,
                reset_in_between=False)

    def DirectedHCSampleEngineBusyAlarmPositiveTest(self):
        """ Check the HC sample engine busy alarm positive

        Steps:
        1.Follow step in HC rail setting to set rail from off to on
        2.Setup sample engine to polling HC rail vaoltage & current
        3.Follow step in HC rail setting to set rail to a different voltage
        4.Start the sample engine again before the previous sample task done
        5.Check sample engine 'start_error' alarm bit high
        6.Reset the alarm register and reset the sample engine
        7.Start the sample engine again
        8.Check sample engine without error and data is correct
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 9)
        self.hc_sample_engine_busy_alarm_flow(vout=1, rail=rail, hbidps=hbidps, busy_state=True)

    def DirectedLCSampleEngineBusyAlarmPositiveTest(self):
        """ Check the LC sample engine busy alarm positive

        Steps:
        1.Follow step in LC rail setting to set rail from off to on
        2.Setup sample engine to polling LC rail vaoltage & current
        3.Follow step in LC rail setting to set rail to a different voltage
        4.Start the sample engine again before the previous sample task done
        5.Check sample engine 'start_error' alarm bit high
        6.Reset the alarm register and reset the sample engine
        7.Start the sample engine again
        8.Check sample engine without error and data is correct
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.lc_sample_engine_busy_alarm_flow(vout=1, rail=rail, hbidps=hbidps, busy_state=True)

    @skip('FPGA bug; HDMT TICKET 148700')
    def DirectedVtargetSampleEngineBusyAlarmPositiveTest(self):
        """ Check the VM sample engine busy alarm positive

        Steps:
        1.Follow step in VTVM rail setting to set rail from off to on
        2.Setup sample engine to polling VM rail vaoltage & current
        3.Follow step in VTVM rail setting to set rail to a different voltage
        4.Start the sample engine again before the previous sample task done
        5.Check sample engine 'start_error' alarm bit high
        6.Reset the alarm register and reset the sample engine
        7.Start the sample engine again
        8.Check sample engine without error and data is correct
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.vtvm_sample_engine_busy_alarm_flow(vtarget_init=0.25, rail=rail, hbidps=hbidps, busy_state=True)

    def DirectedHCSampleEngineBusyAlarmNegativeTest(self):
        """ Check the HC sample engine busy alarm negative

        Steps:
        1.Follow step in HC rail setting to set rail from off to on
        2.Setup sample engine to polling HC rail vaoltage & current
        3.Follow step in HC rail setting to set rail to a different voltage
        4.Start the sample engine again after the previous sample task done
        5.Check sample engine without error and data is correct
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 9)
        self.hc_sample_engine_busy_alarm_flow(vout=1, rail=rail, hbidps=hbidps, busy_state=False)

    def DirectedLCSampleEngineBusyAlarmNegativeTest(self):
        """ Check the LC sample engine busy alarm negative

        Steps:
        1.Follow step in LC rail setting to set rail from off to on
        2.Setup sample engine to polling LC rail vaoltage & current
        3.Follow step in LC rail setting to set rail to a different voltage
        4.Start the sample engine again after the previous sample task done
        5.Check sample engine without error and data is correct
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.lc_sample_engine_busy_alarm_flow(vout=1, rail=rail, hbidps=hbidps, busy_state=False)

    def DirectedVtargetSampleEngineBusyAlarmNegativeTest(self):
        """ Check the VM sample engine busy alarm negative

        Steps:
        1.Follow step in VTVM rail setting to set rail from off to on
        2.Setup sample engine to polling VM rail vaoltage & current
        3.Follow step in VTVM rail setting to set rail to a different voltage
        4.Start the sample engine again after the previous sample task done
        5.Check sample engine without error and data is correct
        """
        hbidps = random.choice(self.hbidps_list)
        rail = random.randint(0, 15)
        self.vtvm_sample_engine_busy_alarm_flow(vtarget_init=0.25, rail=rail, hbidps=hbidps, busy_state=False)

    @skip('Not implemented')
    def DirectedHCSampleEngineAddressOverflowTest(self):
        """ Check the sample engine base address and data overflow protection

        Might need to split this test into three. (HC/LC/Vtarget)

        Case1: Like DirectedSampleEngineCountRatioTest.
        In step4, set random legal address

        Case2: Like DirectedSampleEngineCountRatioTest.
        In step4, set size small enough to verify oob bit goes high
        in Sample Alarm Register and overflow should not happen
        (the data after the end address should stay the un-changed)

        FPGA Review Note:
        Two region header and data both need to verify
        """
        pass

    @skip('Not implemented')
    def DirectedLCSampleEngineAddressOverflowTest(self):
        pass

    @skip('Not implemented')
    def DirectedVtargetSampleEngineAddressOverflowTest(self):
        """5 engines"""
        pass

    def vtvm_sample_engine_busy_alarm_flow(self, vtarget_init, rail, hbidps, busy_state):
        vtarget_init0 = vtarget_init
        vtarget_init1 = vtarget_init + 0.4
        type = 'vm'
        sample_set0, sample_set1 = \
            self.create_busy_random_sample_setting(type, rail)
        v_set0 = {'vout': self.get_vm_expect_voltage(vtarget_init0, rail)}
        v_set1 = {'vout': self.get_vm_expect_voltage(vtarget_init1, rail)}
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; '
                         f'vtarget_init {vtarget_init0}V; step 0.25V; '
                         f'sample_set {sample_set0}')
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; '
                         f'vtarget_init {vtarget_init1}V; step 0.25V; '
                         f'sample_set {sample_set1}')
        with VTVMRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            delay_us = self.get_vm_sample_engine_delay(type, sample_set0, busy_state)
            self.vtvm_sample_engine_busy_tq_flow(hbidps, vtarget_init0, sample_set0, sample_set1, delay_us)
            if busy_state:
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, busy_state)
                time.sleep(2)
                self.reset_sample_engine_busy_alarm(hbidps, type, sample_set0)
                hbidps.reset_one_sample_engine(type, sample_set0['index'])
                self.vtvm_sample_engine_repeat_tq_flow(hbidps, vtarget_init1, sample_set1)
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, False)
                self.check_sample_engine_data(hbidps, type, sample_set1, v_set1)
            else:
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, busy_state)
                self.check_sample_engine_data_sequence_runs(hbidps, type, sample_set0, sample_set1, v_set0, v_set0)

    def vtvm_sample_engine_sequency_repeat_flow(self, vtarget_init, rail, hbidps, reset_in_between):
        vtarget_init0 = vtarget_init
        vtarget_init1 = vtarget_init + 0.4
        type = 'vm'
        sample_set0, sample_set1 = self.create_repeat_random_sample_setting(type, rail)
        v_set0 = {'vout': self.get_vm_expect_voltage(vtarget_init0, rail)}
        v_set1 = {'vout': self.get_vm_expect_voltage(vtarget_init1, rail)}
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; vtarget_init {vtarget_init0}V; step 0.25V; sample_set {sample_set0}')
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; vtarget_init {vtarget_init1}V; step 0.25V; sample_set {sample_set1}')
        with VTVMRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            self.vtvm_sample_engine_repeat_tq_flow(hbidps, vtarget_init0, sample_set0)
            if reset_in_between:
                hbidps.reset_one_sample_engine(type, sample_set1['index'])
            self.vtvm_sample_engine_repeat_tq_flow(hbidps, vtarget_init1, sample_set1)
            if reset_in_between:
                self.check_sample_engine_data(hbidps, type, sample_set1, v_set1)
            else:
                self.check_sample_engine_data_sequence_runs(hbidps, type, sample_set0, sample_set1, v_set0, v_set1)

    def vtvm_sample_engine_busy_tq_flow(self, hbidps, vtarget_init, sample_set0, sample_set1, delay):
        tq_list = self.create_vt_16rails_voltage_force_with_sample_busy_tq_list(vtarget_init, sample_set0, sample_set1, delay)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start)
        self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
        time.sleep(0.5)  # 0.5 second required for 16 cycles VM sample polling
        self.check_vm_voltage_readback(hbidps, vtarget_init)

    def vtvm_sample_engine_repeat_tq_flow(self, hbidps, vtarget_init, sample_set, delay=0.5):
        tq_list = self.create_vt_16rails_voltage_force_with_sample_tq_list(vtarget_init, sample_set)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start)
        self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
        time.sleep(delay)  # 0.5 second required for 16 cycles VM sample polling
        self.check_vm_voltage_readback(hbidps, vtarget_init)

    def vtvm_single_force_and_readback_all_sample_engine_flow(self, vtarget_init, rail, hbidps):
        type = 'vm'
        sample_set_list = self.create_all_engine_random_sample_setting(type)
        v_set = {'vout':self.get_vm_expect_voltage(vtarget_init, rail)}
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; vtarget_init {vtarget_init}V; step 0.25V')
        for sample_set in sample_set_list:
            self.Log('info', f'{sample_set}')
        with VTVMRailsSampleEngineEnv(hbidps):
            for sample_set in sample_set_list:
                hbidps.reset_one_sample_engine(type, sample_set['index'])
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_vt_16rails_voltage_force_with_all_engine_tq_list(vtarget_init, sample_set_list)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start)
            time.sleep(0.5) #0.5 second required for 16 cycles VM sample polling
            self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
            self.check_vm_voltage_readback(hbidps, vtarget_init)
            self.check_vm_all_sample_engine_data(hbidps, type, rail, sample_set_list, v_set)

    def vtvm_single_force_and_readback_double_sample_engine_flow(self, vtarget_init, rail, hbidps):
        type = 'vm'
        sample_set0, sample_set1 = self.create_double_random_sample_setting(type, rail)
        v_set = {'vout':self.get_vm_expect_voltage(vtarget_init, rail)}
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; vtarget_init {vtarget_init}V; step 0.25V; sample_set0 {sample_set0}; sample_set1 {sample_set1}')
        with VTVMRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.reset_one_sample_engine(type, sample_set1['index'])
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_vt_16rails_voltage_force_with_double_sample_tq_list(vtarget_init, sample_set0, sample_set1)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start)
            time.sleep(0.5) #0.5 second required for 16 cycles VM sample polling
            self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
            self.check_vm_voltage_readback(hbidps, vtarget_init)
            self.check_sample_engine_data(hbidps, type, sample_set0, v_set)
            self.check_sample_engine_data(hbidps, type, sample_set1, v_set)
            self.check_time_stamp_delta(hbidps, type, sample_set0, sample_set1)

    def vtvm_single_force_and_readback_sample_engine_flow(self, vtarget_init, rail, hbidps, shadow_test=False):
        type = 'vm'
        sample_set = self.create_random_sample_setting(type, rail)
        v_set = {'vout':self.get_vm_expect_voltage(vtarget_init, rail)}
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; vtarget_init {vtarget_init}V; step 0.25V; sample_set {sample_set}')
        with VTVMRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set['index'])
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_vt_16rails_voltage_force_with_sample_tq_list(vtarget_init, sample_set, shadow_test)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start)
            time.sleep(0.5) #0.5 second required for 16 cycles VM sample polling
            self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
            self.check_vm_voltage_readback(hbidps, vtarget_init)
            if shadow_test:
                self.check_sample_engine_dma_data(hbidps, type, sample_set, v_set)
            else:
                self.check_sample_engine_data(hbidps, type, sample_set, v_set)

    def get_vm_expect_voltage(self, vtarget_init, rail):
        return (vtarget_init+rail*0.25)

    def vtvm_single_force_and_readback_poll_registers_flow(self, vtarget_init, hbidps):
        self.Log('info', f'vtvm 16rails; hbidps{hbidps.slot}; vtarget_init {vtarget_init}V; step 0.25V')
        with VTVMRailsSampleEngineEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_vt_16rails_voltage_force_tq_list(vtarget_init)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start, log=True)
            self.Log('info', f'trigger queue execution waiting time: {time_delta}')
            time.sleep(1)
            self.check_vm_voltage_readback(hbidps, vtarget_init)

    def lc_sample_engine_busy_alarm_flow(self, vout, rail, hbidps, busy_state):
        vout0 = vout
        vout1 = vout + 0.3
        iout0 = vout0
        iout1 = vout1
        type = 'lc'
        sample_set0, sample_set1 = \
            self.create_busy_random_sample_setting(type, rail)
        vi_set0 = {'vout': vout0, 'iout': iout0}
        vi_set1 = {'vout': vout1, 'iout': iout1}
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout0}V;'
                         f' iout {iout0}A; sample_set0 {sample_set0}')
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout1}V;'
                         f' iout {iout1}A; sample_set1 {sample_set1}')
        with LCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.enable_lc_rails_ctrl()
            hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            delay = self.get_hc_lc_sample_engine_delay(type, sample_set0, busy_state)
            self.lc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout0, iout0, sample_set0, delay)
            self.lc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout1, iout1, sample_set1)
            if busy_state:
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, busy_state)
                self.reset_sample_engine_busy_alarm(hbidps, type, sample_set0)
                hbidps.reset_one_sample_engine(type, sample_set0['index'])
                self.lc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout1, iout1, sample_set1)
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, False)
                self.check_sample_engine_data(hbidps, type, sample_set1, vi_set1)
            else:
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, busy_state)
                self.check_sample_engine_data_sequence_runs(hbidps, type, sample_set0, sample_set1, vi_set0, vi_set1)

    def lc_1rail_sample_engine_sequency_repeat_flow(self, vout, rail, hbidps, reset_in_between):
        vout0 = vout
        vout1 = vout + 0.3
        iout0 = vout0
        iout1 = vout1
        type = 'lc'
        sample_set0, sample_set1 = self.create_repeat_random_sample_setting(type, rail)
        vi_set0 = {'vout':vout0, 'iout':iout0}
        vi_set1 = {'vout':vout1, 'iout':iout1}
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout0}V; iout {iout0}A; sample_set0 {sample_set0}')
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout1}V; iout {iout1}A; sample_set1 {sample_set1}')
        with LCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.enable_lc_rails_ctrl()
            hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            self.lc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout0, iout0, sample_set0)
            if reset_in_between:
                hbidps.reset_one_sample_engine(type, sample_set1['index'])
            self.lc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout1, iout1, sample_set1)
            if reset_in_between:
                self.check_sample_engine_data(hbidps, type, sample_set1, vi_set1)
            else:
                self.check_sample_engine_data_sequence_runs(hbidps, type, sample_set0, sample_set1, vi_set0, vi_set1)

    def lc_rail_sample_engine_repeat_tq_flow(self, hbidps, rail, vout, iout, sample_set, delay=3.2):
        tq_list = self.create_lc_one_rail_operation_with_sample_tq_list(rail, vout, sample_set)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
        self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
        time.sleep(delay)  # 3.2 second required for 16 cycles sample polling
        self.check_lc_vout_iout_readback(hbidps, vout, iout, rail)
        self.check_lc_polling_readback(hbidps, vout, iout, rail)

    def general_lc_1rail_off_to_on_dtb_feedback_all_sample_engine_flow(self, vout, rail, hbidps):
        iout = vout / 1.0
        type = 'lc'
        sample_set_list = self.create_all_engine_random_sample_setting(type)
        vi_set = {'vout':vout, 'iout':iout}
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A')
        for sample_set in sample_set_list:
            self.Log('info', f'{sample_set}')
        with LCRailsSampleEngineEnv(hbidps):
            for sample_set in sample_set_list:
                hbidps.reset_one_sample_engine(type, sample_set['index'])
            hbidps.enable_lc_rails_ctrl()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_lc_rails_operation_with_all_engine_tq_list(rail, vout, sample_set_list)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
            self.Log('info',f'tq execution waiting time: {time_delta:.3f}s')
            time.sleep(3.2) #3.2 second required for 16 cycles sample polling
            self.check_lc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_lc_polling_readback(hbidps, vout, iout, rail)
            self.check_hc_lc_all_sample_engine_data(hbidps, type, rail, sample_set_list, vi_set)

    def general_lc_1rail_off_to_on_dtb_feedback_double_sample_engine_flow(self, vout, rail, hbidps):
        iout = vout / 1.0
        type = 'lc'
        sample_set0, sample_set1 = self.create_double_random_sample_setting(type, rail)
        vi_set = {'vout':vout, 'iout':iout}
        vi_set_ref = {'vout':0, 'iout':0}
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A; sample_set0 {sample_set0}; sample_set1 {sample_set1}')
        with LCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.reset_one_sample_engine(type, sample_set1['index'])
            hbidps.enable_lc_rails_ctrl()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_lc_one_rail_operation_with_double_engine_tq_list(rail, vout, sample_set0, sample_set1)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
            self.Log('info',f'tq execution waiting time: {time_delta:.3f}s')
            time.sleep(3.2) #3.2 second required for 16 cycles LC sample polling
            self.check_lc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_lc_polling_readback(hbidps, vout, iout, rail)
            self.check_sample_engine_data(hbidps, type, sample_set0, vi_set)
            self.check_sample_engine_data(hbidps, type, sample_set1, vi_set_ref)
            self.check_time_stamp_delta(hbidps, type, sample_set0, sample_set1)

    def general_lc_1rail_off_to_on_dtb_feedback_sample_engine_flow(self, vout, rail, hbidps, shadow_test=False):
        iout = vout
        type = 'lc'
        sample_set = self.create_random_sample_setting(type, rail)
        vi_set = {'vout':vout, 'iout':iout}
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A; sample_set {sample_set}')
        with LCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set['index'])
            hbidps.enable_lc_rails_ctrl()
            hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_lc_one_rail_operation_with_sample_tq_list(rail, vout, sample_set, shadow_test)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
            self.Log('info',f'tq execution waiting time: {time_delta:.3f}s')
            time.sleep(3.2) #3.2 second required for 16 cycles sample polling
            self.check_lc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_lc_polling_readback(hbidps, vout, iout, rail)
            if shadow_test:
                self.check_sample_engine_dma_data(hbidps, type, sample_set, vi_set)
            else:
                self.check_sample_engine_data(hbidps, type, sample_set, vi_set)

    def general_lc_1rail_off_to_on_dtb_feedback_poll_registers_flow(self, vout, rail, hbidps):
        vout_initial = None
        iout = vout
        self.Log('info', f'lc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A')
        with LCRailsSampleEngineEnv(hbidps):
            hbidps.enable_lc_rails_ctrl()
            hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_lc_one_rail_operation_tq_list(rail=rail, vout=vout)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
            time.sleep(1)
            self.check_lc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_lc_polling_readback(hbidps, vout, iout, rail)

    def hc_sample_engine_busy_alarm_flow(self, vout, rail, hbidps, busy_state):
        vout0 = vout
        vout1 = vout + 0.3
        iout0 = vout0 / 0.5
        iout1 = vout1 / 0.5
        type = 'hc'
        sample_set0, sample_set1 = \
            self.create_busy_random_sample_setting(type, rail)
        vi_set0 = {'vout': vout0, 'iout': iout0}
        vi_set1 = {'vout': vout1, 'iout': iout1}
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout0}V;'
                         f' iout {iout0}A; sample_set0 {sample_set0}')
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout1}V;'
                         f' iout {iout1}A; sample_set1 {sample_set1}')
        with HCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.enable_hc_run_pins()
            hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            delay = self.get_hc_lc_sample_engine_delay(type, sample_set0, busy_state)
            self.hc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout0, iout0, sample_set0, delay)
            self.hc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout1, iout1, sample_set1)
            if busy_state:
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, busy_state)
                self.reset_sample_engine_busy_alarm(hbidps, type, sample_set0)
                hbidps.reset_one_sample_engine(type, sample_set0['index'])
                self.hc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout1, iout1, sample_set1)
                self.check_sample_engine_data(hbidps, type, sample_set1, vi_set1)
            else:
                self.check_sample_engine_busy_alarm(hbidps, type, sample_set0, False)
                self.check_sample_engine_data_sequence_runs(hbidps, type, sample_set0, sample_set1, vi_set0, vi_set1)

    def get_hc_lc_sample_engine_delay(self, type, sample_set, busy_state):
        if type == 'hc':
            sample_rate = HC_ENGINE_SAMPLE_RATE_US
        elif type == 'lc':
            sample_rate = LC_ENGINE_SAMPLE_RATE_US
        else:
            self.Log('error', 'illegal sample engine delay type ' + type)
            sample_rate = 0
        sample_count = sample_set['sample_count']
        if busy_state:
            delay_time = (sample_count - 2) * (sample_rate/1000000.0)
        else:
            delay_time = (sample_count + 2) * (sample_rate/1000000.0)
        return delay_time

    def get_vm_sample_engine_delay(self, type, sample_set, busy_state):
        if type == 'vm':
            sample_rate = VM_ENGINE_SAMPLE_RATE_US
        else:
            self.Log('error', 'illegal sample engine delay type ' + type)
            sample_rate = 0
        sample_count = sample_set['sample_count']
        if busy_state:
            delay_time_us = (sample_count - 40) * (sample_rate)
        else:
            delay_time_us = (sample_count + 40) * (sample_rate)
        return delay_time_us

    def hc_1rail_sample_engine_sequency_repeat_flow(self, vout, rail, hbidps, reset_in_between):
        vout0 = vout
        vout1 = vout + 0.3
        iout0 = vout0 / 0.5
        iout1 = vout1 / 0.5
        type = 'hc'
        sample_set0, sample_set1 = \
            self.create_repeat_random_sample_setting(type, rail)
        vi_set0 = {'vout':vout0, 'iout':iout0}
        vi_set1 = {'vout':vout1, 'iout':iout1}
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout0}V; iout {iout0}A; sample_set0 {sample_set0}')
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout1}V; iout {iout1}A; sample_set1 {sample_set1}')
        with HCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.enable_hc_run_pins()
            hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            self.hc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout0, iout0, sample_set0)
            if reset_in_between:
                hbidps.reset_one_sample_engine(type, sample_set1['index'])
            self.hc_rail_sample_engine_repeat_tq_flow(hbidps, rail, vout1, iout1, sample_set1)
            if reset_in_between:
                self.check_sample_engine_data(hbidps, type, sample_set1, vi_set1)
            else:
                self.check_sample_engine_data_sequence_runs(hbidps, type, sample_set0, sample_set1, vi_set0, vi_set1)

    def hc_rail_sample_engine_repeat_tq_flow(self, hbidps, rail, vout, iout, sample_set, delay=2.2):
        tq_list = self.create_hc_one_rail_operation_with_sample_tq_list(rail, vout, sample_set)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
        self.Log('info', f'tq execution waiting time: {time_delta:.3f}s')
        time.sleep(delay)  # 2.2 second required for 16 cycles sample polling
        self.check_hc_vout_iout_readback(hbidps, vout, iout, rail)
        self.check_hc_polling_readback(hbidps, vout, iout, rail)

    def general_hc_1rail_off_to_on_dtb_feedback_all_sample_engine_flow(self, vout, rail, hbidps):
        iout = vout / 0.5
        type = 'hc'
        sample_set_list = self.create_all_engine_random_sample_setting(type)
        vi_set = {'vout':vout, 'iout':iout}
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A')
        for sample_set in sample_set_list:
            self.Log('info', f'{sample_set}')
        with HCRailsSampleEngineEnv(hbidps):
            for sample_set in sample_set_list:
                hbidps.reset_one_sample_engine(type, sample_set['index'])
            hbidps.enable_hc_run_pins()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_rails_operation_with_all_engine_tq_list(rail, vout, sample_set_list)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info',f'tq execution waiting time: {time_delta:.3f}s')
            time.sleep(2.2) #2.2 second required for 16 cycles sample polling
            self.check_hc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_hc_polling_readback(hbidps, vout, iout, rail)
            self.check_hc_lc_all_sample_engine_data(hbidps, type, rail, sample_set_list, vi_set)

    def general_hc_1rail_off_to_on_dtb_feedback_double_sample_engine_flow(self, vout, rail, hbidps):
        iout = vout / 0.5
        type = 'hc'
        sample_set0, sample_set1 = self.create_double_random_sample_setting(type, rail)
        vi_set = {'vout':vout, 'iout':iout}
        vi_set_ref = {'vout':0, 'iout':0}
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A; sample_set0 {sample_set0}; sample_set1 {sample_set1}')
        with HCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set0['index'])
            hbidps.reset_one_sample_engine(type, sample_set1['index'])
            hbidps.enable_hc_run_pins()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_one_rail_operation_with_double_engine_tq_list(rail, vout, sample_set0, sample_set1)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info',f'tq execution waiting time: {time_delta:.3f}s')
            time.sleep(2.2) #2.2 second required for 16 cycles sample polling
            self.check_hc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_hc_polling_readback(hbidps, vout, iout, rail)
            self.check_sample_engine_data(hbidps, type, sample_set0, vi_set)
            self.check_sample_engine_data(hbidps, type, sample_set1, vi_set_ref)
            self.check_time_stamp_delta(hbidps, type, sample_set0, sample_set1)

    def general_hc_1rail_off_to_on_dtb_feedback_sample_engine_flow(self, vout, rail, hbidps, shadow_test=False):
        iout = vout / 0.5
        type = 'hc'
        sample_set = self.create_random_sample_setting(type, rail)
        vi_set = {'vout':vout, 'iout':iout}
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A; sample_set {sample_set}')
        with HCRailsSampleEngineEnv(hbidps):
            hbidps.reset_one_sample_engine(type, sample_set['index'])
            hbidps.enable_hc_run_pins()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_one_rail_operation_with_sample_tq_list(rail, vout, sample_set, shadow_test)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info',f'tq execution waiting time: {time_delta:.3f}s')
            time.sleep(2.2) #2.2 second required for 16 cycles sample polling
            self.check_hc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_hc_polling_readback(hbidps, vout, iout, rail)
            if shadow_test:
                self.check_sample_engine_dma_data(hbidps, type, sample_set, vi_set)
            else:
                self.check_sample_engine_data(hbidps, type, sample_set, vi_set)

    def general_hc_1rail_off_to_on_dtb_feedback_poll_registers_flow(self, vout, rail, hbidps):
        iout = vout / 0.5
        self.Log('info', f'hc rail {rail}; hbidps{hbidps.slot}; vout {vout}V; iout {iout}A')
        with HCRailsSampleEngineEnv(hbidps):
            hbidps.enable_hc_run_pins()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_one_rail_operation_tq_list(rail, vout)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            print(time_delta)
            time.sleep(1)
            self.check_hc_vout_iout_readback(hbidps, vout, iout, rail)
            self.check_hc_polling_readback(hbidps, vout, iout, rail)

    def create_vt_16rails_voltage_force_with_all_engine_tq_list(self, vtarget_init, sample_set_list):
        tq_generator = TriggerQueueCommands()
        vt_active_rails = 0xFFFF
        pwr_state = 0b00
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, vt_active_rails)
        for vt_rail_index in range(16):
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE + vt_rail_index)
            dac_reg_write = int((vt_rail_index*0.25+vtarget_init) / (2 * VT_REF_VOLTAGE / 0x10000))
            tq_generator.add_command(tq_cmd.vtarg_set, vt_rail, dac_reg_write)
            tq_generator.add_command(tq_cmd.vtarg_pwr, vt_rail, pwr_state)
        for sample_set in sample_set_list:
            sample_device = self.get_tq_vm_sample_device(sample_set)
            tq_generator.add_command(tq_cmd.delay, sample_device, 200)
            tq_generator.add_command(tq_cmd.smp_count, sample_device, sample_set['sample_count'])
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, sample_set['sample_ratio'])
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, sample_set['sample_metadata'])
            tq_generator.add_command(tq_cmd.smp_select, sample_device, (0x80_00_00_00_00+sample_set['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_vt_16rails_voltage_force_with_double_sample_tq_list(self, vtarget_init, sample_engine_set0, sample_engine_set1):
        tq_generator = TriggerQueueCommands()
        vt_active_rails = 0xFFFF
        pwr_state = 0b00
        sample_device_a = self.get_tq_vm_sample_device(sample_engine_set0)
        sample_device_b = self.get_tq_vm_sample_device(sample_engine_set1)
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, vt_active_rails)
        for vt_rail_index in range(16):
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE + vt_rail_index)
            dac_reg_write = int((vt_rail_index*0.25+vtarget_init) / (2 * VT_REF_VOLTAGE / 0x10000))
            tq_generator.add_command(tq_cmd.vtarg_set, vt_rail, dac_reg_write)
            tq_generator.add_command(tq_cmd.vtarg_pwr, vt_rail, pwr_state)
        tq_generator.add_command(tq_cmd.delay, sample_device_a, sample_engine_set0['delay']+200)
        tq_generator.add_command(tq_cmd.smp_count, sample_device_a, sample_engine_set0['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device_a, sample_engine_set0['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device_a, sample_engine_set0['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device_a, (0x80_00_00_00_00+sample_engine_set0['rail_select']))
        tq_generator.add_command(tq_cmd.delay, sample_device_b, 200)
        tq_generator.add_command(tq_cmd.smp_count, sample_device_b, sample_engine_set1['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device_b, sample_engine_set1['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device_b, sample_engine_set1['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device_b, (0x80_00_00_00_00+sample_engine_set1['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_vt_16rails_voltage_force_with_sample_busy_tq_list(self, vtarget_init, sample_engine_set0, sample_engine_set1, delay):
        tq_generator = TriggerQueueCommands()
        vt_active_rails = 0xFFFF
        pwr_state = 0b00
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, vt_active_rails)
        for vt_rail_index in range(16):
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE + vt_rail_index)
            dac_reg_write = int((vt_rail_index*0.25+vtarget_init) / (2 * VT_REF_VOLTAGE / 0x10000))
            tq_generator.add_command(tq_cmd.vtarg_set, vt_rail, dac_reg_write)
            tq_generator.add_command(tq_cmd.vtarg_pwr, vt_rail, pwr_state)
        sample_device0 = self.get_tq_vm_sample_device(sample_engine_set0)
        tq_generator.add_command(tq_cmd.delay, sample_device0, VT_RISE_DELAY_US)
        tq_generator.add_command(tq_cmd.smp_count, sample_device0, sample_engine_set0['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device0, sample_engine_set0['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device0, sample_engine_set0['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device0, (0x80_00_00_00_00+sample_engine_set0['rail_select']))
        sample_device1 = self.get_tq_vm_sample_device(sample_engine_set1)
        tq_generator.add_command(tq_cmd.delay, sample_device1, VT_RISE_DELAY_US + delay)
        tq_generator.add_command(tq_cmd.smp_count, sample_device1, sample_engine_set1['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device1, sample_engine_set1['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device1, sample_engine_set1['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device1, (0x80_00_00_00_00+sample_engine_set1['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_vt_16rails_voltage_force_with_sample_tq_list(self, vtarget_init, sample_engine_set, shadow_test=False):
        tq_generator = TriggerQueueCommands()
        vt_active_rails = 0xFFFF
        pwr_state = 0b00
        sample_device = self.get_tq_vm_sample_device(sample_engine_set)
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, vt_active_rails)
        for vt_rail_index in range(16):
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE + vt_rail_index)
            dac_reg_write = int((vt_rail_index*0.25+vtarget_init) / (2 * VT_REF_VOLTAGE / 0x10000))
            tq_generator.add_command(tq_cmd.vtarg_set, vt_rail, dac_reg_write)
            tq_generator.add_command(tq_cmd.vtarg_pwr, vt_rail, pwr_state)
        tq_generator.add_command(tq_cmd.delay, sample_device, VT_RISE_DELAY_US)
        if shadow_test:
            tq_generator.add_command(tq_cmd.smp_count, sample_device, 300)
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, 10)
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, 100)
        tq_generator.add_command(tq_cmd.smp_count, sample_device, sample_engine_set['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device, sample_engine_set['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device, sample_engine_set['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device, (0x80_00_00_00_00+sample_engine_set['rail_select']))
        if shadow_test:
            tq_generator.add_command(tq_cmd.smp_count, sample_device, 300)
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, 10)
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, 100)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_vt_16rails_voltage_force_tq_list(self, vtarget_init, pwr_state=0b00):
        tq_generator = TriggerQueueCommands()
        vt_active_rails = 0xFFFF
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, vt_active_rails)
        for vt_rail_index in range(16):
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE + vt_rail_index)
            dac_reg_write = int((vt_rail_index*0.25+vtarget_init) / (2 * VT_REF_VOLTAGE / 0x10000))
            tq_generator.add_command(tq_cmd.vtarg_set, vt_rail, dac_reg_write)
            tq_generator.add_command(tq_cmd.vtarg_pwr, vt_rail, pwr_state)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, vt_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_lc_rails_operation_with_all_engine_tq_list(self, rail, vout, sample_set_list):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <= 15) & (0.6 <= vout <= 5):
            lc_active_rails = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, LC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        for sample_set in sample_set_list:
            sample_device = self.get_tq_lc_sample_device(sample_set)
            tq_generator.add_command(tq_cmd.delay, sample_device, 200000)
            tq_generator.add_command(tq_cmd.smp_count, sample_device, sample_set['sample_count'])
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, sample_set['sample_ratio'])
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, sample_set['sample_metadata'])
            tq_generator.add_command(tq_cmd.smp_select, sample_device, (0x80_00_00_00_00+sample_set['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_lc_one_rail_operation_with_double_engine_tq_list(self, rail, vout, sample_engine_set0, sample_engine_set1):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <= 15) & (0.6 <= vout <= 5):
            lc_active_rails = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)
        sample_device_a = self.get_tq_lc_sample_device(sample_engine_set0)
        sample_device_b = self.get_tq_lc_sample_device(sample_engine_set1)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, LC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.delay, lc_rail, 300000)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.delay, sample_device_a, sample_engine_set0['delay'])
        tq_generator.add_command(tq_cmd.smp_count, sample_device_a, sample_engine_set0['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device_a, sample_engine_set0['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device_a, sample_engine_set0['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device_a, (0x80_00_00_00_00+sample_engine_set0['rail_select']))
        tq_generator.add_command(tq_cmd.smp_count, sample_device_b, sample_engine_set1['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device_b, sample_engine_set1['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device_b, sample_engine_set1['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device_b, (0x80_00_00_00_00+sample_engine_set1['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_lc_one_rail_operation_with_sample_tq_list(self, rail, vout, sample_engine_set, shadow_test=False):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <= 15) & (0.6 <= vout <= 5):
            lc_active_rails = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)
        sample_device = self.get_tq_lc_sample_device(sample_engine_set)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, LC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.delay, sample_device, RAIL_RISE_DELAY_US)
        if shadow_test:
            tq_generator.add_command(tq_cmd.smp_count, sample_device, 100)
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, 10)
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, 100)
        tq_generator.add_command(tq_cmd.smp_count, sample_device, sample_engine_set['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device, sample_engine_set['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device, sample_engine_set['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device, (0x80_00_00_00_00+sample_engine_set['rail_select']))
        if shadow_test:
            tq_generator.add_command(tq_cmd.smp_count, sample_device, 100)
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, 10)
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, 100)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_lc_one_rail_operation_tq_list(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <= 15) & (0.6 <= vout <= 5):
            lc_active_rails = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, LC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_active_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_hc_rails_operation_with_all_engine_tq_list(self, rail, vout, sample_set_list):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <=9) & (0.5 <= vout <= 3):
            hc_avtive_rails = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1.5
            uv = 0.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        for sample_set in sample_set_list:
            sample_device = self.get_tq_hc_sample_device(sample_set)
            tq_generator.add_command(tq_cmd.delay, sample_device, 200000)
            tq_generator.add_command(tq_cmd.smp_count, sample_device, sample_set['sample_count'])
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, sample_set['sample_ratio'])
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, sample_set['sample_metadata'])
            tq_generator.add_command(tq_cmd.smp_select, sample_device, (0x80_00_00_00_00+sample_set['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_hc_one_rail_operation_with_double_engine_tq_list(self, rail, vout, sample_engine_set0, sample_engine_set1):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <=9) & (0.5 <= vout <= 3):
            hc_avtive_rails = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1.5
            uv = 0.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        sample_device_a = self.get_tq_hc_sample_device(sample_engine_set0)
        sample_device_b = self.get_tq_hc_sample_device(sample_engine_set1)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.delay, hc_rail, 200000)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.delay, sample_device_a, sample_engine_set0['delay'])
        tq_generator.add_command(tq_cmd.smp_count, sample_device_a, sample_engine_set0['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device_a, sample_engine_set0['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device_a, sample_engine_set0['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device_a, (0x80_00_00_00_00+sample_engine_set0['rail_select']))
        tq_generator.add_command(tq_cmd.smp_count, sample_device_b, sample_engine_set1['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device_b, sample_engine_set1['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device_b, sample_engine_set1['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device_b, (0x80_00_00_00_00+sample_engine_set1['rail_select']))
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_hc_one_rail_operation_with_sample_tq_list(self, rail, vout, sample_engine_set, shadow_test=False):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <=9) & (0.5 <= vout <= 3):
            hc_avtive_rails = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1.5
            uv = 0.0

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        sample_device = self.get_tq_hc_sample_device(sample_engine_set)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.delay, sample_device, RAIL_RISE_DELAY_US)
        if shadow_test:
            tq_generator.add_command(tq_cmd.smp_count, sample_device, 100)
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, 10)
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, 100)
        tq_generator.add_command(tq_cmd.smp_count, sample_device, sample_engine_set['sample_count'])
        tq_generator.add_command(tq_cmd.smp_ratio, sample_device, sample_engine_set['sample_ratio'])
        tq_generator.add_command(tq_cmd.smp_metadata, sample_device, sample_engine_set['sample_metadata'])
        tq_generator.add_command(tq_cmd.smp_select, sample_device, (0x80_00_00_00_00+sample_engine_set['rail_select']))
        if shadow_test:
            tq_generator.add_command(tq_cmd.smp_count, sample_device, 100)
            tq_generator.add_command(tq_cmd.smp_ratio, sample_device, 10)
            tq_generator.add_command(tq_cmd.smp_metadata, sample_device, 100)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def get_tq_vm_sample_device(self, sample_engine_set):
        if sample_engine_set['index'] == 0:
            device = tq_dev.VmeasSampleEngine0
        elif sample_engine_set['index'] == 1:
            device = tq_dev.VmeasSampleEngine1
        elif sample_engine_set['index'] == 2:
            device = tq_dev.VmeasSampleEngine2
        elif sample_engine_set['index'] == 3:
            device = tq_dev.VmeasSampleEngine3
        elif sample_engine_set['index'] == 4:
            device = tq_dev.VmeasSampleEngine4
        return device

    def get_tq_lc_sample_device(self, sample_engine_set):
        if sample_engine_set['index'] == 0:
            device = tq_dev.LCSampleEngine0
        elif sample_engine_set['index'] == 1:
            device = tq_dev.LCSampleEngine1
        elif sample_engine_set['index'] == 2:
            device = tq_dev.LCSampleEngine2
        elif sample_engine_set['index'] == 3:
            device = tq_dev.LCSampleEngine3
        return device

    def get_tq_hc_sample_device(self, sample_engine_set):
        if sample_engine_set['index'] == 0:
            device = tq_dev.HCSampleEngine0
        elif sample_engine_set['index'] == 1:
            device = tq_dev.HCSampleEngine1
        elif sample_engine_set['index'] == 2:
            device = tq_dev.HCSampleEngine2
        elif sample_engine_set['index'] == 3:
            device = tq_dev.HCSampleEngine3
        return device

    def create_hc_one_rail_operation_tq_list(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if (0 <= rail <=9) & (0.5 <= vout <= 3):
            hc_avtive_rails = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1.5
            uv = 0.5

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_avtive_rails)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_random_sample_setting(self, type, rail):
        if (type == 'hc') or (type == 'lc'):
            sample_engine_index = random.randint(0, 3)
        elif type == 'vm':
            sample_engine_index = random.randint(0, 4)
        rail_select = self.create_rail_select(rail)
        sample_count = random.randint(1, 8)
        sample_ratio = random.randint(0, 1)
        sample_metadata = random.randint(0, (2**32)-1)
        sample_set = {'index': sample_engine_index,
                      'rail_select':rail_select,
                      'sample_count':sample_count,
                      'sample_ratio':sample_ratio,
                      'sample_metadata':sample_metadata}
        return sample_set

    def create_busy_random_sample_setting(self, type, rail):
        if (type == 'hc') or (type == 'lc'):
            sample_engine_index = random.randint(0, 3)
            sample_count = random.randint(6, 8)
        elif type == 'vm':
            sample_engine_index = random.randint(0, 4)
            sample_count = random.randint(100, 200)
        rail_select = self.create_rail_select(rail)
        sample_ratio = 0
        sample_metadata = random.randint(0, (2 ** 32) - 1)
        sample_set0 = {'index': sample_engine_index,
                       'rail_select': rail_select,
                       'sample_count': sample_count,
                       'sample_ratio': sample_ratio,
                       'sample_metadata': sample_metadata}
        sample_count = random.randint(1, 8)
        sample_ratio = random.randint(0, 1)
        sample_metadata = random.randint(0, (2 ** 32) - 1)
        sample_set1 = {'index': sample_engine_index,
                       'rail_select': rail_select,
                       'sample_count': sample_count,
                       'sample_ratio': sample_ratio,
                       'sample_metadata': sample_metadata}
        return sample_set0, sample_set1

    def create_repeat_random_sample_setting(self, type, rail):
        if (type == 'hc') or (type == 'lc'):
            sample_engine_index = random.randint(0, 3)
        elif type == 'vm':
            sample_engine_index = random.randint(0, 4)
        rail_select = self.create_rail_select(rail)
        sample_count = random.randint(1, 8)
        sample_ratio = random.randint(0, 1)
        sample_metadata = random.randint(0, (2**32)-1)
        sample_set0 = {'index': sample_engine_index,
                      'rail_select':rail_select,
                      'sample_count':sample_count,
                      'sample_ratio':sample_ratio,
                      'sample_metadata':sample_metadata}
        sample_count = random.randint(1, 8)
        sample_ratio = random.randint(0, 1)
        sample_metadata = random.randint(0, (2**32)-1)
        sample_set1 = {'index': sample_engine_index,
                      'rail_select':rail_select,
                      'sample_count':sample_count,
                      'sample_ratio':sample_ratio,
                      'sample_metadata':sample_metadata}
        return sample_set0, sample_set1

    def create_all_engine_random_sample_setting(self, type):
        if type == 'hc':
            engine_list = list(range(4))
            rail_list = list(range(10))
        elif type == 'lc':
            engine_list = list(range(4))
            rail_list = list(range(16))
        elif type == 'vm':
            engine_list = list(range(5))
            rail_list = list(range(16))
        sample_set_list = []
        for engine in engine_list:
            sample_metadata = random.randint(0, (2**32)-1)
            rail_select = self.create_rail_select(random.choice(rail_list))
            sample_set = {'index': engine,
                          'rail_select': rail_select,
                          'sample_count':8,
                          'sample_ratio':0,
                          'sample_metadata':sample_metadata}
            sample_set_list.append(sample_set)
        return sample_set_list

    def create_double_random_sample_setting(self, type, rail):
        if (type == 'hc') or (type == 'lc'):
            sample_engine_index_0 = random.randint(0, 3)
            remain_list = [0, 1, 2, 3]
            remain_list.remove(sample_engine_index_0)
            sample_engine_index_1 = random.choice(remain_list)
        elif type == 'vm':
            sample_engine_index_0 = random.randint(0, 4)
            remain_list = [0, 1, 2, 3, 4]
            remain_list.remove(sample_engine_index_0)
            sample_engine_index_1 = random.choice(remain_list)
        rail_select = self.create_rail_select(rail)
        delay0 = random.choice([400000, 500000])
        delay1 = 0
        sample_metadata = random.randint(0, (2**32)-1)
        sample_set0 = {'index': sample_engine_index_0,
                      'rail_select':rail_select,
                      'sample_count':8,
                      'sample_ratio':0,
                      'sample_metadata':sample_metadata,
                      'delay':delay0}
        sample_metadata = random.randint(0, (2**32)-1)
        sample_set1 = {'index': sample_engine_index_1,
                      'rail_select':rail_select,
                      'sample_count':8,
                      'sample_ratio':0,
                      'sample_metadata':sample_metadata,
                      'delay': delay1}
        return sample_set0, sample_set1

    def create_rail_select(self, rail):
        return (1 << rail)

    def check_vm_voltage_readback(self, hbidps, expect_vm_init):
        for vm_rail in range(16):
            expect_vm = expect_vm_init + vm_rail*0.25
            read_vm = hbidps.hbi_dps_vmeasure_voltage_read(vm_rail)
            poll_vm = hbidps.get_vm_rail_poll_voltage(vm_rail)
            if (read_vm>expect_vm+0.3) or (read_vm<expect_vm-0.3):
                self.Log('error', f'rail{vm_rail} vout expect:{expect_vm}V '
                                  f'read: {read_vm}V')
            else:
                self.Log('debug', f'rail{vm_rail} '
                                 f'vout_pass expect:{expect_vm}V '
                                 f'read: {read_vm}V')
            if (poll_vm>expect_vm+0.3) or (poll_vm<expect_vm-0.3):
                self.Log('error', f'rail{vm_rail} vout expect:{expect_vm}V '
                                  f'poll: {poll_vm}V')
            else:
                self.Log('debug', f'rail{vm_rail} '
                                 f'vout_pass expect:{expect_vm}V '
                                 f'poll: {poll_vm}V')

    def check_lc_vout_iout_readback(self, hbidps, expect_vout, expect_iout, rail):
        read_vout = hbidps.lc_rail_voltage_read(rail)
        read_iout = hbidps.lc_rail_current_read(rail)
        if (read_vout>expect_vout+0.3) or (read_vout<expect_vout-0.3) or \
                (read_iout>expect_iout+0.7) or (read_iout<expect_iout-0.7):
            self.Log('error', f'rail{rail} vout expect:{expect_vout}V '
                              f'read: {read_vout}V; '
                              f'iout expect:{expect_iout}A '
                              f'read: {read_iout}A ')
        else:
            self.Log('debug', f'vout_pass expect:{expect_vout}V '
                             f'read: {read_vout}V; '
                             f'iout_pass expect:{expect_iout}A '
                             f'read: {read_iout}A')

    def check_hc_vout_iout_readback(self, hbidps, expect_vout, expect_iout, rail):
        read_vout = hbidps.hc_rail_voltage_read(rail)
        read_iout = hbidps.hc_rail_current_read(rail)
        hbidps.read_hc_rails_fpga_status()
        if (read_vout>expect_vout+0.3) or (read_vout<expect_vout-0.3) or \
                (read_iout>expect_iout+0.7) or (read_iout<expect_iout-0.7):
            self.Log('error', f'rail{rail} vout expect:{expect_vout}V '
                              f'read: {read_vout}V; '
                              f'iout expect:{expect_iout}A '
                              f'read: {read_iout}A ')
            hbidps.read_one_hc_rail_device_status(rail)
        else:
            self.Log('debug', f'vout_pass expect:{expect_vout}V '
                             f'read: {read_vout}V; '
                             f'iout_pass expect:{expect_iout}A '
                             f'read: {read_iout}A')

    def check_lc_polling_readback(self, hbidps, expect_vout, expect_iout, rail):
        read_vout = hbidps.get_lc_rail_poll_voltage(rail)
        read_iout = hbidps.get_lc_rail_poll_current(rail)
        if (read_vout>expect_vout+0.3) or (read_vout<expect_vout-0.3) or \
                (read_iout>expect_iout+0.7) or (read_iout<expect_iout-0.7):
            self.Log('error', f'polling rail{rail} vout expect:{expect_vout}V '
                              f'read: {read_vout}V; '
                              f'iout expect:{expect_iout}A '
                              f'read: {read_iout}A ')
        else:
            self.Log('debug', f'polling vout_pass expect:{expect_vout}V '
                             f'read: {read_vout}V; '
                             f'iout_pass expect:{expect_iout}A '
                             f'read: {read_iout}A')

    def check_hc_polling_readback(self, hbidps, expect_vout, expect_iout, rail):
        read_vout = hbidps.get_hc_rail_poll_voltage(rail)
        read_iout = hbidps.get_hc_rail_poll_current(rail)
        if (read_vout>expect_vout+0.3) or (read_vout<expect_vout-0.3) or \
                (read_iout>expect_iout+0.7) or (read_iout<expect_iout-0.7):
            self.Log('error', f'polling rail{rail} vout expect:{expect_vout}V '
                              f'read: {read_vout}V; '
                              f'iout expect:{expect_iout}A '
                              f'read: {read_iout}A ')
        else:
            self.Log('debug', f'polling vout_pass expect:{expect_vout}V '
                             f'read: {read_vout}V; '
                             f'iout_pass expect:{expect_iout}A '
                             f'read: {read_iout}A')

    def check_vm_all_sample_engine_data(self, hbidps, type, rail, expect_set_list, expect_value):
        expect_voltage = expect_value['vout']
        for expect_set in expect_set_list:
            sample_index = 0
            index = expect_set['index']
            self.check_sample_engine_setting(hbidps, type, index, expect_set)
            self.check_sample_engine_header(hbidps, type, index, sample_index, expect_set)
            rail_index = self.get_rail_index_from_select(expect_set['rail_select'])
            expect_value = {'vout':expect_voltage + (rail_index - rail) * 0.25 }
            self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value)

    def get_rail_index_from_select(self, rail_select):
        rail_index = 0
        for i in range(16):
            if (rail_select >> i) == 1:
                rail_index = i
                break
        else:
            self.Log('error', f'wrong rails select coding 0x{rail_select:08x}')
        return rail_index

    def check_hc_lc_all_sample_engine_data(self, hbidps, type, rail, expect_set_list, expect_value):
        expect_voltage = expect_value['vout']
        expect_value_no_current = {'vout':expect_voltage, 'iout':0}
        expect_value_off = {'vout':0, 'iout':0}
        if type == 'hc':
            on_mask = self.get_hc_on_rails_mask_from_dtb_connection(rail)
        elif type == 'lc':
            on_mask = 0xFFFF
        for expect_set in expect_set_list:
            sample_index = 0
            index = expect_set['index']
            self.check_sample_engine_setting(hbidps, type, index, expect_set)
            self.check_sample_engine_header(hbidps, type, index, sample_index, expect_set)
            if expect_set['rail_select'] & (1<<rail):
                self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value)
            elif expect_set['rail_select'] & on_mask:
                self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value_no_current)
            else:
                self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value_off)

    def get_hc_on_rails_mask_from_dtb_connection(self, rail):
        if rail > 4:
            return 0b11_1110_0000
        else:
            return 0b00_0001_1111

    def check_sample_engine_busy_alarm(self, hbidps, type, sample_set, busy_state):
        read_hc, read_lc, read_vm = hbidps.get_global_alarm_sample_engines()
        if type == 'hc':
            busy_value = hbidps.get_hc_sample_alarms_value()
            expect_hc, expect_lc, expect_vm = 1, 0, 0
        elif type == 'lc':
            busy_value = hbidps.get_lc_sample_alarms_value()
            expect_hc, expect_lc, expect_vm = 0, 1, 0
        elif type == 'vm':
            busy_value = hbidps.get_vm_sample_alarms_value()
            expect_hc, expect_lc, expect_vm = 0, 0, 1
        else:
            self.Log('error', 'illegal sample engine alarm type ' + type)
            busy_value = 0xF0F0F0F0
            expect_hc, expect_lc, expect_vm = 1, 1, 1
        if busy_state:
            expect_busy_value = 1 << sample_set['index']
        else:
            expect_busy_value = 0
            expect_hc, expect_lc, expect_vm = 0, 0, 0
        if (expect_hc, expect_lc, expect_vm) != (read_hc, read_lc, read_vm):
            self.Log('error', type + f' global sample engine alarm mismatch, '
                                     f'hc_lc_vm expect {(expect_hc, expect_lc, expect_vm)}, '
                                     f'read {(read_hc, read_lc, read_vm)} ')
        else:
            self.Log('info', type + f' global sample engine alarm match, '
                                    f'hc_lc_vm expect {(expect_hc, expect_lc, expect_vm)}, '
                                    f'read {(read_hc, read_lc, read_vm)} ')
        if expect_busy_value != busy_value:
            self.Log('error', type + f' sample engine alarm mismatch, '
                                     f'expect {expect_busy_value:08x}, '
                                     f'read {busy_value:08x} ')
        else:
            self.Log('info', type + f' sample engine alarm match, '
                                    f'expect {expect_busy_value:08x}, '
                                    f'read {busy_value:08x} ')

    def reset_sample_engine_busy_alarm(self, hbidps, type, sample_set):
        if type == 'hc':
            hbidps.reset_hc_sample_engine_busy()
        elif type == 'lc':
            hbidps.reset_lc_sample_engine_busy()
        elif type == 'vm':
            hbidps.reset_vm_sample_engine_busy()
        self.Log('info', type + ' sample engine busy reset')
        self.check_sample_engine_busy_alarm(hbidps, type, sample_set, False)

    def check_sample_engine_data_sequence_runs(self, hbidps, type, expect_set0, expect_set1, expect_value0, expect_value1):
        index = expect_set1['index']
        self.check_sample_engine_setting(hbidps, type, index, expect_set1)
        self.Log('info', 'check sample0')
        sample_index = 0
        self.check_sample_engine_header(hbidps, type, index, sample_index, expect_set0)
        self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value0)
        self.Log('info', 'check sample1')
        sample_index = 1
        self.check_sample_engine_header(hbidps, type, index, sample_index, expect_set1)
        self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value1)

    def check_sample_engine_dma_data(self, hbidps, type, expect_set, expect_value):
        sample_index = 0
        index = expect_set['index']
        self.check_sample_engine_header(hbidps, type, index, sample_index, expect_set)
        self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value)

    def check_sample_engine_data(self, hbidps, type, expect_set, expect_value):
        sample_index = 0
        index = expect_set['index']
        self.check_sample_engine_setting(hbidps, type, index, expect_set)
        self.check_sample_engine_header(hbidps, type, index, sample_index, expect_set)
        self.check_sample_engine_sample(hbidps, type, index, sample_index, expect_value)

    def check_sample_engine_setting(self, hbidps, type, index, expect_set):
        rail_select, sample_count, sample_ratio, sample_metadata = \
            hbidps.get_one_sample_engine_settings(type, index)
        if (expect_set['rail_select'] != rail_select) | \
                (expect_set['sample_count'] != sample_count) | \
                (expect_set['sample_ratio'] != sample_ratio) | \
                (expect_set['sample_metadata'] != sample_metadata):
            self.Log('error', type + f' sample engine {index} register mismatch')
            self.Log('error', f'read rail_select 0x{rail_select:08x}, '
                              f'sample_count {sample_count}, '
                              f'sample_ratio {sample_ratio}, '
                              f'sample_metadata 0x{sample_metadata:08x}')
            self.Log('error', f'expect set {expect_set}')
        else:
            self.Log('debug', type + f' sample engine {index} register match')
            self.Log('debug', f'read rail_select 0x{rail_select:08x}, '
                              f'sample_count {sample_count}, '
                              f'sample_ratio {sample_ratio}, '
                              f'sample_metadata 0x{sample_metadata:08x}')
            self.Log('debug', f'expect set {expect_set}')

    def check_sample_engine_header(self, hbidps, type, index, sample_index, expect_set):
        _, sample_count, sample_select, sample_ratio, sample_metadata = \
            hbidps.get_one_sample_engine_header(type, index, sample_index)
        rail_select = self.decode_sample_select_to_rail_select(type, sample_select)
        if (expect_set['rail_select'] != rail_select) | \
                (expect_set['sample_count'] != sample_count) | \
                (expect_set['sample_ratio'] != sample_ratio) | \
                (expect_set['sample_metadata'] != sample_metadata):
            self.Log('error', type + f' sample engine {index} dma header mismatch')
            self.Log('error', f'read rail_select 0x{rail_select:08x}, '
                              f'sample_count {sample_count}, '
                              f'sample_ratio {sample_ratio}, '
                              f'sample_metadata 0x{sample_metadata:08x}')
            self.Log('error', f'expect set {expect_set}')
        else:
            self.Log('debug', type + f' sample engine {index} dma header match')
            self.Log('debug', f'read rail_select 0x{rail_select:08x}, '
                              f'sample_count {sample_count}, '
                              f'sample_ratio {sample_ratio}, '
                              f'sample_metadata 0x{sample_metadata:08x}')
            self.Log('debug', f'expect set {expect_set}')

    def check_time_stamp_delta(self, hbidps, type, sample_set0, sample_set1):
        index0 = sample_set0['index']
        sample_data0 = hbidps.get_one_sample_engine_sample(type, index0, 0)
        time_stamp0= (sample_data0 >> 31) & 0xFFFF_FFFF
        index1 = sample_set1['index']
        sample_data1 = hbidps.get_one_sample_engine_sample(type, index1, 0)
        time_stamp1= (sample_data1 >> 31) & 0xFFFF_FFFF
        self.Log('info', f'time_delta {(time_stamp0-time_stamp1)//1000}ms')

    def check_sample_engine_sample(self, hbidps, type, index, sample_index, expect_set):
        sample_data = hbidps.get_one_sample_engine_sample(type, index, sample_index)
        if (type == 'lc') or (type == 'hc'):
            vout_entry = sample_data & 0xFFFF_FFFF
            read_vout = hbidps.decode_single_precision_floating_value(vout_entry)
            iout_entry = (sample_data>>(32*2)) & 0xFFFF_FFFF
            read_iout = hbidps.decode_single_precision_floating_value(iout_entry)
            if (expect_set['vout'] + 0.3 < read_vout) | \
                    (expect_set['vout'] -0.3 > read_vout) | \
                    (expect_set['iout'] +0.7 < read_iout) | \
                    (expect_set['iout'] -0.7 > read_iout):
                self.Log('error', type + f' sample engine {index} dma data mismatch')
                self.Log('error', f'read vout {read_vout:.3f}, '
                                  f'iout {read_iout:.3f}')
                self.Log('error', f'expect set {expect_set}')
                hbidps.get_one_sample_engine_sample(type, index, sample_index, True)
            else:
                self.Log('debug', type + f' sample engine {index} dma data match')
                self.Log('debug', f'read vout {read_vout:.3f}, '
                                  f'iout {read_iout:.3f}')
                self.Log('debug', f'expect set {expect_set}')
                hbidps.get_one_sample_engine_sample(type, index, sample_index, True)
        elif type == 'vm':
            vout_entry = (sample_data) & 0xFFFF_FFFF
            read_vout = hbidps.decode_single_precision_floating_value(vout_entry)
            if (expect_set['vout']+0.3 < read_vout) | \
                    (expect_set['vout'] - 0.3 > read_vout):
                self.Log('error', type + f' sample engine {index} dma data mismatch')
                self.Log('error', f'read vout {read_vout:.3f}')
                self.Log('error', f'expect set {expect_set}')
                hbidps.get_one_sample_engine_sample(type, index, sample_index, True)
            else:
                self.Log('debug', type + f' sample engine {index} dma data match')
                self.Log('debug', f'read vout {read_vout:.3f}')
                self.Log('debug', f'expect set {expect_set}')
                hbidps.get_one_sample_engine_sample(type, index, sample_index, True)

    def decode_sample_select_to_rail_select(self, type, sample_select):
        if (type == 'lc') or (type == 'hc'):
            rail_select = 0
            for i in range(16):
                if sample_select & (1<<(2*i)):
                    rail_select = rail_select + (1<<i)
        elif type == 'vm':
            rail_select = sample_select
        return rail_select

class HCRailsSampleEngineEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_hc_rails_start_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_hc_rails_to_safe_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)


class LCRailsSampleEngineEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_lc_rails_start_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.set_lc_rails_to_safe_state()
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)


class VTVMRailsSampleEngineEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_vtarget_rails_start_state()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_vtvm_rails_to_safe_state()
        self.hbidps.turn_off_all_vt_vm_switches()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)
