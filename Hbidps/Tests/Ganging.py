################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing rails ganging function"""

import time
import random
from Common.fval import skip, expected_failure
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands
from Hbidps.Tests.HbidpsTest import HbidpsTest


HC_TQ_RAIL_ID_BASE = 0x30
HC_TQ_FDT = 0x1000
HC_TQ_I_HI = 10
VRANGE_LOW = 0xFABC
VRANGE_HIGH = 0x028A
HC_RAIL_LIST = list(range(10))


class Functional(HbidpsTest):
    """Check rails ganging function. (one DPS and multiple DPSs)
    Need more info to create tests.

    Following tests cases are used to verify Phase2.1 Sync Vrange Feature
    """

    def DirectedGangingHCVrangeSingleSyncFollowerVrangeTest(self):
        hbidps = random.choice(self.hbidps_list)
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            sync_set = self.follow_set_for_single_sync_follower_vrange(True)
            tq_list = self.create_hc_1sync_follower_vrange_tq_list(sync_set, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'time_delta {time_delta:.3f}')
            self.check_sync_follower_vrange_result(hbidps, sync_set, 0, True)

    def DirectedGangingHCVrangeSingleSyncLeaderMultiFollowerFailTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, _ = self.follow_set_for_single_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)

    def DirectedGangingHCVrangeNearbySyncLeaderMultiFollowerFailTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, _ = self.follow_set_for_nearby_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)

    def DirectedGangingHCVrangeSingleSyncFollowerAlarmPassTest(self):
        hbidps = random.choice(self.hbidps_list)
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            sync_set = self.follow_set_for_single_sync_follower_alarm_postive(True)
            tq_list = self.create_hc_1sync_follower_vrange_tq_list(sync_set, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'time_delta {time_delta:.3f}')
            self.check_sync_follower_vrange_result(hbidps, sync_set, 0, True)

    def DirectedGangingHCVrangeSingleSyncLeaderMultiFollowerOnOffPassTest(self):
        hbidps = random.choice(self.hbidps_list)
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            sync_set = self.follow_set_for_single_sync_vrange_alarm_on_off_negative(True)
            tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'time_delta {time_delta:.3f}')
            self.check_sync_leader_vrange_result(hbidps, sync_set, 0, True)

    def DirectedGangingHCVrangeSingleSyncLeaderMultiFollowerPassTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set = self.follow_set_for_single_sync_vrange_alarm_negative(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 0, True)

    def DirectedGangingHCVrangeSingleSyncLeaderMultiFollowerOnOffOnPassTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set = self.follow_set_for_single_sync_vrange_alarm_onoff_negative(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 0, True)

    def DirectedGangingHCVrangeNearbySyncLeaderMultiFollowerPassTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set = self.follow_set_for_nearby_sync_vrange_alarm_negative(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 0, True)

    def DirectedHCSingalFollowerRailSelfVrangeAlarmTest(self):
        self.sync_follower_or_non_ganging_rail_selfvrange_flow(True)

    def DirectedHCSingalRailSelfVrangeAlarmTest(self):
        self.sync_follower_or_non_ganging_rail_selfvrange_flow(False)

    def DirectedHCSingalFollowerRailNearVrangeAlarmTest(self):
        self.sync_follower_or_non_ganging_rail_nearvrange_flow(True)

    def DirectedHCSingalRailNearVrangeAlarmTest(self):
        self.sync_follower_or_non_ganging_rail_nearvrange_flow(False)

    def DirectedHCSingalFollowerRailCheckVrangeAlarmTest(self):
        self.sync_follower_or_non_ganging_rail_checkvrange_flow(True)

    def DirectedHCSingalRailCheckVrangeAlarmTest(self):
        self.sync_follower_or_non_ganging_rail_checkvrange_flow(False)

    def DirectedGangingHCVrangeSingleSyncLeaderMultiFollowerFailSelfVrangeTest(self):
        hbidps = random.choice(self.hbidps_list)
        vout = 1
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                # hbidps.enable_hc_run_pins()
                sync_set, rail = self.follow_set_for_single_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_selfvrange_tq_list(vout, sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                time.sleep(1)
                power_state=hbidps.get_hc_power_state()
                self.Log('info', f'power state 0x{power_state:05x}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)
                self.check_original_vrange_alarm(hbidps, rail, True, False, False, True)

    def DirectedGangingHCVrangeSingleSyncLeaderMultiFollowerFailSelfVrangeSeparateTest(self):
        hbidps = random.choice(self.hbidps_list)
        vout = 1
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, rail = self.follow_set_for_single_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_selfvrange_tq_list(vout, [sync_set[0]], True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                time.sleep(1)
                power_state=hbidps.get_hc_power_state()
                self.Log('info', f'power state 0x{power_state:05x}')
                tq_list = self.create_hc_1sync_leader_vrange_selfvrange_tq_list(vout, [sync_set[1]], True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                time.sleep(1)
                power_state=hbidps.get_hc_power_state()
                self.Log('info', f'power state 0x{power_state:05x}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)
                self.check_original_vrange_alarm(hbidps, rail, True, False, False, True)

    @expected_failure('FPGA Function Limitation')
    def DirectedGangingHCVrangeNearbySyncLeaderMultiFollowerNearVrangeFailTest(self):
        hbidps = random.choice(self.hbidps_list)
        vout = 1
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, rail = self.follow_set_for_nearby_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_nearvrange_tq_list(vout, sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                time.sleep(1)
                power_state=hbidps.get_hc_power_state()
                self.Log('info', f'power state 0x{power_state:05x}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)
                self.check_original_vrange_alarm(hbidps, rail, False, True, False, True)

    def DirectedGangingHCVrangeNearbySyncLeaderMultiFollowerNearVrangeFailSeparateTest(self):
        hbidps = random.choice(self.hbidps_list)
        vout = 1
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, rail = self.follow_set_for_nearby_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_nearvrange_tq_list(vout, [sync_set[0]], True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'first time_delta {time_delta:.3f}')
                time.sleep(1)
                power_state=hbidps.get_hc_power_state()
                self.Log('info', f'power state 0x{power_state:05x}')
                tq_list = self.create_hc_1sync_leader_vrange_nearvrange_tq_list(vout, [sync_set[1]], True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'second time_delta {time_delta:.3f}')
                time.sleep(1)
                power_state=hbidps.get_hc_power_state()
                self.Log('info', f'power state 0x{power_state:05x}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)
                self.check_original_vrange_alarm(hbidps, rail, False, True, False, True)

    def DirectedGangingHCVrangeAlarmRestTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, _ = self.follow_set_for_single_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)
                self.reset_sync_vrange_alarm(hbidps)
                self.check_sync_vrange_alarm(hbidps, sync_set, 0, True)

    @expected_failure('FPGA Function Limitation')
    def DirectedGangingHCVrangeAlarmOverTemperatureFoldingTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set = self.follow_set_for_single_sync_vrange_alarm_negative(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 0, True)
                hbidps.log_sync_channel_mask_value()
                self.create_over_temperature_alarm(hbidps)
                hbidps.check_cleared_sync_channel_mask_value(True)

    @expected_failure('FPGA Function Limitation')
    def DirectedGangingHCVrangeAlarmOtherVrangeFoldingTest(self):
        hbidps = random.choice(self.hbidps_list)
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            sync_set = self.follow_set_for_single_sync_vrange_folding_reset(True)
            tq_list = self.create_hc_1_rail_operation_tq_list(1, sync_set[0:3], True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'time_delta {time_delta:.3f}')
            time.sleep(2)
            self.check_clean_sync_vrange_alarm(hbidps, True)
            hbidps.log_sync_channel_mask_value()
            self.check_original_vrange_alarm(hbidps, 6, True, False, False, True)
            hbidps.detect_global_alarm()
            hbidps.reset_global_alarm()
            time.sleep(2)
            tq_list = self.create_hc_1_rail_operation_tq_list(1, [sync_set[3]], True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'time_delta {time_delta:.3f}')
            time.sleep(2)
            self.check_sync_leader_vrange_result(hbidps, [sync_set[3]], 0, True)
            self.check_original_vrange_alarm(hbidps, 0, False, False, False, True)
            hbidps.detect_global_alarm()

    def DirectedGangingHCVrangeAlarmSafeStateTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set = self.follow_set_for_single_sync_vrange_alarm_negative(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 0, True)
                hbidps.log_sync_channel_mask_value()
                hbidps.set_hc_rails_to_safe_state()
                hbidps.check_cleared_sync_channel_mask_value(True)

    def DirectedGangingHCVrangeAlarmMaskUnfreezeTest(self):
        hbidps = random.choice(self.hbidps_list)
        for mode in ['exclude', 'equal', 'random']:
            with HCGangingEnv(hbidps):
                hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
                sync_set, _ = self.follow_set_for_single_sync_vrange_alarm_postive(mode, True)
                tq_list = self.create_hc_1sync_leader_vrange_tq_list(sync_set, True)
                hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
                hbidps.clear_tq_notify_bits()
                time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
                time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
                self.Log('info', f'time_delta {time_delta:.3f}')
                self.check_sync_leader_vrange_result(hbidps, sync_set, 1, True)
                hbidps.check_reset_sync_channel_mask_value()

    def sync_follower_or_non_ganging_rail_selfvrange_flow(self, ext_clk):
        hbidps = random.choice(self.hbidps_list)
        rail = 0
        self.Log('info', f'rail{rail}')
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_transition_tq_list(rail, 1, VRANGE_LOW, ext_clk, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'TQ completion time {time_delta:.3f}')
            time.sleep(0.3)  # time required to HC rail to power up
            tq_list = self.create_hc_1rail_transition_tq_list(rail, 1, VRANGE_HIGH, ext_clk, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'TQ completion time {time_delta:.3f}')
            self.check_clean_sync_vrange_alarm(hbidps, True)
            self.check_original_vrange_alarm(hbidps, rail, True, False, False, True)
            self.check_cleared_sync_vrange_mask(hbidps, True)

    def sync_follower_or_non_ganging_rail_checkvrange_flow(self, ext_clk):
        hbidps = random.choice(self.hbidps_list)
        rail = 0
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_check_vrange_alarm_tq_list(rail, 1, ext_clk, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'TQ completion time {time_delta:.3f}')
            time.sleep(0.3)  # time required to HC rail to power up
            self.check_clean_sync_vrange_alarm(hbidps, True)
            self.check_original_vrange_alarm(hbidps, rail, False, False, True, True)
            self.check_cleared_sync_vrange_mask(hbidps, True)

    def sync_follower_or_non_ganging_rail_nearvrange_flow(self, ext_clk):
        hbidps = random.choice(self.hbidps_list)
        rail0 = 0
        rail1 = 1
        with HCGangingEnv(hbidps):
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_transition_tq_list(rail0, 1, VRANGE_LOW, ext_clk, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'TQ completion time {time_delta:.3f}')
            time.sleep(0.3)  # time required to HC rail to power up
            tq_list = self.create_hc_1rail_transition_tq_list(rail1, 1, VRANGE_HIGH, ext_clk, True)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            self.Log('info', f'TQ completion time {time_delta:.3f}')
            self.check_clean_sync_vrange_alarm(hbidps, True)
            self.check_original_vrange_alarm(hbidps, rail0, False, True, False, True)
            self.check_cleared_sync_vrange_mask(hbidps, True)

    def create_over_temperature_alarm(self, hbidps):
        temp = hbidps.get_max6628_temp(0)
        self.Log('info', f'init temp device0 read {temp}')
        temp_upper_alarm = temp - 100
        hbidps.change_temp_upper_limit(0, temp_upper_alarm)
        time.sleep(1)
        hbidps.detect_global_alarm()
        self.check_hc_rails_all_fold(hbidps)
        hbidps.restore_alarm_settings()
        hbidps.reset_global_alarm()

    def check_hc_rails_all_fold(self, hbidps):
        fold_state = hbidps.get_hc_rails_fold_state()
        if fold_state != 0x3FF:
            self.Log('error', f'hc not all fold 0x{fold_state:03x}')
        else:
            self.Log('info', f'hc all fold 0x{fold_state:03x}')

    def follow_set_for_single_sync_follower_vrange(self, log=False):
        sync_follower = random.choice(HC_RAIL_LIST)
        vrange = random.choice([VRANGE_LOW, VRANGE_HIGH])
        rail_set = {'act_rail': sync_follower, 'rails': [],
                    'turn_on': 1, 'vrange': vrange}
        sync_set = [rail_set]
        if log:
            self.Log('info', f'{sync_set}')
        return sync_set

    def follow_set_for_single_sync_follower_alarm_postive(self, log=False):
        sync_follower = random.choice(HC_RAIL_LIST)
        rail_set_a = {'act_rail': sync_follower, 'rails': [],
                      'turn_on': 1, 'vrange': VRANGE_LOW}
        rail_set_b = {'act_rail': sync_follower, 'rails': [],
                      'turn_on': 1, 'vrange': VRANGE_HIGH}
        sync_set = [rail_set_a, rail_set_b]
        if log:
            self.Log('info', f'{sync_set}')
        return sync_set

    def follow_set_for_single_sync_vrange_alarm_postive(self, mode, log=False):
        sync_leader = random.choice(HC_RAIL_LIST)
        remain_rails = HC_RAIL_LIST.copy()
        remain_rails.remove(sync_leader)
        if mode == 'exclude':
            followers_a = random.sample(remain_rails, random.randint(1, 8))
            for rail in followers_a:
                remain_rails.remove(rail)
            remain_count = len(remain_rails)
            followers_b = random.sample(remain_rails, remain_count)
        elif mode == 'equal':
            followers_a = random.sample(remain_rails, random.randint(1, 9))
            followers_b = followers_a
        elif mode == 'random':
            followers_a = random.sample(remain_rails, random.randint(1, 9))
            followers_b = random.sample(remain_rails, random.randint(1, 9))
        followers_a.sort()
        followers_b.sort()
        rail_set_a = {'act_rail': sync_leader, 'rails': followers_a,
                      'turn_on': 1, 'vrange': VRANGE_LOW}
        rail_set_b = {'act_rail': sync_leader, 'rails': followers_b,
                      'turn_on': 1, 'vrange': VRANGE_HIGH}
        sync_set = [rail_set_a, rail_set_b]
        if log:
            self.Log('info', 'mode ' + mode + f'{sync_set}')
        return sync_set, sync_leader

    def follow_set_for_single_sync_vrange_alarm_onoff_negative(self, mode, log=False):
        sync_leader = random.choice(HC_RAIL_LIST)
        vrange = random.choice([VRANGE_LOW, VRANGE_HIGH])
        remain_rails = HC_RAIL_LIST.copy()
        remain_rails.remove(sync_leader)
        if mode == 'exclude':
            followers_a = random.sample(remain_rails, random.randint(2, 8))
            for rail in followers_a:
                remain_rails.remove(rail)
            remain_count = len(remain_rails)
            followers_c = random.sample(remain_rails, remain_count)
        elif mode == 'equal':
            followers_a = random.sample(remain_rails, random.randint(2, 9))
            followers_c = followers_a
        elif mode == 'random':
            followers_a = random.sample(remain_rails, random.randint(2, 9))
            followers_c = random.sample(remain_rails, random.randint(1, 9))
        followers_a.sort()
        followers_c.sort()
        num_followers_a = len(followers_a)
        num_followers_b = random.randint(1, num_followers_a-1)
        followers_b = random.sample(followers_a, num_followers_b)
        followers_b.sort()
        rail_set_a = {'act_rail': sync_leader, 'rails': followers_a,
                      'turn_on': 1, 'vrange': vrange}
        rail_set_b = {'act_rail': sync_leader, 'rails': followers_b,
                      'turn_on': 0, 'vrange': vrange}
        rail_set_c = {'act_rail': sync_leader, 'rails': followers_c,
                      'turn_on': 1, 'vrange': vrange}
        sync_set = [rail_set_a, rail_set_b, rail_set_c]
        if log:
            self.Log('info', 'mode ' + mode + f'{sync_set}')
        return sync_set

    def follow_set_for_single_sync_vrange_alarm_negative(self, mode, log=False):
        sync_leader = random.choice(HC_RAIL_LIST)
        vrange = random.choice([VRANGE_LOW, VRANGE_HIGH])
        remain_rails = HC_RAIL_LIST.copy()
        remain_rails.remove(sync_leader)
        if mode == 'exclude':
            followers_a = random.sample(remain_rails, random.randint(1, 8))
            for rail in followers_a:
                remain_rails.remove(rail)
            remain_count = len(remain_rails)
            followers_b = random.sample(remain_rails, remain_count)
        elif mode == 'equal':
            followers_a = random.sample(remain_rails, random.randint(1, 9))
            followers_b = followers_a
        elif mode == 'random':
            followers_a = random.sample(remain_rails, random.randint(1, 9))
            followers_b = random.sample(remain_rails, random.randint(1, 9))
        followers_a.sort()
        followers_b.sort()
        rail_set_a = {'act_rail': sync_leader, 'rails': followers_a,
                      'turn_on': 1, 'vrange': vrange}
        rail_set_b = {'act_rail': sync_leader, 'rails': followers_b,
                      'turn_on': 1, 'vrange': vrange}
        sync_set = [rail_set_a, rail_set_b]
        if log:
            self.Log('info', 'mode ' + mode + f'{sync_set}')
        return sync_set

    def follow_set_for_single_sync_vrange_folding_reset(self, log=False):
        sync_leader = 0
        single_rail = 6
        rail_set_a = {'act_rail': sync_leader, 'rails': [2, 3],
                      'turn_on': 1, 'vrange': VRANGE_LOW}
        rail_set_b = {'act_rail': single_rail, 'rails': [],
                      'turn_on': 1, 'vrange': VRANGE_LOW}
        rail_set_c = {'act_rail': single_rail, 'rails': [],
                      'turn_on': 1, 'vrange': VRANGE_HIGH}
        rail_set_d = {'act_rail': sync_leader, 'rails': [2, 3],
                      'turn_on': 1, 'vrange': VRANGE_HIGH}
        sync_set = [rail_set_a, rail_set_b, rail_set_c, rail_set_d]
        if log:
            self.Log('info', 'mode direct ' + f'{sync_set}')
        return sync_set

    def follow_set_for_single_sync_vrange_alarm_on_off_negative(self, log=False):
        sync_leader = random.choice(HC_RAIL_LIST)
        remain_rails = HC_RAIL_LIST.copy()
        remain_rails.remove(sync_leader)
        followers_a = random.sample(remain_rails, random.randint(1, 9))
        followers_b = followers_a
        followers_c = random.sample(remain_rails, random.randint(1, 9))
        followers_a.sort()
        followers_b.sort()
        followers_c.sort()
        rail_set_a = {'act_rail': sync_leader, 'rails': followers_a,
                      'turn_on': 1, 'vrange': VRANGE_LOW}
        rail_set_b = {'act_rail': sync_leader, 'rails': followers_b,
                      'turn_on': 0, 'vrange': VRANGE_LOW}
        rail_set_c = {'act_rail': sync_leader, 'rails': followers_c,
                      'turn_on': 1, 'vrange': VRANGE_HIGH}
        sync_set = [rail_set_a, rail_set_b, rail_set_c]
        if log:
            self.Log('info', f'{sync_set}')
        return sync_set

    def follow_set_for_nearby_sync_vrange_alarm_postive(self, mode, log=False):
        sync_leader = random.choice(HC_RAIL_LIST)
        rails_per_chip = 2
        nearby_leader = ((sync_leader // rails_per_chip) * rails_per_chip) + \
                        (1 ^ (sync_leader % rails_per_chip))
        remain_rails = HC_RAIL_LIST.copy()
        remain_rails.remove(sync_leader)
        remain_rails.remove(nearby_leader)
        if mode == 'exclude':
            followers_a = random.sample(remain_rails, random.randint(1, 7))
            for rail in followers_a:
                remain_rails.remove(rail)
            remain_count = len(remain_rails)
            followers_b = random.sample(remain_rails, remain_count)
        elif mode == 'equal':
            followers_a = random.sample(remain_rails, random.randint(1, 8))
            followers_b = followers_a
        elif mode == 'random':
            followers_a = random.sample(remain_rails, random.randint(1, 8))
            followers_b = random.sample(remain_rails, random.randint(1, 8))
        followers_a.sort()
        followers_b.sort()
        rail_set_a = {'act_rail': sync_leader, 'rails': followers_a,
                      'turn_on': 1, 'vrange': VRANGE_LOW}
        rail_set_b = {'act_rail': nearby_leader, 'rails': followers_b,
                      'turn_on': 1, 'vrange': VRANGE_HIGH}
        sync_set = [rail_set_a, rail_set_b]
        if log:
            self.Log('info', 'mode ' + mode + f'{sync_set}')
        return sync_set, sync_leader

    def follow_set_for_nearby_sync_vrange_alarm_negative(self, mode, log=False):
        sync_leader = random.choice(HC_RAIL_LIST)
        rails_per_chip = 2
        nearby_leader = ((sync_leader // rails_per_chip) * rails_per_chip) + \
                        (1 ^ (sync_leader % rails_per_chip))
        vrange = random.choice([VRANGE_LOW, VRANGE_HIGH])
        remain_rails = HC_RAIL_LIST.copy()
        remain_rails.remove(sync_leader)
        remain_rails.remove(nearby_leader)
        if mode == 'exclude':
            followers_a = random.sample(remain_rails, random.randint(1, 7))
            for rail in followers_a:
                remain_rails.remove(rail)
            remain_count = len(remain_rails)
            followers_b = random.sample(remain_rails, remain_count)
        elif mode == 'equal':
            followers_a = random.sample(remain_rails, random.randint(1, 8))
            followers_b = followers_a
        elif mode == 'random':
            followers_a = random.sample(remain_rails, random.randint(1, 8))
            followers_b = random.sample(remain_rails, random.randint(1, 8))
        followers_a.sort()
        followers_b.sort()
        rail_set_a = {'act_rail': sync_leader, 'rails': followers_a,
                      'turn_on': 1, 'vrange': vrange}
        rail_set_b = {'act_rail': nearby_leader, 'rails': followers_b,
                      'turn_on': 1, 'vrange': vrange}
        sync_set = [rail_set_a, rail_set_b]
        if log:
            self.Log('info', 'mode ' + mode + f'{sync_set}')
        return sync_set

    def create_hc_1rail_check_vrange_alarm_tq_list(self, rail, vout, ext_clk, log=False):
        tq = TriggerQueueCommands()
        hc_rail_array, hc_rail = self.decode_tq_rails(rail)
        pwr_state, vout_hex, uv_hex, ov_hex, fdi_hi_hex, i_hi_hex, vrange_hex \
            = self.encode_tq_power_satus(vout, VRANGE_LOW, ext_clk)
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_vrange, hc_rail, vrange_hex)
        tq.add_command(tq_cmd.set_vrange, hc_rail, VRANGE_HIGH)
        tq.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq.add_command(tq_cmd.set_fdt, hc_rail, hc_rail_array)
        tq.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def create_hc_1rail_transition_tq_list(self, rail, vout, vrange, ext_clk, log=False):
        tq = TriggerQueueCommands()
        hc_rail_array, hc_rail = self.decode_tq_rails(rail)
        pwr_state, vout_hex, uv_hex, ov_hex, fdi_hi_hex, i_hi_hex, vrange_hex \
            = self.encode_tq_power_satus(vout, vrange, ext_clk)
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        self.Log('info', f'VR 0x{vrange_hex:08x}')
        tq.add_command(tq_cmd.set_vrange, hc_rail, vrange_hex)
        tq.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq.add_command(tq_cmd.set_fdt, hc_rail, hc_rail_array)
        tq.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def decode_tq_rails(self, rail):
        if 0 <= rail <= 9:
            hc_rail_array = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
        return hc_rail_array, hc_rail

    def encode_tq_power_satus(self, vout, vrange, ext_clk=False):
        tq = TriggerQueueCommands()
        frequency = vrange
        if vout == None:
            vout_on = 0
            pwr_state = 0b00
            ov = uv = 0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            ov = uv = 0
        elif 0.5 <= vout <= 3:
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.5
        ov_hex = tq.float_to_hex(ov)
        uv_hex = tq.float_to_hex(uv)
        vout_hex = tq.float_to_hex(vout_on)
        fdi_hi_hex = tq.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq.float_to_hex(HC_TQ_I_HI)
        vrange_hex = self.encode_sync_vrange(pwr_state, frequency, ext_clk)
        return pwr_state, vout_hex, uv_hex, ov_hex, fdi_hi_hex, i_hi_hex, vrange_hex

    def encode_sync_vrange(self, pwr_state, frequency, ext_clk):
        if pwr_state == 0b01:
            channel_state = 1
        else:
            channel_state = 0
        vrange_hex = (channel_state << 19) | frequency
        if ext_clk:
            vrange_hex = vrange_hex | (1 << 19)
        return vrange_hex

    def create_hc_1_rail_operation_tq_list(self, vout, sync_set, log=False):
        hc_rail_array = 0b11_1111_1111
        pwr_state, vout_hex, uv_hex, ov_hex, fdi_hi_hex, i_hi_hex, vrange_hex \
            = self.encode_tq_power_satus(vout, VRANGE_LOW)
        tq = TriggerQueueCommands()
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        for index, rail_set in enumerate(sync_set):
            vrange = self.get_sync_leader_vrange_payload(rail_set)
            self.Log('info', f'{rail_set}')
            self.Log('info', f'vrange 0x{vrange:010x}')
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail_set['act_rail'])
            tq.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
            tq.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
            tq.add_command(tq_cmd.set_v, hc_rail, vout_hex)
            tq.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
            tq.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
            tq.add_command(tq_cmd.set_fdt, hc_rail, hc_rail_array)
            tq.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
            tq.add_command(tq_cmd.delay, hc_rail, 300000)
            tq.add_command(tq_cmd.set_vrange, hc_rail, vrange)
            tq.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def create_hc_1sync_leader_vrange_tq_list(self, sync_set, log=False):
        hc_rail_array = 0b11_1111_1111
        tq = TriggerQueueCommands()
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        for rail_set in sync_set:
            vrange = self.get_sync_leader_vrange_payload(rail_set)
            self.Log('info', f'{rail_set}')
            self.Log('info', f'vrange 0x{vrange:010x}')
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail_set['act_rail'])
            tq.add_command(tq_cmd.set_vrange, hc_rail, vrange)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def create_hc_1sync_leader_vrange_selfvrange_tq_list(self, vout, sync_set, log=False):
        hc_rail_array = 0b11_1111_1111
        pwr_state, vout_hex, uv_hex, ov_hex, fdi_hi_hex, i_hi_hex, vrange_hex \
            = self.encode_tq_power_satus(vout, VRANGE_LOW)
        tq = TriggerQueueCommands()
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        for rail_set in sync_set:
            vrange = self.get_sync_leader_vrange_payload(rail_set)
            self.Log('info', f'{rail_set}')
            self.Log('info', f'vrange 0x{vrange:010x}')
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail_set['act_rail'])
            self.Log('info', f'{hc_rail}')
            tq.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
            tq.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
            tq.add_command(tq_cmd.set_v, hc_rail, vout_hex)
            tq.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
            tq.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
            tq.add_command(tq_cmd.set_fdt, hc_rail, hc_rail_array)
            tq.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
            tq.add_command(tq_cmd.set_vrange, hc_rail, vrange)
            tq.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def create_hc_1sync_leader_vrange_nearvrange_tq_list(self, vout, sync_set, log=False):
        hc_rail_array = 0b11_1111_1111
        pwr_state, vout_hex, uv_hex, ov_hex, fdi_hi_hex, i_hi_hex, vrange_hex \
            = self.encode_tq_power_satus(vout, VRANGE_LOW)
        tq = TriggerQueueCommands()
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        for rail_set in sync_set:
            vrange = self.get_sync_leader_vrange_payload(rail_set)
            self.Log('info', f'{rail_set}')
            self.Log('info', f'vrange 0x{vrange:010x}')
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail_set['act_rail'])
            self.Log('info', f'{hc_rail}')
            tq.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
            tq.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
            tq.add_command(tq_cmd.set_v, hc_rail, vout_hex)
            tq.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
            tq.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
            tq.add_command(tq_cmd.set_fdt, hc_rail, hc_rail_array)
            tq.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
            tq.add_command(tq_cmd.delay, hc_rail, 200000)
            tq.add_command(tq_cmd.set_vrange, hc_rail, vrange)
            tq.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def create_hc_1sync_follower_vrange_tq_list(self, sync_set, log=False):
        hc_rail_array = 0b11_1111_1111
        tq = TriggerQueueCommands()
        tq.create_tq_list()
        tq.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_rail_array)
        for rail_set in sync_set:
            vrange = self.get_sync_follower_vrange_payload(rail_set)
            self.Log('info', f'vrange 0x{vrange:010x}')
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail_set['act_rail'])
            tq.add_command(tq_cmd.set_vrange, hc_rail, vrange)
        tq.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_rail_array)
        tq.add_end_command()
        tq_list = tq.get_tq_list(log)
        tq.delete_tq_list()
        return tq_list

    def check_sync_leader_vrange_result(self, hbidps, sync_set, expect_to_fail, log=False):
        self.check_sync_vrange_alarm(hbidps, sync_set, expect_to_fail, log)
        self.check_sync_channel_mask(hbidps, sync_set, expect_to_fail, log)
        self.check_vrange_value(hbidps, sync_set, 0, log)

    def check_sync_follower_vrange_result(self, hbidps, sync_set, expect_to_fail, log=False):
        self.check_sync_vrange_alarm(hbidps, sync_set, expect_to_fail, log)
        self.check_sync_channel_mask(hbidps, sync_set, expect_to_fail, log)
        self.check_vrange_value(hbidps, sync_set, 1, log)
        self.check_pmbus_frequncy_switch(hbidps, sync_set, 1, log)

    def check_vrange_value(self, hbidps, sync_set, is_follower, log):
        self.Log('info', '*'*100)
        self.Log('info', 'check vrange reg value')
        check_rail_set = sync_set[-1]
        rail = check_rail_set['act_rail']
        expect_vrange = check_rail_set['vrange']
        vrange_external, valid, vrange = hbidps.get_sync_channel_vrange_value(rail)
        if (vrange_external != is_follower) | (valid != 1) | \
                (vrange != expect_vrange):
            self.Log('error', f'vrange fail expect_valid {1}, '
                              f'read_valid {valid}, '
                              f'expect_external {is_follower}, '
                              f'read_external {vrange_external}, '
                              f'expect_vrange 0x{expect_vrange:04x}, '
                              f'read_vrange 0x{vrange:04x}')
        if log:
            self.Log('info', f'vrange expect_valid {1}, '
                             f'read_valid {valid}, '
                             f'expect_external {is_follower}, '
                             f'read_external {vrange_external}, '
                             f'expect_vrange 0x{expect_vrange:04x}, '
                             f'read_vrange 0x{vrange:04x}')

    def check_sync_channel_mask(self, hbidps, sync_set, expect_to_fail, log):
        self.Log('info', '*'*100)
        self.Log('info', 'check sync_vrange mask')
        check_rail_set = sync_set[-1]
        chip = check_rail_set['act_rail']//2
        expect_mask = self.get_expect_sync_mask(sync_set, expect_to_fail)
        freeze, mask = hbidps.get_sync_channel_mask_value(chip)
        read_last_mask = hbidps.get_last_sync_channel_mask(chip)
        expect_last_mask = self.get_expect_last_mask(sync_set)
        if (expect_to_fail != freeze) | (expect_mask != mask) | (read_last_mask!=expect_last_mask):
            self.Log('error', f'mask fail expect_freeze {expect_to_fail}, '
                              f'read_freeze {freeze}')
            self.Log('error', f'expect_mask 0b{expect_mask:020b}, '
                              f'read_mask 0b{mask:020b}')
            self.Log('error', f'expect_last_mask{expect_last_mask:020b}, '
                              f'read_last_mask{read_last_mask:020b}')
        if log:
            self.Log('info', f'mask expect_freeze {expect_to_fail}, '
                             f'read_freeze {freeze}')
            self.Log('info', f'expect_mask 0b{expect_mask:020b}, '
                             f'read_mask 0b{mask:020b}')
            self.Log('info', f'expect_last_mask{expect_last_mask:020b}, '
                             f'read_last_mask{read_last_mask:020b}')

    def get_expect_last_mask(self, sync_set):
        check_rail_set = sync_set[-1]
        last_mask = 0
        for rail in check_rail_set['rails']:
            last_mask = last_mask + (1 << rail)
        return last_mask

    def check_original_vrange_alarm(self, hbidps, rail, expect_selfvrange, expect_nearvrange, expect_checkvrange, log=False):
        self.check_selfvrange(hbidps, rail, expect_selfvrange, log)
        self.check_nearvrange(hbidps, rail, expect_nearvrange, log)
        self.check_checkvrange(hbidps, rail, expect_checkvrange, log)

    def check_selfvrange(self, hbidps, rail, expect_selfvrange, log):
        selfvrange = hbidps.get_hc_vrange_alarm().rwc_vrange_self
        if expect_selfvrange and (selfvrange != (1 << rail)):
            self.Log('error', f'selfvrange mismatch, expect 0x{(1<<rail):03x}, read 0x{selfvrange:03x}')
        elif (not expect_selfvrange) and (selfvrange != 0):
            self.Log('error', f'selfvrange mismatch, expect 0x{0:03x}, read 0x{selfvrange:03x}')
        elif log:
            if expect_selfvrange:
                self.Log('info', f'selfvrange match, expect 0x{(1 << rail):03x}, read 0x{selfvrange:03x}')
            else:
                self.Log('info', f'selfvrange match, expect 0x{0:03x}, read 0x{selfvrange:03x}')

    def check_nearvrange(self, hbidps, rail, expect_nearvrange, log):
        nearvrange = hbidps.get_hc_vrange_alarm().rwc_vrange_neighbor
        if expect_nearvrange and (nearvrange != (1 << rail)):
            self.Log('error', f'nearvrange mismatch, expect 0x{(1<<rail):03x}, read 0x{nearvrange:03x}')
        elif (not expect_nearvrange) and (nearvrange != 0):
            self.Log('error', f'nearvrange mismatch, expect 0x{0:03x}, read 0x{nearvrange:03x}')
        elif log:
            if expect_nearvrange:
                self.Log('info', f'nearvrange match, expect 0x{(1 << rail):03x}, read 0x{nearvrange:03x}')
            else:
                self.Log('info', f'nearvrange match, expect 0x{0:03x}, read 0x{nearvrange:03x}')

    def check_checkvrange(self, hbidps, rail, expect_checkvrange, log):
        checkvrange = hbidps.get_hc_vrange_alarm().rwc_vrange_check_fail
        if expect_checkvrange and (checkvrange != (1 << rail)):
            self.Log('error', f'checkvrange mismatch, expect 0x{(1<<rail):03x}, read 0x{checkvrange:03x}')
        elif (not expect_checkvrange) and (checkvrange != 0):
            self.Log('error', f'checkvrange mismatch, expect 0x{0:03x}, read 0x{checkvrange:03x}')
        elif log:
            if expect_checkvrange:
                self.Log('info', f'checkvrange match, expect 0x{(1 << rail):03x}, read 0x{checkvrange:03x}')
            else:
                self.Log('info', f'checkvrange match, expect 0x{0:03x}, read 0x{checkvrange:03x}')

    def check_cleared_sync_vrange_mask(self, hbidps, log=False):
        for chip in range(5):
            freeze, mask = hbidps.get_sync_channel_mask_value(chip)
            if (freeze != 0) or (mask != 0):
                self.Log('error', f'chip{chip} mask not cleared: freeze {freeze}, mask 0x{mask:05x}')
            elif log:
                self.Log('info', f'chip{chip} mask cleared: freeze {freeze}, mask 0x{mask:05x}')

    def check_clean_sync_vrange_alarm(self, hbidps, log=False):
        sync_check_alarm = hbidps.get_sync_check_fail_alarm_value()
        if sync_check_alarm != 0:
            self.Log('error', f'sync vrange alarm not clean 0x{sync_check_alarm:03x}')
        elif log:
            self.Log('info', f'sync vrange alarm clean 0x{sync_check_alarm:03x}')

    def check_sync_vrange_alarm(self, hbidps, sync_set, expect_to_fail, log):
        self.Log('info', '*'*100)
        self.Log('info', 'check sync_vrange alarm')
        check_rail_set = sync_set[-1]
        rail = check_rail_set['act_rail']
        if expect_to_fail:
            expect_alarm = (1 << rail)
        else:
            expect_alarm = 0
        sync_check_alarm = hbidps.get_sync_check_fail_alarm_value()
        if (sync_check_alarm != expect_alarm):
            self.Log('error', f'sync vrange alarm fail '
                              f'expect_alarm 0x{expect_alarm:03x}, '
                              f'read_alarm 0x{sync_check_alarm:03x}')
        if log:
            self.Log('info', f'sync vrange alarm '
                             f'expect_alarm 0x{expect_alarm:03x}, '
                             f'read_alarm 0x{sync_check_alarm:03x}')

    def check_pmbus_frequncy_switch(self, hbidps, sync_set, is_follower, log):
        self.Log('info', '*'*100)
        self.Log('info', 'check pmbus frequency switch value')
        check_rail_set = sync_set[-1]
        chip = check_rail_set['act_rail']//2
        frequncy = hbidps.read_hc_reg_frequency(chip)
        if is_follower & (frequncy != 0):
            self.Log('error', f'follower frequncy fail'
                              f'expect_freq 0x{0:04x}, '
                              f'read_freq 0x{frequncy:04x}')
            hbidps.set_hc_reg_frequency(chip, 0)
            time.sleep(1)
            pmbus_fre = hbidps.read_hc_reg_frequency(chip)
            self.Log('info', f'pmbus write frequncy '
                             f'expect_freq 0x{0:04x}, '
                             f'read_freq 0x{pmbus_fre:04x}')
        if log:
            self.Log('info', f'follower frequncy '
                             f'expect_freq 0x{0:04x}, '
                             f'read_freq 0x{frequncy:04x}')

    def reset_sync_vrange_alarm(self, hbidps):
        hbidps.reset_hc_sync_vrange_fail_alarm()

    def get_expect_sync_mask(self, sync_set, expect_to_fail):
        on_mask = 0
        if expect_to_fail:
            rail_set_list = sync_set[:-1]
        else:
            rail_set_list = sync_set
        for rail_set in rail_set_list:
            all_channel_bit_mask = 0
            for rail in rail_set['rails']:
                all_channel_bit_mask += 1 << rail
            if rail_set['turn_on']:
                on_mask = on_mask | all_channel_bit_mask
            else:
                on_mask = on_mask & (all_channel_bit_mask ^ 0xF_FFFF)
        return on_mask

    def get_sync_leader_checkvrange_alarm_payload(self, rail_set):
        channel_state_request = 0
        all_channel_bit_mask = 0
        for rail in rail_set['rails']:
            all_channel_bit_mask += 1 << rail
        externally_driven = 0
        frequency_switch_payload = rail_set['vrange']
        payload = \
            (all_channel_bit_mask << 20) | (channel_state_request << 19) \
            | (externally_driven << 16) | (frequency_switch_payload << 0)
        return payload

    def get_sync_leader_vrange_payload(self, rail_set):
        channel_state_request = rail_set['turn_on']
        all_channel_bit_mask = 0
        for rail in rail_set['rails']:
            all_channel_bit_mask += 1 << rail
        externally_driven = 0
        frequency_switch_payload = rail_set['vrange']
        payload = \
            (all_channel_bit_mask << 20) | (channel_state_request << 19) \
            | (externally_driven << 16) | (frequency_switch_payload << 0)
        return payload

    def get_sync_follower_vrange_payload(self, rail_set):
        channel_state_request = rail_set['turn_on']
        all_channel_bit_mask = 0
        externally_driven = 1
        frequency_switch_payload = rail_set['vrange']
        payload = \
            (all_channel_bit_mask << 20) | (channel_state_request << 19) \
            | (externally_driven << 16) | (frequency_switch_payload << 0)
        return payload


class HCGangingEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps
        self.name = 'HCGangingEnv'

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_hc_rails_start_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.check_reset_sync_channel_mask_value('enter ' + self.name)
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_hc_rails_to_safe_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.check_reset_sync_channel_mask_value('exit ' + self.name)
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)
