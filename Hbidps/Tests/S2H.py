################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing S2H function"""

from Common.Tests.s2h_base import S2HBase
from Hbidps.Tests.HbidpsTest import HbidpsTest
from Common.fval import skip


class Functional(HbidpsTest, S2HBase):
    """Check S2H function. Need more info to create tests.

        FPGA Review Note:
        FVAL Has some test on that. Content in that package.
        Overflow, memory allocated. Had some reference code.
        Common test area. Reuse by extension but not modification.
        Software document
        regress.py hbidps -tn S2H.Functional.*
    """
