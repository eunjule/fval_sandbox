# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import random
import time

from Hbidps.Tests.HbidpsTest import HbidpsTest

TIME_MARGIN_US = 500


class Functional(HbidpsTest):

    def DirectedMicrosecondCounterTimeDeltaTest(self):
        time.sleep(0.010)
        hbidps = random.choice(self.hbidps_list)
        self.Log('info', f'hbidps{hbidps.slot} under test')
        random_test_time = random.randint(10000, 100000)
        self.Log('info', f'randome_test_time {random_test_time}us')
        counter_us_start = hbidps.get_microsecond_counter_value()
        perf_time_delta_us = self.wait_for_microseconds(random_test_time)
        counter_us_end = hbidps.get_microsecond_counter_value()
        counter_us_delta = counter_us_end - counter_us_start
        self.Log('info', f'UScounter delta {counter_us_delta}us')
        self.check_microsecond_counter_result(counter_us_start, counter_us_end, perf_time_delta_us)

    def DirectedCrossBoardResetTimingTest(self):
        time.sleep(0.010)
        _ = self.read_all_hbidps_microsecond_counter_value()
        hbidps = random.choice(self.hbidps_list)
        self.Log('info', f'hbidps{hbidps.slot} under test')
        hbidps.send_time_reset_trigger_from_rctc_to_dps()
        self.Log('info', f'after the rest')
        time_list, perf_delta_list = self.read_all_hbidps_microsecond_counter_value()
        self.check_microsecond_counter_reset_time_list(time_list, perf_delta_list)

    def DirectedCrossBoardTimingShiftTest(self):
        time.sleep(0.010)
        _ = self.read_all_hbidps_microsecond_counter_value()
        hbidps = random.choice(self.hbidps_list)
        self.Log('info', f'hbidps{hbidps.slot} under test')
        hbidps.send_time_reset_trigger_from_rctc_to_dps()
        self.Log('info', f'after the rest')
        time.sleep(10)
        time_list, perf_delta_list = self.read_all_hbidps_microsecond_counter_value()
        self.check_microsecond_counter_reset_time_list(time_list, perf_delta_list)

    def wait_for_microseconds(self, time_delta_in_microseconds):
        time_start = time.perf_counter()
        time_final_expect = time_start + (time_delta_in_microseconds/1000000)
        while(1):
            time_now = time.perf_counter()
            if time_now >= time_final_expect:
                log_time_microsecond = ((time_now - time_start)*1000000)//1
                self.Log('info', f'perf_counter delta {log_time_microsecond}us')
                return log_time_microsecond

    def read_all_hbidps_microsecond_counter_value(self):
        counter_us_value_list = []
        perf_s_delta_list = []
        time_start = time.perf_counter()
        for hbidps in self.hbidps_list:
            time_now = time.perf_counter()
            counter_us_value = hbidps.get_microsecond_counter_value()
            counter_us_value_list.append(counter_us_value)
            time_delta = time_now - time_start
            perf_s_delta_list.append(time_delta)
            time_start = time_now
        for index, hbidps in enumerate(self.hbidps_list):
            self.Log('info', f'hbidps{hbidps.slot} '
                             f'UScounter {counter_us_value_list[index]}us perf_delta {(perf_s_delta_list[index]*1000000)//1}us')
        return counter_us_value_list, perf_s_delta_list

    def check_microsecond_counter_result(self, start_time_us, end_time_us, expect_delta_us):
        real_delta_us = end_time_us - start_time_us
        if (real_delta_us > (expect_delta_us + TIME_MARGIN_US)) or \
                (real_delta_us < (expect_delta_us - TIME_MARGIN_US)):
            self.Log('error', f'time falure expect {expect_delta_us}us; read {real_delta_us}us')

    def check_microsecond_counter_reset_time_list(self, time_list, perf_delta_list):
        time_base = time_list[0]
        for index, hbidps in enumerate(self.hbidps_list):
            real_time = time_list[index]
            perf_delta = (perf_delta_list[index] * 1000000) // 1
            if (real_time > (time_base + perf_delta + TIME_MARGIN_US)) or \
                    (real_time < (time_base + perf_delta - TIME_MARGIN_US)):
                self.Log('error', f'hbidps{hbidps.slot} 'f'UScounter read {real_time}us; expect {time_base + perf_delta}us;')
            time_base = real_time