################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing BIST function"""

import math
from Hbidps.Tests.HbidpsTest import HbidpsTest
from Hbidps.instrument import hbidps_register


class Functional(HbidpsTest):

    def DirectedBISTEngineRegisterValueCheckTest(self):
        """Feature is to check if three clocks are of expected frequency in HBIDPS design.

        PCIe Reference ClockPCIe Reference Clock = 100MHz
            o	PCIe User Clock (125MHz)
            o	Aurora User Clock (62.5MHz)
            o	DDR4 Reference Clock (33.33MHz)
        Formula used for verification is -
            o	Clock Ratio = (Measure Counter * Division Ratio) / Window Size
            o	Expected Clock Period = (PCIe Reference Clock) / Clock Ratio
        """
        expected_clocks = {'aur_user_clock':62.5,'ddr4_ref_clock':33.33,'pcie_user_clock':125.00}
        for test_loop in range(50):
            for hbidps in self.hbidps_list:
                aur_user_clock = hbidps.read_bar_register(hbidps_register.CLK_FREQ_AUR_USER_CLK)
                aur_user_clock_ratio = self.get_clock_ratio(aur_user_clock)
                aur_user_clock = self.get_clock_period(aur_user_clock_ratio)
                ddr4_ref_clock = hbidps.read_bar_register(hbidps_register.CLK_FREQ_DDR4_REF_CLK)
                ddr4_ref_clock_ratio = self.get_clock_ratio(ddr4_ref_clock)
                ddr4_ref_clock = self.get_clock_period(ddr4_ref_clock_ratio)
                pcie_user_clock = hbidps.read_bar_register(hbidps_register.CLK_FREQ_PCIE_USER_CLK)
                pcie_user_clock_ratio = self.get_clock_ratio(pcie_user_clock)
                pcie_user_clock = self.get_clock_period(pcie_user_clock_ratio)
                if not math.isclose(aur_user_clock,expected_clocks['aur_user_clock'],rel_tol=0.005):
                    self.Log('error',
                             f'Aurora user clock for slot {hbidps.name_with_slot()} is not in expected range. Expected {expected_clocks["aur_user_clock"]} actual {aur_user_clock} ')
                if not math.isclose(ddr4_ref_clock,expected_clocks['ddr4_ref_clock'],rel_tol=0.005):
                    self.Log('error',
                             f'DDR4 reference clock for slot {hbidps.name_with_slot()} is not in expected range. Expected {expected_clocks["ddr4_ref_clock"]} actual {ddr4_ref_clock} ')
                if not math.isclose(pcie_user_clock,expected_clocks['pcie_user_clock'],rel_tol=0.005):
                    self.Log('error',
                             f'Pcie reference clock for slot {hbidps.name_with_slot()} is not in expected range. Expected {expected_clocks["pcie_user_clock"]} actual {pcie_user_clock} ')

    def get_clock_ratio(self,bist_register_data):
        return (bist_register_data.measure_counter * bist_register_data.div_ratio) / bist_register_data.window_size

    def get_clock_period(self,clock_ratio):
        pcie_reference_clock = 100
        return clock_ratio * pcie_reference_clock
