################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing of LTM4680 Force voltage control"""
import ctypes
import random
from string import Template
import time

from Common import hilmon as hil
from Hbidps.testbench.assembler import TriggerQueueAssembler
from Hbidps.testbench.structs import HbidpsStruct
from Hbidps.Tests.HbidpsTest import HbidpsTest
from Common.fval import skip

from Hbidps.instrument.HCRailPMBUS import HCRailPMBUS
from Hbidps.instrument.RailTransitionControl import RailTransitionControl
from Hbidps.instrument.SelfTestBlock import VtVmBus, CommonForceBus
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.instrument.TQMachine import TriggerQueueDDR
from Hbidps.instrument.MdutConfiguration import MdutConfiguration
from Hbidps.instrument import hbidps_register
from Hbidps.instrument.AlarmSystem import AlarmHelper
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands

MAX_FAIL_COUNT = 10
HC_TQ_RAIL_ID_BASE = 0x30
HC_TQ_FDT = 0x1000
HC_TQ_I_HI = 40

class BaseTest(HbidpsTest):

    def compare_expected_with_observed(self, expected, observed, error_count, iteration, target_pmbus_device, rail_under_test):
        if expected != observed:
            error_count.append(
                {'Iteration': iteration, 'Expected': hex(expected), 'Observed': hex(observed),'Rail':rail_under_test, 'Target pmbus device':target_pmbus_device})

    def report_errors(self, device, register_errors,error_message_to_print):
        column_headers = ['Iteration', 'Expected', 'Observed','Rail','Target pmbus device']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print,table), 'error')


class MfrPwnComp(HbidpsStruct):
    _field_ = [('ResComp', ctypes.c_byte, 5),
               ('ErrorAmplifierGmAdjustMs', ctypes.c_byte, 3)]


class MfrConfigAll(HbidpsStruct):
    _field_ = [('execute_clear_faults', ctypes.c_byte, 1),
               ('enable_pmbus_clk_stretching', ctypes.c_byte, 1),
               ('PEC', ctypes.c_byte, 1),
               ('enable_timeout', ctypes.c_byte, 1),
               ('disable_sync_output', ctypes.c_byte, 1),
               ('mask_pmbus', ctypes.c_byte, 1),
               ('ignore_resistor_pins', ctypes.c_byte, 1),
               ('enable_fault_logging', ctypes.c_byte, 1)]

class TqDiagnostics(BaseTest):
    """Test Forcing voltages and voltage bumps using Trigger queue"""
    TEST_ITERATIONS = 10
    ltm_pages = 2
    ltm_devices = 5
    hc_base = 0x30

    def RandomHcRailScratchPadWriteViaTqReadViaPmBusTest(self):
        """Send random data to scratch pad register via Trigger queue. Read data back from device using PM BUS read.

           Confirm results are same.
        """
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            read_back_errors = self.scratch_pad_test_scenario(hbidps)
            self.display_pass_fail_messages(hbidps, read_back_errors)

    def scratch_pad_test_scenario(self, hbidps):
        read_back_errors = []
        error_count = []
        pm_bus_command = hbidps.symbols.PMBUSCOMMAND.USER_DATA_04_SCRATCH_PAD
        for iteration in range(self.TEST_ITERATIONS):
            hbidps.clear_alarm_register()
            hbidps.clear_pmbus_rx_fifo()
            random_data_to_write, random_hc_rail = self.generate_random_data()
            rail_under_test = self.rail_under_test(random_hc_rail)
            target_pmbus_device = self.target_pmbus_device(random_hc_rail)
            self.send_data_from_trigger_queue(hbidps, pm_bus_command, random_data_to_write, random_hc_rail, rail_under_test)
            for retries in range(100):
                read_back_data = self.read_data_from_pmbus_command(hbidps, target_pmbus_device, pm_bus_command)
                if random_data_to_write == read_back_data:
                    break
            else:
                self.compare_expected_with_observed(random_data_to_write, read_back_data, error_count, iteration,target_pmbus_device,rail_under_test)

            if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= self.TEST_ITERATIONS:
                break
        read_back_errors.extend(error_count)
        return read_back_errors

    def generate_random_data(self):
        random_data_to_write = random.randint(0, 0xFFFF)
        random_hc_rail = random.randint(0x30, 0x39)
        return random_data_to_write, random_hc_rail

    def rail_under_test(self, hc_rail):
        return hc_rail - self.hc_base

    def target_pmbus_device(self, hc_rail):
        return self.rail_under_test(hc_rail) // 2 + 4

    def page_num(self, hc_rail):
        return self.rail_under_test(hc_rail) % 2

    def send_data_from_trigger_queue(self, hbidps, pm_bus_command, random_data_to_write, random_hc_rail, rail_under_test):
        trigger_queue_offset = 0x0
        trigger_queue_data = self.generate_trigger_queue(hbidps, random_hc_rail, pm_bus_command, random_data_to_write, rail_under_test)
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)

    def generate_trigger_queue(self, hbidps, random_hc_rail, pm_bus_command, random_data_to_write,rail_under_test):
        trigger_queue = TriggerQueueAssembler()
        tq_string = Template('''
                     WakeRail resourceid =$resourceid,  railmask =$railmask
                     SetRailBusy resourceid =$resourceid, railmask =$railmask
                     WriteWord rail=$rail, value=$data ,pmbuscmd = $pmbus_cmd
                     TqNotify resourceid =$resourceid, railmask =$railmask
                     TimeDelay rail =$rail, value =$value
                     TqComplete rail=$rail, value=0
                 ''').substitute(rail=str(hex(random_hc_rail)), data=random_data_to_write, pmbus_cmd=pm_bus_command,resourceid =0x81, railmask=1 << rail_under_test, value=str(0x186A0))
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        return trigger_queue_data

    def read_data_from_pmbus_command(self, hbidps, target_pmbus_device, pm_bus_command):
        self.generate_pmbus_tx_fifo_data(hbidps, pm_bus_command, target_pmbus_device)
        hbidps.execute_pm_bus_command()
        if not hbidps.is_pm_bus_busy():
            self.Log('error','PM BUS I2C link busy')
            rx_fifo = None
        else:
            rx_fifo = hbidps.read_bar_register(hbidps.registers.PMBUS_RX_FIFO).value
        return rx_fifo

    def generate_pmbus_tx_fifo_data(self, hbidps, pm_bus_command, target_pmbus_device):
        READ_WORD = 0x4
        pm_bus_tx_fifo = hbidps.registers.PMBUS_TX_FIFO()
        pm_bus_tx_fifo.target_device_number = target_pmbus_device
        pm_bus_tx_fifo.target_specific_register_address = pm_bus_command
        pm_bus_tx_fifo.pm_bus_transaction = READ_WORD
        hbidps.write_bar_register(hbidps.registers.PMBUS_TX_FIFO(value=pm_bus_tx_fifo.value))

    def display_pass_fail_messages(self, hbidps, read_back_errors):
        if read_back_errors:
            error_message_to_print = 'LTM 4680 write via Trigger Queue Read via PMBUS failed '
            self.report_errors(hbidps, read_back_errors, error_message_to_print)
        else:
            hbidps.log_device_message(
                'LTM 4680 write via Trigger Queue Read via PMBUS successful for {} iterations.'.format(
                    self.TEST_ITERATIONS), 'info')

    def RandomHcRailPagedScratchPadWriteViaTqReadViaPmBusTest(self):
        """Send random data to paged scratch pad register via Trigger queue. Read data back from device using PM BUS read.

           Confirm results are same.
        """
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            read_back_errors = self.paged_scratch_pad_test_scenario(hbidps)
            self.display_pass_fail_messages(hbidps, read_back_errors)

    def paged_scratch_pad_test_scenario(self, hbidps):
        read_back_errors = []
        error_count = []
        page_command, pm_bus_command = self.set_pm_bus_commands(hbidps)
        for iteration in range(self.TEST_ITERATIONS):
            hbidps.clear_alarm_register()
            hbidps.clear_pmbus_rx_fifo()
            random_data_to_write, _ = self.generate_random_data()
            page_number = random.choice(range(self.ltm_pages))
            target_pmbus_device = random.choice(range(self.ltm_devices)) + 4
            rail_under_test = (target_pmbus_device - 4) * self.ltm_pages + page_number
            random_hc_rail = self.hc_base + rail_under_test
            self.send_paged_data_from_trigger_queue(hbidps, pm_bus_command,  random_hc_rail, rail_under_test,
                                                    target_pmbus_device, error_count, iteration, page_command, page_number)

            if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= self.TEST_ITERATIONS:
                break
        read_back_errors.extend(error_count)
        return read_back_errors

    def set_pm_bus_commands(self, hbidps):
        pm_bus_command = hbidps.symbols.PMBUSCOMMAND.USER_DATA_03_SCRATCH_PAD
        page_command = hbidps.symbols.PMBUSCOMMAND.PAGE_CMD
        return page_command, pm_bus_command

    def generate_paged_trigger_queue(self, hbidps, random_hc_rail, pm_bus_command, target_pmbus_device, error_count,
                                     iteration, rail_under_test, page_command, page_number):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        data_to_write = random.randint(0, 0xFFFF)
        tq_string = Template('''
                    WakeRail resourceid =$resourceid,  railmask =$railmask
                    SetRailBusy resourceid =$resourceid, railmask =$railmask
                    WriteByte rail=$rail, value=$data_1 ,pmbuscmd = $pmbus_cmd_1
                    WriteWord rail =$rail, value =$data, pmbuscmd = $pmbus_cmd
                    TimeDelay rail =$rail, value =$value
                    TqNotify resourceid =$resourceid, railmask =$railmask
                    TqComplete rail =$rail, value = 0
                 ''').substitute(rail=str(hex(random_hc_rail)), data=data_to_write, pmbus_cmd=pm_bus_command,
                                 resourceid=0x81, data_1=page_number, railmask=1 << rail_under_test,
                                 value=str(0x186A0), pmbus_cmd_1=page_command)
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)
        self.read_and_compare_data(data_to_write, error_count, hbidps, iteration, page_command, page_number,
                                   pm_bus_command, rail_under_test, target_pmbus_device)
        return trigger_queue_data

    def read_and_compare_data(self, data_to_write, error_count, hbidps, iteration, page_command, page_number,
                              pm_bus_command, rail_under_test, target_pmbus_device):
        self.set_page_via_pmbus(hbidps, page_number, target_pmbus_device, page_command)
        hbidps.execute_pm_bus_command()

        for retries in range(100):
            read_back_data = self.read_data_from_pmbus_command(hbidps, target_pmbus_device, pm_bus_command)
            if data_to_write == read_back_data:
                break
        else:
            self.compare_expected_with_observed(data_to_write, read_back_data, error_count, iteration, target_pmbus_device,
                                                rail_under_test)

    def send_paged_data_from_trigger_queue(self, hbidps, pm_bus_command, random_hc_rail, rail_under_test,
                                           target_pmbus_device, error_count, iteration,page_command, page_number):

        self.generate_paged_trigger_queue(hbidps, random_hc_rail, pm_bus_command,
                                          target_pmbus_device, error_count, iteration,
                                          rail_under_test, page_command,page_number)

    def set_page_via_pmbus(self, hbidps, page_number, target_pmbus_device, pm_bus_command):
        WRITE_BYTE = 0x1
        pm_bus_tx_fifo = hbidps.registers.PMBUS_TX_FIFO()
        pm_bus_tx_fifo.target_device_number = target_pmbus_device
        pm_bus_tx_fifo.target_specific_register_address = pm_bus_command
        pm_bus_tx_fifo.pm_bus_transaction = WRITE_BYTE
        pm_bus_tx_fifo.tx_data = page_number
        hbidps.write_bar_register(hbidps.registers.PMBUS_TX_FIFO(value=pm_bus_tx_fifo.value))

    def DirectedForceVoltageTest(self):
        """Force a voltage over the trigger bus and compare the output to the read voltage.

           This test sends random voltages over random HC rails on a single DPS.
           This test requires a DTB to be present.
        """
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            if hbidps.hbidtb:
                read_back_errors = self.voltage_test_scenario(hbidps)
                self.display_pass_fail_messages(hbidps, read_back_errors)
            else:
                self.Log('error', f'{hbidps.name_with_slot()}: Failed due to absent of HBIDTB')

    def voltage_test_scenario(self, hbidps):
        read_back_errors = []
        error_count = []

        for iteration in range(self.TEST_ITERATIONS):
            hbidps.clear_alarm_register()
            hbidps.clear_pmbus_rx_fifo()
            page_num = 1 # random.choice(range(self.ltm_pages))
            target_pmbus_device = 6 # random.choice(range(self.ltm_devices)) + 4
            rail_under_test = 5 #(target_pmbus_device - 4 ) * self.ltm_pages + page_num
            random_hc_rail = self.hc_base + rail_under_test #0x35
            self.Log('info', f'Page Number: {page_num} Rail: 0x{random_hc_rail:08X}')
            self.set_LTM4688_run(hbidps, data=0)
            self.set_hc_rail_state_off(hbidps)
            self.set_ground_control_on(hbidps)
            self.set_hc_ld_en(hbidps)
            self.set_ground_control_off(hbidps)
            self.init_ltm_device(hbidps, target_pmbus_device, page_num)

            slot = 1  # dps slot #
            chIndex =  5 # rail_under_test
            hbidps.hbidtb.turnOffAll()
            slotRailPair = "{}_{}".format(slot % 4, int((chIndex % 100)))
            slotRailPairHalf = "{}_{}".format(slot % 4, int((chIndex % 100) / 5))
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_LD_COM_R1_EN_N"])
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_LD_COM_R2_EN_N"])
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_LD_COM_R3_EN_N"])
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_LD_COM_R4_EN_N"])
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_LD_COM_R5_EN_N"])
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_LD_COM_R6_EN_N"])
            reg = 0x02 if (hbidps.hbidtb.dpsHcChToDtbMax14661PinMap[slotRailPair] < 8) else 0x03
            value1 = hbidps.hbidtb.forceSenseBusMapHc[slotRailPairHalf][1]
            value2 = 1 << hbidps.hbidtb.dpsHcChToDtbMax14661PinMap[slotRailPair]
            hbidps.hbidtb.write_max14661(value1, value2, reg)
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_FB1_LD_EN_N"]) # for FB# follow written table
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["HC_FB1_SEN_EN_N"])

            self.set_LTM4688_run(hbidps, data=0x3FF)
            self.setup_delays(hbidps)
            self.off_on_trigger_queue(hbidps, random_hc_rail, rail_under_test)
            voltage = hbidps.read_bar_register(hbidps.registers.HC_CHANNEL_VOLTAGE, rail_under_test).value
            for i in range(25):
                if voltage != 0x040200000:
                    voltage = hbidps.read_bar_register(hbidps.registers.HC_CHANNEL_VOLTAGE, rail_under_test).value
            #        print('voltage =', hex(voltage))
            # self.compare_expected_with_observed(0x040200000, voltage, error_count, iteration,
            #                                    target_pmbus_device, rail_under_test)

            a_info = hbidps.read_bar_register(hbidps.registers.HC_ALARMS)
            #print('alarms = ', a_info)

            # self.on_off_trigger_queue(hbidps,random_lc_rail, rail_under_test)
            if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= self.TEST_ITERATIONS:
                break
        read_back_errors.extend(error_count)
        return read_back_errors

    def setup_delays(self, hbidps):
        self.set_pmbus_command_delay(hbidps, microseconds=100)
        self.set_channel_off_delay(hbidps, microseconds=1000)
        self.set_pmbus_commit_delay(hbidps, microseconds=1000)
        self.set_relay_disconnect_delay(hbidps, microseconds=3)
        self.set_relay_connect_delay(hbidps, microseconds=1500)

    def set_pmbus_command_delay(self, hbidps, microseconds):
        pm_bus_cmd_delay = hbidps.registers.PMBUS_CMD_DELAY()
        pm_bus_cmd_delay.delay = microseconds
        hbidps.write_bar_register(hbidps.registers.PMBUS_CMD_DELAY(value=pm_bus_cmd_delay.value))

    def set_channel_off_delay(self, hbidps, microseconds):
        chan_off_delay = hbidps.registers.CHAN_OFF_DELAY()
        chan_off_delay.delay = microseconds
        hbidps.write_bar_register(hbidps.registers.CHAN_OFF_DELAY(value=chan_off_delay.value))

    def set_pmbus_commit_delay(self, hbidps, microseconds):
        pmbus_com_delay = hbidps.registers.PMBUS_COM_DELAY()
        pmbus_com_delay.delay = microseconds
        hbidps.write_bar_register(hbidps.registers.PMBUS_COM_DELAY(value=pmbus_com_delay.value))

    def set_relay_disconnect_delay(self, hbidps, microseconds):
        relay_dis_delay = hbidps.registers.RELAY_DIS_DELAY()
        relay_dis_delay.delay = microseconds
        hbidps.write_bar_register(hbidps.registers.RELAY_DIS_DELAY(value=relay_dis_delay.value))

    def set_relay_connect_delay(self, hbidps, microseconds):
        relay_conn_delay = hbidps.registers.RELAY_CONN_DELAY()
        relay_conn_delay.delay = microseconds
        hbidps.write_bar_register(hbidps.registers.RELAY_CONN_DELAY(value=relay_conn_delay.value))

    def set_hc_rail_state_off(self, hbidps):
        hc_rail_state = hbidps.registers.HC_RAIL_STATE()
        hc_rail_state.power_state = 0x00
        hbidps.write_bar_register(hbidps.registers.HC_RAIL_STATE(value=hc_rail_state.value))

    def set_ground_control_on(self, hbidps):
        ground_control = hbidps.registers.GROUND_CONTROL_MODE()
        ground_control.rw_mode = 0x1
        hbidps.write_bar_register(hbidps.registers.GROUND_CONTROL_MODE(value=ground_control.value))

    def set_hc_ld_en(self,hbidps):
        hc_ld = hbidps.registers.TPS22976N_ST_HC_LD_EN()
        hc_ld.Data = 0x00
        hbidps.write_bar_register(hbidps.registers.TPS22976N_ST_HC_LD_EN(value=hc_ld.value))

    def set_ground_control_off(self,hbidps):
        ground_control = hbidps.registers.GROUND_CONTROL_MODE()
        ground_control.rw_mode = 0
        hbidps.write_bar_register(hbidps.registers.GROUND_CONTROL_MODE(value=ground_control.value))

    def set_LTM4688_run(self, hbidps, data):
        ltm_run = hbidps.registers.LTM4680_RUN()
        ltm_run.Data = data
        hbidps.write_bar_register(hbidps.registers.LTM4680_RUN(value=ltm_run.value))

    def off_on_trigger_queue(self,hbidps, random_hc_rail, rail_under_test):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        ltmresourceid = 0x81
        railmask = 1 << rail_under_test
        rail = random_hc_rail
        thirty_five_amps = 0x420C0000
        thirty_amps = 0x41F00000
        three_pt_three__volts = 0x40533333
        pt_five_volts = 0x3F000000
        two_pt_five_volts = 0x40200000
        tq_string = f'''
                            WakeRail resourceid ={ltmresourceid},  railmask ={railmask}
                            SetRailBusy resourceid ={ltmresourceid}, railmask ={railmask}
                            SetFdt rail={rail}, value = 100000
                            SetFdCurrentHi rail={rail}, value = {thirty_five_amps}
                            SetPwrState rail={rail}, value = 1
                            SetV rail={rail}, value = {two_pt_five_volts}
                            SetOVLimit rail={rail}, value = {three_pt_three__volts}
                            SetUVLimit rail={rail}, value = {pt_five_volts}
                            SetHiILimit rail={rail}, value = {thirty_amps}
                            ApplyUserAttributes rail={rail}, value = 0
                            TqNotify resourceid ={ltmresourceid}, railmask ={railmask} 
                            TimeDelay rail ={rail}, value = 0x186A0
                            TqComplete rail ={rail}, value = 0
                          '''
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)
        return trigger_queue_data

    def on_off_trigger_queue(self,hbidps, random_hc_rail, rail_under_test):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        ltmresourceid = 0x81
        railmask = 1 << rail_under_test
        rail = random_hc_rail
        four_amps = 0x40800000
        five_amps = 0x40a00000
        seven_volts = 0x40e00000
        tq_string = f'''
                    WakeRail resourceid ={ltmresourceid},  railmask ={railmask}
                    SetRailBusy resourceid ={ltmresourceid}, railmask ={railmask}
                    SetPwrState rail={rail}, value = 0
                    SetUVLimit rail={rail}, value = 0
                    SetFdCurrentHi rail={rail}, value = {four_amps}
                    SetFdt value = 100
                    SetOVLimit rail={rail}, value = {seven_volts}
                    SetHiILimit rail={rail}, value = {five_amps}
                    TqNotify resourceid ={ltmresourceid}, railmask ={railmask}
                    TimeDelay rail ={rail}, value = 0x186A0
                    ApplyUserAttributes rail={rail}, value = 0
                    TqComplete rail ={rail}, value = 0
                 '''
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)
        return trigger_queue_data

    def init_ltm_device(self, hbidps, target_pmbus_device, page_num):
        VOutMode = 0x14
        on_off_config = hbidps.symbols.PMBUSCOMMAND.ON_OFF_CONFIG
        mfr_iin_cal_gain = hbidps.symbols.PMBUSCOMMAND.MFR_IIN_CAL_GAIN
        vin_ov_fault_limit = hbidps.symbols.PMBUSCOMMAND.VIN_OV_FAULT_LIMIT
        vout_transition_rate = hbidps.symbols.PMBUSCOMMAND.VOUT_TRANSITION_RATE
        vout_max = hbidps.symbols.PMBUSCOMMAND.VOUT_MAX
        vout_ov_warn_limit = hbidps.symbols.PMBUSCOMMAND.VOUT_OV_WARN_LIMIT
        vout_uv_warn_limit = hbidps.symbols.PMBUSCOMMAND.VOUT_UV_WARN_LIMIT
        vout_ov_fault_limit = hbidps.symbols.PMBUSCOMMAND.VOUT_OV_FAULT_LIMIT
        vout_uv_fault_limit = hbidps.symbols.PMBUSCOMMAND.VOUT_UV_FAULT_LIMIT
        iout_oc_warn_limit = hbidps.symbols.PMBUSCOMMAND.IOUT_OC_WARN_LIMIT
        iout_oc_fault_limit = hbidps.symbols.PMBUSCOMMAND.IOUT_OC_FAULT_LIMIT
        ot_warn_limit = hbidps.symbols.PMBUSCOMMAND.OT_WARN_LIMIT
        ot_fault_limit = hbidps.symbols.PMBUSCOMMAND.OT_FAULT_LIMIT
        ut_fault_limit = hbidps.symbols.PMBUSCOMMAND.UT_FAULT_LIMIT
        ut_fault_response = hbidps.symbols.PMBUSCOMMAND.UT_FAULT_RESPONSE
        ton_max_fault_response = hbidps.symbols.PMBUSCOMMAND.TON_MAX_FAULT_RESPONSE
        mfr_fault_response = hbidps.symbols.LTM_PMBUSCOMMAND.MFR_FAULT_RESPONSE
        clear_faults = hbidps.symbols.PMBUSCOMMAND.CLEAR_FAULTS
        operation = hbidps.symbols.PMBUSCOMMAND.OPERATION
        iout_oc_fault_response = hbidps.symbols.PMBUSCOMMAND.IOUT_OC_FAULT_RESPONSE
        mfr_pwm_mode = hbidps.symbols.LTM_PMBUSCOMMAND.MFR_PWM_MODE
        mfr_pwm_comp = hbidps.symbols.LTM_PMBUSCOMMAND.MFR_PWM_COMP
        frequency_switch = hbidps.symbols.LTM_PMBUSCOMMAND.FREQUENCY_SWITCH
        vout_ov_fault_response = hbidps.symbols.LTM_PMBUSCOMMAND.VOUT_OV_FAULT_RESPONSE
        vout_uv_fault_response = hbidps.symbols.LTM_PMBUSCOMMAND.VOUT_UV_FAULT_RESPONSE
        ot_fault_response = hbidps.symbols.LTM_PMBUSCOMMAND.OT_FAULT_RESPONSE
        mfr_config_all = hbidps.symbols.LTM_PMBUSCOMMAND.MFR_CONFIG_ALL
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, on_off_config, int.to_bytes(0x1F, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, operation, int.to_bytes(0x00, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, mfr_iin_cal_gain, hil.hilToL11(3.0))

        a = hil.hbiDpsLtm4680PmbusRead(hbidps.slot, target_pmbus_device-4, page_num, mfr_pwm_mode, 1)[0]
        a &= 0x7D
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, mfr_pwm_mode, int.to_bytes(a, 1, byteorder='little'))

        b = MfrPwnComp()
        b.ResComp = 4
        b.ErrorAmplifierGmAdjustMs = 0b11000
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, mfr_pwm_comp, bytes(b))

        c = MfrConfigAll(hil.hbiDpsLtm4680PmbusRead(hbidps.slot, target_pmbus_device-4, page_num, mfr_config_all, 1))
        c.disable_sync_output = 1
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device - 4, page_num, mfr_config_all, c.value)

        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, frequency_switch, int.to_bytes(0x028A, 2, byteorder='little'))
        time.sleep(0.001)
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vin_ov_fault_limit, hil.hilToL11(14.0))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_transition_rate, hil.hilToL11(1.0))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_max, hil.hilToL16(3.6,VOutMode))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_ov_warn_limit, hil.hilToL16(5, VOutMode))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_uv_warn_limit, hil.hilToL16(0, VOutMode))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_ov_fault_response, int.to_bytes(0x40, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_uv_fault_response, int.to_bytes(0x40, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_ov_fault_limit, hil.hilToL16(3.6, VOutMode))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, vout_uv_fault_limit, hil.hilToL16(0, VOutMode))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, iout_oc_warn_limit, hil.hilToL11(50))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, iout_oc_fault_limit, hil.hilToL11(35))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, iout_oc_fault_response, int.to_bytes(0xC0, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, ot_warn_limit, hil.hilToL11(100))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, ot_fault_limit, hil.hilToL11(100))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, ot_fault_response, int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, ut_fault_limit, hil.hilToL11(-45))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, ut_fault_response, int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, ton_max_fault_response, int.to_bytes(0x80, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, mfr_fault_response, int.to_bytes(0xC0, 1, byteorder='little'))
        hil.hbiDpsLtm4680PmbusWrite(hbidps.slot, target_pmbus_device-4, page_num, clear_faults, int.to_bytes(0x00, 1, byteorder='little'))


class Functional(HbidpsTest):
    """ 7 Test Cases of HC Rail Operation Included

        Need to clean up the upper class and test cases;
        TQ helper creation in a separate helper function;
        Please refer to HBI_DPS_Channel_Transition.docx for Operation detail;

        Steps for following tests:
        1.Configure the DTB bord for the output load
        2.Check rail statues and set to steady mode (off) if needed
        3.Check global alarm and reset if needed
        4.Configure DUT_DOMAIN_ID_NN with dut_id
        5.Creat TQ content
        6.Store TQ content and lut content
        7.Clear tq_notify flag in global alarm register
        8.Send trigger with configured dut_id
        9.Check tq_notify flag in global alarm register
        10.Check voltage and current value of selected rail
        11.Check global alarm (should be clean)
        12.Call HC rail off mode TQ method
        13.Check global alarm and reset if needed
    """

    def DirectedSelfTestinitTest(self):
        hbidps_one = self.env.get_fpgas()[0]
        print(hbidps_one)

        vtvm = VtVmBus(hbidps_one)
        cfb = CommonForceBus(hbidps_one)

        vtvm.initialize_vtvm_bus()
        cfb.initialize_common_force_bus()


    def HCRailinitDebugTest(self):
        """ This is an example to use one DPS for following:
        1. HC device initialize
        2. TQ content generation
        3. TQ execution through DDR

        Note: This code will be refactored to make it clean
        Every steps are list here to make it easier for understanding
        the hardware

        To Do:
        1. back to safe state at end of test
        2. refactor 
        """
        print(self.env.get_fpgas())
        hbidps_one = self.env.get_fpgas()[0]
        print(hbidps_one)

        hc_rail = HCRailPMBUS(hbidps_one)
        rail_transition = RailTransitionControl(hbidps_one)
        tq_generator = TriggerQueueCommands()
        tq_ddr = TriggerQueueDDR(hbidps_one)
        mdut_config = MdutConfiguration(hbidps_one)
        alarm_helper = AlarmHelper(hbidps_one)

        alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        alarm_helper.detect_global_alarm_status_one_dps()

        hc_rail.init_hc_rails_hw(True)
        rail_transition.init_rail_transition_time()
        rail_transition.log_rail_transition_time()

        hc_rail.log_hc_run_reg()
        hc_rail.enable_hc_run_reg(True)
        mdut_config.set_uhc_dut_status(uhc_id=0, dut_id=1, valid=1, log=True)

        self.check_hc_fold_status(hbidps_one)

        meta_data = random.randrange(0xfff)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, 0x3)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, 0x3)
        tq_generator.add_command(tq_cmd.set_vrange, tq_dev.HCrail0, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_vrange, tq_dev.HCrail1, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, tq_dev.HCrail0, 0x40400000)
        tq_generator.add_command(tq_cmd.set_ov_limit, tq_dev.HCrail1, 0x40400000)
        tq_generator.add_command(tq_cmd.set_uv_limit, tq_dev.HCrail0, 0x0)
        tq_generator.add_command(tq_cmd.set_uv_limit, tq_dev.HCrail1, 0x0)
        tq_generator.add_command(tq_cmd.set_v, tq_dev.HCrail0, 0x40000000)
        tq_generator.add_command(tq_cmd.set_v, tq_dev.HCrail1, 0x40000000)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, tq_dev.HCrail0, 0x40000000)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, tq_dev.HCrail1, 0x40000000)
        tq_generator.add_command(tq_cmd.set_pwr_state, tq_dev.HCrail0, 0x1)
        tq_generator.add_command(tq_cmd.set_pwr_state, tq_dev.HCrail1, 0x1)
        tq_generator.add_command(tq_cmd.set_fdt, tq_dev.HCrail0, 0x01000)
        tq_generator.add_command(tq_cmd.set_fdt, tq_dev.HCrail1, 0x01000)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, tq_dev.HCrail0, 0x40000000)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, tq_dev.HCrail1, 0x40000000)
        tq_generator.add_command(tq_cmd.apply_user_attributes, tq_dev.HCrail0, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, tq_dev.HCrail1, 0)
        tq_generator.add_command(tq_cmd.smp_metadata, tq_dev.HCSampleEngine0, meta_data)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, 0x3)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()

        tq_data = tq_ddr.get_tq_data(tq_list, True)
        lut_address = tq_ddr.get_dps_lut_address(0, 0)
        tq_content_address = tq_ddr.get_tq_content_address()
        tq_ddr.store_tq_address_to_lut(lut_address, tq_content_address, True)
        tq_ddr.store_tq_content_to_mem(tq_content_address, tq_data, True)

        tq_ddr.clear_tq_notify(True)

        hc_rail.log_hc_rail_dps_power_state()
        # hc_rail.enable_hc_rail_dps_power_state()

        global_alarm = alarm_helper.read_global_alarm_reg()
        self.Log('info', f'alarm read after notify clear {hex(global_alarm.value)}')

        hc_vrange_rail0 = hbidps_one.read_bar_register(hbidps_one.registers.HC_RAIL_NN_VRANGE, index=0)
        self.Log('info', f'hc_vrange_rail0 {hex(hc_vrange_rail0.value)}')
        hc_vrange_rail1 = hbidps_one.read_bar_register(hbidps_one.registers.HC_RAIL_NN_VRANGE, index=1)
        self.Log('info', f'hc_vrange_rail1 {hex(hc_vrange_rail1.value)}')

        tq_ddr.send_dps_trigger_through_rctc(0x01, 1, 0)
        time.sleep(1)

        self.Log('info', '******************trigger_done******************')

        hc_rail.log_hc_rail_dps_power_state()

        meta = hbidps_one.read_bar_register(hbidps_one.registers.HC_SAMPLE_METADATA_0)
        self.Log('info', f' meta {hex(meta_data)} {hex(meta.value)}')

        hc_vrange_rail0 = hbidps_one.read_bar_register(hbidps_one.registers.HC_RAIL_NN_VRANGE, index=0)
        self.Log('info', f'hc_vrange_rail0 {hex(hc_vrange_rail0.value)}')
        hc_vrange_rail1 = hbidps_one.read_bar_register(hbidps_one.registers.HC_RAIL_NN_VRANGE, index=1)
        self.Log('info', f'hc_vrange_rail1 {hex(hc_vrange_rail1.value)}')

        alarm_helper.detect_global_alarm_status_one_dps()
        self.check_hc_fold_status(hbidps_one)
        tq_ddr.check_tq_notify(0, 1, 0, True)
        hc_rail.read_ltm4680_info_set(0, 0)

        # self.Log('info', f'turn on operation')
        # hc_rail.set_operation_on(0, 0)
        # time.sleep(3)
        #
        # hc_rail.read_ltm4680_info_set(0, 0)


    def check_hc_fold_status(self, hbidps):
        hc_fold_status = hbidps.read_bar_register(hbidps_register.HC_FOLDED)
        hc_fold_status.value = 0x3FF
        hbidps.write_bar_register(hc_fold_status)
        time.sleep(0.1)
        hc_fold_status = hbidps.read_bar_register(hbidps_register.HC_FOLDED)
        self.Log('info', f'hc_fold_status {hex(hc_fold_status.value)}')


    def DirectedHCRailOfftoOffTest(self):
        """ Set one HC rail from float mode to float mode"""
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_off_to_off_zvf_on_flow(vout=None, dtb_feedback=False, rail=rail, hbidps=hbidps)

    def DirectedHCRailOfftoZeroVoltForceTest(self):
        """ Set one HC rail from float mode to ground force mode """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_off_to_off_zvf_on_flow(vout=0, dtb_feedback=False, rail=rail, hbidps=hbidps)

    def DirectedHCRailOfftoOnTest(self):
        """ Set one HC rail from float mode to >0.5V force mode """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_off_to_off_zvf_on_flow(vout=1.3, dtb_feedback=False, rail=rail, hbidps=hbidps)

    def DirectedHCRailOfftoOnDTBFeedbackTest(self):
        """ Set one HC rail from float mode to >0.5V force mode with DTB feedback loop"""
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_off_to_off_zvf_on_flow(vout=1.3, dtb_feedback=True, rail=rail, hbidps=hbidps)

    def DirectedHCRailOntoOffTest(self):
        """ Set one HC rail from >0.5V force mode to float mode

            Test steps:
                1. set the rails from the OFF to ON state.
                2. Once the rails are in the ON state, then set the rails from the ON to OFF state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_on_to_off_zvf_on_flow(vout=None, rail=rail, hbidps=hbidps, vout_initial=1.3)

    def DirectedHCRailOntoZeroVoltForceTest(self):
        """ Set one HC rail from >0.5V force mode to ground force mode

            Test steps:
                1. set the rails from the OFF to ON state.
                2. Once the rails are in the ON state, then set the rails from the ON to ZVF state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_on_to_off_zvf_on_flow(vout=0, rail=rail, hbidps=hbidps, vout_initial=1.3)

    def DirectedHCRailVbumpUpTest(self):
        """ Set one HC rail from >0.5V force mode to other >0.5V force mode

            Test steps:
                1. set the rails from the OFF to ON state.
                2. Once the rails are in the ON state, then set the rails from the ON (low output voltage)
                   to ON (high output voltage) state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_on_to_off_zvf_on_flow(vout=1.3, rail=rail, hbidps=hbidps, vout_initial=1)

    def DirectedHCRailVbumpDownTest(self):
        """ Set one HC rail from >0.5V force mode to other >0.5V force mode 

            Test steps:
                1. set the rails from the OFF to ON state.
                2. Once the rails are in the ON state, then set the rails from the ON (high output voltage)
                   to ON (low output voltage) state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_on_to_off_zvf_on_flow(vout=1, rail=rail, hbidps=hbidps, vout_initial=1.3)

    def DirectedHCRailZeroVoltForcetoZeroVoltForceTest(self):
        """ Set one HC rail from ground force mode mode to ground force mode

            FPGA Review Note:
            Blocking transition feature should block this. Check with FPGA

            Test steps:
                1. set the rails from the OFF to ZVF state.
                2. Once the rails are in the ZVF state, then set the rails from the ZVF to ZVF state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_zvf_to_off_zvf_on_flow(vout=0, rail=rail, hbidps=hbidps, vout_initial=0)

    def DirectedHCRailZeroVoltForcetoOffTest(self):
        """ Set one HC rail from ground force mode to float mode

            Test steps:
                1. set the rails from the OFF to ZVF state.
                2. Once the rails are in the ZVF state, then set the rails from the ZVF to OFF state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_zvf_to_off_zvf_on_flow(vout=None, rail=rail, hbidps=hbidps, vout_initial=0)

    def DirectedHCRailZeroVoltForcetoOnTest(self):
        """ Set one HC rail from ground force mode to >0.5V force mode

            Test steps:
                1. set the rails from the OFF to ZVF state.
                2. Once the rails are in the ZVF state, then set the rails from the ZVF to ON state.
        """
        random_slot = random.randint(0, 15)
        hbidps = self.hbidps_list[random_slot]
        for rail in range(10):
            self.general_hc_1rail_zvf_to_off_zvf_on_flow(vout=2, rail=rail, hbidps=hbidps, vout_initial=0)

    @skip('New Feature HDMT Ticket 89603')
    def DirectedHCRailFlushTest(self):
        """ Set one HC rail with flush command

            Test steps:
                1. flush should happen without the delay setting in rail
        """
        pass

    @skip('New Feature HDMT Ticket 127291')
    def DirectedHCRailONtoZVFVoutTest(self):
        """ Set one HC rail from >0.5V force mode to ground force mode

            Test steps:
                1. set the rails from the OFF to ON state.
                2. Once the rails are in the ON state, then set the rails from the ON to ZVF state.
                3. Check the vout register in the external device
        """
        pass

    def disconnect_dtb_max14661_test(self):
        self.hbidps_list[0].disconnect_dtb_all_max14661_devices(True)

    def reset_dtb_pca9505_test(self):
        self.hbidps_list[0].reset_dtb_all_pca9505_devices(True)

    def hc_lc_dtb_init_test(self):
        self.hbidps_list[0].initialize_dtb_hc_lc_mux(True)

    def config_dtb_hc_rail_test(self):
        self.hbidps_list[0].config_dtb_hc_rail_load_feedback(1, True)

    def general_hc_1rail_off_to_off_zvf_on_flow(self, vout, dtb_feedback, rail=None, hbidps=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            hbidps.enable_hc_run_pins()
            hbidps.hc_device_pretest_check(rail, vout)
            self.check_all_dps_power_state()
            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            if dtb_feedback:
                hbidps.config_dtb_hc_rail_load_feedback(rail, False)
            tq_list = self.create_hc_1rail_off_to_on_tq_list(rail=rail, vout=vout)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            print(time_delta)
            time.sleep(0.5)
            self.check_vout_readback(hbidps, vout, rail)
            self.hc_safe_off_flow(hbidps, rail)

    def hc_safe_off_flow(self, hbidps, rail):
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_hc_1rail_safe_off_tq_list(rail=rail)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
        self.Log('info', f'hc safty off done')


    def general_hc_1rail_on_to_off_zvf_on_flow(self, vout, rail=None, hbidps=None, vout_initial=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            self.check_all_dps_power_state()
            hbidps.hc_device_pretest_check(rail, vout_initial)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout_initial)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout)
            self.hc_safe_off_flow(hbidps, rail)

    def general_hc_1rail_zvf_to_off_zvf_on_flow(self, vout, rail=None, hbidps=None, vout_initial=None):
        """ Set one HC rail from float mode to >0.5V force mode """
        if rail == None:
            rail = random.randint(0,9)
        if hbidps == None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'rail{rail}; hbidps{hbidps.slot}')

        with HCRailsEnv(hbidps):
            self.check_all_dps_power_state()
            hbidps.hc_device_pretest_check(rail, vout_initial)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout_initial)
            self.hc_1rail_arbitrary_initial_state_test(hbidps, rail, vout)
            self.hc_safe_off_flow(hbidps, rail)

    def hc_1rail_arbitrary_initial_state_test(self, hbidps, rail, vout):
        hbidps.enable_hc_run_pins()
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_hc_1rail_off_to_on_tq_list(rail=rail, vout=vout)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
        print(time_delta)
        time.sleep(0.5)
        self.check_vout_readback(hbidps, vout, rail)

    def create_hc_1rail_safe_off_tq_list(self, rail):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <=9:
            hc_one_hot = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
            pwr_state = 0b10
            ov = 3.6
            uv = 0.0
        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        # vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        # tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def create_hc_1rail_off_to_on_tq_list(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <=9:
            hc_one_hot = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
        if vout == None:
            vout_on = 1
            pwr_state = 0b00
            ov = 3.6
            uv = 0.0
        elif vout == 0:
            vout_on = 1
            pwr_state = 0b10
            ov = 3.6
            uv = 0.0
        elif 0.5 <= vout <= 3:
            vout_on = vout
            pwr_state = 0b01
            ov = 3.6
            uv = 0
        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.flush_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def check_vout_readback(self, hbidps, expect_vout, rail):
        if expect_vout==None:
            expect_vout = 0
        read_vout = hbidps.hc_rail_voltage_read(rail)
        hbidps.read_hc_rails_fpga_status()
        hbidps.detect_global_alarm()
        if (read_vout>expect_vout+0.3) or (read_vout<expect_vout-0.3):
            self.Log('error', f'rail{rail} vout_fail expect:{expect_vout}V '
                              f'read: {read_vout}V')
            hbidps.read_one_hc_rail_device_status(rail)
            # hbidps.read_hc_rails_device_status()
        else:
            self.Log('info', f'vout_pass expect:{expect_vout}V '
                              f'read: {read_vout}V')

    def check_all_dps_power_state(self):
        for dps in self.hbidps_list:
            power_state_list = dps.get_hc_power_state()
            if power_state_list != 0:
                self.Log('warning', f'dps{dps.slot} power 0x{power_state_list:05x}')

class HCRailsEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_hc_rails_start_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_hc_rails_to_safe_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)
