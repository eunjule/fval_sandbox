################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing VTarget and Vmeasure interface"""
import random
import time

from Hbidps.Tests.HbidpsTest import HbidpsTest
from Hbidps.instrument import hbidps_register
from Hbidps.instrument.AlarmSystem import AlarmHelper
from Hbidps.instrument.RailTransitionControl import RailTransitionControl
from Hbidps.instrument.SelfTestBlock import VtVmBus
from Hbidps.instrument.VtargetDevice import VTargetDevice
from Hbidps.instrument.MdutConfiguration import MdutConfiguration
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.instrument.TQMachine import TriggerQueueDDR
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands

MAX_FAIL_COUNT = 100
VT_TQ_RAIL_ID_BASE = 0x40


class Functional(HbidpsTest):
    """Force voltage via VTarget and measure at Vmeasure"""
    test_iteration_count = 1000

    def debug_hil_vt_dummy_test(self):
        rail = 0
        hbidps_one = self.env.get_fpgas()[0]

        self.vtarget_device = VTargetDevice(hbidps_one)
        self.vtarget_device.init_vtarget_device(True)
        self.vtarget_device.set_dac_reg(rail, 0x1000)  # should be Vref / 4
        self.vtarget_device.read_ad5676_info_set(True)
        self.vtarget_device.init_vtarget_device()
        self.vtarget_device.read_ad5676_info_set(True)
        dac_reg_val = self.vtarget_device.read_dac_reg(rail)
        if dac_reg_val == 0x1000:
            self.Log('info', f'the value is successfully write into rail {rail} dac reg and read back')
        else:
            self.Log('error', f'the value is not successfully write into rail {rail} dac reg and read back;'
                              f'observed: {dac_reg_val}, expected: {0x1000}')

    def debug_hil_vtvm_test(self):
        vt_rail = 0
        vm_rail = 1
        V_ref = 2.5
        V_expected = 1.5
        # dac_reg_write = int(0x10000 // (2*V_ref / V_expected))
        dac_reg_write = int(V_expected/(2*V_ref/0x10000))
        self.Log('info', f'dac_reg_write = {hex(dac_reg_write)}')
        hbidps_one = self.env.get_fpgas()[0]

        self.vtarget_device = VTargetDevice(hbidps_one)
        self.vtarget_device.init_vtarget_device(True)
        self.vtarget_device.set_dac_reg(vt_rail, dac_reg_write)  # should be Vref / 2

        vtvmbus = VtVmBus(hbidps_one)
        vtvmbus.turn_on_and_reset_all_max14662_switch()
        vtvmbus.get_state_attribute_from_board(True)
        vtvmbus.clear_stage_n_attribute()
        vtvmbus.turn_on_a_pair_of_vtvm_channel(vt_rail, vm_rail)
        vtvmbus.check_safe_before_apply_attribute()
        vtvmbus.apply_next_state_attribute()
        vtvmbus.update_state_attribute()
        vtvmbus.check_state_attribute_from_board(True)

        self.vtarget_device.turn_on_rail(0)

        time.sleep(0.5)

        self.vtarget_device.read_ad5676_info_set(True)

        vm_rail_read = hbidps_one.hbi_dps_vmeasure_voltage_read(vm_rail)
        if abs(vm_rail_read - V_expected) < 0.1:
            self.Log('info', f'set voltage at vt rail {vt_rail} and read voltage from vm rail {vm_rail} success!')
        else:
            self.Log('error', f'set voltage at vt rail {vt_rail} and read voltage from vm rail {vm_rail} fails!'
                              f'observed: {vm_rail_read}, expected {V_expected}')

    def debug_tq_vtvm_test(self):
        vt_rail = 0
        vm_rail = 1
        V_ref = 2.5
        V_expected = 0.9
        dac_reg_write = int(V_expected / (2 * V_ref / 0x10000))
        hbidps_one = self.env.get_fpgas()[0]

        self.vtarget_device = VTargetDevice(hbidps_one)

        vtvmbus = VtVmBus(hbidps_one)
        vtvmbus.turn_on_and_reset_all_max14662_switch()
        vtvmbus.get_state_attribute_from_board(True)
        vtvmbus.clear_stage_n_attribute()
        vtvmbus.turn_on_a_pair_of_vtvm_channel(vt_rail, vm_rail)
        vtvmbus.check_safe_before_apply_attribute()
        vtvmbus.apply_next_state_attribute()
        vtvmbus.update_state_attribute()
        vtvmbus.check_state_attribute_from_board(True)

        self.rail_transition = RailTransitionControl(hbidps_one)
        self.tq_generator = TriggerQueueCommands()
        self.tq_ddr = TriggerQueueDDR(hbidps_one)
        self.mdut_config = MdutConfiguration(hbidps_one)
        self.alarm_helper = AlarmHelper(hbidps_one)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        self.alarm_helper.detect_global_alarm_status_one_dps()

        self.vtarget_device.init_vtarget_device(True)

        self.rail_transition.init_rail_transition_time()  # maybe not needed
        self.rail_transition.log_rail_transition_time()

        self.mdut_config.set_uhc_dut_status(uhc_id=0, dut_id=1, valid=1, log=True)

        # check folded status

        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, 0x1)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, 0x1)
        self.tq_generator.add_command(tq_cmd.vtarg_set, tq_dev.Vtarget0, dac_reg_write)
        self.tq_generator.add_command(tq_cmd.vtarg_pwr, tq_dev.Vtarget0, 0b00)
        self.tq_generator.add_command(tq_cmd.apply_user_attributes, tq_dev.Vtarget0, 0)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, 0x1)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()

        tq_data = self.tq_ddr.get_tq_data(tq_list, True)
        lut_address = self.tq_ddr.get_dps_lut_address(0, 0)
        tq_content_address = self.tq_ddr.get_tq_content_address()
        self.tq_ddr.store_tq_address_to_lut(lut_address, tq_content_address, True)
        self.tq_ddr.store_tq_content_to_mem(tq_content_address, tq_data, True)

        self.tq_ddr.clear_tq_notify(True)

        global_alarm = self.alarm_helper.read_global_alarm_reg()

        self.Log('info', f'alarm read after notify clear {hex(global_alarm.value)}')

        self.tq_ddr.send_dps_trigger_through_rctc(0x01, 1, 0)

        self.tq_ddr.wait_for_notify(0, 0, 1, True)

        self.Log('info', '******************trigger_done******************')
        self.vtarget_device.read_ad5676_info_set(True)
        self.check_vt_fold_status(hbidps_one)
        vm_rail_read = hbidps_one.hbi_dps_vmeasure_voltage_read(vm_rail)
        self.Log('info', f'vm_rail_read = {vm_rail_read}, V_expected = {V_expected}')
        if abs(vm_rail_read - V_expected) < 0.1:
            self.Log('info', f'tq set voltage at vt rail {vt_rail} and read voltage from vm rail {vm_rail} success!')
        else:
            self.Log('error', f'tq set voltage at vt rail {vt_rail} and read voltage from vm rail {vm_rail} fails!'
                              f'observed: {vm_rail_read}, expected {V_expected}')

    def check_vt_fold_status(self, hbidps):
        vt_fold_status = hbidps.read_bar_register(hbidps_register.VTARG_FOLDED)
        vt_fold_status.value = 0xFFFF
        hbidps.write_bar_register(vt_fold_status)
        time.sleep(0.1)
        vt_fold_status = hbidps.read_bar_register(hbidps_register.VTARG_FOLDED)
        self.Log('info', f'vtarget_fold_status {hex(vt_fold_status.value)}')

    def RandomForceAndReadbackVoltageTest(self):
        """Force random voltage on vtarget and measure it at vmeasure

        Steps for following tests:
        1.Check Vtarget & Vmeasure statues
        2.Check global alarm and reset if needed
        3.Check the self-test routing channels status and connect channels
        4.Configure DUT_DOMAIN_ID_NN with dut_id
        5.Creat TQ content
        6.Store TQ content and lut content
        7.Clear tq_notify flag in global alarm register
        8.Send trigger with configured dut_id
        9.Check tq_notify flag in global alarm register
        10.Check vmeasure result
        11.Check global alarm (should be clean)
        12.Call Vtarget & Vmeasure rail off mode TQ method
        13.Call self-test disconnect method
        14.Check global alarm and reset if needed

        FPGA Review Note:
        3MODE Vtarget can do. Double check with FPGA side
        """
        for rail in range(16):
            vout = random.random() * 4.5
            self.vtvm_single_force_and_readback_test(v_expected=vout, vm_rail=rail, vt_rail=rail)

    def RandomForceAndReadbackVoltageZeroToOnToZeroTest(self):

        """Force random voltage on vtarget and measure it at vmeasure

            Steps for following tests:
                1.Check Vtarget & Vmeasure statues
                2.Check global alarm and reset if needed
                3.Check the self-test routing channels status and connect channels
                4.Configure DUT_DOMAIN_ID_NN with dut_id
                5.Creat TQ content which turn rail from off to normal state
                6.Store TQ content and lut content
                7.Clear tq_notify flag in global alarm register
                8.Send trigger with configured dut_id
                9.Check tq_notify flag in global alarm register
                10.Check vmeasure result
                11.repeat step 4 to step 10 to other voltage values and back to off in the end
                11.Check global alarm (should be clean)
                12.Call Vtarget & Vmeasure rail off mode TQ method ?set vtarget to off state
                13.Call self-test disconnect method
                14.Check global alarm and reset if needed
        """
        vt_rail = random.randint(0, 15)
        vm_rail = random.randint(0, 15)
        v_1 = random.random() * 4.5
        v_2 = random.random() * 4.5
        self.vtvm_zero_to_on_to_zero_test(v_1=v_1, v_2=v_2, vm_rail=vm_rail, vt_rail=vt_rail)

    def vtvm_single_force_and_readback_test(self, v_expected, vm_rail=None, vt_rail=None, hbidps=None):
        if vm_rail is None:
            vm_rail = random.randint(0, 15)
        if vt_rail is None:
            vt_rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.hbidps_list[0]
        self.Log('info', f'{"*"*10}Testing vt rail {vt_rail} ----> vm rail {vm_rail}; hbidps{hbidps.slot}{"*"*10}')

        with VTVMRailsEnv(hbidps):
            self.execute_one_force_and_readback(hbidps, v_expected, vm_rail, vt_rail)

    def vtvm_zero_to_on_to_zero_test(self, v_1, v_2, vm_rail=None, vt_rail=None, hbidps=None):
        if vm_rail is None:
            vm_rail = random.randint(0, 15)
        if vt_rail is None:
            vt_rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.hbidps_list[4]
        self.Log('info', f'{"*"*10}Testing vt rail {vt_rail} ----> vm rail {vm_rail}; hbidps{hbidps.slot}{"*"*10}')

        with VTVMRailsEnv(hbidps):
            self.execute_one_force_and_readback(hbidps, v_1, vm_rail, vt_rail)
            self.execute_one_force_and_readback(hbidps, v_2, vm_rail, vt_rail)
            self.execute_one_force_and_readback(hbidps, 0, vm_rail, vt_rail)
            self.execute_one_force_and_readback(hbidps, 0, vm_rail, vt_rail, 0b11)
            self.execute_one_force_and_readback(hbidps, 1.2, vm_rail, vt_rail, 0b01)

    # add DTB connect vt vm to be True
    def execute_one_force_and_readback(self, hbidps, v_expected, vm_rail, vt_rail, pwr_state=0b00, dtb_connect=True):
        """ dtb_connect means DTB is coonnected to vt and vm rails and therefore  vt and vm rails
            with the same index are connected by default in DTB
        """
        V_ref = 2.5
        dac_reg_write = int(v_expected / (2 * V_ref / 0x10000))
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        expected_vm_read = []
        for rail in range(16):
            rail_read = hbidps.hbi_dps_vmeasure_voltage_read(rail)
            expected_vm_read.append(rail_read)
        if pwr_state == 0b00:
            expected_vm_read[vm_rail] = v_expected
            if dtb_connect:
                expected_vm_read[vt_rail] = v_expected
        elif pwr_state == 0b01:
            expected_vm_read[vm_rail] = 0
            if dtb_connect:
                expected_vm_read[vt_rail] = 0
        hbidps.connect_vt_vm_through_vtvmbus(vm_rail, vt_rail)
        tq_list = self.create_vt_1rail_voltage_force_tq_list(dac_reg_write, vt_rail, pwr_state)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()

        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=0, vt=1, time_start=time_start, log=True)
        self.Log('info', f'trigger queue execution waiting time: {time_delta}')

        time.sleep(0.5)
        self.check_vt_vm_result(hbidps, expected_vm_read, vm_rail, vt_rail, pwr_state, dtb_connect)

    def create_vt_1rail_voltage_force_tq_list(self, dac_reg_write, vt_rail, pwr_state=0b00):
        tq_generator = TriggerQueueCommands()
        if 0 <= vt_rail <= 15:
            vt_one_hot = 1 << vt_rail
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE + vt_rail)
        else:
            vt_one_hot = 0
            vt_rail = tq_dev(VT_TQ_RAIL_ID_BASE)
            self.Log('error', f'rail number {vt_rail} given to vt trigger queue is not valid')
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, vt_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, vt_one_hot)
        tq_generator.add_command(tq_cmd.vtarg_set, vt_rail, dac_reg_write)
        tq_generator.add_command(tq_cmd.vtarg_pwr, vt_rail, pwr_state)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, vt_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def check_vt_vm_result(self, hbidps, expected_vm_read, vm_rail, vt_rail, pwr_state, dtb_connect=True):
        hbidps.check_vt_fold_status()
        vm_rail_read = hbidps.hbi_dps_vmeasure_voltage_read(vm_rail)
        if pwr_state != 0b11:
            self.Log('info', f'vm_rail_read = {vm_rail_read}, V_expected = {expected_vm_read[vm_rail]}')
            if abs(vm_rail_read - expected_vm_read[vm_rail]) < 0.1:
                self.Log('info', f'tq set voltage at vt rail {vt_rail} '
                                 f'and read voltage from vm rail {vm_rail} success!')
            else:
                self.Log('error', f'tq set voltage at vt rail {vt_rail} and '
                                  f'read voltage from vm rail {vm_rail} fails!'
                                  f'observed: {vm_rail_read}, expected {expected_vm_read[vm_rail]}')
        else:
            if abs(vm_rail_read) < 0.3:
                self.Log('error', f'Three-State VT force has unexpected 0 V voltage read')
            else:
                self.Log('info', f'Three-State VT force test is successful!')
        for rail in range(16):
            if rail != vm_rail:
                if not dtb_connect or rail != vt_rail:
                    rail_read = hbidps.hbi_dps_vmeasure_voltage_read(rail)
                    if abs(rail_read - expected_vm_read[rail]) > 0.2:
                        self.Log('error', f'VM Rail {rail}: has unexpected voltage read: {rail_read}V '
                                          f'while expected is {expected_vm_read[rail]}V')

        power_state = hbidps.read_vtarg_power_state().value
        rail_pwr_state = (power_state >> (vt_rail * 2)) & 0b11
        if rail_pwr_state != pwr_state:
            self.Log('error', f'Vtarget rail {vt_rail} power state {bin(rail_pwr_state)} '
                              f'!= expected {bin(pwr_state)}'
                              f'the whole register: {hex(power_state)}')


class VTVMRailsEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_vtarget_rails_start_state()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_vtvm_rails_to_safe_state()
        self.hbidps.turn_off_all_vt_vm_switches()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)
