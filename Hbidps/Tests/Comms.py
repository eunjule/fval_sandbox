# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


""" Testing of Communication interfaces between - 

The FPGA and LTC2975 over PMBus

The FPGA and LTM4680 over PMBus

The Max14662 Stress Tests
"""
import random
import struct
import time

from ThirdParty.SASS.suppress_sensor import suppress_sensor
from Common.fval import expected_failure
from Common.fval import skip
from Hbidps.testbench.Ganging_Max14662_I2C import MAX_GANG_CHIPS, GangMax14662I2cInterface
from Hbidps.testbench.lc import LCRailTestbench
from Hbidps.testbench.Loopback_Max14662_I2C import MAX_LB_CHIPS, LoopbackMax14662I2cInterface
from Hbidps.testbench.PMBusTest import PMBUSTestInterface
from Hbidps.testbench.VSense_Max14662_I2C import MAX_VS_CHIPS, VSenseMax14662I2cInterface
from Hbidps.Tests.HbidpsTest import HbidpsTest
from Hbidps.instrument.AlarmSystem import AlarmHelper

class BaseTest(HbidpsTest):

    def _report_errors(self, device, register_errors, error_message_to_print):
        column_headers = ['Iteration', 'Expected', 'Observed', 'Rail', 'Target pmbus device']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print, table), 'error')


class PMBusCommTests(BaseTest):
    """
    Run PMBus Tests to LTC2975 and LCM4680
    """

    NUM_LC_PMBUS_INTERFACES = 4
    NUM_HC_PMBUS_INTERFACES = 4
    STRESS_TEST_ITERATIONS = 10000
    LC_COMM_STRESS_REGISTER_WORD = 0xb4
    HC_COMM_STRESS_REGISTER_WORD = 0xb4
    LC_COMM_STRESS_REGISTER_BYTE = 0xda
    HC_COMM_STRESS_REGISTER_BYTE = 0xd8

    def _report_errors(self, device, register_errors, error_message_to_print):
        column_headers = ['Iteration', 'Expected', 'Observed', 'Device']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print, table), 'error')

    def _print_test_results(self, hbidps, read_back_errors, test_name):
            if read_back_errors:
                error_message_to_print = f'{test_name} failed '
                self._report_errors(hbidps, read_back_errors, error_message_to_print)
            else:
                hbidps.log_device_message(
                    f'{test_name} successful.', 'info')

    @expected_failure('All PMBus will fail the reset(TFS110132), retry register(TFS117973) and FIF0(TFS117865) tests ')
    def Directed_PMBus_Test(self):
        """
        This is a set of tests to runthe HVBIDPS PMBus to the 4 LTC2975 and 4 LCM4680
        1. read/write stress test
        2. Check the error retry and busy fields of the PMBus registers
        3. Check the TX/RX Fifo Count
        4. Check Reset bit functionality
        5. Stress the send Byte functionality
        """
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            lcrail = LCRailTestbench()
            lcrail.setup_delays(hbidps)
            lcrail.init_lc_for_test(hbidps)
            pmbus_interfaces = self._create_lc_pmbus_interfaces(hbidps)
            pmbus_interfaces += self._create_hc_pmbus_interfaces(hbidps)
            for pmbus_interface in pmbus_interfaces:
                read_back_errors = self._pmbus_stress_test(pmbus_interface, word=True)
                self._print_test_results(hbidps, read_back_errors,
                                             f'Stress Test Word PMBusTests {pmbus_interface.interface_name}')
                read_back_errors = self._pmbus_stress_test(pmbus_interface, word=False)
                self._print_test_results(hbidps, read_back_errors,
                                             f'Stress Test Byte PMBusTests {pmbus_interface.interface_name}')
                read_back_errors = self._check_error_busy_retry(pmbus_interface)
                self._print_test_results(hbidps, read_back_errors,
                                             f'Check PMBusTests register bits {pmbus_interface.interface_name}')
                read_back_errors = self._test_fifo_count(pmbus_interface)
                self._print_test_results(hbidps, read_back_errors,
                                             f'Test Fifo Count PMBusTests {pmbus_interface.interface_name}')
                read_back_errors = self._check_reset(pmbus_interface)
                self._print_test_results(hbidps, read_back_errors,
                                             f'Test reset PMBusTests {pmbus_interface.interface_name}')
                read_back_errors = self._check_send_byte(pmbus_interface)
                self._print_test_results(hbidps, read_back_errors,
                                             f'Test Send Byte PMBusTests {pmbus_interface.interface_name}')

    def _pmbus_stress_test(self, pmbus_interface, word=True):
        error_list = []
        write_byte = 1
        read_byte = 2
        write_word = 3
        read_word = 4
        if word:
            write = write_word
            read = read_word
            address = pmbus_interface.comm_stress_reg_word
            num_bits = 16
            type = 'word'
        else:
            write = write_byte
            read = read_byte
            address = pmbus_interface.comm_stress_reg_byte
            num_bits = 8
            type = 'byte'
        self.Log('info', f'Starting _pmbus_stress_test_{type} for {pmbus_interface.interface_name}')
        pmbus_interface.reset()

        with suppress_sensor(f'FVAL HBIDPS {pmbus_interface.instrument.slot}', f'HBIDPS:{pmbus_interface.instrument.slot}'):
            reg_orig_value= pmbus_interface.read(address, read, num_transactions=1)
            for iteration in range(self.STRESS_TEST_ITERATIONS):
                random_data = random.getrandbits(num_bits)
                pmbus_interface.write(address, write, random_data)
                read_data = pmbus_interface.read(address, read, num_transactions=1)
                if random_data != read_data:
                    error_list.append({'Iteration': iteration, 'Expected': random_data, 'Observed': read_data,
                                        'Device': pmbus_interface.interface_name})
                    if len(error_list) > 10:
                        return error_list
            pmbus_interface.write(address, write, reg_orig_value)
        return error_list

    def _check_send_byte(self, pmbus_interface):
        self.Log('info', f'Starting _check_send_byte for {pmbus_interface.interface_name}')
        error_list = []
        write_byte = 1
        write_word = 3
        read_word = 4

        with suppress_sensor(f'FVAL HBIDPS {pmbus_interface.instrument.slot}', f'HBIDPS:{pmbus_interface.instrument.slot}'):
            for iteration in range(self.STRESS_TEST_ITERATIONS):
                random_data = random.getrandbits(16)
                hc_pmbus_num_offset = 4
                if pmbus_interface.device >= hc_pmbus_num_offset:
                    random_page = random.getrandbits(1)
                else:
                    random_page = random.getrandbits(2)
                pmbus_interface.write(0x00, write_byte, random_page)
                pmbus_interface.write(0xb3, write_word, random_data)
                read_data = pmbus_interface.read(0xb3, read_word, num_transactions=1)
                if random_data != read_data:
                    error_list.append({'Iteration': iteration, 'Expected': random_data, 'Observed': read_data,
                                        'Device': pmbus_interface.interface_name})
                    if len(error_list) > 10:
                        return error_list
        return error_list

    def _check_error_busy_retry(self, pmbus_interface):
        self.Log('info', f'Starting _check_error_busy_retry for {pmbus_interface.interface_name}')
        non_existent_reg = 0xff
        error_list = []
        read_word = 4
        with suppress_sensor(f'FVAL HBIDPS {pmbus_interface.instrument.slot}', f'HBIDPS:{pmbus_interface.instrument.slot}'):

            pmbus_interface.reset()
            pmbus_interface.read_no_busy_check(non_existent_reg, read_word)
            if not(pmbus_interface.check_busy()):
                error_list.append({'Iteration': 1, 'Expected': '1', 'Observed': '0',
                               'Device': pmbus_interface.interface_name})
            time.sleep(.1)
            if not(pmbus_interface.check_error_retry_bit()):
                error_list.append({'Iteration': 1, 'Expected': 'Error/Retry Bit Set', 'Observed': 'Not Set',
                               'Device': pmbus_interface.interface_name})
        return error_list

    def _test_fifo_count(self, pmbus_interface):
        read_word = 4
        error_list = []
        test_fifo = []
        self.Log('info', f'Starting _test_fifo_count for {pmbus_interface.interface_name}')
        for exponent in range(10):
            test_fifo.append(2**exponent)
        with suppress_sensor(f'FVAL HBIDPS {pmbus_interface.instrument.slot}', f'HBIDPS:{pmbus_interface.instrument.slot}'):
            for num_transactions_expected in test_fifo:
                pmbus_interface.reset()
                for count in range(num_transactions_expected):
                    pmbus_interface.write(pmbus_interface.comm_stress_reg_word, read_word, data=None, go=False)
                num_transactions = pmbus_interface.get_tx_fifo_count()
                pmbus_interface.set_transmit_bit(go=1)
                pmbus_interface.wait_pmbus_busy()
                num_transactions_read = pmbus_interface.get_rx_fifo_count()
                if num_transactions_expected != num_transactions:
                    error_list.append({'Iteration': 1, 'Expected': num_transactions_expected, 'Observed': num_transactions,
                                   'Device': pmbus_interface.interface_name})
                elif num_transactions_expected != num_transactions_read:
                    error_list.append({'Iteration': 1, 'Expected': num_transactions_expected, 'Observed': num_transactions_read,
                                   'Device': pmbus_interface.interface_name})
        return error_list

    def _check_reset(self, pmbus_interface):
        self.Log('info', f'Starting _check_reset for {pmbus_interface.interface_name}')
        non_existent_reg = 0xff
        error_list = []
        read_word = 4
        write_word = 3
        with suppress_sensor(f'FVAL HBIDPS {pmbus_interface.instrument.slot}', f'HBIDPS:{pmbus_interface.instrument.slot}'):
            pmbus_interface.reset()
            pmbus_interface.read_no_busy_check(non_existent_reg, read_word)
            time.sleep(.1)
            pmbus_interface.reset()
            if pmbus_interface.check_error_retry_bit():
                error_list.append({'Iteration': 1, 'Expected': 'no error/retry', 'Observed': 'error or retry',
                                       'Device': f'{pmbus_interface.interface_name} Error/Retry register'})
            random_word = random.getrandbits(16)
            pmbus_interface.write(pmbus_interface.comm_stress_reg_word, write_word, random_word, go=False)
            if pmbus_interface.get_tx_fifo_count() != 1:
                error_list.append({'Iteration': 1, 'Expected': '1', 'Observed': pmbus_interface.get_tx_fifo_count(),
                                       'Device': f'{pmbus_interface.interface_name} tx_fifo count'})
            pmbus_interface.reset()
            if pmbus_interface.get_tx_fifo_count() != 0:
                error_list.append({'Iteration': 1, 'Expected': '0', 'Observed': pmbus_interface.get_tx_fifo_count(),
                                       'Device': f'{pmbus_interface.interface_name} tx_fifo count'})
            pmbus_interface.reset()
            pmbus_interface.write(pmbus_interface.comm_stress_reg_word, read_word)
            if pmbus_interface.get_rx_fifo_count() != 1:
                error_list.append({'Iteration': 1, 'Expected': '1', 'Observed': pmbus_interface.get_tx_fifo_count(),
                                       'Device': f'{pmbus_interface.interface_name} rx_fifo count'})
            pmbus_interface.reset()
            pmbus_interface.read(pmbus_interface.comm_stress_reg_word, read_word)
            if pmbus_interface.get_rx_fifo_count() != 0:
                error_list.append({'Iteration': 1, 'Expected': '0', 'Observed': pmbus_interface.get_tx_fifo_count(),
                                       'Device': f'{pmbus_interface.interface_name} rx_fifo count'})
        return error_list

    def _create_lc_pmbus_interfaces(self, hbidps):
        pmbus_interfaces = []
        for device_num in range(self.NUM_LC_PMBUS_INTERFACES):
            lc_interface_name = f'LCT2975 PMBUS {device_num}'
            lc_pmbus_address = 0xB8
            pmbus_interfaces.append(PMBUSTestInterface(hbidps, lc_pmbus_address, lc_interface_name, device_num,
                                                self.LC_COMM_STRESS_REGISTER_WORD, self.LC_COMM_STRESS_REGISTER_BYTE ))
        return pmbus_interfaces

    def _create_hc_pmbus_interfaces(self, hbidps):
        pmbus_interfaces = []
        hc_device_offset = 4
        for device_num in range(self.NUM_HC_PMBUS_INTERFACES):
            hc_interface_name = f'LTM4680 PMBUS {device_num}'
            hc_pmbus_address = 0x9e
            pmbus_interfaces.append(PMBUSTestInterface(hbidps, hc_pmbus_address, hc_interface_name,
                        device_num + hc_device_offset, self.LC_COMM_STRESS_REGISTER_WORD, self.HC_COMM_STRESS_REGISTER_BYTE))
        return pmbus_interfaces


class General_I2C_Interface_Tests(BaseTest):
    """Verify communication to the Max14662 on the HBIDPS
            Ganging 14662
            Loopback 14662
            Vsense 14662"""
    STRESS_TEST_ITERATIONS = 10000
    MAX_FAIL_COUNT = 2
    TX_TEST_ITERATIONS = 5

    def _report_errors(self, device, register_errors, error_message_to_print):
        column_headers = ['Interface', 'Iteration', 'Expected', 'Observed', 'Device']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print, table), 'error')

    def _print_test_results(self, hbidps, read_back_errors, test_name):
        if read_back_errors:
            error_message_to_print = f'{test_name} failed '
            self._report_errors(hbidps, read_back_errors, error_message_to_print)
        else:
            hbidps.log_device_message(
                f'{test_name} successful.', 'info')

    @expected_failure('All I2C will fail the _i2c_address_nak_test(TFS110130) test')
    def DirectedI2CTest(self):
        """Test1: Stress the HBIDPS I2C busses by sending thousands of transactions

        Test 2: This controller will assert an error in the status register if user attempts to transmit a message with
        less than two bytes.
        Validate the error bit is set appropriately.

        Test 3: Send wrong address to I2C Transmit FIFO register.
        Write one to I2C Transmit register.
        Make sure Status Register reflects ' I2C Address NAK Error'

        Test 4: Push N bytes of data onto tx fifo register
        Make tx fifo count filed in status register equals N
        """
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            lcrail = LCRailTestbench()
            lcrail.setup_delays(hbidps)
            lcrail.init_lc_for_test(hbidps)
            i2c_interfaces = self._create_gang14662_i2C_interfaces(hbidps)
            i2c_interfaces += self._create_loopback_i2C_interfaces(hbidps)
            i2c_interfaces += self._create_vsense_i2C_interfaces(hbidps)
            for i2c_interface in i2c_interfaces:
                read_back_errors = self._i2c_tx_fifo_count_error_bit_checking_test(i2c_interface)
                self._print_test_results(hbidps, read_back_errors, f'TX FIFo count error I2CTests {i2c_interface.interface_name}')
                read_back_errors = self._i2c_address_nak_test(i2c_interface)
                self._print_test_results(hbidps, read_back_errors, f'TX NAK error I2CTests {i2c_interface.interface_name}')
                read_back_errors = self._tx_fifo_count_test(i2c_interface)
                self._print_test_results(hbidps, read_back_errors, f'TX FIFo count I2CTests {i2c_interface.interface_name}')
                read_back_errors = self._comm_stress_I2C(i2c_interface)
                self._print_test_results(hbidps, read_back_errors, f'Comm Stress I2CTests {i2c_interface.interface_name}')

    def _comm_stress_I2C(self, i2c_interface):
        error_list = []
        original_data = self._read_reg_stress(i2c_interface)
        self.Log('info', f'Running I2C Comm Stress Test on {i2c_interface.interface_name}')
        self._flush_status_register(i2c_interface)
        for iteration in range(self.STRESS_TEST_ITERATIONS):
            random_byte = random.randint(0, 2 ** 8 - 1)
            self._write_reg_stress(i2c_interface, random_byte)
            read_data = self._read_reg_stress(i2c_interface)
            if random_byte != read_data:
                error_list.append(
                    {'Interface': i2c_interface.interface_name, 'Iteration': iteration, 'Expected': random_byte,
                     'Observed': read_data, 'Device': i2c_interface.chip_num})
            if len(error_list) >= self.MAX_FAIL_COUNT or len(error_list) >= self.STRESS_TEST_ITERATIONS:
                break
        self._write_reg_stress(i2c_interface, original_data)
        return error_list

    def _i2c_tx_fifo_count_error_bit_checking_test(self, i2c_interface):
        """Send 0 to 4 bytes
                Look for a transmit fifo count error bit set"""
        error_list = []
        msg_list = [
            [],  # Empty message
            [i2c_interface.UNUSED_I2C_ADDR],  # One byte message
            [i2c_interface.UNUSED_I2C_ADDR, i2c_interface.COMM_STRESS_REGISTER],  # Two byte message
            [i2c_interface.UNUSED_I2C_ADDR, i2c_interface.COMM_STRESS_REGISTER, 0, 0]  # Greater than two byte message
        ]
        self.Log('info', f'Running I2C fifo count error Test on {i2c_interface.interface_name}')
        self._flush_status_register(i2c_interface)
        for iteration in range(self.TX_TEST_ITERATIONS):
            for msg in msg_list:
                self._send_msg(i2c_interface, msg)
                tx_count_error = self._get_transmit_fifo_count_error(i2c_interface)
                if not tx_count_error and (len(msg) < 2):
                    i2c_interface.log_status_register()
                    error_list.append(
                        {'Interface': i2c_interface.interface_name, 'Iteration': iteration, 'Expected': 'transmit FIFO error < 2',
                         'Observed': 'no error', 'Device': i2c_interface.chip_num})
                elif tx_count_error and (len(msg) >= 2):
                    i2c_interface.log_status_register()
                    error_list.append(
                        {'Interface': i2c_interface.interface_name, 'Iteration': iteration, 'Expected': 'transmit FIFO error > 2',
                         'Observed': 'no error', 'Device': i2c_interface.chip_num})
            if len(error_list) >= self.MAX_FAIL_COUNT or len(error_list) >= self.TX_TEST_ITERATIONS:
                break
        return error_list

    def _i2c_address_nak_test(self, i2c_interface):
        """Send a byte to an address that doesn't exist
                Look for a Address NAK error bit set"""
        error_list = []
        #test with real device addr
        msg = [i2c_interface.UNUSED_I2C_ADDR, i2c_interface.COMM_STRESS_REGISTER]

        self.Log('info', f'Running I2C address NAK error Test on {i2c_interface.interface_name}')
        self._flush_status_register(i2c_interface)
        for iteration in range(self.TX_TEST_ITERATIONS):
            self._send_msg(i2c_interface, msg)
            received_addr_nak = self._get_address_nak(i2c_interface)

            if not received_addr_nak:
                i2c_interface.log_status_register()
                error_list.append(
                    {'Interface': i2c_interface.interface_name, 'Iteration': iteration, 'Expected': 'NAK error',
                     'Observed': 'no error', 'Device': i2c_interface.chip_num})
            if len(error_list) >= self.MAX_FAIL_COUNT or len(error_list) >= self.TX_TEST_ITERATIONS:
                break
        return error_list

    def _tx_fifo_count_test(self, i2c_interface):
        """Push N bytes of data onto tx fifo register
        Make tx fifo count filed in status register equals N
        """
        error_list = []
        fifo_count_size = 2 ** 10 - 1
        push_count_list = [0, int(fifo_count_size / 2), fifo_count_size]
        self.Log('info', f'Running I2C fifo count Test on {i2c_interface.interface_name}')
        self._flush_status_register(i2c_interface)
        for iteration in range(self.TX_TEST_ITERATIONS):
            for push_count in push_count_list:
                [self._push_data(i2c_interface, data=0) for i in range(push_count)]
                transmit_fifo_count = self._get_transmit_fifo_count(i2c_interface)
                if transmit_fifo_count != push_count:
                    error_list.append(
                        {'Interface': i2c_interface.interface_name, 'Iteration': iteration, 'Expected': push_count,
                         'Observed': transmit_fifo_count, 'Device': i2c_interface.chip_num})
                self._transmit(i2c_interface, 1)
                self._wait_i2c_busy(i2c_interface)
                if len(error_list) >= self.MAX_FAIL_COUNT or len(error_list) >= self.TX_TEST_ITERATIONS:
                    break
            if len(error_list) >= self.MAX_FAIL_COUNT or len(error_list) >= self.TX_TEST_ITERATIONS:
                break
        return error_list

    def _write_reg_stress(self, i2c_interface, data):
        i2c_interface.write(i2c_interface.COMM_STRESS_REGISTER, data)

    def _read_reg_stress(self, i2c_interface):
        return i2c_interface.read(i2c_interface.COMM_STRESS_REGISTER)

    def _reset(self, i2c_interface):
        i2c_interface.reset()

    def _send_msg(self, i2c_interface, msg):
        for data in msg:
            self._push_data(i2c_interface, data)
        self._transmit(i2c_interface)
        if len(msg) >= 2:
            self._wait_i2c_busy(i2c_interface)

    def _push_data(self, i2c_interface, data):
        i2c_interface.push_data_fifo(data)

    def _transmit(self, i2c_interface, enable_stop_bit=1):
        i2c_interface.set_transmit_bit(enable_stop_bit)

    def _get_transmit_fifo_count(self, i2c_interface):
        return i2c_interface.get_transmit_fifo_count()

    def _flush_status_register(self, i2c_interface):
        if self._get_transmit_fifo_count(i2c_interface) > 0:
            self._transmit(i2c_interface, 1)
            self._wait_i2c_busy(i2c_interface)

    def _wait_i2c_busy(self, i2c_interface):
        i2c_interface.wait_i2c_busy()

    def _get_address_nak(self, i2c_interface):
        return i2c_interface.get_address_nak()

    def _get_transmit_fifo_count_error(self, i2c_interface):
        return i2c_interface.get_transmit_fifo_count_error()

    def _log_status_register(self, i2c_interface):
        i2c_interface.log_status_register()

    def _create_gang14662_i2C_interfaces(self, hbidps):
        i2c_interfaces = []
        for chip_num in range(MAX_GANG_CHIPS):
            i2c_interfaces.append(GangMax14662I2cInterface(hbidps,chip_num))
        return i2c_interfaces

    def _create_loopback_i2C_interfaces(self, hbidps):
        i2c_interfaces = []
        for chip_num in range(MAX_LB_CHIPS):
            i2c_interfaces.append(LoopbackMax14662I2cInterface(hbidps, chip_num))
        return i2c_interfaces

    def _create_vsense_i2C_interfaces(self, hbidps):
        i2c_interfaces = []
        for chip_num in range(MAX_VS_CHIPS):
            i2c_interfaces.append(VSenseMax14662I2cInterface(hbidps, chip_num))
        return i2c_interfaces

    def _get_valid_addresses(self, i2c_interface):
        valid_addresses = []
        for addr in range(0, 0x100, 2):
            self._send_msg(i2c_interface, [addr, 0])
            if not self._get_address_nak(i2c_interface):
                valid_addresses.append(addr)
        str_list = [hex(x) for x in valid_addresses]
        print(str_list)
        return valid_addresses

class GeneralSpiInterfaceTests(AlarmHelper):

    max_temp = 42
    min_temp = 15

    def __init__(self):
        pass

    def DirectedStressMax6628SpiCommunication(self, TEST_ITERATIONS=100):
        """Read the MAX6628 and die temperature registers and check alarm

                1. Read the 4 MAX6628 registers and die temp register.
                2. Convert the read value in decimal.
                3. Check if the value is within the limits or not.
                4. Check for the corresponding alarm bit.
        """
        for self.hbidps in self.env.get_fpgas():
            self.hbidps.print_slot_under_test()
            self.previous_max6628_temp = self.convert_max6628_temp_to_degrees(self.read_max6628_temp(0))
            self.previous_die_temp = self.convert_die_temp_to_degrees(self.read_die_temp())
            for self.iteration in range(TEST_ITERATIONS):
                for max6628_device in range(4):
                    self.check_max6628_devices(max6628_device)
                self.check_die_temp_board()

    def check_die_temp_board(self):
        fpga_die_temp = self.read_die_temp()
        fpga_die_temp = self.convert_die_temp_to_degrees(fpga_die_temp)
        self.check_fpga_die_temp_limits(fpga_die_temp)
        self.check_temp_difference(fpga_die_temp, self.previous_die_temp)
        self.prev_die_temp = fpga_die_temp

    def check_max6628_devices(self, max6628_device):
        raw_temp = self.read_max6628_temp(max6628_device)
        max6628_temp = self.convert_max6628_temp_to_degrees(raw_temp)
        self.check_max6628_temp_limits(max6628_device, max6628_temp)
        self.check_temp_difference(max6628_temp, self.previous_max6628_temp)
        self.prev_max6628_temp = max6628_temp

    def convert_die_temp_to_degrees(self, fpga_die_temp):
        if self.is_capabilities_bit_high():
            fpga_die_temp = self.decode_32_bit_floating_point_temp(fpga_die_temp)
        else:
            fpga_die_temp = (fpga_die_temp & 0x3FF) * 693 / 1024 - 265
        return fpga_die_temp

    def check_max6628_temp_limits(self, max6628_device, max6628_temp):
        if max6628_temp > self.min_temp and max6628_temp <= self.max_temp:
            self.Log('debug', f'MAX6628 Board {max6628_device} temp : {max6628_temp}')
            self.check_max6628_alarm_bits('no_alarm', max6628_device, 0)
        else:
            self.Log('error', f'MAX6628 Board {max6628_device} temp Exceeded : {max6628_temp}. Min Temp : {self.min_temp} Max Temp {self.max_temp}.')
            if max6628_temp <= self.min_temp:
                self.check_max6628_alarm_bits('low', max6628_device, 1)
            elif max6628_temp > self.max_temp:
                self.check_max6628_alarm_bits('high', max6628_device, 1)

    def check_fpga_die_temp_limits(self, fpga_die_temp):
        if fpga_die_temp > self.min_temp and fpga_die_temp <= self.max_temp:
            self.Log('debug', f'Fpga Die temp: {fpga_die_temp}')
        else:
            self.Log('error', f'Fpga Die temp Exceeded : {fpga_die_temp}. Min Temp : {self.min_temp} Max Temp {self.max_temp}.')

    def check_max6628_alarm_bits(self, threshold, max6628_device, expected_bit_value):
        temp_alarm = self.hbidps.read_bar_register(self.hbidps.registers.TEMP_ALARMS)
        if threshold == 'low' and (temp_alarm.low_temp >> max6628_device & 1) != expected_bit_value:
            self.Log('error', f'Lower Alarm bits not set correctly. Expected Value : {expected_bit_value}.')
        elif threshold == 'high' and (temp_alarm.high_temp >> max6628_device & 1) != expected_bit_value:
            self.Log('error', f'Higher Alarm bits not set correctly. Expected Value : {expected_bit_value}.')
        elif threshold == 'no_alarm' and (temp_alarm.low_temp >> max6628_device & 1) & (temp_alarm.high_temp >> max6628_device & 1) != expected_bit_value:
            self.Log('error', f'Alarm bits not set correctly. Expected Value : {expected_bit_value}.')

    def is_capabilities_bit_high(self):
        return self.hbidps.read_bar_register(self.hbidps.registers.CAPABILITIES).value & 1

    def decode_32_bit_floating_point_temp(self, fpga_die_temp):
        return struct.unpack('!f', struct.pack('!I', int(bin(fpga_die_temp), 2)))[0]

    def get_twos_compliment(self, decimal_value, bits):
        decimal_value = decimal_value - (1 << bits)
        return decimal_value

    def is_signed(self, raw_temp):
        return raw_temp >> 12 & 1

    def check_temp_difference(self, current_temp, previous_temp):
        if abs(current_temp - previous_temp) > 10:
            self.Log('error', f'The temp difference is too high.')


    @skip('Needs Development')
    def Stress_VM_AD7616_SPI_full_Testing_Test(self):
        """Stress the SPI to the Voltage Measure need to develop SPI testing methodology"""

    def DirectedTli4970BulkCurrentReadAndAlarmCheck(self, TEST_ITERATIONS=30):
        """Read the TLI4970 current register and check the alarm.
           Then change the upper and the lower threshold limit of the current and check for the alarm generated.

           1. Read TLI4970 Current. Convert the current to degrees.
           2. Set some random filter time.
           3. Check if the current is within the limits.
           4. If the current is not within the limits, wait for the filter_time amount of time.
           5. Read the current again after filter_time amount of time.
           6. If the current is still exceeding limits after the filter_time amount of time, check for the alarm bits.
           7. Repeat the same by changing the lower and upper threshold current limits.
        """

        self.filter_time = 10
        self.sleep_time_more_than_filter_time = self.filter_time + 2
        self.sleep_time_less_than_filter_time = self.filter_time - 2

        for self.hbidps in self.env.get_fpgas():
            self.hbidps.print_slot_under_test()
            for self.iteration in range(TEST_ITERATIONS):
                self.update_min_max_current()
                self.check_current_within_limits()
                self.check_current_exceeding_upper_threshold()
                self.check_current_exceeding_lower_threshold()

    def check_current_exceeding_lower_threshold(self):
        self.set_lower_current_threshold()
        self.read_and_convert_lti4970_current()
        self.reset_tli4970_reg_default_values()

    def check_current_exceeding_upper_threshold(self):
        self.set_upper_current_threshold()
        self.read_and_convert_lti4970_current()
        self.reset_tli4970_reg_default_values()

    def check_current_within_limits(self):
        self.read_and_convert_lti4970_current()

    def read_and_convert_lti4970_current(self):
        raw_current = self.read_TLI4970_current()
        tli4970_current = self.convert_raw_current_to_decimal(raw_current)
        self.set_sample_filter_time(self.filter_time)
        self.check_current_limit(tli4970_current, self.sleep_time_more_than_filter_time)
        self.check_current_limit(tli4970_current, self.sleep_time_less_than_filter_time)

    def convert_raw_current_to_decimal(self, raw_current):
        return self.decode_single_precision_floating_point_current(raw_current)

    def check_current_limit(self, current, sleep_time):
        if current > self.min_current and current <= self.max_current:
            self.Log('debug', f'TLI4970 current is within limits. Value : {current}. Max : {self.max_current} Min : {self.min_current}.')
            self.check_tli4970_alarm_bits('no_alarm', 0)
        else:
            time.sleep(sleep_time * 0.000001)
            current = self.convert_raw_current_to_decimal(self.read_TLI4970_current())
            if current <= self.min_current:
                self.Log('debug', f'TLI4970 current exceeded lower limits. Value : {current}. Max : {self.max_current} Min : {self.min_current}.')
                self.check_tli4970_alarm_bits('low', 1)
            elif current > self.max_current:
                self.Log('debug', f'TLI4970 current exceeded upper limits. Value : {current}. Max : {self.max_current} Min : {self.min_current}.')
                self.check_tli4970_alarm_bits('high', 1)

    def check_tli4970_alarm_bits(self, threshold, expected_bit_value):
        alarm_reg = self.hbidps.read_bar_register(self.hbidps.registers.SYS_CURRENT_ALARMS)
        if threshold == 'low' and alarm_reg.neg_limit != expected_bit_value:
                self.Log('error', f'Lower Alarm bits not set correctly. Expected Value : {expected_bit_value}.')
        elif threshold == 'high' and alarm_reg.pos_limit != expected_bit_value:
                self.Log('error', f'Higher Alarm bits not set correctly. Expected Value : {expected_bit_value}.')
        elif threshold == 'no_alarm' and (alarm_reg.pos_limit | alarm_reg.neg_limit):
                self.Log('error', f'Alarm bits not set correctly. Expected Value : {expected_bit_value}.')

    def set_upper_current_threshold(self):
        raw_current = self.read_TLI4970_current()
        current = self.decode_single_precision_floating_point_current(raw_current)
        max = self.encode_float_to_single_precision_floating_format(current - 2)
        self.hbidps.write_bar_register(self.hbidps.registers.TLI4970_OVERCURRENT_LIMIT(value=max))
        self.update_min_max_current()

    def set_lower_current_threshold(self):
        raw_current = self.read_TLI4970_current()
        current = self.decode_single_precision_floating_point_current(raw_current)
        min = self.encode_float_to_single_precision_floating_format(current + 2)
        self.hbidps.write_bar_register(self.hbidps.registers.TLI4970_UNDERCURRENT_LIMIT(value=min))
        self.update_min_max_current()

    def update_min_max_current(self):
        over_current_limit = self.hbidps.read_bar_register(self.hbidps.registers.TLI4970_OVERCURRENT_LIMIT).value
        self.max_current = self.decode_single_precision_floating_point_current(over_current_limit)
        under_current_limit = self.hbidps.read_bar_register(self.hbidps.registers.TLI4970_UNDERCURRENT_LIMIT).value
        self.min_current = self.decode_single_precision_floating_point_current(under_current_limit)

    def reset_tli4970_reg_default_values(self):
        under_current_default = self.encode_float_to_single_precision_floating_format(-10)
        self.hbidps.write_bar_register(self.hbidps.registers.TLI4970_UNDERCURRENT_LIMIT(value=under_current_default))
        over_current_default = self.encode_float_to_single_precision_floating_format(40)
        self.hbidps.write_bar_register(self.hbidps.registers.TLI4970_OVERCURRENT_LIMIT(value=over_current_default))
        self.set_sample_filter_time(0)
        sys_current_alarm = self.hbidps.read_bar_register(self.hbidps.registers.SYS_CURRENT_ALARMS)
        sys_current_alarm.neg_limit = 1
        sys_current_alarm.pos_limit = 1
        self.hbidps.write_bar_register(sys_current_alarm)

    @skip('Needs Development')
    def Stress_VTarget_SPI_full_Testing_Test(self):
        """Stress the SPI to the Voltage Measure need to develop SPI testing methodology"""
