################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing of DDR Memory of HBIDPS"""
import os
import random
from Common.fval import skip

from Hbidps.Tests.HbidpsTest import HbidpsTest
MAX_FAIL_COUNT = 2


class Diagnostics(HbidpsTest):
    """Access complete ddr memory and confirm data integrity"""
    
    def DirectedDMAWriteReadExhaustiveTest(self):
        """Access 1 GB of DDR memory five times and confirm no corruption."""
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            memory_size = 1 << 30
            memory_start_address = 0
            test_iteration_count = 5
            for iteration in range(test_iteration_count):
                random_test_data = os.urandom(memory_size)
                hbidps.dma_write(memory_start_address, random_test_data)
                read_back_data = hbidps.dma_read(memory_start_address, memory_size)
                if (random_test_data != read_back_data):
                    self.Log('error', 'Data compare mismatch for run:{}'.format(iteration))
                    fail_count += 1
                else:
                    pass_count += 1
                if fail_count == MAX_FAIL_COUNT:
                    break
            if pass_count == test_iteration_count:
                self.Log('info', 'DMA read/Write was successful for {} iterations'.format(test_iteration_count))
            else:
                self.Log('error', 'DMA read/Write was unsuccessful for {} iterations'.format(fail_count))

    def DirectedDMAWriteReadExhaustive2GBTest(self):
        """Access 2 GB of DDR memory five times and confirm no corruption."""
        for hbidps in self.hbidps_list:
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            memory_size = 1 << 31
            memory_start_address = 0
            test_iteration_count = 2
            for iteration in range(test_iteration_count):
                random_test_data = os.urandom(memory_size)
                hbidps.dma_write(memory_start_address, random_test_data)
                read_back_data = hbidps.dma_read(memory_start_address, memory_size)
                if (random_test_data != read_back_data):
                    self.Log('error', 'Data compare mismatch for run:{}'.format(iteration))
                    fail_count += 1
                else:
                    pass_count += 1
                if fail_count == MAX_FAIL_COUNT:
                    break
            if pass_count == test_iteration_count:
                self.Log('info', 'DMA read/Write was successful for {} iterations'.format(test_iteration_count))
            else:
                self.Log('error', 'DMA read/Write was unsuccessful for {} iterations'.format(fail_count))

    def DirectedDMAWriteReadRandomAccessSizeTest(self):
        """Access RANDOME size (<=2GB and multiple of 4 Bytes) of DDR memory and confirm no corruption."""
        for hbidps in self.hbidps_list:
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            memory_size = 1 << 31
            test_iteration_count = 2
            for iteration in range(test_iteration_count):
                access_address = 4 * random.randrange(0, memory_size//4)
                remain_bytes = memory_size - access_address
                access_size = 4 * random.randint(1, remain_bytes//4)
                random_test_data = os.urandom(access_size)
                hbidps.dma_write(access_address, random_test_data)
                read_back_data = hbidps.dma_read(access_address, access_size)
                if (random_test_data != read_back_data):
                    self.Log('error', 'Data compare mismatch for run:{}'.format(iteration))
                    self.Log('error', f'address {hex(access_address)}')
                    self.Log('error', f'access_size {hex(access_size)}')
                    fail_count += 1
                else:
                    pass_count += 1
                if fail_count == MAX_FAIL_COUNT:
                    break
            if pass_count == test_iteration_count:
                self.Log('info', 'DMA read/Write was successful for {} iterations'.format(test_iteration_count))
            else:
                self.Log('error', 'DMA read/Write was unsuccessful for {} iterations'.format(fail_count))

    @skip('Not implemented')
    def DirectedDDR4PeekPokeTest(self):
        """ Side interface can be used for debug.

         FPGA Review Note:
         Set address to read and write.
         Pick DMA read DMA write and Pock
         """
        pass
