################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing sample engine scopeshot function"""

from Hbidps.Tests.HbidpsTest import HbidpsTest
from Common.fval import skip


class Functional(HbidpsTest):
    """Check sample engine scopeshot function. Need more info to create tests.

        FPGA Review Note:
        Software side has the document (Matt)
        Extension to sample engine
        Pj is working on the port of the Scopeshot
    """

    @skip('not implemented')
    def DirectedHCScopeShotTest(self):
        """ Use HC ScopeShot function of HC sample Engine
        """
        pass

    @skip('not implemented')
    def DirectedLCScopeShotTest(self):
        """ Use LC ScopeShot function of LC sample Engine
        """
        pass

    @skip('not implemented')
    def DirectedVMScopeShotTest(self):
        """ Use VM ScopeShot function of VM sample Engine
        """
        pass

    @skip('not implemented')
    def DirectedScopeShotBIRMTest(self):
        """ Use ScopeShot function with BIRM trigger
        """
        pass