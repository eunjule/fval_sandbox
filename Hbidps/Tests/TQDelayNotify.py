################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing TQ delay and tq_notify command function"""
import time

from Common.fval import skip
from Hbidps.instrument import hbidps_register
from Hbidps.instrument.AlarmSystem import AlarmHelper
from Hbidps.instrument.HCRailPMBUS import HCRailPMBUS
from Hbidps.instrument.RailTransitionControl import RailTransitionControl
from Hbidps.instrument.MdutConfiguration import MdutConfiguration
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.instrument.TQMachine import TriggerQueueDDR
from Hbidps.Tests.HbidpsTest import HbidpsTest
from Hbidps.Tests.HC_ForceVoltage import HCRailsEnv
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands


class Functional(HbidpsTest):
    """Check Delay Timing and TQ Notify Check"""

    def DirectedTqDelayAllDpsTest(self, hbidps=None):
        """Check for the delay provided by the Trigger Queue
           command for all DPS.

        Steps for following tests:
        1.Check global alarm and reset if needed
        2.Configure DUT_DOMAIN_ID_NN with dut_id
        3.Creat TQ content with time delay command
        4.Store TQ content and lut content
        5.Clear tq_notify flag in global alarm register
        6.Send trigger with configured dut_id
        7.Check tq_notify flag in global alarm register
        8.Check the delay time
        9.Check global alarm (should be clean)
        """
        for self.hbidps in self.hbidps_list:
            self.DirectedTqDelayTest(self.hbidps)

    def DirectedTqDelayTest(self, hbidps=None):
        """Check for the delay provided by the Trigger Queue
           command.

        Steps for following tests:
        1.Check global alarm and reset if needed
        2.Configure DUT_DOMAIN_ID_NN with dut_id
        3.Creat TQ content with time delay command
        4.Store TQ content and lut content
        5.Clear tq_notify flag in global alarm register
        6.Send trigger with configured dut_id
        7.Check tq_notify flag in global alarm register
        8.Check the delay time
        9.Check global alarm (should be clean)
        """
        if hbidps == None:
            self.hbidps = self.hbidps_list[0]
        else:
            self.hbidps = hbidps
        self.hbidps.print_slot_under_test()

        with HCRailsEnv(self.hbidps):
            self.reset_sw_trigger()
            self.create_object_interface()
            self.setup_hc_rail_config()

            active_rails = 0b11
            delay = 20000
            self.check_tq_delay_hc_rails(active_rails, delay)
            self.reset_uhc_dut_config()
            self.hbidps.check_rc_sw_trigger_source()
            self.reset_sw_trigger()

    def DirectedTqNotifyAllDpsTest(self, hbidps=None):
        """Check the TqNotify bit in the Global Alarm and RTOS trigger
           received in the RCTC for all DPS.

        Steps for following tests:
        1.Check global alarm and reset if needed
        2.Configure DUT_DOMAIN_ID_NN with dut_id
        3.Creat TQ content with tq_notify command
        4.Store TQ content and lut content
        5.Clear tq_notify flag in global alarm register
        6.Send trigger with configured dut_id
        7.Check tq_notify flag in global alarm register
        8.Check RTOS trigger received in RCTC
        9.Check global alarm (should be clean)
        """
        for self.hbidps in self.hbidps_list:
            self.DirectedTqNotifyTest(self.hbidps)

    def DirectedTqNotifyTest(self, hbidps=None):
        """Check the TqNotify bit in the Global Alarm and RTOS trigger
           received in the RCTC.

        Steps for following tests:
        1.Check global alarm and reset if needed
        2.Configure DUT_DOMAIN_ID_NN with dut_id
        3.Creat TQ content with tq_notify command
        4.Store TQ content and lut content
        5.Clear tq_notify flag in global alarm register
        6.Send trigger with configured dut_id
        7.Check tq_notify flag in global alarm register
        8.Check RTOS trigger received in RCTC
        9.Check global alarm (should be clean)
        """
        if hbidps == None:
            self.hbidps = self.hbidps_list[0]
        else:
            self.hbidps = hbidps
        self.hbidps.print_slot_under_test()

        with HCRailsEnv(self.hbidps):
            self.reset_sw_trigger()
            self.create_object_interface()
            self.setup_hc_rail_config()

            active_rails = 0b11
            self.check_tq_notify_hc_rails(active_rails)
            self.check_tq_notify_lc_rails(active_rails)
            self.check_tq_notify_vtarget(active_rails)
            self.check_tq_notify_all_rails(active_rails)
            self.reset_uhc_dut_config()
            self.hbidps.check_rc_sw_trigger_source()
            self.reset_sw_trigger()

    def DirectedTqNotifyNegativeAllDpsTest(self, hbidps=None):
        """Check the TqNotify bit in the Global ALarm and RTOS trigger
           (should be zero) for all DPS.

        Steps for following tests:
        1.Check global alarm and reset if needed
        2.Configure DUT_DOMAIN_ID_NN with dut_id
        3.Creat TQ content without tq_notify command
        4.Store TQ content and lut content
        5.Clear tq_notify flag in global alarm register
        6.Send trigger with configured dut_id
        7.Check no tq_notify flag in global alarm register
        8.Check no RTOS trigger received in RCTC(Check SW Trigger Count)
        9.Check global alarm (should be clean)
        """
        for self.hbidps in self.hbidps_list:
            self.DirectedTqNotifyNegativeTest(self.hbidps)

    def DirectedTqNotifyNegativeTest(self, hbidps=None):
        """Check the TqNotify bit in the Global ALarm and RTOS trigger
           (should be zero)

        Steps for following tests:
        1.Check global alarm and reset if needed
        2.Configure DUT_DOMAIN_ID_NN with dut_id
        3.Creat TQ content without tq_notify command
        4.Store TQ content and lut content
        5.Clear tq_notify flag in global alarm register
        6.Send trigger with configured dut_id
        7.Check no tq_notify flag in global alarm register
        8.Check no RTOS trigger received in RCTC(Check SW Trigger Count)
        9.Check global alarm (should be clean)
        """
        if hbidps == None:
            self.hbidps = self.hbidps_list[0]
        else:
            self.hbidps = hbidps
        self.hbidps.print_slot_under_test()

        with HCRailsEnv(self.hbidps):
            self.reset_sw_trigger()
            self.create_object_interface()
            self.setup_hc_rail_config()

            active_rails = 0b11
            self.check_negative_tq_notify_hc_rails(active_rails)
            self.check_negative_tq_notify_lc_rails(active_rails)
            self.check_negative_tq_notify_vtarget(active_rails)
            self.check_negative_tq_notify_all_rails(active_rails)
            self.reset_uhc_dut_config()

    @skip('New Feature HDMT Ticket 89605')
    def DirectedBroadCastTQNotifyTest(self):
        """ Set TQ Notify Bits through BRIM Trigger
        """
        pass

    def check_tq_delay_hc_rails(self, active_rails, delay):
        self.mdut_config.set_uhc_hc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_tq_delay_commands_hc_rails(active_rails, delay)
        time_delta = self.send_tq_delay_hc_rails(tq_list)
        self.check_time_delay(time_delta, delay)
        self.tq_ddr.check_tq_notify(0, 1, 0, False)
        trigger_at_rc_expected = self.create_rtos_trigger(dut_id=3, resource_type='hc_rails')
        self.check_rtos_trigger(trigger_at_rc_expected, False)
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_time_delay(self, time_delta, delay):
        delay = delay * 0.000001
        self.Log('info', f'Expected Delay : {delay}s. Actual Delay : {time_delta}s.')
        if time_delta < (0.8 * delay) or time_delta > (1.2 * delay):
            self.Log('error', f'Error in the Delay. Expected : {delay}s. Actual : {time_delta}s.')

    def send_tq_delay_hc_rails(self, tq_list):
        tq_data = self.tq_ddr.get_tq_data(tq_list)
        lut_address = self.tq_ddr.get_dps_lut_address(0, 0)
        tq_content_address = self.tq_ddr.get_tq_content_address()
        self.tq_ddr.store_tq_address_to_lut(lut_address, tq_content_address)
        self.tq_ddr.store_tq_content_to_mem(tq_content_address, tq_data)
        self.tq_ddr.clear_tq_notify()
        time_start = self.hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=3, index=0)
        time_delta = self.hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
        self.check_hc_fold_status(self.hbidps)
        return time_delta

    def create_tq_delay_commands_hc_rails(self, active_rails, delay):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.delay, tq_dev.HCrail0, delay)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def reset_sw_trigger(self):
        self.hbidps.reset_rc_trigger()
        sw_trigger_fifo_count = self.hbidps.get_rc_sw_trigger_count()
        if sw_trigger_fifo_count != 0:
            self.Log('error', f'Software trigger FIFO count reset failed.')

    def reset_uhc_dut_config(self):
        self.mdut_config.set_uhc_dut_status(uhc_id=0, dut_id=0, valid=1, log=False)

    def check_tq_notify_all_rails(self, active_rails):
        self.mdut_config.set_uhc_hc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.mdut_config.set_uhc_lc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.mdut_config.set_uhc_vtarget_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_tq_commands_all_rails(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(1, 1, 1, False)
        trigger_at_rc_expected = self.create_rtos_trigger(dut_id=3, resource_type='hc_rails')
        self.check_rtos_trigger(trigger_at_rc_expected, False)
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_tq_notify_vtarget(self, active_rails):
        self.mdut_config.set_uhc_vtarget_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_tq_commands_vtarget(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(0, 0, 1, False)
        trigger_at_rc_expected = self.create_rtos_trigger(dut_id=3, resource_type='vtarget')
        self.check_rtos_trigger(trigger_at_rc_expected, False)
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_tq_notify_lc_rails(self, active_rails):
        self.mdut_config.set_uhc_lc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_tq_commands_lc_rails(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(1, 0, 0, False)
        trigger_at_rc_expected = self.create_rtos_trigger(dut_id=3, resource_type='lc_rails')
        self.check_rtos_trigger(trigger_at_rc_expected, False)
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_tq_notify_hc_rails(self, active_rails):
        self.mdut_config.set_uhc_hc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_tq_commands_hc_rails(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(0, 1, 0, False)
        trigger_at_rc_expected = self.create_rtos_trigger(dut_id=3, resource_type='hc_rails')
        self.check_rtos_trigger(trigger_at_rc_expected, False)
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_rtos_trigger(self, trigger_at_rc_expected, log=False):
        trigger_at_rc = self.hbidps.get_rc_trigger(0)
        if trigger_at_rc_expected != trigger_at_rc:
            self.Log('error',
                     f'RC didn\'t received correct trigger. Expected {trigger_at_rc_expected} received {trigger_at_rc} ')
        if log:
            self.Log('info', f'Expected Trigger : {bin(trigger_at_rc_expected)}')
            self.Log('info', f'Trigger at RCTC  : {bin(trigger_at_rc)}')

    def create_rtos_trigger(self, dut_id, resource_type):
        for_sw = 1
        card_type = 1
        if resource_type == 'hc_rails':
            r_type = 1
        if resource_type == 'lc_rails':
            r_type = 0
        if resource_type == 'vtarget':
            r_type = 2
        trigger_at_rc = (card_type << 26) + (dut_id << 20) + (for_sw << 19) + r_type
        return trigger_at_rc

    def create_object_interface(self):
        self.alarm_helper = AlarmHelper(self.hbidps)
        self.tq_generator = TriggerQueueCommands()
        self.tq_ddr = TriggerQueueDDR(self.hbidps)
        self.hc_rail = HCRailPMBUS(self.hbidps)
        self.rail_transition = RailTransitionControl(self.hbidps)
        self.mdut_config = MdutConfiguration(self.hbidps)

    def setup_hc_rail_config(self):
        self.rail_transition.init_rail_transition_time()
        self.rail_transition.log_rail_transition_time()
        self.hc_rail.log_hc_run_reg()
        self.hc_rail.enable_hc_run_reg()
        self.mdut_config.set_uhc_dut_status(uhc_id=0, dut_id=3, valid=1, log=False)

    def create_tq_commands_all_rails(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def create_tq_commands_hc_rails(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def create_tq_commands_lc_rails(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def create_tq_commands_vtarget(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def send_trigger_queue(self, tq_list):
        tq_data = self.tq_ddr.get_tq_data(tq_list)
        lut_address = self.tq_ddr.get_dps_lut_address(0, 0)
        tq_content_address = self.tq_ddr.get_tq_content_address()
        self.tq_ddr.store_tq_address_to_lut(lut_address, tq_content_address)
        self.tq_ddr.store_tq_content_to_mem(tq_content_address, tq_data)
        self.tq_ddr.clear_tq_notify()
        self.tq_ddr.send_dps_trigger_through_rctc(0x01, 3, 0)
        self.check_hc_fold_status(self.hbidps)

    def check_hc_fold_status(self, hbidps):
        hc_fold_status = hbidps.read_bar_register(hbidps_register.HC_FOLDED)
        hc_fold_status.value = 0x3FF
        hbidps.write_bar_register(hc_fold_status)
        time.sleep(0.1)
        hc_fold_status = hbidps.read_bar_register(hbidps_register.HC_FOLDED)
        self.Log('info', f'hc_fold_status {hex(hc_fold_status.value)}')

    def check_negative_tq_notify_all_rails(self, active_rails):
        self.mdut_config.set_uhc_hc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.mdut_config.set_uhc_lc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.mdut_config.set_uhc_vtarget_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_negative_tq_commands_all_rails(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(0, 0, 0, False)
        self.check_sw_trigger_count()
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_negative_tq_notify_vtarget(self, active_rails):
        self.mdut_config.set_uhc_vtarget_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_negative_tq_commands_vtarget(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(0, 0, 0, False)
        self.check_sw_trigger_count()
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_negative_tq_notify_lc_rails(self, active_rails):
        self.mdut_config.set_uhc_lc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_negative_tq_commands_lc_rails(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(0, 0, 0, False)
        self.check_sw_trigger_count()
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_negative_tq_notify_hc_rails(self, active_rails):
        self.mdut_config.set_uhc_hc_rail_status(uhc_id=0, rails=active_rails, log=False)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        tq_list = self.create_negative_tq_commands_hc_rails(active_rails)
        self.send_trigger_queue(tq_list)
        self.tq_ddr.check_tq_notify(0, 0, 0, False)
        self.check_sw_trigger_count()
        self.mdut_config.reset_uhc_dut_rails(uhc_id=0)

    def check_sw_trigger_count(self):
        sw_trigger_fifo_count = self.hbidps.get_rc_sw_trigger_count()
        if sw_trigger_fifo_count != 0:
            self.Log('error', f'Software trigger count incorrect. Expected : 0.')

    def create_negative_tq_commands_all_rails(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def create_negative_tq_commands_hc_rails(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def create_negative_tq_commands_lc_rails(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list

    def create_negative_tq_commands_vtarget(self, active_rails):
        self.check_hc_fold_status(self.hbidps)
        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.VtargetBroadcast, active_rails)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()
        return tq_list