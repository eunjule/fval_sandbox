################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing of PCIe access with scratch pad register"""
import collections
import math
import os
import random
import time


from Hbidps.Tests.HbidpsTest import HbidpsTest
MAX_FAIL_COUNT = 10


class Diagnostics(HbidpsTest):
    """Access scratch pad register multiple times"""
    test_iteration_count = 10000

    def RandomFpgaScratchRegisterReadWriteTest(self):
        """Write random data to the FPGA scratch register with BAR write command. Read back the data with BAR read command.

        Compare the sent and received content.
        """
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            for iteration in range(self.test_iteration_count):
                expected = random.randrange(0, 0xFFFFFFFF)
                hbidps.write_bar_register(hbidps.registers.BAR1_SCRATCH(value=expected))
                observed = hbidps.read_bar_register(hbidps.registers.BAR1_SCRATCH).value
                if observed != expected:
                    self.Log('error', 'Received 0x{:x}, expected=0x{:x}'.format(observed, expected))
                    fail_count += 1
                else:
                    pass_count += 1
                if fail_count == MAX_FAIL_COUNT:
                    break
            if pass_count == self.test_iteration_count:
                self.Log('info', 'Scratch Register read/Write was successful for iterations: {}'.format(iteration))
            else:
                self.Log('error', 'Scratch Register read/Write was unsuccessful for iterations {}'.format(fail_count))