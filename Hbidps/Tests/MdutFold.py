################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing MDUT and fold trigger function"""

from Hbidps.Tests.HbidpsTest import HbidpsTest
from Common.fval import skip


class Functional(HbidpsTest):
    """Check MDUT configure and folding event

        Some MDUT feaure are not ready. More Tests Should be expected.

    """

    @skip('New Feature HDMT Ticket 126848')
    def DirectedFoldingEventTest(self):
        """Temp and sys_current global alarm will trigger the folding event
        and send out fold trigger with the dut_id configured in MDUT register

        Steps:
        1.Follow step in HC/LC/Vtarget rail setting to set one rail voltage
        2.Check alarms and reset if needed
        3.Set temp/current limit to cause the alarm
        4.Check the alarm
        5.Check fold event (rails go off)
          (1) hc/lc/varget rail turn off run/control pin
          (2) sample engine clear content
        6.Check RCTC for fold trigger from DPS
        7.Set temp/current back to normal value
        8.Check global alarm and reset if needed

        FPGA Review Note:
        Sample engine alarm not trigger fold
        Same DPS control 4 rails. Only fold the UHC and not the others
        For now, all rails will fold
        All member of one UHC will fold together
        """
        pass
