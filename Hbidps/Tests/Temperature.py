################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing of ADT7411, LTM4644 ,Max6627 and FPGA internal temperature"""
import collections
import math
import os
import random
import time


from Hbidps.Tests.HbidpsTest import HbidpsTest
MAX_FAIL_COUNT = 10

channel_number = collections.namedtuple('channel_number', ['channel'])
channel_name_value_combination = collections.namedtuple('channel_name_value_combination', ['channel_name', 'default_value'])
default_values_collection = {channel_number(channel=0, ): channel_name_value_combination(channel_name='U15_ADT7411_Temperature', default_value=35),
                                    channel_number(channel=1, ): channel_name_value_combination(channel_name='U12_LTM4644_Temperature', default_value=35),
                                    channel_number(channel=2, ): channel_name_value_combination(channel_name='U32_Max6627_Temperature', default_value=27.6875),
                                    channel_number(channel=3, ): channel_name_value_combination(channel_name='U33_Max6627_Temperature', default_value=33.1875),
                                    channel_number(channel=4, ): channel_name_value_combination(channel_name='U34_Max6627_Temperature', default_value=26.9375),
                                    channel_number(channel=5, ): channel_name_value_combination(channel_name='U35_Max6627_Temperature', default_value=26.5),
                                    channel_number(channel=6, ): channel_name_value_combination(channel_name='U63_FPGA_Temperature ', default_value=21)}


class Diagnostics(HbidpsTest):
    """Read ADT7411, LTM4644 ,Max6627 and FPGA internal temperature repeatedly and confirm stability of results."""
    vmon_test_iteration_count = 50
    test_iteration_count = 1000
    temperature_expected_pass_count = 350

    def DirectedTemperatureReadTest(self):
        """Read temperature from ADT7411, Max6627 and LTM4644. Make sure they don't deviate more than 5%"""
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            hbidps.initialize_hbi_dps()
            for iteration in range(self.vmon_test_iteration_count):
                for channel_number in range(7):
                    tmon_read = hbidps.read_t_mon(channel_number)
                    channel_name,default_value = default_values_collection[(channel_number,)]
                    if tmon_read > 15 and tmon_read <= 42:
                        pass_count += 1
                    else:
                        fail_count += 1
                        self.Log('error', 'At iteration {} , reading for channel {} deviated from dafault value:{} actual value:{}.'.format(
                            iteration, channel_name ,default_value, tmon_read))
                    if fail_count == MAX_FAIL_COUNT:
                        break
            if pass_count == self.temperature_expected_pass_count :
                self.Log('info', 'Temperature read was successful for iterations: {}'.format(pass_count))
            else:
                self.Log('error', 'Temperature read was successful for only iterations {} out of {}'.format(pass_count, self.temperature_expected_pass_count))