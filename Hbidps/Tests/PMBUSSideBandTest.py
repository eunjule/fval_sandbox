################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing PMBUS Busy Issue"""

import time
import random

from Hbidps.Tests.HbidpsTest import HbidpsTest


class Functional(HbidpsTest):
    """Check MDUT configure and folding event

        Some MDUT feaure are not ready. More Tests Should be expected.

    """

    def DirectedLTC2975UserDataWriteReadTest(self):
        """Use PMBUS Sideband to Perform LTC2975 USER_DATA_03 and USER_DATA_04
        write and read test with random word

        Steps:
        """
        hbidps = self.hbidps_list[0]
        write_value = random.randrange(2**16)
        fifo_list = self.create_user_data_write_read_tx_fifo_list(write_value)
        hbidps.pmbus_send_tx_commands(fifo_list)
        rx_data_list = hbidps.read_pmbus_rx_data_list()
        for data in rx_data_list:
            if data != write_value:
                self.Log('error', f'write_read mis-match; read: {data:04x}'
                                  f'expect: {write_value:04x}')

    def DirectedLTC2975BusyTest(self):
        """Use PMBUS Sideband to Perform LTC2975 USER_DATA_03 and USER_DATA_04
        write and read test with random word

        Steps:
        """
        hbidps = self.hbidps_list[0]
        hbidps.check_ltc2975_busy_status()
        fifo_list = self.create_config_busy_fifo_list()
        hbidps.pmbus_send_tx_commands(fifo_list)
        hbidps.check_ltc2975_busy_status()

    def create_config_busy_fifo_list(self):
        fifo_list = []
        for chip in range(4):
            fifo_list += self.create_one_chip_config_busy_fifo_list(chip)
        return fifo_list

    def create_one_chip_config_busy_fifo_list(self, chip):
        fifo_list = []
        fifo_list.append(self.create_one_tx_data(chip, 0x00, 1, 0x00))
        fifo_list.append(self.create_one_tx_data(chip, 0xD0, 3, 0x0080))
        fifo_list.append(self.create_one_tx_data(chip, 0x00, 1, 0x01))
        fifo_list.append(self.create_one_tx_data(chip, 0xD0, 3, 0x0080))
        fifo_list.append(self.create_one_tx_data(chip, 0x00, 1, 0x02))
        fifo_list.append(self.create_one_tx_data(chip, 0xD0, 3, 0x0080))
        fifo_list.append(self.create_one_tx_data(chip, 0x00, 1, 0x03))
        fifo_list.append(self.create_one_tx_data(chip, 0xD0, 3, 0x0080))
        return fifo_list

    def create_user_data_write_read_tx_fifo_list(self, value):
        fifo_list = []
        #LC0; CMD 0XB4; 0XB3; W WORD;
        fifo_list.append(self.create_one_tx_data(0, 0xB4, 3, value))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x00))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 3, value))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x01))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 3, value))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x02))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 3, value))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x03))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 3, value))
        #LC0; CMD 0XB4; 0XB3; R WORD;
        fifo_list.append(self.create_one_tx_data(0, 0xB4, 4, 0x0000))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x00))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 4, 0x0000))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x01))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 4, 0x0000))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x02))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 4, 0x0000))
        fifo_list.append(self.create_one_tx_data(0, 0x00, 1, 0x03))
        fifo_list.append(self.create_one_tx_data(0, 0xB3, 4, 0x0000))
        return fifo_list

    def create_one_tx_data(self, device, cmd_code, transaction, tx_data):
        pmbus_data_set = {"device": device, "cmd_code": cmd_code,
                          "transaction": transaction, "tx_data": tx_data}
        return pmbus_data_set


class PMBUSEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.initialize_dtb_hc_lc_mux()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.initialize_dtb_hc_lc_mux()
        time.sleep(0.2)
