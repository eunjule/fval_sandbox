################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


from Common.fval import TestCase
from Hbidps.testbench.env import Env

class HbidpsTest(TestCase):
    dutName = 'hbidps'
    
    def setUp(self):
        self.LogStart()
        self.env = Env(self)
        self.hbidps_list = self.reorder_hbidps_list()

    def reorder_hbidps_list(self):
        fpgas = self.env.get_fpgas()
        hbidps_list = []
        if len(fpgas) == 0:
            self.Log('error', 'No hbidps cards on tester')
        else:
            for i in range(len(fpgas)):
                for j in range(len(fpgas)):
                    hbidps_found = fpgas[j]
                    if hbidps_found.slot == i:
                        hbidps_list.append(hbidps_found)
                        break
        return hbidps_list

    def tearDown(self):
        self.LogEnd()