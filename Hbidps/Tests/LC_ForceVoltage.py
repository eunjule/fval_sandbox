################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing of LTC2975 Force voltage control"""
import random
import time
import struct
from string import Template

from Common.fval import skip
from Hbidps.Tests.Alarms import AlarmHelper
from Hbidps.Tests.HbidpsTest import HbidpsTest
from Hbidps.instrument import hbidps_register
from Hbidps.instrument.LCRailPMBUS import LCRailPMBUS
from Hbidps.instrument.MdutConfiguration import MdutConfiguration
from Hbidps.instrument.RailTransitionControl import RailTransitionControl
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.instrument.TQMachine import TriggerQueueDDR
from Hbidps.testbench.assembler import TriggerQueueAssembler
from Hbidps.testbench.lc import LCRailTestbench
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands

MAX_FAIL_COUNT = 10
LC_TQ_RAIL_ID_BASE = 0x20
LC_TQ_I_HI = 2.0
LC_INITIAL_I_HI = 4


class BaseTest(HbidpsTest):

    def compare_expected_with_observed(self, expected, observed, error_count, iteration, target_pmbus_device,
                                       rail_under_test):
        if observed != expected:
            error_count.append(
                {'Iteration': iteration, 'Expected': hex(expected), 'Observed': hex(observed), 'Rail': rail_under_test,
                 'Target pmbus device': target_pmbus_device})

    def report_errors(self, device, register_errors, error_message_to_print):
        column_headers = ['Iteration', 'Expected', 'Observed', 'Rail', 'Target pmbus device']
        table = device.contruct_text_table(column_headers, register_errors)
        device.log_device_message('{} {}'.format(error_message_to_print, table), 'error')

class TqDiagnostics(BaseTest):
    """Test Forcing voltages and voltage bumps using Triggers"""
    TEST_ITERATIONS = 100
    ltc_pages = 4
    ltc_devices = 4
    lc_base = 0x20
    limit_tolerance_in_volts = .2

    def RandomLcRailScratchPadWriteViaTqReadViaPmBusTest(self):
        """Send random data to scratch pad register via Trigger queue. Read data back from device using PM BUS read.

           Confirm results are same.
        """
        testname = 'RandomLcRailScratchPadWriteViaTqReadViaPmBusTest'
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            read_back_errors = self.scratch_pad_test_scenario(hbidps)
            self.display_pass_fail_messages(hbidps, testname, read_back_errors)

    def scratch_pad_test_scenario(self, hbidps):
        read_back_errors = []
        error_count = []
        pm_bus_command = hbidps.symbols.PMBUSCOMMAND.USER_DATA_04_SCRATCH_PAD
        for iteration in range(self.TEST_ITERATIONS):
            hbidps.clear_alarm_register()
            hbidps.clear_pmbus_rx_fifo()
            random_data_to_write, random_lc_rail = self.generate_random_data()
            rail_under_test = self.rail_under_test(random_lc_rail)
            target_pmbus_device = self.target_pmbus_device(random_lc_rail)
            self.send_data_from_trigger_queue(hbidps, pm_bus_command, random_data_to_write, random_lc_rail,
                                              rail_under_test)
            for retries in range(100):
                read_back_data = self.read_data_from_pmbus_command(hbidps, target_pmbus_device, pm_bus_command)
                if random_data_to_write == read_back_data:
                    break
            else:
                self.compare_expected_with_observed(random_data_to_write, read_back_data, error_count, iteration,
                                                    target_pmbus_device, rail_under_test)

            if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= self.TEST_ITERATIONS:
                break
        read_back_errors.extend(error_count)
        return read_back_errors

    def generate_random_data(self):
        random_data_to_write = random.randint(0, 0xFFFF)
        random_lc_rail = random.randint(0x20, 0x2f)
        return random_data_to_write, random_lc_rail

    def rail_under_test(self, lc_rail):
        return lc_rail - 0x20

    def target_pmbus_device(self, lc_rail):
        return self.rail_under_test(lc_rail) // 4

    def page_num(self, lc_rail):
        return self.rail_under_test(lc_rail) % 4

    def send_data_from_trigger_queue(self, hbidps, pm_bus_command, random_data_to_write, random_lc_rail,
                                     rail_under_test):
        trigger_queue_offset = 0x0
        trigger_queue_data = self.generate_trigger_queue(hbidps, random_lc_rail, pm_bus_command, random_data_to_write,
                                                         rail_under_test)
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)

    def generate_trigger_queue(self, hbidps, random_lc_rail, pm_bus_command, random_data_to_write, rail_under_test):
        trigger_queue = TriggerQueueAssembler()
        tq_string = Template('''
                    WakeRail resourceid =$resourceid,  railmask =$railmask
                    SetRailBusy resourceid =$resourceid, railmask =$railmask
                    WriteWord rail =$rail, value =$data, pmbuscmd = $pmbus_cmd
                    TqNotify resourceid =$resourceid, railmask =$railmask
                    TimeDelay rail =$rail, value =$value
                    TqComplete rail =$rail, value = 0
                 ''').substitute(rail=str(hex(random_lc_rail)), data=random_data_to_write, pmbus_cmd=pm_bus_command,
                                 resourceid=0x80, railmask=1 << rail_under_test, value=str(0x186A0))
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        return trigger_queue_data

    def read_data_from_pmbus_command(self, hbidps, target_pmbus_device, pm_bus_command):
        self.generate_pmbus_tx_fifo_data(hbidps, pm_bus_command, target_pmbus_device)
        hbidps.execute_pm_bus_command()
        if not hbidps.is_pm_bus_busy():
            self.Log('error', 'PM BUS I2C link busy')
            rx_fifo = None
        else:
            rx_fifo = hbidps.read_bar_register(hbidps.registers.PMBUS_RX_FIFO).value
        return rx_fifo

    def generate_pmbus_tx_fifo_data(self, hbidps, pm_bus_command, target_pmbus_device):
        READ_WORD = 0x4
        pm_bus_tx_fifo = hbidps.registers.PMBUS_TX_FIFO()
        pm_bus_tx_fifo.target_device_number = target_pmbus_device
        pm_bus_tx_fifo.target_specific_register_address = pm_bus_command
        pm_bus_tx_fifo.pm_bus_transaction = READ_WORD
        hbidps.write_bar_register(hbidps.registers.PMBUS_TX_FIFO(value=pm_bus_tx_fifo.value))

    def display_pass_fail_messages(self, hbidps, testname, read_back_errors):
        if read_back_errors:
            error_message_to_print = f'LTC 2975 {testname} failed '
            self.report_errors(hbidps, read_back_errors, error_message_to_print)
        else:
            self.Log('info', f'LTC 2975 {testname} passed for {self.TEST_ITERATIONS} iterations.')

    def RandomLcRailPagedScratchPadWriteViaTqReadViaPmBusTest(self):
        """Send random data to paged scratch pad register via Trigger queue.Read data back from device using PM BUS read.

           Confirm results are same.
        """
        testname = 'RandomLcRailPagedScratchPadWriteViaTqReadViaPmBusTest'
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            read_back_errors = self.paged_scratch_pad_test_scenario(hbidps)
            self.display_pass_fail_messages(hbidps, testname, read_back_errors)

    def paged_scratch_pad_test_scenario(self, hbidps):
        read_back_errors = []
        error_count = []
        page_command, pm_bus_command = self.set_pm_bus_commands(hbidps)
        for iteration in range(self.TEST_ITERATIONS):
            hbidps.clear_alarm_register()
            hbidps.clear_pmbus_rx_fifo()
            random_data_to_write, _ = self.generate_random_data()
            page_number = random.choice(range(self.ltc_pages))
            target_pmbus_device = random.choice(range(self.ltc_devices))
            rail_under_test = target_pmbus_device * self.ltc_devices + page_number
            random_lc_rail = self.lc_base + rail_under_test
            self.send_paged_data_from_trigger_queue(hbidps, pm_bus_command, random_lc_rail, rail_under_test,
                                                    target_pmbus_device, error_count, iteration, page_command,
                                                    page_number)
            if len(error_count) >= MAX_FAIL_COUNT or len(error_count) >= self.TEST_ITERATIONS:
                break
        read_back_errors.extend(error_count)
        return read_back_errors

    def set_pm_bus_commands(self, hbidps):
        pm_bus_command = hbidps.symbols.PMBUSCOMMAND.USER_DATA_03_SCRATCH_PAD
        page_command = hbidps.symbols.PMBUSCOMMAND.PAGE_CMD
        return page_command, pm_bus_command

    def generate_paged_trigger_queue(self, hbidps, random_lc_rail, pm_bus_command, target_pmbus_device, error_count,
                                     iteration, rail_under_test, page_command, page_number):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        data_to_write = random.randint(0, 0xFFFF)
        tq_string = Template('''
                    WakeRail resourceid =$resourceid,  railmask =$railmask
                    SetRailBusy resourceid =$resourceid, railmask =$railmask
                    WriteByte rail=$rail, value=$data_1 ,pmbuscmd = $pmbus_cmd_1                    
                    WriteWord rail =$rail, value =$data, pmbuscmd = $pmbus_cmd
                    TimeDelay rail =$rail, value =$value
                    TqNotify resourceid =$resourceid, railmask =$railmask
                    TqComplete rail =$rail, value = 0
                 ''').substitute(rail=str(hex(random_lc_rail)), data=data_to_write, pmbus_cmd=pm_bus_command,
                                 resourceid=0x80, data_1=page_number, railmask=1 << rail_under_test,
                                 value=str(0x186A0), pmbus_cmd_1=page_command)
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)
        self.read_and_compare_data(data_to_write, error_count, hbidps, iteration, page_command, page_number,
                                   pm_bus_command, rail_under_test, target_pmbus_device)
        return trigger_queue_data

    def read_and_compare_data(self, data_to_write, error_count, hbidps, iteration, page_command, page_number,
                              pm_bus_command, rail_under_test, target_pmbus_device):
        self.set_page_via_pmbus(hbidps, page_number, target_pmbus_device, page_command)
        hbidps.execute_pm_bus_command()

        for retries in range(100):
            read_back_data = self.read_data_from_pmbus_command(hbidps, target_pmbus_device, pm_bus_command)
            if data_to_write == read_back_data:
                break
        else:
            self.compare_expected_with_observed(data_to_write, read_back_data, error_count, iteration,
                                                target_pmbus_device,
                                                rail_under_test)

    def send_paged_data_from_trigger_queue(self, hbidps, pm_bus_command, random_lc_rail, rail_under_test,
                                           target_pmbus_device, error_count, iteration, page_command, page_number):

        self.generate_paged_trigger_queue(hbidps, random_lc_rail, pm_bus_command,
                                          target_pmbus_device, error_count, iteration,
                                          rail_under_test, page_command, page_number)

    def set_page_via_pmbus(self, hbidps, page_number, target_pmbus_device, pm_bus_command):
        WRITE_BYTE = 0x1
        pm_bus_tx_fifo = hbidps.registers.PMBUS_TX_FIFO()
        pm_bus_tx_fifo.target_device_number = target_pmbus_device
        pm_bus_tx_fifo.target_specific_register_address = pm_bus_command  # page_command
        pm_bus_tx_fifo.pm_bus_transaction = WRITE_BYTE
        pm_bus_tx_fifo.tx_data = page_number
        hbidps.write_bar_register(hbidps.registers.PMBUS_TX_FIFO(value=pm_bus_tx_fifo.value))

    def DirectedForceVoltageTest(self):
        """Force a voltage over the trigger bus and compare the output to the read voltage.

        This test sends random voltages over random LC rails on a single DPS.
        This test requires a DTB to be present.
        """
        testname = 'DirectedForceVoltageTest'
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            # Check that DTB is installed'
            if hbidps.hbidtb:
                read_back_errors = self._voltage_test_scenario(hbidps)
                self.display_pass_fail_messages(hbidps, testname, read_back_errors)
            else:
                self.Log('error', f'{hbidps.name_with_slot()}: Failed due to absent of HBIDTB')

    def _voltage_test_scenario(self, hbidps):
        lcrail = LCRailTestbench()
        error_list = []
        lcrail.init_lc_for_test(hbidps)
        lcrail.shutdown_lc_rails(hbidps)
        for iteration in range(self.TEST_ITERATIONS):
            # clear and init
            lcrail.initialize_dps_dtb(hbidps)
            # do randomization for rail by randomizing the device and the rail on the device'
            target_pmbus_device = random.choice(range(self.ltc_devices))
            page_num = random.choice(range(self.ltc_pages))
            rail_under_test = target_pmbus_device * lcrail.ltc_devices + page_num
            random_lc_rail = lcrail.lc_base + rail_under_test
            lcrail.init_2975_for_test(hbidps, target_pmbus_device, page_num)
            lcrail.setup_dtb_for_single_rail_lc_test(hbidps, target_pmbus_device, rail_under_test)
            # enable all LTC'
            lcrail.set_LTC_control(hbidps, control=0xF)
            # Set Target Voltage to 1,2,3,or 4 V
            random_voltage = random.choice(range(4))+1
            random_voltage_str = '0x' + str(struct.pack('>f', random_voltage).hex())
            if hbidps.wait_for_ltc2975(target_pmbus_device, page_num, 100):
                self.Log('info', 'LTC2975 Busy')
            # run the trigger to set output voltage'
            lcrail.off_on_trigger_queue(hbidps, random_lc_rail, rail_under_test, random_voltage_str)
            if not(hbidps.wait_for_trigger(100)):
                self.Log('info', "trigger_Busy")
            time.sleep(0.2)
            # check for voltage being set
            self._check_vm_result(hbidps, rail_under_test, random_voltage, error_list, iteration)
            time.sleep(.002)
            # Shutdown LC Rail'
            lcrail.shutdown_lc_rails(hbidps)
            if len(error_list) >= MAX_FAIL_COUNT or len(error_list) >= self.TEST_ITERATIONS:
                break
        lcrail.init_dtb_for_lc(hbidps)
        return error_list

    def _check_vm_result(self, hbidps, rail_under_test, target_voltage, error_list, iteration):
        hil_voltage = hbidps.get_lc_rail_voltage(rail_under_test)
        for i in range(100):
            if abs(target_voltage - hil_voltage) > self.limit_tolerance_in_volts:
                hil_voltage = hbidps.get_lc_rail_voltage(rail_under_test)
            else:
                return
        hbidps.print_ltc2975_status_registers(rail_under_test//4, rail_under_test % 4)
        error_list.append({'Iteration': iteration, 'Expected': target_voltage, 'Observed': hil_voltage, 'Rail': rail_under_test, 'Target pmbus device': rail_under_test//4})


class Functional(BaseTest):
    """ 7 Test Cases of LC Rail Operation Included

        Need to clean up the upper class and test cases;
        TQ helper creation in a separate helper function;
        Please refer to HBI_DPS_Channel_Transition.docx for Operation detail;

        Steps for following tests:
        1.Configure the DTB bord for the output load
        2.Check rail statues and set to steady mode (off) if needed
        3.Check global alarm and reset if needed
        4.Configure DUT_DOMAIN_ID_NN with dut_id
        5.Creat TQ content
        6.Store TQ content and lut content
        7.Clear tq_notify flag in global alarm register
        8.Send trigger with configured dut_id
        9.Check tq_notify flag in global alarm register
        10.Check voltage and current value of selected rail
        11.Check global alarm (should be clean)
        12.Call LC rail off mode TQ method
        13.Check global alarm and reset if needed
    """

    def DirectedLCRailOfftoZeroVoltForceTest(self):
        """ Set one LC rail from float mode to ground force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the rail to ZVF state
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            self.general_lc_1rail_off_initial_state_test(vout=0, dtb_feedback=False, rail=rail, hbidps=hbidps)

    def DirectedLCRailOfftoOnDTBFeedbackTest(self):
        """ Set one LC rail from float mode to >0.5V force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail to On state
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            vout = 3
            self.general_lc_1rail_off_initial_state_test(vout=vout, dtb_feedback=True, rail=rail, hbidps=hbidps)

    def DirectedLCRailOfftoOnTest(self):
        """ Set one LC rail from float mode to >0.5V force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail to On state
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            vout = random.random() * 3 + 2
            self.general_lc_1rail_off_initial_state_test(vout=vout, dtb_feedback=False, rail=rail, hbidps=hbidps)

    def DirectedLCRailZeroVoltForcetoOffTest(self):
        """ Set one LC rail from ground force mode to float mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to ZVF
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from ZVF state to off.

            Note: in the second transition, the initial power state should be ZVF
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            self.general_lc_1rail_zero_voltage_initial_state_test(vout=None, rail=rail, hbidps=hbidps)

    def DirectedLCRailZeroVoltForcetoOnTest(self):
        """ Set one LC rail from ground force mode to >0.5V force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to ZVF
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from ZVF state to On.

            Note: in the second transition, the initial power state should be ZVF
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            vout = random.random() * 3 + 2
            self.general_lc_1rail_zero_voltage_initial_state_test(vout=vout, rail=rail, hbidps=hbidps)

    def DirectedLCRailOntoOffTest(self):
        """ Set one LC rail from >0.5V force mode to float mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to On
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from On state to Off.

            Note: in the second transition, the initial power state should be On
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            self.general_lc_1rail_on_initial_state_test(vout=None, rail=rail, hbidps=hbidps)

    def DirectedLCRailOntoZeroVoltForceTest(self):
        """ Set one LC rail from >0.5V force mode to ground force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to On
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from On state to ZVF.

            Note: in the second transition, the initial power state should be On
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            self.general_lc_1rail_on_initial_state_test(vout=0, rail=rail, hbidps=hbidps)

    def DirectedLCRailVbumpUpTest(self):
        """ Set one LC rail from >0.5V force mode to other >0.5V force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to On
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from On(v1) to On(v2).

            Note: in the second transition, the initial power state should be On; v1 < v2
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            vout_initial = random.random() * 2.5 + 1
            vout = vout_initial + 0.5 + random.random()
            if vout <= vout_initial:
                self.Log('error', f'vout: {vout}V is not higher than vout_initial: {vout_initial}V')
            self.lc_1rail_arbitrary_initial_state_test(vout_initial=vout_initial, vout=vout, rail=rail, hbidps=hbidps)

    def DirectedLCRailVbumpDownTest(self):
        """ Set one LC rail from >0.5V force mode to other >0.5V force mode

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to On
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from On(v1) to On(v2).

            Note: in the second transition, the initial power state should be On; v1 > v2
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            vout_initial = random.random() * 2 + 3
            vout = vout_initial - 0.5 - random.random()
            if vout >= vout_initial:
                self.Log('error', f'vout: {vout}V is not lower than vout_initial: {vout_initial}V')
            self.lc_1rail_arbitrary_initial_state_test(vout_initial=vout_initial, vout=vout, rail=rail, hbidps=hbidps)

    def DirectedLCRailOfftoOffTest(self):
        """ Set one LC rail from float mode to float mode

            FPGA Review Note:
            Blocking transition feature should block this. Check with FPGA

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail to On state
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            self.general_lc_1rail_off_initial_state_test(vout=None, dtb_feedback=False, rail=rail, hbidps=hbidps)

    def DirectedLCRailZeroVoltForcetoZeroVoltForceTest(self):
        """ Set one LC rail from ground force mode mode to ground force mode

            FPGA Review Note:
            Blocking transition feature should block this. Check with FPGA

            Test steps:
                1.  choose one rail of the selected hbidps
                2.  detect and reset global alarms
                3.  check the lc device status and reinitialize if needed
                4.  enable all the lc rails by turning on all the control pins
                5.  setup dut, uhc mapping
                6.  create trigger queue content which transit the lc rail from off to ZVF
                7.  store tq address to LUT and trigger queue content to tq address in memory
                8.  clear trigger queue notify and check whether power state is off
                9.  send the trigger and wait for lc trigger queue notify flag
                10. verify trigger queue execution results: vout, power state and lc device info set
                11. start another transition with same steps from 5 to 10 but from ZVF state to ZVF.

            Note: in the second transition, the initial power state should be ZVF
        """
        hbidps = self.hbidps_list[0]
        for rail in range(16):
            self.general_lc_1rail_zero_voltage_initial_state_test(vout=0, rail=rail, hbidps=hbidps)

    @skip('New Feature HDMT Ticket 89603')
    def DirectedLCRailFlushTest(self):
        """ Set one LC rail with flush command

            Test steps:
                1. flush should happen without the delay setting in rail
        """
        pass

    @skip('New Feature HDMT Ticket 127291')
    def DirectedLCRailONtoZVFVoutTest(self):
        """ Set one LC rail from >0.5V force mode to ground force mode

            Test steps:
                1. set the rails from the OFF to ON state.
                2. Once the rails are in the ON state, then set the rails from the ON to ZVF state.
                3. Check the vout register in the external device
        """
        pass


    def general_lc_1rail_off_initial_state_test(self, vout, dtb_feedback, rail=None, hbidps=None):
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        self.Log('info', f'{"*" * 10}Testing lc rail {rail}; hbidps{hbidps.slot}; vout = {vout}V{"*" * 10}')

        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            if dtb_feedback:
                hbidps.config_dtb_lc_rail_load_feedback(rail, False)
            self.execute_one_tq_transition(hbidps, rail, None, vout)

    def execute_one_tq_transition(self, hbidps, rail, vout_initial, vout):
        self.Log('info', f'execute transition: vout_initial = {vout_initial}V; vout = {vout}V')
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_lc_1rail_tq_list(rail=rail, vout=vout)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        observed_power_state = (hbidps.get_lc_power_state().value >> (rail * 2)) & 0b11
        _, initial_pwr_state, _, _, _ = self.get_vout_settings_and_power_state(vout_initial)
        if observed_power_state != initial_pwr_state:
            self.Log('error', f'lc rail {rail} initial power state{bin(observed_power_state)}'
                              f'!= expected: {bin(initial_pwr_state)}')
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
        time.sleep(0.5)
        self.check_tq_execution_result(hbidps, vout_initial, vout, rail)

    def create_lc_1rail_tq_list(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <= 15:
            lc_one_hot = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
        else:
            lc_one_hot = 0
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE)
            self.Log('error', f'rail number {lc_rail} given to lc trigger queue is not valid')
        ov, pwr_state, uv, vout_on, oc = self.get_vout_settings_and_power_state(vout)

        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)
        self.Log('info', f'ov: {ov}, uv: {uv}, vout: {vout_on}, fdi_hi: {LC_TQ_I_HI}, i_hi_hex: {oc}, pwr_state: {pwr_state}')

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, 0x01000)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def get_vout_settings_and_power_state(self, vout):
        if vout is None:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 6.0
            oc = 2.0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            uv = 0
            ov = 0.5
            oc = 1.5
        elif 0.6 <= vout <= 5:
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0
        else:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 7.0
            oc = 0.5
            self.Log('error', f'vout: {vout}V is not a valid value for lc rail')
        return ov, pwr_state, uv, vout_on, oc

    def check_tq_execution_result(self, hbidps, vout_initial, vout, rail):
        self.Log('info', f'checking test result')
        hbidps.detect_global_alarm()
        ov, pwr_state, uv, expected_vout, oc = self.get_vout_settings_and_power_state(vout)
        read_vout = hbidps.lc_rail_voltage_read(rail)
        if abs(read_vout - expected_vout) > 0.2:
            self.Log('error', f'vout_fail expect:{expected_vout}V '
                              f'read: {read_vout}V')
        else:
            self.Log('info', f'vout read from rail {rail}: {read_vout}V '
                             f'is the same as expected: {expected_vout}V')
        observed_power_state = (hbidps.get_lc_power_state().value >> (rail * 2)) & 0b11
        if observed_power_state != pwr_state:
            self.Log('error', f'lc rail {rail} power state reg expected: {pwr_state}'
                              f'observed: {observed_power_state}')
        else:
            self.Log('info', f'power state from rail {rail}: {observed_power_state} '
                             f'is the same as expected: {pwr_state}')
        self.check_full_lc_info_set(vout_initial, vout, hbidps, ov, rail, uv, oc)

    def check_full_lc_info_set(self, vout_initial, vout, hbidps, ov, rail, uv, oc):
        if vout is None:
            expected_vout = 0
        else:
            expected_vout = vout
        expected_oc = oc
        info_set_list = hbidps.read_lc_rails_device_status()
        for chip in hbidps.lc_rail_pmbus.chips:
            expected_chip_info_map = {
                'STATUS_INPUT': hex(0x0),
                'STATUS_CML': hex(0x0),
                'MFR_FAULT_LOG_STATUS': hex(0x0),
                'VIN_ON': 9.0,
                'VIN_OFF': 8.0,
                'VIN_OV_FAULT_LIMIT': 14.0,
                'VIN_OV_WARN_LIMIT': 15.0,
                'VIN_OV_FAULT_RESPONSE': hex(0x80),
                'VIN_UV_WARN_LIMIT': 0.0,
                'VIN_UV_FAULT_LIMIT': 0.0,
                'VIN_UV_FAULT_RESPONSE': hex(0x80)
            }
            hbidps.lc_rail_pmbus.check_chip_common_info_set(chip, expected_chip_info_map, info_set_list)
            for page in hbidps.lc_rail_pmbus.pages_in_chip:
                if chip == (rail // 4) and page == (rail % 4):
                    expected_status = int(not (expected_vout > 0)) << 6
                    expected_paged_info_map = {
                        'STATUS_BYTE': hex(expected_status),
                        'STATUS_WORD': hex(expected_status),
                        'STATUS_VOUT': hex(0x0),
                        'STATUS_IOUT': hex(0x0),
                        'STATUS_TEMPERATURE': hex(0x0),
                        'STATUS_MFR_SPECIFIC': hex((int(expected_vout > 0) << 3)),
                        'OT_FAULT_LIMIT': 1000.0,
                        'OT_WARN_LIMIT': 1000.0,
                        'OT_FAULT_RESPONSE': hex(0x80),
                        'UT_WARN_LIMIT': -1000.0,
                        'UT_FAULT_LIMIT': -1000.0,
                        'UT_FAULT_RESPONSE': hex(0x80),
                        'READ_VOUT': float(expected_vout),
                        'VOUT_MODE': hex(0x13),
                        'VOUT_COMMAND': 5.0,
                        'VOUT_MAX': 6.0,
                        'VOUT_MARGIN_HIGH': 1.05,
                        'VOUT_MARGIN_LOW': 0.95,
                        'VOUT_OV_FAULT_LIMIT': ov,
                        'VOUT_OV_WARN_LIMIT': 7.0,
                        'VOUT_UV_WARN_LIMIT': 0.0,
                        'VOUT_UV_FAULT_LIMIT': uv,
                        'VOUT_OV_FAULT_RESPONSE': hex(0x80),
                        'VOUT_UV_FAULT_RESPONSE': hex(0x80),
                        'IOUT_OC_FAULT_LIMIT': expected_oc,
                        'IOUT_OC_WARN_LIMIT': 7.0,
                        'IOUT_UC_FAULT_LIMIT': -10.0
                    }
                    if (vout_initial is None and vout is None) or (vout_initial == 0 and vout == 0):
                        expected_paged_info_map.update({'VOUT_OV_FAULT_LIMIT': self.previous_ov})
                        expected_paged_info_map.update({'VOUT_UV_FAULT_LIMIT':  self.previous_uv})
                        expected_paged_info_map.update({'IOUT_OC_FAULT_LIMIT': self.previous_oc})
                    else:
                        self.Log('info', f'Update uv: {uv}, ov: {ov} and oc: {oc}')
                        self.previous_ov = ov
                        self.previous_uv = uv
                        self.previous_oc = oc
                else:
                    expected_paged_info_map = {
                        'OPERATION': hex(0x0),
                        'STATUS_BYTE': hex(0x40),
                        'STATUS_WORD': hex(0x40),
                        'STATUS_VOUT': hex(0x0),
                        'STATUS_IOUT': hex(0x0),
                        'STATUS_TEMPERATURE': hex(0x0),
                        'STATUS_MFR_SPECIFIC': hex(0x0),
                        'OT_FAULT_LIMIT': 1000.0,
                        'OT_WARN_LIMIT': 1000.0,
                        'OT_FAULT_RESPONSE': hex(0x80),
                        'UT_WARN_LIMIT': -1000.0,
                        'UT_FAULT_LIMIT': -1000.0,
                        'UT_FAULT_RESPONSE': hex(0x80),
                        'READ_VOUT': float(expected_vout),
                        'VOUT_MODE': hex(0x13),
                        'VOUT_COMMAND': 5.0,
                        'VOUT_MAX': 6.0,
                        'VOUT_MARGIN_HIGH': 1.05,
                        'VOUT_MARGIN_LOW': 0.95,
                        'VOUT_OV_FAULT_LIMIT': 7.0,
                        'VOUT_OV_WARN_LIMIT': 7.0,
                        'VOUT_UV_WARN_LIMIT': 0.0,
                        'VOUT_UV_FAULT_LIMIT': 0.0,
                        'VOUT_OV_FAULT_RESPONSE': hex(0x80),
                        'VOUT_UV_FAULT_RESPONSE': hex(0x80),
                        'IOUT_OC_FAULT_LIMIT': LC_INITIAL_I_HI,
                        'IOUT_OC_WARN_LIMIT': 7.0,
                        'IOUT_UC_FAULT_LIMIT': -10.0

                    }
                hbidps.lc_rail_pmbus.check_chip_paged_info_set(chip, expected_paged_info_map, info_set_list, page)

    def init_limits(self):
        self.previous_ov = 7
        self.previous_uv = 0
        self.previous_oc = 4

    def general_lc_1rail_zero_voltage_initial_state_test(self, vout, rail=None, hbidps=None):
        self.lc_1rail_arbitrary_initial_state_test(vout_initial=0, vout=vout, hbidps=hbidps, rail=rail)

    def lc_1rail_arbitrary_initial_state_test(self, vout_initial, vout, hbidps=None, rail=None):
        _, pwr_state_initial, _, _, _ = self.get_vout_settings_and_power_state(vout_initial)
        if rail is None:
            rail = random.randint(0, 15)
        if hbidps is None:
            hbidps = self.env.get_fpgas()[0]
        self.Log('info', f'{"*" * 10}Testing lc rail {rail}; hbidps{hbidps.slot}; vout = {vout}V{"*" * 10}')
        with LCRailsEnv(hbidps, self):
            hbidps.enable_lc_rails_ctrl()
            self.execute_one_tq_transition(hbidps, rail, None, vout_initial)

            self.execute_one_tq_transition(hbidps, rail, vout_initial, vout)

    def general_lc_1rail_on_initial_state_test(self, vout, rail=None, hbidps=None):
        vout_initial = random.random() * 3 + 2
        self.lc_1rail_arbitrary_initial_state_test(vout_initial=vout_initial, vout=vout, hbidps=hbidps, rail=rail)

    def lc_rail_init_debug_test(self):
        """This is a example to use one DPS for the following(This test is only for debugging purpose):

        1. LC device initialization and comparison of ltc2975 info set before and after
        2. TQ content generation
        3. TQ execution through DDR
        4. check ltc2975 info set after TQ execution
        """
        hbidps_one = self.env.get_fpgas()[0]

        self.create_tq_test_interface(hbidps_one)

        info_set_before = self.lc_rail.read_ltc2975_info_set()  # don't need in offontest
        hbidps_one.initialize_hbi_dps_board()  # don't need in offontest
        self.lc_rail.init_lc_rails_hw(False)
        info_set_after = self.lc_rail.read_ltc2975_info_set()
        self.log_info_set_comparison(hbidps_one, info_set_after, info_set_before)
        self.lc_rail.check_info_set(info_set_after)

        self.rail_transition.init_rail_transition_time()  # maybe not needed in offontest
        self.rail_transition.log_rail_transition_time()

        self.lc_rail.enable_lc_chips(self.lc_rail.chips)
        self.mdut_config.set_uhc_dut_status(uhc_id=0, dut_id=1, valid=1, log=True)

        self.check_lc_fold_status(hbidps_one)
        meta_data = random.randrange(0xfff)

        self.tq_generator.create_tq_list()
        self.tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, 0x1)
        self.tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, 0x1)
        self.tq_generator.add_command(tq_cmd.set_ov_limit, tq_dev.LCrail0, self.tq_generator.float_to_hex(3.0))
        self.tq_generator.add_command(tq_cmd.set_uv_limit, tq_dev.LCrail0, self.tq_generator.float_to_hex(0.0))
        self.tq_generator.add_command(tq_cmd.set_v, tq_dev.LCrail0, self.tq_generator.float_to_hex(2.0))
        self.tq_generator.add_command(tq_cmd.set_fd_current_hi, tq_dev.LCrail0, self.tq_generator.float_to_hex(2.0))
        self.tq_generator.add_command(tq_cmd.set_pwr_state, tq_dev.LCrail0, 0x1)
        self.tq_generator.add_command(tq_cmd.set_fdt, tq_dev.LCrail0, 0x01000)
        self.tq_generator.add_command(tq_cmd.set_hi_i_limit, tq_dev.LCrail0, self.tq_generator.float_to_hex(2.0))
        self.tq_generator.add_command(tq_cmd.apply_user_attributes, tq_dev.LCrail0, 0)
        self.tq_generator.add_command(tq_cmd.smp_metadata, tq_dev.LCSampleEngine0, meta_data)
        self.tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, 0x1)
        self.tq_generator.add_end_command()
        tq_list = self.tq_generator.get_tq_list()
        self.tq_generator.delete_tq_list()

        tq_data = self.tq_ddr.get_tq_data(tq_list, True)
        lut_address = self.tq_ddr.get_dps_lut_address(0, 0)
        tq_content_address = self.tq_ddr.get_tq_content_address()
        self.tq_ddr.store_tq_address_to_lut(lut_address, tq_content_address, True)
        self.tq_ddr.store_tq_content_to_mem(tq_content_address, tq_data, True)

        self.tq_ddr.clear_tq_notify(True)

        self.lc_rail.log_lc_rail_dps_power_state()

        global_alarm = self.alarm_helper.read_global_alarm_reg()

        self.Log('info', f'alarm read after notify clear {hex(global_alarm.value)}')

        self.tq_ddr.send_dps_trigger_through_rctc(0x01, 1, 0)

        self.tq_ddr.wait_for_notify(1, 0, 0, True)

        self.Log('info', '******************trigger_done******************')

        self.lc_rail.log_lc_rail_dps_power_state()

        meta = hbidps_one.read_bar_register(hbidps_one.registers.LC_SAMPLE_METADATA_0)
        self.Log('info', f' meta {hex(meta_data)} {hex(meta.value)}')

        self.alarm_helper.detect_global_alarm_status_one_dps()

        self.check_lc_fold_status(hbidps_one)

        self.tq_ddr.check_tq_notify(lc_notify=1, hc_notify=0, vtarg_notify=0, log=True)
        #add a delay 1 s; check channel transition; check VOUT related cmds
        time.sleep(1)
        self.lc_rail.read_ltc2975_info_set()
        info_set_after_tq = self.lc_rail.read_ltc2975_info_set()
        self.lc_rail.print_one_chip_table(0, info_set_after_tq)
        self.lc_rail.print_one_chip_table(1, info_set_after_tq)
        self.lc_rail.print_one_chip_table(2, info_set_after_tq)
        self.lc_rail.print_one_chip_table(3, info_set_after_tq)

    def create_tq_test_interface(self, hbidps_one):
        self.lc_rail = LCRailPMBUS(hbidps_one)
        self.rail_transition = RailTransitionControl(hbidps_one)
        self.tq_generator = TriggerQueueCommands()
        self.tq_ddr = TriggerQueueDDR(hbidps_one)
        self.mdut_config = MdutConfiguration(hbidps_one)
        self.alarm_helper = AlarmHelper(hbidps_one)
        self.alarm_helper.reset_one_hbi_dps_global_alarm_reg()
        self.alarm_helper.detect_global_alarm_status_one_dps()

    def check_lc_fold_status(self, hbidps): # folded means close, not on
        lc_fold_status = hbidps.read_bar_register(hbidps_register.LC_FOLDED)
        lc_fold_status.value = 0xFFFF  # are these register bits also reset by writing a one to it?
        hbidps.write_bar_register(lc_fold_status)
        time.sleep(0.1)
        lc_fold_status = hbidps.read_bar_register(hbidps_register.LC_FOLDED)
        self.Log('info', f'lc_fold_status {hex(lc_fold_status.value)}')

    def log_info_set_comparison(self, hbidps_one, info_set_after, info_set_before):
        info_set_comp_list = []
        for i, info in enumerate(info_set_before):
            comp_info = dict(info)
            comp_info['Before'] = info['Value']
            comp_info['After'] = info_set_after[i]['Value']
            comp_info.pop('Value')
            info_set_comp_list.append(comp_info)

        table = hbidps_one.contruct_text_table(data=info_set_comp_list, title_in=f'LC Device Info Set')
        self.Log('info', f'{table}')


class LCRailsEnv(object):
    def __init__(self, hbidps, test):
        self.hbidps = hbidps
        self.test = test

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_lc_rails_start_state()
        self.test.init_limits()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.set_lc_rails_to_safe_state()
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)