################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing command direct inject"""

import random
import time

from Common.fval import skip
from Hbidps.instrument import hbidps_register
from Hbidps.instrument.TQMachine import device as tq_dev
from Hbidps.instrument.TQMachine import command as tq_cmd
from Hbidps.testbench.TriggerQueueCommands import TriggerQueueCommands
from Hbidps.Tests.HbidpsTest import HbidpsTest

HC_TQ_RAIL_ID_BASE = 0x30
HC_TQ_FDT = 0x1000
HC_TQ_I_HI = 10

LC_TQ_RAIL_ID_BASE = 0x20
LC_TQ_I_HI = 2.0
LC_INITIAL_I_HI = 4

class Functional(HbidpsTest):
    """Check command direct inject method should do the same as DDR TQ"""

    def DirectedHCCmdDirectInjectTest(self):
        """Verify command direct inject has the same functionality of DDR TQ

        Steps for following tests:
        1.Configure the DTB bord for the output load
        2.Check rail statues and set to steady mode (off) if needed
        3.Check global alarm and reset if needed
        4.Configure DUT_DOMAIN_ID_NN with dut_id
        5.Command direct inject and pay attention to delay
        6.Check voltage and current value of selected rail
        7.Check global alarm (should be clean)
        8.Call HC rail off mode TQ method
        9.Check global alarm and reset if needed
        """
        hbidps = self.hbidps_list[0]
        chip = random.choice(range(5))
        page = random.choice(range(2))
        self.write_hc_word_test(hbidps, chip, page)
        self.write_hc_byte_test(hbidps, chip, page)
        self.send_hc_byte_test(hbidps, chip, page)

    def DirectedLCCmdDirectInjectTest(self):
        """Modify LTC 2975 commands using trigger queue command

            use send_byte, write_byte, write_word trigger queue command to
            modify ltc 2975 command registers.
            Test steps:
                1. choose chip, page and hbidps
                2. use write_word trigger queue command to modify user data 03(0xb3) and user data 04(0xb4)
                3. use write_byte trigger queue command to modify MFR_RETRY_COUNT(0xf7) and operation(0x01)
                4. user send_byte trigger queue command to send MFR_CLEAR_ENERGY(0xcc)
        """
        hbidps = self.hbidps_list[0]
        chip = random.choice(range(4))
        page = random.choice(range(4))
        self.write_lc_word_test(hbidps, chip, page)
        self.write_lc_byte_test(hbidps, chip, page)
        self.send_lc_byte_test(hbidps, chip, page)

    @skip('Not implemented')
    def DirectedTQMachineArbiterTest(self):
        """Check Command Direct Inject has higher priority than DDR Arbiter

        Case1: Send command inject before DDR TQ, check which one get excuted
        Case2: Send command inject after DDR TQ, check which one get exceted
        """
        pass

    def write_hc_word_test(self, hbidps, chip, page):
        with HCRailsCmdDirectInjectEnv(hbidps):
            rail = chip * 2 + page
            cmd = 0xb3
            data = random.getrandbits(16)
            self.write_hc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_word)
            observed_data = hbidps.get_hc_user_data_03(chip, page)
            if observed_data != data:
                self.Log('error', f'observed user data at CMD{hex(cmd)}: '
                                  f'observed: {hex(observed_data)} != expected {hex(data)}')
            else:
                self.Log('info', f'Chip {chip}, Page {page} write word test to cmd {hex(cmd)} is successful'
                                 f'observed: {hex(observed_data)} == expected {hex(data)}')

            rail = chip * 2
            cmd = 0xb4
            data = random.getrandbits(16)
            self.write_hc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_word)
            observed_data = hbidps.get_hc_user_data_04(chip)
            if observed_data != data:
                self.Log('error', f'observed user data at CMD{hex(cmd)}: '
                                  f'observed: {hex(observed_data)} != expected {hex(data)}')
            else:
                self.Log('info', f'Chip {chip}, Unpaged write word test to cmd {hex(cmd)} is successful'
                                 f'observed: {hex(observed_data)} == expected {hex(data)}')

    def write_hc_byte_test(self, hbidps, chip, page):
        with HCRailsCmdDirectInjectEnv(hbidps):
            rail = chip * 2
            cmd = 0x0
            # data = (random.getrandbits(2) << 6) | (random.choice([0b111, 0b000]) << 3) | random.getrandbits(3)
            data = random.randrange(2)
            observed_data = hbidps.read_hc_reg_command(0x0, 1, chip)
            self.Log('info', f'at first, page is: {hex(observed_data)}')
            hbidps.write_ltm4680(chip, cmd, hbidps.hc_rail_pmbus.val_to_reg(data), page)
            observed_data = hbidps.read_hc_reg_command(cmd, 1, chip)
            self.Log('info', f'before tq, page is: {hex(observed_data)}')
            data = 1 - data
            self.write_hc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_byte)
            observed_data = hbidps.read_hc_reg_command(cmd, 1, chip)
            if observed_data != data:
                self.Log('error', f'observed unpaged byte CMD {hex(cmd)}: '
                                  f'observed: {hex(observed_data)} != expected {hex(data)}')
            else:
                self.Log('info', f'write byte test to chip: {chip}, unpaged cmd {hex(cmd)} is successful'
                                 f'observed: {hex(observed_data)} == expected {hex(data)}')

            # hbidps.write_ltm4680(chip, 0xD1, hbidps.hc_rail_pmbus.val_to_reg(0x21))
            hbidps.detect_global_alarm()

            ltm_4680_run = hbidps.read_bar_register(hbidps_register.LTM4680_RUN)
            ltm_4680_on_of_config = hbidps.read_hc_reg_command(0x02, 1, chip)

            if ltm_4680_run.run == 0 and ltm_4680_on_of_config == 0x1F:
                rail = chip * 2 + page
                cmd = 0x01
                data = random.choice([0xA8, 0x98, 0x80])
                observed_data = hbidps.read_hc_reg_command(0x00, 1, chip)
                self.Log('info', f'before tq, page is: {hex(observed_data)}')
                self.write_hc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_byte)

                # hbidps.write_ltm4680(chip, cmd, hbidps.hc_rail_pmbus.val_to_reg(data), page)
                observed_data = hbidps.read_hc_reg_command(0x00, 1, chip)
                self.Log('info', f'after tq, page is: {hex(observed_data)}')
                for chip_check in range(5):
                    for page_check in range(2):
                        rail_check = chip_check * 2 + page_check
                        if rail == rail_check:
                            expected_data = data
                        else:
                            expected_data = 0
                        observed_data = hbidps.read_hc_reg_command(cmd, 1, chip_check, page_check)
                        if observed_data != expected_data:
                            self.Log('error', f'Chip {chip_check}, Page {page_check}: '
                                              f'observed paged byte CMD {hex(cmd)}: observed: '
                                              f'{hex(observed_data)} != expected {hex(expected_data)}')
                        else:
                            self.Log('info', f'Chip {chip_check}, Page {page_check} write byte test '
                                             f'to cmd {hex(cmd)} is successful observed: {hex(observed_data)} '
                                             f'== expected {hex(expected_data)}')
            else:
                self.Log('error', f'RUN Pins not all off or on_off_config not using control pins: '
                                  f'Contrl PINs: {bin(ltm_4680_run.rw_ctrl)}, '
                                  f'On_Off_Config: {bin(ltm_4680_on_of_config)}')
            hbidps.write_ltm4680(chip, 0x01, hbidps.hc_rail_pmbus.val_to_reg(0x00))
            observed_data = hbidps.read_hc_reg_command(0x00, 1, chip)
            self.Log('info', f'in the end, page is: {hex(observed_data)}')

    def send_hc_byte_test(self, hbidps, chip, page):
        with HCRailsCmdDirectInjectEnv(hbidps):
            rail = chip * 2 + page

            hbidps.enable_hc_run_pins()
            hbidps.alarm.update_global_alarm_mask(0, 0, 1)

            hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
            tq_list = self.create_hc_1rail_off_to_on_over_voltage_tq_list(rail=rail, vout=2)
            hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
            hbidps.clear_tq_notify_bits()
            time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)

            time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
            time.sleep(0.5)

            status_byte = hbidps.read_hc_reg_command(0x78, 1, chip, page)
            if status_byte & 0b100000 == 0:
                self.Log('error', f'ov fault didn\'t happen, status_byte: {bin(status_byte)}!')

            self.write_hc_cmd_using_tq(0x03, 0, hbidps, rail, tq_cmd.send_byte)

            status_byte = hbidps.read_hc_reg_command(0x78, 1, chip, page)
            if status_byte & 0b100000:
                self.Log('error', f'ov fault not cleared, status_byte: {hex(status_byte)}')
            else:
                self.Log('info', f'ov fault successfully cleared, status_byte: {hex(status_byte)}')

            hbidps.alarm.update_global_alarm_mask(0, 0, 0)

    def write_lc_word_test(self, hbidps, chip, page):
        self.Log('info', f'{"*"*10}HBIDPS {hbidps.slot}: Chip {chip}, Page{page}: write_word test{"*"*10}')
        with LCRailsCmdDirectInjectEnv(hbidps, self):
            rail = chip * 4 + page
            cmd = 0xb3
            data = random.getrandbits(16)
            self.write_lc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_word)
            observed_data = hbidps.get_lc_user_data_03(chip, page)
            if observed_data != data:
                self.Log('error', f'observed user data at CMD{hex(cmd)}: '
                                  f'observed: {hex(observed_data)} != expected {hex(data)}')
            else:
                self.Log('info', f'Chip {chip}, Page {page} write word test to cmd {hex(cmd)} is successful'
                                 f'observed: {hex(observed_data)} == expected {hex(data)}')

            rail = chip * 4
            cmd = 0xb4
            data = random.getrandbits(16)
            self.write_lc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_word)
            observed_data = hbidps.get_lc_user_data_04(chip)
            if observed_data != data:
                self.Log('error', f'observed user data at CMD{hex(cmd)}: '
                                  f'observed: {hex(observed_data)} != expected {hex(data)}')
            else:
                self.Log('info', f'Chip {chip}, Unpaged write word test to cmd {hex(cmd)} is successful'
                                 f'observed: {hex(observed_data)} == expected {hex(data)}')

    def write_lc_byte_test(self, hbidps, chip, page):
        self.Log('info', f'{"*" * 10}HBIDPS {hbidps.slot}: Chip {chip}, Page{page}: write_byte test{"*" * 10}')
        with LCRailsCmdDirectInjectEnv(hbidps, self):
            rail = chip * 4
            cmd = 0xf7 # MFR_RETRY_COUNT cmd
            data = random.getrandbits(2)
            self.write_lc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_byte)
            observed_data = hbidps.read_lc_reg_command(cmd, 1, chip)
            if observed_data != data:
                self.Log('error', f'observed unpaged byte CMD {hex(cmd)}: '
                                  f'observed: {hex(observed_data)} != expected {hex(data)}')
            else:
                self.Log('info', f'write byte test to chip: {chip}, unpaged cmd {hex(cmd)} is successful'
                                 f'observed: {hex(observed_data)} == expected {hex(data)}')
            ltc_2975_ctrl = hbidps.read_bar_register(hbidps_register.LTC2975_CTRL_REGISTER)
            ltc_on_of_config = hbidps.read_lc_reg_command(0x02, 1, chip)
            if ltc_2975_ctrl.rw_ctrl == 0 and ltc_on_of_config & 0b10000:
                rail = chip * 4 + page
                cmd = 0x01 #OPERATION cmd
                data7_6 = random.choice([0b00, 0b10, 0b01])
                data5_4 = random.choice([0b00, 0b01, 0b10])
                data3_2 = random.choice([0b01, 0b10])
                data1_0 = 0
                data = (data7_6 << 6) | (data5_4 << 4) | (data3_2 << 2)
                self.write_lc_cmd_using_tq(cmd, data, hbidps, rail, tq_cmd.write_byte)
                observed_data = hbidps.read_lc_reg_command(cmd, 1, chip, page)
                if observed_data != data:
                    self.Log('error', f'Chip {chip}, Page {page}: observed paged byte CMD {hex(cmd)}: '
                                      f'observed: {hex(observed_data)} != expected {hex(data)}')
                else:
                    self.Log('info', f'Chip {chip}, Page {page} write byte test to cmd {hex(cmd)} is successful'
                                     f'observed: {hex(observed_data)} == expected {hex(data)}')

            else:
                self.Log('error', f'Control Pins not all off or on_off_config not using control pins: '
                                  f'Contrl PINs: {bin(ltc_2975_ctrl.rw_ctrl)}, '
                                  f'On_Off_Config: {bin(ltc_on_of_config)}')

    def send_lc_byte_test(self, hbidps, chip, page):
        self.Log('info', f'{"*" * 10}HBIDPS {hbidps.slot}: Chip {chip}, Page{page}: send_byte test{"*" * 10}')
        with LCRailsCmdDirectInjectEnv(hbidps, self):
            rail = chip * 4 + page

            hbidps.enable_lc_rails_ctrl()
            hbidps.alarm.update_global_alarm_mask(0, 1, 0)
            self.execute_lc_one_tq_transition_with_ov_fault(hbidps, rail, None, 3.0)

            status_byte = hbidps.read_lc_reg_command(0x78, 1, chip, page)
            print(hex(status_byte))
            if status_byte & 0b100000 == 0:
                self.Log('error', f'ov fault didn\'t happen, status_byte: {bin(status_byte)}!')

            self.write_lc_cmd_using_tq(0x03, 0, hbidps, rail, tq_cmd.send_byte)

            status_byte = hbidps.read_lc_reg_command(0x78, 1, chip, page)
            if status_byte & 0b100000:
                self.Log('error', f'ov fault not cleared, status_byte: {hex(status_byte)}')
            else:
                self.Log('info', f'ov fault successfully cleared, status_byte: {hex(status_byte)}')

            hbidps.alarm.update_global_alarm_mask(0, 0, 0)

    def write_hc_cmd_using_tq(self, cmd, data, hbidps, rail, tq_command):
        print(f'rail is {rail}')
        hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
        hc_one_hot = 1 << rail
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_generator = TriggerQueueCommands()
        tq_generator.create_tq_list()

        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_command, hc_rail, (cmd << 16) | data)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list(True)
        tq_generator.delete_tq_list()
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=0, hc=1, vt=0, time_start=time_start)
        time.sleep(0.5)

    def create_hc_1rail_off_to_on_over_voltage_tq_list(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <=9:
            hc_one_hot = 1 << rail
            hc_rail = tq_dev(HC_TQ_RAIL_ID_BASE + rail)
        if vout == None:
            vout_on = 0
            pwr_state = 0b00
            ov = uv = 0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            ov = uv = 0
        elif 0.5 <= vout <= 3:
            vout_on = vout
            pwr_state = 0b01
            ov = vout - 1
            uv = 0.5
        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(HC_TQ_I_HI)

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_command(tq_cmd.set_vrange, hc_rail, (0xFABC+(1<<19)))
        tq_generator.add_command(tq_cmd.set_ov_limit, hc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, hc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, hc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, hc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, hc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, hc_rail, HC_TQ_FDT)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, hc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.apply_user_attributes, hc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.HCBroadcast, hc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list


    def execute_lc_one_tq_transition_with_ov_fault(self, hbidps, rail, vout_initial, vout):
        self.Log('info', f'execute transition: vout_initial = {vout_initial}V; vout = {vout}V')
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_list = self.create_lc_1rail_ov_tq_list(rail=rail, vout=vout)
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        observed_power_state = (hbidps.get_lc_power_state().value >> (rail * 2)) & 0b11
        _, initial_pwr_state, _, _, _ = self.get_lc_overvoltage_vout_settings_and_power_state(vout_initial)
        if observed_power_state != initial_pwr_state:
            self.Log('error', f'lc rail {rail} initial power state{bin(observed_power_state)}'
                              f'!= expected: {bin(initial_pwr_state)}')
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
        time.sleep(0.5)

    def get_lc_overvoltage_vout_settings_and_power_state(self, vout):
        if vout is None:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 6.0
            oc = 2.0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            uv = 0
            ov = 0.5
            oc = 1.5
        elif 0.6 <= vout <= 5:
            vout_on = vout
            pwr_state = 0b01
            ov = vout - 1
            uv = 0.6
            oc = 3.0
        else:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 7.0
            oc = 0.5
            self.Log('error', f'vout: {vout}V is not a valid value for lc rail')
        return ov, pwr_state, uv, vout_on, oc

    def create_lc_1rail_ov_tq_list(self, rail, vout):
        tq_generator = TriggerQueueCommands()
        if 0 <= rail <= 15:
            lc_one_hot = 1 << rail
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
        else:
            lc_one_hot = 0
            lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE)
            self.Log('error', f'rail number {lc_rail} given to lc trigger queue is not valid')
        ov, pwr_state, uv, vout_on, oc = self.get_lc_vout_settings_and_power_state(vout)
        ov = vout-1
        ov_hex = tq_generator.float_to_hex(ov)
        uv_hex = tq_generator.float_to_hex(uv)
        vout_hex = tq_generator.float_to_hex(vout_on)
        fdi_hi_hex = tq_generator.float_to_hex(LC_TQ_I_HI)
        i_hi_hex = tq_generator.float_to_hex(oc)
        self.Log('info', f'ov: {ov}, uv: {uv}, vout: {vout_on}, fdi_hi: {LC_TQ_I_HI}, i_hi_hex: {oc}, pwr_state: {pwr_state}')

        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_ov_limit, lc_rail, ov_hex)
        tq_generator.add_command(tq_cmd.set_uv_limit, lc_rail, uv_hex)
        tq_generator.add_command(tq_cmd.set_v, lc_rail, vout_hex)
        tq_generator.add_command(tq_cmd.set_fd_current_hi, lc_rail, fdi_hi_hex)
        tq_generator.add_command(tq_cmd.set_pwr_state, lc_rail, pwr_state)
        tq_generator.add_command(tq_cmd.set_fdt, lc_rail, 0x01000)
        tq_generator.add_command(tq_cmd.set_hi_i_limit, lc_rail, i_hi_hex)
        tq_generator.add_command(tq_cmd.apply_user_attributes, lc_rail, 0)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        return tq_list

    def write_lc_cmd_using_tq(self, cmd, data, hbidps, rail, tq_command):
        lc_rail = tq_dev(LC_TQ_RAIL_ID_BASE + rail)
        lc_one_hot = 1 << rail
        hbidps.set_uhc_dut_map(uhc_id=0, dut_id=1, valid=1)
        tq_generator = TriggerQueueCommands()
        tq_generator.create_tq_list()
        tq_generator.add_command(tq_cmd.wake, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_cmd.set_busy, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_command(tq_command, lc_rail, (cmd << 16) | data)
        tq_generator.add_command(tq_cmd.tq_notify, tq_dev.LCBroadcast, lc_one_hot)
        tq_generator.add_end_command()
        tq_list = tq_generator.get_tq_list()
        tq_generator.delete_tq_list()
        hbidps.store_dps_tq_list_and_lut_in_memory(tq_list=tq_list, dps_uhc=0, index=0)
        hbidps.clear_tq_notify_bits()
        time_start = hbidps.send_trigger_from_rctc_to_dps(card_type=0x01, dut_id=1, index=0)
        time_delta = hbidps.wait_for_tq_notify_bits(lc=1, hc=0, vt=0, time_start=time_start)
        time.sleep(0.5)

    def get_lc_vout_settings_and_power_state(self, vout):
        if vout is None:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 6.0
            oc = 2.0
        elif vout == 0:
            vout_on = 0
            pwr_state = 0b10
            uv = 0
            ov = 0.5
            oc = 1.5
        elif 0.6 <= vout <= 5:
            vout_on = vout
            pwr_state = 0b01
            ov = vout + 1
            uv = 0.6
            oc = 3.0
        else:
            vout_on = 0.0
            pwr_state = 0b00
            uv = 0.0
            ov = 7.0
            oc = 0.5
            self.Log('error', f'vout: {vout}V is not a valid value for lc rail')
        return ov, pwr_state, uv, vout_on, oc

    def init_limits(self):
        self.previous_ov = 7
        self.previous_uv = 0
        self.previous_oc = 4

class LCRailsCmdDirectInjectEnv(object):
    def __init__(self, hbidps, test):
        self.hbidps = hbidps
        self.test = test

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_lc_rails_start_state()
        self.test.init_limits()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.set_lc_rails_to_safe_state()
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)


class HCRailsCmdDirectInjectEnv(object):
    def __init__(self, hbidps):
        self.hbidps = hbidps

    def __enter__(self):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.check_init_hc_rails_start_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)

    def __exit__(self, exc_type, exc_value, traceback):
        self.hbidps.detect_global_alarm()
        self.hbidps.reset_global_alarm()
        self.hbidps.set_hc_rails_to_safe_state()
        self.hbidps.initialize_dtb_hc_lc_mux()
        self.hbidps.preset_hc_lc_vt_uhc()
        time.sleep(0.2)