################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
"""Testing of ADT7411 Voltage Monitor"""
import collections
import math
import os
import random
import time


from Hbidps.Tests.HbidpsTest import HbidpsTest
MAX_FAIL_COUNT = 10

adt7411_channel_number = collections.namedtuple('channel_number', ['channel'])
adt7411_channel_name_value_combination = collections.namedtuple('channel_name_value_combination', ['channel_name', 'default_value'])
adt7411_default_values_collection = {adt7411_channel_number(channel=0, ): adt7411_channel_name_value_combination(channel_name='V5P0_SYS', default_value=4.9013671875),
                                    adt7411_channel_number(channel=1, ): adt7411_channel_name_value_combination(channel_name='TEMP_LTM4644_V95_18', default_value=0.5944921875),
                                    adt7411_channel_number(channel=2, ): adt7411_channel_name_value_combination(channel_name='V2P5_DDR4_DF', default_value=2.5071093749999998),
                                    adt7411_channel_number(channel=3, ): adt7411_channel_name_value_combination(channel_name='V3P3_ISNS_DF', default_value=3.317578125),
                                    adt7411_channel_number(channel=4, ): adt7411_channel_name_value_combination(channel_name='V5P0_SYS_DF', default_value=12.336046875),
                                    adt7411_channel_number(channel=5, ): adt7411_channel_name_value_combination(channel_name='V3P15_DPS_DF', default_value=3.16171875),
                                    adt7411_channel_number(channel=6, ): adt7411_channel_name_value_combination(channel_name='V1P8_DPS_DF', default_value=1.80796875),
                                    adt7411_channel_number(channel=7, ): adt7411_channel_name_value_combination(channel_name='V1P2_DPS_DF', default_value=1.1956640625),
                                    adt7411_channel_number(channel=8, ): adt7411_channel_name_value_combination(channel_name='P95_DPS_DF', default_value=0.9507421875)}


class Diagnostics(HbidpsTest):
    """Test ADT7411 Voltages at multiple channels"""
    test_iteration_count = 10000
    vmon_test_iteration_count = 50
    expected_pass_count = 450
    temperature_expected_pass_count = 600

    def DirectedADT7411VoltageReadTest(self):
        """Read Voltage measurements from adt7411 900 times and make sure they don't deviate more than 5%"""
        for hbidps in self.env.get_fpgas():
            hbidps.print_slot_under_test()
            fail_count = pass_count = 0
            hbidps.initialize_hbi_dps()
            for iteration in range(self.vmon_test_iteration_count):
                for channel_number in range(9):
                    vmon_read = hbidps.read_v_mon(channel_number)
                    channel_name,default_value = adt7411_default_values_collection[(channel_number,)]
                    if math.isclose(vmon_read,default_value,rel_tol=0.05):
                        pass_count += 1
                    else:
                        fail_count += 1
                        self.Log('error', 'At iteration {} , reading for ADT7411 for channel {} deviated from dafault value:{} actual value:{}.'.format(
                            iteration, channel_name ,default_value, vmon_read))
                    if fail_count == MAX_FAIL_COUNT:
                        break
            if pass_count == self.expected_pass_count :
                self.Log('info', 'ADT7411 read was successful for iterations: {}'.format(pass_count))
            else:
                self.Log('error', 'ADT7411 read was successful for only iterations {} out of {}'.format(pass_count, self.expected_pass_count))