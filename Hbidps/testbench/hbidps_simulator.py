# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from enum import IntEnum

from Common.fval import Object
from Common import hilmon as hil


class Hbidps(Object):
    def __init__(self, slot):
        self._slot = slot
        self._registers = {}
        self._setup_registers()
        self._monitor_temperatures = None
        self._ram = b''

    def _setup_registers(self):
        self._setup_temperature_registers()
        self._read_actions = {0x700: self._global_alarms,
                              0x710: self._temperature_alarms}

    def _setup_temperature_registers(self):
        self._registers[0x0A0] = 0x00000FFF
        self._registers[0x0A4] = 0x00000FFF
        self._registers[0x0A8] = 0x00000FFF
        self._registers[0x0AC] = 0x00000FFF
        self._registers[0x0B0] = 0x00000000
        self._registers[0x0B4] = 0x00000000
        self._registers[0x0B8] = 0x00000000
        self._registers[0x0BC] = 0x00000000
        self._registers[0x0C0] = 100
        self._registers[0x0C4] = 100
        self._registers[0x0C8] = 100
        self._registers[0x0CC] = 100

    def read_blt(self):
        blt = hil.BLT()
        return blt

    def bar_read(self, bar, offset):
        if bar == 1:
            data = self._read_actions.get(offset, self._default_read_action)(offset)
            # print(f'DPS_{self._slot} RD bar={bar} offset={offset:03X} data={data:08X}')
            return data
        else:
            self.Log('error', f'Unexpected access to BAR {bar}')

    def _default_read_action(self, offset):
        return self._registers.get(offset, 0x00000000)

    def bar_write(self, bar, offset, data):
        self._registers[offset] = data
        # print(f'DPS_{self._slot} WR bar={bar} offset={offset:03X} data={data:08X}')

    def dma_write(self, address, data):
        self._ram = data

    def dma_read(self, address, length):
        return self._ram

    def lcm_gang_max14662_write(self, chip, data):
        pass

    def ltc2975_write(self, chip, page, command, data):
        self._registers[0x22C] = 0xA

    def ltc2975_read(self, chip, page, command, length):
        return b'\x00'*length

    def _global_alarms(self, offset):
        alarms = 0
        alarms |= self._temperature_alarm_status() << 6
        return alarms

    def _temperature_alarm_status(self):
        return int(self._temperature_alarms() != 0)

    def _temperature_alarms(self, offset=None):
        lo_temp = 0
        hi_temp = 0
        for i in range(4):
            lo_temp |= (self._max6627_temperature(i) < self._max6627_lower_temperature_limit(i)) << i
            hi_temp |= (self._max6627_temperature(i) > self._max6627_upper_temperature_limit(i)) << i
        return (hi_temp << 4) | lo_temp

    def _max6627_upper_temperature_limit(self, index):
        return self._registers[0x0A0 + 4*index] & 0xFFF

    def _max6627_lower_temperature_limit(self, index):
        return self._registers[0x0B0 + 4*index] & 0xFFF

    def _max6627_temperature(self, index):
        return self._registers[0x0C0 + 4*index] & 0xFFF

    def voltage_monitor_read(self, channel):
        return 0

    def temperature_monitor_read(self, channel):
        if self._monitor_temperatures is None:
            self._initialize_monitor_temps()
        return self._monitor_temperatures.get(channel, 0)

    def _initialize_monitor_temps(self):
        self._monitor_temperatures = {TemperatureMonitorChannels.ADT7411_U15: 35,
                                      TemperatureMonitorChannels.LTM4644_U12: 35,
                                      TemperatureMonitorChannels.MAX_6627_U32: 28,
                                      TemperatureMonitorChannels.MAX_6627_U33: 33,
                                      TemperatureMonitorChannels.MAX_6627_U34: 27,
                                      TemperatureMonitorChannels.MAX_6627_U35: 26,
                                      TemperatureMonitorChannels.FPGA_U63: 21}

    def receive_sync_pulse(self):
        pass


class TemperatureMonitorChannels(IntEnum):
    ADT7411_U15 = 0
    LTM4644_U12 = 1
    MAX_6627_U32 = 2
    MAX_6627_U33 = 3
    MAX_6627_U34 = 4
    MAX_6627_U35 = 5
    FPGA_U63 = 6
