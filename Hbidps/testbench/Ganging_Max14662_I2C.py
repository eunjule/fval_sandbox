# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.instruments.i2c_test_interface import I2cTestInterface

from Hbidps.instrument.hbidps_register import\
    MAX14662_01_GANG_LTC2975_I2C_STATUS_REG,\
    MAX14662_01_GANG_LTC2975_I2C_CONTROL_REG,\
    MAX14662_01_GANG_LTC2975_I2C_TX_FIFO_DATA_REG,\
    MAX14662_01_GANG_LTC2975_I2C_RX_FIFO_DATA_REG
from Hbidps.instrument.hbidps_register import\
    MAX14662_23_GANG_LTC2975_I2C_STATUS_REG,\
    MAX14662_23_GANG_LTC2975_I2C_CONTROL_REG,\
    MAX14662_23_GANG_LTC2975_I2C_TX_FIFO_DATA_REG,\
    MAX14662_23_GANG_LTC2975_I2C_RX_FIFO_DATA_REG


MAX14662_CHIP0_ADDR = 0x98
MAX14662_CHIP1_ADDR = 0x9A
MAX14662_CHIP2_ADDR = 0x9C
MAX14662_CHIP3_ADDR = 0x9E
MAX14662_DUMMY_WRITE_REG_ADDR = 0x00
MAX14662_LCM_00_01_GANG_I2C_BASE_REG = 0x0150
MAX14662_LCM_02_03_GANG_I2C_BASE_REG = 0x0180
MAX_GANG_CHIPS = 8
NUM_CHIPS_PER_CONTROLLER = 4

I2C_ADDR = [MAX14662_CHIP0_ADDR,
            MAX14662_CHIP1_ADDR,
            MAX14662_CHIP2_ADDR,
            MAX14662_CHIP3_ADDR]  # If read, I2C_ADDR | 1
REG_ADDR = 0


class GangMax14662I2cInterface(I2cTestInterface):

    COMM_STRESS_REGISTER =0x00
    UNUSED_I2C_ADDR = 0x20
    SHUTDOWN_NAME = 'gang'
    NUM_CHIPS_PER_SHUTDOWN_CONTROL = 2

    def __init__(self, instrument, chip_num):
        self.chip_num = chip_num

        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR[self.chip_num % NUM_CHIPS_PER_CONTROLLER],
                         interface_name=f'Ganging 14662 Chip {self.chip_num}',
                         interface_index=0)

        self.update_register_table(self.select_register_table(self.chip_num))

    def select_register_table(self, chip_num):
        if self.chip_num < NUM_CHIPS_PER_CONTROLLER:
            return {'STATUS': MAX14662_01_GANG_LTC2975_I2C_STATUS_REG,
                     'CONTROL': MAX14662_01_GANG_LTC2975_I2C_CONTROL_REG,
                     'TX_FIFO': MAX14662_01_GANG_LTC2975_I2C_TX_FIFO_DATA_REG,
                     'RX_FIFO': MAX14662_01_GANG_LTC2975_I2C_RX_FIFO_DATA_REG,
                     'RESET': None}
        else:
            return {'STATUS': MAX14662_23_GANG_LTC2975_I2C_STATUS_REG,
                     'CONTROL': MAX14662_23_GANG_LTC2975_I2C_CONTROL_REG,
                     'TX_FIFO': MAX14662_23_GANG_LTC2975_I2C_TX_FIFO_DATA_REG,
                     'RX_FIFO': MAX14662_23_GANG_LTC2975_I2C_RX_FIFO_DATA_REG,
                     'RESET': None}

    def reset(self):
        pass
