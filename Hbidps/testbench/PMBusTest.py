# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time

from Common.fval import Object
from Hbidps.instrument.hbidps_register import\
    PMBUS_TX_FIFO,\
    PMBUS_RX_FIFO,\
    PMBUS_CONTROL_STATUS

register_table_template = {'CONTROL_STATUS': None,
                           'TX_FIFO': None,
                           'RX_FIFO': None}


class PMBUSTestInterface(Object):
    PMBUS_STATUS_TIMEOUT = 60000
    FIFO_COUNT_BIT_SIZE = 10

    def __init__(self, instrument, pmbus_addr, interface_name, device_num,comm_stress_reg_word,comm_stress_reg_byte):
        super().__init__()

        self.instrument = instrument
        self.register_table = {'CONTROL_STATUS': PMBUS_CONTROL_STATUS,
                     'TX_FIFO': PMBUS_TX_FIFO,
                     'RX_FIFO': PMBUS_RX_FIFO}
        self.interface_name = interface_name
        self.pmbus_addr = pmbus_addr
        self.device = device_num
        self.comm_stress_reg_word = comm_stress_reg_word
        self.comm_stress_reg_byte = comm_stress_reg_byte
        self.index = 0

    def read_control_status_register(self):
        return self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index)

    def get_transmit_fifo_count(self):
        return self.read_control_status_register().tx_fifo_count

    def log_control_status_register(self):
        reg = self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index)
        self.Log('info', f'Control Status register for interface {self.index}:\n\
                    rx_fifo_count          : {reg.rx_fifo_count}\n\
                    tx_fifo_count          : {reg.tx_fifo_count}\n\
                    reset                  : {reg.reset}\n\
                    go                     : {reg.go}\n\
                    busy                   : {reg.busy}')

    def log_tx_fifo_register(self):
        reg = self.instrument.read_bar_register(self.register_table['TX_FIFO'], self.index)
        self.Log('info', f'TX FIFO register for interface {self.index}:\n\
                    tx_data                 : {reg.tx_data}\n\
                    pm_bus_transaction      : {reg.pm_bus_transaction}\n\
                    command_code            : {reg.command_code}\n\
                    target_device_number    : {reg.target_device_number}')

    def log_rx_fifo_register(self):
        reg = self.instrument.read_bar_register(self.register_table['RX_FIFO'], self.index)
        self.Log('info', f'RX FIFO register for interface {self.index}:\n\
                    rx_data                 : {reg.rx_data}\n\
                    retries                 : {reg.retries}\n\
                    error                   : {reg.error}')

    def reset(self):
        reg = self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index)
        reg.reset = 1
        self.instrument.write_bar_register(reg, self.index)

    def wait_pmbus_busy(self):
        for timeout in range(PMBUSTestInterface.PMBUS_STATUS_TIMEOUT):
            reg = self.read_control_status_register()
            if not reg.busy and not reg.tx_fifo_count:
                break
        else:
            self.log_control_status_register()
            self.log_tx_fifo_register()
            self.log_rx_fifo_register()
            raise PMBUSTestInterface.PMBUSTestInterfaceError('PMBus({self.interface_name}, {self.index}): ' +
                                                                f'Timed out waiting on busy signal to de-assert.')

    def set_transmit_bit(self, go=1):
        reg = self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index)
        reg.go = go
        self.instrument.write_bar_register(reg, self.index)

    def set_tx_fifo(self, address, transaction, data=None):
        reg = self.instrument.read_bar_register(self.register_table['TX_FIFO'], self.index)
        reg.pm_bus_transaction = transaction
        reg.target_device_number = self.device
        reg.command_code = address
        if data:
            reg.tx_data = data
        self.instrument.write_bar_register(reg, self.index)

    def pop_data_fifo(self):
        return self.instrument.read_bar_register(self.register_table['RX_FIFO'], self.index).rx_data

    def write(self, address, pmbus_transaction, data=None, go=True):
        if isinstance(data, int):
            data = [data]
        if data:
            for data_item in data:
                self.set_tx_fifo(address, pmbus_transaction, data_item)
        else:
            self.set_tx_fifo(address,pmbus_transaction)
        if go:
            self.set_transmit_bit(go)
            self.wait_pmbus_busy()

    def read(self, address, pmbus_transaction, num_transactions=1):
        self.set_tx_fifo(address, pmbus_transaction)
        self.set_transmit_bit(go=1)
        self.wait_pmbus_busy()
        num_transactions_read = self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index).rx_fifo_count
        if num_transactions_read != num_transactions:
            self.log_control_status_register()
            self.log_rx_fifo_register()
            self.log_tx_fifo_register()
            raise PMBUSTestInterface.PMBUSTestInterfaceError(f'Invalid number of bytes read (expected, actual): '
                                                                f'{num_transactions}, {num_transactions_read}')
        data = [self.pop_data_fifo() for i in range(num_transactions_read)]
        if num_transactions_read == 1:
            return data[0]
        else:
            return data

    def read_no_busy_check(self, address, pmbus_transaction):
        self.reset()
        self.wait_pmbus_busy()
        self.set_tx_fifo(address, pmbus_transaction)
        self.set_transmit_bit(go=1)

    def check_error_retry_bit(self):
        reg = self.instrument.read_bar_register(self.register_table['RX_FIFO'], self.index)
        if reg.retries == 100:
            if reg.error == 1:
                return True
            else:
                self.Log('info', f'Failed error register check . Error register field not set.')
                return False
        else:
            self.Log('info', f'Failed retry register check . Retry register field not 100.')
            return False

    def check_busy(self):
        reg = self.read_control_status_register()
        if reg.busy == 1:
            return True
        else:
            self.Log('info', f'Failed busy register check . Busy register not set.')
            return False

    def get_tx_fifo_count(self):
        reg = self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index)
        return reg.tx_fifo_count

    def get_rx_fifo_count(self):
        reg = self.instrument.read_bar_register(self.register_table['CONTROL_STATUS'], self.index)
        return reg.rx_fifo_count


    class PMBUSTestInterfaceError(Exception):
        pass
