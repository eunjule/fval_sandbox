# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
import struct

from Common.fval import Object
from Hbidps.instrument.TQMachine import device, command

class TriggerQueueCommands(Object):
    """ Helper Class for HBI DPS TQ Command List Generation

        Usage Example:
        create_tq_list()
        add_command()
        add_end_command()
        get_tq_list()
        delete_tq_list()
    """

    def __init__(self):
        super().__init__()

        self.tq_list = None

    def create_tq_list(self):
        self.tq_list = []

    def delete_tq_list(self):
        self.tq_list = None

    def get_tq_list(self, log=False):
        if log:
            self.log_tq_list()
        return self.tq_list

    def log_tq_list(self):
        self.Log('info', 'tq log start')
        list_len = len(self.tq_list)
        for i in range(list_len):
            self.Log('info', f'{hex(self.tq_list[i])} line {i}')
        self.Log('info', 'tq log end')

    def add_raw_command(self, command, device, payload):
        tq_command = self.assemble_command(command, device, payload)
        self.tq_list.append(tq_command)

    def add_command(self, command, device, payload):
        tq_command = self.assemble_command(command.value, device.value, payload)
        self.tq_list.append(tq_command)

    def add_end_command(self):
        tq_command = self.assemble_command(command.tq_end.value, device.NONE.value, 0)
        self.tq_list.append(tq_command)

    def assemble_command(self, command, device, payload, wr_wd_n=1):
        tq_command = (wr_wd_n << 63) + (command << 48) + (device << 40) + payload
        return tq_command

    @staticmethod
    def float_to_hex(value):
        return int.from_bytes(struct.pack('<f', value), 'little')
