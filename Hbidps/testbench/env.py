# Copyright 2018 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.


from Common import fval
from Common.instruments.tester import get_tester


class Env(fval.Env):
    def __init__(self, test):
        super().__init__(test)
        self.Log('debug', 'Creating new Hbidps test environment instance')
        self.duts = self.get_fpgas()

    def under_test_hbidps_devices(self):
        return self.tester().get_undertest_hbidps_instruments()

    def get_fpgas(self):
        hbidps_instrument = self.tester().get_undertest_instruments('Hbidps')
        fpgas = [hbidps for hbidps in hbidps_instrument]
        if len(fpgas) == 0:
            self.Log('error', 'No hbidps cards on tester')
        return fpgas

    def tester(self):
        return get_tester()
