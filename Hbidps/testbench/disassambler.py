# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os

if __name__ == '__main__':
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..', '..')
    sys.path.append(os.path.abspath(repo_root_path))
    
import structs

def print_command(i):
    c = structs.GenericPayloadRailCommand()
    c.from_integer(i)
    print(c)

if __name__ == '__main__':
    print_command(0x80a0800000000200)
    print_command(0x80a1800000000200)
    print_command(0x806429003f800000)
    print_command(0x8063290000000000)
    print_command(0x8066290040800000)
    print_command(0x8062290000000000)
    print_command(0x806500003f800000)
    print_command(0x8061290040e00000)
    print_command(0x8060290040a00000)
    print_command(0x80a2800000000200)
    print_command(0x8070290000000000)
    print_command(0x80302900000186a0)
    print_command(0x80ff290000000000)
    
    












