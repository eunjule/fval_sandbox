################################################################################
# INTEL CONFIDENTIAL - Copyright 2020. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed,
#-------------------------------------------------------------------------------
#    Filename: lc.py
#-------------------------------------------------------------------------------
#     Purpose: HBIDPS Low Current Rail testbench
#-------------------------------------------------------------------------------
#  Created by: Craig Romei
#        Date: 03/05/20
#       Group: HDMT FPGA Validation
################################################################################

import time
import struct
from Hbidps.testbench.assembler import TriggerQueueAssembler


class LCRailTestbench():
    ltc_pages = 4
    ltc_devices = 4
    lc_base = 0x20

    def init_lc_for_test(self, hbidps):
        self.setup_delays(hbidps)
        self._setup_lc_startup_register(hbidps)
        self.init_all_ltc2975(hbidps)
        # hbidps.wake_lc_rails()
        # if not(hbidps.wait_for_trigger(100)):
        #     self.Log('info', "trigger_Busy")
        # time.sleep(0.2)
        self.set_max14662_shutdown_control(hbidps)
        time.sleep(.002)
        # Shutdown LC Rail'
        self.set_LTC_control(hbidps, control=0x00)
        # not defined in HLD - probably disable LC rails'
        self.set_lc_rail_state_off(hbidps)
        # not defined in HLD'
        self.set_ground_control_on(hbidps)
        # LC load disabled'
        self.set_lc_ld_en(hbidps)
        # Turn off LC Ganging'
        self.turn_off_ganging(hbidps)
        # not defined in HLD'
        self.set_ground_control_off(hbidps)

    def _setup_lc_startup_register(self, hbidps):
        register_setting_for_startup = 0x420
        hbidps.write_bar_register(hbidps.registers.MFR_CONFIG_LTC2975(value=register_setting_for_startup))

    def setup_delays(self, hbidps):
        self.set_pmbus_command_delay(hbidps, microseconds=100)
        self.set_channel_off_delay(hbidps, microseconds=1000)
        self.set_pmbus_commit_delay(hbidps, microseconds=1000)
        self.set_relay_disconnect_delay(hbidps, microseconds=3)
        self.set_relay_connect_delay(hbidps, microseconds=1500)

    def set_pmbus_command_delay(self, hbidps, microseconds):
        hbidps.write_bar_register(hbidps.registers.PMBUS_CMD_DELAY(value=microseconds))

    def set_channel_off_delay(self, hbidps, microseconds):
        hbidps.write_bar_register(hbidps.registers.CHAN_OFF_DELAY(value=microseconds))

    def set_pmbus_commit_delay(self, hbidps, microseconds):
        hbidps.write_bar_register(hbidps.registers.PMBUS_COM_DELAY(value=microseconds))

    def turn_off_ganging(self, hbidps):
        hbidps.write_lcm_gang_max14662(chip=1, data=0x55)
        hbidps.write_lcm_gang_max14662(chip=3, data=0x55)
        hbidps.write_lcm_gang_max14662(chip=5, data=0x55)
        hbidps.write_lcm_gang_max14662(chip=7, data=0x55)

    def set_relay_disconnect_delay(self, hbidps, microseconds):
        hbidps.write_bar_register(hbidps.registers.RELAY_DIS_DELAY(value=microseconds))

    def set_relay_connect_delay(self, hbidps, microseconds):
        hbidps.write_bar_register(hbidps.registers.RELAY_CONN_DELAY(value=microseconds))

    def set_lc_rail_state_off(self, hbidps):
        hbidps.write_bar_register(hbidps.registers.LC_RAIL_STATE(value=0x00))

    def set_ground_control_on(self, hbidps):
        hbidps.write_bar_register(hbidps.registers.GROUND_CONTROL_MODE(value=0x1))

    def set_lc_ld_en(self, hbidps):
        hbidps.write_bar_register(hbidps.registers.TPS22976N_ST_LC_LD_EN(value=0x00))

    def set_ground_control_off(self, hbidps):
        ground_control = hbidps.read_bar_register(hbidps.registers.GROUND_CONTROL_MODE)
        ground_control.rw_mode = 0
        hbidps.write_bar_register(ground_control)

    def set_LTC_control(self, hbidps, control):
        ltc_control = hbidps.read_bar_register(hbidps.registers.LTC2975_CTRL_REGISTER)
        ltc_control.rw_ctrl = control
        hbidps.write_bar_register(ltc_control)

    def get_LTC_control(self, hbidps):
        return hbidps.read_bar_register(hbidps.registers.LTC2975_CTRL_REGISTER)


    def set_max14662_shutdown_control(self, hbidps):
        hbidps.write_bar_register(hbidps.registers.MAX14662_SHUTDOWN_CONTROL(value=0))

    def off_on_trigger_queue(self, hbidps, rail_address, rail_number, target_voltage):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        ltc_resource_id = 0x80
        railmask = 1 << rail_number
        rail = rail_address
        six_amps = 0x40c00000
        pt_six_volts = 0x3f19999a
        five_volts = 0x40a00000
        four_volts_l16 = 0xa000
        hbidps.set_ltc2975_ov_warn(rail_number//4, rail_number % 4, four_volts_l16)
        tq_string = f'''
                            WakeRail resourceid ={ltc_resource_id},  railmask ={railmask}
                            SetRailBusy resourceid ={ltc_resource_id}, railmask ={railmask}
                            SetFdt rail={rail}, value = 100000
                            SetFdCurrentHi rail={rail}, value = {six_amps}
                            SetV rail={rail}, value = {target_voltage}
                            SetOVLimit rail={rail}, value = {five_volts}
                            SetHiILimit rail={rail}, value = {six_amps}
                            SetUVLimit rail={rail}, value = {pt_six_volts}
                            WriteWord rail={rail}, value = 0xD380 , pmbuscmd = {hbidps.symbols.PMBUSCOMMAND.VIN_OV_WARN_LIMIT}
                            WriteWord rail={rail}, value = 0x8000 , pmbuscmd = {hbidps.symbols.PMBUSCOMMAND.VIN_UV_WARN_LIMIT}
                            SetPwrState rail={rail}, value = 1
                            ApplyUserAttributes rail={rail}, value = 0
                            TqNotify resourceid ={ltc_resource_id}, railmask ={railmask} 
                            TqComplete rail ={rail}, value = 0
                          '''
        #
        # TimeDelay rail ={rail}, value = 0x186A0

        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)
        return trigger_queue_data

    def init_all_ltc2975(self, hbidps):
        for chip in range(4):
            hbidps.initialize_ltc2975(chip)

    def initialize_dps_dtb(self, hbidps):
        self.init_dtb_for_lc(hbidps)
        hbidps.clear_pmbus_rx_fifo()
        hbidps.clear_alarm_register()

    def init_2975_for_test(self, hbidps, chip, page_num):
        hbidps.init_ltc_device(chip, page_num)
        hbidps.init_paged_ltc_device(chip, page_num)

    def setup_dtb_for_single_rail_lc_test(self, hbidps, chip, rail_under_test):
        dps_to_dtb_max14661_chip_map = {0: 2, 1: 3, 2: 6, 3: 7}
        reg = 0x02 if (chip < 2) else 0x03
        hbidps.hbidtb.write_max14661(dps_to_dtb_max14661_chip_map[hbidps.slot % 4], reg,
                                     1 << ((rail_under_test % 100) % 8))
        # setup DTB connections - need to check for what value
        hbidps.hbidtb.turnOffPinByName("LC_LD_COM_EN_N")
        hbidps.hbidtb.turnOffPinByName(f'LC_FB{hbidps.slot % 4}_SEN_EN_N')
        hbidps.hbidtb.turnOffPinByName(f'LC_FB{hbidps.slot % 4}_LD_EN_N')

    def init_dtb_for_lc(self, hbidps):
        dps_to_dtb_max14661_chip_map = {0: 2, 1: 3, 2: 6, 3: 7}
        hbidps.hbidtb.write_max14661(dps_to_dtb_max14661_chip_map[hbidps.slot % 4], 0x02, 0x0)
        hbidps.hbidtb.write_max14661(dps_to_dtb_max14661_chip_map[hbidps.slot % 4], 0x03, 0x0)
        hbidps.hbidtb.write_max14661(dps_to_dtb_max14661_chip_map[hbidps.slot % 4], 0x01, 0x0)
        hbidps.hbidtb.write_max14661(dps_to_dtb_max14661_chip_map[hbidps.slot % 4], 0x00, 0x0)
        hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays["LC_LD_COM_EN_N"])
        for dut in range(4):
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays[f'LC_FB{dut}_SEN_EN_N'])
            hbidps.hbidtb.turnOnPin(hbidps.hbidtb.relays[f'LC_FB{dut}_LD_EN_N'])

    def shutdown_lc_rails(self, hbidps):
        lc_discharge_time_in_sec = .1
        self.set_LTC_control(hbidps, control=0x00)
        hbidps.turn_off_all_LTC()
        self.set_lc_rail_state_off(hbidps)
        time.sleep(lc_discharge_time_in_sec)

    def ltc2975_float_to_l16(self, number_to_convert):
        return int(number_to_convert//(2 ** - 13))

    def on_off_trigger_queue(self, hbidps, random_lc_rail, rail_under_test):
        trigger_queue = TriggerQueueAssembler()
        trigger_queue_offset = 0x0
        ltc_resource_id = 0x80
        railmask = 1 << rail_under_test
        rail = random_lc_rail
        four_amps = 0x40800000
        five_amps = 0x40a00000
        seven_volts = 0x40e00000
        tq_string = f'''
                    WakeRail resourceid ={ltc_resource_id},  railmask ={railmask}
                    SetRailBusy resourceid ={ltc_resource_id}, railmask ={railmask}
                    SetPwrState rail={rail}, value = 0
                    SetUVLimit rail={rail}, value = 0
                    SetFdCurrentHi rail={rail}, value = {four_amps}
                    SetFdt rail={rail}, value = 100000
                    SetOVLimit rail={rail}, value = {seven_volts}
                    SetHiILimit rail={rail}, value = {five_amps}
                    TqNotify resourceid ={ltc_resource_id}, railmask ={railmask}
                    TimeDelay rail ={rail}, value = 0x186A0
                    ApplyUserAttributes rail={rail}, value = 0
                    TqComplete rail ={rail}, value = 0
                 '''
        trigger_queue.LoadString(tq_string)
        trigger_queue_data = trigger_queue.CachedObj()
        hbidps.write_trigger_queue_to_memory(trigger_queue_offset, trigger_queue_data)
        hbidps.execute_trigger_queue(trigger_queue_offset)
        return trigger_queue_data

    def turn_on_and_set_voltage(self, hbidps, device, page_num, voltage_in_volts):
        rail_under_test = self.ltc_devices * device + page_num
        rail_address = self.lc_base + rail_under_test
        self.initialize_dps_dtb(hbidps)
        self.setup_dtb_for_single_rail_lc_test(hbidps, device, page_num)
        self.set_LTC_control(hbidps, control=0xff)
        if hbidps.wait_for_ltc2975(device, page_num, 100):
            hbidps.Log('info', 'LTC2975 Busy')
        # run the trigger to set output voltage'
        voltage_in_volts_str = '0x' + str(struct.pack('>f', voltage_in_volts).hex())
        self.off_on_trigger_queue(hbidps, rail_address, rail_under_test, voltage_in_volts_str)
        if not (hbidps.wait_for_trigger(100)):
            hbidps.Log('info', "trigger_Busy")
        time.sleep(0.2)

    def set_all_ltc_ov_limits(self, hbidps, voltage):
        for device in range(4):
            for pages in range(4):
                hbidps.set_ltc2975_ov_limit(device, pages, self.ltc2975_float_to_l16(voltage))
                hbidps.set_ltc2975_ov_warn(device, pages, self.ltc2975_float_to_l16(voltage))

    def get_ltc2975_ov_warn(self, hbidps, device, page):
        return hbidps.get_ltc2975_register(device, page, hbidps.symbols.PMBUSCOMMAND.VOUT_OV_WARN_LIMIT, 2)

    def enable_ltc_ctrl_pin(self, hbidps, chip):
        hbidps.set_ltc2975_internal_control(chip, 0xff, bytes([0x1f]))
