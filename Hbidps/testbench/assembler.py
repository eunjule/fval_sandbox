################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: assembler.py
#-------------------------------------------------------------------------------
#     Purpose: HBIDPS Trigger Queue Assembler
#-------------------------------------------------------------------------------
#  Created by: Gaurav Telang
#        Date: 03/06/19
#       Group: HDMT FPGA Validation
################################################################################

import os
import math
import struct

from ThirdParty import pyparsing

from Hbidps.testbench import structs
from Common import fval
from Hbidps.instrument import symbols

def CreateTriggerQueueAssemblerContext(assembler):
    #context = hddpstbc.TriggerQueueAssembler()
    #context.startaddr = assembler.Resolve('startaddr', False)
    #context.vecaddr = assembler.Resolve('vecaddr', False)
    return None  # context

class TriggerQueueAssembler(fval.asm.Assembler):
    _ENCODED_WORD_SIZE       = 8  # 8 bytes = 64 bits
    _BUILTIN_DIRECTIVES      = ['Q', 'R']
    _BUILTIN_GENERATORS      = ['SequenceBreak']
    _GENERATOR_CONTEXT_MAKER = CreateTriggerQueueAssemblerContext
    LITERAL_CHARACTERS       = pyparsing.Word('01XZLHSKRE')
    _CUSTOM_LITERALS         = None
    _EXTENDED_EVAL_SYMBOLS   = True


    def Q(self, array, offset, cmd, arg = 0x0, data = 0x0, write = 0x1,pmbuscmd = 0x0):
        list_of_pm_bus_commands = ['SEND_BYTE','WRITE_BYTE','WRITE_WORD','READ_BYTE','READ_WORD']
        tq_attributes = TqAttributes()
        tq_attributes.cmd = cmd
        tq_attributes.arg = arg
        tq_attributes.data = data
        tq_attributes.write = write
        tq_attributes.pmbuscmd = pmbuscmd
        if cmd in list_of_pm_bus_commands:
            self.generate_pmbus_payload_command(array,offset,tq_attributes)
        else:
            self.generate_generic_payload_command(array,offset,tq_attributes)

    def generate_generic_payload_command(self,array,offset,tq_attributes):
        tq_rail_command = structs.GenericPayloadRailCommand()
        tq_rail_command.wr_rd_n = self.Resolve(tq_attributes.write)
        tq_rail_command.command = self.Resolve(tq_attributes.cmd)
        tq_rail_command.rail_number = self.Resolve(tq_attributes.arg)
        tq_rail_command.payload = self.Resolve(tq_attributes.data)
        array[offset:offset + 8] = tq_rail_command.value

    def generate_pmbus_payload_command(self,array,offset,tq_attributes):
        pm_bus_tq_rail_command = structs.PmBusPayloadRailCommand()
        pm_bus_tq_rail_command.wr_rd_n = self.Resolve(tq_attributes.write)
        pm_bus_tq_rail_command.pm_bus_transaction = self.Resolve(tq_attributes.cmd)
        pm_bus_tq_rail_command.rail_number = self.Resolve(tq_attributes.arg)
        pm_bus_tq_rail_command.target_specific_register_address = self.Resolve(tq_attributes.pmbuscmd)
        pm_bus_tq_rail_command.payload = self.Resolve(tq_attributes.data)
        array[offset:offset + 8] = pm_bus_tq_rail_command.value

    class SequenceBreak:
        def __init__(self, asm, context, rail, delay = 60150):
            if rail < 0 or rail > 25:
                raise Exception('Rail {} out of range'.format(rail))
            self.asm = asm
            self.rail = rail
            self.delay = delay
        def Size(self):
            return 25
        def Generate(self, array, offset):
            self._words = 0
            self._offset = offset
            for i in range(26):
                if i != self.rail:
                    self.asm.Q(array, self._offset, cmd = self.asm.symbols['TIME_DELAY'], arg = i, data = self.delay)
                    self._Next()
            return self._words
        def _Next(self):
            self._words = self._words + 1
            self._offset = self._offset + self.asm._ENCODED_WORD_SIZE

    def __init__(self, startAddress = 0):
        super(TriggerQueueAssembler, self).__init__(startAddress)
        self.evalSymbols['sfp'] = self._encode_to_sfp
        self.evalSymbols['q3_12'] = self._encode_to_fixed_point_16
        self.evalSymbols['fp'] = self._encode_to_fixed_point_16
        self.evalSymbols['ieee754'] = self._encode_ieee754

    @staticmethod
    def _encode_to_sfp(current_in_amps):
        if int(current_in_amps) not in range(-1024,1024):
            print('error', 'Current out of range. Expected value is between -1024 to 1024')
        sign = 0
        if current_in_amps < 0.0:
            sign = 1
            current_in_amps = - current_in_amps
        fraction = math.frexp(current_in_amps)
        current_in_sfp = 0
        current_in_sfp |= (10 - fraction[1]) & 0x0001f
        current_in_sfp |= (int(fraction[0] * 0x0400) & 0x03ff) << 5
        current_in_sfp |= sign << 15
        return current_in_sfp

    @staticmethod
    def _encode_to_fixed_point_16(value):
        result = TriggerQueueAssembler.calculate_power(value)
        result = int(result * 0xFFFF)
        return result

    @staticmethod
    def _encode_ieee754(value):
        return int.from_bytes(struct.pack('<f', value), byteorder='little')

    @staticmethod
    def calculate_power(value):
        result = 1
        base = 2
        exp = 12
        while exp:
            if exp & 1:
                result *= base
            exp >>= 1
            base *= base
        result = result * value
        return result

    # This method resolves simple expressions, symbols, and literals
    # Supported proto-functions / expressions:
    #   - eval[evalExpr] or `evalExpr`
    #   - LABEL
    #   - startaddr
    #   - curaddr
    def Resolve(self, expr, secondPass = True):
        if isinstance(expr, int) or isinstance(expr, float):
            return expr
        elif isinstance(expr, str):
            if (expr.startswith('eval[') and expr.endswith(']')) or (expr.startswith('`') and expr.endswith('`')):
                evalExpr = None
                if expr.startswith('eval['):
                    evalExpr = expr[5:-1]
                else:
                    evalExpr = expr[1:-1]
                # Build dictionary of symbols that can be used in eval
                self.evalSymbols['startaddr'] = self.Resolve('startaddr')
                if secondPass:
                    if hasattr(self, 'curaddr'):
                        self.evalSymbols['curaddr'] = self.curaddr
                else:
                    self.evalSymbols['curaddr'] = self.sizeStack[-1]
                result = self.Resolve(eval(evalExpr, self.evalSymbols), secondPass)
                return result
            elif expr == 'startaddr':
                return self.startaddr
            elif expr == 'curaddr':
                if secondPass:
                    return self.curaddr
                else:
                    return self.sizeStack[-1]
            elif expr in self.vars:
                return self.Resolve(self.vars[expr], secondPass)
            elif expr in self.symbols:
                return self.Resolve(self.symbols[expr], secondPass)
            elif expr in self.labels:
                return self.Resolve(self.labels[expr], secondPass)
            else:
                # Let the _num() function take care of it. Supported formats:
                #   - Decimal (e.g. 1, 234)
                #   - Binary (e.g. 0b001, 0b000101010100111100)
                #   - Hexadecimal (e.g. 0xAB6969)
                #   - Float numbers
                return TriggerQueueAssembler._num(expr)
        elif hasattr(expr, '__call__'):
            return self.Resolve(expr(), secondPass)
        else:
            raise Exception('Invalid expression \'{}\''.format(expr))

# Load standard symbols
constants, functions = symbols.Load()
standardSymbols = {}
standardSymbols.update(constants)
standardSymbols.update(functions)
TriggerQueueAssembler.classSymbols = standardSymbols

# Load standard macros
macro_file = os.path.abspath(os.path.join(os.path.dirname(__file__), '..','..','Dps','hddpstb','macros.rtq'))
TriggerQueueAssembler.classMacros = fval.asm.LoadMacros(macro_file, TriggerQueueAssembler)

class TqAttributes():
    def __init__(self):
        self.cmd = None
        self.arg = None
        self.data = None
        self.write = None
        self.pmbuscmd = None

if __name__ == "__main__":
    # Parse command line options
    import argparse
    parser = argparse.ArgumentParser(description = 'HBIDPS Trigger Q assembler')
    parser.add_argument('infile', help = 'Source code input filename', type = str)
    parser.add_argument('outfile', help = 'Output filename', type = str)
    parser.add_argument('-s', '--symbols', help = 'Create symbols', action = "store_true")
    args = parser.parse_args()
    
    pattern = TriggerQueueAssembler()
    pattern.Load(args.infile)
    pattern.SaveObj(args.outfile)
    if args.symbols:
        pattern.SaveSymbols(args.outfile + '.symbols')

