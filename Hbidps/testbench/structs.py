################################################################################
#  INTEL CONFIDENTIAL - Copyright 2017. Intel Corporation All Rights Reserved.
################################################################################
import ctypes

from Hbidps.instrument import symbols


class HbidpsStruct(ctypes.LittleEndianStructure):
    """Structure base class - defines setter/getter for bytes value of register"""
    def __init__(self, value=None):
        super().__init__()
        if value is not None:
            self.value = value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return bytes(self)

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i, len(i))
        
    def from_integer(self, i):
        self.value = i.to_bytes(8, byteorder='little')

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        try:
            l.append('ADDR=0x{:04X}'.format(self.ADDR))
        except AttributeError:
            pass
        try:
            l.append('REGCOUNT={}'.format(self.REGCOUNT))
        except AttributeError:
            pass
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class GenericPayloadRailCommand(HbidpsStruct):
    _fields_ = [('payload', ctypes.c_uint64, 40),
                ('rail_number', ctypes.c_uint64, 8),
                ('command', ctypes.c_uint64, 8),
                ('reserved', ctypes.c_uint64, 7),
                ('wr_rd_n', ctypes.c_uint64,  1)]
    
    def __str__(self):
        s = f''
        s += ['READ  ', 'WRITE '][self.wr_rd_n]
        s += f'RAIL 0x{self.rail_number:02X} '
        s += f'CMD {symbols.RAILCOMMANDS(self.command).name} (0x{self.command:02X}) '
        s += f'PAYLOAD 0x{self.payload:010X} ({self.payload}) '
        return s


class PmBusPayloadRailCommand(HbidpsStruct):
    _fields_ = [('payload', ctypes.c_uint64,  16),
                ('target_specific_register_address', ctypes.c_uint64, 8),
                ('reserved', ctypes.c_uint64, 16),
                ('rail_number',  ctypes.c_uint64,  8),
                ('pm_bus_transaction', ctypes.c_uint64,  8),
                ('reserved',ctypes.c_uint64,  7),
                ('wr_rd_n', ctypes.c_uint64,  1)]
