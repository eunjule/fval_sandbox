################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
#
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import random
import time
import os

from Common import hilmon as hil
from Common.instruments.hbi_instrument import HbiInstrument
from Common.cache_blt_info import CachingBltInfo

MAX14661_LC_CHIP_LIST = [2,3,6,7]
MAX14661_HC_CHIP_LIST = [0,1,4,5]

class Hbidtb(HbiInstrument):
    def __init__(self, slot):
        self.slot = slot
        self.blt = CachingBltInfo(callback_function=self.read_blt,
                                  name=f'{self.name()}')

    def name(self):
        return f'HBIDTB{self.slot:02}'

    def discover(self):
        try:
            if hil.hbiDtbTmonRead(self.slot, 0):
                self.Log('info', f'Found {self.name()}')
                return self
        except RuntimeError:
            self.Log('warning', f'Unable to find {self.name()} using hbiDtbTmonRead HIL API.')
            return None

    def read_blt(self):
        return hil.hbiDtbBltBoardRead(self.slot)

    def log_board_blt(self):
        self.blt.write_to_log()

    def turnOnPin(self, relay):
        defaultString = list('0b11111111')
        defaultString[9 - relay[1]] = '0'
        defaultString = "".join(defaultString)  # Inserting 0 into correct index
        # print("chip index{}, bank:{}, bit mask: {}".format(relay[2], relay[0], defaultString))
        pinContentIO = hil.hbiDtbPca9505Read(self.slot, relay[2], relay[0] + 0x18)
        pinContentOutput = hil.hbiDtbPca9505Read(self.slot, relay[2], relay[0] + 0x08)
        # print("before: bits {}, out reg {}".format(hex(pinContentIO), hex(pinContentOutput)))
        newPinContentIO = pinContentIO & int(defaultString, 2)
        newOutput = pinContentOutput & int(defaultString, 2)
        # print("after: bits {}, out reg {}".format(hex(newPinContentIO), hex(newOutput)))
        hil.hbiDtbPca9505Write(self.slot, relay[2], relay[0] + 0x18, newPinContentIO)  # turn ioc into output
        hil.hbiDtbPca9505Write(self.slot, relay[2], relay[0] + 0x08, newOutput)  # turn output low

    def turnOffPin(self, pin):
        defaultString = list('0b00000000')

        defaultString[9 - pin[1]] = '1'
        defaultString = "".join(defaultString)  # Inserting 0 into correct index
        pinContentIO = hil.hbiDtbPca9505Read(self.slot, pin[2], pin[0] + 0x18)
        pinContentOutput = hil.hbiDtbPca9505Read(self.slot, pin[2], pin[0] + 0x08)
        newPinContentIO = pinContentIO | int(defaultString, 2)
        newOutput = pinContentOutput | int(defaultString, 2)
        hil.hbiDtbPca9505Write(self.slot, pin[2], pin[0] + 0x18, newPinContentIO)  # turn ioc into input
        hil.hbiDtbPca9505Write(self.slot, pin[2], pin[0] + 0x08, newOutput)  # turn output high

    def turnOffPinByName(self, pin_name):
        self.turnOffPin(self.relays[pin_name])

    def turnOffAll(self):
        for chip in range(3):
            for bank in range(5):
                hil.hbiDtbPca9505Write(self.slot, chip, bank + 0x18, 0xFF)

    def readAll(self):
        data = []
        for chip in range(3):
            entry = {'Chip': chip}
            for bank in range(5):
                value = hil.hbiDtbPca9505Read(self.slot, chip, bank + 0x18)
                entry.update({'Bank': bank, 'Value': f'0x{value:08X}'})
                entry_copy = entry.copy()
                data.append(entry_copy)
        table = self.contruct_text_table(data=data, title_in=f'{self.name()}: Reading All PCA9505')
        self.Log('info', f'{table}')
        return data

    def log_tmon_read(self, channels=range(5)):
        data = []
        for channel in channels:
            value = hil.hbiDtbTmonRead(self.slot, channel)
            data.append({'Channel': channel, 'Value': value})
        table = self.contruct_text_table(data=data, title_in=f'{self.name()}: Temperature Read of DTB Channel(s)')
        self.Log('info', f'{table}')
        return data

    def log_all_lc_max14661_devices(self):
        for chip in range(4):
            self.read_one_max14661_lc_negative_feedback(chip, True)

    def log_all_hc_max14661_devices(self):
        for chip in range(4):
            self.read_one_max14661_hc_negative_feedback(chip, True)

    def log_all_pca9505_devices(self):
        for chip in range(3):
            self.Log('info', f'device {chip}')
            self.read_one_pca9505_device(chip, True)

    def read_one_pca9505_device(self, chip_id, log=False):
        io_set_list = self.read_one_pca9505_io_set(chip_id)
        input_list = self.read_one_pca9505_input(chip_id)
        if log:
            for bank in range(5):
                self.Log('info', f'bank{bank} io {io_set_list[bank]:08b}')
                self.Log('info', f'bank{bank} in {input_list[bank]:08b}')
        return io_set_list, input_list

    def read_one_pca9505_io_set(self, chip_id):
        io_set_list = []
        for bank in range(5):
            io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
            io_set_list.append(io_set)
        return io_set_list

    def read_one_pca9505_input(self, chip_id):
        input_list = []
        for bank in range(5):
            input = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x00)
            input_list.append(input)
        return input_list

    def reset_all_pca9505_devices(self, log=False):
        for chip_id in range(3):
            self.reset_one_pca9505_device(chip_id, log)

    def reset_one_pca9505_device(self, chip_id, log=False):
        self.reset_one_pca9505_output(chip_id)
        self.reset_one_pca9505_io_set(chip_id)
        self.read_one_pca9505_device(chip_id, log)

    def reset_one_pca9505_io_set(self, chip_id):
        for bank in range(5):
            hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, 0xFF)

    def reset_one_pca9505_output(self, chip_id):
        for bank in range(5):
            hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, 0xFF)

    def write_max14661(self, chip, register, data):
        hil.hbiDtbMax14661Write(self.slot, chip, register, data)

    def read_max14661(self, chip, register):
        return hil.hbiDtbMax14661Read(self.slot, chip, register)

    def read_one_max14661_lc_negative_feedback(self, dut_id, log=False):
        chip_id = MAX14661_LC_CHIP_LIST[dut_id]
        coma = (self.read_max14661(chip_id, 0x01) << 8) + \
               self.read_max14661(chip_id, 0x00)
        comb = (self.read_max14661(chip_id, 0x03) << 8) + \
               self.read_max14661(chip_id, 0x02)
        if log:
            self.Log('info', f'lc dut{dut_id} coma: 0b{coma:016b}')
            self.Log('info', f'lc dut{dut_id} comb: 0b{comb:016b}')
        return coma, comb

    def read_one_max14661_hc_negative_feedback(self, chip_index, log=False):
        chip_id = MAX14661_HC_CHIP_LIST[chip_index]
        coma = (self.read_max14661(chip_id, 0x01) << 8) + \
               self.read_max14661(chip_id, 0x00)
        comb = (self.read_max14661(chip_id, 0x03) << 8) + \
               self.read_max14661(chip_id, 0x02)
        if log:
            self.Log('info', f'hc max{chip_index} coma: 0b{coma:010b}')
            self.Log('info', f'hc max{chip_index} comb: 0b{comb:010b}')
        return coma, comb

    def disconnect_all_max14661_devices(self, log=False):
        self.disconnect_all_lc_negative_feedback(log)
        self.disconnect_all_hc_negative_feedback(log)

    def disconnect_all_lc_negative_feedback(self, log=False):
        for i in range(4):
            self.disconnect_one_max14661_lc_negative_feedback(i)
            self.read_one_max14661_lc_negative_feedback(i, log)

    def disconnect_all_hc_negative_feedback(self, log=False):
        for i in range(4):
            self.disconnect_one_max14661_hc_negative_feedback(i)
            self.read_one_max14661_hc_negative_feedback(i, log)

    def disconnect_one_max14661_lc_negative_feedback(self, dut_id):
        chip_id = MAX14661_LC_CHIP_LIST[dut_id]
        for cmd_address in range(4):
            cmd_data = 0b0
            self.write_max14661(chip_id, cmd_address, cmd_data)

    def disconnect_one_max14661_hc_negative_feedback(self, chip_index):
        chip_id = MAX14661_HC_CHIP_LIST[chip_index]
        for cmd_address in range(4):
            cmd_data = 0b0
            self.write_max14661(chip_id, cmd_address, cmd_data)

    def connect_lc_common_load(self):
        chip_id = 1
        bank = 1
        pin = 2
        output = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x08)
        io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
        output = output & (0xFFFF ^ (1 << pin))
        io_set = io_set & (0xFFFF ^ (1 << pin))
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, output)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, io_set)

    def connect_lc_dtb_feedback(self, dut_id, lc_rail, log=False):
        self.connect_lc_positive_feedback(dut_id, log)
        self.connect_lc_negative_feedback(dut_id, lc_rail, vt_gnd='gnd', log=False)

    def connect_lc_positive_feedback(self, dut_id, log=False):
        lc_rail_force, lc_rail_feedback = self.decode_lc_rail_pca9505(dut_id)
        chip_id = lc_rail_force["chip"]
        bank = lc_rail_force["bank"]
        pin = lc_rail_force["pin"]
        output = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x08)
        io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
        output = output & (0xFFFF ^ (1 << pin))
        io_set = io_set & (0xFFFF ^ (1 << pin))
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, output)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, io_set)
        chip_id = lc_rail_feedback["chip"]
        bank = lc_rail_feedback["bank"]
        pin = lc_rail_feedback["pin"]
        output = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x08)
        io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
        output = output & (0xFFFF ^ (1 << pin))
        io_set = io_set & (0xFFFF ^ (1 << pin))
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, output)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, io_set)
        for chip_id in range(3):
            self.read_one_pca9505_device(chip_id, log)

    def connect_lc_negative_feedback(self, dut_id, lc_rail, vt_gnd, log=False):
        chip_id = MAX14661_LC_CHIP_LIST[dut_id]
        cmd_data = lc_rail
        if cmd_data < 8:
            if vt_gnd == 'vt':
                cmd_address = 0x00
            elif vt_gnd == 'gnd':
                cmd_address = 0x02
        else:
            cmd_data = cmd_data - 8
            if vt_gnd == 'vt':
                cmd_address = 0x01
            elif vt_gnd == 'gnd':
                cmd_address = 0x03
        self.write_max14661(chip_id, cmd_address, (1<<cmd_data))
        for i in range(4):
            self.read_one_max14661_lc_negative_feedback(i, log)

    def connect_hc_common_load(self, r_count):
        chip_id = 1
        bank = 0
        pin = 2**r_count - 1
        output = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x08)
        io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
        output = output & (0xFFFF ^ pin)
        io_set = io_set & (0xFFFF ^ pin)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, output)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, io_set)

    def connect_hc_dtb_feedback(self, dut_id, hc_rail, log=False):
        self.connect_hc_positive_feedback(dut_id, hc_rail, log)
        self.connect_hc_negative_feedback(dut_id, hc_rail, vt_gnd='gnd')

    def connect_hc_positive_feedback(self, dut_id, hc_rail, log=False):
        hc_rail_force, hc_rail_feedback = \
            self.decode_hc_rail_pca9505(dut_id, hc_rail)
        chip_id = hc_rail_force["chip"]
        bank = hc_rail_force["bank"]
        pin = hc_rail_force["pin"]
        output = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x08)
        io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
        output = output & (0xFFFF ^ (1<<pin))
        io_set = io_set & (0xFFFF ^ (1<<pin))
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, output)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, io_set)
        chip_id = hc_rail_feedback["chip"]
        bank = hc_rail_feedback["bank"]
        pin = hc_rail_feedback["pin"]
        output = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x08)
        io_set = hil.hbiDtbPca9505Read(self.slot, chip_id, bank + 0x18)
        output = output & (0xFFFF ^ (1<<pin))
        io_set = io_set & (0xFFFF ^ (1<<pin))
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x08, output)
        hil.hbiDtbPca9505Write(self.slot, chip_id, bank + 0x18, io_set)
        for chip_id in range(3):
            self.read_one_pca9505_device(chip_id, log)

    def connect_hc_negative_feedback(self, dut_id, hc_rail, vt_gnd='gnd', log=False):
        chip_id, cmd_data = self.decode_hc_rail_max14661(dut_id, hc_rail)
        if cmd_data < 8:
            if vt_gnd == 'vt':
                cmd_address = 0x00
            elif vt_gnd == 'gnd':
                cmd_address = 0x02
        else:
            cmd_data = cmd_data - 8
            if vt_gnd == 'vt':
                cmd_address = 0x01
            elif vt_gnd == 'gnd':
                cmd_address = 0x03
        self.write_max14661(chip_id, cmd_address, (1<<cmd_data))
        for i in range(4):
            self.read_one_max14661_hc_negative_feedback(i, log)

    def decode_lc_rail_pca9505(self, dut_id):
        if dut_id == 0:
            chip_0 = 0
            bank_0 = 4
            pin_0 = 5
            chip_1 = 1
            bank_1 = 1
            pin_1 = 3
        elif dut_id == 1:
            chip_0 = 2
            bank_0 = 4
            pin_0 = 5
            chip_1 = 1
            bank_1 = 1
            pin_1 = 4
        elif dut_id == 2:
            chip_0 = 0
            bank_0 = 4
            pin_0 = 6
            chip_1 = 1
            bank_1 = 1
            pin_1 = 5
        elif dut_id == 3:
            chip_0 = 2
            bank_0 = 4
            pin_0 = 6
            chip_1 = 1
            bank_1 = 1
            pin_1 = 6
        lc_rail_force = {"chip": chip_0, "bank": bank_0, "pin": pin_0}
        lc_rail_feedback = {"chip": chip_1, "bank": bank_1, "pin": pin_1}
        return lc_rail_force, lc_rail_feedback

    def decode_hc_rail_pca9505(self, dut_id, hc_rail):
        if ((dut_id == 0)&(hc_rail < 5))|((dut_id == 2)&(hc_rail > 4)):
            chip_0 = 0
            bank_0 = 4
            pin_0 = 3
            chip_1 = 1
            bank_1 = 0
            pin_1 = 6
        elif ((dut_id == 0)&(hc_rail > 4))|((dut_id == 2)&(hc_rail < 5)):
            chip_0 = 0
            bank_0 = 4
            pin_0 = 4
            chip_1 = 1
            bank_1 = 0
            pin_1 = 7
        elif ((dut_id == 1)&(hc_rail < 5))|((dut_id == 3)&(hc_rail > 4)):
            chip_0 = 2
            bank_0 = 4
            pin_0 = 3
            chip_1 = 1
            bank_1 = 1
            pin_1 = 0
        elif ((dut_id == 1)&(hc_rail > 4))|((dut_id == 3)&(hc_rail < 5)):
            chip_0 = 2
            bank_0 = 4
            pin_0 = 4
            chip_1 = 1
            bank_1 = 1
            pin_1 = 1
        hc_rail_force = {"chip": chip_0, "bank": bank_0, "pin": pin_0}
        hc_rail_feedback = {"chip": chip_1, "bank": bank_1, "pin": pin_1}
        return hc_rail_force, hc_rail_feedback

    def decode_hc_rail_max14661(self, dut_id, hc_rail):
        if dut_id < 2:
            if hc_rail < 5:
                chip_index = dut_id * 2
                cmd_data = hc_rail
            else:
                chip_index = dut_id * 2 + 1
                cmd_data = hc_rail - 5
        else:
            if hc_rail > 4:
                chip_index = dut_id % 2
                cmd_data = hc_rail
            else:
                chip_index = dut_id % 2 + 1
                cmd_data = hc_rail + 5
        chip_id = MAX14661_HC_CHIP_LIST[chip_index]
        return chip_id, cmd_data

    forceSenseBusMapHc = {"0_0" : [0, 1],
                          "0_1" : [1, 2],
                          "1_0" : [2, 4],
                          "1_1" : [3, 5],
                          "2_0" : [1, 2],
                          "2_1" : [0, 1],
                          "3_1" : [2, 4],
                          "3_0" : [3, 5]}

    dpsHcChToDtbMax14661PinMap = {"0_0" : 0,
                                  "0_1" : 1,
                                  "0_2" : 2,
                                  "0_3" : 3,
                                  "0_4" : 4,
                                  "0_5" : 0,
                                  "0_6" : 1,
                                  "0_7" : 2,
                                  "0_8" : 3,
                                  "0_9" : 4,
                                  "1_0" : 0,
                                  "1_1" : 1,
                                  "1_2" : 2,
                                  "1_3" : 3,
                                  "1_4" : 4,
                                  "1_5" : 0,
                                  "1_6" : 1,
                                  "1_7" : 2,
                                  "1_8" : 3,
                                  "1_9" : 4,
                                  "2_0" : 5,
                                  "2_1" : 6,
                                  "2_2" : 7,
                                  "2_3" : 8,
                                  "2_4" : 9,
                                  "2_5" : 5,
                                  "2_6" : 6,
                                  "2_7" : 7,
                                  "2_8" : 8,
                                  "2_9" : 9,
                                  "3_0" : 5,
                                  "3_1" : 6,
                                  "3_2" : 7,
                                  "3_3" : 8,
                                  "3_4" : 9,
                                  "3_5" : 5,
                                  "3_6" : 6,
                                  "3_7" : 7,
                                  "3_8" : 8,
                                  "3_9" : 9}


    U22 = 1
    U24 = 0
    U78 = 2
    relays = { #bank, pin, chip
        "LC_FB0_SEN_EN_N" : [1,3,U22], #slot%4
        "LC_FB1_SEN_EN_N" : [1,4,U22], #slot%4
        "LC_FB2_SEN_EN_N" : [1,5,U22], #slot%4
        "LC_FB3_SEN_EN_N" : [1,6,U22], #slot%4

        "LC_FB0_LD_EN_N" : [4,5,U24], #slot%4
        "LC_FB1_LD_EN_N" : [4,5,U78], #slot%4
        "LC_FB2_LD_EN_N" : [4,6,U24], #slot%4
        "LC_FB3_LD_EN_N" : [4,6,U78], #slot%4

        "LC_LD_COM_EN_N" : [1,2,U22], #slot%4

        'HC_FB0_LD_EN_N' : [4,3,U24],
        'HC_FB1_LD_EN_N' : [4,4,U24],
        'HC_FB2_LD_EN_N' : [4,3,U78],
        'HC_FB3_LD_EN_N' : [4,4,U78],

        'HC_FB0_SEN_EN_N' : [0,6,U22],
        'HC_FB1_SEN_EN_N' : [0,7,U22],
        'HC_FB2_SEN_EN_N' : [1,0,U22],
        'HC_FB3_SEN_EN_N' : [1,1,U22],

        'HC_LD_COM_R1_EN_N' : [0,0,U22],
        'HC_LD_COM_R2_EN_N' : [0,1,U22],
        'HC_LD_COM_R3_EN_N' : [0,2,U22],
        'HC_LD_COM_R4_EN_N' : [0,3,U22],
        'HC_LD_COM_R5_EN_N' : [0,4,U22],
        'HC_LD_COM_R6_EN_N' : [0,5,U22]}
