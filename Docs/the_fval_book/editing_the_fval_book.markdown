INTEL CONFIDENTIAL

Copyright 2020 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.

# Editing the FVAL Book
The FVAL Book is written in a subset of [Markdown](https://daringfireball.net/projects/markdown/syntax)

PyCharm does not implement all Markdown syntax, so use the preview pane to
ensure the output is as expected. Sometimes you have to click back and forth
between 'Show Preview Only' and the other views to get the result to render.
