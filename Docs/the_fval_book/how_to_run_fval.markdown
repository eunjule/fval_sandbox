INTEL CONFIDENTIAL

Copyright 2020 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.

#How to run FVAL
The FVAL repository can be cloned from: 
`\\datagrovehf.hf.intel.com\hdmt_mercurial\FpgaValidation`

##To run fval
Open a command prompt at the repo location type Tools\regress.py<br/>
Use its -h args to learn about the different options available.

###On HDMT:
1.  When running full regression without any custom output folder<br/>
    `ABC_repo_directory_path\Tools\regress.py instrument_name -s slot_number_of_instrument`

2.  When running full regression with custom output folder<br/>
    `ABC_repo_directory_path\Tools\regress.py instrument_name -s slot_number_of_instrument -o your_custom_output_folder_name`

3.  To run a particular test only<br/>
    `ABC_repo_directory_path\Tools\regress.py instrument_name -s slot_number_of_instrument -tn file_name.class_name.test_name`

4.  To repeat a test x number of times<br/>
    `ABC_repo_directory_path\Tools\regress.py instrument_name -s slot_number_of_instrument -tn file_name.class_name.test_name -r 10`

###On HBI tester:
`ABC_repo_directory_path\Tools\regress.py hbi_instrument_name`

###Additional options:
use the `-sk` option to skip FPGA loads and other instrument initialization sequence (at present only supported for HBI)

use the `-simulate` option to run local simulation (not on hardware)

use the `-lo` option to store results to directory to local folder (D:\FVALRegressionData) on the tester.