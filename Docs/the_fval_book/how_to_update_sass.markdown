INTEL CONFIDENTIAL

Copyright 2020 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.

#How to update SASS
1. Find the version of TOS that contains the SASS version you want to install
    * TOS daily builds can be found at: \\orststtde02.amr.corp.intel.com\HDMT\Users\SiteCDaily_Builds\3XGen2

2. Extract the TOS release .zip file where other TOS releases are found on the tester
    * Example for HF2HWHBI10006: C:\Intel\hdmtregression
    * On some systems it may be a different folder

3. In the TOS release folder:
    * Open a command prompt in the <path_to_new_TOS>\Runtime\Release\HdmtSupervisorService directory
        (ignore TOS Dashboard changes while running commands, if it is running)
    * Run -> HdmtTosCtrl.exe stoptos
    * Run -> HdmtTosCtrl.exe stopcore
    * Run -> HdmtTosCtrl.exe switchtos <path_to_new_TOS>

4. Ensure that the HDMT Supervisor Service is setup correctly:
    * Open the Windows Services App
        * Click on  the Windows Start Menu
        * Type 'services'
        * Select the Services App
    * Look for the HDMT Supervisor Service
    * Ensure that the 'Startup Type' is set to Automatic
        * If it is not, double click on the service
        * From the 'Startup type:' dropdown menu select 'Automatic'
        * Select 'OK'
    * Ensure that Status is running
        * If it is not, click the 'Start the service' link

5. Delete entries in the Windows "Startup" menu
    * Shortcuts to any version of SASS
    * Shortcut to the SC Dashboard
