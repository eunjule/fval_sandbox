INTEL CONFIDENTIAL

Copyright 2020 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.
#How to Make Release Notes
##Location
Release Notes are placed on the
[HDMTTOS Wiki](https://wiki.ith.intel.com/display/HDMTTOS)
under the [FPGA Releases](https://wiki.ith.intel.com/display/HDMTTOS/FPGA+Releases)
page.

##Contents
Release Notes should contain:
* A link to the release image
* A list of new features and enhancements, with a link to TFS for each fixed issue
* A list of known issues with links to TFS for each
* A description of the tested configuration
* Results from regression testing, including explanations for any waived failures
