INTEL CONFIDENTIAL

Copyright 2020 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.

#FVAL Coding Standard
##Language</h1>
Python is used to provide ease of development and enable quickly tweaking code in the lab.

Users, such as FPGA developers, lab technicians, validation team members, etc., can be directed to code that they can modify to explore FPGA or HW behavior.<br/>
Python 3.6 is installed in all testers as part of the site controller image.

Other languages are used where speed or code obfuscation has been an expectation.

Before porting code from Python to other languages for speed, consider profiling and algorithmic improvements.
* Algorithms are more important than language choice for speed.
* It is easier to experiment with algorithms in Python than lower level languages
* Keep in mind that functionality crossing implementation boundaries between languages will be more difficult to maintain and improve

##Style
Generally follow [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/)

Higher level guidance is given in [PEP 20 -- The Zen of Python](https://www.python.org/dev/peps/pep-0020/)

Code that is not written in Python will be called from Python, so use Python conventions.

##Structure
Use the design principles in the mnemonic [_SOLID_](https://en.wikipedia.org/wiki/SOLID).

Understand the [_DRY_](https://web.archive.org/web/20131204221336/http://programmer.97things.oreilly.com/wiki/index.php/Don't_Repeat_Yourself) 
principle as it relates to code and process.
* Sometimes there is a tradeoff between code duplication and effort duplication.
For example, when code is shared between instruments, changes will need to be validated with all affected instruments

##Logging
* The transcript and console output should provide useful information.
  Logging unimportant information interferes with the use the the transcript.
* The 'DEBUG' logging level is used for diagnosing ongoing issues.
* Log messages used to develop tests should not be converted to DEBUG at the end of test development.
  Instead, delete extraneous logging.
