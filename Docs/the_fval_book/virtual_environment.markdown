INTEL CONFIDENTIAL

Copyright 2019 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.
 
### create the virtual environment (first time only)
    py -m venv my_env

### enter the virtual environment
    my_env\Scripts\activate.bat

### update pip and install modules (first time only)
     set HTTPS_PROXY=http://proxy-chain.intel.com:912
     python -m pip install --upgrade pip
     python -m pip install snakeviz
     --  AND / OR --
     python -m pip install pyprof2calltree

### run the script
    py -m cProfile -o <profile_results_file> <script_to_profile + args>

### viewing the result in snakeviz
    py -m snakeviz profile_results_file

### viewing the result in QCacheGrind
    py -m pyprof2calltree -i profile_results_file -o callgrind.<whatever>
then open QCacheGrind and open the file

### exit the virtual environment
    my_env\Scripts\deactivate.bat
