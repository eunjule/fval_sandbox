INTEL CONFIDENTIAL

Copyright 2020 Intel Corporation.

This software and the related documents are Intel copyrighted materials,
and your use of them is governed by the express license under which they
were provided to you ("License"). Unless the License provides otherwise,
you may not use, modify, copy, publish, distribute, disclose or transmit
this software or the related documents without Intel's prior written
permission.

This software and the related documents are provided as is, with no express
or implied warranties, other than those that are expressly stated in the
License.

# The FVAL Book
The definitive book of all things FVAL.

[comment]: # (Purpose, structure, processes, intents, and history, it is all here!<br/>)
[comment]: # (Although a work-in-progress, as it shall ever be...)

## Contents
- [Editing this book](editing_the_fval_book.markdown)
- [How to run FVAL](how_to_run_fval.markdown)
- [How to update SASS](how_to_update_sass.markdown)
- [Coding Standard](coding_standard.markdown)
- [How to make release notes](how_to_make_release_notes.markdown)
- [Using a virtual environment](virtual_environment.markdown)
