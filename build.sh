#!/bin/bash
################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: build.sh
#-------------------------------------------------------------------------------
#     Purpose: Linux build script
#-------------------------------------------------------------------------------
#  Created by: Dean E. Glazeski
#        Date: 02/19/16
#       Group: HDMT FPGA Validation
################################################################################

config=Release

if [ -n "$1" ]
then
    config=$1
fi

mkdir _build
cd _build

if [ -n "$EC_ENV_ROOT" ]
then
    # special values required for EC machine builds
    cmake_dir=/usr/intel/pkgs/cmake/2.8.1
    python_dir=/usr/intel/pkgs/python3/3.6.3
    gcc_dir=/usr/intel/pkgs/gcc/7.3.0
    swig_dir=/usr/intel/pkgs/swig/3.0.12

    export PATH=$python_dir/bin:$swig_dir/bin:$cmake_dir/bin:$PATH
    export CXX=$gcc_dir/bin/g++
    export CC=$gcc_dir/bin/gcc
    cmake -DPYTHON_LIBRARY=$python_dir/lib/libpython3.so -DPYTHON_INCLUDE_DIR=$python_dir/include/python3.6m -DCMAKE_BUILD_TYPE=$config ..
else
    cmake -DCMAKE_BUILD_TYPE=$config ..
fi

make -j

