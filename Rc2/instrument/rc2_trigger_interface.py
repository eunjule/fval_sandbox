# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from enum import Enum

from Common.fval import Log, Object


MAX_NUM_LINKS = 12


def create_interface(instrument):
    Log('info', f'{instrument.name()}: Initializing for trigger interface')
    instrument.ensure_initialized()

    return [TriggerInterface(instrument, link)
            for link in range(MAX_NUM_LINKS)]


class TriggerInterface(Object):
    def __init__(self, instrument, link):
        super().__init__()
        self._rc = instrument
        self._link = link

    def setup_interface(self):
        pass

    def send_down_trigger(self, trigger):
        self._rc.send_trigger(trigger, self.link())

    def check_for_up_trigger(self, expected_trigger, num_retries=10):
        actual_trigger = 0
        for retry in range(num_retries):
            actual_trigger = self._rc.read_trigger_up(self.link())
            if actual_trigger == expected_trigger:
                break

        return actual_trigger

    def log_status(self):
        pass

    def aurora_link_is_stable(self):
        return self._rc.aurora_link_is_stable(self.link())

    # def reset_trigger_interface(self):
    #     self._rc.reset_trigger_interface(self.link())

    def create_table_aurora_link_status(self):
        status_reg = self._rc.read_bar_register(
            self._rc.registers.AURORA_STATUS, self.link())
        return self._rc.create_table_from_register(status_reg)

    def get_rx_fifo_info(self):
        status_reg = self._rc.read_bar_register(
            self._rc.registers.AURORA_STATUS, self.link())
        return status_reg.rx_fifo_full, status_reg.rx_fifo_count

    def get_tx_fifo_info(self):
        status_reg = self._rc.read_bar_register(
            self._rc.registers.AURORA_STATUS, self.link())
        return status_reg.tx_fifo_full, status_reg.tx_fifo_count

    def get_pll_lock_status(self):
        status_reg = self._rc.read_bar_register(
            self._rc.registers.AURORA_STATUS, self.link())
        return status_reg.tx_pll_locked, status_reg.transceiver_pll_locked

    def get_error_status(self):
        status_reg = self._rc.read_bar_register(
            self._rc.registers.AURORA_STATUS, self.link())
        return status_reg.hard_error, status_reg.soft_error

    def get_up_status(self):
        status_reg = self._rc.read_bar_register(
            self._rc.registers.AURORA_STATUS, self.link())
        return status_reg.channel_up, status_reg.lane_up

    def link(self):
        return self._link

    def name(self):
        return f'{self._rc.name()}.{TriggerLink(self.link()).name}'

    def slot(self):
        return None


class TriggerLink(Enum):
    SLOT_0 = 0
    SLOT_1 = 1
    SLOT_2 = 2
    SLOT_3 = 3
    SLOT_4 = 4
    SLOT_5 = 5
    SLOT_6 = 6
    SLOT_7 = 7
    SLOT_8 = 8
    SLOT_9 = 9
    SLOT_10 = 10
    SLOT_11 = 11
