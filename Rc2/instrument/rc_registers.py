# Copyright 2019 Intel Corporation. All rights reserved.
#
# This file is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import ctypes

import Common.register as register

class RcRegister(register.Register):
    def __init__(self, uint=0, length=32):
        super().__init__(value=uint)

class AuroraStatus1(RcRegister):
    BAR = 1
    ADDR = 0x0110
    _fields_ = [('Aurora0_HardError', ctypes.c_uint, 1),
                ('Aurora0_SoftError', ctypes.c_uint, 1),
                ('Aurora0_ChannelUp', ctypes.c_uint, 1),
                ('Aurora0_LaneUp', ctypes.c_uint, 1),
                ('Aurora1_HardError', ctypes.c_uint, 1),
                ('Aurora1_SoftError', ctypes.c_uint, 1),
                ('Aurora1_ChannelUp', ctypes.c_uint, 1),
                ('Aurora1_LaneUp', ctypes.c_uint, 1),
                ('Aurora2_HardError', ctypes.c_uint, 1),
                ('Aurora2_SoftError', ctypes.c_uint, 1),
                ('Aurora2_ChannelUp', ctypes.c_uint, 1),
                ('Aurora2_LaneUp', ctypes.c_uint, 1),
                ('Aurora3_HardError', ctypes.c_uint, 1),
                ('Aurora3_SoftError', ctypes.c_uint, 1),
                ('Aurora3_ChannelUp', ctypes.c_uint, 1),
                ('Aurora3_LaneUp', ctypes.c_uint, 1),
                ('Aurora4_HardError', ctypes.c_uint, 1),
                ('Aurora4_SoftError', ctypes.c_uint, 1),
                ('Aurora4_ChannelUp', ctypes.c_uint, 1),
                ('Aurora4_LaneUp', ctypes.c_uint, 1),
                ('Aurora5_HardError', ctypes.c_uint, 1),
                ('Aurora5_SoftError', ctypes.c_uint, 1),
                ('Aurora5_ChannelUp', ctypes.c_uint, 1),
                ('Aurora5_LaneUp', ctypes.c_uint, 1),
                ('Aurora6_HardError', ctypes.c_uint, 1),
                ('Aurora6_SoftError', ctypes.c_uint, 1),
                ('Aurora6_ChannelUp', ctypes.c_uint, 1),
                ('Aurora6_LaneUp', ctypes.c_uint, 1),
                ('Aurora7_HardError', ctypes.c_uint, 1),
                ('Aurora7_SoftError', ctypes.c_uint, 1),
                ('Aurora7_ChannelUp', ctypes.c_uint, 1),
                ('Aurora7_LaneUp', ctypes.c_uint, 1)]

class AuroraStatus2(RcRegister):
    BAR = 1
    ADDR = 0x0100
    _fields_ = [('Aurora8_HardError', ctypes.c_uint, 1),
                ('Aurora8_SoftError', ctypes.c_uint, 1),
                ('Aurora8_ChannelUp', ctypes.c_uint, 1),
                ('Aurora8_LaneUp', ctypes.c_uint, 1),
                ('Aurora9_HardError', ctypes.c_uint, 1),
                ('Aurora9_SoftError', ctypes.c_uint, 1),
                ('Aurora9_ChannelUp', ctypes.c_uint, 1),
                ('Aurora9_LaneUp', ctypes.c_uint, 1),
                ('Aurora10_HardError', ctypes.c_uint, 1),
                ('Aurora10_SoftError', ctypes.c_uint, 1),
                ('Aurora10_ChannelUp', ctypes.c_uint, 1),
                ('Aurora10_LaneUp', ctypes.c_uint, 1),
                ('Aurora11_HardError', ctypes.c_uint, 1),
                ('Aurora11_SoftError', ctypes.c_uint, 1),
                ('Aurora11_ChannelUp', ctypes.c_uint, 1),
                ('Aurora11_LaneUp', ctypes.c_uint, 1),
                ('AuroraBlock0LockBits', ctypes.c_uint, 5),
                ('AuroraBlock1LockBits', ctypes.c_uint, 5),
                ('AuroraBlock2LockBits', ctypes.c_uint, 5),
                ('Reserved', ctypes.c_uint, 1)]

class TriggerOverrun(RcRegister):
    BAR = 1
    ADDR = 0x0120
    _fields_ = [('TriggerValue', ctypes.c_uint, 16),
                ('TriggerSource', ctypes.c_uint, 4),
                ('TriggerID', ctypes.c_uint, 10),
                ('OverrunIndicator', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 1)]
    
class TriggerOverrun32(RcRegister):
    BAR = 1
    ADDR = 0x0128
    _fields_ = [('TriggerValue', ctypes.c_uint, 32)]

class TriggerStatus(RcRegister):
    BAR = 1
    ADDR = 0x0130
    _fields_ = [('TriggerValue', ctypes.c_uint, 16),
                ('TriggerSource', ctypes.c_uint, 4),
                ('TriggerID', ctypes.c_uint, 8),
                ('QueuedTriggers', ctypes.c_uint, 1),
                ('StopReceived', ctypes.c_uint, 1),
                ('TriggerDataValid', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 1)]

class TriggerStatus32(RcRegister):
    BAR = 1
    ADDR = 0x0138
    _fields_ = [('TriggerValue', ctypes.c_uint, 32)]
    
class TriggerHistoryStatus(RcRegister):
    BAR = 1
    ADDR = 0x0140
    _fields_ = [('TriggerValue', ctypes.c_uint, 16),
                ('TriggerSource', ctypes.c_uint, 4),
                ('TriggerID', ctypes.c_uint, 8),
                ('QueuedTriggers', ctypes.c_uint, 1),
                ('StopReceived', ctypes.c_uint, 1),
                ('TriggerDataValid', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 1)]
    
class TriggerHistoryStatus32(RcRegister):
    BAR = 1
    ADDR = 0x0148
    _fields_ = [('TriggerValue', ctypes.c_uint, 32)]

class SoftwareTrigger(RcRegister):
    BAR = 1
    ADDR = 0x0150
    _fields_ = [('TriggerValue', ctypes.c_uint, 16),
                ('TriggerActive', ctypes.c_uint, 1),
                ('TriggerSystemInterruptEnable', ctypes.c_uint, 1),
                ('TriggerSystemInterruptEnableWriteEnable', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 12),
                ('SoftwareEnabled', ctypes.c_uint, 1)]

class SoftwareTrigger32(RcRegister):
    BAR = 1
    ADDR = 0x0158
    _fields_ = [('TriggerValue', ctypes.c_uint, 32)]

class TriggerSync(RcRegister):
    BAR = 1
    ADDR = 0x0160
    _fields_ = [('TriggerSyncValue', ctypes.c_uint, 12),
                ('Reserved', ctypes.c_uint, 20)]
    
class DownTriggerValue(RcRegister):
    BAR = 1
    ADDR = 0x0168
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot0(RcRegister):
    BAR = 1
    ADDR = 0x0170
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot1(RcRegister):
    BAR = 1
    ADDR = 0x0178
    _fields_ = [('Data', ctypes.c_uint, 32)]

class UpTriggerValueSlot2(RcRegister):
    BAR = 1
    ADDR = 0x0180
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot3(RcRegister):
    BAR = 1
    ADDR = 0x0188
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot4(RcRegister):
    BAR = 1
    ADDR = 0x0190
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot5(RcRegister):
    BAR = 1
    ADDR = 0x0198
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot6(RcRegister):
    BAR = 1
    ADDR = 0x01A0
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot7(RcRegister):
    BAR = 1
    ADDR = 0x01A8
    _fields_ = [('Data', ctypes.c_uint, 32)]

class UpTriggerValueSlot8(RcRegister):
    BAR = 1
    ADDR = 0x01B0
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot9(RcRegister):
    BAR = 1
    ADDR = 0x01B8
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot10(RcRegister):
    BAR = 1
    ADDR = 0x01C0
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class UpTriggerValueSlot11(RcRegister):
    BAR = 1
    ADDR = 0x01C8
    _fields_ = [('Data', ctypes.c_uint, 32)]
    
class FifoStatusReg(RcRegister):
    BAR = 1
    ADDR = 0x01D0
    _fields_ = [('AuroraInputFIFOEmptySignals', ctypes.c_uint, 12),
                ('AuroraInputFIFOFullSignals', ctypes.c_uint, 12),
                ('AuroraOutputFIFOEmptySignals', ctypes.c_uint, 3),
                ('AuroraOutputFIFOFullSignals', ctypes.c_uint, 3),
                ('Reserved', ctypes.c_uint, 2)]

class Reset(RcRegister):
    BAR = 0
    ADDR = 0x8010
    _fields_ = [('Aurora0_Reset', ctypes.c_uint, 1),
                ('Aurora1_Reset', ctypes.c_uint, 1),
                ('Aurora2_Reset', ctypes.c_uint, 1),
                ('Aurora3_Reset', ctypes.c_uint, 1),
                ('Aurora4_Reset', ctypes.c_uint, 1),
                ('Aurora5_Reset', ctypes.c_uint, 1),
                ('Aurora6_Reset', ctypes.c_uint, 1),
                ('Aurora7_Reset', ctypes.c_uint, 1),
                ('Aurora8_Reset', ctypes.c_uint, 1),
                ('Aurora9_Reset', ctypes.c_uint, 1),
                ('Aurora10_Reset', ctypes.c_uint, 1),
                ('Aurora11_Reset', ctypes.c_uint, 1),
                ('AuroraResetCards0_3', ctypes.c_uint, 1),
                ('AuroraResetCards4_7', ctypes.c_uint, 1),
                ('AuroraResetCards8_11', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 17)]

class GeneralPurposeStatus(RcRegister):
    BAR = 0
    ADDR = 0x8090
    _fields_ = [('TIUInstallStatus', ctypes.c_uint, 1),
                ('Reserved', ctypes.c_uint, 31)]

class GeneralPurposeOutputReg(RcRegister):
    BAR = 0
    ADDR = 0x80A0
    _fields_ = [('Data', ctypes.c_uint, 32)]

class CapabilitiesReg(RcRegister):
    BAR = 0
    ADDR = 0x80B0
    _fields_ = [('Data', ctypes.c_uint, 32)]

