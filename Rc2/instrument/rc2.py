################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: rc2.py
# -------------------------------------------------------------------------------
#     Purpose: Class for Resource Card
# -------------------------------------------------------------------------------
#  Created by: Yuan Feng
#        Date: 04/13/15
#       Group: HDMT FPGA Validation
################################################################################

import os
import time

from Common import configs
from Common import hilmon as hil
from Common.cache_blt_info import CachingBltInfo
from Rc2.instrument import rc_registers
from Common.instruments.base import Instrument
from Common.instruments.tester import get_tester
from Common.models.rc import Rc as RcModel

from ThirdParty.SASS.suppress_sensor import suppress_sensor


AVAIL_TRIGGER_INTERFACES = ['hpcc', 'hddps']


class Rc2(Instrument):
    registers = rc_registers

    CYP_PORTA_I2CSEL_MASK = 0x3F
    I2C_SELECT = {
        'I2C_FP_SEL': 0x00,
        'I2C_BLKPS_SEL': 0x80,
        'I2C_TIU_SEL': 0x40,
        'I2C_LOC_SEL': 0xC0,
    }

    def __init__(self, cardList=[], name=None):
        super().__init__(name)
        self.cardList = cardList
        self.fpgaImage = configs.RC2_FPGA_IMAGE
        self.slot = None
        self.under_test = False
        self.rcModel = RcModel()
        self.boardblt = CachingBltInfo(callback_function=self.read_board_blt,
                                       name='RC2 board')
        self.is_initialized = False

    def Initialize(self):
        # self.Connect() # not needed
        self.Log('info', 'Previous RC FPGA 0x{:X}'.format(self.GetFpgaVersion()))
        self.FpgaLoad(self.fpgaImage)
        self.Log('info', 'Current RC FPGA 0x{:X}'.format(self.GetFpgaVersion()))
        self.ClockInit()
        self.reset_aurora_bus()
        self.update_aurora_link_slots()
        # Enable software triggers
        data = self.Read('SoftwareTrigger')
        data.TriggerSystemInterruptEnable = 1
        data.TriggerSystemInterruptEnableWriteEnable = 1
        self.Write('SoftwareTrigger', data)

    def ensure_initialized(self):
        if not self.is_initialized:
            self.Initialize()

    def update_aurora_link_slots(self):
        self.aurora_link_slots = []
        t = get_tester()

        self.aurora_link_slots = [
            device.slot for device in t.devices
            if (device.name() != self.name() and
                device.name().lower() in AVAIL_TRIGGER_INTERFACES)]

        slots = os.getenv('DBG_REMOVE_HDMT_SLOTS')
        if slots is not None:
            slots = slots.split(',')
            for slot in slots:
                slot = int(slot)
                try:
                    self.aurora_link_slots.remove(slot)
                except ValueError as e:
                    self.Log('warning', f'{self.name()}: Unable to remove slot'
                                        f' {slot} from aurora_link_list. {e}')

    def Connect(self):
        return hil.rcConnect()

    def Disconnect(self):
        return hil.rcDisconnect()

    def BarRead(self, bar, offset):
        return hil.rcBarRead(bar, offset)

    def BarWrite(self, bar, offset, data):
        return hil.rcBarWrite(bar, offset, data)

    def Read(self, registerName):
        register = getattr(self.registers, registerName)
        return register(uint=self.BarRead(register.BAR, register.ADDR), length=32)

    def Write(self, registerName, data):
        register = getattr(self.registers, registerName)
        if isinstance(data, int):
            return self.BarWrite(register.BAR, register.ADDR, data)
        else:
            return self.BarWrite(register.BAR, register.ADDR, data.Pack())

    def FpgaLoad(self, fpgaImage):
        if not configs.RC2_FPGA_LOAD:
            return
        self.fpgaImage = fpgaImage
        if not os.path.exists(self.fpgaImage):
            raise Exception('RC Image not found at: {}'.format(self.fpgaImage))

        with suppress_sensor('FVAL RC2', 'HDMT_RC'):
            time.sleep(1)
            hil.rcDeviceDisable()
            result = hil.rcFpgaLoad(self.fpgaImage)
            hil.rcDeviceEnable()
            time.sleep(1)
        return result

    def rcLmk04808Write(self, addr, value):
        self.Log('debug', "rc lmk4808 write addr 0x{:X} value 0x{:X}".format(addr, value))
        hil.rcLmk04808Write(addr, value)

    def ClockInit(self):
        self.rcLmk04808Write(0x00, 0x00001000)  # reset
        time.sleep(1)
        self.rcLmk04808Write(0x00,
                             0x0000A018)  # CLKout00_01_PD=0,                        , CLKout01_ADLY_SEL=0, CLKout00_ADLY_SEL=0, CLKout00_01_DDLY=0x005, RESET=0, CLKout01_02_HS=0, CLKout00_01_DIV=0x018
        self.rcLmk04808Write(0x01,
                             0x0000A018)  # CLKout02_03_PD=0,                        , CLKout03_ADLY_SEL=0, CLKout02_ADLY_SEL=0, CLKout02_03_DDLY=0x005, PWRDN=0, CLKout02_03_HS=0, CLKout02_03_DIV=0x018
        self.rcLmk04808Write(0x02,
                             0x0000A018)  # CLKout04_05_PD=0,                        , CLKout05_ADLY_SEL=0, CLKout04_ADLY_SEL=0, CLKout04_05_DDLY=0x005,        , CLKout04_05_HS=0, CLKout04_05_DIV=0x018
        self.rcLmk04808Write(0x03,
                             0x0000A060)  # CLKout06_07_PD=0, CLKout06_07_OSCin_Sel=0, CLKout07_ADLY_SEL=0, CLKout06_ADLY_SEL=0, CLKout06_07_DDLY=0x005,        , CLKout06_07_HS=0, CLKout06_07_DIV=0x018
        self.rcLmk04808Write(0x04,
                             0x0000A01E)  # CLKout08_09_PD=0, CLKout08_09_OSCin_Sel=0, CLKout09_ADLY_SEL=0, CLKout08_ADLY_SEL=0, CLKout08_09_DDLY=0x005,        , CLKout08_09_HS=0, CLKout08_09_DIV=0x01E
        self.rcLmk04808Write(0x05,
                             0x0000A12C)  # CLKout10_11_PD=0,                        , CLKout11_ADLY_SEL=0, CLKout10_ADLY_SEL=0, CLKout10_11_DDLY=0x005,        , CLKout10_11_HS=0, CLKout10_11_DIV=0x12C
        self.rcLmk04808Write(0x06,
                             0x00A20800)  # CLKout03_TYPE=0x01, CLKout02_TYPE=0x04, CLKout01_TYPE=0x04, CLKout00_TYPE=0x01, CLKout02_03_ADLY=0x00, 0, CLKout00_01_ADLY=0x00
        self.rcLmk04808Write(0x07,
                             0x02208800)  # CLKout07_TYPE=0x04, CLKout06_TYPE=0x04, CLKout05_TYPE=0x01, CLKout04_TYPE=0x01, CLKout06_07_ADLY=0x00, 0, CLKout04_05_ADLY=0x00
        self.rcLmk04808Write(0x08,
                             0x02222000)  # CLKout11_TYPE=0x04, CLKout10_TYPE=0x04, CLKout09_TYPE=0x04, CLKout08_TYPE=0x04, CLKout10_11_ADLY=0x00, 0, CLKout08_09_ADLY=0x00
        self.rcLmk04808Write(0x09,
                             0x02AAAAAA)  # 010_1010_1010_1010_1010_1010_1010 - Fixed bit values specified by the data sheet. - Must be written
        self.rcLmk04808Write(0x0A,
                             0x00880a4c)  # OSCout1_LVPECL_AMP=0x3, 0, 1, OSCout0_TYPE=0x01, EN_OSCout1=1, EN_OSCout0=1, OSCout1_MUX=0, OSCout0_MUX=0, PD_OSCin=0, OSCout_DIV=0x2, 0, 1, 0 VCO_MUX=0, EN_FEEDBACK_MUX=1, VCO_DIV=0x1, FEEDBACK_MUX=0x4
        self.rcLmk04808Write(0x0B,
                             0x00200180)  # MODE=0x00, EN_SYNC=0, NO_SYNC_CLKout10_11=0, NO_SYNC_CLKout8_9=0, NO_SYNC_CLKout6_7=0, NO_SYNC_CLKout4_5=0, NO_SYNC_CLKout2_3=0, NO_SYNC_CLKout0_1=0, SYNC_MUX=0x0, SYNC_QUAL=0, SYNC_POL_INV=0, SYNC_EN_AUTO=0, SYNC_TYPE=0x3, 0,0,0,0,0,0, EN_PLL2_XTAL=0x0
        self.rcLmk04808Write(0x0C,
                             0x00d96003)  # LD_MUX=0x03, LD_TYPE=0x3, SYNC_PLL2_DLD=0, SYNCPLL1_DLD=0, 0,0,1,1,0,0,0,0,0,0,0,0,0, EN_TRACK=0, HOLDOVER_MODE=0x1, 1
        self.rcLmk04808Write(0x0D,
                             0x01D81403)  # HOLDOVER_MUX=0x7, HOLDOVER_TYPE=0x3, 0, Status_CLKin1_MUX=0x0, 0, Status_CLKin0_TYPE=0x2, DISABLE_DLD1_DET=1, Status_CLKin0_MUX=0x0, CLKin_Select_MODE=0x0, CLKin_Sel_INV=0, 0, EN_CLKin1=1, EN_CLKin0=1
        # hil.rcLmk04808Write(0x0D, 0x01181403)   # HOLDOVER_MUX=0x7, HOLDOVER_TYPE=0x3, 0, Status_CLKin1_MUX=0x0, 0, Status_CLKin0_TYPE=0x2, DISABLE_DLD1_DET=1, Status_CLKin0_MUX=0x0, CLKin_Select_MODE=0x0, CLKin_Sel_INV=0, 0, EN_CLKin1=1, EN_CLKin0=1
        self.rcLmk04808Write(0x0E,
                             0x00100000)  # LOS_TIMEOUT=0x0, 0, EN_LOS=0, 0, Status_CLKin1_TYPE=0x4, 0, 0, CLKin1_BUF_TYPE=0, CLKin0_BUF_TYPE=0, DAC_HIGH_TRIP=0x00, 0, 0, DAC_LOW_TRIP=0x00, EN_VTUNE_RAIL_DET=0
        self.rcLmk04808Write(0x0F,
                             0x04000400)  # MAN_DAC=0x200, 0, EN_MAN_DAC=0, HOLDOVER_DLD_CNT=0x0200, FORCE_HOLDOVER=0
        self.rcLmk04808Write(0x10, 0x060AA820)  # XTAL_LVL=0x3, 0_0000_1010_1010_1000_0010_0000
        self.rcLmk04808Write(0x18,
                             0x00000002)  # PLL2_CF_LF=0x0, PLL2_C3_LF=0x0, 0, PLL2_R4_LF=0x0, 0, PLL2_R3_LF=0x0, 0, PLL1_N_DLY=0x0, 0, PLL1_R_DLY=0x0, PLL1_WND_SIZE=0x1, 0
        self.rcLmk04808Write(0x19, 0x00164E20)  # DAC_CLK_DIV=0x00B, 0, 0, PLL1_DLD_CNT=0x4E20
        self.rcLmk04808Write(0x1A,
                             0x047D4000)  # PLL2_WND_SIZE=0x2, EN_PLL2_REF_2X=0, PLL2_CP_POL=0, PLL2_CP_GAIN=0x3, 1, 1, 1, 0, 1, 0, PLL2_DLD_CNT=0x2000, PLL2_CP_TRI=0
        self.rcLmk04808Write(0x1B,
                             0x00800004)  # 0,0,0, PLL1_CP_POL=1, PLL1_CP_GAIN0x0, 0,0, CLKin1_PreR_DIV=0x0, CLKin0_PreR_DIV=0x0, PLL1_R=0x0002, PLL1_CP_TRI=0
        self.rcLmk04808Write(0x1C, 0x00008032)  # PLL2_R=0x0001, PLL1_N=0x0019, 0
        self.rcLmk04808Write(0x1D, 0x000C000C)  # 0,0,0,0,0, OSCin_FREQ=0x1, PLL2_FAST_PDF=1, PLL2_N_CAL=0x0000C
        self.rcLmk04808Write(0x1E, 0x0010000C)  # 0,0,0,0,0, PLL2_P=0x2, 0, PLL2_N=0x0000C
        self.rcLmk04808Write(0x1F,
                             0x0000F800)  # 0,0,0,0,0,0,0,0,0,0, READBACK_LE=0, READBACK_ADDR=0x1F, 0,0,0,0,0,0,0,0,0,0, uWire_LOCK=0

    def reset_trigger_interface(self, slot):
        self.Log('debug', 'Resetting trigger interface for slot {}'.format(slot))
        data = self.Read('Reset')
        setattr(data, 'Aurora{}_Reset'.format(slot), 1)
        self.Write('Reset', data)
        # Poll the Reset register until the Aurora_Reset bit self-clears
        self.wait_for_reset_to_clear()

    def wait_for_reset_to_clear(self):
        for i in range(20):
            data = self.Read('Reset').Pack()
            if data == 0x0:
                return
            time.sleep(0.05)
        raise Exception('RC reset polling timeout expired')

    def reset_aurora_bus(self, slot=None):
        AuroraUp = False
        retryCount = 0
        retryLimit = 50
        AuroraError = False
        while not AuroraUp and retryCount < retryLimit:
            while not AuroraUp or AuroraError:
                lockBitsCleared = False
                for n in range(5):  # FIXME: Hacky workaround
                    self.Log('debug', 'Resetting trigger logic, try #{}'.format(n))
                    # Reset Aurora trigger interfaces for all the slots
                    for slot in range(12):
                        self.reset_trigger_interface(slot)
                    # Make sure that all the lock bits go away
                    lockBitsCleared = False
                    for i in range(20):
                        data = self.Read('AuroraStatus2')
                        if data.AuroraBlock0LockBits == 0x0 and data.AuroraBlock1LockBits == 0x0 and data.AuroraBlock2LockBits == 0x0:
                            lockBitsCleared = True
                            break
                        time.sleep(0.05)
                    if lockBitsCleared:
                        # Lock bits cleared, no need to try again
                        break
                if not lockBitsCleared:
                    raise Rc2.AuroraError('RC aurora status lock bits polling timeout expired')

                # Reset the Trigger Cores One at a Time, after GTx Reset
                data = self.Read('Reset')
                data.AuroraResetCards0_3 = 1
                self.Write('Reset', data)
                self.wait_for_reset_to_clear()

                data = self.Read('Reset')
                data.AuroraResetCards4_7 = 1
                self.Write('Reset', data)
                self.wait_for_reset_to_clear()

                data = self.Read('Reset')
                data.AuroraResetCards8_11 = 1
                self.Write('Reset', data)
                self.wait_for_reset_to_clear()

                try:
                    slotUp = self.log_aurora_status()
                    AuroraError = False
                except Rc2.AuroraError as e:
                    AuroraError = True
                    self.Log('warning',
                             f'Aurora error occurred ({retryCount+1} of '
                             f'{retryLimit} retries). Error: {e}')
                    retryCount = retryCount + 1
                    if retryCount == retryLimit:
                        self.Log('error', 'Aurora Bus retry limit hit!')
                    continue

                AuroraUp = True
                for slot in self.cardList:
                    if slot not in slotUp:
                        AuroraUp = False
                if not AuroraUp:
                    retryCount = retryCount + 1
                    if retryCount == retryLimit:
                        self.Log('error', 'Aurora Bus retry limit hit!')
                    self.Log('warning',
                             f'Aurora Bus is not completely up ({retryCount+1}'
                             f' of {retryLimit} retries).')

    def log_aurora_status(self, slot=None, log_level='debug'):
        status1 = self.Read('AuroraStatus1')
        status2 = self.Read('AuroraStatus2')
        self.Log(log_level, 'AuroraStatus1=0x{:x}'.format(status1.Pack()))
        self.Log(log_level, 'AuroraStatus2=0x{:x}'.format(status2.Pack()))
        slotUp = []
        for slot in range(12):
            status = None
            if slot < 8:
                status = status1
            else:
                status = status2

            hardError = getattr(status, 'Aurora{}_HardError'.format(slot))
            softError = getattr(status, 'Aurora{}_SoftError'.format(slot))
            channelUp = getattr(status, 'Aurora{}_ChannelUp'.format(slot))
            laneUp = getattr(status, 'Aurora{}_LaneUp'.format(slot))

            if hardError:
                raise Rc2.AuroraError(f'RC Aurora Hard Error bit is set for '
                                      f'slot {slot}')
            if softError:
                raise Rc2.AuroraError(f'RC Aurora Soft Error bit is set for '
                                      f'slot {slot}')
            if channelUp == 0x0 and laneUp == 0x0:
                self.Log('info', f'Aurora interface is up for slot {slot}')
                slotUp.append(slot)
        return slotUp

    class AuroraError(Exception):
        pass

    def log_aurora_error_counts(self, slot, log_level='info'):
        pass

    def clear_aurora_error_counts(self, slot):
        pass

    def aurora_link_is_stable(self, slot):
        if slot < 8:
            status = self.Read('AuroraStatus1')
        else:
            status = self.Read('AuroraStatus2')
        hardError = getattr(status, f'Aurora{slot}_HardError')
        softError = getattr(status, f'Aurora{slot}_SoftError')
        channelUp = getattr(status, f'Aurora{slot}_ChannelUp')
        laneUp = getattr(status, f'Aurora{slot}_LaneUp')

        if not hardError and not softError and channelUp == 0 and laneUp == 0:
            return True
        else:
            return False

    def verify_aurora_link_is_stable(self, slots, enable_info_log=False):
        if type(slots) != list:
            slots = [slots]

        for slot in slots:
            is_aurora_stable = self.aurora_link_is_stable(slot)
            if not is_aurora_stable:
                self.log_aurora_status(slot, log_level='error')
            elif enable_info_log:
                self.log_aurora_status(slot, log_level='info')

    def clear_rc_trigger(self):
        trigger = self.next_trigger()
        while trigger is not None:
            triggerId, triggerSource, triggerValue = trigger
            self.Log('warning',
                     'Cleared RC trigger 0x{:x}, triggerId = {}, triggerSource = {}'.format(triggerValue, triggerId,
                                                                                            triggerSource))
            trigger = self.next_trigger()

    def CheckNextRCTrigger(self, expectedTrigger, expectedSource):
        receivedTrigger = self.next_trigger()
        passing = True
        if not (receivedTrigger is None):
            triggerId, triggerSource, triggerValue = receivedTrigger
            if expectedTrigger is None:
                self.Log('error', 'RC received incorrect trigger: expected=None, actual=0x{:x}'.format(triggerValue))
                self.Log('error', 'RC received trigger 0x{:x}, triggerId = {}, triggerSource = {}'.format(triggerValue, triggerId, triggerSource))
                passing = False
            elif triggerValue != expectedTrigger:
                self.Log('error',
                         'RC received incorrect trigger: expected=0x{:x}, actual=0x{:x}'.format(expectedTrigger, triggerValue))
                self.Log('error', 'RC received trigger 0x{:x}, triggerId = {}, triggerSource = {}'.format(triggerValue, triggerId, triggerSource))
                passing = False
            elif triggerSource != expectedSource:
                self.Log('error', 'RC received trigger from incorrect source: expected=0x{:x}, actual=0x{:x}'.format(expectedSource, triggerSource))
                self.Log('error', 'RC received trigger 0x{:x}, triggerId = {}, triggerSource = {}'.format(triggerValue, triggerId, triggerSource))
                passing = False
            else:
                self.Log('debug', 'RC received trigger 0x{:x}, triggerId = {}, triggerSource = {}'.format(triggerValue, triggerId, triggerSource))
        else:
            if expectedTrigger is None:
                self.Log('debug', 'RC did not received trigger: matches expectation')
            else:
                self.Log('error', 'RC did not received trigger: expected=0x{:x}'.format(expectedTrigger))
                passing = False

        return passing

    def next_trigger(self, slot=0):
        # bit[15:0] TriggerValue
        # bit[3:0]  TriggerSource
        # bit[7:0]  TriggerID
        # bit       QueuedTriggers
        # bit       StopReceived
        # bit       TriggerDataValid
        # bit       Reserved
        data = self.Read('TriggerStatus')
        # self.Log('debug', 'RC received trigger 0x{:x}, triggerId = {}, triggerSource = {}, valid = {}'.format(self.Read('TriggerStatus32').Pack(), data.TriggerID, data.TriggerSource, data.TriggerDataValid))
        if data.TriggerDataValid == 0x1:
            triggerValue = self.Read('TriggerStatus32').Pack()
            result = (data.TriggerID, data.TriggerSource, triggerValue)
            self.AcknowledgeTrigger()
            return result
        return None

    def read_trigger_up(self, slot):
        TriggerUp = 'UpTriggerValueSlot' + str(slot)
        data = self.Read(TriggerUp)
        self.Log('debug', 'The RC trigger Up register for slot {} is set to 0x{:x}'.format(slot, data.Pack()))
        return data.Pack()

    def read_trigger_down(self, slot):
        data = self.Read('DownTriggerValue')
        return data.Pack()

    def read_clock_compensation_status(self):
        data = self.Read('GeneralPurposeOutputReg')
        self.Log('info', 'Clock Compensation Status Register at RC: 0x{:x}'.format(data.Pack()))

    def read_clock_compensation_capability(self):
        data = self.Read('CapabilitiesReg')
        self.Log('info', 'Capability Register at RC 0x{:x}'.format(data.Pack()))
        return data.Pack()

    def disable_clock_compensation(self):
        self.Write('GeneralPurposeOutputReg',0x0)
        data = self.Read('GeneralPurposeOutputReg')
        self.Log('info', 'Clock Compensation Status Register at RC after CC disable: 0x{:x}'.format(data.Pack()))

    def enable_clock_compensation(self):
        self.Write('GeneralPurposeOutputReg',0x1)
        data = self.Read('GeneralPurposeOutputReg')
        self.Log('info', 'Clock Compensation Status Register at RC after CC enable: 0x{:x}'.format(data.Pack()))

    def AcknowledgeTrigger(self):
        self.Write('TriggerStatus32', 0x0)
        self.Write('TriggerStatus', 0x0)

    def CheckTriggerOverrun(self):
        data = self.Read('TriggerOverrun')
        if data.OverrunIndicator == 0x1:
            triggerValue = self.Read('TriggerOverrun32').Pack()
            result = (data.TriggerID, data.TriggerSource, triggerValue)
            self.AcknowledgeOverrunTrigger()
            return result
        return None

    def AcknowledgeOverrunTrigger(self):
        self.Write('TriggerOverrun32', 0x0)
        self.Write('TriggerOverrun', 0x0)

    def GetTriggerBusStatus(self, slot):
        return hil.rcTriggerBusStatus(slot)

    # TODO: make sure MS Clock is init-ed once per cold reboot
    def IsClockInit(self):
        pass

    def GetFpgaVersion(self):
        return hil.rcFpgaVersion()

    def is_tiu_physically_connected(self):
        data = self.Read('GeneralPurposeStatus')
        self.Log('debug', 'GeneralPurposeStatus \n {}'.format(data))
        if data.TIUInstallStatus == 1:
            return False
        else:
            return True

    def send_sync_pulse(self, slots=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]):
        data = self.registers.TriggerSync(uint=0, length=32)
        data.TriggerSyncValue = 0x0
        for slot in slots:
            if 0 <= slot and slot < 12:
                self.Log('debug', 'Sending sync pulse to slot {}'.format(slot))
                data.TriggerSyncValue = data.TriggerSyncValue | (1 << slot)
            else:
                raise Exception('Invalid slot number: {}'.format(slot))
        self.Write('TriggerSync', data)

    def send_trigger(self, triggerValue, slot=-1):
        data = self.Read('SoftwareTrigger32')
        data.TriggerValue = triggerValue
        self.Write('SoftwareTrigger32', data)
        self.Log('debug',
                 'The SoftwareTrigger32 register has value 0x{:x}'.format(self.Read('SoftwareTrigger32').Pack()))

        data = self.Read('SoftwareTrigger')
        data.TriggerActive = 1
        data.TriggerSystemInterruptEnable = 1
        data.TriggerSystemInterruptEnableWriteEnable = 1
        data.SoftwareEnabled = 1
        self.Write('SoftwareTrigger', data)
        self.Log('debug', 'The SoftwareTrigger register has value 0x{:x}'.format(self.Read('SoftwareTrigger').Pack()))

    def read_board_blt(self):
        return hil.rcBltBoardRead()

    def clear_aurora_fifos(self, slot):
        # Empty method created to be compatible with Rc3
        pass
