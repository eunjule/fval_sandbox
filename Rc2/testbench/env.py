# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval


class Rc2Env(fval.Env):

    def __init__(self, test, tester):
        super().__init__(test)
        self.set_rc(tester)
        self.Log('info', 'Creating new Rc2 test environment instance')

    def set_rc(self, tester):
        instruments = tester.get_undertest_instruments('rc2')
        if instruments:
            self.rc = instruments[0]
        else:
            error_msg = f'Rc2 not found in instrument list: {instruments}'
            self.Log('error', error_msg)
            raise Rc2Env.InstrumentNotFoundError(error_msg)

    class InstrumentNotFoundError(Exception):
        pass
