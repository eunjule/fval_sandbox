# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 uses an Aurora Trigger interface

FPGA can communicate information using the Aurora Trigger to up to 14 trigger
link partners

Trigger Link Index (index), Trigger Link Partner (partner)
index|partner
---------------------------------------------------------
0....| Slot 0
1....| Slot 1
2....| Slot 2
3....| Slot 3
4....| Slot 4
5....| Slot 5
6....| Slot 6
7....| Slot 7
8....| Slot 8
9....| Slot 9
10...| Slot 10
11...| Slot 11
12...| Debug
13...| Accessory Card
"""

from Common.fval import SkipTest, TimeElapsedLogger
from Common.instruments import tester
from Common.instruments.hdmt_trigger_interface import \
    generate_random_payload_with_exclude, \
    HdmtTriggerInterface,\
    validate_aurora_statuses
from Common.triggers import CardType, generate_trigger
from Rc2.Tests.Rc2Test import Rc2Test

NUM_TEST_ITERATIONS = 100000
MAX_FAIL_COUNT = 2

class RcToInstrumentDiagnostics(Rc2Test):
    """Test communication robustness by sending Down triggers

    Up/Down Trigger registers have the following format:
    Bits.| Description
    --------------------------------
    31:26| Card Type:0, 1, 2, 5, 9, 63 - HPCC, DPS, RTOS, Event, SCS, Broadcast
    25:20| DUT Domain ID
    19...| Software Trigger Type: 0 == Not a sw trigger, 1 == sw trigger
    18:0.| Payload
    """

    def setUp(self, tester=None):
        super().setUp(tester)
        self.interfaces = HdmtTriggerInterface.get_interfaces()
        self.test_iterations = NUM_TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    def DirectedRcToInstrumentSlot0TriggerTest(self):
        """Send random data from Rc to the instrument in slot 0

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 0
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(0)

    def DirectedRcToInstrumentSlot1TriggerTest(self):
        """Send random data from Rc to the instrument in slot 1

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 1
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(1)

    def DirectedRcToInstrumentSlot2TriggerTest(self):
        """Send random data from Rc to the instrument in slot 2

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 2
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(2)

    def DirectedRcToInstrumentSlot3TriggerTest(self):
        """Send random data from Rc to the instrument in slot 3

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 3
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(3)

    def DirectedRcToInstrumentSlot4TriggerTest(self):
        """Send random data from Rc to the instrument in slot 4

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 4
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(4)

    def DirectedRcToInstrumentSlot5TriggerTest(self):
        """Send random data from Rc to the instrument in slot 5

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 5
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(5)

    def DirectedRcToInstrumentSlot6TriggerTest(self):
        """Send random data from Rc to the instrument in slot 6

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 6
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(6)

    def DirectedRcToInstrumentSlot7TriggerTest(self):
        """Send random data from Rc to the instrument in slot 7

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 7
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(7)

    def DirectedRcToInstrumentSlot8TriggerTest(self):
        """Send random data from Rc to the instrument in slot 8

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 8
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(8)

    def DirectedRcToInstrumentSlot9TriggerTest(self):
        """Send random data from Rc to the instrument in slot 9

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 9
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(9)

    def DirectedRcToInstrumentSlot10TriggerTest(self):
        """Send random data from Rc to the instrument in slot 10

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 10
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(10)

    def DirectedRcToInstrumentSlot11TriggerTest(self):
        """Send random data from Rc to the instrument in slot 11

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from Rc to instrument in slot 11
        3) Read data at corresponding instrument and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_rc_to_instrument_trigger(11)

    def validate_rc_to_instrument_trigger(self, slot):
        interface = self.get_interfaces(slot)
        skip_test_if_no_instrument(interface, slot)

        rc_interface = self.interfaces.get(None)

        self.Log('info', f'Checking Trigger Status before test...')
        validate_aurora_statuses(slot)

        self.run_rc_to_instrument_trigger(
            slot=slot,
            send_interface=rc_interface[slot],
            receive_interface=interface)

    def get_interfaces(self, slot):
        return self.interfaces.get(slot)

    def run_rc_to_instrument_trigger(self, slot, send_interface,
                                     receive_interface):
        setup_interfaces(send_interface, receive_interface)

        with TimeElapsedLogger(
                f'Time elapsed running {self.test_name()} for'
                f' {self.test_iterations} iterations'):

            for iteration in range(self.test_iterations):
                success = True

                for sub_interface in receive_interface:
                    trigger = generate_trigger(
                        card_type=sub_interface.name(),
                        payload=generate_random_payload_with_exclude(0))

                    send_interface.send_down_trigger(trigger)

                    actual_trigger = \
                        sub_interface.check_for_down_trigger(trigger)
                    if actual_trigger != trigger:
                        self.Log(
                            'error',
                            error_msg(
                                iteration,
                                sub_interface.name(),
                                trigger,
                                actual_trigger))
                        validate_aurora_statuses(slot)
                        success = False

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()


class InstrumentToRcDiagnostics(Rc2Test):
    """Test communication robustness by sending Up triggers

    Up/Down Trigger registers have the following format:
    Bits.| Description
    --------------------------------
    31:26| Card Type:0, 1, 2, 5, 9, 63 - HPCC, DPS, RTOS, Event, SCS, Broadcast
    25:20| DUT Domain ID
    19...| Software Trigger Type: 0 == Not a sw trigger, 1 == sw trigger
    18:0.| Payload
    """

    def setUp(self, tester=None):
        super().setUp(tester)
        self.interfaces = HdmtTriggerInterface.get_interfaces()
        self.test_iterations = NUM_TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    def DirectedInstrumentToRcSlot0TriggerTest(self):
        """Send random data from instrument at slot 0 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 0 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(0)

    def DirectedInstrumentToRcSlot1TriggerTest(self):
        """Send random data from instrument at slot 1 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 1 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(1)

    def DirectedInstrumentToRcSlot2TriggerTest(self):
        """Send random data from instrument at slot 2 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 2 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(2)

    def DirectedInstrumentToRcSlot3TriggerTest(self):
        """Send random data from instrument at slot 3 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 3 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(3)

    def DirectedInstrumentToRcSlot4TriggerTest(self):
        """Send random data from instrument at slot 4 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 4 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(4)

    def DirectedInstrumentToRcSlot5TriggerTest(self):
        """Send random data from instrument at slot 5 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 5 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(5)

    def DirectedInstrumentToRcSlot6TriggerTest(self):
        """Send random data from instrument at slot 6 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 6 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(6)

    def DirectedInstrumentToRcSlot7TriggerTest(self):
        """Send random data from instrument at slot 7 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 7 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(7)

    def DirectedInstrumentToRcSlot8TriggerTest(self):
        """Send random data from instrument at slot 8 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 8 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(8)

    def DirectedInstrumentToRcSlot9TriggerTest(self):
        """Send random data from instrument at slot 9 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 9 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(9)

    def DirectedInstrumentToRcSlot10TriggerTest(self):
        """Send random data from instrument at slot 10 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 10 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(10)

    def DirectedInstrumentToRcSlot11TriggerTest(self):
        """Send random data from instrument at slot 11 to Rc

        1) Verify Aurora link is stable. If not, clear aurora error count and
        re-verify.
        2) Send a randomized payload trigger from instrument at slot 11 to Rc
        3) Read data at corresponding Rc Trigger Up register and confirm
        4) Repeat steps 2 to 3 a total of 100000 times
        """
        self.validate_instrument_to_rc_trigger(11)

    def validate_instrument_to_rc_trigger(self, slot):
        interface = self.get_interfaces(slot)
        skip_test_if_no_instrument(interface, slot)

        rc_interface = self.interfaces.get(None)

        self.Log('info', f'Checking Trigger Status before test...')
        validate_aurora_statuses(slot)

        self.run_instrument_to_rc_trigger(
            slot=slot,
            send_interface=self.interfaces[slot],
            receive_interface=rc_interface[slot])

    def get_interfaces(self, slot):
        return self.interfaces.get(slot)

    def run_instrument_to_rc_trigger(self, slot, send_interface,
                                     receive_interface):
        setup_interfaces(send_interface, receive_interface)

        with TimeElapsedLogger(
                f'Time elapsed running {self.test_name()} for'
                f' {self.test_iterations} iterations'):

            for iteration in range(self.test_iterations):
                success = True

                for sub_interface in send_interface:
                    down_trigger_pass = True
                    trigger = generate_trigger(
                        card_type=CardType.INVALID.name,
                        payload=generate_random_payload_with_exclude(0))

                    sub_interface.send_up_trigger(trigger)

                    actual_trigger = \
                        receive_interface.check_for_up_trigger(trigger)
                    up_trigger_pass = (actual_trigger == trigger)
                    if not up_trigger_pass:
                        self.Log(
                            'error',
                            error_msg(
                                iteration,
                                receive_interface.name(),
                                trigger,
                                actual_trigger))


                    # DPS instrument will not register an invalid card_type
                    if 'dps' not in sub_interface.name().lower():
                        broadcast = sub_interface.check_for_down_trigger(
                            trigger)
                        down_trigger_pass = (broadcast == trigger)
                        if not down_trigger_pass:
                            self.Log(
                                'error',
                                error_broadcast_msg(
                                    iteration,
                                    sub_interface.name(),
                                    trigger,
                                    broadcast))

                    if not up_trigger_pass or not down_trigger_pass:
                        success = False
                        self.log_debug_info(sub_interface, trigger)
                        validate_aurora_statuses(slot)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()

    def log_debug_info(self, sub_interface, expected_trigger):
        sub_interface.log_status()
        self.rc.log_aurora_status(sub_interface.slot())
        self.read_all_up_triggers('warning')
        self.read_all_instrument_down_triggers(
            log_level='warning')
        self.verify_trigger_path_registers_if_hpcc(
            sub_interface=sub_interface,
            expected_trigger=expected_trigger)

    def read_all_instrument_down_triggers(self, log_level):
        for slot, interface in self.interfaces.items():
            if slot is None:
                continue
            for sub_interface in interface:
                trigger = sub_interface.read_down_trigger()
                self.Log(log_level, f'{sub_interface.name()}: '
                                    f'Down Trigger 0x{trigger:08X}')

    def read_all_up_triggers(self, log_level):
        for slot in self.rc.aurora_link_slots:
            trigger = self.rc.read_trigger_up(slot)
            self.Log(log_level, f'{self.rc.name()}_{slot:02d}: '
                                f'Up Trigger 0x{trigger:08X}')

    def verify_trigger_path_registers_if_hpcc(self,
                                              sub_interface,
                                              expected_trigger):
        if 'hpcc' in sub_interface.name().lower():
            hpcc = tester.get_tester().get_hdmt_instrument(
                sub_interface.slot())[0]
            hpcc.verify_trigger_at_hpccdc_up(expected_trigger)
            hpcc.verify_trigger_at_rctc_up(expected_trigger)
            hpcc.verify_trigger_at_rctc_down(expected_trigger)
            hpcc.verify_trigger_hpccdc_down(expected_trigger)

    def log_hpcc_link_stabililty_sent_triggers(self,
                                               sub_interface,
                                               num_triggers=10):
        if 'hpcc' in sub_interface.name().lower():
            hpcc = tester.get_tester().get_hdmt_instrument(
                sub_interface.slot())[0]
            total_triggers = len(hpcc.check_link_stability_trigger_list)
            for i in range(num_triggers):
                i = total_triggers - i
                trigger = hpcc.check_link_stability_trigger_list.pop()
                self.Log('info', f'{i}) 0x{trigger:08X}')


def get_pass_msg(method_name, test_iterations):
    """Returns a message of to be used for tests that pass
        - method_name: name of the test method
        - test_iterations: total run of repeated tests for method_name"""
    return f'{method_name} was successful for iterations: {test_iterations}'


def get_fail_msg(method_name, fail_count, test_iterations):
    """Returns a message of to be used for tests that fail
        - method_name: name of the test method
        - fail_count: number of failed test runs for method_name
        - test_iterations: total run of repeated tests for method_name"""
    return f'{method_name} was unsuccessful for {fail_count} out of ' \
           f'{test_iterations} iterations'


def error_msg(iteration, receive_name, expected, actual):
    return f'Iteration {iteration}: {receive_name} failed to receive ' \
           f'trigger (expected, actual): 0x{expected:08X}, 0x{actual:08X}'


def error_broadcast_msg(iteration, receive_name, expected, actual):
    return f'Iteration {iteration}: {receive_name} failed to receive ' \
           f'broadcast trigger (expected, actual): ' \
           f'0x{expected:08X}, 0x{actual:08X}'


def skip_test_if_no_instrument(interface, slot):
    if interface is None:
        raise SkipTest(f'Instrument not available in slot {slot}')


def setup_interfaces(send, receive):
    if type(send) is not list:
        send.setup_interface()
    else:
        for sub_interface in send:
            sub_interface.setup_interface()
    if type(receive) is not list:
        receive.setup_interface()
    else:
        for sub_interface in receive:
            sub_interface.setup_interface()
