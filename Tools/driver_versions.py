################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import subprocess

if __name__ == '__main__':
    import os
    import sys
    repo_root_path = os.path.join(os.path.dirname(__file__), '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common import fval


def write_to_log():
    fval.Log('info', 'Driver Versions:')
    drivers = driver_lines()
    for driver in drivers:
        fval.Log('info', str(driver))
       

    unique_drivers = set([x.name for x in drivers])
    #if len(unique_drivers) < len(drivers):
        # logged errors during initialization do not stop the regression run
        # so an exception is raised directly from here
        #raise Exception('Some device type has more than one driver in use!')
        

def driver_lines():
    command = 'powershell Get-WmiObject Win32_PnPSignedDriver | '\
              'select devicename, driverversion | '\
              'where {$_.devicename -like \\"*HDMT*\\"}'
    powershell_utility_output = subprocess.check_output(command).decode('utf-8')
    all_lines = [x.strip() for x in powershell_utility_output.split('\n')]
    driver_info_lines = [x for x in all_lines if x.startswith('HDMT')]
    unique_driver_info_lines = set(driver_info_lines)
    return [DriverInfo(x) for x in sorted(unique_driver_info_lines)]


class DriverInfo:
    def __init__(self, line):
        self.name, self.version = line.rsplit(None, 1)

    def __str__(self):
        return '{:60s} {}'.format(self.name, self.version)


if __name__ == '__main__':
    fval.SetLoggerLevel(fval.logging.DEBUG)
    fval.ConfigLogger()
    write_to_log()
