################################################################################
# INTEL CONFIDENTIAL - Copyright 2019. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
'''Report test implementation status'''

import argparse
import os
import sys
import unittest

repo_root = os.path.abspath(os.path.join(__file__, '..', '..'))
if repo_root not in sys.path:
    sys.path.insert(0, repo_root)

from Common.fval.discovery import discover_tests


class TestStatistics():
    def __init__(self, instrument):
        self.instrument = instrument
        self.tests = []

    def _print_statistics(self):
        self._discover_tests()
        self._print_total_count()
        self._print_expected_to_fail_count()
        self._print_skipped_count()
        self._print_skip_reasons()
        self._print_fail_reasons()

    def print_total_active_percent(self):
        self._discover_tests()
        total = len(self.tests)
        skipped = len(self._skipped_tests())
        percent = (total - skipped) / total * 100
        print(f'{percent:5.1f}% of the tests are active')

    def print_module_active_percent(self):
        self._discover_tests()
        modules = set([x.id().split('.')[0] for x in self.tests])
        for module in sorted(modules, key=str.casefold):
            tests_in_module = [x for x in self.tests if x.id().startswith(module + '.')]
            skip_count = len([x for x in tests_in_module if not x.is_skipped()])
            count = len(tests_in_module)
            percent = skip_count / count * 100
            print(f'{percent:5.1f}% {module} ({skip_count} / {count})')

    def _discover_tests(self):
        directory = test_directory(self.instrument)
        self.tests = get_tests_from_directory(directory)

    def _print_total_count(self):
        print(f'{len(self.tests):>4} total tests')

    def _print_expected_to_fail_count(self):
        print(f'{len(self._expected_to_fail_tests()):>4} expected to fail')

    def _expected_to_fail_tests(self):
        return [x for x in self.tests if self._is_expected_to_fail(x)]

    def _print_skipped_count(self):
        print(f'{len(self._skipped_tests()):>4} skipped')

    def _skipped_tests(self):
        return [x for x in self.tests if self._is_skipped(x)]

    def _print_skip_reasons(self):
        reasons = [self._skip_reason(t) for t in self.tests if self._is_skipped(t)]
        reason_set = set(reasons)
        print('---- Reasons for skipped tests ----')
        for r in sorted(reason_set):
            count = reasons.count(r)
            if not r:
                r = '""'
            print(f'{count:>4} {r}')

    def _print_fail_reasons(self):
        reasons = [self._fail_reason(t) for t in self.tests if self._is_expected_to_fail(t)]
        reason_set = set(reasons)
        print('---- Reasons for expected failing tests ----')
        for r in sorted(reason_set):
            count = reasons.count(r)
            if not r:
                r = '""'
            print(f'{count:>4} {r}')

    def _skip_reason(self, test):
        if hasattr(test.__class__, '__unittest_skip_why__'):
           return getattr(test.__class__, '__unittest_skip_why__', False).strip()
        else:
            method = getattr(test, test._testMethodName)
            return getattr(method, '__unittest_skip_why__', False).strip()

    def _fail_reason(self, test):
        if hasattr(test.__class__, '__expecting_failure_reason__'):
           return getattr(test.__class__, '__expecting_failure_reason__', False).strip()
        else:
            method = getattr(test, test._testMethodName)
            return getattr(method, '__expecting_failure_reason__', False).strip()

    def _is_skipped(self, test):
        return self._has_attribute(test, '__unittest_skip__')

    def _is_expected_to_fail(self, test):
        return self._has_attribute(test, '__unittest_expecting_failure__')

    def _has_attribute(self, test, attribute):
        class_has_attribute = getattr(test.__class__, attribute, False)
        method = getattr(test, test._testMethodName)
        method_has_attribute = getattr(method, attribute, False)
        return class_has_attribute or method_has_attribute


class TestDirectoryNotFoundError(Exception):
    pass


def test_directory(instrument):
    for d in instrument_directories():
        if instrument == d.lower():
            return os.path.join(repo_root, d, 'Tests')
    else:
        raise TestDirectoryNotFoundError(os.path.join(repo_root, instrument, 'Tests'))

def get_tests_from_directory(directory, matching=[]):
    if not matching:
        matching = ['*.*.*']
    test_suite = discover_tests(directory, matching)
    return _suite_to_test_list(test_suite)


def _suite_to_test_list(suite):
    test_list = []
    for item in suite:
        if type(item) is unittest.suite.TestSuite:
            test_list.extend(_suite_to_test_list(item))
        else:
            test_list.append(item)
    return test_list


def instrument_directories():
    '''Find all the instrument directories'''
    return [x for x in root_level_directories() \
         if x != 'Common' and 'Tests' in list_repo_directory(x)]


def root_level_directories():
    '''List all directories at the root of the repository'''
    return [x for x in os.listdir(repo_root) \
            if os.path.isdir(os.path.join(repo_root, x))]


def list_repo_directory(dir):
    return os.listdir(os.path.join(repo_root, dir))


def parse_args():
    instruments = [os.path.basename(x).lower() for x in instrument_directories()]
    parser = argparse.ArgumentParser()
    parser.add_argument('instrument', type=str.lower, choices=instruments)
    parser.add_argument('-a', '--active', action='store_true', help='Print actively running tests')
    parser.add_argument('-m', '--module', action='store_true', help='Report by module')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    t = TestStatistics(args.instrument)
    if args.active:
        if args.module:
            t.print_module_active_percent()
        else:
            t.print_total_active_percent()
    else:
        t._print_statistics()

