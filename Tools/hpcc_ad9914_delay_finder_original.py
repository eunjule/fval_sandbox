################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import sys
import io
import os
import argparse


from os.path import join, isdir, isfile
from fractions import Fraction

repo_root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

sys.path.append(os.path.abspath(repo_root_path))

from Hpcc.instrument import ad9914_registers
import Hpcc.instrument.hpccAcRegs as ac_registers
from ThirdParty.HIL.x64.Release import hil

PICO_SECONDS_PER_SECOND = 1000000000000
MINIMUM_PRECISION = 10

class EdgeFinder():
    def __init__(self, slot,foldername):
        self.slot = slot
        self._lastPeriod = 8e-9
        self._period = 8e-9
        self.foldername = foldername

    def run(self):
        for slice_under_test in range(2):
            self.setup_ad9914(slice_under_test)
            sample_list = self.get_sample_delay_tuple_list(slice_under_test)
            rising_edge_delay, rising_edge_sample = self.find_rising_edge(sample_list)
            self.write_distribution_to_csv(sample_list, rising_edge_delay,slice_under_test,rising_edge_sample)

    def get_sample_delay_tuple_list(self, slice_under_test):
        sample_list = []
        number_of_offsets = 2048
        number_of_samples = 100
        for phase_offset in range(number_of_offsets):
            delay_to_set = phase_offset * (2 ** 5)
            self.set_delay(delay_to_set, slice_under_test)
            latch_state = 0
            for each_sample in range(number_of_samples):
                latch_state += self.hpcc_read('SyncStartEdgeResult', slice_under_test).Count & 0x1
            sample = latch_state / number_of_samples
            sample_list.append((delay_to_set,sample))
        return sample_list

    def set_delay(self,delay_to_set, slice_under_test):
        r = ad9914_registers.ProfilePhaseAmplitudeRegister()
        r.phase_offset = delay_to_set
        r.amplitude_scale_factor = 0  # not used except in OSK mode
        self.setup_and_write_ad9914(r.offset(profile=0), r, slice_under_test)

    def setup_and_write_ad9914(self, address, data, slice_under_test):
        if not isinstance(data, int):
            data = data.Pack()
        regAD9914WRData = ac_registers.AD9914WRData(data)
        self.hpcc_write('AD9914WRData', regAD9914WRData, slice_under_test)

        regAD9914Control = self.hpcc_read('AD9914Control', slice_under_test)
        # print('Data in AD9914Control register: {}'.format(hex(regAD9914Control.Pack())))
        regAD9914Control.Address = address
        regAD9914Control.RdEnable = 0
        regAD9914Control.EnableAction = 1
        self.hpcc_write('AD9914Control', regAD9914Control, slice_under_test)

        for attempt in range(100):
            if self.hpcc_read('AD9914Control', slice_under_test).Ad9914AccessComplete:
                break
        else:
            print('Write 0x{:X} to Ad9914 address 0x{:X} for hpcc {} slice {} Failed. Tried {} times.'.format(data,
                                                                                                              address,
                                                                                                              self.slot,
                                                                                                              slice_under_test,
                                                                                                              attempt))

    def hpcc_write(self,registerName, data, slice_under_test):
        register = getattr(ac_registers, registerName)
        if type(data) is int:
            return self.bar_write(register.BAR, register.ADDR, data, slice_under_test)
        else:
            return self.bar_write(register.BAR, register.ADDR, data.value, slice_under_test)

    def bar_write(self,bar, offset, data, slice_under_test):
        return hil.hpccAcBarWrite(self.slot, slice_under_test, bar, offset, data)

    def hpcc_read(self,registerName, slice_under_test):
        register = getattr(ac_registers, registerName)
        return register(self.bar_read(register.BAR, register.ADDR, slice_under_test))

    def bar_read(self,bar, offset, slice_under_test):
        return hil.hpccAcBarRead(self.slot, slice_under_test, bar, offset)

    def find_rising_edge(self,sample_list):
        previous_sample = sample_list[0][1]
        for i in range(len(sample_list)):
            current_sample = sample_list[i][1]
            if current_sample > previous_sample:
                rising_edge_delay = sample_list[i][0]
                rising_edge_sample = sample_list[i][1]
                break
            else:
                previous_sample = current_sample
        return rising_edge_delay, rising_edge_sample

    def write_distribution_to_csv(self,sample_list,rising_edge_delay,slice_under_test,rising_edge_sample):
        SaveDir = 'Z:\\Users\\grtelang\\HPCC-AC_Cal_v1\\results\\find_edge\\{}'.format(self.foldername)
        SaveFileFmt = join(SaveDir, "Ad9914SyncDistribution_slice{}_at_edge_delay_{}.csv".format(slice_under_test,rising_edge_delay))
        if not isfile(SaveFileFmt):
            filename = SaveFileFmt
        else:
            SaveFileFmt = join(SaveDir, "Ad9914SyncDistribution_slice{}_at_edge_delay_{}.csv".format(slice_under_test,rising_edge_delay))
            filename = SaveFileFmt
        SaveFileFmt = join(SaveDir, "Ad9914_edge_delay_tuple_slice{}_at_{}.csv".format(slice_under_test,rising_edge_delay))
        if not isfile(SaveFileFmt):
            filename_1 = SaveFileFmt
        else:
            SaveFileFmt = join(SaveDir, "Ad9914_edge_delay_tuple_slice{}_at_{}.csv".format(slice_under_test,rising_edge_delay))
            filename_1 = SaveFileFmt
        print('*******************************************************************************************')
        print('Rising edge delay for {} slot {} slice{}  is {} at sample {}'.format(self.foldername,self.slot, slice_under_test,rising_edge_delay,rising_edge_sample))
        print('*******************************************************************************************')
        self.write_distribution_to_file(filename, sample_list)
        self.write_edge_to_file(filename_1, rising_edge_delay,rising_edge_sample)

    def write_distribution_to_file(self,filename, sample_list, mode='w'):
        with open(filename, mode) as fp:
            self.write_distribution(fp, sample_list)

    def write_distribution(self,ioStream, sample_list):
        assert isinstance(ioStream, io.IOBase)
        for syncState in sample_list:
            print("{},{}".format(syncState[0], syncState[1]), file=ioStream)
    
    def write_edge_to_file(self,filename, rising_edge_delay,rising_edge_sample, mode='w'):
        with open(filename, mode) as fp:
            self.write_edge(fp, rising_edge_delay,rising_edge_sample)

    def write_edge(self,ioStream, rising_edge_delay,rising_edge_sample):
        assert isinstance(ioStream, io.IOBase)
        print("{},{}".format(rising_edge_delay,rising_edge_sample), file=ioStream)
            

    def setup_ad9914(self,slice_under_test):
        self.set_period(8e-9)
        self.set_delay(0,slice_under_test)
        self.enable_output(False, slice_under_test)
        self.enable_output(True, slice_under_test)

    def set_period(self, period):
        self._period = period

    def enable_output(self, enable, slice_under_test):
        if not enable:
            # Slow the clock to nearest multiple of 125 (8ns), and keeping doubling it until we hit our target period
            targetPeriod = 8e-9 * 16
            if self._lastPeriod < 2e-9:
                currentPeriod = 2e-9
            elif self._lastPeriod < 4e-9:
                currentPeriod = 4e-9
            else:
                currentPeriod = 8e-9
            # currentPeriod = ((self._lastPeriod / 8e-9) + 1) * 8e-9
            # Slow the clock down
            while (currentPeriod < targetPeriod):
                self.write_period(currentPeriod,slice_under_test)
                currentPeriod *= 2
            self.write_period(targetPeriod,slice_under_test)
            # Finally, clear the DAC, FPGA will time the IO update correctly
            self.enable_ad9914_output(False,slice_under_test)
            # Now we can set our real frequency back
            self.write_period(self._period,slice_under_test)
            self._lastPeriod = self._period
        else:
            self.enable_ad9914_output(True,slice_under_test)

    def enable_ad9914_output(self, enable,slice_under_test):
        cfr1 = ad9914_registers.ControlFunction1Register(uint = 0x00010008, length = 32)
        cfr1.ExternalPowerDownControl = 1
        # By disabling auto clear phase accumulator and enabling clear phase accumulator, we lock the phase low
        # Once we de-assert the clear, the phase will start again, cleanly
        cfr1.AutoclearPhaseAccumulator = 0
        if enable:
            cfr1.ClearPhaseAccumulator = 0
        else:
            cfr1.ClearPhaseAccumulator = 1
        cfr1.EnableSineOutput = 1
        cfr1.VcoCalEnable = 0
        self.setup_and_write_ad9914(ad9914_registers.CONTROL_FUNCTION1_ADDR, cfr1, slice_under_test)

    def write_period(self, period,slice_under_test):
        PERIOD_SOURCE = Fraction(1000,3) # 3 GHz, 333.33ps
        periodFraction = Fraction(int(period * PICO_SECONDS_PER_SECOND * MINIMUM_PRECISION), MINIMUM_PRECISION)
        # FTW + A/B = (fo*2^32)/fs
        # Since A/B < 1, the first term is just the integer and fraction part of the equation
        # First, find fo/fs (round fo to nearest hz) (fo is 1/period)
        # That is equivalent to ps/po * 2^32
        ratio = Fraction((PERIOD_SOURCE / periodFraction) * (1 << 32))
        ftw = int(ratio)
        ratio -= ftw
        a = ratio.numerator
        b = ratio.denominator

        #Write frequency registers
        # print('period = {}, ftw/lowLimit = {}, b/upLimit = {}, a/stepSize = {}'.format(period, ftw, b, a))
        self.setup_and_write_ad9914(ad9914_registers.DIGITAL_RAMP_LOWER_LIMIT_ADDR, ftw, slice_under_test)
        self.setup_and_write_ad9914(ad9914_registers.DIGITAL_RAMP_UPPER_LIMIT_ADDR, b, slice_under_test)
        self.setup_and_write_ad9914(ad9914_registers.RISING_DIGITAL_RAMP_STEP_SIZE_ADDR, a, slice_under_test)


def parse_args():
    # Parse command line options
    parser = argparse.ArgumentParser(description='VH to CTYPE register generator')
    parser.add_argument('-slot', help='hpcc_slot_number', type=int, action="store", required=True)
    parser.add_argument('-f', '--foldername', help='Output filename (default is <instrument>_status.html)')
    return parser.parse_args()


if __name__ == '__main__':
    repo_root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    sys.path.append(os.path.abspath(repo_root_path))
    args = parse_args()

    edge_finder = EdgeFinder(slot=args.slot,foldername=args.foldername)
    edge_finder.run()
