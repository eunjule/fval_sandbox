# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import sys

if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common import configs
from Hpcc import hpcc_configs
from ThirdParty import colorama

colorama.init()

not_found_color = colorama.Style.BRIGHT + colorama.Fore.RED + colorama.Back.BLACK
okay_color = colorama.Style.BRIGHT + colorama.Fore.GREEN + colorama.Back.BLACK
reset_color = colorama.Style.RESET_ALL

exit_code = 0

def check_path(path):
    global exit_code
    print(f'{path}... ', end='')
    if os.path.exists(path):
        print(f'{okay_color}okay{reset_color}')
    else:
        print(f'{not_found_color}not found{reset_color}')
        exit_code = 1


for item in dir(configs):
    if 'FPGA_IMAGE' in item and 'LOAD' not in item:
        path = getattr(configs, item)
        check_path(path)

for item in dir(hpcc_configs):
    if 'fab' in item.lower():
        path = getattr(hpcc_configs, item)
        check_path(path)


exit(exit_code)
