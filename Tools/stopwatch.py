
# INTEL CONFIDENTIAL
# Copyright 2018 Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.
# 
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or
# delivery of the Materials, either expressly, by implication, inducement,
# estoppel or otherwise. Any license under such intellectual property rights
# must be express and approved by Intel in writing.

from time import perf_counter

class StopWatch :

    def __init__ ( self ) :
        self.creation_time = perf_counter ( )
        self.start_time    = perf_counter ( )
        self.stop_time     = self.start_time
        self.intervals     = [ ]
        self.running       = False

    def start ( self ) :
        self.start_time = perf_counter ( )
        self.running = True

    def stop ( self ) :
        if self.running :
            self.stop_time = perf_counter ( )
            self.intervals.append ( self.stop_time - self.start_time )
            self.running = False

    def elapsed_seconds ( self ) :
        if not self.running :
            return ( sum ( self.intervals ) )

        else :
            lap_time = perf_counter ( )
            return ( sum ( self.intervals ) + lap_time - self.start_time )

    def elapsed_milliseconds ( self ) :
        return ( self.elapsed_seconds ( ) * 1.0E3 )

    def elapsed_microseconds ( self ) :
        return ( self.elapsed_seconds ( ) * 1.0E6 )

    def clear ( self ) :
        self.intervals.clear ( )
        self.running = False

    def spin ( self, seconds ) :
        self.start ( )
        while True :
            lap_time = perf_counter ( )
            if lap_time - self.start_time >= seconds :
                self.stop_time = lap_time;
                self.intervals.append ( self.stop_time - self.start_time )
                self.running = False
                break

    def spinclear ( self, seconds ) :
        self.spin ( seconds )
        elapsed = self.elapsed_seconds ( )
        self.clear ( )
        return elapsed

    @staticmethod
    def start_new ( ) :
        sw = StopWatch ( )
        sw.start ( )
        return sw
