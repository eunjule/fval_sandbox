#!/usr/bin/env python3
# INTEL CONFIDENTIAL

# Copyright 2015-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import subprocess

source_extensions = ['bat', 'cc', 'cs', 'ds', 'flex', 'h', 'i', 'lemon',
                     'py', 'rpat', 'rtq', 'sh', 'txt']

not_source = ['bin', 'chw', 'cmake', 'code-workspace', 'csproj', 'csv', 'db',
              'dll', 'dotsettings', 'exe', 'gif', 'gz', 'hgignore', 'hgtags',
              'html', 'json', 'lib', 'obj', 'orig', 'pdb', 'png', 'pptx',
              'props', 'pyc', 'pyd', 'rbf', 'rej', 'rst', 'sln', 'so',
              'xlsx']

ignored_directories = ['.hg', 'ThirdParty', 'CMakeFiles', 'results',
                       '_build', '.idea', '__pycache__']

ignored_files = [os.path.join('Hbicc', 'tools', 'CaptureDecoder', 'SourceSafe', 'CSharp', 'Example.txt'),
                 os.path.join('Hbicc', 'tools', 'CaptureDecoder', 'SourceSafe', 'CSharp', 'FakeFile.txt'),
                 os.path.join('Hbicc', 'tools', 'CaptureDecoder', 'SourceSafe', 'CSharp', 'WorkFile.txt')]


def repo_root():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


def remove_ignored_directories(dirs):
    for i in ignored_directories:
        try:
            dirs.remove(i)
        except ValueError:
            pass


def needs_copyright(filename):
    if os.path.getsize(filename) == 0:
        return False
    with open(filename, 'r') as f:
        try:
            for i, line in enumerate(f):
                if 'copyright' in line.lower():
                    return False
                if i > 10:
                    break
        except UnicodeDecodeError:
            return False
    if is_not_source_controlled(filename):
        return False
    else:
        return True

def is_not_source_controlled(filename):
    r = subprocess.run(['hg', 'stat', filename], stdout=subprocess.PIPE)
    return '?' in r.stdout.decode('utf-8')

def is_ignored_file(filename):
    for i in ignored_files:
        if i in filename:
            return True
    else:
        return False

for root, dirs, files in os.walk(repo_root()):
    remove_ignored_directories(dirs)
    for file in files:
        extension = file.split('.')[-1].lower()
        filename = os.path.join(root, file)
        if is_ignored_file(filename):
            continue
        if extension in source_extensions:
            if needs_copyright(filename):
                print(f'Missing copyright statement in: {filename}')
        elif extension in not_source:
            continue
        else:
            if needs_copyright(filename):
                print(f'Check file (unknown file type): {filename}')
