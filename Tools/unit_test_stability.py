# INTEL CONFIDENTIAL

# Copyright 2019 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import logging
import os
import subprocess
import sys
import time


start_time = time.strftime('%Y-%m-%d_%H.%M.%S')
output_dir = os.path.join(os.getcwd(), f'unit_test_stability_results_{start_time}')

base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.chdir(base_dir)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('testnames', nargs='*', default=['.'], help='Test names to pass as argument to run_tests.py')
    return parser.parse_args()

def setup_logging():
    log_file = os.path.join(output_dir, f'run_tests_stability_log.txt')
    formatter = logging.Formatter('%(message)s')
    handlers = [logging.StreamHandler(), logging.FileHandler(log_file)]
    for h in handlers:
        h.setFormatter(formatter)
    logging.basicConfig(format='%(message)s', level = logging.INFO, handlers=handlers)
    logging.info(f'Logging to file {log_file}')
    
def transcript_name(i):
    return os.path.join(output_dir, f'run_tests_transcript_{i}.txt')

def loop_until_control_c(testnames):
    fail_counts, transcripts, completed_loops = loop(testnames, loops=1000000000)
    print_transcript_lists(transcripts)
    print_summary(fail_counts, completed_loops)

def loop(testnames, loops):
    try:
        fail_counts = {}
        transcripts = {}
        for i in range(loops):
            logging.info(f'iteration {i}')
            completed_process = subprocess.run([sys.executable, 'run_tests.py'] + testnames, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            output = completed_process.stdout.decode('utf-8')
            if len(output) < 1000:
                if 'No such file or directory' in output or 'ModuleNotFoundError' in output:
                    print(output)
                    exit()
            this_iteration_failed = False
            for line in output.split('\n'):
                if 'FAIL:' in line or 'ERROR:' in line:
                    this_iteration_failed = True
                    line = line.strip()
                    logging.info(line)
                    fail_counts[line] = fail_counts.get(line, 0) + 1
                    transcripts[line] = transcripts.get(line, []) + [i]
            
            if this_iteration_failed:
                with open(transcript_name(i), 'wb') as f:
                    f.write(completed_process.stdout)
    except KeyboardInterrupt:
        pass
    return fail_counts, transcripts, i


def print_transcript_lists(transcripts):
    logging.info('')
    for test, iterations in transcripts.items():
        logging.info(f'transcripts with: {test}')
        logging.info('='*80)
        for iteration in iterations:
            logging.info(f'   {transcript_name(iteration)}')
    

def print_summary(fail_counts, iterations):
    logging.info('\n Summary')
    logging.info('='*80)
    for test, count in fail_counts.items():
        fail_percentage = 100  *count / iterations
        logging.info(f'{fail_percentage:03.3f}% fails={count} {test}')

if __name__ == '__main__':
    args = parse_arguments()
    os.mkdir(output_dir)
    setup_logging()
    loop_until_control_c(args.testnames)
