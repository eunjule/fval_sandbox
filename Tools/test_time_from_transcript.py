# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import os


def times_to_csv(transcript):
    rows = get_times_from_transcript(transcript)
    csv_file = csv_file_name(transcript)
    write_rows_to_csv(rows, csv_file)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('transcript')
    return parser.parse_args()


def is_test_status_line(line):
    return 'PASSED' in line or 'FAILED' in line


def status_to_csv(line):
    minutes, seconds, test = time_from_status(line)
    return f'{minutes:02d}:{seconds:06.03f},{test}'


def time_from_status(line):
    tokens = line.split()
    for i, token in enumerate(tokens):
        if token in ['PASSED', 'FAILED']:
            test_index = i + 1
            break
    test = tokens[test_index]
    milliseconds = float(tokens[-1][1:-3])
    minutes = int(milliseconds // 60000)
    seconds = (milliseconds / 1000) - (minutes * 60)
    return minutes, seconds, test


def get_times_from_transcript(transcript):
    with open(transcript, 'r') as f:
        rows = [status_to_csv(line) for line in f if is_test_status_line(line)]
    rows.sort(reverse=True)
    return rows


def csv_file_name(transcript):
    root = os.path.dirname(transcript)
    csv_file = os.path.join(root, 'test_times.csv')
    return csv_file


def write_rows_to_csv(rows, csv_file):
    with open(csv_file, 'w') as f:
        for row in rows:
            print(row, file=f)


if __name__ == '__main__':
    args = parse_args()
    times_to_csv(args.transcript)
