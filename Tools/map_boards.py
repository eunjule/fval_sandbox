################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import os
import sys

if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Common.instruments.tester import get_tester

blt_attributes = {
    'Rc2':        ['boardblt'],
    'Rc3':        ['instrumentblt', 'boardblt'],
    'Hpcc':       ['instrumentblt', 'dcboardblt',    'acboardblt'],
    'Hddps':      ['instrumentblt', 'main_boardblt', 'daughter_boardblt'],
    'HvdpsCombo': ['instrumentblt', 'hvdpsboardblt', 'hvilboardblt'],
    'Tdaubnk':    ['instrumentblt', 'boardblt']
}

blt_fields = ['DeviceName', 'ManufactureDate', 'PartNumberAsBuilt',
              'PartNumberCurrent', 'SerialNumber', 'VendorName']


def main():
    myTester = get_tester()
    myTester.discover_all_instruments()

    for device in sorted(myTester.devices, key=device_sort_key):
        print(f'     ------ Slot {device.slot} ------')
        for field in blt_fields:
            print_field(device, field)
        print(f'')


def print_field(device, field_name):
    fields = [field_name]
    for blt in blts(device):
        fields.append(getattr(blt, field_name))
    print(f''.join([f'{name:20}' for name in fields]))


def blts(device):
    blts = []
    for attribute in blt_attributes[device.name()]:
        blts.append(getattr(device, attribute))
    return blts

def device_sort_key(device):
    if device.slot is None:
        key = -1
    else:
        key = device.slot
    return key


if __name__ == '__main__':
    main()
