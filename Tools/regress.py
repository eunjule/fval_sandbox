################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
# -------------------------------------------------------------------------------
#    Filename: regress.py
# -------------------------------------------------------------------------------
#     Purpose: Regression runner script
# -------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez / Yuan Feng
#        Date: 03/30/15
#       Group: HDMT FPGA Validation
################################################################################

import argparse
import datetime
import itertools
import logging
import os
import platform
import subprocess
import sys
import time
import traceback

repo_root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

if __name__ == '__main__':
    sys.path.append(os.path.abspath(repo_root_path))

from ThirdParty.SASS.suppress_sensor import sass_version_string

from Common import configs
from Common import fval
from Common.fval import discovery
from Common.fval.runner import TestRunner
from Common.instruments.tester import get_tester
from Hpcc import hpcc_configs as hpccConfigs
from Common import hilmon
from Tools.mercurial_settings import check_repository_user_name_is_set, \
    UserNameError

OUTPUT_PATH = None


def RunRegression(dut, slots=None, repeat=1, output=None, localoutput=None, uniqueoutput=False,skipinit=False,skipmemoryinit=False,
                  overrideconfigs=[], verbosity='INFO', regressionseed=None,
                  testseed=None, testname=None, testlist=None,
                  testlistordered=None, simulation=False, failfast=False):
    DATETIME = time.strftime('%A - %B %d, %Y - %I:%M:%S%p')
    TIMESTAMP = time.strftime('%Y-%m-%d_%H.%M.%S')

    overrides = []
    for oc in overrideconfigs:
        if oc != '':
            var, value = oc.split('=')
            overrides.append((var, value))
            setattr(configs, var, value)
            setattr(hpccConfigs, var, value)

    # set logging level
    if verbosity.isdigit():
        fval.SetLoggerLevel(int(verbosity))
    elif verbosity.upper() == "CRITICAL":
        fval.SetLoggerLevel(50)
    elif verbosity.upper() == "ERROR":
        fval.SetLoggerLevel(40)
    elif verbosity.upper() == "WARNING":
        fval.SetLoggerLevel(30)
    elif verbosity.upper() == "INFO":
        fval.SetLoggerLevel(20)
    elif verbosity.upper() == "DEBUG":
        fval.SetLoggerLevel(10)
    else:
        raise RuntimeError("Verbosity \'{}\' is not supported".format(verbosity))

    now = datetime.datetime.now()
    host_name = platform.node()
    if localoutput:
        OUTPUT_PATH = os.path.join('D:\\', 'FVALRegressionData', f'{TIMESTAMP}_{localoutput}')
    elif output is None:
        OUTPUT_PATH = os.path.join(repo_root_path, configs.RESULTS_DIR, TIMESTAMP + '_' + host_name)
    else:
        if uniqueoutput:
            OUTPUT_PATH = os.path.join(repo_root_path, configs.RESULTS_DIR, output, platform.node(), TIMESTAMP)
        else:
            OUTPUT_PATH = os.path.join(repo_root_path, configs.RESULTS_DIR, output, f'{TIMESTAMP}_{host_name.upper()}')

    os.makedirs(OUTPUT_PATH, exist_ok=True)
    fval.ConfigLogger(os.path.join(OUTPUT_PATH, 'transcript.txt'))

    os.chdir(OUTPUT_PATH)

    # seed > 0, and cannot specify both -rs and -ts
    if (regressionseed is not None) and (regressionseed < 0):
        raise RuntimeError("Seed has to be a positive integer.")
    if (testseed is not None) and (testseed < 0):
        raise RuntimeError("Seed has to be a positive integer.")
    if (regressionseed is not None) and (testseed is not None):
        raise RuntimeError("Cannot specify both Regression Seed and Test Seed.")
    # if test seed is specified, apply it to every test
    # if (testseed is not None) and (testname is None):
    #    raise RuntimeError("Need to specify the corresponding test name for test seed.")

    test_names = ['*.*.*']  # default, will get all Mini, Directed, Random, Thr, Stress tests
    if (testlist is not None):
        test_names = fval.TestList(testlist).GetTestList()
    elif (testlistordered is not None):
        test_names, orderedSeedList = fval.TestList(testlistordered).GetOrderedTestList()
    elif (testname is not None):
        test_names = testname

    for t in overrides:
        var, value = t
        fval.Log('info', "Overriding config '{}' to '{}'".format(var, value))

    fval.Log('info', '{}***** FPGA Regression Environment *****{}'.format(fval.RepeatColor(), fval.ResetColor()))
    try:
        fval.Log('info', 'Mercurial Node = {}'.format(subprocess.check_output('hg id -i').decode('utf-8').strip()))
    except:
        fval.Log('warning', 'Unable to print mercurial node, is mercurial installed?')
    fval.Log('info', 'Hostname = {}'.format(platform.node()))
    fval.Log('info', 'Date/Time = {}'.format(DATETIME))
    fval.Log('info', 'TIMESTAMP = {}'.format(TIMESTAMP))
    fval.Log('info', 'ROOT_PATH = "{}"'.format(repo_root_path))
    fval.Log('info', 'OUTPUT_PATH = "{}"'.format(OUTPUT_PATH))

    fval.Log('info', f'HIL Version: {hilmon.hil.hilVersionString()}')
    fval.Log('info', f'SASS Version: {sass_version_string()}')
    for var in dir(configs):
        if not (var.startswith('__')):
            value = getattr(configs, var)
            if type(value) == str:
                fval.Log('info', "{} = r'{}'".format(var, value))
            else:
                fval.Log('info', "{} = {}".format(var, value))
    # for var in dir(hpccConfigs):
    #     if not (var.startswith('__')):
    #         value = getattr(hpccConfigs, var)
    #         if type(value) == str:
    #             fval.Log('info', "{} = r'{}'".format(var, value))
    #         else:
    #             fval.Log('info', "{} = {}".format(var, value))

    report_sensor_masks()
    
    resultList = fval.ResultList(OUTPUT_PATH)
    fval.OUTPUT_PATH = OUTPUT_PATH

    fval.Log('info', 'Create tester and do discovery here')
    if simulation:
        _setup_simulated_tester(dut, slots)

    tester = get_tester()
    tester.discover_all_instruments()
        
    if configs.CONFIGURABLE_AURORA_CLOCK_COMPENSATION == True :
        for device in tester.devices:
            try:
                if device.name() == 'Hpcc':
                    fval.Log('info', "Programming HPCC images for slot {}".format(device.slot))
                    device.Initialize()
                elif 'Hddps' in device.name():
                    fval.Log('warning', "Work in progress. Waiting for CONFIGURABLE_AURORA_CLOCK_COMPENSATION images to be built.Programming Current available images for device:{} in slot:{}".format(device.name(), device.slot))
                    device.subslots[0].FpgaLoad(configs.HDDPS_MB_FPGA_IMAGE)
                    fval.Log('info', 'Current HDDPS MB Image slot {} subslot {}: {}'.format(device.slot, 0, hex(
                        device.subslots[0].GetFpgaVersion())))
                    device.subslots[1].FpgaLoad(configs.HDDPS_DB_FPGA_IMAGE)
                    fval.Log('info', 'Current HDDPS MB Image slot {} subslot {}: {}'.format(device.slot, 0, hex(
                        device.subslots[1].GetFpgaVersion())))
                elif device.name() == 'Tdaubnk':
                    fval.Log('info', "Programming TDAU Bank images for slot{}".format(device.slot))
                    device.FpgaLoad(configs.TDAUBNK_FPGA_IMAGE)
                elif device.name() == 'Rc2' or device.name() == 'Rc3':
                    pass
                elif device.name() == 'HvdpsCombo':
                    fval.Log('warning',
                             "Work in progress. Waiting for CONFIGURABLE_AURORA_CLOCK_COMPENSATION images to be built for device {} in slot:{}".format(device.name(), device.slot))
                else:
                    fval.Log('info','Unknown Device {} in Slot {}'.format(device.name(), device.slot))
            except:
                if device.name() != 'Rc2' and device.name() != 'Rc3':
                    fval.Log('info','Unable to program FPGA images for Instrument {} on slot {}'.format(device.name(), device.slot))

    verify_and_activate_devices(dut, slots, tester)
    if skipinit:
        setattr(configs, 'SKIP_INIT', skipinit)
        fval.Log('warning','---------Configs overwritten to bypass FPGA load and init--------')
    if skipmemoryinit:
        setattr(configs, 'SKIP_MEMORY_INIT', skipmemoryinit)
        fval.Log('warning','---------Configs overwritten to skip memory init--------')
    tester.Initialize()

    if dut in [x.lower() for x in configs.DPS_DEVICE_TYPES]:
        test_directory = os.path.join(repo_root_path, 'Dps', 'Tests')
    else:
        test_directory = os.path.join(repo_root_path, dut.capitalize(), 'Tests')

    suite = discovery.discover_tests(test_directory, test_names)
    fval.Log('info', "{}===== Device under Test: {} ====={}".format(fval.RepeatColor(), dut, fval.ResetColor()))

    '''
    Seed generation and usuage:
    1) Create List of tests
    2) Create Master Seed
    3) Create List of test seeds using master seed as base and equal to length of #tests + 1
    4) Set current seed to first in TS list
    5) Run Test
        a) Pick random test from list of tests
        b) use current seed
        c) run test
    6) Remove test from list of tests
    7) Set test seed to next seed in list
    8) GOTO 5
    '''

    # generate the regression seed
    seedLen = repeat * suite.countTestCases() + 1
    if (regressionseed is not None):
        regSeed = fval.MasterSeed(seedLen, regressionseed)
    else:
        regSeed = fval.MasterSeed(seedLen)
    if (testseed is None):
        fval.Log('info', "Regression Seed = " + str(regSeed.seed))

    if testlistordered == None:
        runner = TestRunner(regSeed, failfast=failfast)
    else:
        runner = TestRunner(regSeed, ordered_seed_list=orderedSeedList, failfast=failfast)

    try:
        result = runner.run(suite, testseed, repeat)
    except Exception as e:
        exceptionMsg = traceback.format_exc().split('\n')
        for line in exceptionMsg:
            fval.Log('warning', line)

    # generate result lists
    resultList.GenerateResultList(result, suite)

    return result.was_successful()


def _setup_simulated_tester(dut, slots):
    if dut in [x.lower() for x in configs.HBI_DEVICE_TYPES]:
        from Common.hbi_simulator import HbiSimulator
        hilmon.hil = HbiSimulator()
    else:
        from Common.hdmt_simulator import HdmtSimulator
        hilmon.hil = simulator = HdmtSimulator()
        try:
            simulator.add_instruments(dut, slots)
            if dut == 'rc3':
                simulator.add_hpcc(1)
                simulator.add_hddps(0)
        except HdmtSimulator.SlotError:
            fval.Log('error', f'Slot error, requested slots = {slots}')


def report_sensor_masks():
    for sensor in get_masked_sensors():
        fval.Log('warning', f'!!!!!  Sensor mask in use: {sensor:<15}  !!!!!')


def get_masked_sensors():
    try:
        return get_masked_sensors_from_file()
    except FileNotFoundError:
        fval.Log('info', "SASSSensorMask.txt not available.")
        return ''


def get_masked_sensors_from_file():
    with open(r'C:\SASS\SensorMask.txt', 'r') as f:
        return f.read().split()


def verify_and_activate_devices(dut, slots, tester):
    device_found_list = []
    device_found_slot_list = []

    if not tester.devices:
        raise Exception('No devices found on tester')

    instrument_list = [class_name.__class__.__name__.lower() for class_name in tester.devices]

    if tester.is_hbi_tester():
        for device in tester.devices:
            if dut == device.name().lower():
                device_found_list.append(device.name().lower())
                if 'hbidps' in device.name().lower():
                    device_found_slot_list.append(device.slot)
                device.under_test = True
    else:
        for slot, device in itertools.product(slots, tester.devices):
            if device.slot in [slot, None] and dut == device.name().lower():
                device_found_list.append(device.name().lower())
                device_found_slot_list.append(device.slot)
                device.under_test = True

    if tester.is_hbi_tester():
        device_types = [x.lower() for x in configs.HBI_DEVICE_TYPES]
        if dut in device_types:
            is_dut_slot_in_device_list(device_found_slot_list, instrument_list, dut, slots, tester)
        else:
            raise RuntimeError("DUT does not exist in HBI tester")
    else:
        device_types = [x.lower() for x in configs.HDMT_DEVICE_TYPES]
        if dut in device_types:
            is_dut_slot_in_device_list(device_found_slot_list, instrument_list, dut, slots, tester)
        else:
            raise RuntimeError("DUT does not exist in Gen2 tester")


def is_dut_slot_in_device_list(device_found_slot_list, device_found_list, dut, slots, tester):
    if tester.is_hbi_tester():
        if dut.lower() == 'hbidps':
            if dut not in device_found_list:
                raise_runtime_error(device_found_slot_list, dut, tester, slots)
        else:
            if dut not in device_found_list:
                raise_runtime_error(device_found_slot_list, dut, tester)
    else:
        if dut not in device_found_list or not (set(slots)).issubset(set(device_found_slot_list)):
            raise_runtime_error(device_found_slot_list, dut, tester,slots)
    return True


def raise_runtime_error(device_found_slot_list, dut, tester, slots = None):
    fval.Log('info', '\tDevices Found')
    for device in tester.devices:
        if device.__class__.__name__ == 'Rc2' or device.__class__.__name__ == 'Hbirctc':
            fval.Log('info', '\t\t{:<10}'.format(device.__class__.__name__))
        else:
            fval.Log('info', '\t\t{:<10}'.format(device.__class__.__name__))
    fval.Log('info', '\t\tdut requested: {}'.format(dut))
    fval.Log('info', '\t\tslots found: {}'.format(device_found_slot_list))
    fval.Log('info', '\t\tslots requested: {}'.format(slots))
    raise RuntimeError('Devices/Slots requested not found '
                       '(possibly invalid or missing -s argument?)')


def parse_args(args):
    # print(args)
    # Parse command line options
    parser = argparse.ArgumentParser(description='HDMT FPGA Regression Runner')
    parser.add_argument('dut', help='DUT to regress', type=str)
    parser.add_argument('-s', '--slots', help='Slots to use (comma separated and no spaces)', type=str, action="store", default=None)
    parser.add_argument('-r', '--repeat', help='Number of times to repeat the regression', type=int, action="store",
                        default=1)
    parser.add_argument('-o', '--output', help='Output directory name to store regression results', type=str,
                        action="store", default=None)
    parser.add_argument('-lo', '--localoutput', help='Output directory to local folder (D:\FVALRegressionData) and uses the argument as folder name suffix.', type=str,
                        action="store", default=None)
    parser.add_argument('-u', '--uniqueoutput', help='Create unique output directory using tester name and timestamp',
                        action="store_true", default=False)
    parser.add_argument('-sk', '--skipinit', help='Skip FPGA loads and instrument init',
                        action="store_true", default=False)
    parser.add_argument('-skm', '--skipmemoryinit', help='Skip DDR init only. There is a risk for ECC error after FPGA reflash but no memorry init.',
                        action="store_true", default=False)
    parser.add_argument('-oc', '--overrideconfigs', help='Override config values', type=str, action="store", default='')
    parser.add_argument('-v', '--verbosity', help='Set verbosity level', type=str, action="store", default='15')
    parser.add_argument('-rs', '--regressionseed', help='Specify regression seed to reproduce the whole regression run',
                        type=int, action="store")
    parser.add_argument('-ts', '--testseed', help='Specify test seed to repeat the test', type=int, action="store")
    parser.add_argument('-tn', '--testname', help='Test name (wildcards okay)', action='append')
    parser.add_argument('-tl', '--testlist', help='Specify a file name for test list', type=str, action="store")
    parser.add_argument('-tlo', '--testlistordered',
                        help='Specify a file name for a test list that contains test name and seed comma sep per line',
                        type=str, action="store")
    parser.add_argument('-d', '--debugcode',
                        help='Adds a wait block to allow user to attach debugger to python process',
                        action="store_true", default=False)
    parser.add_argument('--simulate', action='store_true', help='Simulate the hardware')
    parser.add_argument('--failfast', action='store_true', help='Stop on first failure')

    args = parser.parse_args(args)

    if args.debugcode:
        print('Attach a debugger to this python process (pid={})'.format(os.getpid()))
        input('Press <ENTER> to continue...')

    if args.slots is None or args.slots.lower() == 'none':
        slots = [None]
    else:
        slots = [int(x) for x in args.slots.split(',')]

    if args.testlist is not None:
        if (args.testlistordered is not None):
            raise RuntimeError("Can only specify a test list or ordered test list, but not both")

    return args, slots


def Main(user_args=None):
    global OUTPUT_PATH
    try:
        check_repository_user_name_is_set()
    except UserNameError as e:
        print('ERROR:', str(e))
        exit(1)

    args, slots = parse_args(user_args)

    success = RunRegression(
        overrideconfigs=args.overrideconfigs.split(','),
        verbosity=args.verbosity,
        output=args.output,
        localoutput=args.localoutput,
        uniqueoutput=args.uniqueoutput,
        skipinit=args.skipinit,
        skipmemoryinit=args.skipmemoryinit,
        slots=slots,
        repeat=args.repeat,
        regressionseed=args.regressionseed,
        testseed=args.testseed,
        testname=args.testname,
        testlist=args.testlist,
        testlistordered=args.testlistordered,
        dut=args.dut.lower(),
        simulation=args.simulate,
        failfast=args.failfast
    )

    if success:
        sys.exit(0)
    else:
        sys.exit(1)


if __name__ == '__main__':
    try:
        Main()
    except Exception as e:
        level = 'critical'
        kwargs = {}
        kwargs['extra'] = {}
        kwargs['extra']['LEVEL'] = level.upper()[0]
        kwargs['extra']['COLOR'] = fval.LevelToColor(level)
        kwargs['extra']['COLORRESET'] = fval.ResetColor()
        kwargs['extra']['TIMESTAMP'] = fval.Timestamp()
        logging.exception(e, **kwargs)
