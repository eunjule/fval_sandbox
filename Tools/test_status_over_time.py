# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import subprocess
import csv
import shutil 
import os
import sys
import argparse

repo_root = os.path.abspath(os.path.join(__file__, '..', '..'))
sys.path.insert(0, repo_root)

from Tools.test_status import instrument_directories

test_StatusRoot = os.path.join(repo_root, 'Tools','test_status.py')
test_StatusRootCopy = os.path.join(repo_root, 'Tools','test_status_Copy.py')

def update_node_tip():
    pullIncomingChanges= subprocess.run(
        ["hg", "pull", "-u"],
         stdout=subprocess.PIPE
         )
    updateTip= subprocess.run(
        ["hg", "update"],
         stdout=subprocess.PIPE
         )
  
def get_hg_rev(startdate,enddate,instrument):
    node_data = get_node_data(instrument, startdate, enddate)
    headers = get_headers(node_data)
    with open('StatusofInstrumentTests.csv','w', newline='') as f:
        w=csv.writer(f)
        w.writerow(headers)
        for node in node_data:
            row = [node.get(header, '') for header in headers]
            w.writerow(row)


def get_headers(node_data):
    headers = set()
    for node in node_data:
        headers |= set(node.keys())
    return ordered_headers(headers)


def ordered_headers(headers):
    '''date first, then the rest in alphabetical order'''
    headers = sorted(headers)
    index_of_date = headers.index('date')
    headers.pop(index_of_date)
    headers.insert(0, 'date')
    return headers


def get_node_data(instrument, startdate, enddate):
    path = test_StatusRootCopy
    cmd = [sys.executable, path, instrument]
    stats_list = []
    for node, date in node_and_date(startdate, enddate):
        subprocess.run(["hg", "update", '-C', node], stdout=subprocess.DEVNULL)
        p = subprocess.run(cmd, stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT, encoding='utf-8',
                           timeout=10)
        stats_list.append(get_stats_from_node(date, node, p.returncode, p.stdout))
    return stats_list


def get_stats_from_node(date, node, return_code, stdout):
    if return_code != 0:
        return node_error_stats(date, node, return_code, stdout)
    else:
        return node_stats(date, node, return_code, stdout)


def node_stats(date, node, return_code, stdout):
    table = {}
    table['node'] = node
    table['date'] = date
    table['exit_code'] = return_code
    flattened_output = ' '.join(stdout.split('\n')[:3])
    print(f'{node} {return_code} {flattened_output}')
    table['error'] = ''
    table.update(counts(stdout))
    return table


def node_error_stats(date, node, return_code, stdout):
    table = {}
    table['node'] = node
    table['date'] = date
    table['exit_code'] = return_code
    error = stdout.strip().split('\n')[-1]
    print(f'{node} {return_code} {error}')
    table['error'] = error
    return table


def node_and_date(startdate, enddate):
    '''generate node id and date for each node in the date range'''
    for node, date in list_of_node_ids_and_dates(startdate, enddate):
        yield node, date

def list_of_node_ids_and_dates(startdate, enddate):
    p = subprocess.run(
        ['hg', 'log', '-r', f"date('{startdate} to {enddate}')",
        '--template', '{node|short},{date|shortdate}\n'],
        stdout=subprocess.PIPE, encoding='utf-8')
    return [x.split(',') for x in p.stdout.split()]

def counts(output):
    '''Extract counts from test_status.py output'''
    table = {}
    for line in output.split('\n'):
        try:
            value, key = line.split(maxsplit=1)
        except ValueError:
            pass
        else:
            table[key] = value
    return table
    
def parse_arguments():
    instruments = [os.path.basename(x).lower() for x in instrument_directories()]
    parser = argparse.ArgumentParser()
    parser.add_argument('instrument', type=str.lower, choices=instruments)
    parser.add_argument('startDate', help='Date in format: YYYY-MM-DD')
    parser.add_argument('endDate', help='Date in format: YYYY-MM-DD')
    parser.add_argument('-p', '--pull', action='store_true', help='Pull latest changes')
    return parser.parse_args()
    
if __name__ == '__main__':
    args=parse_arguments()
    shutil.copy(test_StatusRoot, test_StatusRootCopy, follow_symlinks=True)  
    if args.pull:
        update_node_tip()
    get_hg_rev(args.startDate,args.endDate,args.instrument) 
