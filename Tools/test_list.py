# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import os
import sys


repo_root = os.path.abspath(os.path.join(__file__, '..', '..'))
if repo_root not in sys.path:
    sys.path.insert(0, repo_root)

from Tools import test_status


def parse_args():
    instruments = [os.path.basename(x).lower() for x in test_status.instrument_directories()]
    parser = argparse.ArgumentParser()
    parser.add_argument('instrument', type=str.lower, choices=instruments)
    parser.add_argument('-m', '--modules', action='store_true', help='Print test modules')
    parser.add_argument('-c', '--cases', action='store_true', help='Print test cases')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    directory = test_status.test_directory(args.instrument)
    tests = test_status.get_tests_from_directory(directory)
    if args.modules:
        items = list(set([test.id().split('.')[0] for test in tests]))
    elif args.cases:
        items = list(set([test.id().rsplit('.', maxsplit=1)[0] for test in tests]))
    else:
        items = [test.id() for test in tests]
    items.sort(key=str.casefold)
    for item in sorted(items):
        print(item)