# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import argparse
import inspect
import os
import sys


repo_root = os.path.abspath(os.path.join(__file__, '..', '..'))
if repo_root not in sys.path:
    sys.path.insert(0, repo_root)

from Tools import test_status


def parse_args():
    instruments = [os.path.basename(x).lower() for x in test_status.instrument_directories()]
    parser = argparse.ArgumentParser()
    parser.add_argument('instrument', type=str.lower, choices=instruments, help='Name of instrument')
    parser.add_argument('-f', '--filename', help='Output filename (default is <instrument>_status.html)')
    filter_group = parser.add_mutually_exclusive_group()
    filter_group.add_argument('-m', '--matching', action='append', default=[],
                        help='Test name glob strings, may be added more than once')
    filter_group.add_argument('-t', '--test_list', help='Test list input file, one glob string per line')
    return parser.parse_args()

class HtmlGenerator():
    def __init__(self, instrument, filename=None):
        self.instrument = instrument
        self.matching = ['*.*.*']
        if filename is None:
            self.filename = f'{self.instrument}_status.html'
        else:
            self.filename = filename
        self.file = None
        self._filter_filename = None
        self.tests = []
        self.test_cases_per_module = {}
        self.tests_per_test_case = {}
        self.modules_already_written = []
        self.test_cases_already_written = []

    def add_filter_file(self, filename):
        self._filter_filename = filename
        with open(filename, 'r') as f:
            lines = [x.strip() for x in f]
        self.matching = [x for x in lines if x]

    def add_filter(self, filter):
        self.matching = filter

    def run(self):
        directory = test_status.test_directory(self.instrument)
        tests = test_status.get_tests_from_directory(directory, self.matching)
        self.tests = sorted(tests, key=test_id)
        self._count_test_cases_per_module()
        self._count_tests_per_test_case()
        self.generate_html()

    def generate_html(self):
        with open(self.filename, 'w') as self.file:
            self.header()
            self.html_body()
            self.footer()

    def header(self):
        self.write('''<!DOCTYPE html>
                 <html>
                 <head>
                 <meta charset="UTF-8">
                 <style>
                 table, th, td {
                   border: 1px solid black;
                   border-collapse: collapse;
                   position: relative
                 }
                 td {
                   padding: 5px;
                   vertical-align: top
                 }
                 td#skipped {
                   background-color: #C0C0C0
                 }
                 td#expected_fail {
                   background-color: #FF9090
                 }
                 td#implemented {
                   background-color: #90FF90
                 }
                 div#sticky {
                   position: sticky;
                   top: 0
                 }
                 </style>
                 </head>
                ''')

    def html_body(self):
        self.write('<body>')
        self.write(hg_node())
        self.write(f'Instrument: {self.instrument}</br>')
        if self._filter_filename:
            self.write(f'Filter File: {self._filter_filename}</br>')
        else:
            self.write(f'Filter: {self.matching}</br>')
        self.write('</br>')
        self.html_table()
        self.write('</body>')

    def footer(self):
        self.write('''</html>''')

    def html_table(self):
        self.write('<table>')
        for i, test in enumerate(self.tests):
            self.test_to_row(test, i)
        self.write('</table>')

    def write(self, s):
        for line in s.split('\n'):
            print(line.strip(), file=self.file)

    def test_to_row(self, test, row_number):
        cells = [self._module_table_data(test),
                 self._test_case_table_data(test),
                 self._test_table_data(test),
                 f'<td><div id="sticky">{row_number}</div></td>']

        self.write(f'<!-- {test.id()} -->')
        self.make_row('\n'.join(cells))

    def _test_table_data(self, test):
        test_name = self.split_test_id(test)[2]
        status = self._status(test)
        if status == 'skipped':
            s = f'<i>Skip Reason: {test.skip_reason()}</i><br/>\n'
        elif status == 'expected_fail':
            s = f'<i>Expected to Fail Reason: '\
                f'{test.expecting_failure_reason()}</i><br/>\n'
        else:
            s = ''
        return f'<td id="{status}"><div id="sticky">'\
               f'<b>{test_name}</b></b><br/>\n' \
               f'{s}'\
               f'{test_method_description(test)}'\
               f'</div></td>'

    def _status(self, test):
        if test.is_skipped():
            status = 'skipped'
        elif test.is_expected_to_fail():
            status = 'expected_fail'
        else:
            status = 'implemented'
        return status

    def _test_case_table_data(self, test):
        module, test_case = self.split_test_id(test)[0:2]
        long_test_case_name = f'{module}.{test_case}'
        if long_test_case_name in self.test_cases_already_written:
            data = f'<!-- {test_case} -->'
        else:
            row_span = self.tests_per_test_case[long_test_case_name]
            data = f'<td rowspan="{row_span}"><div id="sticky">'\
                   f'<b>{test_case}</b>'\
                   f'<br/>{test_case_description(test)}'\
                   f'</div></td>'
            self.test_cases_already_written.append(long_test_case_name)
        return data

    def _module_table_data(self, test):
        module = self.split_test_id(test)[0]
        if module in self.modules_already_written:
            module_cell = f'<!-- {module} -->'
        else:
            row_span = self.test_cases_per_module[module]
            module_cell = f'<td rowspan="{row_span}"><div id="sticky"><b>{module}</b>' \
                          f'<br/>{module_description(test)}</div></td>'
            self.modules_already_written.append(module)
        return module_cell

    def make_row(self, s):
        self.write(f'<tr>\n{s}\n</tr>')

    def _count_test_cases_per_module(self):
        modules = set([self.split_test_id(x)[0] for x in self.tests])
        self.test_cases_per_module = {}
        for m in modules:
            count = len([x for x in self.tests if x.id().startswith(m + '.')])
            self.test_cases_per_module[m] = count

    def _count_tests_per_test_case(self):
        test_cases = set(['.'.join(self.split_test_id(x)[0:2]) for x in self.tests])
        self.tests_per_test_case = {}
        for tc in test_cases:
            count = len([x for x in self.tests if x.id().startswith(tc)])
            self.tests_per_test_case[tc] = count

    def split_test_id(self, test):
        return test.id().split('.', maxsplit=2)


def hg_node():
    import subprocess
    node = subprocess.run('hg id -i', stdout=subprocess.PIPE, encoding='utf-8').stdout.strip()
    branch = subprocess.run('hg branch', stdout=subprocess.PIPE, encoding='utf-8').stdout.strip()
    tags = subprocess.run('hg id -t', stdout=subprocess.PIPE, encoding='utf-8').stdout.strip()
    if '+' in node:
        phase = 'Uncommitted changes'
    else:
        phase = subprocess.run('hg phase', stdout=subprocess.PIPE, encoding='utf-8').stdout.split()[-1]
    date = subprocess.run('hg log -T changelog -l 1', stdout=subprocess.PIPE, encoding='utf-8').stdout.split()[0]
    return f'''Documentation generated from FVAL source code.<br/>
               Node: {node}<br/>
               Phase: {phase}<br/>
               Branch: {branch}<br/>
               Tags: {tags}<br/>
               Date: {date}<br/>
            '''


def test_id(test):
    """key method for sorting tests"""
    return test.id().lower()


def module_description(test):
    module = inspect.getmodule(test)
    description = inspect.getdoc(module)
    return format_description(description)


def test_case_description(test):
    description = inspect.getdoc(test)
    return format_description(description)


def test_method_description(test):
    testmethod = getattr(test, test._testMethodName)
    description = inspect.getdoc(testmethod)
    return format_description(description)


def format_description(description):
    if description is None:
        description = 'No description'
    description = description.replace('\n', '<br/>\n')
    return description


if __name__ == '__main__':
    args = parse_args()
    generator = HtmlGenerator(instrument=args.instrument,
                              filename=args.filename)
    if args.test_list:
        generator.add_filter_file(args.test_list)
    elif args.matching:
        generator.add_filter(args.matching)
    generator.run()