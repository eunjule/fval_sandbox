################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import argparse
import sys
import os.path


def extractRegisterData(fileData):
    bar_regs = []
    for registerInfo in fileData:
        # localparam    P2P_CTRL                                  =   8'hc0;  // (0x0300)
        registerInfoRegex = re.compile(r'\s*localparam\s+([0-9a-zA-Z_]+)\s*=\s*\d+\'h(\w*);.*', re.IGNORECASE)
        registerInfo = registerInfoRegex.search(registerInfo)
        try:
            registerAddress = hex(int(registerInfo.group(2), 16) * 4)
            registerName = registerInfo.group(1)
            bar_regs.append((registerName, registerAddress))
        except:
            pass
    return bar_regs


def printRegisterClasses(bar_regs, registerClass, registerBar):
    for registerName, registerAddress in bar_regs:
        print('class {}({}):'.format(registerName, registerClass))
        print('    BAR = {}'.format(registerBar))
        print('    ADDR = {}'.format((registerAddress)))
        print('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]')
        print('')


def writeRegisterClasses(bar_regs, registerClass, registerBar, outputFileName):
    with open(outputFileName, 'w') as registerFile:
        for registerName, registerAddress in bar_regs:
            registerFile.write('class {}({}):\n'.format(registerName, registerClass))
            registerFile.write('    BAR = {}\n'.format(registerBar))
            registerFile.write('    ADDR = {}\n'.format((registerAddress)))
            registerFile.write('    _fields_ = [(\'Data\', ctypes.c_uint, 32)]\n\n')


def read_and_clean_input_file(fileName):
    fileData = []
    with open(fileName) as f:
        for line in f:
            fileData.append(line)
    return fileData


def output_register_classes(OutputFileName, bar_regs, args):
    if OutputFileName == None:
        printRegisterClasses(bar_regs, args.regclass, args.bar)
    else:
        if os.path.isfile(OutputFileName):
            print('Output file already exists:')
            overwrite = input('\tWould you like to overwrite existing output file {}?\n'.format(OutputFileName))
            if overwrite.lower() == 'y' or overwrite.lower == 'yes':
                print('\n\toverwriting existing register output file')
            else:
                print('\tFile already exists: Exiting')
                sys.exit(1)
        writeRegisterClasses(bar_regs, args.regclass, args.bar, OutputFileName)


def parse_args(args):
    # Parse command line options
    parser = argparse.ArgumentParser(description='VH to CTYPE register generator')
    parser.add_argument('inputFile', help='Input .vh file that contains register definitions', type=str, action="store")
    parser.add_argument('-bar', help='Bar for registers', type=int, action="store", required=True)
    parser.add_argument('-regclass', help='Register class that registers will derive from', type=str, action="store",
                        required=True)
    parser.add_argument('-o', '--outputfilename', help='optional output filename, otherwise prints to screen', type=str,
                        action="store", default=None)
    args = parser.parse_args(args)
    return args


def Main(user_args=None):
    args = parse_args(user_args)

    outputFileName = args.outputfilename
    inputFileName = args.inputFile

    try:
        with open(inputFileName):
            pass
    except FileNotFoundError:
        print('Input File \'{}\' does not exist!'.format(inputFileName))
        sys.exit(1)

    fileData = read_and_clean_input_file(inputFileName)
    bar_regs = extractRegisterData(fileData)
    output_register_classes(outputFileName, bar_regs, args)


if __name__ == '__main__':
    Main()
