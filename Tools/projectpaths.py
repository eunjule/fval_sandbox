################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: projectpaths.py
#-------------------------------------------------------------------------------
#     Purpose: Project path management / self-discovery module
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 09/16/15
#       Group: HDMT FPGA Validation
################################################################################

import os
import sys

ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

def add_to_system_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

add_to_system_path(ROOT_PATH)

if __name__ == '__main__':
    print('System Path:')
    for path in sys.path:
        print(path)
    print('')
    print('PATH Environment Variable:')
    for path in os.environ['PATH'].split(';'):
        print(path)

