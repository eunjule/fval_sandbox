################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: nightly.py
#-------------------------------------------------------------------------------
#     Purpose: Nightly regression runner
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 08/02/15
#       Group: HDMT FPGA Validation
################################################################################

import os
from os import listdir
import glob
import re
import platform
import time
import datetime
import subprocess
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

SEARCH_PATH           = r'X:\FPGAs\HPCC_AC\debug\FabD\A3_silicon'
WILDCARD_EXPR         = r'\hpcc_ac_variant0_dimms*'
MAX_IMAGES_TO_REGRESS = 5
EMAIL_FROM            = 'rodny.rodriguez@intel.com'
EMAIL_TO              = 'rodny.rodriguez@intel.com;yuan.feng@intel.com'

def FindImages():
    fpgaImages = list(filter(os.path.isdir, glob.glob(SEARCH_PATH + WILDCARD_EXPR)))
    fpgaImages.sort(key = lambda x: -os.path.getmtime(x))
    return fpgaImages[0:MAX_IMAGES_TO_REGRESS]

def SendEmail(to, subject, textMessage = '', htmlMessage = ''):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = EMAIL_FROM
    msg['To'] = to

    part1 = MIMEText(textMessage, 'plain')
    part2 = MIMEText(htmlMessage, 'html')
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via our own SMTP server, but don't include the envelope header.
    s = smtplib.SMTP('SMTP.intel.com')
    s.sendmail(EMAIL_FROM, [to], msg.as_string())
    s.quit()


def FindHpccAcFpgaImages(path):
    AC0_REGEX = re.compile('hpcc_ac_variant0_fpga0_(dimms_|mini_build_)?v\.\d\.\d\.\d\.bin')
    AC1_REGEX = re.compile('hpcc_ac_variant0_fpga1_(dimms_|mini_build_)?v\.\d\.\d\.\d\.bin')
    hpccAc0Bin = None
    hpccAc1Bin = None
    for fn in filter(os.path.isfile, glob.glob(os.path.join(path, '*.bin'))):
        print(fn)
        if AC0_REGEX.match(os.path.basename(fn)):
            hpccAc0Bin = fn
        if AC1_REGEX.match(os.path.basename(fn)):
            hpccAc1Bin = fn
    return hpccAc0Bin, hpccAc1Bin

def RunHpccAcRegression(hpccAcImagePath, jobuid, verbosity):
    # Create output directory if required
    directory = os.path.join(hpccAcImagePath, 'regression')
    if not os.path.exists(directory):
        print('Created output directory: {}'.format(directory))
        os.makedirs(directory)
    else:
        print('Output directory already exists: {}'.format(directory))
    # Run the regression
    logname = '{}.log'.format(jobuid)
    hpccAc0Bin, hpccAc1Bin = FindHpccAcFpgaImages(hpccAcImagePath)

    command = []
    if hpccAc0Bin is not None and hpccAc1Bin is not None:
        command = [
            'python', r'Tools\regress.py', 'Hpcc',
            '--overrideconfigs', 'OUTPUT_DIR={},HPCC_AC0_FPGA_IMAGE={},HPCC_AC1_FPGA_IMAGE={},HPCC_AC0_FPGA_LOAD=True,HPCC_AC1_FPGA_LOAD=True'.format(directory, hpccAc0Bin, hpccAc1Bin),
            '--logname', logname,
            '--verbosity', verbosity,
        ]
    elif hpccAc0Bin is not None:
        command = [
            'python', r'Tools\regress.py', 'Hpcc',
            '--overrideconfigs', 'OUTPUT_DIR={},HPCC_AC0_FPGA_IMAGE={},HPCC_AC0_FPGA_LOAD=True,HPCC_SLICE0_ONLY=True'.format(directory, hpccAc0Bin, hpccAc1Bin),
            '--logname', logname,
            '--verbosity', verbosity,
        ]
    else:
        print('ERROR: Failed to find FPGA images in \'{}\''.format(hpccAcImagePath))
        return
    print('Regress command: ' + ' '.join(command))
    subprocess.call(command)
    
    summary = None
    failList = None
    passList = None
    try:
        summary = open(os.path.join(directory, 'summary.txt')).read()
    except:
        pass
    try:
        failList = open(os.path.join(directory, 'failList.txt')).read()
    except:
        pass
    try:
        passList = open(os.path.join(directory, 'passList.txt')).read()
    except:
        pass
    return (summary, failList, passList)


emailContent = ''

hostname = platform.node()

for imagePath in FindImages():
    imagePath = os.path.abspath(imagePath)
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')
    jobuid = '{}-{}'.format(hostname, timestamp)
    result = RunHpccAcRegression(imagePath, jobuid, 'debug')
    if result is not None:
        summary, failList, passList = result
        emailContent = emailContent + """\
FPGA Image Path: {}

Failing Tests:
{}

Passing Tests:
{}
""".format(imagePath, failList, passList)

SendEmail(EMAIL_TO, 'HPCC AC FVAL Nightly Regression - {}'.format(hostname), emailContent, emailContent.replace('\n', '<br>'))

