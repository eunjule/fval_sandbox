# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from subprocess import run, PIPE


class Node():
    def __init__(self, rev):
        self._rev = rev
        self._user = None
        self._date = None
        self._changes = {}

    def set_user(self, user):
        self._user = user

    def user(self):
        return self._user

    def set_date(self, date):
        self._date = date

    def add_file(self, name, lines_changed):
        self._changes[name] = lines_changed

    def __str__(self):
        s = f'{self._rev},{self._date},"{self._user}"'

        for file in self._changes:
            s += f',{file},{self._changes[file]}'
        return s


nodes = []

for i in range(100000):
    print(i)
    result = run(f'hg export --rev {i}', stdout=PIPE, stderr=PIPE, encoding='cp437')
    node = Node(i)
    user = None
    filename = None
    added = 0
    removed = 0
    parent_count = 0
    for line in result.stdout.split('\n'):
        if line.startswith('# Parent'):
            parent_count += 1
            if parent_count == 2:
                break
        elif line.startswith('+++'):
            if filename is not None:
                node.add_file(filename, max(added, removed))
            added = 0
            removed = 0
            try:
                filename = line.split()[1][2:]
            except IndexError:
                # was actually an added line
                added += 1
        elif line.startswith('+'):
            added += 1
        elif line.startswith('---'):
            # might be a removed line instead of a before file
            # but this should not cause enough error to matter...
            pass
        elif line.startswith('-'):
            removed += 1
        elif line.startswith('# User'):
            user = line[7:]
            node.set_user(user)
        elif line.startswith('# Date'):
            _, _, utc, offset = line.split()
            node.set_date(utc)

    if parent_count == 2:
        print('skipped merge')
    else:
        node.add_file(filename, max(added, removed))

        if user is None:
            break
        else:
            nodes.append(node)

with open('project_activity.csv', 'w') as f:
    for node in nodes:
        print(node, file=f)



