################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.


import ast
from collections import OrderedDict
import fnmatch
import os
import queue
import re
import sys
import threading
import _thread as thread

if __name__ == '__main__':
    repo_root_path = os.path.join(os.path.dirname(__file__), '..')
    sys.path.append(os.path.abspath(repo_root_path))

from Tools import projectpaths
from ThirdParty.HIL.x64.Release import hil_utils
from Common import configs
from Common import fval
#from Common.instruments.tester import Tester
from Common.instruments.tester import get_tester

from Hpcc import hpcctb
from Tools import regress

#Global Variabled that needs to be accessed by GUI environment
USER_OUTPUT_PATH = None  # USER output Path Variable under Result Folder
TESTLIST_WIDGET = None # Test list in scrolled listbox as global widget
ACLIST_WIDGET = None # HPCC AC Slot or Slot/Slice list in scrolled listbox as global widget
TESTSEED = None # HPCC AC Test Seed Number
REGRESSIONSEED = None # HPCC AC Regression Seed Number
REPEAT = 1 # Regression Repeat Count
LOG = None
CHK_BTN_STATE = {'HPCC_ENV_INIT':'FULL'}

#fval.SetLoggerLevel(20)
#fval.ConfigLogger()
#fval.Log('info', 'LOG TEXT')
#logProxy = hpcctb.LogProxy()
#hpcctb.hpcctbc.SetCommonLogger(logProxy)
        
import tkinter
import tkinter.ttk
from tkinter.filedialog import askdirectory
from tkinter.simpledialog import askstring
from tkinter.messagebox import showwarning
from tkinter.messagebox import showinfo
#from tkinter.filedialog import askdirectory
#from tkinter.filedialog import askopenfilename

class MultiTab(tkinter.Frame):
    def __init__(self, master=None):
        tkinter.Frame.__init__(self, master)
        self.pack(fill='both', expand=1)
        
        # TTK Notebook Configuration
        self.mainWindow = tkinter.ttk.Notebook(self)
        self.mainWindow.enable_traversal()
        self.mainWindow.grid(row=0, column=0, sticky='nswe')

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)
        
        # Multiple Tab Creation
        self.CreatePanes()
    
    def CreatePanes(self):
        self.hpccAc = HpccAc(self)
        self.hddps = HdDps(self)
        
        self.mainWindow.add(self.hpccAc, text="HPCCAC", sticky='EWNS', underline=0)
        self.mainWindow.add(self.hddps, text="HDDPS", sticky='EWNS', underline=2)

# HPCCAC Tab GUI Implementation
class HpccAc(tkinter.Frame):
    def __init__(self, parent):
        tkinter.Frame.__init__(self, parent)
        self.grid(row=0, column=0, sticky='nswe')
        self.createDetails()

    def createDetails(self):
        global regress
        global fval
        global TESTLIST_WIDGET
        global ACLIST_WIDGET
        global LOG

        # Three Paned Window Widget Creation
        p1 = tkinter.PanedWindow( self, orient='horizontal')
        seedPane = tkinter.PanedWindow( self, orient='horizontal')
        p2 = tkinter.PanedWindow( self, orient='horizontal')

        # Top Paned Window Implementation
        f1 = tkinter.LabelFrame(p1, text='HPCC AC Regression Config', width=250, height=180)
        f2 = tkinter.LabelFrame(p1, text='HPCC AC Regression Run', width=250, height=180)
        f1.grid_propagate(False)  # By Setting False, the child widget inside paned window doesn't regulate the window size. Window size of widget in higher hierachy rules the window size.
        f2.grid_propagate(False)
        p1.add(f1, stretch='always')
        p1.add(f2, stretch='always')

        # Middle Paned Window Implementation
        seedEntryButton = tkinter.LabelFrame(seedPane, text='Seeds Selection', width=500, height=50)
        seedEntryButton.grid_propagate(False)
        seedPane.add(seedEntryButton, stretch='always')

        # Middle Paned Window Implementation
        scrolledList2 = tkinter.LabelFrame(p2, text='Test Selection', width=500, height=240)
        scrolledList2.grid_propagate(False)
        p2.add(scrolledList2, stretch='always')

        # Bottom Paned Window Implementation
        #f22 = tkinter.LabelFrame(p3, text='Log', width=500, height=250)
        #f22.grid_propagate(False)
        #p3.add(f22, stretch='always')
        
        # Panedwindow Grid Placement
        p1.grid(row=0, sticky='nswe')
        seedPane.grid(row=1, sticky='nswe')
        p2.grid(row=2, sticky='nswe')

        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.columnconfigure(0, weight=1)

        #log = ScrolledText(parent=f22)
        #f22.rowconfigure(0,weight=1)
        #f22.columnconfigure(0,weight=1)
        scrolledList2.rowconfigure(0,weight=1)
        scrolledList2.columnconfigure(0,weight=1)
        
        # Bottom Paned Window Widget Implementation
        #LOG = ScrolledText(parent=f22)
        #fval.SetLoggerCallback(LOG.write)

        # HPCCAC Options Setting, HPCC Regression Test List Generation, Available HPCC AC Slot/Slice 
        RadioButtonOptions = OrderedDict( [('FULL', 'FULL'), ('LOADCAL','LOADCAL'), ('Bypass CAL', 'ZEROCAL') ] )
        ChkButtonOptions = OrderedDict( [ ( 'Take Snapshot','HPCC_TAKE_SNAPSHOT' ), ('Bypass RC init','COMMON_INITIALIZE'), ('Slice 0 Only', 'HPCC_SLICE0_ONLY'), ('Slice 1 Only', 'HPCC_SLICE1_ONLY'), ('Take DCCal Snapshot', 'HPCC_TAKE_DCCAL_SNAPSHOT'), ('Loop Count', 'Dummy') ]  ) #('Collect ILAs','DummyPlaceHolder'),
        testOptions = regress.GetTestNames('Hpcc')
        hpccAcList = self.AvailableHpccAC()
        
        # Top Paned Window Widget Implementation
        hpccRadioBtnOptions = HpccAcRadioButtonOptions(parent=f1, options=RadioButtonOptions)
        hpccAcChkOptions = HpccAcChkButtonOptions(parent=f1, options=ChkButtonOptions)
        hpccRadioBtnOptions.grid( row=0, column=0 )
        hpccAcChkOptions.grid(row=1, column=0)
        regression = RegressionInterface(f2, None)
        ACLIST_WIDGET = ScrolledListGrid(root=f2)
        ACLIST_WIDGET.fillTestList(hpccAcList)
        
        # Middle Paned Window Widget Implementation
        SeedButton(['Test Seed', 'Regression Seed'], root=seedEntryButton)

        # Middle Paned Window Widget Implementation
        TESTLIST_WIDGET = ScrolledListGrid(root=scrolledList2)
        TESTLIST_WIDGET.fillTestList(testOptions)
        searchField = SearchEntryButton(['IncSearch', 'ExcSearch'], testOptions, TESTLIST_WIDGET.fillTestList, root=scrolledList2)
        testSelection = TestSelectionButton(scrolledList2, TESTLIST_WIDGET)


        # Widget inside Paned Windows Grid Packing
        ACLIST_WIDGET.grid( row=0, column=0,  sticky='news')
        regression.grid( row=0, column=1, sticky='news')
        searchField.grid(row=0, column=0, sticky='ew')  # to avoid stretching north and south
        testSelection.grid(row=0, column=1, sticky='ew') # to align with search field
        TESTLIST_WIDGET.grid(row=1, column=0, columnspan=2, sticky='news')

        #sys.stdout = LOG
        '''
        def Logging(line):
            print('testtttttttttttttt')
            LOG.write( line)
        #saveStreams = sys.stdin, sys.stdout
        #sys.stdout = log
        '''

    def AvailableHpccAC(self):
        myTester = get_tester()
        HpccAcList = []
        deviceDict = myTester.discover_all_instruments()[2]
        print(deviceDict)
        for dtup in deviceDict:
            print(dtup)
            device, slot, subslot  = dtup
            print('device:',device)
            if 'hpccAc' == device:
                HpccAcList.append( 'Slot {}'.format( slot ) )
                #HpccAcList.append( (slot, 0) )
                #HpccAcList.append( (slot, 1) )
        print('hpcclist: ',HpccAcList)
        return HpccAcList

        
##
class HdDps(tkinter.Frame):pass

# HPCC AC Radio Button that implements to selecte one of HPCC_ENV_INIT options.
class HpccAcRadioButtonOptions(tkinter.Frame):
    def __init__(self, parent=None, options=[]):
        tkinter.Frame.__init__(self, parent)
        self.grid(row=0, column=0, sticky='nswe')
        self.optionVar = tkinter.StringVar()
        self.optionVar.set('FULL')
        self.RadioButtons(parent, options)
        #print(self.optionVar.get())
    def RadioButtons(self, root, options):
        i = 0
        for (key, value) in options.items():
            radioOption = tkinter.Radiobutton(self, text = key, command = self.RegressOptionConfigure, variable=self.optionVar, value=value)
            radioOption.grid(row=0, column=i)
            i += 1
        
    def RegressOptionConfigure(self):
        global CHK_BTN_STATE
        overrides = []
        #indexForHpccEnvInit = [ i for i, j in enumerate( self.optionKey) if j == 'HPCC_ENV_INIT' ]
        
        setattr(configs, 'HPCC_ENV_INIT', self.optionVar.get()  )
        CHK_BTN_STATE['HPCC_ENV_INIT'] = self.optionVar.get()
        #print(CHK_BTN_STATE)
        
    def onPress(self):
        pick = self.optionVar.get()
        print('You pressed', pick )
        #print( 'resut:', options[pick] )

    def report(self):
        print( self.var.get() )

# HPCC AC Regression Config Implementation
class HpccAcChkButtonOptions(tkinter.Frame):
    def __init__(self, parent=None, options=[]):
        tkinter.Frame.__init__(self, parent)
        self.grid(row=0, column=0, sticky='nswe')
        self.slotStates = []
        self.sliceStates = []
        self.optionStates = []
        #self.optionStates = OrderedDict()
        self.entryStates = None
        self.optionKey = []
        self.OptionsButtons(parent, options)

    def OptionsButtons(self, root, options):
        i = 0
        for key, value in options.items():
            optionVar = tkinter.StringVar()
            if key == 'Loop Count':
                loopCountEntry = SearchEntry([str(key)], root)
                loopCountEntry.grid(row=i, sticky='w')
                rptVar = loopCountEntry.variables[0]
                rptVar.trace('w', self.SetRPTCount) 
                self.entryStates = ( rptVar ) 
            else:
                optionChk = tkinter.Checkbutton(self, text='{}'.format( str(key) ), variable=optionVar, command=self.RegressOptionConfigure, offvalue='False', onvalue='True' )
                self.optionKey.append(value)
                #self.optionStates[key] = ( value, optionVar)
            optionChk.config(anchor='w')
            optionChk.deselect()
            if key != 'HPCC_ENV_INIT':
                optionChk.grid(row=i, sticky='w')
            i += 1
            self.optionStates.append(optionVar)

    def SetRPTCount(self, a, b, c):
        #print(self.entryStates.get() )
        global REPEAT
        REPEAT =  self.entryStates.get()

    def RegressOptionConfigure1(self):
        overrides = []
        
        for key, state in zip(self.optionKey, self.optionStates):
            if key == 'HPCC_ENV_INIT':
                setattr(configs, key, (state.get())  )
                overrides.append((key, state.get()))
            else:
                setattr(configs, key, ast.literal_eval(state.get())  )
                overrides.append((key, state.get()))

    def RegressOptionConfigure(self):
        global CHK_BTN_STATE
        overrides = []
        #indexForHpccEnvInit = [ i for i, j in enumerate( self.optionKey) if j == 'HPCC_ENV_INIT' ]
        
        for key, state in zip(self.optionKey, self.optionStates):
            if key == 'HPCC_ENV_INIT':
                setattr(configs, key, (state.get())  )
                CHK_BTN_STATE[key] = state.get()
            else:
                setattr(configs, key, ast.literal_eval(state.get())  )
                CHK_BTN_STATE[key] = ast.literal_eval( state.get() )
        #print(CHK_BTN_STATE)
        #print(overrides)
        #for t in overrides:
        #    var, value = t
        #    fval.Log('debug', "Overriding config '{}' to '{}'".format(var, value))

    def report(self):
        dummyList = []
        for var in self.optionStates:
            dummyList.append( var.get() )
        #print(dummyList)

    # Slot Check Button Implementation that is not used in the current GUI
    def SlotButtons(self, root):
        for i in range(12):
            slotVar = tkinter.IntVar()
            slotChk = tkinter.Checkbutton(root, text='Slot {}'.format( str(i) ), variable=slotVar, onvalue=i)
            if i == 7:
                slotChk.select()
            slotChk.pack(side='left')
            self.slotStates.append(slotVar)

    # Slice Check Button Implementation that is not used in the current GUI
    def SliceButtons(self, root):
        for i in range(2):
            sliceVar = tkinter.IntVar()
            sliceChk = tkinter.Checkbutton(root, text='Slice {}'.format( str(i) ), variable=sliceVar, onvalue=i)
            sliceChk.select()
            sliceChk.pack(side='right')
            self.sliceStates.append(sliceVar)

# Test selection All/None Implementation
class TestSelectionButton(tkinter.Frame):
    def __init__(self, root, scrolledListObject):
        tkinter.Frame.__init__(self, root)
        self.grid(row=0, column=0, sticky='nswe')
        self.scrollListObject =  scrolledListObject
        self.makeWidgets()

    def makeWidgets(self):

        paddingX = (7, 0)
        self.selectAll = tkinter.ttk.Button(self, text ='Select All', command= self.SelectAll)
        self.selectAll.grid(row=0,column=0, sticky='nswe', padx=paddingX) 

        self.selectNone = tkinter.ttk.Button(self, text ='Select None ', command= self.SelectNone)
        self.selectNone.grid(row=0,column=1, sticky='nswe', padx=paddingX)

    def SelectAll(self):
        self.scrollListObject.selectAll()

    def SelectNone(self):
        self.scrollListObject.selectNone()

# Regression Widget Implementation
class RegressionInterface(tkinter.Frame):
    def __init__(self, root, options):
        tkinter.Frame.__init__(self, root)
        self.grid(row=0, column=0, sticky='nswe')
        self.makeWidgets(options)

    def makeWidgets(self,options):
        paddingX = (7, 0)
        paddingY = (0, 7)
        self.imageUpdate = tkinter.ttk.Button(self, text ='Flash Image', command= self.threadedFlashImage)
        self.imageUpdate.grid(row=0, sticky='nswe', padx=paddingX, pady=paddingY) 

        self.initWithoutRefresh = tkinter.ttk.Button(self, text =' Init ', command= self.dummy)
        self.initWithoutRefresh.grid(row=1, sticky='nswe', padx=paddingX, pady=paddingY)

        self.runRegression = tkinter.ttk.Button(self, text ='Run Regression', command= self.threadedRunRegress)
        self.runRegression.grid(row=2, sticky='nswe', padx=paddingX, pady=paddingY)

        self.runRegression = tkinter.ttk.Button(self, text ='Output', command= self.OutputPath)
        self.runRegression.grid(row=3, sticky='nswe', padx=paddingX, pady=paddingY)


    def dummy(self):
        print('Not Yet Implemented')

    def threadedRunRegress(self):
        global TESTLIST_WIDGET
        global ACLIST_WIDGET
        acListExist = all( isinstance(item, int) for item in ACLIST_WIDGET.slotList) and len(ACLIST_WIDGET.slotList)
        testListExist =  len(TESTLIST_WIDGET.labelList)

        if acListExist:
            if testListExist:
                #self.RunRegress()
                t1 =threading.Thread(target = self.RunRegress)
                t1.setDaemon(True)
                t1.start()
                #print('threading should end run regress')
            else :
                showwarning('Warning', 'No test is selected. Pls, select the test' ) 
        else:
            showwarning('Warning', 'Slot is not selected. Pls, select the HPCC AC Slots' ) 


    def RunRegress(self):
        global USER_OUTPUT_PATH
        global TESTLIST_WIDGET
        global ACLIST_WIDGET
        global REPEAT
        global TESTSEED
        global REGRESSIONSEED

        #sys.stdout = LOG
        OUTPUT_PATH = os.path.join(projectpaths.ROOT_PATH, configs.RESULTS_DIR)
        if not(os.path.isdir(OUTPUT_PATH)):
            os.makedirs(OUTPUT_PATH)

        fileName = os.path.join( OUTPUT_PATH, 'temp_testList')
        with open( fileName, 'w') as tempFile:
            for i in TESTLIST_WIDGET.labelList:
                 tempFile.write( i + '\n')
    
        #print( CHK_BTN_STATE['HPCC_ENV_INIT'] )
        print('slots:',ACLIST_WIDGET.slotList)

        if ( TESTSEED and not REGRESSIONSEED ):
            success = regress.RunRegression(
                overrideconfigs = ['HPCC_ENV_INIT=NONE'],
                output = USER_OUTPUT_PATH,
                slots = ACLIST_WIDGET.slotList,
                repeat = int( REPEAT),
                testlist = fileName,
                duts = ['Hpcc'],
                testseed = TESTSEED,
                )
        elif ( not TESTSEED and REGRESSIONSEED):
            success = regress.RunRegression(
                overrideconfigs = ['HPCC_ENV_INIT=NONE'],
                output = USER_OUTPUT_PATH,
                slots = ACLIST_WIDGET.slotList,
                repeat = int( REPEAT),
                testlist = fileName,
                duts = ['Hpcc'],
                regressionseed = REGRESSIONSEED,
                )

        elif ( TESTSEED and REGRESSIONSEED):
            success = regress.RunRegression(
                overrideconfigs = ['HPCC_ENV_INIT=NONE'],
                output = USER_OUTPUT_PATH,
                slots = ACLIST_WIDGET.slotList,
                repeat = int( REPEAT),
                testlist = fileName,
                duts = ['Hpcc'],
                testseed = TESTSEED,
                regressionseed = REGRESSIONSEED,
                )
        else:
            success = regress.RunRegression(
                overrideconfigs = ['HPCC_ENV_INIT=NONE'],
                output = USER_OUTPUT_PATH,
                slots = ACLIST_WIDGET.slotList,
                repeat = int( REPEAT),
                testlist = fileName,
                duts = ['Hpcc'],
        )

        # Other regress.RunRegression options not used in GUI
        #regressionseed = args.regressionseed,
        #testseed = args.testseed,
        #uniqueoutput = args.uniqueoutput,
        #nocolor = args.nocolor,
        #overrideconfigs = args.overrideconfigs.split(','),
        #verbosity = args.verbosity,
        
        #if success:
        #    sys.exit(0)
        #else:
        #    sys.exit(1)
        
        os.remove(fileName)

    def OutputPath(self):
        global USER_OUTPUT_PATH

        OUTPUT_PATH = os.path.join(projectpaths.ROOT_PATH, configs.RESULTS_DIR)
        userOutputFolderName = askstring('Entry', 'Enter Output Folder Name. Folder will be created under Results' )
        if userOutputFolderName:
            USER_OUTPUT_PATH = os.path.join(OUTPUT_PATH, userOutputFolderName)
        else:
            USER_OUTPUT_PATH = OUTPUT_PATH
        #print(USER_OUTPUT_PATH)


    def threadedFlashImage(self):
        initialDir = r'X:\FPGAs\HPCC_AC\debug\FabD\A3_silicon'
        imagePath = askdirectory( initialdir = initialDir)
        #print(imagePath)
        
        if imagePath != '' :
            setattr(configs, 'HPCC_AC_FPGA_PATH',  imagePath  )
        else:
            showwarning('Warning', 'HPCC_AC_FPGA_PATH is still set to the path {}'.format(getattr(configs, 'HPCC_AC_FPGA_PATH') ) )
            return
        
        #print('HPCC_AC_FPGA_PATH is set to {}'.format(getattr(configs, 'HPCC_AC_FPGA_PATH'))  )
        #print( getattr(configs, 'HPCC_AC_FPGA_PATH') )
        #print(ACLIST_WIDGET.slotList)
        acListExist = all( isinstance(item, int) for item in ACLIST_WIDGET.slotList) and len(ACLIST_WIDGET.slotList)
        if acListExist:
            #self.FlashImage()
            t =threading.Thread(target = self.FlashImage)
            t.setDaemon(True)
            t.start()
            #print('threading should end')
        else:
            showwarning('Warning', 'Slot is not selected. Pls, select the HPCC AC Slots' ) 
        #startThread( action=self.FlashImage, args=())

    def FlashImage(self):
        global hpcctb
        global fval
        global CHK_BTN_STATE
    
        OUTPUT_PATH = os.path.join(projectpaths.ROOT_PATH, configs.RESULTS_DIR)
        if not(os.path.isdir(OUTPUT_PATH)):
            os.makedirs(OUTPUT_PATH)

        fileName = os.path.join( OUTPUT_PATH, 'temp_testList')
        with open( fileName, 'w') as tempFile:
            for i in ['']:
                 tempFile.write( i + '\n')

        #print( CHK_BTN_STATE['HPCC_ENV_INIT'] )
        override = []
        if CHK_BTN_STATE['HPCC_ENV_INIT'] == 'ZEROCAL':
            override.append( 'HPCC_ENV_INIT=ZEROCAL' )
        elif CHK_BTN_STATE['HPCC_ENV_INIT'] == 'LOADCAL':
            override.append( 'HPCC_ENV_INIT=LOADCAL' )
        else:
            override.append( 'HPCC_ENV_INIT=FULL' )
        
        success = regress.RunRegression(
            overrideconfigs = override,
            output = USER_OUTPUT_PATH,
            slots = ACLIST_WIDGET.slotList,
            repeat = int( 1 ),
            testlist = fileName,
            duts = ['Hpcc'],
        )

        os.remove(fileName)

        #hpcctb.Env.Initialize(ACLIST_WIDGET.slotList)
        # Run initializer hooks
        '''
        passInit = True
        try:
            fval.Log('info', 'test Run Initializer')
            fval.RunInitializers()
        except Exception as e:
            exceptionMsg = traceback.format_exc().split('\n')
            for line in exceptionMsg:
                fval.Log('error', line)
            passInit = False
        '''
        #hpcctb.Env.Finalize()

# Regression/Test Seed Button Widget Implementation
class SeedButton(tkinter.Frame):
    def __init__(self, fields,  root = None):
        tkinter.Frame.__init__(self, root)
        self.grid(row=0, column=0, sticky='nswe')
        self.variables = {}
        self.entryForms = {}
        self.buttons = {}
        self.makeForm(fields)

    def fetch(self, field):
        global TESTSEED
        global REGRESSIONSEED
        seedNumber = str(self.variables[field].get() ) 
        #print(field)
        #print(seedNumber)
        if field == "Test Seed":
            TESTSEED = int(seedNumber)
        elif field == "Regression Seed":
            REGRESSIONSEED = int(seedNumber)

    def makeForm(self, fields):
        tsVar = tkinter.StringVar()
        rsVar = tkinter.StringVar()
        i = 0
        for field in fields:
            if field == "Test Seed":
                self.entryForms[field] = tkinter.Entry(self, width=27, relief='sunken', textvariable=tsVar)
                self.variables[field] = tsVar
                self.buttons[field] = tkinter.Button(self, text=field, command=(lambda: self.fetch('Test Seed') ) )
            elif field == "Regression Seed":
                self.entryForms[field] = tkinter.Entry(self, width=27, relief='sunken', textvariable=rsVar)
                self.variables[field] = rsVar
                self.buttons[field] = tkinter.Button(self, text=field, command=(lambda: self.fetch('Regression Seed') ) )
            self.buttons[field].grid(row=0, column=i+1, sticky='nswe',padx=(0,7))
            self.entryForms[field].grid(row=0, column=i, sticky='nswe')
            tsVar.set('0')
            rsVar.set('0')
            self.rowconfigure(0, weight=1)
            self.columnconfigure(0, weight=1)
            i += 2

# Test Search Button Widget Implementation
class SearchEntryButton(tkinter.Frame):
    def __init__(self, fields, allTests, refreshFunc,  root = None):
        tkinter.Frame.__init__(self, root)
        self.grid(row=0, column=0, sticky='nswe')
        self.makeForm(fields, allTests, refreshFunc)
        self.matchedTestList = []
        self.antiMatchedTestList = []

    def fetch(self, entry, allTestList, refresh):
        searchedStr = str(entry.get() ) 
        reSearchedStr = fnmatch.translate(  searchedStr  )
        ref = "^[a-zA-Z0-9\.\*\?]*$"
        if re.match(ref, searchedStr):
            self.matchedTestList = [ i for i in allTestList if re.match(reSearchedStr, i, re.IGNORECASE) ]
            #print( 'Input => {}, matchedList is {} '.format( entry.get(), self.matchedTestList ) )
        else:
            self.matchedTestList = allTestList
        refresh(self.matchedTestList)

    def antifetch(self, entry, allTestList, refresh):
        searchedStr = str(entry.get() ) 
        reSearchedStr = fnmatch.translate(  searchedStr  )
        ref = "^[a-zA-Z0-9\.\*\?]*$"
        if re.match(ref, searchedStr):
            self.antiMatchedTestList = [ i for i in allTestList if not re.match(reSearchedStr, i, re.IGNORECASE) ]
            #print( 'Input => {}, matchedList is {} '.format( entry.get(), self.matchedTestList ) )
        else:
            self.antiMatchedTestList = allTestList
        refresh(self.antiMatchedTestList)

    def makeForm(self, fields, allTests, refreshFunc):
        entryForms = {}
        allTestList = allTests
        i = 0
        for field in fields:
            if field =='IncSearch':
                ent = tkinter.Entry(self, width=20, relief='sunken')
                entryForms['IncSearch'] = ent
                lab = tkinter.Button(self, text=field, command=(lambda: self.fetch(entryForms['IncSearch'], allTestList, refreshFunc) ) )
            elif field == 'ExcSearch':
                ent = tkinter.Entry(self, width=20, relief='sunken')
                entryForms['ExcSearch'] = ent
                lab = tkinter.Button(self, text=field, command=(lambda: self.antifetch(entryForms['ExcSearch'], self.matchedTestList, refreshFunc) ) )
            lab.grid(row=i, column=1, sticky='nswe')
            ent.grid(row=i, column=0, sticky='nswe')
            self.rowconfigure(0, weight=1)
            self.columnconfigure(0, weight=1)
            i += 1

# Regression Loop Count Entry Box
class SearchEntry(tkinter.Frame):
    def __init__(self, fields, root = None):
        tkinter.Frame.__init__(self, root)
        self.grid(row=0, column=0, sticky='nswe')
        self.variables = []
        self.makeForm(fields)

    def fetch(self):
        for entry in self.variables:
            print( 'Repeat => "%s" ' % entry.get() )

    def makeForm(self, fields):
        entries = [ ]
        var = tkinter.StringVar()
        i = 0
        for field in fields:
            lab = tkinter.Label(self, width=10, text=field, relief='ridge')
            ent = tkinter.Entry(self, width=5, relief='sunken', textvariable=var )
            lab.grid(row=i, column=1, sticky='nswe')
            ent.grid(row=i, column=0, sticky='nswe')
            var.set('1')
            self.variables.append(var)

# Scrolled Textbox Widget implementation for Log Windoe
class ScrolledText(tkinter.Frame):
    def __init__(self, parent=None, text='test', file=None):
        tkinter.Frame.__init__(self, parent)
        self.grid(row=0, column=0, sticky='nswe')
        self.makeWidgets()
        #self.write(text)

    def makeWidgets(self):
        sbar = tkinter.Scrollbar(self)
        text = tkinter.Text(self, relief='sunken')
        sbar.config(command=text.yview)
        text.config(yscrollcommand=sbar.set)
        
        sbar.grid(row=0, column=1, sticky='ns' )
        text.grid(row=0, column=0 , sticky='nswe')
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        
        self.text = text

    def Logging(self, line):
        print('testtttttttttttttt')
        self.write( line)

    def write(self, text):
        self.text.insert('end', str(text))
        self.text.see('end')
        self.text.update()
    
    def writelines(self, lines):
        for line in lines: self.write(line)

    def settext(self, text='', file=None):
        if file:
            text = open(file, 'r').read()
        #self.text.delete('1.0', tkinter.END)
        #self.text.insert('1.0', text)
        self.text.insert('end', str(text))
        self.text.see('end')
        self.text.update()

        #self.text.insert('end', text)
        #self.text.mark_set(tkinter.INSERT, '1.0')
        #self.text.focus()

    def gettext(self):
        return self.text.get('1.0', tkinter.END+'-1c')


# Scrolled Listbox for either HPCC AC Slices or HPCC Tests.
class ScrolledListGrid(tkinter.Frame):
    def __init__(self,  root = None):
        tkinter.Frame.__init__(self, root)
        self.grid(row=0, column=0, sticky='nswe')
        self.labelList = []
        self.slotList = []
        self.makeWidgets()
    
    def makeWidgets(self):
        sbar = tkinter.Scrollbar(self)
        list = tkinter.Listbox(self, relief='sunken', selectmode='extended', exportselection=0)
        sbar.config(command = list.yview)
        list.config(yscrollcommand = sbar.set)
        sbar.grid(row=0, column=1, sticky='nswe')
        list.grid(row=0, column=0, sticky='nswe')
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        '''
        pos = 0
        for label in options:
            list.insert(pos, label)
            pos += 1
        '''
        # list.bind
        list.bind('<<ListboxSelect>>', self.handleList)
        self.listbox = list

    def runCommand(self, selection):
        print('You selected ', selection)

    def fillTestList(self, options):
        self.listbox.delete(0, 'end')
        pos = 0
        for label in options:
            self.listbox.insert(pos, label)
            pos += 1

    def selectAll(self):
        self.listbox.select_set(0, 'end')
        self.handleListNoEvent()
    
    def selectNone(self):
        self.listbox.select_clear(0, 'end')
        self.handleListNoEvent()

    def handleListNoEvent(self):
        tempList = []
        index = self.listbox.curselection()
        index = [int(x) for x in index]
        for i in index :
            label = self.listbox.get( i )
            tempList.append(label)
        self.labelList = tempList
        self.slotList = [ int(s) for sl in tempList for s in sl.split() if s.isdigit()]
        #label = self.listbox.get('active')
        #print(self.labelList)
        #self.runCommand(self.labelList)

    def handleList(self, event):
        tempList = []
        index = self.listbox.curselection()
        index = [int(x) for x in index]
        for i in index :
            label = self.listbox.get( i )
            tempList.append(label)
        self.labelList = tempList
        self.slotList = [ int(s) for sl in tempList for s in sl.split() if s.isdigit()]
        #label = self.listbox.get('active')
        #print(self.labelList)
        #self.runCommand(self.labelList)


# Multi Thread implementation using Pool in GUI. Tried, but not working, so using simpler and straightforward way
threadQueue = queue.Queue(maxsize=0)
def ThreadChecker(widget, delayMsecs=100, perEvent=1):
    for i in range(perEvent):
        try: 
            (callback, args) = threadQueue.get(block=False)
        except queue.Empty:
            break
        else:
            callback(*args)
    
    widget.after(delayMsecs, lambda: ThreadChecker(widget, delayMsecs, perEvent) )

def threaded(action, args):
    threadQueue.put( (action, args) )

def startThread(action, args): 
    thread.start_new_thread(
            threaded, (action, args) )

#If ran by itself
if __name__ == '__main__':

    root = tkinter.Tk()
    tab = MultiTab(root)
    root.title('Regression GUI')
    #sys.stdout = LOG
    #ThreadChecker(tab)
    tab.mainloop()
