################################################################################
# INTEL CONFIDENTIAL - Copyright 2018. Intel Corporation All Rights Reserved.
# 
# The source code contained or described herein and all documents related to
# the source code ("Material") are owned by Intel Corporation or its
# suppliers or licensors. Title to the Material remains with Intel
# Corporation or its suppliers and licensors. The Material contains trade
# secrets and proprietary and confidential information of Intel or its
# suppliers and licensors. The Material is protected by worldwide copyright
# and trade secret laws and treaty provisions. No part of the Material may be
# used, copied, reproduced, modified, published, uploaded, posted,
# transmitted, distributed, or disclosed in any way without Intel's prior
# express written permission.

import re
import sys
import io
import os
import argparse


from os.path import join, isdir, isfile

repo_root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

sys.path.append(os.path.abspath(repo_root_path))

from Hpcc.instrument import ad9914_registers
import Hpcc.instrument.hpccAcRegs as ac_registers
from ThirdParty.HIL.x64.Release import hil


def find_rising_edge(sample_list):
    previous_sample = sample_list[0][0]
    for i in range(len(sample_list)):
        current_sample = sample_list[i][0]
        if current_sample > previous_sample:
            rising_edge_delay = sample_list[i][1]
            rising_edge_sample = sample_list[i][0]
            break
        else:
            previous_sample = current_sample
    return rising_edge_delay, rising_edge_sample


def get_sample_delay_tuple_list(slot_under_test,slice_under_test):
    sample_list = []
    number_of_offsets = 2048
    number_of_samples = 100
    for phase_offset in range(number_of_offsets):
        delay_to_set = phase_offset * (2 ** 5)
        set_delay(delay_to_set,slot_under_test,slice_under_test)
        latch_state = 0
        for each_sample in range(number_of_samples):
            latch_state += hpcc_read('SyncStartEdgeResult',slot_under_test,slice_under_test).Count & 0x1
        sample = latch_state / number_of_samples
        sample_list.append((sample, delay_to_set))
    return sample_list

def set_delay(delay_to_set,slot_under_test,slice_under_test):
    r = ad9914_registers.ProfilePhaseAmplitudeRegister()
    r.phase_offset = delay_to_set
    r.amplitude_scale_factor = 0 # not used except in OSK mode
    write_to_hpcc(r.offset(profile=0), r,slot_under_test,slice_under_test)

def write_to_hpcc(address, data,slot_under_test,slice_under_test):
    if not isinstance(data, int):
        data = data.Pack()
    regAD9914WRData = ac_registers.AD9914WRData(data)
    hpcc_write('AD9914WRData', regAD9914WRData,slot_under_test,slice_under_test)

    regAD9914Control = hpcc_read('AD9914Control',slot_under_test,slice_under_test)
    # print('Data in AD9914Control register: {}'.format(hex(regAD9914Control.Pack())))
    regAD9914Control.Address = address
    regAD9914Control.RdEnable = 0
    regAD9914Control.EnableAction = 1
    hpcc_write('AD9914Control', regAD9914Control,slot_under_test,slice_under_test)

    for attempt in range(100):
        if hpcc_read('AD9914Control',slot_under_test,slice_under_test).Ad9914AccessComplete:
            break
    else:
        print('Write 0x{:X} to Ad9914 address 0x{:X} for hpcc {} slice {} Failed. Tried {} times.'.format(data,
                                                                                                             address,
                                                                                                             slot_under_test,
                                                                                                             slice_under_test,
                                                                                                             attempt))


def hpcc_write(registerName, data,slot_under_test,slice_under_test):
    register = getattr(ac_registers, registerName)
    if type(data) is int:
        return bar_write(register.BAR, register.ADDR, data,slot_under_test,slice_under_test)
    else:
        return bar_write(register.BAR, register.ADDR, data.value,slot_under_test,slice_under_test)

def bar_write(bar, offset, data,slot_under_test,slice_under_test):
    return hil.hpccAcBarWrite(slot_under_test, slice_under_test, bar, offset, data)

def hpcc_read(registerName,slot_under_test,slice_under_test):
    register = getattr(ac_registers, registerName)
    return register(bar_read(register.BAR, register.ADDR,slot_under_test,slice_under_test))

def bar_read(bar, offset,slot_under_test,slice_under_test):
    return hil.hpccAcBarRead(slot_under_test, slice_under_test, bar, offset)

def write_distribution_to_csv(sample_list,rising_edge_delay):
    SaveDir = os.getcwd()
    SaveFileFmt = join(SaveDir, "Ad9914SyncDistribution_slice0_edge_delay_{}.csv".format(rising_edge_delay))
    if not isfile(SaveFileFmt):
        filename = SaveFileFmt
    else:
        SaveFileFmt = join(SaveDir, "Ad9914SyncDistribution_slice1_edge_delay_{}.csv".format(rising_edge_delay))
        filename = SaveFileFmt
    write_distribution_to_file(filename, sample_list)

def write_distribution_to_file(filename, sample_list, mode='w'):
    with open(filename, mode) as fp:
        write_distribution(fp, sample_list)


def write_distribution(ioStream, sample_list):
    assert isinstance(ioStream, io.IOBase)
    for syncState in sample_list:
        print("{},{}".format(syncState[0], syncState[1]), file=ioStream)

def Main(user_args=None):
    args = parse_args(user_args)
    slot_under_test = args.slot
    for slice_under_test in range (2):
        sample_list = get_sample_delay_tuple_list(slot_under_test,slice_under_test)
        rising_edge_delay, rising_edge_sample = find_rising_edge(sample_list)
        write_distribution_to_csv(sample_list,rising_edge_delay)

def parse_args(args):
    # Parse command line options
    parser = argparse.ArgumentParser(description='VH to CTYPE register generator')
    parser.add_argument('-slot', help='hpcc_slot_number', type=int, action="store", required=True)
    args = parser.parse_args(args)
    return args


if __name__ == '__main__':
    repo_root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    sys.path.append(os.path.abspath(repo_root_path))
    Main()
