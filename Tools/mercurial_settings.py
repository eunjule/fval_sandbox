# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from configparser import ConfigParser
from getpass import getuser
from os.path import basename, dirname, isfile, join, normpath


_root = normpath(join(dirname(__file__), '..'))
repo_name = basename(_root)
repository_settings_file = join(_root, '.hg', 'hgrc')
global_settings_file = join('c:\\', 'Users', getuser(), 'mercurial.ini')


def check_repository_user_name_is_set():
    try:
        name = get_repository_specific_user_name()
    except FileNotFoundError:
        # If there is no hgrc file, the user name does not need to be set
        pass
    else:
        if not name:
            raise UserNameError('The repository settings username must be set'
                                ' (the global setting is not sufficient)\n'
                                f'In TortoiseHg: File->Settings->{repo_name} '
                                f'repository settings->Commit->Username\n'
                                f'this should update {repository_settings_file}')


def get_repository_specific_user_name():
    return _get_user_name(repository_settings_file)


def _get_user_name(settings_file):
    config = ConfigParser()
    if not isfile(settings_file):
        raise FileNotFoundError
    config.read(settings_file)
    return config['ui'].get('username', '')


class UserNameError(Exception):
    pass


if __name__ == '__main__':
    print(f'Logged in user name: {getuser()}')
    try:
        print(f'global user name: {_get_user_name(global_settings_file)}')
        print(f'global settings file: {global_settings_file}')
    except FileNotFoundError:
        print(f'No settings file at: {global_settings_file}')
    try:
        print(f'repository user name: {get_repository_specific_user_name()}')
        print(f'repository settings file: {repository_settings_file}')
    except FileNotFoundError:
        print(f'No settings file at: {repository_settings_file}')
