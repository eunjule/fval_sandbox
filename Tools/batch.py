################################################################################
#  INTEL CONFIDENTIAL - Copyright 2015. Intel Corporation All Rights Reserved.
#-------------------------------------------------------------------------------
#    Filename: batch.py
#-------------------------------------------------------------------------------
#     Purpose: Batch regressions script
#-------------------------------------------------------------------------------
#  Created by: Rodny Rodriguez
#        Date: 06/08/15
#       Group: HDMT FPGA Validation
################################################################################

import os
from os import listdir
import glob
import re
import platform
import time
import datetime
import subprocess
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

SEARCH_PATH = r'X:\Users\fpgaval\jobs'
RESULT_PATH = r'X:\Users\fpgaval\jobs'
SEARCH_WILDCARD = '*.job'
EMAIL_FROM = 'rodny.rodriguez@intel.com'

# "Job" variables
jobname   = None
hostname  = None
timestamp = None
jobuid    = None
email     = None
verbosity = None

def FindJobs():
    # Get all the files in SEARCH_PATH that match the SEARCH_WILDCARD
    files = list(filter(os.path.isfile, glob.glob(os.path.join(SEARCH_PATH, SEARCH_WILDCARD))))
    # Sort by file modified time, get older files first
    files.sort(key = lambda x: os.path.getmtime(x))
    return [os.path.abspath(f) for f in files]

def SendEmail(to, subject, textMessage = '', htmlMessage = ''):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = EMAIL_FROM
    msg['To'] = to

    part1 = MIMEText(textMessage, 'plain')
    part2 = MIMEText(htmlMessage, 'html')
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via our own SMTP server, but don't include the envelope header.
    s = smtplib.SMTP('SMTP.intel.com')
    #s.sendmail(me, [you], msg.as_string())
    #s.quit()


def FindHpccAcFpgaImages(path):
    AC0_REGEX = re.compile('hpcc_ac_variant0_fpga0_(dimms_|mini_build_)?v\.\d\.\d\.\d\.bin')
    AC1_REGEX = re.compile('hpcc_ac_variant0_fpga1_(dimms_|mini_build_)?v\.\d\.\d\.\d\.bin')
    hpccAc0Bin = None
    hpccAc1Bin = None
    for fn in filter(os.path.isfile, glob.glob(os.path.join(path, '*.bin'))):
        print(fn)
        if AC0_REGEX.match(os.path.basename(fn)):
            hpccAc0Bin = fn
        if AC1_REGEX.match(os.path.basename(fn)):
            hpccAc1Bin = fn
    return hpccAc0Bin, hpccAc1Bin

def RunHpccAcRegression(hpccAcImagePath):
    # Create output directory if required
    directory = os.path.join(RESULT_PATH, jobname)
    if not os.path.exists(directory):
        print('Created output directory: {}'.format(directory))
        os.makedirs(directory)
    else:
        print('Output directory already exists: {}'.format(directory))
    # Run the regression
    logname = '{}.log'.format(jobuid)
    hpccAc0Bin, hpccAc1Bin = FindHpccAcFpgaImages(hpccAcImagePath)

    command = []
    if hpccAc0Bin is not None and hpccAc1Bin is not None:
        command = [
            'python', r'Tools\regress.py', 'Hpcc',
            '--overrideconfigs', 'OUTPUT_DIR={},HPCC_AC0_FPGA_IMAGE={},HPCC_AC1_FPGA_IMAGE={},HPCC_AC0_FPGA_LOAD=True,HPCC_AC1_FPGA_LOAD=True'.format(directory, hpccAc0Bin, hpccAc1Bin),
            '--logname', logname,
            '--verbosity', verbosity,
        ]
    elif hpccAc0Bin is not None:
        command = [
            'python', r'Tools\regress.py', 'Hpcc',
            '--overrideconfigs', 'OUTPUT_DIR={},HPCC_AC0_FPGA_IMAGE={},HPCC_AC0_FPGA_LOAD=True,HPCC_SLICE0_ONLY=True'.format(directory, hpccAc0Bin, hpccAc1Bin),
            '--logname', logname,
            '--verbosity', verbosity,
        ]
    else:
        print('ERROR: Failed to find FPGA images in \'{}\''.format(hpccAcImagePath))
        return
    print('Regress command: ' + ' '.join(command))
    subprocess.call(command)

    #SendEmail(email, '')

for jobfn in FindJobs():
    script = open(jobfn, 'r').read()
    # Set defaults
    jobname, ext = os.path.splitext(os.path.basename(jobfn))
    hostname = platform.node()
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')
    jobuid = '{}-{}'.format(hostname, timestamp)
    email = 'rodny.rodriguez@intel.com'
    verbosity = 'info'
    exec(script, globals(), locals())

