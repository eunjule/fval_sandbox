# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import csv
import datetime
import time

from Common import hilmon as hil
from PinControlFunc import *
import RM_I2C_SPI as RM_I2C_SPI

Duts = 8
Channels = 175
PSDBNum = 0

hil.hbiPsdbInit(PSDBNum)

#enable pin output.  not sure if this is needed anymore..
# for idx in range(2):
    # hil.hbiPsdbBarWrite(PSDBNum,idx,0,0x3c,0xF)
    # hil.hbiPsdbBarWrite(PSDBNum,idx,1,0x3c,0xF)
    # hil.hbiPsdbBarWrite(PSDBNum,idx,2,0x3c,0xF)
    # hil.hbiPsdbBarWrite(PSDBNum,idx,3,0x3c,0xF)

def set_dac(chan,voltage):
    RM_I2C_SPI.RM_W_Vref(PSDBNum, chan, voltage)
    return hil.hbiPsdbVmonRead(PSDBNum, 8)

def set_all_dacs(setv):
    for EachDAC in range(4):
        set_dac(EachDAC, setv)
        #print('DAC CH',EachDAC,'set to',setv,'v')
    #can read chan 0 via ADC:
    return hil.hbiPsdbVmonRead(PSDBNum, 8)
    #print('Vref 0 ADC read:',Vref0read,'\tErr:',setv - Vref0read)

def get_duty_cycle(dut,pin,samples=1000):
    sum=0
    for x in range(samples):
        sum+=int(ReadPin(dut,pin))
    return sum/samples

#--------------------------------------------------------------------------
testId = ('PSDB_EvenToOddLoopback_PerPinDutyCycle') # Add unique identifier for a test run ex: testing sample rate, this will be added to output file name and content 
d = os.path.dirname("D:\\HBI\Data\\")
if not os.path.exists(d):
    os.makedirs(d)

data_save_path = 'D:\\HBI\Data\\'
i = datetime.datetime.now()
rev = i.minute 
file_suffix = ("_%s_%s_%s_%s_%s.csv" %(i.month, i.day, i.year, i.hour, rev ))
file_name = data_save_path+testId+file_suffix
if os.path.isfile(file_name):
    rev += i.second
    print (rev)
    file_suffix = ("_%s_%s_%s_%s_%s_%s.csv" %(i.month, i.day, i.year, i.hour, i.minute, rev ))
    file_name = data_save_path+testId+file_suffix
else:
    print ("")

csvfile = open(file_name, 'w', newline = '')
writer = csv.writer(csvfile)
_=writer.writerow( ['Test ID','PSDB Index','Dut','Channel','VCCIO','VREF','Samples','DutyCycle','TimeStamp'] )
#---------------------------------------------------------------------------

sampleCount=10000
vccio = 1.8 #pin strapped default

VerifyUpdateFPGAs()

for i in range(1,4):
    vref = vccio*i/4
    #set vref on all ch
    vref_adc = set_all_dacs(vref)
    for psdb in range(1,2):
        for dut in range(4):
            for channel in range(0, 172):
                print('here')
                i = datetime.datetime.now()
                input_channel = channel + 1

                if channel % 2:
                    input_channel = channel - 1
                else:
                    input_channel = channel + 1
                if channel == 172:
                    SetPin(psdb, dut, 174, PIN_INPUT)
                elif channel == 173:
                    SetPin(psdb, dut, 174, PIN_INPUT)
                elif channel == 174:
                    SetPin(psdb, dut, 172, PIN_INPUT)
                    input_channel = 173

                print('*'*100)
                print('\nSet to knonw values')
                # SetPin(psdb, dut, channel, PIN_LOW)
                # SetPin(psdb, dut, channel, PIN_HIGH)
                SetPin(psdb, dut, channel, PIN_LOW)
                SetPin(psdb, dut, input_channel, PIN_LOW)

                print('Set driver pin to 0 and read expected')
                SetPin(psdb, dut, input_channel, PIN_INPUT)
                # SetPin(psdb, dut, channel, PIN_LOW)
                observed =ReadPin(psdb, dut, input_channel)
                print('Channel: {} Expected: {} Observed: {}'.format(input_channel, '0', observed))
                assert (observed == 0)

                print('Set driver pin to 1 and read expected')
                SetPin(psdb, dut, channel, PIN_HIGH)
                # SetPin(psdb, dut, channel+2, PIN_HIGH)
                # time.sleep(2)
                observed =ReadPin(psdb, dut, input_channel)
                assert (observed == 1)
                print('Toggle: Channel: {} Expected: {} Observed: {}'.format(input_channel, '1', observed))
                print('Set both channels to 0')
                SetPin(psdb, dut, channel, PIN_LOW)
                SetPin(psdb, dut, input_channel, PIN_LOW)

csvfile.close()

# SCRATCH_PAGE                        = 'h0000,
# VERSION_PAGE                        = 'h0020,
# RING_BUS_PAGE                       = 'h0040,
# PM_RECONFIG_PAGE                    = 'h0060,
# OPEN_PAGE_PLEASE_USE                = 'h0080,
# DUT_INPUT_PAGE0                     = 'h00A0,
# VOLTAGE_SENSOR_PAGE                 = 'h00C0,
# PM_RING_BUS_COUNT_PAGE              = 'h00E0,
# RM_RING_BUS_COUNT_PAGE              = 'h0100,
# RM_RECONFIG_PAGE                    = 'h0120,
# CLOCK_FREQUENCY_PAGE                = 'h0140,
# FIXED_DRIVE_STATE_CONTROL_L         = 'h0160,
# FIXED_DRIVE_STATE_CONTROL_U         = 'h0180,
# CHANNEL_CFG_PAGE                    = 'h0200,


# //----------
# FIXED_DRIVE_STATE_CONTROL0_CH15_CH00 = FIXED_DRIVE_STATE_CONTROL_L + 'h00,
# FIXED_DRIVE_STATE_CONTROL0_CH31_CH16 = FIXED_DRIVE_STATE_CONTROL_L + 'h04,
# FIXED_DRIVE_STATE_CONTROL0_CH34_CH32 = FIXED_DRIVE_STATE_CONTROL_L + 'h08,
# FIXED_DRIVE_STATE_CONTROL1_CH15_CH00 = FIXED_DRIVE_STATE_CONTROL_L + 'h0C,
# FIXED_DRIVE_STATE_CONTROL1_CH31_CH16 = FIXED_DRIVE_STATE_CONTROL_L + 'h10,
# FIXED_DRIVE_STATE_CONTROL1_CH34_CH32 = FIXED_DRIVE_STATE_CONTROL_L + 'h14,
# FIXED_DRIVE_STATE_CONTROL2_CH15_CH00 = FIXED_DRIVE_STATE_CONTROL_L + 'h18,
# FIXED_DRIVE_STATE_CONTROL2_CH31_CH16 = FIXED_DRIVE_STATE_CONTROL_L + 'h1C,
# //----------                           
# FIXED_DRIVE_STATE_CONTROL2_CH34_CH32 = FIXED_DRIVE_STATE_CONTROL_U + 'h00,
# FIXED_DRIVE_STATE_CONTROL3_CH15_CH00 = FIXED_DRIVE_STATE_CONTROL_U + 'h04,
# FIXED_DRIVE_STATE_CONTROL3_CH31_CH16 = FIXED_DRIVE_STATE_CONTROL_U + 'h08,
# FIXED_DRIVE_STATE_CONTROL3_CH34_CH32 = FIXED_DRIVE_STATE_CONTROL_U + 'h0C,

# The controls are:
# 2’h0: drive 0
# 2’h1: drive 1
# 2’h2: drive Z (input)
# 2’h3: drive a toggle
