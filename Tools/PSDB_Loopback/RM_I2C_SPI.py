# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import ctypes
from ctypes import c_uint
import time

from Common import hilmon as hil

RmFpgaPath = r'FPGA\ring_multiplier_faba_top_19_01_11.rbf'
RmFpgaVer = 0x00000000

class Register(ctypes.LittleEndianStructure):
    """Register base class - defines setter/getter for integer value of register"""
    BAR = 0
    ADDR = 0
    BASEMUL = 4
    REGCOUNT = 1

    def __init__(self, value=None, **kwargs):
        super().__init__()
        if value is not None:
            self.value = value
        for field, value in kwargs.items():
            if not hasattr(self, field):
                raise AttributeError('{} does not have attribute {}'.format(self.__class__.__name__, field))
            setattr(self, field, value)
    
    def Pack(self):
        return self.value

    @property
    def value(self):
        """getter - returns integer value of register"""
        return int.from_bytes(self, byteorder='little')

    @value.setter
    def value(self, i):
        """setter - fills register fields from integer value"""
        ctypes.memmove(ctypes.addressof(self), i.to_bytes(4, byteorder='little'), 4)

    @classmethod
    def address(cls, index=0):
        return cls.ADDR + index * cls.BASEMUL
    
    @classmethod
    def fields(cls):
        return [x[0] for x in cls._fields_]

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        try:
            l.append('ADDR=0x{:04X}'.format(self.ADDR))
        except AttributeError:
            pass
        try:
            l.append('REGCOUNT={}'.format(self.REGCOUNT))
        except AttributeError:
            pass
        for field in self._fields_:
            l.append('{}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)

class RingMultiplierRegister(Register):
    BASEMUL = 4
    BAR = 1

class RingMultiplierStruct(Register):
    ADDR = None

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)

#Register  class and their bit field mappings:
class DC_TRIGGER_QUEUE_RECORD_LOW(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xE0
    _fields_ = [('PayLoadLower', ctypes.c_uint, 32)]

class DC_TRIGGER_QUEUE_RECORD_HIGH(RingMultiplierRegister):
    BAR = 1
    ADDR = 0xE4
    _fields_ = [('PayLoadUpper', ctypes.c_uint, 16), #LSB
                ('InterfaceTarget', ctypes.c_uint, 4),
                ('ChannelSet', ctypes.c_uint, 3),
                ('SPITarget', ctypes.c_uint, 3),
                ('ValidPayloadByteCount', ctypes.c_uint, 3),
                ('PSDBTarget', ctypes.c_uint, 1),
                ('WriteReadn', ctypes.c_uint, 1),
                ('CommandData', ctypes.c_uint, 1)] # MSB
                
def HexPad(val, len = 10):
   return '{0:#0{1}x}'.format(val,len)
   #   {   # Format identifier
   #   0:  # first parameter
   #   #   # use "0x" prefix
   #   0   # fill with zeroes
   #   {1} # to a length of n characters (including 0x), defined by the second parameter
   #   x   # hexadecimal number, using lowercase letters for a-f
   #   }   # End of format identifier

def VerifyUpdateRmFpga():
    #RC=0, PG=1. RM=2
    FpgaVer = hil.hbiMbFpgaVersion(2)
    print('RM FPGA version = ',HexPad(FpgaVer))
    if (FpgaVer != RmFpgaVer):
        print('Updating RM FPGA to ',RmFpgaPath)
        MbFpga(2, RmFpgaPath)

def MbFpga(indx, filename):
    try:
        hil.hbiMbDeviceDisable(indx)
    except RuntimeError:
        print('Driver did not disable.  Reboot when complete')
        input('press ENTER to continue...')

    print('Loading FPGA', indx)
    hil.hbiMbFpgaLoad(indx, filename)
    time.sleep(1) # Needed?

    try:
        hil.hbiMbDeviceEnable(indx)
        print(f'FPGA version = {hil.hbiMbFpgaVersion(indx):08X}h')
    except RuntimeError:
        print('Driver did not enable.  Reboot.')
        print('You may need to manually enable the driver in device manager after reboot.')

def RM_RW_I2C(PSDBNum, PMBusNum, RegAddr, ReadWrite, Length, Data = 0, Verbose = False):
    #    PSDBNum = 0 # choice of PSDB
    #    PMBusNum = 1 # which PMBUS device to talk to (in this case this is PMBUS 0 vccio_0-1)
    #        # PMBUS_0     = 1        I2C_ADDRESS_VCCIO_0_1_0 = 'b100_1011, 
    #        # PMBUS_1     = 2        I2C_ADDRESS_VCCIO_0_1_1 = 'b100_1010, 
    #        # PMBUS_2     = 3        I2C_ADDRESS_VCCIO_2_3_0 = 'b100_0111, 
    #        # PMBUS_3     = 4        I2C_ADDRESS_VCCIO_2_3_1 = 'b100_1100, 
    #        # PMBUS_VADJ  = 5        I2C_ADDRESS_VADJ        = 'b101_1100, 
    #    RegAddr = Register Address
    #    ReadWrite = 0 #(write=1 read=0)
    #    Length = 1 # no of bytes to read/write
    #    Data = data to be written (for writes only)
    dc_trigger_queue_low = DC_TRIGGER_QUEUE_RECORD_LOW #board.registers.DC_TRIGGER_QUEUE_RECORD_LOW # register address 0xE0
    dc_trigger_queue_low_obj = dc_trigger_queue_low()
    if (ReadWrite):  #Write
        dc_trigger_queue_low_obj.PayLoadLower = RegAddr   | (Data  << 8)
    else:  #read
        dc_trigger_queue_low_obj.PayLoadLower = RegAddr
    
    dc_trigger_queue_high = DC_TRIGGER_QUEUE_RECORD_HIGH #board.registers.DC_TRIGGER_QUEUE_RECORD_HIGH # register address 0xE4
    dc_trigger_queue_high_obj = dc_trigger_queue_high()

    dc_trigger_queue_high_obj.InterfaceTarget = PMBusNum
    dc_trigger_queue_high_obj.ValidPayloadByteCount = Length
    dc_trigger_queue_high_obj.PSDBTarget = PSDBNum
    dc_trigger_queue_high_obj.WriteReadn = ReadWrite
    dc_trigger_queue_high_obj.CommandData = 0 # This is data hence 0. Command encoding is start of trigger queue/end of trigger queue which aren’t enabled yet.

    #hil.hbiPsdbBarRead(SLOT, fpga, bar, addr) & (0xffffffff - (3<<bit))) | (Function<<bit)
    hil.hbiMbBarWrite(2,1,dc_trigger_queue_low_obj.ADDR,dc_trigger_queue_low_obj.value)
    #board.write_bar_register(dc_trigger_queue_low_obj)# bar writes to both the DC_TRIGGER_QUEUE_RECORD_HIGH and DC_TRIGGER_QUEUE_RECORD_LOW registers after constructing the entire value as indicated above
    hil.hbiMbBarWrite(2,1,dc_trigger_queue_high_obj.ADDR,dc_trigger_queue_high_obj.value)
    #board.write_bar_register(dc_trigger_queue_high_obj)

    time.sleep(0.1) # wait for several ms.
    Result = hil.hbiMbBarRead(2,1,0xE8)
    if (Verbose):
        print('DC TRIGGERQUEUECOMMAND lower {:x} upper {:x}',hex(hil.hbiMbBarRead(2,1,0xE0)),hex(hil.hbiMbBarRead(2,1,0xE4)))
        print('PSDB_DC_TRIGGER_PAGE_I2C_META_DATA {:x}',hex(hil.hbiMbBarRead(2,1,0xF0))) # read back the values in 0xF0 register for I2C meta data
        print('DC_TRIGGER_PAGE_I2C_DATA (result) {:x}',hex(Result)) # read back the values in 0xE8 register for I2C data

    return Result

def RM_W_Vref(PSDBNum, VrefNum, voltage_to_set, Verbose = False):
    dc_trigger_queue_low = DC_TRIGGER_QUEUE_RECORD_LOW #board.registers.DC_TRIGGER_QUEUE_RECORD_LOW # register address 0xE0
    dc_trigger_queue_low_obj = dc_trigger_queue_low()
    dac_select = 1 << VrefNum   #0xF
    write_and_update_cmdad5684 = 3
    payload_lower = ad5684_command_encoding(write_and_update_cmdad5684,dac_select,voltage_to_set)
    #Write
    dc_trigger_queue_low_obj.PayLoadLower = payload_lower
    
    dc_trigger_queue_high = DC_TRIGGER_QUEUE_RECORD_HIGH #board.registers.DC_TRIGGER_QUEUE_RECORD_HIGH # register address 0xE4
    dc_trigger_queue_high_obj = dc_trigger_queue_high()

    dc_trigger_queue_high_obj.InterfaceTarget = 0   #PMBusNum
    dc_trigger_queue_high_obj.ValidPayloadByteCount = 3   #3 bytes
    dc_trigger_queue_high_obj.PSDBTarget = PSDBNum
    dc_trigger_queue_high_obj.WriteReadn = 1  #Write
    dc_trigger_queue_high_obj.CommandData = 0 # This is data hence 0. Command encoding is start of trigger queue/end of trigger queue which aren’t enabled yet.

    hil.hbiMbBarWrite(2,1,dc_trigger_queue_low_obj.ADDR,dc_trigger_queue_low_obj.value)
    hil.hbiMbBarWrite(2,1,dc_trigger_queue_high_obj.ADDR,dc_trigger_queue_high_obj.value)

    time.sleep(0.1) # wait for several ms.
    Result = hil.hbiMbBarRead(2,1,0xE8)
    if (Verbose):
        print('DC TRIGGERQUEUECOMMAND lower {:x} upper {:x}',hex(hil.hbiMbBarRead(2,1,0xE0)),hex(hil.hbiMbBarRead(2,1,0xE4)))
        print('PSDB_DC_TRIGGER_PAGE_I2C_META_DATA {:x}',hex(hil.hbiMbBarRead(2,1,0xF0))) # read back the values in 0xF0 register for I2C meta data
        print('DC_TRIGGER_PAGE_I2C_DATA (result) {:x}',hex(Result)) # read back the values in 0xE8 register for I2C data

    return Result

def ad5684_command_encoding(command_code, dac_select, voltage_to_be_applied):
    voltage_in_dac_code = int(0xFFF * (voltage_to_be_applied/2.5))
    dac_command_encoding_24bit = command_code<<20|dac_select<<16|voltage_in_dac_code<<4
    return dac_command_encoding_24bit


if __name__ == '__main__':

    #for reg in range(0, 0x161, 4):
    #    print('addr',HexPad(reg,6),'=',HexPad(hil.hbiMbBarRead(2,1,reg)))
    #input('\nabout to check FPGA ver...')

    VerifyUpdateRmFpga()
    PSDBNum = 0
    PMBusNum = 5

    PSDBNum = int(input('\nPSDB Number (0-1)?'))
    VrefNum = int(input('Vref Number (0-3)?'))
    Voltage = float(input('Set Vref 0 to what voltage?'))
    RM_W_Vref(PSDBNum, VrefNum, Voltage)

    #print('0x19 Read: ', hex(RM_RW_I2C(PSDBNum, PMBusNum, 0x98, 0, 1, 0, True)))
    #
    ## RegAddr = 0x41
    #RegAddr = 0xb4
    #input('\nabout to read...')
    #print(hex(RegAddr), 'Read: ', hex(RM_RW_I2C(PSDBNum, PMBusNum, RegAddr, 0, 2, 0, True)))
    #input('\nabout to write...')
    #print(hex(RegAddr), 'Write: ', hex(RM_RW_I2C(PSDBNum, PMBusNum, RegAddr, 1, 3, 0xBEEF, True)))
    #input('\nabout to read...')
    #print(hex(RegAddr), 'Read: ', hex(RM_RW_I2C(PSDBNum, PMBusNum, RegAddr, 0, 2)))
    #
    #input('\nabout to read...')
    #print('0x02 Read: ', hex(RM_RW_I2C(PSDBNum, PMBusNum, 0x02, 0, 1)))

        #RM_RW_I2C(PSDBNum, PMBusNum, RegAddr, ReadWrite, Length, Data = 0, Verbose = False)
        #    PSDBNum = 0 # choice of PSDB
        #    PMBusNum = 1 # which PMBUS device to talk to (in this case this is PMBUS 0 vccio_0-1)
        #        # PMBUS_0     = 1        I2C_ADDRESS_VCCIO_0_1_0 = 'b100_1011, 
        #        # PMBUS_1     = 2        I2C_ADDRESS_VCCIO_0_1_1 = 'b100_1010, 
        #        # PMBUS_2     = 3        I2C_ADDRESS_VCCIO_2_3_0 = 'b100_0111, 
        #        # PMBUS_3     = 4        I2C_ADDRESS_VCCIO_2_3_1 = 'b100_1100, 
        #        # PMBUS_VADJ  = 5        I2C_ADDRESS_VADJ        = 'b101_1100, 
        #    RegAddr = Register Address
        #    ReadWrite = 0 #(write=1 read=0)
        #    Length = 1 # no of bytes to read/write
        #    Data = data to be written (for writes only)
