# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import time
import os
import sys
import struct
import pmbuspsdb

from Common import hilmon as hil

SLOT = 0
PinReg = [[[0 for addr in range(12)] for bar in range(6)] for fpga in range(2)] #create lists and set all to 0
FpgaPath0 = r'Z:\FPGAs\HBI_FPGAs\hbi_pin_multiplier\Debug\pin_multiplier0_faba_top\pin_multiplier0_faba_top_2019-01-30_09.54.01_xcvr_at_7gbps_led_v2.0.0\pin_multiplier0_faba_top.rbf'
FpgaPath1 = r'Z:\FPGAs\HBI_FPGAs\hbi_pin_multiplier\Debug\pin_multiplier1_faba_top\pin_multiplier1_faba_top_2019-01-30_09.54.01_xcvr_at_7gbps_led_v2.0.0\pin_multiplier1_faba_top.rbf'
# FpgaPath0 = r'FPGA\pin_multiplier0_faba_top_2019-01-30_09.54.01_xcvr_at_7gbps_led_v2.0.0\pin_multiplier0_faba_top.rbf'
# FpgaPath1 = r'FPGA\pin_multiplier1_faba_top_2019-01-30_09.54.01_xcvr_at_7gbps_led_v2.0.0\pin_multiplier1_faba_top.rbf'
FPGAVerExp = 0x00020000

PIN_LOW = 0
PIN_HIGH = 1
PIN_INPUT = 2
PIN_CLOCK = 3

def SetPin(fpga, Dut, Chan, Function):
    # if (Dut > 3):
    #     fpga = 1
    #     Dut -= 4
    # else:
    #     fpga = 0
    addr = 0x160 + Dut * 12 #3 bits per channel, 96 bits for 32 channels
    bar = int(Chan/35)
    # bit = (Chan - 35 * bar)
    bit = 2*(Chan - 35 * bar)
    # print('bit: ', bit)
    bar = bar + 1   #added for FPGA ver>=2.0 as bar regs shifted
    while (bit > 31): #extra space for channel 32-36
        addr += 4
        bit -= 32
    #pin function reg reads don't work, use local copy:
    y = ((3<<bit) ^ 0xffffffff)
    # print('y: 0x{:X}'.format(y))
    x = (PinReg[fpga][bar][int((addr-0x160)/4)] & y)
    # print('x: 0x{:X}'.format(x))
    # print('bit shift: 0x{:X}'.format(Function<<bit))
    value = x | (Function << bit)
    # print('Writing: 0x{:X}'.format(value))
    PinReg[fpga][bar][int((addr-0x160)/4)] = x | (Function<<bit)
    #NewVal = (hil.hbiPsdbBarRead(SLOT, fpga, bar, addr) & (0xffffffff - (3<<bit))) | (Function<<bit)
    #NewVal = Function<<bit
    #print('vals:',Dut, Chan, SLOT, fpga, bar, addr, bit, hil.hbiPsdbBarRead(SLOT, fpga, bar, addr), PinReg[fpga][bar][int((addr-0x160)/4)])
    #print('fpga=',fpga,', bar=',bar,', addr=',HexPad(addr,6),', RegVal=',HexPad(RegVal))
    hil.hbiPsdbBarWrite(SLOT, fpga, bar, addr, PinReg[fpga][bar][int((addr-0x160)/4)])
    print('SetPin to {}: fpga {} dut {}  ch {}  bar {} add 0x{:X}  O{:o} '.format(Function, fpga, Dut, Chan, bar, addr, PinReg[fpga][bar][int((addr-0x160)/4)]))
    
def InitPins(PinState): 
    RegVal = 0x55555555 * PinState
    for fpga in range(2):
        for addr in range(0x160, 0x190, 4):
            for bar in range (1,6):
                hil.hbiPsdbBarWrite(SLOT, fpga, bar, addr, RegVal) 
                PinReg[fpga][bar][int((addr-0x160)/4)] = RegVal  #set local copy to match
                #print('fpga=',fpga,', bar=',bar,', addr=',HexPad(addr,6),', RegVal=',HexPad(RegVal))
                
def ReadPin(fpga, Dut, Chan):
    # if (Dut > 3):
    #     fpga = 1
    #     Dut -=4
    # else:
    #     fpga = 0
    addr = 0xA0 + Dut * 8 # 64 bits total
    bar = int(Chan/35)
    bit = Chan - 35 * bar
    bar = bar + 1   #added for FPGA ver>=2.0 as bar regs shifted
    if (bit > 31):
        addr += 4
        bit -= 32
    # print('vals:',Dut, Chan, SLOT, fpga, bar, addr, bit)
    print('readPin: 0x{:x} bit: {}'.format(hil.hbiPsdbBarRead(SLOT, fpga, bar, addr), bit))
    return (hil.hbiPsdbBarRead(SLOT, fpga, bar, addr) >> bit) & 0x00000001


    # if (hil.hbiPsdbBarRead(SLOT, fpga, bar, addr) & (1<<bit)): #0b10 & 0b010
    #     print((hil.hbiPsdbBarRead(SLOT, fpga, bar, addr) & (1<<bit)), '\n')
        # return
    # else:
    #     return (hil.hbiPsdbBarRead(SLOT, fpga, bar, addr) >> bit) & 0x00000001
            
def FindHighs(PrintOut = True):
    if(PrintOut):
        print('High Pins: ', end='')
    First = True
    for NextDUT in range(8):
        for NextChan in range(175):
            if (ReadPin(NextDUT, NextChan) == '1'):
                if (not First):
                    print(', ', end='')
                First = False
                print('D', NextDUT, ':P', NextChan, end='')
    if (PrintOut and First):
        print('No high inputs')
    if (not First):
        print()

    return not First #returns true if any 1s are found
    
def ReadDUT(DutNum):
    print('DUT', DutNum, 'read [174:88]: ', end = '')
    for NextChan in range(174, 87, -1):
        print(ReadPin(DutNum, NextChan), end = '')
        if (NextChan % 8) ==0:
            print(' ', end = '')
    print()

    print('DUT', DutNum, 'read  [87:0]: ', end = '')
    for NextChan in range(87, -1, -1):
        print(ReadPin(DutNum, NextChan), end = '')
        if (NextChan % 8) ==0:
            print(' ', end = '')
    print()
    
def HexPad(val, len = 10):
   return '{0:#0{1}x}'.format(val,len)
   #   {   # Format identifier
   #   0:  # first parameter
   #   #   # use "0x" prefix
   #   0   # fill with zeroes
   #   {1} # to a length of n characters (including 0x), defined by the second parameter
   #   x   # hexadecimal number, using lowercase letters for a-f
   #   }   # End of format identifier

def InputPinFunc():
    PinFunc = input('pin function (h,l,c, or i): ')
    if PinFunc == 'l':
        return PIN_LOW
    if PinFunc == 'h':
        return PIN_HIGH
    if PinFunc == 'i':
        return PIN_INPUT
    if PinFunc == 'c':
        return PIN_CLOCK
    return -1
   
def VerifyUpdateFPGAs():
    print('Loading/Upadting...')
    try:
        Fpga0Ver = hil.hbiPsdbFpgaVersion(SLOT, 0)
        Fpga1Ver = hil.hbiPsdbFpgaVersion(SLOT, 1)
        print(f'PM FPGA0 version = ',HexPad(Fpga0Ver))
        print(f'PM FPGA1 version = ',HexPad(Fpga1Ver))
    except RuntimeError:
        print('Couuld not read PM FPGA version')
        Fpga0Ver = 0xDEADBEEF
        Fpga1Ver = 0xDEADBEEF
    if (Fpga0Ver != FPGAVerExp):
        print('Updating 0')
        PsdbFpga(0, FpgaPath0)
    if (Fpga1Ver != FPGAVerExp):
        print('Updating 1')
        PsdbFpga(1, FpgaPath1)

def PsdbFpga(indx, filename):
    try:
        hil.hbiPsdbDeviceDisable(SLOT, indx)
    except RuntimeError:
        print('Driver did not disable.  Reboot when complete')
        input('press ENTER to continue...')

    print('Loading FPGA', indx, 'with: ', filename)
    hil.hbiPsdbFpgaLoad(SLOT, indx, filename)
    time.sleep(1) # Needs a delay before enabling...possibly due to down-training issue.

    try:
        hil.hbiPsdbDeviceEnable(SLOT, indx)
        print(f'FPGA version = {hil.hbiPsdbFpgaVersion(SLOT, indx):08X}h')
    except RuntimeError:
        print('Driver did not enable.  Reboot.')
        print('You may need to manually enable the driver in device manager after reboot.')

def ReadLTM4678(idx,pg):
    print('{:>22s} {:8.3f} V'.format('\tread_vout',pmbuspsdb.read_vout(SLOT,idx,pg)))
    print('{:>22s} {:8.3f} A'.format('\tread_iout',pmbuspsdb.read_iout(SLOT,idx,pg)))
    print('{:>22s} {:8.1f} °C'.format('\tread_temperature_1',pmbuspsdb.read_temperature_1(SLOT,idx,pg)))
    print('{:>22s} {:8.1f} °C'.format('\tread_temperature_2',pmbuspsdb.read_temperature_2(SLOT,idx,pg)))
    print('{:>22s} {:8.1f} Hz'.format('\tread_frequency',pmbuspsdb.read_frequency(SLOT,idx,pg)))

def ReadLTM4678s():
    print('\n***LTM4678 U55 (FPGA#0 0.95v):')
    ReadLTM4678(0,0)
    print('\n***LTM4678 U56 (FPGA#0 1.03v):')
    ReadLTM4678(1,0)
    print('\n***LTM4678 U56 (FPGA#0 1.80v):')
    ReadLTM4678(1,1)
    print('\n***LTM4678 U10 (FPGA#1 0.95v):')
    ReadLTM4678(2,0)
    print('\n***LTM4678 U9 (FPGA#1 1.03v):')
    ReadLTM4678(3,0)
    print('\n***LTM4678 U9 (FPGA#1 1.80v):')
    ReadLTM4678(3,1)


