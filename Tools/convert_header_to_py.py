# INTEL CONFIDENTIAL

# Copyright 2018 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
#
"""
I want to be able to see APIs created using swig, documented with doxygen in python.

For this, I think we can parse header files which describe the APIs. and convert that in to stubs utilizing
restructuretext for our doc strings

This should output a pyi file which will provide inspection information for pycharm


"""
import os
import logging
import re
from collections import OrderedDict

file_path = os.path.abspath(__file__)
root = os.path.dirname((os.path.dirname(file_path)))
src = os.path.join(root, 'ThirdParty', 'HIL', 'include')
dst = os.path.join(root, 'ThirdParty', 'HIL', 'stubs')
print(root, src, dst)


def process_files(input_files, output_root, namespace=None, overwrite=True):
    """
    Files will be parsed and we'll build a module file based in output_root.
    Namespace will indicate if these files should be placed under a package structure or as an individual module

    :param input_files: list of parse_files
    :param output_root: base output path if none we'll build the stub directory in the same path as the given file
    :param namespace: if namespace is selected, we'll build a package file with the name - namespace
    :param overwrite: If false attempt to add to an existing package otherwise delete and rewrite
    """
    if namespace is not None:
        output_root = os.path.join(output_root, namespace)
        os.makedirs(output_root, exist_ok=True)
        init_file = open(os.path.join(output_root, '__init__.pyi'), 'w' if overwrite else 'a')
    else:
        init_file = None

    for parse_file in input_files:
        base_name = os.path.splitext(os.path.basename(parse_file.file_path))[0]
        py_file_name = base_name + '.pyi'
        output_file = os.path.join(output_root, py_file_name)
        try:
            parse_file.parse_file(output_file)
        except Exception:
            logging.getLogger("parsefile").exception("Error reading  %s", parse_file.file_path)
            raise
        if init_file is not None:
            init_file.write(f"\nfrom .{base_name} import *")

    if init_file is not None:
        init_file.close()


class BaseParseFile:


    def __init__(self, file_path):
        self.file_path = file_path
        self.triggers = []
        for name in dir(self):
            if name.startswith('get'):
                method = getattr(self, name)
                if callable(method):
                    self.triggers.append(method)
        self.out_file = None  # created in parse_file
        self.in_file_lines = None

    def parse_file(self, output_file_path):
        """ We'll expect get_* functions to be created which will be called at each line of a file
            If they trigger, they will return a True to say no other thing should trigger on this line
            And they will write to the opened new_file.
            Triggers will write to the output files

        """
        # Open Files
        in_file_obj = open(self.file_path)
        in_file_lines = self.in_file_lines = in_file_obj.readlines()
        out_file = self.out_file = open(output_file_path, 'w')
        self.write_imports()
        try:
            for loc, line in enumerate(in_file_lines):
                for trig in self.triggers:
                    if trig(line, loc):
                        break
        finally:
            in_file_obj.close()
            out_file.close()

    def write_imports(self):
        self.out_file.write("from typing import Any\n\n")

    def write_function(self, function_name, args=(), return_type=None, doc_string=""):
        args_string = ", ".join(args)
        self.out_file.write(f"\n\ndef {function_name}({args_string}) -> {return_type}:")
        self.out_file.write('\n    """')
        for line in doc_string.split('\n'):
            self.out_file.write("\n    " + line)
        self.out_file.write('\n    """')
        self.out_file.write('\n    pass\n')


class HilFileParser(BaseParseFile):
    # This should be based on the SWIG config file (for now I am guessing)
    ignore_arguments = ["cmdLength", "length"]

    def get_function(self, line, loc):
        """
        The plan is to get the function name based on the function described in HIL_API lines
        Then use the docxygen formatted info to get everything else.

        Args:
            line: string of file
            loc: line in file

        Returns:

        """
        if line.strip().startswith("HIL_API"):
            function_name = line[:line.find('(')]
            function_name = function_name[function_name.rfind(' '):].strip()
            try:
                doc_lines = list()
                for n_line in reversed(self.in_file_lines[:loc]):
                    if "//!" not in n_line:
                        break
                    n_line = n_line.replace("//!", "").strip()
                    doc_lines.insert(0, n_line.strip())
                raw_string = "\n".join(doc_lines)
                # Next we walk backwards until we find a line without //!
                doc_string, parameters, returns = build_py_function(raw_string)  # self.read_doxygen_func(loc)
                arg_names = []
                for parm, parm_type in parameters.items():
                    if parm_type is not None:
                        raise NotImplementedError
                    if parm in self.ignore_arguments:
                        continue
                    arg_names.append(parm)
            except Exception:
                logging.getLogger("getFunction").exception("Error in Function parsing %s", function_name)
                raise RuntimeError(f"Failed to parse function {function_name}") from None

            self.write_function(function_name, arg_names, "Any", doc_string)
        else:
            return False


def find_in_str(full_string, *search_pattern, start=0, end=None):
    """
    This will return the first found search_pattern. If none are found return end of string (better for my needs)
    :param full_string:
    :param search_pattern:
    :param start:
    :param end:
    :return:
    """
    first_found = len(full_string)
    for ch in search_pattern:
        index = full_string.find(ch, start)
        if index > -1:
            if index < first_found:  # Searching from the right will require rethink
                first_found = index  # replace first found with nearest index
    return first_found


logger = logging.getLogger("extractDoxygen")


def find_nonwhitespace(in_string, start):
    val = 0
    for char in in_string[start:]:
        if char != " ":
            break
        val += 1
    return val + start


def extract_doxygen_commands(doxygen_text):
    """
    Inputs a generic form doxygen text and outputs dict of commands with start/end locations of each argument in a dict
    {cmd_loc:(command, {arg1:(start, end)}, ...}

    :param doxygen_text: String of doxygen text
    :return cmd_dict: {cmd_loc:(command, {arg1:(start, end)}, ...}
    """
    # <> single word argument
    # () end of line argument
    # {} end of paragraph
    # find command
    index = 0
    command_dict = OrderedDict()
    assert "\r" not in doxygen_text, "This is not prepared for carriage returns \\r"
    str_len = len(doxygen_text)
    while True:
        cmd, cmd_loc, cmd_end = find_next_command(doxygen_text, index)
        # no new command found
        if cmd_loc == str_len:
            break
        index = find_nonwhitespace(doxygen_text, cmd_end)
        try:
            args = DoxygenCommand.get_cmd_args(cmd)
            logger.debug("***Arguments for cmd %s: %s", cmd, args)
        except RuntimeError:
            logger.exception("Failed to find command. %s", doxygen_text[index: index + 60])
            logger.error("Index: %s", index)
            raise
        args_dict = OrderedDict()
        for arg in args:
            # Word argument
            if arg.startswith("<"):
                arg_name = arg.replace("<", "").replace(">", "")
                end = find_in_str(doxygen_text, " ", start=index)
                args_dict[arg_name] = (index, end)
                index = find_nonwhitespace(doxygen_text, end)
            # end of line or section command
            if arg.startswith("("):
                arg_name = arg.replace("(", "").replace(")", "")
                end = find_end_section(doxygen_text, "\n", start=index)
                args_dict[arg_name] = (index, end)
                index = find_nonwhitespace(doxygen_text, end)
            # end of paragraph or section command
            if arg.startswith("{"):
                arg_name = arg.replace("{", "").replace("}", "")
                # Plus one to include both newlines
                end = find_end_section(doxygen_text, "\n\n", start=index)
                # Due to the double newlines we need to -1 the end
                args_dict[arg_name] = (index, end-1)
                index = end
            # Block of text Like code ... endcode. We want to ignore all internal commands
            if arg.startswith("block-"):
                end_block_cmd = arg.replace("block-", '')
                end = doxygen_text.find(end_block_cmd, index) + len(end_block_cmd)
                if end == -1:
                    raise RuntimeError(f"Could not find end block {end_block_cmd}. Start index: {index}")
                args_dict[arg] = (index, end)
                index = end
        command_dict[cmd_loc] = (cmd, args_dict)
        log_args(doxygen_text, args_dict)
        if index >= str_len:
            break

    return command_dict


def log_args(in_string, args_dict):
    """
    Log the string

    :param in_string:
    :param args_dict:
    :return:
    """
    for arg, val in args_dict.items():
        logger.debug("%s - %s", arg, in_string[val[0]: val[1]])


def find_next_command(in_string, start=0):
    """
    Finds next command in string

    :param in_string:
    :param start:
    :return:
    """
    cmd_loc = find_in_str(in_string, *["\\", "@"], start=start)
    cmd_end = find_in_str(in_string, *[" ", "\n"], start=cmd_loc)
    if cmd_end == -1:
        cmd_end = len(in_string)
    cmd = in_string[cmd_loc + 1:cmd_end]
    return cmd, cmd_loc, cmd_end


def find_end_section(in_string, section_end, start=0):
    """
    Search in_string for section ending chars/commands such as new paragraph or @param
    For now I am going to assume any command with line or paragraph arguments are sections.
    :param section_end: string for end of section
    :param in_string: raw string
    :param start: start search here.
    :return: Location of next section
    """
    index = start
    # Find end of paragraph.
    end_paragraph = in_string.find(section_end, index)
    if end_paragraph == -1:
        end_paragraph = len(in_string)
    while True:
        # Find next command until it is a section command
        cmd, cmd_loc, cmd_end = find_next_command(in_string, index)
        # logging.debug("dmds: %s, %s, %s", cmd, cmd_loc, cmd_end)
        index = cmd_end
        if cmd_loc > end_paragraph or cmd_loc == len(in_string):
            break
        sorry = False
        for arg in DoxygenCommand.get_cmd_args(cmd):
            if "{" in arg or "(" in arg or "block-" in arg:
                sorry = True
                break
        if sorry:
            break
    # Compare end_paragraph to section command return lower
    return end_paragraph if end_paragraph < cmd_loc else cmd_loc


class DoxygenCommand:
    """ The default conversion functions are reStructuredText based
        New formats should inherit from this class
    """
    doxygen_commands = {}

    def __init__(self, name, args=(), conversion="Ignore", end_block_cmd=None):
        self.name = name
        self.args = args
        self.__conversion_func = self.get_conversion_func(conversion)
        self.doxygen_commands[name] = self
        self.end_block_cmd = end_block_cmd
        if end_block_cmd:
            self.args = self.args + (f"block-{end_block_cmd}",)

    def conversion(self, in_string, args_dict, command_loc):
        len_before = len(in_string)
        # for each arg that is not a word we need to process the sub commands (which must be words)
        offset_args_dict = args_dict.copy()
        for arg in self.args:
            if arg.startswith("(") or arg.startswith("{"):
                arg_name = arg.replace("(", "").replace(")", "").replace("{", "").replace("}", "")
                # we know this exists.
                arg_start, arg_end = args_dict[arg_name]
                sub_cmd_name, sub_cmd_loc, sub_cmd_end = find_next_command(in_string, arg_start)
                offset = 0
                # Check that it is within the argument
                # everything within sub command is based on current offset
                while sub_cmd_loc < (arg_end + offset):
                    sub_cmd = self.get_cmd(sub_cmd_name)
                    for sub_arg in self.get_cmd_args(sub_cmd_name):
                        if sub_arg.startswith("<"):
                            sub_arg_name = arg.replace("<", "").replace(">", "")
                            sub_arg_start = sub_cmd_end + 1
                            sub_arg_end = find_in_str(in_string, " ", start=sub_arg_start)
                            in_string = sub_cmd.conversion(in_string, {sub_arg_name: (sub_arg_start, sub_arg_end)},
                                                           sub_cmd_loc)
                            len_after = len(in_string)
                            offset = len_after - len_before
                            offset_args_dict[arg_name] = (arg_start, arg_end + offset)
                            break
                    sub_cmd_name, sub_cmd_loc, sub_cmd_end = find_next_command(in_string, sub_cmd_end)
        return self.__conversion_func(in_string, offset_args_dict, command_loc)

    def get_conversion_func(self, conversion_name):
        try:
            conversion_function = getattr(self, conversion_name.lower())
        except AttributeError:
            raise

        return conversion_function

    # Conversion functions will have three arguments: the full string, arguments dict and command start location.
    @staticmethod
    def ignore(in_string, args_dict, command_loc):
        cmd, start, end_cmd = find_next_command(in_string, command_loc)
        return in_string[:start] + in_string[end_cmd:]

    @staticmethod
    def escape_char(in_string, args_dict, command_loc):
        cmd, start, end_cmd = find_next_command(in_string, command_loc)
        # just hide / or @
        return in_string[:start] + in_string[start+1:]

    @staticmethod
    def convert_parameter(in_string, args_dict, command_loc):
        name_start, name_end = args_dict['parameter-name']
        param_name = in_string[name_start:name_end]
        if "out" in in_string[command_loc: in_string.find(" ", command_loc)].lower():
            name_type = "return"
        else:
            name_type = "param"
        # indent each newline (except the last one)
        desc_start, desc_end = args_dict['parameter-description']
        rep = "\n      " if name_type == "param" else "\n       "
        description = in_string[desc_start:desc_end].replace("\n", rep)
        description = description.strip(" ")
        return in_string[:command_loc] + f"\n:{name_type} {param_name}:" + in_string[name_end:desc_start] + description \
               + in_string[desc_end:]

    @staticmethod
    def convert_return(in_string, args_dict, command_loc):
        cmd, start, end_cmd = find_next_command(in_string, command_loc)
        return in_string[:start] + "\n:return:" + in_string[end_cmd:]

    @staticmethod
    def strong_emphasis(in_string, args_dict, command_loc):
        cmd, start_cmd, end_cmd = find_next_command(in_string, command_loc)
        initial_len = len(in_string)
        # no offset needed because we are going in reverse
        for arg_start, arg_end in reversed(list(args_dict.values())):
            in_string = in_string[:arg_start] + "**" + in_string[arg_start: arg_end] + "**" + in_string[arg_end:]
        # Remove space at start of arg
        return in_string[:start_cmd] + in_string[end_cmd+1:]

    @staticmethod
    def emphasis(in_string, args_dict, command_loc):
        cmd, start_cmd, end_cmd = find_next_command(in_string, command_loc)
        initial_len = len(in_string)
        # no offset needed because we are going in reverse
        for arg_start, arg_end in reversed(list(args_dict.values())):
            in_string = in_string[:arg_start] + "*" + in_string[arg_start: arg_end] + "*" + in_string[arg_end:]
        # Remove space at start of arg
        return in_string[:start_cmd] + in_string[end_cmd+1:]

    @staticmethod
    def code(in_string, args_dict, command_loc):
        cmd, start_cmd, end_cmd = find_next_command(in_string, command_loc)
        initial_len = len(in_string)
        # no offset needed because we are going in reverse
        for arg_start, arg_end in reversed(list(args_dict.values())):
            in_string = in_string[:arg_start] + "``" + in_string[arg_start: arg_end] + "``" + in_string[arg_end:]
        # End plus one to remove space
        return in_string[:start_cmd] + in_string[end_cmd+1:]

    @staticmethod
    def replace_newlines(substring, replacement):
        description = substring.replace("\n", replacement)
        description = description.strip(" ")
        return description

    def block_literal(self, in_string, args_dict, command_loc):
        # find end block command to remove it
        for arg, start_end in reversed(list(args_dict.items())):
            if "block-" in arg:
                arg_start, arg_end = start_end
                rep = "\n    "
                block = self.replace_newlines(in_string[arg_start:arg_end], rep)
                in_string = in_string[:arg_start] + "\n::\n\n    " + block + in_string[arg_end:]
                # Remove end block command
                end_block_cmd = arg.replace("block-", '')
                end = in_string.find(end_block_cmd, command_loc) + len(end_block_cmd)
                in_string = in_string[:end - len(end_block_cmd) - 1] + in_string[end:]
        # Remove command
        cmd, start_cmd, end_cmd = find_next_command(in_string, command_loc)
        return in_string[:start_cmd] + in_string[end_cmd+1:]

    @staticmethod
    def new_line(in_string, args_dict, command_loc):
        cmd, start, end_cmd = find_next_command(in_string, command_loc)
        return in_string[:start] + "\n" + in_string[end_cmd:]

    @classmethod
    def get_cmd_args(cls, cmd):
        return cls.get_cmd(cmd).args

    @classmethod
    def get_cmd(cls, cmd):
        # Use re to check for matches where * is replaced with .+
        if cmd in cls.doxygen_commands:
            return cls.doxygen_commands[cmd]
        else:
            # look for the * based commands
            for test_cmd in cls.doxygen_commands:
                if "*" in test_cmd:
                    pattern = test_cmd.replace("*", ".+")
                    for char in "[]{}()":
                        pattern = pattern.replace(char, "\\" + char)
                    match = re.match(pattern, cmd)
                    if match:
                        return cls.doxygen_commands[test_cmd]
        raise RuntimeError(f"cmd {cmd} not recognized")


# These get stored in an internal class level dictionary.
# Todo: I am not sure if this is good coding.
DC = DoxygenCommand
DC("code{*}", conversion='block_literal', end_block_cmd="endcode")
DC("code", conversion='block_literal', end_block_cmd="endcode")
# table is not a real command. I created it so I could reused the command code - it is not a perfect solution but good
# enough which is the story of this module
DC("table", conversion='block_literal', end_block_cmd="endtable")
DC("note", ("{text}",), conversion='emphasis')
DC("par", ("(paragraph title)", "{ paragraph }"))
DC("param[*]", ("<parameter-name>", "{parameter-description}"), "convert_parameter")
DC("brief", ("{brief-description}",))
DC("c", ("<word>",), "code")
DC("p", ("<word>",), "code")
DC("returns", ("{return_description}",), "convert_return")
DC("return", ("{return_description}",), "convert_return")
DC("retval", ("<return_value>", "{return_description}",), "convert_return")
DC('"', conversion="escape_char")
DC('warning', ("{warning_message}",), "strong_emphasis")
DC('see', ("{references}",))
DC('n', conversion="new_line")
DC('n\\n', conversion="new_line")
DC('b', ("<word>",), conversion="strong_emphasis")
DC('a', ("<word>",), conversion="emphasis")
DC("ref", ("<reference>",))
DC("page", ("<name>", "(title)"))
DC("li", ("{ item-description }",))
DC("image")


def find_tables(in_string):
    """
    Parse the input string looking for tables.  We know something is a table line if it starts with \
    The table ends when there is no | in the line . This is not perfect but I think it will work well enough

    :param in_string:
    :return:
    """
    tables = []
    start = None
    end = 0
    for line in in_string.split("\n"):
        if line.strip().startswith("|") and start is None:
            start = in_string.find(line, end)
        # Find end
        if start is not None and "|" not in line:
            end = in_string.find(line, end)
            tables.append((start, end))
            start = None
    return tables


def build_py_function(in_string):
    """
    Use doxygen commands to build doc_string and find parameters and return values.
    Parameters and retun will be of the form {"ParamName": Type}
    If Type is None then no type is provided.

    :param in_string:
    :param doxygen_commands:
    :return: doc_string, parameters, returns
    """
    returns_dict = OrderedDict()
    param_dict = OrderedDict()
    # Add table commands, there are no actual table commands but I am going to make some to fit my method
    for table_start, table_end in reversed(find_tables(in_string)):
        in_string = in_string[:table_start] + "\n@table\n" + in_string[table_start:table_end] + "@endtable\n" + in_string[table_end:]
    # TODO: I don't like this but it is done so that we don't have two spaces after a command - which brakes some things


    doxygen_commands = extract_doxygen_commands(in_string)
    # we want the indexes to continue to reference the same location even as we shift around.
    # To do this we go in reverse so any changes to index happened after the index.
    for cmd_loc, val in reversed(list(doxygen_commands.items())):
        cmd_name, args_dict = val
        cmd = DoxygenCommand.get_cmd(cmd_name)
        if "param" in cmd_name:
            nam_start, name_end = args_dict["parameter-name"]
            param_name = in_string[nam_start:name_end]
            if "out" in cmd_name:
                returns_dict[param_name] = None
            else:
                param_dict[param_name] = None
        # log_args(in_string, args_dict)
        in_string = cmd.conversion(in_string, args_dict, cmd_loc)
    in_string = in_string.strip()
    return in_string, OrderedDict(reversed(list(param_dict.items()))), OrderedDict(reversed(list(returns_dict.items())))


testStr = r""" 
@brief Verifies an HVDPS calibration card is present.

This function verifies an HVDPS calibration card is present.  It connects to and caches driver resources for use
by other \c calHvdpsXXXXX functions.  calHvdpsDisconnect() is its complementary function and frees any cached resources.

Neither calHvdpsConnect() or calHvdpsDisconnect() are required to be called to use the other \c calHvdpsXXXXX functions.  All
\c calHvdpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
perform their function.
@param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
@returns \ref HIL_STATUS
   """

if __name__ == '__main__':
    logging.basicConfig(level="INFO")

    # hil_dir = src
    # out_dir = os.path.join(root, 'stubs')
    parse_files = list()
    for root, dirs, files in os.walk(src):
        for file_name in files:
            this_file_path = os.path.join(root, file_name)
            parse_files.append(HilFileParser(this_file_path))

    process_files(parse_files, dst, 'hil')
