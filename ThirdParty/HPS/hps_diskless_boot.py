# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import time

from Common import fval
from Common import hilmon as hil


class BootHps():
    MSG_MAILBOX = 0x4000
    MSG_TIMEOUT = 100

    HPS2SYS_SYNC_0 = 0xf0f0f0f0
    SYS2HPS_SYNC_1 = 0xf1f1f1f1

    HPS2SYS_UBOOT_WAIT = 0xf2f2f2f2
    SYS2HPS_UBOOT_DONE = 0xf3f3f3f3

    HPS2SYS_LINUX_WAIT = 0xf4f4f4f4
    SYS2HPS_LINUX_DONE = 0xf5f5f5f5

    U_BOOT_LOAD_ADDRESS = 0xfc0
    LINUX_FDT_LOAD_ADDRESS = 0x8000000
    LINUX_KERNEL_LOAD_ADDRESS = 0x2000000

    DMA_CHUNK_SIZE = 64

    def __init__(self, rc_name):
        self.name = self.__class__.__name__
        self.rc_name = rc_name.upper()
        self.dma_write_action = {'HBIRCTC': self.hbirctc_dma_write,
                                 'RC3': self.rc3_dma_write}
        self.bar_write_action = {'HBIRCTC': self.hbirctc_bar_write,
                                 'RC3': self.rc3_bar_write}
        self.bar_read_action = {'HBIRCTC': self.hbirctc_bar_read,
                                'RC3': self.rc3_bar_read}
        if self.rc_name not in self.dma_write_action.keys():
            fval.Log('error',
                     f'Rc instrument name {self.rc_name} not recognized.')

        self.run_intialization()

    def run_intialization(self):
        self.wait_on_sync()
        self.load_uboot_image()
        self.load_fdt_image()
        self.load_kernel_image()

    def get_uboot_image_path(self):
        return os.path.join(os.path.dirname(__file__),
                            'hbi_mb_tc_uboot_dtb_v1.1.img')

    def get_linux_fdt_image_path(self):
        return os.path.join(os.path.dirname(__file__),
                            'hbi_mb_tc_socfpga_stratix10_socdk_v1.1.dtb')

    def get_linux_kernel_image_path(self):
        return os.path.join(os.path.dirname(__file__),
                            'hbi_mb_tc_linux_kernel_v1.1')

    def _get_file_content(self, file_name):
        content = None
        with open(file_name, mode='rb') as file:
            content = file.read()
            file.close()

        for i in range(4 - (len(content) % 4)):
            content += b'\x00'
        return content

    def _dma_load_content(self, address, content):
        for i in range(0, len(content), BootHps.DMA_CHUNK_SIZE):
            self.dma_write_action.get(self.rc_name)(
                address + i, content[i: i + BootHps.DMA_CHUNK_SIZE])

    def send_message(self, message):
        self.bar_write_action.get(self.rc_name)(BootHps.MSG_MAILBOX, message)

    def wait_for_message(self, message):
        for timeout in range(BootHps.MSG_TIMEOUT):
            read_data = self.bar_read_action.get(self.rc_name)(
                BootHps.MSG_MAILBOX)
            if(read_data == message):
                break
            time.sleep(0.1)
        else:
            fval.Log('error', f'HPS initialization timed out waiting for '
                              f'message: 0x{message:X}')

    def wait_on_sync(self):
        fval.Log('info', f'{self.name}: Waiting for HPS2SYS_SYNC_0')
        self.wait_for_message(BootHps.HPS2SYS_SYNC_0);
        fval.Log('info', f'{self.name}: Sending SYS2HPS_SYNC_1')
        self.send_message(BootHps.SYS2HPS_SYNC_1)

    def load_uboot_image(self):
        fval.Log('info', f'{self.name}: Waiting for HPS2SYS_UBOOT_WAIT')
        self.wait_for_message(BootHps.HPS2SYS_UBOOT_WAIT)

        content = self._get_file_content(self.get_uboot_image_path())
        self._dma_load_content(BootHps.U_BOOT_LOAD_ADDRESS, content)

        fval.Log('info', f'{self.name}: Sending SYS2HPS_UBOOT_DONE')
        self.send_message(BootHps.SYS2HPS_UBOOT_DONE)

    def load_fdt_image(self):
        fval.Log('info', f'{self.name}: Waiting for HPS2SYS_LINUX_WAIT')
        self.wait_for_message(BootHps.HPS2SYS_LINUX_WAIT)

        content = self._get_file_content(self.get_linux_fdt_image_path())
        self._dma_load_content(BootHps.LINUX_FDT_LOAD_ADDRESS, content)

    def load_kernel_image(self):
        content = self._get_file_content(self.get_linux_kernel_image_path())
        self._dma_load_content(BootHps.LINUX_KERNEL_LOAD_ADDRESS, content)

        fval.Log('info', f'{self.name}: Sending SYS2HPS_LINUX_DONE')
        self.send_message(BootHps.SYS2HPS_LINUX_DONE)

    def hbirctc_dma_write(self, address, data):
        hil.hbiMbHpsDmaWrite(address, data)

    def rc3_dma_write(self, address, data):
        hil.rc3HpsDmaWrite(address, data)

    def hbirctc_bar_write(self, offset, data):
        hil.hbiMbBarWrite(0, 1, offset, data)

    def rc3_bar_write(self, offset, data):
        hil.rc3BarWrite(1, offset, data)

    def hbirctc_bar_read(self, offset):
        return hil.hbiMbBarRead(0, 1, offset)

    def rc3_bar_read(self, offset):
        return hil.rc3BarRead(1, offset)
