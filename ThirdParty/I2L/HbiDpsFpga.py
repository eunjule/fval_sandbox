from enum import Enum

from .Utility import Log, Sim
from .HilWrap import HilWrap as hil

def _GetPageOffset(pageNumber):
    offsets = {
        0 : 0x0000,
        1 : 0x0020,
        2 : 0x0040,
        3 : 0x0060,
        4 : 0x0080,
        5 : 0x00A0,
        6 : 0x00C0,
        7 : 0x00E0,
        8 : 0x0100,
        9 : 0x0120,
        10 : 0x0140,
        11 : 0x0180,
        12 : 0x01A0,
        13 : 0x01C0,
        14 : 0x01E0,
        15 : 0x0200
    }
    return offsets[pageNumber]
    
class BAR1(Enum):
    BAR1_SCRATCH                                  = _GetPageOffset(0) + 0x00
    FPGA_VERSION                                  = _GetPageOffset(0) + 0x04
    RESETS                                        = _GetPageOffset(0) + 0x08
    STATUS                                        = _GetPageOffset(0) + 0x0C
    SLOT_ID                                       = _GetPageOffset(0) + 0x10
    EXECUTE_TRIGGER                               = _GetPageOffset(0) + 0x14
    EXECUTE_INTERRUPT                             = _GetPageOffset(0) + 0x18
    SET_SAFE_STATE                                = _GetPageOffset(0) + 0x1C
    
    #//////////////////////////////////////////////////////
    #//Start defining _GetPageOffset(1 Register Map here
    #//////////////////////////////////////////////////////
    TRIG_RX_TEST_FIFO_DATA_COUNT                  = _GetPageOffset(1) + 0x00,  #// (0x020)
    
    #BAR 1 Point and Shoot DDR4 Registers
    MEM_WR_DATA_TAG                               = _GetPageOffset(1) + 0x04
    MEM_RD_DATA_TAG                               = _GetPageOffset(1) + 0x08
    MEM_ADDRESS_TAG                               = _GetPageOffset(1) + 0x0C
    
    #Aurora Trigger Registers
    TRIGGER_UP_TEST                               = _GetPageOffset(1) + 0x10
    TRIGGER_DN_TEST                               = _GetPageOffset(1) + 0x14
    
    AURORA_ERROR_COUNTS                           = _GetPageOffset(1) + 0x18
    
    # {270x0, VMeasure V data polling, HC V/I data polling, LC V/I data polling, VMeasure ADC as float, S2H enabled}
    CAPABILITIES                                  = _GetPageOffset(1) + 0x1C
    
    #////////////////////////////////////////////////////
    #Start defining _GetPageOffset(2 Register Map here
    #////////////////////////////////////////////////////
    
    #LC Rails PMBus 1.2 I2C Interface Registers
    LTC2975_00_I2C_STATUS_REG                 = _GetPageOffset(2) + 0x00
    LTC2975_00_I2C_CONTROL_REG                = _GetPageOffset(2) + 0x04
    LTC2975_00_I2C_TX_FIFO_DATA_REG           = _GetPageOffset(2) + 0x08
    LTC2975_00_I2C_RX_FIFO_DATA_REG           = _GetPageOffset(2) + 0x0C
    
    LTC2975_01_I2C_STATUS_REG                 = _GetPageOffset(2) + 0x10
    LTC2975_01_I2C_CONTROL_REG                = _GetPageOffset(2) + 0x14
    LTC2975_01_I2C_TX_FIFO_DATA_REG           = _GetPageOffset(2) + 0x18
    LTC2975_01_I2C_RX_FIFO_DATA_REG           = _GetPageOffset(2) + 0x1C
    
    #////////////////////////////////////////////////////
    #Start defining _GetPageOffset(3 Register Map here
    #////////////////////////////////////////////////////
    LTC2975_02_I2C_STATUS_REG                 = _GetPageOffset(3) + 0x00
    LTC2975_02_I2C_CONTROL_REG                = _GetPageOffset(3) + 0x04
    LTC2975_02_I2C_TX_FIFO_DATA_REG           = _GetPageOffset(3) + 0x08
    LTC2975_02_I2C_RX_FIFO_DATA_REG           = _GetPageOffset(3) + 0x0C
    
    LTC2975_03_I2C_STATUS_REG                 = _GetPageOffset(3) + 0x10
    LTC2975_03_I2C_CONTROL_REG                = _GetPageOffset(3) + 0x14
    LTC2975_03_I2C_TX_FIFO_DATA_REG           = _GetPageOffset(3) + 0x18
    LTC2975_03_I2C_RX_FIFO_DATA_REG           = _GetPageOffset(3) + 0x1C
    
    #////////////////////////////////////////////////////
    #Start defining _GetPageOffset(4 Register Map here
    #////////////////////////////////////////////////////
    
    #ADT 7411 VMON I2C  Interface Registers
    ADT7411_I2C_STATUS_REG                    = _GetPageOffset(4) + 0x00
    ADT7411_I2C_CONTROL_REG                   = _GetPageOffset(4) + 0x04
    ADT7411_I2C_TX_FIFO_DATA_REG              = _GetPageOffset(4) + 0x08
    ADT7411_I2C_RX_FIFO_DATA_REG              = _GetPageOffset(4) + 0x0C
    
    #Self Test Load Switch GPIO Control
    TPS22976N_ST_LC_LD_EN                     = _GetPageOffset(4) + 0x10
    TPS22976N_ST_HC_LD_EN                     = _GetPageOffset(4) + 0x14
    TPS22976N_ST_LD_EN                        = _GetPageOffset(4) + 0x18
    
    #Current Sensors
    TLI4970_V12P0_SYS_CURRENT_VALUE               = _GetPageOffset(4) + 0x1C
    
    #////////////////////////////////////////////////////
    #Start defining _GetPageOffset(5 Register Map here
    #////////////////////////////////////////////////////
    #MAX6627 Temperature Sensors (SPI Interface)
    MAX6627_00_UPPER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x00
    MAX6627_01_UPPER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x04
    MAX6627_02_UPPER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x08
    MAX6627_03_UPPER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x0C
    MAX6627_00_LOWER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x10
    MAX6627_01_LOWER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x14
    MAX6627_02_LOWER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x18
    MAX6627_03_LOWER_TEMP_LIMIT                   = _GetPageOffset(5) + 0x1C
    
    #////////////////////////////////////////////////////
    #Start defining _GetPageOffset(6 Register Map here
    #////////////////////////////////////////////////////
    #MAX6627 Temperature Sensors (SPI Interface)
    MAX6627_00_TEMP_VALUE                         = _GetPageOffset(6) + 0x00
    MAX6627_01_TEMP_VALUE                         = _GetPageOffset(6) + 0x04
    MAX6627_02_TEMP_VALUE                         = _GetPageOffset(6) + 0x08
    MAX6627_03_TEMP_VALUE                         = _GetPageOffset(6) + 0x0C
    
    #Sample Collector for ADC
    SAMPLE_COLLECTOR_INDEX                        = _GetPageOffset(6) + 0x14
    SAMPLE_COLLECTOR_COUNT                        = _GetPageOffset(6) + 0x18
    SAMPLE_COLLECTOR_DATA                         = _GetPageOffset(6) + 0x1C
    
    #/////////////////////////////////////////////////////
    #Start defining _GetPageOffset(7 Register Map here
    #/////////////////////////////////////////////////////
    #HC Rails PMBus 1.2 I2C Interface Registers
    LTC4680_00_I2C_STATUS_REG                     = _GetPageOffset(7) + 0x00
    LTC4680_00_I2C_CONTROL_REG                    = _GetPageOffset(7) + 0x04
    LTC4680_00_I2C_TX_FIFO_DATA_REG               = _GetPageOffset(7) + 0x08
    LTC4680_00_I2C_RX_FIFO_DATA_REG               = _GetPageOffset(7) + 0x0C
    
    LTC4680_01_I2C_STATUS_REG                     = _GetPageOffset(7) + 0x10
    LTC4680_01_I2C_CONTROL_REG                    = _GetPageOffset(7) + 0x14
    LTC4680_01_I2C_TX_FIFO_DATA_REG               = _GetPageOffset(7) + 0x18
    LTC4680_01_I2C_RX_FIFO_DATA_REG               = _GetPageOffset(7) + 0x1C
    
    #/////////////////////////////////////////////////////
    #Start defining _GetPageOffset(8 Register Map here
    #/////////////////////////////////////////////////////
    #HC Rails PMBus 1.2 I2C Interface Registers
    LTC4680_02_I2C_STATUS_REG                     = _GetPageOffset(8) + 0x00
    LTC4680_02_I2C_CONTROL_REG                    = _GetPageOffset(8) + 0x04
    LTC4680_02_I2C_TX_FIFO_DATA_REG               = _GetPageOffset(8) + 0x08
    LTC4680_02_I2C_RX_FIFO_DATA_REG               = _GetPageOffset(8) + 0x0C
    
    LTC4680_03_I2C_STATUS_REG                     = _GetPageOffset(8) + 0x10
    LTC4680_03_I2C_CONTROL_REG                    = _GetPageOffset(8) + 0x14
    LTC4680_03_I2C_TX_FIFO_DATA_REG               = _GetPageOffset(8) + 0x18
    LTC4680_03_I2C_RX_FIFO_DATA_REG               = _GetPageOffset(8) + 0x1C
    
    #/////////////////////////////////////////////////////
    #Start defining _GetPageOffset(9 Register Map here
    #/////////////////////////////////////////////////////
    #HC Rails PMBus 1.2 I2C Interface Registers
    LTC4680_04_I2C_STATUS_REG                     = _GetPageOffset(9) + 0x00
    LTC4680_04_I2C_CONTROL_REG                    = _GetPageOffset(9) + 0x04
    LTC4680_04_I2C_TX_FIFO_DATA_REG               = _GetPageOffset(9) + 0x08
    LTC4680_04_I2C_RX_FIFO_DATA_REG               = _GetPageOffset(9) + 0x0C
    
    #MAX14662 Switches
    #MAX14662 VT VM LoopBack Switch I2C Interface
    MAX14662_VT_VM_LB_I2C_STATUS_REG              = _GetPageOffset(9) + 0x10
    MAX14662_VT_VM_LB_I2C_CONTROL_REG             = _GetPageOffset(9) + 0x14
    MAX14662_VT_VM_LB_I2C_TX_FIFO_DATA_REG        = _GetPageOffset(9) + 0x18
    MAX14662_VT_VM_LB_I2C_RX_FIFO_DATA_REG        = _GetPageOffset(9) + 0x1C
    
    
    #///////////////////////////////////////////////////////////
    #Start defining _GetPageOffset(10 Register Map here
    #///////////////////////////////////////////////////////////
    #MAX14662 ISOUCE to VMEASURE I2C Interface
    MAX14662_ISOURCE_VSENSE_I2C_STATUS_REG        = _GetPageOffset(10) + 0x00
    MAX14662_ISOURCE_VSENSE_I2C_CONTROL_REG       = _GetPageOffset(10) + 0x04
    MAX14662_ISOURCE_VSENSE_I2C_TX_FIFO_DATA_REG  = _GetPageOffset(10) + 0x08
    MAX14662_ISOURCE_VSENSE_I2C_RX_FIFO_DATA_REG  = _GetPageOffset(10) + 0x0C
    
    #MAX14662 LC Rail Ganging Control I2C Interface
    MAX14662_01_GANG_LTC2975_I2C_STATUS_REG       = _GetPageOffset(10) + 0x10
    MAX14662_01_GANG_LTC2975_I2C_CONTROL_REG      = _GetPageOffset(10) + 0x14
    MAX14662_01_GANG_LTC2975_I2C_TX_FIFO_DATA_REG = _GetPageOffset(10) + 0x18
    MAX14662_01_GANG_LTC2975_I2C_RX_FIFO_DATA_REG = _GetPageOffset(10) + 0x1C

    #///////////////////////////////////////////////////////////
    #Start defining _GetPageOffset(11 Register Map here
    #///////////////////////////////////////////////////////////
    MAX14662_23_GANG_LTC2975_I2C_STATUS_REG       = _GetPageOffset(11) + 0x00
    MAX14662_23_GANG_LTC2975_I2C_CONTROL_REG      = _GetPageOffset(11) + 0x04
    MAX14662_23_GANG_LTC2975_I2C_TX_FIFO_DATA_REG = _GetPageOffset(11) + 0x08
    MAX14662_23_GANG_LTC2975_I2C_RX_FIFO_DATA_REG = _GetPageOffset(11) + 0x0C

    #HC Rail Ganging Circuitry GPIO Control, active low signals
    HC_GANG_COMPA_HCR_EN                          = _GetPageOffset(11) + 0x10
    HC_GANG_COMPB_HCR_EN                          = _GetPageOffset(11) + 0x14
    HC_GANG_SYNC_HCM_EN                           = _GetPageOffset(11) + 0x18
    
    #///////////////////////////////////////////////////////////
    #Start defining _GetPageOffset(12 Register Map here
    #///////////////////////////////////////////////////////////
    #VTARGET DAC AD5676 0 and 1 SPI Read and Write Registers
    AD5676_VT_DAC_00_WR_DATA                      = _GetPageOffset(12) + 0x00
    AD5676_VT_DAC_00_RD_DATA                      = _GetPageOffset(12) + 0x04
    AD5676_VT_DAC_01_WR_DATA                      = _GetPageOffset(12) + 0x08
    AD5676_VT_DAC_01_RD_DATA                      = _GetPageOffset(12) + 0x0C
    
    LTC2975_CTRL_REGISTER                         = _GetPageOffset(12) + 0x10
    
    #HC Rail PMBus Chip ON/OFF Control
    LTC4680_RUN                                   = _GetPageOffset(12) + 0x14
    
    #VMEASURE ADC AD7616 Registers (SPI Interface)
    AD7616_VM_ADC_CHANNEL_SEL                     = _GetPageOffset(12) + 0x18
    
    #///////////////////////////////////////////////////////////
    #Start defining _GetPageOffset(13 Register Map here
    #///////////////////////////////////////////////////////////
    #BAR 1 Instantaneous ADC Voltage Value Registers will reflect floating point values
    AD7616_CHANNEL_00_VM_ADC                      = _GetPageOffset(13) + 0x00
    AD7616_CHANNEL_01_VM_ADC                      = _GetPageOffset(13) + 0x04
    AD7616_CHANNEL_02_VM_ADC                      = _GetPageOffset(13) + 0x08
    AD7616_CHANNEL_03_VM_ADC                      = _GetPageOffset(13) + 0x0C
    AD7616_CHANNEL_04_VM_ADC                      = _GetPageOffset(13) + 0x10
    AD7616_CHANNEL_05_VM_ADC                      = _GetPageOffset(13) + 0x14
    AD7616_CHANNEL_06_VM_ADC                      = _GetPageOffset(13) + 0x18
    AD7616_CHANNEL_07_VM_ADC                      = _GetPageOffset(13) + 0x1C
    
    #///////////////////////////////////////////////////////////
    #Start defining _GetPageOffset(14 Register Map here
    #///////////////////////////////////////////////////////////
    #BAR 1 Instantaneous ADC Voltage Value Registers will reflect floating point values
    AD7616_CHANNEL_08_VM_ADC                      = _GetPageOffset(14) + 0x00
    AD7616_CHANNEL_09_VM_ADC                      = _GetPageOffset(14) + 0x04
    AD7616_CHANNEL_10_VM_ADC                      = _GetPageOffset(14) + 0x08
    AD7616_CHANNEL_11_VM_ADC                      = _GetPageOffset(14) + 0x0C
    AD7616_CHANNEL_12_VM_ADC                      = _GetPageOffset(14) + 0x10
    AD7616_CHANNEL_13_VM_ADC                      = _GetPageOffset(14) + 0x14
    AD7616_CHANNEL_14_VM_ADC                      = _GetPageOffset(14) + 0x18
    AD7616_CHANNEL_15_VM_ADC                      = _GetPageOffset(14) + 0x1C
    
    #///////////////////////////////////////////////////////////
    #Start defining _GetPageOffset(15 Register Map here
    #///////////////////////////////////////////////////////////
    #GPIO Register for FIVR DAC {290x0, VTARGERT_SHRTTEST_SEL_N, CURRENT_SOURCE_EN_N, VTARGET_00_SEL_N}
    FIVR_DAC_GPIO_CONTROL                         = _GetPageOffset(15) + 0x00
    DEBUG_LED_CTRL_GREEN                          = _GetPageOffset(15) + 0x04
    DEBUG_LED_CTRL_RED                            = _GetPageOffset(15) + 0x08
    #MAX14662 SD CONTROL {250x0, lcm03_gang_sd_n, lcm02_gang_sd_n, lcm01_gang_sd_n, lcm00_gang_sdn, isource_vsense_sd_n, st_vm_lb_sd_n, st_vt_lb_sd_n}
    MAX14662_SHUTDOWN_CONTROL                     = _GetPageOffset(15) + 0x0C

class HbiDpsFpga:
    BAR1 = 1
    HC_NUM_COMP_RELAYS = 9  #Each relay ties together adjacent channels, so 9 relays to connect 10 channels
    HC_COMP_SLAVE_BIT = 9
    HC_COMP_MASTER_BIT = 10

    HC_NUM_SYNC_RELAYS = 4  #Each relay ties together adjacent LTM4680 modules, so 4 relays to connect 5 modules
    HC_SYNC_SLAVE_BIT = 4
    HC_SYNC_MASTER_BIT = 5

    LC_NUMBER_OF_MODULES = 4
    LC_CHANNELS_PER_MODULE = 4
    ALL_LC_MODULES = list(range(LC_NUMBER_OF_MODULES))
    ALL_LC_CHANNELS = list(range(LC_NUMBER_OF_MODULES * LC_CHANNELS_PER_MODULE))

    #TODO: Re-evaluate SYNC control since it's per module, not per channel
    @staticmethod
    def HcGangControl(slot, channelNumbers, enableSlaveIn, enableMasterOut, turnOn):
        #Write to COMPA register
        compBits = 0
        for relayNumber in range(HbiDpsFpga.HC_NUM_COMP_RELAYS):
            #Each relay ties together adjacent channels, so 9 relays to connect 10 channels
            #Ex. relay 0 connects channel 0 and 1, hence checking for presence of channel x and x + 1 before turning on the relay
            channelA, channelB = relayNumber, relayNumber + 1
            if channelA in channelNumbers and channelB in channelNumbers:
                compBits = compBits | (1 << relayNumber)
        if enableSlaveIn:
            compBits = compBits | (1 << HbiDpsFpga.HC_COMP_SLAVE_BIT)
        if enableMasterOut:
            compBits = compBits | (1 << HbiDpsFpga.HC_COMP_MASTER_BIT)
        mask = 0x7FF #11 bits in the COMP registers
        HbiDpsFpga.ReadModifyWrite(slot, HbiDpsFpga.BAR1, BAR1.HC_GANG_COMPA_HCR_EN, compBits, mask, turnOn)

        #Write to COMPB register
        HbiDpsFpga.ReadModifyWrite(slot, HbiDpsFpga.BAR1, BAR1.HC_GANG_COMPB_HCR_EN, compBits, mask, turnOn)

        #Write to SYNC register
        #Need to determine modules which are present in the gang, since the relays tie entire modules together
        modules = [channel // 2 for channel in channelNumbers]
        syncBits = 0
        for relayNumber in range(HbiDpsFpga.HC_NUM_SYNC_RELAYS):
            #Each relay ties together adjacent LTM4680 modules, so 4 relays to connect 5 modules
            #Ex. relay 0 connects module 0 and 1, hence checking for presence of module x and x + 1 before turning on the relay
            moduleA, moduleB = relayNumber, relayNumber + 1
            if moduleA in modules and moduleB in modules:
                syncBits = syncBits | (1 << relayNumber)
        if enableSlaveIn:
            syncBits = syncBits | (1 << HbiDpsFpga.HC_SYNC_SLAVE_BIT)
        if enableMasterOut:
            syncBits = syncBits | (1 << HbiDpsFpga.HC_SYNC_MASTER_BIT)
        mask = 0x3F #6 bits total
        HbiDpsFpga.ReadModifyWrite(slot, HbiDpsFpga.BAR1, BAR1.HC_GANG_SYNC_HCM_EN, syncBits, mask, turnOn)

    @staticmethod
    def HcGangClearAll(slot):
        #Write to COMPA register
        HbiDpsFpga.BarWrite(slot, HbiDpsFpga.BAR1, BAR1.HC_GANG_COMPA_HCR_EN, 0)
        #Write to COMPB register
        HbiDpsFpga.BarWrite(slot, HbiDpsFpga.BAR1, BAR1.HC_GANG_COMPB_HCR_EN, 0)
        #Write to SYNC register
        HbiDpsFpga.BarWrite(slot, HbiDpsFpga.BAR1, BAR1.HC_GANG_SYNC_HCM_EN, 0)

    @staticmethod
    def HcRailControl(slot, channelNumber, turnOn):
        HbiDpsFpga.HcRailGroupControl(slot, [channelNumber], turnOn)

    @staticmethod
    def HcRailStatus(slot, channelNumber):
        registerValue = HbiDpsFpga.BarRead(slot, HbiDpsFpga.BAR1, BAR1.LTC4680_RUN)
        bitToCheck = 1 << channelNumber
        return (registerValue & bitToCheck) == bitToCheck

    @staticmethod
    def HcRailGroupControl(slot, channelNumbers, turnOn):
        mask = 0x000003FF
        bits = 0
        for channelNumber in channelNumbers:
            bits = bits | 1 << channelNumber
        HbiDpsFpga.ReadModifyWrite(slot, HbiDpsFpga.BAR1, BAR1.LTC4680_RUN, bits, mask, turnOn)

    #LC Module control turns the CONTROL pins on and off, which is the enable pin for the chip
    @staticmethod   
    def LcModuleControl(slot, moduleNumber, turnOn):
        HbiDpsFpga.LcModuleGroupControl(slot, [moduleNumber], turnOn)

    @staticmethod
    def LcModuleGroupControl(slot, moduleNumbers, turnOn):
        mask = 0x0000000F #Bits [3:0] represent the GPIO lines for the LC modules
        bits = 0
        for module in moduleNumbers:
            bits = bits | (1 << module)
        HbiDpsFpga.ReadModifyWrite(slot, HbiDpsFpga.BAR1, BAR1.LTC2975_CTRL_REGISTER, bits, mask, turnOn)

    #LC Gang switch control enables the switches for serial communication, effectively turning the switch IC on and off
    @staticmethod
    def LcGangSwitchGroupControl(slot, moduleNumbers, turnOn):
        mask = 0x00000078 #Bits [6:3] represent the GPIO lines for shutdown control of the MAX14662 switches
        bits = 0
        for module in moduleNumbers:
            bits = bits | (1 << (module + 3)) #3 is to shift the modules numbers into the correct range
        #Note the logic inversion - the BAR register exposes a "shutdown enable" bit... instead of "set 1 to shutdown", the function is "set 1 to enable"
        HbiDpsFpga.ReadModifyWrite(slot, HbiDpsFpga.BAR1, BAR1.MAX14662_SHUTDOWN_CONTROL, bits, mask, not turnOn)
        
    #LC Gang control manages the ganged signals for the LC modules
    @staticmethod
    def LcGangGroupControl(slot, channelsInGang, updateOnlyAffectedModules=True):
        #Per Travis, we're not using COMP, we are using TRACK, and both COMP and TRACK inter-rail connections need to be connected
        channelNumbersUsingCompCap, channelNumbersUsingTrackCap = [], HbiDpsFpga.ALL_LC_CHANNELS
        channelNumbersInGangComp, channelNumbersInGangTrack = channelsInGang, channelsInGang
        #Local chip number calculation function based on module and comp vs. track caps
        #Within the HIL calls, there are 2 LC modules managed by each hbiDpsLcmXAndYGangMax14662 write,
        # hence the // 2. If switching for the COMP RC network, it's an even chip number,
        # and TRACK caps are odd numbers.
        #TODO: Untested chip mapping, need to double-check before testing LC ganging
        getChipNumFunc = lambda module, isComp: 2 * module if isComp else 2 * module + 1

        hilWriteCallsByModuleTrackResistor = {
            0 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(0, False), data),
            1 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(1, False), data),
            2 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(2, False), data),
            3 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(3, False), data)
        }
        hilWriteCallsByModuleCompRc = {
            0 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(0, True), data),
            1 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(1, True), data),
            2 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(2, True), data),
            3 : lambda data : hil.hbiDpsLcmGangMax14662Write(slot, getChipNumFunc(3, True), data)
        }

        #Determine all of the modules that require switch modification
        if updateOnlyAffectedModules:
            modulesInvolved = set([channel // HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in channelNumbersInGangComp])
            modulesInvolved |= set([channel // HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in channelNumbersInGangTrack])
            modulesInvolved |= set([channel // HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in channelNumbersUsingCompCap])
            modulesInvolved |= set([channel // HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in channelNumbersUsingTrackCap])
        else:
            modulesInvolved = HbiDpsFpga.ALL_LC_MODULES

        #Loop through all switches
        for module in modulesInvolved:
            compMax14662Data = 0
            trackMax14662Data = 0
            for moduleChannel in range(HbiDpsFpga.LC_CHANNELS_PER_MODULE):
                instrumentChannel = HbiDpsFpga.LC_CHANNELS_PER_MODULE * module + moduleChannel #Correspondence to instrument level channel
                if instrumentChannel in channelNumbersUsingCompCap:
                    compMax14662Data = compMax14662Data | (1 << (2 * moduleChannel)) #Even channels correspond to the module-local channel's COMP RC network being connected
                
                #Check for gang configuration and OR in bits to bridge the comp pins of adjacent channels together
                if moduleChannel < HbiDpsFpga.LC_CHANNELS_PER_MODULE - 1 and instrumentChannel in channelNumbersInGangComp and instrumentChannel + 1 in channelNumbersInGangComp:
                    compMax14662Data = compMax14662Data | (1 << (2 * moduleChannel + 1)) #Odd channels correspond to linking adjacent channels together

                if instrumentChannel in channelNumbersUsingTrackCap:
                    trackMax14662Data = trackMax14662Data | (1 << (2 * moduleChannel)) #Even channels correspond to the module-local channel's TRACK capacitor being connected

                #Check for gang configuration and OR in bits to bridge the comp pins of adjacent channels together
                if moduleChannel < HbiDpsFpga.LC_CHANNELS_PER_MODULE - 1 and instrumentChannel in channelNumbersInGangTrack and instrumentChannel + 1 in channelNumbersInGangTrack:
                    trackMax14662Data = trackMax14662Data | (1 << (2 * moduleChannel + 1)) #Odd channels correspond to linking adjacent channels together
            hilWriteCallsByModuleCompRc[module](compMax14662Data)
            hilWriteCallsByModuleTrackResistor[module](trackMax14662Data)

    @staticmethod
    def ReadModifyWrite(slot, bar, address, bits, mask, setBits):
        oldRegister = HbiDpsFpga.BarRead(slot, bar, address)
        if setBits:
            newRegister = oldRegister | bits
        else:
            inverseAndMaskedBits = bits ^ (0xFFFFFFFF & mask)
            newRegister = oldRegister & inverseAndMaskedBits
        if oldRegister != newRegister:
            HbiDpsFpga.BarWrite(slot, bar, address, newRegister)
        elif Log.Enabled:
            print("Skipped HBI DPS BAR WRITE: Slot {}, BAR{}, {}({}), value={}".format(slot, bar, address.name, hex(address.value), hex(newRegister)))

    @staticmethod
    def BarRead(slot, bar, address):
        if not Sim.Enabled:
            registerValue = hil.hbiDpsBarRead(slot, bar, address.value)
        else:
            registerValue = 0xFFFFFFFF

        if Log.Enabled:
            print("HBI DPS FPGA BAR READ: Slot {}, BAR{}, {}({}), value={}".format(slot, bar, address.name, hex(address.value), hex(registerValue)))
        return registerValue

    @staticmethod
    def BarWrite(slot, bar, address, registerValue):
        if Log.Enabled:
            print("HBI DPS FPGA BAR WRITE: Slot {}, BAR{}, {}({}), value={}".format(slot, bar, address.name, hex(address.value), hex(registerValue)))
        if not Sim.Enabled:
            hil.hbiDpsBarWrite(slot, bar, address.value, registerValue)
        

