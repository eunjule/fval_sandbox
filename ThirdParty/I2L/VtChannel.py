from .HilWrap import HilWrap as hil
from .Utility import Sim, Log

class VtChannel:
    RESOURCE_TYPE = "Vt"
    NUMBER_OF_CHANNELS = 16
    def __init__(self, channelInstrument, channel):
        self.slot = channelInstrument.GetSlotId()
        self.channel = channel

    def GetChannelId(self):
        raise NotImplementedError()

    def GetChannelNumber(self):
        return self.channel

    def Apply(self):
        #TODO: Implement
        raise NotImplementedError()

    def GetResults(self):
        #TODO: Implement
        raise NotImplementedError()

    def GetName(self):
        return "{}Rail_Slot{}_Channel{}".format(VtChannel.RESOURCE_TYPE, self.slot, self.channel)

    def GetResourceType(self):
        return VtChannel.RESOURCE_TYPE

    def SetAttribute(self, attributeName, attributeValue):
        #TODO: Implement
        raise NotImplementedError()

    def SetAttributes(self, attributeMap):
        #TODO: Implement
        #AttributeMap is probably a dictionary of string to value
        raise NotImplementedError()

    def GetAttributes(self):
        #TODO: Implement
        #Return attributeMap is probably a dictionary of string to value
        raise NotImplementedError()

    def GetAttributesFromHw(self):
        #TODO: Implement
        #Return attributeMap is probably a dictionary of string to value
        raise NotImplementedError()

    def ResetAttributes(self):
        #TODO: Restore attributes to default values
        raise NotImplementedError()

if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()