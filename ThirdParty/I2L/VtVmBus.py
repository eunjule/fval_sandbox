from enum import Enum

from .HilWrap import HilWrap as hil
from .Utility import Sim, Log

class VtVmBus:

    class Load(Enum):
        Ohm2_0 = 0
        Ohm5_0 = 1
        Open = 2

    def __init__(self, channelInstrument):
        self.slot = channelInstrument.GetSlotId()

    def SelectLoad(self, load):
        #TODO: Implement
        raise NotImplementedError()

    def SelectChannel(self, channel):
        #TODO: Implement
        raise NotImplementedError()

    def SelectVTarget(self, channel):
        #TODO: Implement
        raise NotImplementedError()

    def SelectVMeasure(self, channel):
        #TODO: Implement
        raise NotImplementedError()

    def ClearAll(self):
        #TODO: Implement
        raise NotImplementedError()

    def ApplyToHw(self):
        #TODO: Implement
        raise NotImplementedError()

if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()