This file describes how to use the I2L-level Python scripts for the HBI DPS.

First, follow all steps outlined in README.txt. This includes loading the CPLD, FPGA, and programming BLT.
After those steps are completed, the I2L scripts will be able to run.

I2L File structure and usage:

	The I2L release contains several Python files. These are divided into modules based on hardware functionality.
	The main module that all I2L users will call into is hbiDps.py. Other modules should not be used directly, 
		except in the event that variable or constant definitions are needed.
	Please reference HcRailTest.py to see a fully-functional example of the intended usage of the scripts.
	
To use the HC or LC channels:
	- Follow the structure in HcChannelTest.py, HcGangTest.py, LcChannelTest.py, LcGangTest.py
	- The attributes that can be set (as well as their defaults and limits) are in these files
	- Make sure to either turn off the rail with the PowerSequence attribute = False, or call SafeState() on the rail object
	
Errata/Behavior notes:
	- HC Channels: FreeDriveRampTime of less than 0.5 ms (0.0005) might cause enough of a voltage spike to cause an overvoltage clamp


Release Notes:

Version 0.9.4:
- HC: Refactored ganging such that attributes can now be set on a ganged object. Setting attributes on the master channel still works
		for backwards compatibility reasons.
	NOTE: At some point, the ability to set attributes on the master channel will be removed.
- HC: Fixed some bugs with HC Channel initialization
- HC and LC: Moved constant PMBus register settings to initialization.
- HC: Added CrossboardGangTest.py as an example for targeted hardware testing in a PSDB fixture with two DPS instruments.
	NOTE: The exact functions/usage of I2L will change in the near future.

Version 0.9.3:
- LC: Fixed MFR_DISCHARGE_FAULT issue and added it back to statuses reported in CheckStatus()

Version 0.9.2:
- LC: Changed IClampLo to allow a 10ms deglitch period
- LC: Removed MFR_DISCHARGE_FAULT, DAC_CONNECTED, and statuses from the MFR_COMMON status register from appearing in CheckStatus()

Version 0.9.1:
- Fixed a hardcoded "slot 9" parameter in HbiDpsFpga.HcRailStatus()
- Added delay in between disabling MAX14662 shutdown pin and starting communication with the device
- Added STATUS_IOUT to the collection of registers that LcChannel.CheckStatus() reads if the corresponding status bit is set

Version 0.9.0:
- Added LC Ganging support
	- Gang can only be made within a single 4-channel module (ex. 0-3 is valid, 1-4 is not)
	- Can only gang one module at a time with this release
	- Current sharing isn't great, but matches what Travis was seeing with his scripts
	- Added LcGangTest.py to show example usage
- Updated initialization behavior so that calling hbiDps.GetInstrument(slot) multiple times
	will return a reference to the same object.
- Exposed Initialize() functions that restore initial state of the hardware.
- LC Channels: Updated over current fault response to have a 10ms deglitch period instead of 10ms
- HC Channels: Fixed GetAttributesFromHw() to return a value for PowerSequence that truly represented the state of the channel

Version 0.8.0:
- Added LC Channel support
- Added LcChannelTest.py to demonstrate basic usage.
	- The test loops over all channels and forces different voltages on different channels.
- ERRATA: This was tested on FPGA version F6000005, as there are I2C errors on F600000B

Version 0.7.2:
- HIL function compatibility update with HIL 4.50.0.255

Version 0.7.1:
- Fixed a bug where an unhandled exception would occur if the HC device has a MFR_SPECIFIC error.
- Version 0.7 had some updates that were lost, resulting in an incomplete release. Added what was missing,
	and up-revved to 0.7.1 to avoid confusion.

Version 0.7:
- Standardized on "Channel" for terminology instead of using "Rail" and "Channel" interchangeably
	- HcRailTest.py has been renamed to HcChannelTest.py
	- HcChannel.CheckRailStatus() has been renamed to HcChannel.CheckStatus()
- Added HC Gang functionality. See HcGangTest.py for example usage.
- Restructured object initialization, lifetime, and ownership. Ex. calling ChannelInstrument.GetChannel() multiple times
	will now return references to the same object instead of creating new objects.

Version 0.6:
- Fixed a bug where VForce on HC Channel could not be changed while the channel was on (PowerSequence = True)
- Fixed slowness when applying attributes to hardware

Version 0.5:
- HC Channels are functional, see HcRailTest.py for example usage