from .HcChannel import HcChannel
from .LcChannel import LcChannel
from .VtChannel import VtChannel
from .VmChannel import VmChannel
from .SourceSenseBus import SourceSenseBus
from .VtVmBus import VtVmBus
from .HcGang import HcGang
from .LcGang import LcGang
from .Utility import Sim, Log
from .HilWrap import HilWrap as hil

class ChannelInstrument:
    RESOURCE_TYPES = [HcChannel.RESOURCE_TYPE, LcChannel.RESOURCE_TYPE, VtChannel.RESOURCE_TYPE, VmChannel.RESOURCE_TYPE]

    def __init__(self, instrument):
        self.slot = instrument.GetSlotId()
        self.hcGang = HcGang(self)
        self.lcGang = LcGang(self)
        self.vtvmBus = VtVmBus(self)
        self.sourceSenseBus = SourceSenseBus(self)

        self.hcChannels = []
        for channelIndex in range(HcChannel.NUMBER_OF_CHANNELS):
            self.hcChannels.append(HcChannel(self, self.hcGang, channelIndex))

        self.lcChannels = []
        for channelIndex in range(LcChannel.NUMBER_OF_CHANNELS):
            self.lcChannels.append(LcChannel(self, self.lcGang, channelIndex))

        self.vtChannels = []
        for channelIndex in range(VtChannel.NUMBER_OF_CHANNELS):
            self.vtChannels.append(VtChannel(self, channelIndex))

        self.vmChannels = []
        for channelIndex in range(VmChannel.NUMBER_OF_CHANNELS):
            self.vmChannels.append(VmChannel(self, channelIndex))

    #TODO: Need a dtor?

    def Initialize(self):
        #TODO: Clear VtVmBus and CommonForceBus
        #TODO: Clear gangs

        HcChannel.SafestateAllChannels(self.slot)
        for hcChannel in self.hcChannels:
            hcChannel.Initialize()
        
        LcChannel.InitAllLcModules(self.slot)
        for lcChannel in self.lcChannels:
            lcChannel.Initialize()
        LcChannel.EnableAllChannels(self.slot)

        #TODO: Initialize Vtarget Channels

        #TODO: Initialize VMeasure Channels

        self.hcGang.Initialize()

    def ReleaseResources(self):
        raise NotImplementedError()

    def GetResourceTypes(self):
        return ChannelInstrument.RESOURCE_TYPES
    
    def GetAttributeDetails(self, resourceType):
        #Return type is map<std::string, IAttributeDetailsPtr>
        raise NotImplementedError()

    def GetChannelMap(self, resourceType):
        #Return type is vector<pair<int, int>>, not sure what it means
        raise NotImplementedError()

    def GetResourceDescription(self, resourceType):
        if resourceType == HcChannel.RESOURCE_TYPE:
            return "High current rail, driven by an LTC4688f integrated circuit."
        elif resourceType == LcChannel.RESOURCE_TYPE:
            return "Low(er) current rail, driven by an LTM2975f and LTM4644fe integrated circuit pair."
        elif resourceType == VtChannel.RESOURCE_TYPE:
            return "VTarget resource, which is a DAC used in the self-test circuitry."
        elif resourceType == VmChannel.RESOURCE_TYPE:
            return "VMeasure resource, which is an ADC used in the self-test circuitry."
        else:
            raise RuntimeError("Invalid resource type '{}'".format(resourceType))

    def GetChannel(self, dutId, channel, resourceType):
        if resourceType == HcChannel.RESOURCE_TYPE:
            if channel < 0 or channel >= len(self.hcChannels):
                raise RuntimeError('Invalid HC channel index {}. Valid channels are between 0 - {}'.format(channel, len(self.hcChannels)))
            return self.hcChannels[channel]
        elif resourceType == LcChannel.RESOURCE_TYPE:
            if channel < 0 or channel >= len(self.lcChannels):
                raise RuntimeError('Invalid LC channel index {}. Valid channels are between 0 - {}'.format(channel, len(self.lcChannels)))
            return self.lcChannels[channel]
        elif resourceType == VtChannel.RESOURCE_TYPE:
            if channel < 0 or channel >= len(self.vtChannels):
                raise RuntimeError('Invalid VT channel index {}. Valid channels are between 0 - {}'.format(channel, len(self.vtChannels)))
            return self.vtChannels[channel]
        elif resourceType == VmChannel.RESOURCE_TYPE:
            if channel < 0 or channel >= len(self.vmChannels):
                raise RuntimeError('Invalid VM channel index {}. Valid channels are between 0 - {}'.format(channel, len(self.vmChannels)))
            return self.vmChannels[channel]
        else:
            raise RuntimeError("Invalid resource type")

    def FormatAttributeAsDataType(self, value, attributeDataType):
        #Value is a variant, attributeDataType is a string, return is a string
        raise NotImplementedError()

    def QueryCast_SourceSenseBus(self):
        return self.sourceSenseBus

    def QueryCast_VtVmBus(self):
        return self.vtvmBus

    def QueryCast_HcGang(self):
        return self.hcGang

    def QueryCast_LcGang(self):
        return self.lcGang

    # Python helper functions that aren't in the interface
    def GetSlotId(self):
        return self.slot

    def Safestate(self):
        self.hcGang.DisableGang() #Turns off all rails in gang
        self.hcGang.Reset()

        HcChannel.SafestateAllChannels(self.slot)
        for hcChannel in self.hcChannels:
            hcChannel.Safestate()
            #self.hcChannels[channelIndex].Safestate()

        for lcChannel in self.lcChannels:
            lcChannel.Safestate()

        #TODO: Add safestate support for other rail types here

class Instrument:
    INSTR_CACHE = {}

    @staticmethod
    def GetCachedInstrument(slot):
        if slot not in Instrument.INSTR_CACHE.keys():
            Instrument.INSTR_CACHE[slot] = Instrument(slot)
        return Instrument.INSTR_CACHE[slot]

    def __init__(self, slot):
        self.slot = slot
        self.channelInstrument = ChannelInstrument(self)
        self.Initialize()
        
    #TODO: Need a dtor?

    def Initialize(self):
        if Sim.Enabled is False:
            #hil.hbiDpsComputeLocId(slot) #TODO: Confirm API Signature
            hil.hbiDpsConnect(self.slot)
            hil.hbiDpsInit(self.slot)
        self.channelInstrument.Initialize()

    def GetSlotId(self):
        if Log.Enabled:
            print("Slot is {}".format(self.slot))
        return self.slot
    
    def GetInstrumentId(self):
        #TBD, not sure what "Instrument ID" means
        raise NotImplementedError()
    
    def GetInstrumentName(self):
        return 'HBI DPS'

    def GetVendorId(self):
        return 1 #Intel is VendorID 1

    def GetVendorName(self):
        return 'Intel'

    def RequiresWaterCooling(self):
        return True

    def IsMultiBoardInstrument(self):
        return False

    def ResetStateToInitialValues(self):
        #TBD, requires HW reset maybe?
        raise NotImplementedError()

    def Safestate(self, setShutdownSafeState=False):
        #TODO: Interpret the setShutdownSafeState param and use it
        self.channelInstrument.Safestate()

    def PrepareForUnload(self):
        raise NotImplementedError()

    def GetBoardBltGen2(self):
        #TODO: Implement based on HIL
        raise NotImplementedError()

    def GetInstrumentAssemblyBltGen2(self):
        #TODO: Implement based on HIL
        raise NotImplementedError()

    def QueryCast_IChannelInstrument(self):
        return self.channelInstrument

    #Helpers for Power On
    def FpgaVersion(self):
        return hil.hbiDpsFpgaVersionString(self.slot)

    def FpgaUpdate(self, binFileName):
        hil.hbiDpsDeviceDisable(self.slot)
        hil.hbiDpsFpgaLoad(self.slot, binFileName)
        hil.hbiDpsDeviceEnable(self.slot)

    def FpgaBaseUpdate(self, binFileName):
        hil.hbiDpsFpgaBaseLoad(self.slot, binFileName)

    def CpldVersion(self):
        return hil.hbiDpsCpldVersionString(self.slot)

    def CpldUpdate(self, binFileName):
        return hil.hbiDpsCpldProgram(self.slot, binFileName)

def GetInstrument(slot):
    return Instrument.GetCachedInstrument(slot)

def GetI2lVersion():
    return 'Version 0.9.4'

def EnableSimulation():
    Sim.Enabled = True

def DisableSimulation():
    Sim.Enabled = False

def EnableLogging():
    Log.Enabled = True

def DisableLogging():
    Log.Enabled = False

if __name__ == "__main__":
    print("This module is meant to be imported and used, not run directly. Please use a script which utilizes this module instead.")
    exit()