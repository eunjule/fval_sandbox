from .HilWrap import HilWrap as hil
from .Utility import Sim, Log
from .HcChannel import HcChannel
from .HbiDpsFpga import *

class HcGang:
    #TODO: This does not work for mDUT. Need to re-visit when we care about getting it to work for mDUT
    #       That includes the entire structure and how this works.
    def __init__(self, channelInstrument):
        self.slot = channelInstrument.GetSlotId()
        self.channelInstrument = channelInstrument
        self.masterChannel = None
        self.slaveChannels = []

    def Initialize(self):
        self.Reset()

    def Reset(self):
        self.dutId = 0 #TODO: Fix for mDUT
        self.attributes = {}
        channelsToReset = self.slaveChannels.copy()
        if self.masterChannel is not None:
            channelsToReset.append(self.masterChannel)
        for channel in channelsToReset:
            channel = self.channelInstrument.GetChannel(self.dutId, self.masterChannel, HcChannel.RESOURCE_TYPE)
            channel.ClearGangConfiguration()
            channel.ResetAttributes()
        self.masterChannel = None
        self.slaveChannels = []
        self.DisableCrossBoardGangInput()
        self.DisableCrossBoardGangOutput()
        #Note that this configuration is not applied immediately... it will rely on the master HC Rail calling back into this object to initiate the correct sequence.

    def ConfigureAsMaster(self, channel):
        if self.masterChannel is not None:
            raise RuntimeError('Cannot configure a second channel as a master. The current master channel is {}; please configure it as a slave before setting a new master channel.'.format(self.masterChannel))
        hcChannel = self.channelInstrument.GetChannel(self.dutId, channel, HcChannel.RESOURCE_TYPE)
        hcChannel.SetAsMaster()
        self.masterChannel = channel

    def ConfigureAsSlave(self, channel):
        if channel in self.slaveChannels:
            return
        hcChannel = self.channelInstrument.GetChannel(self.dutId, channel, HcChannel.RESOURCE_TYPE)
        hcChannel.SetAsSlave()
        self.slaveChannels.append(channel)
        
        if self.masterChannel == channel:
            self.masterChannel = None
        
    def EnableCrossBoardGangOutput(self):
        self.crossBoardGangOutputEnabled = True

    def DisableCrossBoardGangOutput(self):
        self.crossBoardGangOutputEnabled = False

    def EnableCrossBoardGangInput(self):
        self.crossBoardGangInputEnabled = True

    def DisableCrossBoardGangInput(self):
        self.crossBoardGangInputEnabled = False

    def SetAttributes(self, attributeMap):
        for attribute, value in attributeMap.items():
            self.SetAttribute(attribute, value)

    def SetAttribute(self, attribute, value):
        self.attributes[attribute] = value

    def PrepareGang(self):
        self.ValidateGangConfiguration()
        self._TurnOffRailsInGang()
        self._DisconnectGangSignals()
        self._ApplyAttributesToMasterChannel()
        self._ApplyAttributesToSlaveChannels()
        self._ConfigureSlaves()
        #Note that master configuration needs to happen after slave configuration due to current code and the fact that the SYNC signal is potentially changed by both channels in the LTM4680 module.
        #For example, if ch0 is master and ch1 is slave, ch1 would disable SYNC output because it is a slave, and the master would re-enable it afterwards (master is the setting we want to stick)
        #There eventually needs to be more "smarts" for settings that affect both channels... potentially a refactor (maybe a Ltm4680 object "owns" two channels?).
        self._ConfigureMaster() 
        self._ConnectGangSignals()

    def StartGang(self):
        self._TurnOnRailsInGang()

    def SequenceGang(self):
        self.PrepareGang()
        self.StartGang()

    def StopGang(self):
        self._TurnOffRailsInGang()

    def DisableGang(self):
        self.StopGang()
        self._DisconnectGangSignals()
        self._ConfigureAllAsIndependent()
        
    def ValidateGangConfiguration(self):
        #Check for valid master channel, or that
        validChannels = range(HcChannel.NUMBER_OF_CHANNELS)
        if self.masterChannel is not None:
            if self.crossBoardGangInputEnabled is True:
                raise RuntimeError('Cannot have a master channel when crossboard gang input is enabled')
        else: #if self.masterChannel is None
            if self.crossBoardGangInputEnabled is False:
                raise RuntimeError('Either a master channel must be set or crossboard gang input must be enabled in order to create a gang.')
            elif self.crossBoardGangOutputEnabled is True:
                raise RuntimeError('Cannot enable crossboard gang output if no master rail exists in this gang object')

        #Check that all of the channel numbers are valid
        for channel in self.slaveChannels:
            if channel not in validChannels:
                raise RuntimeError('Internal error: invalid slave channel {}'.format(self.masterChannel))

        #Now check that they form a contiguous sequence (channels must be immediately adjacent to each other without gaps)
        allChannels = self.slaveChannels.copy()
        allChannels.append(self.masterChannel)
        allChannels.sort()

        lastChannel = None
        missingRails = []
        for channel in allChannels:
            if lastChannel is not None and channel != lastChannel + 1:
                missingRails = missingRails + [chan for chan in range(lastChannel, channel)]
            lastChannel = channel

        if len(missingRails) > 0:
            raise RuntimeError('Illegal gang - collection of channels are not contiguous. Ganged channels = {}, Missing channels= {}'.format(allChannels, missingRails))

        if self.crossBoardGangInputEnabled and 0 not in allChannels:
            raise RuntimeError('Illegal gang - cannot enable crossboard gang input without channel 0 being present in the gang.')

        channelNeededForGangOutput = HcChannel.NUMBER_OF_CHANNELS - 1
        if self.crossBoardGangOutputEnabled and channelNeededForGangOutput not in allChannels:
            raise RuntimeError('Illegal gang - cannot enable crossboard gang output without channel {} being present in the gang.'.format(channelNeededForGangOutput))

    def _TurnOffRailsInGang(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        HbiDpsFpga.HcRailGroupControl(self.slot, allChannels, False)

    def _TurnOnRailsInGang(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        HbiDpsFpga.HcRailGroupControl(self.slot, allChannels, True)

    def _ApplyAttributesToMasterChannel(self):
        #TODO: This probably breaks crossboard gang... a board doesn't necessarily need a master. It could all be slaves.
        #       The logic needs to be changed to check if crossboard gang input is enabled, and somehow need to get attributes
        #       in between instrument objects in the case of an instrument with all slave rails?
        #       Also need to check that, when crossboard gang output is enabled, that either a master exists or that the input is enabled.
        if self.masterChannel is not None:
            masterChannel = self.channelInstrument.GetChannel(self.dutId, self.masterChannel, HcChannel.RESOURCE_TYPE)
            masterChannel._DisableGangConfigCheck()
            masterChannel.SetAttributes(self.attributes)
            masterChannel.Apply()
            masterChannel._EnableGangConfigCheck()

    def _ApplyAttributesToSlaveChannels(self):
        for channel in self.slaveChannels:
            slaveChannel = self.channelInstrument.GetChannel(self.dutId, channel, HcChannel.RESOURCE_TYPE)
            slaveChannel._DisableGangConfigCheck()
            slaveChannel.SetAttributes(self.attributes)
            slaveChannel.Apply()
            slaveChannel._EnableGangConfigCheck()

    #This function internally sets the Ltm4680 registers required to designate a chip as a slave
    def _ConfigureSlaves(self):
        for channel in self.slaveChannels:
            slaveChannel = self.channelInstrument.GetChannel(self.dutId, channel, HcChannel.RESOURCE_TYPE)
            slaveChannel._SetSyncSlave()

    #This function internally sets the Ltm4680 registers required to designate a chip as a slave
    def _ConfigureMaster(self):
        #TODO: This probably breaks crossboard gang... a board doesn't necessarily need a master. It could all be slaves.
        #       The logic needs to be changed to check if crossboard gang input is enabled, and somehow need to get attributes
        #       in between instrument objects in the case of an instrument with all slave rails?
        #       Also need to check that, when crossboard gang output is enabled, that either a master exists or that the input is enabled.
        if self.masterChannel is None:
            raise RuntimeError('Internal Error: Master should have already been set')
        masterChannel = self.channelInstrument.GetChannel(self.dutId, self.masterChannel, HcChannel.RESOURCE_TYPE)
        masterChannel._SetSyncMaster()

    def _ConfigureAllAsIndependent(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        for channelNumber in allChannels:
            channel = self.channelInstrument.GetChannel(self.dutId, channelNumber, HcChannel.RESOURCE_TYPE)
            channel._SetSyncIndependent()
            channel.ClearGangConfiguration()

    def _ConnectGangSignals(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        allChannels.sort()
        HbiDpsFpga.HcGangControl(self.slot, allChannels, self.crossBoardGangInputEnabled, self.crossBoardGangOutputEnabled, True)

    def _DisconnectGangSignals(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        allChannels.sort()
        #Indicate which channels should be modified, including slave in/master out, and the final "False" means "Clear all"
        HbiDpsFpga.HcGangControl(self.slot, allChannels, self.crossBoardGangInputEnabled, self.crossBoardGangOutputEnabled, False)


if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()