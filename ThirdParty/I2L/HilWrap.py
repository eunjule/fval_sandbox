
from .Utility import *
from Common import hilmon as hil

class myDecorator:
    def __init__(self, func, isSimSafe):
        self.func = func
        self.isSimSafe = isSimSafe

    def __call__(self, *args):
        try:
            #Could add useful pre/post function call things like profiling, optional logging, bypass in SIM mode, etc.
            if Log.TranscriptEnabled is True:
                print('{}({})'.format(self.func.__name__, args))
            if not Sim.Enabled or self.isSimSafe:
                return self.func(*args)
        except RuntimeError as e:
            raise RuntimeError("Error while calling {}, args={}. HIL message: {}".format(self.func.__name__, args, e))
        except ValueError as e:
            raise ValueError("Parameter error while calling {}, args={}. Message: {}".format(self.func.__name__, args, e))
        except TypeError as e:
            raise TypeError("Type error while calling {}, args={}. Message {}".format(self.func.__name__, args, e))

def import_and_wrap_methods(decorator, mod, wrapExcludeList, simSafeList):
    def decorate(cls):
        for attr in mod.__dict__: # there's propably a better way to do this
            if callable(getattr(mod, attr)) and attr not in wrapExcludeList:
                setattr(cls, attr, decorator(getattr(mod, attr), attr in simSafeList))
            else:
                setattr(cls, attr, getattr(mod, attr))
        return cls
    return decorate

@import_and_wrap_methods(myDecorator, hil, ['cmds'], ['hilToL11', 'hilFromL11', 'hilToL16', 'hilFromL16'])
class HilWrap:
    @staticmethod
    def cmds(searchString):
        print('\n'.join(sorted(f for f in HilWrap.__dict__ if searchString.lower() in f.lower())))

    @staticmethod
    def hbiDpsLtm4680PmbusPagedWrite(slot, chip, page, command, writeData):
        #slot, chip, page, are UINTs, command is BYTE, writeData is bytestring
        HilWrap.hbiDpsLtm4680PmbusWrite(slot, chip, page, command, writeData)

    @staticmethod
    def hbiDpsLtm4680PmbusPagedRead(slot, chip, page, command, readCount):
        #slot, chip, page, are UINTs, command is BYTE, readCount is UINT. Return is a bytestring
        return HilWrap.hbiDpsLtm4680PmbusRead(slot, chip, page, command, readCount)

    @staticmethod
    def hbiDpsLtc2975PmbusPagedWrite(slot, chip, page, command, writeData):
        #slot, chip, page, are UINTs, command is BYTE, writeData is bytestring
        HilWrap.hbiDpsLtc2975PmbusWrite(slot, chip, page, command, writeData)

    @staticmethod
    def hbiDpsLtc2975PmbusPagedRead(slot, chip, page, command, readCount):
        #slot, chip, page, are UINTs, command is BYTE, readCount is UINT. Return is a bytestring
        return HilWrap.hbiDpsLtc2975PmbusRead(slot, chip, page, command, readCount)

    @staticmethod
    def ConvertToBytes(value, byteCount=2):
        byteArray = []
        for i in range(byteCount):
            byteArray.append((value >> 8 * i) & 0xFF)
        return bytes(byteArray)

    @staticmethod
    def ConvertToInt(byteString):
        x = 0
        for index in range(len(byteString)):
            x += byteString[index] << (8 * index)
        return x

if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()